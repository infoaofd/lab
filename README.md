# LAB
[[_TOC_]]
## 検索法

画面左上のサイバーをクリックすると, 検索窓が現れる。

![SEARCH_GITLAB](FIG/SEARCH_GITLAB.png)

## wiki

https://gitlab.com/infoaofd/lab/-/wikis/home

## GitLAB
https://gitlab.com/infoaofd/lab/-/blob/master/

### SSHのKeyの設定

![image-20241013144647977](README_IMAGE/image-20241013144647977.png)

<img src="README_IMAGE/image-20241013144809725.png" alt="image-20241013144809725" style="zoom: 50%;" />

### TortoiseGit  

TortoiseGitを用いたGitLabへの接続  
https://qiita.com/SkyLaptor/items/6347f38c8c010f4d5bd2  




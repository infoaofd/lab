# 接地層

[[_TOC_]]

## 接地層の定義

地表面付近の乱流を観測すると, 運動量, 熱, 水蒸気などの鉛直方向のフラックスが地表面での値とほぼ等しい層が形成されることがよくある。このような鉛直方向のフラックスが地表面での値とほぼ等しいとみなされる層のことを, 接地層と呼ぶ。

接地層 $\simeq$ フラックス一定の層



## 接地層の厚さの見積もり

運動方程式を用いて, 接地層の厚さを見積もる。いま, $u_r$, $v_r$をそれぞれ測定された速度の$x$, $y$方向成分, $f$を コリオリ係数, $p$を気圧, $\rho$を密度とすると, 運動方程式の$x$方向成分は,
$$
\begin{eqnarray}
\frac{du_r}{dt}=\frac{\partial u_r}{\partial t}+u_r\frac{\partial u_r}{\partial x}+v_r\frac{\partial u_r}{\partial y}+w_r\frac{\partial u_r}{\partial z}=-\frac{1}{\rho}\frac{\partial p}{\partial x}-f v_r
\end{eqnarray}\tag{1}
$$
$v_G$を地衡風の$y$方向成分とすると,

$v_G=-(1/\rho) \partial p / \partial x$であるので, (1)は,
$$
\begin{eqnarray}
\frac{\partial u_r}{\partial t}+u_r\frac{\partial u_r}{\partial x}+v_r\frac{\partial u_r}{\partial y}+w_r\frac{\partial u_r}{\partial z}=f(v_r-v_g)
\end{eqnarray}\tag{1}
$$
さらに連続の式,
$$
\frac{\partial u_r}{\partial x}+\frac{\partial v_r}{\partial y}+\frac{\partial w_r}{\partial x}=0
$$
から得られる,
$$
\begin{eqnarray}
u_r\frac{\partial u_r}{\partial x}+u_r\frac{\partial v_r}{\partial y}+u_r\frac{\partial w_r}{\partial x}=0
\end{eqnarray}\tag{2}
$$
を (1)に足すと, 積の微分公式 ($(uv)'=u'v+uv'$) より,
$$
\begin{eqnarray}
\frac{\partial u_r}{\partial t}+\frac{\partial u_ru_r}{\partial x}+\frac{\partial u_rv_r}{\partial y}+\frac{\partial u_rw_r}{\partial z}=f(v_r-v_g)
\end{eqnarray}\tag{3}
$$


$U$: 平均流の$x$方向成分

$V$: 平均流の$y$方向成分

$u$: 擾乱 (乱流) の$x$方向成分

$v$: 擾乱  (乱流) の$y$方向成分

と定義すると,
$$
\begin{eqnarray}
u_r= U + u\\
v_r= V + v\\
\end{eqnarray}
$$
である。これを(3)に代入すると,
$$
\begin{eqnarray}
&&\frac{\partial (U+u)}{\partial t}+\frac{\partial (U+u)(U+u)}{\partial x}+\frac{\partial (U+u)(V+v)}{\partial y}+\frac{\partial (V+v)(W+w)}{\partial z}\\
&=&f((V+v)-(V_g+v_g))
\end{eqnarray}\tag{4}
$$
 $\overline{}$を物理量の平均値と定義する。(4)の両辺の平均をとり, $\overline{u}=\overline{v}=\overline{w}=\overline{v_g}=0$を用いると,
$$
\begin{eqnarray}
&&\frac{\partial U}{\partial t}
+\frac{\partial UU}{\partial x}
+\frac{\partial \overline{uu}}{\partial x}
+\frac{\partial UV}{\partial y}
+\frac{\partial \overline{uv}}{\partial y}
+\frac{\partial UW}{\partial z}
+\frac{\partial \overline{uw}}{\partial z}
&=&f(V-V_g)
\end{eqnarray}\tag{5}
$$
である。ここで, $\partial U/\partial x + \partial U/\partial y + \partial W/\partial z=0$と, $dU/dt:=\partial u/ \partial t+u\partial u/ \partial x+ v\partial u/ \partial y + w\partial u/ \partial z$を用いることで,


$$
\begin{eqnarray}
&&\frac{\partial U}{d t}
+\frac{\partial uu}{\partial x}
+\frac{\partial uv}{\partial y}
+\frac{\partial uw}{\partial z}
&=&f(V-V_g)
\end{eqnarray}\tag{6}
$$
が得られる,

乱流による物理量の輸送は鉛直方向 ($z$方向) 成分 ($\partial uw / \partial z$) が卓越し, レイノルズ応力の$x$, $y$方向の変化 ($\partial uu/ \partial x, \partial uv / \partial y$)が無視できると仮定すると, 運動方程式の$x$方向成分の平均をとると,
$$
\begin{eqnarray}
\frac{\partial U}{\partial t}=f(V-V_g)+\frac{\partial (-\overline{uw})}{\partial z}
\end{eqnarray}\tag{1}
$$
が得られる。$V_g$は地衡風 ($V_g:= \partial \phi / \partial x$)である。いま平均流は定常状態にあると仮定する ($dU/dt=0$)。また, レイノルズ応力を$\tau$と書くことにすると,
$$
\tau:=-\rho \, \overline{uw}
$$
である。このとき(1)は, 
$$
\begin{eqnarray}
\frac{\partial \, \overline{\tau}}{\partial z}=-\rho f(V-V_g)
\end{eqnarray}\tag{2}
$$
となる。これを$z$で積分すると,
$$
\begin{eqnarray}
\tau(z)-\tau(0)=f \int_0^z \rho(V_g-V) \,dz
\end{eqnarray}\tag{2}
$$
が得られる。いま, $f \simeq 10^{-4} \, \mathrm{[s^{-1}]}$, $\rho \simeq \, \text{constant} = 1 \,\mathrm{[kg\,m^{-3}]}$, $V \simeq 0$とする。地衡風の大きさ=$G$, 地表風と地衡風の角度を$\alpha$とすると, $\alpha \ll 1$のとき, $V_g = G\sin \alpha \simeq G \alpha$ であるから,
$$
\begin{eqnarray}
\frac{\tau(z)-\tau(0)}{\tau(0)} \simeq \frac{\rho f z G \alpha}{\tau(0)}
\end{eqnarray}\tag{2}
$$
となる。いま$\tau(0)/(\rho G^2) \simeq 10^{-3}$, $\alpha \simeq 0.3 \, \mathrm {[rad]}\, (\simeq 13 \degree)$, $G\simeq 10 \, \mathrm{[m \, s^{-1}]}$とすると, 
$$
\begin{eqnarray}
\frac{\tau(z)-\tau(0)}{\tau(0)} \simeq 3 \times 10^{-3} \,z \quad \mathrm{[m]}
\end{eqnarray}\tag{2}
$$
左辺 $\simeq$ 0.1とすれば, 接地層の厚み$z$の見積もりとして, $z \simeq 30 \, \mathrm{[m]}$という値が得られる。 
# 大気熱力学

## 乾燥大気

### 乾燥大気の気体定数

$m_d$: 乾燥大気の分子量 $[\mathrm{kg\,mol^{-1}}]$
$$
R_v\equiv\frac{R^*}{m_v}\approx\frac{8.3 \, [\mathrm{J\,mol^{-1}\,K^{-1}}]}{29\times10^{-3}[\mathrm{kg\,mol^{-1}]}\approx 287\quad[\mathrm{J\,kg^{-1}\,K^{-1}}]}
$$



## 湿潤大気

乾燥大気と水蒸気（と大気水象）の混合気体

### 水蒸気の気体定数

$$
R_v\equiv\frac{R^*}{m_v}\approx\frac{8.3 \, [J\,mol^{-1}\,K^{-1}]}{18\times10^{-3}[kg\,mol^{-1}]}\approx 461\quad[J\,kg^{-1}\,K^{-1}]
$$



### ドルトンの分圧の法則

複数の物質から構成される混合気体を考える。$i$は混合気体を構成する$i$番目の気体のことを意味する。

モル数, $n_i$について, モル分率, $x_i \equiv n_i / \sum n_i$を定義する。混合気体の圧力を$p$で表し、全圧と呼ぶことにする。また、分圧, $p_i$を$p_i \equiv p x_i$と定義する。このとき,
$$
\sum p_i = p \sum x_i = p
$$
であることから, 全圧は分圧の和となることがわかる。これをドルトンの分圧の法則と呼ぶ。





### 水蒸気混合比

$M_d$: 気塊に含まれる乾燥大気の質量 $[kg]$
$M_v$: 気塊に含まれる水蒸気の質量 $[kg]$
$V$:  気塊の体積 $[m^3]$
$\rho_d$: 乾燥大気の密度 $[kg/m^3]$
$\rho_v$: 水蒸気の密度 $[kg/m^3]$

水蒸気混合比, $r$は, 
$$
r \equiv \frac{M_v}{M_d}=\frac{M_v/V}{M_d/V}=\frac{\rho_v}{\rho_d} \quad [kg/kg]\, or \, [g/kg]
$$
で定義される。$p_d$, $e$をそれぞれ乾燥空気の分圧, 水蒸気圧とすると,
$$
r=\frac{e/R_vT}{p_d/R_dT}=\frac{R_d}{R_v}\frac{e}{p_d}=\epsilon\frac{e}{p-e}.
$$
ここで,
$$
\epsilon \approx = R_d/R_v=m_v/m_d \approx 18/29 \approx 0.622.
$$

### クラウジウス―クラペイロン則（CC則）
$$
\frac{1}{e_s}\frac{de_s}{dT}=\frac{L}{RT^2}
$$

#### CC則の別表記

$$
L_v=T(s_v^*-s_l)
$$



### 全比エントロピー (Total specific entropy)



### 湿潤断熱減率

$$
\begin{eqnarray}
\Gamma_m & \equiv & - \frac{dT}{dz} \\
& = & \frac{-g(1+r_t) \biggl(\ 1+ \frac{L_vr}{R_d T} \biggr)}
{c_{pd}\biggl( 1+ \frac{c_{pv}r}{c_{pd}} \biggr) \biggl[ 1+ \frac{c_l r_l}{c_{pd}+c_{pv}r}+\frac{(1+\frac{r}{\epsilon})r L_v^2}{(c_{pd}+c_{pv}r)R_v T^2} \biggr]} \\
\end{eqnarray}
$$


# Thermodynamic constants and parameters

Emanuel, K, 1994: Atmospheric convection, Oxford University Press, 580 pp. (p. 546)

|Symbol|Name                             |Value |Unit |Source|
| ---- | --------------------------------| ---- | ---- | ---- |
| $R_d$|Gas constant of dry air      |287.04 | $\mathrm{J \, kg^{-1} \, K^{-1}}$ | 1 |
| $R_v$ | Gas constant of water vapor | 461.50 |$ \mathrm{J \, kg^{-1} \, K^{-1}}$ | 1 |
| $R'$ | Effective gas constant for mixture of dry air and H~2~O vapor | Variable | $ \mathrm{J \, kg^{-1} \, K^{-1}}$ |p. 111 (4.2.9) |
| $\epsilon$ | $R_d/R_v$ |0.6220| | |
| $c_{vd}$ | Heat capacity at constant <u>volume</u> for **dry air** |$719 \,\pm \, 2.5 $ | $\mathrm{J \, kg^{-1} \, K^{-1}}$ |1, 2|
| $c_{pd}$ | Heat capacity at constant <u>pressure</u> for **dry air** |$1005.7 \,\pm \, 2.5 $|$\mathrm{J \, kg^{-1} \, K^{-1}}$|1|
| $c_{vv}$ | Heat capacity at constant <u>volume</u> for **water vapor** |$1410 \,\pm \, 25 $|$\mathrm{J \, kg^{-1} \, K^{-1}}$|1, 2|
| $c_{vp}$ | Heat capacity at constant <u>pressure</u> for **water vapor** |$1870 \,\pm \, 25 $|$\mathrm{J \, kg^{-1} \, K^{-1}}$|1, 2|
| $c_{v}'$ | Effective heat capacity at constant <u>volume</u> for  **mixture of dry air and H~2~O vapor** |Variable|$\mathrm{ J \, kg^{-1} \, K^{-1}}$|p. 110 (4.2.4)|
| $c_{p}'$ | Effective heat capacity at constant <u>pressure</u> for  **mixture of dry air and H~2~O vapor** |Variable|$\mathrm{ J \, kg^{-1} \, K^{-1}}$|p. 110 (4.2.5)|
| $c_{l}$ | Heat capacity of liquid water |$4190 \,\pm \, 30 \, (for\,T \geq 0 \, ^oC)$Rises to $4770 \, (at \,T = - 40 \, ^oC)$|$\mathrm{J \, kg^{-1} \, K^{-1}}$|2|
| $c_i$ | Heat capacity of ice |$2106 + 7.3 T\,\pm \, 6 \, (T \, in \, ^oC)$|$\mathrm{J \, kg^{-1} \, K^{-1}}$|2|
| $L_v$ | Latent heat of vaporization |Variable||p. 115 (4.4.4)|
| $L_{v0}$ | Latent heat of vaporization at 0 ^o^C |$2.501 \times 10^6$|$\mathrm{J \, kg^{-1}}$|1|
| $L_{s}$ | Latent heat of sublimation |$2.834 \pm 0.01 \times 10^6 \, (-100^oC \leq T \leq 0^oC )$|$\mathrm{J \, kg^{-1}}$|2|
| $L_{f}$ | Latent heat of fusion |$0.3337  \times 10^6 \, (at \, 0^oC)$ $0.2357  \times 10^6 \, (at \, -40^oC)$|$\mathrm{J \, kg^{-1}}$|2|

p. 111, (4.2.9)
p. 110, (4.2.4)
p. 110, (4.2.5)
p. 115, (4.4.4)



**概略値 **
$\kappa\equiv R_d/C_p \approx $1005.7



**References**

1. List, R. J., 1951: Smithsonian meteorological Tables, 6th rev. ed., Smithsonian Institution Press, 527 pp, https://repository.si.edu/handle/10088/23746
2. Iribarne, J. V. and W. L. Godson, 1973: Atmospheric Thermodynamics, D. Riedel, 222 pp.




# RH TENDENCY EQUATION (Babic et al. 2019)

Babić, K., Adler, B., Kalthoff, N., Andersen, H., Dione, C., Lohou, F., Lothon, M., and Pedruzo-Bagazgoitia, X.: The observed diurnal cycle of low-level stratus clouds over southern West Africa: a case study, Atmos. Chem. Phys., 19, 1281–1299, https://doi.org/10.5194/acp-19-1281-2019, 2019.

[[_TOC_]]

## DESCRIPTION

The observed changes in RH are a result of temperature (*T*) and/or specific humidity (*q*) changes, i.e., RH increases due to increase in specific humidity and/or decrease in temperature. In order to quantify whether the *q* or *T* change has a stronger influence on the RH tendency and consequently on LLC formation, we determine their respective contributions using consecutive radiosonde measurements. The terms of the RH tendency equation are derived from the time derivative of $e/e_s$, where $e$ is the water vapor pressure and $e_s$ is the saturation water vapor pressure. In the next step, we incorporate the Clausius–Clapeyron relation and the definition of water vapor pressure, , where *T* is the air temperature in Kelvin, *p* is the air pressure in hPa, $L_v$ is the latent heat of vaporization ($2.5\times10^6$ J kg−1), and $R_v$=$461.5$ J kg−1 K−1 is the gas constant for water vapor. Finally, the contribution of absolute values and tendencies of *q* and *T* to RH tendency is calculated according to
$$
\frac{\partial RH}{\partial t}=\frac{P}{e_s}\frac{0.622}{(0.378q+0.622)^2}\frac{\partial q}{\partial t}-\frac{P}{e_s}\frac{qL_v}{(0.378q+0.622)R_vT^2}\frac{\partial T}{\partial t}
$$
Terms of Eq. ([1](https://acp.copernicus.org/articles/19/1281/2019/#Ch1.E1)) are calculated directly from available radiosonde measurements of <u>$RH$, $q$, $T$ and $p$</u>. In Eq. ([1](https://acp.copernicus.org/articles/19/1281/2019/#Ch1.E1)), 

(I)  observed RH tendency

(II) contribution from *q* change

(III) contribution from *T* change. 

Term (**III**) includes the minus sign, which means that **a positive value** of this term indicates **cooling** and vice versa.

## FORTRAN CODE

```
Lv=2.501E6'; Rv=461.5 !J/K
```

```
es=
```

```fortran
dRHdQ = P/es*(0.622)/(0.378*Qv+0.622)**2*dQ 
```

```
dRHdT =-P/es*(QV*Lv)/((0.378*Qv+0.622)*Rv*T**2)*DT
```



## GrADS CODE

```
'Lv=2.501E6'; 'Rv=461.5'
```

```BASH
'dQ=qv.2(time=$TIME2)-qv.2(time=$TIME1)'
'DT=temp.2(time=$TIME2)-temp.2(time=$TIME1)'
'Lv=2.501E6'; 'Rv=461.5'
'tc=(temp.2-273.15)'
'es=6.112*exp((17.67*tc)/(tc+243.5))' 
'dRHdQ=lev/es*(0.622)/pow((0.378*QV+0.622),2)*dQ*100'
'dRHdT=-lev/es*(QV*Lv)/((0.378*QV+0.622)*Rv*pow(temp,2))*DT*100'
```


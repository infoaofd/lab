#!/bin/bash
# /work09/am/00.WORK/2022.ECS2022/26.LFM.3/54.12.LFM_RH_TND

# RH TENDENCY EQUATION (Babic et al. 2019)
# https://gitlab.com/infoaofd/lab/-/blob/master/GFD/AOFD/40.00.THERMODYNAMICS/RH_TENDENCY_EQUATION_Babic_2019.md
# dRH=P/es*(0.622)/(0.378*Q+0.622)**2*dQ - P/es*(q*Lv)/((0.378*Q+0.622)*Rv*T**2)*DT
# Lv=2.501E6 #[J/kg]


GS=$(basename $0 .sh).GS

yyyy=2022; mm=06; mmm=JUN; dd1=18; hh1=23; dd2=19; hh2=00
TIME1=${hh1}Z${dd1}${mmm}${yyyy}
TIME2=${hh2}Z${dd2}${mmm}${yyyy}

lonw=126; lone=131; lats=29; latn=31.6
LEV1=850
FIG=$(basename $0 .sh)_${yyyy}${mm}${dd}_${hh1}_${LEV1}.pdf

KIND1="-kind white->mistyrose->lightpink->mediumvioletred->blue->dodgerblue->aqua"
CLEVS1='82 98 2'

KIND2='-kind maroon->saddlebrown->darkgoldenrod->khaki->white->white->palegreen->lightgreen->limegreen->darkgreen'
CLEVS2='10 16 0.5'

KIND3='-kind midnightblue->deepskyblue->green->wheat->orange->red->magenta'
CLEVS3='288 291 0.2'

CTL1=RADAR_10MIN.ctl
if [ ! -f $CTL1 ];then echo ERROR: NO SUCH FILE, $CTL1; exit 1; fi

CTL2=LFM00.CTL
if [ ! -f $CTL2 ];then echo ERROR: NO SUCH FILE, $CTL2; exit 1; fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"


cat <<EOF>$GS
'cc';'set datawarn off'

'open ${CTL1}'; 'open ${CTL2}'

'set vpage 0.0 8.5 0.0 11.0'
xmax = 3; ymax = 3; xwid = 7/xmax; ywid = 7/ymax
nmax=9

'set lon '${lonw}' '${lone}; 'set lat '${lats}' '${latn}
'set mpdset hires'
'set grid off';'set grads off'
'set xlab on';'set ylab on';'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'
'set xlint 2';'set ylint 1'

'set lev $LEV1'

nmap = 1; xmap=1; ymap = 1
'color $CLEVS1 $KIND1 -gxout shaded'
'set time $TIME1'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd rh.2'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' RH [%] ${TIME1}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



nmap = 1; xmap=1; ymap = 2
'color $CLEVS2 $KIND2 -gxout shaded'
'set time $TIME1'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd qv.2*1000'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' QV [g/kg] ${TIME1}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



nmap = 1; xmap=1; ymap = 3
'color $CLEVS3 $KIND3 -gxout shaded'
'set time $TIME1'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd temp.2'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' T [K] ${TIME1}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 4 -ft 3 -line on -edge circle'



nmap = 1; xmap=2; ymap = 1
'color $CLEVS1 $KIND1 -gxout shaded'
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd rh.2'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' RH [%] ${TIME2}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



nmap = 1; xmap=2; ymap = 2
'color $CLEVS2 $KIND2 -gxout shaded'
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd qv.2*1000'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' QV [g/kg] ${TIME2}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



nmap = 1; xmap=2; ymap = 3
'color $CLEVS3 $KIND3 -gxout shaded'
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd temp.2'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' T [K] ${TIME2}'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 4 -ft 3 -line on -edge circle'



'set gxout shaded'
'color -10 10 2 $KIND4 -gxout shaded'
nmap = 1; xmap=3; ymap = 1
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd rh.2(time=$TIME2)-rh.2(time=$TIME1)'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

#'set xlab off';'set ylab off'
#'set gxout contour';'set cint 0.5';'set ccolor 0';'set cthick 6'
#'set clab off'
#'d vpt.2'
#'set gxout contour';'set cint 0.5';'set ccolor 1';'set cthick 1'
#'d vpt.2'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' \`3D\`0RH [%]'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'


'set dfile 2'
'set lev $LEV1'
'dQ=qv.2(time=$TIME2)-qv.2(time=$TIME1)'
'DT=temp.2(time=$TIME2)-temp.2(time=$TIME1)'
'Lv=2.501E6'; 'Rv=461.5'
'tc=(temp.2-273.15)'
'es=6.112*exp((17.67*tc)/(tc+243.5))' 
'dRHdQ=lev/es*(0.622)/pow((0.378*QV+0.622),2)*dQ*100'
'dRHdT=-lev/es*(QV*Lv)/((0.378*QV+0.622)*Rv*pow(temp,2))*DT*100'

nmap = 1; xmap=3; ymap = 2
'color -10 10 2 $KIND4 -gxout shaded'
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd dRHdQ'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

'set xlab off';'set ylab off'

'WVCNV=-hdivg(wvx,wvy)*1E6'
'set gxout contour';'set clevs -1';'set ccolor 1';'set cthick 1';'set clstyle 2'
'set clab off'
'd WVCNV'
'set gxout contour';'set clevs  1';'set ccolor 1';'set cthick 1';'set clstyle 1'
'd WVCNV'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' \`3D\`0RH_QV [%]'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



nmap = 1; xmap=3; ymap = 3
'color -10 10 2 $KIND4 -gxout shaded'
'set time $TIME2'
xs = 0.5 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 9.5 - (ywid+0.10)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'd dRHdT'
'q dims';say sublin(result,4); 'q dims';say sublin(result,5)
'02.04.OBS.BOX.DASH.GS'

#'set xlab off';'set ylab off'
#'set gxout contour';'set cint 0.5';'set ccolor 0';'set cthick 6'
#'set clab off'
#'d vpt.2'
#'set gxout contour';'set cint 0.5';'set ccolor 1';'set cthick 1'
#'d vpt.2'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
'set strsiz 0.10 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.18
'draw string 'x' 'y' \`3D\`0RH_T [%]'
x1=xl; x2=xr; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

'set strsiz 0.13 0.15'; 'set string 1 c 3 0'
x=8.5/2; y=9.7
'draw string 'x' 'y' ${LEV1}hPa'



'gxprint $FIG'
'quit'
EOF

if [ -f $GS ];then 
grads -bcp "${GS}"
else
echo NO SUCH FILE, $GS; exit 1
fi
rm -vf $GS

if [ -f ${FIG} ]; then echo; echo FIG: ${FIG};echo; fi

exit 0

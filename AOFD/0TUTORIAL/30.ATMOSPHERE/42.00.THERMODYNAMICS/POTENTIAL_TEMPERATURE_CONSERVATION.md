# 温位の保存

一般気象学3章
$$
\begin{eqnarray}
\theta \left( 1+ \frac{\Delta \theta}{\theta} \right) &=& T \left( \frac{p_0}{p} \right)^{\frac{R_d}{C_p}} \left( 1+ \frac{\Delta T}{T} \right)\left( 1 + \frac{\Delta p}{p} \right)^{\frac{-R_d}{C_p}}. \\
\end{eqnarray}
$$

$$
\begin{eqnarray}
1+ \frac{\Delta \theta}{\theta} &=& \frac{T \left( \frac{p_0}{p}\right)^{\frac{R_d}{C_p}}}{\theta}  \left( 1+ \frac{\Delta T}{T} \right)\left( 1 + \frac{\Delta p}{p} \right)^{\frac{-R_d}{C_p}} \\
& \approx & \left( 1+ \frac{\Delta T}{T} \right)\left( 1 - \frac{R_d}{C_p}\frac{\Delta p}{p} \right) \\
& = & 1-\frac{R_d}{C_p}\frac{\Delta p}{p}+\frac{\Delta T}{T}-\frac{R_d}{C_p}\frac{\Delta p}{p}\frac{\Delta T}{T} \\
& \approx & 1-\frac{R_d}{C_p}\frac{\Delta p}{p}+\frac{\Delta T}{T}.
\end{eqnarray}
$$

$$
\begin{eqnarray}
\frac{\Delta \theta}{\theta} 
& = & \frac{\Delta T}{T}-\frac{R_d}{C_p}\frac{\Delta p}{p}.
\end{eqnarray}
$$


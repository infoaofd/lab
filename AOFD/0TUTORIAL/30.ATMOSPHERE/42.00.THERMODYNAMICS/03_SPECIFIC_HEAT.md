# 比熱

単位質量の物体の温度を$1 [\mathrm{K}]$上げるのに必要な熱量を比熱という。

## 定積比熱

気体の体積を一定に保った時の比熱のことを定積比熱という。



## 定圧比熱

気体の圧力を一定に保った時の比熱のことを定積比熱という。この場合，気体の膨張・圧縮に伴う仕事の影響を考慮する必要がある。



## 

## 記号

$\Delta U$: 系の内部エネルギ―の変化量 $[\mathrm{J}]$

$\Delta Q$: 系に加えられた熱エネルギー $[\mathrm{J}]$

$\Delta W$: 系に加えられた仕事 $[\mathrm{J}]$



## 熱力学第一法則

$$
\begin{eqnarray}
\Delta U = \Delta Q + \Delta W
\end{eqnarray}
$$
が成り立つ。これを熱力学第一法則と呼ぶ。単位質量あたりの量を小文字で表すことにすると, 
$$
\begin{eqnarray}
\Delta u = \Delta q + \Delta w
\end{eqnarray}
$$

となる。$\Delta w$として気体の膨張・圧縮に伴う仕事を考えると,
$$
\begin{eqnarray}
\Delta w = -p(\Delta \alpha)
\end{eqnarray}
$$
このとき, 熱力学第一法則は, 
$$
\Delta q = \Delta u + p(\Delta \alpha)
$$
となる。


# 混合比から飽和混合比を計算する

水蒸気混合比$q_v$
$$
q_v:=\frac{\rho_v}{\rho_d}
$$

$$
q_v=
\frac{e/(R_vT)}{p_d/(R_dT)}=
\frac{R_d}{R_v}\frac{e}{p-e}=
\epsilon \frac{e}{p-e}=\epsilon\frac{e}{p_d}
$$

$$
q_v(p-e)=\epsilon e \\
\epsilon e + q_v e = q_v p \\
\therefore e=\frac{q_v p}{\epsilon + q_v}
$$

相対湿度, $RH$
$$
RH:=\frac{e}{e_s} \\

= \frac{\frac{q_v p}{e+q_v}}{\frac{q_sp}{e+q_s}} \\

= \frac{q_v}{q_s}\bigg( \frac{\epsilon + q_s}{\epsilon + q_v}\bigg)
$$

$$
\therefore \quad q_s=\frac{\epsilon q_v}{RH(\epsilon+q_v)-q_v}
$$

$q_v \sim 0.01 \ll 0.1 $, $\epsilon \simeq 0.622$より, 下記の近似式が得られる。
$$
q_s \simeq q_v/RH
$$
$0.02/0.622 \simeq 0.03$なので, 誤差は3%程度である。

ただし, $RH$は$\%$標記から, $0<RH<1$に直しておく。

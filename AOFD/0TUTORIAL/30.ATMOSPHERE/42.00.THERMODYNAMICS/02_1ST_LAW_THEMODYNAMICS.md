# 熱力学第一法則

## 記号

$\Delta U$: 系の内部エネルギ―の変化量 $[\mathrm{J}]$

$\Delta Q$: 系に加えられた熱エネルギー $[\mathrm{J}]$

$\Delta W$: 系に加えられた仕事 $[\mathrm{J}]$



## 熱力学第一法則

$$
\begin{eqnarray}
\Delta U = \Delta Q + \Delta W
\end{eqnarray}
$$
が成り立つ。これを熱力学第一法則と呼ぶ。**単位質量あたり**の量を**小文字**で表すことにすると, 
$$
\begin{eqnarray}
\Delta u = \Delta q + \Delta w
\end{eqnarray}
$$

となる。$\Delta w$として気体の膨張・圧縮に伴う仕事を考えると, $\alpha$は単位質量当たりの体積であるから，
$$
\begin{eqnarray}
\Delta w = -p(\Delta \alpha)
\end{eqnarray}
$$
このとき, 熱力学第一法則は, 
$$
\Delta q = \Delta u + p(\Delta \alpha)
$$
となる。


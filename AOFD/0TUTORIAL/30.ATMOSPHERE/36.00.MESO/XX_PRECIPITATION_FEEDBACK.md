# フィードバックによる降水強化

[[_TOC_]]

水蒸気収支と乾燥静的エネルギーの収支を組み合わせることにより, 水蒸気量増加に対する降水量の増分を定量化する。



## 記号

$q$: 水蒸気混合比 $[\mathrm{kg/kg}]$

$c_p$: 定圧比熱 $[\mathrm{J/K/kg}]$

$s$: 乾燥静的エネルギー (dry static energy; DSE) $[\mathrm{J/kg}]$
$$
s:=c_p T + gz
$$
$L$: 蒸発の潜熱 $[\mathrm{J/K/kg}]$ ($0^{\degree}\mathrm{C}$で$2.501 \times 10^6$) 

$m$: 乾燥静的エネルギー (moist static energy; MSE) 
$$
m:=s+Lq=c_p T + gz+Lq \, [\mathrm{J/kg}]
$$
$\braket{X}$: 物理量$X$の鉛直積分
$$
\braket{X}:=\int_0^{Z_T}\rho dz \simeq \int_0^{p_s}X\frac{dp}{g} \quad \bigg( \because  \, dz\simeq -\frac{dp}{\rho g}\bigg)
$$
$\partial_\alpha$ := $\frac{\partial}{\partial \alpha}$ (偏微分記号の略記法)

$\nabla := \mathbf{i} \frac{\partial}{\partial x} + \mathbf{j} \frac{\partial}{\partial y}$

$\mathbf{v}:=u\mathbf{i}+v\mathbf{j}$



## 基本法則

### 水蒸気収支

$$
\begin{eqnarray}
\partial_t q+\nabla \cdot(\mathbf{v}q)+\frac{\partial}{\partial q}(\omega q)=q_s
\tag{1}
\end{eqnarray}
$$

### DSE収支

$$
\begin{eqnarray}
\partial_t s+\nabla \cdot(\mathbf{v}s)+\frac{\partial}{\partial q}(\omega s)=Q_c+R_a+H\tag{2}
\end{eqnarray}
$$

$Q_c$, $R_a$, $H$: 凝結, 放射, 顕熱輸送による加熱率 $[\mathrm{J/kg/s}]$



## 収支式の鉛直積分

### 水蒸気収支の鉛直積分

$\braket{\mathrm{(1)}式 }$を計算する。まず, $\braket{\partial_p(\omega q)}$を変形する。
$$
\braket{\partial_p(\omega q)}=\frac{1}{g}\int_0^{p_s}\frac{\partial}{\partial p}(\omega q)dp = \frac{1}{g}\bigg[(\omega q)\bigg|_{p=p_s}-(\omega q)\bigg|_{p=0}\bigg]=0
\tag{3}
$$
であるから,
$$
\begin{eqnarray}
&&\braket{\nabla \cdot (\mathbf{v}q)}+\underbrace{\braket{\partial_p(\omega q)}}_{=0} \\
&=&
\underbrace{\braket{\mathbf{v} \cdot \nabla q}+ \braket{q \nabla \cdot \mathbf{v}}}_{\because \,(fg)'=f'g+fg'}+\underbrace{\braket{\omega\partial_p q}+\braket{q\partial_p \omega}}_{=0} \\
&=&
\braket{\mathbf{v} \cdot \nabla q}+\braket{\omega \partial_p q} +\braket{q( \underbrace{\nabla \cdot \mathbf{v}+ \partial_p \omega}_{=0})} 
\end{eqnarray}
$$

$$
\begin{eqnarray}
\therefore \, \braket{\nabla \cdot (\mathbf{v}q)}=\braket{\mathbf{v} \cdot \nabla q}+\braket{\omega \partial_p q}
\tag{4}
\end{eqnarray}
$$

$E$を地表面からの蒸発量, $P$を降水量 $[\mathrm{J/m^2/s}]$とすると,
$$
\begin{eqnarray}
\braket{q_s}=\int_0^{p_s}q_s\frac{dp}{g}=\int_0^{Z_T}\rho q_s dz = E-P
\tag{5}
\end{eqnarray}
$$
(1), (5)より, 
$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\nabla \cdot(\mathbf{v}q)}=E-P
\tag{1a}
\end{eqnarray}
$$
左辺第2項を展開すると,
$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\mathbf{v} \cdot \nabla q}+ \braket{q \nabla \cdot \mathbf{v}}=E-P
\tag{6}
\end{eqnarray}
$$
さらに(1a)は(4)を使うと,
$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\mathbf{v} \cdot \nabla q}+\braket{\omega\partial_p q}=E-P
\tag{1b}
\end{eqnarray}
$$
と書ける。



### DSE収支の鉛直積分

$F_s$を$F_s:=\braket{R_a}+\braket{H}$と定義する。$\braket{\mathrm{(2)}式 }$は(1b)と同様に,
$$
\begin{eqnarray}
\braket{\partial_t s}+\braket{\mathbf{v} \cdot \nabla s}+\braket{\omega\partial_p s}=\braket{Q_c}+F_s
\tag{}
\end{eqnarray}
$$
ここで, $s=c_pT+gz$なので, $\partial s=\partial_t(C_p T)$である。また，

$\braket{\mathbf{v} \cdot \nabla s}=\braket{\mathbf{v} \cdot \nabla (c_p T)}$である。よって, $\braket{\mathrm{(2)}式 }$は
$$
\begin{eqnarray}
\braket{\partial_t (c_p T)}+\braket{\mathbf{v} \cdot \nabla (c_p T)}+\braket{\omega\partial_p s}=\braket{Q_c}+F_s
\tag{}
\end{eqnarray}
$$
ここで，
$$
\begin{eqnarray}
\braket{Q_c}=LP
\tag{}
\end{eqnarray}
$$
と近似する。いま水平収束, $-\nabla \cdot \mathbf{v}$を$C$と書くことにすると ($C:=-\nabla \cdot \mathbf{v}$), 連続の式$\nabla \cdot \mathbf{v}+ \partial_p \omega=0$より,
$$
\begin{eqnarray}
C= \partial_p \omega
\tag{}
\end{eqnarray}
$$
となる。

## 鉛直積分の式のまとめ

### 水蒸気収支

$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\nabla \cdot(q\mathbf{v})}=Q
\end{eqnarray}
$$

### DSE収支

$$
\begin{eqnarray}
\braket{\partial_t s}+\braket{\nabla \cdot(s\mathbf{v})}=S
\end{eqnarray}
$$

### Leading Order Balance

#### 水蒸気収支

$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\mathbf{v} \cdot \nabla q}+ \braket{q \nabla \cdot \mathbf{v}}=E-P
\tag{6}
\end{eqnarray}
$$

のLeading order balanceは,
$$
\begin{eqnarray}
 \braket{q \nabla \cdot \mathbf{v}}\simeq-P
\tag{}
\end{eqnarray}
$$
となることが多い。$C:=-\nabla \cdot \mathbf{v}$を使うと, 
$$
\begin{eqnarray}
 \braket{q C}\simeq P
\tag{7}
\end{eqnarray}
$$
である。また，連続の式$\nabla \cdot \mathbf{v}+ \partial_p \omega=0$より$C=-\partial_p \omega$であるから, 
$$
\begin{eqnarray}
 \braket{q \partial_p \omega}\simeq P
\tag{8}
\end{eqnarray}
$$
さらに, 
$$
\begin{eqnarray}
 \braket{q \partial_p \omega}&\simeq& \frac{1}{g}\int_0^{p_s}q\frac{\partial \omega}{\partial p}\frac{dp}{g}
=\frac{1}{g}\bigg[q \omega \bigg]^{p_s}_0-\frac{1}{g}\int_0^{p_s}\omega\frac{\partial q}{\partial p}\frac{dp}{g} \\
&=& - \braket{\omega{\partial_pq}}
\tag{}
\end{eqnarray}
$$
を使うと,
$$
\begin{eqnarray}
- \braket{\omega \partial_p q}\simeq P
\tag{8}
\end{eqnarray}
$$
となる。



#### DSE収支

DSE収支,
$$
\begin{eqnarray}
\braket{\partial_t s}+\braket{\mathbf{v} \cdot \nabla s}+ \braket{s \nabla \cdot \mathbf{v}}=LP+F_s
\tag{9}
\end{eqnarray}
$$
のleading order balanceは,
$$
-\braket{s C}\simeq LP
$$
となる。これより, 水蒸気収支と同様に，
$$
\begin{eqnarray}
\braket{s \partial_p \omega}\simeq -LP
\tag{10} \\
\braket{\omega \partial_p s}\simeq LP
\tag{11} \\
\end{eqnarray}
$$
が言える。



## 気候値からの偏差

### 記号

$\overline{X}:=$$X$という量のhistorical気候値からの偏差

$X'$:=$X$の$\overline{X}$からの偏差 ($X':=X-\overline{X}$)



### $q’$の収支式

$$
\begin{eqnarray}
\braket{\partial_t q}+\braket{\nabla \cdot(q\mathbf{v})}=E-P
\tag{1a}
\end{eqnarray}
$$

$q':=q-\bar{q}$, $u':=u-\bar{u}$, $v':=v-\bar{v}$, $\mathbf{v}:=u\mathbf{i}+v\mathbf{j}$,  $\overline{\mathbf{v}}:=\overline{u}\mathbf{i}+\overline{v}\mathbf{j}$, $\mathbf{v}':=u'\mathbf{i}+v'\mathbf{j}$と定義する。

まず，$\nabla \cdot (q\mathbf{v})=\frac{\partial (qu)}{\partial x}+\frac{\partial (qv)}{\partial y}$の右辺第1項($\frac{\partial (qu)}{\partial x}$)を変形する。
$$
\begin{eqnarray}
\frac{\partial (qu)}{\partial x}&=&\frac{\partial }{\partial x}(\overline{q}+q')(\overline{u}+u') \\
&=& \frac{\partial }{\partial x}(\bar{q}\bar{u})
+\frac{\partial }{\partial x}(\bar{q}u') 
+\frac{\partial }{\partial x}(q'\bar{u}) 
+\frac{\partial }{\partial x}(q'u') \\
&=& \underbrace{\frac{\partial }{\partial x}(\bar{q}\bar{u})}_{あ}
+\underbrace{\bar{q}\frac{\partial u'}{\partial x}}_{い}
+\underbrace{u'\frac{\partial \bar{q}}{\partial x}}_{う}
+\underbrace{q'\frac{\partial \bar{u}}{\partial x}}_{え} 
+\underbrace{\bar{u}\frac{\partial q'}{\partial x}}_{お}
+\underbrace{\frac{\partial }{\partial x}(u'q')}_{か} \\
\tag{}
\end{eqnarray}
$$
$\frac{\partial (vq)}{\partial x}$についても同様に展開できるので, 
$$
\begin{eqnarray}
\nabla \cdot(q\mathbf{v})
&=&\underbrace{\nabla \cdot (\bar{q}\bar{\mathbf{v}})}_{あ}
+\underbrace{\bar{q} \nabla \cdot \mathbf{v}'}_{い}
+\underbrace{\mathbf{v}' \cdot \nabla \bar{q}}_{う}
+\underbrace{q'\nabla \cdot \bar{\mathbf{v}}}_{え}
+\underbrace{\bar{\mathbf{v}} \cdot \nabla q'}_{お}
+\underbrace{\nabla \cdot (q' \mathbf{v}')}_{か}
\tag{}
\end{eqnarray}
$$
が得られる。よって，
$$
\begin{eqnarray}
\braket{\nabla \cdot(q\mathbf{v})}
&=&\braket{\nabla \cdot (\bar{q}\bar{\mathbf{v}})}
+\braket{\bar{q} \nabla \cdot \mathbf{v}'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
+\braket{q'\nabla \cdot \bar{\mathbf{v}}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}
\tag{}
\end{eqnarray}
$$
となる。

いま，$\overline{C}:=-\nabla \cdot \overline{\mathbf{v}}$, $C':=-\nabla \cdot \mathbf{v}'$と定義すると, 上の式は
$$
\begin{eqnarray}
\braket{\nabla \cdot(q\mathbf{v})}
&=&\braket{\nabla \cdot (\bar{q}\bar{\mathbf{v}})}
-\braket{\bar{q} C'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
-\braket{q'\overline{C}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}
\tag{}
\end{eqnarray}
$$
と書ける。これを使うと，(1a)は,
$$
\begin{eqnarray}
&&
\braket{\partial_t \bar{q}}+\braket{\partial_t q'}
+\braket{\nabla \cdot (\bar{q}\bar{\mathbf{v}})}
-\braket{\bar{q} C'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
-\braket{q'\overline{C}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}\\
&=&(\overline{E}-\overline{P})+(E'-P')
\end{eqnarray}
\tag{12}
$$
となる。

気候場について，
$$
\begin{eqnarray}
&&
\braket{\partial_t \bar{q}}
+\braket{\nabla \cdot (\bar{q}\bar{\mathbf{v}})}=\overline{E}-\overline{P}
\tag{}
\end{eqnarray}
$$
が成り立っているとき，(12)は,
$$
\begin{eqnarray}
\braket{\partial_t q'}
-\braket{\bar{q} C'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
-\braket{q'\overline{C}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}=E'-P'
\end{eqnarray}
\tag{13}
$$
となる。よって，水蒸気の気候場からの偏差に関する式として
$$
\begin{eqnarray}
&&
\underbrace{\braket{q'\overline{C}}}_{\mathrm{THM}}+\underbrace{\braket{\bar{q} C'}}_{\mathrm{DYN}}
+\big(
\underbrace{
-\braket{\partial_t q'}
-\braket{\mathbf{v}' \cdot \nabla \bar{q}}
-\braket{\bar{\mathbf{v}} \cdot \nabla q'}
-\braket{\nabla \cdot (q' \mathbf{v}')}
}_{\mathrm{RES}}\big)
=P'-E'
\end{eqnarray}
\tag{14}
$$
を得ることが出来る。ここで，

$\mathrm{THM}$: Thermodynamic term (熱力学項)

$\mathrm{DYN}$: Dynamic term (力学項)

$\mathrm{RES}$: Residual term (残差項)

と呼ぶことにする。熱力学項はDynamic moisture effect (水蒸気の直接効果)と呼ばれることもある。(14)で，残差項$\mathrm{RES}$と蒸発量偏差$E'$を無視すると，
$$
\begin{eqnarray}
&&
\underbrace{\braket{q'\overline{C}}}_{\mathrm{THM}}+\underbrace{\braket{\bar{q} C'}}_{\mathrm{DYN}}
\simeq P'
\end{eqnarray}
\tag{15}
$$
が得られる。



### $s’$の収支式

$\braket{Q_c}=LP$と近似し，$F_s$を無視すると, DSE収支
$$
\begin{eqnarray}
\braket{\partial_t s}+\braket{\nabla \cdot(s\mathbf{v})}=\braket{Q_c}+F_s
\tag{}
\end{eqnarray}
$$
についても(15)を求めた時と同様の手順により，
$$
\begin{eqnarray}
&&
-\braket{s'\overline{C}}-\braket{\bar{s} C'}=LP'
\end{eqnarray}
\tag{16}
$$
が得られる。(15)と(16)を組み合わせると,
$$
\braket{q'\overline{C}}+\braket{\bar{q} C'}=-\braket{s'\overline{C}}/L-\braket{\bar{s} C'}/L+\cdots
$$
$\cdots$は無視された項を表す。これより，
$$
\begin{eqnarray}
\braket{(s'+Lq')\overline{C}}=-\braket{(\bar{s}+L\bar{q}) C'}+\cdots
\end{eqnarray}
\tag{17}
$$
となる。(17)は，左辺の水蒸気量やDSEの変化($s'$と$q'$)に応じて右辺の収束の変化($C'$)が起こることを意味する。すなわち，収束は水蒸気量と独立に変化するのではなく，収束の変化($\simeq$上昇流の変化)のために，水蒸気量やDSEの変化が要請される。



## フィードバックの定量化

### C-C Scaling

飽和蒸気圧に関するClausius-Clapeyron (C-C)の式より，
$$
\frac{q'/\bar{q}}{T'}\simeq 6-7 \,[\mathrm{\%/K}]
$$
を示すことが出来る。これをC-C則と呼ぶことがある。

豪雨が発生するとき，大気中の水蒸気はほぼ飽和した状態にあるはずである (Peters and Neelin, 2006)。このため，気温1Kあたりの降水量の変化率 ($\mathrm{\%/K}$)は，大気境界層の中の飽和水蒸気混合比の変化率に等しくなるはずであると考えられる。飽和水蒸気圧の変化率がC-C則に従うのであれば，気温1Kあたりの降水量の変化率もC-C測に従い，$6-7 \,[\mathrm{\%/K}]$になると考えられる。これをC-C Scalingと呼ぶ。

一方，これを超える降水量の増加率を示す研究結果もあり，降水量がC-C Scalingを超える増加率を示すことをSuper C-C Scalingと呼ぶことがある。

### Super C-C Scaling

$\mathbf{v}_s'$を地表付近の風速ベクトルの水平方向成分とし，
$$
C_s':=-\nabla \cdot \mathbf{v}_s'
$$
と定義する。さらに，
$$
M_s:=-\frac{\braket{\overline{s}C'}}{C_s'}, \quad M_q:=\frac{\braket{L\overline{q}C'}}{C_s'}
$$

$$
M:=M_s-M_q
$$

と定義する。$M$はGross Moist Stabilityと呼ばれることがある。

水蒸気偏差の収支式
$$
\begin{eqnarray}
\braket{\partial_t q'}
-\braket{\bar{q} C'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
-\braket{q'\overline{C}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}=E'-P'
\end{eqnarray}
\tag{13}
$$
とDSE偏差の収支
$$
\begin{eqnarray}
\braket{\partial_t s'}
-\braket{\bar{s} C'}
+\braket{\mathbf{v}' \cdot \nabla \bar{s}}
-\braket{s'\overline{C}}
+\braket{\bar{\mathbf{v}} \cdot \nabla s'}
+\braket{\nabla \cdot (s' \mathbf{v}')}=LP'
\end{eqnarray}
$$
を組み合わせると，降水量の変化$P'$は
$$
\begin{eqnarray}
P'=\braket{q'\overline{C}}
+\frac{M_q}{M}\bigg(
\braket{q'\overline{C}}+L^{-1}\braket{s'\overline{C}}
-L^{-1}R_s
\bigg)
+\frac{M_s}{M}\bigg(E'-R_q\bigg)
\end{eqnarray}
\tag{18}
$$
と表すことが出来る。ここで，
$$
\begin{eqnarray}
R_s:=\braket{\partial_t s'}
+\braket{\mathbf{v}' \cdot \nabla \bar{s}}
+\braket{\bar{\mathbf{v}} \cdot \nabla s'}
+\braket{\nabla \cdot (s' \mathbf{v}')}
\end{eqnarray}
$$

$$
\begin{eqnarray}
R_q:=\braket{\partial_t q'}
+\braket{\mathbf{v}' \cdot \nabla \bar{q}}
+\braket{\bar{\mathbf{v}} \cdot \nabla q'}
+\braket{\nabla \cdot (q' \mathbf{v}')}
\end{eqnarray}
$$

である。



(18)において，Leading order terms以外を無視すると，
$$
\begin{eqnarray}
P'&\simeq&
\underbrace{\braket{q'\overline{C}}}_{\mathrm{THM}}
+\underbrace{
\frac{M_q}{M}\bigg(
\braket{q'\overline{C}}+L^{-1}\braket{s'\overline{C}}
\bigg)}_{\mathrm{DYN}} \\
\end{eqnarray}
\tag{19}
$$
湿潤静的エネルギー (moist static energy; MSE)$m$を$m:=s+Lq$で定義すると, (19)は
$$
\begin{eqnarray}
P'&\simeq&

\underbrace{\braket{q'\overline{C}}}_{\mathrm{THM}}
+\underbrace{\frac{M_q}{M}L^{-1}
m'\overline{C}}_{\mathrm{DYN}} \\
\end{eqnarray}
\tag{20}
$$
となる。

$\mathrm{DYN}$によって，水蒸気量の変化$q'$から予想される以上の降水量となるかどうかが決まる。

大気の熱力学的な変化(気温と水蒸気量の変化)$m'$に対する$\mathrm{DYN}$の感度は, パラメータ$M_q/M$の大きさで決まる。

ここで，

- $M$は気候場のMSEと発散偏差の積の鉛直積分

- $M_q$は気候場の水蒸気分布と発散偏差の積の鉛直積分

であり，大雑把な表現をすれば，(19)式は

降水域に流入する水蒸気が効果的に収束し，MSEが効果的に上昇流の強化に用いられれば，降水が増える

と解釈することができる。








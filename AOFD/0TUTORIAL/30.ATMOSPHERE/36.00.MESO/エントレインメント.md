# エントレインメント

## 定義

マスフラックス (mass flux), $M \, [\mathrm{kg \,m^{-2}\,s^{-1}}]$ 

エントレインメント率 (entrainment rate), $\epsilon$
$$
\epsilon := \frac{1}{M}\frac{\partial M}{\partial z}
$$
エントレインメント率の単位

$[\epsilon]=\frac{1}{\mathrm{kg \,m^{-2}\,s^{-1}}}\frac{\mathrm{kg \,m^{-2}\,s^{-1}}}{\mathrm{m}}$=$\mathrm{m}^{-1}$

単位の変換
$$
\frac{1}{M}\frac{\Delta M}{\Delta z}=\frac{\Delta M}{M}\frac{1}{\Delta z}
$$
であるから, $\mathrm{m}^{-1}$単位の$\epsilon$の値を$10^5$(=$100\times1000$)倍すれば, $\%/ \mathrm{km}$の単位で表すことができる。
# 湿潤大気に関する諸量

## 水蒸気混合比, $r$

### 定義

- $M_d$ :　乾燥大気の質量 $\mathrm{[kg]}$
- $M_v$ :　湿潤大気の質量 $\mathrm{[kg]}$
- $V$: 体積 $\mathrm{[m^3]}$
- $\rho_d$: 乾燥大気の密度 $\mathrm{[kg \, m^{-3}]}$
- $\rho_v$: 湿潤大気の密度 $\mathrm{[kg \, m^{-3}]}$

$$
\begin{eqnarray}
r&:=&M_v/M_d \\
&=&\frac{M_v/V}{M_d/V} \\
&=&\frac{\rho_v}{\rho_d}  \\
\end{eqnarray}
$$

$r$の単位は$[\mathrm{kg/kg}]$であるが，最大でも0.02程度の値なので，1000倍して$[\mathrm{g/kg}]$を用いることがある。

### 計算式

- $p_d$: 乾燥空気の分圧 $\mathrm{[Pa]}$
- $e$:  水蒸気圧 $\mathrm{[Pa]}$
- $\epsilon := R_d/R_v$

とすると, 
$$
\begin{eqnarray}
r&=&\frac{e/R_vT}{e/R_dT} =\frac{R_d}{R_v}\frac{e}{p_d}=\epsilon\frac{e}{p_d}.
\end{eqnarray}
$$
$p_d=p-e$であるから，
$$
\therefore \, r =\epsilon\frac{e}{p-e}  \\
$$


### 定数の値

$R_v:=R^*/m_v=\frac{8.3 [\mathrm{J \,mol^{-1} \, K^{-1}}]}{18 \times 10^{-3} [\mathrm{J \,mol^{-1}}]} \simeq 461 [\mathrm{J \, kg^{-1} K^{-1}}]$

$R_d:=R^*/m_d$, $R_v:=R^*/m_v$

$\epsilon=\frac{R_d}{R_v}=\frac{m_v}{R_d}\simeq \frac{18}{29}\simeq0.622$



## 比湿, $q$

水蒸気の密度/湿潤空気の密度のことを比湿 (specific humidity)と呼ぶ。specificには単位質量あたりという意味がある。
$$
\begin{eqnarray}
q&=&\frac{\rho_v}{\rho_d+\rho_v}
=\frac{\frac{\rho_v}{\rho_d}}{1+\frac{\rho_v}{\rho_d}}
=\frac{r}{1+r}
=\frac{\epsilon\frac{e}{p-e}}{1+\epsilon\frac{e}{p-e}}. \\
\end{eqnarray}
$$

$$
\therefore \, q=\frac{\epsilon e}{p-e(1-\epsilon)}
$$

一方, $q=\frac{r}{1+r}$より, $(1+r)q=r$より $r(1-q)=q$であるので，
$$
\begin{eqnarray}
\therefore \, r=\frac{q}{1-q}.
\end{eqnarray}
$$
水蒸気混合比と比湿はほぼ同じ値となる。大きめに見積もっても$q\sim10^{-2}\mathrm{[kg/kg]}$なので，$1-q\simeq 1$となる（誤差1%程度）。

$\therefore \, q\simeq r$ (誤差$\sim O(1 \%)$ 

$O$はランダウのオーという記号であり，上の場合「1%のオーダー」と読む。$A=O(B)$のとき, $A/B \sim 1$を意味する。



## 相対湿度, $H$

- $e$: 水蒸気圧
- $e^*$: 飽和水蒸気圧

とする。このとき，相対湿度$H$を
$$
H:=e/e^*
$$
と定義する。$H$の値は0~1程度の範囲の無次元量(単位の無い量)となるが，100倍して$\%$で表すことが多い。過飽和という現象があるため，$H$の値は1 (100%)を超えることもある。

$r=\epsilon e/(p-e)$であることを使うと, 
$$
\begin{eqnarray}
(p-e)r=\epsilon e \\
pr - er = \epsilon e \\
e=\frac{rp}{r+\epsilon}
\end{eqnarray}
$$
であるから，
$$
H=\frac{\frac{rp}{r+\epsilon}}{\frac{r^*p}{r^*+\epsilon}}=\frac{r}{r^*}\frac{r^*+\epsilon}{r+\epsilon}=\frac{r}{r^*}\bigg(\frac{1+r^*/\epsilon}{1+r/\epsilon} \bigg)\simeq\frac{r}{r^*}.
$$

$$
\therefore \, H:=\frac{e}{e^*}\simeq\frac{r}{r^*}
$$



## 仮温度と仮温位

湿潤空気は水蒸気量によって，気体定数の値が変わる。このため，気体定数は乾燥空気の値を使う代わりに，気温の値を調節することが良く行われる。この調節された気温のことを仮温度 (virtual temperature)と呼ぶ。

乾燥空気の比容, $\alpha_d:=\frac{V}{M_d}$, 湿潤空気の比容, $\alpha := \frac{V}{M_d+M_v}$と定義する。このとき，
$$
\alpha=\frac{V}{M_d+M_v}=\frac{\frac{V}{M_d}}{1+\frac{M_V}{M_d}}=\frac{\alpha_d}{1+r}
$$
となる。状態方程式$p_d \alpha_d=R_dT$と分圧の法則$p=p_d+e$を使うと,
$$
\begin{eqnarray}
\alpha=\frac{\alpha_d}{1+r}=\frac{R_dT}{p_d}\frac{1}{1+r}=\frac{R_dT}{p}\frac{p_d+e}{p_d}\frac{1}{1+r}.
\end{eqnarray}
$$
さらに$r=\epsilon\frac{e}{p_d}$であるから,
$$
\begin{eqnarray}
\frac{p_d+e}{p_d}=1+\frac{e}{p_d}=1+\frac{r}{\epsilon}.
\end{eqnarray}
$$
$R':=R_d\frac{(1+r/\epsilon)}{1+r}$と定義すると
$$
\alpha=\frac{R_dT}{p}\frac{1+r/\epsilon}{1+r}=R'\frac{T}{p}.
$$
となる。$\epsilon < 1$より，$R'>R_d$である。

いま，仮温度, $T_v$と呼ばれる量を,
$$
T_v:=\frac{R'}{R_d}T
$$
と定義すると, $R_dT_v=R'T$より, 
$$
T_v=\frac{1+r/\epsilon}{1+r}T\simeq T\bigg(1+\frac{r}{\epsilon}\bigg)(1-r).
$$
ここで，$(1+r)^{-1}\simeq 1-r$を使った。よって，
$$
T_v\simeq T\bigg(1-r+\frac{r}{\epsilon}(1-r)\bigg) = T\bigg(1-r+\frac{r}{\epsilon}-\frac{r^2}{\epsilon}\bigg) \simeq T\bigg(1+\bigg(\frac{1}{\epsilon}-1 \bigg)r \bigg)
$$
$1/\epsilon-1\simeq 1/0.622 -1 \simeq0.608$を使うと,
$$
T_v=T(1+0.608r)
$$
となる。仮温度を使うと，次のように気体定数を乾燥空気の値としたままで，状態方程式を用いることができる。$T_v:=\frac{R'}{R_d}T$より，$R'=\frac{R_dT_v}{T}$であるが，これを状態方程式$p\alpha=R'T$に代入すると，
$$
\therefore \, p\alpha=R_d T_v.
$$
$T=300 \, \mathrm{[K]}$, $r=0.015 \mathrm{[kg/kg]}$のとき, $T_v$の値は$T\simeq303 \, \mathrm{[K]}$となり，水蒸気混合比が$r$が$0$から$0.015 \mathrm{[kg/kg]}$に増えることは$3 \, \mathrm{[K]}$の気温上昇に相当する。

$T_v$を使って仮温位と呼ばれる量, $\theta_v$を,
$$
\theta_v:=T_v\bigg(\frac{p_0}{p}\bigg)^\kappa
$$
で定義する ($\kappa=R_d/C_p \simeq 287.04/1005.7 \simeq 0.285$)。


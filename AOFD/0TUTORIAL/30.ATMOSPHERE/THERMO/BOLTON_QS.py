import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

plt.rcParams["font.size"] = 14

ax = fig.add_subplot(111)
ax.set_xlabel("T", fontsize = 14)
ax.set_ylabel("$e_{s}$", fontsize = 14)

x = np.arange(-5,35,0.1)
y = 6.112*np.exp((x*17.67)/(x+234.15))

ax.plot(x,y,color="black")

#FIG="BOLTON_ES.PDF"
FIG="BOLTON_ES.PNG"
plt.savefig(FIG)
print("FIG: "+FIG)



# Saturation vapor pressure and mixing ratio



## Saturation vapor pressure

Bolton (1980) modification of Tetens's (1930) formula. See Emanuel (1994)  
$$
e_s=6.112\exp\bigg( \frac{17.67T_c}{T_c+243.5} \bigg)
$$
where, $T_c$ is temperature in **degree Celsius** and $e_s$ is saturation vapor pressure in **hPa**.  

![BOLTON_ES](BOLTON_ES.PNG)

```python
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

plt.rcParams["font.size"] = 14

ax = fig.add_subplot(111)
ax.set_xlabel("T", fontsize = 14)
ax.set_ylabel("$e_{s}$", fontsize = 14)

x = np.arange(-5,35,0.1)
y = 6.112*np.exp((x*17.67)/(x+234.15))

ax.plot(x,y,color="black")

#FIG="BOLTON_ES.PDF"
FIG="BOLTON_ES.PNG"
plt.savefig(FIG)
print("FIG: "+FIG)
```



## Saturation water vapor mixing ratio  

$$
q_s=\epsilon\frac{e_s}{p-e_s}
$$

$$
\epsilon \equiv \frac{R_d}{R_v} \approx 0.622
$$




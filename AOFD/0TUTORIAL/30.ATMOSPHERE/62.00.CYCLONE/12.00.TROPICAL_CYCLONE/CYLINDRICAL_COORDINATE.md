# CYLINDRICAL_COORDINATE

成熟した熱帯低気圧を, 軸対称の渦と見なすことがよくある。軸対称渦を表現するのに適した円筒座標系におけるベクトルの微分 (導ベクトル)について解説する。

[[_TOC_]]

## 座標の取り方

動径方向の座標を$r$, 方位角方向の座標を$\theta$, 鉛直方向の座標を$z$とする。$r$, $\theta$, $z$方向の単位ベクトルをそれぞれ, $\mathbf{e}_r$,  $\mathbf{e}_\theta$,  $\mathbf{e}_z$とする。$\mathbf{e}_r$,  $\mathbf{e}_\theta$の方向が方位角$\theta$とともに変化するために, 円筒座標系の方程式にはデカルト座標系 $(x, y, z)$には無かった項が現れる。



## 基本ベクトルの正規直交性

$\mathbf{e}_r$,  $\mathbf{e}_\theta$,  $\mathbf{e}_z$の大きさはすべて1とする。
$$
|\mathbf{e}_r|=1, \quad |\mathbf{e}_\theta|=1, \quad |\mathbf{e}_z|=1
\tag{1}
$$
また, $\mathbf{e}_r$,  $\mathbf{e}_\theta$,  $\mathbf{e}_z$は各々直交する。
$$
\mathbf{e}_r \cdot \mathbf{e}_\theta = \mathbf{e}_\theta \cdot \mathbf{e}_r = 0, \\
\mathbf{e}_\theta \cdot \mathbf{e}_z = \mathbf{e}_z \cdot \mathbf{e}_\theta = 0, \\
\mathbf{e}_r \cdot \mathbf{e}_z = \mathbf{e}_z \cdot \mathbf{e}_r = 0, \\
\tag{2}
$$


## 速度ベクトル

 速度ベクトル$\mathbf{v}$は, $r$, $\theta$, $z$方向成分をそれぞれ, $v_r$, $v_\theta$, $v_z$と書くことにすると,
$$
\mathbf{v}=v_r \mathbf{e}_r + v_\theta \mathbf{e}_\theta + v_z   \mathbf{e}_z
\tag{3}
$$
と書くことができる。



## 基本ベクトルの導ベクトル

$$
\frac{\partial }{\partial \theta}\mathbf{e}_r=\mathbf{e}_\theta \\
\frac{\partial }{\partial \theta}\mathbf{e}_\theta=-\mathbf{e}_r \\
\frac{\partial }{\partial \theta}\mathbf{e}_z=\mathbf{0} \\
\tag{4}
$$

である。また, $r$, $z$に関する導ベクトルはすべて$\mathbf{0}$になる。



## 勾配ベクトル

スカラー関数$f (r,\theta,z)$の勾配ベクトル$\nabla f$は,
$$
\nabla f=\bigg(  \mathbf{e}_r \frac{\partial }{\partial r}
+  \mathbf{e}_\theta \frac{1}{r} \frac{\partial }{\partial \theta}
+  \mathbf{e}_z \frac{\partial }{\partial z}
\bigg)f
\tag{5}
$$
と書ける。



## ベクトルの発散

(1)～(5)を用いると,
$$
\begin{eqnarray}
&& \nabla \cdot \mathbf{v} \\
&=&\bigg(  \mathbf{e}_r \frac{\partial }{\partial r}
+  \mathbf{e}_\theta \frac{1}{r} \frac{\partial }{\partial \theta}
+  \mathbf{e}_z \frac{\partial }{\partial z}
\bigg)\cdot
(v_r \mathbf{e}_r + v_\theta \mathbf{e}_\theta + v_z   \mathbf{e}_z)\\
&=&\frac{\partial v_r}{\partial r}+\frac{1}{r}v_r+\frac{1}{r}
\frac{\partial v_\theta}{\partial \theta}+\frac{\partial v_z}{\partial z}
\end{eqnarray}
\tag{6}
$$
が得られる。



## スカラーのラプラシアン

$\nabla^2f = \text{div} \, \text{grad} \, f = \nabla \cdot (\nabla f)$を用いると,
$$
&&\nabla^2 f \\
&=&
\bigg(  \mathbf{e}_r \frac{\partial }{\partial r}
+  \mathbf{e}_\theta \frac{1}{r} \frac{\partial }{\partial \theta}
+  \mathbf{e}_z \frac{\partial }{\partial z}
\bigg) \cdot
\bigg(  \mathbf{e}_r \frac{\partial f}{\partial r}
+  \mathbf{e}_\theta \frac{1}{r} \frac{\partial f}{\partial \theta}
+  \mathbf{e}_z \frac{\partial f}{\partial z}
\bigg)\\
&=& \bigg(   \frac{\partial^2 f}{\partial r^2}
+  \frac{1}{r} \frac{\partial }{\partial r}
+   \frac{1}{r^2} \frac{\partial^2 f}{\partial \theta^2}
+  \frac{\partial^2 f}{\partial z^2}
\bigg)f
\tag{7}
$$
となる。



## 移流項

$$
(\mathbf{v}\cdot \nabla):=v_r\frac{\partial }{\partial r}
+v_\theta \frac{1}{r}\frac{\partial }{\partial \theta}
+v_z \frac{\partial }{\partial z}
\tag{8}
$$

と定義する。このとき, $\mathbf{v}$の移流項$(\mathbf{v}\cdot \nabla)\mathbf{v}$は,
$$
&&(\mathbf{v}\cdot \nabla)\mathbf{v}\\
&=&\bigg(v_r\frac{\partial }{\partial r}
+v_\theta \frac{1}{r}\frac{\partial }{\partial \theta}
+v_z \frac{\partial }{\partial z}\bigg)
(v_r \mathbf{e}_r + v_\theta \mathbf{e}_\theta + v_z   \mathbf{e}_z) \\

&=& \mathbf{e}_r\bigg( (\mathbf{v}\cdot \nabla)v_r-\frac{1}{r}v_\theta^2 \bigg) \\
&+& \mathbf{e}_r\bigg( (\mathbf{v}\cdot \nabla)v_\theta+\frac{1}{r}v_\theta v_r \bigg) \\
&+& \mathbf{e}_r\bigg( (\mathbf{v}\cdot \nabla)v_z \bigg) \\
\tag{9}
$$
となる。



## ベクトルのラプラシアン

$\nabla^2\mathbf{v}=$を使うと,
$$
&&\nabla^2\mathbf{v}\\
&=&\mathbf{e}_r\bigg( \nabla^2 v_r - \frac{1}{r^2}v_r 
- \frac{2}{r^2}\frac{\partial v_\theta }{\partial \theta}  \bigg) \\

&+& \mathbf{e}_\theta\bigg( \nabla^2 v_\theta 
+\frac{2}{r^2}\frac{\partial v_r }{\partial \theta} 
-\frac{1}{r^2}v_\theta  \bigg) \\

&+& \mathbf{e}_z\bigg( \nabla^2 v_z \bigg)
\tag{10}
$$
を得ることができる。


# GRADIENT

成熟した熱帯低気圧を, 軸対称の渦と見なすことがよくある。軸対称渦を表現するのに適した円筒座標系における微分法について考える。その準備として, **勾配**と**方向微分**と呼ばれる量を導入する

[[_TOC_]]

[[_TOC_]]

## 全微分

2変数$x$, $y$をもつ平面上の関数$f$を考える。平面上で点Aから点Cまで移動したときの関数$f$の変化を調べることにする。関数の変化は$\delta f=f_C-f_A$と表すことができる。ここで, $f_A$は点Aにおける関数の値を表す。$f_C$も同様。図1より,


$$
\left\{
   \begin{align}
      f_A &=& f(x,y) \\
      f_B &=& f(x+\delta x, y) \\
      f_C &=& f(x+\delta x, y+\delta y)
   \end{align}
\right. \tag{1}
$$
である。これに点Bでの値$f_B$を入れて,
$$
\delta f = f_C - f_A = f_B - f_A + f_C - f_B　\tag{2}
$$
とする ($f_B$を足して$f_B$を引く)。$\delta x$, $\delta y$が微小のとき,
$$
f(x+\delta x,y)-f(x,y) \simeq \frac{\partial f}{\partial x} \delta x \\
f(x,y+\delta y)-f(x,y) \simeq \frac{\partial f}{\partial y} \delta y \\
$$
であるから,
$$
f_B-f_A \simeq \frac{\partial f}{\partial x}(x,y) \delta x \\
f_C-f_B \simeq \frac{\partial f}{\partial y}(x+\delta x, y) \delta y \\
$$
となる。これを(2)に代入すると,
$$
\delta f = \frac{\partial f}{\partial x}(x,y) \delta x + \frac{\partial f}{\partial y}(x+\delta x, y)\delta y
\tag{3}
$$
となる。右辺第2項に現れた
$$
\frac{\partial f}{\partial y}(x+\delta x, y)
$$
をテイラー展開すると,
$$
\frac{\partial f}{\partial y}(x+\delta x, y)=\frac{\partial f}{\partial y}(x, y)\delta x+\frac{\partial^2 f}{\partial x\partial y}(x, y)\delta x \delta y
$$
であるが, 第2項は微小量$\delta x$, $\delta y$の積がかかっているため, 第一項と比べ無視できるとみなし,
$$
\frac{\partial f}{\partial y}(x+\delta x, y)\simeq\frac{\partial f}{\partial y}(x, y)\delta x
$$
と近似する。このとき, (3)は,


$$
\delta f = \frac{\partial f}{\partial x}(x,y) \delta x + \frac{\partial f}{\partial y}(x, y)\delta y
\tag{4}
$$
と表すことができる。(4)を関数$f$の全微分と呼ぶことがある。



## 勾配ベクトル

式(4)の右辺は, 成分表示で$(\partial f/\partial x, \partial f/\partial y)$, $(\delta x, \delta y)$ と表されるベクトルの内積と見なすことができる。すなわち, 2つのベクトル$\nabla f$, $\delta \mathbf{r}$を
$$
\nabla f:=\begin{bmatrix}
\frac{\partial f}{\partial x} \\
\frac{\partial f}{\partial y}
\end{bmatrix}
$$

$$
\delta \mathbf{r}:=\begin{bmatrix}
\delta x \\
\delta y
\end{bmatrix}
$$

と定義すると, (4)は
$$
\delta f ＝(\nabla f \cdot \delta \mathbf{r})
$$
と書くことができる。$\nabla$はナブラと読み, $\nabla f$を勾配ベクトルと呼ぶことがある。



## 3変数関数

次に3変数関数について考える。3変数関数$f(x,y,z)$をテイラー展開すると
$$
&&f(x+\delta x, y+\delta y, z+\delta z)-f(x,y,z)\\
&=&\frac{\partial f}{\partial x}\delta x+\frac{\partial f}{\partial y}\delta y+\frac{\partial f}{\partial z}\delta z \\

&+&\frac{1}{2}\bigg( 
\frac{\partial^2 f}{\partial^2 x}(\delta x)^2 
+\frac{\partial^2 f}{\partial^2 y}(\delta y)^2 
+\frac{\partial^2 f}{\partial^2 z}(\delta z)^2 \\

&+&\frac{\partial^2 f}{\partial x \partial y}(\delta x\delta y)
+\frac{\partial^2 f}{\partial y \partial z}(\delta y\delta z)
+\frac{\partial^2 f}{\partial z \partial x}(\delta z\delta x)\bigg)\\
&+&\cdots \\

&\simeq& 

\frac{\partial f}{\partial x}\delta x+\frac{\partial f}{\partial y}\delta y+\frac{\partial f}{\partial z}\delta z
$$
となる。ここで,
$$
\delta f :=f(x+\delta x, y+\delta y, z+\delta z)-f(x,y,z)
$$
と定義すると,
$$
\delta f = \frac{\partial f}{\partial x}\delta x+\frac{\partial f}{\partial y}\delta y+\frac{\partial f}{\partial z}\delta z
$$
と書き表すことができる。いま,
$$
\nabla f:=\begin{bmatrix}
\frac{\partial f}{\partial x} \\
\frac{\partial f}{\partial y} \\
\frac{\partial f}{\partial z} \\
\end{bmatrix}
$$

$$
\delta \mathbf{r}:=\begin{bmatrix}
\delta x \\
\delta y \\
\delta z \\
\end{bmatrix}
$$

と定義すると, 先ほどの2変数関数の場合と同様に,
$$
\delta f ＝(\nabla f \cdot \delta \mathbf{r})
$$
と書くことができる。



## 勾配ベクトルの性質

$\delta \mathbf{r}$と$\nabla f$のなす角を$\theta$とすると, $\delta f ＝(\nabla f \cdot \delta \mathbf{r})$は,
$$
\delta f ＝\|\nabla f\|\|\mathbf{\delta r}\|\cos\theta
$$
と書くことができる。

いま, $\theta = 0 \degree$とする。すなわち, $\delta \mathbf{r}$と$\nabla f$を平行になるようにとる。このとき$\delta f$の値は最大となる。すなわち, $\nabla f$は$f$の値の変化が最大となる方向を向く。このとき,
$$
\| \nabla f\| = \frac{\delta f}{\| \delta \mathbf{r}\|}
$$
である。すなわち, $\|\nabla f\|$の大きさは, $\delta f$を$\delta \mathbf{r}$の大きさで割った値に等しい。

次に, $\delta \mathbf{r}$を$f$の等値線に平行になるようにとる。$f$の等値線上では$f$の値は変化しないので, $\delta f=0$である。$\|\nabla f\| \ne 0$,  $\|\mathbf{\delta r}\| \ne 0$であれば, $\cos \theta = 0$となる。すなわち, $f$の等値線と$\nabla f$は直交する。



## 方向微分

ベクトル値関数$f (\mathbf{r})$を単位ベクトル$\hat{\mathbf{n}}$で与えられる方向に変化させたとき, 
$$
\frac{df}{ds}(\mathbf{r})=\lim_{\delta s \to0}\frac{f(\mathbf{r}+\hat{\mathbf{n}} \delta s)-f(\mathbf{r}) }{\delta s}
$$
で定義される量$df/ds$を$f$の方向微分と呼ぶ。

$\delta f = (\nabla f \cdot \delta \mathbf{r})$に, $\delta \mathbf{r}=\hat{\mathbf{n}}\delta s$を代入すると,
$$
\frac{\delta f}{\delta s}=\nabla f \cdot \hat{\mathbf{n}}
$$
$\delta s \to 0$とすると,
$$
\frac{df}{ds}= \hat{\mathbf{n}} \cdot \nabla f
$$
がえられる。この式は, $\nabla f$の値が分かっていれば, $f$の任意の方向の方向微分を計算できるという意味で重要である。

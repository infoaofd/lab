# 安定度の時間的変化

$$
\frac{\partial}{\partial t}\bigg(-\frac{\partial \theta_e}{\partial p}\bigg)
=\frac{\partial}{\partial p}\bigg(u\frac{\partial \theta_e}{\partial x}
+v\frac{\partial \theta_e}{\partial y}
+\omega\frac{\partial \theta_e}{\partial p}\bigg)
+\frac{\partial}{\partial p}\bigg(\frac{\partial \overline{\omega'\theta_e'}}{\partial p}\bigg)
$$

右辺第一項: 鉛直差分移流 (differential advection)　**負号がつかない**ことに注意

成層不安定化をもたらす過程

1. 上層の寒気・乾気移流
2. 下層の暖気・湿気移流
3. 上昇流 ($\omega$による$\theta_e$移流, または気柱の伸びによる効果)
4. 下層の非断熱熱源と水蒸気源, 上層の放射冷却
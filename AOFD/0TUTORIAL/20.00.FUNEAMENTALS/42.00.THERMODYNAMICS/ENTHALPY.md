# エンタルピー

熱力学第一法則 $du=\Delta q + \Delta w$において，単位質量の気体が行う仕事$\Delta w$を$\Delta w=-pd \alpha$とすると, $du=\Delta q - p d\alpha$である。

いま定容(定積)過程 ($d\alpha = 0$)のときの$\Delta q$を$\Delta q_v$と書くことにすると, $du=\Delta q_v$である。一方, 定圧仮定 ($p=$一定)のときの, $\Delta q$を$\Delta q_p$と書くことにすると, 
$$
du = \Delta q_p - p d\alpha
$$
よって，
$$
\begin{eqnarray}
\Delta q_p &=& du + p d\alpha \\
&=& \Delta q_v + p d\alpha
\end{eqnarray}
$$
となる。$d \alpha>0$のとき, $\Delta q_p>\Delta q_v$である。これは，定圧過程において加えた熱の一部は体積変化に使われるため，内部エネルギーを増加させるために定積仮定よりも多くの熱を必要とすることを意味する (**定圧過程の場合, 加えた熱のすべてが内部エネルギーの増加に使われるわけではない**)。

エンタルピー, $h$と言う量を
$$
\begin{eqnarray}
h:=u+p\alpha
\end{eqnarray}
$$
で定義する。定圧過程のとき$\Delta q_p=du+pd\alpha$であるから, $dh=\Delta q_p$。よって，**定圧過程において加えた熱はエンタルピーの変化に等しい** (加えた熱を気体の状態量の変化として表すことが出来る)。



## 例：開いた系におけるエンタルピー

物質の流入(流出)がある系のことを**開いた系**と呼ぶ。

$U_1$: 系に流入するエネルギー

$\delta M_1$: 系1の外から系1に流入する流体$F_1$の質量

$\delta M_2$: 系1の中から流出する流体の$F_2$質量

$\alpha_i$: 流体$F_i$ ($i=1,2$)の比容(単位質量あたりの体積)

$u_i$: 流体$F_i$ ($i=1,2$)の比内部エネルギー(単位質量あたりの内部エネルギー)
$$
\begin{eqnarray}
U_1=\underbrace{\delta M_1 u_1}_{あ} + \underbrace{p\delta M_1 \alpha_1}_{い}
\tag{1}
\end{eqnarray}
$$
ここで,

あ: 質量$\delta M_1$の流体$F_1$のもつ内部エネルギー

い: 流体$F_1$を系$S_1$に入れるのに必要な仕事

である。エンタルピーの定義$h:=u+p\alpha$を使うと, (1)は,
$$
\begin{eqnarray}
U_1=\delta M_1 (u_1+p_1 \alpha_1)=\delta M_1 h_1
\tag{1}
\end{eqnarray}
$$
となる。同様に系から流出するエネルギーは$U_2$は，
$$
\begin{eqnarray}
U_2=\delta M_2 (u_2+p_2 \alpha_2)=\delta M_2 h_2
\tag{2}
\end{eqnarray}
$$
である。よって, 対象とする系のエネルギーの変化$\delta U$は
$$
\begin{eqnarray}
\delta U=\delta M_2 h_2 - \delta M_1 h_1
\tag{1}
\end{eqnarray}
$$
となる。以上の結果は, 対象とする系への**物質の流入(流出)があるとき**，系**のエネルギーの変化は**内部エネルギーのフラックスではなく，**エンタルピーのフラックスで表される**ことを意味する。

## 定圧比熱の表現

エンタルピーを使うと, 定圧比熱$c_p$は
$$
c_p:=\lim_{\Delta T \to 0}\bigg(\frac{\Delta q_p}{\Delta T}\bigg)\underbrace{=}_{定圧のとき\Delta q_p=h}\frac{dh}{dT}
$$
と表すことが出来る。




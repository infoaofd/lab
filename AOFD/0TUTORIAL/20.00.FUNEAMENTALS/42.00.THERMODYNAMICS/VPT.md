# 仮温度と仮温位

湿潤空気は水蒸気量によって，気体定数の値が変わる。このため，気体定数は乾燥空気の値を使う代わりに，気温の値を調節することが良く行われる。この調節された気温のことを仮温度 (virtual temperature)と呼ぶ。

乾燥空気の比容, $\alpha_d:=\frac{V}{M_d}$, 湿潤空気の比容, $\alpha := \frac{V}{M_d+M_v}$と定義する。このとき，
$$
\alpha=\frac{V}{M_d+M_v}=\frac{\frac{V}{M_d}}{1+\frac{M_V}{M_d}}=\frac{\alpha_d}{1+r}
$$
となる。状態方程式$p_d \alpha_d=R_dT$と分圧の法則$p=p_d+e$を使うと,
$$
\begin{eqnarray}
\alpha=\frac{\alpha_d}{1+r}=\frac{R_dT}{p_d}\frac{1}{1+r}=\frac{R_dT}{p}\frac{p_d+e}{p_d}\frac{1}{1+r}.
\end{eqnarray}
$$
さらに$r=\epsilon\frac{e}{p_d}$であるから,
$$
\begin{eqnarray}
\frac{p_d+e}{p_d}=1+\frac{e}{p_d}=1+\frac{r}{\epsilon}.
\end{eqnarray}
$$
$R':=R_d\frac{(1+r/\epsilon)}{1+r}$と定義すると
$$
\alpha=\frac{R_dT}{p}\frac{1+r/\epsilon}{1+r}=R'\frac{T}{p}.
$$
となる。$\epsilon < 1$より，$R'>R_d$である。

いま，仮温度, $T_v$と呼ばれる量を,
$$
T_v:=\frac{R'}{R_d}T
$$
と定義すると, $R_dT_v=R'T$より, 
$$
T_v=\frac{1+r/\epsilon}{1+r}T\simeq T\bigg(1+\frac{r}{\epsilon}\bigg)(1-r).
$$
ここで，$(1+r)^{-1}\simeq 1-r$を使った。よって，
$$
T_v\simeq T\bigg(1-r+\frac{r}{\epsilon}(1-r)\bigg) = T\bigg(1-r+\frac{r}{\epsilon}-\frac{r^2}{\epsilon}\bigg) \simeq T\bigg(1+\bigg(\frac{1}{\epsilon}-1 \bigg)r \bigg)
$$
$1/\epsilon-1\simeq 1/0.622 -1 \simeq0.608$を使うと,
$$
T_v=T(1+0.608r)
$$
となる。仮温度を使うと，次のように気体定数を乾燥空気の値としたままで，状態方程式を用いることができる。$T_v:=\frac{R'}{R_d}T$より，$R'=\frac{R_dT_v}{T}$であるが，これを状態方程式$p\alpha=R'T$に代入すると，
$$
\therefore \, p\alpha=R_d T_v.
$$
$T=300 \, \mathrm{[K]}$, $r=0.015 \mathrm{[kg/kg]}$のとき, $T_v$の値は$T\simeq303 \, \mathrm{[K]}$となり，水蒸気混合比が$r$が$0$から$0.015 \mathrm{[kg/kg]}$($15 \mathrm{[g/kg]}$)に増えることは$3 \, \mathrm{[K]}$の気温上昇に相当する。

$T_v$を使って仮温位と呼ばれる量, $\theta_v$を,
$$
\theta_v:=T_v\bigg(\frac{p_0}{p}\bigg)^\kappa
$$
で定義する ($\kappa=R_d/C_p \simeq 287.04/1005.7 \simeq 0.285$)。


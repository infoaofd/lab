# SATURATION WATER VAPOR MIXING RATIO

! C:\Users\foome\lab\AOFD\0TUTORIAL\20.00.FUNEAMENTALS\42.00.THERMODYNAMICS\EPT

```FORTRAN
! INPUT: tk [kelvin]; p [hPa]
tc=tk-273.15
es= 6.112*exp((17.67*tc)/(tc+243.5))   ! Eq.10 of Bolton (1980)
mixr= 0.62197*(es/(p-es))              ! Eq.4.1.2 (p.108) of Emanuel(1994) 
```


# 湿潤大気に関する諸量

## 用語

湿潤大気: 水蒸気を含む大気

乾燥大気: 湿潤大気から水蒸気を除いた(仮想的な)大気



## 定義

- $M_d$ :　乾燥大気の質量 $\mathrm{[kg]}$
- $M_v$ :　湿潤大気の質量 $\mathrm{[kg]}$
- $V$: 体積 $\mathrm{[m^3]}$
- $\rho_d$: 乾燥大気の密度 $\mathrm{[kg \, m^{-3}]}$
- $\rho_v$: 湿潤大気の密度 $\mathrm{[kg \, m^{-3}]}$



## 水蒸気混合比, $r$

$$
\begin{eqnarray}
r&:=&M_v/M_d \\
&=&\frac{M_v/V}{M_d/V} \\
&=&\frac{\rho_v}{\rho_d}  \\
\end{eqnarray}
$$

$r$の単位は$[\mathrm{kg/kg}]$であるが，最大でも0.02程度の値なので，1000倍して$[\mathrm{g/kg}]$を用いることがある。

### 計算式

- $p_d$: 乾燥空気の分圧 $\mathrm{[Pa]}$
- $e$:  水蒸気圧 $\mathrm{[Pa]}$
- $\epsilon := R_d/R_v$

とすると, 
$$
\begin{eqnarray}
r&=&\frac{e/R_vT}{e/R_dT} =\frac{R_d}{R_v}\frac{e}{p_d}=\epsilon\frac{e}{p_d}.
\end{eqnarray}
$$
$p_d=p-e$であるから，
$$
\therefore \, r =\epsilon\frac{e}{p-e}  \\
$$


### 定数の値

$R_v:=R^*/m_v=\frac{8.3 [\mathrm{J \,mol^{-1} \, K^{-1}}]}{18 \times 10^{-3} [\mathrm{J \,mol^{-1}}]} \simeq 461 [\mathrm{J \, kg^{-1} K^{-1}}]$

$R_d:=R^*/m_d$, $R_v:=R^*/m_v$

$\epsilon=\frac{R_d}{R_v}=\frac{m_v}{R_d}\simeq \frac{18}{29}\simeq0.622$



## 比湿, $q$

水蒸気の密度/湿潤空気の密度のことを比湿 (specific humidity)と呼ぶ。specificには単位質量あたりという意味がある。
$$
\begin{eqnarray}
q&=&\frac{\rho_v}{\rho_d+\rho_v}
=\frac{\frac{\rho_v}{\rho_d}}{1+\frac{\rho_v}{\rho_d}}
=\frac{r}{1+r}
=\frac{\epsilon\frac{e}{p-e}}{1+\epsilon\frac{e}{p-e}}. \\
\end{eqnarray}
$$

$$
\therefore \, q=\frac{\epsilon e}{p-e(1-\epsilon)}
$$

一方, $q=\frac{r}{1+r}$より, $(1+r)q=r$より $r(1-q)=q$であるので，
$$
\begin{eqnarray}
\therefore \, r=\frac{q}{1-q}.
\end{eqnarray}
$$
水蒸気混合比と比湿はほぼ同じ値となる。大きめに見積もっても$q\sim10^{-2}\mathrm{[kg/kg]}$なので，$1-q\simeq 1$となる（誤差1%程度）。

$\therefore \, q\simeq r$ (誤差$\sim O(1 \%)$ 

$O$はランダウのオーという記号であり，上の場合「1%のオーダー」と読む。$A=O(B)$のとき, $A/B \sim 1$を意味する。



## 比容

単位質量あたりの体積のことを比容 (specific volume)と呼ぶ。

乾燥大気の比容, $\alpha_d := V/M_d$

湿潤大気の比容, $\alpha$
$$
\begin{eqnarray}
\alpha&:=&\frac{V}{M_d+M_v} \\
&=&\frac{\frac{V}{M_d}}{1+\frac{M_v}{M_d}} \\
&=&\frac{\alpha_d}{1+r}
\end{eqnarray}
$$


## 凝結物を含む気体の比容

凝結物を含む大気の比容を便宜的に先ほどと同じ$\alpha$で表すことにする。

$V_a$: 湿潤空気の体積

$V_l$: 液相の水の体積

$V_i$: 固相の水の体積

とすると,
$$
\begin{eqnarray}
\alpha&:=&\frac{V_a+V_l+V_i}{M_d+M_v+M_l+M_i} \\
&=&\frac{\frac{V_a}{M_d}+\frac{V_l}{M_d}+\frac{V_i}{M_d}}{1+\frac{M_v}{M_d}+\frac{M_l}{M_d}+\frac{M_i}{M_d}} \\
\end{eqnarray}
$$
である。液相の水の比容$\alpha_l:=V_l/M_l$, 混合比$r_l:=M_l/M_d$と定義すると, 
$$
\begin{eqnarray}
\frac{V_l}{M_d}=\frac{V_l}{M_l}\frac{M_l}{M_d}=\alpha_l r_l
\end{eqnarray}
$$
である。同様に，固相の水の比容$\alpha_i:=V_i/M_i$, 混合比$r_i:=M_i/M_d$と定義すると, 
$$
\begin{eqnarray}
\frac{V_i}{M_d}=\frac{V_i}{M_l}\frac{M_i}{M_d}=\alpha_i r_i
\end{eqnarray}
$$
である。ここで，
$$
\begin{eqnarray}
\alpha_d&:=&\frac{V_a}{M_d} \\
r_T&:=&r+r_l+r_i
\end{eqnarray}
$$
と定義すると, 
$$
\begin{eqnarray}
\alpha
&=&\frac{\frac{V_a}{M_d}+\frac{V_l}{M_d}+\frac{V_i}{M_d}}{1+\frac{M_v}{M_d}+\frac{M_l}{M_d}+\frac{M_i}{M_d}} \\
&=&\alpha_d\frac{ 1 +r_l\frac{\alpha_l}{\alpha_d}+r_i\frac{\alpha_i}{\alpha_d}}{1+r+r_l+r_i} \\
&\simeq&\frac{\alpha_d}{1+r_T}
\end{eqnarray}
$$
が得られる ($\because \,\alpha_l, \alpha_i \lesssim \frac{\alpha_d}{1000}$)。



## 相対湿度, $H$

- $e$: 水蒸気圧
- $e^*$: 飽和水蒸気圧

とする。このとき，相対湿度$H$を
$$
H:=e/e^*
$$
と定義する。$H$の値は0~1程度の範囲の無次元量(単位の無い量)となるが，100倍して$\%$で表すことが多い。過飽和という現象があるため，$H$の値は1 (100%)を超えることもある。

$r=\epsilon e/(p-e)$であることを使うと, 
$$
\begin{eqnarray}
(p-e)r=\epsilon e \\
pr - er = \epsilon e \\
e=\frac{rp}{r+\epsilon}
\end{eqnarray}
$$
であるから，
$$
H=\frac{\frac{rp}{r+\epsilon}}{\frac{r^*p}{r^*+\epsilon}}=\frac{r}{r^*}\frac{r^*+\epsilon}{r+\epsilon}=\frac{r}{r^*}\bigg(\frac{1+r^*/\epsilon}{1+r/\epsilon} \bigg)\simeq\frac{r}{r^*}.
$$

$$
\therefore \, H:=\frac{e}{e^*}\simeq\frac{r}{r^*}
$$




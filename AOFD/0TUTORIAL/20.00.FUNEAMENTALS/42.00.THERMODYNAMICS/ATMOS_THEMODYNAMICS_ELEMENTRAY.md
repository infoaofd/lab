# 大気熱力学

[[_TOC_]]

## 熱力学第一法則

### 記号

$\Delta U$: 系の内部エネルギ―の変化量 $[\mathrm{J}]$

$\Delta Q$: 系に加えられた熱エネルギー $[\mathrm{J}]$

$\Delta W$: 系に加えられた仕事 $[\mathrm{J}]$



### 熱力学第一法則

$$
\begin{eqnarray}
\Delta U = \Delta Q + \Delta W
\end{eqnarray}
$$

が成り立つ。これを熱力学第一法則と呼ぶ。単位質量あたりの量を小文字で表すことにすると, 
$$
\begin{eqnarray}
\Delta u = \Delta q + \Delta w
\end{eqnarray}
$$

となる。$\Delta w$として気体の膨張・圧縮に伴う仕事を考えると,
$$
\begin{eqnarray}
\Delta w = -p(\Delta \alpha)
\end{eqnarray}
$$
このとき, 熱力学第一法則は, 
$$
\Delta q = \Delta u + p(\Delta \alpha)
$$
となる。



# 理想気体の状態方程式

気体の圧力，温度，体積はある一つの関係式を満たすことが要請される。即ち，圧力，温度，体積のうちの2つの量を指定すると，残った最後の量の値は自動的に定まってしまう。この関係式のことを状態方程式と呼ぶ。

ここでは，下記の仮定をみたす理想気体と呼ばれる気体の状態方程式について考える。空気や水蒸気は非常によい精度で，理想気体をみなすことが出来る。

## 仮定

- 分子間力を無視することができる
- 気体の温度が常温程度である

## 記号

- $p$: 圧力 [$\mathrm{Pa}$]
- $V$: 体積 [$\mathrm{m}^3$]
- $n$: モル数 (分子の数を$6.02\times10^{23}$を単位として表した数) 
- $T$: 温度 [$\mathrm{K}$]
- $R^*$: 普遍気体定数 ($\simeq8.3 \mathrm{[J mol^{-1}K^{-1}]}$)

## 状態方程式

$p$, $V$, $n$, $T$に関して,

$$
\begin{eqnarray}
pV=nR^*T
\end{eqnarray}
$$
という関係が非常に高い精度で成り立つ。この式を理想気体の状態方程式と呼ぶ。水蒸気を含まなない空気を**乾燥空気**と呼ぶ。いま, $M$を質量 $[\mathrm{kg}]$, $m$を乾燥空気の分子量[$\mathrm{mol/kg}$]とすると,
$$
\begin{eqnarray}
pV&=&nR^*T=\frac{M}{m}R^*T \\
p&=&\frac{M}{V}\frac{R^*}{m}T \\
&=&\rho R_dT
\end{eqnarray}
$$
$$
\therefore \quad p=\rho R_d T
$$

ここで, $R_d$:=$R^*/m$と定義し, $R_d$を乾燥空気の気体定数と呼ぶことにする。地球大気の場合, $R_d\simeq273 \mathrm{[Jkg^{-1}K^{-1}]}$である。



単位質量の空気の体積のことを**比容**と呼ぶ。この定義に従うと比容$\alpha$は
$$
\alpha:=V/M=1/\rho \, \mathrm{[m^3/kg]}
$$
と書くことが出来る。このとき状態方程式は,
$$
\begin{eqnarray}
p\alpha=R_dT
\end{eqnarray}
$$
と書き表すこともできる。



温位
-----------------------------------------------------------

### 結論
#### 温位の式
はじめに気圧と気温が$(p, T)$であった気塊の気圧を断熱変化によって$p_0$にした時の気塊の温度を$\theta$と定義すると、

$$
\begin{eqnarray}
\theta&=&T \left( \frac{p_0}{p} \right)^\kappa
\end{eqnarray}
$$
ここで、$\kappa$は、定圧比熱$c_p$と気体定数$R$を用いて, $\kappa=R/c_p$で定義される。

#### 温位の性質
断熱過程において$\theta$は一定値をとる。すなわち$\theta$は断熱過程における保存量である。



### 導出

ここではすべての量を単位質量あたりの量として表すことにする。気体分子の乱雑な動きに起因するエネルギーを気体の内部エネルギーという。今気体の内部エネルギーの変化量を$\Delta u$, 気体に加えられる熱を$\Delta q$, 気体になされる仕事を$\Delta w$と書くことにする。この時、エネルギー保存則を表す熱力学第一法則は、
$$
\begin{eqnarray}
\Delta u = \Delta q + \Delta w
\tag{1}
\end{eqnarray}
$$
と書くことができる。

単位質量当たりの気体の体積を比容といい$\alpha$で表す。比容の変化を$\Delta \alpha$, 気圧を$p$とすると, 
$$
\begin{eqnarray}
\Delta w = -p \times \Delta \alpha
\tag{2}
\end{eqnarray}
$$
と書くことができる。このとき,(1)は、
$$
\begin{eqnarray}
\Delta u = \Delta q -p \Delta \alpha
\tag{3}
\end{eqnarray}
$$
空気はほぼ理想気体と考えることが出来、内部エネルギーは温度のみによって決まるという法則がある（ジュールの法則）。よって,
$$
\begin{eqnarray}
u=u(T)
\end{eqnarray}
$$
と書ける。

いま、体積を一定に保った場合において ($\Delta \alpha = 0$), 気温を1 [K]上げるのに必要な熱を定積比熱$c_v$と定義すると,
$$
\begin{eqnarray}
c_v & = & \lim_{\Delta T \to 0 \\ \Delta \alpha=0} \frac{\Delta q}{\Delta T} \\
 & = & \lim_{\Delta T \to 0 \\ \Delta \alpha=0} \frac{\Delta u +p \Delta \alpha}{\Delta T} \\
 & = & \lim_{\Delta T \to 0 \\ \Delta \alpha=0} \frac{\Delta u }{\Delta T} \\
& = & \frac{du}{dT}
\end{eqnarray}
$$
$c_v=du/dT$より,
$$
\begin{eqnarray}
\Delta u = c_v \Delta T
\tag{4}
\end{eqnarray}
$$
(4)を(3)に代入すると,
$$
\begin{eqnarray}
\Delta q = c_v \Delta T +p \Delta \alpha
\tag{5}
\end{eqnarray}
$$
いま、理想気体の状態方程式,
$$
p=\rho R T
$$
の両辺を$\rho$で割ると, $\alpha=1/\rho$であるから、
$$
\begin{eqnarray}
p\alpha= R T
\tag{6}
\end{eqnarray}
$$
(6)より,
$$
\begin{eqnarray}
p (\Delta \alpha) + (\Delta p) \alpha = R \Delta T
\tag{7}
\end{eqnarray}
$$
(7)を(6)に代入すると, 
$$
\begin{eqnarray}
\Delta q = c_v \Delta T + R \Delta T - \alpha \Delta p 
\tag{8}
\end{eqnarray}
$$
いま、気圧を一定に保った場合において ($\Delta p = 0$), 気温を1 [K]上げるのに必要な熱を定圧比熱$c_p$と定義すると,
$$
\begin{eqnarray}
c_p & = & \lim_{\Delta T \to 0 \\ \Delta p=0} \frac{\Delta q}{\Delta T} \\
 & = & \lim_{\Delta T \to 0 \\ \Delta \alpha=0} \frac{(c_v + R)\Delta T}{\Delta T} \\
& = & c_v + R
\end{eqnarray}
$$
よって、
$$
\begin{eqnarray}
c_p & = & c_v + R
\tag{9}
\end{eqnarray}
$$
式(9)をマイヤーの式という。これを使うと, (8)は、
$$
\begin{eqnarray}
\Delta q = c_p \Delta T - \alpha \Delta p 
\tag{10}
\end{eqnarray}
$$
となる。(10)は熱力学第一法則(1)を気温$T$、比容$\alpha$, 気圧$p$を使って表した式であることに注意。

いま、気塊の外部から熱が加えられることがないとする。このような過程を**断熱過程**という。大気現象の多くは断熱過程とみなすことができる。断熱過程においては、$\Delta q=0$であるから、(10)は、
$$
\begin{eqnarray}
c_p \Delta T = \alpha \Delta p 
\tag{11}
\end{eqnarray}
$$
となり、これに(6)を代入すると,
$$
\begin{eqnarray}
c_p \Delta T & = & \frac{RT}{p} \Delta p \\ 
\frac{\Delta T}{T} & = & \kappa \frac{\Delta p}{p} \hspace{15pt} (\kappa=R/c_p)
\tag{12}
\end{eqnarray}
$$
(12)を微分の形で書くと,
$$
\begin{eqnarray}
\frac{d T}{T} & = & \kappa \frac{d p}{p}
\tag{13}
\end{eqnarray}
$$
これを$(p, T)$から$(p_0, T_0)$までで積分すると、
$$
\begin{eqnarray}
\int_T^{T_0}\frac{dT}{T} & = & \kappa\int_p^{p_0} \frac{d p}{p} \\
\ln{T_0}-\ln{T}&=&\kappa(\ln{p_0}-\ln{p}) \\
\ln\left( \frac{T_0}{T} \right)&=&\ln\left( \frac{p_0}{p} \right)^\kappa
\tag{14}
\end{eqnarray}
$$
ここで, $\ln(x)=\log_{e}x$である。$e$はネイピア数と呼ばる無理数であり, $e \approx 2.718 $ である。

ここで、$T_0$を温位$\theta$と定義すると,
$$
\begin{eqnarray}
\theta&=&T \left( \frac{p_0}{p} \right)^\kappa
\tag{15}
\end{eqnarray}
$$
(15)は、はじめに気圧と気温が$(p, T)$であった気塊の気圧を断熱変化によって$p_0$にした時の気塊の温度が$\theta \,(=T_0)$であることを意味している。

次に断熱過程において, $\theta$が保存量であることを示す。(15)の対数をとると、
$$
\begin{eqnarray}
\ln{\theta}&=&\ln{T}+\kappa(\ln{p_0}-\ln{p})
\tag{16}
\end{eqnarray}
$$
(16)の微分をとると, 
$$
\begin{eqnarray}
d(\ln{\theta})&=&d(\ln{T})+\kappa\, d(\ln{p_0}-\ln{p})\\
\frac{d \theta}{\theta}&=&\frac{dT}{T}+\kappa\,\frac{dp}{p}\\
\tag{17}
\end{eqnarray}
$$
$p_0$は定数なので$d(\ln p_0)=0$となることを使った。ここで断熱過程において成り立つ(13)を用いると、(17)の右辺は0となることが示せるので$d \theta=0$。これは断熱過程において$\theta$が一定値をとること（$\theta$が保存量であること）を意味している。

### 定数の値
$$
\begin{eqnarray}
c_p & = & 1004 \, [\mathrm{J \, kg \, K^{-1}}] \\
R &=& 287  \, [\mathrm{J \, kg \, K^{-1}}] \\
\kappa&=&R/c_p \\
&=& 0.286\\
\end{eqnarray}
$$


断熱変化に伴う気体の状態変化
------------------------------------------
$$
\begin{eqnarray}
c_v \Delta T =  - p \Delta \alpha
\tag{5}
\end{eqnarray}
$$
体積増加にともなって気温は減少する

$$
\begin{eqnarray}
c_p \Delta T =  \alpha \Delta p 
\tag{10}
\end{eqnarray}
$$
気圧が下がると気温も下がる



## エンタルピー

熱力学第一法則 $du=\Delta q + \Delta w$において，単位質量の気体が行う仕事$\Delta w$を$\Delta w=-pd \alpha$とすると, $du=\Delta q - p d\alpha$である。

いま定容(定積)過程 ($d\alpha = 0$)のときの$\Delta q$を$\Delta q_v$と書くことにすると, $du=\Delta q_v$である。一方, 定圧仮定 ($p=$一定)のときの, $\Delta q$を$\Delta q_p$と書くことにすると, 
$$
du = \Delta q_p - p d\alpha
$$
よって，
$$
\begin{eqnarray}
\Delta q_p &=& du + p d\alpha \\
&=& \Delta q_v + p d\alpha
\end{eqnarray}
$$
となる。$d \alpha>0$のとき, $\Delta q_p>\Delta q_v$である。これは，定圧過程において加えた熱の一部は体積変化に使われるため，内部エネルギーを増加させるために定積仮定よりも多くの熱を必要とすることを意味する (**定圧過程の場合, 加えた熱のすべてが内部エネルギーの増加に使われるわけではない**)。

エンタルピー, $h$と言う量を
$$
\begin{eqnarray}
h:=u+p\alpha
\end{eqnarray}
$$
で定義する。定圧過程のとき$\Delta q_p=du+pd\alpha$であるから, $dh=\Delta q_p$。よって，**定圧過程において加えた熱はエンタルピーの変化に等しい** (加えた熱を気体の状態量の変化として表すことが出来る)。



### 例：開いた系におけるエンタルピー

物質の流入(流出)がある系のことを**開いた系**と呼ぶ。

$U_1$: 系に流入するエネルギー

$\delta M_1$: 系1の外から系1に流入する流体$F_1$の質量

$\delta M_2$: 系1の中から流出する流体の$F_2$質量

$\alpha_i$: 流体$F_i$ ($i=1,2$)の比容(単位質量あたりの体積)

$u_i$: 流体$F_i$ ($i=1,2$)の比内部エネルギー(単位質量あたりの内部エネルギー)
$$
\begin{eqnarray}
U_1=\underbrace{\delta M_1 u_1}_{あ} + \underbrace{p\delta M_1 \alpha_1}_{い}
\tag{1}
\end{eqnarray}
$$
ここで,

あ: 質量$\delta M_1$の流体$F_1$のもつ内部エネルギー

い: 流体$F_1$を系$S_1$に入れるのに必要な仕事

である。エンタルピーの定義$h:=u+p\alpha$を使うと, (1)は,
$$
\begin{eqnarray}
U_1=\delta M_1 (u_1+p_1 \alpha_1)=\delta M_1 h_1
\tag{1}
\end{eqnarray}
$$
となる。同様に系から流出するエネルギーは$U_2$は，
$$
\begin{eqnarray}
U_2=\delta M_2 (u_2+p_2 \alpha_2)=\delta M_2 h_2
\tag{2}
\end{eqnarray}
$$
である。よって, 対象とする系のエネルギーの変化$\delta U$は
$$
\begin{eqnarray}
\delta U=\delta M_2 h_2 - \delta M_1 h_1
\tag{1}
\end{eqnarray}
$$
となる。以上の結果は, 対象とする系への**物質の流入(流出)があるとき**，系**のエネルギーの変化は**内部エネルギーのフラックスではなく，**エンタルピーのフラックスで表される**ことを意味する。

### 定圧比熱の表現

エンタルピーを使うと, 定圧比熱$c_p$は
$$
c_p:=\lim_{\Delta T \to 0}\bigg(\frac{\Delta q_p}{\Delta T}\bigg)\underbrace{=}_{定圧のとき\Delta q_p=h}\frac{dh}{dT}
$$
と表すことが出来る。



## 湿潤大気に関する諸量

### 用語

湿潤大気: 水蒸気を含む大気

乾燥大気: 湿潤大気から水蒸気を除いた(仮想的な)大気



### 定義

- $M_d$ :　乾燥大気の質量 $\mathrm{[kg]}$
- $M_v$ :　湿潤大気の質量 $\mathrm{[kg]}$
- $V$: 体積 $\mathrm{[m^3]}$
- $\rho_d$: 乾燥大気の密度 $\mathrm{[kg \, m^{-3}]}$
- $\rho_v$: 湿潤大気の密度 $\mathrm{[kg \, m^{-3}]}$



### 水蒸気混合比, $r$

$$
\begin{eqnarray}
r&:=&M_v/M_d \\
&=&\frac{M_v/V}{M_d/V} \\
&=&\frac{\rho_v}{\rho_d}  \\
\end{eqnarray}
$$

$r$の単位は$[\mathrm{kg/kg}]$であるが，最大でも0.02程度の値なので，1000倍して$[\mathrm{g/kg}]$を用いることがある。

#### 計算式

- $p_d$: 乾燥空気の分圧 $\mathrm{[Pa]}$
- $e$:  水蒸気圧 $\mathrm{[Pa]}$
- $\epsilon := R_d/R_v$

とすると, 
$$
\begin{eqnarray}
r&=&\frac{e/R_vT}{e/R_dT} =\frac{R_d}{R_v}\frac{e}{p_d}=\epsilon\frac{e}{p_d}.
\end{eqnarray}
$$
$p_d=p-e$であるから，
$$
\therefore \, r =\epsilon\frac{e}{p-e}  \\
$$


#### 定数の値

$R_v:=R^*/m_v=\frac{8.3 [\mathrm{J \,mol^{-1} \, K^{-1}}]}{18 \times 10^{-3} [\mathrm{J \,mol^{-1}}]} \simeq 461 [\mathrm{J \, kg^{-1} K^{-1}}]$

$R_d:=R^*/m_d$, $R_v:=R^*/m_v$

$\epsilon=\frac{R_d}{R_v}=\frac{m_v}{R_d}\simeq \frac{18}{29}\simeq0.622$



### 比湿, $q$

水蒸気の密度/湿潤空気の密度のことを比湿 (specific humidity)と呼ぶ。specificには単位質量あたりという意味がある。
$$
\begin{eqnarray}
q&=&\frac{\rho_v}{\rho_d+\rho_v}
=\frac{\frac{\rho_v}{\rho_d}}{1+\frac{\rho_v}{\rho_d}}
=\frac{r}{1+r}
=\frac{\epsilon\frac{e}{p-e}}{1+\epsilon\frac{e}{p-e}}. \\
\end{eqnarray}
$$

$$
\therefore \, q=\frac{\epsilon e}{p-e(1-\epsilon)}
$$

一方, $q=\frac{r}{1+r}$より, $(1+r)q=r$より $r(1-q)=q$であるので，
$$
\begin{eqnarray}
\therefore \, r=\frac{q}{1-q}.
\end{eqnarray}
$$
水蒸気混合比と比湿はほぼ同じ値となる。大きめに見積もっても$q\sim10^{-2}\mathrm{[kg/kg]}$なので，$1-q\simeq 1$となる（誤差1%程度）。

$\therefore \, q\simeq r$ (誤差$\sim O(1 \%)$ 

$O$はランダウのオーという記号であり，上の場合「1%のオーダー」と読む。$A=O(B)$のとき, $A/B \sim 1$を意味する。



### 比容

単位質量あたりの体積のことを比容 (specific volume)と呼ぶ。

乾燥大気の比容, $\alpha_d := V/M_d$

湿潤大気の比容, $\alpha$
$$
\begin{eqnarray}
\alpha&:=&\frac{V}{M_d+M_v} \\
&=&\frac{\frac{V}{M_d}}{1+\frac{M_v}{M_d}} \\
&=&\frac{\alpha_d}{1+r}
\end{eqnarray}
$$


### 凝結物を含む気体の比容

凝結物を含む大気の比容を便宜的に先ほどと同じ$\alpha$で表すことにする。

$V_a$: 湿潤空気の体積

$V_l$: 液相の水の体積

$V_i$: 固相の水の体積

とすると,
$$
\begin{eqnarray}
\alpha&:=&\frac{V_a+V_l+V_i}{M_d+M_v+M_l+M_i} \\
&=&\frac{\frac{V_a}{M_d}+\frac{V_l}{M_d}+\frac{V_i}{M_d}}{1+\frac{M_v}{M_d}+\frac{M_l}{M_d}+\frac{M_i}{M_d}} \\
\end{eqnarray}
$$
である。液相の水の比容$\alpha_l:=V_l/M_l$, 混合比$r_l:=M_l/M_d$と定義すると, 
$$
\begin{eqnarray}
\frac{V_l}{M_d}=\frac{V_l}{M_l}\frac{M_l}{M_d}=\alpha_l r_l
\end{eqnarray}
$$
である。同様に，固相の水の比容$\alpha_i:=V_i/M_i$, 混合比$r_i:=M_i/M_d$と定義すると, 
$$
\begin{eqnarray}
\frac{V_i}{M_d}=\frac{V_i}{M_l}\frac{M_i}{M_d}=\alpha_i r_i
\end{eqnarray}
$$
である。ここで，
$$
\begin{eqnarray}
\alpha_d&:=&\frac{V_a}{M_d} \\
r_T&:=&r+r_l+r_i
\end{eqnarray}
$$
と定義すると, 
$$
\begin{eqnarray}
\alpha
&=&\frac{\frac{V_a}{M_d}+\frac{V_l}{M_d}+\frac{V_i}{M_d}}{1+\frac{M_v}{M_d}+\frac{M_l}{M_d}+\frac{M_i}{M_d}} \\
&=&\alpha_d\frac{ 1 +r_l\frac{\alpha_l}{\alpha_d}+r_i\frac{\alpha_i}{\alpha_d}}{1+r+r_l+r_i} \\
&\simeq&\frac{\alpha_d}{1+r_T}
\end{eqnarray}
$$
が得られる ($\because \,\alpha_l, \alpha_i \lesssim \frac{\alpha_d}{1000}$)。



### 相対湿度, $H$

- $e$: 水蒸気圧
- $e^*$: 飽和水蒸気圧

とする。このとき，相対湿度$H$を
$$
H:=e/e^*
$$
と定義する。$H$の値は0~1程度の範囲の無次元量(単位の無い量)となるが，100倍して$\%$で表すことが多い。過飽和という現象があるため，$H$の値は1 (100%)を超えることもある。

$r=\epsilon e/(p-e)$であることを使うと, 
$$
\begin{eqnarray}
(p-e)r=\epsilon e \\
pr - er = \epsilon e \\
e=\frac{rp}{r+\epsilon}
\end{eqnarray}
$$
であるから，
$$
H=\frac{\frac{rp}{r+\epsilon}}{\frac{r^*p}{r^*+\epsilon}}=\frac{r}{r^*}\frac{r^*+\epsilon}{r+\epsilon}=\frac{r}{r^*}\bigg(\frac{1+r^*/\epsilon}{1+r/\epsilon} \bigg)\simeq\frac{r}{r^*}.
$$

$$
\therefore \, H:=\frac{e}{e^*}\simeq\frac{r}{r^*}
$$

# 仮温度と仮温位

湿潤空気は水蒸気量によって，気体定数の値が変わる。このため，気体定数は乾燥空気の値を使う代わりに，気温の値を調節することが良く行われる。この調節された気温のことを仮温度 (virtual temperature)と呼ぶ。

乾燥空気の比容, $\alpha_d:=\frac{V}{M_d}$, 湿潤空気の比容, $\alpha := \frac{V}{M_d+M_v}$と定義する。このとき，
$$
\alpha=\frac{V}{M_d+M_v}=\frac{\frac{V}{M_d}}{1+\frac{M_V}{M_d}}=\frac{\alpha_d}{1+r}
$$
となる。状態方程式$p_d \alpha_d=R_dT$と分圧の法則$p=p_d+e$を使うと,
$$
\begin{eqnarray}
\alpha=\frac{\alpha_d}{1+r}=\frac{R_dT}{p_d}\frac{1}{1+r}=\frac{R_dT}{p}\frac{p_d+e}{p_d}\frac{1}{1+r}.
\end{eqnarray}
$$
さらに$r=\epsilon\frac{e}{p_d}$であるから,
$$
\begin{eqnarray}
\frac{p_d+e}{p_d}=1+\frac{e}{p_d}=1+\frac{r}{\epsilon}.
\end{eqnarray}
$$
$R':=R_d\frac{(1+r/\epsilon)}{1+r}$と定義すると
$$
\alpha=\frac{R_dT}{p}\frac{1+r/\epsilon}{1+r}=R'\frac{T}{p}.
$$
となる。$\epsilon < 1$より，$R'>R_d$である。

いま，仮温度, $T_v$と呼ばれる量を,
$$
T_v:=\frac{R'}{R_d}T
$$
と定義すると, $R_dT_v=R'T$より, 
$$
T_v=\frac{1+r/\epsilon}{1+r}T\simeq T\bigg(1+\frac{r}{\epsilon}\bigg)(1-r).
$$
ここで，$(1+r)^{-1}\simeq 1-r$を使った。よって，
$$
T_v\simeq T\bigg(1-r+\frac{r}{\epsilon}(1-r)\bigg) = T\bigg(1-r+\frac{r}{\epsilon}-\frac{r^2}{\epsilon}\bigg) \simeq T\bigg(1+\bigg(\frac{1}{\epsilon}-1 \bigg)r \bigg)
$$
$1/\epsilon-1\simeq 1/0.622 -1 \simeq0.608$を使うと,
$$
T_v=T(1+0.608r)
$$
となる。仮温度を使うと，次のように気体定数を乾燥空気の値としたままで，状態方程式を用いることができる。$T_v:=\frac{R'}{R_d}T$より，$R'=\frac{R_dT_v}{T}$であるが，これを状態方程式$p\alpha=R'T$に代入すると，
$$
\therefore \, p\alpha=R_d T_v.
$$
$T=300 \, \mathrm{[K]}$, $r=0.015 \mathrm{[kg/kg]}$のとき, $T_v$の値は$T\simeq303 \, \mathrm{[K]}$となり，水蒸気混合比が$r$が$0$から$0.015 \mathrm{[kg/kg]}$($15 \mathrm{[g/kg]}$)に増えることは$3 \, \mathrm{[K]}$の気温上昇に相当する。

$T_v$を使って仮温位と呼ばれる量, $\theta_v$を,
$$
\theta_v:=T_v\bigg(\frac{p_0}{p}\bigg)^\kappa
$$
で定義する ($\kappa=R_d/C_p \simeq 287.04/1005.7 \simeq 0.285$)。





気象学でよく使う数学公式
------------------------------------------

### 微分の定義
$y=y(x)$とする。
$$
dx \equiv \Delta x
$$

$$
dy \equiv \frac{dy}{dx}dx
$$

$dx$, $dy$のことを微分と呼ぶ。



### 微分の公式

$f=f(x)$, $g=g(x)$のとき, 積の微分公式から,
$$
\frac{d (fg)}{dx} = \frac{df}{dx}g+f\frac{dg}{dx}
$$
これを使うと微分の定義から,
$$
d(fg)=fdg + gdf
$$

対数関数の微分公式から,
$$
\frac{d \ln x}{dx} = \frac{1}{x}
$$
微分の定義から,
$$
d (\ln x) = \frac{1}{x}dx
$$
$\Delta x \to 0$のとき, $\Delta y \approx dy$なので、
$$
\Delta(fg)=f\Delta g+ g \Delta f
$$

$$
\Delta (\ln x) = \frac{1}{x}\Delta x
$$
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

plt.rcParams["font.size"] = 14   # 文字の大きさ

fig, ax = plt.subplots(1, 1, figsize=(5, 5))
#ax = fig.add_subplot(111)
ax.set_xlabel("T [$\deg C$]", fontsize = 14)
ax.set_ylabel("$(de_{s}/e_{s})/dT$ [%/K]", fontsize = 14)

T = np.arange(-5,40,0.1)
Li00=2.501E6
cpii=1870.
cpi=4190.
Rv=461.50
L=Li00+(cpii-cpi)*T # Emanuel (1994), p.115
Tk=T+273.16
y = L/Rv/Tk**2 * 100

ax.plot(T,y,color="black")

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
fig.savefig(FIG, bbox_inches='tight')
print("FIG: "+FIG)



import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()

plt.rcParams["font.size"] = 14   # 文字の大きさ

ax = fig.add_subplot(111)
ax.set_xlabel("T [$\deg C$]", fontsize = 14)
ax.set_ylabel("$e_{s}$ [hPa]", fontsize = 14)

x = np.arange(-5,35,0.1)
y = 6.112*np.exp((x*17.67)/(x+234.15))

ax.plot(x,y,color="black")

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
fig.savefig(FIG)
print("FIG: "+FIG)



# 準地衡モデル

[TOC]

## 温帯低気圧の特徴

温帯低気圧

- 日々の天気の変化を支配
- 地球規模の熱・水の輸送に重要

水平スケール$\sim$数千kmであり，風はほぼ地衡風バランスしている.

## 準地衡モデルの導出

### 速度ベクトルの分解

風速ベクトルの$x$, $y$方向成分をそれぞれ$u$, $v$で表すことにし， 風速の水平成分を表すベクトルを$\mathbf{u}$と書くことにする. $\mathbf{i}, \mathbf{j}$はそれぞれ$x$, $y$方向の単位ベクトルとすると$\mathbf{u}=u\mathbf{i}+v\mathbf{j}$である.

$\phi$をジオポテンシャルとする ($\phi:=\int_0^z gdz$). コリオリ係数を$f_0$と書くことにすると, 地衡風の$x$, $y$方向成分$u_g$, $v_g$はそれぞれ, 
$$
u_g:=-\frac{1}{f_0}\frac{\partial \phi}{\partial y}, \quad v_g:=\frac{1}{f_0}\frac{\partial \phi}{\partial x}
$$
で定義される. 

---

#### 練習

地衡風の水平成分を表すベクトル$\mathbf{u}_g$を, $\mathbf{u}_g=u_g\mathbf{i}+v_g\mathbf{j}$と定義する. $\mathbf{k}$を$z$方向の単位ベクトルとし，水平方向の勾配演算子$\nabla$ (ナブラ)を$\nabla:=\mathbf{i}\frac{\partial }{\partial x}+\mathbf{j}\frac{\partial }{\partial y}$で定義する. $\times$をベクトルの外積の記号とするとき,
$$
\mathbf{u}_g:=\frac{1}{f_0}\mathbf{k}\times\nabla\phi
$$
と書ける. このことを示せ.

---



実際の風から地衡風を差し引いた風の成分を非地衡風 (**a**geostrophic wind)と呼び, 非地衡風の水平成分$\mathbf{u}_a$を, $\mathbf{u}_a:=\mathbf{u}-\mathbf{u}_g$で定義する (接頭辞aは後に続く内容の否定を表すので，ageostrophicはgeostrophicで無い, という意味になる). 

ここで，非地衡風の大きさが地衡風の大きさがよりもずっと小さく ($|\mathbf{u}_a|\ll |\mathbf{u}_g|$), 風速の鉛直成分$\omega$が微小であるケースを考え, **運動方程式において**, $\mathbf{u}\simeq \mathbf{u}_g$, $\omega \simeq 0$と近似する. この近似を**準地衡近似** (Quasi-geostrophic approximation)と呼ぶ. 

このとき, 運動方程式における移流項$\frac{D}{D t}=\frac{\partial }{\partial t}+u\frac{\partial }{\partial x}+v\frac{\partial }{\partial y}+\omega\frac{\partial }{\partial p}$は,
$$
\frac{D_g}{D t}:=\frac{\partial }{\partial t}+u_g\frac{\partial }{\partial x}+v_g\frac{\partial }{\partial y}
$$
で近似される. 



### 運動方程式

---

#### 練習

$\mathbf{k}$を$z$方向の単位ベクトルとして, $\mathbf{k}=(0,0,1)$と定義する.　このとき, $\mathbf{k} \times (\mathbf{k} \times \mathbf{a})= -\mathbf{a}$となることを示せ.

---

$f_0\mathbf{u}_g=\mathbf{k}\times\nabla\phi$より, $f_0\mathbf{k} \times \mathbf{u}_g=\mathbf{k}\times (\mathbf{k}\times\nabla\phi)=-\nabla\phi$となる. これを用いると, コリオリ力 $-f_0\mathbf{k}\times \mathbf{u}$と圧力傾度力$-\nabla \phi$の和を,
$$
\begin{eqnarray}
&&-f_0\mathbf{k}\times \mathbf{u}-\nabla \phi \\
&=&-f_0\mathbf{k}\times (\mathbf{u}_g+\mathbf{u}_a)+f_0\mathbf{k}\times\mathbf{u}_g \\
&=&-f_0\mathbf{k}\times \mathbf{u}_a
\end{eqnarray}
$$
と表すことができる. よって, 準地衡近似のもとで, 運動方程式の水平成分は,
$$
\frac{D_g \mathbf{u}_g}{Dt}=-f_0\times \mathbf{u}_a
$$
となる. 



### 連続の式

連続の式,
$$
\frac{\partial u}{\partial x}+\frac{\partial v}{\partial y}+\frac{\partial \omega}{\partial p}=0
$$
は, 
$$
\frac{\partial u_g}{\partial x}+\frac{\partial v_g}{\partial y}=0
$$
と$u=u_g+u_a$, $v=v_g+v_a$を使うと,
$$
\frac{\partial u_a}{\partial x}+\frac{\partial v_a}{\partial y}+\frac{\partial \omega}{\partial p}=0
$$
となる. 上の式は, 非地衡風の水平収束・発散 ($\frac{\partial u_a}{\partial x}+\frac{\partial v_a}{\partial y}$)が, 鉛直流$\omega$の発生に必要であることを示している.



### 熱力学第一法則

熱力学第一法則の移流項の$u$, $v$も地衡風で近似できるとすると, 
$$
\bigg(\frac{\partial }{\partial t}+\mathbf{u}_g\cdot\nabla\bigg)T-S_p\omega=0
$$
となる. ここで, $\sigma := \frac{RS_p}{p}$と定義し, 静水圧バランスの式, $\frac{\partial \phi}{\partial p}=-\frac{RT}{p}$を代入すると, 
$$
\bigg(\frac{\partial }{\partial t}+\mathbf{u}_g\cdot\nabla\bigg)\bigg( \frac{\partial \phi}{\partial p}\bigg)-\sigma \omega=0
$$
が得られる. 



## まとめ

### 定義

地衡風: $\mathbf{u}_g:=\frac{1}{f_0}\mathbf{k}\times\nabla\phi$

非地衡風: $\mathbf{u}_a:=\mathbf{u}-\mathbf{u}_g$

準地衡近似における移流項: $\frac{D_g}{D t}:=\frac{\partial }{\partial t}+u_g\frac{\partial }{\partial x}+v_g\frac{\partial }{\partial y}$

### 運動方程式

$$
\frac{D_g \mathbf{u}_g}{Dt}=-f_0 \mathbf{k}\times \mathbf{u}_a
$$

### 連続の式

$$
\frac{\partial u_a}{\partial x}+\frac{\partial v_a}{\partial y}+\frac{\partial \omega}{\partial p}=0
$$

### 熱力学第一法則

$$
\bigg(\frac{\partial }{\partial t}+\mathbf{u}_g\cdot\nabla\bigg)\bigg( \frac{\partial \phi}{\partial p}\bigg)-\sigma \omega=0
$$

ここで, $\sigma := \frac{RS_p}{p}$である.

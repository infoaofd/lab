# 準地衡モデルの要約

[TOC]

## はじめに

天気の変化を調べるためには，低気圧，高気圧の時間変化や上昇・下降気流の位置を知ることが重要である。低気圧周辺で上昇気流があるところでは，気圧低下に伴う断熱冷却によって水蒸気が水滴 (雲)となり，雨が降りやすくなる。中緯度の大気の流れはおおよそ地衡風となっている。そのため，地衡風の分布に応じた上昇流の分布や低気圧の時間変化がわかれば，大変有益となる。このための理論を準地衡モデルと呼ぶ。以下に準地衡モデルの概略を述べる。詳細はゼミのノートを参照のこと。

## 連続の式

$x$と $y$をそれぞれ東西・南北方向の座標，風速の$x$,$y$ 方向成分を$u$, $v$, 気圧を$p$, 鉛直$p$速度を$\omega$と定義する ($\omega:=Dp/Dt$)。このとき，気圧座標系における連続の式は，
$$
\begin{eqnarray}
\frac{\partial u}{\partial y}+\frac{\partial v}{\partial y}+\frac{\partial \omega}{\partial p} =0\\
\end{eqnarray}
\tag{1}
$$
となる。(1)式を$p$で積分すると,
$$
\begin{eqnarray}
\omega(p_0)-\omega(p)=- \int _{p} ^{p_0} \bigg( \frac{\partial u}{\partial y}+\frac{\partial v}{\partial y} \bigg)dp\\
\end{eqnarray}
\tag{2}
$$
が得られる。$p_0$を地表面の気圧とすると，地表面における$\omega$の値$\omega(p_0)$と，$p$から$p_0$における風の水平収束, $CNV$
$$
\begin{eqnarray}
CNV=-\bigg(\frac{\partial u}{\partial y}+\frac{\partial v}{\partial y}\bigg)
\tag{3}
\end{eqnarray}
$$
の値が分かっていれば，(2)より任意の気圧面($p$)における上昇流の値$\omega(p)$を知ることができる。

## 地衡風平衡

地衡風平衡とは，コリオリ力と気圧傾度力が釣り合った状態ことである。ジオポテンシャル高度を$Z$, コリオリ係数を$f_0$とし (ここでは定数と見なす), $x$と $y$をそれぞれ東西・南北方向の座標，地衡風の風速の$x$,$y$ 方向成分を$u_g$, $v_g$と定義すると地衡風平衡は，「コリオリ力＝気圧傾度力」という関係であることから，次の式で表すことができる。
$$
\begin{eqnarray}
f_0 u_g &=& -g\frac{\partial Z}{\partial y} \tag{4}\\
f_0 v_g &=& g\frac{\partial Z}{\partial x} \tag{5}
\end{eqnarray}
$$

## 地衡風の水平収束

中緯度の大気の流れはおおよそ地衡風となっていると述べた。そこで，地衡風の水平収束を求めることで，上昇流の大きさを見積もることができるか調べてみよう。(3)の右辺の$u$と$v$にそれぞれ，（4）と（5）の$u_g$, $v_g$を代入すると，
$$
\begin{eqnarray}
CNV&=&-\frac{g}{f_0}\bigg[\frac{\partial }{\partial x}\bigg(-\frac{\partial Z}{\partial y}\bigg)+
\frac{\partial }{\partial y}\bigg(\frac{\partial Z}{\partial x}\bigg)
\bigg] \\
&=& 0
\tag{6}
\end{eqnarray}
$$
となる。すなわち**地衡風の水平収束は0である**。このことから，地衡風の大きさからだけでは，上昇気流の大きさを見積もることができないことが分かる。

## 非地衡風

実際の風$u$, $v$は地衡風から少しずれている。そのずれのことを非地衡風とよぶ。ここで非地衡風の$x$, $y$成分をそれぞれ$u_a$,$v_a$と書くことにすると，
$$
\begin{eqnarray}
u&=&u_g+u_a \tag{7}\\
v&=&v_g+v_a \tag{8}\\
\end{eqnarray}
$$
という関係がある。（7），（8）を（1）に代入して，（6）を使うと，
$$
\begin{eqnarray}
\frac{\partial u_a}{\partial y}+\frac{\partial v_a}{\partial y}+\frac{\partial \omega}{\partial p} =0\\
\end{eqnarray}
\tag{9}
$$
という関係が得られる。これを$p$で積分すると，
$$
\begin{eqnarray}
\omega(p_0)-\omega(p)=- \int _{p} ^{p_0} \bigg( \frac{\partial u_a}{\partial y}+\frac{\partial v_a}{\partial y} \bigg)dp\\
\end{eqnarray}
\tag{10}
$$
となる。これより，地表面における$\omega$の値$\omega(p_0)$と，$p$から$p_0$における**非地衡風の水平収束**が分かっていれば， 任意の気圧面($p$)における上昇流の値$\omega(p)$を知ることができる。

地表面での地表面における$\omega$の値$\omega(p_0)$が0で，(10)の右辺が正(収束)であれば，$\omega(p)$は負(上昇流)となることが分かる。

## 渦度方程式

地衡風近似，非粘性を仮定した場合，気圧座標系における運動方程式の$x$,$y$成分はそれぞれ次のようになる。
$$
\begin{eqnarray}
 \frac{D u_g}{D t}= - f_0v_a \tag{11} \\
 \frac{D v_g}{D t}=  f_0u_a \tag{12} \\
\end{eqnarray}
$$
ここで，$\frac{D}{Dt}$は実質微分やラグランジュ微分と呼ばれ，ここでは，
$$
\begin{eqnarray}
 \frac{D}{D t}:=\frac{\partial }{\partial t}+u_g\frac{\partial }{\partial x}
 +v_g\frac{\partial }{\partial y} \tag{13} \\
\end{eqnarray}
$$
で定義される。いま，$\zeta_g$を地衡風の相対渦度の$p$方向成分とし，
$$
\begin{eqnarray}
 \zeta_g := \frac{\partial v_g}{\partial x}
 -\frac{\partial u_g}{\partial y}\tag{14} \\
\end{eqnarray}
$$
と定義する。(12)を$x$で偏微分した式から，(11)を$y$で偏微分した式を引くと，
$$
\begin{eqnarray}
 \frac{D \zeta_g}{D t}= - f_0 \bigg( \frac{\partial u_a}{\partial x}
 +\frac{\partial v_a}{\partial y} \bigg)\tag{15} \\
\end{eqnarray}
$$
という$\zeta_g$の時間変化を記述する方程式が得られる。この式より，右辺>0のとき（非地衡風が水平収束しているとき），地衡風の相対渦度が増すことが分かる。北半球において，相対渦度>0は低気圧を意味する。よって，(15)は，**非地衡風が水平収束する時，低気圧性の渦が強化される**ことを意味する。

また，(15)の右辺に(9)を使うと, 
$$
\begin{eqnarray}
 \frac{D \zeta_g}{D t}=  f_0 \frac{\partial \omega}{\partial p} \tag{16} 
\end{eqnarray}
$$
となる。つまり上昇流の鉛直分布が分かれば低気圧 (高気圧)の発達が予測できる。

## Qベクトル

非地衡風の大きさを精度よく求めるのは難しい。そのため，準地衡風近似のもとで，**地衡風の分布だけから上昇流域を推定する方法**が考案されている。

図のように，$x$, $y$を水平方向の座標とし, $x$を等温線に平行な方向（高温部を右手に見る方向を正とする)，$y$を$x$に直交する方向にとる。

<img src="C:\Users\boofo\Dropbox\LAB\GFD\QG\QG01_FIG01.png" alt="QG01_FIG01" style="zoom:67%;" />

$\mathbf{U_g}$は地衡風速を表すベクトルで， $x$軸上では$y$方向成分のみを持ち，その大きさは $x$方向に対し直線的に変化すると仮定する。円は等圧線を表し，点Lは低気圧の中心位置を表す。いま，座標系は右手系とし，鉛直方向の単位ベクトルを$\mathbf{k}$で表す。

また，ベクトルの外積を$\times$という記号で定義する。 $G$を気温の$y$方向の勾配の絶対値に比例する係数とし($G\ge0$)，ベクトル $\mathbf{Q}$を，
$$
\begin{eqnarray}
\mathbf{Q}=G(-\mathbf{k})×\frac{\partial \mathbf{U_g}}{\partial x} 
\tag{17} \\
\end{eqnarray}
$$
で定義すると，理論的には $\mathbf{Q}$の収束域が上昇流域に対応する。

上図において，点Aにおける$\mathbf{U_g}$は下向き，点Bにおける$\mathbf{U_g}$は上向きであることから，$x$が正方向に変化したとき，$\mathbf{U_g}$は上向き成分が大きくなることが分かる。よって，$\frac{\partial \mathbf{U_g}}{\partial x}$は上向きである。$\mathbf{k}$が上向きのベクトルの時，$x-y$平面上のベクトル$\mathbf{A}$は, $-\mathbf{k}\times\mathbf{A}$という演算によって，**右向きに90º曲げられる**。したがって，上図の場合，$\mathbf{Q}$は右向きとなる。

このようにして，等温線上の地衡風ベクトル$\mathbf{U_g}$の向きを次々に調べると，それをもとにして$\mathbf{Q}$の分布を求めることができる。$\mathbf{Q}$が収束している領域は準地衡モデルにおいて上昇流が期待される領域となる。

地衡風ベクトルの向きは，等高度線を調べれば分かるので，結局，等温線と等高度線が一枚の図に書いてあれば，上昇流域を推定することができる。

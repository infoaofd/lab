# 鉛直モード展開の初歩

[[_TOC_]]

## 概要

一般に流れを表す変数は東西($x$), 南北($y$), 鉛直($z$)方向および時間($t$)の関数として表される。例えば流速ベクトルの東西方向成分$u$は
$$
u=u(x,y,z,t)
$$
と表すことができる。大気・海洋の流れの多くは, 水平方向のスケール$\gg$鉛直方向のスケールの条件を満たし, 水平方向の運動が卓越することが多い。そこで問題を考えやすくするため, 

- 変数を水平方向と時間変化を表現する部分と
- 鉛直方向の構造を表す部分

の積として表すことで, 水平方向と時間のみが変化する複数の方程式のセット(方程式系と呼ぶ)に分解することがよくある。これを鉛直モード展開と呼ぶ。

正規直交基底の回で, 変数の水平方向の構造を三角関数で表し, 運動方程式を時間のみに依存する常微分方程式に分解するスペクトル法について解説した。ここでは似た分解を鉛直方向に対して行う。



## モード構造を決定するための方程式

ここで用いる方程式系は以下に述べるとおりである。ここで, 添え字は偏微分を行うことを意味し, 例えば,
$$
u_t=\partial u/\partial t, \quad u_{xt}=\partial^2 u/\partial t\partial x
$$
である。また, $\rho_0$は基準密度 (一定)を表し, $\overline{\rho}(z)$は基本場(流れがない状態)における密度の鉛直分布を表す。密度$\rho$と圧力$p$は静止状態からの偏差を表し, $\rho \ll \overline{\rho}(z)$である。

運動方程式の水平方向成分は,
$$
u_t-fv&=&-p_x/\rho_0 \\
v_t+fu&=&-p_y/\rho_0 \\
\tag{1}
$$
である。極端に強い流れは無いとして移流項を無視している。連続の式は
$$
u_x+v_y+w_z=0
\tag{3}
$$
である。流体の圧縮性は無視している。密度の保存式(熱力学第一法則に対応)は,
$$
\rho_t=-w\overline{\rho}_z
\tag{4}
$$
である。移流項は基本場における値が鉛直方向に移流される部分以外を無視した。さらに運動方程式の鉛直方向成分は静水圧平衡で近似できるとして,
$$
p_z=-\rho g
\tag{5}
$$
で与えられる。(4), (5)より直ちに,
$$
p_{zt}=w\overline{\rho}_z g
\tag{6}
$$
が得られる。

同一の方程式に含まれる変数は鉛直方向に同一の構造を持つとする。このとき, (1), (2)に含まれる変数$u$, $v$, $p$は$z$の関数$F(z)$でその鉛直構造を表現できる。水平方向と時間変化を表す関数を$\tilde{\quad}$ (チルダ)という記号を使って表すことにすると,
$$
\begin{eqnarray}
u=\tilde{u}(x,y,t)F(z) \tag{7} \\
v=\tilde{v}(x,y,t)F(z) \tag{8} \\
w=\rho_0\tilde{p}(x,y,t)F(z) \tag{9} \\
\end{eqnarray}
$$
となる。(1), (2)の方程式の形から, $\tilde{u}$, $\tilde{v}$と$\tilde{p}$で同じ$F(z)$を用いるため, (9)の$\tilde{p}$には$\rho_0$を掛けた。

$w$の鉛直構造を表す関数を$G(z)$と書くことにすると,
$$
w=\tilde{w}(x,y,t)G(z) \tag{10}
$$
となる。(7), (8), (10)を連続の式(3)$u_x+v_y+w_z=0$に代入すると,
$$
(\tilde{u}_x+\tilde{v}_y)F+\tilde{w}G_z=0 \tag{11}
$$
が得られる。次に(6)より,
$$
\rho_0 \tilde{p}_t F_z=\tilde{w} g \overline{\rho}_z G \tag{12}
$$
である。(11)を$z$で微分して, (12)を代入すると,
$$
(\tilde{u}_x+\tilde{v}_y)F_z+\tilde{w}G_{zz}
=(\tilde{u}_x+\tilde{v}_y)F_z + \frac{\tilde{p}_t \rho_0 F_z}{G g \overline{\rho}_z} G_{zz} \tag{13}
$$
となる。$N^2 =-g \overline{\rho}_z / \rho_0$とおき, (13)の両辺を$F_z$で割ると,
$$
\tilde{u}_x+\tilde{v}_y-\frac{1}{N^2}\frac{G_{zz}}{G} \tilde{p}_t=0 \tag{14}
$$
である。これより,
$$
\frac{\tilde{u}_x+\tilde{v}_y}{\tilde{p}_t}=\frac{G_{zz}}{N^2G}
$$
が得られる。この式の左辺は$x$, $y$, $t$の関数, 右辺は$z$のみの関数であるから, 上の式の両辺は$x$, $y$, $z$, $t$のいずれにも依存しない定数である必要がある。そこでこの定数を$-\lambda^2$とおくと, $x$, $y$, $t$に関する依存性を決定する方程式,
$$
\tilde{u}_x+\tilde{v}_y=-\lambda^2 \tilde{p}_t \tag{15}
$$
と, $z$に関する依存性を決定する方程式,
$$
G_{zz}=-\lambda^2 N^2 G \tag{16}
$$
という2つの方程式に分解することができる。



## モード構造の決定

### $w$の鉛直構造

ここでは簡単なケースとして, $N^2$ = 一定の場合について考える。まず, $w$の鉛直構造を決める$G(z)$を求める。 対象領域の上端と下端で鉛直流速は$0$であるとして, $z=0$と$z=H$において, $w=0$という条件を課すことにする。このとき, (10)$w=\tilde{w}(x,y,t)G(z)$より, $G=0$である。(15)より, $A$, $B$を任意定数として,
$$
G=A \sin (\lambda N z)+B \cos (\lambda N z)
$$
であるが, $z=0$で$G=0$より, $B=0$である。さらに, $z=H$で$G=0$より, 
$$
\lambda N H = n \pi \quad (n=1,2, \cdots)
$$
である。$\lambda$の値は$n$によって変化するのでこれ以降$\lambda_n$と書くことにする。

次に$A \sin (\frac{n \pi}{H}z)$が正規直交基底となるように$A$の値を決める。ここで関数$f$と$g$の内積$\big< f,g \big> $を,
$$
\big< f,g \big> = \int_0^H fg \, dz
$$
で定義する。$A \sin (\frac{n \pi}{H}z)$が直交性を有することは, 
$$
&&\int_0^H \sin \big ( \frac{m \pi}{H} z\big) \sin \big ( \frac{n \pi}{H} z\big) \, dz \\
&=&\frac{1}{2}\int_0^H \bigg( \cos \frac{(m-n) \pi}{H} z - \cos \frac{(m+n) \pi}{H} z \bigg) \, dz \\
&=&\frac{1}{2}\frac{H}{(m-n)\pi}\bigg[ \sin \frac{(m-n) \pi}{H} z \bigg]_0^H 
 - \frac{1}{2}\frac{H}{(m+n)\pi}\bigg[ \sin \frac{(m+n) \pi}{H} z \bigg]_0^H \\
&=& 0
$$
より分かる。次に, 
$$
&&\int_0^H \sin \big ( \frac{n \pi}{H} z\big) \sin \big ( \frac{n \pi}{H} z\big) \, dz \\
&=&\frac{1}{2}\int_0^H \bigg( \cos \frac{(n-n) \pi}{H} z - \cos \frac{(n+n) \pi}{H} z \bigg) \, dz \\
&=&\frac{1}{2}\big[ z \big]_0^H 
 - \frac{1}{2}\frac{H}{2n\pi}\bigg[ \sin \frac{2n \pi}{H} z \bigg]_0^H \\
&=& \frac{H}{2}
$$
である。よって, $A=2/H$とおいて,
$$
G_n(z)=\frac{2}{H} \sin \bigg(\frac{n\pi}{H}z\bigg) \tag{17}
$$
とすると, $G_n(z)$は, 内積
$$
\big< G_m, G_n \big> = \int_0^H G_m(z) G_n(z) \, dz
$$
に対して, 正規直交基底となる。



### $u$, $v$, $p$の鉛直構造

次に, $u$, $v$, $p$の鉛直方向の構造を決める関数$F(z)$を求める。(11)$(\tilde{u}_x+\tilde{v}_y)F+\tilde{w}G_z=0$より, $F \propto G_z$である。よって, $F_z \propto G_{zz}$であるので, (16) $G_{zz}=-\lambda_n^2 N^2 G$から, 
$$
\frac{1}{N^2} F_z = \lambda^2_n G
$$
だが, この両辺を$z$で偏微分すると,
$$
\bigg( \frac{1}{N^2} F_z \bigg)_z = \lambda^2_n F \tag{18}
$$
が得られる。$N^2$ = 一定のとき(18)は,
$$
F_{zz}=-\lambda^2 N^2 F \tag{19}
$$
となり, (16)と同じ形の方程式となる。よって, 
$$
F_n(z)=\frac{2}{H} \sin \bigg(\frac{n\pi}{H}z\bigg) \tag{20}
$$
とすると, $F_n(z)$も正規直交基底となる。



### 鉛直構造の特徴

#### $n=1$のとき

このとき,
$$
F_1(z)=\frac{2}{H} \sin \bigg(\frac{\pi}{H}z\bigg)
$$
であるが, $0<z<H$で, 常に$F_1(z)>0$であることから, $n=1$のとき, 流れの水平方向成分$u$, $v$は鉛直方向に同じ方向を向くことが分かる。$n=1$のモードを順圧モードと呼ぶ。また, $z=H/2$で, $F_1(z)$は最大値をとる。



#### $n=2$のとき

このとき,
$$
F_2(z)=\frac{2}{H} \sin \bigg(\frac{2\pi}{H}z\bigg)
$$
である。この場合, $z=H/2$で $F_2(z)=0$となり, $z<H/2$で$F_2(z)>0$, $z>H/2$で$F_2(z)<0$となることが分かる。したがって, $n=2$のとき, $z=H/2$を境に, $u$, $v$は向きを変える。$n=2$のモードを傾圧第一モードと呼ぶ。講義で大気を2層の流体に理想化したのは, このモードを取り出して考察したことを意味する。



### 水平方向成分の時間発展方程式

ここでは, 正規直交基底(17), (20)を用いて, (1)～(3)を分解する。

まず各変数がモードの和で表されるとして,
$$
u=\sum_{m=1}^{\infty} \tilde{u}_m F_m(z), \quad v=\sum_{m=1}^{\infty} \tilde{v}_m F_m(z), \\
p=\sum_{m=1}^{\infty} \rho_0 \tilde{p}_m F_m(z), \quad w=\sum_{m=1}^{\infty} \tilde{w}_m G_m(z),
$$
とおく。これらを運動方程式の$x$方向成分(1)に代入すると,
$$
\sum_{m=1}^{\infty} \bigg[ (\tilde{u_m})_t - f\tilde{v}_m + (\tilde{p}_m)_x\bigg]F_m(z)=0
$$
である。両辺に$F_n(z)$を掛けて, $0$から$H$まで$z$で積分すると, $F_m$の直交性から
$$
(\tilde{u}_n)_t - f\tilde{v}_n = - (\tilde{p}_n)_x \tag{21}
$$
が得られる。例えば,
$$
&&\int_0^H \bigg(\sum_{m=1}^{\infty} (\tilde{u_m})_t \, F_m(z) F_n(z) \bigg) \, dz \\
&=&\sum_{m=1}^{\infty}\bigg( (\tilde{u_m})_t \, \int_0^H F_m(z) F_n(z) \bigg) \, dz \\
&=&(\tilde{u_n})_t
$$
となる。他の項も同様に計算すればよい。 運動方程式の$y$方向成分(2)についても同様にして,
$$
(\tilde{v_n})_t + f\tilde{u}_n = - (\tilde{p}_n)_y \tag{22}
$$
を得ることができる。次に, 連続の式(14) $\tilde{u}_x+\tilde{v}_y-\frac{1}{N^2}\frac{G_{zz}}{G} \tilde{p}_t=0$に, (16)$G_{zz}=-\lambda^2_n N^2 G$を代入すると,
$$
(\tilde{u_m})_x+(\tilde{v_m})_y + \lambda^2_n \, (\tilde{p_m})_t=0
$$
となるので, これより, 
$$
(\tilde{p}_n)_t + \frac{1}{\lambda^2_n} \bigg( (\tilde{u_n})_x+(\tilde{v_n})_y \bigg)=0 \tag{23}
$$
が得られる。$\tilde {\quad}$を省略して書くことにすると,  第$n$モードの水平方向の時間発展を表す方程式は,
$$
\begin{eqnarray}
\frac{\partial u_n}{\partial t}-f v_n &=& - \frac{\partial p_n}{\partial x}, \tag{24} \\
\frac{\partial v_n}{\partial t}+f u_n &=& - \frac{\partial p_n}{\partial y}, \tag{25}\\
\frac{\partial p_n}{\partial t}&=& -\frac{1}{\lambda_n^2} \bigg( \frac{\partial u_n}{\partial x}+\frac{\partial v_n}{\partial y} \bigg), \tag{26}\\
\end{eqnarray}
$$
と書き表すことができる。



## 密度の鉛直微分が一定でない場合

鉛直構造を決定するために用いたのは次の2つの方程式
$$
G_{zz}=-\lambda^2_n N^2 G \tag{16}
$$


$$
\bigg( \frac{1}{N^2} F_z \bigg)_z = \lambda^2_n F \tag{18}
$$
であった。上記2つの方程式は, スツルム-リュービル型の方程式と呼ばれ, この型の方程式は, 適当な境界条件のもとでは, $N^2$ が一定とならない場合においても解が直交し, さらに実数$\lambda^2_n$の値が決められることが示されている (下記)。今回は簡単のため, $N^2$=一定の場合を考えたが, 基本場の密度構造が複雑で$N^2$が鉛直方向に変化する場合においても, 正規直交基底と$\lambda^2_n$の値を求めることができて, ここで説明したような鉛直モードへの分解が可能となる。



## スツルム-リュービル型方程式の解の直交性

### 微分方程式と境界条件

$p(x)>0$, $q(x)>$, $\rho(x)>0$に対し, 以下の$\phi(x)$を解とする微分方程式はスツルム・リュービル型と呼ばれる。
$$
\begin{eqnarray}
\frac{d}{dx}\bigg(p(x) \frac{d \,\phi(x)}{dx}\bigg) + q(x)\,\phi(x)=\lambda \, \rho(x) \,\phi(x)
\tag{19}
\end{eqnarray}
$$
$\rho(x)$のことを重み関数と呼ぶことがある。ここで, 演算子$L$を
$$
\begin{eqnarray}
L=\frac{d}{dx}\bigg(p(x) \frac{d }{dx}\bigg) + q(x)
\tag{20}
\end{eqnarray}
$$
で定義すると,  (19)は,
$$
\begin{eqnarray}
L\,\phi(x)=\lambda \, \rho(x) \, \phi(x)\
\tag{21}
\end{eqnarray}
$$
と書ける。解$\,\phi(x)$のことを$L$の固有関数, 定数$\lambda$を固有値と呼ぶ。$f(x)$と$g(x)$が$L$の固有関数であるとき,
$$
\begin{eqnarray}
L\,f(x)=\lambda \, \rho(x) \,f(x), \quad L\,g(x)=\lambda \, \rho(x)\,g(x)
\tag{22}
\end{eqnarray}
$$
である。解が計算領域の端で満たすべき条件のことを境界条件と呼ぶ。いま計算領域を$a<x<b$とする。(22)の境界条件として, ここでは,
$$
\begin{eqnarray}
f(a)=f(b)=0 \\
g(b)=g(b)=0
\tag{23}
\end{eqnarray}
$$
を課す。



### 内積の定義

ここで, $f(x)$と$g(x)$の内積 $\big< f, g \big>$を,
$$
\begin{eqnarray}
\big< f, g \big>=\int_a^b \rho(x) f(x)g(x)\,dx
\tag{24}
\end{eqnarray}
$$
と定義する（重み付き内積と呼ぶことがある）。



### 固有関数の直交性

適当な境界条件のもとで$f_i$, $f_j$が$L$の固有関数となるとき, 各々の固有値を$\lambda_i$, $\lambda_j$ ($\lambda_i \ne \lambda_j$)とすると, $\lambda_i \ne \lambda_j$のとき, $\big<  f_i, \, f_j \big>=0$となる。すなわち, **$\lambda_i \ne \lambda_j$のとき, $f_i$と$f_j$は直交する**。

#### 導出

$f_i$, $f_j$が$L$の固有関数であるとき, 
$$
\begin{eqnarray}
\frac{d}{dx}\bigg(p \frac{d f_i}{dx}\bigg) + qf_i = \lambda_i \rho f_i \tag{25} \\

\frac{d}{dx}\bigg(p \frac{d f_j}{dx}\bigg) + qf_j = \lambda_j \rho f_j \tag{26} \\

\end{eqnarray}
$$
(25)の第1式の両辺に右から$f_j$を掛けたものから, (22)の第2式の両辺に右から$f_i$を掛けたものを引くと,
$$
\begin{eqnarray}

\frac{d}{dx}\bigg(p \frac{d f_i}{dx}\bigg) - \frac{d}{dx}\bigg(p \frac{d f_j}{dx}\bigg)
=  (\lambda_i - \lambda_j ) \, \rho f_i f_j 

\end{eqnarray}
$$
両辺を$a$から$b$まで積分する。右辺は,
$$
\begin{eqnarray}
(\lambda_i - \lambda_j ) \, \int_a^b (\rho f_i f_j) \, dx 
=  (\lambda_i - \lambda_j ) \big< f_i, \, f_j \big>
\tag{27}
\end{eqnarray}
$$
となる。左辺は部分積分を用いると,

$$
\begin{eqnarray}

&& \int_a^b \bigg [\frac{d}{dx}\bigg(p \frac{d f_i}{dx}\bigg) \, f_j 
 - \frac{d}{dx}\bigg(p \frac{d f_j}{dx}\bigg) \,f_i \bigg] dx\\

&=& \bigg[p \frac{d f_i}{dx} f_j \bigg]_a^b 
 - \int_a^b \bigg [ \, p  \frac{d f_i}{dx} \frac{d f_j}{dx} \bigg] dx\\
&-& \bigg[p \frac{d f_j}{dx} f_i \bigg]_a^b
+ \int_a^b \bigg [ \, p  \frac{d f_j}{dx} \frac{d f_i}{dx} \bigg] dx\\

&=& \bigg[p \bigg( \frac{d f_i}{dx} f_j - \frac{d f_j}{dx} f_i  \bigg)  \bigg]_a^b
\tag{28}
\end{eqnarray}
$$
となる。いま境界条件として,
$$
\begin{eqnarray}
f_i(a)=f_i(b)=0 \\
f_j(a)=f_j(b)=0 
\tag{29}
\end{eqnarray}
$$
を課すと, (28)=0となり, (27)より, $\lambda_i \ne \lambda_j$のとき, $\big<  f_i, \, f_j \big>=0$となる。境界条件は（29）だけでなく,
$$
\begin{eqnarray}
f_i'(a)=f_i'(b)=0 \\
f_j'(a)=f_j'(b)=0 
\tag{30}
\end{eqnarray}
$$
でもよい。(29)の境界条件をディリクレ条件, (30)をノイマン条件と呼ぶ。以上より, ディリクレ条件(29)やノイマン条件(30)のもとで, スツルム・リュービル型の常微分方程式(19)の固有関数は内積(24)に関して直交する。




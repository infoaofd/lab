# 環境解析基礎III レジュメ 2

# ベクトルの微分法

<img src="FIG_RESUME/WIND_STICK_YATSUHIRO.png" alt="WIND_STICK_YATSUHIRO" style="zoom:15%;" />

<div style="text-align: center;">
    台風9919号上陸時の八代市(熊本県)における風向風速の時間変化
</div>
## 概要

流れは大きさと向きを持ったベクトル量として表現される。流体力学では流れの変化を扱うため，ここではベクトルの変化を表現するための方法である, ベクトルの微分法について学ぶ。



## 参考文献

高木隆司, 1993. キーポイント ベクトル解析. 岩波書店, 150pp.

ジョナサン・Ｅ・マーティン, 著, 近藤豊 , 市橋正生 訳, 2016. 大気力学の基礎 中緯度の総観気象, 東京大学出版会, 356pp.



## ベクトル関数

例えば，定点で風向・風速を1時間毎に測定したとする。この場合，風向・風速を表すベクトル$\mathbf{v}$は時刻$t$の関数を考えることができる。このときベクトル$\mathbf{v}$をベクトル関数と呼び, $\mathbf{v}(t)$と書く。



## 導ベクトル

$t$に関するベクトル関数$\mathbf{A}(t)$に関して, $t$が変化したときの$\mathbf{A}$の変化量を意味する$\Delta \mathbf{A}$というベクトルを,
$$
\Delta \mathbf{A}:=\mathbf{A}(t+\Delta t)-\mathbf{A}(t)
$$
と定義する。$\Delta t \to 0$の極限における, $\Delta \mathbf{A}/\Delta t$というベクトルを$\mathbf{A}$の**導ベクトル**$\frac{d \mathbf{A}}{dt}$と定義する。すなわち,
$$
\frac{d \mathbf{A}}{dt}:=\lim_{\Delta t \to 0} \frac{\Delta\mathbf{A}}{\Delta t}
$$
である。

**注意**: 誤解が生じないときには, 導ベクトルのことを便宜的に$\mathbf{A}$の微分と呼ぶことがある。



## 導ベクトルの成分表示

直交直線座標系 (デカルト座標系)$O-xyz$において，$x$, $y$, $z$軸方向の単位ベクトルを$\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$とする。このとき$\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$を**基本ベクトル**と呼ぶ。このとき，$\mathbf{A}(t)$を基本ベクトルと$x$, $y$, $z$軸方向の成分$A_x(t)$, $A_y(t)$, $A_z(t)$を使って, 
$$
\mathbf{A}(t)=A_x(t)\mathbf{i} + A_y(t)\mathbf{j} + A_z(t)\mathbf{k}
$$
と表した場合，**基本ベクトルが空間に固定され，動かないとする**と,
$$
\frac{d \mathbf{A}}{dt}=\frac{d A_x(t)}{dt}\mathbf{i}+\frac{d A_y(t)}{dt}\mathbf{j}+\frac{d A_z(t)}{dt}\mathbf{k}
$$
と表すことができる。$\mathbf{A}(t)=(A_x(t), A_y(t), A_z(t))$と書き表した場合は，
$$
\frac{d \mathbf{A}}{dt}=\bigg(\frac{d A_x(t)}{dt}, \frac{d A_y(t)}{dt}, \frac{d A_z(t)}{dt}\bigg)
$$
と書くこともできる。

**注**: 地球上の流体の運動を考えるときは，地球とともに回転する座標系を導入する (後述).　この場合, 基本ベクトルの向きが時々刻々変化する効果を考慮する必要があり，これがコリオリ力の由来となる。



## ベクトルの偏微分法

ベクトル$\mathbf{A}$が空間座標$x$と時間$t$の双方に依存して変化する場合, そのことを明示したい場合は$\mathbf{A}(x,t)$と書く。このとき, $\partial \mathbf{A}/\partial x$と$\partial \mathbf{A}/\partial t$は,
$$
\frac{\partial \mathbf{A}(x,t)}{\partial x}:=\lim_{\Delta t \to 0} \frac{\mathbf{A}(x+\Delta x, t)-\mathbf{A}(x, t)}{\Delta x}
$$

$$
\frac{\partial \mathbf{A}(x,t)}{\partial t}:=\lim_{\Delta t \to 0} \frac{\mathbf{A}(x, t+\Delta t)-\mathbf{A}(x, t)}{\Delta t}
$$

と定義される。成分表示, $\mathbf{A}(x,t)=A_x(x,t)\mathbf{i} + A_y(x,t)\mathbf{j} + A_z(x,t)\mathbf{k}$を使った場合, 基本ベクトルが空間に固定され，動かないとすると,
$$
\frac{\partial \mathbf{A}(x,t)}{\partial x}:=\frac{\partial A_x(x,t)}{\partial x}\mathbf{i}+\frac{\partial A_y(x,t)}{\partial x}\mathbf{j}+\frac{\partial A_z(x,t)}{\partial x}\mathbf{k}
$$

$$
\frac{\partial \mathbf{A}(x,t)}{\partial t}:=\frac{\partial A_x(x,t)}{\partial t}\mathbf{i}+\frac{\partial A_y(x,t)}{\partial t}\mathbf{j}+\frac{\partial A_z(x,t)}{\partial t}\mathbf{k}
$$

となる。


# 予備知識の確認


## 記号

$\mathrm{A}:=\mathrm{B}$  	$\mathrm{A}$を$\mathrm{B}$で定義する。$\mathrm{B}$の内容を表すのに，今後$\mathrm{A}$を用いると約束する。



## 問1

圧力の単位, 1 [hPa] (ヘクトパスカル)を[kg] (キログラム), [m] (メートル), [s] (秒)を使って表せ。

### 解答例
h (ヘクト)は100を表す接頭辞である。  

[$\mathrm{Pa}$]は単位面積あたりの力なので,　[$\mathrm{N} \,\mathrm{m}^{-2}$]  

力の大きさは, 質量と加速度の積に等しくなるので (運動の第2法則), [$\mathrm{N}$]は[$\mathrm{kg} \, \mathrm{m} \, \mathrm{s}^{-2}$]である。  

以上まとめると,  

1 [$\mathrm{hPa}$] = 100 [$\mathrm{kg} \, \mathrm{m}^{-1} \, \mathrm{s}^{-2}$]



## 問2

物理学において，「速度」と「速さ」という用語は区別して使用される。2つの用語の違いを述べよ。

### 解答例
速度はベクトル量で，速さはスカラー量である。  

速さは，速度(ベクトル)の大きさとして定義される。



## 問3

(1) 加速度の**定義**を述べよ。

解答例  

加速度は，単位時間当たりの速度変化，と定義される。  


(2) 加速度の**単位**を述べよ。 

解答例  

速度の単位は, [$\mathrm{m} \,\mathrm{s}^{-1}$]で, 加速度は単位時間あたりの速度の変化なので, 加速度の単位は[$\mathrm{m} \,\mathrm{s}^{-2}$]となる。



## 問4

$\phi (x,t) = kx-\omega t$のとき, 次の(1)と(2)を求めよ。  

(1) $\partial \phi / \partial x$
### 解答例
$\phi (x,t)$より, $\omega$と$k$は**定数**である。  

さらに$t$を**定数とみなして**, $\phi$を$x$で微分すると,
$$
\begin{array}{ll}
&\partial \phi / \partial x \\
&= \partial (kx-\omega t) / \partial x \\
&= k
\end{array}
$$


(2) $\partial \phi / \partial t$
### 解答例
$\phi (x,t)$より, $\omega$と$k$は**定数**である。  

さらに$x$を**定数とみなして**, $\phi$を$t$で微分すると,
$$
\begin{array}{ll}
&\partial \phi / \partial t \\
&= \partial (kx-\omega t) / \partial t \\
&= -\omega
\end{array}
$$


## 問5

$u=u(x)$, $x=x(t)$のとき, $u=u(x(t))$である。このとき，以下の公式を導け。

$$
\frac{du}{dt}=\frac{du}{dx}\frac{dx}{dt}
$$
### 解答例
$$
\frac{d u}{d t} := \lim_{\Delta t \to 0}\frac{1}{\Delta t}\bigg[ u (x(t+\Delta t)) - u(x(t) )\bigg]
$$

$\Delta x=x(t+\Delta t) - x(t)$とおくと,   $x(t+\Delta t)= x(t)+\Delta x$である。このとき,    


$$
\frac{d u}{d t} = \lim_{\Delta t \to 0}\frac{1}{\Delta x}\bigg[ u (x+\Delta x)) - u(x )\bigg]\frac{\Delta x}{\Delta t} \\
$$

$x(t)$が連続の時, $\Delta t \to 0$で$\Delta x \to 0$であることと，
$$
\lim_{\Delta t \to 0}F(t)G(t)=\lim_{\Delta t \to 0}F(t)\lim_{\Delta t \to 0}G(t) \\
$$
を用いると,

$$
\frac{d u}{d t} = \lim_{\Delta x \to 0}\frac{1}{\Delta x}\bigg[ u (x+\Delta x)) - u(x )\bigg]\lim_{\Delta t \to 0}\frac{\Delta x}{\Delta t} \\
$$
$$
=\frac{d u}{d x}\frac{d x}{d t}
$$
(終わり)



## 問6

$z (x,t) = \sin (kx-\omega t)$のとき, 次の(1)と(2)を求めよ。

(1) $\partial z / \partial x$

(2) $\partial z / \partial t$ 



### (1) $\partial z / \partial x$

### 解答例

$\phi=kx-\omega t$とおくと, $z=\sin \phi$である。  

このとき, 合成関数の微分公式(前問の結果の式)より,
$$
\begin{array}{ll}
& \partial z / \partial x \\
&= \frac{\partial z}{\partial \phi}\frac{\partial \phi}{\partial x} \\
&= \cos \phi  \cdot \frac{\partial (kx-\omega t)}{\partial x} \\
&=\cos(kx-\omega t) \cdot k
\end{array}
$$

$\therefore \, \partial z / \partial x=k\cos(kx-\omega t)$

## (2) $\partial z / \partial t$ 
### 解答例
$\phi=kx-\omega t$とおくと, $z=\sin \phi$である。  

このとき, 合成関数の微分公式(前問の結果の式)より,
$$
\begin{array}{ll}
& \partial z / \partial x \\
&= \frac{\partial z}{\partial \phi}\frac{\partial \phi}{\partial t} \\
&= \cos \phi  \cdot \frac{\partial (kx-\omega t)}{\partial t} \\
&=\cos(kx-\omega t) \cdot (- \omega)
\end{array}
$$

$\therefore \, \partial z / \partial x=-\omega \cos(kx-\omega t)$

## 問7

関数の積の導関数の公式, $(fg)' = f'g+fg'$を導出せよ。



### 解答例
$$
\begin{array}{ll}
(fg)\prime := \lim_{\Delta x \to 0}\frac{1}{\Delta x}\bigg[ f(x+\Delta x)g(x+\Delta x) - f(x)g(x) )\bigg] \\
= \lim_{\Delta x \to 0}\frac{1}{\Delta x}\bigg[ f(x+\Delta x)g(x+\Delta x)-f(x)g(x+\Delta x) \\
+f(x)g(x+\Delta x) - f(x)g(x) )\bigg] \\
= \lim_{\Delta x \to 0}g(x+\Delta x)\ \lim_{\Delta x \to 0}\frac{1}{\Delta x}\bigg[ f(x+\Delta x)-f(x)\bigg] \\
+f(x)\lim_{\Delta x \to 0}\frac{1}{\Delta x}\bigg[g(x+\Delta x) - g(x) )\bigg] \\
=g(x)f\prime(x)+f(x)g\prime(x)
\end{array}
$$
$$
\therefore \, (fg)\prime = f \prime g + f g\prime
$$



## 問8

関数$f$の微分と呼ばれる量$df$を,  

$$
df:=\frac{df}{dx} dx
$$

と定義する。このとき,   

$$
d(fg)=(df)g+f(dg)
$$

を導け。

### 解答例
$(fg)' = f'g+fg'$より,

$$
\frac{d (fg)}{dx}=\frac{d f}{dx}g+f\frac{d g}{dx}
$$

両辺に$dx$をかけると,

$$
\frac{d (fg)}{dx}dx=\bigg( \frac{d f}{d x}{dx}\bigg)g+f\bigg(\frac{d g}{dx}dx\bigg)
$$
微分の公式,

$$
df=\frac{df}{dx} dx
$$

をつかうと,

$$
d(fg)=(df)g+f(dg)
$$
が言える。

## 問9

$R$を気体定数と呼ばれる定数とする。$p$, $V$, $T$をそれぞれ，気圧, 体積, 温度としたとき (これらは変数), 常温の空気は次の式にほぼ従う。

$$
pV=RT
$$

このとき,

$$
p \,dV + V \, dp = R \, dT
$$
となることを示せ。

### 解答例
$pV=RT$の両辺の微分をとると，

$$
d(pV)=d(RT)
$$

$R$は定数なので，$d(RT)=RdT$となる。よって，
$$
\begin{eqnarray}
d(pV)=RdT
\tag{あ}
\end{eqnarray}
$$


また，微分の公式$d(fg)=(df)g+f(dg)$より,
$$
\begin{eqnarray}
d(pV)=p(dV)+V(dp)
\tag{い}
\end{eqnarray}
$$

である。(あ), (い)より，
$$
\therefore \, pdV+Vdp = RdT
$$



## 問10

$f=f(x,y)$のとき, $f$の増分, $\Delta f$を

$$
\Delta f := f(x+d x, y+d y)-f(x,y)
$$

で定義する。また，$f$の全微分と呼ばれる量, $df$を

$$
df:=\frac{\partial f}{\partial x}dx+\frac{\partial f}{\partial y}dy
$$

で定義する。$f(x, y)=xy$のとき, $\Delta f$ と$df$の差が, $(dx)(dy)$となることを示せ。



### 解答例
$$
\Delta f=(x+d x)(y+d y) - xy \\
= xdy+ydx+(dx)(dy)
$$

$$
df=\frac{\partial(xy)}{dx}dx  + \frac{\partial(xy)}{dy}dy \\
=ydx+xdy
$$

$$
\therefore \Delta f - df=(dx)(dy)
$$





## 問11

逆関数の微分公式

$$
\frac{dy}{dx}=\frac{1}{\frac{dx}{dy}}
$$

を導出せよ。

### 解答例
$y=f(x)$が連続の時, $\Delta x \to 0$ならば, $\Delta y \to 0$である。よって，

$$
\frac{d y}{d x}=\lim_{\Delta x \to 0}\frac{\Delta y}{\Delta x} \\
=\lim_{\Delta x \to 0}\frac{1}{\frac{\Delta x}{\Delta y}} \\
=\frac{1}{\lim_{\Delta y \to 0}\frac{\Delta x}{\Delta y}} \\
=\frac{1}{\frac{dx}{dy}}
$$

$$
\therefore \frac{dx}{dy}=\frac{1}{\frac{dy}{dx}}
$$

## 練習

### 練習1

逆関数の導関数の公式を用いて, $y=\ln x$の導関数が$y \prime = 1/x$となることを示せ。

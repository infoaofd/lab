# コリオリ力 その2

その1では一部の式の導出に幾何学的な方法を使ったので，慣れないとイメージがつかみにくいかもしれません。その2では，代数的な手法を使って説明します。こちらの方が式の変形だけなので，人によってはしっくりくるかもしれません。時間変化する回転運動を記述するには，ベクトルの微分と外積が必要ですので，本資料ではこれらを使用しています。  

ベクトルの微分と外積の概要については，本資料に記載しましたが，未習の方は図書館などで適宜書籍を参照するとよいと思います。大抵の力学の教科書には，ベクトルの微分と外積の説明が記載されています。また，「ベクトル解析」と名のついた書籍にも解説されています。  

[TOC]

## 記号

- A := Bは「AをBと定義する」という意味で用いられる  
- 物理量の後の[]はその物理量の単位を意味する。T [$\mathrm{s}$]は, Tの単位が$\mathrm{s}$という意味  
- [$\mathrm{s}$]は秒, [$\mathrm{m}$]はメートル, [$\mathrm{kg}$]はキログラムを意味する  
- 太字はベクトルを表す。例えば, $\mathbf{A}$はベクトル$A$の意味  
- 単位ベクトル := 長さ1のベクトル  
- $\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$はそれぞれ$x$, $y$, $z$軸方向の単位ベクトルを表す。
- 従って，ベクトル$\mathbf{A}$の$x$, $y$, $z$方向成分をそれぞれ, $A_x$, $A_y$, $A_z$とするとき,

$$
\begin{eqnarray}
\mathbf{A} &=& A_x  \mathbf{i}  + A_y  \mathbf{j} + A_z  \mathbf{k} \\
 &=& (A_x, A_y, A_z) \\
\end{eqnarray}
$$

- 経度を$\varphi$ , 緯度を$\theta$と表す。

- 余緯度 $\hat{\theta}$(シータ・ハット)と呼ばれる量を, $\hat{\theta}:= \pi/2-\theta$と定義する。




## 予備知識

### 角速度ベクトル

角速度ベクトル, $\mathbf{\Omega}=({\Omega}_x, {\Omega}_y, {\Omega}_z)$

#### 定義

- 向き=回転軸に平行で，右ねじの進む方向

- 長さ＝回転角速度の大きさに比例

角速度 := 1秒間あたりの角度変化 [ラジアン/秒]

※ラジアンは正確には単位ではない(割合)なので，角速度の正確な単位は, [1/s]  



### ベクトルの外積

$$
\begin{eqnarray}
\mathbf{A}\times\mathbf{B}=\left|\mathbf{A}\right|\left|\mathbf{B}\right|\sin\theta\mathbf{m}
\end{eqnarray}
$$

$\theta$ := $\mathbf{A}$と$\mathbf{B}$のなす角度。$\mathbf{A}$から$\mathbf{B}$に向かって測る。  

$\mathbf{m}$ := $\mathbf{A}$と$\mathbf{B}$の双方に直交する単位ベクトル。向きは$\mathbf{A}$を$\mathbf{B}$に向かって回したとき, 右ねじが進む方向  

### 外積の性質

外積の定義から，次の性質を示すことができる。
$$
\begin{eqnarray}
\mathbf{A}\times\mathbf{A}&=&\mathbf{0}\\
\mathbf{B}\times\mathbf{A}&=&-\mathbf{A}\times\mathbf{B}
\end{eqnarray}
$$

### 単位ベクトルの外積

$\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$をそれぞれ$x$, $y$, $z$軸方向の単位ベクトルとする。このとき, $\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$はすべて直交しているので，
$$
\begin{eqnarray}
\mathbf{i}\times\mathbf{i}&=&\mathbf{0}, \quad \mathbf{j}\times\mathbf{j}&=&\mathbf{0}, \quad \mathbf{k}\times\mathbf{k}&=&\mathbf{0}\\

\mathbf{i}\times\mathbf{j}&=&\quad \mathbf{k}, \quad \mathbf{j}\times\mathbf{k}&=&\quad \mathbf{i}, \quad \mathbf{k}\times\mathbf{i}&=&\mathbf{j}\\

\mathbf{j}\times\mathbf{i}&=&-\mathbf{k}, \quad \mathbf{k}\times\mathbf{j}&=&-\mathbf{i}, \quad \mathbf{i}\times\mathbf{k}&=&-\mathbf{j}\\

\end{eqnarray}
$$

### 外積の成分表示

$$
\begin{eqnarray}
\mathbf{A} &=& A_x  \mathbf{i}  + A_y  \mathbf{j} + A_z  \mathbf{k} = (A_x, A_y, A_z) \\
\mathbf{B} &=& B_x  \mathbf{i}  + B_y  \mathbf{j} + B_z  \mathbf{k} = (B_x, B_y, B_z) \\
\end{eqnarray}
$$

のとき, 
$$
\begin{eqnarray}
&&\mathbf{A} \times \mathbf{B}  \\
&=& (A_y B_z - B_y A_z) \mathbf{i} + (A_z B_x - B_z A_x) \mathbf{j}+(A_x B_y - B_x A_y) \mathbf{k} \\
\end{eqnarray}
$$

#### 導出

$$
\begin{eqnarray}
&&\mathbf{A} \times \mathbf{B}  \\

&=& (A_x \mathbf{i} + A_y \mathbf{j}+ A_z \mathbf{k})\times (B_x \mathbf{i} + B_y \mathbf{j}+ B_z \mathbf{k})\\
\\
&=&A_x\mathbf{i}\times B_x \mathbf{i}+A_x\mathbf{i}\times B_y \mathbf{j}+A_x\mathbf{i}\times B_y \mathbf{k}\\
&+&A_y\mathbf{j}\times B_x \mathbf{i}+A_y\mathbf{j}\times B_y \mathbf{j}+A_y\mathbf{j}\times B_y \mathbf{k}\\
&+&A_z\mathbf{k}\times B_x \mathbf{i}+A_z\mathbf{k}\times B_y \mathbf{j}+A_z\mathbf{k}\times B_y \mathbf{k}\\
\\
&=&A_xB_x (\mathbf{i}\times \mathbf{i})+A_xB_y(\mathbf{i}\times  \mathbf{j})+A_xB_y(\mathbf{i}\times \mathbf{k})\\
&+&A_yB_x(\mathbf{j}\times  \mathbf{i})+A_yB_y(\mathbf{j}\times  \mathbf{j})+A_yB_y(\mathbf{j}\times \mathbf{k})\\
&+&A_zB_x(\mathbf{k}\times  \mathbf{i})+A_zB_y(\mathbf{k}\times  \mathbf{j})+A_zB_y(\mathbf{k}\times \mathbf{k})\\
\\
&=&A_xB_x (\mathbf{0})+A_xB_y(\mathbf{k})+A_xB_y(-\mathbf{j})\\
&+&A_yB_x(-\mathbf{k})+A_yB_y(\mathbf{0})+A_yB_y(\mathbf{i})\\
&+&A_zB_x(-\mathbf{j})+A_zB_y(-\mathbf{i})+A_zB_y(\mathbf{0})\\
\\
&=& (A_y B_z - B_y A_z) \mathbf{i} + (A_z B_x - B_z A_x) \mathbf{j}+(A_x B_y - B_x A_y) \mathbf{k} \\
\end{eqnarray}
$$

**導出終わり**



### ベクトルの微分係数

　ベクトル$\mathbf{A}$が時間に依存して変化する場合，$\mathbf{A}$を時間の関数と考え, $\mathbf{A}(t)$と書く。時刻が$t$から$t+\Delta t$まで変化するのに応じて,  $\mathbf{A}$が$\mathbf{A}(t)$から$\mathbf{A}(t+\Delta t)$へと変化したとき，$\mathbf{A}(t)$の微分係数 (**導ベクトル**)と呼ばれる量$\frac{d\mathbf{A}}{dt}$を,
$$
\frac{d\mathbf{A}}{dt}:=\lim_{\Delta t \to 0}\frac{\mathbf{A}(t+\Delta t)-\mathbf{A}(t)}{\Delta t}
$$
と定義する。$\Delta \mathbf{A}:=\mathbf{A}(t+\Delta t)-\mathbf{A}(t)$と定義すると，上式は，
$$
\begin{eqnarray}
\frac{d\mathbf{A}}{dt}:=\lim_{\Delta t \to 0}\frac{\Delta\mathbf{A}}{\Delta t}
\end{eqnarray}
$$
と書くこともできる。



### 慣性系・慣性力

　空間内に固定された座標系，空間内で等速直線運動している座標系のことを**慣性系**と呼ぶ。慣性系でない座標系のことを非慣性系と呼ぶ。  

非慣性系においては，重力や電磁気力といった物体や空間の相互作用によって生じる力以外に，**座標系自体が動いている**ことによって生じる**慣性力**と呼ばれる力を考慮する必要がある。  

代表的な慣性力として，遠心力がある。遠心力は，座標系が回転運動している際，その座標系に対して物体が静止していても静止していなくても働く力である。一方，ここでの主題のコリオリ力は，回転している座標系において，<u>物体が相対的に運動している場合のみ</u>働くという違いがある。  



## 慣性系と回転系でのベクトルの相互の関係

　物体の位置の変化のことを変位という。物体の変位を表すベクトル$\Delta \mathbf{r}$を慣性系と回転系で見た場合の相互の関係を定式化しておく。

<img src="COR2_FIG01.png" alt="COR2_FIG01" style="zoom:67%;" />

$(\Delta \mathbf{r})_I$ := 慣性系で見た場合の変位  

$(\Delta \mathbf{r})_R$ := 回転系で見た場合の変位  

$(\Delta \mathbf{r})_{rot}$ := 座標系が回転していることによって生じる見かけの変位  

とする。ベクトルの和の定義に基づき，これらのベクトルについて，  
$$
\begin{eqnarray}
(\Delta \mathbf{r})_{I}=(\Delta \mathbf{r})_{R}+(\Delta \mathbf{r})_{rot}
\end{eqnarray}
$$
が成り立つ。  

　上記の関係は，単にベクトルの和の定義に基づくものであるから，さらに変位以外の 一般の ベクトル$\mathbf{B}$についても，その微小時間$\Delta t$経過後の変化量$\Delta \mathbf{B}$に関する相互の関係として,  
$$
\begin{eqnarray}
(\Delta \mathbf{B})_{I}=(\Delta \mathbf{B})_{R}+(\Delta \mathbf{B})_{rot}
\end{eqnarray}
 \tag{1}
$$
が成り立つ。  



## 回転系における速度の表現



### 一定の角速度で回転している座標系の定ベクトルを慣性系で表す

　$\mathbf{\Omega}$を回転軸として**回転している座標系を考える**。座標系と同じ角速度で回転するベクトル$\mathbf{C}$が，空間内に固定された座標系(慣性系)でどのように観察されるか考える。$\mathbf{C}$は回転系では定点を表すが，慣性系では動いて見えることに注意。  

<img src="COR2_FIG02.png" alt="COR2_FIG02" style="zoom: 50%;" />

　時間が$\Delta t$だけ経過した後の$\mathbf{C}$の変化を表すベクトル$\Delta\mathbf{C}$は, 線分O'Pに直交する単位ベクトル$\mathbf{m}$を使って,   
$$
\begin{eqnarray}
\Delta\mathbf{C}&=&(\left| \mathbf{C} \right|\sin\hat{\theta})(\left| \mathbf{\Omega}\right|\Delta t )\mathbf{m}\\
 &=& \left| \mathbf{C} \right|\left| \mathbf{\Omega}\right|\sin\hat{\theta}\mathbf{m}\Delta t\\
 &=& (\mathbf{\Omega}\times\mathbf{C})\Delta t
\end{eqnarray}
$$
と書ける。よって，<u>空間内に固定された座標系で観察した場合</u>の$\mathbf{C}$の単位時間当たりの変化を$\left( \frac{d\mathbf{C}}{dt} \right)_{rot}$と書くことにすると，
$$
\begin{eqnarray}
 \left( \frac{d\mathbf{C}}{dt} \right)_{rot}:=\lim_{\Delta t \to 0}\frac{\Delta\mathbf{C}}{\Delta t}=\mathbf{\Omega}\times\mathbf{C}
 \tag{2}
\end{eqnarray}
$$
となる。**この式がコリオリ力の定式化の基礎となる**。



### 回転系での速度ベクトルの表現

(2)で$\mathbf{C}=\mathbf{r}$とおくと，  
$$
\begin{eqnarray}
\left( \frac{d\mathbf{r}}{dt} \right)_{rot}=\mathbf{\Omega}\times\mathbf{r}
\end{eqnarray}
 \tag{3}
$$
$(\Delta \mathbf{r})_{I}=(\Delta \mathbf{r})_{R}+(\Delta \mathbf{r})_{rot}$より，  
$$
\begin{eqnarray}
\left( \frac{d\mathbf{r}}{dt} \right)_{I}=\left( \frac{d\mathbf{r}}{dt} \right)_{R}+\left( \frac{d\mathbf{r}}{dt} \right)_{rot}
\end{eqnarray}
$$
これに(3)を代入すると，
$$
\begin{eqnarray}
\left( \frac{d\mathbf{r}}{dt} \right)_{I}=\left( \frac{d\mathbf{r}}{dt} \right)_{R}+\mathbf{\Omega}\times\mathbf{r}
\end{eqnarray}
 \tag{4}
$$
慣性系，回転系それぞれから見た速度ベクトル$\mathbf{v}_I$, $\mathbf{v}_R$を，
$$
\begin{eqnarray}
\mathbf{v}_I:=\left( \frac{d\mathbf{r}}{dt} \right)_{I},\quad  \mathbf{v}_R:=\left( \frac{d\mathbf{r}}{dt} \right)_{R}
\end{eqnarray}
$$
と定義すると, (4)は,
$$
\begin{eqnarray}
\mathbf{v}_I=\mathbf{v}_{R}+\mathbf{\Omega}\times\mathbf{r}
\end{eqnarray}
$$
と表すことができる。

(1)式$(\Delta \mathbf{B})_{I}=(\Delta \mathbf{B})_{R}+(\Delta \mathbf{B})_{rot}$において, $\mathbf{B}=\mathbf{v}_{R}$とすると, 
$$
(\Delta \mathbf{v}_{R})_{I}=(\Delta \mathbf{v}_{R})_{R}+(\Delta \mathbf{v}_{R})_{rot}
$$
となる。これより, 
$$
\begin{eqnarray}
\left( \frac{d\mathbf{v}_R}{dt} \right)_{I}&=&\left( \frac{d\mathbf{v}_R}{dt} \right)_{R}+\left( \frac{d\mathbf{v}_R}{dt} \right)_{rot}\\
&=&\left( \frac{d\mathbf{v}_R}{dt} \right)_{R}+\mathbf{\Omega}\times\mathbf{v}_R

\end{eqnarray}
$$
上の式に，さらに$\mathbf{v}_I=\mathbf{v}_{R}+\mathbf{\Omega}\times\mathbf{r}$を代入すると,
$$
\begin{eqnarray}
\left( \frac{d (\mathbf{v}_I-\mathbf{\Omega}\times\mathbf{r})}{dt} \right)_{I}
=\left( \frac{d\mathbf{v}_R}{dt} \right)_{R}+\mathbf{\Omega}\times\mathbf{v}_R

\end{eqnarray}
$$
ここで, 
$$
\begin{eqnarray}
\left( \frac{d }{dt}(\mathbf{\Omega}\times\mathbf{r}) \right)_{I}
&=&\mathbf{\Omega}\times\left( \frac{d\mathbf{r}}{dt} \right)_{I} \\
&=&\mathbf{\Omega}\times\mathbf{v}_I \\
&=&\mathbf{\Omega}\times(\mathbf{v}_{R}+\mathbf{\Omega}\times\mathbf{r}) \\
\end{eqnarray}
$$
であるから,
$$
\begin{eqnarray}
\left( \frac{d\mathbf{v}_I}{dt} \right)_{I}&=&\left( \frac{d\mathbf{v}_R}{dt} \right)_{R}+2\mathbf{\Omega}\times\mathbf{v}_{R}+\mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r})
\end{eqnarray}
$$
上式が慣性系における加速度$\left(d\mathbf{v}_I/dt \right)_{I}$と$\left(d\mathbf{v}_R/dt \right)_{R}$の関係を結ぶ式である。



## 回転系における運動方程式

いま，単位質量の物体の運動方程式を考える。物体に働く重力などの単位質量あたりの力を$\mathbf{F}$とすると，<u>慣性系における</u>運動方程式は，
$$
\begin{eqnarray}
\left( \frac{d\mathbf{v}_I}{dt} \right)_{I}&=&\mathbf{F}
\end{eqnarray}
$$
となる。左辺の加速度$\left(d\mathbf{v}_I/dt \right)_{I}$を, 回転系の$\left(d\mathbf{v}_R/dt \right)_{R}$を使って表すことにすると，<u>回転系における</u>運動方程式は, 
$$
\begin{eqnarray}
\left( \frac{d\mathbf{v}_R}{dt}\right)_R = \mathbf{F}-2\mathbf{\Omega}\times\mathbf{v}_{R}-\mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r})
\end{eqnarray}
$$
となる。ここで,

-  $-2\mathbf{\Omega}\times\mathbf{v}_{R}$をコリオリ力 

-  $-\mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r})$を遠心力

と呼ぶ (どちらも単位質量あたり)。  



## コリオリ力の性質

 <img src="COR2_FIG03.png" alt="COR2_FIG03" style="zoom: 50%;" />



コリオリ力 = $-2\mathbf{\Omega}\times\mathbf{v}$であるから($\mathbf{v}$の添え字$_R$は省略した)，下記のことが分かる

- コリオリ力は$\mathbf{\Omega}$と$\mathbf{v}$の両方に直交する。  

- コリオリ力は，回転軸に直交する平面上に働く  

- $\mathbf{v}$を$\mathbf{\Omega}$の方向に回したとき，右ねじの進む方向にコリオリ力は働く。  

- コリオリ力の大きさは，$\mathbf{\Omega}$と$\mathbf{v}$の大きさに比例する  

<img src="COR2_FIG04.png" alt="COR2_FIG04" style="zoom: 40%;" />

  

## コリオリ力の緯度依存性

<img src="COR2_FIG05.png" alt="COR2_FIG05" style="zoom:50%;" />

緯度$\theta$の点における接平面において，東向きの座標軸を$x$軸, 北向きの座標軸を$y$軸, 接平面に直交し鉛直上向きの座標軸を$z$軸と定義する。このとき,  
$$
\begin{eqnarray}
\mathbf{\Omega}&=&({\Omega}_x, {\Omega}_y, {\Omega}_z)
&=&(0, {\Omega}\cos\theta, {\Omega}\sin\theta)\\
\end{eqnarray}
$$
である。いま，  
$$
\begin{eqnarray}
\left|\mathbf{\Omega}\right|&:=&\Omega
\end{eqnarray}
$$
と定義すると，  
$$
\begin{eqnarray}
&&\mathbf{\Omega}\times\mathbf{v}\\
&=&(0, \Omega\cos\theta, \Omega\sin\theta)\\
&\times&(u, v, w)\\
&=&(\Omega\cos\theta w-\Omega\sin\theta v, \Omega\sin\theta u, -\Omega\cos\theta u)\\

\end{eqnarray}
$$
$f := 2 \Omega \sin \theta$, $f^* := 2 \Omega \sin \theta$と定義すると，コリオリ力$-2\mathbf{\Omega}\times\mathbf{v}$（$\mathbf{v}$の添え字$_R$は省略）の成分表示は，  
$$
\begin{eqnarray}
&&-2\mathbf{\Omega}\times\mathbf{v}\\

&=&(f v- f^*w,\quad -fu,\quad f^*u)\\

\end{eqnarray}
$$
となる。$w\ll u, v$のとき,   
$$
\begin{eqnarray}
&&-2\mathbf{\Omega}\times\mathbf{v}\\

&\simeq&(f v,\quad -fu,\quad f^*u)\\

\end{eqnarray}
$$
となる。$z$方向成分, $f^*u$ は重力に比べてずっと小さくなることが多いので(重力の1万分の1程度), 通常は無視される。  



## 遠心力

<img src="COR2_FIG06.png" alt="COR2_FIG06" style="zoom:50%;" />

単位質量の物体に働く遠心力 = $-\mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r})$ = $\mathbf{\Omega}\times(-\mathbf{\Omega}\times\mathbf{r})$

$-\mathbf{\Omega}\times\mathbf{r}$は紙面から手前側を向く  

$\mathbf{\Omega}\times(-\mathbf{\Omega}\times\mathbf{r})$は回転軸から見て外側を向く  
$$
\begin{eqnarray}
\left| \mathbf{\Omega}\times\mathbf{r} \right| 
= \left| \mathbf{\Omega}\right|\left|\mathbf{r}\right|\sin\hat{\theta}
= \left| \mathbf{\Omega}\right|r_{\perp}
\end{eqnarray}
$$

$$
\begin{eqnarray}
\left| \mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r}) \right| 
= \left| \mathbf{\Omega}\right| (\left| \mathbf{\Omega}\right|r_{\perp}\sin (\pi/2))
= \Omega^2r_{\perp}
\end{eqnarray}
$$

よって，(単位質量あたりの)遠心力の大きさ = $\Omega^2r_{\perp}$であり，遠心力の大きさは, 回転軸からの距離$r_{\perp}$と回転角速度の大きさ$\Omega$のみに依存する。物体の速度は遠心力に無関係。  



## 有効重力

回転角速度が不変であるとすると，遠心力は物体の位置だけで決まるので，遠心力は重力に含めて考えることがよくある。地球と物体に働く万有引力(重力)と遠心力の和を有効重力と呼ぶ。有効重力$\mathbf{g}^*$は，  
$$
\mathbf{g}^* := \mathbf{g}-\mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r})
$$
で定義される。  

$\left| \mathbf{\Omega}\times(\mathbf{\Omega}\times\mathbf{r}) \right| 
=  \Omega^2r_{\perp}$で，$\Omega\sim10^{-5}$ [$\mathrm{s^{-1}}$], $r_{\perp}\sim6\times10^{3}$ [$\mathrm{m}$], $\left| \mathbf{g} \right|\sim10$ [$\mathrm{m s^{-2}}$]であるから，遠心力は重力の1万分の1以下の大きさであるため，遠心力はしばしば無視される。  
# 絶対渦度保存則

絶対渦度保存則とは、大気の大規模な運動を記述するための最も基本的な法則である。

以下、数式展開が煩雑とならないようにいくつかの近似を導入するが、そのことによって問題の本質が損なわれることはない。

## ベータ面近似

ベータ面近似とはコリオリ係数の緯度変化の近似表現である。$a$を地球半径として, コリオリ係数, $f$を緯度$\varphi$でテーラー展開すると,
$$
\begin{eqnarray}
f & =       & 2 \Omega \sin \varphi \\
  & \approx & 2 \Omega \sin \varphi_0 + \frac{d f}{d \varphi}|_{\varphi_0}\Delta \varphi \\
  & =       & 2 \Omega \sin \varphi_0 + \frac{2 \Omega \cos \varphi_0}{a}y \qquad      (y=a\Delta\varphi)\\
  & =       & f_0 + \beta y .
\end{eqnarray}
$$

$$
\begin{eqnarray}
f_0 & =& 2 \Omega \sin \varphi_0 \\
    &\approx & 10^{-4} \, s^{-1} \qquad (\varphi \approx 40 ^oN).
\end{eqnarray}
$$

$$
\begin{eqnarray}
\beta & \equiv & \frac{2 \Omega \cos \varphi_0}{a} \\
      & =      & \frac{2 \frac{2 \pi}{1 \,[day]}\cos \varphi_0}{a}  \\
      & \sim   & \frac{2 \frac{6}{10^{5}}\frac{1}{2}}{6000\times1000}
      \qquad   (\cos \varphi_0 =1/2 \quad at \quad\varphi = 60 ^oN) \\
      & \sim   & 10^{-11}\,[m^{-1}\,s^{-1}].
\end{eqnarray}
$$
球面の場合コリオリ係数は正弦関数($\sin \varphi$)的に変化するが、ベータ面近似ではコリオリ数の緯度変化率を一定とみなす。

## ベータ面近似のもとでの運動方程式
$x$, $y$をそれぞれ東西方向, 南北方向の座標とする. $x$, $y$はそれぞれ, 東向き, 北向き, を正とする。気塊の速度$\vec v$の$x$, $y$方向成分をそれぞれ$u$, $v$ 緯度$\varphi$におけるコリオリ係数$f$を上述のように$f_0 + \beta y$と近似し, 圧力を$p(x,y,z,t)$と表すことにすると, 単位質量の空気の塊(気塊)に関する運動方程式の$x, y$成分は次のようになる.  
$$
\begin{eqnarray}
\frac{d u}{d t} & = & (f_0 + \beta y)v　-\frac{1}{\rho} \frac{\partial p}{\partial x}\\
\end {eqnarray} \tag{1}
$$

$$
\begin{eqnarray}
\frac{d v}{d t} & = & -(f_0 + \beta y)u -\frac{1}{\rho} \frac{\partial p}{\partial y}\\
\end {eqnarray} \tag{2}
$$

## 絶対渦度保存則
$$
\frac{\partial (2)}{\partial x}-\frac{\partial (1)}{\partial y}
$$
という演算を行うと,
$$
\begin{eqnarray}
\frac{d}{dt}\biggl(\frac{\partial v}{\partial x}-\frac{\partial u}{\partial y} \biggr) = -f\biggl( \frac{\partial u}{\partial x}+\frac{\partial v}{\partial y} \biggr) - \beta v
\end {eqnarray} \tag{3}
$$
風はほぼ地衡風平衡にあるとし,
$$
\begin{eqnarray}
u \approx u_g & = & - \frac{1}{f \rho} \frac{\partial p}{\partial y} 
\end {eqnarray} \tag{4}
$$

$$
\begin{eqnarray}
v \approx v_g & = & \frac{1}{f \rho} \frac{\partial p}{\partial x} 
\end {eqnarray} \tag{5}
$$
と近似すると,
$$
\frac{\partial u}{\partial x}+\frac{\partial v}{\partial y} \approx 0
$$
となるので、$(3)$の右辺第一項を無視すると$(3)$は,
$$
\begin{eqnarray}
\frac{d}{dt}\biggl(\frac{\partial v_g}{\partial x}-\frac{\partial u_g}{\partial y} \biggr) + \beta v = 0
\end {eqnarray} \tag{6}
$$
となる。また, 相対渦度$\zeta$を
$$
\zeta \equiv \frac{\partial v_g}{\partial x}-\frac{\partial u_g}{\partial y}
$$
で定義し, $v=dy/dt$を使うと, $(6)$は,
$$
\begin{eqnarray}
\frac{d}{dt}\biggl(\zeta + \beta y \biggr) = 0
\end {eqnarray} \tag{7}
$$
となる。$(6)$, $(7)$を絶対渦度保存則と呼ぶ。$(7)$の$\beta y$は地球が自転していることによって生じる項で、惑星渦度と呼ばれることがある。



## 相対渦度の意味

直感的な表現を使うと, $\zeta \sim $ 流れのなかに置いた羽根車の回る強さ。$\zeta > 0$のとき羽根車は反時計回りに回り, $\zeta < 0$のとき時計回りに回る。

- $\zeta > 0$のとき気塊は反時計回りに回る傾向が強くなる
- $\zeta <  0$のとき気塊は時計回りに回る傾向が強くなる



## 絶対渦度保存則の意味

$(7)$より気塊が大気中を動くとき, 絶対渦度, $\zeta + \beta y$は一定値に保たれることが分かる。したがって,

- 気塊が北上して惑星渦度が増えると、相対渦度が減少する
- 気塊が南下して惑星渦度が減ると、相対渦度が増加する


# ロスビー波

ロスビー波とはコリオリ力が緯度によって異なることによって生じる波である。

以下、数式展開が煩雑とならないようにいくつかの近似を導入するが、そのことによって問題の本質が損なわれることはない。

## 予備知識

### 絶対渦度保存則

$\varphi$を緯度, $\Omega$を地球の回転角速度, $a$を地球の半径とし, $f_0   =  2 \Omega \sin \varphi_0$, $\beta  =  \frac{2 \Omega \cos \varphi_0}{a}$とする。いま、密度は一定とし, $y=a\Delta y$, 相対渦度 $\zeta$を $ \zeta \equiv \partial v_g / \partial x -  \partial u_g / \partial y$と定義すると, 流れがほぼ地衡風平衡しているとみなせる場合, 以下の絶対渦度保存則, $(1)$, $(2)$が成り立つ。
$$
\begin{eqnarray}
\frac{d}{dt}\biggl(\frac{\partial v_g}{\partial x}-\frac{\partial u_g}{\partial y} \biggr) + \beta v = 0
\end {eqnarray} \tag{1}
$$

$$
\begin{eqnarray}
\frac{d}{dt}\biggl(\zeta + \beta y \biggr) = 0
\end {eqnarray} \tag{2}
$$
$(2)$の$\beta y$は地球が回転していることによって生じる項で、惑星渦度と呼ばれることがある。

### 相対渦度の意味

直感的な表現を使うと, $\zeta \sim $ 流れのなかに置いた羽根車の回る強さ。$\zeta > 0$のとき羽根車は反時計回りに回り, $\zeta < 0$のとき時計回りに回る。

- $\zeta > 0$のとき気塊は反時計回りに回る傾向が強くなる
- $\zeta <  0$のとき気塊は時計回りに回る傾向が強くなる

### 絶対渦度保存則の意味
$(2)$より気塊が大気中を動くとき, 絶対渦度, $\zeta + \beta y$は一定値に保たれることが分かる。したがって,

- 気塊が北上して惑星渦度が増えると、相対渦度が減少する

- 気塊が南下して惑星渦度が減ると、相対渦度が増加する

### 波の式
$x, y$方向の波長がそれぞれ$L_x$, $L_y$, 周期が$T$であり, 波形が三角関数で表される波は, 
$$
\psi = A \cos \bigg( \frac{2 \pi}{L_x}x + \frac{2 \pi}{L_y}y - \frac{2 \pi}{T}t \bigg)
$$
と書ける。括弧の中に現れた,
$$
\theta = \frac{2 \pi}{L_x}x + \frac{2 \pi}{L_y}y - \frac{2 \pi}{T}t
$$
という量を波の位相と呼ぶ。今簡単のため, $L_x=L$, $L_y=0$とおき, $\theta = 0$のときの$\psi$を$\psi_0$とする。このとき,

$$
\begin{eqnarray}
0 &=& \frac{2 \pi}{L}x - \frac{2 \pi}{T}t \\
\frac{x}{t} &=& \frac{L}{t} \\
\end{eqnarray}
$$
となるが、最後の等式は、$\psi=$$\psi_0$の値が, $x$方向に$L/T$の速さで進んでいくことを意味する。
$$
\begin{eqnarray}
c_x & \equiv & L_x/T　\\
c_y & \equiv & L_y/T
\end{eqnarray}
$$
のことを波の位相速度という。

## ロスビー波の直感的説明
東西方向に一直線上に並んだ気塊が何かの拍子で南北に波打ったとする。元の位置より南側に変位した気塊は惑星渦度が減少するので、相対渦度が増加する。このことから南側に変位した気塊の周りには反時計周りの運動が励起される。逆に元の位置より北側に変位した気塊は惑星渦度が増加するので、相対渦度が減少する。このことから北側に変位した気塊の周りには時計周りの運動が励起される。これら気塊の周りに励起された運動によって元の波うったパターンは西向きに伝わるようになる。

## 絶対渦度保存則を使った解析
式の簡略化のため、流線関数$\psi$(プサイ)を,
$$
\psi \equiv \frac{p}{f_0 \rho}
$$
で定義する。このとき, $u_g = - \partial \psi / \partial y$, $v_g = \partial \psi / \partial x$である。これを$(1)$に代入すると,

$$
\begin{eqnarray}
\frac{d}{dt}\biggl[ \frac{\partial}{\partial x} \bigg( \frac{\partial \psi} {\partial x} \bigg) -\frac{\partial}{\partial y} \bigg(\frac{\partial \psi}{\partial y} \bigg) \biggr] + \beta \frac{\partial \psi}{\partial x} = 0
\end {eqnarray} \tag{3}
$$

いま$(3)$の解として, $x, y$方向の波長が$L_x$, $L_y$, 周期が$T$である波の式
$$
\begin{eqnarray}
\psi = A \cos \bigg( \frac{2 \pi}{L_x}x + \frac{2 \pi}{L_y}y - \frac{2 \pi}{T}t \bigg)
\end{eqnarray} \tag{4}
$$
を考える。ここで周波数$\omega$, $x, y$方向の波数$k, l$と呼ばれる量を, $\omega \equiv 2 \pi / T$, $k \equiv 2 \pi / L_x$, $l \equiv 2 \pi / L_y$と定義すると, $(4)$は,
$$
\begin{eqnarray}
\psi = A \cos ( k x + l y - \omega t)
\end{eqnarray} \tag{5}
$$

となる。ここで$(3)$に下記を代入して整理する。

$$
\begin{eqnarray}
\frac{\partial \psi}{\partial x} & = & - Ak \frac{\partial}{\partial x}\sin (kx+ly-\omega t) \tag{6}\\
\frac{\partial \psi}{\partial y} & = & - Al \frac{\partial}{\partial x}\sin (kx+ly-\omega t) \\
\end{eqnarray}
$$

$$
\begin{eqnarray}
\frac{\partial}{\partial x}\bigg(\frac{\partial \psi}{\partial x} \bigg) & = & - Ak^2 \frac{\partial}{\partial x}\cos (kx+ly-\omega t) \\

\frac{\partial}{\partial y}\bigg(\frac{\partial \psi}{\partial y} \bigg) & = & - Al^2 \frac{\partial}{\partial y}\cos (kx+ly-\omega t) \\
\end{eqnarray}
$$

$$
\begin{eqnarray}
\frac{d}{dt} \bigg[ \frac{\partial}{\partial x}\bigg(\frac{\partial \psi}{\partial x} \bigg) \bigg] & = & Ak^2 \omega \frac{\partial}{\partial x}\sin (kx+ly-\omega t) \\

\frac{d}{dt} \bigg[ \frac{\partial}{\partial y}\bigg(\frac{\partial \psi}{\partial y} \bigg)  \bigg] & = & Al^2 \omega \frac{\partial}{\partial y}\sin (kx+ly-\omega t) \\
\tag{7}
\end{eqnarray}
$$

その結果、
$$
\begin{eqnarray}
\frac{\omega}{k} = \frac{L}{T} = -\frac{\beta}{k^2+l^2}
\tag{8}
\end{eqnarray}
$$
が得られる。$\beta > 0$なので、この波の進む速度$L/T$は常に負となる。すなわち、この波のパターンは常に西向きに伝わる。この西向きに進む波のことをロスビー波といい, $(8)$をロスビー波の分散関係 (dispersion relation)と呼ぶ。

西向きにすすむロスビー波の速さが、東向きの風速の大きさと等しいとき、波は止まって見える (定在波)。定在波の生じる波の波長を見積もってみる。いま、簡単のため$L_x=L_y=L$とし, $u = 10 \, m s^{-1}$, $\beta \sim 10^{-11} \, m^{-1} s^{-1}$ (おおよそ緯度60$^o$における値)とすると, 
$$
10 \, m s^{-1} = \frac{10^{-11} \, [m^{-1}s^{-1}]}{\big(\frac{2 \pi}{L \, [m]}\big)^2 \times2}
$$
より, $L \sim 10^4 \, [km]$となり、この条件下では、おおよそ波長$L$が$1$万$\mathrm{km}$ぐらいの波が止まって見える。このような定在波は比較的長期間持続するので, 中緯度の日々の天気の変化よりも長い時間スケールの大気の変動を考える上で重要である。極端に振幅が大きい定在波が長期間存在すると、平年と異なる大気状態が長期間持続することとなり、これが異常気象の要因となることがよくある。

## ロスビー波の分散性

$(8)$を
$$
\begin{eqnarray}
\frac{L}{T} = -\frac{\beta}{\big( \frac{2 \pi}{L_x} \big)^2+\big( \frac{2 \pi}{L_y} \big)^2}
\tag{9}
\end{eqnarray}
$$
と書き換えると, 波長$L_x, L_y$が大きい波ほど、$L/T$が大きくなる、すなわち、波が速く進むことがわかる（位相速度が大きい）。位相速度が波長に依存する波のことを、分散性の波という。

## 波束と波の群速度
ロスビー波は分散性の波なので、ほっておいてもいくつかの波長の波に分かれていく。ここでは、簡単のため、ごく近い波長をもつ2つの波が重ね合わさったとき、どのようなふるまいをするかについて調べる。いま、簡単のため$y$方向の波数$k$を$0$とする。次の2つの波, 
$$
\begin{eqnarray}
\psi_1 & = & A \cos ( (k+\Delta k) x - (\omega + \Delta \omega)t )\\
\psi_2 & = & A \cos ( (k-\Delta k) x - (\omega - \Delta \omega)t )\\
\end{eqnarray}
$$
の重ね合わせ,
$$
\begin{eqnarray}
\psi & = & \psi_1 + \psi_2 \\
     & = & A \cos ( (k+\Delta k) x - (\omega + \Delta \omega)t ) \\
     & + & A \cos ( (k-\Delta k) x - (\omega - \Delta \omega)t ) \\
     & = & A \cos ( (kx-\omega t) + ( (\Delta k x - \Delta \omega t)) \\
     & + & A \cos ( (kx-\omega t) - ( (\Delta k x - \Delta \omega t)) \\
     & = & 2A\cos(kx-\omega t)\cos(\Delta k x - \Delta \omega t)
\tag{10} \\
\end{eqnarray}
$$

$(10)$の$\psi$は下図で水色で表した早く変化する波$\cos(kx-\omega t)$と、橙と緑色で表したゆっくり変化する波$\cos(\Delta k x - \Delta \omega t)$の掛け算と考えることができる。

![output_2_1](output_5_1.png)

$\cos(\Delta k x - \Delta \omega t)$の波が進む速さ($\Delta \omega/\Delta k$)のことを群速度とよぶ 。より厳密には群速度, $c_g$は
$$
c_g = \frac{d \omega}{d k}
$$

で定義される。重ね合わせた波$\psi$の振幅が大きくなっているところを波束とよぶことがある。位相速度$c_x=\omega/k=0$でも, $c_g \neq 0$のとき、分散性の波の場合、波束が伝播することがある。これは定在ロスビー波の振幅が徐々に大きくなっていくような現象を考える場合に重要になる。






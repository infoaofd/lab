

```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60
k0=2*np.pi/L
y=0
x = np.linspace(0, 360, 360)
y=y + np.cos(k0*x)
plt.xlabel('longitude [deg]', fontsize = 16)
plt.plot(x, y)
```




    [<matplotlib.lines.Line2D at 0x1d3b85a5550>]




![png](output_0_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
y=0

x = np.linspace(0, 360, 360)

omega=2*np.pi/T
t=0
k=-2*np.pi/(L)
y=y + np.cos(k*x-omega*t)

plt.xlabel('longitude [deg]', fontsize = 16)
plt.plot(x, y)

t=1
y=0
k=-2*np.pi/(L)
y=y + np.cos(k*x-omega*t)
plt.plot(x, y)

```




    [<matplotlib.lines.Line2D at 0x1d3b99bdac8>]




![png](output_1_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T

y=0

x = np.linspace(0, 360, 360)

t=0
k=-2*np.pi/(L-4)
y=y + np.cos(k*x-omega*t)

k=-2*np.pi/(L+4)
y=y + np.cos(k*x-omega*t)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.plot(x, y)

t=1
y=0
k=-2*np.pi/(L-4)
y=y + np.cos(k*x-omega*t)

k=-2*np.pi/(L+4)
y=y + np.cos(k*x-omega*t)

plt.plot(x, y)
```




    [<matplotlib.lines.Line2D at 0x1d3b9bd8278>]




![png](output_2_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T

y=0

x = np.linspace(0, 360, 360)

t=0
k=-2*np.pi/(L-4)
y=y + np.cos(k*x-omega*t)

k=-2*np.pi/(L+4)
y=y + np.cos(k*x-omega*t)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.plot(x, y)

t=1
y=0
k=-2*np.pi/(L-4)
y=y + np.cos(k*x-omega*t)

k=-2*np.pi/(L+4)
y=y + np.cos(k*x-omega*t)

plt.plot(x, y)

t=2
y=0
k=-2*np.pi/(L-4)
y=y + np.cos(k*x-omega*t)

k=-2*np.pi/(L+4)
y=y + np.cos(k*x-omega*t)

plt.plot(x, y)

```




    [<matplotlib.lines.Line2D at 0x1d3b9c41a58>]




![png](output_3_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T

k=-2*np.pi/(L)
cp=U/L

U=cp
omega=k*(U-cp)

x = np.linspace(0, 360, 360)

t=0
y=0
y=y + np.cos(k*x-omega*t)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.plot(x, y)

t=1
y=0
y=y + np.cos(k*x-omega)

plt.plot(x, y)

```




    [<matplotlib.lines.Line2D at 0x1d3bb170d30>]




![png](output_4_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T
x = np.linspace(0, 360, 360)

plt.xlim(0, 360)
plt.ylim(-3, 3)
plt.grid()


t=0
y=0
k=-2*np.pi/L
delk=k/20
delo=omega/20
y=2*np.cos(k*x)*np.cos(delk*x+delo*t)
plt.plot(x, y, label="t=0")

y2=2*np.cos(delk*x+delo*t)
plt.plot(x, y2)

y3=-2*np.cos(delk*x+delo*t)
plt.plot(x, y3)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.legend()
```




    <matplotlib.legend.Legend at 0x1d3bfaff518>




![png](output_5_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T
x = np.linspace(0, 360, 360)

plt.xlim(0, 360)
plt.ylim(-3, 3)
plt.grid()


t=2
y=0
k=-2*np.pi/L
delk=k/20
delo=omega/20
y=2*np.cos(k*x)*np.cos(delk*x+delo*t)
plt.plot(x, y, label="t=2")

y2=2*np.cos(delk*x+delo*t)
plt.plot(x, y2)

y3=-2*np.cos(delk*x+delo*t)
plt.plot(x, y3)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.legend()
```




    <matplotlib.legend.Legend at 0x1d3c27858d0>




![png](output_6_1.png)



```python
%matplotlib inline
import matplotlib.pyplot as plt
import numpy as np
d2r=np.pi/180.
L=60 # degree
T=6  # day
omega=2*np.pi/T
x = np.linspace(0, 360, 360)

plt.xlim(0, 360)
plt.ylim(-3, 3)
plt.grid()


t=4
y=0
k=-2*np.pi/L
delk=k/20
delo=omega/20
y=2*np.cos(k*x)*np.cos(delk*x+delo*t)
plt.plot(x, y, label="t=4")

y2=2*np.cos(delk*x+delo*t)
plt.plot(x, y2)

y3=-2*np.cos(delk*x+delo*t)
plt.plot(x, y3)

plt.xlabel('longitude [deg]', fontsize = 16)

plt.legend()
```




    <matplotlib.legend.Legend at 0x1d3c27f45f8>




![png](output_7_1.png)



```python

```

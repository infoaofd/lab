# コリオリ力 その1

コリオリ力には幾通りもの説明方法がありますが，その1では一部の式の導出に予備知識が少なくて済む幾何学的な方法を採用しています。その2では代数的な手法を使って説明します。代数的な方法は式の変形だけで導出が完了するので，人によってはしっくりくるかもしれません。その1と2を一通り眺めてみて，とっつきやすい方から読んでみてください。  

[TOC]

## 0. 記号

- A := Bは「AをBと定義する」という意味で用いられる

- 物理量の後の[]はその物理量の単位を意味する。T [s]は, Tの単位がsという意味

- [s]は秒, [m]はメートル, [kg]はキログラムを意味する

- 太字はベクトルを表す。例えば, $\mathbf{A}$はベクトル$A$の意味

- 単位ベクトル := 長さ1のベクトル

- $\mathbf{i}$, $\mathbf{j}$, $\mathbf{k}$はそれぞれ$x$, $y$, $z$軸方向の単位ベクトルを表す。

- 従って，ベクトル$\mathbf{A}$の$x$, $y$, $z$方向成分をそれぞれ, $A_x$, $A_y$, $A_z$とするとき,

$$
\begin{eqnarray}
\mathbf{A} &=& A_x  \mathbf{i}  + A_y  \mathbf{j} + A_z  \mathbf{k} \\
 &=& (A_x, A_y, A_z) \\
\end{eqnarray}
$$

## 1. 予備知識

### 1.1 加速度

時間$t$の変化に伴って物体の速度$v$が変化したとき、$v$の単位時間当たりの変化（時間変化率）を加速度と定義する。加速度を$a$と表すことにすると, 単位時間として1 [s]を取った場合には，  

**加速度 = 1秒当たりの速度の変化 ** 

である。  

微分係数を使うと，$a$は  
$$
a :=\frac{dv}{dt}
$$
と定義できる。  



### 1.2 2次元空間の速度と加速度

座標系$(x, y)$で表される水平2次元空間における物体の位置$\mathbf{r}$を、$x$, $y$方向の単位ベクトル$\mathbf{i} $, $\mathbf{j}$を使って、$\mathbf{r}=x \mathbf{i} + y \mathbf{j}$ で表す。このとき、速度ベクトル$\mathbf{v}$を、  
$$
\begin{eqnarray}
\mathbf{v} & = & \frac{d \mathbf{r}}{d t}　\\
& = & \frac{d x}{d t}\mathbf{i} + \frac{d y}{d t}\mathbf{j} \\
\end {eqnarray}
$$
と定義する。$u=dx/dt$, $v=dy/dt$と表すことにすると  
$$
\begin{eqnarray}
\mathbf{v} & = & \frac{d \mathbf{r}}{d t}　\\
& = & u\mathbf{i} + v\mathbf{j} \\
& = & \left( u,v \right)\\
\end {eqnarray}
$$
と書くこともできる。同様に加速度ベクトル$\mathbf{a}$を、  
$$
\begin{eqnarray}
\mathbf{a} & = & \frac{d \mathbf{v}}{d t}　\\
& = & \frac{d u}{d t}\mathbf{i} + \frac{d v}{d t}\mathbf{j} \\
& = & \left( \frac{d u}{d t},  \frac{d v}{d t} \right)\\
\end {eqnarray}
$$
と定義する。2階微分を使うと、  
$$
\begin{eqnarray}
\frac{du}{dt} & = & \frac{d}{dt}\left( \frac{dx}{dt} \right) & = & \frac{d^2x}{dt^2} \\
\frac{dv}{dt} & = & \frac{d}{dt}\left( \frac{dy}{dt} \right) & = & \frac{d^2y}{dt^2}
\end{eqnarray}
$$
と表すこともできるので、  
$$
\begin{eqnarray}
\mathbf{a} & = & \frac{d^2 \mathbf{r}}{d t^2}　\\
& = & \frac{d^2 x}{d t^2}\mathbf{i} + \frac{d^2 y}{d t^2}\mathbf{j} \\
& = & \left( \frac{d^2 x}{d t^2},  \frac{d^2 y}{d t^2} \right)\\
\end {eqnarray}
$$
とも書ける。  



### 1.3 運動方程式

質量$m$の物体に、力$\mathbf{F}=(F_x,F_y)$が働くとき、物体の速度$\mathbf{v}=(u,v)$は、下記のように変化する。
$$
m\frac{d \mathbf{v}}{d t}  =  \mathbf{F}
$$
これを運動方程式（ニュートンの運動の第2法則）という。位置ベクトル$\mathbf{r}$を使うと、運動方程式は
$$
m\frac{d^2 \mathbf{r}}{d t^2}  =  \mathbf{F}
$$
と書くこともできる。さらにベクトルの成分表示を使うと運動方程式は、
$$
\begin{eqnarray}
m \frac{d u}{d t} & = & F_x　\\
m \frac{d v}{d t} & = & F_y　\\
\end {eqnarray}
$$
もしくは、
$$
\begin{eqnarray}
m \frac{d^2 x}{d t^2} & = & F_x　\\
m \frac{d^2 y}{d t^2} & = & F_y　\\
\end {eqnarray}
$$
とも書ける。



### 1.4 慣性力
電車が急に止まると、だれが押したわけでもわけでもないのに力が働いたように感じる。また、車が急カーブを曲がると誰が押したわけでもないのに体が押されるように感じる。これは移動している座標系の速度が変化したことによって現れる力 で、慣性力と呼ばれる。  

運動方程式は、**座標系自体が運動していてその速度が変化する場合にはそもそも使えない**（運動の第一法則）。しかし、慣性力を導入すると移動している座標系の速度が変化する場合にも運動方程式が使えるようになる。いま、座標系$(x,y)$, $(x',y')$の2つの座標系を考え、座標系$(x,y)$は空間に対し静止しているのに対し、座標系$(x',y')$は$(x,y)$に対し、ある加速度で運動しているとする。座標系$(x,y)$から見た、座標系$(x',y')$の原点$O'$の座標を$(x_0,y_0)$とすると、  
$$
\begin{eqnarray}
x & = & x_0 + x'　\\
y & = & y_0 + y'　\\

\end {eqnarray}
$$
となる。これより、
$$
\begin{eqnarray}
\frac{d^2 x}{d t^2} & = & \frac{d^2 x_0}{d t^2} + \frac{d^2 x'}{d t^2}　\\
\frac{d^2 y}{d t^2} & = & \frac{d^2 y_0}{d t^2} + \frac{d^2 y'}{d t^2} \\
\end {eqnarray}
$$
である。質量$m$の物体に力$\mathbf{F}=(F_x,F_y)$が働くとき、運動方程式は座標系$(x,y)$では、
$$
\begin{eqnarray}
m \frac{d^2 x}{d t^2} & = & F_x　\\
m \frac{d^2 y}{d t^2} & = & F_y　\\
\end {eqnarray}
$$
と書ける。上の式に、
$$
\begin{eqnarray}
\frac{d^2 x}{d t^2} & = & \frac{d^2 x_0}{d t^2} + \frac{d^2 x'}{d t^2}　\\
\frac{d^2 y}{d t^2} & = & \frac{d^2 y_0}{d t^2} + \frac{d^2 y'}{d t^2} \\
\end {eqnarray}
$$
を代入すると、座標系$(x',y')$における運動方程式は、
$$
\begin{eqnarray}
m \frac{d^2 x'}{d t^2} & = & F_x - m \frac{d^2 x_0}{d t^2}　 \\
m \frac{d^2 y'}{d t^2} & = & F_y - m \frac{d^2 y_0}{d t^2} 　\\
\end {eqnarray}
$$
と書くことができる。上の式の$(- m \frac{d^2 x_0}{d t^2}, - m\frac{d^2 y_0}{d t^2} )$が、座標系$(x,y)$に対し、加速度$(\frac{d^2 x_0}{d t^2},\frac{d^2 y_0}{d t^2})$で運動している座標系$(x',y')$で物体の運動を観察した場合に現れる見かけの力（慣性力）である。



### 1.5 角度と弧の長さの関係

円弧の角度を$\theta $ [ラジアン], 半径を$r$としたとき、円弧の長さ$l$は、		
$$
l=r \theta
$$
で与えられる。



### 1.6 角速度

物体の位置を極座標$(r, \theta)$で表す。時間$t$の変化に伴って角度$\theta$が変化したとき、$\theta$の単位時間当たりの時間変化率を角速度と定義する。角速度を$\omega$で表すことにすると、
$$
\omega = \frac{d \theta}{dt}
$$
となる。



## 2. コリオリ力の大きさは「２×速度×回転角速度」

回転角速度$\omega$で反時計回りに回転している座標系上で、物体速度$v$で動径方向に等速直線運動している単位質量の物体を考える（図1）。

<img src="Coriolis_FIG01SMALL.jpg" alt="Coriolis_FIG01SMALL" style="zoom:67%;" />

​																		図1. 等速円運動する物体

この物体は時間$t$の間に$vt$だけ進み, その間に座標系は角度$\omega t$だけ回転している。したがって, 運動している物体は当初予定より$x=v \omega  t^2$だけ右にずれた位置にあるように見える。物体の 向きが変わっているので、物体には加速度が働いている。このとき，加速度の大きさ$d^2x /dt^2$を計算すると,
$$
\begin{equation}
\frac{d^2x}{dt^2}=2 \omega v
\end{equation}
$$
となる。

いま力を$F$とすると、ニュートンの運動方程式は$d^2x /dt^2 = F$と書くことができる。ここで, $F= 2 \omega v $とおくと, 回転系上で物体が右に曲げられるという上記の運動は$F= 2 \omega v $という慣性力によってもたらされたと考えることができる。このとき,  $F= 2 \omega v $という力のことをコリオリ力 (Coriolis Force)と称し, $2\omega$をコリオリ係数と呼ぶ。



## 3. 緯度$\varphi$におけるコリオリ係数

北極のように自転軸が地面に対して垂直となっている場合は，回転角速度$\omega$を地球の回転角速度$\Omega $とおき,  コリオリ係数を$2 \Omega v$と表すことができる。しかし、極以外の点では地球が球体であることを考慮する必要がある。たとえば、赤道ではコリオリ係数は0となり, 緯度$\varphi$におけるコリオリ係数は$2\Omega \sin \varphi$となる（図2）。この理由について考える。  

<img src="Coriolis_FIG02SMALL.jpg" alt="Coriolis_FIG02SMALL" style="zoom:67%;" />

​																			図2. 緯度とコリオリ係数の関係

​			

​																<img src="Coriolis_FIG03SMALL.jpg" alt="Coriolis_FIG03SMALL" style="zoom: 67%;" />		

​															図3. 緯度$\varphi$における局所デカルト座標系



図3で緯度$\varphi$上の点Aでの接平面（地面）における、真北の方向を$y$軸、真東の方向を$x$軸とし, 鉛直方向を$z$軸にとる。このとき, 地球が角度$2 \pi$ 回転(一回転)する間に, A点における$y$軸がどれだけ回転するか考える。  

図3で地球半径を$R$とすると, $AC \sin \varphi = R \cos \varphi$より, AC=$\frac{R \cos \varphi}{\sin \varphi}$ であることがわかる。次に地球の自転により, 緯度$\varphi$での水平面が原点Aの移動に伴ってつくる円錐ABCについて, これをACで切って平面上に広げると図4の扇型CABA'ができる。   

<img src="Coriolis_FIG04SMALL.jpg" alt="Coriolis_FIG04SMALL" style="zoom: 67%;" />

​													図4. 地球が一回転する間に$y$軸はどれだけ回転するか

いま弧ABA'がつくる角度を$\theta$とおく。弧ABA'の長さは図3においてABを通る半径$R\cos \varphi$の円周の長さに等しいので、  
$$
\biggl(\frac{R \cos \varphi}{sin \varphi}\biggr)\theta = 2 \pi R \cos \varphi
$$
となる。これより, $\theta=2\pi \sin \varphi$が得られる。地球が一回転するのに要する時間を$T$とするとその回転角速度$\Omega$は$\Omega = 2\pi/T$となる。この間に$y$軸は角度が$\theta=2\pi \sin \varphi$だけ変化したので、その角速度$\omega$は$\omega=\theta/T=\Omega \sin \varphi$で与えられる。これより，緯度$\varphi$におけるコリオリ係数を$f$と書くことにすると,  
$$
f=2 \Omega \sin \varphi
$$
となる。結局コリオリ力の大きさ$COR$は、  
$$
COR=fv \\
(f=2\Omega \sin \varphi) \\
$$
と表される。  



## 4. 遠心力とコリオリ力
###　4.1 予備知識

#### 4.1.1 円運動

デカルト座標系$(x, y)$で表される物体の位置を、極座標$(r, \theta)$で表すことにすると、$x$, $y$は、$r$, $\theta$を使って、
$$
\begin{eqnarray}
x & = & r \cos \theta \\
y & = & r \sin \theta
\end{eqnarray}
$$
と表される。いま、半径$r$は一定で、角速度$\Omega$も一定の円運動（等速円運動）を考えると、$\theta = \omega t$であるから、加速度の$x$, $y$方向成分、$d^2x/dt^2$、$d^2y/dt^2$を計算すると、
$$
\begin{eqnarray}
\frac{d^2 x}{d t^2} & = & -r \omega^2 \cos \Omega t & = & -\omega^2x\\
\frac{d^2 y}{d t^2} & = & -r \omega^2 \sin \Omega t & = & -\omega^2y
\end{eqnarray}
$$
となる。したがって、質量$m$の等速円運動する物体の運動方程式は、
$$
\begin{eqnarray}
m\frac{d^2 x}{d t^2} & = & -m\Omega^2x\\
m\frac{d^2 y}{d t^2} & = & -m\Omega^2y
\end{eqnarray}
$$

原点$O$から座標$(x,y)$の点$P$に向かう方向を動径方向とよぶ。いまベクトル$\vec {OP}=\vec R$と書くことにすると、単位質量$m$の等速円運動する物体の運動方程式は、
$$
m\frac {d^2\vec R}{dt^2} = -m\Omega ^2 \vec R
$$
となる。右辺に現れた、$-m\Omega ^2 \vec R$のことを向心力とよぶ。向心力は常に円の中心に向かう。等速円運動を行う物体には向心力が働く必要がある。



#### 4.1.2 遠心力

向心力の$x$, $y$成分をそれぞれ, $F_x$, $F_y$と書くことにする. いま, 一定角速度で円運動する座標系$(x'$, $y')$を考えると、運動方程式は下記のように書きあらわされる（**1.4**にて説明）。
$$
\begin{eqnarray}m \frac{d^2 x'}{d t^2} & = & F_x - m \frac{d^2 x_0}{d t^2}　 \\m \frac{d^2 y'}{d t^2} & = & F_y - m \frac{d^2 y_0}{d t^2} 　\\\end {eqnarray}
$$
右辺第2項は座標系の変換によって現れる慣性力である。いま、**物体が座標系と同じ角速度で円運動し、回転半径が変化しない場合**, 座標系$(x'$, $y'$)から見た物体の位置は変化しないはずである。すなわち、$x'=$一定, $y'=$一定となる。このとき慣性力$(- m \frac{d^2 x_0}{d t^2}, - m\frac{d^2 y_0}{d t^2} )$は、
$$
\begin{eqnarray}
- m \frac{d^2 x_0}{d t^2} & = & - F_x 　 \\
- m \frac{d^2 y_0}{d t^2} & = & - F_y  　\\
\end{eqnarray}
$$
となり、**向心力と大きさが同じ向きで反対向きとなる**。この慣性力$(-F_x, -F_y)=(- m \frac{d^2 x_0}{d t^2}, - m\frac{d^2 y_0}{d t^2} )$のことを遠心力と呼ぶ。遠心力 (Centrifugal force)を$CEN$と書くことにすると、
$$
CEN = m \Omega ^2 \vec R
$$
となる。向きは動径方向（円の中心から外に向かう方向）である。

### 4.2 コリオリ力の由来



回転軸に直交し回転軸から着目する点に向かう動径方向のベクトルを$\vec R$とする。このとき回転角速度$\Omega$で回転する<u>単位質量</u>の物体に働く遠心力$CEN$は,
$$
CEN = \Omega^2 \vec R
$$
である。いま、物体の速度が円周方向に$u$だけ増えたとする。この時の遠心力の変化$\Delta CEN$は、
$$
\Delta CEN = \left( \Omega + \frac{u}{R}\right)^2 \vec{R}  \approx\ \Omega^2 \vec R + 2\Omega u \frac{\vec R}{R}
$$
となる。ここで、$u/R$は物体の速度変化に伴う角速度の変化である。$R$が十分大きい場合、$R^{-2}$に比例する項は他の項と比べて十分小さくなるので無視した。第2項がコリオリ力に対応する。物体が円周方向に運動している場合、コリオリ力は<u>回転する座標系から見て</u>**物体の速度が変化した場合の遠心力の超過**とみなすことができる。



### 4.3 コリオリ力の働く方向
#### 4.3.1 予備知識
##### 4.3.1.1 角運動量保存則 
半径$R$, 角速度$\Omega$で円運動する<u>単位質量</u>の物体に対し, 
$$
\Omega R^2 = constant
$$
上式を角運動量保存則という. この法則によると, 半径$R$, 角速度$\Omega$で円運動する物体に対し,
$$
\begin{eqnarray}
R増加 \quad & \longrightarrow & \quad \Omega 減少,  \\
R減少 \quad & \longrightarrow & \quad \Omega 増加.
\end{eqnarray}
$$
という関係が成り立つことがわかる.  

例：フィギュア・スケートのスピン  



##### 4.3.1.2 角運動量保存則の導出

**力のトルク**

物体を回転させようとする働きのことをトルクという。いま$x-y$平面上での回転運動を考える。物体の座標を$(x, y)$とし, 力の$x$, $y$方向成分をそれぞれ$F_x$, $F_y$とすると, 原点$O$に関するトルク, $N$は  
$$
N=xF_y - yF_x
$$
で定義される.  物体を反時計回りに回転させようとすときトルク$N$を正の値となり,  時計回りにときは負となる. 力の大きさが同じであっても, 原点から物体までの距離が長くなればトルクの大きさは大きくなる. 身近な例としては, シーソーが挙げられる.　次に角運動量($p$)と呼ばれる量を,  
$$
p=m(xv-yu)
$$
で定義する. ここで, $u$, $v$は, それぞれ速度の$x$, $y$方向成分である.　いま, $dp/dt$を計算すると,  
$$
\begin{eqnarray}
\frac{dp}{dt} & = & m\bigl( \frac{dx}{dt}v + x \frac{dv}{dt} - \frac{dy}{dt} u - y \frac{du}{dt}  \bigr) \\
 & = & m \biggl(uv + x \frac{d^2y}{dt^2} - vu - y \frac{d^2x}{dt^2} \biggl) \\
 & = & mF_x - yF_x \\
 & = & N
\end{eqnarray}
$$
ここで, $N=0$の場合, $dp/dt=$一定となる. すなわち, 力のトルクが$0$のとき, 角運動量$p=m(xv-yu)$は一定値に保たれる. ここで, 座標系として極座標,  
$$
\begin{eqnarray}
x & = & r \cos \theta \\
y & = & r \sin \theta \\
\end{eqnarray}
$$
を用いることにすると, $r=R=$一定で, $\theta=\Omega t$のとき ($\Omega$は回転角速度), 角運動量保存則は,  $R^2 \Omega =$一定, となる.  



#### 4.3.2 コリオリ力の$y$方向成分

![Coriolis_FIG05SMALL](Coriolis_FIG05SMALL.jpg)

図5. コリオリ力を成分に分解する

いま、物体が真東の向きに速度$u$で運動しているとする。この場合には、先ほどの「物体の速度が円周方向に$u$だけ増えた」場合とみなすことができる。この場合のコリオリ力は上述のように、$2\Omega u \vec R/R$で与えられるが、これは、大きさが$2\Omega u$で、向きが$\vec R$の方向を向くベクトルである。このベクトルは、  
$$
\begin{eqnarray}
y方向成分 & = & -2\Omega u \sin \varphi & = & -fu\\
z方向成分 & = & 2\Omega u \cos \varphi \\ 
\end{eqnarray}
$$

を持つ。コリオリ力の水平方向成分（今の場合$y$方向成分のみ）に着目すると、コリオリ力は東向きに運動する物体を南向きに曲げようとする。一般に、コリオリ力の水平方向成分は$f$が正の時は物体の運動方向に対し直角右向きとなる。コリオリ力には$z$方向成分 $(2\Omega u \cos \varphi)$も存在するが、$z$方向成分は重力よりも圧倒的に小さくなるので、ほとんどのケースで無視される。  

  

#### 4.3.3 コリオリ力の$x$方向成分
いま、半径$R$, 角速度$\Omega$で円運動する<u>単位質量</u>の物体の半径が$R+\Delta R$ ($\Delta R>0$)だけ変化し, その結果、回転角速度が$\Delta u/(R+\Delta R)$だけ変化したとする。このとき角運動量保存則から、  
$$
\Omega R^2 = \biggl (\Omega + \frac{\Delta u}{R+\Delta R}\biggr)(R+\Delta R)^2
$$

微小な項を無視して整理すると,  
$$
\Omega R^2 = \Omega R^2 + 2 \Omega R \Delta R + \frac{R^2 \Delta u}{R+ \Delta R},
$$
$$
\Delta u = -2 \Omega \Delta R.
$$

今、緯度$\varphi$における南北方向を$y$方向とする。いま、物体が$y$方向に$-\Delta y$だけ変化することで、回転半径が$\Delta R$だけ変化したとする (図6).  

<img src="CORIOLIS_FIG06SMALL.jpg" alt="CORIOLIS_FIG06SMALL" style="zoom: 50%;" />

​															図6. y方向の変位に伴う回転半径の変化

このとき,  
$$
\sin \varphi = \frac{\Delta R}{-\Delta y}
$$
となる。よって、  
$$
\Delta u = -2 \Omega (-\Delta y \sin \varphi).
$$
両辺を$\Delta t$で割って, $\Delta t \rightarrow 0$とすると,  
$$
\frac{du}{dt}=f\frac{\Delta y}{\Delta t}=fv
$$
となる。上式より、<u>単位質量</u>の物体に働くコリオリ力のx方向成分=$fv$が得られる。  



#### 4.3.4 遠心力とコリオリ力の相違点

同じ角速度で回転している座標系において、質量が同じ物体に対しては、遠心力の大きさは回転軸からの距離で決まるの対し、コリオリ力の大きさは物体の速度と緯度できまる。  
$$
\begin{eqnarray}
CEN & = & \Omega^2 |\vec R| \\
COR & = & fv \\
\end{eqnarray}
$$
遠心力の方向は常に回転軸に直交し、回転軸から外向きの方向を向くのに対し、コリオリ力は$f$が正の時は物体の運動方向に対し直角右向きとなる。  



## まとめ

- 運動方程式は、**座標系自体が運動していてその速度が変化する場合にはそもそも使えない**（運動の第一法則）。しかし、座標系の変換によって現れる慣性力を導入すると、この場合にも運動方程式が使えるようになる。

- 一定角速度で回転する座標系において、遠心力を無視すると、物体に外部から力が働かない場合の運動方程式は
$$
\begin{eqnarray}
m \frac{du}{dt} & = & mfv \\
m \frac{dv}{dt} & = & -mfu
\end{eqnarray}
$$
となる。右辺は座標系が回転することによる慣性力の一種でコリオリ力と呼ぶ。ここで、 $f$はコリオリ係数と呼ばれ, 緯度$\varphi$におけるコリオリ係数は$2\Omega \sin \varphi$となる。

- 同じ角速度で回転している座標系において、質量が同じ物体に対しては、遠心力の大きさは回転軸からの距離で決まるの対し、コリオリ力の大きさは物体の速度と緯度できまる。

- 遠心力の方向は常に回転軸に直交し、回転軸から外向きの方向を向くのに対し、コリオリ力は$f$が正(北半球)の時は, 物体の運動方向に対し直角右向きとなり、$f$が負(南半球)の時は, 物体の運動方向に対し直角左向きとなる。
# 気象学における運動方程式

## 1. 予備知識
### 1.1 質点の力学における運動方程式

質量$m$の物体に、力$\vec F=(F_x,F_y,F_z)$が働くとき、物体の速度$\vec v=(u,v, w)$は、下記のように変化する。
$$
m\frac{d \vec v}{d t}  =  \vec F
$$
これを運動方程式（ニュートンの運動の第2法則）という。ベクトルの成分表示を使うと運動方程式は、
$$
\begin{eqnarray}
m \frac{d u}{d t} & = & F_x　\\
m \frac{d v}{d t} & = & F_y　\\
m \frac{d w}{d t} & = & F_z　\\
\end {eqnarray}
$$
となる。

### 1.2 回転系の運動方程式
一定角速度で回転する座標系において遠心力を無視すると、運動方程式は
$$
\begin{eqnarray}
m \frac{du}{dt} & = &  mfv + F_x \\
m \frac{dv}{dt} & = & -mfu + F_y \\
m \frac{dz}{dt} & = & F_z \\
\end{eqnarray}\tag{1}
$$
となる。右辺に現れた, $mfv$, $-mfu$は, 座標系が回転することによる慣性力の一種でコリオリ力と呼ぶ。ここで、 $f$はコリオリ係数と呼ばれ, 緯度$\varphi$におけるコリオリ係数は$f=2\Omega \sin \varphi$となる。

**コリオリ力の性質**
コリオリ力は$f$が正(北半球)の時は, 物体の運動方向に対し直角右向きとなり、$f$が負(南半球)の時は, 物体の運動方向に対し直角左向きとなる。

## 気象学における運動方程式
### 2.1 気象学で特に重要となる力
大気にはさまざまな種類の力が作用しているが、気象学における特に重要な力として,
- 気圧傾度力
- 重力
がある. 

### 2.2 気圧傾度力の導出
　気圧傾度力は気圧が場所によって異なることによって生じる力で$x$, $y$, $z$の3方向すべてに働く. いま, 一定の大きさの空気の塊（かたまり）を考える。これを気象学では気塊と称する。簡単のため気塊を, $\Delta x$, $\Delta y$, $\Delta z$の寸法をもつ直方体とする。いま圧力を$p(x,y,z,t)$と表すことにすると, $x$軸に直交する2つの面 (面積は$\Delta y \Delta z$)に働く力の合力, $F_x$は, 
$$
F_x = p(x,y,z,t)\Delta y \Delta z - p(x+\Delta x, y, z, t)\Delta y \Delta z
$$
となる. これを, 
$$
\begin{eqnarray}
F_x & = &-\biggl( p(x+\Delta x, y, z, t) - p(x,y,z,t) \biggr)\\
& = & -\biggl( \frac{p(x+\Delta x, y, z, t) - p(x,y,z,t)}{\Delta x}\biggr)\Delta x \Delta y \Delta z
\end {eqnarray}
$$
と変形し, さらに, $\Delta x \longrightarrow 0$の極限をとると,
$$
\begin{eqnarray}
F_x = -\frac{\partial p}{\partial x}\Delta x \Delta y \Delta z
\end {eqnarray} \tag{2}
$$
となる. これが気圧傾度力の$x$成分となる. 同様に, $y$成分 ($F_y$), $z$成分 ($F_z$)を求めると, 
$$
\begin{eqnarray}
F_y & = & -\frac{\partial p}{\partial y}\Delta x \Delta y \Delta z \\
F_z & = & -\frac{\partial p}{\partial z}\Delta x \Delta y \Delta z
\end {eqnarray} \tag{3}
$$
となる。

### 2.3 重力 
 通常鉛直($z$)方向は, 重力の向きと逆向きにとるので、 重力は$z$方向成分のみとなる. 重力を$G_z$と書くことにすると, 気塊の質量を$m$として, 
$$
\begin{eqnarray}
G_z=-mg
\end {eqnarray} \tag{4}
$$
となる. 負号が付くのは, 重力の働く向きと反対方向を$z$軸の正の方向としているからである.

### 2.4 気象学における運動方程式の導出
今, 気塊の密度を$\rho$とすると, 気塊の質量$m$は$m=\rho \Delta x \Delta y \Delta z$である。上の式$(2)-(4)$式を$(1)$に代入して, 両辺を$\rho \Delta x \Delta y \Delta z$で割ると, 
$$
\begin{eqnarray}
\frac{d u}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial x} + fv \\
\frac{d v}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial y} -fu\\
\frac{d w}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial z} - \rho g\\
\end {eqnarray} \tag{5}
$$

気象学では$(5)$の形の運動方程式を用いる. 
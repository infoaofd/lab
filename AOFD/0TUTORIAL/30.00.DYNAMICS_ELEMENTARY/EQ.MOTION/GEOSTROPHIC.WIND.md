# 地衡風

## 1. 予備知識
### 1.1 気象学における運動方程式
 いま, $x$, $y$, $z$をそれぞれ東西方向, 南北方向, 鉛直方向の座標とする. 気象学では, $x$, $y$, $z$はそれぞれ, 東向き, 北向き, 上向きを正とするのが慣例である。気塊の速度$\vec v$の$x$, $y$, $z$方向成分をそれぞれ$u$, $v$, $w$, 緯度$\varphi$におけるコリオリ係数を$f=2\Omega \sin \varphi$, 圧力を$p(x,y,z,t)$と表すことにすると, 単位質量の空気の塊(気塊)に関する運動方程式は次のようになる.  
$$
\begin{eqnarray}
\frac{d u}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial x} + fv
\end {eqnarray} \tag{1}
$$

$$
\begin{eqnarray}
\frac{d v}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial y} -fu\\
\end {eqnarray} \tag{2}
$$

$$
\begin{eqnarray}
\frac{d w}{d t} & = & -\frac{1}{\rho} \frac{\partial p}{\partial z} - \rho g\\
\end {eqnarray} \tag{3}
$$
## 2. 地衡風

偏西風、温帯低気圧、移動性高気圧のような数千キロメートル以上の空間規模をもつ大気現象について考える。様々な量のおおよその大きさやおおよその変化幅をオーダーと呼ぶことがある。 $f \approx \, 10^{-4} \,s$, $\rho \approx 1 \, kg/m^3$とし, $u, v$のオーダーを$10 \, m/s$, $t$のオーダーを$1$日$\, \approx \,10^5 \,s$, $\frac{\partial p}{\partial x}$のオーダーを, 
$$
\frac{\partial p}{\partial x} \sim \frac{1 \, hPa}{1000 \, km} = \frac{100 \, kg \, m^{-1}s^{-2}}{10^5 \, m}
$$
と見積もると, $(1)$の各項のおおよその大きさは
$$
\frac{d u}{d t} \sim 10^{-4} \, m \, s^{-2}
$$
$$
\frac{1}{\rho} \frac{\partial p}{\partial z} \sim 10^{-3} \, m \, s^{-2}
$$
$$
fv \sim 10^{-3} \, m \, s^{-2}
$$
となり, 高々10%程度の誤差で$(1)$は,
$$
\begin{eqnarray}
fv & \approx & \frac{1}{\rho} \frac{\partial p}{\partial x} 
\end {eqnarray} \tag{4}
$$

と近似できることが分かる。同様に$(2)$に関しても, 
$$
\begin{eqnarray}
fu & \approx & - \frac{1}{\rho} \frac{\partial p}{\partial y} 
\end {eqnarray} \tag{5}
$$

と近似できる。気象学では, 観測される風を, コリオリ力と気圧傾度力が完全に釣り合っているような成分,
$$
\begin{eqnarray}
fv_g & = & \frac{1}{\rho} \frac{\partial p}{\partial x} 
\end {eqnarray} \tag{6}
$$
$$
\begin{eqnarray}
fu_g & = & - \frac{1}{\rho} \frac{\partial p}{\partial y} 
\end {eqnarray} \tag{7}
$$
と残りの成分, $u_a$, $v_a$に分けて考えることが非常に多い。式で書くと,
$$
\begin{eqnarray}
u &=& u_g+u_a \\
v &=& v_g+v_a
\end {eqnarray}
$$
となる。$u_g, v_g$を地衡風 (geostrophic wind), $u_a$, $v_a$を非地衡風 (**a**geostrophic wind)と呼び, $(6)$, $(7)$の関係を地衡風バランスとか地衡風平衡(geostrophic balance)と称する。 $(6)$,$(7)$より次のことが分かる。

- 地衡風の風向と等圧線は平行となる

- コリオリ係数$f$が正の時, 地衡風は高圧部を右に見るように吹く

- コリオリ係数$f$が負の時, 地衡風は高圧部を左に見るように吹く

  

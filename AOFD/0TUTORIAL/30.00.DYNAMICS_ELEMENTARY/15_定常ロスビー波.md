# 定常ロスビー波

<img src="FIG_RESUME\ROSSBY_Z300_DJF.png" alt="ROSSBY_Z300_DJF" style="zoom: 25%;" />

<div style="text-align: center;"<div style="text-align: center;"つ>
    図1. 12月から2月の300hPaの等圧面高度の平年値 ([km])。等圧面高度が北極を中心とする同心円と平行とはならずに南北に波打っている状態となっていることが分かる。異常気象と呼ばれる現象の多くは，このような等高度線の南北変位が極端に大きくなった状態が持続した場合に起こることが多い。
</div>


[[_TOC_]]

ジェット気流を表現するため，東西方向に一様な流れ$U$が存在するとする。ここでは簡単のため，ジェット気流は東西方向成分($x$方向成分)のみをもち，南北($y$), 鉛直($z$)方向成分は0とする。さらに，$U$=一定とする ($U$は時間的にも空間的にも変化しないと仮定する)。また，$u$を$U$からのズレと定義する。$u$は高気圧・低気圧による風速変化を想定している。このとき移流項の$x$方向成分は，
$$
\begin{eqnarray}
&&(u+U)\frac{\partial (u+U)}{\partial x}+v\frac{\partial (u+U)}{\partial y}+w\frac{\partial (u+U)}{\partial z}    \\
&=&(u+U)\frac{\partial u}{\partial x}+v\frac{\partial u}{\partial x}+w\frac{\partial u}{\partial x}    \\
\end{eqnarray}
$$
となる。さらに$u$, $v$, $w$, $\ll U$と仮定すると，移流項は，
$$
\begin{eqnarray}
&&(u+U)\frac{\partial (u+U)}{\partial x}+v\frac{\partial (u+U)}{\partial y}+w\frac{\partial (u+U)}{\partial z}    \\
&=&U\frac{\partial u}{\partial x}
\end{eqnarray}
$$
と近似できる。このとき運動方程式は，
$$
\begin{eqnarray}
 \frac{\partial u}{\partial t}+U\frac{\partial u}{\partial x} &=& (f_0+\beta y)v -\frac{\partial }{\partial x}\bigg(\frac{p}{\rho_0} \bigg)   \tag{1}\\

  \frac{\partial v}{\partial t}+U\frac{\partial v}{\partial x} &=& -(f_0+\beta y)u -\frac{\partial }{\partial y}\bigg(\frac{p}{\rho_0} \bigg)    \tag{2}\\

\end{eqnarray}
$$
となる。いま，$\psi=p/(f_0\rho_0)$, $\zeta:=\partial v/\partial x -\partial u/\partial y$, $\nabla^2=\partial^2/\partial x^2+\partial^2/\partial y^2$と定義し，$\frac{\partial}{\partial x}$(2) -$\frac{\partial}{\partial x}$(1)という計算を行い，$u$, $v$が地衡流で近似できるとすると，
$$
\begin{eqnarray}
 \frac{\partial }{\partial t}\nabla^2\psi+U\frac{\partial }{\partial x}\nabla^2\psi+\beta\frac{\partial \psi}{\partial x} =0   \tag{3}\\

\end{eqnarray}
$$
が得られる。

(3)の解として，
$$
\begin{eqnarray}
\psi=\sin(kx+ly-\omega t)\end{eqnarray}
\tag{4}
$$
を仮定し，(4)を(3)に代入して計算すると，

$$
\begin{eqnarray}
((k^2+l^2)\omega-(k^2+l^2)Uk+\beta k)\cos(lx+ly-\omega t)=0
\end{eqnarray}
\tag{5}
$$
が得られる。
$$
\begin{eqnarray}
\omega=Uk-\frac{\beta k}{k^2+l^2}
\end{eqnarray}
\tag{6}
$$
という関係が成り立っていれば，(4)は(3)の解となる。(6)より，
$$
\begin{eqnarray}
U=\frac{\beta}{k^2+l^2}
\end{eqnarray}
\tag{7}
$$
を満たす$(k,l)$に対して，$\omega=0$となる。このとき(4)は，
$$
\begin{eqnarray}
\psi=\sin(kx+ly)
\end{eqnarray}
\tag{4}
$$
となり，$\psi$が$t$に依存しないことから分かるように, 動かずに止まって見える。このような止まって動かない波を定常ロスビー波という。

**例**

- ジェット気流の蛇行
- 黒潮大蛇行

大気中で動かない波が長時間存在するということは，ある定点でみれば，普段より暑い（寒い）状態が長期間持続する，ということを意味する。異常気象と呼ばれる現象の多くは，定常ロスビー波の長期間の持続，強化と関連している。海流が大幅に蛇行すると，普段よりも水温が高い(低い)状態が持続するので，海洋生態系に大きな影響を及ぼす。気候への影響も現在盛んに研究されている。


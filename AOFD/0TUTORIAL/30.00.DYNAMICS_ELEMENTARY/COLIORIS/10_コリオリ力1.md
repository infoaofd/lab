# コリオリ力 1



## 概要

地球のような回転している惑星における流れを扱う際，ほとんどの場合において**地球とともに回転する座標系 **(**回転系**)が用いられる。

**回転系は非慣性系である**ため，第二法則をそのまま適用することができない。この場合, 座標系を変換することによって，第二法則が適用可能となるように運動方程式を修正する。この際に遠心力とコリオリ力と呼ばれる2つの修正項が現れる。

地球は回転する**球体**であるが，ここでは最初のステップとして，回転する**平面**における座標変換から，遠心力とコリオリ力を導く。

大気と海洋の様々な運動を考察する際に，球面上の運動を，球面上の接平面の運動と近似することが頻繁に行われるので (後述)，ここでの結果は実用的な観点からも重要である。

コリオリ力は本質的には，**回転系における座標変換にともなって現れる慣性力**である。コリオリ力を導く方法は，数式を用いるものや作図によるものなど多数考案されているが，どの方法をとるにせよ，コリオリ力な正確な表現を得るためには少々煩雑な手順を踏む必要がある。

ここで述べる方法はその中でもあまり技巧の要らないものであるが，それでも難しく感じる場合には，まず下記の3つの要点を押さえておくことを推奨する。

- コリオリ力は**回転系における運動方程式**に現れる**修正項**である
- コリオリ力は**回転軸**と物体の**運動方向**の**双方に対し直交**する方向を向き，**大きさは速度に比例**する
- コリオリ力は**地球の北半球**では**運動方向を右にそらせるように働く**

コリオリ力の直感的イメージは，補足資料 (コリオリ力の直感的説明.pdf)に記載したので，適宜参照のこと。



## 予備知識

### 角度の表し方

角度の大きさを表すのに，度($^\circ$)よりもラジアンがよく用いられる。360度=$2\pi$ラジアンと定める。これは半径1の円の円周の長さが$2\pi$であることに由来する。

**ラジアンは**，「円弧の長さ/半径」で定義される**割合である**ため，**厳密には単位ではない**が，慣例で単位のように扱われ, $[\mathrm{rad}]$のように表記されることがある。

度とラジアンの換算は以下の通り,
$$
\begin{eqnarray}
\begin{matrix}
1^\circ=\frac{\pi}{180}\simeq 0.0175 [\mathrm{rad}]\\
1 [\mathrm{rad}]=\frac{180}{\pi}\simeq 57.3 ^\circ\\
\end{matrix}
\tag{1}
\end{eqnarray}
$$


#### 角速度

回転軸と平行で，大きさが単位時間あたりの回転角に等しいベクトルのことを**角速度ベクトル**と呼ぶ。角速度ベクトルは，$\mathbf{\omega}$や$\mathbf{\Omega}$ (オメガ)という文字であらわされることが多い。

回転方向が右回転か左回転かで角速度ベクトルの向きを区別している。通常，角速度ベクトルをネジのように回転方向と同じ向きに回転させたとき，右ネジが進む方向を角速度ベクトルの向きとしている。

単位時間あたりの回転角 (角速度の大きさ)$\omega$で$z$軸を回転軸として回転する座標系を考える。時間を$t$，回転角を$\varphi$ (ファイ)で表すことにすると，$\varphi=\omega t$である。



## 座標変換
<img src="FIG_RESUME\2D_ROTATE_FORCE.png" alt="2D_ROTATE_FORCE" style="zoom: 50%;" />

<div style="text-align: center;">
図1. 力の成分の慣性系から回転系への変換
</div>


慣性系において，力$F$の$x$, $y$方向成分がそれぞれ$X$, $Y$で表されたとする。

慣性系Oに対して, 反時計周りに角度$\varphi$だけ回転した回転系O'を考える(図1)。O'系の力$F$の, $x'$, $y'$方向成分をそれぞれ$X'$, $Y'$とする。このとき$(X',Y')$は$(X,Y)$と$\varphi$を用いて，
$$
\begin{eqnarray}
\begin{matrix}
X'&=& X \cos\varphi+ Y \sin\varphi \\
Y'&=&-X \sin\varphi+ Y \cos\varphi 
\end{matrix}
\tag{2}
\end{eqnarray}
$$
と表される。

次に, 図2を参照して慣性系における座標 $(x,y,z)$と回転系における座標 $(x',y',z')$の間の変換式を求めると，

<img src="FIG_RESUME\2D_ROTATE_XY_COORD.png" alt="2D_ROTATE_XY_COORD" style="zoom:50%;" />

<div style="text-align: center;">
図2. 回転系から慣性系への座標変換
<div>

$$
\begin{eqnarray}
\begin{matrix}
x &=& x' \cos\varphi - y' \sin\varphi \\
y &=& x' \sin\varphi + y' \cos\varphi \\
z &=& z'
\end{matrix}
\tag{3}
\end{eqnarray}
$$
となる。

慣性系Oにおいて，座標$(x,y,z)$に質量$m$の質点が存在したとする。この質点に関する運動方程式は，
$$
\begin{eqnarray}
m\frac{d^2x}{dt^2}=X, \quad m\frac{d^2y}{dt^2}=Y, \quad m\frac{d^2z}{dt^2}=Z
\end{eqnarray}\tag{4}
$$
である。

これに(3)を代入すると，
$$
\begin{eqnarray}
m\frac{d^2}{dt^2}(x' \cos\varphi - y' \sin\varphi)&=&X, \\
m\frac{d^2}{dt^2}(x' \sin\varphi + y' \cos\varphi)&=&Y, \\
m\frac{d^2}{dt^2}z'&=&Z
\end{eqnarray}\tag{5}
$$
となる。

(5)の第一式の左辺を計算してみる。$\varphi=\omega t$であることに注意する。三角関数の微分公式
$$
\begin{eqnarray}
\frac{d(\sin x)}{dx}&=&\cos x \\
\frac{d(\cos x)}{dx}&=&-\sin x
\end{eqnarray}
$$

および，合成関数の微分公式, 
$$
\begin{eqnarray}
\frac{du}{dt}=\frac{du}{dx}\frac{dx}{dt}
\end{eqnarray}
$$
を用いる。一例を挙げると，$u=\cos\varphi$, $\varphi=\omega t$とおき，
$$
\begin{eqnarray}
\frac{du}{dt}&=&\frac{du}{d\varphi}\frac{d\varphi}{dt}\\
&=&\frac{d(\cos\varphi)}{d\varphi}\frac{d\varphi}{dt}\\
&=&-\sin\varphi\cdot\omega \\
\end{eqnarray}
$$
のように計算する。上記の例に加え，関数の積の導関数の公式
$$
(fg)' = f'g+fg'
$$
も使って実際に計算すると，
$$
\begin{eqnarray}
& &\frac{d}{dt}(x' \cos\varphi - y' \sin\varphi) \\
&=& \bigg(\frac{dx'}{dt}\cos\varphi-x'\sin\varphi \cdot \omega \bigg)-\bigg(\frac{dy'}{dt}\sin\varphi+y'\cos\varphi \cdot \omega \bigg).
\end{eqnarray}
$$

となる。さらに，二階の導関数の定義
$$
\begin{eqnarray}
\frac{d^2x}{dt^2}:=\frac{d}{dt}\bigg(\frac{dx}{dt}\bigg)
\end{eqnarray}
$$
を用いて計算を進めると，
$$
\begin{eqnarray}
& &\frac{d^2}{dt^2}(x' \cos\varphi - y' \sin\varphi) \\
&=&\frac{d}{dt}\bigg[ \bigg(\frac{dx'}{dt}\cos\varphi-x'\sin\varphi \cdot \omega \bigg)-\bigg(\frac{dy'}{dt}\sin\varphi+y'\cos\varphi \cdot \omega \bigg) \bigg] \\
&=& \frac{d^2x'}{dt^2}\cos\varphi- \frac{dx'}{dt}\sin\varphi \cdot \omega -\omega\frac{dx'}{dt}\sin\varphi-\omega^2x'\cos\varphi \\
&-&\bigg(\frac{d^2y'}{dt^2}\sin\varphi + \frac{dy'}{dt}\cos\varphi \cdot \omega +\omega\frac{dy'}{dt}\cos\varphi -\omega^2y'\sin\varphi \bigg)
\end{eqnarray}
$$

が得られる。



上式より，運動方程式の$x$方向成分$m\frac{d^2x}{dt^2}=X$は，
$$
\begin{eqnarray}
m\bigg[\frac{d^2x'}{dt^2}\cos\varphi - \frac{d^2y'}{dt^2}\sin\varphi - 2\omega\bigg(\frac{dx'}{dt}\sin\varphi+\frac{dy'}{dt}\cos\varphi \bigg) \\ - \omega^2(x'\cos\varphi-y'\sin\varphi)\bigg]=X
\tag{6}
\end{eqnarray}
$$
となる。同様に，運動方程式の$y$方向成分$m\frac{d^2y}{dt^2}=Y$は，
$$
\begin{eqnarray}
\frac{d^2}{dt^2}(x' \sin\varphi + y' \cos\varphi) \\
\end{eqnarray}
$$
を計算することにより，
$$
\begin{eqnarray}
m\bigg[\frac{d^2x'}{dt^2}\sin\varphi + \frac{d^2y'}{dt^2}\cos\varphi + 2\omega\bigg(\frac{dx'}{dt}\cos\varphi-\frac{dy'}{dt}\sin\varphi \bigg) \\ - \omega^2(x'\sin\varphi+y'\cos\varphi)\bigg]=Y
\tag{7}
\end{eqnarray}
$$
となることが分かる。(6)$\times\cos\varphi$+(7)$\times\sin\varphi$を計算すると, (2)$X'=X \cos\varphi+ Y \sin\varphi$より，
$$
\begin{eqnarray}
m\bigg[\frac{d^2x'}{dt^2} - 2\omega\frac{dy'}{dt} - \omega^2 x'\bigg]=X'
\tag{8}
\end{eqnarray}
$$
が得られる。

同様にして，(6)$\times(-\sin\varphi)$+(7)$\times\cos\varphi$を計算すると, (2)$Y'=-X \sin\varphi+ Y \cos\varphi $より, 
$$
\begin{eqnarray}
m\bigg[\frac{d^2y'}{dt^2} + 2\omega\frac{dx'}{dt} - \omega^2 y'\bigg]=Y'
\tag{9}
\end{eqnarray}
$$
が得られる。

回転系における速度の$x$,$y$,$z$方向成分$u$,$v$,$w$をそれぞれ，$u':=\frac{dx'}{dt}$, $v':=\frac{dy'}{dt}$, $w':=\frac{dz'}{dt}$と定義すると，回転系における運動方程式は
$$
\begin{eqnarray}
m\frac{du'}{dt} &=&X'+ 2m\omega v' + m\omega^2 x' \\
m\frac{dv'}{dt}&=&Y' - 2m\omega u' + m\omega^2 y'\\
m\frac{dw'}{dt}&=&Z'
\end{eqnarray}
\tag{10}
$$
と表すことができる。

表記を簡潔にするため，$'$(プライム)を取ることにすると，最終的に, 
$$
\begin{eqnarray}
m\frac{du}{dt} &=&X+ 2m\omega v + m\omega^2 x \\
m\frac{dv}{dt}&=&Y - 2m\omega u + m\omega^2 y\\
m\frac{dw}{dt}&=&Z
\end{eqnarray}
\tag{11}
$$
が得られる。

右辺第2項 $(2m\omega v,  - 2m\omega u)$をコリオリ力, 第3項$(m\omega^2 x,m\omega^2 y) $を遠心力と呼ぶ。

コリオリ力と遠心力は**慣性系から回転系へ座標変換を行ったことで現れた項である**ことに注意。



## 練習

1. 図1を作図して，式(2)が成立することを確認せよ

2. 図2を作図して，式(3)が成立することを確認せよ。

3. (11)式をもとに, 遠心力は回転系における物体の速度とは無関係であることを説明せよ

4. (11)式をもとに, 遠心力の大きさは回転軸からの距離に比例することを説明せよ。

   ヒント：力はベクトルなので，力の大きさはベクトルの大きさとして求める。また，原点から座標が$(x,y)$である点までの距離は, $\sqrt{x^{2}+y^{2}}$となる。

5. (11)式をもとに, 遠心力の向きは回転軸を中心とする円の半径の方向(円の中心から外に向かう方向)であることを説明せよ。

   ヒント：ベクトル$(x,y)$は座標原点から延びる半直線上の点の位置ベクトルである。

6. コリオリ力は回転系における速度ベクトル $(u,v)$と直交することを説明せよ。

   ヒント：内積を計算せよ。

7. $\omega$が正の時，コリオリ力は速度ベクトルから見ると右向きになることを説明せよ。

8. (7)式を導け

9. (8)式を導け

10. (9)式を導け

---
marp: true
theme: default
size: 16:9
paginate: true
headingDivider: 2
header: 
footer: © AM
---

# 記号

講義でよく使用する記号についてまとめておく。


## 定義を表す記号
$\mathrm{A}:=\mathrm{B}$  	$\mathrm{A}$を$\mathrm{B}$で定義する。$\mathrm{B}$の内容を表すのに，今後$\mathrm{A}$を用いると約束する。      　　　　
## 等しいことを表す記号
$\mathrm{A}\simeq\mathrm{B}$		$\mathrm{A}$は$\mathrm{B}$とおおよそ等しい ($\fallingdotseq$と同じ意味)。  

$\mathrm{A}\sim\mathrm{B}$		$\mathrm{A}$は$\mathrm{B}$と同じオーダーである。

この記号は下記の意味で用いられる

- $\mathrm{A}/\mathrm{B}$がおおよそ一桁となる
- 細かい数値の違いに目をつぶれば，おおよそ同程度の大きさの値と見なすことができる
-  $\simeq$よりも一致の程度は低い  

## 大小関係を表す記号
$\mathrm{A}\ll\mathrm{B}$		$\mathrm{A}$は$\mathrm{B}$よりもずっと小さい (無視してもよいぐらい小さい)。  

$\mathrm{A}\gg\mathrm{B}$		$\mathrm{A}$は$\mathrm{B}$よりもずっと大きい。  

## 比例関係を表す記号
$\mathrm{A} \propto \mathrm{B}$		$\mathrm{A}$は$\mathrm{B}$に比例する ($c$を定数として, $\mathrm{A}=c\mathrm{B}$と書ける)。  



## 自然対数

$e:=$ネイピア数 ($e \simeq 2.718$, ふなひとはち)。  

$\ln x := \log_ex$  ($x$の自然対数)

$y=\ln x$とおくと，$y=\log_ex$より，$x=e^y$

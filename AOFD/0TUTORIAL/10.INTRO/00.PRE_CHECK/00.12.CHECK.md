# 予備知識の確認

<u>**解答用紙**</u>の<u>**右上**</u>に，<u>**学生番号**</u>と<u>氏名</u>を記入せよ。

## 記号

$\mathrm{A}:=\mathrm{B}$  	$\mathrm{A}$を$\mathrm{B}$で定義する。$\mathrm{B}$の内容を表すのに，今後$\mathrm{A}$を用いると約束する。

## 問1

圧力の単位, 1 [hPa] (ヘクトパスカル)を[kg] (キログラム), [m] (メートル), [s] (秒)を使って表せ。

## 問2

物理学において，「速度」と「速さ」という用語は区別して使用される。2つの用語の違いを述べよ。

## 問3

(1) 加速度の**定義**を述べよ。

(2) 加速度の**単位**を述べよ。

## 問3

$f(x)$を何度でも微分可能な関数とする。いま，$f(x)$がべき級数を使って，
$$
f(x)=a_0+a_1x+a_2x^2+\cdots+a_nx^n+\cdots
$$
のように展開できたとする。このとき,
$$
a_n=\frac{1}{n!}f^{(n)}(x)
$$
と表せることを示せ。ここで，$n!=n(n-1)\cdots2\cdot1$, $f^{(n)}(x)$を$f(x)$の$n$階導関数と定義すする。

## 問4

(1) $e^x$の$x=0$におけるテイラー展開を第3項まで求めよ。

(2) $\sin x$の$x=0$におけるテイラー展開を第3項まで求めよ。

(3) $\cos x$の$x=0$におけるテイラー展開を第3項まで求めよ。

## 問5

$\phi (x,t) = kx-\omega t$のとき, 次の(1)と(2)を求めよ。

(1) $\partial \phi / \partial x$

(2) $\partial \phi / \partial t$

## 問6

$u=u(x)$, $x=x(t)$のとき, $u=u(x(t))$である。このとき，以下の公式を導け。

$$
\begin{eqnarray}
\frac{du}{dt}=\frac{du}{dx}\frac{dx}{dt}
\end{eqnarray}
$$

## 問7

$z (x,t) = \sin (kx-\omega t)$のとき, 次の(1)と(2)を求めよ。

(1) $\partial z / \partial x$

(2) $\partial z / \partial t$

## 問8

関数の積の導関数の公式, $(fg)' = f'g+fg'$を導出せよ。

## 問9

関数$f$の微分と呼ばれる量$df$を,  

$$
df:=\frac{df}{dx} dx
$$
と定義する。このとき,   

$$
d(fg)=(df)g+f(dg)
$$

を導け。

## 問10

$f=f(x,y)$のとき, $f$の増分, $\Delta f$を

$$
\Delta f := f(x+d x, y+d y)-f(x,y)
$$

で定義する。また，$f$の全微分と呼ばれる量, $df$を

$$
df:=\frac{\partial f}{\partial x}dx+\frac{\partial f}{\partial y}dy
$$

で定義する。$f(x, y)=xy$のとき, $\Delta f$ と$df$の差が, $(dx)(dy)$となることを示せ。

## 問11

逆関数の微分公式

$$
\frac{dy}{dx}=\frac{1}{\frac{dx}{dy}}
$$

を導出せよ。

## 問

$R$を気体定数と呼ばれる定数とする。$p$, $V$, $T$をそれぞれ，気圧, 体積, 温度としたとき (これらは変数), 常温の空気は次の式にほぼ従う。

$$
pV=RT
$$

このとき,

$$
p \,dV + V \, dp = R \, dT
$$

となることを示せ。
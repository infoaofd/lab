import pandas as pd
import matplotlib.pyplot as plt
import os

# データファイルのパス
file_path = 'meiv2.data'  # ここに実際のファイル名を指定

# データの読み込み
def read_data(file_path):
    # ファイルを読み込んで行を処理する
    data = []
    months = ["DJ", "JF", "FM", "MA", "AM", "MJ", "JJ", "JA", "AS", "SO", "ON", "ND"]
    
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # 1行目（開始年と終了年）は無視
    # 2行目以降のデータを処理
    for line in lines[1:]:
        # コメント行（欠損値の記録部分）や空行をスキップ
        if line.startswith('Multivariate') or line.startswith('https://'):
            break
        if line.strip() == '':
            continue
        
        # 行を空白で分割
        values = line.split()
        
        # 年を取得
        try:
            year = int(values[0])
        except ValueError:
            continue  # 年が無効な場合はスキップ（-999.00 など）

        # 月ごとのデータ（2月ごとに12値）
        months_data = [float(value) if float(value) != -999.00 else None for value in values[1:]]
        
        # 年と月ごとのデータをリストに追加
        data.append([year] + months_data)

    # Pandas DataFrameに変換
    df = pd.DataFrame(data, columns=["Year"] + months)
    return df

# グラフを描画
def plot_data(df):
    # 連続した年月のリストを作成
    dates = []
    values = []
    
    # データを1本の線として描画
    for _, row in df.iterrows():
        for month, value in zip(row[1:].index, row[1:]):
            # 年と月を組み合わせてX軸のラベルとして使用
            dates.append(f"{row['Year']} {month}")
            values.append(value)

    # X軸を年月の連続としてプロット
    plt.figure(figsize=(12, 6))  # 縦のサイズを大きくして見やすくする

    # 塗りつぶし: 正の部分は赤、負の部分は青
    plt.fill_between(dates, values, where=[v > 0 for v in values], color='r', alpha=0.5, label='Positive')  # 正の部分を赤で塗りつぶし
    plt.fill_between(dates, values, where=[v < 0 for v in values], color='b', alpha=0.5, label='Negative')  # 負の部分を青で塗りつぶし

    # グラフの設定
    plt.title('Multivariate ENSO Index Version 2 (MEI.v2)', fontsize=16)
    plt.xlabel('Year', fontsize=12)
    plt.ylabel('Value', fontsize=12)

    # X軸のラベルを5年ごとに表示
    years = df["Year"]
    tick_positions = [i * 12 for i in range(len(df))]  # チックマークを1年ごとに設定
    tick_labels = years[::5]  # ラベルを5年ごとに表示
    
    plt.xticks(tick_positions[::5], tick_labels, rotation=45)  # チックマークの位置とラベルを対応させる

    # 正のピークに横軸の値（年と月）を追加
    for i in range(1, len(values) - 1):
        if values[i] > 1.5 and values[i] > values[i - 1] and values[i] > values[i + 1]:
            plt.text(dates[i], values[i] + 0.1, dates[i], ha='center', fontsize=10, color='black')

    # 負のピークに横軸の値（年と月）を追加
    for i in range(1, len(values) - 1):
        if values[i] < -1.5 and values[i] < values[i - 1] and values[i] < values[i + 1]:
            plt.text(dates[i], values[i] - 0.1, dates[i], ha='center', fontsize=10, color='black')

    # 縦線（グリッド）を無効化
    plt.grid(False)
    
    plt.tight_layout()

    # スクリプト名を取得してPDFファイル名に使用
    script_name = os.path.basename(__file__).replace(".py", "")
    output_pdf = f"{script_name}_timeseries_plot.pdf"
    
    # PDFとして保存
    plt.savefig(output_pdf, format='pdf')
    plt.close()
    
    # 作成したPDFファイル名を表示
    print(f"Plot saved as {output_pdf}")

# メイン処理
def main():
    df = read_data(file_path)
    plot_data(df)

if __name__ == "__main__":
    main()

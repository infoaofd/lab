# PV_INVERSION_PLANETRAY_GEOSTROPHIC

[[_TOC_]]

## はじめに

海洋の大規模場を考えるときにしばしば用いられる惑星地衡近似  (PLANETRAY GEOSTROPHIC APPROXIMATION)における, 渦位逆変換　(Potential vorticity inversion)について考える。



## 浅水系 (Shallow Water System)

$\eta$を基準面 ($z=0$)から測った水位偏差(もしくは層厚偏差), $\eta_b$を基準海底面 ($z=0$)から測った海底面の高さとする。このとき,
$$
\begin{eqnarray}
\eta=h+\eta_b
\tag{1}
\end{eqnarray}
$$


惑星地衡近似における渦位$Q$は，コリオリ係数を$f$, $h$を基準面の水深(もしくは基本場の層厚)とすると，
$$
\begin{eqnarray}
Q:=\frac{f}{h}
\tag{2}
\end{eqnarray}
$$
で定義される。渦位保存則は,
$$
\begin{eqnarray}
\frac{DQ}{Dt}=0
\tag{3}
\end{eqnarray}
$$
であり，ベクトル$\mathbf{f}$を$\mathbf{f}:=f\mathbf{k}$, ベクトル$\mathbf{u}$を$\mathbf{u}:=(u, v, 0)$で定義すると, 地衡風平衡の式は,
$$
\begin{eqnarray}
\mathbf{f}\times\mathbf{u}=-g\nabla\eta
\tag{4}
\end{eqnarray}
$$
となる。(1)より，$Q$と$f$が分かれば$h$が分かり，さらに(1)より，$\eta$が分かる。さらに(4)より$\mathbf{u}$が算出することができる。



## 連続成層モデル

密度$\rho$を基準密度$\rho_0$ (海水の場合 1000 [$\mathrm{kg/m^3}$]の程度の大きさの一定値を用いる)，および偏差$\delta \rho$の和で表すことにすると,
$$
\rho=\rho_0+\delta \rho
$$
となる。このとき, 浮力$b$を
$$
b:=-g\delta \rho/\rho_0
$$
で定義する。$\phi$を圧力偏差 (圧力のうち$b$と静水圧平衡にある成分)とすると,
$$
\begin{eqnarray}
\frac{\partial \phi}{\partial z}=b
\tag{5}
\end{eqnarray}
$$
の関係がある。地衡風平衡の式は,
$$
\begin{eqnarray}
\mathbf{f}\times\mathbf{u}=-g\nabla\phi
\tag{6}
\end{eqnarray}
$$


また，渦位$Q$は
$$
\begin{eqnarray}
Q:=f\frac{\partial b}{\partial z}
\tag{7}
\end{eqnarray}
$$
で定義される。$Q$が既知の時, (7)より$b$の値を求めることができる。さらに(5)から$\phi$が, (6)から$\mathbf{u}$を算出することができる。



## 引用文献

Vallis, G. (2007) Atmospheric-Oceanic Fluild Dynamics, 2nd. Ed. Cambridge University Press, 178-180.



## 
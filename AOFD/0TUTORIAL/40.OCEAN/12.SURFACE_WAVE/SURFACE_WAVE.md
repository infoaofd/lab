# 水面波の位相速度

[[_TOC_]]

## 基礎概念

**波長**: 波の峰から峰までの距離 (ここではギリシャ文字$\lambda$を用いる)。

**周期**: 一つの固定点で波を観察したとき, ある波の峰が通過して次の峰が通過するまでに要する時間 (ここでは$T$を用いる)。

**波数**: 波長$\lambda$の逆数に$2 \pi$をかけたもの (ここでは$k$を用いる)。$k=2 \pi/\lambda$。単位長さの中に何個の峰があるか。

**周波数**: 周期$T$の逆数に$2 \pi$をかけたもの (ここではギリシャ文字$\omega$を用いる)。$\omega = 2 \pi / T$。単位時間に何個の峰が通過するか。

**位相速度**: 位相速度とは, 波のパターンの伝わる速度のことである。位相速度を$c$と書くことにすると, 
$$
c:=\frac{\lambda}{T}
$$
$k=2 \pi/\lambda$, $\omega = 2 \pi / T$であるから,
$$
c=\frac{\frac{2 \pi}{k}}{\frac{2 \pi}{\omega}}=\frac{\omega}{k}
$$
である。

### 練習

上記の用語の定義を自分で紙に書き下せ。記憶しておくと後の内容を理解しやすくなる。



## 要点: 水面波の位相速度

### 一般の場合

$k$を波の波数, $g$を重力加速度, $H$を水深とする。このとき, 水面波の位相速度$c$は
$$
c=\sqrt{\frac{g}{k}\tanh(kH)}
$$
で与えられる。$\tanh$はハイパボリック・タンジェントと呼ばれる関数である(下記参照)。

### 波の波長が水深よりもずっと長い場合

この場合の波を, 浅水波 (shallow water wave)や長波 (long wave)と呼ぶ。このとき, 
$$
c \simeq \sqrt{gH}
$$

### 水深が波の波長がよりもずっと大きい場合

この場合の波を, 深水波 (deep water wave)とよぶ。波の波長を$\lambda$とすると, 
$$
c \simeq \sqrt{\frac{g \lambda}{2 \pi }}
$$


## 次元解析による浅水波と深水波の位相速度の導出

ここでは, 物理量の単位だけを用いて, 物理量の関数形を求める次元解析と呼ばれる手法を用いて, 位相速度を求める。

### 深水波

位相速度 $c \, \mathrm{[m \, s^{-1}]}$を左右する要因として考えられるものとして, 波長$\lambda \, \mathrm{[m]}$, 水の密度 $\rho \, \mathrm{[kg \, m^{-3}]}$, 重力加速度 $g \, \mathrm{[m \, s^{-2}]}$, 水深$h \, \mathrm{[m]}$が考えられる。まず, 波長に比べて水深が十分深い場合を考える。波のよる水の運動は, 水面から水底方向に遠ざかるにつれて減少することを考慮すると, 水深が十分深い場合, 海底の影響が水面波に及ぼす影響は, 水深が十分深くなると ($\lambda/h \to 0$) 無視できると考えられる。この場合水深$H$は, 位相速度に影響を与えないと仮定することが出来るので，位相速度$c$は, , 波長$\lambda \, \mathrm{[m]}$, 水の密度 $\rho \, \mathrm{[kg \, m^{-3}]}$, 重力加速度 $g \, \mathrm{[m \, s^{-2}]}$の組み合わせで書けるはずである。そのような組み合わせで，単位が$c$と同じ $\mathrm{[m \, s^{-1}]}$となるのは, $\sqrt{g \lambda}$しかない。したがって, このやり方で決められない単位の無い定数を$\beta$とすれば,
$$
c=\beta\sqrt{g \lambda}
$$
となる。

### 浅水波

さきほどの深水波の場合，水深が十分深くなると ($\lambda/h \to 0$)，水深$h$の影響が無視できると仮定した。浅水波の場合 ( $h/\lambda \to 0$), 波長$\lambda$が長いということのみが重要で具体的な波長$\lambda$の値そのものは位相速度に関係しないと仮定すると, 位相速度$c$は,  水深$h \, \mathrm{[m]}$, 水の密度 $\rho \, \mathrm{[kg \, m^{-3}]}$, 重力加速度 $g \, \mathrm{[m \, s^{-2}]}$の組み合わせで書けるはずである。そのような組み合わせで単位が$c$と同じ $\mathrm{[m \, s^{-1}]}$となるのは, $\sqrt{g h}$しかない。したがって, このやり方で決められない単位の無い定数を$\gamma$とすれば,
$$
c=\gamma\sqrt{g h}
$$
となる。

上記の方法は簡便であるが,

- 仮定を設ける際の根拠が明瞭でない

- 単位の無い定数の値を決めることができない

などいくつか難点もある。そこで, 以降物理法則に基づく方法について考える。



## ベルヌイの定理による浅水波の位相速度の導出

波の波長が水深に比べて非常に長い場合, 浅水波 (shallow water wave)や長波 (long wave)と呼ぶ。ここでは，

・ベルヌイの定理 (力学的エネルギー保存則)

・連続の式 (質量保存則)

を用いて，長波の位相速度を導く。長波の位相速度$c$は, 重力加速度を$g$, 水深を$h$とすると, $c=\sqrt{gh}$と表すことが出来る。この関係は, ベルヌイの定理を用いると以下のように比較的簡単に求めることが出来る。

一部の水面が高まった波動が通過すると, 波につれて流体が運動する。水深が浅いので, 運動はほとんど水平に起こると考えてよい。

波と一緒に動く座標系で見ると, 水は平均水面のところでは, 速さ$c$で走っている。

<img src="重力波_浅水波_長波_位相速度_SHALLOW.png" style="zoom: 33%;" />

**図1**

水の密度$\rho$は一定と近似する(その値はおおよそ$\mathrm{1000 \, [kg/m^3]}$である)。ある断面での流速を$c+dc$とし, そこでの水面の高さを$h+dh$とすれば, 連続の式は（図1参照）,
$$
\begin{eqnarray}
ch=(c+dc)(h+dh)
\tag{1}
\end{eqnarray}
$$
ゆえに高次の項を落とせば
$$
\begin{eqnarray}
hdc+dch=0
\tag{2}
\end{eqnarray}
$$

他方で, 水面が$dh$だけ高い断面では同じ水平面内で圧力が$\rho g dh$だけ高い. したがって, ベルヌーイの定理により
$$
\begin{eqnarray}
\frac{1}{2}(c+dc)^2=\frac{1}{2}c^2-gdh
\tag{3}
\end{eqnarray}
$$
ゆえに高次の項を落とせば
$$
\begin{eqnarray}
cdc+gdh=0
\tag{4}
\end{eqnarray}
$$
(2)と(4)が成り立つためには,
$$
\begin{eqnarray}
\frac{h}{c}=\frac{c}{g}
\tag{5}
\end{eqnarray}
$$
あるいは

$$
c^2=gh
$$

で, これは証明しようとした式に他ならない。

#### 練習

上記の手順を自分で繰り返し, $c^2=gh$を導け。





## 予備知識

先ほどは波の波長が水深に比べて非常に長い場合，という条件を課していた。この条件を外して一般的な波に拡張する場合には, 少し詳細な解析が必要になる。そこで必要となる予備知識のうち特に重要なものを下記にまとめておく。

### 正弦波

波形が三角関数で表現できる波のこと。$x$座標の方向にだけ伝わる空間1次元の波の場合, 時間を$t$で表すことにすると, 正弦波は
$$
\cos(kx-\omega t)
$$
と書き表すことが出来る。

#### 練習

$\lambda$を波の波長, $T$を波の周期とする。$k=2 \pi/ \lambda$, $\omega = 2 \pi / T$である。

(1) $x=0$で固定する。$t$が$0$のときの$\cos(kx-\omega t)$と$t=T$のときの$\cos(kx-\omega t)$の値が一致することを示せ。

このことは, $x=0$の点が$t=0$のとき波の峰であれば，$t=T$の時も波の峰であることを示す。即ちこの波の周期は$T$である。

(2) $t=0$で固定する。$x$が$0$のときの$\cos(kx-\omega t)$と$x=\lambda$のときの$\cos(kx-\omega t)$の値が一致することを示せ。

このことは, $x=0$が波の峰であれば，$x=\lambda$も波の峰であることを示す。即ちこの波の波長は$\lambda$である。



### 正弦波の複素表現

いま, $i=\sqrt{-1}$と定義し，複素数の指数関数$\exp(ix)$を,
$$
\exp (ix):=\cos x + i \sin(x)
$$
で定義する。この式をオイラーの公式と呼ぶことがある。$\mathrm{Re}$を複素数の実部をとる記号と定義する。このとき,
$$
\mathrm{Re}\exp(ix)=\cos x
$$
である。これらの記号を用いると, 
$$
\mathrm{Re}\exp{(i(kx-\omega t}))=\cos(kx-\omega t)
$$
と書くことができる。

三角関数は微分演算を行うたびに関数形が変わってしまい計算が煩雑になるため, 複素関数を使って上のように正弦波を表すことがある。

表記の煩雑さを避けるため, $\mathrm{Re}$の記号は省略されることが多い。このとき，計算結果が得られたら明記されていなくてもその実部をとる習慣となっている。



### 双曲線関数

**ハイパボリック・サイン**, $\sinh x$
$$
\sinh{x}:=\frac{1}{2}(e^x-e^{-x})
$$
**ハイパボリック・コサイン**, $\cosh x$
$$
\cosh{x}:=\frac{1}{2}(e^x+e^{-x})
$$
**ハイパボリック・タンジェント** , $\tanh {x}$ 
$$
\tanh{x}:=\frac{e^x-e^{-x}}{e^x+e^{-x}}
$$
**ハイパボリック・タンジェントのグラフ**

<img src="tanh.png" alt="tanh" style="zoom:50%;" />

**ハイパボリック・タンジェントのテイラー展開**
$$
\tanh{x} \simeq x - \frac{1}{3}x^3 + \cdots
$$

**ハイパボリック・タンジェントの漸近値**

$x$が十分大きいとき, $\tanh {x} \simeq 1$ (上のグラフ参照)

#### 練習

(1) ハイパボリック・サインとコサインの定義式を自分で紙にかけ

(2) ハイパボリック・タンジェントのグラフの概形を自分で紙にかけ。また，テイラー展開の式を自分で紙にかけ。

これらの関数は後の重要な個所で使用されるので，ここでなじんでおくと理解が容易になる。





### 一般の波

結論を先に示す。根拠は後程詳述する。$k$を波の波数とすると, 一般の水面波の位相速度$c$は,
$$
c=\sqrt{\frac{g}{k} \tanh(kh)}
$$
で与えられる。

### 波の波長が水深よりもずっと長い場合

この場合の波を, 浅水波 (shallow water wave)や長波 (long wave)と呼ぶ。このとき, $kh=2\pi h/\lambda \ll 1$であるので,  
$$
\tanh (kh) \simeq kh
$$
である。このとき,
$$
c \simeq \sqrt{\frac{g}{k}kh} = \sqrt{gh}
$$
となり, 先ほど導出した結果が再現される。浅水波は大気波動を調べる際にも頻繁に用いられる。

### 水深が波の波長がよりもずっと大きい場合

この場合の波を, 深水波 (deep water wave)とよぶ。このとき, $kh=2\pi h/\lambda \gg 1$であるので,  
$$
\tanh (kh) \simeq 1
$$
である。このとき,
$$
c \simeq \sqrt{\frac{g}{k}}= \sqrt{\frac{g \lambda}{2 \pi }}
$$



## 運動方程式と連続の式を用いた一般の水面波の位相速度の導出

ここでは，

・運動方程式 (運動量保存則)

・連続の式 (質量保存則)

を用いて, より一般的な形で水面波の位相速度を導出する。また, 波長に比べて水深が非常に深い場合と浅い場合の極限として, 深水波と浅水波の位相速度を導く。

### 設定

簡単のため, 水平方向$x$, 鉛直方向$z$ (上向き正) の鉛直2次元の波について考える。$y$方向の変化は無視する。密度$\rho$も一定とする。

### 支配方程式

運動方程式の$x$, $z$方向成分は,それぞれ
$$
\begin{eqnarray}
\frac{\partial u}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p}{\partial x} \tag{1}\\
\frac{\partial w}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p}{\partial z}-\rho g \tag{2}
\end{eqnarray}
$$
である。ここで, 波にともなって生じる流れは小さいと仮定して,　左辺の加速度項を
$$
\begin{eqnarray}
\frac{Du}{Dt}&:=&\frac{\partial }{\partial t}+
u\frac{\partial }{\partial x}+w\frac{\partial }{\partial z}\\
&\simeq & \frac{\partial }{\partial t}
\end{eqnarray}
$$
と近似している。

圧力$p$を静水圧平衡している分$p_0$とそれからの偏差$p'$にわける。即ち,
$$
p=p_0(z)+p'(x,z)　\tag{3}
$$
とする。以下に示すように静水圧平衡とは鉛直方向の力のつり合いであるので, $p_0$は$z$のみの関数となる。重力加速度を$g$で表すことにすると, 静水圧平衡の式は,
$$
\frac{\partial p_0(z)}{\partial z}=-\rho g \tag{4}
$$
と表される。(4)を(2)の右辺に代入すると, 
$$
\begin{eqnarray}

-\frac{1}{\rho}\frac{\partial p}{\partial z}-\rho g
= -\frac{1}{\rho}\frac{\partial p_0(z)}{\partial z}
-\frac{1}{\rho}\frac{\partial p'}{\partial z} -\rho g
\end{eqnarray}
$$
となるが, これは (4)より
$$
\begin{eqnarray}
-\frac{1}{\rho}\frac{\partial p}{\partial z}-\rho g
=
-\frac{1}{\rho}\frac{\partial p'}{\partial z} 
\end{eqnarray}
$$
となる。また, $p=p(z)$なので,
$$
\begin{eqnarray}
-\frac{1}{\rho}\frac{\partial p}{\partial x}
=
-\frac{1}{\rho}\frac{\partial p'}{\partial x} 
\end{eqnarray}
$$
以上より, 運動方程式は
$$
\begin{eqnarray}
\frac{\partial u}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p'}{\partial x} \tag{5}\\
\frac{\partial w}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p'}{\partial z} \tag{6}
\end{eqnarray}
$$
となる。

密度が一定なので，連続の式は
$$
\begin{eqnarray}
\frac{\partial u}{\partial x}+\frac{\partial w}{\partial z}=0\\
\end{eqnarray}  \tag{7}
$$
(発散＝0)となる。

#### まとめ

上述の仮定のもとで, 波を表わす方程式の組 (支配方程式)は，以下のようになる。
$$
\begin{eqnarray}
\frac{\partial u}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p'}{\partial x} \tag{5}\\
\frac{\partial w}{\partial t}&=&-\frac{1}{\rho}\frac{\partial p'}{\partial z} \tag{6}
\end{eqnarray}
$$

$$
\begin{eqnarray}
\frac{\partial u}{\partial x}+\frac{\partial w}{\partial z}=0\\
\end{eqnarray}  \tag{7}
$$





### 境界条件

波が無い状態の平均水深を$H$とする。水面の高さの$H$からの偏差を$h$で表す。このとき, $|h| \ll H$を仮定する。これが仮定された波のことを**微小振幅波**と呼ぶことがある。

いま, 水底と海面が満足されなければならない条件を求める。底面($z=0$)では, 鉛直速度はゼロだから,
$$
w=0 \quad at \quad z=0
\tag{8}
$$
一方, 水面付近 ($z\simeq H$)では, 水面の高さの偏差に応じた圧力偏差が生じるので,
$$
p'=\rho g h
$$
が成り立つ。両辺に$D/Dt$を演算すると,
$$
\begin{eqnarray}
\frac{Dp'}{Dt}&=& \rho g \frac{Dh}{Dt}
\end{eqnarray}
$$
となる。次に，水面の高さの時間変化率 ($Dh/Dt$) は, 水面における鉛直流速 ($w|_{z=H+h}$) に等しいはずであるとする,
$$
\begin{eqnarray}
\frac{Dh}{Dt}=w\bigg|_{z=H+h} \simeq w \bigg|_{z=H}
\end{eqnarray}
$$


という関係を考慮すると,
$$
\begin{eqnarray}
\frac{Dp'}{Dt}&=& \rho g w \bigg|_{z=H}
\end{eqnarray}
$$
と表せる。波の振幅が小さい ($|h| \ll H$)と仮定した場合, 波に伴う流れも小さいので,
$$
\begin{eqnarray}
\frac{Dp'}{Dt}&:=&\frac{\partial p'}{\partial t}+
u\frac{\partial p'}{\partial x}+w\frac{\partial p'}{\partial z}\\
&\simeq & \frac{\partial p'}{\partial t}
\end{eqnarray}
$$
と近似することができる。これより, 水面における境界条件
$$
\begin{eqnarray}
\frac{\partial p'}{\partial t}&=& \rho g w 
\quad at \quad z=H\tag{9}
\end{eqnarray}
$$
が得られる。



### 位相速度

#### 支配方程式の整理

支配方程式と境界条件を使って, 波の位相速度を求める。(5)を$z$で偏微分した式から, (6)を$x$で偏微分した式を引くことにより$p'$を消去すると下記の式が得られる。
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\bigg(
\frac{\partial w}{\partial x}-\frac{\partial u}{\partial z}
\bigg)&=&0
\end{eqnarray}
$$
この式は, $x-z$平面内での渦度 ($\frac{\partial w}{\partial x}-\frac{\partial u}{\partial z}$) が時間変化しないことを示している。この式を$x$で偏微分すると,
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\bigg(
\frac{\partial^2 w}{\partial x^2}-\frac{\partial}{\partial x}\frac{\partial u}{\partial z}
\bigg)&=&0
\end{eqnarray}
$$
となる。連続の式(7)を用いて$u$を消去すると,
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\bigg(
\frac{\partial^2 w}{\partial x^2}+
\frac{\partial^2 w}{\partial z^2}
\bigg)&=&0 \tag{10}
\end{eqnarray}
$$
が得られる。

次に水面における境界条件(9)を$x$で偏微分すると,
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\frac{\partial p'}{\partial x}&=& \rho g \frac{\partial w}{\partial x} 
\quad at \quad z=H
\end{eqnarray}
$$
となり, 運動方程式の$x$方向成分(5)を用いて$p$を消去すると,
$$
\begin{eqnarray}
-\frac{\partial^2 u}{\partial t^2}&=& g \frac{\partial w}{\partial x} 
\quad at \quad z=H
\end{eqnarray}
$$
が得られる。さらに$x$で偏微分すると,
$$
\begin{eqnarray}
-\frac{\partial^2 }{\partial t^2}
\frac{\partial u}{\partial x}
&=& g \frac{\partial^2 w}{\partial x^2} 
\quad at \quad z=H
\end{eqnarray}
$$
連続の式(7)を用いて$u$を消去すると, 
$$
\begin{eqnarray}
\frac{\partial^2 }{\partial t^2}
\frac{\partial w}{\partial z}
&=& g \frac{\partial^2 w}{\partial x^2} 
\quad at \quad z=H  \tag{11}
\end{eqnarray}
$$
が得られる。支配方程式より, 下記のような$w$のみの方程式と境界条件が得られた。

#### 整理された方程式系

**渦度方程式**
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\bigg(
\frac{\partial^2 w}{\partial x^2}+
\frac{\partial^2 w}{\partial z^2}
\bigg)&=&0 \tag{10}
\end{eqnarray}
$$
**境界条件**

底面
$$
w=0 \quad at \quad z=0
\tag{8}
$$
水面
$$
\begin{eqnarray}
\frac{\partial^2 }{\partial t^2}
\frac{\partial w}{\partial z}
&=& g \frac{\partial^2 w}{\partial x^2} 
\quad at \quad z=H  \tag{11}
\end{eqnarray}
$$
次にこれを用いて, 波の空間構造を調べる。



#### 波の空間構造

東西方向に伝播する波を仮定する。このとき, $w$は,
$$
w=  \hat{w}(z) \exp [i(kw-\omega t)] 
\tag{12}
$$
である。$\hat{w}(z)$は複素数であることに注意する。また，複素数で波を表現した場合, 上述のように計算結果がでたら，最後に実部をとることに注意する。

指数関数の微分公式は, $a$が定数の時
$$
\frac{d}{dx}\exp{ax}=a \exp{ax}
$$
である。これを用いて以下の計算を行っていく。

(12)を(10)に代入すると,
$$
-i\omega \bigg(-k^2+\frac{d^2}{dz^2} \bigg)\hat{w}=0
$$
となるから,
$$
\frac{d^2}{dz^2}\hat{w}=k^2\hat{w}
\tag{13}
$$
がえられる。

一方, 底面での境界条件(8)は,
$$
\hat{w}=0 \quad at \quad z=0
\tag{14}
$$
水面での境界条件(11)は,
$$
\omega^2\frac{d \hat{w}}{dz}=gk^2 \hat{w} \quad at \quad z = H
\tag{15}
$$
となる。もとの支配方程式(5)-(7)はかなり複雑な連立偏微分方程式だった。しかし, 以上の演算を行うことにより, 境界条件(14), (15)のもとでただ1つの変数$\hat{w}$に関する常微分方程式(13)を解くという簡単な問題に帰着された。

##### 常微分方程式の解

(13)の式の形を観察してみると，$\hat{w}$は，

- $\hat{w}$を$z$で2回微分すると元の関数$\hat{w}$にもどる
- 2回微分しても正負の符号は変わらない

という関係を満たす関数であることがわかる。このことから, 指数関数の形の解が考えられるので, $C_1$, $C_2$を定数として,
$$
\hat{w}=C_1e^{kz}+C_2e^{-kz} \tag{16}
$$
とおく。また, 底面での境界条件(14)より,
$$
\begin{eqnarray}
\hat{w}=C_1+C_2=0 \\
\therefore \quad C_2=-C_1 \tag{17}
\end{eqnarray}
$$
が得られる。(17)を(16)に代入して,
$$
\hat{w}=C_1(e^{kz}-e^{-kz})=2C_1\sinh(kz)
$$
が得られる。これを水面の境界条件 (15)に代入すると,
$$
\omega^2k\cosh(kH)=gk^2\sinh(kH)
$$

$$
\therefore \quad \omega^2=gk \tanh(kH) \tag{18}
$$

この式を水面波の**分散関係式**と呼ぶ。(18)は波の周波数 (周期) が波数 (波長)に依存するということを意味する。これは音波や真空中の電磁波にはない性質で, このような関係があることが水の波のふるまいを豊かなものにしている。ちなみにもし音波に分散性があると, 声の高い人と低い人で音速が変わってしまうので, コミュニケーションに支障が生じるということがありうる。



### まとめ: 水面波の位相速度

#### 一般の場合

冒頭で説明したように位相速度$c$は, $c=\omega /k$で与えられるので, (18)より水面波の位相速度は
$$
c=\sqrt{\frac{g}{k}\tanh(kH)}
$$
となる。



#### 波の波長が水深よりもずっと長い場合

この場合の波を, 浅水波 (shallow water wave)や長波 (long wave)と呼ぶ。このとき, $kh=2\pi h/\lambda \ll 1$であるので,  
$$
\tanh (kh) \simeq kh
$$
である。このとき,
$$
c \simeq \sqrt{\frac{g}{k}kh} = \sqrt{gh}
$$

#### 水深が波の波長がよりもずっと大きい場合

この場合の波を, 深水波 (deep water wave)とよぶ。このとき, $kh=2\pi h/\lambda \gg 1$であるので,  
$$
\tanh (kh) \simeq 1
$$
である。このとき,
$$
c \simeq \sqrt{\frac{g}{k}}= \sqrt{\frac{g \lambda}{2 \pi }}
$$

# LARGE_SCALE_DYNAMICS_NOTE

C:\Users\foome\lab\GFD\AOFD\30.ATMOSPHERE\LARGE_SCALE

[[_TOC_]]

## 5.1.6 定常ロスビー波の鉛直構造

p. 291

山岳に西風が当たり続けて，定常状態になった場合のロスビー波の鉛直構造について考える。

(5.1.21)で$\sigma=0$とすると，
$$
m^2=\frac{N^2}{f_0^2} \bigg\{\frac{\beta}{U}-(k^2+l^2)-\frac{f_0^2}{4H^2N^2} \bigg\}
$$
(James, p.193) 鉛直方向に波動が形成される（波が鉛直方向に伝播できる) のは$m$が実数 ($m^2>0$​)のときである。これより，

$U<0$ (東風) のとき, $m$は虚数

$U \gg 1$だと$\frac{\beta}{U}-(k^2+l^2)-\frac{f_0^2}{4H^2N^2} < 0$で$m$は虚数

であるから、

① 波数$k$の小さな波 (波長が長い波)の方が鉛直伝播しやすい。

②$U$が西風 ($U>0$)でなければ鉛直伝播できないが，西風が強すぎても伝播できない。

となる。

$m^2>0$となるための$k$​の条件

$l=0$とすると，
$$
\begin{eqnarray}
 \frac{\beta}{U}-\frac{f_0^2}{4H^2N^2} > k^2
\end{eqnarray}
$$

$$
\begin{eqnarray}
\therefore \quad k^2 < K_S^2 = \frac{\beta}{U}-\frac{f_0^2}{4H^2N^2}
\end{eqnarray}
$$
$k \lesssim 2$の波長の長いロスビー波だけが成層圏に伝播できる。短いものは対流圏に捕捉される。



## Takaya & Nakamura Flux

高谷 p. 20
$$
\begin{eqnarray}
\mathbf{F}_s=\frac{1}{2}
\begin{bmatrix}
\psi_x'^2 - \psi'\psi'_{xx}  \\
\psi_x'\psi_y' - \psi'\psi'_{xy}  \\
\frac{f^2}{S^2}(\psi_x'\psi_p' - \psi'\psi'_{xp})  \\
\end{bmatrix}
\end{eqnarray}
$$

<img src="image-20240225120359727.png" alt="image-20240225120359727" style="zoom: 33%;" />

$\mathbf{F}_s$の$x$成分 $\psi_x'^2$$(=v'^2)$

点Eにおいて$v'^2>0$であれば，B点においては南向きの，D点においては北向きの地衡風を作り出そうとする。地衡風は非発散なので，A点では東向きの，C点では西向きの加速が存在する。このことはC点からA点に向かって西向きの地衡風運動量が運び出されていると解釈できる (点Cから点Aへの二次の西風運動量の輸送に対応する)。運動量輸送は東向きの群速度とは逆である。



$\mathbf{F}_s$の$y$成分$\psi_x'\psi_y'(=-u'v')$

南北方向の群速度とは逆向きの，二次の西風運動量の南北方向の輸送を表してる。
# 9.7 A KINEMATIC VIEW OF BAROCLINIC INSTABILITY

2層モデルを用いた運動学的考察により傾圧不安定のメカニズムについて考える。2層モデルはイーディー問題における上端と下端の波を扱っていることに相当する。まず2つの波をそれぞれ別個に扱い, その後2つの波の相互作用について考える。

## 9.7.1 The Two-layer Model

[[_TOC_]]

### A simple dynamical model

ここでは2層モデルにおける準地衡渦度方程式を用いる。順圧, 傾圧の流線関数をそれぞれ,
$$
\psi:= \frac{1}{2}(\psi_1+\psi_2), \quad \tau=\frac{1}{2}(\psi_1-\psi_2)
$$
と定義する。鉛直平均は0となるが, 一定の鉛直シア―をもつ次のような一般流を考える。
$$
\psi = 0 + \psi ', \quad \tau=-Uy + \tau '
$$
$\beta$を0としたとき, (9.107)は,
$$
\begin{eqnarray}
\frac{\partial}{\partial t}\nabla^2\psi '&=&-U\frac{\partial}{\partial x}\nabla^2 \tau '
\tag{9.128a} \\

\frac{\partial}{\partial t}(\nabla^2-k_d^2)\tau '&=&-U\frac{\partial}{\partial x}(\nabla^2 +k_d^2)\psi '
\tag{9.128b} \\
\end{eqnarray}
$$
となる。

いま, 簡単のため南北($y$)方向の依存性を無視し, 以下のような$x$方向への進行波解
$$
(\psi ', \tau ')=\mathrm{Re} \, (\tilde{\psi}, \tilde{\tau}) \, \exp [ik(x-ct)] \\
c=ic_i+c_r
$$
を仮定する ($c_i$と$c_r$はそれぞれ$c$の虚部と実部でどちらも実数)。これを(9.128)に代入すると,
$$
\begin{eqnarray}
-U \tilde{\tau} + c \tilde{\psi} &=& 0 \tag{9.129a} \\
c(K^2 + k_d^2)\tilde{\tau} - U (K^2 - k_d^2)&=&0 \tag{9.129b}

\end{eqnarray}
$$
が得られる (いま$y$方向の依存性を無視しているので$K^2=k^2$)。

これが$(\psi ', \tau ')=(0,0)$以外の解を持つためには,
$$
\begin{vmatrix}
 -U            & c \\
c(K^2+k_d^2)   & -U(K^2-k_d^2)  \\
\end{vmatrix}
=0
$$
でなければならない。これより,
$$
U^2(K^2-k_d^2)-c^2(K^2+k_d^2)=0
$$
となるが, これを$c$について解くと,
$$
c=\pm U \bigg( \frac{K^2-k_d^2}{K^2+k_d^2} \bigg)^{\frac{1}{2}}
$$
となる。$K^2 -k_d^2 < 0$のとき$c$は虚数となり, さらに$c$の実部は0となる。よって, $c_i$を実数として$c=i c_i$とおくと,
$$
\exp [ik(x-ct)]=\exp(ikx)\exp(-ikct)=\exp(ikx)\exp(k c_i t)
$$
となり, $c_i>0$であれば, 擾乱の振幅は時間と共に増大する (擾乱は不安定となる)。

(9.129)より不安定モードについて,
$$
\tilde{\tau}=i \frac{c_i}{U}\tilde{\psi}=\exp\bigg(i\frac{\pi}{2}\bigg)\frac{c_i}{U}\tilde{\psi}
$$
となるので, $c_i>0$であれば, $\tilde{\tau}$は$\tilde{\psi}$よりも$\frac{\pi}{2}$だけ位相が遅れている。一方, $c_i<0$ (振幅が減衰するモード)については, 
$$
\tilde{\tau}=i \frac{c_i}{U}\tilde{\psi}=\exp\bigg(i\frac{-\pi}{2}\bigg)
\frac{
\overbrace{-c_i}^{>0}}{U}\tilde{\psi}
$$
となり,  $\tilde{\tau}$は$\tilde{\psi}$よりも$\frac{\pi}{2}$だけ位相が進んでいる。

いま, 風速ベクトルの鉛直平均の南北成分を$v$とすると, $v=\partial \psi/\partial x$である。よって, 発達する擾乱については,
$$
\tilde{v}=\tilde{\tau}\frac{kU}{c_i} \tag{9.132}
$$
となる。$\tau$は層厚に相当する量なので気温に比例する。よって, (9.132)は南風の領域は周囲より高温であり, 北風の領域は相対的に低温であることを意味している ($v$と$\tau$の位相が一致)。

一方, 中立な擾乱については, $\tau=c_r \tilde{\psi}/U$, $\tilde{v}=i k \tilde{\tau}U/c_r$となるので, $\tilde{v}$と$\tilde{\tau}$の位相は$\pi/2$だけずれている。以上まとめると,

- 発達する擾乱は熱を北向きに輸送する（高温気塊を北向きに, 低温気塊を南向きに）
- 減衰する擾乱は熱を南向きに輸送する
- 中立な擾乱の南北熱輸送は0

となる。

# 9.7 A KINEMATIC VIEW OF BAROCLINIC INSTABILITY

## 9.7.1 The Two-layer Model

[[_TOC_]]

### Further simplification to the two-layer model

ここではさらにモデルを単純化させるため, 擾乱の波長が変形半径よりも十分長い波 ($K^2 \ll k^2_d$) について考える。 このとき, (9.128)は,
$$
\begin{eqnarray}
\frac{\partial}{\partial t}\nabla^2\psi '&=&-U\frac{\partial}{\partial x}\nabla^2 \tau '
\tag{9.133a} \\

\frac{\partial}{\partial t}\tau '&=&U\frac{\partial}{\partial x}\psi '
\tag{9.133b} \\
\end{eqnarray}
$$
のように近似できる。このとき, $x$方向への進行波解が解を持つための条件式(9.129b)
$$
c(K^2 + k_d^2)\tilde{\tau} - U (K^2 - k_d^2)=0 \tag{9.129b}
$$
において, $K^2\to0$とすると, $c=\pm i U$となることから, 十分波長の長い波は常に不安定となる。

このことをさらに詳しく見てみる。(9.133b)より, 一般流が東向き $U>0$で, 擾乱の順圧成分が北向き ($v=\partial \psi / \partial x > 0$)であれば, $\tau$の正偏差が生じる (気温が上昇する)。また, (9.129a) $c\psi = U\tau$を思い出すと, 
$$
\tau=\frac{c}{U}\psi=\frac{\pm i U}{U}\psi = \pm i \psi 
$$
なので, 擾乱の持つ順圧流と傾圧流は位相が$\pi/2$ずれる。よって, (9.133a)より, 平均流のシア― ($U$)による$\tau$の移流によって$\tau$が増加する。

これとは逆に, 変形半径に対し, 波長が非常に短い波 ($K^2 \gg k_d^2$)については, (9.128)より,
$$
\begin{eqnarray}
\frac{\partial}{\partial t}\nabla^2\psi '&=&-U\frac{\partial}{\partial x}\nabla^2 \tau '
\tag{9.134a} \\

\frac{\partial}{\partial t}\nabla^2 \tau '&=&U\frac{\partial}{\partial x}\nabla^2 \psi '
\tag{9.134b} \\
\end{eqnarray}
$$
(2番目の式の$\nabla^2$に注意)となる。これを各層についての方程式に戻すと,
$$
\begin{eqnarray}
\frac{\partial}{\partial t}\nabla^2\psi_1 &=&-U\frac{\partial}{\partial x}\nabla^2 \psi_1
\tag{9.134a} \\

\frac{\partial}{\partial t}\nabla^2 \psi_2 &=&-U\frac{\partial}{\partial x}\nabla^2 \psi_2
\tag{9.134b} \\
\end{eqnarray}
$$
となり, 各層は全く独立に運動することが分かる。

このことを踏まえ, $\beta$と平均流のシア―が存在する二層流体中をそれぞれの層において独立に伝播する波を考える。その後でそれらの波の相互作用を考察することにより傾圧不安定のメカニズムに関する一つの解釈を得る。

まず, (9.107)
$$
\begin{eqnarray}
\bigg(\frac{\partial }{\partial t}+U\frac{\partial }{\partial x}\bigg)
\bigg[\nabla^2 \psi_1' + \frac{k_d^2}{2}(\psi_2'-\psi_1') \bigg]
+\frac{\partial \psi_1'}{\partial x}(\beta+k_d^2 U)=0 \tag{9.107a} \\

\bigg(\frac{\partial }{\partial t}-U\frac{\partial }{\partial x}\bigg)
\bigg[\nabla^2 \psi_2' + \frac{k_d^2}{2}(\psi_1'-\psi_2') \bigg]
+\frac{\partial \psi_2'}{\partial x}(\beta+k_d^2 U)=0 \tag{9.107b}

\end{eqnarray}
$$
において, ストレッチング項 ($\frac{k_d^2}{2}(\psi_i'-\psi_j')$)を無視する。このとき, $\partial Q_1/\partial y=\beta+k_d^2 U$, $\partial Q_2/\partial y=\beta-k_d^2 U$として, (9.107)は,
$$
\begin{eqnarray}
\bigg(\frac{\partial }{\partial t}+U\frac{\partial }{\partial x}\bigg)
\nabla^2 \psi_1'
+\frac{\partial \psi_1'}{\partial x}\frac{\partial Q_1}{\partial y}=0 \tag{9.136a} \\

\bigg(\frac{\partial }{\partial t}-U\frac{\partial }{\partial x}\bigg)
\nabla^2 \psi_2'
+\frac{\partial \psi_2'}{\partial x}\frac{\partial Q_2}{\partial y}=0 \tag{9.136b}

\end{eqnarray}
$$
となる。(9.108) $\tilde{\psi_i}=\mathrm {Re} \, \tilde{\psi_i}\,e^{i(k(x-ct)} e^{ily}$のような平面波解が存在する場合, これを(9.136)に代入することにより, 次の分散関係式が得られる ($K^2=k^2+l^2$)。
$$
c_1=U-\frac{\partial_y Q_1}{K^2}, \quad c_2=-U-\frac{\partial _y Q_2}{K^2} \tag{9.137a,b}\\
$$
上層の波の位相速度$c_1$は, 上層の東向きの一般流の流速($U$)と, 西向きに早く伝播するロスビー波の位相速度の和となっている。下層の波の位相速度$c_2$は, 下層の西向きの一般流の流速 ($-U$)とゆっくり西向きに伝播するロスビー波の位相速度の和となっている。

不安定が起こるためにはこれら2つの波の位相速度が一致する必要があるが, (9.137a, b)より,
$$
c_1=c_2=\frac{\beta}{k_d^2}
$$
のとき, この条件が満足される。これは, $k^2_d>K^2$が不安定の必要条件であることを示した, p. 362の(9.125)のHigh-wavenumber cut-offに対応している。波長の短い波 (波数の大きい波)は, 相互作用することなくそれぞれ独立に伝播する。

Fig. 9.17のように, $\tau_1=(\psi_1-\psi_2)/2$ (層厚)と$v (= \partial \psi / \partial x)$が同位相にとき, 暖気の北上と寒気の南下がおこる。熱力学エネルギー保存則 (温度の移流方程式)は,
$$
\frac{\partial \overline{\tau}}{\partial y}
=\frac{1}{2}\frac{\partial }{\partial y}(\psi_1-\psi_2)
=\frac{1}{2}\frac{\partial }{\partial y}(-U_1y-U_1y)=-U_1
$$
より,
$$
\frac{\partial \tau}{\partial t}=-v \frac{\partial \overline{\tau}}{\partial y}=v U
\tag{9.138}
$$
となるが, この式は左辺の温度上昇率が擾乱のもつ流速の南北成分$v$と一般流の流速$U$に比例することを示している。



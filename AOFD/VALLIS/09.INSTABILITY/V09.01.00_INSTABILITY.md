Vallis, G. K., 2017. Atmospheric and Oceanic Fluid Dynamics: Fundamentals and Large-scale Circulation, 2nd edn. Cambridge University Press. 946 pp.

# Chap. 9 Barotropic & Baroclinic Instability

一般流に重なった微小振幅擾乱が時間の経過とともに成長する場合，流れは不安定であるという。地球上の大規模な定常流は不安定であることが知られている。

[[_TOC_]]

# 9.1 KELVIN-HELMHOLTZ INSTABILITY

p. 335

流体力学における不安定性解析を紹介するため, 取り扱いやすく応用用も重要な現象であるケルビン―ヘルムホルツ不安定について説明する。

いま一般流としてFig. 9.1のような2層流体における流れを考える。密度はどちらの層も同じとする。2つの層の一般流の境界は$y=0$に存在し, $y>0$での一般流の流速は$+U$, $y<0$での一般流の流速は$-U$とする。



Fig. 9.1 一般流のプロファイル。$y=0$で一般流の流速が不連続となっている。密度は一定とする。



流向に直交する方向に流速の大きさが変化する流れをシアー流という。一般流がシアーを持つとき，流れが不安定となる時の条件を求める。いま,

$U$: 一般流の$x$成分

$u'$: 擾乱の$x$成分

$p'$: 圧力の擾乱

とする。物理量の実際の値, 平均, 偏差との関係を示すと, 次のようになる。
$$
\begin{eqnarray}
\underbrace{u}_{実際の値}=\underbrace{U}_{平均}+\underbrace{u'}_{偏差}
\end{eqnarray}
$$


$y>0$における擾乱に関する方程式は,
$$
\begin{eqnarray}
\frac{\partial \mathbf{u'}}{\partial t} + U\frac{\partial \mathbf{u'}}{\partial x}=-\nabla p', \quad \nabla \cdot \mathbf{u'}=0
\tag{9.1a,b}
\end{eqnarray}
$$
である。$y<0$における方程式は上の式において, $U$を$-U$に置き換えればよい。

$x$方向に周期性を持つ解を仮定すると, 
$$
\phi '(x,y,t)=\mathrm{Re}\sum_k \tilde{\phi_k}(y)\exp[ik(x-ct)]
\tag{9.2}
$$
と表すことができる。ここで, $\phi$は$\mathbf{u}'$, $p'$などの物理量を表し, $\mathrm{Re}$は, 実部をとることを意味する。(9.1a)は$\mathbf{u}'$に関して線形なので, (9.2)のフーリエ・モード$\tilde{\phi_k}(y)\exp[ik(x-ct)]$は相互作用しないため, ここでは単一のモードについて考える (どれか一つの$k$について考える)。

(9.1a)の発散をとると,　(9.1b)$\nabla \cdot \mathbf{u'}=0$より,
$$
\begin{eqnarray}
\frac{\partial}{\partial t} \, \underbrace{\nabla \cdot\mathbf{u'}}_{=0} + U\frac{\partial}{\partial x} \, \underbrace{\nabla \cdot\mathbf{u'}}_{=0}
&=&-\nabla^2 p'
\\
\end{eqnarray}
$$

$$
\therefore \quad \nabla^2 p'=0 \tag{9.3}
$$

(9.3)のラプラス方程式は, 次の$x$, $y$に関する調和関数
$$
p'=\left\{
   \begin{align*}
      \mathrm{Re} \, \tilde{p_1}\exp(ikx-ky)e^{\sigma t} \quad (y>0)\\
      \mathrm{Re} \, \tilde{p_2}\exp(ikx+ky)e^{\sigma t} \quad (y<0)
   \end{align*}
\right.
\tag{9.4}
$$
時間的に成長するモードが存在していることを仮定し, $\sigma=-ikc$とおく。$\sigma$が正の実数のとき$p'$は不安定となり, $\sigma$が虚数のとき振動解となる。(9.1a)を用いて, 分散関係式を得る。
$$
\begin{eqnarray}
\frac{\partial v'_1}{\partial t} + U\frac{\partial v'_1}{\partial x}=-\frac{\partial p'_1}{\partial y} 
\tag{9.1aの$y$成分}
\end{eqnarray}
$$
に, $v_1=\tilde{v}_1\exp(ikx+\sigma t)$, $p'=\tilde{p}_1\exp[ikx-ly]e^{\sigma t}$を代入すると, 
$$
\sigma \tilde{v}_1+U_i\tilde{v}_1=-\tilde{p}_1(-k)
$$
すなわち,
$$
(\sigma + ikU)=k\tilde{p}_1
\tag{9.6}
$$
が得られる。2つの流体層の境界面上での$v'$は境界面の変位$\eta'$のラグランジュ微分に等しいから,
$$
v_1=\frac{\partial \eta}{\partial t}+U\frac{\partial \eta'}{\partial x}
$$
である。$\eta'$が$y=0$からそれほど大きく離れることがなければ, 上の式を$y=+0$における境界条件として使うことができる。

先ほど導入したフーリエモードを用いると, 上の式は,
$$
\tilde{v}_1=(\sigma+ikU)\tilde{\eta}
\tag{9.8}
$$
と書ける。これを用いると, (9.6)$(\sigma + ikU)=k\tilde{p}_1$は, 
$$
(\sigma+ikU)^2\tilde{\eta}=k\tilde{p}_1
\tag{9.9}
$$
となる。同様にして, $y=-0$において,
$$
(\sigma-ikU)^2\tilde{\eta}=-k\tilde{p}_2
\tag{9.10}
$$
が得られる。

圧力が不連続の場合圧力傾度力が無限大となってしまう。したがって圧力は境界面上で連続である必要がある。このことから, $y=0$において$p_1=p_2$でなければならない。

(9.9) $(\sigma+ikU)^2\tilde{\eta}=k\tilde{p}_1$, (9.10) $(\sigma-ikU)^2\tilde{\eta}=-k\tilde{p}_2$より,
$$
\begin{eqnarray}
(\sigma+ikU)^2\tilde{\eta}&=&-(\sigma-ikU)^2\tilde{\eta} \\
\sigma^2+2ikU-kU^2&=&-(\sigma^2-2ikU-kU^2) \\
\sigma^2&=&k^2U^2
\end{eqnarray}
$$
よって,
$$
\therefore \quad \sigma=\pm kU
$$
となる。$\sigma > 0$のとき, $p'=p_i\exp(\cdots)e^{\sigma t}$は成長する (時間的に振幅が増大する)。$v'$も同様である。**$\sigma > 0$のとき擾乱は不安定**であると言い, **$\sigma < 0$のとき安定**であるという。

# 9.3 NECESSARY CONDITIONS FOR INSTABILITY

## 9.3.1 Rayleigh's Criterion

[[_TOC_]]

一般的な$U (y)$のプロファイルに関して, 波数と成長率の関係や最大成長率を持つ不安定波の空間パターンを解析的に求めるのは困難である。そこで, ここでは傾圧波が不安定となる必要条件 (言い換えれば安定となる十分条件)について考える。

以前導出した平衡流の不安定条件を記述するレイリー・クオ方程式は
$$
\tilde{\psi}_{yy} - k^2 \tilde{\psi} + \frac{(\beta - U_{yy})}{U-c}\tilde{\psi} = 0 
\tag{9.11}
$$
であった。この方程式の両辺に$\tilde{\psi}$の複素共役$\tilde{\psi}^{*}$を両辺にかけて, $y=y_1$から$y_2$の範囲で$y$について積分すると,
$$
\int_{y_1}^{y_2} \bigg(\bigg| \frac{\partial \tilde{\psi}}{\partial y}\bigg|^2 
+ k^2 \big| \tilde{\psi} \big|^2 \bigg)dy

- \int_{y_1}^{y_2} \frac{\beta - U_{yy}}{U-c} \big| \tilde{\psi} \big|^2 dy =0 
\tag{9.49}
$$
となる。ここで左辺第一項は部分積分を用いて, 
$$
\int_{y_1}^{y_2} \tilde{\psi}_{yy}\tilde{\psi}^{*}dy

=\bigg[ \psi_y \tilde{\psi}^{*} \bigg]^{y_1}_{y_2} - \int_{y_1}^{y_2} \psi_y \tilde{\psi}^{*} dy \\

= - \int_{y_1}^{y_2}\bigg| \frac{\partial \tilde{\psi}}{\partial y}\bigg|^2 dy
$$
のようにすると求めることが出来る（$y=y_1$と $y=y_2$で$\tilde{\psi}^{*}=0$とした）。

(9.49)の最初の積分の値は実数になる。2番目の積分に現れる$1/(U-c)$の項を有理化すると, $c$の実部と虚部をそれぞれ, $c_r$と$c_i$として,
$$
\frac{1}{U-c}=\frac{U}{|U-c|^2}-\frac{c_r+c_i}{|U-c|^2}
$$
となる。よって, (9.49)が恒等的に0になるためには,
$$
c_i \int_{y_1}^{y_2} \frac{\beta - U_{yy}}{|U-c|^2} \big| \tilde{\psi} \big|^2 dy =0
$$
が必要である。波が不安定であるとき, $c_i\ne0$であることが必要なので, 
$$
\int_{y_1}^{y_2} \frac{\beta - U_{yy}}{|U-c|^2} \big| \tilde{\psi} \big|^2 dy =0
$$
が必要となる。上式より, 波が**不安定**となるための**必要条件**は,

​                                        $\beta - U_{yy}$が$y=y_1$から$y=y_2$の範囲で**符号を変える**こと

である。この命題の対偶をとると, 波が**安定**であるときの**十分条件**は,

​                                        $\beta - U_{yy}$が$y=y_1$から$y=y_2$の範囲で**0とならない**こと

である。これをレイリー・クオの変曲点定理と呼ぶ。



### A more general derivation

$x$軸に平行な一般流$U(y)$が与えられたとき, 線形化された渦度方程式は,
$$
\frac{\partial \zeta}{\partial t} + U\frac{\partial \zeta}{\partial x} + v \bigg( \frac{\partial \Zeta}{\partial y} + \beta \bigg)=0 \tag{9.51}
$$
となる（擾乱を表すプライム$'$は省略している）。(9.51)に$\zeta /(\beta + \Zeta_y)$を掛けると,
$$
\frac{\partial }{\partial t}\bigg( \frac{\zeta^2}{\beta + \Zeta_y}\bigg)
+ \frac{U}{\beta + \Zeta_y} \frac{\partial \zeta^2}{\partial x} + 2 v \zeta=0
$$
が得られる。この式を$x$に関して積分すると, $x$に関する周期境界条件のもとで,
$$
\frac{\partial }{\partial t}\int \frac{\zeta^2}{\beta + \Zeta_y} dx
= - 2 \int v \zeta \, dx=0 \tag{9.53}
$$
が得られる。

　連続の式$\frac{\partial u}{\partial x} + \frac{\partial v}{\partial y}=0$より, 
$$
v\frac{\partial v}{\partial y}=-v\frac{\partial u}{\partial x}
$$
であるから, これを用いると, 渦度フラックスの$y$成分$v \zeta$は,
$$
\begin{eqnarray}
v\zeta &=& v\frac{\partial v}{\partial x}-v\frac{\partial u}{\partial y} \\

&=& \frac{1}{2}\frac{\partial }{\partial x}v^2-\frac{\partial }{\partial y}(uv)
+u\frac{\partial v}{\partial y} \\

&=& \frac{1}{2}\frac{\partial }{\partial x}v^2-\frac{\partial }{\partial y}(uv)
-u\frac{\partial u}{\partial x} \\

&=& \frac{1}{2}\frac{\partial }{\partial x}v^2-\frac{\partial }{\partial y}(uv)
-\frac{1}{2}\frac{\partial u^2}{\partial x} \\

\end{eqnarray}
$$
より, 
$$
v \zeta = - \frac {\partial}{\partial y}(uv)+\frac{1}{2}\frac{\partial }{\partial x}(v^2-u^2)
$$
となる。すなわち, 渦度フラックスは,
$$
\bigg( \frac{1}{2}(v^2-u^2), \, -uv \bigg)
$$
という2次元のベクトル量の発散に等しい。南北の境界においてこれらのフラックスの影響が0であると仮定すると, (9.53)を$y$方向に積分することにより,
$$
\begin{eqnarray}
\frac{\partial }{\partial t}\iint \bigg( \frac{\zeta^2}{\beta + \Zeta_y} \bigg) dxdy
= - 2 \underbrace{\iint v \zeta \, dxdy}_{0} = 0 \\

\therefore \quad 

\frac{\partial }{\partial t}\iint \bigg( \frac{\zeta^2}{\beta + \Zeta_y} \bigg) dxdy
=0 \tag{9.55}
\end{eqnarray}
$$
したがって, 擾乱が発達しても, $\frac{\zeta^2}{\beta + \Zeta_y}$という量の面積分の値は保存される。この条件を満足させるためには,
$$
\beta + \Zeta_y \, (= \beta -U_{yy})
$$
が領域内のどこかの点で0になる必要がある。$\frac{\zeta^2}{\beta + \Zeta_y}$は**波の活動度密度**と呼ばれ, 発達する擾乱に関しても保存量となるので, 発達する擾乱のメカニズムを考える際に便利な量である。


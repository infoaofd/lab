#!/bin/bash
# Tue, 19 Nov 2024 07:41:09 +0900
# /work09/$(whoami)/24.00.TEACHING/KANKYO_KAISEKI/2020_KANKYO_KAISEKI/07-07.KS_ZONAL_MEAN

function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

YMDHM1=$1; YMDHM1=${YMDHM1:-202108120200}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}


TIME=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}

#CTL=$(basename $0 .sh).CTL
#if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

INDIR=/work03/DATA/NCEP1/LTM.MON
INFLE=uwnd.mon.ltm.1991-2020.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
LEV=200

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${LEV}.PDF

# NetCDF OUT
# https://gitlab.com/infoaofd/lab/-/tree/master/00.SKILL/00.TOOL/GRADS/GrADS_RECIPE/GrADS_NETCDF書き出し
#NC=$(basename $0 .sh).nc

LONW=0 ;LONE=180 ; LATS=-10 ;LATN=90

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
# KIND='midnightblue->deepskyblue->paleturquoise->white->orange->red->magenta'
# FS=2
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 1
xwid = 4.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grads off';'set grid off'

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set t 12'

say 'MMMMM ZONAL MEAN OF U, uzm'
'uzm=ave(uwnd,lon=${LONW},lon=${LONE})'

say 'MMMMM beta parameter'
pi=3.14159265359; FAC=1E5 ;# km->m
# dtr=pi/180
'dtr=0.0174532925199'
say 'MMMMM dtr='dtr
r=6.371e6 ;# Radius of the Earth
#OMG=2*pi/86156.09 ;# Angular velocity of the Earth ;#04.絶対渦度保存則.pptx
OMG=7.29279300764461e-05
say 'MMMMM OMG='OMG
#f0='2*'OMG'*sin('lat'*0.0174532925199)'   ;# Coriolis parameter
#say 'MMMMM f0='f0
'beta=2*7.29279300764461e-05*cos('lat'*0.0174532925199)/6.371e6' ;# beta parameter ;#14.惑星ロスビー波の基本的性質.pptx

say 'MMMMM SECOND DERIVATIVE OF uzm WITH RESPECT TO Y'
'define dy = 6.371e6*cdiff(lat,y)*3.14159265359/180'
'dtdy=cdiff(uzm,y)/dy'
'd2udy2=cdiff(dtdy,y)/dy'
say 'MMMMM RADIUS'
'RE=6.371e6*COS(lat*0.0174532925199)'

'set lat 0';'set lon 0'
'd beta';say 'beta 'result
'd dy';say 'dy 'result
'd d2udy2'; say 'd2udy2 'result
'd uzm';say 'uzm 'result
'Ks0=sqrt(abs(beta-d2udy2)/abs(uzm)) )'
'd Ks0';say 'Ks0 'result

'set lat 10 90'
'define Ks=sqrt((beta-d2udy2)/uzm))'
'set cmark 0';'set cthick 7';'set ccolor 1'
'set xyrev on'
'set vrange 0 10'
'd Ks*RE'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.15 0.18'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=yb-0.5
'draw string 'x' 'y' Ks'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

#say; say 'MMMMM NetCDF OUT'
#'define VAROUT = VAR'
#'set sdfwrite '
#'sdfwrite VAROUT'
#say 'MMMMM DONE.'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

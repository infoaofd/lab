#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
#MMM=JUN
MMM=JUL
DATETIME=00Z01${MMM}0001
LEV=500
INDIR=/work05/manda/DATA/NCEP1.LTM/P
CTL1="air.mon.ltm.nc"
CTL2="shum.mon.ltm.nc"
CTL3="hgt.mon.ltm.nc"
CTL4="uwnd.mon.ltm.nc"
CTL5="vwnd.mon.ltm.nc"


FIG=$(basename $0 .sh)_${MMM}_${VAR}${LEV}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='white->red'
CLEV='6 20 2'

cat <<EOF>$GS
'sdfopen ${INDIR}/$CTL1'
'sdfopen ${INDIR}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'
'sdfopen ${INDIR}/$CTL5'


say
i=1
ie=5
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'

'color ${CLEV} -kind ${KIND}'
'set mpdset mres'
'set map 1 1 3'
'set lon  90 180'
'set lat  10  60'
'set time ${DATETIME}'
'set lev ${LEV}'
'set grid off'

'd mag(uwnd.4,vwnd.5)' ;*m/s

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
'color ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2'


'set gxout contour'
'set ccolor 0'
'set cthick 0'
'set cint 20'
'set clab off'
'set xlab off'
'set ylab off'
'd hgt.3'

'set ccolor 1'
'set cthick 2'
'set cint 20'
'set clab on'
'set clskip 2'
'd hgt.3'

'set ccolor 0'
'set cthick 10'
'vec skip(uwnd.4,2,2);vwnd.5 -SCL 0.5 20 -P 100 100'

x1=xl+0.1
y1=y1-0.2
'set ccolor 1'
'set line 1 1 5'
'set cthick 2'
'vec skip(uwnd.4,2,2);vwnd.5 -SCL 0.5 20 -P ' x1 ' ' y1 ' -SL m/s'


'draw title ${MMM} ${LEV}hPa'

say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo
rm -vf $GS

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

#!/bin/bash

# Sun, 20 Dec 2020 21:13:53 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/05-01.Z500.CFSv2FCST

#CTL=$(basename $0 .sh).CTL
GS=$(basename $0 .sh).GS

LONW=80
LONE=180
LATS=-20
LATN=60

FIG=$(basename $0 .sh)

LEVS="-200 200 20"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2


HOST=$(hostname)
CWD=$(pwd)
NOW=$(date -R)
CMD="$0 $@"

INDIR=/work05/manda/DATA/CFS/FCST_MCHECN
INFLE1=CFSv2.z500.1219.wkly.anom.nc
INFLE2=CFSv2.tmpsfc.1219.wkly.anom.nc

TEXT="CLR=Z500A CNT=TsfcA FCST VALID: "

cat << EOF > ${GS}

# Sun, 20 Dec 2020 21:13:53 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/05-01.Z500.CFSv2FCST

'sdfopen ${INDIR}/${INFLE1}' ;#${CTL}'
'sdfopen ${INDIR}/${INFLE2}' ;#${CTL}'


xmax = 1
ymax = 1

ytop=9

xwid = 6.0/xmax
ywid = 6.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye


'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set grid off'
'set grads off'
'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set t 1'
'q dims'
lin=sublin(result,5)
outtime=subwrd(lin,6)

'd anom.1'

'set xlab off'
'set ylab off'

'set gxout contour'
'd anom.2'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'

# TEXT
x=xl ;# (xl+xr)/2
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 l 3 0'
'draw string 'x' 'y' ${TEXT}'outtime

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}_'outtime'.eps'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

echo
if [ -f $FIG\* ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG\*
fi
echo

echo "DONE $0."
echo

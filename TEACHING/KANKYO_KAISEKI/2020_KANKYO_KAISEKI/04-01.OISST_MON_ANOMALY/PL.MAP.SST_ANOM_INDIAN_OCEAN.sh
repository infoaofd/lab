#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=SST
#MMM=JUN
MMM=JUL
YYYY=1997
DATETIMER=00Z01${MMM}${YYYY}
DATETIMEC=00Z01${MMM}0001

LONW=40; LONE=110
LATS=-20; LATN=20

INDIR=.
CTL1=OISST.CTL
CTL2=OISST_LTM.CTL


TITLE="ANOM SST ${MMM} ${YYYY}"

FIG=$(basename $0 .sh)_${YYYY}${MMM}_${VAR}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='cyan->blue->palegreen->white->white->gold->red->magenta'
CLEV='-1.8 1.8 0.2'
UNIT=K

cat <<EOF>$GS
'open ${INDIR}/$CTL1'
'open ${INDIR}/$CTL2'


say
i=1
ie=2
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
'color ${CLEV} -kind ${KIND}'
'set mpdset mres'
'set map 1 1 3'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set t 1'
'set grid off'
'set xlint 10'
'set ylint 5'

'ITPSST=lterp(SST.1(time=${DATETIMER}),sst.2(time=${DATETIMEC}),bilin)'
'set time ${DATETIMEC}'
'd ITPSST-sst.2'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr-1
y1=yb-0.5
y2=y1+0.1
'color ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -fs 2 -ft 2'

'set strsiz 0.1 0.16'
'set string 1 l 3'
x=xr-0.5
y=(y1+y2)/2
'draw string 'x' 'y' ${UNIT}'


'set time ${DATETIMEC}'
'set gxout contour'
'set ccolor 0'
'set cthick 0'
'set cint 2'
'set clab off'
'd sst.2'

'set ccolor 1'
'set cthick 2'
'set cint 2'
'set clab on'
'set clskip 2'
'd sst.2'

'set strsiz 0.18 0.25'
'set string 1 c 3'

x=(xl+xr)/2
y=yt+0.2
'draw string 'x' 'y' ${TITLE}'

say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo
rm -vf $GS

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

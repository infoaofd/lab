#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
#MMM=JUN
MMM=AUG
DATETIMEC=00Z01${MMM}0001
DATETIME=00Z01${MMM}1997

LEV=1000
LONW=40; LONE=110
LATS=-20; LATN=20

INDIRC=/work05/manda/DATA/NCEP1.LTM/P
CTL1="uwnd.mon.ltm.nc"
CTL2="vwnd.mon.ltm.nc"
INDIR=/work05/manda/DATA/NCEP1/NC
CTL3="uwnd.mon.mean.nc"
CTL4="vwnd.mon.mean.nc"



FIG=$(basename $0 .sh)_${MMM}_${VAR}${LEV}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='white->red'
CLEV='6 20 2'

cat <<EOF>$GS
'sdfopen ${INDIRC}/$CTL1'
'sdfopen ${INDIRC}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'


say
i=1
ie=4
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'

'set mpdset mres'
'set map 1 1 3'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set time ${DATETIME}'
'set lev ${LEV}'
'set grid off'
'set xlint 10'
'set ylint 5'

'set ccolor 0'
'set cthick 10'
'vec skip(uwnd.3,1);vwnd.4 -SCL 0.5 20 -P 100 100'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1

x1=xl+0.4
y1=y1
'set ccolor 3'
'set line 3 1 5'
'set cthick 3'
'vec skip(uwnd.3,1);vwnd.4 -SCL 0.5 20 -P ' x1 ' ' y1 ' -SL m/s'

'set xlab off'
'set ylab off'




'set time ${DATETIMEC}'
'set ccolor 0'
'set cthick 10'
'vec skip(uwnd.1,1);vwnd.2 -SCL 0.5 20 -P 100 100'


x1=xl+1.5
y1=y1
'set ccolor 1'
'set line 1 1 5'
'set cthick 3'
'vec skip(uwnd.1,1);vwnd.2 -SCL 0.5 20 -P ' x1 ' ' y1 ' -SL m/s'



'draw title ${MMM} ${LEV}hPa'

say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo
rm -vf $GS

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

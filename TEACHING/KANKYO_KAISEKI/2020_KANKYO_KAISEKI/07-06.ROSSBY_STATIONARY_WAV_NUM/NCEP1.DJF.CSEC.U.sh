#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
UNIT="[m/s]"
MMM=DJF

LONW=0
LONE=360
LATS=-70
LATN=70
LEVB=1000
LEVT=100
LEV2=850

INDIR=/work05/manda/DATA/NCEP1.LTM/P
CTL1="air.mon.ltm.nc"
CTL2="shum.mon.ltm.nc"
CTL3="hgt.mon.ltm.nc"
CTL4="uwnd.mon.ltm.nc"
CTL5="vwnd.mon.ltm.nc"


FIG=$(basename $0 .sh)_${MMM}_${VAR}_PT.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='blue->skyblue->white->orange->red'
CLEV='-50 -40 -30 -20 -10 0 10 20 30 40 50'



cat <<EOF>$GS
'sdfopen ${INDIR}/$CTL1'
'sdfopen ${INDIR}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'
'sdfopen ${INDIR}/$CTL5'


say
i=1
ie=5
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEVB} ${LEVT}'

'set t 12'
'utave=uwnd.4'
'set t 1'
'utave=utave+uwnd.4'
'set t 2'
'utave=utave+uwnd.4'
'utave=utave/3'
'set lon ${LONW}'
'ucsc=ave(utave,lon=${LONW},lon=${LONE})'



'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=2

ytop=9

xwid =  7/xmax
ywid =  6/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shaded'
'color -levs ${CLEV} -kind ${KIND}'

'set grid off'
'd ucsc'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
'color -levs ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 1'


'set gxout contour'
'set ccolor 0'
'set cthick 1'
'set clevs ${CLEV}'
'set clab off'
'set xlab off'
'set ylab off'
'd ucsc'



'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEVB} ${LEVT}'
'set t 12'
'ttave=air.1'
'set t 1'
'ttave=ttave+air.1'
'set t 2'
'ttave=ttave+air.1'
'ttave=ttave/3'
'theta=(ttave+273.15)*pow((1000/lev),287/1004)'
'set lon ${LONW}'
'tcsc=ave(theta,lon=${LONW},lon=${LONE})'


'set xlab off'
'set ylab off'
'set cthick 10'
'set ccolor 0'
'set cint 10'
'set ylpos 0 l'
'd tcsc'
'set cthick 1'
'set ccolor 1'
'set cint 10'
'set clab on'
'set clskip 3'
'd tcsc'

x=xl-0.7
y=(yt+yb)/2
'set strsiz 0.14 0.17'
'set string 1 c 3 90'
'draw string 'x' 'y' P[hPa]'
'set string 1 c 1 0'

'draw title ${MMM} ${VAR} ${UNIT} AVG OVER ${LONW}-${LONE}'



say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'




'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

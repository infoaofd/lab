C   This program calculates isentropic potential vorticity (IPV)
C   on an isentropic surface from stream function and
C   temperature data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.
C   NMAX: Number of vertical layers
C   NMAX=17.

      PARAMETER (LMAX=144,MMAX=73,NMAX=17)

C   PS: Stream function
C   T:  Temperature
C   TH: Potential temperature

      REAL PS(LMAX,MMAX,NMAX),T(LMAX,MMAX,NMAX),TH(LMAX,MMAX,NMAX),
     +     PS1(LMAX,MMAX),TU1(LMAX,MMAX),TL1(LMAX,MMAX)

C   ZT: Absolute vorticity
C   TP: Pressure-derivative of potential temperature
C   Q:  Isentropic potential vorticity [PVU = 10^-6 K m^2 /kg s]

      REAL Q(LMAX,MMAX,NMAX),
     +     ZT1(LMAX,MMAX),TP1(LMAX,MMAX),Q1(LMAX,MMAX)

C   Please specify missing value.

      PARAMETER (RMISS=-0.999E+34)

C ========================================

      I = 1
   11 CONTINUE

C ----------------------------------------

C   Files of variable field are read.

      CALL RDDAT (LMAX,MMAX,NMAX,I,NEND,PS,T)

      IF (NEND.EQ.1) GO TO 19

C ----------------------------------------

C   Fill the IPV array with missing values.

      CALL MISS (LMAX,MMAX,NMAX,RMISS,Q)

C ----------------------------------------

C   Do-loop for each layer.

      DO 21 N=2,NMAX-1

C ----------------------------------------

C   Vertical layer is specified.

      CALL STLEV (N,PRU,PRC,PRL)
      CALL PICK  (LMAX,MMAX,NMAX,N  ,PS,PS1)
      CALL PICK  (LMAX,MMAX,NMAX,N+1,T ,TU1)
      CALL PICK  (LMAX,MMAX,NMAX,N-1,T ,TL1)

C ----------------------------------------

C   Calculation of horizontal vorticity

      CALL NABLA (LMAX,MMAX,PS1,ZT1)
      CALL COR   (LMAX,MMAX,ZT1)

C ----------------------------------------

C   Calculation of stratification

      CALL STRAT (LMAX,MMAX,PRU,PRL,TU1,TL1,TP1)

C ----------------------------------------

C   Calculation of isentropic potential vorticity

      CALL ERTEL (LMAX,MMAX,ZT1,TP1,Q1)
      CALL MULT  (LMAX,MMAX,1.0E6,Q1)

C ----------------------------------------

C   IPV at each layer is substituted to the array.

      CALL ENTER (LMAX,MMAX,NMAX,N,Q1,Q)

C ----------------------------------------

   21 CONTINUE

C ----------------------------------------

C   Conversion of the values in pressure surfaces into that in a
C   ithothermal surface.

      CALL THETA (LMAX,MMAX,NMAX,RMISS,T,TH)
      TH0 = 350.
      CALL ISOTH (LMAX,MMAX,NMAX,RMISS,TH0,TH,Q,Q1)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A11,I5,A1,1X,A31,F6.1)')
     +  'Record # = ', I,',',
     +  'Output (35 deg.N, 140 deg.E) = ', Q1(57,23)

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,TH0,I,Q1)

C ----------------------------------------

      I = I + 1
      GO TO 11
   19 CONTINUE

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE RDDAT (LMAX,MMAX,NMAX,I,NEND,PS,T)
      REAL PS(LMAX,MMAX,NMAX),T(LMAX,MMAX,NMAX),GG(144,73)
      CHARACTER INFL*80,CHAR3*3
      NEND = 0
      DO 11 N=1,NMAX
      IPR = IPSTAN (N)
      INFL = 'P'//CHAR3(IPR)//'.dat'
      OPEN(10,FILE=INFL,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=I,ERR=19) GG
      DO 21 M=1,MMAX
      DO 22 L=1,LMAX
        PS(L,M,N) = GG(L,M)
   22 CONTINUE
   21 CONTINUE
      CLOSE(10)
      INFL = 'T'//CHAR3(IPR)//'.dat'
      OPEN(10,FILE=INFL,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=I,ERR=19) GG
      DO 31 M=1,MMAX
      DO 32 L=1,LMAX
        T(L,M,N) = GG(L,M)
   32 CONTINUE
   31 CONTINUE
      CLOSE(10)
   11 CONTINUE
      RETURN
   19 NEND=1
      RETURN
      END

      SUBROUTINE STLEV (N,PRU,PRC,PRL)
      IPRU = IPSTAN(N+1)
      IPRC = IPSTAN(N  )
      IPRL = IPSTAN(N-1)
      PRU = 1.0E2 * REAL(IPRU)
      PRC = 1.0E2 * REAL(IPRC)
      PRL = 1.0E2 * REAL(IPRL)
      RETURN
      END


      SUBROUTINE WRDAT (LMAX,MMAX,TH,I,G)
      REAL G(LMAX,MMAX)
      CHARACTER OUTFL*80,CHAR3*3
      ITH = NINT(TH)
      OUTFL = 'Q'//CHAR3(ITH)//'.dat'
      OPEN(10,FILE=OUTFL,
     +     STATUS='UNKNOWN',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=I) G
      CLOSE(10)
      RETURN
      END

      INTEGER FUNCTION IPSTAN (IL)
      IPSTAN = 0
      IF (IL.EQ. 1) IPSTAN = 1000
      IF (IL.EQ. 2) IPSTAN =  925
      IF (IL.EQ. 3) IPSTAN =  850
      IF (IL.EQ. 4) IPSTAN =  700
      IF (IL.EQ. 5) IPSTAN =  600
      IF (IL.EQ. 6) IPSTAN =  500
      IF (IL.EQ. 7) IPSTAN =  400
      IF (IL.EQ. 8) IPSTAN =  300
      IF (IL.EQ. 9) IPSTAN =  250
      IF (IL.EQ.10) IPSTAN =  200
      IF (IL.EQ.11) IPSTAN =  150
      IF (IL.EQ.12) IPSTAN =  100
      IF (IL.EQ.13) IPSTAN =   70
      IF (IL.EQ.14) IPSTAN =   50
      IF (IL.EQ.15) IPSTAN =   30
      IF (IL.EQ.16) IPSTAN =   20
      IF (IL.EQ.17) IPSTAN =   10
      RETURN
      END

      CHARACTER*3 FUNCTION CHAR3 (I)
      CHARACTER CHAR*1
      CHAR3 = CHAR(48+MOD(I/100,10))//CHAR(48+MOD(I/10 ,10))
     +      //CHAR(48+MOD(I    ,10))
      RETURN
      END

C ========================================

      SUBROUTINE MISS (LMAX,MMAX,NMAX,RMISS,G)
      REAL G(LMAX,MMAX,NMAX)
      DO 11 N=1,NMAX
      DO 12 M=1,MMAX
      DO 13 L=1,LMAX
        G(L,M,N) = RMISS
   13 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE PICK (LMAX,MMAX,NMAX,N,G,GG)
      REAL G(LMAX,MMAX,NMAX),GG(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        GG(L,M) = G(L,M,N)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ENTER (LMAX,MMAX,NMAX,N,GG,G)
      REAL GG(LMAX,MMAX),G(LMAX,MMAX,NMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M,N) = GG(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

C ========================================

      SUBROUTINE THETA (LMAX,MMAX,NMAX,RMISS,T,TH)
      REAL T(LMAX,MMAX,NMAX),TH(LMAX,MMAX,NMAX)
      DO 11 N=1,NMAX
      P = 1.0E2 * REAL(IPSTAN(N))
      EX = (P/1.0E5)**(0.2E1/0.7E1)
      DO 12 M=1,MMAX
      DO 13 L=1,LMAX
      IF (T(L,M,N).NE.RMISS) THEN
        TH(L,M,N) = T(L,M,N)/EX
      ELSE
        TH(L,M,N) = RMISS
      ENDIF
   13 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ISOTH (LMAX,MMAX,NMAX,RMISS,TH0,TH,G,GG)
      REAL TH(LMAX,MMAX,NMAX),G(LMAX,MMAX,NMAX),GG(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        GG(L,M) = RMISS
      DO 21 N=1,NMAX-1
      IF (     (TH(L,M,N  ).NE.RMISS)
     +    .AND.(TH(L,M,N+1).NE.RMISS)) THEN
      IF (     (TH(L,M,N  ).LE.TH0)
     +    .AND.(TH(L,M,N+1).GE.TH0)) THEN
        GG(L,M) = (  (TH0-TH(L,M,N  ))*G(L,M,N+1)
     +             + (TH(L,M,N+1)-TH0)*G(L,M,N  ))
     +            /(TH(L,M,N+1)-TH(L,M,N))
      ENDIF
      ENDIF
   21 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

C ========================================

      SUBROUTINE NABLA (LMAX,MMAX,F,G)
      REAL F(LMAX,MMAX),G(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
        DX1 = 0.5E0*(DX+DX1)
        DX2 = 0.5E0*(DX+DX2)
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        G(L,M) = + (DX1*(F(L,M1)-F(L,M))+DX2*(F(L,M2)-F(L,M)))
     +             / (DX*DY**2)
     +           + (F(L1,M)+F(L2,M)-0.2E1*F(L,M)) / DX**2
      ELSE
        G(L,M) = 0.0E0
        IF (M.EQ.   1) MM = 2
        IF (M.EQ.MMAX) MM = MMAX-1
        DO 21 LL=1,LMAX
          G(LL,M) = G(LL,M)
     +      + 0.4E1 * (F(LL,MM)-F(LL,M)) / DY**2 / REAL(LMAX)
   21   CONTINUE
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE COR (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      DO 11 M=1,MMAX
        TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
        F  = 0.2E1 * OM * SIN(TH)
      DO 12 L=1,LMAX
        G(L,M) = G(L,M) + F
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE STRAT (LMAX,MMAX,PRU,PRL,TU,TL,TP)
      REAL TU(LMAX,MMAX),TL(LMAX,MMAX),TP(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        THU = TU(L,M) / (PRU/1.0E5)**(0.2E1/0.7E1)
        THL = TL(L,M) / (PRL/1.0E5)**(0.2E1/0.7E1)
        TP(L,M) = (THU-THL) / (PRU-PRL)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ERTEL (LMAX,MMAX,ZT,TP,Q)
      REAL ZT(LMAX,MMAX),TP(LMAX,MMAX),Q(LMAX,MMAX)
      GC = 0.9807E1
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        Q(L,M) = - GC * ZT(L,M) * TP(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE MULT (LMAX,MMAX,C,G)
      REAL G(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M) = C * G(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

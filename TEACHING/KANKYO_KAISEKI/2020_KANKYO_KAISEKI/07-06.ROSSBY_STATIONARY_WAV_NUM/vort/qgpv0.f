C   This program calculates total quasi-geostrophic potential vorticity
C   from temperature and geopotential height data in so-called GrADS
C   format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   T: Temperature
C   Z: Geopotential height

      REAL TU(LMAX,MMAX),TC(LMAX,MMAX),TL(LMAX,MMAX),
     +     ZU(LMAX,MMAX),ZC(LMAX,MMAX),ZL(LMAX,MMAX)

C   PS:  Geostrophic stream function
C   PSP: Pressure-derivative of geostrophic stream function

      REAL PSU (LMAX,MMAX),PSC (LMAX,MMAX),PSL (LMAX,MMAX),
     +     PSP1(LMAX,MMAX),PSP2(LMAX,MMAX)

C   F:  Planetary vorticity
C   XI: Relative vorticity
C   ZT: Absolute vorticity
C   ST: Stretching vorticity
C   Q:  Quasi-geostrophic potential vorticity

      REAL F (LMAX,MMAX),XI(LMAX,MMAX),ZT(LMAX,MMAX),
     +     ST(LMAX,MMAX),Q (LMAX,MMAX)

C   Note: Reference latitude is specified by parameter M0.

C ========================================

C   Parameters are set.

C   Reference latitude is specified as meridional grid # M0.
C   M0=1: 90 deg.N, M0=37: 0 deg., M0=73: 90 deg.S.
C   It is recommended to set reference latitude within the mid- or
C   high-latitudes where you focus on.

      M0 = 19
      CALL PARAM (MMAX,M0,TH0,F0)

      WRITE(6,'(1X,A21,F5.1)')
     +  'Reference latitude = ',0.180E3*(0.5E0-REAL(M0-1)/REAL(MMAX-1))

C ----------------------------------------

C   Type of calculation is selected.

C   MODE=1: Planetary vorticity
C   MODE=2: Relative vorticity
C   MODE=3: Absolute vorticity
C   MODE=4: Stretching vorticity
C   MODE=5: Quasi-geostrophic potential vorticity

      CALL SELEC (MODE)

C ----------------------------------------

C   Pressure levels are specified.

      CALL STLEV (IPRU,IPRC,IPRL,PRU,PRC,PRL)

C ----------------------------------------

C   Files of variable field are read.

      CALL RDDAT (LMAX,MMAX,IPRU,IPRC,IPRL,TU,TC,TL,ZU,ZC,ZL)

C ----------------------------------------

C   Calculation of geostrophic stream function

      CALL GSTRM (LMAX,MMAX,F0,ZU,PSU)
      CALL GSTRM (LMAX,MMAX,F0,ZC,PSC)
      CALL GSTRM (LMAX,MMAX,F0,ZL,PSL)
      CALL NORM  (LMAX,MMAX,M0,PSU)
      CALL NORM  (LMAX,MMAX,M0,PSC)
      CALL NORM  (LMAX,MMAX,M0,PSL)

C ----------------------------------------

C   Calculation of horizontal vorticity

      CALL NABLA (LMAX,MMAX,PSC,XI)
      CALL COR   (LMAX,MMAX,TH0,F)
      CALL ADD   (LMAX,MMAX,F,XI,ZT)

C ----------------------------------------

C   Calculation of stretching vorticity

      CALL STRET (LMAX,MMAX,F0,PRU,PRC,PRL,M0,TU,TL,PSU,PSC,PSL,
     +            SS1,SS2,PSP1,PSP2,ST)

C ----------------------------------------

C   Calculation of total potential vorticity

      CALL ADD   (LMAX,MMAX,ZT,ST,Q)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A32,F6.1,A7)')
     +  'F       (35 deg.N, 140 deg.E) = ', 1.0E6*F (57,23), ' *10^-6'
      WRITE(6,'(1X,A32,F6.1,A7)')
     +  'Xi      (35 deg.N, 140 deg.E) = ', 1.0E6*XI(57,23), ' *10^-6'
      WRITE(6,'(1X,A32,F6.1,A7)')
     +  'Zeta    (35 deg.N, 140 deg.E) = ', 1.0E6*ZT(57,23), ' *10^-6'
      WRITE(6,'(1X,A32,F6.1,A7)')
     +  'Stretch (35 deg.N, 140 deg.E) = ', 1.0E6*ST(57,23), ' *10^-6'
      WRITE(6,'(1X,A32,F6.1,A7)')
     +  'Q       (35 deg.N, 140 deg.E) = ', 1.0E6*Q (57,23), ' *10^-6'

C ----------------------------------------

C   Output data are written down.

      IF (MODE.EQ.1) CALL WRDAT (LMAX,MMAX,F )
      IF (MODE.EQ.2) CALL WRDAT (LMAX,MMAX,XI)
      IF (MODE.EQ.3) CALL WRDAT (LMAX,MMAX,ZT)
      IF (MODE.EQ.4) CALL WRDAT (LMAX,MMAX,ST)
      IF (MODE.EQ.5) CALL WRDAT (LMAX,MMAX,Q )

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE PARAM (MMAX,M0,TH0,F0)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      TH0 = PI*(0.5E0-REAL(M0-1)/REAL(MMAX-1))
      F0 = 0.2E1 * OM * SIN(TH0)
      RETURN
      END

      SUBROUTINE SELEC (MODE)
      WRITE(6,*) 'Calculation mode:'
      WRITE(6,*) '1: Planetary vorticity,'
      WRITE(6,*) '2: Relative vorticity,'
      WRITE(6,*) '3: Absolute vorticity,'
      WRITE(6,*) '4: Stretching vorticity,'
      WRITE(6,*) '5: Quasi-geostrophic potential vorticity.'
      READ (5,*) MODE
      RETURN
      END

      SUBROUTINE STLEV (IPRU,IPRC,IPRL,PRU,PRC,PRL)
      WRITE(6,*) 'Pressure levels (upper, center, lower) [hPa] (I) ?'
      READ (5,*) IPRU,IPRC,IPRL
      PRU = 1.0E2 * REAL(IPRU)
      PRC = 1.0E2 * REAL(IPRC)
      PRL = 1.0E2 * REAL(IPRL)
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,IPRU,IPRC,IPRL,
     +                  TU,TC,TL,ZU,ZC,ZL)
      REAL TU(LMAX,MMAX),TC(LMAX,MMAX),TL(LMAX,MMAX),
     +     ZU(LMAX,MMAX),ZC(LMAX,MMAX),ZL(LMAX,MMAX)
      CHARACTER INFL1*80,INFL2*80,INFL3*80,INFL4*80,INFL5*80,INFL6*80,
     +          CHAR3*3
      INFL1 = 'T'//CHAR3(IPRU)//'.dat'
      INFL2 = 'T'//CHAR3(IPRC)//'.dat'
      INFL3 = 'T'//CHAR3(IPRL)//'.dat'
      INFL4 = 'Z'//CHAR3(IPRU)//'.dat'
      INFL5 = 'Z'//CHAR3(IPRC)//'.dat'
      INFL6 = 'Z'//CHAR3(IPRL)//'.dat'
      WRITE(6,*) 'Input files are automatically designated:'
  101 FORMAT (1X,A30,1X,A8)
      WRITE(6,101) 'Temperature (upper)          =',INFL1
      WRITE(6,101) 'Temperature (center)         =',INFL2
      WRITE(6,101) 'Temperature (lower)          =',INFL3
      WRITE(6,101) 'Geopotential height (upper)  =',INFL4
      WRITE(6,101) 'Geopotential height (center) =',INFL5
      WRITE(6,101) 'Geopotential height (lower)  =',INFL6
      OPEN(10,FILE=INFL1,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) TU
      CLOSE(10)
      OPEN(10,FILE=INFL2,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) TC
      CLOSE(10)
      OPEN(10,FILE=INFL3,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) TL
      CLOSE(10)
      OPEN(10,FILE=INFL4,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) ZU
      CLOSE(10)
      OPEN(10,FILE=INFL5,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) ZC
      CLOSE(10)
      OPEN(10,FILE=INFL6,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) ZL
      CLOSE(10)
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER OUTFL*80
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) G
      CLOSE(10)
      WRITE(6,*) 'Output was written to the file.'
      RETURN
      END

      CHARACTER*3 FUNCTION CHAR3 (I)
      CHARACTER CHAR*1
      CHAR3 = CHAR(48+MOD(I/100,10))//CHAR(48+MOD(I/10 ,10))
     +      //CHAR(48+MOD(I    ,10))
      RETURN
      END

C ========================================

      SUBROUTINE GSTRM (LMAX,MMAX,F0,Z,PS)
      REAL Z(LMAX,MMAX),PS(LMAX,MMAX)
      GC = 0.9807E1
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        PS(L,M) = GC / F0 * Z(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE NORM (LMAX,MMAX,M0,G)
      REAL G(LMAX,MMAX)
        SUM = 0.0E0
      DO 11 L=1,LMAX
        SUM = SUM + G(L,M0)
   11 CONTINUE
        G0 = SUM / REAL(LMAX)
      DO 21 M=1,MMAX
      DO 22 L=1,LMAX
        G(L,M) = G(L,M) - G0
   22 CONTINUE
   21 CONTINUE
      RETURN
      END

      SUBROUTINE NABLA (LMAX,MMAX,F,G)
      REAL F(LMAX,MMAX),G(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
        DX1 = 0.5E0*(DX+DX1)
        DX2 = 0.5E0*(DX+DX2)
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        G(L,M) = + (DX1*(F(L,M1)-F(L,M))+DX2*(F(L,M2)-F(L,M)))
     +             / (DX*DY**2)
     +           + (F(L1,M)+F(L2,M)-0.2E1*F(L,M)) / DX**2
      ELSE
        G(L,M) = 0.0E0
        IF (M.EQ.   1) MM = 2
        IF (M.EQ.MMAX) MM = MMAX-1
        DO 21 LL=1,LMAX
          G(LL,M) = G(LL,M)
     +      + 0.4E1 * (F(LL,MM)-F(LL,M)) / DY**2 / REAL(LMAX)
   21   CONTINUE
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE COR (LMAX,MMAX,TH0,F)
      REAL F(LMAX,MMAX)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      DO 11 M=1,MMAX
        TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
        FF = 0.2E1 * OM * (SIN(TH0) + (TH-TH0)*COS(TH0))
      DO 12 L=1,LMAX
        F(L,M) = FF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ADD (LMAX,MMAX,G1,G2,G)
      REAL G1(LMAX,MMAX),G2(LMAX,MMAX),G(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M) = G1(L,M) + G2(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE STRET (LMAX,MMAX,F0,PRU,PRC,PRL,M0,
     +                  TU,TL,PSU,PSC,PSL,SS1,SS2,PSP1,PSP2,ST)
      REAL TU (LMAX,MMAX),TL (LMAX,MMAX),
     +     PSU(LMAX,MMAX),PSC(LMAX,MMAX),PSL(LMAX,MMAX),
     +     PSP1(LMAX,MMAX),PSP2(LMAX,MMAX),
     +     ST  (LMAX,MMAX)
      CALL STRAT (LMAX,MMAX,PRU,PRC,PRL,M0,TU,TL,SS1,SS2)
      CALL PDIFF (LMAX,MMAX,PRU,PRC,PSU,PSC,PSP1)
      CALL PDIFF (LMAX,MMAX,PRC,PRL,PSC,PSL,PSP2)
      PR1 = 0.5E0*(PRU+PRC)
      PR2 = 0.5E0*(PRC+PRL)
      CALL STVRT (LMAX,MMAX,F0,PR1,PR2,SS1,SS2,PSP1,PSP2,ST)
      RETURN
      END

      SUBROUTINE PDIFF (LMAX,MMAX,P1,P2,G1,G2,G)
      REAL G1(LMAX,MMAX),G2(LMAX,MMAX),G(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M) = (G2(L,M)-G1(L,M)) / (P2-P1)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE STRAT (LMAX,MMAX,PR1,PR2,PR3,M0,T1,T3,SS1,SS2)
      REAL T1(LMAX,MMAX),T3(LMAX,MMAX)
      RC = 0.2870E3
      PR = 0.5E0*(PR1+PR3)
        SUM = 0.0E0
      DO 11 L=1,LMAX
        TH1 = T1(L,M0) / (PR1/1.0E5)**(0.2E1/0.7E1)
        TH3 = T3(L,M0) / (PR3/1.0E5)**(0.2E1/0.7E1)
        SS = - RC/PR * ((PR/1.0E5)**(0.2E1/0.7E1))
     +         * (TH3-TH1)/(PR3-PR1)
        SUM = SUM + SS
   11 CONTINUE
        SS = SUM / REAL(LMAX)
        SS1 = SS * (0.5E0*(PR1+PR3))**2 / (0.5E0*(PR1+PR2))**2
        SS2 = SS * (0.5E0*(PR1+PR3))**2 / (0.5E0*(PR2+PR3))**2 
      RETURN
      END

      SUBROUTINE STVRT (LMAX,MMAX,F0,PR1,PR2,SS1,SS2,PSP1,PSP2,ST)
      REAL PSP1(LMAX,MMAX),PSP2(LMAX,MMAX),ST(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        ST(L,M) = (F0**2/SS2*PSP2(L,M)-F0**2/SS1*PSP1(L,M))
     +            / (PR2-PR1)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

C   This program calculates geostrophic stream function from
C   geopotential height data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   Z: Geopotential height
C   PS:  Geostrophic stream function

      REAL Z(LMAX,MMAX),PS(LMAX,MMAX)

C   Note: Reference latitude is specified by parameter M0.

C ========================================

C   Parameters are set.

C   Reference latitude is specified as meridional grid # M0.
C   M0=1: 90 deg.N, M0=37: 0 deg., M0=73: 90 deg.S.
C   It is recommended to set reference latitude within the mid- or
C   high-latitudes where you focus on.

      M0 = 19
      CALL PARAM (MMAX,M0,TH0,F0)

      WRITE(6,'(1X,A21,F5.1)')
     +  'Reference latitude = ',0.180E3*(0.5E0-REAL(M0-1)/REAL(MMAX-1))

C ----------------------------------------

C   Files of variable field are read.

      CALL RDDAT (LMAX,MMAX,Z)

C ----------------------------------------

C   Calculation of geostrophic stream function

      CALL GSTRM (LMAX,MMAX,F0,Z,PS)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A28,F7.1,A7)')
     +  'Z   (35 deg.N, 140 deg.E) = ', Z(57,23)
      WRITE(6,'(1X,A28,F7.1,A7)')
     +  'Z   (45 deg.N, 140 deg.E) = ', Z(57,19)
      WRITE(6,'(1X,A28,F6.1,A7)')
     +  'Psi (35 deg.N, 140 deg.E) = ', 1.0E-6*PS(57,23), ' *10^6'
      WRITE(6,'(1X,A28,F6.1,A7)')
     +  'Psi (45 deg.N, 140 deg.E) = ', 1.0E-6*PS(57,19), ' *10^6'

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,PS)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE PARAM (MMAX,M0,TH0,F0)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      TH0 = PI*(0.5E0-REAL(M0-1)/REAL(MMAX-1))
      F0 = 0.2E1 * OM * SIN(TH0)
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER INFL*80
      WRITE(6,*) 'Geopotential height data ?'
      READ (5,'(A80)') INFL
      OPEN (10,FILE=INFL,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ (10,REC=1) G
      CLOSE(10)
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER OUTFL*80
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) G
      CLOSE(10)
      WRITE(6,*) 'Output was written to the file.'
      RETURN
      END

C ========================================

      SUBROUTINE GSTRM (LMAX,MMAX,F0,Z,PS)
      REAL Z(LMAX,MMAX),PS(LMAX,MMAX)
      GC = 0.9807E1
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        PS(L,M) = GC / F0 * Z(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

D:\TOOLS_200804\TOOLBOX\METEOROLOGY\TOOLS_GAKUGEI_DAI

Analyses for Global GPV Data

http://kishou.u-gakugei.ac.jp/tools/analys/global/

Data format

The following data format is assumed in the following programs.

2-dimendinal (144*73) 4-byte real variables (so-called GrADS format)

Coordinate system

  NP   (  1, 1)----------------(144, 1)
           |                       |
  EQ       |                       |      
           |                       |
  SP   (  1,73)----------------(144,73)
           
           0    (E)   180   (W)   2.5 W

    (L =   1, M =  1) = (  0.0 E, 90.0 N)
    (L =   2, M =  1) = (  2.5 E, 90.0 N)
    (L =   3, M =  1) = (  5.0 E, 90.0 N)
      ...
    (L = 144, M =  1) = (  2.5 W, 90.0 N)
    (L =   1, M =  2) = (  0.0 E, 87.5 N)
      ...
    (L = 144, M = 73) = (  2.5 W, 90.0 S)

  Note: The meridional axis (y-axis) is reversed.

Most of the following programs analyze daily data set.

========================================================================

Program name             Function

------------------------------------------------------------------------

Basic Calculations

basic/calc1.f            Add a constant to a variable field, or multiply
                         a variable field by a constant
basic/calc1_multi.f      Add a constant to a variable field, or multiply
                         a variable field by a constant, for more than
                         one records
basic/calc2.f            Calculate sum, difference, product or quotient
                         of two variable fields
basic/calc2_multi.f      Calculate sum, difference, product or quotient
                         of two variable fields, for more than one
                         records
basic/mean.f             Calculate mean of the records
basic/meanl.f            Calculate mean of the records neglecting
                         missing values
basic/meanreduc.f        Subtract mean of the records form each record

------------------------------------------------------------------------

Processing of spatial data

spatial/gpv.f            Print out grid point values
spatial/gpv_multi.f      Print out grid point values, for more than one
                         records
spatial/gpv_daily.f      Print out grid point values, for a daily data
                         set
spatial/region1.f        Print out regional mean over specified latitude
                         range
spatial/region2.f        Print out regional mean over region specified
                         by grid #
spatial/corr1.f          Print out pattern correlation over specified
                         latitude range
spatial/zonal.f          Calculate zonal mean, or deviation from zonal
                         mean
spatial/zrot.f           Rotate a variable field in zonal direction
spatial/zfilter.f        Calculate variable field which is zonally
                         low-(high-)passed
spatial/zfilter_multi.f  Calculate variable field which is zonally
                         low-(high-)passed, for more than one records

------------------------------------------------------------------------

Processing of temporal data

temporal/pick.f          Pick out a variavble field by specifying record
                         #
temporal/picks.f         Pick out variavble fields by specifying record
                         #s
                         picks_list.dat: a sample of record # list
temporal/snap.f          Pick out snap shot on a specified day
temporal/snaps.f         Pick out snap shots on specified days
                         snaps_list.dat: a sample of date list
temporal/snaps_period.f  Pick out daily snap shots on a specified period
temporal/snaps_period2.f Pick out daily snap shots on a specified period
                         which is long
temporal/tmean.f         Calculate temporal mean
temporal/tmeans.f        Calculate temporal means
                         tmeans_list.dat: a sample of period list
temporal/clim.f          Calculate climatological mean
temporal/clim_all.f      Calculate climatological mean for all daily
                         data files
temporal/clim_pentad.f   Calculate climatological mean for each pentad
                         period of the year
temporal/clim_tenday.f   Calculate climatological mean for each ten-day
                         period of the year
temporal/clim_monthly.f  Calculate climatological mean for each month of
                         the year
temporal/compo.f         Calculate composite mean
                         compo_list.dat: a sample of period list
temporal/compolag.f      Calculate lag composite mean
                         compo_list.dat: a sample of period list
temporal/vari.f          Calculate interannual variance

------------------------------------------------------------------------

Processing of temporal data for 6-hourly data

temporal6/snap.f         Pick out snap shot on a specified time step
temporal6/sixhr2daily.f  Calculate daily mean data from six-hourly data
                         set

------------------------------------------------------------------------

Processing of yearly data

yearly/yearly.f          Edit yearly mean data set (seasonal mean field
                         of each year)
yearly/yearly_all.f      Edit yearly mean data set (seasonal mean field
                         of each year) for all daily data files
yearly/yranom.f          Edit yearly anomaly data from climatology
                         (seasonally-averaged anomaly field of each year)
yearly/yranom_all.f      Edit yearly anomaly data from climatology for
                         all daily data files
yearly/clim.f            Calculate climatological mean from yearly data
yearly/vari.f            Calculate interannual variance from yearly data
yearly/compo1.f          Calculate composite mean from yearly data (one-
                         sample problem)
yearly/compo2.f          Calculate composite difference from yearly data
                         (two-sample problem)
                         (between specified years and rest years)
yearly/compo3.f          Calculate composite difference from yearly data
                         (two-sample problem)
                         (between two groups of specified years)
                         compo_list?.dat: samples of year list
yearly/telecon.f         Calculate teleconnectivity from yearly data

------------------------------------------------------------------------

Processing of annual cycle data

annual/clim.f            Calculate climatological mean for each day of
                         the year
annual/compo.f           Calculate composite difference for each day of
                         the year
                         (two-sample problem)
                         (between specified years and rest years)

------------------------------------------------------------------------

Temporal filter

tfilter/rmean.f          Calculate running mean field
tfilter/rmean_interp.f   Calculate running mean field with temporal
                         interpolation
tfilter/rmeandif.f       Calculate difference of two running mean fields
tfilter/rmeandif_interp.f  Calculate difference of two running mean
                         fields with temporal interpolation
tfilter/lowpass.f        Lowpass filter
tfilter/highpass.f       Highpass filter
tfilter/bandpass.f       Bandpass filter
tfilter/climreduc.f      Calculate deviation from daily climatological
                         mean
tfilter/climreduc2.f     Calculate deviation from daily climatological
                         mean
tfilter/climreduc3.f     Calculate deviation from daily climatological
                         mean
tfilter/calc2.f          The same as basic/calc2.f, except for temporal
                         data
tfilter/intra.f          Calculate intraseasonal variation

------------------------------------------------------------------------

Monthly data

monthly/climmon.f        Calculate climatological mean for
                         monthly/10-day/pentad data
monthly/yearlymon.f      Edit yearly mean data set (seasonal mean field
                         of each year) for montly/10-day/pentad data

------------------------------------------------------------------------

Zonal wind

zwind/zwind.f            Print out zonally-averaged zonal wind
zwind/wavenum.f          Calculate total wavenumber of stationary Rossby
                         wave
zwind/ray.f              Performs ray trace analysis of stationary Rossby
                         wave

------------------------------------------------------------------------

Vorticity/divergence analyses

vort/psi2uv.f            Calculate wind field from stream function/
                         velocity potential
vort/uv2xi.f             Calculate vorticity/divergence from wind field
vort/uv2xi_multi.f       Calculate vorticity/divergence from wind field
                         for data file with more than one records
vort/uv2xi_multimiss.f   Calculate vorticity/divergence from wind field
                         which contains missing values for data file
                         with more than one records
vort/psi2xi.f            Calculate vorticity/divergence from stream
                         function/velocity potential
vort/rossby.f            Calculate total wavenumber of stationary Rossby
                         wave
vort/qgpv0.f             Calculate total quasi-geostrophic potential
                         vorticity
vort/qgpv1.f             Calculate anomalous quasi-geostrophic potential
                         vorticity
vort/ipv0.f              Approximately calculate isentropic potential
                         vorticity
vort/ipv0_multi.f        Approximately calculate isentropic potential
                         vorticity for data file with more than one
                         records
vort/ipv_multi.f         Calculate isentropic potential vorticity for
                         data file with more than one records
vort/gstream.f           Calculate gestrophic stream function

------------------------------------------------------------------------

Stream function/velocity potential analyses

stream/bases.f           Need to be executed to make spherical harmonic
                         function files and weight function file.
                         bases_cos.dat: Spherical harmonic functions
                                        (cos-type)
                         bases_sin.dat: Spherical harmonic functions
                                        (sin-type)
                         weight.dat: Weight function
stream/uv2psi.f          Calculate stream function/velocity potential
                         from wind field
stream/uv2psi_multi.f    Calculate stream function/velocity potential
                         from wind field for data file with more than one
                         records
stream/uv2psi_layers.f   Calculate stream function/velocity potential
                         from wind field for data file with more than one
                         records at all layers
stream/xi2psi.f          Calculate stream function/velocity potential
                         from wind field vorticity/divergence
stream/lowpass.f         Low-pass/high-pass filter

------------------------------------------------------------------------

Stream function/velocity potential analyses (resolution T42)

stream42/bases.f         Need to be executed to make spherical harmonic
                         function files and weight function file.
                         bases_cos.dat: Spherical harmonic functions
                                        (cos-type)
                         bases_sin.dat: Spherical harmonic functions
                                        (sin-type)
                         weight.dat: Weight function
stream42/uv2psi.f        Calculate stream function/velocity potential
                         from wind field
stream42/xi2psi.f        Calculate stream function/velocity potential
                         from wind field vorticity/divergence

------------------------------------------------------------------------

Moisture analyses

moist/shum.f             Calculate specific humidity from temperature
                         and relative humidity field
moist/shum_multi.f       Calculate specific humidity from temperature
                         and relative humidity field
                         for data file with more than one records
moist/ept.f              Calculate equivalent potential temperature
                         from temperature and specific humidity field
moist/ept_multi.f        Calculate equivalent potential temperature
                         from temperature and specific humidity field
                         for data file with more than one records
moist/quv.f              Calculate moisture flux from temperature and
                         specific humidity and wind field
moist/quv_multi.f        Calculate moisture flux from temperature and
                         specific humidity and wind field for data file
                         with more than one records

------------------------------------------------------------------------

Dynamic budget analyses

budget/vbudget1.f        Calculate vorticity budget
budget/vbudget2.f        Calculate vorticity budget for basic and anomaly
                         fields
budget/vbudget2_multi.f  Calculate vorticity budget for basic and anomaly
                         fields for data file with more than one records
budget/rsource1.f        Calculate Rossby source term
budget/rsource1_multi.f  Calculate Rossby source term for data file with
                         more than one records
budget/rsource2.f        Calculate Rossby source term for basic and
                         anomaly fields
budget/kenergy.f         Calculate barotropic kinetic energy conversion
budget/kenergyw.f        Calculate barotropic kinetic energy conversion
                         from wind data instead of stream function
budget/kenergyd.f        Calculate barotropic kinetic energy conversion
                         for eddy components only
budget/enstrophy.f       Calculate eddy enstrophy generation
budget/waveact.f         Calculate wave activity generation

------------------------------------------------------------------------

Thermodynamic analyses

heat/hbudget1.f          Calculate heat budget
heat/hbudget1_multi.f    Calculate heat budget for data file with more
                         than one records
heat/hbudget2.f          Calculate heat budget for basic and anomaly
                         fields
heat/hbudget2s.f         Calculate heat budget for basic and anomaly
                         fields (simplified version)
heat/adia_multi.f        Calculate apparent adiabatic heating
heat/adiaconst_multi.f   Calculate apparent adiabatic heating neglecting
                         time-derivative

------------------------------------------------------------------------

Flux analyses

flux/wafd.f              Wave activity flux for eddy components only
flux/wafd_multi.f        Wave activity flux for eddy components only,
                         for data file with more than one records
flux/waf.f               Wave activity flux
flux/waf_multi.f         Wave activity flux for data file with more than
                         one records
flux/qvect.f             Q-vector

------------------------------------------------------------------------

Transient activity

trans/vortfb1.f          Calculate convergence of vorticity flux
trans/vortfb1_multi.f    Calculate convergence of vorticity flux for
                         each record
trans/vortfb2.f          Calculate convergence of eddy vorticity flux
trans/vortfb2_multi.f    Calculate convergence of eddy vorticity flux for
                         each record
trans/evect.f            Calculate E-vector
trans/waf86.f            Calculate wave activity flux (Plumb 1986)

------------------------------------------------------------------------

Surface flux

surf/oflux.f             Calculate surface heat flux over the ocean

------------------------------------------------------------------------

Statistical analysis

stat/stat.f              Calculate average/deviaion/skewness/kurtosis
stat/regr.f              Calculate regression/correlation to temporal
                         index
stat/regrlag.f           Calculate lag regression/correlation to
                         temporal index
stat/regrlags.f          Calculate lag regression/correlation to
                         temporal index for consecutive lag values
stat/locarregr.f         Calculate temporal regression/correlation
                         at each point between the two variable fields
stat/onepregr.f          Calculate one-point regression/correlation

------------------------------------------------------------------------

EOF analysis

eof/anal.f               Perform EOF analysis
                         grid.dat: a sample of grid list
eof/pcrev.f              Reverse sign of PC
eof/regr.f               Calculate regression/correlation to PCs
eof/gridshift.f          Shift grid list zonally
eof/pctimeconv_num.f     Convert time step # of pc?.dat
eof/pctimeconv_date.f    Convert time step # of pc?.dat into calendar
                         date

------------------------------------------------------------------------

Format conversion

conv/yrev.f              Reverse y-axis
conv/yrev_multi.f        Reverse y-axis for data file with more than one
                         records
conv/yrev_patch.f        Subroutine to modify a program into non-yrev
                         version
conv/land.f              Subroutine to fill land surface with missing
                         value
conv/tinterp.f           Subroutine to replace a missing value with the
                         temporally-interpolated value 
conv/endian.f            Subroutine to convert files in big (little)
                         endian into ones in little (big) endian
conv/endian42048.f       Subroutine to convert files in big (little)
                         endian into ones in little (big) endian
                         assuming that the size of the file in bytes is
                         a multiple of 42048

------------------------------------------------------------------------

Data check

check/check.f            Check missing value or non-positive value in a
                         data file

------------------------------------------------------------------------

Temporal section

hov/quick.f              Obtain temporal section
                         grid.dat: a sample of grid list
hov/hov.f                Obtain temporal section
                         grid.dat: a sample of grid list
hov/xtsec.f              Obtain zonal-time section
hov/ytsec.f              Obtain meridional-time section
hov/hov_all.f            Obtain temporal section for all input records

------------------------------------------------------------------------

Vertical structure

vertical/vconnect.f      Make 3-D data by connecting data at each
                         vertical level.
vertical/vconnect_all.f  Make 3-D data by connecting data at each
                         vertical level, for U, V, W, T, Z.
vertical/isentropic.f    Obtain data at an isentropic level.

------------------------------------------------------------------------

Vertical section

cross/cross.f            Obtain vertical section
                         grid.dat: a sample of grid list

------------------------------------------------------------------------

Index

index/index.f            Obtain index from variable field
index/gpv_daily.f        Obtain grid point value from daily variable
                         field
index/gpv_daily3.f       Obtain grid point values from three daily
                         variable fields
index/ret_daily.f        Replace a missing value with the
                         temporally-interpolated value for output of
                         gpv_daily.f
index/ret_daily3.f       Replace a missing value with the
                         temporally-interpolated value for output of
                         gpv_daily3.f
index/clim_daily.f       Calculate climatological mean for output of
                         gpv_daily.f
index/clim_daily3.f      Calculate climatological mean for output of
                         gpv_daily3.f
index/climreduc_daily.f  Calculate deviation from daily climatological
                         mean for output of gpv_daily.f
index/bandpass_daily.f   Bandpass filter for output of gpv_daily.f
index/basic.f            Obtain average and variance
index/rank.f             Obtain rank of index
index/corr.f             Calculates correlation and linear regression
                         between two indices
index/trendreduc.f       Reduce trend

------------------------------------------------------------------------

Constants

const/const.f            Constants

------------------------------------------------------------------------

Grid conversion

global2tbb.f             Covert 2.5 deg. * 2.5 deg. GrADS data into
                         1 deg. * 1 deg. TBB data format.
tbb2global.f             Covert 1 deg. * 1 deg. TBB data into
                         2.5 deg. * 2.5 deg. GrADS data format.
sample_TBB.ctl           Sample of GrADS control file for TBB data

------------------------------------------------------------------------

Script for compilation/uncompilation

compile.csh              Compile all the programs
uncompile.csh            Uncompile all the programs

------------------------------------------------------------------------

Sample of GrADS control file

sample.ctl               Sample of GrADS control file to help you
                         to understand data format

C   This program calculates vorticity/divergence from stream function/
C   velocity potential data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   F: Stream function/velocity potential
C   G: Vorticity/divergence

      REAL F(LMAX,MMAX),G(LMAX,MMAX)

C   Note: It is assumed that data do not contain missing value.

C ========================================

C   Type of calculation is selected.

C   MODE=1: Stream function -> Relative vorticity
C   MODE=2: Stream function -> Absolute vorticity
C   MODE=3: Velocity potential -> Divergence

      CALL SELEC (MODE)

C ----------------------------------------

C   File of stream function/velocity potential field is read.

      CALL RDDAT (LMAX,MMAX,F)

C ----------------------------------------

C   Calculation is operated.

      CALL NABLA (LMAX,MMAX,F,G)
      IF (MODE.EQ.2) CALL COR (LMAX,MMAX,G)
      IF (MODE.EQ.3) CALL REV (LMAX,MMAX,G)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A31,F6.1,A6)')
     +  'Input  (35 deg.N, 140 deg.E) = ', 1.0E-6*F(57,23), ' *10^6'
      WRITE(6,'(1X,A31,F6.1,A7)')
     +  'Output (35 deg.N, 140 deg.E) = ', 1.0E+6*G(57,23), ' *10^-6'

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,G)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE SELEC (MODE)
      WRITE(6,*) 'Calculation mode:'
      WRITE(6,*) '1: Stream function -> Relative vorticity,'
      WRITE(6,*) '2: Stream function -> Absolute vorticity,'
      WRITE(6,*) '3: Velocity potential -> Divergence.'
      READ (5,*) MODE
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER INFL*80
      WRITE(6,*) 'Input file ?'
      READ (5,'(A80)') INFL
      OPEN (10,FILE=INFL,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ (10,REC=1) G
      CLOSE(10)
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER OUTFL*80
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) G
      CLOSE(10)
      WRITE(6,*) 'Output was written to the file.'
      RETURN
      END

C ========================================

      SUBROUTINE NABLA (LMAX,MMAX,F,G)
      REAL F(LMAX,MMAX),G(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
        DX1 = 0.5E0*(DX+DX1)
        DX2 = 0.5E0*(DX+DX2)
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        G(L,M) = + (DX1*(F(L,M1)-F(L,M))+DX2*(F(L,M2)-F(L,M)))
     +             / (DX*DY**2)
     +           + (F(L1,M)+F(L2,M)-0.2E1*F(L,M)) / DX**2
      ELSE
        G(L,M) = 0.0E0
        IF (M.EQ.   1) MM = 2
        IF (M.EQ.MMAX) MM = MMAX-1
        DO 21 LL=1,LMAX
          G(LL,M) = G(LL,M)
     +      + 0.4E1 * (F(LL,MM)-F(LL,M)) / DY**2 / REAL(LMAX)
   21   CONTINUE
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE COR (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      DO 11 M=1,MMAX
        TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
        F  = 0.2E1 * OM * SIN(TH)
      DO 12 L=1,LMAX
        G(L,M) = G(L,M) + F
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE REV (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M) = - G(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

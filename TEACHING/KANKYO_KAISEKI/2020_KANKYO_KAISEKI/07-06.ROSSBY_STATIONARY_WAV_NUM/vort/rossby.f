C   This program calculates total wavenumber of stationary Rossby wave
C   from zonal wind data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   U:   Zonal wind
C   B:   Beta effect
C   RKS: Total wavenumber of stationary Rossby wave

      REAL U(LMAX,MMAX),B(LMAX,MMAX),RKS(LMAX,MMAX)

C   Note: It is assumed that data do not contain missing value.

C ========================================

C   File of wind field is read.

      CALL RDDAT (LMAX,MMAX,U)

C ----------------------------------------

C   Calculation is operated.

      CALL BETA  (LMAX,MMAX,U,B)
      CALL WAVE  (LMAX,MMAX,U,B,1.0E3,RKS)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A27,F6.1)')
     +  'U  (35 deg.N, 140 deg.E) = ', U(57,23)
      WRITE(6,'(1X,A27,F6.1)')
     +  'Ks (35 deg.N, 140 deg.E) = ', RKS(57,23)

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,RKS)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE RDDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER INFL*80
      WRITE(6,*) 'Zonal wind data ?'
      READ (5,'(A80)') INFL
      OPEN (10,FILE=INFL,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ (10,REC=1) G
      CLOSE(10)
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER OUTFL*80
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) G
      CLOSE(10)
      WRITE(6,*) 'Output was written to the file.'
      RETURN
      END

C ========================================

      SUBROUTINE CONST (PI,RE,OM)
      PI = 0.31415926535E1
      RE = 0.6368E7
      OM = 0.7292E-4
      RETURN
      END

      SUBROUTINE BETA (LMAX,MMAX,U,B)
      REAL U(LMAX,MMAX),B(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
      OM = 0.7292E-4
      DY = PI*RE/REAL(MMAX-1)
      DO 11 M=1,MMAX
      TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
      MM = MIN(MAX(M,2),MMAX-1)
      DO 12 L=1,LMAX
        BP = 0.2E1 * OM * COS(TH) / RE
        UYY = (U(L,MM-1)+U(L,MM+1)-0.2E1*U(L,MM))/DY**2
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        UY = 0.5E0*(U(L,M-1)-U(L,M+1))/DY
CCC
C        B(L,M) = BP - UYY
        B(L,M) = (BP - UYY
     +            + UY*TAN(TH)/RE + U(L,M)/(COS(TH)**2)/(RE**2))
     +           * COS(TH)
      ELSE
        B(L,M) = BP - UYY
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE WAVE (LMAX,MMAX,U,B,RKSMAX,RKS)
      REAL U(LMAX,MMAX),B(LMAX,MMAX),RKS(LMAX,MMAX)
      RE = 0.6368E7
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
CCC
C      IF (U(L,M).NE.0.0E0) THEN
      IF (U(L,M).GT.0.0E0) THEN
        RKS2 = B(L,M)/U(L,M)
        IF (RKS2.GE.0.0E0) THEN
          RKS(L,M) = +SQRT(+RKS2)
          RKS(L,M) = RE*RKS(L,M)
          RKS(L,M) = AMIN1(RKS(L,M),RKSMAX)
        ELSE
          RKS(L,M) = -SQRT(-RKS2)
          RKS(L,M) = RE*RKS(L,M)
          RKS(L,M) = AMAX1(RKS(L,M),-RKSMAX)
        ENDIF
      ELSE
        RKS(L,M) = RKSMAX
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

C   This program calculates wind filed from stream function/velocity
C   potential data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   G: Stream function/velocity potential
C   U: Zonal wind
C   V: Meridional wind

      REAL G(LMAX,MMAX),U(LMAX,MMAX),V(LMAX,MMAX)

C   Note: It is assumed that data do not contain missing value.

C ========================================

C   Type of calculation is selected.

C   MODE=1: Stream function -> Wind
C   MODE=2: Velocity potential -> Wind

      CALL SELEC (MODE)

C ----------------------------------------

C   File of stream function/velocity potential field is read.

      CALL RDDAT (LMAX,MMAX,G)

C ----------------------------------------

C   Calculation is operated.

      IF (MODE.EQ.1) CALL WINDR (LMAX,MMAX,G,U,V)
      IF (MODE.EQ.2) CALL WINDD (LMAX,MMAX,G,U,V)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A30,F6.1,A6)')
     +  'Input (35 deg.N, 140 deg.E) = ', 1.0E-6*G(57,23), ' *10^6'
      WRITE(6,'(1X,A30,F6.1)')
     +  'U     (35 deg.N, 140 deg.E) = ', U(57,23)
      WRITE(6,'(1X,A30,F6.1)')
     +  'V     (35 deg.N, 140 deg.E) = ', V(57,23)

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,U,V)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE SELEC (MODE)
      WRITE(6,*) 'Calculation mode:'
      WRITE(6,*) '1: Stream function -> Wind,'
      WRITE(6,*) '2: Velocity potential -> Wind.'
      READ (5,*) MODE
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      CHARACTER INFL*80
      WRITE(6,*) 'Input data ?'
      READ (5,'(A80)') INFL
      OPEN (10,FILE=INFL,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      READ(10,REC=1) G
      CLOSE(10)
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,U,V)
      REAL U(LMAX,MMAX),V(LMAX,MMAX)
      CHARACTER OUTFL*80
      WRITE(6,*) 'Zonal wind file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) U
      CLOSE(10)
      WRITE(6,*) 'Meridional wind file ?'
      READ (5,'(A80)') OUTFL
      OPEN (10,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(10,REC=1) V
      CLOSE(10)
      WRITE(6,*) 'Output was written to the files.'
      RETURN
      END

C ========================================

      SUBROUTINE WINDD (LMAX,MMAX,C,U,V)
      REAL C(LMAX,MMAX),U(LMAX,MMAX),V(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX = DX0 * SIN(PI*REAL(M-1)/REAL(MMAX-1))
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        U(L,M) = - (C(L2,M)-C(L1,M)) / (0.2E1*DX)
        V(L,M) = + (C(L,M2)-C(L,M1)) / (0.2E1*DY)
      ELSE
        U(L,M) = 0.0E0
        V(L,M) = 0.0E0
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE WINDR (LMAX,MMAX,P,U,V)
      REAL P(LMAX,MMAX),U(LMAX,MMAX),V(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX = DX0 * SIN(PI*REAL(M-1)/REAL(MMAX-1))
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        U(L,M) = + (P(L,M2)-P(L,M1)) / (0.2E1*DY)
        V(L,M) = + (P(L2,M)-P(L1,M)) / (0.2E1*DX)
      ELSE
        U(L,M) = 0.0E0
        V(L,M) = 0.0E0
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

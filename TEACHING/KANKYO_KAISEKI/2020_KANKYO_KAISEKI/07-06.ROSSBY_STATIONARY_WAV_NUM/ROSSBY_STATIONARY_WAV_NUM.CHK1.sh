#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=hgt; VAROUT=ZETA
POW=5
UNIT="[10\`a-${POW}\`ns\`a-1\`n]"; UNIT2="[km]"
MMM=DJF

LONW=0
LONE=360
LATS=-85
LATN=85

LEV=300
YMIN=9.2
YMAX=9.6
YINT=0.1

CTL1=EFFECTIVE_BETA.CTL


FIG=$(basename $0 .sh)_${MMM}_${VAROUT}${LEV}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='darkblue->deepskyblue->skyblue->white->white->gold->orange->crimson'
CLEV='-6 6 1'


cat <<EOF>$GS
'open $CTL1'

say
i=1
ie=2
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
#'set lon ${LONW} ${LONE}'
#'set lat ${LATS} ${LATN}'
'set lev ${LEV}'

'utave=uwnd'

'vtave=vwnd'



'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=1

ytop=9

xwid =  6/xmax
ywid =  6/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grid off'

'set xlopts 1 3 0.18'
'set ylopts 1 3 0.18'

#'set mproj nps'
'set map 1 1 1'
#'set mpdset mres'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'


'set gxout shaded'
'color ${CLEV} -kind ${KIND}'
#'d hcurl(utave, vtave)*1E${POW}'
'd zeta*1E${POW}'

'set xlab off'
'set ylab off'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)
x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1

say x1' 'x2' 'y1' 'y2
'xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 2 -line on'
'color ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 2 -line on'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'


x=xl-0.7
y=(yt+yb)/2
'set strsiz 0.17 0.2'
'set string 1 c 3 90'
#'draw string 'x' 'y' ${VAROUT}'

x=(xl+xr)/2
y=yt+0.2
'set string 1 c 3 0'
'set strsiz 0.15 0.2'
'draw string 'x' 'y' ${MMM} ${LEV}hPa CNTR=Z ${UNIT2} CLR=${VAROUT} ${UNIT}'



say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.9
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo

echo
ls -lh --time-style=long-iso $CTL1
echo
ls -lh --time-style=long-iso $FIG

rm -fv $GS

echo "DONE $(basename $0) $@".
echo

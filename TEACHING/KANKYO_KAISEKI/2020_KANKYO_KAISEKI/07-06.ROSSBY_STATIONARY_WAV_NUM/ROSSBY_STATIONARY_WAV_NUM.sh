#!/bin/bash
#
# Fri, 22 Jan 2021 20:06:27 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/07-05.EFFECTIVE_BETA
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
OPT=" -fpp -convert big_endian -assume byterecl"
DOPT=" -fpp -CB -traceback -fpe0 -check all"
# http://www.rcs.arch.t.u-tokyo.ac.jp/kusuhara/tips/memorandum/memo1.html

OUT=EFBETA.MON.LTM.DJF_300.bin

cat<<EOF>$nml
&para
INFL1="UWND.MON.LTM.DJF_300.bin"
INFL2="VWND.MON.LTM.DJF_300.bin"
IM=144
JM=73
LON0=0
DLON=2.5
LAT0=-90
DLAT=2.5
OUT="${OUT}"
&end
EOF

CTL=$(basename $0 .sh).CTL

cat <<EOF>$CTL
dset ^${OUT}
title monthly ltm uwnd from the NCEP Reanalysis
options byteswapped
undef -9.96921e+36
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 1 levels 300
tdef 1 linear 00Z01JAN0001 1mo
vars 10
uwnd 1 99 uwnd DJF 300
vwnd 1 99 vwnd DJF 300
zeta 1 99 REL VOR DJF 300
muy  1 99 -dudy
muyy 1 99 -d2udy2
f    1 99 CORIOLIS
beta 1 99 BETA
efbeta 1 99 EFFECTIVE BETA
LMD  1 99 WAVE LENGTH OF S R WAVE
KS   1 99 TOTAL WAVE NUM (S R WAVE)
endvars
EOF

#echo
#echo Created ${nml}.
#echo
#ls -lh --time-style=long-iso ${nml}
#echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

echo
echo $CTL
echo
ls -lh --time-style=long-iso ${CTL}
echo

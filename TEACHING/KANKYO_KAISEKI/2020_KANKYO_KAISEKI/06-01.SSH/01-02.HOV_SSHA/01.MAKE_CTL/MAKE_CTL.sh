#!/bin/bash

INDIR=/work05/manda/DATA/SSH_GEOVELO_COPERNICUS/
IN=dt_global_allsat_phy_l4_20190210_20190909.nc 
FIG=ABSV_$(basename $IN .nc).eps

LEVS="0.2 0.8 0.05"
KIND='white->lightyellow->wheat->gold->orange->tomato->red->firebrick'

HOST=$(hostname)
CWD=$(pwd)
TIMESTAMP=$(date -R)
CMD="$0 $@"
GS=$(basename $0 .sh).GS


cat <<EOF>$GS
'cc'
'sdfopen $INDIR/$IN'
'set lon 136 139'
'set lat 33.5 35.5'                                                                             
'q ctlinfo'
say result

'quit'
EOF

grads -bcp $GS
rm -v $GS

echo
echo FIG: $FIG
echo

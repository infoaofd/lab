#!/bin/bash

# Sun, 27 Dec 2020 19:05:20 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/06-01.SSH/01-01.MAP_SSHA

CTL=SSHA.CTL #$(basename $0 .sh).CTL
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).eps

LONW=110; LONE=240
LATS=20
TIME="01JAN2008 31DEC2017"
TEXT="Sea surface height anomaly"

LEVS="-0.5 0.5 0.1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
#KIND='midnightblue->deepskyblue->lightcyan->white->white->orange->red->crimson'
KIND='cyan->blue->palegreen->white->white->gold->red->magenta'
FS=2
UNIT=[m]

HOST=$(hostname)
CWD=$(pwd)
NOW=$(date -R)
CMD="$0 $@"

cat << EOF > ${GS}

# Sun, 27 Dec 2020 19:05:20 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/06-01.SSH/01-01.MAP_SSHA

'open ${CTL}'

xmax = 1
ymax = 1

ytop=9

xwid = 5.0/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye


'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'
'set lat ${LATS}'
'set time ${TIME}'
'set xlint 20'

'd sla'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'

x=xr
y=y1
'set strsiz 0.12 0.15'
'set string 1 r 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

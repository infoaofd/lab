#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/TEACHING/2020_KANKYO_KAISEKI/06-01.SSH/01-01.MAP_SSHA
#
# Revision history:
#  This file is created by /work05/manda/mybin/nbscr.sh at 19:57 on 12-27-2020.

EXE=$(basename $0 .RUN.sh).sh

ISTEP=5

#if [ $# -ne 2 ]; then
#  echo
#  echo Error in $0 : Wrong arguments.
#  echo $0 yyyymmdd1 yyyymmdd2
#  echo
#  exit 1
#fi


yyyymmdd1=$1
yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:=20150101}
yyyymmdd2=${yyyymmdd2:=20171231}

echo $yyyymmdd1 $yyyymmdd2

yyyy1=${yyyymmdd1:0:4}
  mm1=${yyyymmdd1:4:2}
  dd1=${yyyymmdd1:6:2}

yyyy2=${yyyymmdd2:0:4}
  mm2=${yyyymmdd2:4:2}
  dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}
  end=${yyyy2}/${mm2}/${dd2}

echo $start
echo $end


jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

echo "nday=" $nday

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}
  mm=${date_out:4:2}
  dd=${date_out:6:2}

  echo $yyyy $mm $dd


  ${EXE} $yyyy $mm $dd

  i=$(expr $i + $ISTEP)
done

exit 0


echo "Done $0"
echo





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

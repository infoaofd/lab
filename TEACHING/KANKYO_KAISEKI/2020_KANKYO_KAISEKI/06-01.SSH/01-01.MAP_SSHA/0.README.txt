2020-12-27_17-59 
manda@calypso
/work05/manda/DATA/SSH_GEOVELO_COPERNICUS/2003
$ grads -bcp 
Note: -c option was specified, but no command was provided

Grid Analysis and Display System (GrADS) Version 2.1.1.b0
Copyright (C) 1988-2017 by George Mason University
GrADS comes with ABSOLUTELY NO WARRANTY
See file COPYRIGHT for more information

Config: v2.1.1.b0 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids,stn geotiff shapefile cairo
Issue 'q config' command for more detailed configuration information
GX Package Initialization: Size = 8.5 11 
Running in Batch mode
ga-> sdfopen dt_global_allsat_phy_l4_20031231_20190101.nc 
Scanning self-describing file:  dt_global_allsat_phy_l4_20031231_20190101.nc
SDF file dt_global_allsat_phy_l4_20031231_20190101.nc is open as file 1
LON set to 0 360 
LAT set to -89.875 89.875 
LEV set to 0 0 
Time values set: 2003:12:31:0 2003:12:31:0 
E set to 1 1 
ga-> q ctlinfo
dset dt_global_allsat_phy_l4_20031231_20190101.nc
title DT merged all satellites Global Ocean Gridded SSALTO/DUACS Sea Surface Height L4 product and derived variables
undef -2.14748e+09
dtype netcdf
xdef 1440 linear 0.125 0.25
ydef 720 linear -89.875 0.25
zdef 1 linear 0 1
tdef 1 linear 00Z31DEC2003 1mn
vars 7
err=>err  0  t,y,x  Formal mapping error
adt=>adt  0  t,y,x  Absolute dynamic topography
ugos=>ugos  0  t,y,x  Absolute geostrophic velocity: zonal component
vgos=>vgos  0  t,y,x  Absolute geostrophic velocity: meridian component
sla=>sla  0  t,y,x  Sea level anomaly
ugosa=>ugosa  0  t,y,x  Geostrophic velocity anomalies: zonal component
vgosa=>vgosa  0  t,y,x  Geostrophic velocity anomalies: meridian component
endvars



ga-> set time 10JUN2018
Time values set: 2018:6:10:0 2018:6:10:0 
ga-> d sla
Contouring: -1 to 0.8 interval 0.2 



ga-> set lat 20        
LAT set to 20.125 20.125 
ga-> cc
ga-> set lat 20
LAT set to 20.125 20.125 
ga-> set lon 110 250  
LON set to 110 250 
ga-> set time 01MAR2003 10JUN2018
Time values set: 2003:3:1:0 2018:6:10:0 
ga-> rgbset2
ga-> set cmin -1
cmin = -1 
ga-> set cmax 1
cmax = 1 
ga-> set cint 0.1
cint = 0.1 
ga-> d sla
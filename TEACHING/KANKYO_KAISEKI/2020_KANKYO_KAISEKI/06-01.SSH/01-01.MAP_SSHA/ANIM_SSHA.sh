#!/bin/bash

ANIM=ANIM_SSHA.gif

D1=$(mydate)
convert -crop 1200x400+160+500 -density 160 FIG_PL_MAP_SSHA/* \
SHA_2015-2017.eps ${ANIM}

D2=$(mydate)

echo $D1
echo $D2

ls -lh --time-style=long-iso $ANIM

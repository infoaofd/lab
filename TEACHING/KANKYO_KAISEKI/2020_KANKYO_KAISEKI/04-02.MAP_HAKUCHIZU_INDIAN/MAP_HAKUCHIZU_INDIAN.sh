#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/TEACHING/2020_KANKYO_KAISEKI/04-02.MAP_HAKUCHIZU_INDIAN
#
# Revision history:
#  This file is created by /work05/manda/mybin/ngmt.sh at Sun, 20 Dec 2020 20:14:50 +0900.

. ./gmtpar.sh
echo "Bash script $0 starts."

range=20/150/-30/60
size=Q70/6
xanot=a30f10
yanot=a30f10
anot=${xanot}/${yanot}WSne

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi
figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
out=${figdir}/$(basename $0 .sh).ps

pscoast -R$range -J$size -Dc -B$anot -W2 -G200 -P -K -X1.5 -Y5 >$out


xoffset=
yoffset=4

export LANG=C

curdir1=$(pwd)
now=$(date)
host=$(hostname)

#time=$(ls -l ${in} | awk '{print $6, $7, $8}')
#time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.45  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF
#0 0.90  9 0 1 LM INPUT: ${in} (${time})
#0 0.75  9 0 1 LM INPUT:
#0 0.60  9 0 1 LM INPUT:

echo
#echo "INPUT : "
#ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $out
echo

echo "Done $0"

#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=hgt; VAROUT=Z
UNIT="[km]"
MMM=DJF

LONW=0
LONE=360
LATS=10
LATN=90

LEV=300
YMIN=9.2
YMAX=9.6
YINT=0.1

INDIR=/work05/manda/DATA/NCEP1.LTM/P
CTL1="air.mon.ltm.nc"
CTL2="shum.mon.ltm.nc"
CTL3="hgt.mon.ltm.nc"
CTL4="uwnd.mon.ltm.nc"
CTL5="vwnd.mon.ltm.nc"


FIG=$(basename $0 .sh)_${MMM}_${VAR}${LEV}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='blue->skyblue->white->orange->red'
CLEV='-50 -40 -30 -20 -10 0 10 20 30 40 50'



cat <<EOF>$GS
'sdfopen ${INDIR}/$CTL1'
'sdfopen ${INDIR}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'
'sdfopen ${INDIR}/$CTL5'


say
i=1
ie=5
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
#'set lon ${LONW} ${LONE}'
#'set lat ${LATS} ${LATN}'
'set lev ${LEV}'

'set t 12'
'utave=hgt.3'
'set t 1'
'utave=utave+hgt.3'
'set t 2'
'utave=utave+hgt.3'

'utave=utave/3'
#'ucsc=ave(utave,lat=${LATS},lat=${LATN})'


'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=1

ytop=9

xwid =  6/xmax
ywid =  6/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grid off'

'set xlopts 1 3 0.18'
'set ylopts 1 3 0.18'

'set mproj nps'
'set map 1 1 1'
#'set mpdset mres'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'

'set gxout contour'
'set ccolor 1'
'set cthick 3'
'set cint 0.1'
'set clab on'
'set clskip 3'
'set cstyle 1'
'set clopts 1 3 0.12'
#'set cint 0.1'

'set frame off'
'set grid on'

'd utave/1000'

'set font 0'
'CIRCLON.GS 30'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
#'color -levs ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 1'


x=xl-0.7
y=(yt+yb)/2
'set strsiz 0.17 0.2'
'set string 1 c 3 90'
#'draw string 'x' 'y' ${VAROUT} ${UNIT}'

x=(xl+xr)/2
y=yt+0.5
'set string 1 c 3 0'
'set strsiz 0.17 0.2'
'draw string 'x' 'y' ${MMM} ${VAROUT}${LEV} ${UNIT}'



say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.9
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

dset ^EFBETA.MON.LTM.DJF_300.bin
title monthly ltm uwnd from the NCEP Reanalysis
options byteswapped
undef -9.96921e+36
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 1 levels 300
tdef 1 linear 00Z01JAN0001 1mo
vars 8
uwnd 1 99 uwnd DJF 300
vwnd 1 99 vwnd DJF 300
zeta 1 99 REL VOR DJF 300
muy  1 99 -dudy
muyy 1 99 -d2udy2
f    1 99 CORIOLIS
beta 1 99 BETA
efbeta 1 99 EFFECTIVE BETA
endvars

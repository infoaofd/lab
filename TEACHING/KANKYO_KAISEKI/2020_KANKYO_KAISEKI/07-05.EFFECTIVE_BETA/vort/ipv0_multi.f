C   This program _approximately_ calculates isentropic potential
C   vorticity (IPV) on a pressure surface from stream function and
C   temperature data in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   PS: Stream function
C   T:  Temperature

      REAL PS(LMAX,MMAX),TU(LMAX,MMAX),TL(LMAX,MMAX)

C   ZT: Absolute vorticity
C   TP: Pressure-derivative of potential temperature
C   Q:  Isentropic potential vorticity [PVU = 10^-6 K m^2 /kg s]

      REAL ZT(LMAX,MMAX),TP(LMAX,MMAX),Q(LMAX,MMAX)

C   Note: Exactly speaking, IPV should be calculated on an isentropic
C         surface. At this point, calculation here is approximate.

C ========================================

C   Pressure levels are specified.

      CALL STLEV (IPRU,IPRC,IPRL,PRU,PRC,PRL)

C ----------------------------------------

C   Files are opened.

      CALL FOPEN (LMAX,MMAX,IPRU,IPRC,IPRL)

C ----------------------------------------

      I = 1
   11 CONTINUE

C ----------------------------------------

C   Files of variable field are read.

      CALL RDDAT (LMAX,MMAX,I,NEND,PS,TU,TL)

      IF (NEND.EQ.1) GO TO 19

C ----------------------------------------

C   Calculation of horizontal vorticity

      CALL NABLA (LMAX,MMAX,PS,ZT)
      CALL COR   (LMAX,MMAX,ZT)

C ----------------------------------------

C   Calculation of stratification

      CALL STRAT (LMAX,MMAX,PRU,PRL,TU,TL,TP)

C ----------------------------------------

C   Calculation of isentropic potential vorticity

      CALL ERTEL (LMAX,MMAX,ZT,TP,Q)
      CALL MULT  (LMAX,MMAX,1.0E6,Q)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A11,I5,A1,1X,A31,F6.1)')
     +  'Record # = ', I,',',
     +  'Output (35 deg.N, 140 deg.E) = ', Q (57,23)

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,I,Q)

C ----------------------------------------

      I = I + 1
      GO TO 11
   19 CONTINUE

      CLOSE(10)
      CLOSE(11)
      CLOSE(12)
      CLOSE(13)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE STLEV (IPRU,IPRC,IPRL,PRU,PRC,PRL)
      WRITE(6,*) 'Pressure levels (upper, center, lower) [hPa] (I) ?'
      READ (5,*) IPRU,IPRC,IPRL
      PRU = 1.0E2 * REAL(IPRU)
      PRC = 1.0E2 * REAL(IPRC)
      PRL = 1.0E2 * REAL(IPRL)
      RETURN
      END

      SUBROUTINE FOPEN (LMAX,MMAX,IPRU,IPRC,IPRL)
      CHARACTER INFL1*80,INFL2*80,INFL3*80,CHAR3*3,OUTFL*80
      INFL1 = 'P'//CHAR3(IPRC)//'.dat'
      INFL2 = 'T'//CHAR3(IPRU)//'.dat'
      INFL3 = 'T'//CHAR3(IPRL)//'.dat'
      WRITE(6,*) 'Input files are automatically designated:'
  101 FORMAT (1X,A25,1X,A8)
      WRITE(6,101) 'Steam function (center) =',INFL1
      WRITE(6,101) 'Temperature (upper)     =',INFL2
      WRITE(6,101) 'Temperature (lower)     =',INFL3
      OPEN(10,FILE=INFL1,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      OPEN(11,FILE=INFL2,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      OPEN(12,FILE=INFL3,
     +     STATUS='OLD',FORM='UNFORMATTED',
     +     ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (13,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,I,NEND,PS,TU,TL)
      REAL PS(LMAX,MMAX),TU(LMAX,MMAX),TL(LMAX,MMAX)
      CHARACTER INFL1*80,INFL2*80,INFL3*80,CHAR3*3
      NEND = 0
      READ(10,REC=I,ERR=19) PS
      READ(11,REC=I,ERR=19) TU
      READ(12,REC=I,ERR=19) TL
      RETURN
   19 NEND=1
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,I,G)
      REAL G(LMAX,MMAX)
      WRITE(13,REC=I) G
      RETURN
      END

      CHARACTER*3 FUNCTION CHAR3 (I)
      CHARACTER CHAR*1
      CHAR3 = CHAR(48+MOD(I/100,10))//CHAR(48+MOD(I/10 ,10))
     +      //CHAR(48+MOD(I    ,10))
      RETURN
      END

C ========================================

      SUBROUTINE NABLA (LMAX,MMAX,F,G)
      REAL F(LMAX,MMAX),G(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
        DX1 = 0.5E0*(DX+DX1)
        DX2 = 0.5E0*(DX+DX2)
      DO 12 L=1,LMAX
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        G(L,M) = + (DX1*(F(L,M1)-F(L,M))+DX2*(F(L,M2)-F(L,M)))
     +             / (DX*DY**2)
     +           + (F(L1,M)+F(L2,M)-0.2E1*F(L,M)) / DX**2
      ELSE
        G(L,M) = 0.0E0
        IF (M.EQ.   1) MM = 2
        IF (M.EQ.MMAX) MM = MMAX-1
        DO 21 LL=1,LMAX
          G(LL,M) = G(LL,M)
     +      + 0.4E1 * (F(LL,MM)-F(LL,M)) / DY**2 / REAL(LMAX)
   21   CONTINUE
      ENDIF
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE COR (LMAX,MMAX,G)
      REAL G(LMAX,MMAX)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      DO 11 M=1,MMAX
        TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
        F  = 0.2E1 * OM * SIN(TH)
      DO 12 L=1,LMAX
        G(L,M) = G(L,M) + F
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE STRAT (LMAX,MMAX,PRU,PRL,TU,TL,TP)
      REAL TU(LMAX,MMAX),TL(LMAX,MMAX),TP(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        THU = TU(L,M) / (PRU/1.0E5)**(0.2E1/0.7E1)
        THL = TL(L,M) / (PRL/1.0E5)**(0.2E1/0.7E1)
        TP(L,M) = (THU-THL) / (PRU-PRL)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ERTEL (LMAX,MMAX,ZT,TP,Q)
      REAL ZT(LMAX,MMAX),TP(LMAX,MMAX),Q(LMAX,MMAX)
      GC = 0.9807E1
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        Q(L,M) = - GC * ZT(L,M) * TP(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE MULT (LMAX,MMAX,C,G)
      REAL G(LMAX,MMAX)
      DO 11 M=1,MMAX
      DO 12 L=1,LMAX
        G(L,M) = C * G(L,M)
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

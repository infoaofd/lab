C   This program calculates vorticity/divergence from wind data
C   which contain missing values in so-called GrADS format.

C   LMAX: Number of zonal grids
C   MMAX: Number of meridional grids
C   LMAX=144, MMAX=73 for 2.5 deg. * 2.5 deg. data set.

      PARAMETER (LMAX=144,MMAX=73)

C   U: Zonal wind
C   V: Meridional wind
C   G: Vorticity/divergence

      REAL U(LMAX,MMAX),V(LMAX,MMAX),G(LMAX,MMAX)

C   Please specify missing value.

      PARAMETER (RMISS=-0.999E+34)

C ========================================

C   Type of calculation is selected.

C   MODE=1: Wind -> Relative vorticity
C   MODE=2: Wind -> Absolute vorticity
C   MODE=3: Wind -> Divergence

      CALL SELEC (MODE)

C ----------------------------------------

C   Files are opened.

      CALL FOPEN (LMAX,MMAX)

C ----------------------------------------

      I = 1
   11 CONTINUE

C ----------------------------------------

C   Files of wind field are read.

      CALL RDDAT (LMAX,MMAX,I,NEND,U,V)

      IF (NEND.EQ.1) GO TO 19

C ----------------------------------------

C   Calculation is operated.

      IF (MODE.EQ.1) CALL ROT (LMAX,MMAX,RMISS,U,V,G)
      IF (MODE.EQ.2) THEN
        CALL ROT (LMAX,MMAX,RMISS,U,V,G)
        CALL COR (LMAX,MMAX,RMISS,G)
      ENDIF
      IF (MODE.EQ.3) CALL DIV (LMAX,MMAX,RMISS,U,V,G)

C ----------------------------------------

C   Data are monitored.

      WRITE(6,'(1X,A11,I5,A1,1X,A31,F6.1,A7)')
     +  'Record # = ', I,',',
     +  'Output (35 deg.N, 140 deg.E) = ', 1.0E6*G(57,23), ' *10^-6'

C ----------------------------------------

C   Output data are written down.

      CALL WRDAT (LMAX,MMAX,I,G)

C ----------------------------------------

      I = I + 1
      GO TO 11
   19 CONTINUE

      CLOSE(10)
      CLOSE(11)
      CLOSE(12)

C ----------------------------------------

      STOP
      END

C ========================================
C   Subroutines
C ========================================

      SUBROUTINE SELEC (MODE)
      WRITE(6,*) 'Calculation mode:'
      WRITE(6,*) '1: Wind -> Relative vorticity,'
      WRITE(6,*) '2: Wind -> Absolute vorticity,'
      WRITE(6,*) '3: Wind -> Divergence.'
      READ (5,*) MODE
      RETURN
      END

      SUBROUTINE FOPEN (LMAX,MMAX)
      CHARACTER INFLU*80,INFLV*80,OUTFL*80
      WRITE(6,*) 'Zonal wind data ?'
      READ (5,'(A80)') INFLU
      OPEN (10,FILE=INFLU,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(6,*) 'Meridional wind data ?' 
      READ (5,'(A80)') INFLV
      OPEN (11,FILE=INFLV,
     +      STATUS='OLD',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      WRITE(6,*) 'Output file ?'
      READ (5,'(A80)') OUTFL
      OPEN (12,FILE=OUTFL,
     +      STATUS='UNKNOWN',FORM='UNFORMATTED',
     +      ACCESS='DIRECT',RECL=4*(LMAX*MMAX))
      RETURN
      END

      SUBROUTINE RDDAT (LMAX,MMAX,I,NEND,U,V)
      REAL U(LMAX,MMAX),V(LMAX,MMAX)
      NEND=0
      READ (10,REC=I,ERR=19) U
      READ (11,REC=I,ERR=19) V
      RETURN
   19 NEND=1
      RETURN
      END

      SUBROUTINE WRDAT (LMAX,MMAX,I,G)
      REAL G(LMAX,MMAX)
      WRITE(12,REC=I) G
      RETURN
      END

C ========================================

      SUBROUTINE DIV (LMAX,MMAX,RMISS,U,V,D)
      REAL U(LMAX,MMAX),V(LMAX,MMAX),D(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
      DO 12 L=1,LMAX
        D(L,M) = RMISS
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        IF (U(L1,M).EQ.RMISS) GO TO 19
        IF (U(L2,M).EQ.RMISS) GO TO 19
        IF (V(L,M1).EQ.RMISS) GO TO 19
        IF (V(L,M2).EQ.RMISS) GO TO 19
        D(L,M) = - (DX2*V(L,M2)-DX1*V(L,M1)) / (0.2E1*DX*DY)
     +           + (U(L2,M)-U(L1,M)) / (0.2E1*DX)
      ELSE
        DXX1 = 0.5E0*DX1
        DXX2 = 0.5E0*DX2
        DXX  = 0.5E0*(DXX1+DXX2)
        DYY  = 0.5E0*DY
        IF (V(L,M1).EQ.RMISS) GO TO 19
        IF (V(L,M2).EQ.RMISS) GO TO 19
        D(L,M) = - (DXX2*V(L,M2)-DXX1*V(L,M1)) / (DXX*DYY)
      ENDIF
   19 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE ROT (LMAX,MMAX,RMISS,U,V,R)
      REAL U(LMAX,MMAX),V(LMAX,MMAX),R(LMAX,MMAX)
      PI = 0.31415926535E1
      RE = 0.6368E7
        DX0 = 0.2E1 * PI * RE / REAL(LMAX)
        DY  = PI * RE / REAL(MMAX-1)
      DO 11 M=1,MMAX
      M1 = MAX(M-1,   1)
      M2 = MIN(M+1,MMAX)
        DX  = DX0 * SIN(PI*REAL(M -1)/REAL(MMAX-1))
        DX1 = DX0 * SIN(PI*REAL(M1-1)/REAL(MMAX-1))
        DX2 = DX0 * SIN(PI*REAL(M2-1)/REAL(MMAX-1))
      DO 12 L=1,LMAX
        R(L,M) = RMISS
      L1 = MOD(L+LMAX-1-1,LMAX)+1
      L2 = MOD(L+LMAX-1+1,LMAX)+1
      IF ((M.NE.1).AND.(M.NE.MMAX)) THEN
        IF (U(L,M1).EQ.RMISS) GO TO 19
        IF (U(L,M2).EQ.RMISS) GO TO 19
        IF (V(L1,M).EQ.RMISS) GO TO 19
        IF (V(L2,M).EQ.RMISS) GO TO 19
        R(L,M) = + (DX2*U(L,M2)-DX1*U(L,M1)) / (0.2E1*DX*DY)
     +           + (V(L2,M)-V(L1,M)) / (0.2E1*DX)
      ELSE
        DXX1 = 0.5E0*DX1
        DXX2 = 0.5E0*DX2
        DXX  = 0.5E0*(DXX1+DXX2)
        DYY  = 0.5E0*DY
        IF (U(L,M1).EQ.RMISS) GO TO 19
        IF (U(L,M2).EQ.RMISS) GO TO 19
        R(L,M) = + (DXX2*U(L,M2)-DXX1*U(L,M1)) / (DXX*DYY)
      ENDIF
   19 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

      SUBROUTINE COR (LMAX,MMAX,RMISS,G)
      REAL G(LMAX,MMAX)
      PI = 0.31415926535E1
      OM = 0.7292E-4
      DO 11 M=1,MMAX
        TH = PI*(0.5E0-REAL(M-1)/REAL(MMAX-1))
        F  = 0.2E1 * OM * SIN(TH)
      DO 12 L=1,LMAX
      IF (G(L,M).NE.RMISS)
     +  G(L,M) = G(L,M) + F
   19 CONTINUE
   12 CONTINUE
   11 CONTINUE
      RETURN
      END

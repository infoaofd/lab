#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
UNIT="[m/s]"
MMM=JAN

DATETIME=00Z01${MMM}0001
LONW=0
LONE=360
LATS=-90
LATN=90
LEVB=1000
LEVT=100
LEV2=850
LON=135

INDIR=/work05/manda/DATA/NCEP1.LTM/P
CTL1="air.mon.ltm.nc"
CTL2="shum.mon.ltm.nc"
CTL3="hgt.mon.ltm.nc"
CTL4="uwnd.mon.ltm.nc"
CTL5="vwnd.mon.ltm.nc"


FIG=$(basename $0 .sh)_${MMM}_${VAR}${LEV}_PT${LEV2}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='darkblue->skyblue->white->orange->darkred'
CLEV='-30 -25 -20 -15 -10 -5 -1 0 1 5 10 15 20 25 30'

cat <<EOF>$GS
'sdfopen ${INDIR}/$CTL1'
'sdfopen ${INDIR}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'
'sdfopen ${INDIR}/$CTL5'


say
i=1
ie=5
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEVB} ${LEVT}'
'set time ${DATETIME}'


'set lon ${LON}'
'ucsc=uwnd.4'



'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=2

ytop=9

xwid =  7/xmax
ywid =  6/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shaded'
'color -levs ${CLEV} -kind ${KIND}'
'd ucsc'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
'color -levs ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 1'


'set gxout contour'
'set ccolor 0'
'set cthick 1'
'set clevs ${CLEV}'
'set clab off'
'set xlab off'
'set ylab off'
'd ucsc'

x=xl-0.7
y=(yt+yb)/2
'set strsiz 0.14 0.17'
'set string 1 c 3 90'
'draw string 'x' 'y' P[hPa]'
'set string 1 c 1 0'

'draw title ${MMM} ${VAR} ${UNIT} ${LON}E'



say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'



xmap=1; ymap=2
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set time ${DATETIME}'
'set lev ${LEVB} ${LEVT}'
'set lon ${LON}'
'theta=air.1*pow((1000/lev),287/1004)'

'set lev ${LEV2}'
'tcsc=theta'
'set gxout line'
'set grid off'
'set xlab on'
'set ylab on'
'set cmark 0'
'set ccolor 1'
'set cthick 7'
'set ylpos 0 l'
'd tcsc'


'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo
rm -rv $GS

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

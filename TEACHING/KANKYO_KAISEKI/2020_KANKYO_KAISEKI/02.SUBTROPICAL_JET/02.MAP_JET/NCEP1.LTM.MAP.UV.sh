#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
UNIT="[m/s]"
MMM=NOV

DATETIME=00Z01${MMM}0001
LONW=0
LONE=360
LATS=0
LATN=90
LEV=200
#LEVB=1000
#LEVT=100
#LEV2=850
#LON=135

INDIR=/work05/manda/DATA/NCEP1.LTM/P
CTL1="air.mon.ltm.nc"
CTL2="shum.mon.ltm.nc"
CTL3="hgt.mon.ltm.nc"
CTL4="uwnd.mon.ltm.nc"
CTL5="vwnd.mon.ltm.nc"


FIG=$(basename $0 .sh)_${MMM}_${VAR}.eps #_PT${LEV2}.eps

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='cyan->blue->palegreen->white->gold->red->magenta'
CLEV='-50 -40 -30 -20 -10 10 20 30 40 50'

cat <<EOF>$GS
'sdfopen ${INDIR}/$CTL1'
'sdfopen ${INDIR}/$CTL2'
'sdfopen ${INDIR}/$CTL3'
'sdfopen ${INDIR}/$CTL4'
'sdfopen ${INDIR}/$CTL5'


say
i=1
ie=5
while ( i <= ie)

say 'FILE: 'i

'q ctlinfo 'i
if (i = 1)
say result
endif

if (i>1)
say sublin(result,10)
say sublin(result,11)
say
endif

i=i+1
endwhile


'cc'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEV}' ;#${LEVB} ${LEVT}'
'set time ${DATETIME}'
'set grid off'


#'set lon ${LON}'
'u=uwnd.4'
'v=vwnd.5'



'set vpage 0.0 8.5 0 10.5'

xmax=1 
ymax=2

ytop=9

xwid =  7/xmax
ywid =  7/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shaded'
'color -levs ${CLEV} -kind ${KIND}'
'set ylint 30'
'd u'

'set xlab off'
'set ylab off'


'set gxout vector'
'set cthick 10'
'set ccolor  0'
'vec.gs skip(maskout(u,mag(u,v)-10),5);v  -SCL 0.5 50 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

xx=xr-0.65
yy=yt+0.35
'set cthick  2'
'set ccolor  1'
'vec.gs skip(maskout(u,mag(u,v)-10),5);v  -SCL 0.5 50 -P 'xx' 'yy' -SL m/s'


'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'color -levs ${CLEV} -kind ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 3 -fs 1'

x1=xl; x2=xr-0.5
y1=yb-0.5; y2=y1
'set strsiz 0.13 0.16'
'set string 1 c 4 0'
'draw string 'x2' 'y2' U [m/s]'


x=(xl+xr)/2
y=yt+0.2
'set strsiz 0.2 0.23'
'set string 1 c 4 0'
'draw string 'x' 'y'  ${MMM} ${LEV} hPa'





say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*

'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo
rm -rv $GS

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

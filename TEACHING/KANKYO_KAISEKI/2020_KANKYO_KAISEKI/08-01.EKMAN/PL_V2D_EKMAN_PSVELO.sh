#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/TEACHING/2020_KANKYO_KAISEKI/08-01.EKMAN
#
# Revision history:
#  This file is created by /work05/manda/mybin/ngmt.sh at Tue, 09 Feb 2021 20:08:28 +0900.

. ./gmtpar.sh
echo "Bash script $0 starts."

range=-1/0.5/-0.5/1
size=X4
xanot=a.5f.5
yanot=a.5f.5
anot=${xanot}/${yanot}WSne
in=EKMAN.TXT
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
out=${figdir}/$(basename $0 .sh)_$(basename $in .TXT).ps

PI=3.1415926535

CPT=$(basename $0 .sh).CPT
makecpt -Cpanoply -I -T0/4/0.2 > $CPT


# https://hydrocoast.jp/index.php?GMT/psvelo

# -Se velscale / confidence / fontsize
sc=2.65 # 1->1 inch
conf=0.0
fs=12

# -A LineWidth / HeadLength / HeadSize
LW=0.01
HL=0.15
HS=0.05

#awk '{print 0,0,$2,$3}' $in |\
#psvelo -R$range -J$size -A0.01/0.15/0.05 -Se2.65/1/0 \



awk '{print 0,0,$2,$3}' $in |\
psvelo -J$size -R$range -B$anot -A$LW/$HL/$HS -Gblack -Se$sc/$conf/0 \
-X1.5 -Y5 -K -P >$out


# scale (outside)
psvelo -J -R -A$LW/$HL/$HS -Gblack -Se$sc/$conf/$fs -O -K <<EOF >> $out
-0  2.80  1.00  0.00  0.00  0.00  0.0  1.0 [unit]
EOF


awk '{print $2,$3}' $in |\
psxy -R$range -J$size -B$anot -W3 -K -O >>$out



xoffset=
yoffset=4

export LANG=C

curdir1=$(pwd)
now=$(date)
host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${in} (${time})
0 0.75  9 0 1 LM INPUT:
0 0.60  9 0 1 LM INPUT:
0 0.45  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $out
echo

echo "Done $0"

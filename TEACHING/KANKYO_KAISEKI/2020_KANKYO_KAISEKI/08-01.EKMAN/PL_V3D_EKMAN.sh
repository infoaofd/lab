#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/TEACHING/2020_KANKYO_KAISEKI/08-01.EKMAN
#
# Revision history:
#  This file is created by /work05/manda/mybin/ngmt.sh at Tue, 09 Feb 2021 20:08:28 +0900.

. ./gmtpar.sh
echo "Bash script $0 starts."
gmtset ANNOT_FONT_SIZE_PRIMARY = 20p
gmtset LABEL_FONT_SIZE         = 30p
range=-1/0.5/-0.5/1
range3d=${range}/-4/0

size=X4
sizez=4

xanot=a.5f.5
yanot=a.5f.5
zanot=a1f1
anot=${xanot}/${yanot}WSne
anot3d=${xanot}:"x":/${yanot}:"y":/${zanot}:"z":SnEwZ+

in=EKMAN.TXT
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
out=${figdir}/$(basename $0 .sh)_$(basename $in .TXT).ps

PI=3.1415926535

CPT=$(basename $0 .sh).CPT
makecpt -Cpanoply -I -T-4/0/0.2 > $CPT

# 1 => 4 inch 
uscl=2.65
ulegmag=1
ufac=$(echo "scale=5; $uscl/$ulegmag" | bc)

aspect=1

# https://sites.google.com/site/afcgmt/home/tips/vector
# http://kdo.la.coocan.jp/gmt26_psxy.html


awk -v uf=$ufac -v pi=${PI} '{ \
printf "%12.6f %12.6f %12.6f %12.6f %12.6f %12.6f\n", \
0,0,$1,$1,(180.0/pi)*atan2($3,$2),sqrt($2*$2+$3*$3)*uf}' $in | \
psxyz  -R$range3d -J${size} -JZ${sizez} -Sv0.03/0.3/0.05n0.2 \
 -G0 -E100/30 \
  -B${anot3d} \
-C$CPT \
  -X1.5 -Y2 -K -P >$out


# https://sites.google.com/site/afcgmt/home/turotial
# psscaleに必要なオプション
xpos=2
ypos=-0.5
length=4
width=0.1
zint=0.1
psscale -D${xpos}/${ypos}/${length}/${width}h -Ba1f0.5:z: -C${CPT} -O -K >>${out}


xoffset=
yoffset=6.5

export LANG=C

curdir1=$(pwd)
now=$(date)
host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${in} (${time})
0 0.75  9 0 1 LM INPUT:
0 0.60  9 0 1 LM INPUT:
0 0.45  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF

rm -v $CPT

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $out
echo

echo "Done $0"

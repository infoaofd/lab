#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/TEACHING/2020_KANKYO_KAISEKI/08-01.EKMAN
#
# Revision history:
#  This file is created by /work05/manda/mybin/ngmt.sh at Tue, 09 Feb 2021 20:08:28 +0900.

. ./gmtpar.sh
echo "Bash script $0 starts."

range=-1/1/-4/0
size=X2.5
xanot=a.5f.5
yanot=a2f1
anotu=${xanot}:"u":/${yanot}:z:WSne
anotv=${xanot}:"v":/${yanot}:z:WSne

in=EKMAN.TXT
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
out=${figdir}/$(basename $0 .sh)_$(basename $in .TXT).ps

PI=3.1415926535


# 1 => 4 inch 
uscl=2.65
ulegmag=1
ufac=$(echo "scale=5; $uscl/$ulegmag" | bc)

aspect=1

# https://sites.google.com/site/afcgmt/home/tips/vector
# http://kdo.la.coocan.jp/gmt26_psxy.html

awk -v uf=$ufac -v pi=${PI} \
'{ \
printf "%12.6f %12.6f\n", \
$2,$1}' $in| \
psxy -R$range -J$size -B$anotu -W3 \
-X1.5 -Y5 -K -P >$out

awk -v uf=$ufac -v pi=${PI} \
'{ \
printf "%12.6f %12.6f\n", \
$3,$1}' $in| \
psxy -R$range -J$size -B$anotv -W3 \
-X3.5 -Y0 -O -K >>$out



xoffset=-4.5
yoffset=4

export LANG=C

curdir1=$(pwd)
now=$(date)
host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${in} (${time})
0 0.75  9 0 1 LM INPUT:
0 0.60  9 0 1 LM INPUT:
0 0.45  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $out
echo

echo "Done $0"

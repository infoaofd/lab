#!/bin/bash
#
# Tue, 09 Feb 2021 19:52:07 +0900
# calypso.bosai.go.jp
# /work05/manda/TEACHING/2020_KANKYO_KAISEKI/08-01.EKMAN
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
OPT=" -fpp -convert big_endian -assume byterecl"
DOPT=" -fpp -CB -traceback -fpe0 -check all"
# http://www.rcs.arch.t.u-tokyo.ac.jp/kusuhara/tips/memorandum/memo1.html

cat<<EOF>$nml
&para
&end
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: "
echo "END:   "
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: "
echo "END:   "

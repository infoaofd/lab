    0.0000    -0.707     0.707
   -0.1000    -0.573     0.700
   -0.2000    -0.452     0.682
   -0.3000    -0.346     0.655
   -0.4000    -0.252     0.621
   -0.5000    -0.171     0.582
   -0.6000    -0.101     0.539
   -0.7000    -0.042     0.495
   -0.8000     0.007     0.449
   -0.9000     0.046     0.404
   -1.0000     0.078     0.359
   -1.1000     0.103     0.317
   -1.2000     0.121     0.276
   -1.3000     0.134     0.237
   -1.4000     0.142     0.201
   -1.5000     0.146     0.169
   -1.6000     0.147     0.139
   -1.7000     0.145     0.111
   -1.8000     0.140     0.087
   -1.9000     0.134     0.066
   -2.0000     0.127     0.047
   -2.1000     0.118     0.031
   -2.2000     0.109     0.017
   -2.3000     0.100     0.006
   -2.4000     0.091    -0.004
   -2.5000     0.081    -0.012
   -2.6000     0.072    -0.018
   -2.7000     0.063    -0.023
   -2.8000     0.055    -0.026
   -2.9000     0.047    -0.028
   -3.0000     0.040    -0.030
   -3.1000     0.033    -0.031
   -3.2000     0.027    -0.030
   -3.3000     0.022    -0.030
   -3.4000     0.017    -0.029
   -3.5000     0.013    -0.027
   -3.6000     0.009    -0.026
   -3.7000     0.006    -0.024
   -3.8000     0.003    -0.022
   -3.9000     0.001    -0.020
   -4.0000    -0.001    -0.018

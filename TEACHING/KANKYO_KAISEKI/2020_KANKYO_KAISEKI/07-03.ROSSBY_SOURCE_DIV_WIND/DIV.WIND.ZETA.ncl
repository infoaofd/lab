;*************************************************
; /work05/manda/TEACHING/2020_KANKYO_KAISEKI/07-03.ROSSBY_SOURCE_DIV_WIND
; https://www.ncl.ucar.edu/Document/Functions/Built-in/dv2uvf.shtml
; https://www.ncl.ucar.edu/Document/Functions/Built-in/vr2uvf.shtml
;*************************************************
;
; These files are loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"    
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" 
;*************************************************
begin

  f1    = addfile ("UWND.MON.LTM.DJF_300.nc", "r")
  f2    = addfile ("VWND.MON.LTM.DJF_300.nc", "r")
  u    = f1->utave
  v    = f2->vtave
  lat  = f1->lat
  lon  = f1->lon

  dv    = new ( dimsizes(u), typeof(u))  	; divergence
  ud    = new ( dimsizes(dv),typeof(dv))	; zonal divergent wind 
  vd    = new ( dimsizes(dv),typeof(dv))	; meridional divergent wind
  zeta  = new ( dimsizes(u), typeof(u))         ; vorticity  (relative)

  uv2dvf   (u,v,dv)          ; u,v ==> divergence
  dv2uvf   (dv,ud,vd)        ; dv  ==> divergent wind components
  uv2vrf   (u,v,zeta)        ; u,v ==> vorticity (rel)

  copy_VarCoords(u, ud ) 
  copy_VarCoords(u, vd ) 
  copy_VarCoords(u, zeta ) 

;  ud@long_name  = "Zonal Divergent Wind"
;  ud@units      = u@units
;  vd@long_name  = "Meridional Divergent Wind"
;  vd@units      = v@units
;*************************************************
; plot results
;*************************************************    
  wks  = gsn_open_wks("eps","DIV.WIND.ZETA.LTM.DJF.300")           ; send graphics to PNG file

  cres = True              
  cres@gsnDraw                 = False
  cres@gsnFrame                = False                               
  cres@cnFillOn = True
  cres@cnLineLabelsOn = False
  cres@gsnCenterString   = "Divergent Wind DJF 300 hPa"
  cres@gsnCenterStringOrthogonalPosF = 0.05
  cres@gsnLeftStringFontHeightF=16

  res                 = True
  res@gsnDraw                 = False
  res@gsnFrame                = False                               

  res@vcRefMagnitudeF = 3.                    ; make vectors larger
  res@vcRefLengthF    = 0.050                 ; reference vector length
;  res@vcGlyphStyle    = "CurlyVector"         ; turn on curly vectors
  res@vcMinDistanceF  = 0.012                 ; thin the vectors


zeta=zeta*1E5
  cplot = gsn_csm_contour_map(wks,zeta,cres)
  vplot= gsn_csm_vector(wks,ud,vd,res)

  overlay(cplot,vplot)
  draw(cplot)
  frame(wks)

end


#!/bin/bash
# Tue, 05 Nov 2024 08:29:10 +0900
# /work09/$(whoami)/24.00.TEACHING/KANKYOU_KAISEKI/2024-11-05_VPR_RHO

function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

YMDHM1=$1; YMDHM1=${YMDHM1:-202108120200}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}

LONW=140 ;#LONE=360 ; 
LATS=0 ;LATN=80
LEV1=1; LEV2=30
FIG=$(basename $0 .sh)_LON${LONW}.PDF
TEXT="TEMPERATURE (ANNUAL MEAN) ${LONW}E"
#TIME=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}

INDIR=/work03/DATA/NCEP2/LTM.MON
INFLE1=RH_LTM_MON_1991-2020_Z-COORD.nc
IN1=${INDIR}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi
#INFLE2=P_LTM_MON_1991-2020_Z-COORD.nc 
#IN2=${INDIR}/${INFLE2}
#if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

CTL1=$(basename $0 .sh).CTL
cat <<EOF>$CTL1
dset $IN1
title Monthly NCEP/DOE Reanalysis 2
options yrev
undef -9.96921e+36
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 30 linear 1 1
tdef 12 linear 00Z01JAN0001 1mo
vars 1
rh=>rh 30  t,z,y,x  Long Term Mean Monthly RH on Height Levels
endvars
EOF

# LEV=
GS=$(basename $0 .sh).GS

# NetCDF OUT
# https://gitlab.com/infoaofd/lab/-/tree/master/00.SKILL/00.TOOL/GRADS/GrADS_RECIPE/GrADS_NETCDF書き出し
#NC=$(basename $0 .sh).nc


LEVS="0 90 10"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
KIND='white->lavender->cornflowerblue->lime->yellow->orange->red->magenta'
# KIND='midnightblue->deepskyblue->paleturquoise->white->orange->red->magenta'
FS1=2; FS2=1
UNIT=% #kg/m3

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';say;'q ctlinfo';say sublin(result,1) 
#'sdfopen ${IN2}';say;'q ctlinfo';say sublin(result,1) 
say

'set lon $LONW'
'set lat $LATS $LATN';
'set lev ${LEV1} ${LEV2}'
'q dims';say result; say ;#sublin(result,1);say sublin(result,3)1

#'temp=tkgt.1'
#'tc=temp-273.15'
#'rh=rhum.2'

#'es= 6.112*exp((17.67*tc)/(tc+243.5))'     ;# [hPa]
# Eq.10 of Bolton (1980)
#'e=0.01*rh*es'                             ;# [hPa]
# Eq.4.1.5 (p. 108) of Emanuel (1994)

#'QV=0.62197*(e/(lev-e))' ;# [kg/kg]
#'TV=temp*(1.0 + 0.61*QV)'
#'Rd=287' ;# J/kg/K
#'RHO=lev*100/(TV*Rd)'
#'RHOTAV=ave(RHO,t=1,t=12)'

'TTAV=ave(rh,t=1,t=12)'
# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 1
xwid = 3.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grid off';'set grads off'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set time ${TIME}'

'd TTAV'

'trackplot 0 500 80 500  -c 1 -l 1 -t 6'
# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

xx=xl-0.7; yy=(yt+yb)/2
'set strsiz 0.14 0.16'; 'set string 1 c 3 90'
'draw string 'xx' 'yy' Altitude [km]'

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS2 -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

#say; say 'MMMMM NetCDF OUT'
#'define VAROUT = VAR'
#'set sdfwrite '
#'sdfwrite VAROUT'
#say 'MMMMM DONE.'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS $CTL1

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

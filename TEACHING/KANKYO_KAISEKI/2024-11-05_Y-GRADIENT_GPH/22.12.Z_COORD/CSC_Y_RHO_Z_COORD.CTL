dset /work03/DATA/NCEP2/LTM.MON/TK_LTM_MON_1991-2020_Z-COORD.nc
title Monthly NCEP/DOE Reanalysis 2
options yrev
undef -9.96921e+36
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 30 linear 1 1
tdef 12 linear 00Z01JAN0001 1mo
vars 1
temp=>temp 30  t,z,y,x  Long Term Mean Monthly air temp. on Height Levels
endvars

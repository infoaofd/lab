#!/bin/bash

# Wed, 30 Aug 2023 10:15:19 +0900
# /work09/am/16.TOOL/32.MODEL/02.SHALLOW_WATER/12.SW01/24.SW.MID_LAT

T=$1
T=${T:-1}

CTL=SW.MID-LAT_h.CTL
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
GS=$(basename $0 .sh).GS

FIGDIR=FIG_$(basename $0 .sh); mkd $FIGDIR
TTT=$(printf %03d $T)
FIG=${FIGDIR}/$(basename $0 .sh)_h_${TTT}.PDF

# LONW= ;LONE= ; LATS= ;LATN=
# LEV=

LEVS="-0.05 0.05 0.005"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=4
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

xmax = 1; ymax = 1

ytop=7

xwid = 9.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 11 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'
'set mpdraw off'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set t $T'

'set xlopts 1 3 0.2';'set ylopts 1 3 0.2';
'set ylint 20'
'd h'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl+2; x2=xr-2; y1=yb-0.8; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=xr; y=y1
'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.25
'set strsiz 0.18 0.23'; 'set string 1 c 4 0'
'q dims';line=sublin(result,5);wrd=subwrd(line,6);tout=substr(wrd,4,9)
say 'MMMMM 't' 'tout
'draw string 'x' 'y' h 'tout

# HEADER
'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = 1.5; yy=1 ;#yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcl "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

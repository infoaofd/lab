Shallow-Water Equation Model

by Naoki Sato  April 19, 2004

========================================

doc.tex  : Tex source of the document
doc.ps   : Document in PostScript format
model.f  : Main program
fig.f    : A program to draw a figure for stream function
fig.ps   : A figure drawn by program fig.f (Time step # = 20)

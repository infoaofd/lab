C An example of simply read and write data 
!---------------------------------------------------------------------
!  At first, create GrADS data for required variables (by ARWpost.exe). 
!
!  plot, fields, and interp_levels in namelist.ARWpost 
!  for this case are 
!
!  plot='list'
!
!  fields = 'U,V,W,RAINNCV'
!
!   interp_levels = 0.05, 0.3, 0.55, 0.8, 1.05, 1.3, 1.55, 1.8, 2.05, 
!   2.3, 2.55,  2.8, 3.05, 3.3, 3.55, 3.8, 4.05, 4.3, 4.55, 4.8,  
!   5.05, 5.3, 5.55, 5.8, 6.05, 6.3, 6.55, 6.8, 7.05, 7.3, 7.55, 
!   7.8, 8.05, 8.3, 8.55,  8.8,  9.05, 9.3, 9.55, 9.8, 10.05, 
!-----------------------------------------------------------------------
! Then compile and run this program 
! compile: ifort -convert big_endian -o a.out readwrite.f 
! run:    ./a.out 
!-------------------------------------------------------------------
!     (IMD,JMD,KMD)=> dimension of original data 
!      LMD = dimension with respect to time   

      PARAMETER (IMD=450,JMD=450,KMD=41,LMD=3)
      REAL  RAINNCV(IMD,JMD)       ! RAINNCV
      REAL  U(IMD,JMD,KMD)   ! U 
      REAL  V(IMD,JMD,KMD)   ! V 
      REAL  W(IMD,JMD,KMD)   ! W 

      irecin=0
      irec=0

! open input data file 

      open (10,file='data_in.dat'
     *       ,form='unformatted',access='direct',recl=imd)


! open output data file 
      open (20,file='data_out.dat',
     *          form='unformatted',access='direct',recl=imd)


      do 3000 L=1,LMD

      write(6,*)'L=',L
      write(6,*)'READ INPUT DATA'



      do 205 k=1,kmd
      do 205 j=1,jmd
      irecin=irecin+1   
      read(10,rec=irecin)  (U(i,j,k),i=1,imd)
 205  continue

      do 210 k=1,kmd
      do 210 j=1,jmd
      irecin=irecin+1   
      read(10,rec=irecin)  (V(i,j,k),i=1,imd)
 210  continue

      do 215 k=1,kmd
      do 215 j=1,jmd
      irecin=irecin+1   
      read(10,rec=irecin)  (W(i,j,k),i=1,imd)
 215  continue


      do 220 j=1,jmd
      irecin=irecin+1   
      read(10,rec=irecin)  (RAINNCV(i,j),i=1,imd)
 220  continue


!**** write data 



      do 1010 k=1,kmd
      do 1010 j=1,jmd
      irec=irec+1
 1010 write(20,rec=irec)  (U(i,j,k),i=1,imd)

      do 1020 k=1,kmd
      do 1020 j=1,jmd
      irec=irec+1
 1020 write(20,rec=irec)  (V(i,j,k),i=1,imd)


      do 1100 k=1,kmd
      do 1100 j=1,jmd
      irec=irec+1
 1100 write(20,rec=irec)  (W(i,j,k),i=1,imd)


      do 1110 j=1,jmd
      irec=irec+1
 1110 write(20,rec=irec)  (RAINNCV(i,j),i=1,imd)

 3000 continue
      stop
      end 

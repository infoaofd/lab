dset ^transgrid.dat
options  byteswapped
undef 1.e30
title  OUTPUT FROM WRF V3.2 MODEL
xdef  200 linear  0.0  0.666666
ydef  200 linear   0.0 0.666666
zdef   41 linear 0.05 0.25  
tdef   3 linear 00Z07MAR1979      20MN      
VARS   34
XLAT           1  0  LATITUDE, SOUTH IS NEGATIVE (degree_north)
XLONG          1  0  LONGITUDE, WEST IS NEGATIVE (degree_east)
U             41  0  original x-wind component (m s-1)
V             41  0  original y-wind component (m s-1)
UROT          41  0  transformed x-wind component (m s-1)
VROT          41  0  transformed y-wind component (m s-1)
W             41  0  z-wind component (m s-1)
T             41  0  perturbation potential temperature (theta-t0) (K)
P             41  0  perturbation pressure (Pa)
PB            41  0  BASE STATE PRESSURE (Pa)
PH            41  0  Hydrostatic PRESSURE (Pa)
QVAPOR        41  0  Water vapor mixing ratio (kg kg-1)
QCLOUD        41  0  Cloud water mixing ratio (kg kg-1)
QRAIN         41  0  Rain water mixing ratio (kg kg-1)
QICE          41  0  Ice mixing ratio (kg kg-1)
QSNOW         41  0  Snow mixing ratio (kg kg-1)
QGRAUP        41  0  Graupel mixing ratio (kg kg-1)
QNRAIN        41  0  Graupel mixing ratio (kg kg-1)
diaba            41  0  diabatic heating (K/s)
tk            41  0  Temperature (K)
rh            41  0  Relative Humidity (%)
dbz           41  0  Reflectivity (WRF default, not correct)
cape          41  0  CAPE (J/kg)
te          41  0  Equivalent potential temperature (K)
tes          41  0  Saturated equivalent potential temperature (K)
dbzr           41  0  calculated reflectivity (correct)
BVF          41  0  Squared Brunt Vaisala frequency (using Lv) 
BVFI          41  0  Squared Brunt Vaisala frequency (using Ls)
RHO          41  0  Air density (kg/m^3)
GRADT          41  0  horizontal temperature gradient (K/km) 
FGC          41  0  frontogenesis (confluence)
FGS         41  0  frontogenesis (shear)
slp            1  0  Sea Levelp Pressure (hPa)
RAINNCV        1  0  TIME-STEP NONCONVECTIVE PRECIPITATION (mm)
ENDVARS

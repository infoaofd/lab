#!/bin/bash

DSET=ERA5
LONW=100
LONE=160
LATS=20
LATN=60

YYYYS=1995
MMS=11
DDS=25

YYYYE=1996
MME=03
DDE=01

LOGDIR=LOG_$(basename $0 .sh)
mkdir -vp $LOGDIR
URL="https://dreambooker.site/2019/10/03/Initializing-the-WRF-model-with-ERA5-pressure-level/"

GDIR=GRIB
mkdir -vp $GDIR
PDIR=PY
mkdir -vp $PDIR


start=${YYYYS}/${MMS}/${DDS}
  end=${YYYYE}/${MME}/${DDE}

jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${YYYYS}/${MMS}/${DDS} ${i}day" +%Y%m%d)
  YYYY=${date_out:0:4}
  MM=${date_out:4:2}
  DD=${date_out:6:2}

YYYYMMDD=${YYYY}${MM}${DD}
PYP=PY_PRS_$(basename $0 .sh)_${YYYYMMDD}.py
PYS=PY_SFC_$(basename $0 .sh)_${YYYYMMDD}.py
PYT=PY_SST_$(basename $0 .sh)_${YYYYMMDD}.py

GRP=${GDIR}/${DSET}_${YYYYMMDD}_P.grib
GRS=${GDIR}/${DSET}_${YYYYMMDD}_S.grib
GRT=${GDIR}/${DSET}_${YYYYMMDD}_SST.grib

cat <<EOF >$PYS
import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type':'reanalysis',
        'format':'grib',
        'variable':[
            '10m_u_component_of_wind','10m_v_component_of_wind','2m_dewpoint_temperature',
            '2m_temperature','land_sea_mask','mean_sea_level_pressure',
            'sea_ice_cover','skin_temperature',
            'snow_depth','soil_temperature_level_1','soil_temperature_level_2',
            'soil_temperature_level_3','soil_temperature_level_4','surface_pressure',
            'volumetric_soil_water_layer_1','volumetric_soil_water_layer_2','volumetric_soil_water_layer_3',
            'volumetric_soil_water_layer_4'
        ],
        'year': '${YYYY}',
        'month': '${MM}',
        'day': [
            '${DD}',
        ],
        'area':'${LATN}/${LONW}/${LATS}/${LONE}',
        'time':[
            '00:00',
            '03:00',
            '06:00',
            '09:00',
            '12:00',
            '15:00',
            '18:00',
            '21:00',
        ]
    },
    '${GRS}')
EOF

cat <<EOF >$PYT
import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type':'reanalysis',
        'format':'grib',
        'variable':[
            'sea_surface_temperature',
        ],
        'year': '${YYYY}',
        'month': '${MM}',
        'day': [
            '${DD}',
        ],
        'area':'${LATN}/${LONW}/${LATS}/${LONE}',
        'time':[
            '00:00',
            '03:00',
            '06:00',
            '09:00',
            '12:00',
            '15:00',
            '18:00',
            '21:00',
        ]
    },
    '${GRT}')
EOF
cat <<EOF >$PYP
import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-pressure-levels',
    {
        'product_type':'reanalysis',
        'format':'grib',
        'pressure_level':[
            '10',
            '20','30','50',
            '70','100','125',
            '150','175','200',
            '225','250','300',
            '350','400','450',
            '500','550','600',
            '650','700','750',
            '775','800','825',
            '850','875','900',
            '925','950','975',
            '1000'
        ],
        'year': '${YYYY}',
        'month': '${MM}',
        'day': [
            '${DD}',
        ],
        'area':'${LATN}/${LONW}/${LATS}/${LONE}',
        'time':[
            '00:00',
            '03:00',
            '06:00',
            '09:00',
            '12:00',
            '15:00',
            '18:00',
            '21:00',
        ],
        'variable':[
            'geopotential','relative_humidity','specific_humidity',
            'temperature','u_component_of_wind','v_component_of_wind'
        ]
    },
    '${GRP}')
EOF


PY_LIST="${PYS} ${PYP} ${PYT}"
for PY in $PY_LIST; do

LOG=${LOGDIR}/$(basename $PY .py)_${YYYY}${MM}${DD}.LOG
date -R >$LOG
pwd    >>$LOG
echo $(basename $0 ) >>$LOG
echo $URL >>$LOG
echo >>$LOG

if [ $PY = ${PYS} ]; then
GRIB=$GRS
elif [ $PY = ${PYP} ]; then
GRIB=$GRP
else
GRIB=$GRT
fi

echo "========================" >>$LOG
echo " $YYYY $MM $DD"  |tee -a $LOG
echo " $PY"  |tee -a $LOG
echo "========================" >>$LOG

echo $GRIB
python ${PY} > $LOG 2>&1
echo

echo   >>$LOG
dtime2=$(date -R)
if [ -f $GRIB ];then
echo >>$LOG
echo $dtime1 >>$LOG
echo $dtime2 >>$LOG
echo >>$LOG
ls -lh --time-style=long-iso ${GRIB} >>$LOG
echo   >>$LOG

#g1print.exe ${GRIB} >> $LOG

echo >>$LOG
echo DONE. >>$LOG
else
echo ERROR in $0 : NO SUCH FILE, $GRIB >>$LOG
echo ABNORMAL END. >>$LOG
echo >>$LOG
echo $dtime1 >>$LOG
echo $dtime2 >>$LOG
fi
echo >>$LOG

rm $PY 
done #PY


  i=$(expr $i + 1)
done #i

echo
echo LOG:
ls -lh --time-style=long-iso $LOG
echo
echo "DONE $(basename $0)!"
echo 
exit 0


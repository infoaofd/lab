# WRF ES 

moduleをロードする

## コンパイル

### ＷRF

Registoryの変更　(必須ではない)

Registry/Registry.EM_COMMON

**変更前にバックアップを取っておく**

雲微物理過程における凝結加熱等の出力

```
# Other Misc State Variables                                            
state   real    h_diabatic     ikj     misc         1         -      rhdu      "h_diabatic"            "MICROPHYSICS LATENT HEATING"         "K s-1"      
state   real    qv_diabatic    ikj     misc         1         -      rhdu      "qv_diabatic"           "MICROPHYSICS QV TENDENCY"            "g g-1 s-1"      
state   real    qc_diabatic    ikj     misc         1         -      rhdu      "qc_diabatic"  
```

風の水平発散の出力

```
state   real    div            ikj      misc        1         -     rhdu         "div"                   "DIVERGENCE"                                                      "s-1"
```

clean -aする

configureする

#### configure.wrfの変更

configure.wrf並列用コンパイラの指定を**mpiifort**, **mpiicc**に変更

```
DM_FC               = mpiifort
DM_CC               = mpiicc
```

**WRF_LIB**に**-qopenmp**を追加

```
WRF_LIB         =       -L$(WRF_DIR)/external/io_grib1 -lio_grib1 \
                        -L$(WRF_DIR)/external/io_grib_share -lio_grib_share \
                        -L$(WRF_DIR)/external/io_int -lwrfio_int \
                        -L$(WRF_DIR)/external/io_netcdf -lwrfio_nf \
                        -L$(NETCDF)/lib -lnetcdff -lnetcdf  -qopenmp
```

compileする



### WPS

clean -aする

configureする

configure.wrf並列用コンパイラの指定を**mpiifort**, **mpiicc**に変更

```
DM_FC               = mpiifort
DM_CC               = mpiicc
```

compileする



## データの準備

### WPS

namelist.wpsのバックアップ

namelist.wpsの編集

- 地形ファイルの所在
- 格子数
- ネスティング領域の始点の位置
- 時間ステップ
- 計算開始時刻, 終了時刻

#### 計算領域の確認

util/plotgrid_new.nclで計算領域の確認 (nclが必要)

```
$ ncl util/plotgrids_new.ncl 
```

#### geogrid.exe



#### ungrib.exe

Vtableコピー

link_grib.csh

ungrib.exe

##### 注意

- 気圧面, 地表面, (海面水温)をひとつづつungribする。

met_em*.ncの移動（必要に応じて）



### WRF

namelist.inputのバックアップ

namelist.inputの編集

- 格子の数
- ネスティング領域の始点の位置
- 時間ステップ
- 計算開始時刻, 終了時刻
- 計算結果の出力間隔
- リスタートファイルの出力間隔

- 海面水温のアップデートを行うか

  **行う場合**の設定は下記の通り（auxinput4_intervalの単位は分。360=6時間）

  ```bash
  &time_control
  
  io_form_auxinput4 = 2
  auxinput4_inname = "wrflowinp_d<domain>",
  auxinput4_interval = 360,360,
  
  &physics
  sst_update = 1,
  ```

  

- met_emファイルの所在

  ```bash
  auxinput1_inname = "./met_em.d<domain>.<date>"
  ```

- 出力先

  ```bash
  history_outname = "実験名称など_d<domain>_<date>",
  ```

  

#### real.exe

リソースセット時間 = リソースセット x 実行時間 (Real elapse: hour)

##### 1リソースセット

```
#!/bin/bash
#PBS -q cpu
#PBS -T intmpi
#PBS -b 1
#PBS -r n
#PBS -M EMAIL_ADDRESS
#PBS -m n
#PBS -l elapstim_req=1:00:00
#PBS --custom cpusetnum-lhost=1
#PBS -o stdout.%s.%j
#PBS -e stderr.%s.%j
#PBS -v NETCDF="/opt/share/NetCDF4_intel/netcdf-fortran/4.5.2-parallel/"
#PBS -v NETCDF_classic=0
#PBS -v PNETCDF="/opt/share/PNetCDF_intel/1.12.1/"
#PBS -v export HDF5="/opt/share/HDF5_intel/1.10.5/"
#PBS -v export PHDF5="/opt/share/PHDF5_intel/1.10.5/"
#PBS -v export WRF_EM_CORE=1
#PBS -v export JASPERLIB="/opt/share/jasper/1.900.1/lib"
#PBS -v export JASPERINC="/opt/share/jasper/1.900.1/include"

module load szip_intel/2.1.1
module load zlib_intel/1.2.11
module load PNetCDF_intel/1.12.1
module load PHDF5_intel/1.10.5
module load Jasper/1.900.1
module load NetCDF4_intel-parallel/all
module load Intel_oneAPI/2021.1.1/all
module load IntelMPI/2021.1.1

cd $PBS_O_WORKDIR

ulimit -s unlimited

mpirun -f ${PBS_NODEFILE} -n 64  -ppn 64 -genvall ./real.exe 
```



#### wrf.exe

リソースセット時間 = リソースセット x 実行時間 (Real elapse: hour)

##### 1リソースセット

```
#!/bin/bash
#PBS -q cpu
#PBS -T intmpi
#PBS -b 1
#PBS -r n
#PBS -M EMAIL_ADDRESS
#PBS -m n
#PBS -l elapstim_req=1:00:00
#PBS --custom cpusetnum-lhost=1
#PBS -o stdout.%s.%j
#PBS -e stderr.%s.%j
#PBS -v NETCDF="/opt/share/NetCDF4_intel/netcdf-fortran/4.5.2-parallel/"
#PBS -v NETCDF_classic=0
#PBS -v PNETCDF="/opt/share/PNetCDF_intel/1.12.1/"
#PBS -v export HDF5="/opt/share/HDF5_intel/1.10.5/"
#PBS -v export PHDF5="/opt/share/PHDF5_intel/1.10.5/"
#PBS -v export WRF_EM_CORE=1
#PBS -v export JASPERLIB="/opt/share/jasper/1.900.1/lib"
#PBS -v export JASPERINC="/opt/share/jasper/1.900.1/include"

module load szip_intel/2.1.1
module load zlib_intel/1.2.11
module load PNetCDF_intel/1.12.1
module load PHDF5_intel/1.10.5
module load Jasper/1.900.1
module load NetCDF4_intel-parallel/all
module load Intel_oneAPI/2021.1.1/all
module load IntelMPI/2021.1.1

cd $PBS_O_WORKDIR

ulimit -s unlimited

mpirun -f ${PBS_NODEFILE} -n 64  -ppn 64 -genvall ./wrf.exe 
```



##### 16リソースセット

```
#!/bin/bash
#PBS -q cpu
#PBS -T intmpi
#PBS -b 8
#PBS -r n
#PBS -M EMAIL_ADDRESS
#PBS -m n
#PBS -l elapstim_req=6:00:00
#PBS --custom cpusetnum-lhost=2
#PBS -o stdout.%s.%j
#PBS -e stderr.%s.%j
#PBS -v NETCDF="/opt/share/NetCDF4_intel/netcdf-fortran/4.5.2-parallel/"
#PBS -v NETCDF_classic=0
#PBS -v PNETCDF="/opt/share/PNetCDF_intel/1.12.1/"
#PBS -v export HDF5="/opt/share/HDF5_intel/1.10.5/"
#PBS -v export PHDF5="/opt/share/PHDF5_intel/1.10.5/"
#PBS -v export WRF_EM_CORE=1
#PBS -v export JASPERLIB="/opt/share/jasper/1.900.1/lib"
#PBS -v export JASPERINC="/opt/share/jasper/1.900.1/include"

module load szip_intel/2.1.1
module load zlib_intel/1.2.11
module load PNetCDF_intel/1.12.1
module load PHDF5_intel/1.10.5
module load Jasper/1.900.1
module load NetCDF4_intel-parallel/all
module load Intel_oneAPI/2021.1.1/all
module load IntelMPI/2021.1.1

cd $PBS_O_WORKDIR

ulimit -s unlimited

mpirun -f ${PBS_NODEFILE} -n 1024  -ppn 128 -genvall ./wrf.exe
```


dset  ^MSM/met_em.d01.2012-07-11_00:00:00.nc
dtype netcdf
title wrf
undef -32768
unpack scale_factor add_offset
*options template
pdef 180 180 lcc 32.5 128 90 90 60 30 140.0 6000 6000
xdef 360 linear 120 0.05
ydef 360 linear  25 0.05
zdef  22 linear 0 1
tdef 1 linear 12z30oct2010 3hr
vars 10
SST=>sst1  0 t,y,x latitude
LANDMASK=>land  0 t,y,x latitude
PMSL=>slp  0 t,y,x latitude
PSFC=>ps  0 t,y,x latitude
HGT_M=>hgt  0 t,y,x latitude
RH=>rh1  22 t,z,y,x latitude
UU=>u    22 t,z,y,x latitude
VV=>v    22 t,z,y,x latitude
TT=>t    22 t,z,y,x latitude
GHT=>z    22 t,z,y,x latitude
endvars

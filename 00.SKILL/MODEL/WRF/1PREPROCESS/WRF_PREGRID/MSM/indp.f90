
      subroutine indp(num, arrya, value, npt)
      implicit none
! ======================================================================
!     indp = index of nearest data point within "array" corresponding to
!            "value".
! ======================================================================
      integer :: num
      real    :: array(num)
      real    :: value
      integer :: npt

! ... local
      logical :: keep_going
      integer :: n

      if (value < array(1) then
        npt = 1
      else if (value > array(ia) then
        npt = num
      else
        n = 1
        keep_going = .true.
        do while (n <= num .and. keep_going)
          n = n+1
          if (value <= array(n)) then
            npt = n
            if (array(n)-value > value-array(n-1)) npt = n-1
            keep_going = .false.
          end if
        end do
      end if

      return
      end

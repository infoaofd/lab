
      subroutine hinterp2(nx,ny,var,land,undef,cyclic)
      implicit none
!=======================================================================
!     bi-linear interpolation
!=======================================================================

! ... output
      integer :: nx, ny
      real    :: var(nx,ny), land(nx,ny)
      real    :: undef
      logical :: cyclic

! ... local
      real   , allocatable :: mis(:,:)
      integer :: i, j, n, k
      integer :: is, ie, js, je, ic, jc, ii, jj
      real    :: xs, xc, xe, ys, yc, ye
      real    :: fisjs, fisje, fiejs, fieje, farea, ar, sum
      real    :: weight(-4:4,-4:4)

      real   , parameter :: crit = 0.01
      integer, parameter :: maxscan = 30
      logical :: done
      real    :: absres, resmax
      integer :: ier

      allocate(mis(nx,ny))
      do j = 1, ny
      do i = 1, nx
        mis(i,j) = 0.0
        if (land(i,j) <= 0.5 .and. var(i,j) == undef) mis(i,j) = 1.0
      end do
      end do

!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
      n = 0
      done = .false.
      do while (.not. done .and. n <= maxscan)
      n = n + 1
      do k = 1, 1, -1

        do j = 1, ny
        do i = 1, nx
          if (mis(i,j) <= 0.5) cycle
          weight = 1.0
          ar  = 0.0
          sum = 0.0
          do jc = -k, k
          do ic = -k, k
            if (ic == 0 .and. jc == 0) cycle
            ii = i+ic
            jj = j+jc
            if (ii < 1) then
              if (cyclic) then
                ii = ii + nx
              else
                ii = 1
              end if
            end if
            if (ii > nx) then
              if (cyclic) then
                ii = ii - nx
              else
                ii = nx
              end if
            end if
            if (jj < 1) then
              jj = 1
            end if
            if (jj > ny) then
              jj = ny
            end if
            if (mis(ii,jj) >= 0.5) weight(ic,jc) = 0.0
            if (land(ii,jj) >= 0.5) weight(ic,jc) = 0.0
            ar  = ar  + weight(ic,jc)
            sum = sum + var(ii,jj)*weight(ic,jc)
!           print *,'',weight(ic,jc),var(ii,jj),var(ii,jj)*weight(ic,jc)
          end do
          end do
          if (ar > 0.1) then
            var(i,j) = sum/ar
            mis(i,j) = 0.0
!           print *,'x',sum,var(i,j)
!           stop
          end if
        end do
        end do

        ier = 0
        do j = 1, ny
        do i = 1, nx
          if (mis(i,j) >= 0.5) ier = ier + 1
        end do
        end do

        print *,' ==> Extrapolated',n,k,'missing points =',ier

        if (ier == 0) exit
      end do
      if (ier == 0) done = .true.
      end do

      do j = 1, ny
      do i = 1, nx
        if (land(i,j) == 1.0) var(i,j) = undef
        if (mis (i,j) == 1.0) var(i,j) = -undef
        if (mis (i,j) == 1.0) then
          print *,' failed to extapolate at (i,j) = ',i,',',j
        end if
      end do
      end do

      deallocate(mis)
      return
      end

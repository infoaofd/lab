
      subroutine distance(lat1,lon1,lat2,lon2,dist)
      implicit none
!=======================================================================
!     get great-circle distance between any two points on the Earth
!=======================================================================

! ... intent(in)
      real    :: lat1
      real    :: lon1
      real    :: lat2
      real    :: lon2
! ... intent(out)
      real    :: dist
! ... local
      real(8), parameter :: radius = 6371.e3 ! radius of earth [m]
      real(8) :: sin1, sin2, cos1, cos2, dlon, tmp
      real(8) :: pi, deg2rad

      pi = 4.*atan(1.)
      deg2rad = pi/180.

      sin1 = sin( deg2rad*lat1 )
      sin2 = sin( deg2rad*lat2 )
      cos1 = cos( deg2rad*lat1 )
      cos2 = cos( deg2rad*lat2 )
      dlon = cos( deg2rad*(lon2-lon1) )
      tmp  = sin1*sin2 + cos1*cos2*dlon
      tmp  = max(-1.0,min(1.0,tmp))
      dist = dacos(tmp)*radius

      end subroutine distance

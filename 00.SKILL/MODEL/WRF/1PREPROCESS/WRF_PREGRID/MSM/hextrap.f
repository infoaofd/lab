
      subroutine hextrap (a, land, sor, res, il, jl, maxscn, crit, text
     &,                  gtype, fill, cyclic)
c=======================================================================
c
c
c     extrapolate original data into land areas
c     mask = 0 for land, 1 for ocean
c
!     call extrap (a, mask, scr1, scr2, im, jm, 100, 0.001, 'extrap'
!    &, 1, land)
c=======================================================================
c
c     utility to extrapolate values into specified areas neglecting
c     non-uniformity or asymmetry in the grid by solving a simple
c     heat eqn: del**2(a) = 0 over cells to be "extrapolated" using 
c     values over cells which are not to be extrapolated as boundaries.
c     this alleviates the problem of mismatched land/sea areas due to
c     different geometries or resolutions when interpolating between
c     atmospheric and ocean model grids.
c     the intent is to force reasonable values into land areas
c     near coastlines. far from coasts, the extrapolations may not be
c     reasonable.
c
c     note: incomming values over "extrapolated areas" are used as  
c           an initial guess and need to be specified
c
c
c     inputs:
c
c     a       = array with cells to be extrapolated. cells to
c               be extrapolated need to be set with an initial guess.
c     land    = mask to control which cells are to be extrapolated.
c     fill    = cells with mask=fill will get extrapolated values
c               other cells remain unchanged.
c     il      = number of points along 1st dimension of arrays
c     jl      = number of points along 2nd dimension of arrays
c     maxscn  = maximum number of passes allowed in relaxation
c     crit    = criterion for ending relaxation before "maxscn" limit
c     text    = character string (up to 15 chars) to identify data
c     gtype   = grid type = (1,2) to identify (ocean, atmosphere) grid
c     sor     = scratch area
c     res     = scratch area
c
c
c     outputs:
c
c     a       = array with extrapolated values in land areas.
c               non land areas remain unchanged.
c
c     author:      r. c. pacanowski      e-mail=> rcp@gfdl.gov
c=======================================================================
c
      logical done
      logical :: cyclic
      integer gtype, fill
      character*(*) text
      parameter (c0=0.0, p25=0.25)
      real :: land(il,jl)
      real :: a(il,jl), res(il,jl), sor(il,jl)
      integer :: stdout = 6
c
c-----------------------------------------------------------------------
c
c     solve a simple poisson eqn by relaxation to extrapolate data into
c     land areas using values over non land areas as boundary values.
c
c     note: successive calls to extrap will require fewer scans because
c           the initial guess field over land areas gets better with
c           each call.
c-----------------------------------------------------------------------
c
c
c     check on the grid type: atmosphere or ocean
c
      if (gtype .ne. 1 .and. gtype .ne. 2) then
        write (stdout,98) gtype
        call abort()
      endif     
c
c-----------------------------------------------------------------------
c     set the relaxation coefficient to zero over cells to be filled
c     relc is somewhat arbitrary
c-----------------------------------------------------------------------
c
      relc = 0.6
      do j=1,jl
        do i=1,il
          if (land(i,j) .eq. fill) then
            sor(i,j) = relc
          else
            sor(i,j) = c0
          endif
        enddo
      enddo
c
c-----------------------------------------------------------------------
c     iterate until errors are acceptable.
c-----------------------------------------------------------------------
c     
      n = 0
      done = .false.
      do while (.not. done .and. n .le. maxscn)
        resmax = c0
        done   = .true.
        n    = n + 1
        do j=2,jl-1
          do i=2,il-1
            res(i,j) = p25*(a(i-1,j) + a(i+1,j) + a(i,j-1) + a(i,j+1)) 
     &                 - a(i,j)
          enddo
        enddo
        do j=2,jl-1
          do i=2,il-1
            res(i,j) = res(i,j)*sor(i,j)
            a(i,j) = a(i,j) + res(i,j)
            absres = abs(res(i,j))
            if (absres .gt. crit) done = .false.
            resmax = max(absres,resmax)
          enddo
        enddo
c
c-----------------------------------------------------------------------
c       set conditions at edge of grid
c-----------------------------------------------------------------------
c
        if (gtype .eq. 1) then
c
c         use cyclic or no flux conditions on ocean grids
c
          do j=1,jl
            if (cyclic) then
              a(1,j)  = a(il-1,j)
              a(il,j) = a(2,j)
            else
              a(1,j)  = a(2,j)
              a(il,j) = a(il-1,j)
            end if
          enddo
        elseif (gtype .eq. 2) then
c
c         always put cyclic conditions on atmosphere grids
c
          do j=1,jl
            a(1,j)  = a(il-1,j)
            a(il,j) = a(2,j)
          enddo
        endif
c
c       no flux condition at northern and southern boundaries
c
        do i=1,il
          a(i,1)  = a(i,2)
          a(i,jl) = a(i,jl-1)
        enddo
c
      enddo
c
      write (stdout,99) text, n, resmax
99    format (1x,'==> Extrapolated ',a15,' into land using ',i4
     &,       ' scans.  max residual=', g14.7)
98    format (1x,'==> Error:   gtype =',i6,' in extrap')
      return
      end

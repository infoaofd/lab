dset  ^LANDSEA.MSM_5K_20071121
title msm
options big_endian yrev
undef -999.9
xdef  481 linear 120.0 0.0625
ydef  505 linear  22.5 0.0500
zdef    1 levels 0
tdef 1 linear 00z08jul2013 3hr
vars 1
land 0 99 land/sea mask = [1/0]
*land 0 -1,40,4  99 land/sea mask = [1/0]
endvars


      subroutine smooth(nlon,nlat,gdat,gmsk,smth,max_itr,cyclic)
      implicit none
!=======================================================================
!     9pt smoothing
!=======================================================================
      logical :: cyclic
      integer :: max_itr
      integer :: nlon, nlat
      real    :: gdat(nlon,nlat)
      real    :: gmsk(nlon,nlat)
      real    :: smth(nlon,nlat)
      real, allocatable :: buf(:,:)

      integer :: num
      real    :: sum
      integer :: i, j, ii, jj, ic, jc, itr
      integer, parameter :: inc = 1

      allocate(buf(nlon,nlat))
      buf = gdat

      do itr = 1, max_itr

        smth = buf

        do j = 1, nlat
        do i = 1, nlon
        if (gmsk(i,j) == 0.0) cycle

          num = 0
          sum = 0
          do jj = j-inc, j+inc
            if (jj < 1 .or. jj > nlat) cycle
            jc = jj

            do ii = i-inc, i+inc

              ic = ii
              if (ic < 1) then
                if (cyclic) then
                  ic = ic + nlon
                else
                  cycle
                end if
              end if

              if (ic > nlon) then
                if (cyclic) then
                  ic = ic - nlon
                else
                  cycle
                end if
              end if

              if (gmsk(ic,jc) == 0.0) cycle

              num = num + 1
              sum = sum + gdat(ic,jc)

            end do
          end do

          if (num > 1) then
            smth(i,j) = sum/num
          end if

        end do
        end do

        buf = smth

      end do
      deallocate(buf)

      return
      end

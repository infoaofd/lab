#!/bin/sh

site="http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf"

beg=20210311
end=20210313
beg=20220801
end=20220815
year=`echo ${beg} | cut -c 1-4`
month=`echo ${beg} | cut -c 5-6`
date=`echo ${beg} | cut -c 7-8`
while [ ${year}${month}${date} -le ${end} ]
do

  sfile="MSM-S/msm_surf.${year}${month}${date}.nc"
  pfile="MSM-P/msm_plev.${year}${month}${date}.nc"
 
  wget -O ${sfile} ${site}/MSM-S/${year}/${month}${date}.nc
  wget -O ${pfile} ${site}/MSM-P/${year}/${month}${date}.nc

  date=`expr ${date} + 1`
  dten=10
  end_month=31
  case ${month} in
    04)
        end_month=30;;
    06)
        end_month=30;;
    09)
        end_month=30;;
    11)
        end_month=30;;
  esac
  if [ ${month} -eq 02 ]
  then
    intqyear=`expr $year / 4`
#       echo $intqyear
        intqyear=`expr $intqyear \* 4`
#       echo $intqyear
        xyear=$((year - intqyear))
#  echo ${year}${month}${date}
#       echo $xyear
    if [ ${xyear} -eq 0 ]
        then
          end_month=29
        else
          end_month=28
        fi
        echo $end_month
  fi
  if [ ${date} -lt $dten ]; then
    date=0${date}
  elif [ ${date} -gt ${end_month} ]; then
    month=`expr ${month} + 1`
    if [ ${month} -lt 10 ]; then
      month=0${month}
    elif [ ${month} -gt 12 ]; then
      year=`expr ${year} + 1`
      if [ ${year} -lt 1000 ]; then
        year=0${year}
        if [ ${year} -lt 100 ]; then
          year=0${year}
          if [ ${year} -lt 10 ]; then
            year=0${year}
          fi
        fi
      fi
      month=01
    fi
    date=01
  fi

done
exit 0


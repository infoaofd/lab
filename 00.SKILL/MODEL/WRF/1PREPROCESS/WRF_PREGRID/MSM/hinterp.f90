
      subroutine hinterp(nx,ny,xt,yt,var,land,undef,cyclic,&
     &                   nlon,nlat,rlon,rlat,gdat,gland,global)
      implicit none
!=======================================================================
!     bi-linear interpolation
!=======================================================================

! ... output
      integer :: nx, ny
      real    :: xt(nx), yt(ny)
      real    :: var(nx,ny), land(nx,ny)
      real    :: undef
      logical :: cyclic

! ... input
      integer :: nlon, nlat
      real    :: rlon(nlon), rlat(nlat)
      real    :: gdat(nlon,nlat), gland(nlon,nlat)
      logical :: global

! ... local
      real   , allocatable :: ipt(:,:), jpt(:,:)
      real   , allocatable :: sor(:,:), res(:,:), mis(:,:)
      integer :: i, j, n, k
      integer :: is, ie, js, je, ic, jc, ii, jj
      real    :: xs, xc, xe, ys, yc, ye
      real    :: fisjs, fisje, fiejs, fieje, farea, ar, sum
      real    :: weight(-4:4,-4:4)

      real   , parameter :: crit = 0.01
      integer, parameter :: maxscan = 30
      logical :: done
      real    :: absres, resmax
      integer :: ier

      if (rlat(1) > rlat(nlat)) stop 'order of input in lat. is error'
      allocate(ipt(nx,ny), jpt(nx,ny),mis(nx,ny))

      ipt(:,:) = -999
      do j = 1, ny
      do i = 1, nx
        do is = nlon, 1, -1
          ipt(i,j) = is
          if (rlon(is) <= xt(i)) exit
        end do
      end do
      end do

      jpt(:,:) = -999
      do j = 1, ny
      do i = 1, nx
        do js = nlat, 1, -1
          jpt(i,j) = js
          if (rlat(js) <= yt(j)) exit
        end do
      end do
      end do

      var = undef
      mis = 0.0

      do j = 1, ny
      do i = 1, nx
        if (land(i,j) == 1.0) cycle

        yc  = yt(j)
        js  = jpt(i,j)
        je  = js + 1
        ys  = rlat(js)
        if (je > nlat) then
          je  = nlat
          ye  = rlat(js) + (rlat(js)-rlat(js-1))
        else
          ye  = rlat(je)
        end if

        xc  = xt(i)
        is  = ipt(i,j)
        ie  = is + 1
        xs  = rlon(is)
        if (ie > nlon) then
          if (global) then
            ie  = ie-nlon
            xe  = rlon(ie) + 360.0
          else
            ie  = nlon
            xe  = rlon(ie)
          end if
        else
          xe  = rlon(ie)
        end if

        farea = (xe-xs)*(ye-ys)
!       print *,'',farea,xe,xc,xs,ye,yc,ys
        if (farea <= 0.0) then
          if (xe == xs .and. ye /= ys) then
            farea = (ye-ys)
            fisjs = (ye-yc)/farea
            fisje = (yc-ys)/farea
            fiejs = (ye-yc)/farea
            fieje = (yc-ys)/farea
          else if (xe /= xs .and. ye == ys) then
            farea = (xe-xs)
            fisjs = (xe-xc)/farea
            fisje = (xe-xc)/farea
            fiejs = (xc-xs)/farea
            fieje = (xc-xs)/farea
          else
            fisjs = 0.0
            fisje = 0.0
            fiejs = 0.0
            fieje = 0.0
          end if
        else
          fisjs = (xe-xc)*(ye-yc)/farea
          fisje = (xe-xc)*(yc-ys)/farea
          fiejs = (xc-xs)*(ye-yc)/farea
          fieje = (xc-xs)*(yc-ys)/farea
        end if
        if (gland(is,js) == 1.0) fisjs = 0.0
        if (gland(is,je) == 1.0) fisje = 0.0
        if (gland(ie,js) == 1.0) fiejs = 0.0
        if (gland(ie,je) == 1.0) fieje = 0.0

        var(i,j) = gdat(is,js)*fisjs &
     &           + gdat(is,je)*fisje &
     &           + gdat(ie,js)*fiejs &
     &           + gdat(ie,je)*fieje
        ar = fisjs + fisje + fiejs + fieje
        if (ar > 0.9) then
          var(i,j) = var(i,j)/ar
        else
          print *,' failed to interpolate at (i,j) = ',i,',',j
!         stop
          var(i,j) = undef
          mis(i,j) = 1.0
        end if

      end do
      end do

      deallocate(ipt, jpt)
      go to 7

      ier = 0
      do j = 1, ny
      do i = 1, nx
        if (mis(i,j) >= 0.5) ier = ier + 1
      end do
      end do
      if (ier == 0) go to 7

!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
      n = 0
      done = .false.
      do while (.not. done .and. n <= maxscan)
      n = n + 1
      do k = 1, 1

        do j = 1, ny
        do i = 1, nx
          if (mis(i,j) <= 0.5) cycle
          weight = 1.0
          ar  = 0.0
          sum = 0.0
          do jc = -k, k
          do ic = -k, k
            if (ic == 0 .and. jc == 0) cycle
            ii = i+ic
            jj = j+jc
            if (ii < 1) then
              if (cyclic) then
                ii = ii + nx
              else
                ii = 1
              end if
            end if
            if (ii > nx) then
              if (cyclic) then
                ii = ii - nx
              else
                ii = nx
              end if
            end if
            if (jj < 1) then
              jj = 1
            end if
            if (jj > ny) then
              jj = ny
            end if
            if (mis(ii,jj) >= 0.5) weight(ic,jc) = 0.0
            if (land(ii,jj) >= 0.5) weight(ic,jc) = 0.0
            ar  = ar  + weight(ic,jc)
            sum = sum + var(ii,jj)*weight(ic,jc)
!           print *,'',weight(ic,jc),var(ii,jj),var(ii,jj)*weight(ic,jc)
          end do
          end do
          if (ar > 0.1) then
            var(i,j) = sum/ar
            mis(i,j) = 0.0
!           print *,'x',sum,var(i,j)
!           stop
          end if
        end do
        end do

        ier = 0
        do j = 1, ny
        do i = 1, nx
          if (mis(i,j) >= 0.5) ier = ier + 1
        end do
        end do

        print *,' ==> Extrapolated',n,k,'missing points =',ier

        if (ier == 0) exit
      end do
      if (ier == 0) done = .true.
      end do

      do j = 1, ny
      do i = 1, nx
        if (mis (i,j) == 1.0) var(i,j) = -undef
        if (mis (i,j) == 1.0) then
          print *,' failed to extapolate at (i,j) = ',i,',',j
        end if
      end do
      end do

7     continue
      do j = 1, ny
      do i = 1, nx
        if (land(i,j) == 1.0) var(i,j) = undef
      end do
      end do

      deallocate(mis)
      return

!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
      allocate(sor(nx,ny), res(nx,ny))
      do j = 1, ny
      do i = 1, nx
        if (mis(i,j) >= 0.5) then
          sor(i,j) = 0.6
        else
          sor(i,j) = 0.0
        end if
      end do
      end do

      n = 0
      done = .false.
      do while (.not. done .and. n <= maxscan)

        resmax = 0.0
        done = .true.
        n = n + 1
        do j = 2, ny-1
        do i = 2, nx-1
          weight(:,:) = 1.0
          if (land(i-1,j) == 1.0) fisjs = 0.0
          if (land(i+1,j) == 1.0) fiejs = 0.0
          if (land(i,j-1) == 1.0) fisjs = 0.0
          if (land(i,j+1) == 1.0) fisjs = 0.0
          ar = fisjs + fisje + fiejs + fieje
          absres = var(i-1,j)*land(i-1,j) &
     &           + var(i+1,j)*land(i+1,j) &
     &           + var(i,j-1)*land(i,j-1) &
     &           + var(i,j+1)*land(i,j+1)
          res(i,j) = absres/ar - var(i,j)
        end do
        end do
        do j = 2, ny-1
        do i = 2, nx-1
          res(i,j) = res(i,j) * sor(i,j)
          var(i,j) = var(i,j) + res(i,j)
          absres = abs(res(i,j))
          if (absres .gt. crit) done = .false.
          resmax = max(absres,resmax)
        end do
        end do

        do i = 2, nx-1
          if (mis(i,1) >= 0.5) var(i,1) = var(i,2)
          if (mis(i,ny) >= 0.5) var(i,ny) = var(i,ny-1)
        end do

        do j = 1, ny
          if (cyclic) then
            if (mis(1,j) >= 0.5) var(1,j) = var(nx-1,j)
            if (mis(nx,j) >= 0.5) var(nx,j) = var(2,j)
          else
            if (mis(1,j) >= 0.5) var(1,j) = var(2,j)
            if (mis(nx,j) >= 0.5) var(nx,j) = var(nx-1,j)
          end if
        end do

      end do

      do j = 1, ny
      do i = 1, nx
        if (land(i,j) == 1.0) var(i,j) = undef
      end do
      end do

      return
      end

dset  ^test.dat
title msm
options big_endian
undef -9.99e33
xdef  521 linear 117.5 0.0625
ydef  505 linear  22.4 0.05
zdef    1 levels 0
tdef 1 linear 00z08jul2013 3hr
vars 5
topo  0  99 topography [m]
ts    0  99 topography [m]
us    0  99 topography [m]
vs    0  99 topography [m]
slp   0  99 topography [m]
endvars

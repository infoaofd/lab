dset  ^test0.dat
title msm
*options big_endian
undef -9.99e33
xdef  50 linear 117.5 0.0166
ydef  30 linear  22.4 0.0166
zdef    1 levels 0
tdef 1 linear 00z08jul2013 3hr
vars 3
mask  0  99 topography [m]
b     0  99 topography [m]
a     0  99 topography [m]
endvars


      subroutine hinterp3(nlon,nlat,rlon,rlat,gdat,gland,
     &                    nx,ny,xt,yt,var,land,undef,global)
      implicit none
!=======================================================================
!     interpolate lat-lon grid bathymetry data on model grid
!=======================================================================

!-----------------------------------------------------------------------
!     input
!-----------------------------------------------------------------------
      integer :: nlon, nlat
      real    :: rlon(nlon)
      real    :: rlat(nlat)
      real    :: gdat(nlon,nlat)
      real    :: gland(nlon,nlat)
      logical :: global

!-----------------------------------------------------------------------
!     output
!-----------------------------------------------------------------------
      integer :: nx, ny
      real    :: xt(nx)
      real    :: yt(ny)
      real    :: var(nx,ny)
      real    :: land(nx,ny)
      real    :: undef

!-----------------------------------------------------------------------
!     set influence radius
!-----------------------------------------------------------------------
! ... local
      real    :: dist_e
      real    :: dlon, dlat, xlon
      real    :: dist, weight, sum1, sum2
      integer :: max_dis
      integer :: i, j, ix, jy, ic, jc, is, ie, js, je, n
      integer :: ier
      integer, parameter :: maxscan = 30

! ... set dlon & dlat
      dlon = rlon(2)-rlon(1)
      dlat = rlat(2)-rlat(1)

! ... set dist_e
      call distance
     &    (yt(4),xt(4),yt(1),xt(1),dist_e)
! ... set serach area
      call distance
     &    (rlat(2),rlon(2),rlat(1),rlon(1),dist)
      max_dis = nint(dist_e/dist)*2
      if (max_dis < 1) then
        print *,' dsit_e = ',dist_e
        print *,' dsit   = ',dist
        print *,' dsit_e is small!'
        stop
      end if

      do n = 1, maxscan

! ... set undef
      var = undef

! ... interpolate
      do j = 1, ny
      do i = 1, nx
        if (land(i,j) == 1.0) cycle

        ix = (xt(i)-rlon(1))/dlon + 1
        jy = (yt(j)-rlat(1))/dlat + 1
        is = ix-max_dis
        ie = ix+max_dis
        js = max(jy-max_dis,1)
        je = min(jy+max_dis,nlat)
        sum1 = 0.0
        sum2 = 0.0
        do jy = js, je
        do ix = is, ie
          ic = ix
          jc = jy

          if (ic > nlon) then
            if (global) then
              ic   = ic-nlon
              xlon = rlon(ic) + 360.0
            else
              ic   = nlon
              xlon = rlon(ic)
            end if
          else if (ic < 1) then
            if (global) then
              ic   = ic+nlon
              xlon = rlon(ic) - 360.0
            else
              ic   = 1
              xlon = rlon(ic)
            end if
          else
            xlon = rlon(ic)
          end if

          if (gdat(ic,jc) <= undef-1) cycle

          call distance
     &    (yt(j),xt(i),rlat(jc),rlon(ic),dist)
          if (dist > dist_e) cycle
          weight = (dist_e**2-dist**2)/(dist_e**2+dist**2)
          sum1   = sum1 + weight
          sum2   = sum2 + weight * gdat(ic,jc)
!         sum2   = sum2 + weight *max(0.0, gdat(ic,jc))
        end do
        end do

        if (sum1 > 0.1) then
          var(i,j) = sum2/sum1
        else
!         print *,'sum1 = ',sum1
!         print *,'i, j = ',i,j,' xt, yt = ',xt(i),yt(j)
!         print *,'',rlon(1),rlon(nlon)
!         print *,'',rlat(1),rlat(nlat)
!         stop 'dist_e may be too small'
          var(i,j) = undef
        end if

      end do
      end do

      gdat = var

! ... check
      ier = 0
      do j = 1, ny
      do i = 1, nx
        if (land(i,j) <= 0.5 .and. var(i,j) == undef) ier = ier + 1
      end do
      end do

      print *,' ==> Extrapolated',n,'missing points =',ier
      if (ier == 0) exit

      end do

      return
      end

      subroutine distance(lat1,lon1,lat2,lon2,dist)
      implicit none
!=======================================================================
!     get great-circle distance between any two points on the Earth
!=======================================================================

! ... intent(in)
      real    :: lat1
      real    :: lon1
      real    :: lat2
      real    :: lon2
! ... intent(out)
      real    :: dist
! ... local
      real(8), parameter :: radius = 6371.e3 ! radius of earth [m]
      real(8) :: sin1, sin2, cos1, cos2, dlon, tmp
      real(8) :: pi, deg2rad

      pi = 4.*atan(1.)
      deg2rad = pi/180.

      sin1 = sin( deg2rad*lat1 )
      sin2 = sin( deg2rad*lat2 )
      cos1 = cos( deg2rad*lat1 )
      cos2 = cos( deg2rad*lat2 )
      dlon = cos( deg2rad*(lon2-lon1) )
      tmp  = sin1*sin2 + cos1*cos2*dlon
      tmp  = max(-1.0,min(1.0,tmp))
      dist = dacos(tmp)*radius

      end subroutine distance

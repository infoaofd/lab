#!/bin/bash

########################################
#  user
########################################
hostname=glen.ess.bosai.go.jp
filepath=/xnet/product/jma-model

my_info="/home/iizuka/.file_glen"
username=`cat $my_info | cut -d ' ' -f1`
password=`cat $my_info | cut -d ' ' -f2`

beg=20220616
end=20220616

yy=`echo ${beg} | cut -c 1-4`
mm=`echo ${beg} | cut -c 5-6`
dd=`echo ${beg} | cut -c 7-8`

########################################
if (! -d $yy) then
  mkdir $yy
endif
if (! -d $yy/$mm) then
  mkdir $yy/$mm
endif
########################################
while [ ${yy}${mm}${dd} -le ${end} ]
do
  for hh in 00 03 06 09 12 15 18 21
  do

    file=`echo $filepath/4v-msm/$yy/$mm/$dd/4v-msm_${yy}-${mm}-${dd}_${hh}utc.nc.gz`
    echo $file

    expect -c "
    set timeout 10
#   spawn scp ${username}@${hostname}:${file} ${yy}/${mm}/.

    expect password:
    send \"${password}\n\"

    expect \"$\"
    "
    sleep 1
#   exit
# interact
# expect eof

    file=`echo $filepath/4p-msm/$yy/$mm/$dd/4p-msm_${yy}-${mm}-${dd}_${hh}utc.nc.gz`

    expect -c "
    set timeout 10
    spawn scp ${username}@${hostname}:${file} ${yy}/${mm}/.

    expect password:
    send \"${password}\n\"

    expect \"$\"
    "
    sleep 1
 
  done

  beg=`date -d "${beg} 1 day" "+%Y%m%d"`
  yy=`echo ${beg} | cut -c 1-4`
  mm=`echo ${beg} | cut -c 5-6`
  dd=`echo ${beg} | cut -c 7-8`
  echo $beg
done

exit

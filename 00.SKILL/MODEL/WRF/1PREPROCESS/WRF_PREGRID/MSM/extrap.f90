
      subroutine extrap(nlon,nlat,bufr,undef,global)
      implicit none
!=======================================================================
!     extrapolate data on missing points
!=======================================================================

! ... input/output
      logical :: global
      integer :: nlon, nlat
      real    :: bufr(nlon,nlat)
      real    :: undef

! ... local
      real    :: weight(8)
      real, allocatable :: guess(:,:)
      real    :: x1, x2
      integer :: itr, i, j, iw, ie, js, jn

      allocate(guess(nlon,nlat))

      do itr = 1, 10
      guess = bufr
      do j = 1, nlat
      do i = 1, nlon
        if (bufr(i,j) /= undef) cycle

        js = j-1
        if (js < 1   ) js = 1

        jn = j+1
        if (jn > nlat) jn = nlat

        iw = i-1
        if (iw < 1   ) then
          if (global) then
            iw = nlon
          else
            iw = 1
          end if
        end if

        ie = i+1
        if (ie > nlon) then
          if (global) then
            ie = 1
          else
            ie = nlon
          end if
        end if

        weight(:) = 1.0
        if (bufr(iw,j ) == undef) weight(1) = 0.0
        if (bufr(iw,jn) == undef) weight(2) = 0.0
        if (bufr(i ,jn) == undef) weight(3) = 0.0
        if (bufr(ie,jn) == undef) weight(4) = 0.0
        if (bufr(ie,j ) == undef) weight(5) = 0.0
        if (bufr(ie,js) == undef) weight(6) = 0.0
        if (bufr(i ,js) == undef) weight(7) = 0.0
        if (bufr(iw,js) == undef) weight(8) = 0.0
        x1 = bufr(iw,j )*weight(1) &
     &     + bufr(iw,jn)*weight(2) &
     &     + bufr(i ,jn)*weight(3) &
     &     + bufr(ie,jn)*weight(4) &
     &     + bufr(ie,j )*weight(5) &
     &     + bufr(ie,js)*weight(6) &
     &     + bufr(i ,js)*weight(7) &
     &     + bufr(iw,js)*weight(8) 
        x2 = sum(weight(1:8))
        if (x2 >= 1.0) then
          guess(i,j) = x1/x2
        else
          guess(i,j) = undef
        end if
      end do
      end do
      bufr = guess
      end do
      deallocate(guess)

      return
      end subroutine extrap

#!/bin/csh -f

set NCLIB = "$home/local/netcdf/*.o -L/usr/local/netcdf-3.6.3/lib -lnetcdf"
set FC = ifort
set OPT = "-convert big_endian -assume byterecl"

set DIR_PATH = /work/iizuka/WRF/input
set DIR_PATH = /raid2/iizuka/OISST/input

set year = 1993
while ($year <= 2010)
@ nyear = $year + 1

cat > pregrid.namelist << EOF
&record1
 START_YEAR  = $year
 START_MONTH = 11
 START_DAY   = 21
 START_HOUR  = 00

 END_YEAR    = $nyear
 END_MONTH   = 03
 END_DAY     = 01
 END_HOUR    = 00
&

&record2
 FILE_DIR    = '$DIR_PATH'
&
EOF
cat pregrid.namelist

cat > pregrid.F90 << EOF
module netcdf_parameter
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  implicit none
  include 'netcdf.inc'

!--- USER may change ---
  character(len=4) :: tim_name='time'
  character(len=3) :: lon_name='lon'
  character(len=3) :: lat_name='lat'
! character(len=3) :: lev_name='lev'
  character(len=5) :: lev_name='level'
! character(len=5) :: lev_name='depth'

  integer :: ncid
end module netcdf_parameter

program netcdf_read
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  use netcdf_parameter
  implicit none

!--- local ---
  integer :: start_year   = 0
  integer :: start_month  = 0
  integer :: start_day    = 0
  integer :: start_hour   = 0
  integer :: start_minute = 0
  integer :: start_second = 0

  integer :: end_year     = 0
  integer :: end_month    = 0
  integer :: end_day      = 0
  integer :: end_hour     = 0
  integer :: end_minute   = 0
  integer :: end_second   = 0

  integer :: interval     = 0

  namelist /record1/ &
  start_year, start_month, start_day, start_hour, start_minute, start_second,&
    end_year,   end_month,   end_day,   end_hour,   end_minute,   end_second,&
    interval

!--- local ---
  logical :: exist
  character(len=200) :: file_dir

  namelist /record2/ &
  file_dir

!--- dimension size ---
  integer :: ndims, nlon, nlat, nlev, ntim, maxlev

!--- data array ---
  real(4), dimension(:,:), allocatable :: bufr, temp
  character(len=80) :: var_name

!--- local for file ---
  character(len=200) :: file_output, file_input, file_mask
  character(len=200) :: command
  integer :: iounit_file=99

  real(4), dimension(:,:), allocatable :: land, smooth
  real(4), dimension(:), allocatable :: rlon, rlat
  real(4) :: undef = -999.9
  logical :: cyclic = .false.
  integer :: i, j

!--- local for time ---
  character(len=24) :: jdate
  integer :: leap_year, loop_year, loop_month, loop_hour, loop_day
  integer :: time_index, level_index, var_index
  integer :: days_per_month(12,0:1)
  days_per_month(1:12,0) = (/31,29,31,30,31,30,31,31,30,31,30,31/)
  days_per_month(1:12,1) = (/31,28,31,30,31,30,31,31,30,31,30,31/)

!----------------------------------------------------------------------
!--- read namelist ---
!----------------------------------------------------------------------
  open (10, file='pregrid.namelist', status='old')
  read (10, record1)
! write( 6, record1)
  read (10, record2)
! write( 6, record2)
  close(10)

!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  nlon = 1440
  nlat =  720
  allocate(rlon(nlon))
  allocate(rlat(nlat))
  do i = 1, nlon
    rlon(i) =   0.0 + (i-0.5)*0.25
  end do
  do j = 1, nlat
    rlat(j) = -90.0 + (j-0.5)*0.25
  end do

!======================================================================
!--- loop for time ---
!======================================================================
  do loop_year  = start_year, end_year 

  if (mod(loop_year,4) == 0) then
    leap_year = 0
  else
    leap_year = 1
  end if

  do loop_month = 1, 12
  do loop_day   = 1, days_per_month(loop_month,leap_year)

!----------------------------------------------------------------------
!--- skip ---
!----------------------------------------------------------------------
    if ( loop_year == start_year ) then
      if ( loop_month < start_month ) cycle
      if ( loop_month == start_month ) then
        if ( loop_day < start_day ) cycle
!       if ( loop_day == start_day ) then
!         if ( loop_hour < start_hour ) cycle
!       end if
      end if
    end if

    if ( loop_year == end_year ) then
      if ( loop_month > end_month ) exit
      if ( loop_month == end_month ) then
        if ( loop_day > end_day ) exit
!       if ( loop_day == end_day ) then
!         if ( loop_hour > end_hour ) exit
!       end if
      end if
    end if

!----------------------------------------------------------------------
!--- set input file (FILE) ---
!----------------------------------------------------------------------

  write(file_input,'(a,a,i4.4,i2.2,i2.2,a)') &
      trim(file_dir),'/avhrr-only-v2-japan-sea.', &
      loop_year,loop_month,loop_day,'.nc'
  inquire(file=trim(file_input),exist=exist)
  if (exist) then
  else
    print *,trim(file_input)
    stop 'no file'
  end if

  do loop_hour  = 0, 23, 6
!----------------------------------------------------------------------
!--- count time_index ---
!----------------------------------------------------------------------
    time_index = 0
    time_index = time_index + 1
!   print *,'time_index',time_index
!   print *,loop_year,loop_month,loop_day,loop_hour
!   cycle 

!----------------------------------------------------------------------
!--- time stamp ---
!----------------------------------------------------------------------
    write(jdate,'(i4,a1,i2.2,a1,i2.2,a1,i2.2,a1,a2,a1,a2)') &
       loop_year,'-',loop_month,'-',loop_day,'_',           &
       loop_hour,':','00',':','00'
    write(6,*) jdate

!----------------------------------------------------------------------
!--- open output file (FILE) ---
!----------------------------------------------------------------------
    write(file_output,'(a,i4,a1,i2.2,a1,i2.2,a1,i2.2)')  &
       'OISST-CLM:',loop_year,'-',loop_month,'-',loop_day,'_',loop_hour
    open(UNIT=iounit_file,FILE=file_output, FORM='unformatted')

!----------------------------------------------------------------------
!--- SST ---
!----------------------------------------------------------------------
    do var_index   = 1, 1
      var_name   = 'sstc'
      allocate(bufr(nlon,nlat))
      allocate(temp(nlon,nlat))
      temp(:,:)=0.0
      call read_nc_data(trim(file_input), var_name, &
                        nlon,nlat,temp,1,time_index,undef)

      where (temp < -9.0)
!!       bufr = 0.0
        bufr = undef
      else where
        bufr = max(temp, -1.8)
        bufr = bufr + 273.15
      end where

!----------------------------------------------------------------------
! ... smoothing
!----------------------------------------------------------------------
!     call make_smooth(nlon,nlat,rlon,rlat,
!    &                 sst,smooth,land,cyclic)

      call output (iounit_file,  &
                   jdate, nlon, nlat, bufr, var_index, 1)
      deallocate(bufr)
      deallocate(temp)
    end do

  end do
  end do
  end do
  end do

  deallocate(rlon,rlat)
  stop ' normal end '
  
 end

!======================================================================
!
!======================================================================
      subroutine make_smooth(nlon,nlat,rlon,rlat, &
     &                       sst,smooth,land,cyclic)
      implicit none
      logical :: cyclic
      integer :: nlon, nlat
      real    :: rlon(nlon)
      real    :: rlat(nlat)
      real    :: sst(nlon,nlat)
      real    :: smooth(nlon,nlat)
      real    :: land(nlon,nlat)

      real   , dimension(:,:), allocatable :: buf

      integer :: num
      real    :: sum
      real    :: res1
      real    :: res2
      integer :: num_itr
      integer :: i, j, ii, jj, ic, jc, itr

      real   , parameter :: slon = 140.0
      real   , parameter :: elon = 180.0
      real   , parameter :: slat =  35.0
      real   , parameter :: elat =  39.0

      integer, parameter :: inc = 1
      integer, parameter :: max_itr = 200
      real   , parameter :: res_crt = 1.e-3

! ... copy sst to work array
      allocate(buf(nlon,nlat))
      buf = sst

      num_itr = 0
      res1 = 0
      res2 = 0

      do itr = 1, max_itr
      num_itr = num_itr + 1

      smooth = buf

      do j = 1, nlat
      do i = 1, nlon

      if (rlon(i) < slon) cycle
      if (rlon(i) > elon) cycle
      if (rlat(j) < slat) cycle
      if (rlat(j) > elat) cycle

      if (land(i,j) > 0.5) cycle

        num = 0
        sum = 0
        do jj = j-inc, j+inc
          if (jj < 1 .or. jj > nlat) cycle
          jc = jj

          do ii = i-inc, i+inc
            ic = ii

            if (ic < 1) then
              if (cyclic) then
                ic = ic + nlon
              else
                cycle
              end if
            end if

            if (ic > nlon) then
              if (cyclic) then
                ic = ic - nlon
              else
                cycle
              end if
            end if

            if (land(ic,jc) > 0.5) cycle

            num = num + 1
            sum = sum + buf(ic,jc)

          end do
        end do

        if (num > 1) then
          smooth(i,j) = sum/num
        end if

      end do
      end do

      res2 = 0
      num  = 0
      do j = 1, nlat
      do i = 1, nlon
      if (rlon(i) < slon) cycle
      if (rlon(i) > elon) cycle
      if (rlat(j) < slat) cycle
      if (rlat(j) > elat) cycle

      if (land(i,j) > 0.5) cycle

      res2 = res2 + (sst(i,j) - smooth(i,j))**2.0
      num  = num  + 1
      end do
      end do

      res2 = sqrt(res2/num)
      if ( abs(res2-res1) < res_crt ) exit
      if ( itr >= max_itr ) exit
      res1 = res2
      buf = smooth
      end do

      deallocate(buf)
      print *,' check : ',num_itr,res1,res2,res2-res1

      end subroutine make_smooth

!----------------------------------------------------------------------
! output
!----------------------------------------------------------------------
  subroutine output (iounit, &
             jdate, nlon, nlat, bufr, var_index, level_index)
  implicit none

!--- intent ---
  integer, intent(in) :: iounit
  character(len=24), intent(in) :: jdate
  integer, intent(in) :: nlon, nlat
  integer, intent(in) :: var_index, level_index
  real(4), intent(inout) :: bufr(nlon,nlat)

!--- for MM5 --- ! see REGRID/regridder/Doc/README.v3-format
  integer           :: iversion = 3
  real              :: xfcst    = 0.0
  integer           :: flag     
            ! =(0,1,3,4,5)=(Lat-Lon,Mercator,Lambert,Gaussian,Polar)

  character(len=9)  :: field(3)= (/'SST     ',&
                                   'SEAICE  ',&
                                   'MASKSEA '/)
!                                  'LANDSEA '/)

  character(len=25) :: units(3)= (/'K      ',&
                                   '[1/0]  ',&
                                   '[1/0]  '/)

  character(len=46) :: desc(3) = (/'Sea Surface Temperature',&
                                   'Sea/Ice flag           ',&
                                   'Land/Sea flag          '/)

  real :: startlat, deltalat
  real :: startlon, deltalon
  real :: pressure_slp=201300
  real :: pressure_sfc=200100
  real :: pressure_level(17)=(/100000, 92500, 85000, 70000, 60000, &
                                50000, 40000, 30000, 25000, 20000, &
                                15000, 10000,  7000,  5000,  3000, &
                                 2000,  1000/)

  if (var_index >= 1 .and. var_index <= 3) then
    flag = 0  ! Lat/Lon 
    deltalat= 0.25 ;  startlat=-90.0 + deltalat*(1.0-0.5)
    deltalon= 0.25 ;  startlon=  0.0 + deltalon*(1.0-0.5)
    write(iounit) iversion
    write(iounit) jdate, xfcst,  &
                  field(var_index), units(var_index), desc(var_index), &
                  pressure_sfc, nlon, nlat, flag
    write(iounit) startlat, startlon, deltalat, deltalon
    write(iounit) bufr
  else
    stop 'error : output'
  end if

  return
  end subroutine output
EOF
$FC $OPT pregrid.F90 $NCLIB
./a.out
/bin/rm a.out pregrid.F90 pregrid.namelist

@ year = $year + 1
end

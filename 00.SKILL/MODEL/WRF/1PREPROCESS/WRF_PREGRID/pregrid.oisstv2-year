#!/bin/csh -f

set NCPATH_LIB = /usr/local/netcdf-3.6.3/lib
set NCPATH_INC = /usr/local/netcdf-3.6.3/include
set FC = ifort
set COPT = "-convert big_endian -assume byterecl"

set DIR_PATH = /work/iizuka/WRF/input

set year = 2010
while ($year >= 1993)

@ nyear = $year + 1

cat > pregrid.namelist << EOF
&record1
 START_YEAR  = $year
 START_MONTH = 11
 START_DAY   = 21
 START_HOUR  = 00

 END_YEAR    = $nyear
 END_MONTH   = 03
 END_DAY     = 01
 END_HOUR    = 00
&

&record2
 TARGET_YEAR  = 2005
 FILE_DIR    = '$DIR_PATH'
&
EOF
cat pregrid.namelist

cat > pregrid.F90 << EOF
module netcdf_parameter
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  implicit none
  include 'netcdf.inc'

!--- USER may change ---
  character(len=4) :: tim_name='time'
  character(len=3) :: lon_name='lon'
  character(len=3) :: lat_name='lat'
! character(len=3) :: lev_name='lev'
  character(len=5) :: lev_name='level'
! character(len=5) :: lev_name='depth'

  integer :: ncid
end module netcdf_parameter

program netcdf_read
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  use netcdf_parameter
  implicit none

  integer :: target_year  = 2005
!--- local ---
  integer :: start_year   = 0
  integer :: start_month  = 0
  integer :: start_day    = 0
  integer :: start_hour   = 0
  integer :: start_minute = 0
  integer :: start_second = 0

  integer :: end_year     = 0
  integer :: end_month    = 0
  integer :: end_day      = 0
  integer :: end_hour     = 0
  integer :: end_minute   = 0
  integer :: end_second   = 0

  integer :: interval     = 0

  namelist /record1/ &
  start_year, start_month, start_day, start_hour, start_minute, start_second,&
    end_year,   end_month,   end_day,   end_hour,   end_minute,   end_second,&
    interval

!--- local ---
  logical :: exist
  character(len=200) :: file_dir

  namelist /record2/ &
  target_year, &
  file_dir

!--- dimension size ---
  integer :: ndims, nlon, nlat, nlev, ntim, maxlev

!--- data array ---
  real(4), dimension(:,:), allocatable :: bufr, temp
  character(len=80) :: var_name

!--- local for file ---
  character(len=200) :: file_output, file_input, file_mask
  character(len=200) :: command
  integer :: iounit_file=99

!--- local for time ---
  character(len=24) :: jdate
  integer :: leap_year, loop_year, loop_month, loop_hour, loop_day
  integer :: time_index, level_index, var_index
  integer :: days_per_month(12,0:1)
  days_per_month(1:12,0) = (/31,29,31,30,31,30,31,31,30,31,30,31/)
  days_per_month(1:12,1) = (/31,28,31,30,31,30,31,31,30,31,30,31/)

!----------------------------------------------------------------------
!--- read namelist ---
!----------------------------------------------------------------------
  open (10, file='pregrid.namelist', status='old')
  read (10, record1)
! write( 6, record1)
  read (10, record2)
! write( 6, record2)
  close(10)

!======================================================================
!--- loop for time ---
!======================================================================
  do loop_year  = start_year, end_year 

  if (mod(loop_year,4) == 0) then
    leap_year = 0
  else
    leap_year = 1
  end if

  do loop_month = 1, 12
  do loop_day   = 1, days_per_month(loop_month,leap_year)

!----------------------------------------------------------------------
!--- skip ---
!----------------------------------------------------------------------
    if ( loop_year == start_year ) then
      if ( loop_month < start_month ) cycle
      if ( loop_month == start_month ) then
        if ( loop_day < start_day ) cycle
!       if ( loop_day == start_day ) then
!         if ( loop_hour < start_hour ) cycle
!       end if
      end if
    end if

    if ( loop_year == end_year ) then
      if ( loop_month > end_month ) exit
      if ( loop_month == end_month ) then
        if ( loop_day > end_day ) exit
!       if ( loop_day == end_day ) then
!         if ( loop_hour > end_hour ) exit
!       end if
      end if
    end if

!----------------------------------------------------------------------
!--- set input file (FILE) ---
!----------------------------------------------------------------------
! write(file_mask,'(a,a)') &
!     trim(file_dir),'land-sea-mask-v2.nc'

  write(file_input,'(a,a,i4.4,i2.2,i2.2,a)') &
      trim(file_dir),'/avhrr-only-v2-only-japan-sea.', &
      loop_year,loop_month,loop_day,'.nc'

  inquire(file=trim(file_input),exist=exist)
  if (exist) then
  else
    print *,trim(file_input)
    stop 'no file'
  end if

  do loop_hour  = 0, 23, 6
!----------------------------------------------------------------------
!--- count time_index ---
!----------------------------------------------------------------------
    time_index = 0
    time_index = time_index + 1
!   print *,'time_index',time_index
!   print *,loop_year,loop_month,loop_day,loop_hour
!   cycle 

!----------------------------------------------------------------------
!--- time stamp ---
!----------------------------------------------------------------------
    write(jdate,'(i4,a1,i2.2,a1,i2.2,a1,i2.2,a1,a2,a1,a2)') &
       target_year,'-',loop_month,'-',loop_day,'_',           &
       loop_hour,':','00',':','00'
    write(6,*) jdate

!----------------------------------------------------------------------
!--- open output file (FILE) ---
!----------------------------------------------------------------------
    write(file_output,'(a,i4,a,i4,a1,i2.2,a1,i2.2,a1,i2.2)')  &
       'OISST-MONSOON-',loop_year,':',target_year,'-',loop_month,'-',loop_day,'_',loop_hour
    open(UNIT=iounit_file,FILE=file_output, FORM='unformatted')

!----------------------------------------------------------------------
!--- SST ---
!----------------------------------------------------------------------
    do var_index   = 1, 1
      var_name   = 'sst'
      call netcdf_read_header_dimension(file_input, &
                                        ndims, nlon, nlat, nlev, ntim)
      ndims=3
      allocate(bufr(nlon,nlat))
      allocate(temp(nlon,nlat))
      temp(:,:)=0.0
      call netcdf_read_data(file_input, var_name, &
                            ndims, nlon, nlat,  &
                            temp, 1, time_index)
!     call interp_sst(temp,nlon,nlat)
!     open(80,file='test.dat',form='unformatted',&
!             access='direct',recl=nlon*nlat*4)
!     write(80,rec=1) temp
!     close(80)
!     stop

      where (temp < -9.0)
!!       bufr = 0.0
        bufr = -999.9
      else where
        bufr = max(temp, -1.8)
        bufr = bufr + 273.15
      end where

      call output (iounit_file,  &
                   jdate, nlon, nlat, bufr, var_index, 1)
      deallocate(bufr)
      deallocate(temp)
    end do

    cycle
!----------------------------------------------------------------------
!--- Seaice Flag ---
!----------------------------------------------------------------------
    do var_index   = 2, 2
      var_name   = 'ice'
      call netcdf_read_header_dimension(file_input, &
                                        ndims, nlon, nlat, nlev, ntim)
      allocate(bufr(nlon,nlat))
      bufr(:,:)=0.0
      call netcdf_read_data(file_input, var_name, &
                            ndims, nlon, nlat,  &
                            bufr, 1, time_index)

      where (bufr < -9.0)
        bufr = 0.0
      else where
        bufr = min(max(0.0,bufr),1.0)
      end where

      call output (iounit_file,  &
                   jdate, nlon, nlat, bufr, var_index, 1)
      deallocate(bufr)
    end do

!   cycle
!----------------------------------------------------------------------
!--- Landsea Flag ---
!----------------------------------------------------------------------
    do var_index   = 3, 3
      var_name   = 'mask'
      call netcdf_read_header_dimension(file_mask, &
                                        ndims, nlon, nlat, nlev, ntim)
      allocate(bufr(nlon,nlat))
      allocate(temp(nlon,nlat))
      temp(:,:)=0.0
      call netcdf_read_data(file_mask, var_name, &
                            ndims, nlon, nlat,  &
                            temp, 1, time_index)

      where (temp < 0.5)
        bufr = 1.0  ! land
      else where
        bufr = 0.0  ! ocean
      end where

      call output (iounit_file,  &
                   jdate, nlon, nlat, bufr, var_index, 1)
      deallocate(temp)
      deallocate(bufr)
    end do

  end do
  end do
  end do
  end do

  stop ' normal end '
  
contains

!----------------------------------------------------------------------
! output
!----------------------------------------------------------------------
  subroutine output (iounit, &
             jdate, nlon, nlat, bufr, var_index, level_index)
  implicit none

!--- intent ---
  integer, intent(in) :: iounit
  character(len=24), intent(in) :: jdate
  integer, intent(in) :: nlon, nlat
  integer, intent(in) :: var_index, level_index
  real(4), intent(inout) :: bufr(nlon,nlat)

!--- for MM5 --- ! see REGRID/regridder/Doc/README.v3-format
  integer           :: iversion = 3
  real              :: xfcst    = 0.0
  integer           :: flag     
            ! =(0,1,3,4,5)=(Lat-Lon,Mercator,Lambert,Gaussian,Polar)

  character(len=9)  :: field(3)= (/'SST     ',&
                                   'SEAICE  ',&
                                   'MASKSEA '/)
!                                  'LANDSEA '/)

  character(len=25) :: units(3)= (/'K      ',&
                                   '[1/0]  ',&
                                   '[1/0]  '/)

  character(len=46) :: desc(3) = (/'Sea Surface Temperature',&
                                   'Sea/Ice flag           ',&
                                   'Land/Sea flag          '/)

  real :: startlat, deltalat
  real :: startlon, deltalon
  real :: pressure_slp=201300
  real :: pressure_sfc=200100
  real :: pressure_level(17)=(/100000, 92500, 85000, 70000, 60000, &
                                50000, 40000, 30000, 25000, 20000, &
                                15000, 10000,  7000,  5000,  3000, &
                                 2000,  1000/)

  if (var_index >= 1 .and. var_index <= 3) then
    flag = 0  ! Lat/Lon 
    deltalat= 0.25 ;  startlat=-90.0 + deltalat*(1.0-0.5)
    deltalon= 0.25 ;  startlon=  0.0 + deltalon*(1.0-0.5)
    write(iounit) iversion
    write(iounit) jdate, xfcst,  &
                  field(var_index), units(var_index), desc(var_index), &
                  pressure_sfc, nlon, nlat, flag
    write(iounit) startlat, startlon, deltalat, deltalon
    write(iounit) bufr
  else
    stop 'error : output'
  end if

  return
  end subroutine output

!----------------------------------------------------------------------
! check NetCDF error
!----------------------------------------------------------------------
  subroutine netcdf_check (status, text)

  implicit none

  include 'netcdf.inc'

!--- intent ---
  integer, intent(in) :: status
  character(len=*), intent(in) :: text

  if (status /= nf_noerr) then
    write(*,*) text
    write(*,*) nf_strerror(status)
    stop
  end if

  return
  end subroutine netcdf_check

!----------------------------------------------------------------------
! read netCDF header
!----------------------------------------------------------------------
  subroutine netcdf_read_header_dimension(file_name, &
             ndims, nlon, nlat, nlev, ntim)

  use netcdf_parameter
  implicit none

!--- intent ---
  integer, intent(out) :: ndims, nlon, nlat, nlev, ntim
  character(len=*), intent(in) :: file_name

!--- local ---
  integer :: status
  integer :: xid, yid, zid, tid
  character(len=300) :: text

!--- open file ---
  status = nf_open(file_name, nf_nowrite, ncid)
  call netcdf_check(status,'failed to open netcdf file')

!--- check global attribute ---
  text = ' '
  status = nf_get_att_text(ncid, nf_global, 'title', text)
  call netcdf_check(status,'cannot read global attribute')
! write(*,*) trim(text)

!--- check number of dimensions ---
  status = nf_inq_ndims(ncid, ndims)
  call netcdf_check(status,'cannot read number of dimensions')

!--- check name & size of dimension for longitude ---
  status = nf_inq_dimid (ncid, lon_name, xid)
  call netcdf_check(status,'cannot read dimension for longitude')
  status = nf_inq_dimlen(ncid,  xid, nlon)
  call netcdf_check(status,'cannot read dimension for longitude')

!--- check name & size of dimension for latitude ---
  status = nf_inq_dimid (ncid, lat_name, yid)
  call netcdf_check(status,'cannot read dimension for latitude')
  status = nf_inq_dimlen(ncid,  yid, nlat)
  call netcdf_check(status,'cannot read dimension for latitude')

!--- check name & size of dimension for level ---
  status = nf_inq_dimid (ncid, lev_name, zid)
  if (status == nf_noerr) then
    status = nf_inq_dimlen(ncid,  zid, nlev)
    call netcdf_check(status,'cannot read dimension for level')
  else
    nlev = 1  ! 2 dimensional data (e.g. SST)
  end if

!--- check name & size of dimension for time ---
  status = nf_inq_dimid (ncid, tim_name, tid)
  call netcdf_check(status,'cannot read dimension for time')
  status = nf_inq_dimlen(ncid,  tid, ntim)
  call netcdf_check(status,'cannot read dimension for time')

!--- close file ---
  status = nf_close(ncid)
  call netcdf_check(status,'failed to close netcdf file')

  return
  end subroutine netcdf_read_header_dimension

!----------------------------------------------------------------------
! read netCDF file
!----------------------------------------------------------------------
  subroutine netcdf_read_data(file_name, varname, ndims, nlon, nlat, &
                              bufr, irec_level, irec_time)

  use netcdf_parameter
  implicit none

!--- intent ---
  integer, intent(in) :: ndims, nlon, nlat
  character(len=*), intent(in) :: file_name
  character(len=*), intent(in) :: varname
  real(4), intent(inout) :: bufr(nlon,nlat)
  integer, intent(in) :: irec_time, irec_level

!--- local ---
  logical :: debug = .false.
! logical :: debug = .true.
  integer :: status
  integer :: varid
  integer, dimension(:), allocatable :: start
  integer, dimension(:), allocatable :: count
  integer(1), dimension(:,:), allocatable :: bufr_i1
  integer(2), dimension(:,:), allocatable :: bufr_i2
  integer(4), dimension(:,:), allocatable :: bufr_i4
  real(4),    dimension(:,:), allocatable :: bufr_r4
  real(8),    dimension(:,:), allocatable :: bufr_r8

  integer :: idims
  integer :: ivars
  integer :: xtype
  real(4) :: scale, offset, missing_value

  integer :: i,j,k

  character(len=80) :: long_name = ' '
  character(len=80) :: unit = ' '
  character(len=38), dimension(6) :: what_type = &
  (/ 'NCBYTE   (8 bit data                )',  &
     'NCCHAR   (character                 )',  &
     'NCSHORT  (16 bit integer            )',  &
     'NCLONG   (32 bit integer            )',  &
     'NCFLOAT  (32 bit IEEE floating-point)',  &
     'NCDOUBLE (64 bit IEEE floating-point)'/)

!--- open file ---
  status = nf_open(file_name, nf_nowrite, ncid)
  call netcdf_check(status,'failed to open netcdf file')

!--- get ID ---
  status = nf_inq_varid(ncid, varname, varid)
  if (status /= 0) then
    print *,'',varname
  end if
  call netcdf_check(status,'cannot read id')

!--- check num. of dimensions ---
  status = nf_inq_varndims(ncid, varid, idims)
  call netcdf_check(status,'cannot read dimension')
  if (idims /= ndims) then
    write(*,*) 'num. of dimension for parameter = ',ndims
    write(*,*) 'num. of dimension for netCDF    = ',idims
    stop 'num. of variable dimension'
  end if

!--- get attribute ---
  long_name = ''
  status = nf_get_att_text(ncid, varid, 'long_name'    ,long_name)
  call netcdf_check(status,'cannot read attribute')
  unit = ''
  status = nf_get_att_text(ncid, varid, 'units'        ,unit)
  status = nf_inq_vartype (ncid, varid, xtype)
  call netcdf_check(status,'cannot read variable type')
  call netcdf_check(status,'cannot read attribute')
  status = nf_get_att_real(ncid, varid, 'scale_factor' ,scale)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    scale = 1.0
  end if
  status = nf_get_att_real(ncid, varid, 'add_offset'   ,offset)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    offset = 0.0
  end if
  status = nf_get_att_real(ncid, varid, 'missing_value',missing_value)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    missing_value = -9.99e33
  end if

  if (debug) then
    write(*,*) ' '
    write(*,*) ' ',irec_time, irec_level
    write(*,*) 'long name         =',trim(long_name)
    write(*,*) 'Variable name     =',trim(varname)
    write(*,*) 'Variable ID       =',varid
    write(*,*) 'units             =',trim(unit)
    write(*,*) 'num. of dimension =',idims
    write(*,*) 'size of dimension =',nlon, nlat
    write(*,*) 'type of variable  =',what_type(xtype)
    write(*,*) 'missing value     =',missing_value
    write(*,*) 'scale            =',scale
    write(*,*) 'offset           =',offset
    write(*,*) ' '
  end if

!--- set count & start ---
  allocate(start(ndims)) ; allocate(count(ndims))
  select case (ndims)
    case (3)
      count(1)=nlon  ; start(1)=1
      count(2)=nlat  ; start(2)=1
      count(3)=1     ; start(3)=irec_time
    case (4)
      count(1)=nlon  ; start(1)=1
      count(2)=nlat  ; start(2)=1
      count(3)=1     ; start(3)=irec_level
      count(4)=1     ; start(4)=irec_time
    case default
      stop 'count start'
  end select

!--- get data ---
  select case (xtype)
    case (nf_byte)
      allocate(bufr_i1(nlon,nlat))
      status = nf_get_vara_int1  (ncid, varid, start, count, bufr_i1)
      bufr(:,:) = bufr_i1(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_i1 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_i1)
    case (nf_short)
      allocate(bufr_i2(nlon,nlat))
      status = nf_get_vara_int2(ncid, varid, start, count, bufr_i2)
      bufr(:,:) = bufr_i2(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_i2 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_i2)
    case (nf_int)
      allocate(bufr_i4(nlon,nlat))
      status = nf_get_vara_int   (ncid, varid, start, count, bufr_i4)
      bufr(:,:) = bufr_i4(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_i4 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_i4)
    case (nf_real)
      allocate(bufr_r4(nlon,nlat))
      status = nf_get_vara_real  (ncid, varid, start, count, bufr_r4)
      bufr(:,:) = bufr_r4(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_r4 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_r4)
    case (nf_double)
      allocate(bufr_r8(nlon,nlat))
      status = nf_get_vara_double(ncid, varid, start, count, bufr_r8)
      bufr(:,:) = bufr_r8(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_r8 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_r8)
    case default
      allocate(bufr_r4(nlon,nlat))
      status = nf_get_vara_real  (ncid, varid, start, count, bufr_r4)
      bufr(:,:) = bufr_r4(:,:)
      bufr(:,:) = bufr(:,:) * scale + offset
      where (bufr_r4 == missing_value) 
        bufr(:,:) = -9.99e33
      end where
      deallocate(bufr_r4)
  end select

  deallocate(start)
  deallocate(count)

!--- close file ---
  status = nf_close(ncid)
  call netcdf_check(status,'failed to close netcdf file')

  return
  end subroutine netcdf_read_data
end program netcdf_read

  subroutine interp_sst(sst,nlon,nlat)
  integer :: nlon, nlat
  real    :: sst(nlon,nlat)
  real    :: weight(8), xxx1, xxx2
  integer :: iw, ie, js, jn
  real, allocatable :: guess(:,:)

  allocate(guess(nlon,nlat))
  undef = -9.0
  print *,'',sst(1,1)

  do itr = 1, 100

    num = 0
    do j = 1, nlat
    do i = 1, nlon
      if (sst(i,j) < undef) then
        num = num + 1
      end if
    end do
    end do
    if (num == 0) return

    do j = 1, nlat
    do i = 1, nlon
      guess(i,j) = sst(i,j)
      if (sst(i,j) < undef) then
        iw = i-1 ; if (iw < 1) iw = nlon
        ie = i+1 ; if (ie > nlon) ie = 1
        js = j-1 ; if (js < 1   ) js = 1
        jn = j+1 ; if (jn > nlat) jn = nlat
        xxx1   = 0.0
        xxx2   = 0.0
        weight(:) = 1.0
        if (sst(iw,j ) < undef) weight(1) = 0.0
        if (sst(iw,jn) < undef) weight(2) = 0.0
        if (sst(i ,jn) < undef) weight(3) = 0.0
        if (sst(ie,jn) < undef) weight(4) = 0.0
        if (sst(ie,j ) < undef) weight(5) = 0.0
        if (sst(ie,js) < undef) weight(6) = 0.0
        if (sst(i ,js) < undef) weight(7) = 0.0
        if (sst(iw,js) < undef) weight(8) = 0.0
        xxx1 = sst(iw,j )*weight(1) &
             + sst(iw,jn)*weight(2) &
             + sst(i ,jn)*weight(3) &
             + sst(ie,jn)*weight(4) &
             + sst(ie,j )*weight(5) &
             + sst(ie,js)*weight(6) &
             + sst(i ,js)*weight(7) &
             + sst(iw,js)*weight(8) 
        xxx2  = sum(weight(1:8))
        if (xxx2 > 0.0) guess(i,j) = xxx1/xxx2
      end if

    end do
    end do

    do j = 1, nlat
    do i = 1, nlon
      sst(i,j) = guess(i,j)
    end do
    end do
  end do

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt1 = ',num

  do j = 1, nlat
    do k = 1, 2
      do i = 1, nlon
        if (sst(i,j) < undef) then
          iw = i-1 ; if (iw < 1   ) iw = nlon
          ie = i+1 ; if (ie > nlon) ie = 1
          if (sst(iw,j) > undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = 0.5*(sst(iw,j)+sst(ie,j))
          else if (sst(iw,j) > undef .and. &
            sst(ie,j) < undef) then
            sst(i,j) = sst(iw,j)
          else if (sst(iw,j) < undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = sst(ie,j)
          else
          end if
        end if
      end do
    end do
  end do

  do i = 1, nlon
    do j = nlat/2, 1, -1
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j+1)
      end if
    end do
    do j = nlat/2+1, nlat
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j-1)
      end if
    end do
  end do

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt2 = ',num

  deallocate(guess)
  return
  end subroutine interp_sst
EOF
$FC $COPT -L$NCPATH_LIB -I$NCPATH_INC pregrid.F90 -lnetcdf
./a.out
/bin/rm a.out pregrid.F90 pregrid.namelist

@ year = $year - 1
end

#!/bin/sh

CASE=$1; RUN1=$2; RUN2=$3
CASE=${CASE:-RW3A.00.05.05.05}; RUN1=0000; RUN2=0097
CASEC=${CASEC:-RW3A.00.03.05.05}; RUNC=0802

runname1=${runname1:-${CASE}.${RUN1}}
runname2=${runname2:-${CASE}.${RUN2}}
runnameC=${runname3:-${CASEC}.${RUNC}}

OUTCASE=RW3A.00.05.05.05
runnameO=RW3A.00.05.05.05.${RUN2}

INDIR=/work04/manda/WPS.OUT_work04/RW3A.009X.WPS/${runname1}
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR, $INDIR;exit 1;fi
INDIRC=/work04/manda/WPS.OUT_work04/RW3A.WPS/${runnameC}
if [ ! -d $INDIRC ];then echo EEEEE NO SUCH DIR, $INDIRC;exit 1;fi
INC=$INDIRC/met_em.d01.2021-08-10_00:00:00.nc
if [ ! -f $INC ];then echo EEEEE NO SUCH FILE, $INC;exit 1;fi

ODIR=/work04/manda/WPS.OUT_work04/RW3A.009X.WPS/${runnameO}
mkd $ODIR
cp -r $INDIR/*nc $ODIR
chmod u+w $ODIR/*nc

domain=d01

ncl=$(basename $0 .sh).ncl
if [ ! -f $ncl ];then echo EEEEE NO SUCH FILE, $ncl ;exit 1 ;fi

INLIST=$(ls $ODIR/met_em*nc)
for IN in $INLIST; do
runncl.sh $ncl $OUTCASE $RUN2 $IN $RUNC $INC
echo 
done

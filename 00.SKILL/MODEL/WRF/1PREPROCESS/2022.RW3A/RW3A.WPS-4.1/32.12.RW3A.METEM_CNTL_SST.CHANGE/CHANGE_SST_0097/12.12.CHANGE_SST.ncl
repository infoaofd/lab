; CHANGE SST 0096
; Wed, 23 Oct 2024 14:02:05 +0900
; /work04/$(whoami)/2022.RW3A/RW3A.WPS-4.1/32.12.RW3A.METEM_CNTL_SST.CHANGE/CHANGE_SST_0095
; 4.      If you would like to reveal the impact of cold (warm) SST anomalies, an additional sensitivity experiments without only cold (warm) SST anomalies would be useful.
; もし、海面水温の寒色偏差（暖色偏差）の影響を明らかにしたいのであれば、海面水温の寒色偏差（暖色偏差）だけを除いた感度実験を追加することが有効であろう。

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin

wallClock1 = systemfunc("date") ; retrieve wall clock time

scriptname_in = getenv("NCL_ARG_1")
;print("script name ="+scriptname_in)
scriptname=systemfunc("basename " + scriptname_in + " .ncl")

CASE = getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
IN = getenv("NCL_ARG_4")
RUNC = getenv("NCL_ARG_5")
INC = getenv("NCL_ARG_6")

print("MMMMM INPUT: "+IN)
print("MMMMM INC: "+INC)
;print("")

FIGDIR="FIG_CHANGE_SST_"+CASE+"_"+RUN+"/"
system("mkdir -vp "+FIGDIR)

; The WRF ARW input file.  
; This needs to have a ".nc" appended, so just do it.
f0 = addfile(IN,"rw")
sst0in=  f0->SST
lon0in = f0->XLONG_M
lat0in = f0->XLAT_M
msk0in = f0->LANDMASK
time = chartostring(f0->Times(0,:))

fC = addfile(INC,"r")
sstCin=  fC->SST
lonCin = fC->XLONG_M
latCin = fC->XLAT_M


DSST=sst0in-sstCin
LONE=131.5
print("MMMMM")
print("NNNNN CHANGE SST: LONE="+LONE +" NNNNN")
sst0out = where((DSST .le. 0 ) , sstCin+1e-5, sst0in) 

print("NNNNN DSST .le. 0 ")
print("MMMMM")

; print("NNNNN SMOOTHING smth9_Wrap(sst0in, 0.50, -0.25, False)")
; sst0out = smth9_Wrap(sst0in, 0.50, -0.25, False)
; print("MMMMM")

print("MMMMM WRITE NEW SST")
print("MMMMM SST in FILE f0 is sst0out") ;sst0in.")
print("MMMMM FILE f0 = "+IN)
f0->SST = sst0out ; sst0in
print("MMMMM")

; We generate plots, but what kind do we prefer?
typ = "PDF" ;ps x11  pdf  ngcm

; Set some Basic Plot options
res = True

dsizes  = new( (/3/),integer)
dsizes  = dimsizes(sst0in)
;print(dsizes)
im=dsizes(1)
jm=dsizes(2)
lon=new( (/im,jm/), typeof(lon0in))
lat=new( (/im,jm/), typeof(lat0in))
sst0=new( (/im,jm/), typeof(sst0in))
msk0=new( (/im,jm/), typeof(msk0in))

sst0@_FillValue = default_fillvalue(typeof(sst0in))

figfile= FIGDIR + "SST_"+CASE+"_"+RUN+"_" + systemfunc("basename "+IN+" .nc" )
print("MMMMM fig file = " + figfile +"."+typ)

wks = gsn_open_wks(typ, figfile)


gsn_define_colormap(wks,"matlab_jet")

; print("MMMMM 3D -> 2D FOR PLOTTING")
lon(:,:)=lon0in(0,:,:)
lat(:,:)=lat0in(0,:,:)
msk0(:,:)=msk0in(0,:,:)
sst0(:,:)=sst0out(0,:,:)-273.15


; Plotting options
opts = res

opts@tiMainString    = CASE+" "+RUN ; add titles
opts@gsnCenterString = time
opts@gsnLeftString = ""
opts@gsnAddCyclic = False
opts@sfXArray = lon
opts@sfYArray = lat
opts@mpMinLonF            = min(lon)               ; select a subregion
opts@mpMaxLonF            = max(lon)
opts@mpMinLatF            = min(lat)
opts@mpMaxLatF            = max(lat)

opts@mpDataBaseVersion    = "HighRes" ; fill land in gray and ocean in transparent

opts@mpFillOn = True
opts@mpFillColors = (/"background","transparent","gray","transparent"/)
opts@mpFillDrawOrder = "PreDraw"; "PostDraw"

opts@cnFillOn = True
opts@cnLinesOn            = False            ; turn off contour lines
opts@pmLabelBarOrthogonalPosF = 0.0

opts@lbOrientation            = "Horizontal" ;"Vertical"     ; orientation of label bar
opts@pmLabelBarOrthogonalPosF = 0.1          ; move label bar closer
opts@lbLabelFontHeightF = 0.015
opts@lbLabelStride            = 1
opts@lbTitleString = "deg.C"
opts@lbTitlePosition = "bottom"
opts@lbTitleFontHeightF = 0.015
opts@pmLabelBarHeightF = 0.08

opts@gsnDraw          = False                ; turn off draw and frame
opts@gsnFrame         = False                ; b/c this is an overlay plot

opts@cnLevelSelectionMode = "ManualLevels"   ; set manual contour levels
opts@cnMinLevelValF       =   22.              ; set min contour level
opts@cnMaxLevelValF       =   32.             ; set max contour level
opts@cnLevelSpacingF      =   0.2              ; set contour spacing

sst_plt=sst0
;sst_plt = where(msk0.eq.1, \
;                sst0@_FillValue, sst0)
plot = gsn_csm_contour_map(wks,sst_plt,opts)

;delete(opts)

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")

gsn_text_ndc(wks,IN,  0.05,0.875,txres)
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)


; MAKE PLOTS
draw(plot)
frame(wks)
end

#!/bin/sh

# /work06/manda/WRF3.7.1/METEM.CHECK
CASE=RW3A
RUN=00.05
PARAM=05.05
MM=00
BP=97
BIPEN=${MM}${BP}
runname0=${CASE}.${RUN}.${PARAM}.${BIPEN} #CNTL

RUN1=0000; RUN2=0096; RUNC=0802
runnameC=RW3A.00.03.05.05.${RUNC}

domain=d01

start=20210810
  end=20210814
#  end=20180708

exe=runncl.sh

ncl=$(basename $0 .sh).ncl
if [ ! -f $ncl ];then echo EEEEE 1 NO SUCH FILE, $ncl ;exit 1 ;fi

#indir_root=/work04/manda/WPS.OUT_CHK
#indir_root=/work04/manda/WPS.OUT_work04

if [ ! -d $indir_root ]; then
echo
echo ERROR in $0 : NO SUCH DIR, $indir_root
echo
exit 1
fi

indir0=/work04/manda/WPS.OUT_work04/RW3A.009X.WPS/${runname0}

hhlist="00 12 18"
#hhlist="00 03 06 09 12 15 18 21" # 06 12 18"
#hhlist="18 19 20 21 22 23" # 06 12 18"
starts=$(date -d "${start}" '+%s')
ends=$(date -d "${end}" '+%s')

times=$(expr $ends - $starts)
days=$(expr $times / 86400 )

sdate=${start:0:4}/${start:4:2}/${start:6:2}
#echo $sdate
yesterday=$(date -d"$sdate"-1days '+%Y/%m/%d')
#echo $yesterday

INDIRC=/work04/manda/WPS.OUT_work04/RW3A.WPS/${runnameC}
if [ ! -d $INDIRC ];then echo EEEEE 2 NO SUCH DIR, $INDIRC;exit 1;fi
INC=$INDIRC/met_em.d01.2021-08-10_00:00:00.nc
if [ ! -f $INC ];then echo EEEEE 3 NO SUCH FILE, $INC;exit 1;fi

n=0
while [ $n -le $days ]; do

today=$(date -d"$yesterday"+1days '+%Y-%m-%d')

for hh in $hhlist; do
datetime=${today}_${hh}:00:00

echo $datetime

met_em_file="met_em.${domain}.${datetime}.nc"
in=${indir0}/${met_em_file}
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi

$exe $ncl "$runname0" "$indir0" "$domain" "$datetime" "$CASE" "$RUN" "$RUNC" "$INC"

done #hh

n=$(expr $n + 1)
yesterday=$today
done #n

exit 0

#!/bin/bash
#
# Wed, 29 Jan 2020 16:17:06 +0900
# calypso.bosai.go.jp
# /work06/manda/WRF.PRE.H30/SST.ERA5.1HR/CHECK.INPUT
# manda
#

LOG=$(basename $0 .sh).LOG
# LOG=$(basename $0 .sh)_$(date +"%y%m%d_%H%M").LOG

date -R  |tee    $LOG
hostname |tee -a $LOG
pwd      |tee -a $LOG
ls -lh --time-style=long-iso $(basename $0) |tee -a $LOG
echo     |tee -a $LOG

GS=$(basename $0 .sh).GS

INDIR=/work05/manda/DATA/ERA5/6HR/NetCDF
YYYY=$1
MM=$2
DD=$3
HH=$4
MMM=$5

YYYYMMDD=${YYYY}${MM}${DD}
NC=ERA5_${YYYYMMDD}_SST.nc
TIME=${HH}Z${DD}${MMM}${YYYY}

FIG=$(basename $NC .nc)_${YYYYMMDD}_${HH}.eps
cat <<EOF>$GS
'sdfopen $INDIR/$NC'
'q ctlinfo'
say result

'set time $TIME'
'cc'
'set gxout shaded'
'color 6 31 1 -kind rainbow'
'd sst-273.15'
'cbarn'
'set gxout contour'
'set cmin 6'
'set cmax 31'
'set cint 1'
'd sst-273.15'
'gxprint $FIG'
say
'!ls -lh $FIG'
say
'quit'
EOF

grads -bcp "$GS"

echo |tee -a $LOG
echo "Done $(basename $0)" |tee -a $LOG
echo |tee -a $LOG
echo "LOG: $LOG" 
echo

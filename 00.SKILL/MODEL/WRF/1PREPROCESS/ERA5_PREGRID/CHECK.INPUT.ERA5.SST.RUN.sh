#!/bin/bash

EXE=$(basename $0 .RUN.sh).sh

Y=2018
M=06
D=26
ND=13

I=0
while [ $I -le $ND ]; do

yyyy=$(date -d"${Y}/${M}/${D} $I day" +"%Y %m %d" |awk '{print $1}')
mm=$(date   -d"${Y}/${M}/${D} $I day" +"%Y %m %d" |awk '{print $2}')
dd=$(date   -d"${Y}/${M}/${D} $I day" +"%Y %m %d" |awk '{print $3}')


if [ $mm = "01" ]; then mmm="Jan"; fi
if [ $mm = "02" ]; then mmm="Feb"; fi
if [ $mm = "03" ]; then mmm="Mar"; fi
if [ $mm = "04" ]; then mmm="Apr"; fi
if [ $mm = "05" ]; then mmm="May"; fi
if [ $mm = "06" ]; then mmm="Jun"; fi
if [ $mm = "07" ]; then mmm="Jul"; fi
if [ $mm = "08" ]; then mmm="Aug"; fi
if [ $mm = "09" ]; then mmm="Sep"; fi
if [ $mm = "10" ]; then mmm="Oct"; fi
if [ $mm = "11" ]; then mmm="Nov"; fi
if [ $mm = "12" ]; then mmm="Dec"; fi
echo "$yyyy $mm $dd $mmm"

hh_list="00 06 12 18"
for hh in $hh_list; do
$EXE $yyyy $mm $dd $hh $mmm
done


I=$(expr $I + 1)
done


#!/bin/bash
#
# Fri, 07 Feb 2020 16:53:43 +0900
# calypso.bosai.go.jp
# /work06/manda/WRF.PRE.H30/SST.ERA5.6HR/ERA5_PREGRID
# manda
#

LOG=$(basename $0 .sh).LOG
# LOG=$(basename $0 .sh)_$(date +"%y%m%d_%H%M").LOG

date -R  |tee    $LOG
hostname |tee -a $LOG
pwd      |tee -a $LOG
ls -lh --time-style=long-iso $(basename $0) |tee -a $LOG
echo     |tee -a $LOG

ulimit -s unlimited

FC=ifort
SRC=$(basename $0 .sh).f90
EXE=$(basename $0 .sh).exe

NCPATH_LIB=/usr/local/netcdf4/lib
NCPATH_INC=/usr/local/netcdf4/include
COPT=" -convert big_endian -assume byterecl " #-traceback -CB"


$FC $SRC -o $EXE $COPT  -L$NCPATH_LIB -I$NCPATH_INC \
-lnetcdf -lnetcdff 2>&1 |tee -a $LOG

echo >>$LOG
ls -lh --time-style=long-iso $EXE >>$LOG
echo >>$LOG

./$EXE  2>&1 |tee -a $LOG

#/bin/rm $EXE #pregrid.F90 pregrid.namelist


echo |tee -a $LOG
echo "Done $(basename $0)" |tee -a $LOG
echo |tee -a $LOG
echo "LOG: $LOG" 
echo

netcdf ERA5_20180626_SST {
dimensions:
	longitude = 281 ;
	latitude = 201 ;
	time = 4 ;
variables:
	float longitude(longitude) ;
		longitude:units = "degrees_east" ;
		longitude:long_name = "longitude" ;
	float latitude(latitude) ;
		latitude:units = "degrees_north" ;
		latitude:long_name = "latitude" ;
	int time(time) ;
		time:units = "hours since 1900-01-01 00:00:00.0" ;
		time:long_name = "time" ;
		time:calendar = "gregorian" ;
	short sst(time, latitude, longitude) ;
		sst:scale_factor = 0.000444358066737369 ;
		sst:add_offset = 290.993186024092 ;
		sst:_FillValue = -32767s ;
		sst:missing_value = -32767s ;
		sst:units = "K" ;
		sst:long_name = "Sea surface temperature" ;

// global attributes:
		:Conventions = "CF-1.6" ;
		:history = "2020-02-07 05:34:05 GMT by grib_to_netcdf-2.16.0: /opt/ecmwf/eccodes/bin/grib_to_netcdf -o /cache/data3/adaptor.mars.internal-1581053642.8701572-14279-21-0024d647-7f74-490b-aab0-cf6b47fc8e99.nc /cache/tmp/0024d647-7f74-490b-aab0-cf6b47fc8e99-adaptor.mars.internal-1581053642.870697-14279-8-tmp.grib" ;
}

#!/bin/bash

LOG=$(basename $0 .sh)_$(myymd).LOG
#LOG=$(basename $0 .sh)_$(mydate).LOG

echo 2>&1 |tee   $LOG
echo PREGRID TIME INTERPOLATION. 2>&1 |tee -a $LOG
echo 2>&1 |tee -a $LOG


ulimit -s unlimited

NCPATH_LIB=/usr/local/netcdf4/lib
NCPATH_INC=/usr/local/netcdf4/include
FC=ifort
COPT="-convert big_endian -assume byterecl " #-traceback -CB"

#=============================================
OUTPUT_INTERVAL=1.0 # HR
#=============================================

#dset="ABOM"
#suffix="-ABOM-L4LRfnd-GLOB-v01-fv01_0-GAMSSA_28km.nc"
#
#dset="CMC"
#suffix="120000-CMC-L4_GHRSST-SSTfnd-CMC0.2deg-GLOB-v02.0-fv02.0.nc"
#dset="REMSS_mw_OI"
#suffix="120000-REMSS-L4_GHRSST-SSTsubskin-MW-mw.fusion.v04.0-v02.0-fv01.0.nc"
#dset="JPL"
#suffix="-JPL-L4UHfnd-GLOB-v01-fv04-MUR.nc"
#dset="NCEI"
#suffix="-NCDC-L4LRblend-GLOB-v01-fv02_0-AVHRR_OI.nc"
#dset="REMSS_mw_ir_OI"
#suffix="-REMSS-L4HRfnd-GLOB-v01-fv03-mw_ir_rt_OI.nc"
#
#dset="JPL_OUROCEAN"
#suffix="-JPL_OUROCEAN-L4UHfnd-GLOB-v01-fv01_0-G1SST.nc"
#
#dset="NAVO"
#suffix="-NAVO-L4HR1m-GLOB-v01-fv01_0-K10_SST.nc"

dset="UKMO"
suffix="-UKMO-L4HRfnd-GLOB-v01-fv02-OSTIA.nc"

indir="/work05/manda/DATA/SST/2018/"


DSET_CHECK="${dset}_CHECK"
ODIR="OUT/${dset}"
CDIR="CHK/${dset}"

mkdir -vp $ODIR $CDIR

cat > pregrid.namelist << EOF
&record1
 START_YEAR  = 2018
 START_MONTH = 07
 START_DAY   = 03
 START_HOUR  = 00

 END_YEAR    = 2018
 END_MONTH   = 07
 END_DAY     = 08
 END_HOUR    = 00
&

EOF
cat pregrid.namelist


cat > pregrid.F90 << EOF
module netcdf_parameter
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  implicit none
  include 'netcdf.inc'

!--- USER may change ---
  character(len=4) :: tim_name='time'
  character(len=3) :: lon_name='lon'
  character(len=3) :: lat_name='lat'
! character(len=3) :: lev_name='lev'
  character(len=5) :: lev_name='level'
! character(len=5) :: lev_name='depth'

  integer :: ncid
end module netcdf_parameter

program netcdf_read
!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
  use netcdf_parameter
  implicit none

!--- local ---
  integer :: start_year   = 0
  integer :: start_month  = 0
  integer :: start_day    = 0
  integer :: start_hour   = 0
  integer :: start_minute = 0
  integer :: start_second = 0

  integer :: end_year     = 0
  integer :: end_month    = 0
  integer :: end_day      = 0
  integer :: end_hour     = 0
  integer :: end_minute   = 0
  integer :: end_second   = 0

  integer :: interval     = 0

  namelist /record1/ &
  start_year, start_month, start_day, start_hour, start_minute, start_second,&
    end_year,   end_month,   end_day,   end_hour,   end_minute,   end_second,&
    interval

!--- local ---
  logical :: exist

!--- dimension size ---
  integer :: ndims, nlon, nlat, nlev, ntim

!--- data array ---
  real(4), dimension(:,:), allocatable :: buf1,buf2,buf
  real(4), dimension(:), allocatable :: lon, lat
  real(8) :: dlon8, dlat8
  real(4) :: dlon, dlat
  real(4) :: elon, elat
  real(4) :: slon, slat
  character(len=80) :: vname
  real(4) :: undef = -999.9

!--- local for file ---
  character(len=1000) :: ofile, ifile,ifile1,ifile2, mfile, cfile
  character(len=1000) :: command
  integer :: io=99, irec

!--- local for smoothing
  real(4), dimension(:,:), allocatable :: smooth
  real    :: ave
  integer :: num
  integer :: i, j, ii, jj, iii, jjj

!--- local for time ---
  character(len=24) :: jdate
  integer :: leap_year, iyr, mon, ihr, loop_day, loop_day_p1
  integer :: time_index, level_index, var_index
  integer :: days_per_month(12,0:1)

! TIME INTERPOLATION
  real hr1dy,hr_now, wgt1, wgt2

  days_per_month(1:12,0) = (/31,29,31,30,31,30,31,31,30,31,30,31/)
  days_per_month(1:12,1) = (/31,28,31,30,31,30,31,31,30,31,30,31/)



!----------------------------------------------------------------------
!--- read namelist ---
!----------------------------------------------------------------------
  open (10, file='pregrid.namelist', status='old')
  read (10, record1)
! write( 6, record1)
  close(10)

!======================================================================
!--- loop for time ---
!======================================================================
  do iyr  = start_year, end_year 

  if (mod(iyr,4) == 0) then
    leap_year = 0
  else
    leap_year = 1
  end if

  do mon = 1, 12
  do loop_day   = 1, days_per_month(mon,leap_year)

!----------------------------------------------------------------------
!--- skip ---
!----------------------------------------------------------------------
    if ( iyr == start_year ) then
      if ( mon < start_month ) cycle
      if ( mon == start_month ) then
        if ( loop_day < start_day ) cycle
!       if ( loop_day == start_day ) then
!         if ( ihr < start_hour ) cycle
!       end if
      end if
    end if

    if ( iyr == end_year ) then
      if ( mon > end_month ) exit
      if ( mon == end_month ) then
        if ( loop_day > end_day ) exit
!       if ( loop_day == end_day ) then
!         if ( ihr > end_hour ) exit
!       end if
      end if
    end if

!----------------------------------------------------------------------
!--- set input file (FILE) ---
!----------------------------------------------------------------------
! TODAY
    write(ifile1,'(a,i4.4,i2.2,i2.2,a)') &
    '${indir}/${dset}/',iyr,mon,loop_day,'${suffix}'

    inquire(file=trim(ifile1),exist=exist)
    if (exist) then
      print *
      print *,'ifile1 = ',trim(ifile1)
      print *
    else
      print *,trim(ifile1)
      stop 'NO SUCH FILE.'
    end if

! TOMORROW
    loop_day_p1 = loop_day + 1
    write(ifile2,'(a,i4.4,i2.2,i2.2,a)') &
    '${indir}/${dset}/',iyr,mon,loop_day_p1,'${suffix}'

    call get_nc_dim(trim(ifile1),'lon',nlon)
    call get_nc_dim(trim(ifile1),'lat',nlat)

    allocate(lon(nlon))
    allocate(lat(nlat))
    allocate(buf(nlon,nlat),buf1(nlon,nlat),buf2(nlon,nlat))

    call read_nc_dim(trim(ifile1),'lon',nlon,lon)
    call read_nc_dim(trim(ifile1),'lat',nlat,lat)

    dlat8 = (lat(nlat)-lat(1))/(nlat-1)
    dlon8 = (lon(nlon)-lon(1))/(nlon-1)
    dlon  = dlon8
    dlat  = dlat8

    slon = lon(1)
    slat = lat(1)
    elon = lon(1) + (nlon-1)*dlon
    elat = lat(1) + (nlat-1)*dlat

    print *,'nlon,nlat,lon(1),lon(2),lat(1),lat(2)='&
      ,nlon,nlat,lon(1),lon(2),lat(1),lat(2)

    print *,'lon(1),slon,lat(1),slat=',&
     lon(1),slon,lat(1),slat

    print *,'elon,lon(nlon),elat,lat(nlat)',&
      elon,lon(nlon),elat,lat(nlat)

    print *,'dlon,dlat=',dlon,dlat

    write(6,'(f8.4,1x,f8.4)')dlon,dlat



    hr1dy=24.0

    do ihr  = 0, 23, ${OUTPUT_INTERVAL}

!----------------------------------------------------------------------
!--- count time_index ---
!----------------------------------------------------------------------
    time_index = 0
    time_index = time_index + 1

!   print *,'time_index',time_index
!   print *,iyr,mon,loop_day,ihr
!   cycle 

!----------------------------------------------------------------------
!--- time stamp IN OUTPUT FILE---
!----------------------------------------------------------------------
    write(jdate,'(i4,a1,i2.2,a1,i2.2,a1,i2.2,a1,a2,a1,a2)') &
       iyr,'-',mon,'-',loop_day,'_',ihr,':','00',':','00'
    print *,'jdate=',jdate

!----------------------------------------------------------------------
!--- open output file (FILE) ---
!----------------------------------------------------------------------
    write(ofile,'(a,i4,a1,i2.2,a1,i2.2,a1,i2.2)')  &
       '${ODIR}/${dset}:',iyr,'-',mon,'-',loop_day,'_',ihr

    open(UNIT=io,FILE=ofile, FORM='unformatted')

    write(cfile,'(a,i4,a1,i2.2,a1,i2.2,a1,i2.2)')  &
       '${CDIR}/${DSET_CHECK}_',iyr,'-',mon,'-',loop_day,'_',ihr

    open(UNIT=21,FILE=cfile, FORM='unformatted',access='direct',&
      recl=nlon*nlat*4)

!----------------------------------------------------------------------
!--- SST ---
!----------------------------------------------------------------------
    VAR: do var_index   = 1, 1

      call netcdf_read_header_dimension(ifile1, &
           ndims, nlon, nlat, nlev, ntim)

      print *,'DONE netcdf_read_header_dimension ifile1'

      ndims = 3
      vname = 'analysed_sst'

      print *,'READ ifile1'
      call netcdf_read_data(ifile1, vname, &
           ndims, nlon, nlat, buf1, 1, time_index)

      print *,'DONE READ ifile1.'
      print *

      print *,'READ ifile2'
      call netcdf_read_data(ifile2, vname, &
           ndims, nlon, nlat, buf2, 1, time_index)
      print *,'DONE READ ifile2.'
      print *

      print *,'INTERPOLATE IN TIME'
      hr_now=float(ihr)
      wgt2=hr_now/hr1dy
      wgt1=1.0-wgt2

      buf(:,:)=buf1(:,:)*wgt1 + buf2(:,:)*wgt2

      print *,'DONE INTERPOLATE IN TIME'
      print *

      print *,'INTERP_SST2'

      call interp_sst2(buf,nlon,nlat)

!     where (buf < -9.0)
!       buf = -999.9
!     else where
!       buf = max(buf, -1.8)
!         buf = buf + 273.15
!     end where

      go to 33 !END OF SMOOTING

!--- smoothing
!      allocate(smooth(nlon,nlat))
!      smooth = buf
!      do j = 1, nlat
!      do i = 1, nlon
!      if (buf(i,j) <= -999.9) cycle
!
!      if (lon(i) <= 140.0) cycle
!      if (lat(j) <=  32.0) cycle
!
!        ave = 0.0
!        num = 0.0
!        do jj = j-20, j+20
!        do ii = i-20, i+20
!        iii = ii
!        jjj = jj
!        jjj = max(jjj,1)
!        jjj = min(jjj,nlat)
!        if (iii <    1) iii = iii + nlon
!        if (iii > nlon) iii = iii - nlon
!        if (buf(iii,jjj) <= -999.9) cycle
!          ave = ave + buf(iii,jjj)
!          num = num + 1
!        end do
!        end do
!        if (num > 1.0) then
!          smooth(i,j) = ave/num
!        end if
!      end do
!      end do
!!!!!     buf = smooth
!      deallocate(smooth)
!--- end of smoothing

33    continue


      print *,(buf(nlon/2,j),j=1,nlat,100)

      print *
      print *,'===================================='
      print *
      print *,'ifile1'
      print *,trim(ifile1)
      print *
      print *,'ifile2'
      print *,trim(ifile2)
      print *
      print *,'jdate=',trim(jdate)
      print *
      print '(A,f7.3,A,f7.3)','wgt1=',wgt1,' wgt2=',wgt2
      print *
      print *,'io=',io,' ofile'
      print *,trim(ofile)
      print *
      print *,'cfile '
      print *,trim(cfile)
      print *
      print *,'===================================='
      print *

      call output (io, jdate, nlon, nlat, buf, var_index, 1, &
                   slat, slon, dlat, dlon)

      irec=1
      write(21,rec=irec) ((buf(i,j),i=1,nlon),j=1,nlat)

    end do VAR

    cycle
!----------------------------------------------------------------------
!--- Seaice Flag ---
!----------------------------------------------------------------------
    do var_index   = 2, 2
      vname = 'sea_ice_fraction'
      call netcdf_read_header_dimension(ifile, &
                  ndims, nlon, nlat, nlev, ntim)
      call netcdf_read_data(ifile, vname, &
                  ndims, nlon, nlat, buf, 1, time_index)

      where (buf < -9.0)
        buf = 0.0
      else where
        buf = min(max(0.0,buf),1.0)
      end where

      call output (io, jdate, nlon, nlat, buf, var_index, 1, &
                   slat, slon, dlat, dlon)
    end do

!   cycle
!----------------------------------------------------------------------
!--- Landsea Flag ---
!mask:flag_masks = 1b, 2b, 4b, 8b, 16b ;
!flag = "water land optional_lake_surface sea_ice optional_river_surface" ;
!----------------------------------------------------------------------
    do var_index   = 3, 3
      vname   = 'mask'
      mfile = ifile
      call netcdf_read_header_dimension(mfile, &
                  ndims, nlon, nlat, nlev, ntim)
      call netcdf_read_data(mfile, vname, &
                  ndims, nlon, nlat, buf, 1, time_index)

      where (buf > 1.2)
        buf = 1.0  ! land
      else where
        buf = 0.0  ! ocean
      end where

      call output (io, jdate, nlon, nlat, buf, var_index, 1, &
                   slat, slon, dlat, dlon)
    end do

  end do
  deallocate(buf,buf1,buf2)
  deallocate(lon, lat)
! write(command,'(a,a)') 'gzip ',trim(ifile)
! call system(command)
  end do
  end do
  end do

  print *
  print '(A,A)','INPUT: ',trim(ifile)
  print '(A,A)','OUTPUT: ',trim(ofile)

  print '(A,i5)','NLON= ',nlon
  print '(A,i5)','NLAT ' ,nlat


  print *

  stop '*** NORMAL END *** '
  
contains

!----------------------------------------------------------------------
! output
!----------------------------------------------------------------------
  subroutine output (io, &
             jdate, nlon, nlat, buf, var_index, level_index, &
             startlat,startlon, deltalat, deltalon)

  implicit none

!--- intent ---
  integer, intent(in) :: io
  character(len=24), intent(in) :: jdate
  integer, intent(in) :: nlon, nlat
  integer, intent(in) :: var_index, level_index
  real(4), intent(inout) :: buf(nlon,nlat)
  real   , intent(in) :: startlat, deltalat
  real   , intent(in) :: startlon, deltalon

!--- for MM5 --- ! see REGRID/regridder/Doc/README.v3-format
  integer           :: iversion = 3
  real              :: xfcst    = 0.0
  integer           :: flag     
            ! =(0,1,3,4,5)=(Lat-Lon,Mercator,Lambert,Gaussian,Polar)

  character(len=9)  :: field(3)= (/'SST     ',&
                                   'SEAICE  ',&
                                   'MASKSEA '/)
!                                  'LANDSEA '/)

  character(len=25) :: units(3)= (/'K      ',&
                                   '[1/0]  ',&
                                   '[1/0]  '/)

  character(len=46) :: desc(3) = (/'Sea Surface Temperature',&
                                   'Sea/Ice flag           ',&
                                   'Land/Sea flag          '/)

  real :: pressure_slp=201300
  real :: pressure_sfc=200100
  real :: pressure_level(17)=(/100000, 92500, 85000, 70000, 60000, &
                                50000, 40000, 30000, 25000, 20000, &
                                15000, 10000,  7000,  5000,  3000, &
                                 2000,  1000/)

  if (var_index >= 1 .and. var_index <= 3) then
    flag = 0  ! Lat/Lon 
!   deltalat= 0.027778 ;  startlat= 23.986111
!   deltalon= 0.027778 ;  startlon=124.986111
    write(io) iversion
    write(io) jdate, xfcst,  &
              field(var_index), units(var_index), desc(var_index), &
              pressure_sfc, nlon, nlat, flag
    write(io) startlat, startlon, deltalat, deltalon
    write(io) buf
  else
    stop 'error : output'
  end if

  return
  end subroutine output

!----------------------------------------------------------------------
! check NetCDF error
!----------------------------------------------------------------------
  subroutine netcdf_check (status, text)

  implicit none

  include 'netcdf.inc'

!--- intent ---
  integer, intent(in) :: status
  character(len=*), intent(in) :: text

  if (status /= nf_noerr) then
    write(*,*) text
    write(*,*) nf_strerror(status)
    stop
  end if

  return
  end subroutine netcdf_check

!----------------------------------------------------------------------
! read netCDF header
!----------------------------------------------------------------------
  subroutine netcdf_read_header_dimension(file_name, &
             ndims, nlon, nlat, nlev, ntim)

  use netcdf_parameter
  implicit none

!--- intent ---
  integer, intent(out) :: ndims, nlon, nlat, nlev, ntim
  character(len=*), intent(in) :: file_name

!--- local ---
  integer :: status
  integer :: xid, yid, zid, tid
  character(len=300) :: text

!--- open file ---
  status = nf_open(file_name, nf_nowrite, ncid)
  call netcdf_check(status,'failed to open netcdf file')

!--- check global attribute ---
  text = ' '
  status = nf_get_att_text(ncid, nf_global, 'title', text)
  call netcdf_check(status,'cannot read global attribute')
! write(*,*) trim(text)

!--- check number of dimensions ---
  status = nf_inq_ndims(ncid, ndims)
  call netcdf_check(status,'cannot read number of dimensions')

!--- check name & size of dimension for longitude ---
  status = nf_inq_dimid (ncid, lon_name, xid)
  call netcdf_check(status,'cannot read dimension for longitude')
  status = nf_inq_dimlen(ncid,  xid, nlon)
  call netcdf_check(status,'cannot read dimension for longitude')

!--- check name & size of dimension for latitude ---
  status = nf_inq_dimid (ncid, lat_name, yid)
  call netcdf_check(status,'cannot read dimension for latitude')
  status = nf_inq_dimlen(ncid,  yid, nlat)
  call netcdf_check(status,'cannot read dimension for latitude')

!--- check name & size of dimension for level ---
  status = nf_inq_dimid (ncid, lev_name, zid)
  if (status == nf_noerr) then
    status = nf_inq_dimlen(ncid,  zid, nlev)
    call netcdf_check(status,'cannot read dimension for level')
  else
    nlev = 1  ! 2 dimensional data (e.g. SST)
  end if

!--- check name & size of dimension for time ---
  status = nf_inq_dimid (ncid, tim_name, tid)
  call netcdf_check(status,'cannot read dimension for time')
  status = nf_inq_dimlen(ncid,  tid, ntim)
  call netcdf_check(status,'cannot read dimension for time')

!--- close file ---
  status = nf_close(ncid)
  call netcdf_check(status,'failed to close netcdf file')

  return
  end subroutine netcdf_read_header_dimension

!----------------------------------------------------------------------
! read netCDF file
!----------------------------------------------------------------------
  subroutine netcdf_read_data(file_name, varname, ndims, nlon, nlat, &
                              buf, irec_level, irec_time)

  use netcdf_parameter
  implicit none

!--- intent ---
  integer, intent(in) :: ndims, nlon, nlat
  character(len=*), intent(in) :: file_name
  character(len=*), intent(in) :: varname
  real(4), intent(inout) :: buf(nlon,nlat)
  integer, intent(in) :: irec_time, irec_level

!--- local ---
  logical :: debug = .false.
! logical :: debug = .true.
  integer :: status
  integer :: varid
  integer, dimension(:), allocatable :: start
  integer, dimension(:), allocatable :: count
  integer(1), dimension(:,:), allocatable :: buf_i1
  integer(2), dimension(:,:), allocatable :: buf_i2
  integer(4), dimension(:,:), allocatable :: buf_i4
  real(4),    dimension(:,:), allocatable :: buf_r4
  real(8),    dimension(:,:), allocatable :: buf_r8

  integer :: idims
  integer :: ivars
  integer :: xtype
  real(4) :: scale, offset, missing_value

  integer :: i,j,k

  character(len=80) :: long_name = ' '
  character(len=80) :: unit = ' '
  character(len=38), dimension(6) :: what_type = &
  (/ 'NCBYTE   (8 bit data                )',  &
     'NCCHAR   (character                 )',  &
     'NCSHORT  (16 bit integer            )',  &
     'NCLONG   (32 bit integer            )',  &
     'NCFLOAT  (32 bit IEEE floating-point)',  &
     'NCDOUBLE (64 bit IEEE floating-point)'/)

!--- open file ---
  status = nf_open(file_name, nf_nowrite, ncid)
  call netcdf_check(status,'failed to open netcdf file')

!--- get ID ---
  status = nf_inq_varid(ncid, varname, varid)
  if (status /= 0) then
    print *,'',varname
  end if
  call netcdf_check(status,'cannot read id')

!--- check num. of dimensions ---
  status = nf_inq_varndims(ncid, varid, idims)
  call netcdf_check(status,'cannot read dimension')
  if (idims /= ndims) then
    write(*,*) 'num. of dimension for parameter = ',ndims
    write(*,*) 'num. of dimension for netCDF    = ',idims
    stop 'num. of variable dimension'
  end if

!--- get attribute ---
  long_name = ''
  status = nf_get_att_text(ncid, varid, 'long_name'    ,long_name)
  call netcdf_check(status,'cannot read attribute')
  unit = ''
  status = nf_get_att_text(ncid, varid, 'units'        ,unit)
  status = nf_inq_vartype (ncid, varid, xtype)
  call netcdf_check(status,'cannot read variable type')
  call netcdf_check(status,'cannot read attribute')
  status = nf_get_att_real(ncid, varid, 'scale_factor' ,scale)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    scale = 1.0
  end if
  status = nf_get_att_real(ncid, varid, 'add_offset'   ,offset)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    offset = 0.0
  end if
  status = nf_get_att_real(ncid, varid, 'missing_value',missing_value)
! call netcdf_check(status,'cannot read attribute')
  if (status /= nf_noerr) then
    missing_value = -9.99e33
  end if

  if (debug) then
    write(*,*) ' '
    write(*,*) ' ',irec_time, irec_level
    write(*,*) 'long name         =',trim(long_name)
    write(*,*) 'Variable name     =',trim(varname)
    write(*,*) 'Variable ID       =',varid
    write(*,*) 'units             =',trim(unit)
    write(*,*) 'num. of dimension =',idims
    write(*,*) 'size of dimension =',nlon, nlat
    write(*,*) 'type of variable  =',what_type(xtype)
    write(*,*) 'missing value     =',missing_value
    write(*,*) 'scale            =',scale
    write(*,*) 'offset           =',offset
    write(*,*) ' '
  end if

!--- set count & start ---
  allocate(start(ndims)) ; allocate(count(ndims))
  select case (ndims)
    case (3)
      count(1)=nlon  ; start(1)=1
      count(2)=nlat  ; start(2)=1
      count(3)=1     ; start(3)=irec_time
    case (4)
      count(1)=nlon  ; start(1)=1
      count(2)=nlat  ; start(2)=1
      count(3)=1     ; start(3)=irec_level
      count(4)=1     ; start(4)=irec_time
    case default
      stop 'count start'
  end select

!--- get data ---
  select case (xtype)
    case (nf_byte)
      allocate(buf_i1(nlon,nlat))
      status = nf_get_vara_int1  (ncid, varid, start, count, buf_i1)
      buf(:,:) = buf_i1(:,:)
      buf(:,:) = buf(:,:) * scale + offset
      deallocate(buf_i1)
    case (nf_short)
      allocate(buf_i2(nlon,nlat))
      status = nf_get_vara_int2(ncid, varid, start, count, buf_i2)
      buf = -999.9
      do j = 1, nlat
      do i = 1, nlon
        if (buf_i2(i,j) == -32768) cycle
        buf(i,j) = buf_i2(i,j)
        buf(i,j) = buf(i,j) * scale + offset
      end do
      end do
      deallocate(buf_i2)
    case (nf_int)
      allocate(buf_i4(nlon,nlat))
      status = nf_get_vara_int   (ncid, varid, start, count, buf_i4)
      buf(:,:) = buf_i4(:,:)
      buf(:,:) = buf(:,:) * scale + offset
      deallocate(buf_i4)
    case (nf_real)
      allocate(buf_r4(nlon,nlat))
      status = nf_get_vara_real  (ncid, varid, start, count, buf_r4)
      buf(:,:) = buf_r4(:,:)
      buf(:,:) = buf(:,:) * scale + offset
      deallocate(buf_r4)
    case (nf_double)
      allocate(buf_r8(nlon,nlat))
      status = nf_get_vara_double(ncid, varid, start, count, buf_r8)
      buf(:,:) = buf_r8(:,:)
      buf(:,:) = buf(:,:) * scale + offset
      deallocate(buf_r8)
    case default
      allocate(buf_r4(nlon,nlat))
      status = nf_get_vara_real  (ncid, varid, start, count, buf_r4)
      buf(:,:) = buf_r4(:,:)
      buf(:,:) = buf(:,:) * scale + offset
      deallocate(buf_r4)
  end select

  deallocate(start)
  deallocate(count)

!--- close file ---
  status = nf_close(ncid)
  call netcdf_check(status,'failed to close netcdf file')

  return
  end subroutine netcdf_read_data
end program netcdf_read


subroutine interp_sst2(sst,nlon,nlat)

! /work05/manda/WRF.PRE/myutil/pregrid.mgdsst
! 2018-06-25_17-55

  integer :: nlon, nlat
  real    :: sst(nlon,nlat)
  real    :: weight(8), xxx1, xxx2
  integer :: iw, ie, js, jn
  real, allocatable :: guess(:,:)

  allocate(guess(nlon,nlat))
  undef = -9.0
  print *,'',sst(1,1)

  do itr = 1, 100

    num = 0
    do j = 1, nlat
    do i = 1, nlon
      if (sst(i,j) < undef) then
        num = num + 1
      end if
    end do
    end do
    if (num == 0) return

    do j = 1, nlat
    do i = 1, nlon
      guess(i,j) = sst(i,j)
      if (sst(i,j) < undef) then
        iw = i-1 ; if (iw < 1) iw = nlon
        ie = i+1 ; if (ie > nlon) ie = 1
        js = j-1 ; if (js < 1   ) js = 1
        jn = j+1 ; if (jn > nlat) jn = nlat
        xxx1   = 0.0
        xxx2   = 0.0
        weight(:) = 1.0
        if (sst(iw,j ) < undef) weight(1) = 0.0
        if (sst(iw,jn) < undef) weight(2) = 0.0
        if (sst(i ,jn) < undef) weight(3) = 0.0
        if (sst(ie,jn) < undef) weight(4) = 0.0
        if (sst(ie,j ) < undef) weight(5) = 0.0
        if (sst(ie,js) < undef) weight(6) = 0.0
        if (sst(i ,js) < undef) weight(7) = 0.0
        if (sst(iw,js) < undef) weight(8) = 0.0
        xxx1 = sst(iw,j )*weight(1) &
             + sst(iw,jn)*weight(2) &
             + sst(i ,jn)*weight(3) &
             + sst(ie,jn)*weight(4) &
             + sst(ie,j )*weight(5) &
             + sst(ie,js)*weight(6) &
             + sst(i ,js)*weight(7) &
             + sst(iw,js)*weight(8) 
        xxx2  = sum(weight(1:8))
        if (xxx2 > 0.0) guess(i,j) = xxx1/xxx2
      end if

    end do
    end do

    do j = 1, nlat
    do i = 1, nlon
      sst(i,j) = guess(i,j)
    end do
    end do
  end do

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt1 = ',num

  do j = 1, nlat
    do k = 1, 2
      do i = 1, nlon
        if (sst(i,j) < undef) then
          iw = i-1 ; if (iw < 1   ) iw = nlon
          ie = i+1 ; if (ie > nlon) ie = 1
          if (sst(iw,j) > undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = 0.5*(sst(iw,j)+sst(ie,j))
          else if (sst(iw,j) > undef .and. &
            sst(ie,j) < undef) then
            sst(i,j) = sst(iw,j)
          else if (sst(iw,j) < undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = sst(ie,j)
          else
          end if
        end if
      end do
    end do
  end do

  do i = 1, nlon
    do j = nlat/2, 1, -1
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j+1)
      end if
    end do
    do j = nlat/2+1, nlat
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j-1)
      end if
    end do
  end do

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt2 = ',num

  deallocate(guess)
  return
end subroutine interp_sst2



subroutine interp_sst(sst,nlon,nlat)
  integer :: nlon, nlat
  real    :: sst(nlon,nlat)
  real    :: weight(8), xxx1, xxx2
  integer :: iw, ie, js, jn
  real, allocatable :: guess(:,:)

  allocate(guess(nlon,nlat))
  undef = -9.0

  do itr = 1, 10

    num = 0
    do j = 1, nlat
    do i = 1, nlon
      if (sst(i,j) < undef) then
        num = num + 1
      end if
    end do
    end do
    if (num == 0) return

    inc = 2
    do j = 1, nlat
    do i = 1, nlon
      guess(i,j) = sst(i,j)
      if (sst(i,j) < undef) then
        xxx1   = 0.0
        xxx2   = 0.0
        do jj = j-inc, j+inc
        do ii = i-inc, i+inc
          jc = jj
          ic = ii
          if (ic < 1) ic = ic + nlon
          if (ic > nlon) ic = ic - nlon
          if (jc < 1) jc = 1
          if (jc > nlat) jc = nlat

          if (sst(ic,jc) > undef) then
            xxx1 = xxx1 + 1
            xxx2 = xxx2 + sst(ic,jc)
          end if
        end do
        end do
        if (xxx1 > 0.0) guess(i,j) = xxx2/xxx1
      end if

    end do
    end do

    do j = 1, nlat
    do i = 1, nlon
      sst(i,j) = guess(i,j)
    end do
    end do
  end do
  deallocate(guess)

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt1 = ',num
  return

  do j = 1, nlat
    do k = 1, 2
      do i = 1, nlon
        if (sst(i,j) < undef) then
          iw = i-1 ; if (iw < 1   ) iw = nlon
          ie = i+1 ; if (ie > nlon) ie = 1
          if (sst(iw,j) > undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = 0.5*(sst(iw,j)+sst(ie,j))
          else if (sst(iw,j) > undef .and. &
            sst(ie,j) < undef) then
            sst(i,j) = sst(iw,j)
          else if (sst(iw,j) < undef .and. &
            sst(ie,j) > undef) then
            sst(i,j) = sst(ie,j)
          else
          end if
        end if
      end do
    end do
  end do

  do i = 1, nlon
    do j = nlat/2, 1, -1
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j+1)
      end if
    end do
    do j = nlat/2+1, nlat
      if (sst(i,j) < undef) then
        sst(i,j) = sst(i,j-1)
      end if
    end do
  end do

  num = 0
  do j = 1, nlat
  do i = 1, nlon
    if (sst(i,j) < undef) then
      num = num + 1
    end if
  end do
  end do
  print *,' missing pt2 = ',num

  return
  end subroutine interp_sst
EOF



date -R >$LOG
pwd    >>$LOG
echo   >>$LOG

SRC=pregrid.F90
EXE=pregrid.exe

$FC -o $EXE $COPT -L$NCPATH_LIB -I$NCPATH_INC $SRC \
$HOME/local/netcdf/*.o \
-lnetcdf -lnetcdff 2>&1 |tee -a $LOG

echo >>$LOG
ls -lh --time-style=long-iso $EXE >>$LOG
echo >>$LOG

./$EXE  2>&1 |tee -a $LOG

/bin/rm $EXE #pregrid.F90 pregrid.namelist


date -R   |tee -a $LOG
pwd       |tee -a $LOG
echo 2>&1 |tee -a $LOG
echo PREGRID TIME INTERPOLATION. 2>&1 |tee -a $LOG
echo 2>&1 |tee -a $LOG

ls -lh --time-style=long-iso $LOG

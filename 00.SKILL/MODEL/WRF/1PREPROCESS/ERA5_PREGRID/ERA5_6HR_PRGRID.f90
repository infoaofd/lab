
! ncid:ファイルのID番号、 varid: 変 数のID番号

integer ncid,varid
character infle*500,ofle*500
character var*50
real,allocatable :: lon(:),lat(:)
real(8),allocatable :: time(:)
integer(2),allocatable :: sdata(:,:,:)
!  real,allocatable :: sdata(:,:,:)
integer,allocatable :: dimids(:),nshape(:)
character*70 err_message
character*100 dimname !dimnameは各次元の名前。
integer s

integer           :: iversion = 3
real              :: xfcst    = 0.0
integer           :: flag     = 0  ! Lat/Lon 
! =(0,1,3,4,5)=(Lat-Lon,Mercator,Lambert,Gaussian,Polar)


  character(len=9)  :: field(3)= (/'SST     ',&
                                   'SEAICE  ',&
                                   'MASKSEA '/)
!                                  'LANDSEA '/)

  character(len=25) :: units(3)= (/'K      ',&
                                   '[1/0]  ',&
                                   '[1/0]  '/)

  character(len=46) :: desc(3) = (/'Sea Surface Temperature',&
                                   'Sea/Ice flag           ',&
                                   'Land/Sea flag          '/)

integer::im=281,jm=201,nm=4



allocate(lon(im),lat(jm),time(nm),sdata(im,jm,nm))

infle="/work05/manda/DATA/ERA5/6HR/NetCDF/ERA5_20180626_SST.nc"

sta=nf_open(infle, nf_nowrite, ncid)


var='sst'
sta = nf_inq_varid(ncid,var,varid)
print *,'varid=',varid

sta=nf_get_att_real(ncid,varid,'scale_factor',scale)
print *,'scale= ',scale

sta = nf_get_att_real(ncid,varid,'add_offset',offset)
print *,'offset=', offset

stat = nf_inq_varndims(ncid, varid, ndims)
print *,'ndims= ', ndims
print *

! varidからdimidsを得る。dimidsとはそれぞれの次元に対するID

allocate(dimids(ndims))
stat = nf_inq_vardimid(ncid, varid, dimids)
do i=1,ndims
  write(6,'("dimids(",i2,")=",i9 )') i,dimids(i)
enddo

print *

allocate(nshape(ndims))

do i=1,ndims

!   dimidsからnshapeを得る。
!   nshapeとはそれぞれの次元に対する最大データ数 (格子数)

    stat = nf_inq_dimlen(ncid, dimids(i), nshape(i))
    write(6,'("nshape(",i2,")=",i9 )') i,nshape(i)
enddo !i
print *

do i=1,ndims
  stat = nf_inq_dimname(ncid, dimids(i), dimname)
  write(6,'("dimname(",i2,")=",1x,a23)') i,dimname
enddo

print *
print *,'Read ',var(1:lnblnk(var))
stat = nf_get_var_int2(ncid, varid, sdata)
print *,'stat= ',stat

var='longitude'
print '(A)',var(1:lnblnk(var))

  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
sta = nf_inq_varid(ncid,var,varid)

print *,'varid=', varid
print *,'sta= ', sta
print *,'Read ',var(1:lnblnk(var))
stat = nf_get_var_real(ncid, varid, lon)
print *,'stat= ',stat

var='latitude'
print '(A)',var(1:lnblnk(var))
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。

sta = nf_inq_varid(ncid,var,varid)
print *,'varid=', varid
print *,'sta= ', sta
print *,'Read ',var(1:lnblnk(var))

stat = nf_get_var_real(ncid, varid, lat)
print *,'stat= ',stat
print *


TIME: do n=1,nshape(3)

write(io) iversion
write(io) jdate, xfcst,  &
          field(var_index), units(var_index), desc(var_index), &
          pressure_sfc, nlon, nlat, flag
write(io) startlat, startlon, deltalat, deltalon
write(io) sst

end do TIME
end

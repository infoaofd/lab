# WRFでSSTを差し替える

WRFでSSTを差し替えて計算したい

## namelist.inputの編集

```fortran
&time_control
io_form_auxinput4 = 2
auxinput4_inname = "wrflowinp_d<domain>",
auxinput4_interval = 360,360,
```

```FORTRAN
&physics
sst_update = 1,
```

`auxinput4_interval = 360,360,`：これで6時間毎にSSTが更新されていく設定になる。

real.exeを実行

出力されたwrfinput及びwrflowinpを確認する

wrf.exeを実行
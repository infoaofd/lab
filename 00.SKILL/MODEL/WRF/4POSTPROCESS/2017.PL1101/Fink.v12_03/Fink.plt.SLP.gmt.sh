#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.bio.mie-u.ac.jp
# Directory: /work1/am/WRF.Post/NCL/PL1101/Fink.01.Dp
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 14:08 on 11-30-2016.

. ./gmtpar.sh
echo "Bash script $0 starts."

gmtset PLOT_CLOCK_FORMAT hh
gmtset PLOT_DATE_FORMAT "o dd"
gmtset TIME_FORMAT_PRIMARY abbreviated 
gmtset ANOT_FONT_SIZE 12
gmtset INPUT_DATE_FORMAT yyyy-mm-dd
gmtset INPUT_CLOCK_FORMAT hh:mm:ss
black=0/0/0
dash="-"
xrange="2011-01-21T00:00:00/2011-01-25T00:00:00"
yrange1="988/998"
range1=$xrange/$yrange1

size="6T/1.5"
xanot1p=pa6Hf6h
xanot1s=sa1D

yanot1p=a2:"[hPa]":
yanot1s=f1

ylegend1=-3.5
ylegend2=-4
ylegend3=-5.
ylegend4=-1.5


in1=Fink.v11.ncl_PL1101_03.06_d01dt2_hr_post.txt

if [ ! -f $in1 ]; then
  echo Error in $0 : No such file, $in1
  exit 1
fi

out=${figdir}$(basename $0 .sh)_$(basename $in1 .txt).SLP.ps


color=${black}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $5}' |\
psxy -R$range1 -JX$size  -W5,${color} -B${xanot1p}/${yanot1p}WSne \
 -B${xanot1s}/${yanot1s} \
-X1.5 -Y7.5 -P -K > $out

color=${black}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $6}' |\
psxy -R$range1 -JX$size  -W5,${color},${dash} \
-O -K >> $out


xoffset=
yoffset=6.5
in="${in1}"
comment=
. ./note.sh

echo
echo Input : $in1
echo Output : $out
echo
echo "Done $0"

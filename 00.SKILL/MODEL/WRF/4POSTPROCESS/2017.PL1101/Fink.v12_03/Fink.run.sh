#!/bin/sh


ncl=$(basename $0 .run.sh).ncl

if [ ! -f $ncl ]; then
  echo *****
  echo ***** ERROR in $0 : No such file, $ncl
  echo *****
  exit 1
fi

# ncl-6.1.2
export NCARG_ROOT=/usr/local/ncl-6.1.2
export PATH=$NCARG_ROOT/bin:$PATH

exe=./runncl.sh

if [ ! -f $ncl ]; then
  echo Error in $0: No such file, ${ncl}.
  exit 1
fi


domain_list="d01"

indir="/work1/am/WRF/WRF.Result/"


indir2="/work2/am/WRF/WRF.Post/NCL/PL1101/Tracking.Strom.Center"
nheader=7
nline=89
ncolumn=5

runname_list=" \
PL1101_03.07 \
"
#PL1101_03.06_I06 \
#PL1101_03.06_NoSF \
#PL1101_03.06 \

#PL1101_03.05 \
#PL1101_03.04 \
#PL1101_03.03_I06_1HR \
#PL1101_03.03_1HR \

for runname in $runname_list; do
  for domain in $domain_list;do
    nc_file="wrfout_${domain}_2011-01-20_00:00:00.nc"
    infile2=${indir2}/Storm.Center_${runname}_${domain}.txt

    if [ ! -f ${indir}${runname}/WRF_${runname}/${nc_file} ]; then
      echo
      echo "ERROR in $0: No such file:"
      echo ${indir}${runname}/WRF_${runname}/${nc_file}
      echo
      exit 1
    fi

    if [ ! -f ${infile2} ]; then
      echo
      echo "ERROR in $0: No such file:"
      echo ${infile2}
      echo
      exit 1
    fi

    $exe $ncl "$runname" "$nc_file" "$indir" "$domain" \
    "$infile2" "$nheader" "$nline" "$ncolumn"
  done
done

exit 0






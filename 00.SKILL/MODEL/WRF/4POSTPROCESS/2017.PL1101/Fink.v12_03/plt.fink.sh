#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.bio.mie-u.ac.jp
# Directory: /work1/am/WRF.Post/NCL/PL1101/Fink.01.Dp
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 14:08 on 11-30-2016.

. ./gmtpar.sh
echo "Bash script $0 starts."

gmtset PLOT_CLOCK_FORMAT hh
gmtset PLOT_DATE_FORMAT "o dd"
gmtset TIME_FORMAT_PRIMARY abbreviated 
gmtset ANOT_FONT_SIZE 12
gmtset INPUT_DATE_FORMAT yyyy-mm-dd
gmtset INPUT_CLOCK_FORMAT hh:mm:ss

xrange="2011-01-21T00:00:00/2011-01-25T00:00:00"
yrange1="-10/10"
yrange2="-30/30"
yrange3="0/80"
yrange4="-6/6"
range1=$xrange/$yrange1
range2=$xrange/$yrange2
range3=$xrange/$yrange3
range4=$xrange/$yrange4


size="6T/1.5"
size3="6T/1"
anot1=pa6Hf6h
anot2=sa1DS

ylegend3=60
ylegend4=-4.8


#anot=${xanot}/${yanot}WSne

in1=Fink.v11.ncl_PL1101_03.04_d01dt2_hr_post.txt
if [ ! -f $in1 ]; then
  echo Error in $0 : No such file, $in1
  exit 1
fi
#if [ ! -f $in2 ]; then
#  echo Error in $0 : No such file, $in2
#  exit 1
#fi

out=${figdir}$(basename $in1 .txt).ps

echo Input : $in1
#echo Input : $in2
echo Output : $out

echo
echo PANEL 1
echo
echo RESpte gray
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $11}' |\
psxy -R$range1 -JX$size  -W5/${gray} -Bpa6Hf6h/a5f1g100:"[hPa/6hr]":Wsne -Bsa1D/f5 \
-X1.5 -Y7.5 -P -K > $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${gray} -R -JX -O -K >>$out
2011-01-23_16:00:00 -8
2011-01-23_19:00:00 -8
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_20:00:00 -8 14 0 0 ML RES@-PTE@-
EOF

echo DP pink
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $7}' |\
psxy -R$range1 -JX$size  \
-W5/${pink} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 -8
2011-01-21_07:00:00 -8
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 -8 14 0 0 ML Dp
EOF

echo Dphi gold
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $8}' |\
psxy -R$range1 -JX$size  -W5/${gold} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${gold} -R -JX -O -K >>$out
2011-01-21_19:00:00 -8
2011-01-21_22:00:00 -8
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 -8 14 0 0 ML Dphi
EOF


echo ITT midnightblue
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $9}' |\
psxy -R$range1 -JX$size  -W5/${midnightblue} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${midnightblue} -R -JX -O -K >>$out
2011-01-22_10:00:00 -8
2011-01-22_13:00:00 -8
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 -8 14 0 0 ML ITT
EOF


echo EP purple
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $10}' |\
psxy -R$range1 -JX$size  -W5/${purple} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${purple} -R -JX -O -K >>$out
2011-01-23_01:00:00 -8
2011-01-23_04:00:00 -8
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_05:00:00 -8 14 0 0 ML EP
EOF




echo
echo PANEL 2
echo
echo "ITTres gray (:=ITT-TADV-VMT-DIAB)"
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $18}' |\
psxy -R$range2 -JX$size  -Bpa6Hf6h/a10g100:"[hPa/6hr]":Wsne -Bsa1D/f5 -W5/${gray} \
-Y-1.8 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${gray} -R -JX -O -K >>$out
2011-01-23_04:00:00 -23
2011-01-23_07:00:00 -23
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_08:00:00 -23 14 0 0 ML RES@-ITE@-
EOF


echo TADV pink
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $12}'|\
psxy -R$range2 -JX$size \
-W5/${pink} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 -23
2011-01-21_07:00:00 -23
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 -23 14 0 0 ML TADV
EOF


echo VMT gold
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $13}' |\
psxy -R$range2 -JX$size  -W5/${gold} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${gold} -R -JX -O -K >>$out
2011-01-21_19:00:00 -23
2011-01-21_22:00:00 -23
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 -23 14 0 0 ML VMT
EOF


echo DIAB purple
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $14+$15+$16+$17}' |\
psxy -R$range2 -JX$size  -W5/${purple} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${purple} -R -JX -O -K >>$out
2011-01-22_10:00:00 -23
2011-01-22_13:00:00 -23
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 -23 14 0 0 ML DIAB
EOF




echo
echo PANEL 3
echo
echo "DIABptend"
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $19}' |\
psxy -R$range3 -JX$size3  -Bpa6Hf6h/a20g200:"[%]":Wsne -Bsa1D/f20 -Sb0.1b0 -G0 \
-Y-1.3 -O -K >> $out
#psxy -R$range3 -JX$size3  -Bpa6Hf6h/a20g200:"[%]":Wsne -Bsa1D/f20 -W5/${black} \
#-Y-1.3 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${ylegend3} 14 0 0 ML DIAB@-ptend@-
EOF




echo
echo PANEL 4
echo
echo "DIABmp red"
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $14}' |\
psxy -R$range4 -JX$size  -Bpa6Hf6h/a2g100:"[hPa/6hr]":WSne -Bsa1D/f2 -W5/${red} \
-Y-1.8 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${red} -R -JX -O -K >>$out
2011-01-23_04:00:00 $ylegend4
2011-01-23_07:00:00 $ylegend4
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_08:00:00 $ylegend4 14 0 0 ML DIAB@-MP@-
EOF

echo
echo DIABbl pink
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $15}'|\
psxy -R$range4 -JX$size \
-W5/${pink} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 $ylegend4
2011-01-21_07:00:00 $ylegend4
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 $ylegend4 14 0 0 ML DIAB@-BL@-
EOF


echo DIABra gold
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $16}' |\
psxy -R$range4 -JX$size  -W5/${gold} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${gold} -R -JX -O -K >>$out
2011-01-21_19:00:00 $ylegend4
2011-01-21_22:00:00 $ylegend4
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 $ylegend4 14 0 0 ML DIAB@-RA@-
EOF


echo DIABcu purple
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $17}' |\
psxy -R$range4 -JX$size  -W5/${purple} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${purple} -R -JX -O -K >>$out
2011-01-22_10:00:00 $ylegend4
2011-01-22_13:00:00 $ylegend4
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 $ylegend4 14 0 0 ML DIAB@-CU@-
EOF



xoffset=
yoffset=8.5
in="${in1}"
comment=
. ./note.sh

echo "Done $0"

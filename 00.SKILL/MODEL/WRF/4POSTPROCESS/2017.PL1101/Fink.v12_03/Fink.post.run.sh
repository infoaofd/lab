#!/bin/bash

src=$(basename $0 .run.sh).f90
f90="ifort"
opt="-CB -traceback -fpe0 "
exe=$(basename $0 .run.sh).exe
namelist="Fink.post.namelist.txt"

temp=$(basename $src .f90)
prog_name=$(echo $temp| sed -e 's/\./_/g')

infle=Fink.v11.ncl_PL1101_03.06_NoSF_d01dt2_hr.txt
ofle=$(basename $infle .txt)_post.txt
cat <<EOF>${namelist}
&para
infle="${infle}",
ofle="${ofle}",
&end
EOF

cat <<EOF>$src
program ${prog_name}
! Description:
!
! Author: am
!
! Host: aofd165.bio.mie-u.ac.jp
! Directory: /work1/am/WRF.Post/NCL/PL1101/Fink.v11
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 15:14 on 12-28-2016.

!  use
!  implicit none
character(len=500)::nmlfle
character(len=500)::infle,ofle
character(len=2000)::strm
character(len=2000),allocatable::comment(:)
character(len=19),allocatable::datetime(:)
integer,allocatable::tstp(:)
real,allocatable::long(:), lat(:), slp(:), slpave(:), dp(:), dphi(:),&
itt(:), ep(:), respte(:), tadv(:), vmt(:), diabmp(:), diabbl(:),&
diabra(:), diabcu(:), resitt(:)
real,allocatable::DIAB(:),DIABptend(:)

namelist /para/infle,ofle

nmlfle="${namelist}"
open(1,file=nmlfle,action="read")
read(1,nml=para)

open(11,file=infle,action="read")
n=0
nc=0
count_vaild_data: do
  read(11,'(A)',iostat=ios)strm
  if(ios<0)exit
  if(strm(1:1)=="#")then
    nc=nc+1
    cycle
  else if(strm(1:7)=="missing")then
    cycle
  else
    n=n+1
  endif
enddo count_vaild_data
nt=n
nct=nc
rewind(11)

allocate(datetime(nt))
allocate(tstp(nt),long(nt), lat(nt), slp(nt), slpave(nt), dp(nt), &
dphi(nt),itt(nt), ep(nt), respte(nt), tadv(nt), vmt(nt), diabmp(nt), &
diabbl(nt), diabra(nt), diabcu(nt), resitt(nt))
allocate(DIAB(nt))

allocate(comment(nc))


n=0
nc=0
skip_comment: do
  read(11,'(A)',iostat=ios)strm
  if(ios<0)exit
  if(strm(1:1)=="#")then
    nc=nc+1
    read(strm,'(A)')comment(nc)
    cycle
  else if(strm(1:7)=="missing")then
    cycle
  else
    n=n+1
    read(strm(1:19),'(A)')datetime(n)
    read(strm(20: ),*)tstp(n), long(n), lat(n), slp(n), slpave(n), &
    dp(n), dphi(n), itt(n), ep(n), respte(n), tadv(n), vmt(n), &
    diabmp(n), diabbl(n), diabra(n), diabcu(n), resitt(n)
  endif
enddo skip_comment
close(11)


! DIABptend
! Equation (3) of Fink et al. (GRL, 2012) 
allocate(DIABptend(nt))
do n=1,nt
  DIAB(n)=diabmp(n)+diabbl(n)+diabra(n)+diabcu(n)
  A=DIAB(n)
  B=tadv(n)
  C=vmt(n)
  if(A>0 .and. B>0 .and. C>0 )then
  ! print '(A)','A>0 & B>0 & C>0'
    DIABptend(n)=abs(A)/(abs(A)+abs(B)+abs(C))
  else if(A<0 .and. B<0 .and. C<0 )then
  ! print '(A)','A<0 & B<0 & C<0'
    DIABptend(n)=abs(A)/(abs(A)+abs(B)+abs(C))

  else if(A>0 .and. B>0 .and. C<0 )then
  ! print '(A)','A>0 & B>0 & C<0'
    DIABptend(n)=abs(A)/(abs(A)+abs(B))
  else if(A<0 .and. B<0 .and. C>0 )then
  ! print '(A)','A<0 & B<0 & C>0'
    DIABptend(n)=abs(A)/(abs(A)+abs(B))

  else if(A>0 .and. C>0 .and. B<0 )then
  ! print '(A)','A>0 & B<0 & C>0'
    DIABptend(n)=abs(A)/(abs(A)+abs(C))
  else if(A<0 .and. C<0 .and. B>0 )then
  ! print '(A)','A<0 & B>0 & C<0'
    DIABptend(n)=abs(A)/(abs(A)+abs(C))

  else
  ! print '(A)','Exeption'
    DIABptend(n)=0.0
  endif

  DIABptend(n)=DIABptend(n)*100.0
end do !n


do nc=1,nct
  print '(A)',trim(comment(nc))
end do !nc

open(21,file=ofle)
do nc=1,nct
  if(index(comment(nc),'Date')  /= 0)cycle
  if(index(comment(nc),'model') /= 0)cycle
  write(21,'(A)') trim(comment(nc))
end do !nc

write(21,'(A)')&
'#         Date/time tstp       long        lat      SLP  SLPaave&
&       DP     Dphi      ITT       EP   RESpte     TADV      VMT&
&   DIABmp   DIABbl   DIABra   DIABcu   RESitt  DIABptend'
write(*,'(A)') '#                    model'

do n=1,nt
  write(21,&
  '(A19,1x,i4,1x,2(f10.5,1x),2(f8.3,1x),12(f8.4,1x),f10.1)')&
  datetime(n),tstp(n),&
  long(n), lat(n), slp(n), slpave(n), dp(n), &
  dphi(n), itt(n), ep(n), respte(n), tadv(n), vmt(n), diabmp(n), &
  diabbl(n), diabra(n), diabcu(n), resitt(n), DIABptend(n)
end do !n
!close(21)

end program ${prog_name}
EOF

echo


echo Compiling ${src} ...
${f90} ${opt} ${src} -o ${exe}
if [ $? -ne 0 ]; then
echo
echo Compile error!
echo
echo Terminated.
echo
exit 1
fi
echo "Done Compile."


echo
echo ${exe} is running ...
echo
${exe}
if [ $? -ne 0 ]; then
echo
echo ERROR in $exe: Runtime error!
echo
echo Terminated.
echo
exit 1
fi
echo
echo "Output: ${ofle}"
echo
echo "Done ${exe}"
echo

#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.bio.mie-u.ac.jp
# Directory: /work1/am/WRF.Post/NCL/PL1101/Fink.01.Dp
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 14:08 on 11-30-2016.

. ./gmtpar.sh
echo "Bash script $0 starts."

gmtset PLOT_CLOCK_FORMAT hh
gmtset PLOT_DATE_FORMAT "o dd"
gmtset TIME_FORMAT_PRIMARY abbreviated 
gmtset ANOT_FONT_SIZE 12
gmtset INPUT_DATE_FORMAT yyyy-mm-dd
gmtset INPUT_CLOCK_FORMAT hh:mm:ss

xrange="2011-01-21T00:00:00/2011-01-25T00:00:00"
yrange1="-4/2"
yrange2="-15/10"
range1=$xrange/$yrange1
range2=$xrange/$yrange2

size="6T/2"
anot1=pa6Hf6h
anot2=sa1DS
yanot1=a2f2g100
yanot2=a5f5g100
#anot=${xanot}/${yanot}WSne

lgnd_y1=-3/5
lgnd_y2=-13

in1=Fink.09_Eq.1.and.2_v07.ncl_PL1101_03.03_1HR_d01dt2_hr_DIFF.txt
if [ ! -f $in1 ]; then
  echo Error in $0 : No such file, $in1
  exit 1
fi
#if [ ! -f $in2 ]; then
#  echo Error in $0 : No such file, $in2
#  exit 1
#fi

out=${figdir}$(basename $in1 .txt).ps

echo Input : $in1
#echo Input : $in2
echo Output : $out


echo RESpte gray
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $11}' |\
psxy -R$range1 -JX$size  -W8/${gray} -Bpa6Hf6h/${yanot1}:"[hPa/6hr]":WSne -Bsa1D/f5 \
-X1.5 -Y7 -P -K > $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${gray} -R -JX -O -K >>$out
2011-01-23_16:00:00 ${lgnd_y1}
2011-01-23_19:00:00 ${lgnd_y1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_20:00:00 ${lgnd_y1} 14 0 0 ML RES@-PTE@-
EOF


echo DP pink
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $7}' |\
psxy -R$range1 -JX$size  \
-W8/${pink} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 ${lgnd_y1}
2011-01-21_07:00:00 ${lgnd_y1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${lgnd_y1} 14 0 0 ML Dp
EOF

echo Dphi gold
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $8}' |\
psxy -R$range1 -JX$size  -W8/${gold} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${gold} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${lgnd_y1}
2011-01-21_22:00:00 ${lgnd_y1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${lgnd_y1} 14 0 0 ML Dphi
EOF


echo ITT midnightblue
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $9}' |\
psxy -R$range1 -JX$size  -W8/${midnightblue} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${midnightblue} -R -JX -O -K >>$out
2011-01-22_10:00:00 ${lgnd_y1}
2011-01-22_13:00:00 ${lgnd_y1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 ${lgnd_y1} 14 0 0 ML ITT
EOF


echo EP purple
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $10}' |\
psxy -R$range1 -JX$size  -W8/${purple} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${purple} -R -JX -O -K >>$out
2011-01-23_01:00:00 ${lgnd_y1}
2011-01-23_04:00:00 ${lgnd_y1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_05:00:00 ${lgnd_y1} 14 0 0 ML EP
EOF




echo "ITTres gray (:=ITT-TADV-VMT-DIABmp)"
#echo "DIABres gray (:=ITT-TADV-VMT)"
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $15}' |\
psxy -R$range2 -JX$size  -Bpa6Hf6h/${yanot2}:"[hPa/6hr]":WSne -Bsa1D/f5 -W8/${gray} \
-Y-3 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${gray} -R -JX -O -K >>$out
2011-01-23_04:00:00 ${lgnd_y2}
2011-01-23_07:00:00 ${lgnd_y2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_08:00:00 ${lgnd_y2} 14 0 0 ML RES@-ITE@-
EOF

echo
echo TADV pink
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $12}'|\
psxy -R$range2 -JX$size \
-W8/${pink} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 ${lgnd_y2}
2011-01-21_07:00:00 ${lgnd_y2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${lgnd_y2} 14 0 0 ML TADV
EOF


echo VMT gold
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $13}' |\
psxy -R$range2 -JX$size  -W8/${gold} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${gold} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${lgnd_y2}
2011-01-21_22:00:00 ${lgnd_y2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${lgnd_y2} 14 0 0 ML VMT
EOF


echo DIABmp purple
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $14}' |\
psxy -R$range2 -JX$size  -W8/${purple} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W8/${purple} -R -JX -O -K >>$out
2011-01-22_10:00:00 ${lgnd_y2}
2011-01-22_13:00:00 ${lgnd_y2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 ${lgnd_y2} 14 0 0 ML DIABmp
EOF





xoffset=
yoffset=5.5
in="${in1}"
comment=
. ./note.sh

echo "Done $0"

#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.bio.mie-u.ac.jp
# Directory: /work1/am/WRF.Post/NCL/PL1101/Fink.01.Dp
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 14:08 on 11-30-2016.


in1=Fink.ncl_PL1101_03.07_d01dt2_hr.txt
#in1=Fink.ncl_PL1101_03.06_I06_d01dt2_hr.txt



. ./gmtpar.sh
echo "Bash script $0 starts."

gmtset PLOT_CLOCK_FORMAT hh
gmtset PLOT_DATE_FORMAT "o dd"
gmtset TIME_FORMAT_PRIMARY abbreviated 
gmtset ANOT_FONT_SIZE 12
gmtset INPUT_DATE_FORMAT yyyy-mm-dd
gmtset INPUT_CLOCK_FORMAT hh:mm:ss

xrange="2011-01-21T00:00:00/2011-01-25T00:00:00"
yrange1="-10/10"
yrange2="-10/10"
yrange3="-30/30"
yrange4="-7/3"
range1=$xrange/$yrange1
range2=$xrange/$yrange2
range3=$xrange/$yrange3
range4=$xrange/$yrange4


size="6T/1.5"
size3="6T/1"
anot1=pa6Hf6h
anot2=sa1DS

yanot1p=a5g100
yanot1s=f1
yanot2p=a5g100
yanot2s=f1
yanot3p=a10g200
yanot3s=f10
yanot4p=a3g100
yanot4s=f1

ylegend1=-8
ylegend2=-8
ylegend3=-25
ylegend4=-5.5


if [ ! -f $in1 ]; then
  echo Error in $0 : No such file, $in1
  exit 1
fi

out=${figdir}$(basename $in1 .txt).Adi.vs.Dia.ps



echo
echo PANEL 1
echo
echo RESpte gray
color=${gray}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $11}' |\
psxy -R$range1 -JX$size  -W5/${color} -Bpa6Hf6h/${yanot1p}:"[hPa/6hr]":Wsne -Bsa1D/${yanot1s} \
-X1.5 -Y7.5 -P -K > $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-23_16:00:00 ${ylegend1}
2011-01-23_19:00:00 ${ylegend1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_20:00:00 ${ylegend1} 14 0 0 ML RES@-PTE@-
EOF

echo DP pink
color=${pink}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $7}' |\
psxy -R$range1 -JX$size  \
-W5/${color} \
-O -K >> $out

cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_04:00:00 ${ylegend1}
2011-01-21_07:00:00 ${ylegend1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${ylegend1} 14 0 0 ML Dp
EOF

echo Dphi gold
color=${gold}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $8}' |\
psxy -R$range1 -JX$size  -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${ylegend1}
2011-01-21_22:00:00 ${ylegend1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${ylegend1} 14 0 0 ML Dphi
EOF


echo ITT midnightblue
color=${midnightblue}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $9}' |\
psxy -R$range1 -JX$size  -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-22_10:00:00 ${ylegend1}
2011-01-22_13:00:00 ${ylegend1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 ${ylegend1} 14 0 0 ML ITT
EOF


echo EP purple
color=${purple}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $10}' |\
psxy -R$range1 -JX$size  -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-23_01:00:00 ${ylegend1}
2011-01-23_04:00:00 ${ylegend1}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_05:00:00 ${ylegend1} 14 0 0 ML EP
EOF




echo
echo PANEL 2
echo
echo DIABra blue
color=${blue}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $16}' |\
psxy -R$range2 -JX$size  -W5/${color} \
-Y-1.8 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-22_10:00:00 ${ylegend2}
2011-01-22_13:00:00 ${ylegend2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 ${ylegend2} 14 0 0 ML DIAB@-RA@-
EOF

echo "RESitt gray (:=ITT-TADV-VMT-DIAB)"
color=${gray}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $18}' |\
psxy -R$range2 -JX$size  -Bpa6Hf6h/${yanot2p}:"[hPa/6hr]":Wsne -Bsa1D/${yanot2s} \
-W5/${color} \
 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-23_01:00:00 ${ylegend2}
2011-01-23_04:00:00 ${ylegend2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-23_05:00:00 ${ylegend2} 14 0 0 ML RES@-ITT@-
EOF

echo "ADIAB (:=TADV+VMT) orange"
color=${orange}
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $12+$13}'|\
psxy -R$range2 -JX$size -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_04:00:00 ${ylegend2}
2011-01-21_07:00:00 ${ylegend2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${ylegend2} 14 0 0 ML ADIAB
EOF

echo DIABnr purple
color=${purple}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $14+$15+$17}' |\
psxy -R$range2 -JX$size  -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${ylegend2}
2011-01-21_22:00:00 ${ylegend2}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${ylegend2} 14 0 0 ML DIAB@-NR@-
EOF






echo
echo PANEL 3
echo
echo "TADV orange"
color=${orange}
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $12}'|\
psxy -R$range3 -JX$size -W5/${color} \
-Y-1.8 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R \
-Bpa6Hf6h/${yanot3p}:"[hPa/6hr]":Wsne -Bsa1D/${yanot3s} \
-JX${size} -O -K >>$out
2011-01-21_04:00:00 ${ylegend3}
2011-01-21_07:00:00 ${ylegend3}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${ylegend3} 14 0 0 ML TADV
EOF

echo VMT purple
color=${purple}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $13}' |\
psxy -R -JX  -W5/${color} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${ylegend3}
2011-01-21_22:00:00 ${ylegend3}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${ylegend3} 14 0 0 ML VMT
EOF




echo
echo PANEL 4
echo
echo "DIABmp green"
color="green"
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $14}' |\
psxy -R$range4 -JX$size  -Bpa6Hf6h/${yanot4p}:"[hPa/6hr]":WSne -Bsa1D/${yanot4s} \
-W5/${color} \
-Y-1.8 -O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-22_10:00:00 ${ylegend4}
2011-01-22_13:00:00 ${ylegend4}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-22_14:00:00 ${ylegend4} 14 0 0 ML DIAB@-MP@-
EOF

echo
echo DIABbl pink
sed -e 's/_/T/g' $in1 |\
awk '{ if ($1 != "missing" && $1 != "#") print $1, $15}'|\
psxy -R$range4 -JX$size \
-W5/${pink} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${pink} -R -JX -O -K >>$out
2011-01-21_04:00:00 ${ylegend4}
2011-01-21_07:00:00 ${ylegend4}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_08:00:00 ${ylegend4} 14 0 0 ML DIAB@-BL@-
EOF

echo DIABcu purple
color=${purple}
sed -e 's/_/T/g' $in1 |\
awk '{if ($1 != "missing" && $1 != "#") print $1, $17}' |\
psxy -R$range4 -JX$size  -W5/${purple} \
-O -K >> $out
cat <<EOF|sed -e 's/_/T/g'|psxy -W5/${color} -R -JX -O -K >>$out
2011-01-21_19:00:00 ${ylegend4}
2011-01-21_22:00:00 ${ylegend4}
EOF
cat <<EOF|sed -e 's/_/T/g'|pstext -R -JX -O -K >>$out
2011-01-21_23:00:00 ${ylegend4} 14 0 0 ML DIAB@-CU@-
EOF






xoffset=
yoffset=7
in="${in1}"
comment=
. ./note.sh

echo
echo Input : $in1
#echo Input : $in2
echo Output : $out
echo
echo "Done $0"

program Fink_v11_Difference

character(len=300)::infle1, infle2, ofle
character(len=1000)::strm
character(len=1000),allocatable::header1(:),header2(:)
character(len=19),allocatable::datetime1(:),datetime2(:)
integer,allocatable::tstp1(:),tstp2(:)
real,allocatable::x1(:,:),x2(:,:),dx(:,:)
real,parameter::rmis=-999.9

namelist /para/infle1, infle2, ofle

open(1,file="Fink.v11.Difference.namelist.txt",action="read")
read(1,nml=para)
close(1)

  open(11,file=infle1,action="read")
  open(12,file=infle2,action="read")

  nj=17
  n=0
  nh=0
  count_vaild_data: do
    read(11,'(A)',iostat=ios)strm
    if(ios<0)exit
    if(strm(1:1) == "#")then
      nh=nh+1
      cycle
    else if(strm(1:7)=="missing")then
      cycle
    else
      n=n+1
    endif
  enddo count_vaild_data
  nt=n
  rewind(11)

  allocate(x1(nt,nj),datetime1(nt),tstp1(nt),header1(nh))
  allocate(x2(nt,nj),datetime2(nt),tstp2(nt),header2(nh))
  allocate(dx(nt,nj))

  n=0
  nh1=0
  skip_comment1: do
    read(11,'(A)',iostat=ios)strm
    ! print *,trim(strm)
    if(ios<0)exit
    if(strm(1:1) == "#")then
      nh1=nh1+1
      header1(nh1)=trim(strm)
      cycle
    else if(strm(1:7)=="missing")then
      cycle
    else
      n=n+1
      read(strm(1:19),'(a19)')datetime1(n)
      read(strm(20: ),*)      tstp1(n),(x1(n,j),j=1,nj)
    endif
  enddo skip_comment1

  n=0
  nh2=0
  skip_comment2: do
    read(12,'(A)',iostat=ios)strm
    ! print *,trim(strm)
    if(ios<0)exit
    if(strm(1:1) == "#")then
      nh2=nh2+1
      header2(nh2)=trim(strm)
      cycle
    else if(strm(1:7)=="missing")then
      cycle
    else
      n=n+1
      read(strm(1:19),'(a19)')datetime2(n)
      read(strm(20: ),*)      tstp2(n),(x2(n,j),j=1,nj)
    endif
  enddo skip_comment2

  dx(:,:)=rmiss
  js=3
  je=nj
  print *
  do n=1,nt

    if(datetime1(n)/=datetime2(n).or.tstp1(n)/=tstp2(n))then
      print *
      print *,"ERROR in Fink_v11_Difference"
      print *,datetime1(n),'   ',datetime2(n),tstp1(n),tstp2(n)
      stop 1
    end if

    do j=js,je
      dx(n,j)=x2(n,j)-x1(n,j)
    end do

  end do

open(20,file=ofle)
write(20,'(A,A)')'# Input 1: ',trim(infle1)
write(20,'(A)')'#'
write(20,'(A,A)')'# Input 2: ',trim(infle2)
write(20,'(A)')'#'
do i=1,nh1
  write(20,'(A)')trim(header1(i))
end do
do i=1,3; write(20,'(A)')'#'; end do
do i=1,nh2
  write(20,'(A)')trim(header2(i))
end do

do n=1,nt
  write(20,&
  '(A19,1x,i4,1x,2(f10.5,1x),2(f8.3,1x),12(f8.4,1x),f10.1)')&
  datetime1(n),tstp1(n),x1(n,1),x1(n,2),(dx(n,j),j=js,je)
end do

print *

stop

end program Fink_v11_Difference

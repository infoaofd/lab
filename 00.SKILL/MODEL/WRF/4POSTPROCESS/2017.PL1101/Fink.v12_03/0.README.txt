所要時間

/work03/110/work2/am/WRF.POST/PL1101/NCL/Fink.v12_03

[2017年  7月 31日 月曜日 19:09:37 JST]
[~/WRF/WRF.Post/NCL/PL1101/Fink.v12_03]
[am@aofd165]
$ Fink.run.sh

[2017年  7月 31日 月曜日 20:19:38 JST]

鉛直層数を何層にするかに大きく依存



WRF Diabatic Heating Terms

DOCUMENTATION
The Thermodynamic Energy Equation and WRF
http://www.atmos.washington.edu/~scavallo/wrf_thermo.pdf

$ ncdump -h /work1/am/WRF.Result/PL1101_03.04/WRF_PL1101_03.04/wrfout_d01_2011-01-20_00\:00\:00.nc

        float MU(Time, south_north, west_east) ;
                MU:FieldType = 104 ;
                MU:MemoryOrder = "XY " ;
                MU:description = "perturbation dry air mass in column" ;
                MU:units = "Pa" ;
                MU:stagger = "" ;
                MU:coordinates = "XLONG XLAT" ;
        float MUB(Time, south_north, west_east) ;
                MUB:FieldType = 104 ;
                MUB:MemoryOrder = "XY " ;
                MUB:description = "base state dry air mass in column" ;
                MUB:units = "Pa" ;
                MUB:stagger = "" ;
                MUB:coordinates = "XLONG XLAT" ;
        float H_DIABATIC(Time, bottom_top, south_north, west_east) ;
                H_DIABATIC:FieldType = 104 ;
                H_DIABATIC:MemoryOrder = "XYZ" ;
                H_DIABATIC:description = "MICROPHYSICS LATENT HEATING" ;
                H_DIABATIC:units = "K s-1" ;
                H_DIABATIC:stagger = "" ;
                H_DIABATIC:coordinates = "XLONG XLAT" ;
        float RTHBLTEN(Time, bottom_top, south_north, west_east) ;
                RTHBLTEN:FieldType = 104 ;
                RTHBLTEN:MemoryOrder = "XYZ" ;
                RTHBLTEN:description = "COUPLED THETA TENDENCY DUE TO PBL PARAMETERIZATION" ;
                RTHBLTEN:units = "Pa K s-1" ;
                RTHBLTEN:stagger = "" ;
                RTHBLTEN:coordinates = "XLONG XLAT" ;
        float RTHRATEN(Time, bottom_top, south_north, west_east) ;
                RTHRATEN:FieldType = 104 ;
                RTHRATEN:MemoryOrder = "XYZ" ;
                RTHRATEN:description = "COUPLED THETA TENDENCY DUE TO RADIATION" ;
                RTHRATEN:units = "Pa K s-1" ;
                RTHRATEN:stagger = "" ;
                RTHRATEN:coordinates = "XLONG XLAT" ;
        float RTHCUTEN(Time, bottom_top, south_north, west_east) ;
                RTHCUTEN:FieldType = 104 ;
                RTHCUTEN:MemoryOrder = "XYZ" ;
                RTHCUTEN:description = "COUPLED THETA TENDENCY DUE TO CUMULUS SCHEME" ;
                RTHCUTEN:units = "Pa K s-1" ;
                RTHCUTEN:stagger = "" ;
                RTHCUTEN:coordinates = "XLONG XLAT" ;



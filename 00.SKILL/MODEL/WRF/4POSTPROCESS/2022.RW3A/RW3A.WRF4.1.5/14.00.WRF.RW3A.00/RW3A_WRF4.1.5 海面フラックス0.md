# WRF4.1.5 海面フラックス0



## 改変済みコード一覧

```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST
2021-11-02_08-27
$ ls -R |grep BAK
configure_BAK_210225-1552.wrf
U_CP_NO_WRFINOUT_BAK_210219-1913.sh*
module_sf_mynn_BAK_210305-2058.F*
module_surface_driver_BAK_210303-1314.F
U_CP_NO_WRFINOUT_BAK_210219-1913.sh*
```



```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-26
$ ls *BAK*
module_sf_mynn_BAK_210305-2058.F*  module_surface_driver_BAK_210303-1314.F
```





## コード変更箇所

```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-22
$ diff module_sf_mynn_BAK_210305-2058.F module_sf_mynn.F
```

```FORTRAN
<    ENDIF !NOLHF OCEAN
< !NOFFLX
1335a1327,1354
> 
> !NO SFC FLX
>    if(ISFFLX==23)then
>      DO I=its,ite
> 
>        CALL wrf_debug(-1,"SFCLAY1D_mynn SST>299: THF ARE SET TO 0.")
>        IF(XLAND(I)>=1.5 .and. TSK(I)>=299.)THEN
>                  !XLAND=1 FOR LAND, XLAND=2 FOR WATER 
>        QFX(i)  = 0.                                                              
>        HFX(i)  = 0.    
>        FLHC(I) = 0.                                                             
>        FLQC(I) = 0.                                                             
>        LH(I)   = 0.                                                             
>        CHS(I)  = 0.                                                             
>        CH(I)   = 0.                                                             
>        CHS2(i) = 0.                                                              
>        CQS2(i) = 0.                                                              
>        IF(PRESENT(ck)  .and. PRESENT(cd) .and. &
>          &PRESENT(cka) .and. PRESENT(cda)) THEN
>            Ck(I) = 0.
>            Cd(I) = 0.
>            Cka(I)= 0.
>            Cda(I)= 0.
>        ENDIF !PRESENT(ck)
>        END IF !XLAND
>      END DO !I
> 
>    endif !ISFFLX23 
```





```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-26
$ diff module_surface_driver_BAK_210303-1314.F module_surface_driver.F
```

```FORTRAN

4149c4149
<      CALL wrf_debug(-1,"SST>301K: QFX,LH,&ACLHF ARE SET TO 0.")
---
>      CALL wrf_debug(-1,"SST>299K: QFX,LH,&ACLHF ARE SET TO 0.")
4153c4153
<        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=301.)THEN
---
>        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=299.)THEN
4163a4164,4181
>    if(ISFFLX==23)then
>      CALL wrf_debug(-1,"SST>299K: QFX,LH,&ACLHF,HFX,ACHFX ARE SET TO 0.")
>      DO ij = 1 , num_tiles
>      DO J=j_start(ij),j_end(ij)
>      DO I=i_start(ij),i_end(ij)  
>        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=299.)THEN
>                  !XLAND=1 FOR LAND, XLAND=2 FOR WATER 
>          QFX(I,J)=0.0
>          LH(I,J)=0.0
>          ACLHF(I,J)=0.0 
>          HFX(I,J)=0.0 
>          ACHFX(I,J)=0.0 
>        END IF !XLAND
>      END DO !I
>      END DO !J
>      END DO !in
>    endif !ISFFLX23
>  
```


# WRF4.1.5_RW3A

```bash
zamanda@hfront01
/data07/thotspot/zamanda/WPS.RW3A/RW3A.00.00.00
2021-11-02_08-31
$ ls
Vtable.RW3A.00.00.00                         met_em.d01.2021-08-12_06:00:00.nc  met_em.d01.2021-08-14_00:00:00.nc
WPS.DATA.MV_RW3A.00.00.00_20211101-2039.LOG  met_em.d01.2021-08-12_12:00:00.nc  met_em.d01.2021-08-14_06:00:00.nc
met_em.d01.2021-08-11_00:00:00.nc            met_em.d01.2021-08-12_18:00:00.nc  met_em.d01.2021-08-14_12:00:00.nc
met_em.d01.2021-08-11_06:00:00.nc            met_em.d01.2021-08-13_00:00:00.nc  met_em.d01.2021-08-14_18:00:00.nc
met_em.d01.2021-08-11_12:00:00.nc            met_em.d01.2021-08-13_06:00:00.nc  met_em.d01.2021-08-15_00:00:00.nc
met_em.d01.2021-08-11_18:00:00.nc            met_em.d01.2021-08-13_12:00:00.nc  namelist.wps.RW3A.00.00.00
met_em.d01.2021-08-12_00:00:00.nc            met_em.d01.2021-08-13_18:00:00.nc
```



```
auxinput1_inname = "/data07/thotspot/zamanda/WPS.RW3A/RW3A.00.00.00/met_em.d<domain>.<date>"

history_outname = "/data07/thotspot/zamanda/WRF-4.1.5.RW3A/RW3A.00.00.00/RW3A.00.00.00_d<domain>_<date>", 
```

```
 &domains
 time_step                   = 10, !60,
 time_step_fract_num         = 0,
 time_step_fract_den         = 1,
 max_dom                     = 1,
 e_we              =  500
 e_sn              =  600
 e_vert            = 50,50
 p_top_requested          = 5000,
 num_metgrid_levels       = 38, !fnl(2015 or later), !27->fnl, 22->MSM, 18->GSM,
 num_metgrid_soil_levels  = 4,
 dx = 3000
 dy = 3000
 grid_id                        = 1,
 parent_id                      = 0, 
 i_parent_start    =   1,
 j_parent_start    =   1,
 parent_grid_ratio =   1,
 parent_time_step_ratio = 1,
 feedback                       = 0,
 smooth_option                  = 0
 /
```

```

 &physics
 cu_physics                          = 00, 0,
 mp_physics                          = 08, 08, 08
 ra_lw_physics                       = 1,     1,     1, !RRTM
 ra_sw_physics                       = 1,     1,     1, !Dudhia
 radt                                = 30,    30,    30,
 sf_sfclay_physics                   = 05, 05, 05,
 sf_surface_physics                  = 2,     2,     2,
 bl_pbl_physics                      = 05, 05, 05
 bldt                                = 0,     0,     0,
 cudt                                = 5,     5,     5,
 isfflx                              = 01,
 ifsnow                              = 0,
 icloud                              = 1,
 surface_input_source                = 1,
 num_soil_layers                     = 4,
 sf_urban_physics                    = 0,     0,     0,
 num_land_cat                        = 21,
 sst_update = 1,
 /
```

```
 &dynamics
 hybrid_opt                          = 2, 
 w_damping                           = 0,
 diff_opt                            = 1,
 km_opt                              = 4,
 diff_6th_opt                        = 0,      0,      0,
 diff_6th_factor                     = 0.12,   0.12,   0.12,
 base_temp                           = 290.
 damp_opt                            = 0,
 zdamp                               = 5000.,  5000.,  5000.,
 dampcoef                            = 0.2,    0.2,    0.2
 khdif                               = 0,      0,      0,
 kvdif                               = 0,      0,      0,
 non_hydrostatic                     = .true., .true., .true.,
 moist_adv_opt                       = 1,      1,      1,     
 scalar_adv_opt                      = 1,      1,      1,     
 gwd_opt                             = 1,
 /
```





```
d01 2021-08-11_00:00:00  input_wrf.F: SIZE MISMATCH:  namelist e_we                         =          500
d01 2021-08-11_00:00:00  input_wrf.F: SIZE MISMATCH:  input file WEST-EAST_GRID_DIMENSION   =          600
d01 2021-08-11_00:00:00 ---- ERROR: Mismatch between namelist and input file dimensions
d01 2021-08-11_00:00:00  input_wrf.F: SIZE MISMATCH:  namelist num_metgrid_levels           =           38
d01 2021-08-11_00:00:00  input_wrf.F: SIZE MISMATCH:  input file BOTTOM-TOP_GRID_DIMENSION  =           34
d01 2021-08-11_00:00:00 ---- ERROR: Mismatch between namelist and input file dimensions
```



## 改変済みコード一覧

```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST
2021-11-02_08-27
$ ls -R |grep BAK
configure_BAK_210225-1552.wrf
U_CP_NO_WRFINOUT_BAK_210219-1913.sh*
module_sf_mynn_BAK_210305-2058.F*
module_surface_driver_BAK_210303-1314.F
U_CP_NO_WRFINOUT_BAK_210219-1913.sh*
```



```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-26
$ ls *BAK*
module_sf_mynn_BAK_210305-2058.F*  module_surface_driver_BAK_210303-1314.F
```





## コード変更箇所

```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-22
$ diff module_sf_mynn_BAK_210305-2058.F module_sf_mynn.F
```

```FORTRAN
<    ENDIF !NOLHF OCEAN
< !NOFFLX
1335a1327,1354
> 
> !NO SFC FLX
>    if(ISFFLX==23)then
>      DO I=its,ite
> 
>        CALL wrf_debug(-1,"SFCLAY1D_mynn SST>299: THF ARE SET TO 0.")
>        IF(XLAND(I)>=1.5 .and. TSK(I)>=299.)THEN
>                  !XLAND=1 FOR LAND, XLAND=2 FOR WATER 
>        QFX(i)  = 0.                                                              
>        HFX(i)  = 0.    
>        FLHC(I) = 0.                                                             
>        FLQC(I) = 0.                                                             
>        LH(I)   = 0.                                                             
>        CHS(I)  = 0.                                                             
>        CH(I)   = 0.                                                             
>        CHS2(i) = 0.                                                              
>        CQS2(i) = 0.                                                              
>        IF(PRESENT(ck)  .and. PRESENT(cd) .and. &
>          &PRESENT(cka) .and. PRESENT(cda)) THEN
>            Ck(I) = 0.
>            Cd(I) = 0.
>            Cka(I)= 0.
>            Cda(I)= 0.
>        ENDIF !PRESENT(ck)
>        END IF !XLAND
>      END DO !I
> 
>    endif !ISFFLX23 
```





```bash
zamanda@hfront01
/data07/thotspot/zamanda/WRF4.1.5.RW3A/00.00.RW3A.TEST/phys
2021-11-02_08-26
$ diff module_surface_driver_BAK_210303-1314.F module_surface_driver.F
```

```FORTRAN

4149c4149
<      CALL wrf_debug(-1,"SST>301K: QFX,LH,&ACLHF ARE SET TO 0.")
---
>      CALL wrf_debug(-1,"SST>299K: QFX,LH,&ACLHF ARE SET TO 0.")
4153c4153
<        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=301.)THEN
---
>        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=299.)THEN
4163a4164,4181
>    if(ISFFLX==23)then
>      CALL wrf_debug(-1,"SST>299K: QFX,LH,&ACLHF,HFX,ACHFX ARE SET TO 0.")
>      DO ij = 1 , num_tiles
>      DO J=j_start(ij),j_end(ij)
>      DO I=i_start(ij),i_end(ij)  
>        IF(XLAND(I,J)>=1.5 .and. SST(I,J)>=299.)THEN
>                  !XLAND=1 FOR LAND, XLAND=2 FOR WATER 
>          QFX(I,J)=0.0
>          LH(I,J)=0.0
>          ACLHF(I,J)=0.0 
>          HFX(I,J)=0.0 
>          ACHFX(I,J)=0.0 
>        END IF !XLAND
>      END DO !I
>      END DO !J
>      END DO !in
>    endif !ISFFLX23
>  
```


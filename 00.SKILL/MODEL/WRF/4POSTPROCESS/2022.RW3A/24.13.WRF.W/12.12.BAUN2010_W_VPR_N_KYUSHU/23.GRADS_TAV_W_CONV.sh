#!/bin/bash
EXP1=0000; EXP2=0702
RUN1=${RUN1:-RW3A.00.03.05.05.${EXP1}.01}
RUN2=${RUN2:-RW3A.00.03.05.05.${EXP2}.01}
RAIN_TYP=CONVECTIVE 
#RAIN_TYP=STRATIFORM

CTL1=$(basename $0 .sh)_${EXP1}.CTL
cat <<EOF>$CTL1
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN1}/W_VPR/N-KYUSHU/0VPR_W_${RUN1}.d01.basic_p.01HR_%y4-%m2-%d2_%h2_%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
xdef 1 linear  120.56867   0.01351351
ydef 1 linear   18.56023   0.01351351
zdef 61 levels
0.02500 0.05000 0.07500 0.10000 0.12500 0.15000 0.17500 0.20000 0.22500 0.25000
0.27500 0.30000 0.32500 0.35000 0.37500 0.40000 0.45000 0.50000 0.5500  0.60000
0.65000 0.70000 0.75000 0.80000 0.85000 0.90000 0.95000 1.00000 1.05000 1.10000
1.15000 1.20000 1.25000 1.30000 1.35000 1.40000 1.45000 1.50000 2.00000 2.50000
3.00000 3.50000 4.00000 4.50000 5.00000 5.50000 6.00000 7.00000 8.00000 9.00000
10.00000 11.00000 12.00000 13.00000 14.00000 15.00000 16.00000 17.00000 18.00000 19.00000
20.00000
tdef  71 linear 01Z12AUG2021      60MN      
VARS   6
WVC   61 0 W CONVECTIVE AAV
VWVC  61 0 W CONVECTIVE VAR FROM AAV
WVC90 61 0 W CONVECTIVE 90%-TILE
WVS   61 0 W STRATIFORM AAV
VWVS  61 0 W STRATIFORM VAR FROM AAV
WVS90 61 0 W STRATIFORM 90%-TILE
ENDVARS
EOF

CTL2=$(basename $0 .sh)_${EXP2}.CTL
cat <<EOF>$CTL2
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN2}/W_VPR/N-KYUSHU/0VPR_W_${RUN2}.d01.basic_p.01HR_%y4-%m2-%d2_%h2_%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
xdef 1 linear  120.56867   0.01351351
ydef 1 linear   18.56023   0.01351351
zdef 61 levels
0.02500 0.05000 0.07500 0.10000 0.12500 0.15000 0.17500 0.20000 0.22500 0.25000
0.27500 0.30000 0.32500 0.35000 0.37500 0.40000 0.45000 0.50000 0.5500  0.60000
0.65000 0.70000 0.75000 0.80000 0.85000 0.90000 0.95000 1.00000 1.05000 1.10000
1.15000 1.20000 1.25000 1.30000 1.35000 1.40000 1.45000 1.50000 2.00000 2.50000
3.00000 3.50000 4.00000 4.50000 5.00000 5.50000 6.00000 7.00000 8.00000 9.00000
10.00000 11.00000 12.00000 13.00000 14.00000 15.00000 16.00000 17.00000 18.00000 19.00000
20.00000
tdef  71 linear 01Z12AUG2021      60MN      
VARS   6
WVC   61 0 W CONVECTIVE AAV
VWVC  61 0 W CONVECTIVE VAR FROM AAV
WVC90 61 0 W CONVECTIVE 90%-TILE
WVS   61 0 W STRATIFORM AAV
VWVS  61 0 W STRATIFORM VAR FROM AAV
WVS90 61 0 W STRATIFORM 90%-TILE
ENDVARS
EOF

<<COMMENT
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
TIME=${HH}Z${DD}${MMM}${YYYY}
COMMENT

GS=$(basename $0 .sh).GS
FIGDIR=FIG_$(basename $0 .sh); mkd $FIGDIR
FIG=${FIGDIR}/$(basename $0 .sh)_${EXP1}_${EXP2}.PDF

#LONW=128 ;LONE=132 ; LATS=30 ;LATN=34
# LEV=

FS=2
UNIT="W [m/s]"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'

'set t 1'
'set z 1 61'
'R1CT90=ave(WVC90.1,t=1,t=71)'
'R2CT90=ave(WVC90.2,t=1,t=71)'
'R1CTA=ave(WVC.1,t=1,t=71)'
'R2CTA=ave(WVC.2,t=1,t=71)'

'set vpage 0.0 8.5 0.0 11'

xmax = 1; ymax = 2; ytop=8.5
xwid = 1.5/xmax; ywid = 5/ymax
xmargin=0.5; ymargin=1

'cc'

nmap = 1; xmap = 1; ymap = 1
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
xs1=xs
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE

'set lev 0 17'
'set grid off'
#'set xlint 4';'set ylint 2'
#'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'
'set xlab on';'set ylab on'
'set vrange -2 2'
'set xlint 1'
'set gxout line'

'set ccolor 2';'set cthick 3';'set cmark 0'
'd R1CT90'
'set xlab off';'set ylab off'

'set ccolor 4';'set cthick 3';'set cmark 0'
'd R2CT90'

'trackplot 0 0 0 17 -c 1 -l 3 -t 1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yb-0.4
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.13 0.16'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${EXNAME} ${RAIN_TYP} 90%-TILE'

x=xl-0.55; y=(yb+yt)/2
'set strsiz 0.13 0.16'; 'set string 1 c 4 90'
'draw string 'x' 'y' Altitude [km]'



nmap = 1; xmap = 1; ymap = 2
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
xs1=xs
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE

'set lev 0 17'
'set grid off'
#'set xlint 4';'set ylint 2'
#'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'
'set xlab on';'set ylab on'
'set vrange -1 1'
'set xlint 0.5'
'set gxout line'

'set ccolor 2';'set cthick 3';'set cmark 0'
'd R1CTA'
'set xlab off';'set ylab off'

'set ccolor 4';'set cthick 3';'set cmark 0'
'd R2CTA'

'trackplot 0 0 0 17 -c 1 -l 3 -t 1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yb-0.4
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.13 0.16'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${EXNAME} ${RAIN_TYP} AVE'

x=xl-0.55; y=(yb+yt)/2
'set strsiz 0.13 0.16'; 'set string 1 c 4 90'
'draw string 'x' 'y' Altitude [km]'


# TEXT
x=5; y=ytop+0.5
'set strsiz 0.15 0.18'; 'set string 1 c 5 0'
#'draw string 'x' 'y' ${HH}UTC${DD}${MMM}${YYYY}'
#x=(xs1+xe1)/2; y=y+0.3
'draw string 'x' 'y' ${RUN1} ${RUN2} N-KYUSHU'


# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=ytop+0.6
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS $CTL1 $CTL2

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

#!/bin/bash
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/22.12.CONV.STRAT/12.94.BRAUN2010_AAV_N_KYUSHU
# 2024-05-09_20-09

EXP=$1; EXP=${EXP:-RW3A.00.03.05.05.0000.01}
# PERIOD
iy1=2021; im1=8; id1=12; ih1=0; in1=0
iy2=2021; im2=8; id2=14; ih2=23; in2=0

#iy1=2021; im1=8; id1=12; ih1=1; in1=0
#iy2=2021; im2=8; id2=12; ih2=1; in2=0

# FOR PRECIP
INDIR=/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${EXP}
RUN=${EXP}.d01.basic_p.01HR

# FOR XLONG,XLAT
EXP0=RW3A.00.03.05.05.0000.01
INDIR0=/work00/DATA/HD02/RW3A.ARWpost.DAT/traj/${EXP0}
RUN0=${EXP0}.d01.traj.01HR

ODIR=/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${EXP}/RTYPE/N-KYUSHU/
mkdir -vp $ODIR
OUTTSR=${ODIR}/0RAIN_AAV_MASK_${RUN}.dat
OUTTSUM=${ODIR}/0RAIN_TSUM_MASK_${RUN}.dat
rm -vf $OUTTSR $OUTTSUM

README=$ODIR/0.README_$(basename $0).sh
echo >> $README
echo $(pwd) >> $README
echo $0 >>$README

DT_FILE=3600.0
imax=599; jmax=599; kmax=30

slon=130.15;elon=131.2;slat=32.8;elat=33.3

src=$(basename $0 .sh).F90
SUB="$(basename $0 .sh)_DATE.F90 $(basename $0 .sh)_WRF.F90  $(basename $0 .sh)_RTYPE.F90 \
$(basename $0 .sh)_OUT.F90"
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "

# OpenMP
#OPT2=" -fopenmp "

cat<<EOF>$nml
&para
INDIR0="$INDIR0",
RUN0="$RUN0",
INDIR="$INDIR",
RUN="$RUN",
ODIR="$ODIR",
iy1=$iy1,
im1=$im1
id1=$id1
ih1=$ih1
in1=$in1
iy2=$iy2,
im2=$im2
id2=$id2
ih2=$ih2
in2=$in2
DT_FILE=$DT_FILE
slon=$slon
elon=$elon
slat=$slat
elat=$elat
imax=$imax, 
jmax=$jmax, 
kmax=$kmax,
OUTTSR="$OUTTSR",
OUTTSUM="$OUTTSUM",
&end
EOF

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo
ls -lh ${exe}
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
cp -a $0 $ODIR

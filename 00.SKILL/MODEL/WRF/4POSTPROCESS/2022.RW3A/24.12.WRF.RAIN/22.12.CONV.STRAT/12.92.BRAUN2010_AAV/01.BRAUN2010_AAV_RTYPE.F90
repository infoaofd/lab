SUBROUTINE CHK_P1H(imax,jmax,RAINRNC,RTYPE)
integer,intent(in)::imax,jmax
real,intent(in)::RAINRNC(imax,jmax)
real,intent(inout)::RTYPE(imax,jmax)

!RTYPE=1 : CONVECTIVE, RTYPE1=2: STATIFORM, RTYPE=0:OTHER

WHERE(RAINRNC > 20.0 .and. RAINRNC < 1.E5)
RTYPE=1.0
END WHERE

END



SUBROUTINE CHK_TEXTURE(imax,jmax,RAINRNC,UNDEF,RTYPE)
integer,intent(in)::imax,jmax
real,intent(in)::RAINRNC(imax,jmax)
real,intent(in)::UNDEF
real,intent(inout)::RTYPE(imax,jmax)
real ::RTYPE_TMP(imax,jmax)
!RTYPE=1 : CONVECTIVE, RTYPE1=2: STATIFORM, RTYPE=0:OTHER
! Next, a texture algorithm was used, whereby grid points having rainfall rates 
! twice as large as theaverage of their nearest 24 neighbors were classified as 
! convection.  If a grid point is designated as convective in this way, then its
! nearest neighbors (within one grid distance) are also designated as convective. 

RTYPE_TMP=0.0

DO j=5,jmax-5
DO i=5,imax-5

AVE=0.0; NE=0

DO jj=-2,2
jdx=j+jj
DO ii=-2,2
idx=i+ii
IF(RAINRNC(i,j)/=UNDEF)THEN
AVE=AVE+RAINRNC(idx,jdx)
NE=NE+1
END IF
END DO !ii
END DO !jj

AVE=AVE-RAINRNC(i,j)
AVE=AVE/FLOAT(NE-1)
AVE2=AVE*2.0

IF(RTYPE(i,j)==1.0 .or. (RAINRNC(i,j)>=AVE2 .and. RAINRNC(i,j)>0.1))THEN
RTYPE_TMP(i,j)=1.0
END IF
END DO !i
END DO !j

DO j=1,jmax
DO i=1,imax
! If a grid point is designated as convective in this way, then its
! nearest neighbors (within one grid distance) are also designated as convective. 
if(RTYPE_TMP(i,j)==1.0)THEN
RTYPE(i,j)=RTYPE_TMP(i,j)
RTYPE(i-1,j+1)=1.0;RTYPE(i,j+1)=1.0;RTYPE(i+1,j+1)=1.0
RTYPE(i  ,j  )=1.0;                ;RTYPE(i+1,j  )=1.0
RTYPE(i-1,j-1)=1.0;RTYPE(i,j-1)=1.0;RTYPE(i+1,j-1)=1.0
END IF
END DO !i
END DO!j

END



SUBROUTINE CHK_W(imax,jmax,kmax,w,UNDEF,RTYPE)
integer,intent(in)::imax,jmax,kmax
real,intent(in)::W(imax,jmax,kmax)
real,intent(in)::UNDEF
real,intent(inout)::RTYPE(imax,jmax)

DO K=1,kmax
DO J=1,jmax
DO I=1,imax
IF(RTYPE(i,j)==1.0 .or. (w(i,j,k)>3.0 .and. w(i,j,k)<UNDEF))THEN
RTYPE(i,j)=1.0
END IF
END DO !i
END DO !j
END DO !k
END



SUBROUTINE CHK_QCLOUD(imax,jmax,kmax,QCLOUD,UNDEF,RTYPE)
integer,intent(in)::imax,jmax,kmax
real,intent(in)::QCLOUD(imax,jmax,kmax)
real,intent(in)::UNDEF
real,intent(inout)::RTYPE(imax,jmax)
REAL,PARAMETER::QCTH=0.5/1000.0
DO J=1,jmax
DO I=1,imax
DO K=1,kmax
IF(RTYPE(i,j)==1.0 .or. (QCLOUD(i,j,k)>QCTH .and. QCLOUD(i,j,k)<UNDEF))THEN
!PRINT *,I,J,K,RTYPE(I,J),QCLOUD(I,J,K),QCTH
RTYPE(i,j)=1.0
END IF
END DO !k
END DO !i
END DO !j
END


SUBROUTINE CHK_STRAT(imax,jmax,RAINRNC,UNDEF,RTYPE)
integer,intent(in)::imax,jmax
real,intent(in)::RAINRNC(imax,jmax)
real,intent(in)::UNDEF
real,intent(inout)::RTYPE(imax,jmax)

!RTYPE=1 : CONVECTIVE, RTYPE1=2: STATIFORM, RTYPE=0:OTHER

WHERE(RTYPE /= 1.0 .and. RAINRNC > 0.1)
RTYPE=2.0
END WHERE

END



SUBROUTINE SEPARATE_RAIN_TYPE(imax,jmax,UNDEF,RTYPE,RAINRNC,RS,RC)
integer,intent(in)::imax,jmax
real,intent(in)::UNDEF
real,intent(in),dimension(imax,jmax)::RTYPE,RAINRNC
real,intent(inout)::RS(imax,jmax),RC(imax,jmax)

RS=-1E-20; RC=-1E-20

WHERE(RTYPE==1.0)
RC=RAINRNC
END WHERE

WHERE(RTYPE==2.0)
RS=RAINRNC
END WHERE

;PRINT *,MAXVAL(RC),MINVAL(RC),MAXVAL(RS),MINVAL(RS)

END


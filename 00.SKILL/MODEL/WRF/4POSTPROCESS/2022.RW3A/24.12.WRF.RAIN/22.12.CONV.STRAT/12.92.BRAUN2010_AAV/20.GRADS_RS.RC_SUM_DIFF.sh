#!/bin/bash

#YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-2021081201}
RUN1=$1; RUN1=${RUN1:-RW3A.00.03.05.05.0000.01}
RUN2=$2; RUN2=${RUN2:-RW3A.00.03.05.05.0702.01}

RUN=${RUN1}; CTL1=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL1
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/RTYPE/0RAIN_TSUM_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   1 linear 00Z12AUG2021      60MN      
VARS   2
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
ENDVARS
EOF

RUN=${RUN2}; CTL2=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL2
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/RTYPE/0RAIN_TSUM_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   1 linear 00Z12AUG2021      60MN      
VARS   2
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
ENDVARS
EOF

<<COMMENT
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
TIME=${HH}Z${DD}${MMM}${YYYY}
COMMENT

GS=$(basename $0 .sh).GS
FIGDIR=FIG_$(basename $0 .sh); mkd $FIGDIR
FIG=${FIGDIR}/$(basename $0 .sh)_${RUN1}-${RUN2}.PDF

#LONW=128 ;LONE=132 ; LATS=30 ;LATN=34
# LEV=

LEVS="-140 140 20"
KIND='-kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson'

# KIND='-kind white->deepskyblue->mediumblue->green->yellow->magenta->red'
FS=2
UNIT="mm"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'

'set vpage 0.0 8.5 0.0 11'

xmax = 2; ymax = 1; ytop=8.5
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

'cc'

nmap = 1; ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

say 'MMMMM STRATIFORM'
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
xs1=xs
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS} ${KIND} -gxout shaded' ;# SET COLOR BAR

#'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'; 'set lev ${LEV}'
'set t 1'
'set grid off'
'set xlint 4';'set ylint 2'
'set mpdset hires';'set rgb 50 127 73 45' ;# red : 127 Green : 73 Blue : 45
'set map 50 1 3'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'
'DS=RS.1-RS.2'
'd DS'
#'set xlab off';'set ylab off'
# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' STRATIFORM'
aav'=aave('DS',lon=129,lon=132,lat=31,lat=34)'
say 'MMMMM RS DIF AAV = 'aav

say 'MMMMM CONVECTIVE'
xmap=2; nmap=2
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
xe1=xe
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS} ${KIND} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'DC=RC.1-RC.2'
'd DC'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' CONVECTIVE'
'aav=aave(DC,lon=129,lon=132,lat=31,lat=34)'
say 'MMMMM RC DIF AAV = 'aav

# LEGEND COLOR BAR
x1=xs1; x2=xe1-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xs1+xe1)/2; y=yt+0.4
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${HH}UTC${DD}${MMM}${YYYY}'
#x=(xs1+xe1)/2; y=y+0.3
'draw string 'x' 'y' ${RUN1}-${RUN2}'


# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=y+0.3
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS $CTL1 $CTL2

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

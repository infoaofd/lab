dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/RTYPE/RW3A.00.03.05.05.0000.01.d01.basic_p.01HR_RTYPE_%y4-%m2-%d2_%h2_%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   73 linear 00Z12AUG2021      60MN      
VARS   3
xlon   1  0  LONGITUDE
xlat   1  0  LATITUDE
RTYPE  1  0  RAIN TYPE 1=CONVECTIVE, 2=STRATIFORM
ENDVARS

#!/bin/bash

#YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-2021081201}
RUN1=$1; RUN1=${RUN1:-RW3A.00.03.05.05.0000.01}
RUN2=$2; RUN2=${RUN2:-RW3A.00.03.05.05.0702.01}

RUN=${RUN1}; CTL1=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL1
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/RTYPE/0RAIN_AAV_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
xdef 1 linear  120.56867   0.01351351
ydef 1 linear   18.56023   0.01351351
zdef 1 linear 1000 1
tdef  73 linear 00Z12AUG2021      60MN      
VARS   4
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
RSAC   1  0  ACCUMULATED RAIN STRATIFORM
RCAC   1  0  ACCUMULATED RAIN CONVECTIVE
ENDVARS
EOF

RUN=${RUN2}; CTL2=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL2
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/RTYPE/0RAIN_AAV_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
xdef 1 linear  120.56867   0.01351351
ydef 1 linear   18.56023   0.01351351
zdef 1 linear 1000 1
tdef  73 linear 00Z12AUG2021      60MN      
VARS   4
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
RSAC   1  0  ACCUMULATED RAIN STRATIFORM
RCAC   1  0  ACCUMULATED RAIN CONVECTIVE
ENDVARS
EOF

<<COMMENT
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
TIME=${HH}Z${DD}${MMM}${YYYY}
COMMENT

GS=$(basename $0 .sh).GS
FIGDIR=FIG_$(basename $0 .sh); mkd $FIGDIR
FIG=${FIGDIR}/$(basename $0 .sh)_${RUN1}-${RUN2}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'
'q ctlinfo 1'
'set vpage 0.0 8.5 0.0 11'

xmax = 1; ymax = 2; ytop=9.5
xwid = 4.0/xmax; ywid = 7.0/ymax
xmargin=0.5; ymargin=1

'cc'

nmap = 1; ymap = 1; xmap = 1
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
xs1=xs
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE

'set t 1 71'
'set grid off'

'set vrange 0 140';'set ylint 20'

'set ccolor 2'; 'set cstyle 3'; 'set cmark 0';'set cthick 4'
'd RSAC.1'
say result
'set xlab off';'set ylab off'
'set ccolor 2'; 'set cstyle 1'; 'set cmark 0';'set cthick 3'
'd RCAC.1'
'set ccolor 4'; 'set cstyle 3'; 'set cmark 0';'set cthick 4'
'd RSAC.2'
'set ccolor 4'; 'set cstyle 1'; 'set cmark 0';'set cthick 3'
'd RCAC.2'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# TEXT
x=(xl+xr)/2; y=yt+0.4
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${HH}UTC${DD}${MMM}${YYYY}'
#x=(xs1+xe1)/2; y=y+0.3
'draw string 'x' 'y' ${RUN1} ${RUN2}'

nmap = 2; ymap = 2; xmap = 1
xs = 0.8 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
xs1=xs
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE

'set t 1 71'
'set grid off'
'set xlab on';'set ylab on'
'set vrange -5 20';'set ylint 5'
'set ccolor 1'; 'set cstyle 3'; 'set cmark 0';'set cthick 4'
'DS=RSAC.1-RSAC.2'
'd DS'
'set xlab off';'set ylab off'
'set ccolor 1'; 'set cstyle 1'; 'set cmark 0';'set cthick 3'
'DC=RCAC.1-RCAC.2'
'd DC'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=10 ;#y+0.3
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS $CTL1 $CTL2

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

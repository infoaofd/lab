#!/bin/bash

RUN=RW3A.00.03.05.05.0000.01
CTL1=$(basename $0 .sh).CTL
cat <<EOF>$CTL1
dset /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/RTYPE/${RUN}.d01.basic_p.01HR_RTYPE_%y4-%m2-%d2_%h2_%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   73 linear 00Z12AUG2021      60MN      
VARS   3
xlon   1  0  LONGITUDE
xlat   1  0  LATITUDE
RTYPE  1  0  RAIN TYPE 1=CONVECTIVE, 2=STRATIFORM
ENDVARS
EOF

INDIR2=/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUN}/
CTL2=$INDIR2/${RUN}.d01.basic_p.01HR.ctl
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
YYYYMMDDHH=2021081201
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}


#INDIR=
#INFLE=
#IN=$(pwd)/${INDIR}/${INFLE}
#if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${RUN}_${YYYY}-${MM}-${DD}_${HH}.PDF

LONW=128 ;LONE=132 ; LATS=30 ;LATN=34
# LEV=

LEVS="0 1 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->orange->deepskyblue' #midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
FS=1
UNIT="RAIN TYPE"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'

xmax = 1; ymax = 1

ytop=8

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'
'set grid off'
'set xlint 1';'set ylint 1'
'set mpdset hires';'set rgb 50 127 73 45' ;# red : 127 Green : 73 Blue : 45
'set map 50 1 3'
'd RTYPE'
'set xlab off';'set ylab off'

'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set time ${TIME}'
'set grid off'
'set gxout contour'
'set cthick 2';'set ccolor 1'
'set clevs 20 50'
'd RAINRNC'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl+0.5; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=y1-0.35
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.25
'set strsiz 0.15 0.18'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${HH}UTC${DD}${MMM}${YYYY}'
x=(xl+xr)/2; y=y+0.3
'draw string 'x' 'y' ${RUN}'


# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

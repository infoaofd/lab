PROGRAM MAKE_HISTO_DATA
! Sat, 04 May 2024 12:01:28 +0900
! /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

integer :: iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2
integer::isec=0
real    :: slon, elon, slat, elat
real :: DT_FILE  ![sec]
real   , parameter :: daysec = 86400.0 ! [sec]
real(8) :: jd_beg, jd_end, jd
real,allocatable,dimension(:,:)::rlon,rlat,mask
real,allocatable,dimension(:,:)::RAINRNC

integer::imax, jmax, kmax

CHARACTER(LEN=500)::INDIR0,RUN0,INDIR,RUN

namelist /PARA/INDIR0,RUN0,INDIR,RUN,iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2,&
DT_FILE,slon, elon, slat, elat,  imax, jmax, kmax

READ(5,NML=PARA)

allocate(rlon(imax,jmax),rlat(imax,jmax),mask(imax,jmax),&
RAINRNC(imax,jmax))

call date2jd(iy1,im1,id1,ih1,in1,isec,jd_beg)
call date2jd(iy2,im2,id2,ih2,in2,isec,jd_end)

print '(A,4f7.3)','MMMMM SET DOMAIN ',slon, elon, slat, elat
jd = jd_beg
call jd2date(iyr,mon,idy,ihr,imin,isec,jd)
CALL READ_WRF_LON_LAT(INDIR0,RUN0,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,rlon,rlat)
mask(:,:)=0.
DO J=1,jmax
DO I=1,imax
if(rlon(i,j)>=slon .and. rlon(i,j)<=elon .and. &
   rlat(i,j)>=slat .and. rlat(i,j)<=elat)then
   mask(i,j)=1.0
end if
! if(mask(i,j)==1.0)print '(2(3(f7.3,1x),2x))',slon,rlon(i,j),elon,slat,rlat(i,j),elat
END DO !I
END DO !J
PRINT *

print '(A)','MMMMM TIME LOOP'
jd = jd_beg

MAIN_TIME_LOOP: DO 

jd = jd + (dt_file/daysec)
call jd2date(iyr,mon,idy,ihr,imin,isec,jd)

CALL READ_WRF(INDIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,RAINRNC)

DO J=1,jmax
DO I=1,imax
if(mask(i,j)==1.0)print '(2(f7.3,1x),2x,F8.1)',rlon(i,j),rlat(i,j),rainrnc(I,J)
END DO !I
END DO !J

IF (jd >= jd_end) EXIT
END DO MAIN_TIME_LOOP

END PROGRAM MAKE_HISTO_DATA

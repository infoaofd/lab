ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS
YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2021081200}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}
HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${HH}Z${DD}${MMM}${YYYY}

FIG=$(basename $0 .sh)_${YYYYMMDDHH}.eps

#FIG=$(basename $0 .sh).eps

LONW=122; LONE=132
LATS=29;  LATN=35


UNIT="mm/3h"

KIND='white->lightcyan->deepskyblue->wheat->orange->red->crimson'
LEVS='0 80 5'

cat <<EOF>$GS
'open /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/RW3A.00.03.05.05.0000.01.d01.basic_p.01HR.ctl'
'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set time $TIME0 '

'q dims'
say result

'cc'
'set grid off'

'set mpdraw on'
'set mpdset hires'

say '### mm/3d'

'd RAINRNC'

say  '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd RAINRNC'




say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'
'set ylab off'


say '### HGT'
'set gxout contour'
'set cmax 1500'
'set cmin 100'
'set cint 10'
'set clskip 2'
'set clopts 1 2 0.12'
'set ccolor 1'
'set cthick 2'
'd HGT'



say '### TITLE'
'q dims'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' '$TIME0' RAIN3h HGT'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF
echo $TIME0
grads -bcp "$GS "
rm -vf $GS

ls -lh $FIG



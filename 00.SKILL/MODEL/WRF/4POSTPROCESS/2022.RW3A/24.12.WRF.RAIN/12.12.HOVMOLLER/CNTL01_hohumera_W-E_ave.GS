


'open /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/RW3A.00.03.05.05.0000.01.d01.basic_p.01HR.ctl'

'q ctlinfo 1'

'set time 00Z12AUG2021 00Z15AUG2021'
'set lat 31 34'
'set z 1'
'q dims'
say result

'cc'

'set vpage 0.0 8.5 0.0 11'

xs=2; xe=6
ys=3; ye=10
'set parea 'xs ' 'xe' 'ys' 'ye

'set xlab on'
'set ylab on'

'set ylint 3'

'set xlint 1'

say '### mm/h'
'set lon 130'

'd ave(RAINRNC,lon=129, lon=132)'
say  '### COLOR SHADE'
'color 0 20 2 -kind white->wheat->orange->red->purple -gxout shaded'
'd ave(RAINRNC,lon=129, lon=132)'


say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' mm/h'

'set xlab off'
'set ylab off'




say '### TITLE'
'q dims'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' CNTL01_E-W_ave_RAIN1h'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' CNTL01_hohumera_W-E_ave.GS'
yy = yy+0.2
'draw string ' xx ' ' yy ' ./CNTL01_hohumera_W-E_ave.sh '
yy = yy+0.2
'draw string ' xx ' ' yy ' Thu, 02 May 2024 15:41:53 +0900'
yy = yy+0.2
'draw string ' xx ' ' yy ' x86_64-conda-linux-gnu'
yy = yy+0.2
'draw string ' xx ' ' yy ' /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/12.12.HOVMOLLER'


'gxprint CNTL01_hohumera_W-E_ave.pdf'

'quit'

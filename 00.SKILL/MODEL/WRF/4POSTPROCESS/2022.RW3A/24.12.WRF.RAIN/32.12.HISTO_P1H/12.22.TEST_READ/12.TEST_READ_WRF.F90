SUBROUTINE READ_WRF_LON_LAT(INDIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,rlon,rlat)

character(len=*),intent(in):: INDIR,RUN
integer,intent(in):: iyr, mon, idy, ihr, imin
integer,intent(in):: imax, jmax, kmax
real,intent(inout),dimension(imax,jmax)::rlon,rlat
character(len=1000) :: ifile

logical :: yes
integer :: ier, irec
integer :: i, j, k

write(ifile,'(a,a,a,a,i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a)') &
trim(INDIR),'/',TRIM(RUN),'_', &
iyr,'-',mon,'-',idy,'_',ihr,'_',imin,'.dat'

print '(A,A)','MMMMM READ ',trim(ifile)

inquire(file=trim(ifile),exist=yes)
if (.not. yes) then
  print '(A,A)','EEEEE NO SUCH FILE : ',trim(ifile)
  stop
end if

open (10,file=trim(ifile),form='unformatted',action="read",&
      access='direct',recl=imax*jmax*4)

write(*,'(A)',advance='no') 'READ ' 
      
write(*,'(A)',advance='no') 'latitude '
irec=1
read (10,rec=irec) ((rlat(i,j),i=1,imax),j=1,jmax)

write(*,'(A)',advance='no') 'longitude '
irec=irec+1
read (10,rec=irec) ((rlon(i,j),i=1,imax),j=1,jmax)

write(*,*);print '(A)','MMMMM READ_WRF_LON_LAT END'; PRINT *

close(10)

return
end


SUBROUTINE READ_WRF(INDIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,RAINRNC)

character(len=*),intent(in):: INDIR,RUN
integer,intent(in):: iyr, mon, idy, ihr, imin
integer,intent(in):: imax, jmax, kmax
real,intent(inout),dimension(imax,jmax)::RAINRNC
character(len=1000) :: ifile

INTEGER::iskip=30+30+30 +4+ 30+30+30 +1+ 1+1+1

logical :: yes
integer :: ier, irec
integer :: i, j, k

write(ifile,'(a,a,a,a,i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a)') &
trim(INDIR),'/',TRIM(RUN),'_', &
iyr,'-',mon,'-',idy,'_',ihr,':',imin,'.dat'

print '(A,A)','MMMMM READ ',trim(ifile)

inquire(file=trim(ifile),exist=yes)
if (.not. yes) then
  print *,'EEEEE NO SUCH FILE : ',trim(ifile)
  stop
end if


open (10,file=trim(ifile),form='unformatted',action="read",&
      access='direct',recl=imax*jmax*4)

write(*,'(A)',advance='no') 'READ ' 
      
write(*,'(A)',advance='no') 'RAINRNC'
irec=iskip
read (10,rec=irec) ((RAINRNC(i,j),i=1,imax),j=1,jmax)

write(*,*);print '(A)','MMMMM READ_WRF END'; PRINT *

close(10)

return
end

!INTEGER::iskip=30+30+30 +4+ 30+30+30 +1+ 1+1+1
!U             30  0  x-wind component (m s-1)
!V             30  0  y-wind component (m s-1)
!W             30  0  z-wind component (m s-1)
!Q2             1  0  QV at 2 M (kg kg-1)
!T2             1  0  TEMP at 2 M (K)
!U10            1  0  U at 10 M (m s-1)
!V10            1  0  V at 10 M (m s-1)
!QVAPOR        30  0  Water vapor mixing ratio (kg kg-1)
!QCLOUD        30  0  Cloud water mixing ratio (kg kg-1)
!QRAIN         30  0  Rain water mixing ratio (kg kg-1)
!HGT            1  0  Terrain Height (m)
!RAINC          1  0  ACCUMULATED TOTAL CUMULUS PRECIPITATION (mm)
!RAINRC         1  0  RAIN RATE CONV (mm per output interval)
!RAINNC         1  0  ACCUMULATED TOTAL GRID SCALE PRECIPITATION (mm)
!RAINRNC        1  0  RAIN RATE NON-CONV (mm per output interval)

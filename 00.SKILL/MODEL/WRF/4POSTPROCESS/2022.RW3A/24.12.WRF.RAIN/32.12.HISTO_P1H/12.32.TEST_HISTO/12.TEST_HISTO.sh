#!/bin/bash
#
# Sat, 04 May 2024 12:01:28 +0900
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

# FOR XLONG,XLAT
INDIR0=/work00/DATA/HD02/RW3A.ARWpost.DAT/traj/RW3A.00.03.05.05.0000.01
RUN0=RW3A.00.03.05.05.0000.01.d01.traj.01HR
# FOR PRECIP
#INDIR=/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01
#RUN=RW3A.00.03.05.05.0000.01.d01.basic_p.01HR
INDIR=/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0702.01
RUN=RW3A.00.03.05.05.0702.01.d01.basic_p.01HR

iy1=2021; im1=8; id1=12; ih1=0 ; in1=0
iy2=2021; im2=8; id2=14; ih2=23; in2=0
DT_FILE=3600.0
slon=129.;elon=132.;slat=31.;elat=34.
imax=599
jmax=599
kmax=30
b0=0.0
bmin=0.1
bmax=150.
bint=10.

src=$(basename $0 .sh).F90
SUB="$(basename $0 .sh)_SUB.F90 $(basename $0 .sh)_WRF.F90"
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "

# OpenMP
#OPT2=" -fopenmp "

cat<<EOF>$nml
&para
INDIR0="$INDIR0",
RUN0="$RUN0",
INDIR="$INDIR",
RUN="$RUN",
iy1=$iy1,
im1=$im1
id1=$id1
ih1=$ih1
in1=$in1
iy2=$iy2,
im2=$im2
id2=$id2
ih2=$ih2
in2=$in2
DT_FILE=$DT_FILE
slon=$slon
elon=$elon
slat=$slat
elat=$elat
imax=$imax, 
jmax=$jmax, 
kmax=$kmax,
b0=$b0
bmin=$bmin,
bmax=$bmax,
bint=$bint
&end
EOF

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo
ls -lh ${exe}
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

# DATE_LOOP FORTRAN

[[_TOC_]]

Sat, 04 May 2024 12:00:40 +0900
/work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

### 実行例

```bash
$ 12.DATE_LOOP.sh 
```

```bash
MMMMM 12.DATE_LOOP.exe is running ...

 MMMMM START         2021           8          12           0
 MMMMM END           2021           8          15           0
 MMMMM         2021           8          12           1
 MMMMM         2021           8          12           2
 
 MMMMM         2021           8          14          23
 MMMMM         2021           8          15           0

MMMMM Done 12.DATE_LOOP.exe
```

### 12.DATE_LOOP.sh

```bash
#!/bin/bash
#
# Sat, 04 May 2024 12:01:28 +0900
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ
#
src=$(basename $0 .sh).F90; SUB=$(basename $0 .sh)_SUB.F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "


# OpenMP
#OPT2=" -fopenmp "

cat<<EOF>$nml
&para
&end
EOF

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo
ls -lh ${exe}
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
```

### 12.DATE_LOOP.F90

```FORTRAN
PROGRAM MAKE_HISTO_DATA
! Sat, 04 May 2024 12:01:28 +0900
! /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

integer :: id_beg(5) = (/2021, 8, 12, 0, 0/)
integer :: id_end(5) = (/2021, 8, 15, 0, 0/)
real    :: slon, elon, slat, elat
parameter (slon = 129., elon = 132., slat=31., elat = 34.)
real :: DT_FILE=3600.0  ![sec]
real   , parameter :: daysec = 86400.0 ! [sec]
real(8) :: jd_beg, jd_end, jd


iyr = id_beg(1); mon = id_beg(2); idy = id_beg(3); ihr = id_beg(4)
imin =id_beg(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_beg)
call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd_beg)

iyr = id_end(1); mon = id_end(2); idy = id_end(3); ihr = id_end(4)
imin =id_end(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_end)
call jd2date(iyr2,mon2,idy2,ihr2,imin2,isec2,jd_end)

print *,'MMMMM START ',iyr1,mon1,idy1,ihr1
print *,'MMMMM END   ',iyr2,mon2,idy2,ihr2

jd = jd_beg
MAIN_TIME_LOOP: DO 

jd = jd + (dt_file/daysec)
call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd)
print *,'MMMMM ',iyr1,mon1,idy1,ihr1

IF (jd >= jd_end) EXIT
END DO MAIN_TIME_LOOP

END PROGRAM MAKE_HISTO_DATA

```

### 12.DATE_LOOP_SUB.F90

```fortran
      subroutine date2jd(year,month,day,hh,mm,ss,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Julian day from Gregolian caldendar
!-----------------------------------------------------------------------

! ... intent(in)
      integer :: year, month, day, hh, mm, ss
! ... intent(out)
      real(8) :: julian_day
! ... parameter
      real(8), parameter :: jd0  = 1720996.5d0 ! BC4713/ 1/ 1 12:00:00
      real(8), parameter :: mjd0 = 2400000.5d0 !   1858/11/17 00:00:00
! ... local
      integer :: y, m
      if (month < 3) then
        y = year - 1
        m = month + 12
      else
        y = year
        m = month
      end if

      julian_day = 365*(y) + int(y)/4 - int(y)/100 + int(y)/400  &
     &           + int((m+1)*30.6001d0) &
     &           + day           &
     &           + hh/24.0d0     &
     &           + mm/1440.0d0   &
     &           + ss/86400.0d0  &
     &           + jd0

! ... convert julian day to modified julian day
      julian_day = julian_day - mjd0

      end subroutine date2jd

      subroutine jd2date(year,month,day,hour,min,sec,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Gregolian caldendar from Julian day
!-----------------------------------------------------------------------

! ... intent(in)
      real(8) :: julian_day
! ... intent(out)
      integer :: year, month, day, hour, min, sec
! ... parameter
      real(8), parameter :: mjd0 = 2400000.5d0 !  1858/11/17 00:00:00
! ... local
      integer :: jalpha, ia, ib, ic, id
      integer :: itime
      real(8) :: jday, d, xtime

! ... convert modified julian day to julian day
      jday = julian_day + mjd0

      jday = jday + 0.5d0
      if (jday >= 2299161) then
        jalpha = int( (jday - 1867216.25d0)/36524.25d0 )
        d = jday + 1 + jalpha - int(0.25d0*jalpha)
      else
        d = jday
      end if

      ia = int(d) + 1524
      ib = int(6680.0d0 + ((ia-2439870) - 122.1d0)/365.25d0)
      ic = 365*ib + int(0.25d0*ib)
      id = int((ia-ic)/30.6001d0)
      xtime = (d-int(d))*86400.d0
      itime = xtime
      if ( xtime-itime > 0.5d0 ) itime = itime + 1

      day   = ia - ic - int(30.6001*id)

      month = id - 1
      if (month > 12) month = month - 12

      year  = ib - 4715
      if (month > 2) year = year - 1
      if (year <= 0) year = year - 1

      hour  = itime/3600.
      min   = (itime - hour*3600)/60
      sec   = itime - hour*3600 - min*60

      end subroutine jd2date
```


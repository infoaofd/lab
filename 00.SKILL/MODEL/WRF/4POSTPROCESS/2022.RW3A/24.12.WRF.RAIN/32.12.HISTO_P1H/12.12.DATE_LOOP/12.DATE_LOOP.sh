#!/bin/bash
#
# Sat, 04 May 2024 12:01:28 +0900
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ
#
src=$(basename $0 .sh).F90; SUB=$(basename $0 .sh)_SUB.F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "


# OpenMP
#OPT2=" -fopenmp "

cat<<EOF>$nml
&para
&end
EOF

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo
ls -lh ${exe}
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf 
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

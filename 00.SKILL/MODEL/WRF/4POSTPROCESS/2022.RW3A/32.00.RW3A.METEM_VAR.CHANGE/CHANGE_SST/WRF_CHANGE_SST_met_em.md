# WRF SSTの書き変え (met_em)

C:\Users\foome\lab\00.SKILL\MODEL\WRF\2022.RW3A\32.00.RW3A.METEM_VAR.CHANGE\CHANGE_SST

[[_TOC_]]

WPS4.1

TIAMAT

## 書き変える前のデータ

```bash
$  ls /work04/$(whoami)/WPS.OUT_work04/RW3A.WPS/RW3A.00.03.05.05.0802
0.README.TXT
met_em.d01.2021-08-10_00:00:00.nc
met_em.d01.2021-08-10_06:00:00.nc
...
met_em.d01.2021-08-15_00:00:00.nc
namelist.wps.RW3A.00.03.05.05.0802
Vtable.RW3A.00.03.05.05.0802
```

## データのコピー

元: RW3A.00.03.05.05.08<font color="red">0</font>2

コピー:  RW3A.00.03.05.05.08<font color="red">9</font>2

```bash
$ 　cd /work04/$(whoami)/WPS.OUT_work04/RW3A.WPS/
$ 　cp -r RW3A.00.03.05.05.0802 RW3A.00.03.05.05.0892
```

```bash
$  ll -d  RW3A.00.03.05.05.08?2
drwxrwxr-x 2 $(whoami) 4.0K 2022-02-21 21:20 RW3A.00.03.05.05.0802/
drwxrwxr-x 3 $(whoami) 4.0K 2024-08-07 09:52 RW3A.00.03.05.05.0892/

$  cd RW3A.00.03.05.05.0892
$  cat 0.README_RW3A.00.03.05.05.0892.TXT 
Wed, 07 Aug 2024 10:04:52 +0900

CHANGE SST FROM 0802
```

## CNTLと0802のSSTの差

<img src="WRF_CHANGE_SST_SST_DIFF.png" alt="image-20240807123849117" style="zoom:33%;" />

/work09/am/00.WORK/2022.RW3A/24.02.WRF.SST



## SSTの変更 (met_em)

```bash
$  cd /work04/$(whoami)/2022.RW3A/RW3A.WPS-4.1
$  cd 32.00.RW3A.METEM_VAR.CHANGE/CHANGE_SST
```

/work04/$(whoami)/WPS.OUT_work04/RW3A.WPS/RW3A.00.03.05.05.0892/

### CHANGE_SST.sh

```bash
#!/bin/sh

CASE=$1; RUN1=$2; RUN2=$3
CASE=${CASE:-RW3A.00.03.05.05}; RUN1=0802; RUN2=0892

runname1=${runname1:-${CASE}.${RUN1}}
runname2=${runname2:-${CASE}.${RUN2}}
INDIR=/work04/$(whoami)/WPS.OUT_work04/RW3A.WPS/${runname1}
ODIR=/work04/$(whoami)/WPS.OUT_work04/RW3A.WPS/${runname2}
if [ -d $ODIR ];then 
echo MMMMM REMOVE OUTPUT DIR MMMMM; rm -rvf $ODIR; echo MMMMM DONE REMOE OUTPUT DIR MMMMM
fi
echo;echo
echo MMMM MAKE OUTPUT DIR MMMMM
cp -r $INDIR $ODIR

domain=d01

ncl=$(basename $0 .sh).ncl

INLIST=$(ls $ODIR/met_em*nc)
for IN in $INLIST; do
runncl.sh $ncl $CASE $RUN2 $IN
done
```

### CHANGE_SST.ncl

```fortran
; CHANGE SST

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin

wallClock1 = systemfunc("date") ; retrieve wall clock time

scriptname_in = getenv("NCL_ARG_1")
;print("script name ="+scriptname_in)
scriptname=systemfunc("basename " + scriptname_in + " .ncl")

CASE = getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
IN = getenv("NCL_ARG_4")
print("MMMMM INPUT: "+IN)
;print("")

FIGDIR="FIG_CHANGE_SST_"+CASE+"_"+RUN+"/"
system("mkdir -vp "+FIGDIR)

; The WRF ARW input file.  
; This needs to have a ".nc" appended, so just do it.
f0 = addfile(IN,"rw")

sst0in=  f0->SST
lon0in = f0->XLONG_M
lat0in = f0->XLAT_M
msk0in = f0->LANDMASK

THOLD=273.15+28.0
print("MMMMM CHANGE SST : SST_THRESHOLD = "+tostring(THOLD))
sst0in = where(sst0in.le.THOLD, THOLD, sst0in) 
;print("MMMMM")

;print("MMMMM WRITE NEW VARIABLES")
;print("MMMMM SST in FILE f0 is sst0in.")
f0->SST = sst0in

; We generate plots, but what kind do we prefer?
typ = "PDF" ;ps x11  pdf  ngcm

; Set some Basic Plot options
res = True

dsizes  = new( (/3/),integer)
dsizes  = dimsizes(sst0in)
;print(dsizes)
im=dsizes(1)
jm=dsizes(2)
lon=new( (/im,jm/), typeof(lon0in))
lat=new( (/im,jm/), typeof(lat0in))
sst0=new( (/im,jm/), typeof(sst0in))
msk0=new( (/im,jm/), typeof(msk0in))

sst0@_FillValue = default_fillvalue(typeof(sst0in))

figfile= FIGDIR + "SST_"+CASE+"_"+RUN+"_" + systemfunc("basename "+IN+" .nc" )
print("MMMMM fig file = " + figfile +"."+typ)

wks = gsn_open_wks(typ, figfile)


gsn_define_colormap(wks,"matlab_jet")

; print("MMMMM 3D -> 2D FOR PLOTTING")
lon(:,:)=lon0in(0,:,:)
lat(:,:)=lat0in(0,:,:)
msk0(:,:)=msk0in(0,:,:)
sst0(:,:)=sst0in(0,:,:)-273.15


; Plotting options
opts = res

;opts@gsnCenterString = time
opts@gsnAddCyclic = False
opts@sfXArray = lon
opts@sfYArray = lat
opts@mpMinLonF            = min(lon)               ; select a subregion
opts@mpMaxLonF            = max(lon)
opts@mpMinLatF            = min(lat)
opts@mpMaxLatF            = max(lat)

opts@mpDataBaseVersion    = "HighRes" ; fill land in gray and ocean in transparent

opts@mpFillOn = True
opts@mpFillColors = (/"background","transparent","gray","transparent"/)
opts@mpFillDrawOrder = "PostDraw"

opts@cnFillOn = True
opts@cnLinesOn            = False            ; turn off contour lines
opts@pmLabelBarOrthogonalPosF = 0.0

opts@lbOrientation            = "Horizontal" ;"Vertical"     ; orientation of label bar
opts@pmLabelBarOrthogonalPosF = 0.1          ; move label bar closer
opts@lbLabelFontHeightF = 0.015
opts@lbLabelStride            = 1
opts@lbTitleString = "deg.C"
opts@lbTitlePosition = "bottom"
opts@lbTitleFontHeightF = 0.015
opts@pmLabelBarHeightF = 0.08

opts@gsnDraw          = False                ; turn off draw and frame
opts@gsnFrame         = False                ; b/c this is an overlay plot

opts@cnLevelSelectionMode = "ManualLevels"   ; set manual contour levels
opts@cnMinLevelValF       =   12.              ; set min contour level
opts@cnMaxLevelValF       =   32.             ; set max contour level
opts@cnLevelSpacingF      =   1.              ; set contour spacing

opts@tiMainString    = "" ;runname0 ; add titles
sst_plt=sst0
;sst_plt = where(msk0.eq.1, \
;                sst0@_FillValue, sst0)
plot = gsn_csm_contour_map(wks,sst_plt,opts)

;delete(opts)

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")

gsn_text_ndc(wks,IN,  0.05,0.875,txres)
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)


; MAKE PLOTS
draw(plot)
frame(wks)
end
```



## 参考資料

### アップロード

```bash
$  cd /work04/$(whoami)/WPS.OUT_work04/RW3A.WPS
$  PUT.sh RW3A.00.03.05.05.0892
DEST : za$(whoami)@xxxx.:/data07/thotspot/za$(whoami)/
```

### データ移動

```bash
$  myssh
$  pud7
$  mv RW3A.00.03.05.05.0892 RW3A.WPS
$  ls RW3A.WPS/RW3A.00.03.05.05.0892/
```

### REAL.exe



```bash
$  cd RW3A.WRF4.1.5/14.00.WRF.RW3A.00/RW3A.00.04
```

```bash
$ U_CP_NO_WRFINOUT.sh RW3A.00.04.05.05.0802.01 RW3A.00.04.05.05.0892.01
```

```bash
$ mkd /data07/thotspot/za$(whoami)/RW3A.OUT.WRF-4.1.5/RW3A.00.04.05.05.0892.01
mkdir: created directory ‘/data07/thotspot/za$(whoami)/RW3A.OUT.WRF-4.1.5/RW3A.00.04.05.05.0892.01’
```

```bash
/data07/thotspot/za$(whoami)/RW3A.WRF4.1.5/14.00.WRF.RW3A.00/RW3A.00.04
$ grep 0892 -n RW3A.00.04.05.05.0892.02/namelist.input
33: auxinput1_inname = "/data07/thotspot/za$(whoami)/RW3A.WPS/RW3A.00.03.05.05.0892/met_em.d<domain>.<date>"
35: history_outname = "/data07/thotspot/za$(whoami)/RW3A.OUT.WRF-4.1.5/RW3A.00.04.05.05.0892.02/RW3A.00.03.04.05.0892.02_d<domain>_<date>",
```

```bash
/data07/thotspot/za$(whoami)/RW3A.WRF4.1.5/14.00.WRF.RW3A.00/RW3A.00.04
$  U_PARA_REAL.sh
```



## 備考

### NCLの地形データ

/home/$(whoami)/.conda/envs/am/lib/ncarg/database/rangs
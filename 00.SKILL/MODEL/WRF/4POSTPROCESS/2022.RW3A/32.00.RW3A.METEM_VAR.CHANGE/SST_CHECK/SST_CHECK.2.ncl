;
; Plot SST data in met_em file 
; /work06/manda/WRF3.7.1/METEM.CHECK

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/wrf/WRFUserARW.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" 
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
begin

wallClock1 = systemfunc("date") ; retrieve wall clock time

scriptname_in = getenv("NCL_ARG_1")
print("script name ="+scriptname_in)
scriptname=systemfunc("basename " + scriptname_in + " .ncl")

runname0 = getenv("NCL_ARG_2")
print("runname0 ="+runname0)

indir0 = getenv("NCL_ARG_3")
print("indir0 ="+indir0)

domain = getenv("NCL_ARG_4")
print("domain ="+domain)

time = getenv("NCL_ARG_5")
print("time ="+time)

CASE = getenv("NCL_ARG_6")
print("CASE ="+CASE)

RUN = getenv("NCL_ARG_7")
print("RUN ="+RUN)


infile0=indir0+"/met_em."+domain+"."+time+".nc"

print("Input file 0: "+infile0)
print("")

outdir= CASE +"."+ RUN + "/"

print("Creating directory, " + outdir)
command="mkdir -p " + outdir
system(command)
print("Done.")
print("")

;
; The WRF ARW input file.  
; This needs to have a ".nc" appended, so just do it.
f0 = addfile(infile0,"r")

; We generate plots, but what kind do we prefer?
type = "PDF" ;ps x11  pdf  ngcm

; Set some Basic Plot options
res = True


sst0in=  f0->SST
lon0in = f0->XLONG_M
lat0in = f0->XLAT_M
msk0in = f0->LANDMASK


dsizes  = new( (/3/),integer)
dsizes  = dimsizes(sst0in)
print(dsizes)
im=dsizes(1)
jm=dsizes(2)
lon=new( (/im,jm/), typeof(lon0in))
lat=new( (/im,jm/), typeof(lat0in))
sst0=new( (/im,jm/), typeof(sst0in))
msk0=new( (/im,jm/), typeof(msk0in))

printVarSummary(sst0in)

sst0@_FillValue = default_fillvalue(typeof(sst0in))



figfile= outdir + "SST_" + runname0+"."+domain+"."+time
print("fig file = " + figfile )

wks = gsn_open_wks(type, figfile)


gsn_define_colormap(wks,"matlab_jet")

; SST
lon(:,:)=lon0in(0,:,:)
lat(:,:)=lat0in(0,:,:)
sst0(:,:)=sst0in(0,:,:)-273.15
msk0(:,:)=msk0in(0,:,:)


; Plotting options
opts = res

opts@gsnCenterString = time
opts@gsnAddCyclic = False
opts@sfXArray = lon
opts@sfYArray = lat
opts@mpMinLonF            = min(lon)               ; select a subregion
opts@mpMaxLonF            = max(lon)
opts@mpMinLatF            = min(lat)
opts@mpMaxLatF            = max(lat)

opts@mpDataBaseVersion    = "HighRes" ; fill land in gray and ocean in transparent

opts@mpFillOn = True
opts@mpFillColors = (/"background","transparent","gray","transparent"/)
opts@mpFillDrawOrder = "PostDraw"

opts@cnFillOn = True
opts@cnLinesOn            = False            ; turn off contour lines
opts@pmLabelBarOrthogonalPosF = 0.0

opts@lbOrientation            = "Horizontal" ;"Vertical"     ; orientation of label bar
opts@pmLabelBarOrthogonalPosF = 0.1          ; move label bar closer
opts@lbLabelFontHeightF = 0.015
opts@lbLabelStride            = 1
opts@lbTitleString = "deg.C"
opts@lbTitlePosition = "bottom"
opts@lbTitleFontHeightF = 0.015
opts@pmLabelBarHeightF = 0.08

opts@gsnDraw          = False                ; turn off draw and frame
opts@gsnFrame         = False                ; b/c this is an overlay plot

opts@cnLevelSelectionMode = "ManualLevels"   ; set manual contour levels
opts@cnMinLevelValF       =   12.              ; set min contour level
opts@cnMaxLevelValF       =   32.             ; set max contour level
opts@cnLevelSpacingF      =   1.              ; set contour spacing

opts@tiMainString    = runname0 ; add titles
sst_plt=sst0
;sst_plt = where(msk0.eq.1, \
;                sst0@_FillValue, sst0)
plot = gsn_csm_contour_map(wks,sst_plt,opts)

;delete(opts)

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")

gsn_text_ndc(wks,CASE+" "+RUN,  0.05,0.850,txres)
gsn_text_ndc(wks,indir0,  0.05,0.875,txres)
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)


; MAKE PLOTS
draw(plot)
frame(wks)

print("")
print("Input:  "+ infile0)
print("")
print("Output: "+ figfile+"."+type)
print("")
end

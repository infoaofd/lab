#!/bin/sh

CASE=H30
RUN=R14
runname0=${CASE}.${RUN}.00.00 #_00 #CNTL
runname1=${CASE}.${RUN}.01.01 #_00 #

domain=d01

start=20180707
  end=20180707
#  end=20180708

exe=runncl.sh

ncl=$(basename $0 .sh).ncl

indir_root=/work06/manda/WPS.OUT

if [ ! -d $indir_root ]; then
echo
echo ERROR in $0 : NO SUCH DIR, $indir_root
echo
exit 1
fi

indir0=${indir_root}/$runname0
indir1=${indir_root}/$runname1


export LANG=C

hhlist="00 06 12 18"
#hhlist="00 03 06 09 12 15 18 21" # 06 12 18"

starts=$(date -d "${start}" '+%s')
ends=$(date -d "${end}" '+%s')

times=$(expr $ends - $starts)
days=$(expr $times / 86400 )

sdate=${start:0:4}/${start:4:2}/${start:6:2}
#echo $sdate
yesterday=$(date -d"$sdate"-1days '+%Y/%m/%d')
#echo $yesterday


n=0
while [ $n -le $days ]; do

today=$(date -d"$yesterday"+1days '+%Y-%m-%d')

for hh in $hhlist; do
datetime=${today}_${hh}:00:00

echo $datetime

met_em_file="met_em.${domain}.${datetime}.nc"
in=${indir0}/${met_em_file}
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
in=${indir1}/${met_em_file}
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi

$exe $ncl "$runname0" "$runname1" "$indir_root" "$domain" "$datetime" "$CASE" "$RUN"

done #hh

n=$(expr $n + 1)
yesterday=$today
done #n

exit 0

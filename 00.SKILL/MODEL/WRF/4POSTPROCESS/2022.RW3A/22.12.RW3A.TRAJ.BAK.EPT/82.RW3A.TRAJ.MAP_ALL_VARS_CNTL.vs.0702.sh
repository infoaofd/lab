#!/bin/bash
# Description:
# Directory: /work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.32.RW3A.TRAJ.FWD.10MIN.01
#
. ./gmtpar.sh
gmtset ANOT_FONT_SIZE 10 LABEL_FONT_SIZE 13
gmtset HEADER_FONT_SIZE 14p HEADER_OFFSET = -0.5c

RUN1=RW3A.00.03.05.05.0000.01.d01_T00.01
RUN2=RW3A.00.03.05.05.0702.01.d01_T00.01
if [ $RUN1 = "RW3A.00.03.05.05.0000.01.d01_T00.01" ];then RUNNAME1=CNTL;fi
if [ $RUN2 = "RW3A.00.03.05.05.0702.01.d01_T00.01" ];then RUNNAME2=0702;fi

INDIR1=12.RW3A.TRAJ.${RUNNAME1}/${RUN1}
if [ ! -d $INDIR1 ];then echo NO SUCH DIR,$INDIR1;exit 1;fi
INDIR2=14.RW3A.TRAJ.${RUNNAME2}/${RUN2}
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi
LONW=123.5; LONE=131.5; LATS=25;LATN=33.0
ZB2=345; ZT2=365
ZB3=-50; ZT3=250
ZB4=-0.5;ZT4=2
ZB5=0;   ZT5=20

OUT=$(basename $0 .sh)_${RUN}.ps; FIG=$(basename $OUT .ps).PDF

range=${LONW}/${LONE}/${LATS}/${LATN}
size=M4
xanot=a2f1; yanot=a2f1
anot=${xanot}/${yanot}:.${RUNNAME}:WSne

R2=${ZB2}/${ZT2}/${LATS}/${LATN}; S2=X1.1/4.54
xanot=a10f5; yanot=a2f1
anot2=${xanot}/${yanot}:."EPT${sp}[K]":wSne

R3=${ZB3}/${ZT3}/${LATS}/${LATN}; S2=X1.1/4.54
xanot=a100f50; yanot=a2f1
anot3=${xanot}/${yanot}:."LHF${sp}[W/m@+2@+]":wSne

R4=${ZB4}/${ZT4}/${LATS}/${LATN}; S2=X1.1/4.54
xanot=a1f0.5; yanot=a2f1
anot4=${xanot}/${yanot}:."SST-SAT${sp}[K]":wSne

R5=${ZB5}/${ZT5}/${LATS}/${LATN}; S2=X1.1/4.54
xanot=a5f5; yanot=a2f1
anot5=${xanot}/${yanot}wSnE:"Latitude${sp}[deg]"::."U@-10@-${sp}[m/s]":

echo MMMMM MAP
pscoast  -R$range -J$size -Df -W3 -X1 -Y1 -K >$OUT
<<COMMENT
INTOPO=/work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.20.MAKE_HGT.NC/01.MAKE_HGT.NC_RW3A.00.04.05.05.0000.01.2.nc
if [ ! -f $INTOPO ];then echo NO SUCH FILE,$INTOPO;exit 1;fi
CPT=13.RW3A.TRAJ.MAP.CPT
if [ ! -f $CPT ];then echo NO SUCH FILE,$CPT;exit 1;fi
#CPT=$(basename $0 .sh).CPT.TXT
#makecpt -Cdrywet -I -T-1000/1000/100 -Z >$CPT
#grdimage $INTOPO -R$range -J$size -C$CPT -X1 -Y5 -K -P >$OUT
grdcontour $INTOPO -R$range -J$size -W2/165/42/42 -A400f10t -Nm -C200 -L200,2000 -G1/2 -X1 -Y5 -K -P >$OUT
pscoast -R -J -Df -W3 -O -K >>$OUT
COMMENT

INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $6, $7}' $IN |psxy -R -J -W1/100/100/255 -O -K >>$OUT
done #IN

INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $6, $7}' $IN |psxy -R -J -W1/255/100/100 -O -K>>$OUT
done #IN

psbasemap -R -JM -B$anot -K -O >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R2} -J${S2} -B${anot2} -K -O -X4.3 >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $11, $7}' $IN |psxy -R -J -W1/200/200/255 -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $11, $7}' $IN |psxy -R -J -W1/255/200/200 -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $11,$7}' $IN |psxy -R -J -W10/0/0/255 -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $11,$7}' $IN |psxy -R -J -W10/255/0/0 -O -K >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R3} -J${S2} -B${anot3} -K -O -X1.3 >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $13, $7}' $IN |psxy -R -J -W1/200/200/255 -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $13, $7}' $IN |psxy -R -J -W1/255/200/200 -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $13,$7}' $IN |psxy -R -J -W10/0/0/255 -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $13,$7}' $IN |psxy -R -J -W10/255/0/0 -O -K >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R4} -J${S2} -B${anot4} -K -O -X1.3 >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $15, $7}' $IN |psxy -R -J -W1/200/200/255 -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $15, $7}' $IN |psxy -R -J -W1/255/200/200 -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $15,$7}' $IN |psxy -R -J -W10/0/0/255 -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $15,$7}' $IN |psxy -R -J -W10/255/0/0 -O -K >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R5} -J${S2} -B${anot5} -K -O -X1.3 >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $14, $7}' $IN |psxy -R -J -W1/200/200/255 -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $14, $7}' $IN |psxy -R -J -W1/255/200/200 -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $14,$7}' $IN |psxy -R -J -W10/0/0/255 -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $14,$7}' $IN |psxy -R -J -W10/255/0/0 -O -K >>$OUT



xoffset=-8.2; yoffset=4.4
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INDIR: ${INDIR}
0 0.90  9 0 1 LM OUTPUT: ${OUT}
EOF

#rm -vf $CPT
echo "INDIR: $INDIR"
if [ -f $OUT ];then rm -vf $FIG; echo MMMMM $OUT to $FIG; ps2pdfwr $OUT $FIG; fi
if [ -f $FIG ];then rm -vf $OUT; echo FIG: $FIG; fi


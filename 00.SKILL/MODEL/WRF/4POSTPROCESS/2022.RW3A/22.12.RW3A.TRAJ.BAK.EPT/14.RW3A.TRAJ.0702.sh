#!/bin/bash
ulimit -s unlimited

#-------------------------------------
CASE=RW3A; RUN=00.03
EXP=05.05.0702; IC=01
DOMAIN=d01
RUNNAME=${CASE}.${RUN}.${EXP}.${IC}
INTERVAL=01HR #10MN
OUTTYPE=traj
#-------------------------------------

#-------------------------------------
TRAJRN=T00.01
PLONC=129.9; PLATC=32.58 #SOUTHWEST CORNER (NOT CENTER)
PDLON=0.02; PDLAT=0.02
PZBOT=500; PDZ=100.    
NPX=5; NPY=5; NPZ=1
YYYYS=2021; MMS=8;DDS=12; HHS=03; MIS=00
YYYYE=2021; MME=8;DDE=11; HHE=12; MIE=00
DT_FILE=3600.0; TRAJ_STEP=300.0
INROOT=/work00/DATA/HD02/RW3A.ARWpost.DAT/traj/
INDIR=$INROOT"/${RUNNAME}/"

if [ ! -d $INDIR ];then echo NO SUCH DIR: $INDIR ;echo; exit 1;fi


DX=3000.0; DY=3000.0 #m
IMAX=599; JMAX=599; KMAX=61
LONW=120.56867; LONE=140.4200; LATS=18.56023; LATN=35.19536081

OUTDIR=$(basename $0 .sh)/${RUNNAME}.${DOMAIN}_${TRAJRN}
$(which mkd) $OUTDIR; rm -f ${OUTDIR}/${RUNNAME}*.txt

FC="ifort"
OPT="-O2 -assume byterecl -convert big_endian"
#DBG="-ftrapuv -fpe0 -traceback -CB"
DBG= #"-fpe0 -traceback -CB"

SRC=$(basename $0 .sh).f90; EXE=$(basename $0 .sh).exe
SUB="Calc_EPT.f90"
LOG=$(basename $0 .sh).LOG

NOW=$(date -R); CWD=$(pwd); CMD="$0 $@"

echo $NOW >$LOG; echo $CWD >>$LOG; echo $CMD >>$LOG



cat > $SRC << EOF
program make_trajectory

implicit none

integer :: id_beg(5) = (/${YYYYS}, ${MMS}, ${DDS}, ${HHS}, ${MIS}/)
integer :: id_end(5) = (/${YYYYE}, ${MME}, ${DDE}, ${HHE}, ${MIE}/)
real    :: slon, elon, slat, elat
parameter (slon = ${LONW}, elon = ${LONE}, slat=${LATS}, elat = ${LATN})

integer, parameter :: imax = ${IMAX}
integer, parameter :: jmax = ${JMAX}
integer, parameter :: kmax = ${KMAX}
real   , parameter :: dx = ${DX}     ! [m]
real   , parameter :: dy = ${DY}     ! [m]
real   , parameter :: dt_file = ${DT_FILE} ! [sec]
real   , parameter :: daysec = 86400.0 ! [sec]

 real    :: rlev(kmax) = (/ &
0.025, 0.050, 0.075, 0.100, 0.125, 0.150, 0.175, 0.200,&
0.225, 0.250, 0.275, 0.300, 0.325, 0.350, 0.375, 0.400, 0.450, 0.500,&
0.550, 0.600, 0.650, 0.700, 0.750, 0.800, 0.850, 0.900, 0.950, 1.000,&
1.050, 1.100, 1.150, 1.200, 1.250, 1.300, 1.350, 1.400, 1.450, 1.500,&
2.000, 2.500, 3.000, 3.500, 4.000, 4.500, 5.000, 5.500, 6.000, 7.000,&
8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0,&
20.0/)

real,dimension(imax,jmax) :: rlon, rlat, topo
real,dimension(imax,jmax,kmax) :: unow, vnow, wnow
real,dimension(imax,jmax,kmax) :: qnow, ptnow, eptnow
real,dimension(imax,jmax) :: LHnow,HFXnow, U10MAGnow      ! UMAG
real,dimension(imax,jmax) :: TDIFFnow ! SST-SAT

real,dimension(imax,jmax,kmax) :: unext, vnext, wnext
real,dimension(imax,jmax,kmax) :: qnext, ptnext, eptnext
real,dimension(imax,jmax) :: LHnext,HFXnext, U10MAGnext   ! UMAG
real,dimension(imax,jmax) :: TDIFFnext ! SST-SAT

integer, parameter :: maxtraj = 1000
integer :: ntraj = 1
real,dimension(maxtraj) :: xpos, ypos, zpos
real,dimension(maxtraj) :: lonpos, latpos
real,dimension(maxtraj) :: qpos, ptpos, eptpos
real,dimension(maxtraj) :: LHpos, HFXpos, U10MAGpos ! UMAG
real,dimension(maxtraj) :: TDIFFpos ! SST-SAT
real    :: finterp, hinterp

integer :: ifb = -1
real    :: fac1, fac2
real    :: dt_traj
real    :: uunext, uunow, vvnext, vvnow, wwnext, wwnow
real    :: uu, vv, ww
real    :: xnow, ynow, znow
real    :: xmax, ymax, zmax
real    :: fnow, fnext

real   , parameter :: undef  = -999.9

!-----------------------------------------------------------------------
! MEAN
!-----------------------------------------------------------------------
real::EPTAVE, EPTNUM, EPTSD, EPTSDP, EPTSDM
real::LONAVE, LONNUM
real::LATAVE, LATNUM
real::ZPOAVE, ZPONUM, ZPOSD, ZPOSDP, ZPOSDM
real::PTAVE, PTNUM, PTSD, PTSDP, PTSDM
real::QVAVE, QVNUM, QVSD, QVSDP, QVSDM
real::LHAVE,LHNUM,LHSD,LHSDP,LHSDM
real::HFXAVE,HFXNUM,HFXSD,HFXSDP,HFXSDM
real::U10MAGAVE,U10MAGNUM,U10MAGSD,U10MAGSDP,U10MAGSDM ! UMAG
real::TDIFFAVE,TDIFFNUM !SST-SAT

!-----------------------------------------------------------------------
! ... local
!-----------------------------------------------------------------------
character(len=200) :: ofile
integer :: itim
integer :: itr
integer :: ntrajtime
integer :: iyr, mon, idy, ihr, imin, isec
integer :: iyr1, mon1, idy1, ihr1, imin1, isec1
integer :: i, j, k, kk
real(8) :: jd_beg, jd_end, jd

! INITIAL POSITION OF PARCELS
integer n,npi
integer idx,jdx,kdx

real*8,ALLOCATABLE,dimension(:)::plon,plat,pzin
real*8 ph1(imax,jmax),lm1(imax,jmax)
real*8 ph2,lm2
real*8,parameter :: re=6370.d0, d2r=3.141592653589793d0/180.d0
real*8::dd(imax,jmax)
real*8::dlon1g,dlat1g,dlon,dlat
INTEGER, ALLOCATABLE :: AR1(:)
integer ndim

real plonc, platc

integer ip,jp,kp 

print *,'!======================================================================='
print *,''
print *,' NOTE: HEIGHT COORDINATE must be used.'
print *,'       DO NOT USE PRESSURE COORDINATE.'
print *,''
print *,'!======================================================================='
print *

print *,'!-----------------------------------------------------------------------'
print *,' CHECK TIME'
print *,'!-----------------------------------------------------------------------'
print *

iyr = id_end(1); mon = id_end(2); idy = id_end(3); ihr = id_end(4)
imin =id_end(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_end)

iyr = id_beg(1); mon = id_beg(2); idy = id_beg(3); ihr = id_beg(4)
imin =id_beg(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_beg)

jd = jd_beg
if (jd_end > jd_beg) then
  ifb =  1  ! forward
else
  ifb = -1  ! backward
end if

print *,'!-----------------------------------------------------------------------'
print *,'jd_beg,jd_end=',jd_beg,jd_end
print *,'!-----------------------------------------------------------------------'

print *,'!-----------------------------------------------------------------------'
print *,'read_wrf: u, v, w at time = 0'
print *,'imin=',imin
print *,'!-----------------------------------------------------------------------'

CALL READ_WRF(iyr,mon,idy,ihr,imin, &
&  imax,jmax,kmax,rlon,rlat,topo, &
&  unow,vnow,wnow,qnow,ptnow,eptnow,HFXnow,LHnow,U10MAGnow, & !UMAG
&  TDIFFnow, & ! SST-SAT
&  undef)

print *,'!-----------------------------------------------------------------------'
print *,'rlon(1,1)=',rlon(1,1)
print *,'rlat(1,1)=',rlat(1,1)
print *,'!-----------------------------------------------------------------------'

print *,'!-----------------------------------------------------------------------'
print *,'DONE read_wrf'
print *,'!-----------------------------------------------------------------------'

dt_traj   = dt_file/${TRAJ_STEP}
ntrajtime = dt_file/dt_traj

rlev   = rlev*1000. ! *** km -> m ***

xmax   = (imax-1)*dx
ymax   = (jmax-1)*dy
zmax   = rlev(kmax)

print *,'!-----------------------------------------------------------------------'
print *,'     INITIAL POSITIONS OF PARCELS'
print *,'!-----------------------------------------------------------------------'
xpos = undef; ypos = undef; zpos = undef
lonpos = undef; latpos = undef
qpos = undef; ptpos = undef; eptpos = undef

LHpos = undef; HFXpos = undef

ndim = SIZE(SHAPE(dd))
ALLOCATE ( AR1(ndim))

npi=${NPX}*${NPY}*${NPZ}
allocate(plon(${NPX}),plat(${NPY}),pzin(${NPZ}))

ph1(:,:)=rlat(:,:)*d2r
lm1(:,:)=rlon(:,:)*d2r

print *
print *,'!-----------------------------------------------------------------------'
print *,'FINDING PARCELS INITIAL POSITION IN X-Y COORD'
print *,'!-----------------------------------------------------------------------'

n=0

do ip=1,${NPX}
plon(ip)=${PLONC} + ${PDLON}*float(ip-1)
enddo !ip

do jp=1,${NPY}
plat(jp)=${PLATC} + ${PDLAT}*float(jp-1)
enddo !jp

do kp=1,${NPZ}
pzin(kp)=${PZBOT} + ${PDZ}  *float(kp-1)
enddo !kp

ntraj=0

do kp=1,${NPZ}

kdx=0
do k=1,kmax-1
if(rlev(k)<=sngl(pzin(kp)) .and. sngl(pzin(kp))<rlev(k+1))then
kdx=k
endif
enddo !k
print *,kdx,rlev(kdx)
if(kdx==0)cycle

do jp=1,${NPY}

do ip=1,${NPX}

if (plon(ip) < slon) cycle
if (plon(ip) > elon) cycle
if (plat(jp) < slat) cycle
if (plat(jp) > elat) cycle


ph2=plat(jp)*d2r
lm2=plon(ip)*d2r

dd(:,:)=1.E20

do j=1,jmax
do i=1,imax

dd(i,j)=&
re*acos(sin(ph1(i,j))*sin(ph2)+cos(ph1(i,j))\
*cos(ph2)*cos(lm1(i,j)-lm2))

enddo !i
enddo !j

AR1 = MINLOC(dd)
idx=AR1(1)
jdx=AR1(2)

if (pzin(kp)<topo(idx,jdx)+50.0)cycle

dlon1g=(rlon(idx+1,jdx)-rlon(idx-1,jdx))/2.d0
dlat1g=(rlat(idx,jdx+1)-rlat(idx,jdx-1))/2.d0

dlon=plon(ip)-rlon(idx,jdx)
dlat=plat(jp)-rlat(idx,jdx)



if (rlev(kdx)   < topo(idx,jdx)) cycle
if (rlev(kdx-1) < topo(idx,jdx)) cycle
if (unow(idx,jdx,kdx) < undef+1) cycle
if (vnow(idx,jdx,kdx) < undef+1) cycle
if (wnow(idx,jdx,kdx) < undef+1) cycle
if (unow(idx,jdx,kdx-1) < undef+1) cycle
if (vnow(idx,jdx,kdx-1) < undef+1) cycle
if (wnow(idx,jdx,kdx-1) < undef+1) cycle

if (ptnow(idx,jdx,kdx) < undef+1) cycle
if (ptnow(idx,jdx,kdx-1) < undef+1) cycle
if (qnow(idx,jdx,kdx) < undef+1) cycle
if (qnow(idx,jdx,kdx-1) < undef+1) cycle
if (eptnow(idx,jdx,kdx) < undef+1) cycle
if (eptnow(idx,jdx,kdx-1) < undef+1) cycle

if (LHnow(idx,jdx) < undef+1) cycle
if (LHnow(idx,jdx) < undef+1) cycle

if (HFXnow(idx,jdx) < undef+1) cycle
if (HFXnow(idx,jdx) < undef+1) cycle

ntraj=ntraj+1
xpos(ntraj) = (float(idx-1) + 0.5)*dx  + dlon/dlon1g*dx
ypos(ntraj) = (float(jdx-1) + 0.5)*dy  + dlat/dlon1g*dy
zpos(ntraj) = pzin(kp)

print *
print '(A,i3.3)','PARCEL #',ntraj
print '(A)','plon(ip),plat(jp)'
print '(2f10.4)',plon(ip),plat(jp)
!print *
print '(A)','rlon(idx,jdx),rlat(idx,jdx)'
print '(2f10.4)',rlon(idx,jdx),rlat(idx,jdx)
print '(A)','idx,jdx,dd(idx,jdx)'
print '(2i5,f10.3)',idx,jdx,dd(idx,jdx)

print *
print '(A)','xpos(ntraj),ypos(ntraj),zpos(ntraj)'
print '(3f12.3)',xpos(ntraj),ypos(ntraj),zpos(ntraj)
print *

print '(A,i3.3)','DONE PARCEL #',ntraj
print *

end do !ip
end do !jp
end do !kp

print *,'NUMBER OF PARCELS  = ',ntraj


print *
print *,'!----------------------------------------------------------'
print *,'     OPEN OUTPUT FILES'
print *,'!----------------------------------------------------------'

! PARCEL
do itr = 1, ntraj
write(ofile,'(a,i3.3,a)') '${OUTDIR}/${RUNNAME}.${DOMAIN}_${TRAJRN}_',itr,'.txt'
open (100+itr,file=trim(ofile),form='formatted')
end do

! MEAN
open (7,file='${OUTDIR}/AVE_${RUNNAME}.${DOMAIN}_${TRAJRN}.txt')

print *
print *,'!----------------------------------------------------------'
print *,'     PRINT INITIAL CONDITION'
print *,'!----------------------------------------------------------'

do itr = 1, ntraj
   lonpos(itr) = hinterp(imax,jmax,rlon, &
&                          rlon,rlat,dx,dy, &
&                          xpos(itr),ypos(itr),undef)
   latpos(itr) = hinterp(imax,jmax,rlat, &
&                          rlon,rlat,dx,dy, &
&                          xpos(itr),ypos(itr),undef)

   HFXpos(itr) = hinterp(imax,jmax,HFXnow, &
&                  rlon,rlat,dx,dy, &
&                  xpos(itr),ypos(itr),undef)

   LHpos(itr) = hinterp(imax,jmax,LHnow, &
&                  rlon,rlat,dx,dy, &
&                  xpos(itr),ypos(itr),undef)

   U10MAGpos(itr) = hinterp(imax,jmax,U10MAGnow, & ! UMAG
&                  rlon,rlat,dx,dy,              & ! UMAG
&                  xpos(itr),ypos(itr),undef)      ! UMAG

   TDIFFpos(itr) = hinterp(imax,jmax,TDIFFnow,   & ! SST-SAT
&                  rlon,rlat,dx,dy,              & ! SST-SAT
&                  xpos(itr),ypos(itr),undef)      ! SST-SAT

   fnow=finterp(imax,jmax,kmax,qnow, &
&                 rlon,rlat,rlev,dx,dy, &
&                 xpos(itr),ypos(itr),zpos(itr),undef)
   qpos(itr) = fnow

   fnow=finterp(imax,jmax,kmax,ptnow, &
&                 rlon,rlat,rlev,dx,dy, &
&                 xpos(itr),ypos(itr),zpos(itr),undef)
   ptpos(itr) = fnow

   fnow=finterp(imax,jmax,kmax,eptnow, &
&                 rlon,rlat,rlev,dx,dy, &
&                 xpos(itr),ypos(itr),zpos(itr),undef)
   eptpos(itr) = fnow

if (lonpos(itr) < undef+1) cycle
if (latpos(itr) < undef+1) cycle
if (zpos(itr) < undef+1) cycle
if (ptpos(itr) < undef+1) cycle
if (qpos(itr) < undef+1) cycle

   write(100+itr,88) iyr,mon,idy,ihr,imin, &
&    lonpos(itr), latpos(itr), zpos(itr), &
&    qpos(itr)*1000.,ptpos(itr),eptpos(itr), &
&    HFXpos(itr),LHpos(itr),U10MAGpos(itr), & ! UMAG
&    TDIFFpos(itr) !SST-SAT

88    format(i4,1x,i2,1x,i2,1x,i2,1x,i2,   f10.4,1x,f9.4,1x,f8.2,1x,&
     & f8.3,1x,f7.2,1x,f7.2,1x,  3f8.2,1x, 2f8.2, 1x, 2f8.2)

      end do !itr

print *,'!----------------------------------------------------------'
print *,'! AVERAGE OVER PARCELS'
print *,'!----------------------------------------------------------'

LONNUM=0.; LONAVE=0.

LATNUM=0.; LATAVE=0.

EPTAVE=0.; EPTNUM=0.; EPTSD=0.; EPTSDP=0.; EPTSDM=0.
HFXAVE=0.; HFXNUM=0.0
LHAVE=0.; LHNUM=0.0
U10MAGAVE=0.; U10MAGNUM=0.0 ! UMAG
TDIFFAVE=0.; TDIFFNUM=0.0   ! SST-SAT
ZPOAVE=0.; ZPOSD=0.; ZPOSDP=0.; ZPOSDM=0.
PTAVE=0.; PTNUM=0.; PTSD=0.; PTSDP=0.; PTSDM=0.
QVAVE=0.; QVNUM=0.; QVSD=0.; QVSDP=0.; QVSDM=0.

do itr = 1, ntraj
if (zpos(itr) < 50000.)then

LONNUM=EPTNUM+1.0; LONAVE=LONAVE+lonpos(itr)
LATNUM=LATNUM+1.0; LATAVE=LATAVE+latpos(itr)
ZPOAVE=ZPOAVE+zpos(itr)

PTNUM=PTNUM+1.0; PTAVE=PTAVE+PTpos(itr)
QVNUM=QVNUM+1.0; QVAVE=QVAVE+qpos(itr)

EPTNUM=EPTNUM+1.0; EPTAVE=EPTAVE+eptpos(itr)

HFXNUM=HFXNUM+1.0; HFXAVE=HFXAVE+HFXpos(itr)
LHNUM=LHNUM+1.0; LHAVE=LHAVE+LHpos(itr)
U10MAGNUM=U10MAGNUM+1.0; U10MAGAVE=U10MAGAVE+U10MAGpos(itr) ! UMAG
TDIFFNUM=TDIFFNUM+1.0; TDIFFAVE=TDIFFAVE+TDIFFpos(itr)      ! SST-SAT
endif !zpos
enddo !itr

LONAVE=LONAVE/LONNUM; LATAVE=LATAVE/LATNUM
ZPOAVE=ZPOAVE/EPTNUM

PTAVE=PTAVE/PTNUM; QVAVE=QVAVE/QVNUM
EPTAVE=EPTAVE/EPTNUM

HFXAVE=HFXAVE/HFXNUM
LHAVE=LHAVE/LHNUM
U10MAGAVE=U10MAGAVE/U10MAGNUM   ! UMAG
TDIFFAVE=TDIFFAVE/TDIFFNUM      ! SST-SAT

do itr = 1, ntraj
if (zpos(itr) < 50000. .and. eptpos(itr) > undef .and. eptpos(itr) < 1000. )then

PTSD=PTSD+(PTAVE-ptpos(itr))**2
QVSD=QVSD+(QVAVE-qpos(itr))**2
EPTSD=EPTSD+(EPTAVE-eptpos(itr))**2

ZPOSD=ZPOSD+(ZPOAVE-zpos(itr))**2

endif !zpos
enddo !itr

PTSD=sqrt(PTSD/EPTNUM); QVSD=sqrt(QVSD/QVNUM)
EPTSD=sqrt(EPTSD/EPTNUM)

  PTSDP= PTAVE+   PTSD;   PTSDM= PTAVE-   PTSD
  QVSDP= QVAVE+   QVSD;   QVSDM= QVAVE-   QVSD
 EPTSDP=EPTAVE+  EPTSD;  EPTSDM=EPTAVE-  EPTSD

write(7,88) iyr,mon,idy,ihr,imin, &
&    LONAVE, LATAVE, ZPOAVE, &
&    QVAVE*1000.,PTAVE,EPTAVE,&
&    HFXAVE,LHAVE,U10MAGAVE, &        ! UMAG
&    TDIFFAVE

print *
print *,'!----------------------------------------------------------'
print *,' TIME LOOP TO COMPUTE PARCEL POSITIONS'
print *,'!----------------------------------------------------------'

MAIN_TIME_LOOP: DO 

print *
print *,'!----------------------------------------------------------'
print *,'     read u, v, w at next time'
print *,'!----------------------------------------------------------'

! DATE-> JULIAN DAY 
  jd = jd + (dt_file/daysec)*ifb
  call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd)

   if (ifb < 0 .and. ihr1 == 24) then
      call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd+0.01/daysec)
   end if

   call read_wrf(iyr1,mon1,idy1,ihr1,imin1, &
  & imax,jmax,kmax,rlon,rlat,topo, &
  & unext,vnext,wnext,qnext,ptnext,eptnext,HFXnext,LHnext,U10MAGnext, & !UMAG
  & TDIFFnext, & ! SST-SAT
    undef)

!     write(6,'(a,1x,i4.4,5(1x,i2.2),1x,f20.8)')
!    &    'now ',iyr,mon,idy,ihr,imin,isec,jd
!     write(6,'(a,1x,i4.4,5(1x,i2.2),1x,f20.8)')
!    &    'next',iyr1,mon1,idy1,ihr1,imin1,isec1,jd
!     go to 66

print *
print *,'!----------------------------------------------------------'
print *,'     integrate the trajectories'
print *,'!----------------------------------------------------------'

      do itim = 1, ntrajtime
        fac1 = (itim-1)*dt_traj/dt_file
        fac2 =  itim   *dt_traj/dt_file

        do itr = 1, ntraj
          if (xpos(itr) < undef+1) cycle
          if (ypos(itr) < undef+1) cycle
          if (zpos(itr) < undef+1) cycle
!
! ... First iteration
!
          uunow=finterp(imax,jmax,kmax,unow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)
          uunext=finterp(imax,jmax,kmax,unext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)
          vvnow=finterp(imax,jmax,kmax,vnow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)
          vvnext=finterp(imax,jmax,kmax,vnext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)
          wwnow=finterp(imax,jmax,kmax,wnow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)
          wwnext=finterp(imax,jmax,kmax,wnext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xpos(itr),ypos(itr),zpos(itr),undef)

!         print *,'',uunow,uunext,vvnow,vvnext,wwnow,wwnext
          if (uunow < undef+1 .or. uunext < undef+1 .or. &
     &        vvnow < undef+1 .or. vvnext < undef+1 .or. &
     &        wwnow < undef+1 .or. wwnext < undef+1) then
            xpos(itr)=undef; ypos(itr)=undef; zpos(itr)=undef
            lonpos(itr)=undef; latpos(itr)=undef

            qpos(itr)=undef; ptpos(itr)=undef; eptpos(itr)=undef

            cycle
          endif

          uu = (1.-fac1)*uunow + fac1*uunext
          vv = (1.-fac1)*vvnow + fac1*vvnext
          ww = (1.-fac1)*wwnow + fac1*wwnext

          xnow = xpos(itr) + uu*ifb*dt_traj
          ynow = ypos(itr) + vv*ifb*dt_traj
          znow = zpos(itr) + ww*ifb*dt_traj
!
! ... Make sure zknow is within vertical domain.
!
          if (xnow <= 0.0 .or. xnow >= xmax .or. &
     &        ynow <= 0.0 .or. ynow >= ymax .or. &
     &        znow <= 0.0 .or. znow >= zmax ) then
            xpos(itr)=undef; ypos(itr)=undef; zpos(itr)=undef
            lonpos(itr)=undef; latpos(itr)=undef
            qpos(itr)=undef; ptpos(itr)=undef; eptpos(itr)=undef

          end if
!
! ... Second iteration
!
          uunow=finterp(imax,jmax,kmax,unow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)
          uunext=finterp(imax,jmax,kmax,unext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)
          vvnow=finterp(imax,jmax,kmax,vnow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)
          vvnext=finterp(imax,jmax,kmax,vnext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)
          wwnow=finterp(imax,jmax,kmax,wnow, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)
          wwnext=finterp(imax,jmax,kmax,wnext, &
     &                rlon,rlat,rlev,dx,dy, &
     &                xnow,ynow,znow,undef)

          if (uunow < undef+1 .or. uunext < undef+1 .or. &
     &        vvnow < undef+1 .or. vvnext < undef+1 .or. &
     &        wwnow < undef+1 .or. wwnext < undef+1) then
            xpos(itr)=undef; ypos(itr)=undef; zpos(itr)=undef
            lonpos(itr)=undef; latpos(itr)=undef
            qpos(itr)=undef; ptpos(itr)=undef; eptpos(itr)=undef

            cycle
          endif

          uu = 0.5*( uu + ((1.-fac2)*uunow + fac2*uunext) )
          vv = 0.5*( vv + ((1.-fac2)*vvnow + fac2*vvnext) )
          ww = 0.5*( ww + ((1.-fac2)*wwnow + fac2*wwnext) )

          xnow = xpos(itr) + uu*ifb*dt_traj
          ynow = ypos(itr) + vv*ifb*dt_traj
          znow = zpos(itr) + ww*ifb*dt_traj
!
! ... Make sure zknow is within vertical domain.
!
          if (xnow <= 0.0 .or. xnow >= xmax .or. &
     &        ynow <= 0.0 .or. ynow >= ymax .or. &
     &        znow <= 0.0 .or. znow >= zmax ) then
            xpos(itr)=undef; ypos(itr)=undef; zpos(itr)=undef
            lonpos(itr)=undef; latpos(itr)=undef
            qpos(itr)=undef; ptpos(itr)=undef; eptpos(itr)=undef

            cycle
          end if

          i = xnow/dx + 1
          j = ynow/dy + 1
          if (i < 1 .or. i > imax .or. &
     &        j < 1 .or. j > jmax) then
            xpos(itr)=undef; ypos(itr)=undef; zpos(itr)=undef
            lonpos(itr)=undef; latpos(itr)=undef
            qpos(itr)=undef; ptpos(itr)=undef; eptpos(itr)=undef

            cycle
          end if

          xpos(itr) = xnow
          ypos(itr) = ynow
          zpos(itr) = znow
!         lonpos(itr) = rlon(i,j)
!         latpos(itr) = rlat(i,j)

          lonpos(itr) = hinterp(imax,jmax,rlon, &
     &                          rlon,rlat,dx,dy, &
     &                          xpos(itr),ypos(itr),undef)
          latpos(itr) = hinterp(imax,jmax,rlat, &
     &                          rlon,rlat,dx,dy, &
     &                          xpos(itr),ypos(itr),undef)

        fnow = hinterp(imax,jmax,HFXnow, &
     &                  rlon,rlat,dx,dy, &
     &                  xnow,ynow,undef)
        fnext = hinterp(imax,jmax,HFXnext, &
     &                  rlon,rlat,dx,dy, &
     &                  xnow,ynow,undef)
        HFXpos(itr) = (1.-fac1)*fnow + fac1*fnext

        fnow = hinterp(imax,jmax,LHnow, &
     &                  rlon,rlat,dx,dy, &
     &                  xnow,ynow,undef)
        fnext = hinterp(imax,jmax,LHnext, &
     &                  rlon,rlat,dx,dy, &
     &                  xnow,ynow,undef)
        LHpos(itr) = (1.-fac1)*fnow + fac1*fnext

        fnow = hinterp(imax,jmax,U10MAGnow, &        ! UMAG
     &                  rlon,rlat,dx,dy,    &        ! UMAG
     &                  xnow,ynow,undef)             ! UMAG
        fnext = hinterp(imax,jmax,U10MAGnext, &      ! UMAG
     &                  rlon,rlat,dx,dy,      &      ! UMAG
     &                  xnow,ynow,undef)             ! UMAG
        U10MAGpos(itr) = (1.-fac1)*fnow + fac1*fnext ! UMAG


        fnow = hinterp(imax,jmax,TDIFFnow, &         ! SST-SAT
     &                  rlon,rlat,dx,dy,    &        ! SST-SAT
     &                  xnow,ynow,undef)             ! SST-SAT
        fnext = hinterp(imax,jmax,TDIFFnext, &       ! SST-SAT
     &                  rlon,rlat,dx,dy,      &      ! SST-SAT
     &                  xnow,ynow,undef)             ! SST-SAT
        TDIFFpos(itr) = (1.-fac1)*fnow + fac1*fnext  ! SST-SAT

          fnow=finterp(imax,jmax,kmax,qnow, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          fnext=finterp(imax,jmax,kmax,qnext, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          qpos(itr) = (1.-fac1)*fnow + fac1*fnext

          fnow=finterp(imax,jmax,kmax,ptnow, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          fnext=finterp(imax,jmax,kmax,ptnext, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          ptpos(itr) = (1.-fac1)*fnow + fac1*fnext

          fnow=finterp(imax,jmax,kmax,eptnow, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          fnext=finterp(imax,jmax,kmax,eptnext, &
     &                 rlon,rlat,rlev,dx,dy, &
     &                 xnow,ynow,znow,undef)
          eptpos(itr) = (1.-fac1)*fnow + fac1*fnext


        end do ! loop for do itr
      enddo    ! loop for itim

print *
print *,'!----------------------------------------------------------'
print *,'     OUTPUT ',iyr1,mon1,idy1,ihr1,imin1
print *,'!----------------------------------------------------------'

      do itr=1,ntraj
        if (lonpos(itr) < undef+1) cycle
        if (latpos(itr) < undef+1) cycle
        if (zpos(itr) < undef+1) cycle
        write(100+itr,88) iyr1,mon1,idy1,ihr1,imin1, &
     &    lonpos(itr), latpos(itr), zpos(itr), &
     &    qpos(itr)*1000.,ptpos(itr),eptpos(itr),&
     &    HFXpos(itr),LHpos(itr),U10MAGpos(itr), & !UMAG
     &    TDIFFpos(itr) !SST-SAT
      enddo !itr

print *,'!----------------------------------------------------------'
print *,'! AVERAGE OVER PARCELS'
print *,'!----------------------------------------------------------'

LONNUM=0.; LONAVE=0.

LATNUM=0.; LATAVE=0.

EPTAVE=0.; EPTNUM=0.; EPTSD=0.; EPTSDP=0.; EPTSDM=0.
HFXAVE=0.; HFXNUM=0.0
LHAVE=0.; LHNUM=0.0
U10MAGAVE=0.0;U10MAGNUM=0.0  ! UMAG
TDIFFAVE=0.0;TDIFFNUM=0.0    ! SST-SAT
ZPOAVE=0.; ZPOSD=0.; ZPOSDP=0.; ZPOSDM=0.
PTAVE=0.; PTNUM=0.; PTSD=0.; PTSDP=0.; PTSDM=0.
QVAVE=0.; QVNUM=0.; QVSD=0.; QVSDP=0.; QVSDM=0.

do itr = 1, ntraj
if (zpos(itr) < 50000.)then

LONNUM=EPTNUM+1.0; LONAVE=LONAVE+lonpos(itr)
LATNUM=LATNUM+1.0; LATAVE=LATAVE+latpos(itr)
ZPOAVE=ZPOAVE+zpos(itr)

PTNUM=PTNUM+1.0; PTAVE=PTAVE+PTpos(itr)
QVNUM=QVNUM+1.0; QVAVE=QVAVE+qpos(itr)

EPTNUM=EPTNUM+1.0; EPTAVE=EPTAVE+eptpos(itr)

HFXNUM=HFXNUM+1.0; HFXAVE=HFXAVE+HFXpos(itr)
LHNUM=LHNUM+1.0; LHAVE=LHAVE+LHpos(itr)
U10MAGNUM=U10MAGNUM+1.0; U10MAGAVE=U10MAGAVE+U10MAGpos(itr) !UMAG
TDIFFNUM=TDIFFNUM+1.0; TDIFFAVE=TDIFFAVE+TDIFFpos(itr) !SST-SAT
endif !zpos
enddo !itr

LONAVE=LONAVE/LONNUM; LATAVE=LATAVE/LATNUM
ZPOAVE=ZPOAVE/EPTNUM

PTAVE=PTAVE/PTNUM; QVAVE=QVAVE/QVNUM
EPTAVE=EPTAVE/EPTNUM

HFXAVE=HFXAVE/HFXNUM
LHAVE=LHAVE/LHNUM
U10MAGAVE=U10MAGAVE/U10MAGNUM !UMAG
TDIFFAVE=TDIFFAVE/TDIFFNUM    !SST-SAT

do itr = 1, ntraj
if (zpos(itr) < 50000. .and. eptpos(itr) > undef .and. eptpos(itr) < 1000. )then

PTSD=PTSD+(PTAVE-ptpos(itr))**2
QVSD=QVSD+(QVAVE-qpos(itr))**2
EPTSD=EPTSD+(EPTAVE-eptpos(itr))**2

ZPOSD=ZPOSD+(ZPOAVE-zpos(itr))**2

endif !zpos
enddo !itr

PTSD=sqrt(PTSD/EPTNUM); QVSD=sqrt(QVSD/QVNUM)
EPTSD=sqrt(EPTSD/EPTNUM)

  PTSDP= PTAVE+   PTSD;   PTSDM= PTAVE-   PTSD
  QVSDP= QVAVE+   QVSD;   QVSDM= QVAVE-   QVSD
 EPTSDP=EPTAVE+  EPTSD;  EPTSDM=EPTAVE-  EPTSD

write(7,88) iyr1,mon1,idy1,ihr1,imin1, &
&    LONAVE, LATAVE, ZPOAVE, &
&    QVAVE*1000.,PTAVE,EPTAVE,&
&    HFXAVE,LHAVE,U10MAGAVE, &  !UMAG
&    TDIFFAVE
66    continue



print *,'!----------------------------------------------------------'
print *,'     UPDATE'
print *,'!----------------------------------------------------------'

      iyr = iyr1; mon = mon1; idy = idy1; ihr = ihr1
      imin = imin1; isec = isec1

      unow(:,:,:) = unext(:,:,:)
      vnow(:,:,:) = vnext(:,:,:)
      wnow(:,:,:) = wnext(:,:,:)
      qnow(:,:,:) = qnext(:,:,:)
      ptnow(:,:,:) = ptnext(:,:,:)
      eptnow(:,:,:) = eptnext(:,:,:)

      HFXnow(:,:) = HFXnext(:,:)
      LHnow(:,:) = LHnext(:,:)
      U10MAGnow(:,:)=U10MAGnext(:,:)
      TDIFFnow(:,:)=TDIFFnext(:,:)
!-----------------------------------------------------------------------
!
!-----------------------------------------------------------------------
      if (ifb ==  1 .and. jd >= jd_end) exit
      if (ifb == -1 .and. jd <= jd_end) exit

END DO MAIN_TIME_LOOP

print *
print *,'!----------------------------------------------------------'
print *,' NUM OF PARTICLES = ',ntraj
print *,'!----------------------------------------------------------'

end



      real function finterp(imax,jmax,kmax,var, &
     &                      rlon,rlat,rlev,dx,dy,xc,yc,zc,undef)
      implicit none
!=======================================================================
!     computes a variable at position of trajectory
!     by interpolating data from the center of eight nearest boxes  
!=======================================================================
      integer :: imax, jmax, kmax
      real    :: var(imax,jmax,kmax)
      real    :: rlon(imax,jmax)
      real    :: rlat(imax,jmax)
      real    :: rlev(kmax)
      real    :: dx, dy
      real    :: xc, yc, zc
      real    :: undef
  
      integer :: im, jm, km, ip, jp, kp
      real    :: ax, ay, az
      real    :: xp, yp, zp, zm
      real    :: vppp, vppm, vpmp, vpmm, vmpp, vmpm, vmmp, vmmm
      integer :: i, j, k

! ... determining nearest centers of boxes 
      im = int(xc/dx) + 1
      jm = int(yc/dy) + 1
      km = 1
      do k = 1, kmax
        if (rlev(k) > zc) cycle
        km = k
      end do

      ip = min(im+1,imax)
      jp = min(jm+1,jmax)
      kp = min(km+1,kmax)

      xp = (ip-1)*dx
      yp = (jp-1)*dy
      zp = rlev(kp)
      zm = rlev(km)

      ax = (xp-xc)/dx
      ay = (yp-yc)/dy
      az = (zp-zc)/(zp-zm)

      vppp = var(ip,jp,kp)
      if (vppp < undef+1) vppp = var(im,jm,km)
      vppm = var(ip,jp,km)
      if (vppm < undef+1) vppm = var(im,jm,km)
      vpmp = var(ip,jm,kp)
      if (vpmp < undef+1) vpmp = var(im,jm,km)
      vpmm = var(ip,jm,km)
      if (vpmm < undef+1) vpmm = var(im,jm,km)
      vmpp = var(im,jp,kp)
      if (vmpp < undef+1) vmpp = var(im,jm,km)
      vmpm = var(im,jp,km)
      if (vmpm < undef+1) vmpm = var(im,jm,km)
      vmmp = var(im,jm,kp)
      if (vmmp < undef+1) vmmp = var(im,jm,km)
      vmmm = var(im,jm,km)
      if (vmmm < undef+1) vmmm = var(im,jm,km)

      finterp &
     &     = vppp*(1.-ax)*(1.-ay)*(1.-az) &
     &     + vmpp*    ax *(1.-ay)*(1.-az) &
     &     + vpmp*(1.-ax)*    ay *(1.-az) &
     &     + vmmp*    ax *    ay *(1.-az) &
     &     + vppm*(1.-ax)*(1.-ay)*    az  &
     &     + vmpm*    ax *(1.-ay)*    az  &
     &     + vpmm*(1.-ax)*    ay *    az  &
     &     + vmmm*    ax *    ay *    az  

      return
      end

      real function hinterp(imax,jmax,var, &
     &                      rlon,rlat,dx,dy,xc,yc,undef)
      implicit none
!=======================================================================
!     computes a variable at position of trajectory
!     by interpolating data from the center of eight nearest boxes  
!=======================================================================
      integer :: imax, jmax
      real    :: var(imax,jmax)
      real    :: rlon(imax,jmax)
      real    :: rlat(imax,jmax)
      real    :: dx, dy
      real    :: xc, yc
      real    :: undef
  
      integer :: im, jm, ip, jp
      real    :: ax, ay
      real    :: xp, yp
      real    :: vpp, vpm, vmp, vmm
      integer :: i, j

! ... determining nearest centers of boxes 
      im = int(xc/dx) + 1
      jm = int(yc/dy) + 1

      ip = min(im+1,imax)
      jp = min(jm+1,jmax)

      xp = (ip-1)*dx
      yp = (jp-1)*dy

      ax = (xp-xc)/dx
      ay = (yp-yc)/dy

      vpp = var(ip,jp)
      if (vpp < undef+1) vpp = var(im,jm)
      vpm = var(ip,jm)
      if (vpm < undef+1) vpm = var(im,jm)
      vmp = var(im,jp)
      if (vmp < undef+1) vmp = var(im,jm)
      vmm = var(im,jm)
      if (vmm < undef+1) vmm = var(im,jm)

      hinterp &
     &     = vpp*(1.-ax)*(1.-ay) &
     &     + vmp*    ax *(1.-ay) &
     &     + vpm*(1.-ax)*    ay  &
     &     + vmm*    ax *    ay

      return
      end

      subroutine date2jd(year,month,day,hh,mm,ss,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Julian day from Gregolian caldendar
!-----------------------------------------------------------------------

! ... intent(in)
      integer :: year, month, day, hh, mm, ss
! ... intent(out)
      real(8) :: julian_day
! ... parameter
      real(8), parameter :: jd0  = 1720996.5d0 ! BC4713/ 1/ 1 12:00:00
      real(8), parameter :: mjd0 = 2400000.5d0 !   1858/11/17 00:00:00
! ... local
      integer :: y, m
      if (month < 3) then
        y = year - 1
        m = month + 12
      else
        y = year
        m = month
      end if

      julian_day = 365*(y) + int(y)/4 - int(y)/100 + int(y)/400  &
     &           + int((m+1)*30.6001d0) &
     &           + day           &
     &           + hh/24.0d0     &
     &           + mm/1440.0d0   &
     &           + ss/86400.0d0  &
     &           + jd0

! ... convert julian day to modified julian day
      julian_day = julian_day - mjd0

      end subroutine date2jd

      subroutine jd2date(year,month,day,hour,min,sec,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Gregolian caldendar from Julian day
!-----------------------------------------------------------------------

! ... intent(in)
      real(8) :: julian_day
! ... intent(out)
      integer :: year, month, day, hour, min, sec
! ... parameter
      real(8), parameter :: mjd0 = 2400000.5d0 !  1858/11/17 00:00:00
! ... local
      integer :: jalpha, ia, ib, ic, id
      integer :: itime
      real(8) :: jday, d, xtime

! ... convert modified julian day to julian day
      jday = julian_day + mjd0

      jday = jday + 0.5d0
      if (jday >= 2299161) then
        jalpha = int( (jday - 1867216.25d0)/36524.25d0 )
        d = jday + 1 + jalpha - int(0.25d0*jalpha)
      else
        d = jday
      end if

      ia = int(d) + 1524
      ib = int(6680.0d0 + ((ia-2439870) - 122.1d0)/365.25d0)
      ic = 365*ib + int(0.25d0*ib)
      id = int((ia-ic)/30.6001d0)
      xtime = (d-int(d))*86400.d0
      itime = xtime
      if ( xtime-itime > 0.5d0 ) itime = itime + 1

      day   = ia - ic - int(30.6001*id)

      month = id - 1
      if (month > 12) month = month - 12

      year  = ib - 4715
      if (month > 2) year = year - 1
      if (year <= 0) year = year - 1

      hour  = itime/3600.
      min   = (itime - hour*3600)/60
      sec   = itime - hour*3600 - min*60

      end subroutine jd2date



SUBROUTINE READ_WRF(iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,rlon,rlat,topo, &
&            u,v,w,q,theta,thetae, HFX, LH, U10MAG, & !UMAG
& TDIFF, & ! SST-SAT
& undef) 
implicit none
!=======================================================================
!     print '(A)','subroutine read_wrf read u, v, w, ... from ARWpost'
!=======================================================================
integer,intent(in):: iyr, mon, idy, ihr, imin
integer,intent(in):: imax, jmax, kmax

real,dimension(imax,jmax,kmax),intent(inout) :: u,v,w,q,theta,thetae
real,dimension(imax,jmax),intent(inout) :: rlon,rlat,topo,HFX,LH
real,dimension(imax,jmax),intent(inout) :: U10MAG      !UMAG
real,dimension(imax,jmax),intent(inout) :: TDIFF       !SST-SAT
real,intent(in)::undef

real,dimension(imax,jmax) :: LWSDN_OUT,LWSUP_OUT
real,dimension(imax,jmax,kmax) :: pres, qc
real,dimension(imax,jmax,kmax) :: TADV,TTND,TCUM,TRAD,TPBL,TCMP

real,dimension(imax,jmax) :: Q2,T2,U10,V10
real,dimension(imax,jmax) :: SST,XLAND,RAINRC, RAINRNC

real,dimension(imax,jmax     ) :: DUM2
real,dimension(imax,jmax,kmax) :: DUM3

character(len=500) :: ifile, dir

logical :: yes
integer :: ier, irec
integer :: i, j, k

print '(A)','MMMMM SUBROUTINE READ_WRF MMMMM'

dir='${INDIR}'

write(ifile,'(a,a,i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a)') &
trim(dir),'/${RUNNAME}.${DOMAIN}.${OUTTYPE}.${INTERVAL}_', &
iyr,'-',mon,'-',idy,'_',ihr,'_',imin,'.dat'

print '(A,A)','mmmmm READ START mmmmm'
print '(A)',trim(ifile); print *

inquire(file=trim(ifile),exist=yes)
if (.not. yes) then
  print *,'no file : ',trim(ifile)
  stop
end if

open (10,file=trim(ifile),form='unformatted',action="read",&
      access='direct',recl=imax*jmax*4)

write(*,'(A)',advance='no') 'READ ' 
      
write(*,'(A)',advance='no') 'latitude '
irec=1
read (10,rec=irec) ((rlat(i,j),i=1,imax),j=1,jmax)

write(*,'(A)',advance='no') 'longitude '
irec=irec+1
read (10,rec=irec) ((rlon(i,j),i=1,imax),j=1,jmax)

write(*,'(A)',advance='no') ' u'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) ((u(i,j,k),i=1,imax),j=1,jmax)
end do

write(*,'(A)',advance='no') ' v'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) ((v(i,j,k),i=1,imax),j=1,jmax)
end do

write(*,'(A)',advance='no') ' w'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) ((w(i,j,k),i=1,imax),j=1,jmax)
end do

irec=irec+1 !Q2
irec=irec+1 !T2
read (10,rec=irec) ((T2(i,j),i=1,imax),j=1,jmax)          ! SST-SAT
irec=irec+1 !U10
read (10,rec=irec) ((U10(i,j),i=1,imax),j=1,jmax)         ! UMAG
irec=irec+1 !V10
read (10,rec=irec) ((V10(i,j),i=1,imax),j=1,jmax)         ! UMAG
WHERE(U10/=UNDEF .and. V10/=UNDEF)                        ! UMAG
U10MAG=SQRT(U10**2+V10**2)                                ! UMAG
ELSEWHERE     !UMAG
U10MAG=UNDEF  !UMAG
ENDWHERE      !UMAG


write(*,'(A)',advance='no') ' QVAPOR'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) q(:,:,k)
end do

irec=irec+61 ! QCLOUD
irec=irec+61 ! QRAIN
irec=irec+61 ! QICE

write(*,'(A)',advance='no') 'HGT'
irec = irec + 1
read (10,rec=irec) ((topo(i,j),i=1,imax),j=1,jmax)

irec=irec+1 !TSK
irec=irec+1 !XLAND
irec=irec+1 !HFX
read (10,rec=irec) ((HFX(i,j),i=1,imax),j=1,jmax) !FLX
irec=irec+1 !LH
read (10,rec=irec) ((LH(i,j),i=1,imax),j=1,jmax)  !FLX
irec=irec+1 !SST
read (10,rec=irec) ((SST(i,j),i=1,imax),j=1,jmax)  !SST-SAT
WHERE(SST/=UNDEF .and. T2/=UNDEF)                        ! SST-SAT
TDIFF=SST-T2                             ! SST-SAT
ELSEWHERE     ! SST-SAT
TDIFF=UNDEF   ! SST-SAT
ENDWHERE      ! SST-SAT

write(*,'(A)',advance='no') ' EPT'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) thetae(:,:,k)
end do

irec=irec+61 ! SEPT
irec=irec+61 ! pressure

write(*,'(A)',advance='no') ' tk'
do k = 1, kmax
  irec = irec + 1
  read (10,rec=irec) DUM3(:,:,k)
end do

irec=irec+61 ! CAPE
irec=irec+61 ! CIN
irec=irec+61 ! LCL3
irec=irec+61 ! LFC3

write(*,'(A)',advance='yes')'';print '(A)','mmmmm READ END'

close(10)

where (rlat  > 1.e20) rlat  = undef
where (rlon  > 1.e20) rlon  = undef
where (u     > 1.e20) u     = undef
where (v     > 1.e20) v     = undef
where (w     > 1.e20) w     = undef

!      call get_the(imax,jmax,kmax,pres,temp,q,thetae)

where (q      > 1.e20) q      = undef
where (theta  > 1.e20) theta  = undef
where (thetae > 1.e20) thetae = undef

return
end



      subroutine get_the(im,jm,km,pr,tk,qv,the)
      use Equiv_PT
      implicit none
!=======================================================================
!
!=======================================================================
      integer :: im, jm, km
      real    :: pr (im,jm,km)
      real    :: tk (im,jm,km)
      real    :: qv (im,jm,km)
      real    :: the(im,jm,km)

      real    :: q, p, e, t, tlcl
      integer :: i, j, k

      real    :: rgas, rgasmd
      real    :: cp, cpmd
      real    :: gamma, gammamd
      real    :: eps
      real    :: thtecon1, thtecon2, thtecon3
      real    :: tlclc1, tlclc2, tlclc3, tlclc4

      rgas=287.04  !J/K/kg
      rgasmd=.608   ! rgas_moist=rgas*(1.+rgasmd*qvp)
      cp=1004.     ! J/K/kg  Note: not using Bolton's value of 1005.7
      cpmd=.887   ! cp_moist=cp*(1.+cpmd*qvp)
      gamma=rgas/cp
      gammamd=rgasmd-cpmd  ! gamma_moist=gamma*(1.+gammamd*qvp)
!     grav=9.81           ! m/s**2
!     sclht=rgas*256./grav   ! 256 K is avg. trop. temp. from USSA.

      eps   = 0.622

      thtecon1=3376. ! K
      thtecon2=2.54
      thtecon3=0.81

      tlclc1=2840.
      tlclc2=3.5
      tlclc3=4.805
      tlclc4=55.

      the = 1.e30
      do k=1,km
      do j=1,jm
      do i=1,im
        if (pr(i,j,k) > 1.e20) cycle
        if (qv(i,j,k) > 1.e20) cycle
        if (tk(i,j,k) > 1.e20) cycle
!'define x=(287/1004*qvapor/tk)'
!'define thetae=theta*exp(x)'
        q = max(qv(i,j,k),1.e-15)
        p = pr(i,j,k) !*1.e2
        t = tk(i,j,k)

        the(i,j,k) = Calc_EPT(p,t,q)
        cycle

        e = q*p/(eps+q)
        tlcl=tlclc1/(log(t**tlclc2/e)-tlclc3)+tlclc4
        the(i,j,k)=t*(1000./p)**(gamma*(1.+gammamd*q))*&
     &         exp((thtecon1/tlcl-thtecon2)*q*(1.+thtecon3*q))


!       e   = 0.01*prs*qv/(0.6219718+qv+1.e-20)
!       t = th(i,j,k)*( prs/1000.e2 )**(287.04/1005.7)
!       tlcl = 55.0+2840.0/(3.5*log(t)-log(e)-4.805)
!       the(i,j,k) = t*( 1000.e2/prs )**(0.2854*(1.0-0.28*qv)) !&
!    &             *exp(((3376.0/tlcl)-2.54)*qv*(1.0+0.81*qv))
      end do
      end do
      end do

      return
      end
EOF

rm -f ${EXE} 2>&1 |tee -a $LOG


echo "$FC $OPT $DBG $SUB $SRC -o $EXE" 2>&1|tee -a $LOG
      $FC $OPT $DBG $SUB $SRC -o $EXE  2>&1|tee -a $LOG

ls -t --time-style=long-iso -lh $SRC $EXE 2>&1|tee -a $LOG
echo

./$EXE 2>&1|tee -a $LOG
if [ $? -eq 0 ]; then rm -vf $SRC *.mod *.o; fi

rm -vf $EXE

echo |tee -a $LOG
cp -av $LOG ${OUTDIR} 2>&1| head |tee -a $LOG
cp -av $0   ${OUTDIR} 2>&1| head |tee -a $LOG

echo |tee -a $LOG
du -sch ${OUTDIR} 2>&1|tee -a $LOG
ls -t --time-style=long-iso -lh ${OUTDIR} 2>&1| head -5 |tee -a $LOG


echo
echo $LOG
echo

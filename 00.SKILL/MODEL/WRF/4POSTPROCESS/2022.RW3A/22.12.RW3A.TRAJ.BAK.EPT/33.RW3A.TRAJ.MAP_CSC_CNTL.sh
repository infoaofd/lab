#!/bin/bash
# Description:
# Directory: /work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.32.RW3A.TRAJ.FWD.10MIN.01
#
. ./gmtpar.sh
gmtset ANOT_FONT_SIZE 10 LABEL_FONT_SIZE 13

RUN=RW3A.00.03.05.05.0000.01.d01_T00.01
if [ $RUN = "RW3A.00.03.05.05.0000.01.d01_T00.01" ];then RUNNAME=CNTL;fi
if [ $RUN = "RW3A.00.03.05.05.0702.01.d01_T00.01" ];then RUNNAME=0702;fi

INDIR=12.RW3A.TRAJ.CNTL/${RUN}
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
LONW=123.5; LONE=131.5; LATS=25;LATN=33.0; ZB=0; ZT=2

OUT=$(basename $0 .sh)_${RUN}.ps; FIG=$(basename $OUT .ps).PDF

range=${LONW}/${LONE}/${LATS}/${LATN}
size=M4
xanot=a2f2; yanot=a2f2
anot=${xanot}/${yanot}:.${RUNNAME}:WsNe

R2=${LONW}/${LONE}/${ZB}/${ZT}
S2=X4/1.2
xanot=a2f2; yanot=a1f0.5; 
anot2=${xanot}:Longitude:/${yanot}:"Altitude${sp}[km]":WSne

R3=${ZB}/${ZT}/${LATS}/${LATN}
S3=X1.2/4.7
xanot=a1f0.5; yanot=a2f2
anot3=${xanot}:"Altitude${sp}[km]":/${yanot}:Latitude:wSnE


echo MMMMM MAP
pscoast  -R$range -J$size -Df -W3 -X1 -Y5 -K -P >$OUT
<<COMMENT
INTOPO=/work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.20.MAKE_HGT.NC/01.MAKE_HGT.NC_RW3A.00.04.05.05.0000.01.2.nc
if [ ! -f $INTOPO ];then echo NO SUCH FILE,$INTOPO;exit 1;fi
CPT=13.RW3A.TRAJ.MAP.CPT
if [ ! -f $CPT ];then echo NO SUCH FILE,$CPT;exit 1;fi
#CPT=$(basename $0 .sh).CPT.TXT
#makecpt -Cdrywet -I -T-1000/1000/100 -Z >$CPT
#grdimage $INTOPO -R$range -J$size -C$CPT -X1 -Y5 -K -P >$OUT
grdcontour $INTOPO -R$range -J$size -W2/165/42/42 -A400f10t -Nm -C200 -L200,2000 -G1/2 -X1 -Y5 -K -P >$OUT
pscoast -R -J -Df -W3 -O -K >>$OUT
COMMENT

INLIST=$(ls $INDIR/${RUN}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

awk '{print $6, $7}' $IN |psxy -R -J -W1/255/0/0 -O -K >>$OUT
done #IN
psbasemap -R -JM -B$anot -K -O >>$OUT



echo MMMMM X-Z SECTION
psbasemap -R${R2} -J${S2} -B${anot2} -K -O -Y-1.6 >>$OUT
INLIST=$(ls $INDIR/${RUN}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

awk '{print $6, $8/1000}' $IN |psxy -R -J -W1/255/0/0 -O -K >>$OUT
done #IN



echo MMMMM Y-Z SECTION
psbasemap -R${R3} -J${S3} -B${anot3} -K -O -X4.3 -Y1.6 >>$OUT
INLIST=$(ls $INDIR/${RUN}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $8/1000, $7}' $IN |psxy -R -J -W1/255/0/0 -O -K >>$OUT
done #IN



xoffset=-4.6; yoffset=4.7
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INDIR: ${INDIR}
0 0.90  9 0 1 LM OUTPUT: ${OUT}
EOF

#rm -vf $CPT
echo "INDIR: $INDIR"
if [ -f $OUT ];then rm -vf $FIG; echo MMMMM $OUT to $FIG; ps2pdfwr $OUT $FIG; fi
if [ -f $FIG ];then rm -vf $OUT; echo FIG: $FIG; fi


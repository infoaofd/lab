#!/bin/bash

# hfront01
# /data07/thotspot/zamanda/K17.WRF.POST/22.00.DSE_BUDGET/14.00.TEST.TAV

usage(){
  echo USAGE $(basename $0) [-h][-d] [EX1] [EX2] [VAR]; exit 0
}

flagh="false"; flagd="false"; flagb="false"
while getopts hdl OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
    "b" ) flagg="true";;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

C=K17 ; R=R33
EX1=$1; EX2=$2; VAR=$3

EX1=${EX1:=00.00.IC2}
EX2=${EX2:=06.06.IC2}
VAR=${VAR:=RHOTAV}

DOMAIN=d03; INTERVAL=10MN; TYPE=basic_z
RUNNAME1=${C}.${R}.$EX1; RUNNAME2=${C}.${R}.$EX2

CTL1=${RUNNAME1}.${INTERVAL}.${DOMAIN}.${TYPE}_TAV.CTL
CTL2=${RUNNAME2}.${INTERVAL}.${DOMAIN}.${TYPE}_TAV.CTL
if [ ! -f $CTL1 ];then echo ERROR in $0; echo NO SUCH FILE,$CTL1; exit 1;fi
if [ ! -f $CTL2 ];then echo ERROR in $0; echo NO SUCH FILE,$CTL1; exit 1;fi

GS=$(basename $0 .sh).GS

LONW=130.75 ;LONE=
LATS=33.42  ;LATN=
LEV="0 18"

TEXT="$VAR ${EX1}-${EX2} $LONW $LATS"

FIG=${VAR}_${EX1}-${EX2}_${LONW}_${LATS}.eps

#TIME=

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

if [ $VAR = RHOTAV  ];then XMI=-0.006;XMX=0.006;XLINT=0.003; FAC=1;   fi
if [ $VAR = RHOTAV  ];then VAROUT="DRHO"; UNIT="kg/m3"; fi
if [ $VAR = QSATTAV ];then XMI=-2;XMX=2; XLINT=1; FAC=1000;fi
if [ $VAR = QSATTAV ];then VAROUT="DQSAT"; UNIT="g/kg"; fi
if [ $VAR = WTAV    ];then XMI=-6;  XMX=6;XLINT=3   FAC=1   ;fi
if [ $VAR = WTAV    ];then VAROUT="DW"; UNIT="m/s";fi
if [ $VAR = DQDZTAV ];then XMI=-6;XMX=6;XLINT=3;POW=6;FAC=1E${POW};fi
if [ $VAR = DQDZTAV ];then VAROUT="D DQDZTAV"; UNIT="g/kg/m";fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Thu, 21 Apr 2022 16:58:14 +0900
# hfront01
# /data07/thotspot/zamanda/K17.WRF.POST/22.00.DSE_BUDGET/14.00.TEST.TAV

'open ${CTL1}'; 'open ${CTL2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1

#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1) ; xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW}' ;# ${LONE}'
'set lat ${LATS}' ;# ${LATN}'
'set lev ${LEV}'
# 'set time ${TIME}'

'set cmark 0'
'set vrange $XMI $XMX'
'set xlint $XLINT'
'd (${VAR}.1-${VAR}.2)*${FAC}'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# X AXIS 
x=(xl+xr)/2
y=yb-0.4
'set strsiz 0.13 0.16'
'set string 1 c 3 0'
'draw string 'x' 'y' $VAROUT [${UNIT}]'

# LEGEND COLOR BAR
#x1=xl; x2=xr
#y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr
#y=y1
#'set strsiz 0.12 0.15'
#'set string 1 r 3 0'
#'draw string 'x' 'y' '



# TEXT
x=(xl+xr)/2
y=yt+0.3
'set strsiz 0.13 0.16'
'set string 1 c 3 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"

if [ $flagd != "true" ]; then rm -vf $GS; fi

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

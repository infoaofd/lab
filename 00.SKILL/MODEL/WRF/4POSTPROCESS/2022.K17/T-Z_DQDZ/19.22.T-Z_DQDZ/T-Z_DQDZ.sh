#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-h][-d] ARG; exit 0
}

flagh="false"; flagd="false"; flagb="false"
while getopts hdl OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
    "b" ) flagg="true";;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

C=K17; R=R33
EXP=$1  ; EXP=${EXP:-00.00.IC2}

F90=ifort
SRC="$(basename $0 .sh).F90"
SUB="T-Z_DQDZ_SUB_IO.F90 T-Z_DQDZ_SUB_CAL.F90 T-Z_DQDZ_SUB_STAT.F90"
EXE=$(basename $0 .sh).EXE
DOPT="-traceback -fpe0 -CB"
#OPT="    -assume byterecl -convert big_endian"
OPT=" -O2 -assume byterecl -convert big_endian"
NML=$(basename $0 .sh).NML


HDD=data07 #work04
RUNNAME=${C}.${R}.${EXP}
DOMAIN=d03; TYPE=basic_z; INTERVAL=10MN

INDIR_ROOT=/${HDD}/thotspot/zamanda/${C}.ARWpost/${INTERVAL}/${TYPE}
INDIR=${INDIR_ROOT}/ARWpost_${RUNNAME}

if [ ! -d $INDIR ]; then
echo ERROR in $0 : NO SUCH DIR, $INDIR
exit 1
fi

INPFX=${RUNNAME}.${DOMAIN}.${TYPE}.${INTERVAL}

VCOORDF=${C}.${R}.VCOORD.TXT
if [ ! -f $VCOODF ]; then echo ERROR in $0: NO SUCH FIELE, $VCOORDF;exit 1
fi

NUMTOUT=151; OUTDATES=01:00Z05JUL2017; DTOUT=10MN

ODIR=${INDIR_ROOT}/$(basename $0 .sh)_${RUNNAME}
if [ -d $ODIR ]; then rm -rfv $ODIR ;fi
$(which mkd) $ODIR; cp -av $0 $ODIR

OPFX=DQDZ_${INPFX}

OCTL=${ODIR}/${OPFX}.CTL

ALONW=130.4; ALONE=131.1; ALATS=33.3; ALATN=33.55

cat <<EOF>$NML
&para
indir="$INDIR",
inpfx="$INPFX",
iyr1=2017
mon1=07
idy1=05
ihr1=01
imin1=00
isec1=00
iyr2=2017
mon2=07
idy2=05
ihr2=13
imin2=00
isec2=00
dt_file=600
IM=300
JM=300
KM=49
vcoordf="$VCOORDF"
DX=1000.
DY=1000.
ODIR="$ODIR",
OPFX="$OPFX",
OFLE="$OFLE"
ALONW=$ALONW
ALONE=$ALONE
ALATS=$ALATS
ALATN=$ALATN
&end
EOF

echo "$F90 $OPT $DOPT $SRC $SUB -o $EXE"
$F90 $OPT $DOPT $SRC $SUB -o $EXE
if [ $? -ne 0 ]; then echo ERROR in $0 COMPILE ERROR!; exit 1; fi

# ulimit -s unlimited
$EXE < $NML
if [ $? -ne 0 ]; then echo ERROR in $EXE; exit 1; fi



cat<<EOF>$OCTL
dset ^${OPFX}_%y4-%m2-%d2_%h2:%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V3.7.1 MODEL
pdef  300 300 lcc  32.948  130.856  150.500  150.500  34.00000  24.00000  128.00000   1000.000   1000.000
xdef  749 linear  129.18993   0.00450450
ydef  632 linear   31.51415   0.00450450
zdef   49 levels  
   0.10000
   0.20000
   0.30000
   0.40000
   0.50000
   0.60000
   0.70000
   0.80000
   0.90000
   1.00000
   1.10000
   1.20000
   1.30000
   1.40000
   1.50000
   1.60000
   1.70000
   1.80000
   1.90000
   2.00000
   2.10000
   2.20000
   2.30000
   2.40000
   2.50000
   2.75000
   3.00000
   3.50000
   4.00000
   4.50000
   5.00000
   5.50000
   6.00000
   6.50000
   7.00000
   7.50000
   8.00000
   8.50000
   9.00000
   9.50000
  10.00000
  11.00000
  12.00000
  13.00000
  14.00000
  15.00000
  16.00000
  17.00000
  18.00000
tdef  ${NUMTOUT}   linear ${OUTDATES} ${DTOUT}      
VARS 8 
HGT       1 0 m TERRAIN HEIGHT
XLAND     1 0 LAND OR SEA 1 OR 2 
QVAPOR   49 0 g/kg
RHO      49 0 kg/m3
QSAT     49 0 g/kg
W        49 0 m s-1
DQSDZ    49 0 g/kg/m
DQDZ     49 0 g/kg/m
ENDVARS
EOF

cp -av $OCTL .
echo CTL: $OCTL
echo; echo DONE $(basename $0).; echo

exit 0

#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-h][-d] ARG ; exit 0
}

flagh="false"; flagd="false"; flagb="false"
while getopts hdl OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
    "b" ) flagg="true";;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

LOG=$(basename $0 .sh).LOG; GS=$(basename $0 .sh).GS

yyyy=2017; mm=07; dd=05
dp=$(expr $dd + 1)

lonw=130; lone=132; lats=33.2; latn=33.8
hs=01; he=13; dh=1

mlonw=130; mlone=132; mlats=33.2;  mlatn=33.8
hsm=01; hem=13 #09

echo "mmm BOX"
BLONW=130.7; BLONE=131.1; BLATS=33.42; BLATN=33.48.


IC=IC2
runname=$1; runname=${runname:-K17.R33.00.00.${IC}}

if [[ "$runname" == *.00.00* ]]; then
RUN=CNTL
elif [[ "$runname" == *.06.06* ]]; then
RUN=AO80
elif [[ "$runname" == *.00.06* ]]; then
RUN=O80
fi
echo; echo "RUNNAME = $runname; RUN = $RUN"; echo 

DOMAIN=d03

TYPE=basic_z; INTERVAL=10MN

indir=.

figdir=. #FIG_$(basename $0 .sh)_${yyyy}${mm}${dd}_${hs}-${he}_${mlonw}-${mlone}E_${mlats}-${mlatn}N
#mkdir -vp $figdir

figfile=${figdir}/$(basename $0 .sh)_${runname}.${INTERVAL}.${DOMAIN}.${TYPE}_TAV.eps


ctl=${runname}.${INTERVAL}.${DOMAIN}.${TYPE}_TAV.CTL
if [ ! -f $ctl ]; then echo ERROR in $0 ; echo NO SUCH FILE, $ctl; exit 1;fi

CMD="$0 $@"

xsm=0.8; xem=5.0; ysm=5; yem=9.0

xs=6; xe=10

export LANG=C; host=$(hostname); cwd=$(pwd); timestamp=$(date)

cat <<EOF>$GS
'cc'
'set grads off'
'set xlopts 1 2 0.12'; 'set ylopts 1 2 0.12'

'open ${ctl}'

yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif;if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif;if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif;if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif;if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif;if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif;if(mm='12');mmm='DEC';endif


datetime1=${hs}'Z'${dd}''mmm''yyyy; datetime2=${he}'Z'${dd}''mmm''yyyy
dtime1map=${hsm}'Z'${dd}''mmm''yyyy; dtime2map=${hem}'Z'${dd}''mmm''yyyy

kind='white->lavender->cornflowerblue->dodgerblue->blue->yellow->orange->red->darkmagenta'
clevs='5 10 15 20 25 30 35 40'

clevsmap='50 100 200 300 400 500 600 700'

'set vpage 0.0 11.0 0.0 8.5'

# MAP
'set xyrev off'
'set parea ${xsm} ${xem} ${ysm} ${yem}'
'color -levs ' clevsmap ' -gxout shaded -kind ' kind

'set lon ${mlonw} ${mlone}'; 'set lat ${mlats} ${mlatn}'

#'set time 'dtime1map
#'ra=RAINNC(time='dtime2map')-RAINNC(time='dtime1map')'
# ra'=sum(RAINRC+RAINRNC,time='dtime1map',time='dtime2map')'

'set grid off'
'set xlint 0.5'; 'set ylint 0.2'
'set mpdraw off'
'd ACRAIN'

'set mpdraw on'; 'set mpdset hires'; 'draw shp JPN_adm1'



'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

y1=yb-0.5; y2=y1+0.1
'color -levs ' clevsmap ' -gxout shaded -kind ' kind ' -xcbar 'xl' 'xr' 'y1' 'y2' -edge triangle -line on -ft 2'



say 'mmm WRITE AREA AVE'
rave'=aave(' ACRAIN ',lon=${lonw},lon=${lone},lat=${lats},lat=${latn})'
'd 'rave
raveout=subwrd(result,4)
raveoutint=math_nint(raveout)
say 'raveoutint='raveoutint'mm'

x=xl+0.7; y=yb+0.3
'draw string 'x' 'y' MEAN 'raveoutint' mm'

say 'mmm DISTANCE LEGEND'
leglonw=130.05; leglat=33.7; leglen=30 ;*km

re=6372.79548; d2r=3.14159265/180.0
arg=leglat*d2r
leglone=leglonw+leglen/re/math_cos(arg)/d2r
'trackplot 'leglonw' 'leglat' 'leglone' 'leglat'  -c 1 -l 1 -t 6'

texlon=leglonw+leglone; texlon=texlon*0.5
dlat=0.05
texlat=leglat+dlat

'q w2xy 'texlon' 'texlat
x=subwrd(result,3); y=subwrd(result,6)
'set string 1 c 2 0'
'draw string 'x' 'y' 'leglen'km'

say 'mmm BOX $BLONW $BLONE $LATS $BLATN'
'trackplot $BLONW $BLATS $BLONE $BLATS -c 1 -l 3 -t 2'
'trackplot $BLONE $BLATS $BLONE $BLATN -c 1 -l 3 -t 2'
'trackplot $BLONE $BLATN $BLONW $BLATN -c 1 -l 3 -t 2'
'trackplot $BLONW $BLATN $BLONW $BLATS -c 1 -l 3 -t 2'

#say 'ASAKURA'
#lat=33+24.4/60
#lon=130+41.7/60
#'set line 1 1 10'
#'markplot 'lon' 'lat' -c 1 -m 2 -s 0.1'




say 'mmm TITLE'
xx=xl; yy=yt+0.15 ;#7.8
'set strsiz 0.10 0.13'
'set string 1 l 3 0'
'draw string 'xx' 'yy' $RUN'

xx=(xl+xr)/2
title=dtime1map'-'dtime2map
'set string 1 c 3 0'
yy=yy+0.2 ;#7.8
'draw string 'xx' 'yy' 'title

yy=yy+0.2 ;#7.8
'draw string 'xx' 'yy' ${mlats}N-${mlatn}N ${mlonw}E-${mlone}E'

yy=yy+0.2 ;#7.8
'draw string 'xx' 'yy' ${runname}'



# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
xx=0.1; yy=yy+0.15
'draw string 'xx' 'yy' ${timestamp} ${host}'
xx=0.1; yy=yy+0.15
'draw string 'xx' 'yy' ${cwd}'
xx=0.1; yy=yy+0.15
'draw string 'xx' 'yy' ${CMD}'



#'print ${figfile}'
'gxprint ${figfile}'

say
say 'Fig file: ${figfile}'
say

say
say datetime1' - 'datetime2
say 'lon ${lonw} - ${lone}'
say 'lat ${lats} - ${latn}'
say

quit
EOF


date -R   >$LOG; pwd      >>$LOG; hostname >>$LOG; echo     >>$LOG

grads -bcp "$GS" |tee -a $LOG

cp -av $ctl .
if [ $flagd != "true" ]; then rm -vf $GS; fi

echo; echo mmmmmmm OUTPUT
ls -lh --time-style=long-iso $figfile
echo mmmmmmm; echo

exit 0

EOF


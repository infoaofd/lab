#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-d] E1 E2 E3 CN RN; exit 1
}

flagd="false"
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true" ;;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

E1=$1; E2=$2; E3=$3; C=$4; R=$5; IC=IC2
E1=${E1:-00.00}; E2=${E2:-06.06}; E3=${E3:-00.06}
C=${C:-K17}; R=${R:-R33}

EXP1OUT=CNTL; EXP2OUT=AO80; EXP3OUT=O80

MM1=07; MMM1=JUL; HH1=02; MI1=00; DD1=05
MM2=07; MMM2=JUL; HH2=10; MI2=00; DD2=05
MM3=07; MMM3=JUL; HH3=01; MI3=00; DD3=05
MM4=07; MMM4=JUL; HH4=13; MI4=00; DD4=05

TIME1=${HH1}:${MI1}Z${DD1}${MMM1}2017; TIME2=${HH2}:${MI2}Z${DD2}${MMM2}2017
TIME3=${HH3}:${MI3}Z${DD3}${MMM3}2017; TIME4=${HH4}:${MI4}Z${DD4}${MMM4}2017

VAR1="DQSDZ"  ; VAR1OUT="dQsat_dZ_RAW" ; UNIT1="[g/kg/km]"; SCL1="*1E6"

VARS=${VAR1OUT} #_${VAR2OUT}

lonw=130.3; lone=131.4; lats=33.25 ; latn=33.75
XLI1=0.4; YLI=3

RCLW=130.64 ;RCLE=130.97 ;RCLS=33.4 ;RCLN=33.48
#RCLW=130.6 ;RCLE=130.8 ;RCLS=33.4 ;RCLN=33.5;

LEV1="0.1 18"

PAPER=p
XMAX=3; YMAX=4
XLEFT=3; YTOP=8
PWID=7; PHIGHT=5; XMARGIN=1; YMARGIN=1; 
LEVS='-5 5 0.5'; FS=10
KIND='darkblue->blue->skyblue->white->gold->red->firebrick'

CLEVRAIN=500; CLEVHGT1="0.1"; CLEVHGT2="0.5" 
VXSKP=10; VYSKP=10; VLSCL=0.5; VLMAG=40; VLX=10; VLY=2; VLUNIT=${UNIT2} 

runname1=${C}.${R}.${E1}.${IC}
runname2=${C}.${R}.${E2}.${IC}
runname3=${C}.${R}.${E3}.${IC}

domain=d03; p_or_z=z; type=basic_z; interval=10MN

indir_root=/data07/thotspot/zamanda/K17.ARWpost/10MN/${type}

indir1=${indir_root}/T-Z_DQDZ_${runname1}
indir2=${indir_root}/T-Z_DQDZ_${runname2}
indir3=${indir_root}/T-Z_DQDZ_${runname3}

ctl1=${indir1}/DQDZ_${runname1}.${domain}.${type}.${interval}.CTL
ctl2=${indir2}/DQDZ_${runname2}.${domain}.${type}.${interval}.CTL
ctl3=${indir3}/DQDZ_${runname3}.${domain}.${type}.${interval}.CTL

OUTTIME=${MM1}_${DD1}${HH0}-${DD2}${HH2}
fig=T-Z_${VARS}_${C}.${R}_${domain}_${OUTTIME}.eps  #${lonw}-${lone}_${lats}-${latn}.eps

NCDIR=$(basename $fig .eps)
/work05/manda/mybin/mkd $NCDIR
NC1=HGT.nc; NC2=RA1.nc; NC3=RA2.nc; NC4=RA3.nc
NC5=AANO11.nc; NC6=AANO12.nc; NC7=AANO13.nc

TIMESTAMP=$(date -R); HOST=$(hostname); CWD=$(pwd)
COMMAND="$0 $@"
GS=$(basename $0 .sh).GS

BLUE=4; PURPLE=14; GREEN=3; RED=2; ORANGE=8; BLACK=1

cat <<EOF>$GS
'cc'

'open ${ctl1}'
say; say 'mmmmmmm'; say 'OPEN ${ctl1}'; say 'mmmmmmm'

'open ${ctl2}'
say; say 'mmmmmmm'; say 'OPEN ${ctl2}'; say 'mmmmmmm'

'open ${ctl3}'
say; say 'mmmmmmm'; say 'OPEN ${ctl3}'; say 'mmmmmmm'

say; say 'mmmmmmm'; say '${C} ${R} ${E1} ${E2} ${E3} ${VAR1} ${VAR2} '; say 'mmmmmmm'
     say 'mmmmmmm'; say '${TIME1} ${TIME2} ${TIME3} ${TIME4}'; say 'mmmmmmm'


'set vpage 0.0 8.5 0.0 11'
xmax=${XMAX}; ymax=${YMAX}
xleft=${XLEFT}; ytop=${YTOP}
xwid=${PWID}/${XMAX}; ywid=${PHIGHT}/${YMAX}
xmargin=${XMARGIN}; ymargin=${YMARGIN}

'set xlopts 1 3 0.11'; 'set ylopts 1 3 0.11'; 'set tlsupp year'

say; say 'mmmmmmm'; say 'AAV ${VAR1}'; say 'mmmmmmm'

'set time ${TIME1} ${TIME2}'; say '${TIME1} ${TIME2}'
'set lev ${LEV1}'; 'set lon ${lonw}'; 'set lat ${lats}'

say 
'q dims'; say sublin(result,2);say sublin(result,3);say sublin(result,4);
say

say 'mmm AAV ${VAR1}.1 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV11=aave(${VAR1}.1,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR1}.2 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV12=aave(${VAR1}.2,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR1}.3 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV13=aave(${VAR1}.3,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say; say 'mmmmmmm'; say '${VAR1} SCL ${SCL1}'; say 'mmmmmmm'
'AAV11=AAV11${SCL1}'; 'AAV12=AAV12${SCL1}'; 'AAV13=AAV13${SCL1}' 
say

say; say 'mmmmmmm'; say 'PLOT 1 ${EXP1OUT}'; say 'mmmmmmm'

xmap=1; ymap=1; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'
'set xlint $XLI1'; 'set ylint $YLI'
'set mpdset worldmap' ;#hires'

'color ${LEVS} -kind ${KIND} -gxout shaded'
say '${VAR1OUT}'
'd AAV11'

'set xlab off'; 'set ylab off'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xr+0.5; x2=x1+0.1; y1=2.5; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS} -ft 3 -line on -edge circle'

xx =(x1+x2)/2; yy = y2+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' ${UNIT1}'


xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' Altitude [km]'


xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $VAR1 ${EXP1OUT}'


say; say 'mmmmmmm'; say 'PRINT HEADER LINES'; say 'mmmmmmm'
'set string 1 c 3'
'set strsiz 0.10 0.12'
xx=(0+8.5)/2; yy=yy+0.27
'draw string ' xx ' ' yy ' ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'

yy=yy+0.25
'draw string ' xx ' ' yy ' ${C} ${R} ${EXP1OUT} ${EXP2OUT} ${EXP3OUT}'

# Header
'set strsiz 0.1 0.12'
'set string 1 l 3'
yy=yy+0.3
'draw string 0.5 'yy' ${TIMESTAMP} ${HOST}'
yy=yy+0.2
'draw string 0.5 'yy' 10.2 ${CWD}'
yy=yy+0.2
'draw string 0.5 'yy' 10 ${COMMAND}'



say; say 'mmmmmmm'; say 'PLOT 2 ${EXP2OUT}'; say 'mmmmmmm'

xmap=1; ymap=2; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'
'set xlint $XLI1'; 'set ylint $YLI'
'set mpdset worldmap'

'color ${LEVS} -kind ${KIND} -gxout shaded'
say '${VAR1OUT}'
'd AAV12'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' Altitude [km]'

#x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS} -ft 3 -line on -edge circle'

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $VAR1 ${EXP2OUT}'

'set xlab off'; 'set ylab off'



say; say 'mmmmmmm'; say 'PLOT 3 ${EXP3OUT}'; say 'mmmmmmm'

xmap=1; ymap=3; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'
'set xlint $XLI1'; 'set ylint $YLI'
'set mpdset worldmap' ;#hires

'color ${LEVS} -kind ${KIND} -gxout shaded'
say '${VAR1OUT}'
'd AAV13'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' Altitude [km]'

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $VAR1 ${EXP3OUT}'

'set xlab off'; 'set ylab off'



'gxprint $fig'

#'set sdfwrite ${NCDIR}/${NC1}'; 'sdfwrite HGT'
#'set sdfwrite ${NCDIR}/${NC2}'; 'sdfwrite RA1'
#'set sdfwrite ${NCDIR}/${NC3}'; 'sdfwrite RA2'
#'set sdfwrite ${NCDIR}/${NC4}'; 'sdfwrite RA3'
#'set sdfwrite ${NCDIR}/${NC5}'; 'sdfwrite AANO11'
#'set sdfwrite ${NCDIR}/${NC6}'; 'sdfwrite AANO12'
#'set sdfwrite ${NCDIR}/${NC7}'; 'sdfwrite AANO13'

'quit'
EOF

grads -bc${PAPER} "${GS}"

cp -av $ctl1 .
if [ $flagd != "true" ]; then rm -vf $GS; fi

echo; echo mmmmmmm OUTPUT
#ls -lhd --time-style=long-iso $NCDIR
ls -lh --time-style=long-iso $fig
echo mmmmmmm; echo

exit 0

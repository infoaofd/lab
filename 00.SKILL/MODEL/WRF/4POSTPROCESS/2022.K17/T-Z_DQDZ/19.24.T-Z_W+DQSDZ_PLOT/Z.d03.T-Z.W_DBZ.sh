#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-d] E1 E2 E3 CN RN; exit 1
}

flagd="false"
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true" ;;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

E1=$1; E2=$2; E3=$3; C=$4; R=$5; IC=IC2
E1=${E1:-00.00}; E2=${E2:-06.06}; E3=${E3:-00.06}
C=${C:-K17}; R=${R:-R33}

EXP1OUT=CNTL; EXP2OUT=AO80; EXP3OUT=O80

MM1=07; MMM1=JUL; HH1=02; MI1=10; DD1=05
MM2=07; MMM2=JUL; HH2=10; MI2=00; DD2=05

TIME1=${HH1}:${MI1}Z${DD1}${MMM1}2017; TIME2=${HH2}:${MI2}Z${DD2}${MMM2}2017

VAR1="dbz"; VAR1OUT="RR"; UNIT1="[dBZ]"; SCL1=""
VAR2="W"  ; VAR2OUT="W" ; UNIT2="[m/s]"; SCL2=""

VARS=${VAR1OUT}_${VAR2OUT}

lonw=130.3; lone=131.4; lats=33.25 ; latn=33.75

RCLW=130.64 ;RCLE=130.97 ;RCLS=33.4 ;RCLN=33.48;

LEV="0.1 18"

PAPER=p
XLEFT=0.6; XWID=2.4; XMARGIN1=0.1; XMARGIN2=1.5
YTOP=9; YHGT=2; YHGT2=1.5; YMARGIN=1.5

YLI=3

CAP1="(a) CNTL"; CAP2="(b) CNTL-AO80"; CAP3="(c) CNTL-O80"
CAP4="(d) CNTL"; CAP5="(e) CNTL-AO80"; CAP6="(f) CNTL-O80"

LEVS1='15 55 5'; FS1=2
KIND1='white->darkblue->green->yellow->orange->darkred'

LEVS2='-50 50 5'; FS2=5
KIND2='darkblue->blue->dodgerblue->skyblue->white->gold->darkorange->red->darkred' 

LEVS3='-12 12 2'; FS3=6
KIND3='darkblue->blue->dodgerblue->skyblue->white->gold->darkorange->red->darkred' 

LEVS4='-7 7 0.5'; FS4=7 
KIND4='darkblue->blue->dodgerblue->skyblue->white->gold->darkorange->red->darkred' 

runname1=${C}.${R}.${E1}.${IC}
runname2=${C}.${R}.${E2}.${IC}
runname3=${C}.${R}.${E3}.${IC}

domain=d03; p_or_z=z; type=basic_z; interval=10MN

indir_root=/data07/thotspot/zamanda/K17.ARWpost/10MN/${type}

indir1=${indir_root}/ARWpost_${runname1}
indir2=${indir_root}/ARWpost_${runname2}
indir3=${indir_root}/ARWpost_${runname3}

ctl1=${indir1}/${runname1}.${domain}.${type}.${interval}.ctl
ctl2=${indir2}/${runname2}.${domain}.${type}.${interval}.ctl
ctl3=${indir3}/${runname3}.${domain}.${type}.${interval}.ctl

OUTTIME=${MM1}_${DD1}${HH0}-${DD2}${HH2}
fig=T-Z_${VARS}_${C}.${R}.${IC}_${domain}.eps #_${OUTTIME}_${lonw}-${lone}_${lats}-${latn}.eps

#NCDIR=$(basename $fig .eps)
#$(which mkd) $NCDIR
#NC1=HGT.nc; NC2=RA1.nc; NC3=RA2.nc; NC4=RA3.nc

TIMESTAMP=$(date -R); HOST=$(hostname); CWD=$(pwd); CMD="$0 $@"
GS=$(basename $0 .sh).GS

BLUE=4; PURPLE=14; GREEN=3; RED=2; ORANGE=8; BLACK=1

cat <<EOF>$GS
'cc'

'open ${ctl1}'
say; say 'mmmmmmm'; say 'OPEN ${ctl1}'; say 'mmmmmmm'

'open ${ctl2}'
say; say 'mmmmmmm'; say 'OPEN ${ctl2}'; say 'mmmmmmm'

'open ${ctl3}'
say; say 'mmmmmmm'; say 'OPEN ${ctl3}'; say 'mmmmmmm'

say; say 'mmmmmmm'; say '${C} ${R} ${IC} ${E1} ${E2} ${E3} ${VAR1} ${VAR2} '; say 'mmmmmmm'
     say 'mmmmmmm'; say '${TIME1} ${TIME2}'; say 'mmmmmmm'


say; say 'mmmmmmm'; say 'AAV ${VAR1}'; say 'mmmmmmm'
'set time ${TIME1} ${TIME2}'; say '${TIME1} ${TIME2}'
'set lev ${LEV}'; 'set lon ${lonw}'; 'set lat ${lats}'
say 
'q dims'; say sublin(result,2);say sublin(result,3);say sublin(result,4);
say

say 'mmm AAV ${VAR1}.1 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV11=aave(${VAR1}.1,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR1}.2 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV12=aave(${VAR1}.2,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR1}.3 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV13=aave(${VAR1}.3,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'



say; say 'mmmmmmm'; say 'AAV ${VAR2}'; say 'mmmmmmm'
say 'mmm AAV ${VAR2}.1 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV21=aave(${VAR2}.1,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR2}.2 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV22=aave(${VAR2}.2,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'

say 'mmm AAV ${VAR2}.3 ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'; '!date -R'
'AAV23=aave(${VAR2}.3,lon=${RCLW},lon=${RCLE},lat=${RCLS},lat=${RCLN})'



'set vpage 0.0 8.5 0.0 11'

'set grads off'; 'set grid off'

'set xlopts 1 3 0.11'; 'set ylopts 1 3 0.11'; 'set tlsupp year'



say; say 'MMMMMMMMMM'; say 'UPPER PANEL'; say 'MMMMMMMMMM'

'set lev ${LEV}'; 'q dims'; say sublin(result,4)

say; say 'mmmmmmm'; say '$VAR1 RAW ${EXP1OUT}'; say 'mmmmmmm'

'set xlab on'; 'set ylab on'

xs=${XLEFT};xe=xs+${XWID}; ye = ${YTOP} ;ys=ye-${YHGT}
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set parea 'xs ' 'xe' 'ys' 'ye
'set ylint $YLI'

say '${VAR1OUT}'
'color ${LEVS1} -kind ${KIND1} -gxout shaded'
'd AAV11'

'set xlab off'; 'set ylab off'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx =xl-0.5; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' Altitude [km]'

x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS1} -ft 3 -line on -edge circle'

xx = (x1+x2)*0.5; yy = y1-0.3 
'set string 1 c 3 0'; 'set strsiz 0.1 0.12'
'draw string 'xx' 'yy' ${UNIT1}'

xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP1'


say; say 'mmmmmmm'; say 'PRINT HEADER LINES'; say 'mmmmmmm'
'set string 1 c 3'
'set strsiz 0.13 0.15'
xx=(0+8.5)/2; yy=yy+0.27
'draw string ' xx ' ' yy ' ${RCLS}N-${RCLN}N,${RCLW}E-${RCLE}E'

yy=yy+0.25
'draw string ' xx ' ' yy '${VAR1} ${C} ${R} ${IC} ${EXP1OUT} ${EXP2OUT} ${EXP3OUT}'

# Header
'set strsiz 0.1 0.12'
'set string 1 l 3'
yy=yy+0.3
'draw string 0.5 'yy' ${TIMESTAMP} ${HOST}'
yy=yy+0.2
'draw string 0.5 'yy' 10.2 ${CWD}'
yy=yy+0.2
'draw string 0.5 'yy' 10 ${CMD}'



say; say 'mmmmmmm'; say '${VAR1} ${EXP2OUT}'; say 'mmmmmmm'

'set xlab on'; 'set ylab off'

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye


'set parea 'xs ' 'xe' 'ys' 'ye

'set ylint $YLI'

'color ${LEVS2} -kind ${KIND2} -gxout shaded'
say '${VAR1OUT} ${EXP1OUT}-${EXP2OUT}'
'd AAV11-AAV12'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)
XL2=xl

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
### 'draw string 'xx' 'yy' Altitude [km]'

xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP2'

'set xlab off'; 'set ylab off'



say; say 'mmmmmmm'; say '$VAR1 ${EXP3OUT} ${LEV1}'; say 'mmmmmmm'

xs = xe+${XMARGIN1}; xe=xs+${XWID}

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlab on'; 'set ylab off'
'set ylint $YLI'

'color ${LEVS2} -kind ${KIND2} -gxout shaded'

say '${VAR1OUT} ${EXP1OUT}-${EXP3OUT}'
'd AAV11-AAV13'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=XL2; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS2} -ft 3 -line on -edge circle'

xx = (x1+x2)*0.5; yy = y1-0.3 
'set string 1 c 3 0'; 'set strsiz 0.1 0.12'
'draw string 'xx' 'yy' ${UNIT1}'

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
### 'draw string 'xx' 'yy' Altitude [km]'

xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP3'

'set xlab off'; 'set ylab off'



say; say 'MMMMMMMMMM'; say 'LOWER PANEL'; say 'MMMMMMMMMM'

'set lev ${LEV}'; 'q dims'; say sublin(result,4)

say; say 'mmmmmmm'; say '$VAR2 ${EXP1OUT} ${LEV1}'; say 'mmmmmmm'

'set xlab on'; 'set ylab on'

xs=${XLEFT};xe=xs+${XWID}; ye = ${YTOP}-${YHGT}-${YMARGIN};ys=ye-${YHGT2}

say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set parea 'xs ' 'xe' 'ys' 'ye
'set ylint $YLI'

say '${VAR2OUT}'
'color ${LEVS3} -kind ${KIND3} -gxout shaded'
'd AAV21'

'set xlab off'; 'set ylab off'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx =xl-0.5; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' Altitude [km]'


x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS3} -ft 3 -line on -edge circle'

xx = (x1+x2)*0.5; yy = y1-0.3 
'set string 1 c 3 0'; 'set strsiz 0.1 0.12'
'draw string 'xx' 'yy' ${UNIT2}'


xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP4'



say; say 'mmmmmmm'; say '$VAR2 ${EXP2OUT} ${LEV1}'; say 'mmmmmmm'

'set xlab on'; 'set ylab off'

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye


'set parea 'xs ' 'xe' 'ys' 'ye

'set ylint $YLI'

'color ${LEVS4} -kind ${KIND4} -gxout shaded'
say '${VAR2OUT} ${EXP1OUT}-${EXP2OUT}'
'd AAV21-AAV22'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)
XL2=xl

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
### 'draw string 'xx' 'yy' Altitude [km]'

xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP5'

'set xlab off'; 'set ylab off'



say; say 'mmmmmmm'; say '$VAR2 ${EXP3OUT} ${LEV1}'; say 'mmmmmmm'

xs = xe+${XMARGIN1}; xe=xs+${XWID}

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlab on'; 'set ylab off'
'set ylint $YLI'

'color ${LEVS4} -kind ${KIND4} -gxout shaded'

say '${VAR1OUT} ${EXP1OUT}-${EXP3OUT}'
'd AAV21-AAV23'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=XL2; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS4} -ft 3 -line on -edge circle'

xx = (x1+x2)*0.5; yy = y1-0.3 
'set string 1 c 3 0'; 'set strsiz 0.1 0.12'
'draw string 'xx' 'yy' ${UNIT2}'

xx =xl-0.4; yy = (yb+yt)/2 
'set string 1 c 3 90'; 'set strsiz 0.11 0.13'
### 'draw string 'xx' 'yy' Altitude [km]'

xx = xl; yy = yt+0.2
'set string 1 l 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' $CAP6'

'set xlab off'; 'set ylab off'


'gxprint $fig'

#'set sdfwrite ${NCDIR}/${NC1}'; 'sdfwrite HGT'

'quit'
EOF

grads -bc${PAPER} "${GS}"

cp -av $ctl1 .
if [ $flagd != "true" ]; then rm -vf $GS; fi

echo; echo mmmmmmm OUTPUT
#ls -lhd --time-style=long-iso $NCDIR
ls -lh --time-style=long-iso $fig
echo mmmmmmm; echo

exit 0

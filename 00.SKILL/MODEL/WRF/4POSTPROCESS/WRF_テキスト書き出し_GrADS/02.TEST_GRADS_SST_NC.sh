#!/bin/bash

EXP=$1;EXP=${EXP:-0000}
INDIR=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP1=$EXP; CTL1=$CTL

GS=$(basename $0 .sh).GS
OUT=RW3A.00.04.05.05.${EXP1}.01_SST.nc

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'
'set x 1 1469';'set y 1 1231';'set z 1';'set t 1'

'define sst = sst'
'set sdfwrite $OUT'
'sdfwrite sst'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

if [ -f $OUT ]; then echo OUT: $OUT; fi

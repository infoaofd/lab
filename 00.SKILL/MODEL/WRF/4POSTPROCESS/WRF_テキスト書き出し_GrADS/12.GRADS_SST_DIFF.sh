#!/bin/bash

EXP=0000
INDIR=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP1=$EXP; CTL1=$CTL

EXP=0802
INDIR=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP2=$EXP; CTL2=$CTL

GS=$(basename $0 .sh).GS
OUT=RW3A.00.04.05.05.${EXP1}-${EXP2}.01_DIFF_SST.nc

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'
'set x 1 1469';'set y 1 1231';'set z 1';'set t 1'
'diff=sst.1-sst.2'

'define diff = diff'
'set sdfwrite $OUT'
'sdfwrite diff'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

if [ -f $OUT ]; then echo OUT: $OUT; fi

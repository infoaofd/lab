# /work05/manda/WRF.POST/K17/TRAJ/TRAJ.D03

#RUNNAME=K17.R27.00.00
RUNNAME=K17.R27.06.03
#RUNNAME=K17.R27.06.05
TRAJRN=T00.09

INDIR=WRF.TRAJ/WRF.TRAJ_${RUNNAME}_${TRAJRN}

INDIR2=/work06/manda/ARWpost_K17_work06/ARWpost_${RUNNAME}.10m
CTL=$INDIR2/${RUNNAME}.10m.d03.basic_p.10m.ctl
CTL2=$INDIR2/${RUNNAME}.10m.d03.traj.10m.ctl


#LON1=130.27
#LON2=130.7
#LAT1=33.65
#LAT2=33.40
LON1=130.25
LON2=130.7
LAT1=33.25
LAT2=33.45

LAT=33.3
DATE1=05Z05JUL2017
DATE2=06Z05JUL2017

DATE0=01Z05JUL2017
DATE1=00Z05JUL2017
DATE2=13Z05JUL2017

LEVWIND=0.5

BOXLONW=130.5
BOXLONE=130.6
BOXLATS=33.30
BOXLATN=33.40

FIGDIR=TRAJ_FIG #$(basename $0 .sh)_FIG
mkdir -vp $FIGDIR
FIG=${RUNNAME}_${TRAJRN}_$(basename $0 .sh).eps

rm -f $FIGDIR/$FIG

HOST=$(hostname)
CWD=$(pwd)
TIMESTAMP=$(date -R)
CMD="$0 $@"
GS=$(basename $0 .sh).GS

KIND1='darkblue->deepskyblue->skyblue->gold->orange->crimson'
LEVS1="300 310 1"

cat<<EOF>$GS

say '1 OPEN $CTL'
'open $CTL'
say
say '2 OPEN $CTL2'
'open $CTL2'
say


'cc'
'set grads off'



xmax = 1
ymax = 2
xwid = 5 ;#7/xmax
ywid = 3 ;#8/ymax


say 
say 'PANEL 1 UP'
say 

'set dfile 2'

lon1 = ${LON1} ;#130
lon2 = ${LON2} ;#131
lat1 = ${LAT1} ;#33.2
lat2 = ${LAT2} ;#33.5
lon = lon1

xmap = 1
ymap = 1

xs = 1 + (xwid+0.25)*(xmap-1)
xe = xs + xwid
ye = 9.0 - (ywid+0.25)*(ymap-1)
ys = ye - ywid

'set time ${DATE1}'
'set lat 'lat1
'set lon 'lon1' 'lon2

'set lev 0 2'

'd theta*(1+0.61*QVAPOR)'
say
say 'TEMPORARY SCALING ENVIRONMENT'
say

'collect 1 free'
'collect 2 free'
'set x 1'
'set y 1'

say
say 'COLLECTING STATION DATA'
say

'q gxinfo'
xline=sublin(result,3)
yline=sublin(result,4)
xs=subwrd(xline,4)' 'subwrd(xline,6)
ys=subwrd(yline,4)' 'subwrd(yline,6)

while (lon <= lon2)
  'set lev 0 2'
  lat = lat1 + (lat2-lat1)*(lon-lon1) / (lon2-lon1)
  'collect 1 gr2stn(theta*(1+0.61*QVAPOR),'lon','lat')'

#  'set gxout print'
#  'set lat 'lat
#  'set lon 'lon
#  'set z 1'
#  'd HGT.2/1000'


#   press=sublin(result,3);press=subwrd(press,1)
#   'q w2xy 'lon' 'press
#   x=subwrd(result,3)
#   y=subwrd(result,6)
#   if(x<=subwrd(xs,1));x=subwrd(xs,1);endif
#   if(x>=subwrd(xs,2));x=subwrd(xs,2);endif
#   if(y<=subwrd(ys,1));y=subwrd(ys,1);endif
#   if(y>=subwrd(ys,2));y=subwrd(ys,2);endif

#   terrain_arr=terrain_arr''x' 'y' '
#   terrain_arry=terrain_arry''x' 'subwrd(ys,1)' 'x' 'y


  lon = lon + 0.02
endwhile

'set xlab on'
'set ylab on'


xs = 1 + (xwid+0.25)*(xmap-1)
xe = xs + xwid
ye = 9.0 - (ywid+0.25)*(ymap-1)
ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 3 0.12'
#else
#'set xlopts 1 3 0'
#endif
#if (xmap = 1)
'set ylopts 1 3 0.12'
#else
#'set ylopts 1 3 0.12'
#endif
*
'set parea 'xs' 'xe' 'ys' 'ye

say 'CLEAR BEFORE PLOTTING DATA. '

'clear'
'set grads off'

'set lev 0 2'
'set lon 'lon1' 'lon2

'set xlint 0.2'
'set ylint 0.5'


say
say 'FILL MISSING VALUE'
say
'set gxout grfill'
'set clevs 0.9e30 1.1e30'
'set rgb 99 40 20 0'
'set ccols 0 99 0'
'd const(coll2gr(1,-u), 1.e30, -u)'


'set xlab off'
'set ylab off'

say
say 'DRAWING CROSS SECTION'
say
'set gxout contour'
'set ccolor 1'
'set cthick 1'
'set cint 1'
'set clab on'
'set clskip 2'
'set warn off'
'd coll2gr(1,-u)'



nmax = 50
*
Mon.1='Jan';Mon.2='Feb';Mon.3='Mar';Mon.4='Apr'
Mon.5='May';Mon.6='Jun';Mon.7='Jul';Mon.8='Aug'
Mon.9='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

num=1
while (num <= nmax)

numc = num
if (num < 100); numc = '0'num; endif
if (num <  10); numc = '00'num; endif

  ifile = '${INDIR}/${RUNNAME}_${TRAJRN}_'numc'.txt'

  'run DRAW.TRAJ.CSC.UP.GS 'ifile
  num = num + 1
endwhile




'q gxinfo'
#say result
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.12 0.14'
'set string 1 c 3 90'
xx=xl-0.5
yy=(yt+yb)/2
'draw string ' xx ' ' yy ' Altitude [km]'

'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx=xl
yy=yt+0.18
'draw string ' xx ' ' yy ' ${DATE1} (${LAT1}N,${LON1}E)-(${LAT2}N,${LON2}E)'

yy=yy+0.2
'draw string ' xx ' ' yy ' ${RUNNAME} ${TRAJRN}'


'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2

yy=yy+0.4
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'




say
say 'PANEL 2 MAP'
say
'set dfile 2'


xmap = 1
ymap = 2
xs = 1 + (xwid+0.25)*(xmap-1)
xe = xs + xwid
ye = 9 - (ywid+1.5)*(ymap-1)
ys = ye - ywid



'set parea 'xs' 'xe' 'ys' 'ye

'set grads off'

'set xlint 0.5'
'set ylint 0.5'
*
'set datawarn off'

'set xlab on'
'set ylab on'

say
say 'HGT'
say
'set dfile 2'
'set gxout contour'

'set lon 129.19 132'
'set lat  32.5 34'
'set z 1'


#if (ymap = ymax)
'set xlopts 1 3 0.12'
#else
#'set xlopts 1 3 0'
#endif
#if (xmap = 1)
'set ylopts 1 3 0.12'
#else
#'set ylopts 1 3 0.12'
#endif

'set ccolor 16'
'set cthick 3'
'set clevs 250 500 1000'
'd HGT'

'set xlab off'
'set ylab off'



INDIR='${INDIR}'
FIGDIR='${FIGDIR}'

nmax=50
num=1
while (num <= nmax)

numc = num
if (num < 100); numc = '0'num; endif
if (num <  10); numc = '00'num; endif

ifile = '${INDIR}/${RUNNAME}_${TRAJRN}_'numc'.txt'

#say
#say ifile
#say 
  'run DRAW.TRAJ.GS 'ifile
  num = num + 1
endwhile


'set mpdraw on'
'set mpdset hires'
'set ccolor  1'
'set line 1 1 1'
'set map 1 1 1'
'draw shp JPN_adm1'

say
say 'RAIN'
say
'set dfile 1'
'set ccolor 9'
'set clevs 50 100 500'
'set z 1'
'define RAC=RAINC(time=${DATE2})+RAINNC(time=${DATE2})-(RAINC(time=${DATE0})+RAINNC(time=${DATE0}))'
'd RAC'



say
say 'LINE'
say
'trackplot 'lon1' 'lat1' 'lon2' 'lat' -c 1 -l 1 -t 6'



say
say 'WIND'
say
'set dfile 2'
'set lev ${LEVWIND}'
'set gxout vector'
'set cthick 10'
'set ccolor  0'
'vec.gs skip(u,30);v -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

xx=xr-0.65
yy=yb-0.35
'set cthick  2'
'set ccolor  1'
'set line 1 1 3'
'vec.gs skip(u,30);v -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'



say
say 'BOX'
say
'trackplot ${BOXLONW} ${BOXLATS} ${BOXLONE} ${BOXLATS} -c 1 -l 1 -t 1'
'trackplot ${BOXLONE} ${BOXLATS} ${BOXLONE} ${BOXLATN} -c 1 -l 1 -t 1'
'trackplot ${BOXLONE} ${BOXLATN} ${BOXLONW} ${BOXLATN} -c 1 -l 1 -t 1'
'trackplot ${BOXLONW} ${BOXLATN} ${BOXLONW} ${BOXLATS} -c 1 -l 1 -t 1'

'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx=xl
yy=yt+0.18
'draw string ' xx ' ' yy ' WIND (${LEVWIND}km ${DATE1})'
yy=yy+0.22
'draw string ' xx ' ' yy ' RAIN (${DATE0}-${DATE2})'
yy=yy+0.22
'draw string ' xx ' ' yy ' RECT (${BOXLONW}, ${BOXLATS}) (${BOXLONE}, ${BOXLATN})'



'gxprint ${FIGDIR}/${FIG}'
'!ls --time-style=long-iso -lh $FIGDIR/$FIG'



'quit'
EOF

grads -bcp "$GS"

rm -vf $GS

echo |tee -a $LOG
du -sch ${INDIR} 2>&1|tee -a $LOG
ls -t --time-style=long-iso -lh ${INDIR} 2>&1| head -2 |tee -a $LOG

echo |tee -a $LOG
ls -t --time-style=long-iso -lh ${FIGDIR}/${FIG} 2>&1| head -2 |tee -a $LOG

echo
echo $LOG
echo

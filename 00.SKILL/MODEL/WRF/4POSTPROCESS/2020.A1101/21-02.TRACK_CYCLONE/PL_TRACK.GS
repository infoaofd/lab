* /work05/manda/WRF.POST/A1101/GRADS/02.R03/21-02.TRACK_CYCLONE
*
* Script to draw a cyclone-track plot.
* Also assumes that a file has been opened (any file, doesn't matter);
* the set command doesn't work until a file has been opened.

function pstf (args)
fname=subwrd(args,1)
lcolor=subwrd(args,2)


marktype = 3
#marksize = 0.09
marksize = 0.03
#lcolor   = 2
lstyle   = 3
lthick   = 1
start    = 1
jump     = 1
deltax   = 0.1
deltay   = 0.1

Mon.01='Jan';Mon.02='Feb';Mon.03='Mar';Mon.04='Apr'
Mon.05='May';Mon.06='Jun';Mon.07='Jul';Mon.08='Aug'
Mon.09='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

* Read all data points
while (1)
  ret = read(fname)
  rc  = sublin(ret,1)

  if (rc > 0)
    if (rc = 2)
#      say 'EOF : Finished reading 'filename
      break
    else
      say 'Error : while reading  'filname
      res = close(filename)
      break
    endif
  endif

  ii=0
  while (1)
    ii=ii+1
    loc    = sublin(ret,2)
    lon.ii = subwrd(loc,1)
    lat.ii = subwrd(loc,2)
    SLP.ii = subwrd(loc,3)
    dd.ii  = subwrd(loc,8)
    hh.ii  = subwrd(loc,9)
    DIS.ii = subwrd(loc,11)

    ret      = read(fname)
    rc       = sublin(ret,1)
    loc      = sublin(ret,2)
    flag     = subwrd(loc,1)
    if (flag='' | rc!=0)
      break
    endif
  endwhile

endrec = 30 ;#ii

say ' end_record = ' endrec

  'query w2xy 'lon.start' 'lat.start
  xprev = subwrd(result,3)
  yprev = subwrd(result,6)
#  if (hgt.endrec <= 500) 
#    lcolor = 2
#  else
#    if (hgt.endrec > 1500) 
#      lcolor = 4
#    else
#      lcolor = 3
#    endif
#  endif

  'set line 'lcolor' 'lstyle' 'lthick

#  'draw mark 'marktype' 'xprev' 'yprev' 'marksize
* 'set strsiz 0.1'
* 'draw string 'xnext+deltax' 'ynext' 'hour.next'Z'day.next''



  next = start+jump

  while (next <= endrec)
    'query w2xy 'lon.next' 'lat.next
    xnext = subwrd(result,3)
    ynext = subwrd(result,6)

#    if ((dd.next < 23) | (dd.next = 24 & hh.next < 18))
#say dd.next' 'hh.next

  'set line 0 1 2'
    'draw line 'xprev' 'yprev' 'xnext' 'ynext

  'set line 'lcolor' 'lstyle' 'lthick
    'draw line 'xprev' 'yprev' 'xnext' 'ynext
#    endif

    next = next+jump
    xprev = xnext
    yprev = ynext
  endwhile
#  'draw mark 'marktype' 'xnext' 'ynext' 'marksize

endwhile
return

!2020-10-06_09-59 
!manda@calypso
!/work05/manda/WRF.POST/A1101/GRADS/02.R03/01-03.EAVE_Z

PROGRAM AAVE_EAVZ_Z

USE MOD_VAR
USE MOD_AVE
USE MOD_IO
USE MOD_OUT
USE MOD_AAVE
USE MOD_DECOUPLE

character::IDROOT*200,R*100,R1*200,R2*200
CHARACTER(2)::E1
integer IS,IE
CHARACTER II*2
character(len=200)::OCTL1,ODIR,OUT_PREFIX
character(len=500)::OFLE
CHARACTER TIMEAVES*12, TIMEAVEE*12, POSTFIX*50

character(len=200)::INDIR1,CTL1,INDIR2
character(len=200)::DSET, OPTIONS, TITLE,  XDEF, YDEF, ZDEF, TDEF
character(len=200)::XDEF_OUT,YDEF_OUT,TDEF_OUT
character PDEF*200

character(len=200)::INFLE

INTEGER,PARAMETER::KMAX=300
REAL p(KMAX),DT


INTEGER IM,JM,KM

CHARACTER TIME0*12, MMM0*3

INTEGER YYYY0,MM0,DD0,HH0, YYYY,MM,DD,HH

namelist /para/IDROOT,R,E1,IS,IE,OCTL1,ODIR,OUT_PREFIX,TIMEAVES,TIMEAVEE,&
POSTFIX, LONC, LATC, DXI, DYJ

read(5,nml=para)

R1=trim(R)//'.'//trim(E1)

I=1
WRITE(II,'(i2.2)')I
INDIR1=trim(IDROOT)//'ARWpost_'//trim(R1)//'.'//trim(II)

CTL1=trim(R1)//'.'//trim(II)//'.'//trim(POSTFIX)

print *,'READ_CTL'
CALL READ_CTL(INDIR1,CTL1,&
DSET, OPTIONS, UNDEF, IM, JM, XDEF, YDEF, ZDEF, KM, p, KMAX, TIME0, &
MM0, DT, NM, PDEF, TDEF)

read(TIME0(9:),*)YYYY0
read(TIME0(4:5),*) DD0
read(TIME0(1:2),*) HH0
print '(A,A)','TIME0=',TIME0


J0=JULDAY(YYYY0,MM0,DD0)
print *,'YYYY0,MM0,DD0,J0=',YYYY0,MM0,DD0,J0
print *

NH=24/int(DT)
ND=NM/int(NH)+1

print *,'NM=',NM
print *,'ND=',ND,' NH=',NH,' ND*NH=',ND*NH
print *

NE=(IE-IS+1)
print *,'NE=',NE

ND=5



CALL ALLOC(IM,JM,KM,NE)



print '(A)','MAKE_CTL'
write(CHARLONC,'(f7.2)')LONC
write(CHARLATC,'(f7.2)')LATC

TITLE='TITLE LONC='//CHARLONC//'_LATC='//CHARLATC
XDEF_OUT='XDEF 1 LEVELS '//trim(CHARLONC)
YDEF_OUT='YDEF 1 LEVELS '//trim(CHARLATC)
CALL MAKE_CTL&
(OCTL1, ODIR, OUT_PREFIX, R1, OPTIONS, UNDEF, TITLE, PDEF, XDEF_OUT, YDEF_OUT, ZDEF,&
P, KM, KMAX, TDEF)



IENS=1
CALL SET_AAVE_GRID_INDEX(IDROOT, R1, IENS, POSTFIX)



NT=0
DAY_LOOP: DO N=0,ND-1

JD=J0+N
CALL caldat(JD,YYYY,MM,DD)

if (N < ND-1)then
LAST_L=NH-1
else
LAST_L=0
endif

HOUR_LOOP: DO L=0,LAST_L

HH=int(DT)*L

NT=NT+1



CALL INIT_EAVE_VAR

I1=0
ENS_LOOP: DO IENS=IS,IE

I1=I1+1

print '(A,i3,1x,i3,1x,i4,1x,i2.2,1x,i2.2,1x,i2.2)',trim(R1),IENS,NT,YYYY,MM,DD,HH


WRITE(II,'(i2.2)')IENS
INDIR1=trim(IDROOT)//'ARWpost_'//trim(R1)//'.'//trim(II)
CTL1=trim(R1)//'.'//trim(II)//'.'//trim(POSTFIX)

CALL READ_CTL(INDIR1,CTL1,&
DSET, OPTIONS, UNDEF, IM, JM, XDEF, YDEF, ZDEF, KM, p, KMAX, TIME0, &
MM0, DT, NM, PDEF, TDEF)

CALL SET_INFLE(INFLE,INDIR1,DSET,YYYY,MM,DD,HH)

IU=10
open(IU,file=trim(infle),form='unformatted',action="read",&
         access='direct',recl=IM*JM*4,IOSTAT=IERR, ERR=100)

CALL READ_ARWPOST(IU, IM, JM, KM, UNDEF)
close(IU)


CALL MU_TOTAL(IM,JM,UNDEF)

CALL DECOUPLE(IM,JM,KM,UNDEF)


CALL INIT_AAVE_VAR

CALL AAVE(IM,JM,KM,UNDEF)

CALL ADD_VARS_E(IM,JM,KM,UNDEF)

CALL STORE(IM,JM,KM,I1,UNDEF)


END DO ENS_LOOP



CALL EAVE_CALC(IM,JM,KM,NE,UNDEF)

CALL STDEV(IM,JM,KM,NE,UNDEF)



CALL SET_OFLE(OFLE,ODIR,OUT_PREFIX,R1,YYYY,MM,DD,HH)

IU=20
print *
print '(A,i3,A)','IU=',IU,' OUT:'
print '(A)',trim(OFLE)

open(IU,file=trim(OFLE),form='unformatted',&
         access='direct',recl=4)

CALL OUT_EAVE(IU, IM, JM, KM)


print *,'NE=',NE
!print *,'THEA=     ',THEA(5)
!print *,'THSD=     ',THSD(5)
!print *,'THVR=     ',THVR(5)
print '(5(A,G10.3,1X))','TNEA=',TNEA(5),'SD=',TNSD(5)
print '(5(A,G10.3,1X))','ADEA=',ADEA(5),'SD=',ADSD(5)
print '(5(A,G10.3,1X))','DIEA=',DIEA(5),'SD=',DISD(5)
print '(5(A,G10.3,1X))','CUEA=',CUEA(5),'SD=',CUSD(5)
print '(5(A,G10.3,1X))','RAEA=',RAEA(5),'SD=',RASD(5)
print '(5(A,G10.3,1X))','BLEA=',BLEA(5),'SD=',BLSD(5)
print '(5(A,G10.3,1X))','RSEA=',RSEA(5),'SD=',RSSD(5)



print *; print *; print *

END DO HOUR_LOOP

END DO DAY_LOOP


print *
print *,'NORMAL END'
print *
print *,'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv'
print *,'CONTROL FILE FOR OUTPUT'
print *,trim(OCTL1)
print *,'vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv'
print *
stop

100 CONTINUE
print '(A)','ERROR : CANNOT OPEN:'
print '(A)',trim(infle)
stop

END PROGRAM AAVE_EAVZ_Z

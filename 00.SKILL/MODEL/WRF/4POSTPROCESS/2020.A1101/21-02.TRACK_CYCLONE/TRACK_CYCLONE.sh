#!/bin/bash

#2020-10-01_09-46 
#manda@calypso
#/work05/manda/WRF.POST/A1101/GRADS/02.R03/02.TAVE.EAVE.P

R=A1101.R03

E1=$1
E1=${E1:=00}

LONC=$2
LATC=$3

LONC=${LONC:--17.5}
LATC=${LATC:-70.3}

echo ${R} ${E1}
echo $LONC $LATC

D0=$(date -R)

ODIR="$(pwd)/OUT_$(basename $0 .sh)/${R}.${E1}/"
#CDIR="CTL_$(basename $0 .sh)"
#mkdir -vp $ODIR $CDIR


nml=$(basename $0 .sh)_${E1}.nml
cat<<EOF>$nml
&para
IDROOT="/work04/manda/ARWpost_A1101_work04/"
R="$R"
E1="${E1}"
IS=1
IE=23
OCTL1="${CDIR}/$(basename $0 .sh)_${R}.${E1}.CTL"
ODIR="$ODIR"
OUT_PREFIX="$(basename $0 .sh)"
POSTFIX="d01.basic_z.03H.ctl",
ICC=85
JCC=61
&end
EOF
mkdir -vp $ODIR

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo



src=$(basename $0 .sh).F90
#src=EAVE_T_VPT_MIXR.F90
SUB="MOD_VAR.F90 MOD_IO.F90 MOD_CENTER.F90 MOD_OUT.F90 \
CALENDAR.F90"

#" MOD_AVE.F90 MOD_OUT.F90 MOD_AAVE.F90 \
#MOD_DECOUPLE.F90 \

echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src} ${SUB}
echo


exe=$(basename $0 .sh).exe

f90=ifort
OPT=" -O3 -fpp -convert big_endian -assume byterecl"
DOPT= #" -fpp -CB -traceback -fpe0 -check all " #-warn all "

rm -f $exe

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

D1=$(date -R)
echo
echo ${exe} is running ...
echo
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
rm -fv $exe
exit 1
fi

rm -fv $exe $nml

D2=$(date -R)

echo 
echo $0 $@
echo
echo "START: $D0"
echo "     : $D1"
echo "END  : $D2"
echo
echo "Done ${exe}"
echo

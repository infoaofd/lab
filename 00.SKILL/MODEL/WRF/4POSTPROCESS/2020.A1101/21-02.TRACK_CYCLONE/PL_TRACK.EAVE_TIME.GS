* /work05/manda/WRF.POST/A1101/GRADS/02.R03/21-02.TRACK_CYCLONE
*
* Script to draw a cyclone-track plot.
* Also assumes that a file has been opened (any file, doesn't matter);
* the set command doesn't work until a file has been opened.

function pstf (args)
fname=subwrd(args,1)
lcolor=subwrd(args,2)


marktype = 3
marksize = 0.08
#lcolor   = 2
lstyle   = 1
lthick   = 5
start    = 1
jump     = 1
deltax   = 0.1
deltay   = 0.1

Mon.01='Jan';Mon.02='Feb';Mon.03='Mar';Mon.04='Apr'
Mon.05='May';Mon.06='Jun';Mon.07='Jul';Mon.08='Aug'
Mon.09='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

* Read all data points
while (1)
  ret = read(fname)
  rc  = sublin(ret,1)

  if (rc > 0)
    if (rc = 2)
#      say 'EOF : Finished reading 'filename
      break
    else
      say 'Error : while reading  'filname
      res = close(filename)
      break
    endif
  endif

  ii=0
  while (1)
    ii=ii+1
    loc    = sublin(ret,2)
    lon.ii = subwrd(loc,1)
    lat.ii = subwrd(loc,2)
    SLP.ii = subwrd(loc,3)
    dd.ii  = subwrd(loc,7)
    hh.ii  = subwrd(loc,8)
#    DIS.ii = subwrd(loc,11)

    ret      = read(fname)
    rc       = sublin(ret,1)
    loc      = sublin(ret,2)
    flag     = subwrd(loc,1)

  if(ii > 30)
   break
  endif

  'query w2xy 'lon.ii' 'lat.ii
  x = subwrd(result,3)
  y = subwrd(result,6)

  say dd.ii' 'hh.ii
  'set line 'lcolor
  if(hh.ii = 0)
  'draw mark 'marktype' 'x' 'y' 'marksize
  if(lcolor = 4)

  'set string 0 c 10 0'
  'draw string 'x+0.15' 'y+0.15' 'dd.ii
  'set string 1 c 5 0'
  'draw string 'x+0.15' 'y+0.15' 'dd.ii
  endif
  endif

    if (flag='' | rc!=0)
      break
    endif
  endwhile

endwhile

return

# /work05/manda/WRF.POST/K17/TRAJ/TRAJ.D03
#
# /work05/manda/WRF.POST/A1101/GRADS/02.R03/21-02.TRACK_CYCLONE

R=A1101.R03
R0=${R}.00
R1=${R}.01

INDIR=OUT_TRACK_CYCLONE

DIR=/work04/manda/ARWpost_A1101_work04
CTL1=${DIR}/ARWpost_${R0}.01/A1101.R03.00.01.d01.basic_p.03H.ctl
CTL2=${DIR}/ARWpost_${R1}.01/A1101.R03.01.01.d01.basic_p.03H.ctl
EAVE_TRACK1=../21-03.EAVE_CENTRAL_PRES/OUT_EAVE_CENTRAL_PRES/EAVE_CENTRAL_PRES_A1101.R03.00_CLOC.txt
EAVE_TRACK2=../21-03.EAVE_CENTRAL_PRES/OUT_EAVE_CENTRAL_PRES/EAVE_CENTRAL_PRES_A1101.R03.01_CLOC.txt

#####################
ATIME1="00Z21JAN2011"; ATIME2="00Z24JAN2011"; ATIME3="00Z25JAN2011"
NE=23
#####################

PANEL="(a)"

LEVS=" -2 10 1"
#LEVS=" -levs -0.8 -0.6 -0.4 -0.2 -0.1 0.1 0.2 0.4 0.6 0.8"
#KIND="cyan->blue->palegreen->white->gold->red->magenta"
KIND='cornflowerblue->oldlace->orange->tomato->red'
FS=1
UNIT="[\`ao\`nC]"

LEVS2=" 0 3.2 0.4 "
#KIND2='darkgreen->limegreen->lightgreen->palegreen->khaki->darkgoldenrod->saddlebrown->maroon'
KIND2='white->lightgray->gray->dimgray->black'
FS2=1
UNIT2="[km]"


LIST="$CTL1 $CTL2"
for CTL in $LIST; do
if [ ! -f $CTL ]; then
echo NO SUCH FILE, $CTL ; echo TERMINATED.
exit 1
fi
done #CTL

GS=$(basename $0 .sh).GS

FDIR=FIG_$(basename $0 .sh); mkdir -vp $FDIR
FIG=${FDIR}/$(basename $0 .sh)_${R0}-${R1}.eps

HOST=$(hostname)
CWD=$(pwd)
NOW=$(date -R)
CMD="$0 $@"

cat<<EOF>$GS

'open $CTL1'
'open $CTL2'

xmax = 1
ymax = 1

ytop=9

xwid = 5.0/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set map 1 1 1'
'set grid on 3 15 1'

'set mproj nps'
'set frame on'
#'set lon -50 90'
#'set lat 60 90'
'set mpvals -12 54 66 85.5'
'set mpdset hires'
'set time ${ATIME2}'

# SET COLOR BAR
'color ${LEVS2} -kind ${KIND2} -gxout shaded'

'd maskout(HGT.1/1000, HGT.1-10)'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.8
y1=yb-0.2; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.11 -fs $FS -ft 3 -line on -edge circle'

'set strsiz 0.12 0.15'
'set string 1 r 2'
xx=xr-0.05
yy=y1+0.05
'draw string 'xx' 'yy' $UNIT2'



# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'd maskout(SST.1-273.15,xland.1-1.5)'

# LEGEND COLOR BAR
x1=xl; x2=xr-0.8
y1=yb-0.7; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.11 -fs $FS -ft 3 -line on -edge circle'

'set strsiz 0.12 0.15'
'set string 1 r 2'
xx=xr-0.05
yy=y1+0.05
'draw string 'xx' 'yy' $UNIT'

'set gxout contour'
'set time ${ATIME2}'
'set ccolor 0'
'set cthick 10'
'set cstyle 1'
'set clevs 0'
'set clab off'
'd SEAICE.1'

'set ccolor 2'
'set cthick 5'
'set cstyle 3'
'set clevs 0'
'set clab off'
'd SEAICE.1'

'set gxout contour'
'set time ${ATIME2}'
'set ccolor 0'
'set cthick 10'
'set cstyle 1'
'set clevs 0'
'set clab off'
'd SEAICE.2'

'set ccolor 4'
'set cthick 5'
'set cstyle 3'
'set clevs 0'
'set clab off'
'd SEAICE.2'



nmax=23
num=1
while (num <= nmax)

numc = num
#if (num < 100); numc = '0'num; endif
if (num <  10); numc = '0'num; endif

ifile1 = '${INDIR}/${R0}/TRACK_CYCLONE_${R0}.'numc'.dat'
ifile2 = '${INDIR}/${R1}/TRACK_CYCLONE_${R1}.'numc'.dat'

say
say ifile1
say ifile2
say 
'run PL_TRACK.GS 'ifile1' 2'
'run PL_TRACK.GS 'ifile2' 4'

  num = num + 1
endwhile


say '${EAVE_TRACK1}'
#'!ls -lh ${EAVE_TRACK1}'
#say '${EAVE_TRACK2}'
'!ls -lh ${EAVE_TRACK2}'

'run PL_TRACK.EAVE.GS ${EAVE_TRACK1} 2'
'run PL_TRACK.EAVE.GS ${EAVE_TRACK2} 4'

'run PL_TRACK.EAVE_TIME.GS ${EAVE_TRACK1} 2'
'run PL_TRACK.EAVE_TIME.GS ${EAVE_TRACK2} 4'

# TEXT
'set strsiz 0.08 0.1'
'set string 1 c 3 0'
# http://www.atmos.rcast.u-tokyo.ac.jp/okajima/topics/grads/grads.html

'LONLAT'


x=xl
'set strsiz 0.15 0.2'
'set string 1 l 3 0'
y=yt+0.25
'draw string 'x' 'y' ${PANEL}'

x=(xl+xr+0.2)/2
'set strsiz 0.1 0.13'
'set string 1 c 2 0'

y=yt+0.2
'draw string 'x' 'y' ${ATIME2}'

y=y+0.4
'draw string 'x' 'y' ${R0} ${R1}'



# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=y+0.6
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'



'gxprint ${FIG}'
'!ls --time-style=long-iso -lh $FIG'

'quit'
EOF

D1=$(date -R)
grads -bcp "$GS"
D2=$(date -R)

rm -vf $GS

#echo |tee -a $LOG
#du -sch ${INDIR} 2>&1|tee -a $LOG
#ls -t --time-style=long-iso -lh ${INDIR} 2>&1| head -2 |tee -a $LOG

echo |tee -a $LOG
ls -t --time-style=long-iso -lh ${FIG} #2>&1| head -2 |tee -a $LOG

echo
echo $LOG
echo
echo "DONE $0."
echo "START: $D1"
echo "END:   $D2"
echo
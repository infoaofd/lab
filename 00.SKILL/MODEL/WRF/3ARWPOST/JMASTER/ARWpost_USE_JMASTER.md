# ARWpost JMASTER

## 実行時間の短縮

**jmaster**というコマンドを使い，処理する時間帯を分割して，ARWpost.exeを実行する。

### 1 $PATH/J??というディレクトリにJOB.shというファイルを作成する。

$PATHは**フルパス**にする。相対パスは使わない。フルパスで指定するので，ディレクトリが正確かどうか複数回確認する。これを怠ったため，何度も異なるディレクトリのスクリプトを実行するというミスをしたことがある。

```bash
$ cat J01/JOB.sh
cd $PATH/J01
export OMP_NUM_THREADS=1
mpirun -np 1 ./ARWpost.exe
exit
```

```
$ cat J02/JOB.sh
cd $PATH/J02
export OMP_NUM_THREADS=1
mpirun -np 1 ./ARWpost.exe
exit
```

#### まとめて置換する

例：0802.03を0000.03に置換する

事前の確認

```
$ find . -name JOB.sh|xargs grep 0802.03
```

置換

```
$ find . -name JOB.sh|xargs sed -i "s/0802.03/0000.03/g"
```

結果の確認

```
$ find . -name JOB.sh|xargs grep 0000.03
$ find . -name JOB.sh|xargs grep 0802.03
```



### 2. ARWpost.namelistファイルの変更

#### 処理する時間帯の変更

```bash
$ more J??/namelist.ARWpost |grep  -e start_date -e end_date -e namelist.ARWpost
J01/namelist.ARWpost
 start_date = "2021-08-10_12:00:00",
 end_date   = "2021-08-11_00:00:00",
J02/namelist.ARWpost
 start_date = "2021-08-11_00:10:00",
 end_date   = "2021-08-11_12:00:00",
```

#### 入出力ディレクトリ名の変更

#####  まとめてスクリプトを書き変える方法

**置換の前に確認**

**書式**

```bash
find . -name FILE_NAME |xargs grep OLD
```

FILE_NAME : ワイルドカード (*や?)を使って複数ファイルを指定することができる

OLD: 置換前の文字列

NEW: 置換後の文字列

**例**

```
find . -name namelist.ARWpost |xargs grep 0802.01
```

**置換を行う**

**書式**

```BASH
find . -name  FILE_NAME | xargs sed -i "s/OLD/NEW/g"
```

**例**

```basic
find . -name namelist.ARWpost|xargs sed -i "s/0802.01/0802.03/g"
```

**置換後の確認**

**書式**

```bash
# 変更がうまくいっているか
find . -name  FILE_NAME |xargs grep NEW
# 変更前の内容がなくなっているか
find . -name  FILE_NAME |xargs grep OLD

```

**例**

```bash
$ find . -name JOB.sh|xargs grep 0802.03

```

**確認法**

```
find . -name namelist.ARWpost|xargs grep 0802.03
```

```
$ more J??/namelist.ARWpost |grep -e ::: -e start_date -e end_date -A1
```

```
$ more J??/namelist.ARWpost | grep -e :::: -e input_root -e output_root -A1
```



### 3.ジョブ投入用のスクリプトを作る

```bash
$ cat JOBLIST.sh 
sh $PATH/J01/JOB.sh
sh $PATH/J02/JOB.sh
```

#### まとめてスクリプトを書き変える方法

**置換の前に確認**

**書式**

```bash
find . -name FILE_NAME |xargs grep OLD
```

FILE_NAME : ワイルドカード (*や?)を使って複数ファイルを指定することができる

OLD: 置換前の文字列

NEW: 置換後の文字列

**例**

```
find . -name JOB.sh|xargs grep 0802.01
```

**置換を行う**

**書式**

```BASH
find . -name  FILE_NAME | xargs sed -i "s/OLD/NEW/g"
```

**例**

```basic
find . -name JOB.sh|xargs sed -i "s/0802.01/0802.03/g"
```

**置換後の確認**

**書式**

```bash
# 変更がうまくいっているか
find . -name  FILE_NAME |xargs grep NEW
# 変更前の内容がなくなっているか
find . -name  FILE_NAME |xargs grep OLD

```

**例**

```bash
$ find . -name JOB.sh|xargs grep 0802.03
```

### 4.確認

**JOBLIST.sh**: JOB.shのディレクトリ名（フルパス）

```
$ cat JOBLIST.sh
```

**J??/JOB.sh**: J??JOB.shのディレクトリ名（フルパス）

```
$ find . -name JOB.sh|xargs grep 0802.03
```

**J??/JOB.sh**: ARWpost.exeの場所

```
$ more  J??/JOB.sh |grep -e :::: -e ARWpost.exe -A1
```

**J??**: psadilookup.datがJ??/srcに存在するか (psadilookup.datは湿潤断熱線のデータでCAPEを計算するときに必要)

```
$ ls J??/src/*adi*
J01/src/psadilookup.dat  
```

**J??/namelist.ARWpost**: 入出力のディレクトリ名

```
$ more J??/namelist.ARWpost | grep -e :::: -e input_root -e output_root -A1
```

**J??/namelist.ARWpost**: 処理する時間帯

```
$ more J??/namelist.ARWpost | grep -e :::: -e start_date -e end_date -A
```



### 5.ジョブ投入

```bash
jmaster -q C064 -P ${ユーザー名} -np {プロセス数} -e log.err -o log.out ./JOBLIST.sh
```

プロセス数は，**スクリプト数+1**とする。スクリプト(J??/JOB.sh)が2のとき，プロセス数は3とする。

１ノード24コアのマシンの場合，最大分割数は23となる。

**例**

```bash
# $ cat 0.RUN_JOBLIST.sh.TXT 
# jmaster -q CS64 -P zamanda -np 9 -e log.err -o log.out ./JOBLIST.sh

$ jmaster -q CS64 -P zamanda -np 9 -e log.err -o log.out ./JOBLIST.sh
```





## ログ

```bash
RW3A.00.04.05.05が元
RW3A.00.04.05.01が先
の場合

確認
find 変更するディレクトリ -name 対象ファイル | xargs grep 変更したいワード
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOB.sh |xargs grep 00.04.05.05
find RW3A.00.04.05.01.*.d01.traj.10MN -name namelist.ARWpost |xargs grep 00.04.05.05
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOBLIST.sh |xargs grep 00.04.05.05

置換
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOB.sh|xargs sed -i "s/00.04.05.05/00.04.05.01/g"
find RW3A.00.04.05.01.*.d01.traj.10MN -name namelist.ARWpost|xargs sed -i "s/00.04.05.05/00.04.05.01/g"
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOBLIST.sh |xargs sed -i "s/00.04.05.05/00.04.05.01/g"

確認
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOB.sh |xargs grep 00.04.05.01
find RW3A.00.04.05.01.*.d01.traj.10MN -name namelist.ARWpost |xargs grep 00.04.05.01
find RW3A.00.04.05.01.*.d01.traj.10MN -name JOBLIST.sh |xargs grep 00.04.05.01

確認
input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.04.05.01.0000.01/RW3A.00.04.05.01.0000.01_d01*"
output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.01/traj/RW3A.00.04.05.01.0000.01/RW3A.00.04.05.01.0000.01.d01.traj.10MN"
```


#/bin/sh

usage(){
cat <<EOF

Usage: $(basename $0) -c <CASE> -r <RUN> -e <MM.NN> -d <domain>

EOF
}

exe=ARWpost.exe

POSTFIX= "_LFC3P"

while getopts c:r:e:d:v:h OPT; do
  case $OPT in
    "c" ) flagc="true" ; CASE="$OPTARG" ;;
    "r" ) flagr="true" ; RUN="$OPTARG" ;;
    "e" ) flagn="true" ; EXP="$OPTARG" ;;
    "d" ) flagd="true" ; domain="$OPTARG" ;;
    "h" ) flagh="true" ;; 
     *  ) usage
  esac
done

shift $(expr $OPTIND - 1)

if [ "$flagh" = "true"  ]; then
  echo
  usage
  exit 0
fi


echo $CASE $RUN $EXP $domain $outtype $interval $z_or_p

runname=${CASE}.${RUN}.${EXP}
echo
echo "RUNNAME =${runname}"
echo "DOMAIN = ${domain}"
echo 

# input parameters
start_date=2017-07-05_00:00:00
  end_date=2017-07-05_00:00:00

interval_seconds=3600 #21600 #1800,
tacc=0
debug_level=0
interp_method=1

# p
if [ $z_or_p = "p" ]; then
interp_levels="1000., 975., 950., 925., 900., 875., 850.,825., 800.,750.,700.,650.,600.,550.,500.,450.,400.,350.,300.,250.,200.,150.,100.,"
fi

# z
if [ $z_or_p = "z" ]; then
interp_levels="\
0.3, 0.5"
fi

# z
#interp_levels="\
#0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, \
#1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, \
#2.1, 2.2, 2.3, 2.4, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, \
#3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8, 3.9, 4.0, \
#4.1, 4.2, 4.3, 4.4, 4.5, 4.6, 4.7, 4.8, 4.9, 5.0 \
#"

# interp_levels:
#   - hieght in km above sea level.
#   - hPa


echo
echo "RUNNAME =${runname}"
echo "DOMAIN = ${domain}"
echo 

indir=/work07/thotspot/zamanda/WRF3.7.1/WRFV3.K17/test/${runname}
#indir=/work06/manda/WRF.K17/${runname}
if [ ! -d $indir ]; then
  echo Error in $0 : No such directory, $indir
  exit 1
fi

outdir=/work07/thotspot/zamanda/ARWpost_${runname}
#outdir=/work06/manda/ARWpost_K17_work06/ARWpost_${runname}_${POSTFIX}
if [ ! -d $outdir ]; then
  mkdir -vp $outdir
fi

namelist=namelist.ARWpost

cwd=$(pwd)
user=$(whoami)
today=$(export LANG=C; date)

cat << EOF > $namelist
!
! ${runname}
!
! ${cwd}
! ${user}
! ${today}
! $0 $@
&datetime
 start_date = "${start_date}",
 end_date   = "${end_date}",
 interval_seconds = ${interval_seconds},
 tacc = ${tacc},
 debug_level = ${debug_level}
/

 !_${start_date}.nc"

&io
 input_root_name = "${indir}/${runname}_${domain}*"
 output_root_name= "${outdir}/${runname}.${domain}.${z_or_p}${POSTFIX}"
plot = "list"
fields = 'HGT,height,XLAND,LCL3P,LFC3P,EL3P'
 mercator_defs = .true.
 split_output = .true.
 frames_per_outfile = 1
/

&interp
 interp_method = ${interp_method}
 interp_levels = ${interp_levels}
/
EOF

ulimit -s unlimited
$exe

cp -av ${namelist} ${outdir}/${namelist}${POSTFIX}.${runname}

cp -av $0 ${outdir}

echo
echo "z_or_p=$z_or_p"
echo
echo interp_levels
echo $interp_levels
echo

list=LIST_${runname}.${domain}.txt
export LANG=C
date -R >$list
pwd >>$list
echo "======================================================" >>$list
echo " List of ${outdir}" >>$list
echo "======================================================" >>$list
ls -lh ${outdir} >>$list
echo "======================================================" >>$list
echo >>$list

cp $list ${outdir}
cat $list

exit 0


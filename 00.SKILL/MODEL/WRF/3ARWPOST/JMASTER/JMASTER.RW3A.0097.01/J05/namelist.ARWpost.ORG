!
! PL1101_00.04
!
! /work1/am/WRF3.4.1.par/MyARWpost
! am
! Tue Nov  8 13:15:40 JST 2016
!
&datetime
 start_date = "2011-01-18_00:00:00",
 end_date   = "2011-01-23_00:00:00",
 interval_seconds = 21600,
 tacc = 0,
 debug_level = 0
/

 !_2011-01-18_00:00:00.nc"

&io
 input_root_name = "/work1/am/WRF.Result/PL1101_00.04/WRF_PL1101_00.04/wrfout_d02*.nc"
 output_root_name= "/work1/am/WRF.Result/PL1101_00.04/ARWpost_PL1101_00.04/PL1101_00.04.d02.p"
plot = "list"
fields = 'HGT,height,XLAND,
   pressure,slp,PSFC,
   tk,tc,td,td2,T2,theta,SST,ept,
   Q2,rh,QVAPOR,QCLOUD,QRAIN,RAINC,RAINNC,CLDFRA,
   RAINRNC, RAINRC,
   U,V,U10,V10,W,
   LH,HFX,QFX,SWDOWN,SWNORM,GLW,OLR,
   mcape,mcin,lcl,lfc,cape,cin,
   H_DIABATIC,
   PBLH,UST'
 mercator_defs = .true.
 split_output = .false.
 frames_per_outfile = 1
/

&interp
 interp_method = 1
 interp_levels = 1000., 975., 950., 925., 900., 850., 800.,750.,700.,650.,600.,550.,500.,450.,400.,350.,300.,250.,200.,150.,100.,
/

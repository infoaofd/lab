#/bin/sh
#PBS -q C064
#PBS -l walltime=48:00:00
#PBS -l select=1
#PBS -o A009701traj.OUT
#PBS -e A009701traj.ERR
#PBS -N A009701
#PBS -V

export OMP_NUM_THREADS=24
cd ${PBS_O_WORKDIR}

./ARWpost.exe

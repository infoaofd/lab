MyARWpost_LFC3
------------------------

Sun, 10 May 2020 11:39:01 +0900
calypso.bosai.go.jp
/work06/manda/WRF3.7.1/MyARWpost_LFC3

```
srcdump.sh src/module_diagnostics_DIFF.txt src/module_diagnostics.f90 src/module_calc_LFC3.f src/Makefile configure.arwp
```

**Machine info**
processor	: 15
model name	: Intel(R) Xeon(R) CPU E5-2690 0 @ 2.90GHz
MemTotal:       65988728 kB



LFCとLCLの3次元分布 (LCL3, LFC3)を得る。

デフォルトでは、高度3000m以下で最も相当温位の高い格子点から持ち上げたLCLとLFCの2次元分布しか求められない。

2020-05-02_18-55 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src

```bash
$ org.sh module_diagnostics.f90
`module_diagnostics.f90' -> `module_diagnostics.f90.ORG.200502-1855'
~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 28K Aug  8  2019 module_diagnostics.f90
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 28K May  2 18:55 module_diagnostics.f90.ORG.200502-1855
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

```bash
$ export NETCDF=/usr/local/netcdf-3.6.3
```

```bash
/work06/manda/WRF3.7.1/MyARWpost_LFC3
$ clean -a

$ ./configure
Will use NETCDF in dir: /home/manda/local

   1.  PC Linux i486 i586 i686 x86_64, PGI compiler 
   2.  PC Linux i486 i586 i686 x86_64, Intel compiler 
   3.  PC Linux i486 i586 i686 x86_64, gfortran compiler 

Enter selection [1-3] : 2

$ ./compile 2>&1 |tee -a COMPILE_$(myymd).LOG
```
```bash
$ MyArwpost_LFC3_K17.run.sh
```
```bash
$ pushd /work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3
/work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3
$ CHK.LFC3.sh

OPEN dset /work06/manda/ARWpost/ARWpost_K17.R27.00.00_LFC3/K17.R27.00.00.d03.p_LFC3_%y4-%m2-%d2_%h2:%n2.dat

-rw-rw-r-- 1 manda manda 2.6M 2020-05-02 21:25 K17.R27.00.00_LFC3_950_00Z05JUL2017.eps
```



List of the following files:**

- src/module_diagnostics_DIFF.txt
- src/module_diagnostics.f90
- src/Makefile
- configure.arwp
  
### src/module_diagnostics_DIFF.txt
```
33d32
<   use module_calc_LFC3 !LFC3
552,597d550
< !!! 3D LCL/LFC
< !2020-05-02_18-55 
< !manda@calypso
< !/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
<         IF ( INDEX(plot_these_fields,",LCL3,") /= 0 .OR. &
<              INDEX(plot_these_fields,",LFC3,")  /= 0) THEN
<           good_to_go = 0
<           IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
<           IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
<           IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
<           IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 
< 
<             IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
<             IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
<             ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
<             ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
<             SCRa = 0.0
<             SCRb = 0.0
<             CALL calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, 1) 
< 
<             IF ( INDEX(plot_these_fields,",LCL3,") /= 0 ) THEN
<               cname = "LCL3"
<               cdesc = "LCL3"
<               cunits = "meters AGL"
<               CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
<                            data_out, nxout, nyout, nzout, &
<                            vert_array, interp_levels, number_of_zlevs,cname) 
<               CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
<             END IF
<             IF ( INDEX(plot_these_fields,",LFC3,") /= 0) THEN
<               cname = "LFC3"
<               cdesc = "LFC3"
<               cunits = "meters AGL"
<               CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
<                            data_out, nxout, nyout, nzout, &
<                            vert_array, interp_levels, number_of_zlevs,cname) 
<               CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
<             END IF
<             DEALLOCATE(SCRa)
<             DEALLOCATE(SCRb)
< 
<           END IF
<         END IF
< 
< 
< 
```

### src/module_diagnostics.f90
```
!! Diagnostics
!! See moddule_diagnostic for which fields are available 

MODULE module_diagnostics

  USE output_module
  USE gridinfo_module
  USE module_interp
  USE module_arrays
  USE constants_module


  USE module_calc_pressure
  USE module_calc_height
  USE module_calc_theta
  USE module_calc_tk
  USE module_calc_tc
  USE module_calc_td
  USE module_calc_td2
  USE module_calc_rh
  USE module_calc_rh2
  USE module_calc_uvmet
  USE module_calc_wdir
  USE module_calc_wspd
  USE module_calc_slp
  USE module_calc_dbz
  USE module_calc_cape
  USE module_calc_clfr

  USE module_calc_myrh !myrh

  use module_calc_ept !EPT
  use module_calc_LFC3 !LFC3


  CONTAINS

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Name: process_diagnostics
   ! Purpose: All new calls to diagnostics routines go here
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   SUBROUTINE process_diagnostics ( local_time )

      IMPLICIT NONE

      ! Local variables
      character (len=128)                    :: cname, cunits, cdesc, c_nm
      real, allocatable, dimension(:,:)      :: SCR2
      real, allocatable, dimension(:,:,:)    :: SCR3
      real, allocatable, dimension(:,:,:)    :: SCR, SCRa, SCRb, SCRc
      real, allocatable, dimension(:,:,:)    :: data_out 
      integer                                :: nxout, nyout, nzout
      integer                                :: ii, jj, kk
      integer                                :: good_to_go, geo
      integer                                :: local_time

! Equivalent Potential Temperature
        IF ( INDEX(plot_these_fields,",ept,") /= 0 ) THEN  !EPT 
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_ept(SCR, cname, cdesc, cunits)  !EPT
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate pressure in hPA
        IF ( INDEX(plot_these_fields,",pressure,") /= 0 ) THEN
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_pressure(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate height/geopt 
        IF ( INDEX(plot_these_fields,",geopt,") /= 0) THEN      
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",height,") /= 0 ) THEN 
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            cname = "height"
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate temperature 
        IF ( INDEX(plot_these_fields,",tk,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tk(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",tc,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tc(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate theta
        IF ( INDEX(plot_these_fields,",theta,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T ) CALL get_keep_array ( local_time, good_to_go, "T" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_theta(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature
        IF ( INDEX(plot_these_fields,",td,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_td(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature at 2m
        IF ( INDEX(plot_these_fields,",td2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_td2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF

!myrh
        myrh1: IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_myrh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
        END IF myrh1

!!! Relative Humidity    
        IF ( INDEX(plot_these_fields,",rh,")   /= 0 .OR. &
             INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_rh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",rh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            !!!  Cloud fractions
            IF ( INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
              IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
              ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
              SCRa = 0.0
              IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
              ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
              SCRb = 0.0
              IF ( ALLOCATED(SCRc) ) DEALLOCATE(SCRc)
              ALLOCATE(SCRc(west_east_dim,south_north_dim,1))
              SCRc = 0.0
              CALL calc_clfr(SCRa, SCRb, SCRc, cname, cdesc, cunits, SCR) 
              cname = "clflo"
              cdesc = "Low Cloud Fraction"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfmi"
              cdesc = "Mid Cloud Fraction"
              CALL interp (SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfhi"
              cdesc = "High Cloud Fraction"
              CALL interp (SCRc, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

              DEALLOCATE(SCRa)
              DEALLOCATE(SCRb)
              DEALLOCATE(SCRc)
            END IF
            DEALLOCATE(SCR)
          END IF
        END IF

        IF ( INDEX(plot_these_fields,",rh2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_rh2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed       
        IF ( INDEX(plot_these_fields,",wspd,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction       
        IF ( INDEX(plot_these_fields,",wdir,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed  - 10m      
        IF ( INDEX(plot_these_fields,",ws10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction  - 10m      
        IF ( INDEX(plot_these_fields,",wd10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Rotated U and V      
        IF ( INDEX(plot_these_fields,",umet,") /= 0 .OR. &
             INDEX(plot_these_fields,",vmet,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_UUU )   CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV )   CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",umet,") /= 0 ) THEN
              cname = "umet"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",vmet,") /= 0) THEN
              cname = "vmet"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


!!! Rotated U10 and V10      
        IF ( INDEX(plot_these_fields,",u10m,") /= 0 .OR. &
             INDEX(plot_these_fields,",v10m,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_U10 )   CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 )   CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 0) 

            IF ( INDEX(plot_these_fields,",u10m,") /= 0 ) THEN
              cname = "u10m"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",v10m,") /= 0) THEN
              cname = "v10m"
              CALL interp( SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF

        !!! Clobber some wind arrays
        IF (ALLOCATED(UUU)) DEALLOCATE(UUU)
        have_UUU = .FALSE.
        IF (ALLOCATED(VVV)) DEALLOCATE(VVV)
        have_VVV = .FALSE.
        IF (ALLOCATED(U10)) DEALLOCATE(U10)
        have_UUU = .FALSE.
        IF (ALLOCATED(V10)) DEALLOCATE(V10)
        have_VVV = .FALSE.



!!! Sea Level Pressure  
        IF ( INDEX(plot_these_fields,",slp,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_slp(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Reflectivity             
        IF ( INDEX(plot_these_fields,",dbz,")     /= 0 .OR. &
             INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QS )  CALL get_keep_array ( local_time, good_to_go, "QSNOW" )
          IF ( .not. have_QG )  CALL get_keep_array ( local_time, good_to_go, "QGRAUP" )
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_QR )  CALL get_keep_array ( local_time, good_to_go, "QRAIN" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_dbz(SCR, cname, cdesc, cunits) 

            IF ( INDEX(plot_these_fields,",dbz,") /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            IF ( INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
              ALLOCATE(SCR2(west_east_dim,south_north_dim))
              SCR2 = 0.0
              DO jj = 1,south_north_dim
                DO ii = 1,west_east_dim
                  DO kk = 1,bottom_top_dim
                    SCR2(ii,jj) = MAX( SCR2(ii,jj) , SCR(ii,jj,kk) )
                  END DO 
                END DO 
              END DO 
              IF (ALLOCATED(SCR)) DEALLOCATE(SCR)
              ALLOCATE(SCR(west_east_dim,south_north_dim,1))
              SCR(:,:,1) = SCR2(:,:)
              cname = "max_dbz"
              cdesc    = "Max Reflectivity"
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              DEALLOCATE(SCR2)
            END IF
            DEALLOCATE(SCR)

          END IF
        END IF



!!! 3D CAPE/CIN 
        IF ( INDEX(plot_these_fields,",cape,") /= 0 .OR. &
             INDEX(plot_these_fields,",cin,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",cape,") /= 0 ) THEN
              cname = "cape"
              cdesc = "CAPE"
              cunits = "J/kg"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",cin,") /= 0) THEN
              cname = "cin"
              cdesc = "CIN"
              cunits = "J/kg"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 3D LCL/LFC
!2020-05-02_18-55 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
        IF ( INDEX(plot_these_fields,",LCL3,") /= 0 .OR. &
             INDEX(plot_these_fields,",LFC3,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",LCL3,") /= 0 ) THEN
              cname = "LCL3"
              cdesc = "LCL3"
              cunits = "meters AGL"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",LFC3,") /= 0) THEN
              cname = "LFC3"
              cdesc = "LFC3"
              cunits = "meters AGL"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 2D CAPE/CIN & LCL/LFC
        IF ( INDEX(plot_these_fields,",mcape,") /= 0 .OR. &
             INDEX(plot_these_fields,",mcin,")  /= 0 .OR.  &
             INDEX(plot_these_fields,",lcl,")   /= 0 .OR.   &
             INDEX(plot_these_fields,",lfc,")   /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 0) 

            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            IF ( INDEX(plot_these_fields,",mcape,") /= 0 ) THEN
              cname = "mcape"
              cdesc = "MCAPE"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,1)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",mcin,") /= 0) THEN
              cname = "mcin"
              cdesc = "MCIN"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,2)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lcl,") /= 0) THEN
              cname = "lcl"
              cdesc = "LCL"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,3)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lfc,") /= 0) THEN
              cname = "lfc"
              cdesc = "LFC"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,4)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


       IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)

   END SUBROUTINE process_diagnostics

END MODULE module_diagnostics





```

### src/Makefile
```

include ../configure.arwp

OBJS = module_model_basics.o constants_module.o \
gridinfo_module.o ARWpost.o input_module.o \
output_module.o module_map_utils.o \
misc_definitions_module.o module_date_pack.o \
module_debug.o process_domain_module.o \
module_get_file_names.o module_interp.o \
module_basic_arrays.o module_diagnostics.o \
module_arrays.o module_pressure.o module_calc_height.o \
module_calc_pressure.o module_calc_theta.o \
module_calc_tk.o module_calc_tc.o module_calc_td.o \
module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o \
module_calc_cape.o module_calc_wdir.o module_calc_wspd.o \
module_calc_clfr.o module_calc_myrh.o module_calc_ept.o \
module_ept_bolton80.o \
module_calc_LFC3.o


wrong: 
	clear ;
	@echo " "
	@echo "go up one directory and type compile to build ARWpost"
	@echo " "
	@echo " "


all: ARWpost.exe

ARWpost.exe: $(OBJS)
	$(FC) $(FFLAGS) $(LDFLAGS) -o $@ $(OBJS)  \
		-L$(NETCDF)/lib -I$(NETCDF)/include  \
	-lnetcdf


module_model_basics.o:

constants_module.o:

misc_definitions_module.o:

module_date_pack.o:

module_get_file_names.o:

module_arrays.o: module_model_basics.o gridinfo_module.o

module_pressure.o: module_model_basics.o

module_interp.o: input_module.o module_arrays.o module_pressure.o

module_map_utils.o:

module_debug.o:

gridinfo_module.o: misc_definitions_module.o module_debug.o module_get_file_names.o

input_module.o: gridinfo_module.o misc_definitions_module.o 

output_module.o: input_module.o module_model_basics.o module_arrays.o module_interp.o

process_domain_module.o: module_date_pack.o gridinfo_module.o input_module.o \
output_module.o misc_definitions_module.o module_debug.o module_interp.o \
module_basic_arrays.o module_diagnostics.o module_arrays.o \
module_model_basics.o module_pressure.o

ARWpost.o: gridinfo_module.o module_debug.o process_domain_module.o


module_basic_arrays.o: gridinfo_module.o module_arrays.o module_pressure.o \
module_interp.o constants_module.o

module_diagnostics.o: gridinfo_module.o output_module.o module_arrays.o \
module_interp.o module_pressure.o constants_module.o module_calc_height.o \
module_calc_pressure.o module_calc_tk.o module_calc_tc.o module_calc_theta.o \
module_calc_td.o module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o module_calc_cape.o \
module_calc_wdir.o module_calc_wspd.o module_calc_clfr.o module_calc_myrh.o \
module_calc_ept.o module_calc_LFC3.o

module_calc_cape.o: module_model_basics.o constants_module.o
module_calc_dbz.o: module_model_basics.o constants_module.o
module_calc_height.o: module_model_basics.o constants_module.o
module_calc_pressure.o: module_model_basics.o
module_calc_slp.o: module_model_basics.o constants_module.o
module_calc_rh.o: module_model_basics.o constants_module.o
module_calc_rh2.o: module_model_basics.o constants_module.o
module_calc_tk.o: module_model_basics.o constants_module.o
module_calc_tc.o: module_model_basics.o constants_module.o
module_calc_td.o: module_model_basics.o
module_calc_td2.o: module_model_basics.o
module_calc_theta.o: module_model_basics.o
module_calc_uvmet.o: module_model_basics.o
module_calc_wdir.o: module_model_basics.o
module_calc_wspd.o: module_model_basics.o
module_calc_clfr.o: module_model_basics.o
module_calc_myrh.o: module_model_basics.o constants_module.o
module_calc_ept.o: module_model_basics.o constants_module.o module_ept_bolton80.o
module_calc_LFC3.o: module_model_basics.o constants_module.o


clean:
	rm -f $(OBJS) *.mod

clobber:
	rm -f $(OBJS) *.mod
	rm -f *.f
	rm -f ARWpost.exe 
```

### configure.arwp
```
# configure.arwp
#
# This file was automatically generated by the configure script in the
# top level directory. You may make changes to the settings in this
# file but be aware they will be overwritten each time you run configure.
# Ordinarily, it is necessary to run configure once, when the code is
# first installed.
#
# To permanently change options, change the settings for your platform
# in the file arch/configure.defaults, the preamble, and the postamble -
# then rerun configure.
#

.SUFFIXES: .F90 .f90 .F .f .c .o

SHELL           	=       /bin/sh

# Listing of options that are usually independent of machine type.
# When necessary, these are over-ridden by each architecture.

ARFLAGS			=	

PERL			=	perl

RANLIB			=	echo

#### Architecture specific settings ####

# Settings for PC Linux i486 i586 i686 x86_64, Intel compiler	
#
FC		=	ifort
FFLAGS		=	-FR -convert big_endian
F77FLAGS	=	-convert big_endian
FNGFLAGS	=	$(FFLAGS)
LDFLAGS		=	
CC		=	 gcc -DFSEEKO64_OK
CFLAGS		=	
CPP		=	/lib/cpp -traditional
CPPFLAGS	=	-DIO_NETCDF -DIO_GRIB1 -DIO_BINARY -Dbytesw 

###########################################################
#
#	Macros, these should be generic for all machines

LN		=	ln -sf
MAKE		=	make -i -r
RM		= 	/bin/rm -f
CP		= 	/bin/cp
AR		=	ar ru


.IGNORE:
.SUFFIXES: .c .f90 .F90 .f .F .o

#	There is probably no reason to modify these rules

.c.o:
	$(RM) $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<	

.f90.o:
	$(RM) $@ $*.mod
	$(CP) $< $*.f
	$(FC) $(FFLAGS) -I${NETCDF}/include -c $*.f 
	$(RM) $*.f

.F90.o:
	$(RM) $@ $*.mod
	$(CPP) $(CPPFLAGS) $(FDEFS) $< > $*.f
	$(FC) $(FFLAGS) -I${NETCDF}/include -c $*.f 
	$(RM) $*.f
```


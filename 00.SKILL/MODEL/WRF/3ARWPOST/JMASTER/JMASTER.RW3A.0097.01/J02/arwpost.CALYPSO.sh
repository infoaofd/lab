#/bin/sh

usage(){
cat <<EOF

Usage: $(basename $0) -c <CASE> -r <RUN> -e <MM.NN> -d <domain> -t <outtype> -i <interval>
EOF
}
# Default values
CASE=K17
RUN=R27
EXP=00.00
domain=d03
outtype=basic_z
interval=01H
# p=pressure levels, z=height


while getopts c:r:e:d:t:i:v:h OPT; do
  case $OPT in
    "c" ) flagc="true" ; CASE="$OPTARG" ;;
    "r" ) flagr="true" ; RUN="$OPTARG" ;;
    "e" ) flagn="true" ; EXP="$OPTARG" ;;
    "d" ) flagd="true" ; domain="$OPTARG" ;;
    "t" ) flagt="true" ; outtype="$OPTARG" ;;
    "i" ) flagi="true" ; interval="$OPTARG" ;;
    "h" ) flagh="true" ;; 
     *  ) usage
  esac
done

shift $(expr $OPTIND - 1)

echo $CASE $RUN $EXP $domain $outtype $interval

# input parameters
#start_date=2017-07-04_12:00:00
start_date=2017-07-05_00:00:00
  end_date=2017-07-05_00:00:00
#  end_date=2017-07-05_18:00:00

if [ $interval = "10m" ]; then
interval_seconds=600 
else
interval_seconds=3600 #21600 #1800,
fi

tacc=0
debug_level=0
interp_method=1

if [ $outtype =  basic_p ]; then
interp_levels="1000., 975., 950., 925., 900., 875., 850.,825., 800.,750.,700.,650.,600.,550.,500.,450.,400.,350.,300.,250.,200.,150.,100.,"

OUTVARS="HGT,height,XLAND, \
   pressure,slp, \
   tk,theta,SST,rh,ept,sept, \
   Q2,T2,QVAPOR,QCLOUD,QRAIN,RAINC,RAINNC, \
   RAINRNC, RAINRC, \
   U,V,U10,V10,W, \
   LH,HFX,QFX, \
   H_DIABATIC, \
   PBLH,dbz,max_dbz"

elif [ $outtype = "cape3_z" ]; then

interp_levels="0.3, 0.5"
OUTVARS="cin, cape, ept, LCL3P, LFC3P, EL3P"
#OUTVARS="cin"

elif [ $outtype = "basic.zhir_z" ]; then
interp_levels="\
0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 \
1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 \
2.8 2.9 3.0"

OUTVARS="HGT,height,XLAND, \
   pressure,slp, \
   tk,theta,SST,rh,ept,sept, \
   Q2,T2,QVAPOR,QCLOUD,QRAIN,RAINC,RAINNC, \
   RAINRNC, RAINRC, \
   U,V,U10,V10,W, \
   LH,HFX,QFX, \
   H_DIABATIC, \
   PBLH,dbz,max_dbz"

else

echo 
echo "ERROR in $0 : INVALID OPTION (outtype). outtype=${outtype}"
echo
exit 1

fi #outtype

# interp_levels:
#   - hieght in km above sea level.
#   - hPa

exe=arwp.sh #ARWpost.exe

runname=${CASE}.${RUN}.${EXP}
echo
echo "RUNNAME =${runname}"
echo "DOMAIN = ${domain}"
echo 

indir=/work06/manda/WRF.${CASE}/${runname}
#indir=/work07/thotspot/zamanda/WRF3.7.1/WRFV3.${CASE}/test/${runname}

if [ ! -d $indir ]; then
  echo Error in $0 : No such directory, $indir
  exit 1
fi

outdir=/work06/manda/ARWpost_K17_work06/ARWpost_${runname}_CHK
#outdir=/work07/thotspot/zamanda/ARWpost/ARWpost_${runname}

if [ ! -d $outdir ]; then
  mkdir -vp $outdir
fi

namelist=namelist.ARWpost

HOST=$(hostname)
CWD=$(pwd)
HOST=$(hostname)
TODAY=$(date -R)

cat << EOF > $namelist
! ${runname}
! ${TODAY}
! ${CWD}
! ${HOST}
! $0 -c $CASE -r $RUN -e $EXP -d $domain -t $outtype -i $interval
!
&datetime
 start_date = "${start_date}",
 end_date   = "${end_date}",
 interval_seconds = ${interval_seconds},
 tacc = ${tacc},
 debug_level = ${debug_level}
/

&io
 input_root_name = "${indir}/${runname}_${domain}*"
 output_root_name= "${outdir}/${runname}.${domain}.${outtype}.${interval}"

plot = "list"
fields = "$OUTVARS"

 mercator_defs = .true.
 split_output = .true.
 frames_per_outfile = 1
/

&interp
 interp_method = ${interp_method}
 interp_levels = ${interp_levels}
/
EOF



if [ "$flagh" = "true"  ]; then
  echo
  usage
  exit 0
fi

cp -av ${namelist} ${outdir}/${namelist}.${runname}.${outtype}
cp -av $0 ${outdir}

#WPSDIR=/work07/thotspot/zamanda/WPS.${CASE}/${runname}
#WRFDIR=/work07/thotspot/zamanda/WRF3.7.1/WRFV3.${CASE}/test/${runname}
#
#cp -av $WPSDIR/namelist.wps* $outdir
#cp -av $WPSDIR/*.*ps $outdir
#cp -av $WRFDIR/namelist.input ${outdir}/namelist.input.${runname}
#cp -av $WRFDIR/*.sh $outdir
#cp -av $WRFDIR/*.0000 $outdir

cat <<EOF >$exe
#/bin/sh
#PBS -q C064
#PBS -l walltime=24:00:00
#PBS -o ARWP.${EXP}.${outtype}.OUT
#PBS -e ARWP.${EXP}.${outtype}.ERR
#PBS -N A${EXP}
#PBS -V

cd \${PBS_O_WORKDIR}

./ARWpost.exe
EOF

cp -av $exe ${outdir}
echo

#echo "~~~~~~~~~~~~~~~~~~~~~~~"
#echo "qsub $exe"
#qsub $exe
#echo "~~~~~~~~~~~~~~~~~~~~~~~"

./ARWpost.exe

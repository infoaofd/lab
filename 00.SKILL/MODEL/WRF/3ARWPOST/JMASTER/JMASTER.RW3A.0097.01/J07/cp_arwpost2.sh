#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd30
# Directory: /work2/am/12.Work11/42.WRF_ECS/WRFV3/test/em_real
#
# Revision history:
#  This file is created by /usr/local/mybin/nbscr.sh at 17:55 on 11-25-2011.


echo "Shell script, $(basename $0) starts."
echo

# Handling options
# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru


while getopts r OPT; do
  case $OPT in
    "r" ) flagr="true" ;;
     * ) echo "Usage $0 [-r] dest_dir" 1>&2; echo "dest_dir: destination directory" 1>&2
  esac
done
flagr=${flagr:-"NOT_AVAILABLE"}
shift $(expr $OPTIND - 1)

if [ $# -ne 3 ]; then
  echo Error in $0 : Wrong number of arguments.
  echo Usage: $0 runname domain destdir
  echo "domain : domain name (d01, d02, etc.)"
  echo dir: directory name
  exit 1
fi

runname=$1
domain=$2
dir=$3
destdir="${dir}/${runname}/ARWpost_${runname}_${domain}/"


if [ ! -d $destdir ]; then
  mkdir -p ${destdir}
  echo Directory, ${destdir} created.
fi

OPT="-v -p"
inlist=$(ls *wrfout*.ctl)
for infle in $inlist; do
  cp ${OPT} $infle ${destdir}
  if [ $? -ne 0 ]; then
    echo ERROR in $0 : while copying $infle
    exit 1
  fi
  if [ $flagr = "true" ]; then
    echo "rm $infle"
    rm $infle
  fi
done


inlist=$(ls *wrfout*.dat)
for infle in $inlist; do
  cp ${OPT} "$infle" ${destdir}
  if [ $? -ne 0 ]; then
     echo ERROR in $0 : while copying $infle
     exit 1
   fi
  if [ $flagr = "true" ]; then
    echo "rm $infle"
    rm $infle
  fi
done


inlist=$(ls namelist.*)
for infle in $inlist; do
  cp ${OPT} $infle ${destdir}
  if [ $? -ne 0 ]; then
     echo ERROR in $0 : while copying $infle
     exit 1
   fi
#  if [ $flagr = "true" ]; then
#    echo "rm $infle"
#    rm $infle
#  fi
done


echo "Done $0"
echo

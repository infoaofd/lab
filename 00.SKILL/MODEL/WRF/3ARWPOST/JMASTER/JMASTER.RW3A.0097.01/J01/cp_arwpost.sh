#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd30
# Directory: /work2/am/12.Work11/42.WRF_ECS/WRFV3/test/em_real
#
# Revision history:
#  This file is created by /usr/local/mybin/nbscr.sh at 17:55 on 11-25-2011.


echo "Shell script, $(basename $0) starts."
echo

# Handling options
# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru

value_D=""
while getopts rd: OPT; do
  case $OPT in
    "r" ) flag_r="true" ;;
    "d" ) flag_d="true" ; value_d="$OPTARG" ;;
     * ) echo "Usage $0 [-r] [-d description] exp_name dest_dir" 1>&2; echo "dest_dir: destination directory" 1>&2
  esac
done
flag_r=${flag_r:-"NOT_AVAILABLE"}
flag_d=${flag_d:-"NOT_AVAILABLE"}

shift $(expr $OPTIND - 1)

if [ $# -ne 2 ]; then
  echo Error in $0 : Wrong number of arguments.
  echo Usage: $0 runname destdir
  echo dir: directory name
  exit 1
fi

runname=$1
dir=$2
if [ ${value_d} != "" ]; then
  postfix=_${value_d}
else
  postfix=""
fi
destdir="${dir}/${runname}/ARWpost_${runname}${postfix}/"


if [ ! -d $destdir ]; then
  mkdir -p ${destdir}
  echo Directory, ${destdir} created.
fi

inlist=$(ls *.ctl)
for infle in $inlist; do
  mv -v $infle ${destdir}
  if [ $? -ne 0 ]; then
    echo ERROR in $0 : while copying $infle
    exit 1
  fi
done


inlist=$(ls *${runname}*.dat)
for infle in $inlist; do
  mv -v "$infle" ${destdir}
  if [ $? -ne 0 ]; then
     echo ERROR in $0 : while copying $infle
     exit 1
   fi
done


inlist=$(ls namelist.*)
for infle in $inlist; do
  cp -av $infle ${destdir}
  if [ $? -ne 0 ]; then
     echo ERROR in $0 : while copying $infle
     exit 1
   fi
done

echo "Done $0"
echo

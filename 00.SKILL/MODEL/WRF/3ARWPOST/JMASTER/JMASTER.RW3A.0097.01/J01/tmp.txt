バイナリー・ファイルsrc/ARWpost.exeは一致しました
src/gridinfo_module.f90:54:      namelist /datetime/ start_date, start_year, start_month, start_day, start_hour,   &
src/gridinfo_module.f90:92:      READ(funit,datetime)
src/input_module.f90:60:                               stagger, real_array, local_time, istatus)
src/input_module.f90:69:      integer                                :: local_time
src/input_module.f90:93:      istart(idm) = local_time
src/input_module.f90:280:   SUBROUTINE arw_get_next_time (valid_date, datestr, file_time, istatus)
src/input_module.f90:288:      integer               :: iloc, itype, idm, natt, ii, file_time
src/input_module.f90:301:        get_times : DO ii=1,ncDims(ishape(2))
src/input_module.f90:302:          file_time = ii
src/input_module.f90:305:          IF ( istatus /= 0 ) EXIT get_times
src/input_module.f90:307:          !IF ( TRIM(datestr) == TRIM(valid_date) ) EXIT get_times
src/input_module.f90:308:          IF ( abs(isec_diff) .LE. tacc ) EXIT get_times
src/input_module.f90:309:        END DO get_times
src/input_module.f90:314:   END SUBROUTINE arw_get_next_time
バイナリー・ファイルsrc/input_module.oは一致しました
src/module_basic_arrays.f90:12:   SUBROUTINE process_basic_arrays ( local_time )
src/module_basic_arrays.f90:20:      integer                                :: local_time
src/module_basic_arrays.f90:29:          IF ( .not. have_P  ) CALL get_keep_array ( local_time, good_to_go, "P" )
src/module_basic_arrays.f90:30:          IF ( .not. have_PB ) CALL get_keep_array ( local_time, good_to_go, "PB" )
src/module_basic_arrays.f90:42:          IF ( .not. have_MU   ) CALL get_keep_array ( local_time, good_to_go, "MU" )
src/module_basic_arrays.f90:43:          IF ( .not. have_MUB  ) CALL get_keep_array ( local_time, good_to_go, "MUB" )
src/module_basic_arrays.f90:44:          IF ( .not. have_ZNU  ) CALL get_keep_array ( local_time, good_to_go, "ZNU" )
src/module_basic_arrays.f90:45:          IF ( .not. have_ZNW  ) CALL get_keep_array ( local_time, good_to_go, "ZNW" )
src/module_basic_arrays.f90:46:          IF ( .not. have_PTOP ) CALL get_keep_array ( local_time, good_to_go, "P_TOP" )
src/module_basic_arrays.f90:47:          IF ( .not. have_QV   ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_basic_arrays.f90:58:          CALL get_keep_array ( local_time, good_to_go, "PRES" )
src/module_basic_arrays.f90:69:          IF ( .not. have_T  ) CALL get_keep_array ( local_time, good_to_go, "T" )
src/module_basic_arrays.f90:81:          CALL get_keep_array ( local_time, good_to_go, "TT" )
src/module_basic_arrays.f90:92:          IF ( .not. have_PH  ) CALL get_keep_array ( local_time, good_to_go, "PH" )
src/module_basic_arrays.f90:93:          IF ( .not. have_PHB ) CALL get_keep_array ( local_time, good_to_go, "PHB" )
src/module_basic_arrays.f90:105:          CALL get_keep_array ( local_time, good_to_go, "GHT" )
src/module_basic_arrays.f90:111:      IF ( .not. have_QV    ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_basic_arrays.f90:112:      IF ( .not. have_PSFC  ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_basic_arrays.f90:113:      IF ( .not. have_HGT   ) CALL get_keep_array ( local_time, good_to_go, "HGT", "HGT_M" )
src/module_basic_arrays.f90:114:      IF ( .not. have_XLAT  ) CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
src/module_basic_arrays.f90:115:      IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
src/module_basic_arrays.f90:116:      IF ( .not. have_PTOP  ) CALL get_keep_array ( local_time, good_to_go, "P_TOP" )
src/module_date_pack.f90:3:!  This module is able to perform three date and time functions:
src/module_date_pack.f90:6:!  Get the time period between two dates.
src/module_date_pack.f90:9:!  Get the new date based on the old date and a time difference.
src/module_date_pack.f90:23:      !  compute the time difference.
src/module_date_pack.f90:28:      !  on exit      -  idts    -  the change in time in seconds.
src/module_date_pack.f90:261:      !  Determine the time difference in seconds
src/module_date_pack.f90:284:      !  delta-time, compute the new date.
src/module_date_pack.f90:287:      !                  idt    -  the change in time
src/module_date_pack.f90:445:         nday   = ABS(idt)/86400 ! Integer number of days in delta-time
src/module_date_pack.f90:452:         nday   = ABS(idt)/1440 ! Integer number of days in delta-time
src/module_date_pack.f90:459:         nday   = ABS(idt)/24 ! Integer number of days in delta-time
src/module_date_pack.f90:466:         nday   = ABS(idt)/24 ! Integer number of days in delta-time
src/module_diagnostics.f90:38:   SUBROUTINE process_diagnostics ( local_time )
src/module_diagnostics.f90:51:      integer                                :: local_time
src/module_diagnostics.f90:128:          IF ( .not. have_T ) CALL get_keep_array ( local_time, good_to_go, "T" )
src/module_diagnostics.f90:145:          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:162:          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
src/module_diagnostics.f90:163:          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
src/module_diagnostics.f90:164:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics.f90:195:          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:248:          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
src/module_diagnostics.f90:249:          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
src/module_diagnostics.f90:250:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics.f90:268:          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics.f90:269:          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics.f90:286:          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics.f90:287:          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics.f90:304:          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics.f90:305:          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics.f90:322:          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics.f90:323:          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics.f90:341:          IF ( .not. have_UUU )   CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics.f90:342:          IF ( .not. have_VVV )   CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics.f90:343:          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
src/module_diagnostics.f90:344:          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
src/module_diagnostics.f90:380:          IF ( .not. have_U10 )   CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics.f90:381:          IF ( .not. have_V10 )   CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics.f90:382:          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
src/module_diagnostics.f90:383:          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
src/module_diagnostics.f90:429:          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:447:          IF ( .not. have_QS )  CALL get_keep_array ( local_time, good_to_go, "QSNOW" )
src/module_diagnostics.f90:448:          IF ( .not. have_QG )  CALL get_keep_array ( local_time, good_to_go, "QGRAUP" )
src/module_diagnostics.f90:450:          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:451:          IF ( .not. have_QR )  CALL get_keep_array ( local_time, good_to_go, "QRAIN" )
src/module_diagnostics.f90:496:          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:497:          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
src/module_diagnostics.f90:498:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics.f90:541:          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics.f90:542:          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
src/module_diagnostics.f90:543:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics_bak.f90:36:   SUBROUTINE process_diagnostics ( local_time )
src/module_diagnostics_bak.f90:49:      integer                                :: local_time
src/module_diagnostics_bak.f90:126:          IF ( .not. have_T ) CALL get_keep_array ( local_time, good_to_go, "T" )
src/module_diagnostics_bak.f90:143:          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:160:          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
src/module_diagnostics_bak.f90:161:          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
src/module_diagnostics_bak.f90:162:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics_bak.f90:180:          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:233:          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
src/module_diagnostics_bak.f90:234:          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
src/module_diagnostics_bak.f90:235:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics_bak.f90:253:          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics_bak.f90:254:          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics_bak.f90:271:          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics_bak.f90:272:          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics_bak.f90:289:          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics_bak.f90:290:          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics_bak.f90:307:          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics_bak.f90:308:          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics_bak.f90:326:          IF ( .not. have_UUU )   CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
src/module_diagnostics_bak.f90:327:          IF ( .not. have_VVV )   CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
src/module_diagnostics_bak.f90:328:          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
src/module_diagnostics_bak.f90:329:          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
src/module_diagnostics_bak.f90:365:          IF ( .not. have_U10 )   CALL get_keep_array ( local_time, good_to_go, "U10" )
src/module_diagnostics_bak.f90:366:          IF ( .not. have_V10 )   CALL get_keep_array ( local_time, good_to_go, "V10" )
src/module_diagnostics_bak.f90:367:          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
src/module_diagnostics_bak.f90:368:          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
src/module_diagnostics_bak.f90:414:          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:432:          IF ( .not. have_QS )  CALL get_keep_array ( local_time, good_to_go, "QSNOW" )
src/module_diagnostics_bak.f90:433:          IF ( .not. have_QG )  CALL get_keep_array ( local_time, good_to_go, "QGRAUP" )
src/module_diagnostics_bak.f90:435:          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:436:          IF ( .not. have_QR )  CALL get_keep_array ( local_time, good_to_go, "QRAIN" )
src/module_diagnostics_bak.f90:481:          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:482:          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
src/module_diagnostics_bak.f90:483:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_diagnostics_bak.f90:526:          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
src/module_diagnostics_bak.f90:527:          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
src/module_diagnostics_bak.f90:528:          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
src/module_interp.f90:508:   SUBROUTINE get_interp_array (local_time)
src/module_interp.f90:527:      integer                             :: local_time
src/module_interp.f90:549:          istart(idm) = local_time
src/module_interp.f90:566:          istart(idm) = local_time
src/module_interp.f90:584:          istart(idm) = local_time
src/module_interp.f90:613:          istart(idm) = local_time
src/module_interp.f90:654:        istart(idm) = local_time
src/module_interp.f90:680:   SUBROUTINE get_keep_array ( local_time, good_to_go, cname, cname2 )
src/module_interp.f90:702:      integer                             :: local_time
src/module_interp.f90:724:      istart(idm) = local_time
src/module_map_utils.f90:250:    ! calling the coordinate conversion routines multiple times for the
src/output_module.F90:9:   integer                             :: time, rec, ivars   !!! process time ; rec in .dat file ; variables in .dat file
src/output_module.F90:356:    WRITE (tdef,'(i4," linear ",i2.2,"Z",i2.2,A,i4,"  ",i6,"MN")') time, hour, day, cmonth, year, interval_seconds/60
src/output_module.F90:401:      IF (time == 0) THEN
バイナリー・ファイルsrc/output_module.oは一致しました
src/process_domain_module.F90:37:      integer                                :: idiff, n_times, i
src/process_domain_module.F90:45:      ! Compute number of times that we will process
src/process_domain_module.F90:50:      ! Check that the interval evenly divides the range of times to process
src/process_domain_module.F90:51:      n_times = idiff / interval_seconds
src/process_domain_module.F90:54:                   '(end_date - start_date). Check to make sure all requested times '// &
src/process_domain_module.F90:68:      IF ( iprogram == 1 ) n_times = 0
src/process_domain_module.F90:118:      ! Loop over all times to be processed
src/process_domain_module.F90:132:      all_files : DO time=0,n_times
src/process_domain_module.F90:137:           CALL geth_newdate(valid_date, trim(start_date), time*interval_seconds)
src/process_domain_module.F90:141:         IF ( time == 0 ) tdef_date = temp_date
src/process_domain_module.F90:159:         CALL get_fields(temp_date, time) !get_fields(temp_date)
src/process_domain_module.F90:164:      ENDDO all_files  ! Loop over n_times
src/process_domain_module.F90:236:      integer                            :: good_to_go, file_time
src/process_domain_module.F90:241:      CALL mprintf(.true.,STDOUT, ' Processing  time --- %s', s1=trim(valid_date))
src/process_domain_module.F90:242:      get_right_time : DO
src/process_domain_module.F90:253:        CALL arw_get_next_time(valid_date, datestr, file_time, istatus)
src/process_domain_module.F90:259:           CYCLE get_right_time
src/process_domain_module.F90:265:           EXIT get_right_time
src/process_domain_module.F90:270:           EXIT get_right_time
src/process_domain_module.F90:276:      ENDDO get_right_time
src/process_domain_module.F90:281:         CALL get_interp_array (file_time)
src/process_domain_module.F90:286:      CALL process_basic_arrays ( file_time )
src/process_domain_module.F90:297:                             memorder, stagger, real_array, file_time, istatus)
src/process_domain_module.F90:350:                CALL get_keep_array ( file_time , good_to_go, "TMP_ARRAY", dummy2 )
src/process_domain_module.F90:359:                CALL get_keep_array ( file_time , good_to_go, "TMP_ARRAY", dummy2 )
src/process_domain_module.F90:410:        CALL process_diagnostics ( file_time )
src/process_domain_module.F90:414:!! We are DONE for this time
バイナリー・ファイルsrc/process_domain_module.oは一致しました

MyARWpost_LFC3
------------------------

EL（LNB)の3次元分布 (EL3)を得る。

デフォルトでは、高度3000m以下で最も相当温位の高い格子点から持ち上げたLCLとLFCの2次元分布しか求められない。

Sun, 10 May 2020 13:24:20 +0900
calypso.bosai.go.jp
/work06/manda/WRF3.7.1/MyARWpost_LFC3

```
srcdump.sh src/module_diagnostics.f90 src/module_calc_EL3.f90 src/Makefile configure.arwp /work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3/CHK.EL3.sh
```

**Machine info**
processor	: 15
model name	: Intel(R) Xeon(R) CPU E5-2690 0 @ 2.90GHz
MemTotal:       65988728 kB

2020-05-02_18-55 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src

```bash
2020-05-10_11-46 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
$ org.sh module_diagnostics.f90
`module_diagnostics.f90' -> `module_diagnostics.f90.ORG.200510-1146'
~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 30K May  2 19:25 module_diagnostics.f90
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 30K May 10 11:46 module_diagnostics.f90.ORG.200510-1146
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```
2020-05-10_11-46 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
```
$ cp module_calc_LFC3.f90 module_calc_EL3.f90
`module_calc_LFC3.f90' -> `module_calc_EL3.f90'
$ vi module_calc_EL3.f90
```
```
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
$ diff module_calc_LFC3.f90 module_calc_EL3.f90
1,2c1,2
< !! Diagnostics: LFC3
< !2020-05-02_18-55 
---
> !! Diagnostics: EL3 (LNB)
> !2020-05-10_11-46 
5c5
< MODULE module_calc_LFC3
---
> MODULE module_calc_EL3
12c12
<   SUBROUTINE calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, i3dflag)
---
>   SUBROUTINE calc_EL3(SCRa, cname, cdesc, cunits, i3dflag)
32c32
<   real, allocatable, dimension(:,:,:) :: SCRa, SCRb
---
>   real, allocatable, dimension(:,:,:) :: SCRa
45c45
< &       :: prsf, cape, cin, LCL3, LFC3
---
> &       :: prsf, cape, cin, EL3
269,271c269,270
< !        Now we can assign values to LCL3 and LFC3
<          LCL3(i,j,kpar) = zrel(klcl)+ghtpari-ter(i,j)   !meters AGL (LCL)
<          LFC3(i,j,kpar) = zrel(klfc)+ghtpari-ter(i,j)   !meters AGL (LFC)
---
> !        Now we can assign values to EL3
>          EL3(i,j,kpar) = zrel(kel)+ghtpari-ter(i,j)   !meters AGL (EL=LNB)
288,289c287
<     SCRa = LCL3
<     SCRb = LFC3
---
>     SCRa = EL3
297c295
<   END SUBROUTINE calc_LFC3
---
>   END SUBROUTINE calc_EL3
371c369
< END MODULE module_calc_LFC3
---
> END MODULE module_calc_EL3
```

```
$ org.sh Makefile 
`Makefile' -> `Makefile_ORG200510-1157'
~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 3.6K May  2 19:31 Makefile
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 3.6K May 10 11:57 Makefile_ORG200510-1157
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```
```
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
$ diff module_calc_LFC3.f90 module_calc_EL3.f90
1,2c1,2
< !! Diagnostics: LFC3
< !2020-05-02_18-55 
---
> !! Diagnostics: EL3 (LNB)
> !2020-05-10_11-46 
5c5
< MODULE module_calc_LFC3
---
> MODULE module_calc_EL3
12c12
<   SUBROUTINE calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, i3dflag)
---
>   SUBROUTINE calc_EL3(SCRa, cname, cdesc, cunits, i3dflag)
32c32
<   real, allocatable, dimension(:,:,:) :: SCRa, SCRb
---
>   real, allocatable, dimension(:,:,:) :: SCRa
45c45
< &       :: prsf, cape, cin, LCL3, LFC3
---
> &       :: prsf, cape, cin, EL3
269,271c269,270
< !        Now we can assign values to LCL3 and LFC3
<          LCL3(i,j,kpar) = zrel(klcl)+ghtpari-ter(i,j)   !meters AGL (LCL)
<          LFC3(i,j,kpar) = zrel(klfc)+ghtpari-ter(i,j)   !meters AGL (LFC)
---
> !        Now we can assign values to EL3
>          EL3(i,j,kpar) = zrel(kel)+ghtpari-ter(i,j)   !meters AGL (EL=LNB)
288,289c287
<     SCRa = LCL3
<     SCRb = LFC3
---
>     SCRa = EL3
297c295
<   END SUBROUTINE calc_LFC3
---
>   END SUBROUTINE calc_EL3
371c369
< END MODULE module_calc_LFC3
---
> END MODULE module_calc_EL3

2020-05-10_12-10 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
$ diff Makefile_ORG200510-1157 Makefile
19c19,20
< module_calc_LFC3.o
---
> module_calc_LFC3.o \
> module_calc_EL3.o
81c82,83
< module_calc_ept.o module_calc_LFC3.o
---
> module_calc_ept.o module_calc_LFC3.o \
> module_calc_EL3.o
101a104
> module_calc_EL3.o: module_model_basics.o constants_module.o
```

```bash
$ cd /work06/manda/WRF3.7.1/MyARWpost_LFC3
$  NETCDF=/usr/local/netcdf-3.6.3
```

```bash
$ clean -a

$ ./configure
Will use NETCDF in dir: /home/manda/local

   1.  PC Linux i486 i586 i686 x86_64, PGI compiler 
   2.  PC Linux i486 i586 i686 x86_64, Intel compiler 
   3.  PC Linux i486 i586 i686 x86_64, gfortran compiler 

Enter selection [1-3] : 2

$ ./compile 2>&1 |tee -a COMPILE_$(myymd).LOG
```
```bash
$ ulimit -s unlimited
$ MyArwpost_LFC3_K17.run.sh -c K17 -r R27 -e 00.00 -d d03 -v z

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Successful completion of ARWpost  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

======================================================
 List of /work06/manda/ARWpost/ARWpost_K17.R27.00.00_EL3
======================================================
total 9.7M
-rw-rw-r-- 1 manda manda 6.5K May 10 13:19 K17.R27.00.00.d03.z_EL3.ctl
-rw-rw-r-- 1 manda manda 4.9M May 10 13:19 K17.R27.00.00.d03.z_EL3_2017-07-05_00:00.dat
-rw-rw-r-- 1 manda manda  683 May 10 12:08 LIST_K17.R27.00.00.d03.txt
-rwxrw-r-- 1 manda manda 3.0K May 10 13:18 MyArwpost_LFC3_K17.run.sh
-rw-rw-r-- 1 manda manda  687 May 10 13:19 namelist.ARWpost_EL3.K17.R27.00.00
======================================================
```
```bash
$ pushd /work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3
/work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3
```
```
2020-05-10_13-21 
manda@calypso
/work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3
$ CHK.EL3.sh

OPEN dset /work06/manda/ARWpost/ARWpost_K17.R27.00.00_EL3/K17.R27.00.00.d03.z_EL3_%y4-%m2-%d2_%h2:%n2.dat

-rw-rw-r-- 1 manda manda 437K 2020-05-10 13:21 K17.R27.00.00_EL3_0.5_00Z05JUL2017.eps
```


**List of the following files:**
- src/module_diagnostics.f90
- src/module_calc_EL3.f90
- src/Makefile
- configure.arwp
- /work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3/CHK.EL3.sh
  
### src/module_diagnostics.f90
```
!! Diagnostics
!! See moddule_diagnostic for which fields are available 

MODULE module_diagnostics

  USE output_module
  USE gridinfo_module
  USE module_interp
  USE module_arrays
  USE constants_module


  USE module_calc_pressure
  USE module_calc_height
  USE module_calc_theta
  USE module_calc_tk
  USE module_calc_tc
  USE module_calc_td
  USE module_calc_td2
  USE module_calc_rh
  USE module_calc_rh2
  USE module_calc_uvmet
  USE module_calc_wdir
  USE module_calc_wspd
  USE module_calc_slp
  USE module_calc_dbz
  USE module_calc_cape
  USE module_calc_clfr

  USE module_calc_myrh !myrh

  use module_calc_ept  !EPT
  use module_calc_LFC3 !LFC3
  use module_calc_EL3  !EL3 (LNB)



  CONTAINS

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Name: process_diagnostics
   ! Purpose: All new calls to diagnostics routines go here
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   SUBROUTINE process_diagnostics ( local_time )

      IMPLICIT NONE

      ! Local variables
      character (len=128)                    :: cname, cunits, cdesc, c_nm
      real, allocatable, dimension(:,:)      :: SCR2
      real, allocatable, dimension(:,:,:)    :: SCR3
      real, allocatable, dimension(:,:,:)    :: SCR, SCRa, SCRb, SCRc
      real, allocatable, dimension(:,:,:)    :: data_out 
      integer                                :: nxout, nyout, nzout
      integer                                :: ii, jj, kk
      integer                                :: good_to_go, geo
      integer                                :: local_time

! Equivalent Potential Temperature
        IF ( INDEX(plot_these_fields,",ept,") /= 0 ) THEN  !EPT 
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_ept(SCR, cname, cdesc, cunits)  !EPT
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate pressure in hPA
        IF ( INDEX(plot_these_fields,",pressure,") /= 0 ) THEN
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_pressure(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate height/geopt 
        IF ( INDEX(plot_these_fields,",geopt,") /= 0) THEN      
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",height,") /= 0 ) THEN 
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            cname = "height"
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate temperature 
        IF ( INDEX(plot_these_fields,",tk,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tk(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",tc,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tc(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate theta
        IF ( INDEX(plot_these_fields,",theta,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T ) CALL get_keep_array ( local_time, good_to_go, "T" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_theta(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature
        IF ( INDEX(plot_these_fields,",td,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_td(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature at 2m
        IF ( INDEX(plot_these_fields,",td2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_td2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF

!myrh
        myrh1: IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_myrh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
        END IF myrh1

!!! Relative Humidity    
        IF ( INDEX(plot_these_fields,",rh,")   /= 0 .OR. &
             INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_rh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",rh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            !!!  Cloud fractions
            IF ( INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
              IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
              ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
              SCRa = 0.0
              IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
              ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
              SCRb = 0.0
              IF ( ALLOCATED(SCRc) ) DEALLOCATE(SCRc)
              ALLOCATE(SCRc(west_east_dim,south_north_dim,1))
              SCRc = 0.0
              CALL calc_clfr(SCRa, SCRb, SCRc, cname, cdesc, cunits, SCR) 
              cname = "clflo"
              cdesc = "Low Cloud Fraction"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfmi"
              cdesc = "Mid Cloud Fraction"
              CALL interp (SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfhi"
              cdesc = "High Cloud Fraction"
              CALL interp (SCRc, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

              DEALLOCATE(SCRa)
              DEALLOCATE(SCRb)
              DEALLOCATE(SCRc)
            END IF
            DEALLOCATE(SCR)
          END IF
        END IF

        IF ( INDEX(plot_these_fields,",rh2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_rh2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed       
        IF ( INDEX(plot_these_fields,",wspd,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction       
        IF ( INDEX(plot_these_fields,",wdir,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed  - 10m      
        IF ( INDEX(plot_these_fields,",ws10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction  - 10m      
        IF ( INDEX(plot_these_fields,",wd10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Rotated U and V      
        IF ( INDEX(plot_these_fields,",umet,") /= 0 .OR. &
             INDEX(plot_these_fields,",vmet,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_UUU )   CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV )   CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",umet,") /= 0 ) THEN
              cname = "umet"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",vmet,") /= 0) THEN
              cname = "vmet"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


!!! Rotated U10 and V10      
        IF ( INDEX(plot_these_fields,",u10m,") /= 0 .OR. &
             INDEX(plot_these_fields,",v10m,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_U10 )   CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 )   CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 0) 

            IF ( INDEX(plot_these_fields,",u10m,") /= 0 ) THEN
              cname = "u10m"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",v10m,") /= 0) THEN
              cname = "v10m"
              CALL interp( SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF

        !!! Clobber some wind arrays
        IF (ALLOCATED(UUU)) DEALLOCATE(UUU)
        have_UUU = .FALSE.
        IF (ALLOCATED(VVV)) DEALLOCATE(VVV)
        have_VVV = .FALSE.
        IF (ALLOCATED(U10)) DEALLOCATE(U10)
        have_UUU = .FALSE.
        IF (ALLOCATED(V10)) DEALLOCATE(V10)
        have_VVV = .FALSE.



!!! Sea Level Pressure  
        IF ( INDEX(plot_these_fields,",slp,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_slp(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Reflectivity             
        IF ( INDEX(plot_these_fields,",dbz,")     /= 0 .OR. &
             INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QS )  CALL get_keep_array ( local_time, good_to_go, "QSNOW" )
          IF ( .not. have_QG )  CALL get_keep_array ( local_time, good_to_go, "QGRAUP" )
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_QR )  CALL get_keep_array ( local_time, good_to_go, "QRAIN" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_dbz(SCR, cname, cdesc, cunits) 

            IF ( INDEX(plot_these_fields,",dbz,") /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            IF ( INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
              ALLOCATE(SCR2(west_east_dim,south_north_dim))
              SCR2 = 0.0
              DO jj = 1,south_north_dim
                DO ii = 1,west_east_dim
                  DO kk = 1,bottom_top_dim
                    SCR2(ii,jj) = MAX( SCR2(ii,jj) , SCR(ii,jj,kk) )
                  END DO 
                END DO 
              END DO 
              IF (ALLOCATED(SCR)) DEALLOCATE(SCR)
              ALLOCATE(SCR(west_east_dim,south_north_dim,1))
              SCR(:,:,1) = SCR2(:,:)
              cname = "max_dbz"
              cdesc    = "Max Reflectivity"
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              DEALLOCATE(SCR2)
            END IF
            DEALLOCATE(SCR)

          END IF
        END IF



!!! 3D CAPE/CIN 
        IF ( INDEX(plot_these_fields,",cape,") /= 0 .OR. &
             INDEX(plot_these_fields,",cin,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",cape,") /= 0 ) THEN
              cname = "cape"
              cdesc = "CAPE"
              cunits = "J/kg"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",cin,") /= 0) THEN
              cname = "cin"
              cdesc = "CIN"
              cunits = "J/kg"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 3D LCL/LFC
!2020-05-02_18-55 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
        IF ( INDEX(plot_these_fields,",LCL3,") /= 0 .OR. &
             INDEX(plot_these_fields,",LFC3,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",LCL3,") /= 0 ) THEN
              cname = "LCL3"
              cdesc = "LCL3"
              cunits = "meters AGL"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",LFC3,") /= 0) THEN
              cname = "LFC3"
              cdesc = "LFC3"
              cunits = "meters AGL"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 3D EL (LNB)
!2020-05-10_11-46 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
        IF ( INDEX(plot_these_fields,",EL3,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            CALL calc_EL3(SCRa, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",EL3,") /= 0 ) THEN
              cname = "EL3"
              cdesc = "LNB"
              cunits = "meters AGL"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
          END IF
        END IF



!!! 2D CAPE/CIN & LCL/LFC
        IF ( INDEX(plot_these_fields,",mcape,") /= 0 .OR. &
             INDEX(plot_these_fields,",mcin,")  /= 0 .OR.  &
             INDEX(plot_these_fields,",lcl,")   /= 0 .OR.   &
             INDEX(plot_these_fields,",lfc,")   /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 0) 

            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            IF ( INDEX(plot_these_fields,",mcape,") /= 0 ) THEN
              cname = "mcape"
              cdesc = "MCAPE"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,1)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",mcin,") /= 0) THEN
              cname = "mcin"
              cdesc = "MCIN"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,2)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lcl,") /= 0) THEN
              cname = "lcl"
              cdesc = "LCL"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,3)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lfc,") /= 0) THEN
              cname = "lfc"
              cdesc = "LFC"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,4)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


       IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)

   END SUBROUTINE process_diagnostics

END MODULE module_diagnostics





```

### src/module_calc_EL3.f90
```
!! Diagnostics: EL3 (LNB)
!2020-05-10_11-46 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
MODULE module_calc_EL3

  real, dimension(150)     :: buoy, zrel, benaccum
  real, dimension(150)     :: psadithte, psadiprs
  real, dimension(150,150) :: psaditmk

  CONTAINS
  SUBROUTINE calc_EL3(SCRa, cname, cdesc, cunits, i3dflag)

!   If i3dflag=1, this routine calculates CAPE and CIN (in m**2/s**2,
!   or J/kg) for every grid point in the entire 3D domain (treating
!   each grid point as a parcel).  If i3dflag=0, then it
!   calculates CAPE and CIN only for the parcel with max theta-e in
!   the column, (i.e. something akin to Colman's MCAPE).  By "parcel",
!   we mean a 500-m deep parcel, with actual temperature and moisture
!   averaged over that depth.
!
!   In the case of i3dflag=0,
!   MCAPE, MCIN, LCL and LFC (2D fields are calculated)


  USE constants_module
  USE module_model_basics

  IMPLICIT NONE

  !Arguments
  real, allocatable, dimension(:,:,:) :: SCRa
  character (len=128)                 :: cname, cdesc, cunits
  integer                             :: i3dflag

  ! Local variables
  integer :: i, j, k, kk, jt, ip, iustnlist
  integer :: kpar, kpar1, kpar2, kmax, klev, kel
  integer :: ilcl, klcl, klfc
  integer :: nprs, nthte
  real, dimension(west_east_dim,south_north_dim)               :: ter
  real, dimension(west_east_dim,south_north_dim,bottom_top_dim)&
&       :: prs, tmk, ght, qvp
  real, dimension(west_east_dim,south_north_dim,bottom_top_dim)&
&       :: prsf, cape, cin, EL3
  real :: ethpari, zlcl, tvenv
  real :: p1, p2, pp1, pp2, pup, pdn
  real :: totprs, totqvp, totthe
  real :: eslift, ghtlift, qvplift, tmklift, tvlift
  real :: ghtpari, prspari, qvppari, tmkpari
  real :: tmkenv, qvpenv, tlcl
  real :: fac1, fac2, facden, th, deltap
  real :: benamin, davg, pavg, pressure, temp
  real :: e, eth, ethmax, q, dz, cpm
  character (len=20) :: fname
  logical :: is_used

  
     ! Open psadilookup.dat     
     DO iustnlist = 10,100
        INQUIRE(unit=iustnlist, opened=is_used)
        if (.not. is_used) exit
     END DO
     fname = 'src/psadilookup.dat' 
     OPEN (unit=iustnlist,file=fname,form='formatted',status='old')

     DO i = 1,14
       READ (iustnlist,*)
     ENDDO
     READ (iustnlist,*) nthte,nprs
     IF ( nthte.ne.150 .OR. nprs.ne.150 ) then
       PRINT*, 'Number of pressure or theta_e levels in lookup table'
       PRINT*, '     file not = 150.  Check lookup table file.'
       STOP
     ENDIF
     READ (iustnlist,173) (psadithte(jt),jt=1,nthte) 
     READ (iustnlist,173) (psadiprs(ip),ip=1,nprs)
     READ (iustnlist,173) ((psaditmk(ip,jt),ip=1,nprs),jt=1,nthte)
 173 FORMAT (5e15.7)
     CLOSE (iustnlist)  


  !! Get fields we want from the ones we have
     ter      = HGT
     qvp      = QV
     prs      = PRES * 0.01                ! pressure in hPa
     tmk      = TK                         ! temperature in K
     ght      = GEOPT / G                  ! height in m


  !! First calculate a pressure array on FULL SIGMA levels
  !! Similar to the pfcalc.f routine from RIP4
  !! Top full sigma level is ommitted
     prsf(:,:,1) = PSFC(:,:)             !! Lowest full sigma set to surface pressure
     DO k = 2, bottom_top_dim
       prsf(:,:,k)=.5*(prs(:,:,k-1)+prs(:,:,k))
     END DO

     
     cape = 0.0
     cin  = 0.0

     DO j = 1,south_north_dim          !! BIG i/j loop
     DO i = 1,west_east_dim

       !! 3D case
       kpar1=bottom_top_dim-1
       kpar2=1

       DO kpar = kpar1,kpar2,-1
 
!   Calculate temperature and moisture properties of parcel


         qvppari = qvp(i,j,kpar)
         tmkpari = tmk(i,j,kpar)

         prspari = prs(i,j,kpar)
         ghtpari = ght(i,j,kpar)
         cpm     = cp*(1.+CPMD*qvppari)
 
         e       = max(1.e-20,qvppari*prspari/(EPS+qvppari))
         tlcl    = TLCLC1/(log(tmkpari**TLCLC2/e)-TLCLC3)+TLCLC4
         ethpari = tmkpari*(1000./prspari)**(GAMMA_RIP*(1.+GAMMAMD*qvppari))*   &
                   exp((THTECON1/tlcl-THTECON2)*qvppari*                    &
                   (1.+THTECON3*qvppari))
         zlcl    = ghtpari+(tmkpari-tlcl)/(G/cpm)

!   Calculate buoyancy and relative height of lifted parcel at
!   all levels, and store in bottom up arrays.  Add a level at the LCL,
!   and at all points where buoyancy is zero.
 
         kk = 0                    ! for arrays that go bottom to top
         ilcl = 0
         IF ( ghtpari .ge. zlcl ) THEN
           !! initial parcel already saturated or supersaturated.
           ilcl = 2
           klcl = 1
         END IF

         DO k = kpar,bottom_top_dim
 33        kk=kk+1                ! for arrays that go bottom to top

           IF ( ght(i,j,k) .lt. zlcl ) THEN ! model level is below LCL
             qvplift = qvppari
             tmklift = tmkpari-G/cpm*(ght(i,j,k)-ghtpari)
             tvenv   = virtual(tmk(i,j,k),qvp(i,j,k))
             tvlift  = virtual(tmklift,qvplift)
             ghtlift = ght(i,j,k)
           ELSE IF ( ght(i,j,k) .ge. zlcl .AND. ilcl .eq. 0 ) THEN
             !! This model level and previous model level straddle the LCL,
             !! so first create a new level in the bottom-up array, at the LCL.
             tmklift = tlcl
             qvplift = qvppari
             facden  = ght(i,j,k)-ght(i,j,k-1)
             fac1    = (zlcl-ght(i,j,k-1))/facden
             fac2    = (ght(i,j,k)-zlcl)/facden
             tmkenv  = tmk(i,j,k-1)*fac2+tmk(i,j,k)*fac1
             qvpenv  = qvp(i,j,k-1)*fac2+qvp(i,j,k)*fac1
             tvenv   = virtual(tmkenv,qvpenv)
             tvlift  = virtual(tmklift,qvplift)
             ghtlift = zlcl
             ilcl    = 1
           ELSE
             tmklift = tonpsadiabat(ethpari,prs(i,j,k))                                
             eslift  = EZERO*exp(eslcon1*(tmklift-CELKEL)/    &
                       (tmklift-eslcon2))
             qvplift = EPS*eslift/(prs(i,j,k)-eslift)
             tvenv   = virtual(tmk(i,j,k),qvp(i,j,k))
             tvlift  = virtual(tmklift,qvplift)
             ghtlift = ght(i,j,k)
           END IF

           buoy(kk) = G*(tvlift-tvenv)/tvenv  ! buoyancy
           zrel(kk) = ghtlift-ghtpari

           IF ( (buoy(kk)*buoy(kk-1).lt.0.0) .AND. (kk.gt.1) ) THEN
             !! Parcel ascent curve crosses sounding curve, so create a new level
             !! in the bottom-up array at the crossing.
             kk = kk+1
             buoy(kk)   = buoy(kk-1)
             zrel(kk)   = zrel(kk-1)
             buoy(kk-1) = 0.
             zrel(kk-1) = zrel(kk-2) + buoy(kk-2)/(buoy(kk-2)-buoy(kk))*  &
                          (zrel(kk)-zrel(kk-2))
           END IF

           IF (ilcl == 1) THEN
             klcl = kk !**KLCL**
             ilcl = 2
             GOTO 33
           END IF

         END DO         !! END DO k = kpar,bottom_top_dim

         kmax = kk
         IF (kmax .gt. 150) THEN
           PRINT*, 'in calc_cape: kmax got too big. kmax=',kmax
           STOP
         ENDIF

 
!        Get the accumulated buoyant energy from the parcel's starting
!        point, at all levels up to the top level.

         benaccum(1) = 0.0
         benamin     = 9e9
         DO k = 2,kmax
           dz          = zrel(k)-zrel(k-1)
           benaccum(k) = benaccum(k-1)+.5*dz*(buoy(k-1)+buoy(k))
             IF ( benaccum(k) .lt. benamin ) THEN
               benamin = benaccum(k)
             END IF
         END DO


!        Determine equilibrium level (EL), which we define as the highest
!        level of non-negative buoyancy above the LCL. Note, this may be
!        the top level if the parcel is still buoyant there.

         DO k = kmax,klcl,-1
           IF ( buoy(k) .ge. 0. ) THEN
             kel = k   ! **KEL** k of equilibrium level
             GOTO 50
           END IF
         END DO


!        If we got through that loop, then there is no non-negative
!        buoyancy above the LCL in the sounding.  In these situations,
!        both CAPE and CIN will be set to -0.1 J/kg.  Also, where CAPE is
!        non-zero, CAPE and CIN will be set to a minimum of +0.1 J/kg, so
!        that the zero contour in either the CIN or CAPE fields will
!        circumscribe regions of non-zero CAPE.
 
         cape(i,j,kpar) = -0.1
         cin(i,j,kpar)  = -0.1
         klfc = kmax !**KLFC**
 
         GOTO 102
 
 50      CONTINUE

!        If there is an equilibrium level, then CAPE is positive.  We'll
!        define the level of free convection (LFC) as the point below the
!        EL, but at or above the LCL, **where accumulated buoyant energy is a
!        minimum**.  The net positive area (accumulated buoyant energy) from
!        the LFC up to the EL will be defined as the CAPE, and the net
!        negative area (negative of accumulated buoyant energy) from the
!        parcel starting point to the LFC will be defined as the convective
!        inhibition (CIN).
 
!        First get the LFC according to the above definition.
 
         benamin = 9e9
         klfc = kmax
         DO k = klcl,kel
           IF ( benaccum(k) .lt. benamin ) THEN
             benamin = benaccum(k)
             klfc = k
           END IF
         END DO

!        Now we can assign values to cape and cin
 
         cape(i,j,kpar) = max(benaccum(kel)-benamin,0.1)
         cin(i,j,kpar)  = max(-benamin,0.1)

!        Now we can assign values to EL3
         EL3(i,j,kpar) = zrel(kel)+ghtpari-ter(i,j)   !meters AGL (EL=LNB)

!        CIN is uninteresting when CAPE is small (< 100 J/kg), so set
!        CIN to -.1 in that case.
 
         IF ( cape(i,j,kpar) .lt. 100. ) cin(i,j,kpar) = -0.1

 102     CONTINUE

       ENDDO          !! END of BIG 2D/3D loop

     END DO
     END DO                !! END BIG i/j loop

!! These will be set by module_diagnostics as we have more than 1 field

  IF ( i3dflag == 1 ) THEN
    SCRa = EL3
  ENDIF

  cname    = " "
  cdesc    = " "
  cunits   = " "
  

  END SUBROUTINE calc_EL3


!*********************************************************************c
!*********************************************************************c
  FUNCTION tonpsadiabat (thte,prs)
 
  USE constants_module
 
!   This function gives the temperature (in K) on a moist adiabat
!   (specified by thte in K) given pressure in hPa.  It uses a
!   lookup table, with data that was generated by the Bolton (1980)
!   formula for theta_e.

 
!     First check if pressure is less than min pressure in lookup table.
!     If it is, assume parcel is so dry that the given theta-e value can
!     be interpretted as theta, and get temperature from the simple dry
!     theta formula.
 
      IF ( prs .le. psadiprs(150) ) THEN
        tonpsadiabat = thte*(prs/1000.)**GAMMA_RIP
        RETURN
      ENDIF
 

!     Otherwise, look for the given thte/prs point in the lookup table.
 
      DO jtch = 1,150-1
        IF ( thte.ge.psadithte(jtch) .AND. thte.lt.psadithte(jtch+1) ) THEN
          jt = jtch
          GOTO 213
        END IF
      END DO
      jt = -1
 213  CONTINUE

      DO ipch = 1,150-1
        if ( prs.le.psadiprs(ipch) .AND. prs.gt.psadiprs(ipch+1) ) THEN
          ip = ipch
          GOTO 215
        END IF
      ENDDO
      ip = -1
 215  CONTINUE


      IF ( jt.eq.-1 .OR. ip.eq.-1 ) THEN
        PRINT*, 'Outside of lookup table bounds. prs,thte=',prs,thte
        STOP 
      ENDIF


      fracjt  = (thte-psadithte(jt))/(psadithte(jt+1)-psadithte(jt))
      fracjt2 = 1.-fracjt
      fracip  = (psadiprs(ip)-prs)/(psadiprs(ip)-psadiprs(ip+1))
      fracip2 = 1.-fracip

      IF ( psaditmk(ip,jt  ).gt.1e9 .OR. psaditmk(ip+1,jt  ).gt.1e9 .OR.   &
           psaditmk(ip,jt+1).gt.1e9 .OR. psaditmk(ip+1,jt+1).gt.1e9 ) THEN
        PRINT*, 'Tried to access missing tmperature in lookup table.'
        PRINT*, 'Prs and Thte probably unreasonable. prs,thte=',prs,thte
        STOP
      ENDIF

      tonpsadiabat = fracip2*fracjt2*psaditmk(ip  ,jt  )+   &
                     fracip *fracjt2*psaditmk(ip+1,jt  )+   &
                     fracip2*fracjt *psaditmk(ip  ,jt+1)+   &
                     fracip *fracjt *psaditmk(ip+1,jt+1)
      

  END FUNCTION tonpsadiabat 


END MODULE module_calc_EL3
```

### src/Makefile
```

include ../configure.arwp

OBJS = module_model_basics.o constants_module.o \
gridinfo_module.o ARWpost.o input_module.o \
output_module.o module_map_utils.o \
misc_definitions_module.o module_date_pack.o \
module_debug.o process_domain_module.o \
module_get_file_names.o module_interp.o \
module_basic_arrays.o module_diagnostics.o \
module_arrays.o module_pressure.o module_calc_height.o \
module_calc_pressure.o module_calc_theta.o \
module_calc_tk.o module_calc_tc.o module_calc_td.o \
module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o \
module_calc_cape.o module_calc_wdir.o module_calc_wspd.o \
module_calc_clfr.o module_calc_myrh.o module_calc_ept.o \
module_ept_bolton80.o \
module_calc_LFC3.o \
module_calc_EL3.o


wrong: 
	clear ;
	@echo " "
	@echo "go up one directory and type compile to build ARWpost"
	@echo " "
	@echo " "


all: ARWpost.exe

ARWpost.exe: $(OBJS)
	$(FC) $(FFLAGS) $(LDFLAGS) -o $@ $(OBJS)  \
		-L$(NETCDF)/lib -I$(NETCDF)/include  \
	-lnetcdf


module_model_basics.o:

constants_module.o:

misc_definitions_module.o:

module_date_pack.o:

module_get_file_names.o:

module_arrays.o: module_model_basics.o gridinfo_module.o

module_pressure.o: module_model_basics.o

module_interp.o: input_module.o module_arrays.o module_pressure.o

module_map_utils.o:

module_debug.o:

gridinfo_module.o: misc_definitions_module.o module_debug.o module_get_file_names.o

input_module.o: gridinfo_module.o misc_definitions_module.o 

output_module.o: input_module.o module_model_basics.o module_arrays.o module_interp.o

process_domain_module.o: module_date_pack.o gridinfo_module.o input_module.o \
output_module.o misc_definitions_module.o module_debug.o module_interp.o \
module_basic_arrays.o module_diagnostics.o module_arrays.o \
module_model_basics.o module_pressure.o

ARWpost.o: gridinfo_module.o module_debug.o process_domain_module.o


module_basic_arrays.o: gridinfo_module.o module_arrays.o module_pressure.o \
module_interp.o constants_module.o

module_diagnostics.o: gridinfo_module.o output_module.o module_arrays.o \
module_interp.o module_pressure.o constants_module.o module_calc_height.o \
module_calc_pressure.o module_calc_tk.o module_calc_tc.o module_calc_theta.o \
module_calc_td.o module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o module_calc_cape.o \
module_calc_wdir.o module_calc_wspd.o module_calc_clfr.o module_calc_myrh.o \
module_calc_ept.o module_calc_LFC3.o \
module_calc_EL3.o

module_calc_cape.o: module_model_basics.o constants_module.o
module_calc_dbz.o: module_model_basics.o constants_module.o
module_calc_height.o: module_model_basics.o constants_module.o
module_calc_pressure.o: module_model_basics.o
module_calc_slp.o: module_model_basics.o constants_module.o
module_calc_rh.o: module_model_basics.o constants_module.o
module_calc_rh2.o: module_model_basics.o constants_module.o
module_calc_tk.o: module_model_basics.o constants_module.o
module_calc_tc.o: module_model_basics.o constants_module.o
module_calc_td.o: module_model_basics.o
module_calc_td2.o: module_model_basics.o
module_calc_theta.o: module_model_basics.o
module_calc_uvmet.o: module_model_basics.o
module_calc_wdir.o: module_model_basics.o
module_calc_wspd.o: module_model_basics.o
module_calc_clfr.o: module_model_basics.o
module_calc_myrh.o: module_model_basics.o constants_module.o
module_calc_ept.o: module_model_basics.o constants_module.o module_ept_bolton80.o
module_calc_LFC3.o: module_model_basics.o constants_module.o
module_calc_EL3.o: module_model_basics.o constants_module.o


clean:
	rm -f $(OBJS) *.mod

clobber:
	rm -f $(OBJS) *.mod
	rm -f *.f
	rm -f ARWpost.exe 
```

### configure.arwp
```
# configure.arwp
#
# This file was automatically generated by the configure script in the
# top level directory. You may make changes to the settings in this
# file but be aware they will be overwritten each time you run configure.
# Ordinarily, it is necessary to run configure once, when the code is
# first installed.
#
# To permanently change options, change the settings for your platform
# in the file arch/configure.defaults, the preamble, and the postamble -
# then rerun configure.
#

.SUFFIXES: .F90 .f90 .F .f .c .o

SHELL           	=       /bin/sh

# Listing of options that are usually independent of machine type.
# When necessary, these are over-ridden by each architecture.

ARFLAGS			=	

PERL			=	perl

RANLIB			=	echo

#### Architecture specific settings ####

# Settings for PC Linux i486 i586 i686 x86_64, Intel compiler	
#
FC		=	ifort
FFLAGS		=	-FR -convert big_endian
F77FLAGS	=	-convert big_endian
FNGFLAGS	=	$(FFLAGS)
LDFLAGS		=	
CC		=	 gcc -DFSEEKO64_OK
CFLAGS		=	
CPP		=	/lib/cpp -traditional
CPPFLAGS	=	-DIO_NETCDF -DIO_GRIB1 -DIO_BINARY -Dbytesw 

###########################################################
#
#	Macros, these should be generic for all machines

LN		=	ln -sf
MAKE		=	make -i -r
RM		= 	/bin/rm -f
CP		= 	/bin/cp
AR		=	ar ru


.IGNORE:
.SUFFIXES: .c .f90 .F90 .f .F .o

#	There is probably no reason to modify these rules

.c.o:
	$(RM) $@
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $<	

.f90.o:
	$(RM) $@ $*.mod
	$(CP) $< $*.f
	$(FC) $(FFLAGS) -I${NETCDF}/include -c $*.f 
	$(RM) $*.f

.F90.o:
	$(RM) $@ $*.mod
	$(CPP) $(CPPFLAGS) $(FDEFS) $< > $*.f
	$(FC) $(FFLAGS) -I${NETCDF}/include -c $*.f 
	$(RM) $*.f
```

### /work05/manda/WRF.POST/K17/GRADS.2004/CHK.LFC3/CHK.EL3.sh
```
#!/bin/bash

CASE=K17
RUN=R27
EXP=00.00
VAR1=EL3

runname=${CASE}.${RUN}.${EXP}

INDIR=/work06/manda/ARWpost/ARWpost_${runname}_${VAR1}
domain=d03

z_or_p=z
CTL=${runname}.${domain}.${z_or_p}_${VAR1}.ctl


TIME=00Z05JUL2017
LEV=0.5 #1000 #950
VAR=$VAR1
LONW=130
LONE=131.25
LATS=33
LATN=34

FIG=${runname}_${VAR}_${LEV}_${TIME}.eps

levs='9000 14000 500'
kind='white->lightyellow->wheat->gold->orange->tomato->red->firebrick'

export LANG=C
host=$(hostname)
cwd=$(pwd)
timestamp=$(date -R)
CMD="$0 $@"
GS=$(basename $0 .sh).gs

cat <<EOF > $GS
'open $INDIR/$CTL'
say

'q ctlinfo'
say 'OPEN 'sublin(result,1)
say

'cc'
'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $TIME'

'set mpdset hires'
'set grid off'
'set grads off'

'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=1

ytop=9

xwid =  7/xmax
ywid =  9/ymax
xmargin=0.7
ymargin=0.5

ymap=1
xmap=1

xs = 0.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'color ${levs} -kind ${kind} -gxout shaded'

'set xlint 0.5'
'set ylint 0.5'

'd ${VAR1}(lev=${LEV})'
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

x=xl+0.5 ;# (xl+xr)/2
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR1} ${LEV}'

x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

'q gxinfo'
#say result
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2

'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2

yy=yt+0.8
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${timestamp}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${host}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${cwd}'

x1 = 0.5
x2 = 0.5 + (xwid)*(xmax)
x=(x1+x2)/2
y=yt+0.6
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y'  ${runname}'
y=yt+0.4
'draw string 'x' 'y'  ${datetime} ${domain}'


'gxprint $FIG'

say
'!ls -lh --time-style=long-iso $FIG'
say
'quit'
EOF

grads -bcp "$GS"
```


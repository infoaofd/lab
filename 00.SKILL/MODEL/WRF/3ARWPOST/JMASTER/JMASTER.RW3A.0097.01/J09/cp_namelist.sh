#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd30
# Directory: /work2/am/WRF/ARWpost
#
# Revision history:
#  This file is created by /usr/local/mybin/nbscr.sh at 06:22 on 02-28-2012.


echo "Shell script, $(basename $0) starts."
echo

dir="namelist"
if [ ! -d $dir ]; then
  mkdir -p ${dir}
  echo Directory, ${dir} created.
fi

cp -v namelist.ARWpost namelist/namelist.ARWpost_$(date "+%Y-%m-%d_%H%M")

# exe=
# ${exe}

# Handling options
# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0

#if [ 0 -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ 0 -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi


#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi


#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out

# Sample of do-while loop
#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done

# Sample of for loop
# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done

# Sample of if block
#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

echo "Done $0"
echo

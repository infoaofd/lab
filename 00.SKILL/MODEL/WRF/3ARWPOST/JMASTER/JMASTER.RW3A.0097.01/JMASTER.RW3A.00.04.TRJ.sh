RLIST="\
RW3A.00.04.05.05.0000.02 \
RW3A.00.04.05.05.0000.03 \
RW3A.00.04.05.05.0802.01 \
RW3A.00.04.05.05.0802.02 \
RW3A.00.04.05.05.0802.03 \
"
#RW3A.00.04.05.05.0000.01 \

OROOT=/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.0000.01/

for RUN in $RLIST;do

ODIR=${OROOT}/${RUN}
mkdir -vp $ODIR

INDIR=${RUN}.d01.traj.10MN 

cd $INDIR
jmaster -q CS64 -P zamanda -np 10 -e log.err -o log.out ./JOBLIST.sh &
cd ..
done

# https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/MODEL/WRF/ARWpost_TIPS.md?ref_type=heads
# jmasterというコマンドを使い，処理する時間帯を分割して，ARWpost.exeを実行する。


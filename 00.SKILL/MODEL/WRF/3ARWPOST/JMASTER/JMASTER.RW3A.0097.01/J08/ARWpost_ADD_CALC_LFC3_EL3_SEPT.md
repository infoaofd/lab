ARWpost_ADD_CALC_LFC3_EL3_SEPT.md
------------------------

SATURATED EPT

2020-05-14_09-36 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_SEPT/src
$ org.sh module_diagnostics.f90
`module_diagnostics.f90' -> `module_diagnostics.f90.ORG.200514-0937'

~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 31K May 10 11:49 module_diagnostics.f90
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 31K May 14 09:37 module_diagnostics.f90.ORG.200514-0937
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



/work06/manda/WRF3.7.1/MyARWpost_SEPT/src
$ org.sh Makefile
`Makefile' -> `Makefile_ORG200514-0937'

~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 3.7K May 10 12:00 Makefile
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 3.7K May 14 09:37 Makefile_ORG200514-0937

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


```
/work06/manda/WRF3.7.1/MyARWpost_SEPT
$ org.sh configure.arwp
`configure.arwp' -> `configure.arwp.ORG.200514-0937'

~~~~~~~~~~~~~~~~~~~~ SOURCE ~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 1.7K May 10 12:03 configure.arwp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ COPY ~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 1.7K May 14 09:37 configure.arwp.ORG.200514-0937
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

```
/work06/manda/WRF3.7.1/MyARWpost_SEPT/src
$ cp module_calc_ept.f90 module_calc_ept_saturated.f90
`module_calc_ept.f90' -> `module_calc_ept_saturated.f90'
```

CALPYSO
--------------------------

### COMPILE
```bash
$ export NETCDF=/usr/local/netcdf-3.6.3
```
```
$ clean -a
```
```
$ ./configure
Will use NETCDF in dir: /home/manda/local

   1.  PC Linux i486 i586 i686 x86_64, PGI compiler 
   2.  PC Linux i486 i586 i686 x86_64, Intel compiler 
   3.  PC Linux i486 i586 i686 x86_64, gfortran compiler 

Enter selection [1-3] : 2

$ ./compile 2>&1 |tee -a COMPILE_$(myymd).LOG
```
```
ifort -FR -convert big_endian  -o ARWpost.exe module_model_basics.o constants_module.o gridinfo_module.o 
.....
module_calc_ept_sat.o  \
                -L/usr/local/netcdf-3.6.3/lib -I/usr/local/netcdf-3.6.3/include  \
        -lnetcdf
```
$ ll ARWpost.exe
lrwxrwxrwx 1 manda manda 15 2020-05-14 12:22 ARWpost.exe -> src/ARWpost.exe*


### RUN
/work06/manda/WRF3.7.1/MyARWpost_SEPT
$ MyArwpost_SEPT_K17.run.sh -c K17 -r R27 -e 00.00 -d d03 -v p
```
K17 R27 00.00 d03 p
Thu, 14 May 2020 12:29:37 +0900
/work06/manda/WRF3.7.1/MyARWpost_SEPT
======================================================
 List of /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.00.00_EL3
======================================================
total 38M
-rw-rw-r-- 1 manda manda 6.4K May 14 12:29 K17.R27.00.00.d03.p_SEPT.ctl
-rw-rw-r-- 1 manda manda  33M May 14 12:29 K17.R27.00.00.d03.p_SEPT_2017-07-05_00:00.dat
-rw-rw-r-- 1 manda manda 6.5K May 10 13:21 K17.R27.00.00.d03.z_EL3.ctl
-rw-rw-r-- 1 manda manda 4.9M May 10 13:19 K17.R27.00.00.d03.z_EL3_2017-07-05_00:00.dat
-rw-rw-r-- 1 manda manda  920 May 10 13:19 LIST_K17.R27.00.00.d03.txt
-rwxrw-r-- 1 manda manda 3.0K May 10 13:18 MyArwpost_LFC3_K17.run.sh
-rwxrw-r-- 1 manda manda 3.0K May 14 12:28 MyArwpost_SEPT_K17.run.sh
-rw-rw-r-- 1 manda manda  687 May 10 13:19 namelist.ARWpost_EL3.K17.R27.00.00
-rw-rw-r-- 1 manda manda  786 May 14 12:29 namelist.ARWpost_SEPT.K17.R27.00.00
======================================================
```

### CHECK
```
2020-05-14_12-44 
manda@calypso
/work05/manda/WRF.POST/K17/GRADS.2004/P.d03.EPT_SEPT
$ P.d03.SEPT.sh K17.R27.00.00
```


### SOURCE FILES
2020-05-14_21-37 
manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_SEPT/src

**Machine info**
processor	: 15
model name	: Intel(R) Xeon(R) CPU E5-2690 0 @ 2.90GHz
MemTotal:       65988728 kB

```
$ diff module_diagnostics.f90.ORG.200514-0937 module_diagnostics.
f90
32a33
>   use module_calc_ept_sat !SATURATED EPT
72a74,86
> ! SATURATED Equivalent Potential Temperature
>         IF ( INDEX(plot_these_fields,",sept,") /= 0 ) THEN  !SEPT 
>           IF ( have_PRES ) THEN 
>             IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
>             ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
>             CALL calc_sept(SCR, cname, cdesc, cunits)  !SEPT
>             CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
>                          data_out, nxout, nyout, nzout, &
>                          vert_array, interp_levels, number_of_zlevs,cname) 
>             CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
>             DEALLOCATE(SCR)
>           END IF
>         END IF
```

manda@calypso
/work06/manda/WRF3.7.1/MyARWpost_SEPT/src
```
$ diff Makefile_ORG200514-0937 Makefile
20c20,21
< module_calc_EL3.o
---
> module_calc_EL3.o \
> module_calc_ept_sat.o
83c84,85
< module_calc_EL3.o
---
> module_calc_EL3.o \
> module_calc_ept_sat.o
105c107
< 
---
> module_calc_ept_sat.o: module_model_basics.o constants_module.o 
> 
```

```
srcdump.sh module_diagnostics.f90 module_calc_ept_sat.f90 Makefile
```

**List of the following files:**
- module_diagnostics.f90
- module_calc_ept_sat.f90
- Makefile
  
### module_diagnostics.f90
```
!! Diagnostics
!! See moddule_diagnostic for which fields are available 

MODULE module_diagnostics

  USE output_module
  USE gridinfo_module
  USE module_interp
  USE module_arrays
  USE constants_module


  USE module_calc_pressure
  USE module_calc_height
  USE module_calc_theta
  USE module_calc_tk
  USE module_calc_tc
  USE module_calc_td
  USE module_calc_td2
  USE module_calc_rh
  USE module_calc_rh2
  USE module_calc_uvmet
  USE module_calc_wdir
  USE module_calc_wspd
  USE module_calc_slp
  USE module_calc_dbz
  USE module_calc_cape
  USE module_calc_clfr

  USE module_calc_myrh !myrh

  use module_calc_ept  !EPT
  use module_calc_ept_sat !SATURATED EPT
  use module_calc_LFC3 !LFC3
  use module_calc_EL3  !EL3 (LNB)



  CONTAINS

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Name: process_diagnostics
   ! Purpose: All new calls to diagnostics routines go here
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   SUBROUTINE process_diagnostics ( local_time )

      IMPLICIT NONE

      ! Local variables
      character (len=128)                    :: cname, cunits, cdesc, c_nm
      real, allocatable, dimension(:,:)      :: SCR2
      real, allocatable, dimension(:,:,:)    :: SCR3
      real, allocatable, dimension(:,:,:)    :: SCR, SCRa, SCRb, SCRc
      real, allocatable, dimension(:,:,:)    :: data_out 
      integer                                :: nxout, nyout, nzout
      integer                                :: ii, jj, kk
      integer                                :: good_to_go, geo
      integer                                :: local_time

! Equivalent Potential Temperature
        IF ( INDEX(plot_these_fields,",ept,") /= 0 ) THEN  !EPT 
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_ept(SCR, cname, cdesc, cunits)  !EPT
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF

! SATURATED Equivalent Potential Temperature
        IF ( INDEX(plot_these_fields,",sept,") /= 0 ) THEN  !SEPT 
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_sept(SCR, cname, cdesc, cunits)  !SEPT
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF

!!! Calculate pressure in hPA
        IF ( INDEX(plot_these_fields,",pressure,") /= 0 ) THEN
          IF ( have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_pressure(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate height/geopt 
        IF ( INDEX(plot_these_fields,",geopt,") /= 0) THEN      
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",height,") /= 0 ) THEN 
          IF ( have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            cname = "height"
            CALL calc_height(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate temperature 
        IF ( INDEX(plot_these_fields,",tk,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tk(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF
        IF ( INDEX(plot_these_fields,",tc,") /= 0 ) THEN
          IF ( have_TK ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_tc(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Calculate theta
        IF ( INDEX(plot_these_fields,",theta,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T ) CALL get_keep_array ( local_time, good_to_go, "T" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_theta(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature
        IF ( INDEX(plot_these_fields,",td,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_td(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Dewpoint temperature at 2m
        IF ( INDEX(plot_these_fields,",td2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_td2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF

!myrh
        myrh1: IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_myrh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",myrh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
        END IF myrh1

!!! Relative Humidity    
        IF ( INDEX(plot_these_fields,",rh,")   /= 0 .OR. &
             INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV ) CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_rh(SCR, cname, cdesc, cunits) 
            IF ( INDEX(plot_these_fields,",rh,")   /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            !!!  Cloud fractions
            IF ( INDEX(plot_these_fields,",clfr,") /= 0 ) THEN
              IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
              ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
              SCRa = 0.0
              IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
              ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
              SCRb = 0.0
              IF ( ALLOCATED(SCRc) ) DEALLOCATE(SCRc)
              ALLOCATE(SCRc(west_east_dim,south_north_dim,1))
              SCRc = 0.0
              CALL calc_clfr(SCRa, SCRb, SCRc, cname, cdesc, cunits, SCR) 
              cname = "clflo"
              cdesc = "Low Cloud Fraction"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfmi"
              cdesc = "Mid Cloud Fraction"
              CALL interp (SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              cname = "clfhi"
              cdesc = "High Cloud Fraction"
              CALL interp (SCRc, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

              DEALLOCATE(SCRa)
              DEALLOCATE(SCRb)
              DEALLOCATE(SCRc)
            END IF
            DEALLOCATE(SCR)
          END IF
        END IF

        IF ( INDEX(plot_these_fields,",rh2,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_T2 )   CALL get_keep_array ( local_time, good_to_go, "T2" )
          IF ( .not. have_Q2 )   CALL get_keep_array ( local_time, good_to_go, "Q2" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_rh2(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)

            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed       
        IF ( INDEX(plot_these_fields,",wspd,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction       
        IF ( INDEX(plot_these_fields,",wdir,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_UUU ) CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV ) CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 1) 
            CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind speed  - 10m      
        IF ( INDEX(plot_these_fields,",ws10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wspd(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Wind direction  - 10m      
        IF ( INDEX(plot_these_fields,",wd10,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_U10 ) CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 ) CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( good_to_go == 0 ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_wdir(SCR, cname, cdesc, cunits, 0) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Rotated U and V      
        IF ( INDEX(plot_these_fields,",umet,") /= 0 .OR. &
             INDEX(plot_these_fields,",vmet,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_UUU )   CALL get_keep_array ( local_time, good_to_go, "U", "UU" )
          IF ( .not. have_VVV )   CALL get_keep_array ( local_time, good_to_go, "V", "VV" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",umet,") /= 0 ) THEN
              cname = "umet"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",vmet,") /= 0) THEN
              cname = "vmet"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


!!! Rotated U10 and V10      
        IF ( INDEX(plot_these_fields,",u10m,") /= 0 .OR. &
             INDEX(plot_these_fields,",v10m,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_U10 )   CALL get_keep_array ( local_time, good_to_go, "U10" )
          IF ( .not. have_V10 )   CALL get_keep_array ( local_time, good_to_go, "V10" )
          IF ( .not. have_XLAT )  CALL get_keep_array ( local_time, good_to_go, "XLAT", "XLAT_M" )
          IF ( .not. have_XLONG ) CALL get_keep_array ( local_time, good_to_go, "XLONG", "XLONG_M" )
          IF ( good_to_go == 0 ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,1))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,1))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_uvmet(SCRa, SCRb, cname, cdesc, cunits, 0) 

            IF ( INDEX(plot_these_fields,",u10m,") /= 0 ) THEN
              cname = "u10m"
              CALL interp (SCRa, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",v10m,") /= 0) THEN
              cname = "v10m"
              CALL interp( SCRb, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF

        !!! Clobber some wind arrays
        IF (ALLOCATED(UUU)) DEALLOCATE(UUU)
        have_UUU = .FALSE.
        IF (ALLOCATED(VVV)) DEALLOCATE(VVV)
        have_VVV = .FALSE.
        IF (ALLOCATED(U10)) DEALLOCATE(U10)
        have_UUU = .FALSE.
        IF (ALLOCATED(V10)) DEALLOCATE(V10)
        have_VVV = .FALSE.



!!! Sea Level Pressure  
        IF ( INDEX(plot_these_fields,",slp,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            CALL calc_slp(SCR, cname, cdesc, cunits) 
            CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                         data_out, nxout, nyout, nzout, &
                         vert_array, interp_levels, number_of_zlevs,cname) 
            CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            DEALLOCATE(SCR)
          END IF
        END IF


!!! Reflectivity             
        IF ( INDEX(plot_these_fields,",dbz,")     /= 0 .OR. &
             INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QS )  CALL get_keep_array ( local_time, good_to_go, "QSNOW" )
          IF ( .not. have_QG )  CALL get_keep_array ( local_time, good_to_go, "QGRAUP" )
          good_to_go = 0
          IF ( .not. have_QV )  CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_QR )  CALL get_keep_array ( local_time, good_to_go, "QRAIN" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES ) THEN 
            IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)
            ALLOCATE(SCR(west_east_dim,south_north_dim,bottom_top_dim))
            CALL calc_dbz(SCR, cname, cdesc, cunits) 

            IF ( INDEX(plot_these_fields,",dbz,") /= 0 ) THEN
              CALL interp (SCR, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF

            IF ( INDEX(plot_these_fields,",max_dbz,") /= 0) THEN
              ALLOCATE(SCR2(west_east_dim,south_north_dim))
              SCR2 = 0.0
              DO jj = 1,south_north_dim
                DO ii = 1,west_east_dim
                  DO kk = 1,bottom_top_dim
                    SCR2(ii,jj) = MAX( SCR2(ii,jj) , SCR(ii,jj,kk) )
                  END DO 
                END DO 
              END DO 
              IF (ALLOCATED(SCR)) DEALLOCATE(SCR)
              ALLOCATE(SCR(west_east_dim,south_north_dim,1))
              SCR(:,:,1) = SCR2(:,:)
              cname = "max_dbz"
              cdesc    = "Max Reflectivity"
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
              DEALLOCATE(SCR2)
            END IF
            DEALLOCATE(SCR)

          END IF
        END IF



!!! 3D CAPE/CIN 
        IF ( INDEX(plot_these_fields,",cape,") /= 0 .OR. &
             INDEX(plot_these_fields,",cin,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",cape,") /= 0 ) THEN
              cname = "cape"
              cdesc = "CAPE"
              cunits = "J/kg"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",cin,") /= 0) THEN
              cname = "cin"
              cdesc = "CIN"
              cunits = "J/kg"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 3D LCL/LFC
!2020-05-02_18-55 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
        IF ( INDEX(plot_these_fields,",LCL3,") /= 0 .OR. &
             INDEX(plot_these_fields,",LFC3,")  /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_LFC3(SCRa, SCRb, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",LCL3,") /= 0 ) THEN
              cname = "LCL3"
              cdesc = "LCL3"
              cunits = "meters AGL"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",LFC3,") /= 0) THEN
              cname = "LFC3"
              cdesc = "LFC3"
              cunits = "meters AGL"
              CALL interp (SCRb, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF



!!! 3D EL (LNB)
!2020-05-10_11-46 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_LFC3/src
        IF ( INDEX(plot_these_fields,",EL3,") /= 0 ) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            CALL calc_EL3(SCRa, cname, cdesc, cunits, 1) 

            IF ( INDEX(plot_these_fields,",EL3,") /= 0 ) THEN
              cname = "EL3"
              cdesc = "LNB"
              cunits = "meters AGL"
              CALL interp (SCRa, west_east_dim, south_north_dim, bottom_top_dim, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCRa)
          END IF
        END IF



!!! 2D CAPE/CIN & LCL/LFC
        IF ( INDEX(plot_these_fields,",mcape,") /= 0 .OR. &
             INDEX(plot_these_fields,",mcin,")  /= 0 .OR.  &
             INDEX(plot_these_fields,",lcl,")   /= 0 .OR.   &
             INDEX(plot_these_fields,",lfc,")   /= 0) THEN
          good_to_go = 0
          IF ( .not. have_QV )   CALL get_keep_array ( local_time, good_to_go, "QVAPOR" )
          IF ( .not. have_HGT )  CALL get_keep_array ( local_time, good_to_go, "HGT" )
          IF ( .not. have_PSFC ) CALL get_keep_array ( local_time, good_to_go, "PSFC" )
          IF ( good_to_go == 0 .AND. have_TK .AND. have_PRES .AND. have_GEOPT ) THEN 

            IF ( ALLOCATED(SCRa) ) DEALLOCATE(SCRa)
            IF ( ALLOCATED(SCRb) ) DEALLOCATE(SCRb)
            ALLOCATE(SCRa(west_east_dim,south_north_dim,bottom_top_dim))
            ALLOCATE(SCRb(west_east_dim,south_north_dim,bottom_top_dim))
            SCRa = 0.0
            SCRb = 0.0
            CALL calc_cape(SCRa, SCRb, cname, cdesc, cunits, 0) 

            ALLOCATE(SCR(west_east_dim,south_north_dim,1))
            IF ( INDEX(plot_these_fields,",mcape,") /= 0 ) THEN
              cname = "mcape"
              cdesc = "MCAPE"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,1)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",mcin,") /= 0) THEN
              cname = "mcin"
              cdesc = "MCIN"
              cunits = "J/kg"
              SCR(:,:,1) = SCRa(:,:,2)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lcl,") /= 0) THEN
              cname = "lcl"
              cdesc = "LCL"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,3)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            IF ( INDEX(plot_these_fields,",lfc,") /= 0) THEN
              cname = "lfc"
              cdesc = "LFC"
              cunits = "meters AGL"
              SCR(:,:,1) = SCRa(:,:,4)
              CALL interp (SCR, west_east_dim, south_north_dim, 1, &
                           data_out, nxout, nyout, nzout, &
                           vert_array, interp_levels, number_of_zlevs,cname) 
              CALL write_dat (data_out, nxout, nyout, nzout, cname, cdesc, cunits)
            END IF
            DEALLOCATE(SCR)
            DEALLOCATE(SCRa)
            DEALLOCATE(SCRb)

          END IF
        END IF


       IF ( ALLOCATED(SCR) ) DEALLOCATE(SCR)

   END SUBROUTINE process_diagnostics

END MODULE module_diagnostics





```

### module_calc_ept_sat.f90
```
!! Diagnostics: Saturated Equivalent Potential Temperature
!
!2020-05-14_09-43 
!manda@calypso
!/work06/manda/WRF3.7.1/MyARWpost_SEPT/src
! C:\Users\atmos\Dropbox\MARKDOWN\GFD\EPT
!

MODULE module_calc_ept_sat  !SATURATED EPT

  CONTAINS
  SUBROUTINE calc_sept(SCR, cname, cdesc, cunits)

  USE module_model_basics

  use ept_bolton80


  !Arguments
  real, allocatable, dimension(:,:,:) :: SCR
  character (len=128) :: cname, cdesc, cunits

  !Local
  real, dimension(&
  &west_east_dim,south_north_dim,bottom_top_dim) :: tmp


  do k=1,bottom_top_dim
    do j=1,south_north_dim
      do i=1,west_east_dim

        PhPa=PRES(i,j,k)/100.
        call ept_B80_SAT(SCR(i,j,k), TK(i,j,k), PhPa)

      enddo !i
    enddo !j
  enddo !k

  cname    = "sept"
  cdesc    = "Saturated Equivalent Potential Temperature"
  cunits   = "K"
  
  END SUBROUTINE calc_sept



subroutine ept_B80_SAT(sept, tk, p) !SATURATED EPT 
implicit none

!sept: saturated equivalent potential temperature (kelvin)
! tk: absolute temperature (kelvin)
! p : pressure (hPa)
real,intent(out)::sept
real,intent(in)::tk,p

! tc: temperature (degree Celsius)
! pt: potential temparature (K)
! es: saturated water vapor pressure (hPa)
!  e: water vapor pressure (hPa)
!  r: water-vapor mixing ratio (g/kg)
real tc,pt,es,e,rs

real,parameter::p0=1000.0
real pow
real::arg21,arg22

! SATURATED EPT
!  https://unidata.github.io/MetPy/latest/api/generated/
!metpy.calc.saturation_equivalent_potential_temperature.html

!Eq.(39) of B80 (p.1052)
tc=tk-273.15
! Eq.(10) of B80 (p.1047) 
es=6.112*exp((17.67*tc)/(tc+243.5)) !hPa
rs=(0.622 * es/ (p - es))*1.E3       !g/kg
pow=0.2854*(1.0 - 0.28*0.001*rs)
pt=tk*(p0/p)**pow

arg21=3.376/tk - 0.00254
arg22 = rs*(1.0 + 0.81 * 1.0E-3*rs)
sept=pt*exp(arg21 * arg22)

end subroutine ept_B80_SAT



END MODULE module_calc_ept_sat
```

### Makefile
```

include ../configure.arwp

OBJS = module_model_basics.o constants_module.o \
gridinfo_module.o ARWpost.o input_module.o \
output_module.o module_map_utils.o \
misc_definitions_module.o module_date_pack.o \
module_debug.o process_domain_module.o \
module_get_file_names.o module_interp.o \
module_basic_arrays.o module_diagnostics.o \
module_arrays.o module_pressure.o module_calc_height.o \
module_calc_pressure.o module_calc_theta.o \
module_calc_tk.o module_calc_tc.o module_calc_td.o \
module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o \
module_calc_cape.o module_calc_wdir.o module_calc_wspd.o \
module_calc_clfr.o module_calc_myrh.o module_calc_ept.o \
module_ept_bolton80.o \
module_calc_LFC3.o \
module_calc_EL3.o \
module_calc_ept_sat.o


wrong: 
	clear ;
	@echo " "
	@echo "go up one directory and type compile to build ARWpost"
	@echo " "
	@echo " "


all: ARWpost.exe

ARWpost.exe: $(OBJS)
	$(FC) $(FFLAGS) $(LDFLAGS) -o $@ $(OBJS)  \
		-L$(NETCDF)/lib -I$(NETCDF)/include  \
	-lnetcdf


module_model_basics.o:

constants_module.o:

misc_definitions_module.o:

module_date_pack.o:

module_get_file_names.o:

module_arrays.o: module_model_basics.o gridinfo_module.o

module_pressure.o: module_model_basics.o

module_interp.o: input_module.o module_arrays.o module_pressure.o

module_map_utils.o:

module_debug.o:

gridinfo_module.o: misc_definitions_module.o module_debug.o module_get_file_names.o

input_module.o: gridinfo_module.o misc_definitions_module.o 

output_module.o: input_module.o module_model_basics.o module_arrays.o module_interp.o

process_domain_module.o: module_date_pack.o gridinfo_module.o input_module.o \
output_module.o misc_definitions_module.o module_debug.o module_interp.o \
module_basic_arrays.o module_diagnostics.o module_arrays.o \
module_model_basics.o module_pressure.o

ARWpost.o: gridinfo_module.o module_debug.o process_domain_module.o


module_basic_arrays.o: gridinfo_module.o module_arrays.o module_pressure.o \
module_interp.o constants_module.o

module_diagnostics.o: gridinfo_module.o output_module.o module_arrays.o \
module_interp.o module_pressure.o constants_module.o module_calc_height.o \
module_calc_pressure.o module_calc_tk.o module_calc_tc.o module_calc_theta.o \
module_calc_td.o module_calc_td2.o module_calc_rh.o module_calc_rh2.o \
module_calc_uvmet.o module_calc_slp.o module_calc_dbz.o module_calc_cape.o \
module_calc_wdir.o module_calc_wspd.o module_calc_clfr.o module_calc_myrh.o \
module_calc_ept.o module_calc_LFC3.o \
module_calc_EL3.o \
module_calc_ept_sat.o

module_calc_cape.o: module_model_basics.o constants_module.o
module_calc_dbz.o: module_model_basics.o constants_module.o
module_calc_height.o: module_model_basics.o constants_module.o
module_calc_pressure.o: module_model_basics.o
module_calc_slp.o: module_model_basics.o constants_module.o
module_calc_rh.o: module_model_basics.o constants_module.o
module_calc_rh2.o: module_model_basics.o constants_module.o
module_calc_tk.o: module_model_basics.o constants_module.o
module_calc_tc.o: module_model_basics.o constants_module.o
module_calc_td.o: module_model_basics.o
module_calc_td2.o: module_model_basics.o
module_calc_theta.o: module_model_basics.o
module_calc_uvmet.o: module_model_basics.o
module_calc_wdir.o: module_model_basics.o
module_calc_wspd.o: module_model_basics.o
module_calc_clfr.o: module_model_basics.o
module_calc_myrh.o: module_model_basics.o constants_module.o
module_calc_ept.o: module_model_basics.o constants_module.o module_ept_bolton80.o
module_calc_LFC3.o: module_model_basics.o constants_module.o
module_calc_EL3.o: module_model_basics.o constants_module.o
module_calc_ept_sat.o: module_model_basics.o constants_module.o 

clean:
	rm -f $(OBJS) *.mod

clobber:
	rm -f $(OBJS) *.mod
	rm -f *.f
	rm -f ARWpost.exe 
```




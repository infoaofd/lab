# JMASTER

ARWpostの並列化

処理する時刻を分割したスクリプトを複数個作成して，並列化する。並列化効率100％にできる。

[[_TOC_]]



```bash
$ pwd
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
```

## 各JOB投入用のスクリプト作成

### テンプレート作成

```
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$ cat JOB_TEMPLATE.sh 
```

```
### PATH=/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
### PATH CANNOT BE USED.
 
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J01

export OMP_NUM_THREADS=1
mpirun -np 1 ARWpost.exe
exit
```

<font color="red">変数PATHは使えなかった</font>。mpirunが見つからなないというエラーがlog.errに出力される

ARWpost.exeの場所に注意

### テンプレートのコピー

```
$ cp JOB_TEMPLATE.sh J01/JOB.sh
‘JOB_TEMPLATE.sh’ -> ‘J01/JOB.sh’
$ cp JOB_TEMPLATE.sh J02/JOB.sh
.........
$ cp JOB_TEMPLATE.sh J09/JOB.sh
```

### テンプレートの書き変え

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/01.BASH/LINUX_REPLACE_STRING_IN_FILE.md?ref_type=heads

```
$ sed -i -e 's/J01/J02/g' J02/JOB.sh
$ sed -i -e 's/J01/J03/g' J03/JOB.sh
.....
$ sed -i -e 's/J01/J09/g' J09/JOB.sh
```

```bash
$  more J0*/JOB.sh | grep J0
J01/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J01
J02/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J02
J03/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J03
J04/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J04
J05/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J05
J06/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J06
J07/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J07
J08/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J08
J09/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J09
```

## 処理時刻の変更

### namelist.ARWpostのstart_dateとend_dateの編集

```
$ vi J01/namelist.ARWpost

J01/namelist.ARWpost
 start_date = "2021-08-10_12:00:00",
 end_date   = "2021-08-11_00:00:00",
```

```
J01/namelist.ARWpost
 start_date = "2021-08-10_12:00:00",
 end_date   = "2021-08-11_00:00:00",
J02/namelist.ARWpost
 start_date = "2021-08-11_00:10:00",
 end_date   = "2021-08-11_12:00:00",
J03/namelist.ARWpost
 start_date = "2021-08-11_12:10:00",
 end_date   = "2021-08-12_00:00:00",
J04/namelist.ARWpost
 start_date = "2021-08-12_00:10:00",
 end_date   = "2021-08-12_12:00:00",
J05/namelist.ARWpost
 start_date = "2021-08-12_12:10:00",
 end_date   = "2021-08-13_00:00:00",
J06/namelist.ARWpost
 start_date = "2021-08-13_00:10:00",
 end_date   = "2021-08-13_12:00:00",
J07/namelist.ARWpost
 start_date = "2021-08-13_12:10:00",
 end_date   = "2021-08-14_00:00:00",
J08/namelist.ARWpost
 start_date = "2021-08-14_00:10:00",
 end_date   = "2021-08-14_12:00:00",
J09/namelist.ARWpost
 start_date = "2021-08-14_12:10:00",
 end_date   = "2021-08-15_00:00:00",
```

### 処理時刻の確認

```bash
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$ more J??/namelist.ARWpost |grep -e ::: -e start_date -e end_date -A1
::::::::::::::
J01/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-10_12:00:00",
 end_date   = "2021-08-11_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J02/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-11_00:10:00",
 end_date   = "2021-08-11_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J03/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-11_12:10:00",
 end_date   = "2021-08-12_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J04/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-12_00:10:00",
 end_date   = "2021-08-12_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J05/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-12_12:10:00",
 end_date   = "2021-08-13_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J06/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-13_00:10:00",
 end_date   = "2021-08-13_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J07/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-13_12:10:00",
 end_date   = "2021-08-14_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J08/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-14_00:10:00",
 end_date   = "2021-08-14_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J09/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-14_12:10:00",
 end_date   = "2021-08-15_00:00:00",
 interval_seconds = 600,
```

## psadilookupがあるか確認

```
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$ ls J??/src/*adi*
J01/src/psadilookup.dat  J06/src/psadilookup.dat
J02/src/psadilookup.dat  J07/src/psadilookup.dat
J03/src/psadilookup.dat  J08/src/psadilookup.dat
J04/src/psadilookup.dat  J09/src/psadilookup.dat
J05/src/psadilookup.dat
```

## 全ジョブ投入用のスクリプト作成

```bash
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$ vi JOBLIST.sh

$ cat JOBLIST.sh

### PATH=/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01

sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J01/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J02/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J03/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J04/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J05/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J06/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J07/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J08/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J09/JOB.sh
```

<font color="red">変数PATHは使えなかった</font>。JOB.shが見つからなないというエラーがlog.errに出力される

## ジョブ投入前の確認

### 各ジョブ投入スクリプトの確認

```bash
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$  more J0*/JOB.sh | grep J0
J01/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J01
J02/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J02
J03/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J03
J04/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J04
J05/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J05
J06/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J06
J07/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J07
J08/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J08
J09/JOB.sh
cd /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J09

```

### 処理時刻の確認

```bash
more J??/namelist.ARWpost |grep -e ::: -e start_date -e end_date -A1
```

```
::::::::::::::
J01/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-10_12:00:00",
 end_date   = "2021-08-11_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J02/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-11_00:10:00",
 end_date   = "2021-08-11_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J03/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-11_12:10:00",
 end_date   = "2021-08-12_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J04/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-12_00:10:00",
 end_date   = "2021-08-12_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J05/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-12_12:10:00",
 end_date   = "2021-08-13_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J06/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-13_00:10:00",
 end_date   = "2021-08-13_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J07/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-13_12:10:00",
 end_date   = "2021-08-14_00:00:00",
 interval_seconds = 600,
--
::::::::::::::
J08/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-14_00:10:00",
 end_date   = "2021-08-14_12:00:00",
 interval_seconds = 600,
--
::::::::::::::
J09/namelist.ARWpost
::::::::::::::
! RW3A.00.05.05.05.0097.01
--
 start_date = "2021-08-14_12:10:00",
 end_date   = "2021-08-15_00:00:00",
 interval_seconds = 600,
```

### 入出力ファイルの場所の指定の確認

#### 入力ファイルの場所の確認

```bash
$ find . -name namelist.ARWpost|xargs grep input_root_name
./J03/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J02/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J04/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J01/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J08/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J07/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J05/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J09/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
./J06/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01_d01*"
```

#### 合ってなければ変更する

この例では04.05.01.0000から05.05.05.0099へ

```
2024-10-22_13-12
/data07/thotspot/zamanda/RW3A.MyARWpost/RW3A.00.05.05.05.0099.01.d01.traj.10MN
$ find . -name namelist.ARWpost|xargs grep 04.05.01.0000
```

```
find . -name namelist.ARWpost|xargs sed -i "s/04.05.01.0000/05.05.05.0099/g"
```

```
/data07/thotspot/zamanda/RW3A.MyARWpost/RW3A.00.05.05.05.0099.01.d01.traj.10MN
$ find . -name namelist.ARWpost|xargs grep 05.05.05.0099
```

```
./J06/namelist.ARWpost:! RW3A.00.05.05.05.0099.01
./J06/namelist.ARWpost: input_root_name = "/data07/thotspot/zamanda/RW3A.OUT.WRF-4.1.5/RW3A.00.05.05.05.0099.01/RW3A.00.05.05.05.0099.01_d01*"
./J06/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.01/traj/RW3A.00.05.05.05.0099.01/RW3A.00.05.05.05.0099.01.d01.traj.10MN"
```

#### 出力先の確認

```bash
$ find . -name namelist.ARWpost|xargs grep output_root_name
./J03/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J02/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J04/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J01/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J08/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J07/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J05/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J09/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
./J06/namelist.ARWpost: output_root_name= "/data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01/RW3A.00.05.05.05.0097.01.d01.traj.10MN"
```

## 出力先ディレクトリの作成

```bash
mkdir -vp /data07/thotspot/zamanda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.05.05.05/traj/RW3A.00.05.05.05.0097.01
```



## 実行

### 全ジョブ投入スクリプトの確認

```bash
/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01
$ cat JOBLIST.sh

### PATH=/data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01

sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J01/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J02/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J03/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J04/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J05/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J06/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J07/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J08/JOB.sh
sh /data07/thotspot/zamanda/RW3A.MyARWpost/JMASTER.RW3A.0097.01/J09/JOB.sh
```

### 全ジョブ投入スクリプト実行

```
jmaster -q C064 -P ${ユーザー名} -np {プロセス数} -e log.err -o log.out ./JOBLIST.sh
```

プロセス数は，**スクリプト数+1**とする。スクリプト(J??/JOB.sh)が9のとき，プロセス数は10とする。

１ノード24コアのマシンの場合，最大分割数は23となる。

**例**

```bash
$ jmaster -q CS64 -P zamanda -np 10 -e log.err -o log.out ./JOBLIST.sh
```


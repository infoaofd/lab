#!/bin/bash

. ./gmtpar.sh

range=122/138/20/34.8
size=M5
xanot=a4f1; yanot=a2f1
anot=${xanot}/${yanot}WSne

IN=RW3A.00.04.05.05.0000-0802.01_DIFF_SST.nc
if [ ! -f $IN ]; then echo EEEEE NO SUCH FILE, $IN; exit 1; fi
GRD2=06.TEST_GMT4_NC_TO_TEXT_RW3A.00.04.05.05.0802.01_SST_GRD.nc
if [ ! -f $GRD2 ]; then echo EEEEE NO SUCH FILE, $GRD2; exit 1; fi

OUT=$(basename $0 .sh)_$(basename $IN .nc).TXT
NC2=$(basename $0 .sh)_$(basename $IN .nc)_GRD.nc
FIG=$(basename $0 .sh)_$(basename $IN .nc).ps

gmtset D_FORMAT %12.6f
grd2xyz $IN |awk '{if ($3 != "NaN") print $1,$2,$3}' > $OUT
surface -R$range -T0 -I0.02/0.02 $OUT -G$NC2

CPT=$(basename $0 .sh)_CPT.txt; makecpt -Cpolar -T-3/3/0.5 -Z > $CPT
rm -vf $FIG
grdimage $NC2 -R$range -J$size -C$CPT -X1.5 -Y3 -K -K -P >$FIG

gmtset D_FORMAT %3.0f
grdcontour $GRD2 -R$range -J$size -A1f12 -C1 -O -K >>$FIG

pscoast -R -J -B$anot -W1 -G2 -O -K >>$FIG

gmtset D_FORMAT %3.1f
psscale -D5.5/2.45/5/0.1 -C$CPT -Ba1f1 -O -K >>$FIG
psscale -D2.5/-0.7/5/0.1h -C$CPT -Ba1f1 -O -K >>$FIG

xoffset=0; yoffset=5
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $FIG
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${IN}
0 0.90  9 0 1 LM OUTPUT: ${OUT}
EOF
if [ -f $OUT ];then echo OUT: $OUT;fi
if [ -f $FIG ];then echo FIG: $FIG;fi



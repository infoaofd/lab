#!/bin/bash

EXP=$1;EXP=${EXP:-0000}
INDIR=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/01HR/ARWpost_RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.basic_p.01HR.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP1=$EXP; CTL1=$CTL

TIME1=00Z12;TIME2=01Z12
MMYR=AUG2021
DATE1=${TIME1}${MMYR}; DATE2=${TIME2}${MMYR}

GS=$(basename $0 .sh).GS
OUT=RW3A.00.04.05.05.${EXP}.01_${TIME1}-${TIME2}_${MMYR}.nc

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'
'set x 1 1469';'set y 1 1231';'set z 1';'set t 1'

'define P1 = RAINNC(time=$DATE1)'
'define P2 = RAINNC(time=$DATE2)'
'define P=P2-P1'
'set sdfwrite $OUT'
'sdfwrite P'
'quit'
EOF

grads -bcp "$GS"
#rm -vf $GS
cp -v $CTL .
echo;if [ -f $OUT ]; then echo OUT: $OUT; fi;echo

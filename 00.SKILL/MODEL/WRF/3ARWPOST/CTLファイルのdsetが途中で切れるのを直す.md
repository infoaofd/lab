# CTLファイルのdsetが途中で切れるのを直す

## 例1

### 変更前の確認

```bash
$ find *0095.01  -name *ctl |xargs grep %d
dset ^RW3A.00.05.05.05.0095.01.d01.basic_p.01HR_%y4-%m2-%d
```

### 変更

```bash
$ find *0095.01 -name *ctl |xargs sed -i "s/%d/%d2_%h2:%n2.dat/g"
```

### 変更後の確認

```bash
$ head -1 *0095.01/*ctl
dset ^RW3A.00.05.05.05.0095.01.d01.basic_p.01HR_%y4-%m2-%d2_%h2:%n2.dat
```

## 例2

```bash
$ find *009?.0?  -name *ctl |xargs grep %d
$ find *009?.0?  -name *ctl |xargs sed -i "s/%d/%d2_%h2:%n2.dat/g"
$ head -1 *009*.0*/*ctl
```


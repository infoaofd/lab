# ARWpost Tips

[[_TOC_]]

## 時刻が途中で切れるのを直す (namelist.ARWpost)

```bash
$ find ./ARWpost_RW3A.00.04.05.01.*  -name *ctl |xargs grep dset
./ARWpost_RW3A.00.04.05.01.0000.01/RW3A.00.04.05.01.0000.01.d01.basic_p.01HR.ctl:dset ^RW3A.00.04.05.01.0000.01.d01.basic_p.01HR_%y4-%m2-%d
```

**時刻が%y4-%m2-%dで切れている**。

### 修正方法

#### (1) 修正箇所の確認

find . -name namelist.ARWpost |xargs grep 0802.01

```bash
find ./ARWpost_RW3A.00.04.05.01.*  -name *ctl |xargs grep %y4-%m2-%d
```

#### (2) 修正箇所の置換

find . -name  FILE_NAME | xargs sed -i "s/OLD/NEW/g"

```bash
find ./ARWpost_RW3A.00.04.05.01.*  -name *ctl |xargs sed -i "s/%y4-%m2-%d/%y4-%m2-%d_%h2:%n2.dat/g"
```

#### (3) 置換が成功したか最後に確認

$ find ./ARWpost_RW3A.00.04.05.01.*  -name *ctl |xargs grep %y4-%m2-%d

```bash
find ./ARWpost_RW3A.00.04.05.01.*  -name *ctl |xargs grep %y4-%m2-%d
```


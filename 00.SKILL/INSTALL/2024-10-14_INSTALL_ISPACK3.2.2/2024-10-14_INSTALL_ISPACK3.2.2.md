# INSTALL ISPACK3.2.2

/work09/ma/INSTALL/2024-10-14_17_ISPACK-3.2.2/ispack-3.2.2
2024-10-14_18-11

```
# diff ORG_241014-1753_Mkinclude Mkinclude
5c5
< DESTDIR               = .
---
> DESTDIR               = /usr/local/ispack-3.2.2/ifort
11,21c11,14
< FC            = gfortran
< FFLAGS                = -O3 -march=native -fopenmp -fno-range-check -fPIC
< MPIFC         = mpif90
< MPIFFLAGS     = -O3 -march=native -fopenmp -fPIC
< CC            = gcc
< CFLAGS                = -O3 -march=native -fPIC
< #-- for Intel ifort
< #FC           = ifort
< #FFLAGS               = -xHost -qopenmp -align array64byte -fPIC
< #MPIFC                = mpiifort
< #MPIFFLAGS    = -xHost -qopenmp -align array64byte -fPIC
---
> #FC           = gfortran
> #FFLAGS               = -O3 -march=native -fopenmp -fno-range-check -fPIC
> #MPIFC                = mpif90
> #MPIFFLAGS    = -O3 -march=native -fopenmp -fPIC
23a17,23
> #-- for Intel ifort
> FC            = ifort
> FFLAGS                = -xHost -qopenmp -align array64byte -fPIC
> MPIFC         = mpiifort
> MPIFFLAGS     = -xHost -qopenmp -align array64byte -fPIC
> CC            = gcc
> CFLAGS                = -O3 -march=native -fPIC
```

```
# which mpiifort
/opt/intel/oneapi/mpi/2021.7.1/bin/mpiifort
```

```
# locate libimf.so
```

```

# export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/intel/oneapi/clck/2021.7.1/lib_common/intel64/
```

```
# mkdir -vp /usr/local/ispack-3.2.2/ifort/shared/
```

```
# source /opt/intel/oneapi/setvars.sh
```

```
# make
```



```
# tree /usr/local/ispack-3.2.2/ifort
/usr/local/ispack-3.2.2/ifort
├── libispack3.a
└── shared
    └── libispack3.so
```


# INSTALL OpenMPI

[[_TOC_]]

## 注意事項 **Intel OneAPIとの共存はできない**

**Intel OneAPIの環境は使用しない** (下記のコマンドは**使わない**)

```bash
source /opt/intel/oneapi/setvars.sh
```
**condaも終了させておく**
condaにmpiが入っている場合があるようである(下記参照)


## 環境のチェック

```bash
(base) 
/work09/am
$ mpif90
Error: Command line argument is needed!
This script (/work09/am/anaconda3/bin/mpif90) is being called recursively, check that MPICH_FC does not refer to mpifort.
(base) 
am@localhost
2023-06-25_19-11
/work09/am
$ cdda

am@localhost
2023-06-25_19-12
/work09/am
$ mpif90
bash: mpif90: コマンドが見つかりませんでした...

```

**Anacondaにmpif90が入っているようなので注意**。

```
2023-06-25_19-12
/work09/am
$ mpirun
bash: mpirun: コマンドが見つかりませんでした...
```

```bash
2023-06-25_19-06
/work09/am/INSTALL/2023-06-25_19_OpenMPI
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ gfortran -v
組み込み spec を使用しています。
COLLECT_GCC=gfortran
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/4.8.5/lto-wrapper
ターゲット: x86_64-redhat-linux
configure 設定: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-languages=c,c++,objc,obj-c++,java,fortran,ada,go,lto --enable-plugin --enable-initfini-array --disable-libgcj --with-isl=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/isl-install --with-cloog=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/cloog-install --enable-gnu-indirect-function --with-tune=generic --with-arch_32=x86-64 --build=x86_64-redhat-linux
スレッドモデル: posix
gcc バージョン 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ wget https://download.open-mpi.org/release/open-mpi/v4.1/openmpi-4.1.4.tar.gz
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ tar xzvf openmpi-4.1.4.tar.gz 
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ ls
00.README.TXT  openmpi-4.1.4/  openmpi-4.1.4.tar.gz
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ cd openmpi-4.1.4/
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI
$ ./configure --prefix=/usr/local/openmpi-4.1.4 CC=gcc CXX=g++ FC=gfortran
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ make all
```

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ sudo make install
```



## 環境設定

$HOME/.bashrcを開いて、ファイルの最後に次の文を付け加えます。

```
$ vi $HOME/.bashrc
```



```bash
MPIROOT=/usr/local/openmpi-4.1.4
PATH=$MPIROOT/bin:$PATH
LD_LIBRARY_PATH=$MPIROOT/lib:$LD_LIBRARY_PATH
MANPATH=$MPIROOT/share/man:$MANPATH
export MPIROOT PATH LD_LIBRARY_PATH MANPATH
```

```bash
$ source ~/.bashrc
```



## テスト

```bash
$ conda deactivate
```
condaにopenmpiが入っている場合があるようなので，**condaを終了させておく**

```bash
$ mpif90 -v
組み込み spec を使用しています。
COLLECT_GCC=/usr/bin/gfortran
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-redhat-linux/4.8.5/lto-wrapper
ターゲット: x86_64-redhat-linux
configure 設定: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-linker-hash-style=gnu --enable-languages=c,c++,objc,obj-c++,java,fortran,ada,go,lto --enable-plugin --enable-initfini-array --disable-libgcj --with-isl=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/isl-install --with-cloog=/builddir/build/BUILD/gcc-4.8.5-20150702/obj-x86_64-redhat-linux/cloog-install --enable-gnu-indirect-function --with-tune=generic --with-arch_32=x86-64 --build=x86_64-redhat-linux
スレッドモデル: posix
gcc バージョン 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) 
```



## コンパイルと実行

### テストプログラム

```
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ vi test_ompi.f90
```

```fortran
program test_ompi
implicit none

include 'mpif.h'

integer :: nnn
integer :: me
integer :: ierr

call mpi_init(ierr)
call mpi_comm_size(mpi_comm_world,nnn,ierr)
call mpi_comm_rank(mpi_comm_world,me,ierr)

write(6,*) "I am ",me,"/",nnn

call mpi_finalize(ierr)

end program test_ompi
```

参考：MPI超入門

https://www.cc.u-tokyo.ac.jp/events/lectures/13/MPIprogf.pdf



### コンパイル

```bash
$ mpif90 -o test_ompi test_ompi.f90
```

コンパイルオプションはgfortranと同じ。 



### 実行

```bash
$ mpirun -n ４ test
```

4は使用するCPUのコアの数

```bash
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ mpirun -n 4 test_ompi 
 I am            0 /           4
 I am            2 /           4
 I am            3 /           4
 I am            1 /           4
```

## 注意

**Intel OneAPIとの共存はできない**

```
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ alias |grep ift
alias ift='source /opt/intel/oneapi/setvars.sh'
```

```
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ ift
 
:: initializing oneAPI environment ...
   -bash: BASH_VERSION = 4.2.46(2)-release
   args: Using "$@" for setvars.sh arguments: 
:: advisor -- latest
.....
:: mpi -- latest

```

```
2023-06-25_19-42
/work09/am/INSTALL/2023-06-25_19_OpenMPI/openmpi-4.1.4
$ mpirun -n 4 test_ompi
 I am            0 /           1
 I am            0 /           1
 I am            0 /           1
 I am            0 /           1
```

おそらく**並列で実行していない**（**meが常に0**で，**nnnが常に1**となっていることに注意）

STPK-0.9.20.0のインストール

am@p5820
/work03/am/INSTALL/2023-04-20_STPK0.9.20.0/libstpk-0.9.20.0


export FC=ifort; export FCFLAGS="-assume byterecl -convert big_endian -qopenmp"

# 並列化機能が OpenMP* ディレクティブに基づいてマルチスレッド・コードを生成できるようにします。-qopenmp は、古いオプション -openmp の後継オプションです。

./configure --prefix=/usr/local/stpk-0.9.20.0 --includedir=/usr/local/stpk-0.9.20.0/include

$ make

$ make
CDPATH="${ZSH_VERSION+.}:" && cd . && /bin/sh /work03/am/INSTALL/2023-04-20_STPK0.9.20.0/libstpk-0.9.20.0/missing aclocal-1.15 
/work03/am/INSTALL/2023-04-20_STPK0.9.20.0/libstpk-0.9.20.0/missing: 行 81: aclocal-1.15: コマンドが見つかりません
WARNING: 'aclocal-1.15' is missing on your system.
         You should only need it if you modified 'acinclude.m4' or
         'configure.ac' or m4 files included by 'configure.ac'.
         The 'aclocal' program is part of the GNU Automake package:
         <http://www.gnu.org/software/automake>
         It also requires GNU Autoconf, GNU m4 and Perl in order to run:
         <http://www.gnu.org/software/autoconf>
         <http://www.gnu.org/software/m4/>
         <http://www.perl.org/>

$ autoreconf -f -i
# https://stackoverflow.com/questions/33278928/how-to-overcome-aclocal-1-15-is-missing-on-your-system-warning

$ make 

$ sudo make install

$ ls /usr/local/stpk-0.9.20.0/
include/  lib/

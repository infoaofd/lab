# DokuWiki

```bash
$ cat /etc/redhat-release 
```

CentOS Linux release 7.9.2009 (Core)

```bash
$ sudo yum install httpd
```

  httpd.x86_64 0:2.4.6-99.el7.centos.1                                 

依存性を更新しました:
  httpd-devel.x86_64 0:2.4.6-99.el7.centos.1                           
  httpd-manual.noarch 0:2.4.6-99.el7.centos.1                          
  httpd-tools.x86_64 0:2.4.6-99.el7.centos.1                           
  mod_ssl.x86_64 1:2.4.6-99.el7.centos.1  

```bash
$ diff /etc/httpd/conf/httd.conf_BAK 
```

```bash
/etc/httpd/conf/httpd.conf
86c86
< ServerAdmin root@localhost
---
> ServerAdmin 133.67.98.41
95a96,100
> 
> # ADDED 
> ServerTokens Prod
> ServerSignature Off
> TraceEnable off
```



## Apache起動と自動起動

```
$ sudo systemctl start httpd
```

```
$ sudo systemctl enable httpd
```

Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.



### firewalld関連

```
$ sudo systemctl start firewalld
```

```
$ sudo systemctl enable firewalld
```

```
$ sudo firewall-cmd --permanent --zone=public --add-service=http
```

Warning: ALREADY_ENABLED: http
success

```
$ sudo firewall-cmd --reload
```

success

### PHP関連

#### PHPインストール

```
$ sudo yum -y install epel-release
```

```
$ sudo rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
$ sudo rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm
$ sudo yum -y --enablerepo=epel,remi,remi-php70 install php php-pear php-devel php-mysql php-mbstring php-gd php-pdo php-xml php-mcrypt
```

```bash
$  php --version
PHP 7.0.33 (cli) (built: Aug  2 2023 10:48:43) ( NTS )
Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.0.0, Copyright (c) 1998-2017 Zend Technologies
```

#### PHP設定

```
$ diff /etc/php.ini_BAK /etc/php.ini
359c359
< expose_php = On
---
> expose_php = Off 
462c462
< display_errors = Off
---
> display_errors = On 
877c877
< ;date.timezone =
---
> date.timezone = Asia/Tokyo 
1465c1465
< ;mbstring.language = Japanese
---
> mbstring.language = Japanese
```



### Dokuwiki関連

```
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-oldstable.tgz
```

```
$ tar zxfvp dokuwiki-oldstable.tgz
```



#### Dokuwikiの配置

```
$ sudo mv dokuwiki-2022-07-31b/ /var/www/html/dokuwiki
$ sudo chown -R apache:apache /var/www/html/dokuwiki
```

#### Dokuwikiの初期設定


## GMT6 USING CONDA

## INSTALL

```
$ conda config --prepend channels conda-forge
Warning: 'conda-forge' already in 'channels' list, moving to the top
```

```
conda create --name pygmt python=3.9 numpy pandas xarray netcdf4 packaging gmt
```

```
$ conda activate pygmt                                        
(pygmt)  
```

```
conda install pygmt
```

```
conda update pygmt
```


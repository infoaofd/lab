#!/bin/bash
# /work03/am/INSTALL/2023-01-03_GMT4
# C:\Users\boofo\lab\00.SKILL\INSTALL\2023-01-03_GMT4_INSTALL

wget    ftp://ftp.soest.hawaii.edu/gmt/gmt-4.5.18-src.tar.bz2
wget    ftp://ftp.soest.hawaii.edu/gmt/gmt-4.5.18-non-gpl-src.tar.bz2
wget    ftp://ftp.soest.hawaii.edu/gmt/gshhg-gmt-2.3.7.tar.gz

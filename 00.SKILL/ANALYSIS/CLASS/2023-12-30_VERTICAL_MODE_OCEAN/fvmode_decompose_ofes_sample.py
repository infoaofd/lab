#!/usr/local/bin/python3

# ! D:\16.00.TOOL\14.00.OCEAN\VERTICAL_MODE_OCEAN

import numpy as np
#import csv
#import scipy.linalg


fin=open('dofes-ts-pacific-sali.dta','rb')
sal=np.fromfile(fin,dtype='>f',sep='')
fin.close()

fin2=open('dofes-ts-pacific-temp.dta','rb')
temp=np.fromfile(fin2,dtype='>f',sep='')
fin2.close()

fw=open('dofes-density-pacific.dta','wb')

fin3=open('ofes_w.txt','r')
opw=np.fromfile(fin3,sep=' ')
fin3.close()

fin4=open('ofes_p.txt','r')
opp=np.fromfile(fin4,sep=' ')
fin4.close()

dnst=[]
#----------------------------density
for i in range(54):
    s=sal[i]
    s1=s**1.5
    s2=s*s
    s3=s2*s
    s4=s3*s
    t=temp[i]
    t2=t*t
    t3=t2*t
    t4=t3*t
    t5=t4*t
    rh1=999.842594+6.793952*t*10**(-2)-9.095290*t2*10**(-3)
    rh2=1.001685*t3*10**(-4)-1.120083*t4*10**(-6)+6.536332*t5*10**(-9)
    rh3=s*(0.824493-4.0899*t*10**(-3)+7.6438*t2*10**(-5))
    rh4=s*(-8.2467*t3*10**(-7)+5.3875*t4*10**(-9))
    rh5=s1*(-5.72466*10**(-3)+1.0227*t*10**(-4)-1.6546*t2*10**(-6))

    rho=rh1+rh2+rh3+rh4+rh5+4.8314*s2*10**(-4)
    print(rho)
    dnst.append(rho)
    fw.write(rho)

fw.close()
#----------------------------depth
dpw=[]
dpp=[]

for j in range(0,54):
    dp=opw[j+1]-opw[j]
    dpp.append(dp)

for j in range(1,54):
    dw=opp[j]-opp[j-1]
    dpw.append(dw)

dpw0=dpw[0]
dpw.insert(0,dpw0)
dpw.append(dpw[53])

print(dpw)
print(len(dpw))
print(dpp)
print(len(dpp))

undf=-1*10**34
nsq=[]
#print(dnst)

for k in range(1,54):
    if dnst[k-1] > 0.1*undf and dnst[k] > 0.1*undf and k > 0:
        ns=-9.8/1027.0*(dnst[k]-dnst[k-1])/dpw[k]
        ns=max(ns,1.0*10**(-16))
        nsq.append(ns)
    else:
        ns=undf
        nsq.append(ns)

print(nsq)
nsq0=nsq[0]
nsq.insert(0,nsq0)
nsq.append(nsq[52])

insq=[]
nelm=0

for k in range(0,54):
    if nsq[k] > 0.1*undf :
        ins=1.0/nsq[k]
        insq.append(ins)
        if k < 54:
            nelm=nelm+1
            #print(ins)

#print(insq)
#print(nelm)
#------------------------------matrix

if nelm > 0:
    eignr=0.0
    eigni=0.0
    work1d=0.0
    dummy=0.0
    matrix=0.0
    vector=0.0
    celm=nelm
    offset=53-0+1-nelm
    

mat1=[]
mat2=[]
mat3=[]

for l in range(1,celm):
    mata=insq[l+offset]/dpw[l+offset]/dpp[l+offset]
    mat1.append(mata)

for l in range(0,celm-1):
    matb=insq[l+1+offset]/dpw[l+1+offset]/dpp[l+offset]
    mat2.append(matb)

for l in range(1,celm-1):
    matc=-(insq[l+1+offset]/dpw[l+1+offset]+insq[l+offset]/dpw[l+offset])/dpp[l+offset]
    mat3.append(matc)

mate=-insq[1+offset]/dpw[1+offset]/dpp[0+offset]
mat3.insert(0,mate)

matcc=-insq[celm-1+offset]/dpw[celm-1+offset]/dpp[celm-1+offset]
mat3.append(matcc)

print(mat1)
print(mat2)
print(mat3)

dm1=np.diag(mat1,k=-1)

dm2=np.diag(mat2,k=1)

dm3=np.diag(mat3)

diamat=dm1+dm2+dm3
A=np.array(diamat)
#print(A)

    

#-------------------------------------eigenvalue_problem
w,v=np.linalg.eig(A)
with open('numpy_evalue.txt','w') as few:
    few.write(str(w))

#fw2=open('mode_decompose_eigenvalue.txt','w')
#fw3=open('mode_decompose_eigenvector.txt','wt')
print(w)
arr_v=np.array(v)
print(arr_v)
print(arr_v.dtype)
v0=arr_v[:,18]
v1=arr_v[:,19]
v2=arr_v[:,20]
v3=arr_v[:,21]

with open('sample_data.txt','w') as fws:
    for i in range(0,54):
        print(-1*opp[i],v0[i],-1*v1[i],v2[i],v3[i],file=fws)


#-------------------------------------rearrange

#eigen_id=np.argsort(w)[::-1]
#w=w[eigen_id]
#arr_v=arr_v[eigen_id,:]


#tmv=[]
#tm=v[0]
#tmv.append(tm)
#print(tmv)
#for f in range(55):
#    for e in range(1,54):
#        if w[e] > w[e-1] :
#            t=w[e-1]
#            w[e-1]=w[e]
#            w[e]=t
#            tmp=v[e-1]
#            v[e-1]=v[e]
#            v[e]=tmp

print(w)
print(arr_v)

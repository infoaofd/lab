load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

indir1="/work05/manda/DATA/J-OFURO3/CLM"
infle1="J-OFURO3_LHF_V1.0_CLM_HR_1988-2013.nc"

in1=indir1+"/"+infle1

;system("ncdump -h "+in1)

f=addfile(in1,"r")

lon=f->longitude
lat=f->latitude
LHF=f->LHF

;printVarSummary(lon)
;printVarSummary(lat)
;printVarSummary(LHF)

FIG="TEST_LHF_MAP_JJA.eps"
print("FIG "+FIG)
print("")


idx=6

DAY3M=30.0+31.0+31.0
LHF3M=LHF(idx-1,:,:)*30.0/DAY3M + LHF(idx,:,:)*31.0/DAY3M+\
LHF(idx+1,:,:)*31.0/DAY3M

 ;;; 格子情報等の付加
LHF3M!0     = "lat" 
LHF3M!1     = "lon"
LHF3M&lat   = lat
LHF3M&lon   = lon
LHF3M@unit  = "Wm-2"

res=True
wks = gsn_open_wks("eps",FIG)

  res@gsnLeftString   = indir1               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = infle1
res@mpCenterLonF = 140  ; 経度の中心

plot = gsn_csm_contour_map_ce(wks,LHF3M(:,:),res)

end


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

indir1="/work05/manda/DATA/J-OFURO3/CLM"
infle1="J-OFURO3_LHF_V1.0_CLM_HR_1988-2013.nc"
in1=indir1+"/"+infle1


;system("ncdump -h "+in1)

f=addfile(in1,"r")

lon=f->longitude
lat=f->latitude
LHF=f->LHF

;printVarSummary(lon)
;printVarSummary(lat)
;printVarSummary(LHF)

FIG="TEST_EVAPO_COLOR_JJA.eps"
print("FIG "+FIG)
print("")


idx=6

DAY3M=30.0+31.0+31.0
EVAPO=LHF(idx-1,:,:)*30.0/DAY3M + LHF(idx,:,:)*31.0/DAY3M+\
LHF(idx+1,:,:)*31.0/DAY3M

; https://sites.google.com/site/afcanalysis/home/constants/evaporation
FACT=0.001441*24.0 ; W/m2 -> mm/d

EVAPO=EVAPO*FACT




 ;;; 格子情報等の付加
EVAPO!0     = "lat" 
EVAPO!1     = "lon"
EVAPO&lat   = lat
EVAPO&lon   = lon
EVAPO@unit  = "mm/d"

res=True
wks = gsn_open_wks("eps",FIG)

  res@gsnLeftString   = indir1               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = infle1

  res@gsnDraw         = False        ; plotを描かない
  res@gsnFrame        = False        ; WorkStationを更新しない
  res@cnLinesOn       = False
  res@cnFillOn        = True
  res@cnInfoLabelOn   = False

  res@cnLevelSelectionMode = "ManualLevels"
  res@cnMinLevelValF  = 0
  res@cnMaxLevelValF  =  5
  res@cnLevelSpacingF =  0.5
  res@cnFillPalette   = "CBR_wet"

  res@mpMinLonF       = 100
  res@mpMaxLonF       = 160
  res@mpMinLatF       = -10
  res@mpMaxLatF       =  50
  res@mpCenterLonF = 140  ; 経度の中心

res@gsnMaximize        = True 

  res@gsnRightString  = ""
  res@pmLabelBarOrthogonalPosF = 0.1        ; カラーバーの位置を微調整
  ;;; タイトルや軸の文字の大きさを設定
  res@tmYLLabelFontHeightF = 0.016
  res@tmXBLabelFontHeightF = 0.016
  res@tiMainFontHeightF    = 0.024
  res@lbLabelFontHeightF   = 0.016
  ;;; カラーバーにタイトルをつける
  res@lbTitleOn            = True
  res@lbTitleString        = EVAPO@unit
  res@lbTitlePosition      = "Right"
  res@lbTitleDirection     = "Across" 
  res@lbTitleFontHeightF   = 0.016

plot = gsn_csm_contour_map_ce(wks,EVAPO(:,:),res)


  draw(plot)         ; ここでplotを描く
  frame(wks)         ; WorkStationの更新

end


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

indir="/work05/manda/TEACHING/TYPHOON_ENECO_1912/GRIB"

infle="sst"

f=addfile("../GRIB/sst1905.grib","r")
print(f)
lat=f->g0_lat_1
lon=f->g0_lon_2


files = systemfunc("ls "+indir+"/"+infle+"*.grib")

fall=addfiles(files,"r")

sst=fall[:]->WTMP_GDS0_DBSL
time=fall[:]->initial_time0_hours

;printVarSummary (time)
;printVarSummary (lat)
;printVarSummary (lon)
;printVarSummary (sst)

idx=0

utc_date = cd_calendar(time(idx), 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))

print("year month day "+year+" "+month+" "+day)

YYYY=sprinti("%0.4i", year)
  MM=sprinti("%0.2i", month)

FIG="TEST_PLOT_COBE_"+YYYY+MM+".eps"
print("FIG "+FIG)
print("")


res=True
wks = gsn_open_wks("eps",FIG)

  res@gsnLeftString   = indir               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = infle
res@mpCenterLonF = 140  ; 経度の中心

plot = gsn_csm_contour_map_ce(wks,sst(idx,:,:),res)

end


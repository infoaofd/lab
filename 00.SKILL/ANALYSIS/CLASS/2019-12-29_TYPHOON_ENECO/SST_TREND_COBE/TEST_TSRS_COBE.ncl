load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

indir="/work05/manda/TEACHING/TYPHOON_ENECO_1912/GRIB"

infle="sst"

f=addfile("../GRIB/sst1905.grib","r")
;print(f)
lat=f->g0_lat_1
lon=f->g0_lon_2


files = systemfunc("ls "+indir+"/"+infle+"*.grib")

fall=addfiles(files,"r")

sst=fall[:]->WTMP_GDS0_DBSL
time=fall[:]->initial_time0_hours

YYYYMM  = cd_calendar(time,-1) 
;print(YYYYMM)

sst1yr=month_to_annual_weighted(YYYYMM,sst,1)
dim=dimsizes(sst1yr)
ny=dim(0)

yyyy=new(ny,integer)


utc_date = cd_calendar(time(:), 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
dim=dimsizes(year)
nt=dim(0)


;print(nt)
yyyy=ispan(year(0),year(nt-1),1)

;printVarSummary(yyyy)
print("yyyy(0)=" + yyyy(0) + " yyyy(ny-1)="+yyyy(ny-1))


LAT=26.5
LON=126.5
sst1p=sst1yr(:, { LAT},{ LON})
;sst1p=sst1yr(:, { g0_lat_1 |LAT},{ g0_lon_2 |LON})

FIG="TEST_TSRS_COBE_"+sprintf("%4.1f",LAT)+"_"+sprintf("%5.1f",LON)+".eps"

wks = gsn_open_wks("eps",FIG)

res=True


  res@gsnLeftString   = indir               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = sprintf("%4.1f",LAT)+"N "+sprintf("%5.1f",LON)+"E"


plot  = gsn_csm_xy (wks,yyyy,sst1p,res) ; create plot

end


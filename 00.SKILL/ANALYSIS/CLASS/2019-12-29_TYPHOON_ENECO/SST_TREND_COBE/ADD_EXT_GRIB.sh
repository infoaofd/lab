#!/bin/bash


INDIR="/work05/manda/DATA/SST/COBE.1905-2005/GRIB"
OUTDIR="../GRIB"

NOW=$(date -R)
CWD=$(pwd)
COM="$0 $@"

MD=0.README_$(basename $0 .sh).md

cat <<EOF>$MD
$NOW
$CWD
$COM

COBE SST
--------------------

Monthly Report on Climate System Separated Volume No.12

About Gridded Binary Data of Monthly Sea Surface Temperatures

https://ds.data.jma.go.jp/tcc/tcc/library/MRCS_SV12/sstdata/aboutgpv_e.htm

EOF

mkdir -vp $OUTDIR


LOG=$(basename $0 .sh).log

echo $NOW  >$LOG
echo $CWD >>$LOG
echo $COM >>$LOG
echo      >>$LOG


INLIST=$(ls -1 $INDIR/sst????)


for INFLE in $INLIST; do

OFLE=$(basename ${INFLE}).grib

echo INPUT: $INFLE 
echo OUTPUT: $OFLE

IN=$INDIR/$(basename $INFLE)
OUT=$OUTDIR/$OFLE

cp -a $IN $OUT

echo INPUT: >>$LOG
ls -lh --time-style=long-iso $IN 2>&1 >>$LOG
echo
echo OUTPUT: >>$LOG
ls -lh --time-style=long-iso $OUT 2>&1 >>$LOG
echo >>$LOG
echo >>$LOG

done

echo LOG:
ls -lh $LOG
echo


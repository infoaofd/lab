
Variable: a
Type: file
filename:	heat_content_anomaly_0-700_yearly
path:	/work01/DATA/OHC/heat_content_anomaly_0-700_yearly.nc
   file global attributes:
      Conventions : CF-1.6
      title : Ocean Heat Content anomalies from WOA09 : heat_content_anomaly 0-700 m yearly 1.00 degree
      summary : Mean ocean variable anomaly from in situ profile data
      references : Levitus, S., J. I. Antonov, T. P. Boyer, O. K. Baranova, H. E. Garcia, R. A. Locarnini, A.V. Mishonov, J. R. Reagan, D. Seidov, E. S. Yarosh, M. M. Zweng, 2012: World Ocean heat content and thermosteric sea level change (0-2000 m) 1955-2010. Geophys. Res. Lett. , 39, L10603, doi:10.1029/2012GL051106
      institution : National Oceanographic Data Center(NODC)
      comment : 
      id : heat_content_anomaly_0-700_yearly.nc
      naming_authority : gov.noaa.nodc
      time_coverage_start : 1955-01-01
      time_coverage_duration : P67Y
      time_coverage_resolution : P01Y
      geospatial_lat_min : -90
      geospatial_lat_max : 90
      geospatial_lon_min : -180
      geospatial_lon_max : 540
      geospatial_vertical_min :  0
      geospatial_vertical_max : 700
      geospatial_lat_units : degrees_north
      geospatial_lat_resolution : 1.00 degrees
      geospatial_lon_units : degrees_east
      geospatial_lon_resolution : 1.00 degrees
      geospatial_vertical_units : m
      geospatial_vertical_resolution : 
      geospatial_vertical_positive : down
      creator_name : Ocean Climate Laboratory
      creator_email : NODC.Services@noaa.gov
      creator_url : http://www.nodc.noaa.gov
      project : World Ocean Database
      processing_level : processed
      keywords : <ISO_TOPIC_Category> Oceans</ISO_TOPIC_Category>
      keywords_vocabulary : ISO 19115
      standard_name_vocabulary : CF-1.6
      contributor_name : Ocean Climate Laboratory
      contributor_role : Calculation of anomalies
      featureType : Grid
      cdm_data_type : Grid
      nodc_template_version : NODC_NetCDF_Grid_Template_v1.0
      date_created : 2022-01-07 
      date_modified : 2022-01-07 
      publisher_name : US NATIONAL OCEANOGRAPHIC DATA CENTER
      publisher_url : http://www.nodc.noaa.gov/
      publisher_email : NODC.Services@noaa.gov
      license : These data are openly available to the public. Please acknowledge the use of these data with the text given in the acknowledgment attribute.
      Metadata_Conventions : Unidata Dataset Discovery v1.0
      metadata_link : http://www.nodc.noaa.gov/OC5/3M_HEAT_CONTENT/
   dimensions:
      ncl_scalar = 1
      nbounds = 2
      lat = 180
      lon = 360
      depth = 1
      time = 67
   variables:
      integer crs ( ncl_scalar )
         grid_mapping_name :	latitude_longitude
         epsg_code :	EPSG:4326
         longitude_of_prime_meridian :	 0
         semi_major_axis :	6378137
         inverse_flattening :	298.2572

      float lat ( lat )
         standard_name :	latitude
         long_name :	latitude
         units :	degrees_north
         axis :	Y
         bounds :	lat_bnds

      float lat_bnds ( lat, nbounds )
         comment :	latitude bounds

      float lon ( lon )
         standard_name :	longitude
         long_name :	longitude
         units :	degrees_east
         axis :	X
         bounds :	lon_bnds

      float lon_bnds ( lon, nbounds )
         comment :	longitude bounds

      float depth_bnds ( depth, nbounds )
         comment :	depth bounds
         positive :	down
         units :	meters
         axis :	Z

      float time ( time )
         standard_name :	time
         long_name :	time
         units :	months since 1955-01-01 00:00:00
         axis :	T
         climatology :	climatology_bounds

      float climatology_bounds ( time, nbounds )
         comment :	This variable defines the bounds of the climatological time period for each time

      float h18_hc ( time, depth, lat, lon )
         long_name :	Ocean heat content anomaly calculated from objectively analyzed temperature anomaly fields over given depth limits
         coordinates :	time lat lon depth
         cell_methods :	area: mean depth: mean time: mean
         grid_mapping :	crs
         units :	10^18_joules
         _FillValue :	9.96921e+36

      float yearl_h22_WO ( time )
         long_name :	global_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_WO ( time )
         long_name :	global_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_NH ( time )
         long_name :	northern_hemisphere_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_NH ( time )
         long_name :	northern_hemisphere_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_SH ( time )
         long_name :	southern_hemisphere_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_SH ( time )
         long_name :	southern_hemisphere_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_AO ( time )
         long_name :	Atlantic_Ocean_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_AO ( time )
         long_name :	Atlantic_Ocean_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_NA ( time )
         long_name :	North_Atlantic_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_NA ( time )
         long_name :	North_Atlantic_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_SA ( time )
         long_name :	South_Atlantic_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_SA ( time )
         long_name :	South_Atlantic_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_PO ( time )
         long_name :	Pacific_Ocean_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_PO ( time )
         long_name :	Pacific_Ocean_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_NP ( time )
         long_name :	North_Pacific_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_NP ( time )
         long_name :	North_Pacific_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_SP ( time )
         long_name :	South_Pacific_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_SP ( time )
         long_name :	South_Pacific_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_IO ( time )
         long_name :	Indian_Ocean_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_IO ( time )
         long_name :	Indian_Ocean_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_NI ( time )
         long_name :	North_Indian_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_NI ( time )
         long_name :	North_Indian_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      float yearl_h22_SI ( time )
         long_name :	South_Indian_heat_content_integral_0-700 
         units :	10^22_joules

      float yearl_h22_se_SI ( time )
         long_name :	South_Indian_standarderror_heat_content_integral_0-700
         units :	10^22_joules

      integer basin_mask ( lat, lon )
         long_name :	basin mask
         _FillValue :	-100
         grid_mapping :	crs
         flag_values :	Not_used_in_Basin_Integrals Atlantic_Basin Pacific_Basin Indian_Basin
         comment :	For North Hem. use Lats 0.5 to 89.5 For South use Lats -89.5 to -0.5


Done TSR.AAV.ncl



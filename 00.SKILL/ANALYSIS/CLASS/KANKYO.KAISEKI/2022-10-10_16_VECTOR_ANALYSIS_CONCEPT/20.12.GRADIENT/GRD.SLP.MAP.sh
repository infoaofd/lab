#!/bin/bash

Y=2022; MM=08; MMM=AUG; DD=01; HH=00
PLEV1=
LONW=90; LONE=210; LATS=0 ; LATN=80
MLONW=100; MLONE=200; MLATS=20 ; MLATN=70

TIME=${HH}Z${DD}${MMM}${Y}

POW=5
FAC=1E${POW}
SCL=10\`a-${POW}\`n
# https://www.sci.hokudai.ac.jp/grp/poc/top/old/software/other/grads_tips/index.htm

UNIT="${SCL} hPa m\`a-1\`n"
VFAC=10

VAR="SLP GRAD"
TITLE="${VAR} $MMM $Y hPa"

INDIR=/work01/DATA/NCEP2/2022-10-10_16_NCEP2_MON
INFLE=$INDIR/mslp.mon.mean.nc
#/work01/DATA/NCEP2/6HR/2022
#INFLE=$INDIR/mslp.2022.nc

PREFIX=$(basename $0 .sh)
FIG=${PREFIX}_${Y}${MM}${DD}_${HH}.eps

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

# GRADS SCRIPT GOES HERE:
cat <<EOF>$GS
say

'sdfopen $INFLE'

'q ctlinfo 1'

'set time ${TIME}'
#'set lev $PLEV1'
'set lon $LONW $LONE'; 'set lat $LATS $LATN'

say '### GRAD'
'var=mslp/100'

pi=3.14159265359
dtr=pi'/'180

r=6.371e6

dx '=' r '*cos(' dtr '*' lat ')*' dtr '*cdiff(' lon ',' x')'
dy '=' r '*' dtr '*cdiff(' lat ',' y ')'

dtdx '=cdiff(' var ',' x ')/' dx '*${FAC}'
dtdy '=cdiff(' var ',' y ')/' dy '*${FAC}'

grad '=mag(' dtdx ',' dtdy ')'


say '### PLOT'
'cc';'set hershey off'
'set grads off'; 'set grid off'
'set mpdset mres'



'set vpage 0.0 8.5 0.0 11'

xs=1; xe=6
ys=3; ye=10
'set parea 'xs ' 'xe' 'ys' 'ye


'set xlab on'; 'set ylab on'
'set xlint 20'; 'set ylint 10'

say '### COLOR SHADE'
'color 0.6 1.5 0.3 -kind white->gold->orange->red'

'set lon $MLONW $MLONE'; 'set lat $MLATS $MLATN'
#'d maskout(grad,p/100)' ;#-lev)'
'd grad'

'set xlab off'; 'set ylab off'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)


say '### VECTOR'
'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'grdx=dtdx*${VFAC}'; 'grdy=dtdy*${VFAC}'; 
'vec.gs skip(grdx,2);grdy -SCL 0.5 20 -P 20 20 -SL m/s'

xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(grdx,2);grdy -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

x1=xr+0.4; x2=x1+0.1
y1=yb    ; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.12 -fh 0.16 -ft 4 -fs 1'

say '### SCALE & UNITS'
'set strsiz 0.12 0.16'
'set string 1 c 4 0'
xx=(x1+x2)/2+0.2
yy=y2+0.2
'draw string ' xx ' ' yy ' ${UNIT}'

say '### CONTOUR'
'set gxout contour'
'set ccolor 1'
'set cthick 2'
'set cint 2'
'set clskip 2'
'd var'

say '### TITLE'
'set strsiz 0.12 0.14'
'set string 1 c 4 0'
xx = (xl+xr)/2; yy=yt+0.2
'draw string ' xx ' ' yy ' ${TITLE}'

say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yy+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

say '$INFLE'
say
'gxprint $FIG'
say
'!ls -lh $FIG'
say
'quit'
EOF

grads -bcp "$GS"

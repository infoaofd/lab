#!/bin/bash

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

#CTL=$(basename $0 .sh).CTL
YYYY=2020; MM=07; MMM=JUL; DD=05; HH=12
INDIR="/work01/DATA/MSM/MSM-P/${YYYY}/"
INFLE1="${MM}${DD}.nc"

GS=$(basename $0 .sh).GS

MLONW=128 ;MLONE=131; MLATS=31 ;MLATN=32.5
LONW=125.5 ;LONE=132.5; LATS=29.5 ;LATN=35.5
<<COMMENT
MLONW=170 ;MLONE=190
MLATS=33 ;MLATN=43
LONW=130 ;LONE=220
LATS=10 ;LATN=60
COMMENT
#LEV=1000; VMAG=7
LEV=950; VMAG=20; VSKP=2
TIME=${HH}Z${DD}${MMM}${YYYY}

VAROUT=UV_DIV_MSM
FIG=${VAROUT}_${LEV}_${YYYY}${MM}${DD}_${HH}.eps

LEVS="-10 10 1"
#LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2
POW=4;SCL=1E${POW};FAC=10\`a-${POW}\`n
UNIT="[${FAC} s\`a-1\`n]"

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

'sdfopen ${INDIR}/${INFLE1}'

xmax = 1
ymax = 1

ytop=9

xwid = 5.0/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set hershey off'; 'set grid off'; 'set grads off'
'set map 1 1 2'; 'set mpdset hires'
'set xlint 1';'set ylint 1'

'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

'DIV=hdivg(u,v)*${SCL}'
'set lon ${MLONW} ${MLONE}'; 'set lat ${MLATS} ${MLATN}'
'd DIV'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

'set xlab off'; 'set ylab off'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u,$VSKP);v -SCL 0.5 $VMAG -P 20 20 -SL m/s'
xx=xr-0.5; yy=yt+0.2
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(u,$VSKP);v -SCL 0.5 $VMAG -P 'xx' 'yy' -SL m/s'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+1; y=y1
'set strsiz 0.12 0.15'
'set string 1 r 3 0'
'draw string 'x' 'y' ${UNIT}'

'q dims'
LINE=sublin(result,5)
WORD=subwrd(LINE,6)
MMM=substr(WORD,6,3)
YYYY=substr(WORD,9,4)
say MMM' 'YYYY

# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' $VAROUT ${LEV}hPa ${HH}UTC$DD$MMM$YYYY'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

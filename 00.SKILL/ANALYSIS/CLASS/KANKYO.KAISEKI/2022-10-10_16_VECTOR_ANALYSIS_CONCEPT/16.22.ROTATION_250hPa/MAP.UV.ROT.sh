#!/bin/bash

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

#CTL=$(basename $0 .sh).CTL
INDIR="/work01/DATA/NCEP2/2022-10-10_16_NCEP2_MON"
INFLE1="uwnd.mon.mean.nc"; INFLE2="vwnd.mon.mean.nc"


GS=$(basename $0 .sh).GS

MLONW=100 ;MLONE=190
MLATS=20 ;MLATN=50
LONW=95 ;LONE=195
LATS=15 ;LATN=55
LEV=250; VMAG=100
#LEV=300; VMAG=50
YYYY=2022; MM=02; MMM=FEB
TIME=00Z01${MMM}${YYYY}

VAROUT=UV_ROT
FIG=${VAROUT}_${LEV}_MON_${YYYY}${MM}.eps

LEVS=" 40 70 10"
#LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->gold->orange->red->magenta'
FS=1
UNIT="m/s"
POW=5;SCL=1E${POW};FAC=10\`a-${POW}\`n
UNIT2="[${FAC} s\`a-1\`n]"

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

'sdfopen ${INDIR}/${INFLE1}'
'sdfopen ${INDIR}/${INFLE2}'

xmax = 1
ymax = 1

ytop=9

xwid = 8.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set hershey on'; 'set grid off'; 'set grads off'
'set map 1 1 2'
'set xlint 20';'set ylint 5'

'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lev $LEV'
'set lon ${MLONW} ${MLONE}'; 'set lat ${MLATS} ${MLATN}'
'VMAG=mag(uwnd.1,vwnd.2)'

'd VMAG'


'set xlab off'; 'set ylab off'

'set gxout contour';'set cthick 1';'set ccolor 0'
'set cint 20'; 'set clab off'
'd VMAG'



'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)



'set lon ${MLONW} ${MLONE}'; 'set lat ${MLATS} ${MLATN}'
'set gxout vector'
'set cthick 10'; 'set ccolor  0'


'vec.gs skip(uwnd.1,2);vwnd.2 -SCL 0.5 $VMAG -P 20 20 -SL m/s'
xx=xr-0.8; yy=yt+0.2
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(uwnd.1,2);vwnd.2 -SCL 0.5 $VMAG -P 'xx' 'yy' -SL m/s'





'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

'ROT=hcurl(uwnd.1,vwnd.2)*${SCL}'
'set lon ${MLONW} ${MLONE}'; 'set lat ${MLATS} ${MLATN}'

'set gxout contour'
'set cthick 5'; 'set ccolor  0';'set clab off'
'set cint 5'

'set xlab off'; 'set ylab off'
'd ROT'

'set gxout contour'
'set cthick 1'; 'set ccolor  1';'set clab forced'
'set cint 5';'set clskip 1'
'd ROT'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=x1+2
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x=xr-2; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' CNT=ROT ${UNIT2}'

'q dims'
LINE=sublin(result,5)
WORD=subwrd(LINE,6)
MMM=substr(WORD,6,3)
YYYY=substr(WORD,9,4)
say MMM' 'YYYY

# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' $VAROUT ${LEV}hPa 'MMM' 'YYYY

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

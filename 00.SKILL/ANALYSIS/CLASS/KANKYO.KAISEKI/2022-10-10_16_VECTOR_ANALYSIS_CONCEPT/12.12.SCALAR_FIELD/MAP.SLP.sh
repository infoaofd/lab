#!/bin/bash

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

#CTL=$(basename $0 .sh).CTL
INDIR="/work01/DATA/NCEP2/2022-10-10_16_NCEP2_MON"
INFLE="mslp.mon.mean.nc"


GS=$(basename $0 .sh).GS


LONW=90 ;LONE=300
LATS=-10 ;LATN=60
# LEV=
YYYY=2022; MM=08; MMM=AUG
TIME=00Z01${MMM}${YYYY}

VAROUT=SLP
FIG=${VAROUT}_MON_${YYYY}${MM}.eps

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

'sdfopen ${INDIR}/${INFLE}'

xmax = 1
ymax = 1

ytop=9

xwid = 5.0/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set hershey off'; 'set grid off'; 'set grads off'
# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

 'set lon ${LONW} ${LONE}'
 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'

'set xlint 30';'set ylint 30'
'set cint 2'; 'set clskip 2'
'd mslp/100'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr
#y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr
#y=y1
#'set strsiz 0.12 0.15'
#'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'

'q dims'
LINE=sublin(result,5)
WORD=subwrd(LINE,6)
MMM=substr(WORD,6,3)
YYYY=substr(WORD,9,4)
say MMM' 'YYYY

# TEXT
x=(xl+xr)/2; 
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' 'MMM' 'YYYY

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

# gnuplotでワイヤーフレーム図を書く

```bash
(ins) gnuplot> set term pngcairo size 1280, 960 font ",24"

Terminal type is now 'pngcairo'
Options are ' background "#ffffff" enhanced font ",24" fontscale 1.0 size 1280, 960 '
(ins) gnuplot> set output "2D_COS.png"
(ins) gnuplot> set xrange[-20:20]
(ins) gnuplot> set yrange[-20:20]
(ins) gnuplot> set pm3d
(ins) gnuplot> set ztics 1
(ins) gnuplot> splot cos(2*pi/20*x+2*pi/30*y)
```

![2D_COS](2D_COS.png)

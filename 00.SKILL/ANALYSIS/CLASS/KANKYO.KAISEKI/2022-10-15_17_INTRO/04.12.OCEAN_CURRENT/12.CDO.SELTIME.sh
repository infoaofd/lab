INROOT=/work01/DATA/GODAS/
YYYY=2022;MM=08;DD=01
INDIR=$INROOT
INFLE1=pottmp.2022.nc
INFLE2=ucur.2022.nc
INFLE3=vcur.2022.nc

IN1=$INDIR/$INFLE1; IN2=$INDIR/$INFLE2; IN3=$INDIR/$INFLE3

if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi
if [ ! -f $IN3 ];then echo NO SUCH FILE, $IN3; exit 1;fi

OUT1=$(basename $IN1 .nc)${MM}.nc
OUT2=$(basename $IN2 .nc)${MM}.nc
OUT3=$(basename $IN3 .nc)${MM}.nc

DATE=${YYYY}-${MM}-${DD}
cdo seldate,$DATE,$DATE $IN1 $OUT1
cdo seldate,$DATE,$DATE $IN2 $OUT2
cdo seldate,$DATE,$DATE $IN3 $OUT3

echo $OUT1; echo $OUT2;echo $OUT3

#ncdump -h $OUT1



;************************************************
; This file is loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
;************************************************
begin
INROOT="/work01/DATA/"
DSET="NCEP2"
YYYY="2022"
MM="09"
DD="30"
HH="12"
LEV=300
Y=toint(YYYY)
M=toint(MM)
D=toint(DD)
H=toint(HH)
;CLON=300; 130 ;220 ;270.0       ; choose center lon
CLON=130 ;220 ;270.0       ; choose center lon
CLAT=45 

INDIR="./" ;INROOT+"/"+DSET+"/6HR/"+YYYY+"/"
NC1="air_"+YYYY+MM+DD+"_"+HH+".nc"
NC2="uwnd_"+YYYY+MM+DD+"_"+HH+".nc"
NC3="vwnd_"+YYYY+MM+DD+"_"+HH+".nc"

f1 = addfile(INDIR+NC1,"r") 
f2 = addfile(INDIR+NC2,"r") 
f3 = addfile(INDIR+NC3,"r") 

t = f1->air(0,{LEV},:,:)
t=t-273.15
u = f2->uwnd(0,{LEV},:,:)
v = f3->vwnd(0,{LEV},:,:)


LONLAT=tostring(CLON)+"_"+tostring(CLAT)
FIG=DSET+"_"+LEV+"_"+YYYY+MM+DD+LONLAT
TYP="eps"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = DSET+" "+LEV+" "+YYYY+MM+DD
;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = -60.
res@cnMaxLevelValF = -20.
res@cnLevelSpacingF =  2.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,t,res)

vcres=opt
vcres@vcRefAnnoOrthogonalPosF = -1.0   
vcres@vcRefMagnitudeF         = 80.0  
vcres@vcRefLengthF            = 0.045
vcres@vcGlyphStyle            = "FillArrow" 
vcres@vcMinDistanceF          = 0.017
vcres@vcFillArrowWidthF         = 0.05
vcres@vcFillArrowHeadXF         = 0.3      
;; 頭の長さ(外側)。ベクトルの長さに対する比で，0～2の範囲で与える。
; この比はベクトルの長さに対して不変である。
vcres@vcFillArrowHeadYF         = 0.1     
;; 頭の幅。ベクトルの長さに対する比で，0～1の範囲で与える。
vcres@vcRefAnnoPerimOn          = False
plot1 = gsn_csm_vector(wks,u,v,vcres)

overlay(plot,plot1)
draw(plot)

frame(wks)

print("")
print("FIG: "+FIG+"."+TYP)
end

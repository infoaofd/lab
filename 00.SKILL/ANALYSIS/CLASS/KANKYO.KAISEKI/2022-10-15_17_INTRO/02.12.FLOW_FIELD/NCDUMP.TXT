
Variable: f1
Type: file
filename:	uwnd.10m.gauss.2022
path:	/work01/DATA/NCEP2/6HR/2022/uwnd.10m.gauss.2022.nc
   file global attributes:
      Conventions : CF-1.0
      title : 4x Daily NCEP/DOE Reanalysis 2
      comments : Data is from 
NCEP/DOE AMIP-II Reanalysis (Reanalysis-2)
(4x/day).  Data interpolated from model (sigma) surfaces to gaussian grid.
      platform : Model
      source : NCEP/DOE AMIP-II Reanalysis (Reanalysis-2) Model
      institution : National Centers for Environmental Prediction
      dataset_title : NCEP-DOE AMIP-II Reanalysis
      References : https://www.psl.noaa.gov/data/gridded/data.ncep.reanalysis2.html
      source_url : http://www.cpc.ncep.noaa.gov/products/wesley/reanalysis2/
      history : created 2021/02 by NOAA/ESRL/PSD
Converted to chunked, deflated non-packed NetCDF4 2020/05
   dimensions:
      lon = 192
      lat = 94
      level = 1
      time = 1092  // unlimited
      nbnds = 2
   variables:
      float level ( level )
         units :	m
         actual_range :	( 10, 10 )
         long_name :	Level
         positive :	up
         axis :	Z
         coordinate_defines :	point

      float lat ( lat )
         units :	degrees_north
         actual_range :	( 88.542, -88.542 )
         long_name :	Latitude
         standard_name :	latitude
         axis :	Y
         coordinate_defines :	point

      float lon ( lon )
         units :	degrees_east
         long_name :	Longitude
         actual_range :	(  0, 358.125 )
         standard_name :	longitude
         axis :	X
         coordinate_defines :	point

      double time ( time )
         units :	hours since 1800-1-1 00:00:0.0
         long_name :	Time
         delta_t :	0000-00-00 06:00:00
         standard_name :	time
         axis :	T
         actual_range :	( 1946016, 1952562 )
         coordinate_defines :	start

      float uwnd ( time, level, lat, lon )
         long_name :	6-Hourly Forecast of U-wind at 10 m
         units :	m/s
         precision :	2
         least_significant_digit :	1
         GRIB_id :	33
         GRIB_name :	U GRD
         var_desc :	u-wind
         dataset :	NCEP/DOE AMIP-II Reanalysis (Reanalysis-2)
         level_desc :	10 m
         statistic :	Individual Obs
         parent_stat :	Other
         standard_name :	eastward_wind
         missing_value :	-9.96921e+36
         valid_range :	( -120, 120 )
         actual_range :	( -39.9, 45.46 )
         _FillValue :	-9.96921e+36

      double time_bnds ( time, nbnds )


Variable: f2
Type: file
filename:	vwnd.10m.gauss.2022
path:	/work01/DATA/NCEP2/6HR/2022/vwnd.10m.gauss.2022.nc
   file global attributes:
      Conventions : CF-1.0
      title : 4x Daily NCEP/DOE Reanalysis 2
      comments : Data is from 
NCEP/DOE AMIP-II Reanalysis (Reanalysis-2)
(4x/day).  Data interpolated from model (sigma) surfaces to gaussian grid.
      platform : Model
      source : NCEP/DOE AMIP-II Reanalysis (Reanalysis-2) Model
      institution : National Centers for Environmental Prediction
      dataset_title : NCEP-DOE AMIP-II Reanalysis
      References : https://www.psl.noaa.gov/data/gridded/data.ncep.reanalysis2.html
      source_url : http://www.cpc.ncep.noaa.gov/products/wesley/reanalysis2/
      history : created 2021/02 by NOAA/ESRL/PSD
Converted to chunked, deflated non-packed NetCDF4 2020/05
   dimensions:
      lon = 192
      lat = 94
      level = 1
      time = 1092  // unlimited
      nbnds = 2
   variables:
      float level ( level )
         units :	m
         actual_range :	( 10, 10 )
         long_name :	Level
         positive :	up
         axis :	Z
         coordinate_defines :	point

      float lat ( lat )
         units :	degrees_north
         actual_range :	( 88.542, -88.542 )
         long_name :	Latitude
         standard_name :	latitude
         axis :	Y
         coordinate_defines :	point

      float lon ( lon )
         units :	degrees_east
         long_name :	Longitude
         actual_range :	(  0, 358.125 )
         standard_name :	longitude
         axis :	X
         coordinate_defines :	point

      double time ( time )
         units :	hours since 1800-1-1 00:00:0.0
         long_name :	Time
         delta_t :	0000-00-00 06:00:00
         standard_name :	time
         axis :	T
         actual_range :	( 1946016, 1952562 )
         coordinate_defines :	start

      float vwnd ( time, level, lat, lon )
         long_name :	6-Hourly Forecast of V-wind at 10 m
         units :	m/s
         precision :	2
         least_significant_digit :	1
         GRIB_id :	34
         GRIB_name :	V GRD
         var_desc :	v-wind
         dataset :	NCEP/DOE AMIP-II Reanalysis (Reanalysis-2)
         level_desc :	10 m
         statistic :	Individual Obs
         parent_stat :	Other
         standard_name :	northward_wind
         missing_value :	-9.96921e+36
         valid_range :	( -120, 120 )
         actual_range :	( -41.46, 38.36 )
         _FillValue :	-9.96921e+36

      double time_bnds ( time, nbnds )


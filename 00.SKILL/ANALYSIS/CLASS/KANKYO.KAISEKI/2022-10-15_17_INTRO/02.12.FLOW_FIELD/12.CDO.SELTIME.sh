INROOT=/work01/DATA/NCEP2/6HR
YYYY=2022;MM=09;DD=30;HH=12
INDIR=$INROOT/$YYYY
INFLE1=air.$YYYY.nc
INFLE2=uwnd.$YYYY.nc
INFLE3=vwnd.$YYYY.nc

IN1=$INDIR/$INFLE1; IN2=$INDIR/$INFLE2

if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi
if [ ! -f $IN3 ];then echo NO SUCH FILE, $IN3; exit 1;fi

OUT1=$(basename $IN1 .$YYYY.nc)_$YYYY${MM}${DD}_${HH}.nc
OUT2=$(basename $IN2 .$YYYY.nc)_$YYYY${MM}${DD}_${HH}.nc
OUT3=$(basename $IN3 .$YYYY.nc)_$YYYY${MM}${DD}_${HH}.nc

DATE=${YYYY}-${MM}-${DD}T${HH}:00:00
cdo seldate,$DATE,$DATE $IN1 $OUT1
cdo seldate,$DATE,$DATE $IN2 $OUT2
cdo seldate,$DATE,$DATE $IN3 $OUT3

echo $OUT1; echo $OUT2;echo $OUT3

ncdump -h $OUT1


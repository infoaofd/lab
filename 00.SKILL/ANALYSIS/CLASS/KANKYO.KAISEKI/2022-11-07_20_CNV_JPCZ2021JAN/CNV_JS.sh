#!/bin/bash

# Mon, 07 Nov 2022 20:27:21 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-11-07_20_CNV_JPCZ2021JAN

YYYY=2021; MM=01; DD=09 #; HH=03
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME1=00Z${DD}${MMM}${YYYY}
TIME2=21Z${DD}${MMM}${YYYY}

INDIR=/work01/DATA/MSM/MSM-P/${YYYY}
INFLE=${MM}${DD}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; echo ; exit 1; fi
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_${YYYY}-${MM}-${DD}.pdf

MLONW=128 ;MLONE=140; MLATS=32 ;MLATN=42
LONW=127.5 ;LONE=140.5; LATS=31.5 ;LATN=42.5
LEV=975
TIME=${HH}Z${DD}${MMM}${YYYY}

LEVS="-3 3 0.3"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2
UNIT="10\`a-6\`n[s\`a-1\`n]"

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Mon, 07 Nov 2022 20:27:21 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-11-07_20_CNV_JPCZ2021JAN

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME1}'

'UA=ave(u,time=${TIME1},time=${TIME2})'
'VA=ave(v,time=${TIME1},time=${TIME2})'

'set lon ${MLONW} ${MLONE}'; 'set lat ${MLATS} ${MLATN}'
'CNV=-hdivg(UA,VA)*1.E4'

'set mpdset hires'
'set xlint 5';'set ylint 5'
'set grid off';'set grads off'
'd CNV'

'set xlab off';'set ylab off'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(UA,5);VA -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yt+0.3

'set cthick  4'; 'set ccolor  1'
'vec.gs skip(UA,5);VA -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=xr; y=y1-0.5
'set strsiz 0.12 0.15'
'set string 1 r 3 0'
'draw string 'x' 'y' ${UNIT}'



# TEXT
#x=xl ;# (xl+xr)/2; 
#y=yt+0.2
#'set strsiz 0.12 0.15'
#'set string 1 c 3 0'
#'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : $FIG"
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo


# Mon, 07 Nov 2022 20:27:21 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-11-07_20_CNV_JPCZ2021JAN

'sdfopen /work01/DATA/MSM/MSM-P/2021/0109.nc'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color -3 3 0.3 -kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson -gxout shaded'

'set lon 127.5 140.5'; 'set lat 31.5 42.5'
'set lev 975'
'set time 00Z09JAN2021'

'UA=ave(u,time=00Z09JAN2021,time=21Z09JAN2021)'
'VA=ave(v,time=00Z09JAN2021,time=21Z09JAN2021)'

'set lon 128 140'; 'set lat 32 42'
'CNV=-hdivg(UA,VA)*1.E4'

'set mpdset hires'
'set xlint 5';'set ylint 5'
'set grid off';'set grads off'
'd CNV'

'set xlab off';'set ylab off'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(UA,5);VA -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yt+0.3

'set cthick  4'; 'set ccolor  1'
'vec.gs skip(UA,5);VA -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
x=xr; y=y1-0.5
'set strsiz 0.12 0.15'
'set string 1 r 3 0'
'draw string 'x' 'y' 10`a-6`n[s`a-1`n]'



# TEXT
#x=xl ;# (xl+xr)/2; 
#y=yt+0.2
#'set strsiz 0.12 0.15'
#'set string 1 c 3 0'
#'draw string 'x' 'y' '

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' CNV_JS.GS'
yy = yy+0.2
'draw string ' xx ' ' yy ' ./CNV_JS.sh '
yy = yy+0.2
'draw string ' xx ' ' yy ' Mon, 07 Nov 2022 21:09:59 +0900'
yy = yy+0.2
'draw string ' xx ' ' yy ' p5820.bio.mie-u.ac.jp'
yy = yy+0.2
'draw string ' xx ' ' yy ' /work03/am/TEACHING/KANKYO.KAISEKI/2022-11-07_20_CNV_JPCZ2021JAN'

'gxprint CNV_JS_2021-01-09.pdf'
'quit'

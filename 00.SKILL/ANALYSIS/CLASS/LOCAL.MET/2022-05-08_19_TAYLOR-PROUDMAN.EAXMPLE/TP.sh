#!/bin/bash

# Sun, 08 May 2022 20:10:02 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/2022-05-08_19_TAYLOR-PROUDMAN.EAXMPLE

CTL1=NCEP1.U.CTL; CTL2=NCEP1.V.CTL; CTL3=NCEP1.GPH.CTL;

 GS=$(basename $0 .sh).GS


LONW=120 ;LONE=180; LATS=20 ;LATN=50
LEV=700; YYYY=2021; MMM=AUG; TIME=01${MMM}${YYYY}
VSIZE=30

FIG=$(basename $0 .sh)_${YYYY}_${MMM}_${LEV}.eps

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Sun, 08 May 2022 20:10:02 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/2022-05-08_19_TAYLOR-PROUDMAN.EAXMPLE

'open ${CTL1}'; 'open ${CTL2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set grads off'; 'set grid off'; 'set hershey on'

'set xlint 10'; 'set ylint 10'

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

'set gxout vector'
'set cthick 10'
'set ccolor  0'
'vec uwnd.1;vwnd.2  -SCL 0.5 ${VSIZE} -P 20 20 -SL m/s'

'set xlab off'; 'set ylab off'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs uwnd.1;vwnd.2 -SCL 0.5 ${VSIZE} -P 'xx' 'yy' -SL m/s'



# LEGEND COLOR BAR
#x1=xl; x2=xr
#y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr
#y=y1
#'set strsiz 0.12 0.15'
#'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
x=(xl+xr)/2; 
y=yt+0.15
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' ${MMM} ${YYYY} ${LEV}hPa'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

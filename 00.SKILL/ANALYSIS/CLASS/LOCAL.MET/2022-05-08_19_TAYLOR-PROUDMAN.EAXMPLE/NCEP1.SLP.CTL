dset /work01/DATA/NCEP1/MON.MEAN/slp.mon.mean.nc
title monthly mean slp from the NCEP Reanalysis
undef -9.96921e+36
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 1 linear 0 1
tdef 892 linear 00Z01JAN1948 1mo
vars 1
slp=>slp  0  t,y,x  Sea Level Pressure
endvars

dset /work01/DATA/NCEP1/%y4/hgt.%y4.nc
title 4x daily NMC reanalysis 
undef -9.96921e+36
options template
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 17 levels 1000 925 850 700 600 500 400 300
 250 200 150 100 70 50 30 20 10
tdef 30000 linear 00Z01JAN2010 360mn
vars 1
hgt=>hgt  17  t,z,y,x  4xDaily Geopotential height
endvars


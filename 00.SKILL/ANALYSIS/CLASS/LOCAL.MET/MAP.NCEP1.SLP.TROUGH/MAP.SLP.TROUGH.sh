#!/bin/bash

# Sat, 24 Jul 2021 12:13:35 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/MAP.SLP.TROUGH

CTL1=SLP.CTL
CTL2=HGT.CTL
GS=$(basename $0 .sh).GS


LONW=60 ;LONE=180
LATS=20 ;LATN=60
LEV=300
TIME=12Z30OCT2018

FIG=$(basename $0 .sh)_${TIME}.eps

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname)
CWD=$(pwd)
NOW=$(date -R)
CMD="$0 $@"

cat << EOF > ${GS}

# Sat, 24 Jul 2021 12:13:35 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/MAP.SLP.TROUGH

'open ${CTL1}'
'open ${CTL2}'
say
'q ctlinfo 1'
say sublin(result,1)
'q ctlinfo 2'
say sublin(result,1)
say

xmax = 1
ymax = 1

ytop=9

xwid = 6.5/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'


'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set time ${TIME}'
'set z 1'
'q dims'; say result
'set map 1 1 3'

'set grid off'
'set xlint 10'
'set ylint 10'
'set gxout contour'
'set ccolor 4'
'set cthick 2' 
'set cmin 900'
'set cmax 1040'
'set cint 4'
'set clskip 2'
'd slp.1/100'

'set xlab off'; 'set ylab off'
'set lev ${LEV}'
'set gxout contour'
'set ccolor 2'
'set cthick 2' 
'set cmin 8.0'
'set cmax 10.0'
'set cint 0.2'
#'set cmin 4.0'
#'set cmax 6.0'
#'set cint 0.02'
'set clskip 2'
'd hgt.2/1000'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr
#y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr
#y=y1
#'set strsiz 0.12 0.15'
#'set string 1 r 3 0'
#'draw string 'x' 'y' '


# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME}'
say 'x='x' y='y

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

#!/bin/bash

# Sat, 24 Jul 2021 17:19:21 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/MAP.JOFURO3.SST.SHF.LHF

#CTL=$(basename $0 .sh).CTL
INDIR=/work01/DATA/JOFURO3/J-OFURO3/V1.1/CLM/HR/
VAR1=SST
VAR2=SHF
VAR3=LHF
NC1=${VAR1}/J-OFURO3_${VAR1}_V1.1_CLM_HR_1988-2017.nc
NC2=${VAR2}/J-OFURO3_${VAR2}_V1.1_CLM_HR_1988-2017.nc
NC3=${VAR3}/J-OFURO3_${VAR3}_V1.1_CLM_HR_1988-2017.nc

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).eps

LONW=100 ;LONE=160
LATS=15  ;LATN=50
#LEV=
TIME0=00Z01JAN1800
TIME1=00Z01DEC1800

TEXT=${MMMYYYY}

LEVS="50 350 50"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->white->lightyellow->gold->orange->tomato->red->firebrick'
FS=2
UNIT=W/m2

HOST=$(hostname)
CWD=$(pwd)
NOW=$(date -R)
CMD="$0 $@"

cat << EOF > ${GS}

# Sat, 24 Jul 2021 17:19:21 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/LOCAL.MET/MAP.JOFURO3.SST.SHF.LHF

'sdfopen ${INDIR}${NC1}'
'sdfopen ${INDIR}${NC2}'
'sdfopen ${INDIR}${NC3}'
'q ctlinfo 1'; say result
'q ctlinfo 2'; say result
'q ctlinfo 3'; say result

xmax = 1
ymax = 1

ytop=9

xwid = 6.5/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME0}'

'set xlint 10'
'set ylint 5'
'set grid off'
'THF=ave(SHF.2+LHF.3,time=$TIME0,time=$TIME1)' 
'd THF'

'set xlab off'; 'set ylab off'
'var=SST.1'
'set gxout contour'
'set clab off' ;#LABEL OFF
'set cthick 5';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 2'   ;#CONTOUR INTERVAL
'set cmin 0'
'set cmax 30'
'd var'
'set clab on'
'set cthick 1'
'set ccolor 1' ;#BLACK
'set cint 2'
'set cmin 0'
'set cmax 30'
'set clskip 5'
'd  var'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1
y=y1
'set strsiz 0.12 0.15'
'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



# TEXT
x=(xl+xr)/2; 
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 1 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

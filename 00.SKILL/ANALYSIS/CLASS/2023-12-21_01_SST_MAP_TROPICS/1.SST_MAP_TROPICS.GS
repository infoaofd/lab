
'sdfopen /work01/DATA/J-OFURO3/V1.2_PRE/SST/J-OFURO_EM_SST_V0.5.1_MON_2022.nc'
'q ctlinfo';say result

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'
'set ylint 15'
'set xlopts 1 3 0.1';'set ylopts 1 3 0.1'
# SET COLOR BAR
'color 16 30 2 -kind midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson -gxout shaded'

# 'set lon  '; 
'set lat -30 30'
# 'set lev '
'set t 7' ;#'set time Z'

'd SST'

'set xlab off';'set ylab off'
'set gxout contour'
'set cint 1'
'set clab off'
'set ccolor 0';'set cthick 1'
'd SST'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl+0.5; x2=xr-0.3-0.5; y1=yb-0.35; y2=y1+0.08
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 1 -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' `ao`nC'


# TEXT
#x=xl ;# (xl+xr)/2; y=yt+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' '

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5
'draw string ' xx ' ' yy ' J-OFURO_EM_SST_V0.5.1_MON_2022.nc'
yy = yy+0.2
'draw string ' xx ' ' yy ' /work01/DATA/J-OFURO3/V1.2_PRE/SST'
yy = yy+0.2
'draw string ' xx ' ' yy ' /work09/am/16.TOOL/22.ANALYSIS/22.CLASS/2023-12-21_01_SST_MAP_TROPICS'
yy = yy+0.2
'draw string ' xx ' ' yy ' ./1.SST_MAP_TROPICS.sh '
yy = yy+0.2
'draw string ' xx ' ' yy ' Thu, 21 Dec 2023 02:22:29 +0900'

'gxprint 1.SST_MAP_TROPICS_J-OFURO_EM_SST_V0.5.1_MON_2022.PDF'
'quit'

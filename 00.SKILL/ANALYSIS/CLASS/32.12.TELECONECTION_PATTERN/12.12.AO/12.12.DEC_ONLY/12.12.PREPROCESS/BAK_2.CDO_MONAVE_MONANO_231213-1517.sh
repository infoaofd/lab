LEV=1000
SLAT=20
INDIR=/work01/DATA/NCEP1/MON
INFLE=hgt.mon.mean_${SLAT}N_${LEV}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1; fi

ODIR=$INDIR
OFLE_AVE=$(basename $INFLE .nc)_MONAVG.nc
OUT_AVE=$ODIR/$OFLE_AVE
OFLE_ANO=$(basename $INFLE .nc)_MONANO.nc
OUT_ANO=$ODIR/$OFLE_ANO

echo "MMMMM CLIMATOLOGY (NO AREA AVERAGE)"
rm -vf $OUT_AVE
#cdo ymonavg $IN $OUT_AVE 
cdo ymonmean $IN $OUT_AVE 
if [ ! -f $OUT_AVE ];then echo NO SUCH FILE, $OUT_AVE;exit 1; fi

echo MMMMM ANOMALY FROM AREA MEAN
TMP=$(basename $0 .sh)_TMP.nc; rm -vf $TMP
cdo sub $IN -enlarge,$IN -fldmean $IN $TMP

echo "MMMMM CLIMATOLOGY (SUBTRACTED AREA AVERAGE)"
TMP2=$(basename $0 .sh)_TMP2.nc; rm -vf $TMP2
cdo ymonmean $TMP $TMP2 
if [ ! -f $TMP2 ];then echo NO SUCH FILE, $TMP2;exit 1; fi

echo "MMMMM ANOMALY FROM CLIMATOLOGY (SUBTRACTED AREA AVERAGE)"
rm -vf $OUT_ANO
cdo ymonsub $TMP $TMP2 $OUT_ANO
if [ ! -f $OUT_ANO ];then echo NO SUCH FILE, $OUT_ANO;exit 1; fi

rm -vf $TMP $TMP2; echo

if [ -f $OUT_AVE ];then echo OUTPUT: $OUT_AVE;fi 
if [ -f $OUT_ANO ];then echo OUTPUT: $OUT_ANO;fi 



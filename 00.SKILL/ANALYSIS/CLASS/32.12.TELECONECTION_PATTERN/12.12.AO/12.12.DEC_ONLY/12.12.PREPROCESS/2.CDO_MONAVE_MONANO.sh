LEV=1000
SLAT=20
INDIR=/work01/DATA/NCEP1/MON
INFLE=hgt.mon.mean_${SLAT}N_${LEV}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1; fi

ODIR=$INDIR
OFLE_AVE=$(basename $INFLE .nc)_MONAVG.nc
OUT_AVE=$ODIR/$OFLE_AVE
OFLE_ANO=$(basename $INFLE .nc)_MONANO.nc
OUT_ANO=$ODIR/$OFLE_ANO

echo "MMMMM CLIMATOLOGY (NO AREA AVERAGE)"
rm -vf $OUT_AVE
#cdo ymonavg $IN $OUT_AVE 
cdo ymonmean $IN $OUT_AVE 
if [ ! -f $OUT_AVE ];then echo ERROR ymonmean;exit 1; fi
echo

echo "MMMMM ANOMALY FROM CLIMATOLOGY"
TMP=$(basename $0 .sh)_TMP.nc; rm -vf $TMP
cdo ymonsub $IN $OUT_AVE $TMP
if [ ! -f $TMP ];then echo ERROR ymonsub;exit 1; fi
echo

echo MMMMM REMOVE TREND
rm -vf $OUT_ANO
cdo detrend $TMP $OUT_ANO
if [ ! -f $OUT_ANO ];then echo ERROR detrend;exit 1; fi
echo

rm -vf $TMP $TMP2; echo

if [ -f $OUT_AVE ];then echo OUTPUT: $OUT_AVE;fi 
if [ -f $OUT_ANO ];then echo OUTPUT: $OUT_ANO;fi 



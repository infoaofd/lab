LEV=1000
SLAT=20
MON=12
INDIR=/work01/DATA/NCEP1/MON
INFLE=hgt.mon.mean_${SLAT}N_${LEV}_MONANO.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1; fi

ODIR=$INDIR
OFLE=$(basename $INFLE .nc)_${MON}.nc
OUT=$ODIR/$OFLE

rm -vf $OUT
cdo selmon,${MON} $IN $OUT
if [ ! -f $OUT ];then echo NO SUCH FILE, $OUT;exit 1; fi
echo;echo INPUT: $IN
if [ -f $OUT ];then echo OUTPUT: $OUT;fi 



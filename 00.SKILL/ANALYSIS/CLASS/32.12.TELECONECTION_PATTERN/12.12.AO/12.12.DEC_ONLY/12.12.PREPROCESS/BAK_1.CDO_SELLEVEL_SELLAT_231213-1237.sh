INDIR=/work01/DATA/NCEP1/MON
INFLE=hgt.mon.mean.nc
IN=$INDIR/$INFLE
LEV=1000
SLAT=20
ODIR=/work01/DATA/NCEP1/MON
OFLE=$(basename $INFLE .nc)_${SLAT}N_${LEV}.nc
OUT=$ODIR/$OFLE

TMP=$(basename $0 .sh)_TMP.nc
rm -vf $TMPj
cdo sellevel,$LEV $IN $TMP
if [ ! -f $TMP ];then echo NO SUCH FILE, $TMP;exit 1; fi

rm -vf $OUT
cdo sellonlatbox,0,360,${SLAT},90 $TMP $OUT
if [ -f $OUT ];then echo OUTPUT, $OUT;fi 
if [ ! -f $OUT ];then echo NO SUCH FILE, $OUT;exit 1; fi


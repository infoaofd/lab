; 
; PREPROC

script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

INDIR="/work01/DATA/ERA5/JPN/MON/"
INFLE="ERA5_T2m_120-160_20-50.grib"
IN=INDIR+INFLE
a=addfile(IN,"r")

xIN=a->2T_GDS0_SFC_S123
time=a->initial_time0_hours
lat=a->g0_lat_1
lon=a->g0_lon_2

  month_abbr = (/"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", "Oct","Nov","Dec"/)
  utc_date = cd_calendar(time, 0)
  year   = tointeger(utc_date(:,0))    ; Convert to integer for
  month  = tointeger(utc_date(:,1))    ; use sprinti 
  day    = tointeger(utc_date(:,2))
  hour   = tointeger(utc_date(:,3))
  minute = tointeger(utc_date(:,4))
  second = utc_date(:,5)

  date_str = sprinti("%0.2iZ ", hour) + sprinti("%0.2i ", day) + \
              month_abbr(month) + " "  + sprinti("%0.4i", year)
dims=dimsizes(time)
NE=dims(0)-9
NS=NE-32*12+1
print("INPUT FILE: START="+date_str(NS)) 
print("INPUT FILE: END  ="+date_str(NE)) 

dims=dimsizes(time)
NE=dims(0)-9
NS=NE-32*12+1
y1=1991
y2=2022

xMON=xIN(NS:NE,:,:)
xDJF = month_to_season (xMON, "DJF")

xDJF!0="time"
xDJF!1="lat"
xDJF!2="lon"

T2DJF     = xDJF(lat|:,lon|:,time|:) 

var   = dim_rmvmean(T2DJF)                    ; 時間平均を除去して偏差に
T2ADJF=T2DJF
T2ADJF   = dtrend(var,False)                  ; トレンドを除く
T2CDJF = dim_avg_n_Wrap(T2DJF,2)
printVarSummary(T2CDJF)

print("MMMMM PLOTTING")
; 作図開始

y=1991
IT=y-y1

FIG="ERA5_T2mA_DJF_"+tostring(y)
TYP="pdf"
; 図のファイル名とファイルの種類(pdf)を指定

wks = gsn_open_wks(TYP, FIG)
;作図するファイルを開く



res=True
res@gsnDraw          = False
res@gsnFrame         = False

; resmpの情報をresに引き継ぐ
res@cnFillOn = True
; Trueの場合, 色で塗分けする
res@cnLinesOn = False
; Falseの場合, 等値線を書かない

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = -4.
res@cnMaxLevelValF =  4.
res@cnLevelSpacingF =0.2


T2DJF!0 = "lat" ;T2DJFの0番目の配列要素の名称をlatにする
T2DJF!1 = "lon" ;T2DJFの0番目の配列要素の名称をlonにする
T2DJF&lat= lat 
; T2DJFのlatという座標の値として変数g0_lat_1に収納されているものを用いる
T2DJF&lon= lon 
; T2DJFのlonという座標の値として変数g0_lon_2に収納されているものを用いる

 res@gsnLeftString="ERA5 T2m DJF"
 res@gsnCenterString=""
 res@gsnRightString=tostring(y)

resmp=res
resmp@gsnAddCyclic = False
;地球一周分のデータでない場合Falseにする
  resmp@mpMinLatF        = 20                ; 地図の南端
  resmp@mpMaxLatF        = 50                ;       北端
  resmp@mpMinLonF        = 120                 ; 地図の端
  resmp@mpMaxLonF        = 160                 ; 地図の端
;res@mpCenterLonF           = 180
;図の中心となる経度を180度にする

plot=gsn_csm_contour_map(wks,T2ADJF(:,:,IT),resmp)
; カラーシェード図と地図の作図


res@cnFillOn = False
; Trueの場合, 色で塗分けする
res@cnLinesOn = True
; Falseの場合, 等値線を書かない
;res@cnLevelSelectionMode = "AutomaticLevels"
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 250.
res@cnMaxLevelValF = 300.
res@cnLevelSpacingF =5.
 res@gsnLeftString=""
 res@gsnCenterString=""
 res@gsnRightString=""
plot1=gsn_csm_contour(wks,T2CDJF(:,:),res)

overlay(plot,plot1)
draw(plot)
frame(wks)

print("MMMMM FIG: "+FIG+"."+TYP)
print("")

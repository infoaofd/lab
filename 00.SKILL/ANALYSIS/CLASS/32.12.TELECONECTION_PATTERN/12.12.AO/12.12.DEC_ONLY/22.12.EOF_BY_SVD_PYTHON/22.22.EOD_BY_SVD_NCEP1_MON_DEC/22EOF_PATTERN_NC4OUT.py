# -*- coding:utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

INFLE="hgt.mon.mean_20N_1000_MONANO_12.nc"
M=2 #MODE NUMBER
OFLE="hgt.mon.mean_20N_1000_MONANO_12_M"+str(M)+".nc"


ds=xr.open_dataset(INFLE)

import cartopy.crs as ccrs

NLON=144
NLAT=29

#print(ds.lon)
#print(ds.lat)

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = ds.hgt * wgt
X = X.data.reshape(X.shape[0],-1) #.transpose()
# Xの行=時刻, Xの列＝データ点
print(X.shape)

U, s, V = np.linalg.svd(X)
# U,Vは直交行列

# M番目のモードを取り出す
VM=V[M-1,:]*s[M-1] 
# sを掛けて次元量に戻す
print(VM.shape)
D=VM.reshape([NLAT,NLON])
D=-D #nclの計算と符号を合わせる
print(D.shape)


import netCDF4
from numpy import dtype

nc = netCDF4.Dataset(OFLE, 'w', format='NETCDF3_CLASSIC')
#nc.createDimensions('time', None)        # unlimitedにする場合
nc.createDimension('lat', NLAT)                 # e.g. x = 10
nc.createDimension('lon', NLON)                # e.g. y = 10

# その後，各変数を定義します．
# 以下の例では，時間，緯度，経度，3次元変数を定義します．

#time = nc.createVariable('time', dtype('double').char, ('time',))
#time.long_name = 'time'
#time.units = 'hours since 1800-01-01 00:00:0.0'

lat = nc.createVariable('lat', dtype('double').char, ('lat'))
lat.long_name = 'north latitude'
lat.units = 'degrees_north'

lon = nc.createVariable('lon', dtype('double').char, ('lon'))
lon.long_name = 'east longitude'
lon.units = 'degrees_east'

var = nc.createVariable('hgt', dtype('float').char, ('lat', 'lon'))
var.long_name = 'EOF_hgt'
var.units = 'm'

# 最後に，予め np.ndarray 等で作成しておいた値を代入します．

#time[:] = ds.time
lon[:] = ds.lon
lat[:] = ds.lat
var[:,:] = D

nc.close()

print("OUTPUT: "+OFLE)



import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("ERA5_T2m_120-160_20-50_DJF_ANO.nc")

import cartopy.crs as ccrs


def plot_nps(lon, lat, M, data):
#    datac, lonc = add_cyclic_point(data, lon)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111,projection=ccrs.PlateCarree())
    p = ax.contourf(lon, lat, data, transform=ccrs.PlateCarree())
    ax.set_extent([120, 160, 20, 50], ccrs.PlateCarree())
    fig.colorbar(p)
    ax.coastlines()
    ax.set_title('ERA5 T2m DFJ EOF MODE='+str(M))
    fig.savefig("ERA5_T2m_DJF_EOF_M"+str(M)+".PDF")

dslp = ds.T2ADJF

#print(ds.lon)
#print(ds.lat)

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = (X.where(ds.lat<60, drop=True)).data.reshape(X.shape[0],-1).transpose()

U, s, V = np.linalg.svd(X)

D = (dslp.data.transpose() @ V.transpose()).transpose()

M=1
plot_nps(ds.lon, ds.lat, M, D[M-1])
M=2
plot_nps(ds.lon, ds.lat, M, D[M-1])


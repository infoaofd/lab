"""
第0次元: 時間(要素数4)
第1次元: 緯度(要素数3)
第2次元: 経度(要素数2)
とする

"""
print("MMMMM 1. SET INPUT DATA: X")
import numpy as np
X = np.arange(24).reshape(4, 3, 2)
print("X")
print(X)
print("")

print("MMMMM 2. LON,LAT -> 1D & TRANSPOSE")
X1 = X.reshape([4,6]).transpose()
print(X)
print("MMMMM RAW: POSITION, COLUMN: TIME")
print("")

print("MMMMM 3. SVD")
U, s, V = np.linalg.svd(X1)
print("U")
print(U)
print("s")
print(s)
print("V")
print(V)
print("")

print("MMMMM 4. PROJECTION")
print("D=(X.transporse() @ V.transpose() ).transpose()")
D = (X.transpose() @ V.transpose()).transpose()
"""
transpose()
(0次元目, 1次元目, 2次元目)という次元（軸）の順番が反転して(2次元目, 1次元目, 0次元目)という順番になっている。
"""
print("")

print("MMMMM 5. SPATIAL PATTERN OF EACH MODE")
M=1
print("M="+str(M))
print(D[M-1])
print("")
M=2
print("M="+str(M))
print(D[M-1])
print("")

print("MMMMM 6. CONTRIBUTION RATE")
print("s")
print(s)
print("s * s")
print(s * s)
print("s @ s")
print(s @ s)
contrib = (s * s) / (s @ s) * 100
print("contribution rate")
print(contrib[0:4])



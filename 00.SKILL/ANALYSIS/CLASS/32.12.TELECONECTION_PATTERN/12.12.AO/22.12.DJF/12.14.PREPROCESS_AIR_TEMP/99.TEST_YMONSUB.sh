LEV=1000
SLAT=20
INDIR=/work01/DATA/NCEP1/MON
INFLE=hgt.mon.mean_${SLAT}N_${LEV}.nc
IN=$INDIR/$INFLE
echo $IN; echo

cdo -b F64 outputf,%64.32f,1 -fldmean -timmean -ymonsub $IN -ymonmean $IN

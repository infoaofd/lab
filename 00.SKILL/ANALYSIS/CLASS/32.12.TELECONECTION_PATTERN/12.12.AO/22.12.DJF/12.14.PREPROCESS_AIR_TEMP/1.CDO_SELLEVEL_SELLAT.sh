INDIR=/work01/DATA/NCEP1/MON
INFLE=air.mon.mean.nc
IN=$INDIR/$INFLE
LEV=925
SLAT=20
ODIR=/work01/DATA/NCEP1/MON
OFLE=$(basename $INFLE .nc)_${SLAT}N_${LEV}.nc
OUT=$ODIR/$OFLE

TMP=$(basename $0 .sh)_TMP.nc
rm -vf $TMP
cdo sellevel,$LEV $IN $TMP
if [ ! -f $TMP ];then echo NO SUCH FILE, $TMP;exit 1; fi

TMP2=$(basename $0 .sh)_TMP2.nc
rm -vf $TMP2
cdo --reduce_dim -copy $TMP $TMP2
if [ ! -f $TMP2 ];then echo NO SUCH FILE, $TMP2;exit 1; fi

TMP3=$(basename $0 .sh)_TMP3.nc
rm -vf $TMP3
cdo sellonlatbox,0,360,${SLAT},90 $TMP2 $TMP3
if [ ! -f $TMP3 ];then echo NO SUCH FILE, $TMP3;exit 1; fi

rm -vf $OUT
cdo setattribute,air@p=$LEV $TMP3 $OUT 

rm -vf $TMP $TMP2 $TMP3

if [ -f $OUT ];then echo; echo OUTPUT, $OUT;fi 

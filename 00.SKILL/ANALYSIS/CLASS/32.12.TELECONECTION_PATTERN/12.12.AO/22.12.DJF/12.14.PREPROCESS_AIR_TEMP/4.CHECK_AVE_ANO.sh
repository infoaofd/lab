#!/bin/bash

# Tue, 12 Dec 2023 21:06:01 +0900
# /work09/am/16.TOOL/22.ANALYSIS/22.CLASS/32.12.TELECONECTION_PATTERN/12.12.AO/12.12.PREPROCESS

YYYY=$1; YYYY=${YYYY:-2022}
HH=00; DD=01; SEA=DJF; MMM=JAN
TIME=${HH}Z${DD}${MMM}${YYYY}

DSET=NCEP1; LEV=925; SLAT=20; MON=1
INDIR=/work01/DATA/NCEP1/MON
INFLE1=air.mon.mean_${SLAT}N_${LEV}_DJF_ANO.nc; IN1=$INDIR/$INFLE1
INFLE2=air.mon.mean_${SLAT}N_${LEV}_DJF_AVG.nc; IN2=$INDIR/$INFLE2
TEXT="T${LEV} ${SEA} ${YYYY}"
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

GS=$(basename $0 .sh).GS
FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR
FIG=${FIGDIR}/${DSET}_air_${SLAT}N_${LEV}_${SEA}_${YYYY}.png

LATS=20; LATN=90; #LONW= ;LONE=

LEVS="-10 10 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2
UNIT=K

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN1}'
'sdfopen ${IN2}'


xmax = 1; ymax = 1

ytop=10

xwid = 7.5/xmax; ywid = 7.5/ymax

xmargin=0.1; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grads off';'set grid off';'set frame off'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set dfile 1'
'set lat ${LATS} ${LATN}' ;#'set lon -140 140'; 
'set z 1' ;#'set lev ${LEV}'
'set time ${TIME}'
'q dims';say result

'set mproj nps';'set map 1 1 6'
'd air.1'
'circlon 30'

'set dfile 2'
'set t 1'
'set gxout contour'
'set clopts 1 4 0.12'
'set cint 50'
'd air.2'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.15 -fh 0.2 -fs $FS -ft 5 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.15 0.2'; 'set string 1 l 4 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.5
'set strsiz 0.2 0.25'; 'set string 1 c 6 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5
#'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
#'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
#'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

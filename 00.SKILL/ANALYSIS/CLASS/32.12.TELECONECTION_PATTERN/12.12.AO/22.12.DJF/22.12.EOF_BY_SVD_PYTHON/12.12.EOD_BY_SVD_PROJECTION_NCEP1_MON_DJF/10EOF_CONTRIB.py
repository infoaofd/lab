import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("hgt.mon.mean_20N_1000_DJF_ANO.nc")

import cartopy.crs as ccrs

slp = ds.hgt
slp_clim = slp.mean(axis=0)
slp_stddev = slp.std(axis=0)
dslp = (slp - slp_clim) / slp_stddev


wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = (X.where(ds.lat>20, drop=True)).data.reshape(X.shape[0],-1).transpose()

U, s, V = np.linalg.svd(X)

contrib = (s * s) / (s @ s) * 100

plt.rcParams["font.size"] = 15
x = list(range(1, 11))
fig, ax = plt.subplots(figsize=(8,8))
ax.plot(x,contrib[0:10])
ax.plot(x,contrib[0:10],'bo')
ax.set_title("contribution rate %", fontsize=14)
ax.grid()
FIG="10EOF_CONTRIB_hgt.mon.mean_20N_1000_DFJ_ANO.PDF"
fig.savefig(FIG)
print("FIG: "+FIG)

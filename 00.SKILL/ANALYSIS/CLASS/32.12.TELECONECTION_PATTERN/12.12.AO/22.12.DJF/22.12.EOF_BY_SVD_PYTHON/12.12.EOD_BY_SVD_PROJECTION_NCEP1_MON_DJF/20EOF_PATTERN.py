# -*- coding:utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("hgt.mon.mean_20N_1000_DJF_ANO.nc")

import cartopy.crs as ccrs


def plot_map(lon, lat, M, data):
#    datac, lonc = add_cyclic_point(data, lon)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111,projection=ccrs.PlateCarree())
    p = ax.contourf(lon, lat, data, transform=ccrs.PlateCarree())
    ax.set_extent([100, 200, 20, 60], ccrs.PlateCarree())
    fig.colorbar(p)
    ax.coastlines()
    ax.set_title('NCEP1 Z1000 DEC EOF MODE='+str(M))
    fig.savefig("hgt.mon.mean_20N_1000_MONANO_12_MODE"+str(M)+".PDF")

dslp = ds.hgt
print(dslp.shape)

NLON=144
NLAT=29

#print(ds.lon)
#print(ds.lat)

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = X.data.reshape(X.shape[0],-1) #.transpose()
# Xの行=時刻, Xの列＝データ点
print(X.shape)

U, s, V = np.linalg.svd(X)
# U,Vは直交行列

M=1 #MODE NUMBER
VM=V[M-1,:]*s[M-1] 
# sを掛けて次元量に戻す
print(VM.shape)
D=VM.reshape([NLAT,NLON])
D=-D #nclの計算と符号を合わせる
print(D.shape)
plot_map(ds.lon, ds.lat, M, D)

M=2
VM=V[M-1,:]*s[M-1]
# sを掛けて次元量に戻す
print(VM.shape)
D=VM.reshape([NLAT,NLON])
D=-D #nclの計算と符号を合わせる
print(D.shape)
plot_map(ds.lon, ds.lat, M, D)

# -*- coding:utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("hgt.mon.mean_20N_1000_DJF_ANO.nc")

import cartopy.crs as ccrs

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = ds.hgt * wgt
X = X.data.reshape(X.shape[0],-1) #.transpose()
print(X.shape)
# Xの行＝時刻, Xの列＝地点 (20番のスクリプトの転置を使っているのに注意)
U, s, V = np.linalg.svd(X)
# U,Vは直交行列（列ベクトルはすべて規格化済み）

D = U #.transpose()
D=-D #nclの計算と符号を合わせる
print(U.shape)
# Uの第m列＝第mモードの時系列


x = list(range(1948, 2022))


print("")
M=1
y = D[:,M-1]
fig = plt.figure(1)
plt.plot(x, y)
FIG="hgt.mon.mean_20N_1000_DJF_M"+str(M)+".PDF"
fig.savefig(FIG)
print("FIG: "+FIG)

M=2
y = D[:,M-1]
fig = plt.figure(2)
plt.plot(x, y)
FIG="hgt.mon.mean_20N_1000_DJF_M"+str(M)+".PDF"
fig.savefig(FIG)
print("FIG: "+FIG)

print("")


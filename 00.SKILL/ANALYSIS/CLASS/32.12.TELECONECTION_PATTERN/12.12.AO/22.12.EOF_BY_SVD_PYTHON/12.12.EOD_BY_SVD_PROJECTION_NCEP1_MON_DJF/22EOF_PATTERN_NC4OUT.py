# -*- coding:utf-8 -*-
# 元のデータをVに射影したものをモードの空間分布としている。
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

M=1 #MODE NUMBER
INFLE="hgt.mon.mean_20N_1000_DJF_ANO.nc"
OFLE="hgt.mon.mean_20N_1000_DJF_M"+str(M)+".nc"


ds=xr.open_dataset(INFLE)

import cartopy.crs as ccrs

NLON=144
NLAT=29

#print(ds.lon)
#print(ds.lat)

slp = ds.hgt
slp_clim = slp.mean(axis=0)
slp_stddev = slp.std(axis=0)
dslp = (slp - slp_clim) / slp_stddev

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = (X.where(ds.lat>20, drop=True)).data.reshape(X.shape[0],-1).transpose()
#X = X.data.reshape(X.shape[0],-1) #.transpose()
print(X.shape)

U, s, V = np.linalg.svd(X)
# U,Vは直交行列

# 元のデータを射影するVに射影して各モードの空間パターンを得る。
D = (dslp.data.transpose() @ V.transpose()).transpose()


import netCDF4
from numpy import dtype

nc = netCDF4.Dataset(OFLE, 'w', format='NETCDF3_CLASSIC')
#nc.createDimensions('time', None)        # unlimitedにする場合
nc.createDimension('lat', NLAT)                 # e.g. x = 10
nc.createDimension('lon', NLON)                # e.g. y = 10

# その後，各変数を定義します．
# 以下の例では，時間，緯度，経度，3次元変数を定義します．

#time = nc.createVariable('time', dtype('double').char, ('time',))
#time.long_name = 'time'
#time.units = 'hours since 1800-01-01 00:00:0.0'

lat = nc.createVariable('lat', dtype('double').char, ('lat'))
lat.long_name = 'north latitude'
lat.units = 'degrees_north'

lon = nc.createVariable('lon', dtype('double').char, ('lon'))
lon.long_name = 'east longitude'
lon.units = 'degrees_east'

var = nc.createVariable('hgt', dtype('float').char, ('lat', 'lon'))
var.long_name = 'EOF_hgt'
var.units = 'm'

# 最後に，予め np.ndarray 等で作成しておいた値を代入します．

#time[:] = ds.time
lon[:] = ds.lon
lat[:] = ds.lat
var[:,:] = D[M-1]

nc.close()

print("OUTPUT: "+OFLE)



import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-10, 13.1, 0.1) #x軸の描画範囲の生成。0から10まで0.05刻み。
y = np.arange(-4.0, 4.1, 0.1) #y軸の描画範囲の生成。0から10まで0.05刻み。

X, Y = np.meshgrid(x, y)
Z = 0.5*(Y**2-3)*np.exp(-Y**2/4)  
plt.axes().set_aspect('equal') # グラフのアスペクト比を揃える

plt.contourf(X, Y, Z, cmap="RdBu_r", vmin=-1,vmax=1)


pp=plt.colorbar (orientation="horizontal") # カラーバーの表示 
pp.set_label("Label",  fontsize=12)

cont=plt.contour(X,Y,Z,  5, vmin=-1,vmax=1, colors=['black'])
cont.clabel(fmt='%1.1f', fontsize=10)

plt.xlabel('X', fontsize=16)
plt.ylabel('Y', fontsize=16)




FIG="1MATSUNO_GILL_MAP.PDF"
plt.savefig(FIG)

print("FIG: "+FIG)

"""
https://qiita.com/sci_Haru/items/5b4c34d5330a545001cf
"""

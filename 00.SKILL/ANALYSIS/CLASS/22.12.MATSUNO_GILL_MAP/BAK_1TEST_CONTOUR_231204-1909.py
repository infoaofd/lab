import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0, 10, 0.05) #x軸の描画範囲の生成。0から10まで0.05刻み。
y = np.arange(0, 10, 0.05) #y軸の描画範囲の生成。0から10まで0.05刻み。

X, Y = np.meshgrid(x, y)
Z = np.sin(X) + np.cos(Y)   # 表示する計算式の指定。等高線はZに対して作られる。

# 等高線図の生成。
cont=plt.contour(X,Y,Z,  5, vmin=-1,vmax=1, colors=['black'])
cont.clabel(fmt='%1.1f', fontsize=10)


plt.xlabel('X', fontsize=16)
plt.ylabel('Y', fontsize=16)


plt.pcolormesh(X,Y,Z, cmap='RdBu_r') #カラー等高線図
pp=plt.colorbar (orientation="vertical") # カラーバーの表示 
pp.set_label("Label",  fontsize=18)

FIG="TEST_CONTOUR.PDF"
plt.savefig(FIG)

print("FIG: "+FIG)

"""
https://qiita.com/sci_Haru/items/5b4c34d5330a545001cf
"""

from scipy.special import pbdv
import numpy as np
import matplotlib.pyplot as plt

def Dv(v,x):
    return pbdv(v,x)

v=3 #MODE
n=int(v)
FIG="PARABOLIC_CYLINDER_"+str(n)+".PDF"

x = np.linspace(-5, 5, 101)

plt.plot(x, Dv(v, x)[0])

plt.xlabel('y')
plt.ylabel('Dn(y)')
#plt.grid()
plt.xlim(-5, 5)
plt.ylim(-1.2, 1.2)
plt.title('Parabolic Cylinder n='+str(n));

plt.savefig(FIG)

print("FIG: "+FIG)

"""
scipy.special.pbdv(v, x, out=None) = <ufunc 'pbdv'>
Parabolic cylinder function D

Returns (d, dp) the parabolic cylinder function Dv(x) in d and the derivative, Dv’(x) in dp.

Parameters:
v: array_like
Real parameter

x: array_like
Real argument

outndarray, optional
Optional output array for the function results

Returns:
d: scalar or ndarray
Value of the function

dp: scalar or ndarray
Value of the derivative vs x
"""


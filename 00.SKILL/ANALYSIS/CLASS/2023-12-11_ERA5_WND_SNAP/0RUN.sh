#!/bin/bash

yyyymmdd1=20220613; yyyymmdd2=20220620

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}



jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  ih=0
  while [ $ih -le 22 ]; do

  hh=$(printf %02d $ih)
  echo MMMMM $yyyy $mm $dd $hh

  1ERA5_WND_SNAP.sh $yyyy$mm$dd$hh

  ih=$(expr $ih + 2)
  done

 i=$(expr $i + 1)
done

exit 0


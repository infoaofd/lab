import numpy as np
import matplotlib.pyplot as plt

import numpy as np
import matplotlib.pyplot as plt
import math

a=1
x = np.arange(-10, 10, 0.1) # x軸
y = np.arange(-10, 10, 0.1) # y軸

X, Y = np.meshgrid(x, y)

rho=np.sqrt(X**2 + Y**2)

"""
2次元ポアソン方程式の解
今村勤 (1978). 物理とグリーン関数, p.115
"""
"""
                      0   if x1 < 0
heaviside(x1, x2) =  x2   if x1 == 0
                      1   if x1 > 0
"""


mask_outer=np.heaviside((rho-a), 0)
mask_inner=np.heaviside((a-rho), 0)

def f(t):
    return np.log(t)

phi = (mask_outer * f(rho)  + mask_inner * f(a) )
phi= - phi

print(phi)

fig = plt.figure()
ax = fig.add_subplot(111)

cont = plt.contour(X, Y, phi)

ax.set_aspect("equal", adjustable="box")

FIG="1POISSON_2D_SOLUTION.PDF"

plt.savefig(FIG)
print("FIG: "+FIG)


"""
only size-1 arrays can be converted to Python scalars

問題のソースコード
import matplotlib.pyplot as plt
import math
import np
x = np.linspace(0,1, 10000);
def y(a):
    return math.exp(a)
plt.figure(0)
plt.plot(x, y(x))

原因
mathの関数は行列を扱うことができない
expをnumpyの関数をつかってあげれば解決

plt.plot(x, y(x))
この部分では行列投げ込んでるから注意

よって

def y(a):
    return np.exp(a)
としたら治る

解決後のコード
import matplotlib.pyplot as plt
import math
import numpy
x = np.linspace(0,1, 10000);
def y(a):
    return np.exp(a)
plt.figure(0)
plt.plot(x, y(x))
"""

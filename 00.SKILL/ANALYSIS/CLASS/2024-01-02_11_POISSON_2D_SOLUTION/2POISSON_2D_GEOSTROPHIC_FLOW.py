import numpy as np
import matplotlib.pyplot as plt

import numpy as np
import matplotlib.pyplot as plt
import math

a=0.5
x = np.arange(-10, 10, 0.5) # x軸
y = np.arange(-10, 10, 0.5) # y軸

X, Y = np.meshgrid(x, y)



print("MMMMM SOLUTION OF 2D POISSON EQ (nabla^2 = zeta)")
"""
2次元ポアソン方程式の解
今村勤 (1978). 物理とグリーン関数, p.115
"""

rho=np.sqrt(X**2 + Y**2)

"""
                      0   if x1 < 0
heaviside(x1, x2) =  x2   if x1 == 0
                      1   if x1 > 0
"""

mask_outer=np.heaviside((rho-a), 0)
mask_inner=np.heaviside((a-rho), 0)

def f(t):
    return np.log(t)

phi = (mask_outer * f(rho)  + mask_inner * f(a) )

#print(phi)

print("MMMMM GEOSTROPHIC FLOW")
"""
Ey,Ex = numpy.gradient(E ,.2, .2)
第1引数(E)：勾配を求めたい関数
戻り値(Ey,Ex)：E(x, y)のy方向の勾配とx方向の勾配(順番に注意)
"""
u,v = np.gradient(phi ,.2, .2)
u=-u


print("MMMMM PLOT FIGURE")

fig = plt.figure()
ax = fig.add_subplot(111)

vec=plt.quiver(X,Y,u,v,angles="xy",headwidth=5,headlength=5,scale=15)
plt.xlim([-5, 5])
plt.ylim([-5, 5])
#plt.legend()

ax.set_aspect("equal", adjustable="box")

import matplotlib.patches as pat
C = pat.Circle(xy = (0, 0), radius = a, color = "red", alpha=0.2)
ax.add_patch(C)

"""
def draw_circle(ax, radius, P=(0,0), n=257,
                color="red", center_marker=True):

    theta = np.linspace(0, 2*np.pi, n)

    x = P[0] + radius * np.cos(theta)
    y = P[1] + radius * np.sin(theta)

    ax.set_xlabel("x", fontsize = 14)
    ax.set_ylabel("y", fontsize = 14)

    ax.plot(x, y, color=color)

draw_circle(ax, radius=1, P=(0, 0))
"""

FIG="2POISSON_2D_GEOSTROPHIC_FLOW.PDF"

plt.savefig(FIG)
print("FIG: "+FIG)


"""
only size-1 arrays can be converted to Python scalars

問題のソースコード
import matplotlib.pyplot as plt
import math
import np
x = np.linspace(0,1, 10000);
def y(a):
    return math.exp(a)
plt.figure(0)
plt.plot(x, y(x))

原因
mathの関数は行列を扱うことができない
expをnumpyの関数をつかってあげれば解決

plt.plot(x, y(x))
この部分では行列投げ込んでるから注意

よって

def y(a):
    return np.exp(a)
としたら治る

解決後のコード
import matplotlib.pyplot as plt
import math
import numpy
x = np.linspace(0,1, 10000);
def y(a):
    return np.exp(a)
plt.figure(0)
plt.plot(x, y(x))
"""

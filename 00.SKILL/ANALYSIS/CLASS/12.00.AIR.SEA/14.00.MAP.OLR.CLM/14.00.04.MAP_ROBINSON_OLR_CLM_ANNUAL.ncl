
;************************************************
; This file is loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
;************************************************
begin

MMM="ANNUAL"
PROJ="ROBINSON"

;CLON=300; 130 ;220 ;270.0       ; choose center lon
CLON=130 ;220 ;270.0       ; choose center lon
CLAT=0

INDIR="/work01/DATA/OLR/NOAA/"
NC1="olr.mon.ltm.nc"

f1 = addfile(INDIR+NC1,"r") 
;************************************************
; unpack data and convert from Pa to hPa
;************************************************
VAR = f1->olr
printVarSummary(VAR)


VARTAV=dim_avg_n_Wrap(VAR, 0)
printVarSummary(VARTAV)
VARTAV!0="lat"
VARTAV!1="lon"
VARTAV&lat=VAR&lat
VARTAV&lon=VAR&lon

copy_VarAtts(VAR, VARTAV)                    

VARTAV@units = "W/m2"
VARTAV@long_name = "OLR "+MMM

;************************************************
; plotting parameters
;************************************************
LONLAT=tostring(CLON)+"_"+tostring(CLAT)
wks = gsn_open_wks("png","OLR_NOAA_CLIM_"+PROJ+"_"+MMM+"_"+LONLAT)             ; send graphics to PNG file

gsn_define_colormap(wks,"spread_15lev") ;ncl_default") ;NCV_jaisnd")
;gsn_reverse_colormap(wks)

  res                            = True       ; plot mods desired

  res@gsnMaximize                = True

;  res@cnHighLabelBackgroundColor = -1         ; make H background transparent
;  res@cnLowLabelsOn              = True       ; turn on L labels
;  res@cnLowLabelFontHeightF      = 0.024      ; change L font
;  res@cnLowLabelBackgroundColor  = -1

  res@cnLinesOn = False
  res@cnLabelDrawOrder           = "PostDraw" ; draw labels over lines

  res@mpLandFillColor = 1

  res@mpProjection               = "Robinson" ;"Satellite" ; choose map projection
  res@mpCenterLonF               = CLON; 130 ;220 ;270.0       ; choose center lon
  res@mpCenterLatF               = CLAT; 45 ;45.         ; choose center lat
;  res@mpSatelliteDistF           = 3.0         ; choose satellite view
res@mpPerimOn = False ; frame around satellite-projection plot

;  res@mpFillOn                   = True        ; color continents
;  res@mpFillColors               = (/"white","lightcyan","lightgray","lightcyan"/); colors to draw
  res@mpOutlineOn                = True        ; turn on continental outlines
;  res@mpOutlineBoundarySets      = "National"  ; add country boundaries
  res@mpGridLineDashPattern      = 2           ; make lat/lon lines dash
	
  res@cnLevelSelectionMode 	= "ManualLevels"  ; manually set cont levels

res@cnMinLevelValF = 200.
res@cnMaxLevelValF = 250.
res@cnLevelSpacingF = 5.

  res@tiMainString    = "OLR "+MMM+" 1991-2010" ; add title

res@cnFillOn = True
res@pmLabelBarOrthogonalPosF = 0.02
res@pmLabelBarHeightF = 0.05
  map = gsn_csm_contour_map(wks,VARTAV(:,:),res)
end

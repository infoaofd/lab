
;************************************************
; This file is loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
;************************************************
begin

MMM="ANNUAL"

;CLON=300; 130 ;220 ;270.0       ; choose center lon
CLON=130 ;220 ;270.0       ; choose center lon
CLAT=45 

INDIR="/work01/DATA/JOFURO3/J-OFURO3/V1.1/CLM/HR/"
NC1="LHF/J-OFURO3_LHF_V1.1_CLM_HR_1988-2017.nc"
NC2="SHF/J-OFURO3_SHF_V1.1_CLM_HR_1988-2017.nc"

f1 = addfile(INDIR+NC1,"r") 
f2 = addfile(INDIR+NC2,"r") 
;************************************************
; unpack data and convert from Pa to hPa
;************************************************
V1 = f1->LHF
V2 = f2->SHF
printVarSummary(V1)

VAR= V1 + V2

VARTAV=dim_avg_n_Wrap(VAR, 0)
printVarSummary(VARTAV)
VARTAV!0="lat"
VARTAV!1="lon"
VARTAV&lat=V1&latitude
VARTAV&lon=V1&longitude

copy_VarAtts(V1, VARTAV)                    

VARTAV@units = "W/m2"
VARTAV@long_name = "THF "+MMM

;************************************************
; plotting parameters
;************************************************
LONLAT=tostring(CLON)+"_"+tostring(CLAT)
wks = gsn_open_wks("png","THF_J-OFURO_1988-2017_"+MMM+"_"+LONLAT)             ; send graphics to PNG file

gsn_define_colormap(wks,"precip3_16lev") ;ncl_default") ;NCV_jaisnd")

  res                            = True       ; plot mods desired

  res@gsnMaximize                = True

;  res@cnHighLabelBackgroundColor = -1         ; make H background transparent
;  res@cnLowLabelsOn              = True       ; turn on L labels
;  res@cnLowLabelFontHeightF      = 0.024      ; change L font
;  res@cnLowLabelBackgroundColor  = -1

  res@cnLabelDrawOrder           = "PostDraw" ; draw labels over lines

  res@mpLandFillColor = 1

  res@mpProjection               = "Satellite" ; choose map projection
  res@mpCenterLonF               = CLON; 130 ;220 ;270.0       ; choose center lon
  res@mpCenterLatF               = CLAT; 45 ;45.         ; choose center lat
  res@mpSatelliteDistF           = 3.0         ; choose satellite view
res@mpPerimOn = False ; frame around satellite-projection plot

;  res@mpFillOn                   = True        ; color continents
;  res@mpFillColors               = (/"white","lightcyan","lightgray","lightcyan"/); colors to draw
  res@mpOutlineOn                = True        ; turn on continental outlines
;  res@mpOutlineBoundarySets      = "National"  ; add country boundaries
  res@mpGridLineDashPattern      = 2           ; make lat/lon lines dash
	
  res@cnLevelSelectionMode 	= "ManualLevels"  ; manually set cont levels

res@cnMinLevelValF =  30.
res@cnMaxLevelValF = 300.
res@cnLevelSpacingF = 30.

  res@tiMainString    = "THF "+MMM+" 1998-2017" ; add title

res@cnFillOn = True
res@pmLabelBarOrthogonalPosF = 0.02
res@pmLabelBarHeightF = 0.05
  map = gsn_csm_contour_map(wks,VARTAV(:,:),res)
end

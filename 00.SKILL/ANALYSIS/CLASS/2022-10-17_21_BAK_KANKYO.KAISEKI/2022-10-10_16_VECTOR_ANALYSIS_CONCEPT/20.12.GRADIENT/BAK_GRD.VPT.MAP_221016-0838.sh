#!/bin/bash

Y=2020; MM=07; MMM=JUL; DD=03; HH=18
PLEV1=950
LONW=120; LONE=140
LATS=26 ; LATN=42

TIME=${HH}Z${DD}${MMM}${Y}

POW=5
FAC=1E${POW}
SCL=10\`a-${POW}\`n
# https://www.sci.hokudai.ac.jp/grp/poc/top/old/software/other/grads_tips/index.htm

UNIT="${SCL} K m\`a-1\`n"

VAR="GRAD VPT"
TITLE="${VAR} $TIME ${PLEV1}hPa"

INDIR=/work01/DATA/MSM/MSM-P
INFLE=${INDIR}/$Y/${MM}${DD}.nc

PREFIX=$(basename $0 .sh)
FIG=${PREFIX}_${Y}${MM}${DD}_${HH}_${PLEV1}.eps

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

# GRADS SCRIPT GOES HERE:
cat <<EOF>$GS
say

'sdfopen $INFLE'

'q ctlinfo 1'

'set time ${TIME}'
'set lev $PLEV1'
'set lon $LONW $LONE'
'set lat $LATS $LATN'

say '### VPT'
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV= 0.62197*(e/(lev-e))'
'PT=temp*pow(1000/lev,0.286)'
'VPT=PT*(1.0 + 0.61*QV)'

say '### GRAD'
var=VPT

pi=3.14159265359
dtr=pi'/'180

r=6.371e6

dx '=' r '*cos(' dtr '*' lat ')*' dtr '*cdiff(' lon ',' x')'
dy '=' r '*' dtr '*cdiff(' lat ',' y ')'

dtdx '=cdiff(' var ',' x ')/' dx
dtdy '=cdiff(' var ',' y ')/' dy

grad '=mag(' dtdx ',' dtdy ')*$FAC'


say '### PLOT'
'cc'
'set grads off'
'set grid off'
'set mpdset hires'



'set vpage 0.0 8.5 0.0 11'

xs=1; xe=6
ys=3; ye=10
'set parea 'xs ' 'xe' 'ys' 'ye


'set xlab on'
'set ylab on'
'set xlint 4'
'set ylint 2'

say '### COLOR SHADE'
'color 0.5 6 0.5 -kind white->red'

#'d maskout(grad,p/100)' ;#-lev)'
'd grad'

'set xlab off'
'set ylab off'


'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

x1=xr+0.4; x2=x1+0.1
y1=yb    ; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.12 -fh 0.16 -ft 4 -fs 2'

say '### SCALE & UNITS'
'set strsiz 0.12 0.16'
'set string 1 c 4 0'
xx=(x1+x2)/2+0.2
yy=y2+0.2
'draw string ' xx ' ' yy ' ${UNIT}'

say '### CONTOUR'
'set gxout contour'
'set ccolor 1'
'set cthick 2'
'set cint 1'
'set clskip 2'
'd 'vpt

say '### TITLE'
'set strsiz 0.12 0.14'
'set string 1 c 4 0'
xx = (xl+xr)/2; yy=yt+0.2
'draw string ' xx ' ' yy ' ${TITLE}'

say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yy+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

say '$INFLE'
say
'gxprint $FIG'
say
'!ls -lh $FIG'
say
'quit'
EOF

grads -bcp "$GS"

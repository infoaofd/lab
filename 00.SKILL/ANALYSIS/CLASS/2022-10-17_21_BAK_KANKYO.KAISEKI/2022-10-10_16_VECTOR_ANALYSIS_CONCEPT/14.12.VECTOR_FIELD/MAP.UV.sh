#!/bin/bash

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

#CTL=$(basename $0 .sh).CTL
INDIR="/work01/DATA/NCEP2/2022-10-10_16_NCEP2_MON"
INFLE1="uwnd.mon.mean.nc"; INFLE2="vwnd.mon.mean.nc"


GS=$(basename $0 .sh).GS

LONW=90 ;LONE=300
LATS=-10 ;LATN=70
LEV=1000; VMAG=25
#LEV=300; VMAG=50
YYYY=2022; MM=08; MMM=AUG
TIME=00Z01${MMM}${YYYY}

VAROUT=UV
FIG=${VAROUT}_${LEV}_MON_${YYYY}${MM}.eps

LEVS="8 40 1"
#LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2
UNIT=[C]

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Mon, 10 Oct 2022 16:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/TEACHING/KANKYO.KAISEKI/2022-10-10_16_VECTOR_ANALYSIS_CONCEPT/12.12.SCALAR_FIELD

'sdfopen ${INDIR}/${INFLE1}'
'sdfopen ${INDIR}/${INFLE2}'

xmax = 1
ymax = 1

ytop=9

xwid = 5.0/xmax
ywid = 5.0/ymax

xmargin=0.5
ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set hershey off'; 'set grid off'; 'set grads off'
#'color ${LEVS} -kind ${KIND} -gxout shaded'
'set map 1 1 2'

'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'

'set xlint 30';'set ylint 30'


'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(uwnd.1,4);vwnd.2 -SCL 0.5 $VMAG -P 20 20 -SL m/s'
'set xlab off'; 'set ylab off'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)


xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(uwnd.1,4);vwnd.2 -SCL 0.5 $VMAG -P 'xx' 'yy' -SL m/s'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)


'q dims'
LINE=sublin(result,5)
WORD=subwrd(LINE,6)
MMM=substr(WORD,6,3)
YYYY=substr(WORD,9,4)
say MMM' 'YYYY

# TEXT
x=(xl+xr)/2; 
y=yt+0.2
'set strsiz 0.12 0.15'
'set string 1 c 3 0'
'draw string 'x' 'y' $VAROUT ${LEV}hPa 'MMM' 'YYYY

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

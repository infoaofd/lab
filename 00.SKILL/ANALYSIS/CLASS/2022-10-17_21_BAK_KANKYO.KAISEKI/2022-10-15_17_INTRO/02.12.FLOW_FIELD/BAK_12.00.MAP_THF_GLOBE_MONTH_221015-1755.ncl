
;************************************************
; This file is loaded by default in NCL V6.2.0 and newer
; load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"   
;************************************************
begin

MM=1
MN1=MM-1
MMM="JAN"

CLON=300; 130 ;220 ;270.0       ; choose center lon
CLAT=45 

INDIR="/work01/DATA/JOFURO3/J-OFURO3/V1.1/CLM/HR/"
NC1="LHF/J-OFURO3_LHF_V1.1_CLM_HR_1988-2017.nc"
NC2="SHF/J-OFURO3_SHF_V1.1_CLM_HR_1988-2017.nc"

f1 = addfile(INDIR+NC1,"r") 
f2 = addfile(INDIR+NC2,"r") 
;************************************************
; unpack data and convert from Pa to hPa
;************************************************
V1 = f1->LHF
V2 = f2->SHF
;  printVarSummary(V1)
VAR = V1
VAR= V1 + V2

VAR@units = "W/m2"
VAR@long_name = "THF "+MMM

;************************************************
; plotting parameters
;************************************************
LONLAT=tostring(CLON)+"_"+tostring(CLAT)
wks = gsn_open_wks("png","THF_J-OFURO_1988-2017_"+MMM+"_"+LONLAT)             ; send graphics to PNG file
gsn_define_colormap(wks,"NCV_jaisnd")

  res                            = True       ; plot mods desired

  res@gsnMaximize                = True

;  res@cnHighLabelBackgroundColor = -1         ; make H background transparent
;  res@cnLowLabelsOn              = True       ; turn on L labels
;  res@cnLowLabelFontHeightF      = 0.024      ; change L font
;  res@cnLowLabelBackgroundColor  = -1

  res@cnLabelDrawOrder           = "PostDraw" ; draw labels over lines

  res@mpProjection               = "Satellite" ; choose map projection
  res@mpCenterLonF               = CLON; 130 ;220 ;270.0       ; choose center lon
  res@mpCenterLatF               = CLAT; 45 ;45.         ; choose center lat
  res@mpSatelliteDistF           = 3.0         ; choose satellite view

  res@mpFillOn                   = True        ; color continents
  res@mpFillColors               = (/"white","lightcyan","lightgray","lightcyan"/); colors to draw
  res@mpOutlineOn                = True        ; turn on continental outlines
  res@mpOutlineBoundarySets      = "National"  ; add country boundaries
  res@mpGridLineDashPattern      = 2           ; make lat/lon lines dash
	
  res@cnLevelSelectionMode 	= "ManualLevels"  ; manually set cont levels

res@cnMinLevelValF = -500.
res@cnMaxLevelValF = 500.
res@cnLevelSpacingF = 50.

  res@tiMainString    = "THF "+MMM+" 1998-2017" ; add title

res@cnFillOn = True
res@pmLabelBarOrthogonalPosF = 0.02
res@pmLabelBarHeightF = 0.05
  map = gsn_csm_contour_map(wks,VAR(MN1,:,:),res)
end

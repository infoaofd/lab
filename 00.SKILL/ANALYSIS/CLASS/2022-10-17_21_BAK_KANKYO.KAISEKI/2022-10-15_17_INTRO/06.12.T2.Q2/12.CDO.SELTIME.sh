INROOT=/work01/DATA/NCEP2/2022-10-10_16_NCEP2_MON
YYYY=2022;MM=09;DD=01;HH=12
INDIR=$INROOT/
INFLE1=air.2m.mon.mean.nc
INFLE2=shum.2m.mon.mean.nc

IN1=$INDIR/$INFLE1; IN2=$INDIR/$INFLE2

if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

OUT1=$(basename $IN1 .nc)_$YYYY${MM}.nc
OUT2=$(basename $IN2 .nc)_$YYYY${MM}.nc

DATE=${YYYY}-${MM}-${DD} #T${HH}:00:00
cdo seldate,$DATE,$DATE $IN1 $OUT1
cdo seldate,$DATE,$DATE $IN2 $OUT2

echo $OUT1; echo $OUT2;echo $OUT3

ncdump -h $OUT1


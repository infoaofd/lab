# OCEAN COLOR MONTHLY CLIMATOLOGY

https://climate.esa.int/en/projects/ocean-colour/data/

23-07-05_09-32
/work02/DATA/OCEAN-COLOR

FTP server: oceancolour.org
Username: oc-cci-data
Password: ELaiWai8ae

```bash
$ ftp oceancolour.org
Connected to oceancolour.org (192.171.164.182).
220 Welcome to the Plymouth Marine Laboratory FTP service.
Name (oceancolour.org:am): oc-cci-data
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
```

```bash
ftp> ls
227 Entering Passive Mode (192,171,164,182,160,228).
150 Here comes the directory listing.
-rw-rw-r--    1 27207    27207        2253 Mar 13 14:17 README.txt
drwxrwxr-x    2 27207    27207        4096 May 29 20:00 chl
-rwxrw-r--    1 27207    27207        9688 May 29 19:57 chl2022satellite.txt
drwxr-xr-x    2 0        0            4096 Jan 27  2022 occci-v3.1
drwxr-xr-x    2 0        0            4096 Jan 27  2022 occci-v3.1-appendices
dr-xr-xr-x    7 17166    15057         204 Feb 28  2020 occci-v4.2
drwxr-xr-x    7 17173    15057         204 Apr 12  2021 occci-v5.0
drwxrwxr-x    8 17173    15057         224 May 11 14:07 occci-v6.0
```

```
ftp> cd /occci-v6.0/climatology/netcdf
```

```bash
ftp> pwd
257 "/occci-v6.0/climatology/netcdf" is the current directory
ftp> ls
227 Entering Passive Mode (192,171,164,182,195,19).
150 Here comes the directory listing.
-rw-rw-r--    2 17173    15057    1098200121 Oct 26  2022 ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc
.....
```

```bash
ftp> get  ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc
```

local: ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc remote: ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc
227 Entering Passive Mode (192,171,164,182,165,90).
150 Opening BINARY mode data connection for ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc (1098200121 bytes).

```bash
ftp> by
```

221 Goodbye.



```
/work03/am/2022.BGC_ARGO_SO/10.10.SELECT_FILE/00PLOT
2023-07-05_09-58
$ python3 PLOT_FLOAT_LOC_OCEAN_CLR.py
<xarray.Dataset>
Dimensions:             (time: 1, lat: 4320, lon: 8640)
Coordinates:
  * lat                 (lat) float64 89.98 89.94 89.9 ... -89.9 -89.94 -89.98
  * lon                 (lon) float64 -180.0 -179.9 -179.9 ... 179.9 179.9 180.0
  * time                (time) datetime64[ns] 1998-01-01
Data variables: (12/21)
    Rrs_412             (time, lat, lon) float32 ...
    ...                  ...
    chlor_a             (time, lat, lon) float32 ...
```

```python
plt.figure(dpi=100)
ax=plt.axes(projection=ccrs.Mercator())
ax.add_feature(cfeature.LAND) 
# fill in the land areas
ax.coastlines() 
# use the default low-resolution coastline
gl=ax.gridlines(draw_labels=True) 
# default is to label all axes.
gl.xlabels_top=False 
# turn off two of them.
gl.ylabels_right=False
CHL.plot(x='Longitude',y='Latitude',cmap='gist_ncar',vmin=CHL.min(),vmax=CHL.max(),transform=ccrs.PlateCarree())

```


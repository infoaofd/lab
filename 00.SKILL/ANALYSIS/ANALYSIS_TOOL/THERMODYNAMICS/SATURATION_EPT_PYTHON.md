# Saturation Equivalent potential temperature (飽和相当温位)

## データ

標準大気の気温プロファイル

```python
"""
1976 Standard Atmosphere Calculator
https://www.digitaldutch.com/atmoscalc/tableatmosphere.htm

"""
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
    
T=np.array([288.150,
281.650,
275.150,
268.650,
262.150,
255.650,
249.150,
242.650,
236.150,
229.650,
223.150,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
217.650,
218.650,
219.650,
220.650,
221.650,
222.650,
223.650,
224.650,
225.650,
226.650,
])
ppa=np.array([101325,
89874.6,
79495.2,
70108.5,
61640.2,
54019.9,
47181.0,
41060.7,
35599.8,
30742.5,
26436.3,
22632.1,
19330.4,
16510.4,
14101.8,
12044.6,
10287.5,
8786.68,
7504.84,
6410.01,
5474.89,
4677.89,
3999.79,
3422.43,
2930.49,
2511.02,
2153.09,
1847.46,
1586.29,
1362.96,
1171.87,
])
p=ppa/100
fig, ax = plt.subplots()
ax.invert_yaxis()
plt.plot(T, p)
plt.xlabel('T [K]')
plt.ylabel('P [hPa]')
plt.yscale('log')
ax.set_yticks([10, 50, 100, 300,500, 1000])
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.show()
```

## 飽和相当温位のプロファイル

```python
"""
1976 Standard Atmosphere
https://www.digitaldutch.com/atmoscalc/tableatmosphere.htm
https://sites.google.com/site/afcanalysis/home/formulae/saturated-ept
"""
import matplotlib
from matplotlib import pyplot as plt
import numpy as np

def SAT_EPT(t,p):
    """
    t = temperature.to('kelvin').magnitude
    p = pressure.to('hPa').magnitude
    e = saturation_vapor_pressure(temperature).to('hPa').magnitude
    r = saturation_mixing_ratio(pressure, temperature).magnitude
    https://sites.google.com/site/afcanalysis/home/formulae/saturated-ept
    """
    Tc=t-273.15
    es=6.112*np.exp((17.67*Tc)/(Tc + 243.5))
    rs=0.622*es/(p-es)
    th_l = t * (1000 / p )** (0.2854*(1.0 - 0.28*rs))
    print(Tc)
    print(es)
    print(rs)
    return th_l * np.exp( (3.376 / t - 0.00254) * rs * 1000.0 *(1 + 0.81 * rs) )

T=np.array([
288.150,
281.650,
275.150,
268.650,
262.150,
255.650,
249.150,
242.650,
236.150,
229.650,
223.150,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
])
ppa=np.array(
[101325,
89874.6,
79495.2,
70108.5,
61640.2,
54019.9,
47181.0,
41060.7,
35599.8,
30742.5,
26436.3,
22632.1,
19330.4,
16510.4,
14101.8,
12044.6,
10287.5,
8786.68,
])
p=ppa/100.
sept=SAT_EPT(T,p)
fig, ax = plt.subplots()
plt.xlabel('SAT EPT [K]')
plt.ylabel('P [hPa]')
ax.invert_yaxis()
plt.plot(sept, p)
plt.yscale('log')
ax.set_yticks([100, 300, 500, 700, 1000])
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.show()
```
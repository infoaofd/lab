EQUIVALENT POTENTIAL TEMPERATURE
====================================
Formula
--------------------------
### Bolton (1980)
A number of approximate formulations are used for calculating equivalent potential temperature, $\theta_e$, since it is not easy to compute integrations along motion of the parcel.  Bolton (1980; B80) gives review of such procedures with estimates of error.  For most purposes, Eq. (38) is probably to be preferred, since it makes use of $\theta$. As a basis for very accurate work, however, Eq. (39) with $\theta_{DL}$ given Eq. (24) is suggested. 

#### Most accurate formula

His best approximation formula is used when accuracy is needed (Eqs. 15, 21, 22, 24, 39 of B80):

$$
\begin{eqnarray}
e_S(T_C) = 6.112 \exp \left(\frac{17.67T_C}{T_C+243.5} \right)
\tag{10 of B80}
\end{eqnarray}
$$

$$
\begin{eqnarray}
e & = & U/100 \times e_{S} \\
r & = & \frac{\epsilon e}{p-e}
\end{eqnarray}
$$

$$
\begin{eqnarray}
T_K & = & T_C + 273.15
\end{eqnarray}
$$

$$
\begin{eqnarray}
T_L & = &\frac{1}{ \frac{1}{T_K - 55} + \frac{\ln(U/100)}{2840} } + 55 \hspace{10pt}\mathrm{or}, \tag{22 of B80} \\
& = & \frac{2840}{ 3.5 \ln{T_K} + \ln{e} - 4.805 } + 55
\hspace{10pt}\mathrm{or}, \tag{21 of B80} \\
& = & \frac{1}{ \frac{1}{T_d - 56} + \frac{\log_e(T_K/T_d)}{800} } + 56 \tag{15 of B80}
\end{eqnarray}
$$
$$
\begin{eqnarray}
\theta_{DL} = T_K \left(\frac{p_0}{p - e}\right)^{0.2854} \left(\frac{T_K}{T_L}\right)^{0.28 \times 10^{-3}r}
\tag{24 of B80} \\
\theta_e = \theta_{DL} \exp \left[ \left( \frac{3.036}{T_L} - 0.00178 \right) \times r \left(1 + 0.448 \times 10^{-3} r\right)\right] \tag{39 of B80}
\end{eqnarray}
$$


Where:
* $T_C$ is temperature [C] of air at pressure $p$,
* $e_S$ is the saturation water vapor pressure [**hPa**] at $T_C$ [C],
* $U$ is relative humidity [**%**],
* $e$ is the water vapor pressure (to obtain $\theta_{L}$ for dry air),
* $p$ is pressure at the point [**hPa**],
* $\epsilon  \equiv  R_d / R_v $ = 287.04 [J/kg/K] / 481.50 [J/kg/K] = **0.6220**,
* $r$ is mixing ratio of water vapor mass per mass [**g/kg**],
* $T_K$ is temperature [K] of air at pressure $p$,
* $T_d$ is dew point temperature at pressure $p$,
* $T_L$ is (approximated) temperature [K] at LCL,
* $\theta_{DL}$ is dry potential temperature [K] at the lifted condensation level (LCL),
* $p_0$ is standard reference pressure (1000 hPa),
* $\kappa_d = R_d / c_{pd}$ is the ratio of the specific gas constant to the specific heat of dry air at constant pressure (**0.2854**).


Maximum error of Eq. (39) is <u>0.02</u> [K]. (p. 1049 of Bolton).


#### Practical  formula with high accuracy
$$
\begin{eqnarray}
\theta_e = \theta \exp \left[ \left( \frac{3.376}{T_L} - 0.00254 \right) r \left(1 + 0.81 \times 10^{-3}r \right)\right] \tag{38 of B80}\\
\theta = T_K \left( \frac{p_0}{p} \right)^{0.2854 \left(1-0.28 \times 10^{-3} r\right)}
\tag{7 of B80}
\end{eqnarray}
$$
$$
\begin{eqnarray}
T_L & = &\frac{1}{ \frac{1}{T_d - 56} + \frac{\log_e(T_K/T_d)}{800} } + 56
\hspace{10pt}\mathrm{or}, \tag{15 of B80}\\
 & = & \frac{2840}{ 3.5 \ln{T_K} + \ln{e} - 4.805 } + 55
\hspace{10pt}\mathrm{or}, \tag{22 of B80}\\
 & = &\frac{1}{ \frac{1}{T_K - 55} + \frac{\ln(U/100)}{2840} } + 55 \tag{22 of B80}
\\
\end{eqnarray}
$$
Where:
* $T_C$ is temperature [C] of air at pressure $p$,
* $e_S$ is the saturation water vapor pressure [**hPa**],
* $U$ is relative humidity [**%**],
* $e$ is the water vapor pressure (to obtain $\theta_{L}$ for dry air),
* $p$ is pressure at the point [**hPa**],
* $\epsilon  \equiv  R_d / R_v $ = 287.04 [J/kg/K] / 481.50 [J/kg/K] = **0.6220**,
* $r$ is mixing ratio of water vapor mass per mass [**g/kg**],
* $T_K$ is temperature [K] of air at pressure $p$,
* $T_d$ is dew point temperature at pressure $p$,
* $T_L$ is (approximated) temperature [K] at LCL,
* $\theta_{DL}$ is dry potential temperature [K] at the lifted condensation level (LCL),
* $p_0$ is standard reference pressure (1000 hPa),
* $\kappa_d = R_d / c_{pd}$ is the ratio of the specific gas constant to the specific heat of dry air at constant pressure (**0.2854**).

Maximum error of Eq. (39) is <u>0.05</u> [K]. (p. 1049 of Bolton).

## Computer codes
https://sites.google.com/site/afcanalysis/home/formulae/temperatures

### GrADS
#### GRADS EPT USING TEMP, RH, P

```bash
function BOLTON ( temp, rh, lev )
# temp [K], rh [%], lev [hPa]
'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'     ;# [hPa]
# Eq.10 of Bolton (1980)
'e=0.01*rh*es'                             ;# [hPa]
# Eq.4.1.5 (p. 108) of Emanuel (1994)
'TDL=temp*pow(1000/(lev-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'
#Eq.24 of Bolton
'ept=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))' ;# [K]
# Eq.39 of Bolton
return ept
```

#### GRADS EPT USING TEMP, QV, P

```
say 'MMMMM EPT (Bolton 1980) USING TEMP, QV, P'
# tk [K], QVAPOR [kg/kg], P [hPa]

'temp=tk.1';'mixr=QVAPOR.1';'P=pressure.1'
'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'          ;# Eq.10 of Bolton (1980)
#'e=0.01*rh*es'                                 ;# Eq.4.1.5 (p. 108) of Emanuel (1994)
'e=(mixr*P*100)/(0.622+mixr)/100' ;#[hPa]Emanuel 
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))'  ;# Inverting Eq.10 of Bolton since es(Td)=e
'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56'  ;#Eq.15 of Bolton (1980)
'mixr= 0.62197*(e/(P-e))*1000'                 ;# Eq.4.1.2 (p.108) of Emanuel(1994) 
'TDL=temp*pow(1000/(P-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'
'EPT1=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))' ;#Eq.39 of Bolton
```

**SATURATION EPT** (43 of B80 with **rh=100%** and **Tlcl=Tk**)

```bash
function SAT_EPT(tk, lev)
'tc=tk-273.15'
'es=6.112*exp((17.67*tc)/(tc+243.5))'  ;#hPa
'rs=(0.622 * es/ (lev - es))*1.E3'       ;#g/kg
'POWER=0.2854*(1.0 - 0.28*0.001*rs)'
'pt=tk*pow((1000./lev),POWER)'
### 'define tl= 1/(1/(tk-5t)+log(RH/100)/2840)+55'
'arg21=3.376/tk - 0.00254' ;# USE tk INSTEAD OF tl
'arg22 = rs*(1.0 + 0.81 * 1.0E-3*rs)'
'sept=pt*exp(arg21 * arg22)'
return sept
```

### FORTRAN

#### Fortran USING TC, P, RH

```FORTRAN
!INPUT: tc [degC]; p [hPa]; rh [%]
!OUTPUT: ept [K]
tc=tk-273.15
es= 6.112*exp((17.67*tc)/(tc+243.5))        ! Eq.10 of Bolton (1980)
e=0.01*rh*es                                ! Eq.4.1.5 (p. 108) of Emanuel (1994)
td=(243.5*log(e/6.112))/(17.67-log(e/6.112))! Inverting Eq.10 of Bolton since es(Td)=e
dwpk= td+273.15
Tlcl= 1/(1/(dwpk-56)+log(tk/dwpk)/800)+56   ! Eq.15 of Bolton (1980)
mixr= 0.62197*(e/(p-e))*1000                ! Eq.4.1.2 (p.108) of Emanuel(1994) 
TDL=tk*((1000/(p-e))**0.2854)*(tk/Tlcl)**(0.28*0.001*mixr) ! Eq.24 of Bolton
ept=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr)) ! Eq.39 of Bolton
```

#### FORTRAN USING TEMP, QV, QVAPOR

/work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.24.RW3A.TRAJ.BACK.10MIN.02

```FORTRAN
print '(A)', 'MMMMM EPT (Bolton 1980) USING TEMP, QV, P'
REAL temp,mixr,P
REAL Tlcl, tc, es, e, td, dwpk, TDL, EPT

temp=tk !(i,j,k)
mixr=q !(i,j,k)
P=pres !(i,j,k)

tc=(temp-273.15)
es= 6.112*exp((17.67*tc)/(tc+243.5))         ! Eq.10 of Bolton (1980)
e=(mixr*P*100)/(0.622+mixr)/100              ! [hPa] Emanuel 
td=(243.5*log(e/6.112))/(17.67-log(e/6.112)) ! Inverting Eq.10 of Bolton since es(Td)=e
dwpk= td+273.15
Tlcl= 1.0/(1.0/(dwpk-56)+log(temp/dwpk)/800)+56.0 ! Eq.15 of Bolton (1980)
mixr= 0.62197*(e/(P-e))*1000.0                    ! Eq.4.1.2 (p.108) of Emanuel(1994) 
TDL=temp*(1000/(P-e))**(0.2854)*(temp/Tlcl)**(0.28*0.001*mixr)
EPT=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr)) ! Eq.39 of Bolton
```



### Fortran (OLDER VERSION)

```Fortran
program test_ept_sept

implicit none
real ept,sept,es,e,r,tk,p,RH
real tc
integer::debug=0

namelist /para/tk,p,RH

read(*,nml=para)

tc=tk-273.15

print *
print '(A,3f7.1)','tk, p, RH = ',tk,p,RH

print '(A)','     tc      p     RH     es      e      r    ept   sept'

call ept_sept(ept, sept, es, e, r, tk, p, RH, debug)
print '(8f7.1)',tc,p,RH,es,e,r,ept,sept

end program test_ept_sept


subroutine ept_sept(ept, sept, es, e, r, tk, p, RH, debug)
!
! EPT
! Bolton, D., 1980: The computation of equivalent potential
!  Temperature, Monthly Weather Review, 108, 1046-1053.
!  url: http://www.rsmas.miami.edu/users/pzuidema/Bolton.pdf
!
! SATURATED EPT
!  https://unidata.github.io/MetPy/latest/api/generated/
!metpy.calc.saturation_equivalent_potential_temperature.html

implicit none
real,intent(out)::ept,sept,es,e,r

! ept: equivalent potential temperature (kelvin)
!sept: saturated equivalent potential temperature (kelvin)
!  es: saturated water vapor pressure (hPa)
!   e: water vapor pressure (hPa)
!   r: water-vapor mixing ratio (g/kg)

real,intent(in)::tk,p,RH

! tk: absolute temperature (kelvin)
! p : pressure (hPa)
! RH : relative humidity (%)
integer,intent(in)::debug

real u,tc
! u : relative humidity (%)
! tc: temperature (degree Celsius)

real,parameter::p0=1000.0
! p0: pressue (hPa)

real,parameter::L=2.5*1.e6
real,parameter::Rv=461.0
! L : latent heat
! Rv: gas constant
real tl
! tl: Absolute temperature at lifting condensation level (LCL)
!     (kelvin)
real pt
! pt : potential temparature

real pow,arg11,arg12,exp1, denom
real A
real numer, C
real rs
!rs : saturated mixing ratio (g/kg)
real,parameter::kappa=0.2854
real arg21,arg22

u=RH
denom=1.0/(tk - 55.0) - log(u/100.0)/2840.0

tl = 1.0/denom + 55.0
! CHECK VALUES FOR tl
! 1/(1/(280-55))+55=280 (RH=100%)
! 1/(1/(280-55)-(log(50/100)/2840))+55=274.758 (RH=50%)

tc=tk-273.15

! Eq.(10) of B80 (p.1047) 
es=6.112*exp((17.67*tc)/(tc+243.5)) !hPa
e=RH/100.0 * es                     !hPa
r = (0.622 * e/ (p - e))*1.E3       !g/kg

! Eq.(43) of B80. (p.1052)    
pow=0.2854*(1.0 - 0.28*0.001*r)

pt=tk*(p0/p)**pow

arg11 = 3.376/tl - 0.00254
arg12 = r*(1.0 + 0.81 * 1.0E-3*r)
exp1=exp( arg11 * arg12 )
ept=pt * exp1


! SATURATED EPT
!  https://unidata.github.io/MetPy/latest/api/generated/
!metpy.calc.saturation_equivalent_potential_temperature.html

!Eq.(39) of B80 (p.1052)
rs=(0.622 * es/ (p - es))*1.E3       !g/kg
pow=0.2854*(1.0 - 0.28*0.001*rs)
pt=tk*(p0/p)**pow

arg21=3.376/tk - 0.00254
arg22 = rs*(1.0 + 0.81 * 1.0E-3*rs)
sept=pt*exp(arg21 * arg22)

if(debug/=0)then
print *
print '(A)','CHECK:'
print '(A,f10.2)','tc   = ',tc
print '(A,f10.2)','es   = ',es
print '(A,f10.2)','e    = ',e
print '(A,f10.2)','tl   = ',tl
print '(A,f10.2)','rs   = ',rs
print '(A,f10.2)','r    = ',r
print '(A,f10.2)','pt   = ',pt
end if

! 
! Eq. (6.3) of Davies-Jones (MWR, 2009)
!  numer = (2.771*1.E6 - 1109.0*(tl - 273.15))*r*1.E-3
!  denom = 1005.7*tl
!  ept=pt*exp(numer/denom)
! Eq. (2.5) of Davies-Jones (MWR, 2009)
! ept = pt*exp((2.690*1.E6 * 1.0E-3 * r)/(1005.7*tl) )

end subroutine ept_sept
```
### PYTHON

```Python
"""
1976 Standard Atmosphere
https://www.digitaldutch.com/atmoscalc/tableatmosphere.htm
https://sites.google.com/site/afcanalysis/home/formulae/saturated-ept
"""
import matplotlib
from matplotlib import pyplot as plt
import numpy as np

def SAT_EPT(t,p):
    """
    t = temperature.to('kelvin').magnitude
    p = pressure.to('hPa').magnitude
    e = saturation_vapor_pressure(temperature).to('hPa').magnitude
    r = saturation_mixing_ratio(pressure, temperature).magnitude
    https://sites.google.com/site/afcanalysis/home/formulae/saturated-ept
    """
    Tc=t-273.15
    es=6.112*np.exp((17.67*Tc)/(Tc + 243.5))
    rs=0.622*es/(p-es)*1000.
    th_l = t * (1000 / p )** (0.2854*(1.0 - 0.28*rs))
    print(Tc)
    print(es)
    print(rs)
    return th_l * np.exp( (3.376 /t-0.00254)*rs*(1 +0.81*rs) )

def EPT(t,p,RH):
    """
    t = temperature.to('kelvin').magnitude
    p = pressure.to('hPa').magnitude
    e = saturation_vapor_pressure(temperature).to('hPa').magnitude
    r = saturation_mixing_ratio(pressure, temperature).magnitude
    https://sites.google.com/site/afcanalysis/home/formulae/saturated-ept
    """
    Tc=t-273.15
    es=6.112*np.exp((17.67*Tc)/(Tc + 243.5))
    rs=0.622*es/(p-es)*1000.
    
    e=RH/100.0 * es
    r = (0.622*e/(p-e))*1000.

    th_l = t * (1000 / p )** (0.2854*(1.0 - 0.28*r))   

    denom=1.0/(t - 55.0) - np.log(RH/100.0)/2840.0
    tlcl = 1.0/denom + 55.0

    return th_l * np.exp( (3.376 / tlcl - 0.00254)*r*(1 + 0.81*r))
	
T=np.array([
288.150,
281.650,
275.150,
268.650,
262.150,
255.650,
249.150,
242.650,
236.150,
229.650,
223.150,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
216.650,
])
ppa=np.array(
[101325,
89874.6,
79495.2,
70108.5,
61640.2,
54019.9,
47181.0,
41060.7,
35599.8,
30742.5,
26436.3,
22632.1,
19330.4,
16510.4,
14101.8,
12044.6,
10287.5,
8786.68,
])
p=ppa/100.
sept=SAT_EPT(T,p)
fig, ax = plt.subplots()
plt.xlabel('SAT EPT [K]')
plt.ylabel('P [hPa]')
ax.invert_yaxis()
plt.plot(sept, p)
plt.yscale('log')
ax.set_yticks([100, 300, 500, 700, 1000])
ax.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
plt.show()
```



### Holton (1972)

A little more theoretical formula is commonly used in literature like Holton (1972)  when theoretical explanation is important:

$$
\theta_e \approx \theta_L\exp\left[\frac{ r_s(T_L) L_v(T_L) }{ c_{pd} T_L }\right]
$$

Where:
* $r_s(T_L)$ is saturated mixing ratio of water at temperature $T_L$, the temperature at the saturation level of the air,
* $L_v(T_L)$ is [[latent heat]] of evaporation at temperature $T_L$ (2406 kJ/kg {at 40&nbsp;°C} to 2501 kJ/kg {at 0&nbsp;°C}), and
* $c_{pd}$ is specific heat of dry air at constant pressure (1005.7 J/(kg·K)).

### Stull (1988)
Further more simplified formula is used (in, for example, Stull (1988)  for simplicity, if it is desirable to avoid computing $T_L$:

$$
\theta_e = T_e \left( \frac{p_0}{p} \right)^{\kappa_d} \approx \left( T_K + \frac {L_v}{c_{pd}} r \right) \left( \frac{p_0}{p} \right)^\frac{R_d}{c_{pd}}
$$

Where:
* $T_e$ = equivalent temperature
* $R_d$ = specific gas constant for air (287.04 J/(kg·K))



Usage
------------------------
### Synoptic scale meteorology for characterization of air masses
For instance, in a study of the North American Ice Storm of 1998, it was demonstrated that the air masses involved originated from high Arctic at an altitude of 300 to 400&nbsp;hPa the previous week, went down toward the surface as they moved to the Tropics, then moved back up along the Mississippi Valley toward the St. Lawrence Valley. The back trajectories were evaluated using the constant equivalent potential temperatures.
http://journals.ametsoc.org/doi/pdf/10.1175/1520-0493%282001%29129%3C2983%3ATISAOA%3E2.0.CO%3B2

### Mesoscale meteorology
Equivalent potential temperature is also a useful measure of the static stability of the unsaturated atmosphere. Under normal, stably stratified conditions, the potential temperature increases with height,

$\frac{\partial \theta_e}{\partial z} > 0$

and vertical motions are suppressed. If the equivalent potential temperature decreases with height,

$\frac{\partial \theta_e}{\partial z} < 0$

the atmosphere is unstable to vertical motions, and [[Atmospheric convection|convection]] is likely. Situations in which the equivalent potential temperature decreases with height, indicating instability in saturated air, are quite common.

References
-----------------------------------------
Bolton, D., 1980: The Computation of Equivalent Potential Temperature.  *Mon. Wea. Rev.*, **108**, 1046-1053.
Holton, J. R., 1972: An Introduction to Dynamical Meteorology''.  Academic Press, 319 pages.
Stull, R. B., 1988: An Introduction to Boundary Layer Meteorology'', Kluwer, 666 pages,  p.546
# FLEXPART_OPEN_MPI

[[_TOC_]]

## 注意事項 **Intel OneAPIとの共存はできない**

**Intel OneAPIの環境は使用しない** (下記のコマンドは**使わない**)

```bash
source /opt/intel/oneapi/setvars.sh
```

**condaも終了させておく**
condaにmpiが入っている場合があるようである



## プログラム変更箇所

```
/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG/flexpart_v10.4_MB0.1/src14.12mpi
$ org.sh partoutput_mpi.f90 
'partoutput_mpi.f90' -> 'partoutput_mpi_ORG_230625-2015.f90'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 6.8K Jun 25 20:14 partoutput_mpi.f90
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 6.8K Jun 25 20:14 partoutput_mpi_ORG_230625-2015.f90
ccccccccccccccccccccccccccccccccccccccccccccccc
```

```fortran
/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG/flexpart_v10.4_MB0.1/src14.12mpi
$ diff partoutput_mpi_ORG_230625-2015.f90 partoutput_mpi.f90 

ファイル開く箇所にappendを付ける

38a39
>   integer idummy
60a62
> 
74c76
<   if (lroot) write(unitpartout)write(unitpartout,*) itime ! MPI root process only
---
>   if (lroot) write(unitpartout,*) itime ! MPI root process only
194,196c196,198
<       write(unitpartout) npoint(i),xlon,ylat,ztra1(i), &
<            itramem(i),topo,pvi,qvi,rhoi,hmixi,tri,tti, &
<            (xmass1(i,j),j=1,nspec)
---
>       write(unitpartout,101) npoint(i),i,idummy,xlon,ylat,ztra1(i),&
>       topo,pvi,qvi,rhoi,hmixi,tri,tti, & !ppi,
>       (xmass1(i,j),j=1,nspec)
204,206c206,208
<     write(unitpartout) -99999,-9999.9,-9999.9,-9999.9,-99999, &
<          -9999.9,-9999.9,-9999.9,-9999.9,-9999.9,-9999.9,-9999.9, &
<          (-9999.9,j=1,nspec)
---
>   write(unitpartout,101) -99999,-99999,-9999,-9999.9,-9999.9, &
>   -9999.9,-9999.9,-9999.9,-9999.9,-9999.9,-9999.9,-9999.9, &
>   -9999.9, (-9999.9,j=1,nspec)
210a213,214
> 
> 101   format( 2i8,1p,i15, 1p, 22e14.6 )
```



## コンパイル

```
/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG/flexpart_v10.4_MB0.1/src14.12mpi
$ make mpi
```



## 実行

```
/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG
$ cd FLEXPART_MPI_RUN/
```

```
/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG/FLEXPART_MPI_RUN/ECS202206/E0619_T02.00
$ ll FLEXPART_MPI
lrwxrwxrwx. 1 am 54 2023-06-25 20:11 FLEXPART_MPI -> ../../../flexpart_v10.4_MB0.1/src14.12mpi/FLEXPART_MPI*
```

```
$ mpirun -np 16 FLEXPART_MPI
```

粒子数1万個でテストした場合，並列化してもあまり体感で早くなった感じはしない

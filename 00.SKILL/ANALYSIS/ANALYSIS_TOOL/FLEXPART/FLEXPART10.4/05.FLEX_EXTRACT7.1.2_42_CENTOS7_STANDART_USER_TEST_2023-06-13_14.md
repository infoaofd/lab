# 05.FLEX_EXTRACT7.1.2_42_CENTOS7_STANDART_USER_TEST_2023-06-13_14

```bash
$ cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
```

https://www.flexpart.eu/wiki/FpInputMetEcmwf

```bash
/work09/am
$ mkd flex_extract_7.1.2
/work09/am
$ cd flex_extract_7.1.2
```

```bash
/work09/am/flex_extract_7.1.2
$ git clone --single-branch --branch master https://www.flexpart.eu/gitmob/flex_extract
```

https://www.flexpart.eu/flex_extract/installation.html#ref-requirements

```bash
2023-06-13_14-46
/usr/local
$ rpm -q libeccodes-dev
パッケージ libeccodes-dev はインストールされていません。
```



https://www.flexpart.eu/flex_extract/Installation/local.html

```
2023-06-13_14-48
/usr/local
$ python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import eccodes
>>> import genshi
>>> import numpy
>>> import cdsapi
>>> import ecmwfapi
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ModuleNotFoundError: No module named 'ecmwfapi'
```

```
/usr/local
$ sudo pip3 install ecmwf-api-client
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Requirement already satisfied: ecmwf-api-client in /root/.local/lib/python3.6/site-packages
```

```
/work09/am
$ pip3 list
DEPRECATION: The default format will switch to columns in the future. You can use --format=(legacy|columns) (or define a format=(legacy|columns) in your pip.conf under the [list] section) to disable this warning.
attrs (22.2.0)
cdsapi (0.5.1)
certifi (2022.12.7)
cffi (1.15.1)
charset-normalizer (2.0.12)
eccodes (1.5.2)
findlibs (0.0.5)
Genshi (0.7.7)
idna (3.4)
importlib-resources (5.4.0)
numpy (1.12.1)
pip (9.0.3)
pycparser (2.21)
requests (2.27.1)
setuptools (39.2.0)
six (1.16.0)
tqdm (4.64.1)
urllib3 (1.26.13)
zipp (3.6.0)
```





```
/work09/am/flex_extract_7.1.2/flex_extract
$ cd Source/Fortran/
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Source/Fortran
$ ll makefile_local_gfortran 
lrwxrwxrwx. 1 am 13 2023-06-13 14:52 makefile_local_gfortran -> makefile_fast
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Source/Fortran
$ org.sh makefile_fast
'makefile_fast' -> 'makefile_fast_ORG_230613-1504'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 1.7K Jun 13 14:52 makefile_fast
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 1.7K Jun 13 14:52 makefile_fast_ORG_230613-1504
ccccccccccccccccccccccccccccccccccccccccccccccc
```



```
/work09/am
$ ls /usr/local/eccodes/
bin/  include/  lib/  share/
```

```
$ ls /usr/local/eccodes/lib
libeccodes.so*  libeccodes_f90.so*  pkgconfig/
(base) 
```

```
$ ls /usr/local/emoslib-4.5.9/lib
libemos.R32.D64.I32.a@  libemos.a     pkgconfig/
libemos.R64.D64.I32.a@  libemosR64.a
```

```
$ vi makefile_local_gfortran 
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Source/Fortran
$ diff makefile_fast_ORG_230613-1504 makefile_fast 
17,18c17,18
< ECCODES_LIB =  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm 
< EMOSLIB=-lemosR64
---
> ECCODES_LIB =-L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm 
> EMOSLIB=-L/usr/local/emoslib-4.5.9/lib -lemosR64
21c21
< ECCODES_INCLUDE_DIR=/usr/lib/x86_64-linux-gnu/fortran/gfortran-mod-15
---
> ECCODES_INCLUDE_DIR=/usr/local/eccodes/include
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Source/Fortran
$ cd ../..
```

```
/work09/am/flex_extract_7.1.2/flex_extract
$ setup_local.sh 
```

```
/work09/am/flex_extract_7.1.2/flex_extract
$ setup_local.sh 
WARNING: installdir has not been specified
flex_extract will be installed in here by compiling the Fortran source in /work09/am/flex_extract_7.1.2/flex_extract/Source/Fortran
Install flex_extract_v7.1.2 software at local in directory /work09/am/flex_extract_7.1.2/flex_extract

Using makefile: makefile_local_gfortran
/work09/am/anaconda3/lib/python3.9/subprocess.py:935: RuntimeWarning: line buffering (buffering=1) isn't supported in binary mode, the default buffer size will be used
  self.stdin = io.open(p2cwrite, 'wb', bufsize)
/work09/am/anaconda3/lib/python3.9/subprocess.py:941: RuntimeWarning: line buffering (buffering=1) isn't supported in binary mode, the default buffer size will be used
  self.stdout = io.open(c2pread, 'rb', bufsize)
/work09/am/anaconda3/lib/python3.9/subprocess.py:946: RuntimeWarning: line buffering (buffering=1) isn't supported in binary mode, the default buffer size will be used
  self.stderr = io.open(errread, 'rb', bufsize)
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./rwgrib2.f90
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./phgrreal.f90
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./grphreal.f90
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./ftrafo.f90
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./calc_etadot.f90
gfortran   -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c      ./posnam.f90
gfortran  rwgrib2.o calc_etadot.o ftrafo.o grphreal.o posnam.o phgrreal.o -o calc_etadot_fast.out  -O3  -L/usr/local/eccodes/lib  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -L/usr/local/emoslib-4.5.9/lib -lemosR64 -fopenmp
ln -sf calc_etadot_fast.out calc_etadot

lrwxrwxrwx. 1 am oc 20  6月 13 15:09 ./calc_etadot -> calc_etadot_fast.out
```



```
/work09/am/flex_extract_7.1.2/flex_extract/Run/Control
$ cp CONTROL_EA5 CONTROL_EA5_20120101
`CONTROL_EA5' -> `CONTROL_EA5_20120101'
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Run/Control
$ vi CONTROL_EA5_20120101    
```

```
/work09/am/flex_extract_7.1.2/flex_extract/Run/Control
$ diff CONTROL_EA5 CONTROL_EA5_20120101 
1c1
< START_DATE 20180809
---
> START_DATE 20120101
```



```
/work09/am/flex_extract_7.1.2/flex_extract/Source/Python
$ submit.py --controlfile=CONTROL_EA5_20120101 --start_date=201
20101 --public=1
```

```bash
Retrieving ECMWF data!
start date 20120101 
end date 20120101 
Using ECMWF WebAPI: False
Using CDS API: True
... removing old files in /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace
... retrieve 20111231/to/20120102 in dir /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace
marsclass: EA
dataset: None
type: FC
levtype: SFC
levelist: 1
repres: 
date: 20111231/to/20120102
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 06/18
step: 1/to/12/by/1
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.253941.grb
param: 142.128/143.128/146.128/180.128/181.128/176.128
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.253941.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 15:33:49,452 INFO Welcome to the CDS
2023-06-13 15:33:49,452 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-06-13 15:33:49,852 INFO Request is completed
2023-06-13 15:33:49,853 INFO Downloading https://download-0019.copernicus-climate.eu/cache-compute-0019/cache/data0/adaptor.mars.internal-1686561744.0187004-32683-2-8e47d1b2-0e7b-42df-8fe8-c6f3a4dff1b2.grib to /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.253941.grb (75.8M)
2023-06-13 15:34:11,397 INFO Download rate 3.5M/s              
MARS retrieve done ... 
... retrieve 20120101/to/20120101 in dir /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace
marsclass: EA
dataset: None
type: AN
levtype: SFC
levelist: 1
repres: 
date: 20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00
step: 000
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.253941.grb
param: 160.128/027.128/028.128/244.128
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.253941.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 15:34:11,803 INFO Welcome to the CDS
2023-06-13 15:34:11,803 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-06-13 15:34:12,129 INFO Downloading https://download-0017.copernicus-climate.eu/cache-compute-0017/cache/data1/adaptor.mars.internal-1686561817.5392962-12660-14-31d3ae24-fd69-4b88-83ab-8b9965b12cdd.grib to /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.253941.grb (6.3M)
2023-06-13 15:34:15,485 INFO Download rate 1.9M/s              
marsclass: EA
dataset: None
type: AN
levtype: ML
levelist: 1/to/137
repres: 
date: 20120101/to/20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00/01/02/03/04/05/06/07/08/09/10/11/12/13/14/15/16/17/18/19/20/21/22/23
step: 00
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.253941.grb
param: 130.128/133.128/131.128/132.128/077.128/246.128/247.128
target: /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.253941.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 15:34:15,891 INFO Welcome to the CDS
2023-06-13 15:34:15,891 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-complete
2023-06-13 15:34:16,211 INFO Request is queued
2023-06-13 15:34:17,411 INFO Request is running
2023-06-13 16:12:52,461 INFO Request is completed
2023-06-13 16:12:52,461 INFO Downloading https://download-0002-clone.copernicus-climate.eu/cache-compute-0002/cache/data8/adaptor.mars.external-1686638056.4302063-26889-1-b8bb556d-8db9-4e63-a359-b47a4e2f387b.grib to /work09/am/flex_extract_7.1.2/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.253941.grb (3.8G)
```


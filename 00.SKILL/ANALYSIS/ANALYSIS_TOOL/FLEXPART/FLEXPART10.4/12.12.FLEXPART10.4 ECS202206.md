# 12.12.FLEXPART10.4 ECS202206.md

- $ cat /etc/redhat-release
  CentOS Linux release 7.9.2009 (Core)
- FLEXPART10.4

- FLEX_EXTRACT_7.1.3
- $ gfortran --version
  GNU Fortran (GCC) 4.8.5 20150623 (Red Hat 4.8.5-44)

[[_TOC_]]

## FLEX_EXTRACT

```
2023-06-14_11-52
/work09/am/flex_extract/Run/Control
$ cat CONTROL_EA5_202206 
START_DATE 20220618
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT 119.
LOWER 10.
UPPER 35.
RIGHT 132.
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
ECTRANS 1
```

```bash
2023-06-14_11-57
/work09/am/flex_extract/Source/Python
$ submit.py --start_date 20220618 --end_date 20220620 --controlfile CONTROL_EA5_202206 --public=1
```

最後の`--public=1`を忘れないようにする



メモリ不正アクセスのエラー

```bash

Program received signal SIGSEGV: Segmentation fault - invalid memory reference.

Backtrace for this error:
#0  0x7FA6E04346D7
#1  0x7FA6E0434D1E
#2  0x7FA6DF4ED3FF
#3  0x7FA6DF605630
#4  0x7FA6E0515D3A
#5  0x403725 in __rwgrib2_MOD_readlatlon
#6  0x405134 in MAIN__ at calc_etadot.f90:?
... ERROR CODE: -11
... ERROR MESSAGE:
         Command '['/work09/am/flex_extract/Source/Fortran/calc_etadot']' died with <Signals.SIGSEGV: 11>.
... FORTRAN PROGRAM FAILED!
```



### Running preprocessing with previously loaded grib files

 https://www.flexpart.eu/ticket/298

```
FCOG_acc_SL.20220617.262134.287880.grb 


python3 {fe_path_to_source}/prepare_flexpart.py --controlfile={name_of_CONTROLfile} --inputdir=. --outputdir=. --ppid={second_number_in_downloaded_datafiles}
```

```
$ cdda
```

```bash
/work09/am/flex_extract/Run/Workspace
$ python3 /work09/am/flex_extract/Source/Python/Mods/prepare_flexpart.py --controlfile=CONTROL_EA5_202206 --inputdir=. --outputdir=. --ppid=262134
```

**=>Segmentation fault**



```
2023-06-14_14-10
/work09/am/flex_extract/Source/Python
$ ./submit.py --start_date 20220619 --controlfile CONTROL_EA5_202206 --public=1
```

**=> Segmentation fault**



### LEFT, RIGHT, UPPER, LOWERの値を調節する

東西幅 (RIGHT - LEFT)，南北幅 (UPPER - LOWER)が格子幅 (GRID)で**割り切れる**ようにする

```
/work09/am/flex_extract/Source/Python
$ cat ../../Run/Control/CONTROL_EA5_20220619_TEST 
START_DATE 20220619
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT -24.
LOWER 9.875
UPPER 74.
RIGHT 60.375
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
ECTRANS 1
```

```
/work09/am/flex_extract/Source/Python
$ ./submit.py --start_date 20220619 --controlfile CONTROL_EA5_20220619_TEST --public=1
```

```
Grib compression type:
 packingType: grid_simple

Output filelist: 
['EA22061900', 'EA22061901', 'EA22061902', 'EA22061903', 'EA22061904', 'EA22061905', 'EA22061906', 'EA22061907', 'EA22061908', 'EA22061909', 'EA22061910', 'EA22061911', 'EA22061912', 'EA22061913', 'EA22061914', 'EA22061915', 'EA22061916', 'EA22061917', 'EA22061918', 'EA22061919', 'EA22061920', 'EA22061921', 'EA22061922', 'EA22061923']
... clean inputdir!
... done!
FLEX_EXTRACT IS DONE!
```

**==> It WORKS!**



```
2023-06-14_21-04
/work09/am/flex_extract/Run/Control
$ cp CONTROL_EA5_20220619_TEST CONTROL_EA5_20220619_TEST2
```

```
/work09/am/flex_extract/Run/Control
$ cat CONTROL_EA5_20220619_TEST2
START_DATE 20220619
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT 119.
LOWER 10.
UPPER 35.875
RIGHT 131.375
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
ECTRANS 1
```

```

Grib compression type:
 packingType: grid_simple

Output filelist: 
['EA22061900', 'EA22061901', 'EA22061902', 'EA22061903', 'EA22061904', 'EA22061905', 'EA22061906', 'EA22061907', 'EA22061908', 'EA22061909', 'EA22061910', 'EA22061911', 'EA22061912', 'EA22061913', 'EA22061914', 'EA22061915', 'EA22061916', 'EA22061917', 'EA22061918', 'EA22061919', 'EA22061920', 'EA22061921', 'EA22061922', 'EA22061923']
... clean inputdir!
... done!
FLEX_EXTRACT IS DONE!
```

**==> It WORKS!**



```
2023-06-14_22-00
/work09/am/flex_extract/Run/Control
$ cp CONTROL_EA5_20220619_TEST2 CONTROL_EA5_20220618-20
```

```bash
2023-06-14_21-53
/work09/am/flex_extract/Source/Python
$ ./submit.py --start_date 20220618 --end_date 20220620  --controlfile CONTROL_EA5_2022061
8-20  --public=1
```

```bash
2023-06-15_09-56
/work09/am/flex_extract/Run
$ ll Workspace/EA2206* |head -3; echo; ll Workspace/|tail -3
-rw-r--r--. 1 am 11M 2023-06-15 00:06 Workspace/EA22061800
-rw-r--r--. 1 am 11M 2023-06-15 00:06 Workspace/EA22061801
-rw-r--r--. 1 am 11M 2023-06-15 00:06 Workspace/EA22061802

-rw-r--r--. 1 am  11M 2023-06-15 00:06 EA22062021
-rw-r--r--. 1 am  11M 2023-06-15 00:06 EA22062022
-rw-r--r--. 1 am  11M 2023-06-15 00:06 EA22062023
```



## FLEXPART

```bash
2023-06-15_09-58
/work09/am/FLEXPART_RUN
$ mkd ECS202206
mkdir: ディレクトリ `ECS202206' を作成しました
(base) 
am@localhost
2023-06-15_09-59
/work09/am/FLEXPART_RUN
$ cd ECS202206/
```

```bash
/work09/am/FLEXPART_RUN/ECS202206
$ cp -ar ../exercises/Hello_World_fwd/ E0619_T00
```

```bash
$ cd E0619_T00/
```

```bash
$ org.sh AVAILABLE pathnames
'AVAILABLE' -> 'AVAILABLE_ORG_230615-1002'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 1.2K Jun 13 21:09 AVAILABLE
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 1.2K Jun 13 21:09 AVAILABLE_ORG_230615-1002
ccccccccccccccccccccccccccccccccccccccccccccccc

'pathnames' -> 'pathnames_ORG_230615-1002'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 52 Jun 13 15:53 pathnames
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 52 Jun 13 15:53 pathnames_ORG_230615-1002
ccccccccccccccccccccccccccccccccccccccccccccccc
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ vi AVAILABLE 
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ head -5 AVAILABLE ;echo;tail -3 AVAILABLE 
XXXXXX EMPTY LINES XXXXXXXXX
XXXXXX EMPTY LINES XXXXXXXX
YYYYMMDD HHMMSS   name of the file(up to 80 characters)
20220618 000000      EA22061800      ON DISK
20220618 010000      EA22061801      ON DISK

20220619 210000      EA22061921      ON DISK
20220619 220000      EA22061922      ON DISK
20220619 230000      EA22061923      ON DISK
```

```
$ cat pathnames 
./options/
./output_E0619_T00/
./Workspace/
./AVAILABLE
```

```
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ mkd output_E0619_T00
mkdir: ディレクトリ `output_E0619_T00' を作成しました
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ ll Workspace
lrwxrwxrwx. 1 am 37 2023-06-13 12:20 Workspace -> /work09/am/flex_extract/Run/Workspace/
```

```
$ cd options/
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00/options
$ org.sh COMMAND OUTGRID RELEASES
'COMMAND' -> 'COMMAND_ORG_230615-1010'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 3.4K Jun 14 15:26 COMMAND
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 3.4K Jun 14 15:26 COMMAND_ORG_230615-1010
ccccccccccccccccccccccccccccccccccccccccccccccc

'OUTGRID' -> 'OUTGRID_ORG_230615-1010'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 1.3K Jun 13 13:12 OUTGRID
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 1.3K Jun 13 13:12 OUTGRID_ORG_230615-1010
ccccccccccccccccccccccccccccccccccccccccccccccc

'RELEASES' -> 'RELEASES_ORG_230615-1010'
sssssssssssssssssss SOURCE ssssssssssssssssssss
-rw-r--r--. 1 am oc 2.4K Jun 14 15:28 RELEASES
sssssssssssssssssssssssssssssssssssssssssssssss
cccccccccccccccccccc COPY ccccccccccccccccccccc
-rw-r--r--. 1 am oc 2.4K Jun 14 15:28 RELEASES_ORG_230615-1010
ccccccccccccccccccccccccccccccccccccccccccccccc
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ cat options/COMMAND
***************************************************************************************************************
*                                                                                                             *
*      Input file for the Lagrangian particle dispersion model FLEXPART                                       *
*                           ECS202206 E0619_T00                                                        *
*                                                                                                             *
***************************************************************************************************************
&COMMAND
 LDIRECT=              -1, ! Simulation direction in time   ; 1 (forward) or -1 (backward)
 IBDATE=         20220618, ! Start date of the simulation   ; YYYYMMDD: YYYY=year, MM=month, DD=day  
 IBTIME=           000000, ! Start time of the simulation   ; HHMISS: HH=hours, MI=min, SS=sec; UTC
 IEDATE=         20220619, ! End date of the simulation     ; same format as IBDATE 
 IETIME=           030000, ! End  time of the simulation    ; same format as IBTIME
 LOUTSTEP=           3600, ! Interval of model output; average concentrations calculated every LOUTSTEP (s)  
 LOUTAVER=           3600, ! Interval of output averaging (s)
 LOUTSAMPLE=          900, ! Interval of output sampling  (s), higher stat. accuracy with shorter intervals
 ITSPLIT=        99999999, ! Interval of particle splitting (s) 
 LSYNCTIME=           900, ! All processes are synchronized to this time interval (s)
 CTL=          -5.0000000, ! CTL>1, ABL time step = (Lagrangian timescale (TL))/CTL, uses LSYNCTIME if CTL<0
 IFINE=                 4, ! Reduction for time step in vertical transport, used only if CTL>1 
 IOUT=                  1, ! Output type: [1]mass 2]pptv 3]1&2 4]plume 5]1&4, +8 for NetCDF output     
 IPOUT=                 1, ! Particle position output: 0]no 1]every output 2]only at end 3]time averaged
 LSUBGRID=              0, ! Increase of ABL heights due to sub-grid scale orographic variations;[0]off 1]on 
 LCONVECTION=           1, ! Switch for convection parameterization;0]off [1]on
 LAGESPECTRA=           0, ! Switch for calculation of age spectra (needs AGECLASSES);[0]off 1]on
 IPIN=                  0, ! Warm start from particle dump (needs previous partposit_end file); [0]no 1]yes  
 IOUTPUTFOREACHRELEASE= 1, ! Separate output fields for each location in the RELEASE file; [0]no 1]yes 
 IFLUX=                 0, ! Output of mass fluxes through output grid box boundaries
 MDOMAINFILL=           0, ! Switch for domain-filling, if limited-area particles generated at boundary
 IND_SOURCE=            1, ! Unit to be used at the source   ;  [1]mass 2]mass mixing ratio 
 IND_RECEPTOR=          1, ! Unit to be used at the receptor; [1]mass 2]mass mixing ratio 3]wet depo. 4]dry depo.
 MQUASILAG=             0, ! Quasi-Lagrangian mode to track individual numbered particles 
 NESTED_OUTPUT=         0, ! Output also for a nested domain 
 LINIT_COND=            0, ! Output sensitivity to initial conditions (bkw mode only) [0]off 1]conc 2]mmr 
 SURF_ONLY=             0, ! Output only for the lowest model layer, used w/ LINIT_COND=1 or 2
 CBLFLAG=               0, ! Skewed, not Gaussian turbulence in the convective ABL, need large CTL and IFINE
 OHFIELDS_PATH= "../../flexin/", ! Default path for OH file
 /
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ cat options/RELEASES 
***************************************************************************************************************
*                                                                                                             *
*                                                                                                             *
*                                                                                                             *
*   Input file for the Lagrangian particle dispersion model FLEXPART                                          *
*                           ECS202206 E0619_T00                                                           *
*                                                                                                             *
*                                                                                                             *
*                                                                                                             *
***************************************************************************************************************
&RELEASES_CTRL
 NSPEC      =           1, ! Total number of species
 SPECNUM_REL=          24, ! Species numbers in directory SPECIES
 /
&RELEASE                   ! For each release 
 IDATE1  =       20220619, ! Release start date, YYYYMMDD: YYYY=year, MM=month, DD=day
 ITIME1  =         030000, ! Release start time in UTC HHMISS: HH hours, MI=minutes, SS=seconds
 IDATE2  =       20220619, ! Release end date, same as IDATE1
 ITIME2  =         030000, ! Release end time, same as ITIME1
 LON1    =        128.000, ! Left longitude of release box -180 < LON1 <180
 LON2    =        128.500, ! Right longitude of release box, same as LON1
 LAT1    =         30.000, ! Lower latitude of release box, -90 < LAT1 < 90
 LAT2    =         30.500, ! Upper latitude of release box same format as LAT1 
 Z1      =        250.000, ! Lower height of release box meters/hPa above reference level
 Z2      =        250.000, ! Upper height of release box meters/hPa above reference level
 ZKIND   =              1, ! Reference level 1=above ground, 2=above sea level, 3 for pressure in hPa
 MASS    =       1.0000E0, ! Total mass emitted, only relevant for fwd simulations
 PARTS   =          10000, ! Total number of particles to be released
 COMMENT =    "RELEASE 1", ! Comment, written in the outputfile
 /
```

```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ cat options/OUTGRID 
!*******************************************************************************
!                                                                              *
!      Input file for the Lagrangian particle dispersion model FLEXPART        *
!@                           ECS202206 E0619_T00                         *
!                                                                              *
! OUTLON0    = GEOGRAPHYICAL LONGITUDE OF LOWER LEFT CORNER OF OUTPUT GRID     *
! OUTLAT0    = GEOGRAPHYICAL LATITUDE OF LOWER LEFT CORNER OF OUTPUT GRID      *
! NUMXGRID   = NUMBER OF GRID POINTS IN X DIRECTION (= No. of cells + 1)       *
! NUMYGRID   = NUMBER OF GRID POINTS IN Y DIRECTION (= No. of cells + 1)       *
! DXOUT      = GRID DISTANCE IN X DIRECTION                                    *
! DYOUN      = GRID DISTANCE IN Y DIRECTION                                    *
! OUTHEIGHTS = HEIGHT OF LEVELS (UPPER BOUNDARY)                               *
!*******************************************************************************
&OUTGRID
 OUTLON0=    120.00,
 OUTLAT0=     15.00,
 NUMXGRID=       10,
 NUMYGRID=       20,
 DXOUT=           1,
 DYOUT=           1,
 OUTHEIGHTS=  100.0, 500.0, 1000.0 , 2000.0,
 /
```



```bash
/work09/am/FLEXPART_RUN/ECS202206/E0619_T00
$ FLEXPART 

 CONGRATULATIONS: YOU HAVE SUCCESSFULLY COMPLETED A FLEXPART MODEL RUN!
```



## PLOT MAP OF PARTICLES

```
/work09/am/FLEXPART_RUN
$ cd FLEX_PLOT/
```

```
2023-06-15_10-35
/work09/am/FLEXPART_RUN/FLEX_PLOT
$ PRINTPART2.sh

 PROGRAM: PRINTPART2


 date=20220619020000
 MEAN: x =    128.132919      y =    29.8508663      z =    279.384613    

 INDIR=/work09/am/FLEXPART_RUN/ECS202206/E0619_T00/output_E0619_T00/
 ODIR=/work09/am/FLEXPART_RUN/ECS202206/E0619_T00/output_PP/output_E0619_T00/
 OFLE=PP20220619020000.TXT
```





## 6月16日から20日まで

/work09/am/flex_extract/Run/Control
$ cp CONTROL_EA5_20220619_TEST2 CONTROL_EA5_20220616-20
`CONTROL_EA5_20220619_TEST2' -> `CONTROL_EA5_20220616-20'

```bash
/work09/am/flex_extract/Run/Control
$ cat CONTROL_EA5_20220616-20
START_DATE 20220616
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT 115.
LOWER  5.
UPPER 34.250
RIGHT 131.875
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
ECTRANS 1
```

```bash
2023-06-15_11-04
/work09/am/flex_extract/Source/Python
$ ./submit.py --start_date 20220616 --end_date 20220620 --contr
olfile CONTROL_EA5_20220616-20 --public=1
```

```bash
FLEX_EXTRACT IS DONE!
2023-06-15_14-38
/work09/am/flex_extract/Source/Python
```


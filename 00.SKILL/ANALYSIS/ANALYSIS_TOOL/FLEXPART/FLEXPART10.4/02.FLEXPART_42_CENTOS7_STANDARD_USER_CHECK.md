# 02.FLEXPART_42_CENTOS7_STANDARD_USER_CHECK

```bash
$ cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
```

[[_TOC_]]

## FLEX_EXTRACT_7.1.2

### CHECK  .ecmwfapirc and .cdsapirc

```
$ cd $HOME; pwd
/work09/am
```

```
/work09/am
$ ll .ecmwfapirc .cdsapirc 
-rw-r--r--. 1 am  95 2023-05-25 22:12 .cdsapirc
-rw-r--r--. 1 am 119 2022-12-09 19:16 .ecmwfapirc
```



```
$ cd $HOME/flex_extract/Run/Control

$ cat CONTROL_EA5
START_DATE 20180809
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT -24.
LOWER 9.875
UPPER 74.
RIGHT 60.375
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
RRINT 1
ECTRANS 1
```

```
$ cd $HOME/flex_extract/Source/Python
```

```bash
/work09/am/flex_extract/Source/Python
2023-06-12_17-25
$ ll submit.py 
-rwxr-xr-x. 1 am 8.9K 2023-06-12 16:56 submit.py*
```

```bash
/work09/am/flex_extract/Run/Control
$ cp  CONTROL_EA5 CONTROL_EA5_20120101
```

```bash
$ vi CONTROL_EA5_20120101
```

```bash
/work09/am/flex_extract/Run/Control
2023-06-12_18-22
$ cat CONTROL_EA5_20120101 
START_DATE 20120101
DTIME 1
TYPE AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN AN
TIME 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23
STEP 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
ACCTYPE FC
ACCTIME 06/18
ACCMAXSTEP 12
CLASS EA
STREAM OPER
GRID 0.28125  
LEFT -24.
LOWER 9.875
UPPER 74.
RIGHT 60.375
LEVELIST 1/to/137
RESOL 799
ETA 1
FORMAT GRIB2
PREFIX EA
CWC 1
RRINT 1
ECTRANS 1
```

```bash
/work09/am/flex_extract/Source/Python
2023-06-12_18-22
$ submit.py --controlfile=CONTROL_EA5_20120101 --start_date=20120101 --public=1
```

```bash
Retrieving ECMWF data!
start date 20120101 
end date 20120101 
Using ECMWF WebAPI: False
Using CDS API: True
... removing old files in /work09/am/flex_extract/Run/Workspace
... retrieve 20111231/to/20120102 in dir /work09/am/flex_extract/Run/Workspace
marsclass: EA
dataset: None
type: FC
levtype: SFC
levelist: 1
repres: 
date: 20111231/to/20120102
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 06/18
step: 1/to/12/by/1
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.256448.grb
param: 142.128/143.128/146.128/180.128/181.128/176.128
target: /work09/am/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.256448.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 17:09:56,661 INFO Welcome to the CDS
2023-06-13 17:09:56,662 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-06-13 17:09:57,005 INFO Request is completed
2023-06-13 17:09:57,005 INFO Downloading https://download-0019.copernicus-climate.eu/cache-compute-0019/cache/data0/adaptor.mars.internal-1686561744.0187004-32683-2-8e47d1b2-0e7b-42df-8fe8-c6f3a4dff1b2.grib to /work09/am/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.256448.grb (75.8M)
2023-06-13 17:11:19,690 INFO Download rate 938.8K/s            
MARS retrieve done ... 
... retrieve 20120101/to/20120101 in dir /work09/am/flex_extract/Run/Workspace
marsclass: EA
dataset: None
type: AN
levtype: SFC
levelist: 1
repres: 
date: 20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00
step: 000
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.256448.grb
param: 160.128/027.128/028.128/244.128
target: /work09/am/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.256448.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 17:11:20,731 INFO Welcome to the CDS
2023-06-13 17:11:20,731 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-06-13 17:11:21,013 INFO Downloading https://download-0017.copernicus-climate.eu/cache-compute-0017/cache/data1/adaptor.mars.internal-1686561817.5392962-12660-14-31d3ae24-fd69-4b88-83ab-8b9965b12cdd.grib to /work09/am/flex_extract/Run/Workspace/OG_OROLSM__SL.20120101.262134.256448.grb (6.3M)
2023-06-13 17:11:28,806 INFO Download rate 830.1K/s            marsclass: EA
dataset: None
type: AN
levtype: ML
levelist: 1/to/137
repres: 
date: 20120101/to/20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00/01/02/03/04/05/06/07/08/09/10/11/12/13/14/15/16/17/18/19/20/21/22/23
step: 00
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.256448.grb
param: 130.128/133.128/131.128/132.128/077.128/246.128/247.128
target: /work09/am/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.256448.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 17:11:29,203 INFO Welcome to the CDS
2023-06-13 17:11:29,203 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-complete
2023-06-13 17:11:29,405 INFO Request is queued
2023-06-13 17:36:04,168 INFO Request is running
2023-06-13 18:14:27,088 INFO Request is completed
2023-06-13 18:14:27,088 INFO Downloading https://download-0000-clone.copernicus-climate.eu/cache-compute-0000/cache/data1/adaptor.mars.external-1686645305.1914845-16458-17-ab510729-deca-497b-bed8-d33a10585e18.grib to /work09/am/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.256448.grb (3.8G)
2023-06-13 19:28:44,122 INFO Download rate 901.2K/s            
marsclass: EA
dataset: None
type: AN
levtype: SFC
levelist: 1
repres: 
date: 20120101/to/20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00/01/02/03/04/05/06/07/08/09/10/11/12/13/14/15/16/17/18/19/20/21/22/23
step: 00
expver: 1
number: OFF
accuracy: 24
grid: 0.28125/0.28125
gaussian: 
target: /work09/am/flex_extract/Run/Workspace/ANOG__SL.20120101.262134.256448.grb
param: 141.128/151.128/164.128/165.128/166.128/167.128/168.128/129.128/172.128
target: /work09/am/flex_extract/Run/Workspace/ANOG__SL.20120101.262134.256448.grb
RETRIEVE ERA5 WITH CDS API!
2023-06-13 19:28:45,239 INFO Welcome to the CDS
2023-06-13 19:28:45,239 INFO Sending request to https://cds.climate.copernicus.eu/api/v2/resources/reanalysis-era5-single-levels
2023-06-13 19:28:45,595 INFO Downloading https://download-0012-clone.copernicus-climate.eu/cache-compute-0012/cache/data3/adaptor.mars.internal-1686569535.249225-10604-10-36a15304-fcf1-40e0-b2f8-042f94ebaefd.grib to /work09/am/flex_extract/Run/Workspace/ANOG__SL.20120101.262134.256448.grb (11.1M)
2023-06-13 19:28:51,176 INFO Download rate 2M/s                
marsclass: EA
dataset: None
type: AN
levtype: ML
levelist: 1
repres: 
date: 20120101/to/20120101
resol: 799
stream: OPER
area: 74.0/-24.0/9.875/60.375
time: 00/01/02/03/04/05/06/07/08/09/10/11/12/13/14/15/16/17/18/19/20/21/22/23
step: 00
expver: 1
number: OFF
accuracy: 24
grid: OFF
032-12892fb1ee11.grib to /work09/am/flex_extract/Run/Workspace/ANSH__SL.20120101.262134.256448.grb (44M)
2023-06-13 19:45:24,391 INFO Download rate 6.8M/s              MARS retrieve done ... 
Prepare 20120101/to/20120101
... index will be done
Inputfile: /work09/am/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.256448.grb 
... index done
Opening grib file for extraction of information --- /work09/am/flex_extract/Run/Workspace/FCOG_acc_SL.20111231.262134.256448.grb

Information extracted: 
Ni = 301
Nj = 229
latitudeOfFirstGridPointInDegrees = 74.0
longitudeOfFirstGridPointInDegrees = -24.0
latitudeOfLastGridPointInDegrees = 9.875
longitudeOfLastGridPointInDegrees = 60.375
jDirectionIncrementInDegrees = 0.28125
iDirectionIncrementInDegrees = 0.28125
missingValue = 9999
CURRENT PRODUCT:  ('20101231', '600', '3')
CURRENT PRODUCT:  ('20101231', '600', '6')
CURRENT PRODUCT:  ('20101231', '600', '9')
CURRENT PRODUCT:  ('20101231', '600', '12')
CURRENT PRODUCT:  ('20101231', '1800', '3')
CURRENT PRODUCT:  ('20101231', '1800', '6')
outputfile = /work09/am/flex_extract/Run/Workspace/flux2010123122
142 1800 6 68929 0.0 0.109890132682
143 1800 6 68929 0.0 0.0746695225939
146 1800 6 68929 7.86222222222 44.911697141
180 1800 6 68929 0.233611246745 0.220898018015
181 1800 6 68929 -0.131040581597 0.311860922427
176 1800 6 68929 9.99999658697e-16 0.0
CURRENT PRODUCT:  ('20101231', '1800', '9')
outputfile = /work09/am/flex_extract/Run/Workspace/flux2011010101


CURRENT PRODUCT:  ('20121231', '1800', '6')
CURRENT PRODUCT:  ('20121231', '1800', '9')
CURRENT PRODUCT:  ('20121231', '1800', '12')
... disaggregation of precipitation with new method.
... write disaggregated precipitation to files.
... index will be done
Inputfile: /work09/am/flex_extract/Run/Workspace/ANOG__ML.20120101.262134.256448.grb 
Inputfile: /work09/am/flex_extract/Run/Workspace/ANOG__SL.20120101.262134.256448.grb 
Inputfile: /work09/am/flex_extract/Run/Workspace/ANSH__SL.20120101.262134.256448.grb 
... index done
current product:  ('20120101', '0', '0')
STATISTICS:          98233.2697  98134.8506   4396.1770
STOP SUCCESSFULLY FINISHED calc_etadot: CONGRATULATIONS


outputfile = /work09/am/flex_extract/Run/Workspace/EA12010122
current product:  ('20120101', '2300', '0')
STATISTICS:          98331.5493  98230.3661   4459.6821
STOP SUCCESSFULLY FINISHED calc_etadot: CONGRATULATIONS
outputfile = /work09/am/flex_extract/Run/Workspace/EA12010123

Postprocessing:
 Format: GRIB2

Output filelist: 
['EA12010100', 'EA12010101', 'EA12010102', 'EA12010103', 'EA12010104', 'EA12010105', 'EA12010106', 'EA12010107', 'EA12010108', 'EA12010109', 'EA12010110', 'EA12010111', 'EA12010112', 'EA12010113', 'EA12010114', 'EA12010115', 'EA12010116', 'EA12010117', 'EA12010118', 'EA12010119', 'EA12010120', 'EA12010121', 'EA12010122', 'EA12010123']
... clean inputdir!
... done!
FLEX_EXTRACT IS DONE!
```

```bash
/work09/am/flex_extract/Source/Python
2023-06-12_18-22
$ submit.py --controlfile=CONTROL_EA5_20120101 --start_date=20120101 --public=1

FLEX_EXTRACT IS DONE!
2023-06-13_20-21
```



## FLEXPART_v10.4

```
/work09/am
$ cd /work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
```

```
/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ cat AVAILABLE 
XXXXXX EMPTY LINES XXXXXXXXX
XXXXXX EMPTY LINES XXXXXXXX
YYYYMMDD HHMMSS   name of the file(up to 80 characters)
20120101 000000      EA12010100      ON DISK
20120101 010000      EA12010101      ON DISK
.....
20120101 230000      EA12010123      ON DISK
```

```
/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ cat pathnames 
./options/
./output_ECMWF/
./Workspace/
./AVAILABLE
```

```bash
/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ ll Workspace
lrwxrwxrwx. 1 am 37 2023-06-13 12:20 Workspace -> /work09/am/flex_extract/Run/Workspace/

/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ ll Workspace/
合計 3.8G
-rw-r--r--. 1 am 161M 2023-06-13 20:20 EA12010100
.....
-rw-r--r--. 1 am 160M 2023-06-13 20:21 EA12010123
```

```
/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ cdo showname Workspace/EA12010100 
 u v etadot t sp q qc param196.1.0 acpcp
```

```
/work09/am/FLEXPART_RUN/exercises/Hello_World_fwd
$ ll FLEXPART 
lrwxrwxrwx. 1 am 47 2023-06-13 12:29 FLEXPART -> /work09/am/flexpart_v10.4_3d7eebf/src2/FLEXPART*
```


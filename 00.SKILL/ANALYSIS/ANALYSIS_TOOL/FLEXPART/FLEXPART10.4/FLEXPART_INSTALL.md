# FLEXPART10.4_INSTALL

[[_TOC_]]

## 0.準備

```bash
sudo apt-get update
sudo apt-get install g++ gfortran autoconf libtool automake flex bison cmake python-dev python-pip git-core vim curl build-essential libbz2-dev libssl-dev libreadline-dev libsqlite3-dev tk-dev ruby
```



## 1. jasper

```bash
cd /usr/local
wget http://www.ece.uvic.ca/~mdadams/jasper/software/jasper-1.900.1.zip
unzip jasper-1.900.1.zip
mkdir jasper
cd jasper-1.900.1
CFLAGS="-fPIC" ./configure --prefix=/usr/local/jasper-1.900.1
make &> MAKE.LOG; make check &> MAKE.CHECK.LOG; make install
 &> MAKE.INSTALL.LOG
```



## 2. grib_api

```bash
cd /usr/local
wget --no-check-certificate https://people.freebsd.org/~sunpoet/sunpoet/grib_api-1.28.0-Source.tar.gz
tar xvzf grib_api-1.28.0-Source.tar.gz
mkdir grib_api
cd grib_api-1.28.0-Source
./configure --prefix=/usr/local/grib_api --with-jasper=/usr/local/jasper-1.900.1
make &> MAKE.LOG; make check &> MAKE.CHECK.LOG; make install
 &> MAKE.INSTALL.LOG
 
 mv grib_api grib_api-1.28.0
```



## 3. eccodes

### 3.1 cmake

```bash
# yum install cmake
# cmake --version
cmake version 2.8.12.2
```



### 3.2 eccodes

```bash
wget https://confluence.ecmwf.int/download/attachments/45757
960/eccodes-2.7.3-Source.tar.gz?api=v2
mv eccodes-2.7.3-Source.tar.gz?api=v2 eccodes-2.7.3-Source.tar.gz
tar xvzf eccodes-2.7.3-Source.tar.gz
mkdir eccodes
cd eccodes-2.7.3-Source
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/eccodes /usr/local/eccodes-2.7.3-Source -DPYTHON_EXECUTABLE=/usr/bin/python3 &> CMAKE.LOG
make &> MAKE.LOG
ctest &> CTEST.LOG
make install &> MAKE.INSTALL.LOG
export ECCODES_SAMPLES_PATH=$PATH:/usr/local/eccodes/share/eccodes/samples
export ECCODES_DEFINITION_PATH=$PATH:/usr/local/eccodes/share/eccodes/definitions
yum makecache
yum -y install eccodes-devel

python3 -m eccodes selfcheck
Found: ecCodes v2.6.0.
Your system is ready.
```



## 4. NetCDF

VERY IMPORTANT FOR Ubuntu USER

```bash
sudo apt-get install libnetcdf-dev libnetcdff-dev
```

### 4.1 zlib

```bash
v=1.2.8  
sudo wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${v}.tar.gz
sudo tar -xf zlib-${v}.tar.gz && cd zlib-${v}
sudo ./configure --prefix=/usr/local
sudo make install
cd ..
```

### 4.2 szlib

```bash
v=2.1
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/szip-${v}.tar.gz
tar -xf szip-${v}.tar.gz && cd szip-${v}
./configure --prefix=/usr/local/szip
make
make install
```

### 4.3 hdf5

```bash
v=1.8.13
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/hdf5-${v}.tar.gz
tar -xf hdf5-${v}.tar.gz && cd hdf5-${v}
prefix="/usr/local/hdf5-$v"

echo "Add HDF5_DIR=$prefix to .bashrc"
echo "" >> ~/.bashrc
echo "# HDF5 libraries for python" >> ~/.bashrc
echo export HDF5_DIR=$prefix  >> ~/.bashrc

./configure --enable-shared --enable-hl --prefix=$HDF5_DIR
make -j 2 # 2 for number of procs to be used
make install
cd ..
```

### 4.4 netcdf-c

```bash
v=4.1.3
wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${v}.tar.gz
tar -xf netcdf-${v}.tar.gz && cd netcdf-${v}
prefix="/usr/local/"

echo "Add NETCDF4_DIR=$prefix to .bashrc"
echo "" >> ~/.bashrc
echo "# NETCDF4 libraries for python" >> ~/.bashrc
echo export NETCDF4_DIR=$prefix  >> ~/.bashrc

CPPFLAGS=-I$HDF5_DIR/include LDFLAGS=-L$HDF5_DIR/lib ./configure --enable-netcdf-4 --enable-shared --enable-dap --prefix=$NETCDF4_DIR
make 
make install
cd ..
```

### 4.5 python Netcdf4

```bash
$ conda install netCDF4
Collecting package metadata (current_repodata.json): done
Solving environment: done

# All requested packages already installed.
```



## 5. FLEXPART10.4

```bash
su

cd /usr/local
wget https://www.flexpart.eu/downloads/66
tar xvf 66
cd flexpart_v10.4_3d7eebf
cp -r src src2
cd src2
vi makefile
```

```makefile
ROOT_DIR = /usr/local
F90       = /usr/bin/gfortran
MPIF90    = /usr/bin/mpifort
     
INCPATH1  = ${ROOT_DIR}/grib_api/include
INCPATH2  = ${ROOT_DIR}/jasper/include
INCPATH3 = /include
LIBPATH1 = ${ROOT_DIR}/grib_api/lib
LIBPATH2 = ${ROOT_DIR}/jasper/lib
LIBPATH3 = /lib

# Enable netCDF output?
ifeq ($(ncf), yes)
        NCOPT = -DUSE_NCF -lnetcdff -lnetcdf
else
        NCOPT = -UUSE_NCF
endif

# LIBRARIES
LIBS = -lgrib_api_f90 -lgrib_api -lm -ljasper $(NCOPT) -lnetcdff

FFLAGS   = -I$(INCPATH1) -I$(INCPATH2) -I$(INCPATH3) -O$(O_LEV) -g -cpp -m64 -mcmodel=medium -fconvert=little-endian -frecord-marker=4 -fmessage-length=0 -flto=jobserver -O$(O_LEV) $(NCOPT) $(FUSER)  #-Warray-bounds -fcheck=all # -march=native

DBGFLAGS = -I$(INCPATH1) -I$(INCPATH2) -I$(INCPATH3) -O$(O_LEV_DBG) -g3 -ggdb3 -cpp -m64 -mcmodel=medium -fconvert=little-endian -frecord-marker=4 -fmessage-length=0 -flto=jobserver -O$(O_LEV_DBG) $(NCOPT) -fbacktrace   -Wall  -fdump-core $(FUSER)  #  -ffpe-trap=invalid,overflow,denormal,underflow,zero  -Warray-bounds -fcheck=all

LDFLAGS  = $(FFLAGS) -L$(LIBPATH1) -Wl,-rpath,$(LIBPATH1) $(LIBS) -L$(LIBPATH2)
-L$(LIBPATH3)
LDDEBUG  = $(DBGFLAGS) -L$(LIBPATH1) $(LIBS) -L$(LIBPATH2) -L$(LIBPATH3)

make ncf=yes
export LD_LIBRARY_PATH=/usr/local/grib_api/lib:$LD_LIBRARY_PATH
```


## 6. flex_extract

### 6.1 flex_extract

```bash
cd /usr/local
git clone --single-branch --branch master https://www.flexpart.eu/gitmob/flex_extract
```



### 6.2 Dependence

```bash
apt-get install python3
# (usually already available on GNU/Linux systems)
apt-get install python3-eccodes
apt-get install python3-genshi
apt-get install python3-numpy
apt-get install gfortran
apt-get install fftw3-dev
apt-get install libeccodes-dev
apt-get install libemos-dev
```

```
pip install cdsapi
pip install ecmwf-api-client
```

```bash
# python3
```

Check in python3 console

```python
import eccodes
import genshi
import numpy
import cdsapi
import ecmwfapi
import eccodes
```





### 6.3  zlib1.2.9

```bash
wget https://sourceforge.net/projects/libpng/files/zlib/1.2.9/zlib-1.2.9.tar.gz
tar -xvf zlib-1.2.9.tar.gz
cd zlib-1.2.9
sudo -s
./configure; make; make install
cd /lib/x86_64-linux-gnu
ln -s -f /usr/local/lib/libz.so.1.2.9/lib libz.so.1
cd ~
rm -rf zlib-1.2.9
```

### 6.4 install emoslib

```bash
cd /usr/local
mkdir emoslib
wget https://confluence.ecmwf.int/download/attachments/3473472/libemos-4.5.9-Source.tar.gz
tar -xzf libemos-4.5.9-Source.tar.gz
cd libemos-4.5.9-Source
mkdir build ; cd build 
cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/emoslib /usr/local/libemos-4.5.9-Source
make
make install
```

### 6.5 ECMWF ERA5

```bash
cd 
vim .ecmwfapirc
```

```bash
{
    "url"   : "https://api.ecmwf.int/v1",
    "key"   : "YOUR_KEY",
    "email" : "YOUR_EMAIL_ADDRESS"
}
vim .cdsapirc
url: {api-url}
key: {uid}:{api-key}
```

### 6.6 makefile_local_gfortran

```bash
cd /usr/local/flex_extract/Source/Fortran/makefile_local_gfortran
vi makefile_local_gfortran

change the original path of ECCODES_INCLUDE_DIR to 
ECCODES_INCLUDE_DIR=/usr/include

```




### 6.7 makefile_local_gfortran

```bash
cd /usr/local/flex_extract
./setup_local.sh
```

```bash
WARNING: installdir has not been specified
flex_extract will be installed in here by compiling the Fortran source in /usr/local/flex_extract/Source/Fortran
Install flex_extract_v7.1.2 software at local in directory /usr/local/flex_extract

Using makefile: makefile_local_gfortran
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./rwgrib2.f90
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./phgrreal.f90
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./grphreal.f90
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./ftrafo.f90
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./calc_etadot.f90
gfortran   -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c    ./posnam.f90
gfortran  rwgrib2.o calc_etadot.o ftrafo.o grphreal.o posnam.o phgrreal.o -o calc_etadot_fast.out  -O3  -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -fopenmp
ln -sf calc_etadot_fast.out calc_etadot
```

```bash
lrwxrwxrwx 1 root root 20 Nov 13 20:19 ./calc_etadot -> calc_etadot_fast.out
```

### 6.8 flex_extract

```bash
cd Testing/Installation/Calc_etadot

# execute the Fortran progam without arguments
../../../Source/Fortran/calc_etadot
```

```bash
STATISTICS:          98842.4598  98709.7359   5120.5385
STOP SUCCESSFULLY FINISHED calc_etadot: CONGRATULATIONS
```



## 7. FLEXPART MPI 

### 7.1 MPI

```bash
cd /usr/local/
wget http://www.mpich.org/static/downloads/3.3.2/mpich-3.3.2.tar.gz
tar mxvzf pich-3.3.2.tar.gz
./configure --prefix=/usr/local/mpi
make 
make install
```




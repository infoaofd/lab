# FLEXPART_42_CENTOS7_STANDARD_USER_CHECK

```bash
$ cat /etc/redhat-release
CentOS Linux release 7.9.2009 (Core)
```

[[_TOC_]]

## 0.準備

```bash
sudo yum update
sudo yum install g++ gfortran autoconf libtool automake flex bison cmake python-dev python-pip git-core vim curl build-essential libbz2-dev libssl-dev libreadline-dev libsqlite3-dev tk-dev ruby
```

**Decativate Anaconda if activated**.

```bash
conda deactivate
```



## 1. jasper

```bash
cd /usr/local
wget http://www.ece.uvic.ca/~mdadams/jasper/software/jasper-1.900.1.zip
unzip jasper-1.900.1.zip
mkdir jasper
cd jasper-1.900.1
CFLAGS="-fPIC" ./configure --prefix=/usr/local/jasper
make
make check
make install
```



## 2. grib_api

```bash
cd /usr/local
wget https://people.freebsd.org/~sunpoet/sunpoet/grib_api-1.28.0-Source.tar.gz
tar xvzf grib_api-1.28.0-Source.tar.gz
mkdir grib_api
cd grib_api-1.28.0-Source
./configure --prefix=/usr/local/grib_api --with-jasper=/usr/local/jasper
make
make check
make install
```



## 3. eccodes

```bash
wget https://software.ecmwf.int/wiki/download/attachments/45757960/eccodes-2.7.3-Source.tar.gz
tar xvzf eccodes-2.7.3-Source.tar.gz
mkdir eccodes
cd eccodes-2.7.3-Source
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/eccodes /usr/local/eccodes-2.7.3-Source -DPYTHON_EXECUTABLE=/usr/bin/python3
make
ctest
make install
export ECCODES_SAMPLES_PATH=$PATH:/usr/local/eccodes/share/eccodes/samples
export ECCODES_DEFINITION_PATH=$PATH:/usr/local/eccodes/share/eccodes/definitions
sudo apt-get install libeccodes-dev
python3 -m eccodes selfcheck
Found: ecCodes v2.6.0.
Your system is ready.
```



## 4. NetCDF

VERY IMPORTANT FOR Ubuntu USER

```bash
sudo apt-get install libnetcdf-dev libnetcdff-dev
```

### 4.1 zlib

```bash
v=1.2.8  
sudo wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/zlib-${v}.tar.gz
sudo tar -xf zlib-${v}.tar.gz && cd zlib-${v}
sudo ./configure --prefix=/usr/local
sudo make install
cd ..
```

### 4.2 szlib

```bash
v=2.1
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/szip-${v}.tar.gz
tar -xf szip-${v}.tar.gz && cd szip-${v}
./configure --prefix=/usr/local/szip
make
make install
```

### 4.3 hdf5

```bash
v=1.8.13
wget ftp://ftp.unidata.ucar.edu/pub/netcdf/netcdf-4/hdf5-${v}.tar.gz
tar -xf hdf5-${v}.tar.gz && cd hdf5-${v}
prefix="/usr/local/hdf5-$v"

echo "Add HDF5_DIR=$prefix to .bashrc"
echo "" >> ~/.bashrc
echo "# HDF5 libraries for python" >> ~/.bashrc
echo export HDF5_DIR=$prefix  >> ~/.bashrc

./configure --enable-shared --enable-hl --prefix=$HDF5_DIR
make -j 2 # 2 for number of procs to be used
make install
cd ..
```

### 4.4 netcdf-c

```bash
v=4.1.3
wget http://www.unidata.ucar.edu/downloads/netcdf/ftp/netcdf-${v}.tar.gz
tar -xf netcdf-${v}.tar.gz && cd netcdf-${v}
prefix="/usr/local/"

echo "Add NETCDF4_DIR=$prefix to .bashrc"
echo "" >> ~/.bashrc
echo "# NETCDF4 libraries for python" >> ~/.bashrc
echo export NETCDF4_DIR=$prefix  >> ~/.bashrc

CPPFLAGS=-I$HDF5_DIR/include LDFLAGS=-L$HDF5_DIR/lib ./configure --enable-netcdf-4 --enable-shared --enable-dap --prefix=$NETCDF4_DIR
make 
make install
cd ..
```

### 4.5 python Netcdf4

```bash
pip install netCDF4 --upgrade
```



## 5. FLEXPART10.4

```bash
cd /usr/local
wget https://www.flexpart.eu/downloads/66
tar xvf 66
cd flexpart_v10.4_3d7eebf
cp -r src src2
cd src2
```


```
# gcc --version
gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-44)   
```

```
$ vi makefile
```

```makefile
.....
## PROGRAMS
# Unified executable names
# The same executable is used for both ECMWF and GFS metdata

# Parallel processing executable
FLEXPART-MPI = FLEXPART_MPI

# Parallel processing executable with debugging info
FLEXPART-MPI-DBG = DBG_FLEXPART_MPI

# Serial processing executable
FLEXPART-SERIAL = FLEXPART

ROOT_DIR = /work09/am/flexpart_v10.4_3d7eebf
F90       = /usr/bin/gfortran
MPIF90    = /opt/intel/oneapi/mpi/2021.7.1/bin/mpiifort
INCPATH1  = /usr/local/grib_api-1.28.0/include
INCPATH2  = /usr/local/jasper-1.900.1/include
INCPATH3 = /usr/lib64/gfortran/modules
LIBPATH1 = /usr/local/grib_api-1.28.0/lib
LIBPATH2 = /usr/local/jasper-1.900.1/lib
LIBPATH3 = /lib


### Enable netCDF output?
ifeq ($(ncf), yes)
        NCOPT = -DUSE_NCF -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -lnetcdf 
else
        NCOPT = -UUSE_NCF
endif
.....
```

```bash
$ make ncf=yes
```

```bash
$ export LD_LIBRARY_PATH=/usr/local/grib_api-1.28.0/lib:$LD_LIBRARY_PATH
```

```bash
# locate netcdf.mod
/usr2/local/netcdf-c-4.8.0/include/netcdf.mod  
```

```bash
# ls /usr/lib64/gfortran/modules
eccodes.mod  grib_api.mod  netcdf.mod@ 
```

```makefile
INCPATH3 = /usr/lib64/gfortran/modules 

FFLAGS   = -I$(INCPATH1) -I$(INCPATH2) -I$(INCPATH3)  -O$(O_LEV) -g -cpp -m64 -mcmodel=medium -fconvert=little-endian -freco│
rd-marker=4 -fmessage-length=0 -flto=jobserver -O$(O_LEV) $(NCOPT) $(FUSER)
```

```BASH
/usr/local/flexpart_v10.4_3d7eebf/src2
2023-06-12_13-32
$ FLEXPART 

 Welcome to FLEXPART Version 10.4 (2019-11-12)
 FLEXPART is free software released under the GNU General Public License.
  #### TRAJECTORY MODEL ERROR! FILE "pathnames"#### 
  #### CANNOT BE OPENED IN THE CURRENT WORKING #### 
  #### DIRECTORY.                               ####
```



## 6. flex_extract

https://www.flexpart.eu/flex_extract/Installation/local.html

### 6.1 ソースコードのダウンロード

```bash
/work09/am/INSTALL
2023-06-12_16-43
$ mkd $(myymdh)_flex_extract10.4
mkdir: ディレクトリ `2023-06-12_16_flex_extract10.4' を作成しました

am@localhost
/work09/am/INSTALL
2023-06-12_16-44
$ cd $(myymdh)_flex_extract10.4

am@localhost
/work09/am/INSTALL/2023-06-12_16_flex_extract10.4
2023-06-12_16-44
$ git clone --single-branch --branch master https://www.flexpart.eu/gitmob/flex_extract
Cloning into 'flex_extract'...
```



### 6.2 Dependence

以下ライブラリがすでにインストール済みの場合は不要

チェック方法

```bash
pip3 list
```

もしくは

```
pip3 list --format=columns
```

```bash
Package             Version  

------------------- ---------

attrs               22.2.0   
cdsapi              0.5.1    
certifi             2022.12.7
cffi                1.15.1   
charset-normalizer  2.0.12   
eccodes             1.5.2    
ecmwf-api-client    1.6.3    
findlibs            0.0.5    
Genshi              0.7.7    
idna                3.4      
importlib-resources 5.4.0    
numpy               1.12.1   
pip                 9.0.3    
pycparser           2.21     
requests            2.27.1   
setuptools          39.2.0   
six                 1.16.0   
tqdm                4.64.1   
urllib3             1.26.13  
zipp                3.6.0  
```

  

```bash
$ su
パスワード:
```

```bash
yum install python3
# (usually already available on GNU/Linux systems)
```

パッケージ python3-3.6.8-18.el7.x86_64 はインストール済みか最新バージョンです
何もしません



```bash
# ls /bin/pip*
/bin/pip-3@  /bin/pip-3.6@  /bin/pip3*  /bin/pip3.6*
```

pipではなく**pip3を使う**

```bash
# pip3 install cdsapi
```

```
# pip3 install cdsapi
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Requirement already satisfied: cdsapi in /usr2/local/lib/python3.6/site-packages
Requirement already satisfied: requests>=2.5.0 in /usr2/local/lib/python3.6/site-packages (from cdsapi)
Requirement already satisfied: tqdm in /usr2/local/lib/python3.6/site-packages (from cdsapi)
Requirement already satisfied: idna<4,>=2.5; python_version >= "3" in /usr2/local/lib/python3.6/site-packages (from requests>=2.5.0->cdsapi)
Requirement already satisfied: urllib3<1.27,>=1.21.1 in /usr2/local/lib/python3.6/site-packages (from requests>=2.5.0->cdsapi)
Requirement already satisfied: charset-normalizer~=2.0.0; python_version >= "3" in /usr2/local/lib/python3.6/site-packages (from requests>=2.5.0->cdsapi)
Requirement already satisfied: certifi>=2017.4.17 in /usr2/local/lib/python3.6/site-packages (from requests>=2.5.0->cdsapi)
Requirement already satisfied: importlib-resources; python_version < "3.7" in /usr2/local/lib/python3.6/site-packages (from tqdm->cdsapi)
Requirement already satisfied: zipp>=3.1.0; python_version < "3.10" in /usr2/local/lib/python3.6/site-packages (from importlib-resources; python_version < "3.7"->tqdm->cdsapi)
```



```bash
# pip3 install --user ecmwf-api-client
```

```
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Collecting ecmwf-api-client
  Downloading https://files.pythonhosted.org/packages/5b/44/ddeb18174b1cce4fe2c714c1968cd0e4272c6869c5073a3b270f071e5549/ecmwf-api-client-1.6.3.tar.gz
Installing collected packages: ecmwf-api-client
  Running setup.py install for ecmwf-api-client ... done
Successfully installed ecmwf-api-client-1.6.3
```

```
# pip3 install eccodes
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Successfully installed attrs-22.2.0 cffi-1.15.1 eccodes-1.5.2 findlibs-0.0.5 pycparser-2.21
```

```
# pip3 install genshi
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Collecting genshi
  Downloading https://files.pythonhosted.org/packages/3f/b3/3b75ecf6bab4bb52d6605af3a6e61ce3d393dfe349710c8a9425681dfc05/Genshi-0.7.7-py3-none-any.whl (177kB)
Installing collected packages: six, genshi
Successfully installed genshi-0.7.7 six-1.16.0
```

```
# pip3 install numpy
WARNING: Running pip install with root privileges is generally not a good idea. Try `pip3 install --user` instead.
Requirement already satisfied: numpy in /usr/lib64/python3.6/site-packages
```



```bash
# python3
```

Check in python3 console

```python
Python 3.6.8 (default, Nov 16 2020, 16:55:22) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 
import eccodes
import genshi
import numpy
import cdsapi
import ecmwfapi
```



### 6.3 zlib1.2.9

```bash
# wget --no-check-certificate https://sourceforge.net/projects/libpng/files/zlib/1.2.9/zlib-1.2.9.tar.gz
# tar -xvf zlib-1.2.9.tar.gz
# cd zlib-1.2.9
# ./configure --prefix=/usr/local/zlib-1.2.9
# make
# make install
cp libz.a /usr/local/zlib-1.2.9/lib
chmod 644 /usr/local/zlib-1.2.9/lib/libz.a
cp libz.so.1.2.9 /usr/local/zlib-1.2.9/lib
chmod 755 /usr/local/zlib-1.2.9/lib/libz.so.1.2.9
cp zlib.3 /usr/local/zlib-1.2.9/share/man/man3
chmod 644 /usr/local/zlib-1.2.9/share/man/man3/zlib.3
cp zlib.pc /usr/local/zlib-1.2.9/lib/pkgconfig
chmod 644 /usr/local/zlib-1.2.9/lib/pkgconfig/zlib.pc
cp zlib.h zconf.h /usr/local/zlib-1.2.9/include
chmod 644 /usr/local/zlib-1.2.9/include/zlib.h /usr/local/zlib-1.2.9/include/zconf.h

cd /lib/x86_64-linux-gnu
ln -s -f /usr/local/lib/libz.so.1.2.9/lib libz.so.1
cd ~
rm -rf zlib-1.2.9
```





### 6.4 install emoslib

https://confluence.ecmwf.int/display/EMOS/Installation+Guide

### 手順

FFTWのインストール

emoslibのインストール

#### FFTWのRPMパッケージをyumでインストール

emoslibのデフォルト設定では**/usr/lib64/**に**libfftw3.so**が存在している必要がある。yumでインストールすると良い。

If you install from Debian / RPM binary packages, please ensure to install development packages!

```
yum -y install fftw fftw-devel fftw-devel
```

```
# ls /usr/lib64/libfftw3.so
/usr/lib64/libfftw3.so@
```

https://blackswan.hateblo.jp/entry/2016/10/12/193638

[高速フーリエ変換](http://d.hatena.ne.jp/keyword/%B9%E2%C2%AE%A5%D5%A1%BC%A5%EA%A5%A8%CA%D1%B4%B9)のライブラリであるfftwであるが、[fortran](http://d.hatena.ne.jp/keyword/fortran)のプログラムを[コンパイル](http://d.hatena.ne.jp/keyword/%A5%B3%A5%F3%A5%D1%A5%A4%A5%EB)するのにfftw3.hなるヘッダが必要になる。

[yum](http://d.hatena.ne.jp/keyword/yum) -y install fftw ではfftw3.hがインストールされないので

[yum](http://d.hatena.ne.jp/keyword/yum) -y install fftw fftw-develとfftw-develも入れてやる必要がある。



#### FFTWをソースからインストール（RPMでインストールした場合は不要かも）

デフォルトの設定の場合/usr/local/libにインストールされる。

```
./configure
make
make install
```

```
2023-06-12_15-01
# ll /usr/local/lib/libfftw*
-rw-r--r--. 1 root 2.2M 2023-06-12 15:00 /usr/local/lib/libfftw3.a
-rwxr-xr-x. 1 root  888 2023-06-12 15:00 /usr/local/lib/libfftw3.la*
```



If you build your own FFTW, please make sure also to **build the single precision version of FFTW!** This means you **need to build twice** and you **need to add** `--enable-single` **to your configure call** with FFTW.

```
./configure --enable-single
make
make install
```

```
# ll /usr/local/lib/libfftw*
-rw-r--r--. 1 root 2.2M 2023-06-12 15:00 /usr/local/lib/libfftw3.a
-rwxr-xr-x. 1 root  888 2023-06-12 15:00 /usr/local/lib/libfftw3.la*
-rw-r--r--. 1 root 2.1M 2023-06-12 15:07 /usr/local/lib/libfftw3f.a
-rwxr-xr-x. 1 root  891 2023-06-12 15:07 /usr/local/lib/libfftw3f.la*
```



#### emoslibのインストール

https://confluence.ecmwf.int/pages/viewpage.action?pageId=47297151

```bash
# cd /usr/local
# mkdir emoslib
# wget https://confluence.ecmwf.int/download/attachments/3473472/libemos-4.5.9-Source.tar.gz
# tar -xzf libemos-4.5.9-Source.tar.gz
# cd libemos-4.5.9-Source
# mkdir build
# cd build 
```

```bash
# cmake .. -DCMAKE_INSTALL_PREFIX=/usr/local/emoslib-4.5.9 -DECCODES_PATH=/usr/local/eccodes -DENABLE_ECCODES=ON -DFFTW_USE_STATIC_LIBS=ON
```

```bash
-- ecbuild   2.9.4      /work09/am/INSTALL/2023-06-12_14_emoslib_4.5.9/libemos-4.5.9-Source/cmake
-- cmake     2.8.12     /usr/bin/cmake
-- ---------------------------------------------------------
-- Feature TESTS enabled
-- Could NOT find Boost
-- Boost unit test framework -- NOT FOUND
-- ---------------------------------------------------------
-- Feature INSTALL_TABLES enabled
-- Feature INSTALL_TOOLS enabled
-- Feature FORTRAN90 enabled
-- Feature SINGLE_PRECISION enabled
-- Feature GRIBEX_ABORT enabled
-- Feature REQUIRE_FFTW enabled
-- FFTW double precision: /usr/local/lib/libfftw3.a
-- FFTW includes : /usr/local/include
-- FFTW libraries: /usr/local/lib/libfftw3.a
-- Found package FFTW required for feature FFTW
-- Feature FFTW enabled
-- Feature LIBEMOS_BUFRDC enabled
-- Feature LIBEMOS_INTERPOLATION enabled
-- Feature LIBEMOS_GRIBEX enabled
-- [eccodes] (2.7.3)
--    ECCODES_INCLUDE_DIRS : [/usr/local/eccodes/include]
--    ECCODES_LIBRARIES : [eccodes eccodes_f90 /usr/lib64/libm.so]
-- Found package eccodes required for feature ECCODES
-- Feature ECCODES enabled
-- ---------------------------------------------------------
-- [libemos] (4.5.9)
-- BUFR_TABLES_PATH: /usr/local/emoslib-4.5.9/share/libemos/../bufrdc_tables/
-- Test tool int:          /work09/am/INSTALL/2023-06-12_14_emoslib_4.5.9/libemos-4.5.9-Source/build/tools/int
-- Test tool gg_gridname:  /work09/am/INSTALL/2023-06-12_14_emoslib_4.5.9/libemos-4.5.9-Source/build/tools/gg_gridname
-- Test tool grib_compare: /usr/local/eccodes/bin/grib_compare
-- Test tool cmp:          /usr/bin/cmp
-- TEST_BUFR_TABLES_PATH:  /work09/am/INSTALL/2023-06-12_14_emoslib_4.5.9/libemos-4.5.9-Source/bufrtables
-- BUFR_TABLES_PATH:       /usr/local/emoslib-4.5.9/share/libemos/../bufrdc_tables/
-- INTERPOL_TABLES_PATH:   share/libemos/tables
-- pkg-config file created: libemosR64.pc
-- pkg-config file created: libemos.pc
-- LIBEMOS_TPLS: FFTW eccodes
-- ---------------------------------------------------------
-- Project libemos summary
-- ---------------------------------------------------------
-- ---------------------------------------------------------
-- ---------------------------------------------------------
-- Build summary
-- ---------------------------------------------------------
-- system : [localhost.localdomain] [Linux-3.10.0-1160.el7.x86_64] [linux.64]
-- processor        : [x86_64]
-- endiness         : Little Endian -- IEEE []
-- build type       : [RelWithDebInfo]
-- timestamp        : [20230612153521]
-- install prefix   : [/usr/local/emoslib-4.5.9]
--   bin dir        : [/usr/local/emoslib-4.5.9/bin]
--   lib dir        : [/usr/local/emoslib-4.5.9/lib]
--   include dir    : [/usr/local/emoslib-4.5.9/include]
--   data dir       : [/usr/local/emoslib-4.5.9/share/libemos]
--   cmake dir      : [/usr/local/emoslib-4.5.9/share/libemos/cmake]
-- ---------------------------------------------------------
-- C -- GNU 4.8.5
--     compiler   : /usr/bin/cc
--     flags      :  -pipe -O2 -g -DNDEBUG  
--     link flags : 
-- CXX -- GNU 4.8.5
--     compiler   : /usr/bin/c++
--     flags      :  -pipe -O2 -g -DNDEBUG  
--     link flags : 
-- Fortran -- GNU 
--     compiler   : /usr/bin/gfortran
--     flags      :  -ffixed-line-length-none -fcray-pointer -fno-second-underscore -Wuninitialized -Wunused-variable -DSHAREDMEMORY -O2 -g -DNDEBUG -O2 -mtune=native -g  
--     link flags : 
-- linker : /usr/bin/ld
-- ar     : /usr/bin/ar
-- ranlib : /usr/bin/ranlib
-- link flags
--     executable [     -Wl,--disable-new-dtags ]
--     shared lib [  -Wl,--disable-new-dtags ]
--     static lib [  -Wl,--disable-new-dtags ]
-- install rpath  : $ORIGIN/../lib
-- common definitions:  BUFR_TABLES_PATH="/usr/local/emoslib-4.5.9/share/libemos/../bufrdc_tables/"
-- ---------------------------------------------------------
-- Feature summary
-- ---------------------------------------------------------
-- 
-- The following features have been enabled:

 * TESTS , libemos: Enable the unit tests
 * INSTALL_TABLES , libemos: install BUFR/GRIBex/LSM tables
 * INSTALL_TOOLS , libemos: install BUFR/GRIBex tools
 * FORTRAN90 , libemos: enable tools which need Fortran 90 (only required for FC=pgf77)
 * SINGLE_PRECISION , libemos: enable single precision version of library (in addition to double precision)
 * GRIBEX_ABORT , libemos: abort execution on GRIBex calls
 * REQUIRE_FFTW , libemos: require package: Fastest Fourier Transform in the West
 * FFTW , libemos: Fastest Fourier Transform in the West
   allows interpolation of spectral to octahedral reduced Gaussian grid
 * LIBEMOS_BUFRDC , libemos: enable BUFR decoding functionality
 * LIBEMOS_INTERPOLATION , libemos: enable interpolation functionality
 * LIBEMOS_GRIBEX , libemos: enable GRIBex functionality
 * ECCODES , libemos: Use eccodes instead of grib_api

-- The following OPTIONAL packages have been found:

 * Git
 * PkgConfig

-- The following RECOMMENDED packages have been found:

 * FFTW , libemos: Fastest Fourier Transform in the West
   allows interpolation of spectral to octahedral reduced Gaussian grid

-- The following REQUIRED packages have been found:

 * eccodes (required version >= 2.4.1) , Use eccodes instead of grib_api

-- The following features have been disabled:

 * LIBEMOS_TESTS_REGRESS , libemos: additional tests: regression
 * LIBEMOS_BUILD_SHARED_LIBS , libemos: build shared libraries (unsupported)

-- The following OPTIONAL packages have not been found:

 * Boost (required version >= 1.47.0)

-- Configuring done
-- Generating done
-- Build files have been written to: /work09/am/INSTALL/2023-06-12_14_emoslib_4.5.9/libemos-4.5.9-Source/build
```

```
# make
```

```
LOTS OF MESSAGES
....
Linking Fortran static library ../lib/libemos.a
```

```
# make install
```

```
LOTS OF MESSAGES
....
-- Installing: /usr/local/emoslib-4.5.9/lib/libemos.a
-- Installing: /usr/local/emoslib-4.5.9/lib/libemosR64.a
```



### 6.5 ECMWF ERA5

```bash
cd 
vim .ecmwfapirc
```

```bash
{
    "url"   : "https://api.ecmwf.int/v1",
    "key"   : "YOUR_KEY",
    "email" : "YOUR_EMAIL_ADDRESS"
}
```

```
vim .cdsapirc
```

```
url: {api-url}
key: {uid}:{api-key}
```



### 6.6 makefile_local_gfortran

```
/work09/am/INSTALL/2023-06-12_16_flex_extract10.4/flex_extract/Source/Fortran
2023-06-12_17-05
$ ll makefile_local_gfortran 
lrwxrwxrwx. 1 am 13 2023-06-12 16:56 makefile_local_gfortran -> makefile_fast
```

```
$ bak.sh makefile_local_gfortran 
'makefile_local_gfortran' -> 'makefile_local_gfortran_230612-1705'
```



```
# ls /usr/local/eccodes/include
eccodes.h    eccodes_config.h          eccodes_version.h  grib_api.h
eccodes.mod  eccodes_ecbuild_config.h  eccodes_windef.h   grib_api.mod
```

```bash
$ vi makefile_local_gfortran
```

```bash
# change the original path of ECCODES_INCLUDE_DIR to 
ECCODES_INCLUDE_DIR=/usr/local/eccodes/include
```

```bash
ECCODES_LIB = -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm 
```



### 6.7 install flex_extract

```
$ cd /work09/am/INSTALL/2023-06-12_16_flex_extract10.4/
$ cp -ar flex_extract/ $HOME
```

```bash
/work09/am/flex_extract
2023-06-12_17-16
$ setup_local.sh 

WARNING: installdir has not been specified
flex_extract will be installed in here by compiling the Fortran source in /work09/am/flex_extract/Source/Fortran
Install flex_extract_v7.1.2 software at local in directory /work09/am/flex_extract

Using makefile: makefile_local_gfortran
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./rwgrib2.f90
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./phgrreal.f90
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./grphreal.f90
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./ftrafo.f90
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./calc_etadot.f90
gfortran   -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -I. -I/usr/local/eccodes/include -fdefault-real-8 -fopenmp -fconvert=big-endian   -c   ./posnam.f90
gfortran  rwgrib2.o calc_etadot.o ftrafo.o grphreal.o posnam.o phgrreal.o -o calc_etadot_fast.out  -O3  -L/usr/local/emoslib-4.5.9/lib -Bstatic -leccodes_f90 -leccodes -Bdynamic -lm  -lemosR64 -fopenmp
ln -sf calc_etadot_fast.out calc_etadot

lrwxrwxrwx. 1 am oc 20  6月 12 17:16 ./calc_etadot -> calc_etadot_fast.out
```

```bash
lrwxrwxrwx. 1 root root 20  6月 12 16:16 ./calc_etadot -> calc_etadot_fast.out
```



### 6.8 flex_extract

```bash
$ cd /work09/am/flex_extract
$ cd Testing/Installation/Calc_etadot/
```

```
$ ll ../../../Source/Fortran/calc_etadot
lrwxrwxrwx. 1 am 20 2023-06-12 17:16 ../../../Source/Fortran/calc_etadot -> calc_etadot_fast.out*
```

```
$ ../../../Source/Fortran/calc_etadot
STATISTICS:          98842.4598  98709.7359   5120.5385
STOP SUCCESSFULLY FINISHED calc_etadot: CONGRATULATIONS
```







## 7. FLEXPART MPI 

### 7.1 MPI

```bash
cd /usr/local/
wget http://www.mpich.org/static/downloads/3.3.2/mpich-3.3.2.tar.gz
tar mxvzf pich-3.3.2.tar.gz
./configure --prefix=/usr/local/mpi
make 
make install
```



## 備考

### ライブラリの場所

```
$ ll /usr/local/eccodes/lib
合計 9.0M
-rwxr-xr-x. 1 root 8.4M 2023-06-05 14:35 libeccodes.so*
-rwxr-xr-x. 1 root 623K 2023-06-05 14:36 libeccodes_f90.so*
drwxr-xr-x. 2 root   46 2023-06-05 14:39 pkgconfig/
```

```
$ ll /usr/local/grib_api-1.28.0/lib
合計 21M
-rw-r--r--. 1 root  13M 2023-06-05 14:23 libgrib_api.a
-rwxr-xr-x. 1 root  990 2023-06-05 14:23 libgrib_api.la*
lrwxrwxrwx. 1 root   20 2023-06-05 14:23 libgrib_api.so -> libgrib_api.so.1.0.0*
lrwxrwxrwx. 1 root   20 2023-06-05 14:23 libgrib_api.so.1 -> libgrib_api.so.1.0.0*
-rwxr-xr-x. 1 root 6.1M 2023-06-05 14:23 libgrib_api.so.1.0.0*
-rw-r--r--. 1 root 489K 2023-06-05 14:23 libgrib_api_f77.a
-rwxr-xr-x. 1 root 1018 2023-06-05 14:23 libgrib_api_f77.la*
lrwxrwxrwx. 1 root   24 2023-06-05 14:23 libgrib_api_f77.so -> libgrib_api_f77.so.1.0.0*
lrwxrwxrwx. 1 root   24 2023-06-05 14:23 libgrib_api_f77.so.1 -> libgrib_api_f77.so.1.0.0*
-rwxr-xr-x. 1 root 348K 2023-06-05 14:23 libgrib_api_f77.so.1.0.0*
-rw-r--r--. 1 root 670K 2023-06-05 14:23 libgrib_api_f90.a
-rwxr-xr-x. 1 root 1018 2023-06-05 14:23 libgrib_api_f90.la*
lrwxrwxrwx. 1 root   24 2023-06-05 14:23 libgrib_api_f90.so -> libgrib_api_f90.so.1.0.0*
lrwxrwxrwx. 1 root   24 2023-06-05 14:23 libgrib_api_f90.so.1 -> libgrib_api_f90.so.1.0.0*
-rwxr-xr-x. 1 root 440K 2023-06-05 14:23 libgrib_api_f90.so.1.0.0*
drwxr-xr-x. 2 root   48 2023-06-05 14:23 pkgconfig/
```

```
$ ll /usr/local/netcdf-c-4.8.0/lib
合計 7.5M
-rwxr-xr-x. 1 root 1.2K 2022-11-15 16:47 libh5bzip2.la*
-rwxr-xr-x. 1 root 108K 2022-11-15 16:47 libh5bzip2.so*
-rw-r--r--. 1 root 2.4M 2022-11-15 16:47 libnetcdf.a
-rwxr-xr-x. 1 root 1.2K 2022-11-15 16:47 libnetcdf.la*
-rw-r--r--. 1 root 1.3K 2022-11-15 16:47 libnetcdf.settings
lrwxrwxrwx. 1 root   19 2022-11-15 16:47 libnetcdf.so -> libnetcdf.so.19.0.0*
lrwxrwxrwx. 1 root   19 2022-11-15 16:47 libnetcdf.so.19 -> libnetcdf.so.19.0.0*
-rwxr-xr-x. 1 root 1.7M 2022-11-15 16:47 libnetcdf.so.19.0.0*
-rw-r--r--. 1 root 2.0M 2022-11-15 17:58 libnetcdff.a
-rwxr-xr-x. 1 root 1.4K 2022-11-15 17:58 libnetcdff.la*
-rw-r--r--. 1 root 1.1K 2022-11-15 17:58 libnetcdff.settings
lrwxrwxrwx. 1 root   19 2022-11-15 17:58 libnetcdff.so -> libnetcdff.so.7.0.0*
lrwxrwxrwx. 1 root   19 2022-11-15 17:58 libnetcdff.so.7 -> libnetcdff.so.7.0.0*
-rwxr-xr-x. 1 root 1.4M 2022-11-15 17:58 libnetcdff.so.7.0.0*


```

```
$ ll /usr/local/zlib-1.2.13/lib
合計 272K
-rw-r--r--. 1 root 150K 2022-11-15 14:26 libz.a
lrwxrwxrwx. 1 root   14 2022-11-15 14:26 libz.so -> libz.so.1.2.13*
lrwxrwxrwx. 1 root   14 2022-11-15 14:26 libz.so.1 -> libz.so.1.2.13*
-rwxr-xr-x. 1 root 120K 2022-11-15 14:26 libz.so.1.2.13*
drwxr-xr-x. 2 root   21 2022-11-15 14:26 pkgconfig/
```

```
$ ll /usr/local/szip-2.1.1/lib
合計 116K
-rw-r--r--. 1 root 58K 2022-11-15 14:30 libsz.a
-rwxr-xr-x. 1 root 917 2022-11-15 14:30 libsz.la*
lrwxrwxrwx. 1 root  14 2022-11-15 14:30 libsz.so -> libsz.so.2.0.0*
lrwxrwxrwx. 1 root  14 2022-11-15 14:30 libsz.so.2 -> libsz.so.2.0.0*
-rwxr-xr-x. 1 root 52K 2022-11-15 14:30 libsz.so.2.0.0*
```


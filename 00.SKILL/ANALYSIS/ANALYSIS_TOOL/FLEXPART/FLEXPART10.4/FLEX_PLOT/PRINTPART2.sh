EXE=$(basename $0 .sh)
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE;exit 1;fi

DATE=$1; RUN=$2

DATE=${DATE:-20220619020000}
RUN=${RUN:-E0619_T01}

ROOTD=/work09/am/00.WORK/2022.ECS2022/12.12.FLEXPART/FLEXPART_RUN/ECS202206/${RUN}/
INDIR=${ROOTD}/output_${RUN}/
ODIR=${ROOTD}/output_PP/output_${RUN}/

mkd $ODIR

$EXE $INDIR $DATE $ODIR



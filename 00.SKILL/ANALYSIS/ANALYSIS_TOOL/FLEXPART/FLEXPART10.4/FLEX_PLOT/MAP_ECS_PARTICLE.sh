#!/bin/bash
# Directory: /work09/am/FLEXPART_RUN/FLEX_PLOT
#

DATE=$1; RUN=$2
DATE=${DATE:-20220619020000}
RUN=${RUN:-E0619_T01}
Y=${DATE:0:4};M=${DATE:4:2};D=${DATE:6:2};H=${DATE:8:2}
echo $Y $M $D $H

. ./gmtpar.sh

range=-R110/145/15/35; DOMAIN=ECS
size=-JM5
xanot=a10f2; yanot=a10f2
HH=$(printf %02d $H)
if [ $M -eq 1 ];  then MMM="JAN"; fi; if [ $M -eq 2 ];  then MMM="FEB"; fi
if [ $M -eq 3 ];  then MMM="MAR"; fi; if [ $M -eq 4 ];  then MMM="APR"; fi
if [ $M -eq 5 ];  then MMM="MAY"; fi; if [ $M -eq 6 ];  then MMM="JUN"; fi
if [ $M -eq 7 ];  then MMM="JUL"; fi; if [ $M -eq 8 ];  then MMM="AUG"; fi
if [ $M -eq 9 ];  then MMM="SEP"; fi; if [ $M -eq 10 ]; then MMM="OCT"; fi
if [ $M -eq 11 ]; then MMM="NOV"; fi; if [ $M -eq 12 ]; then MMM="DEC"; fi
anot=-B${xanot}/${yanot}:."${HH}\07200UTC${D}${MMM}${Y}":WSne

gmtset HEADER_OFFSET		= -0.3

ROOTD=/work09/am/00.WORK/2022.ECS2022/12.12.FLEXPART/FLEXPART_RUN/ECS202206/${RUN}
INDIR=${INDIR:-${ROOTD}/output_PP/output_${RUN}}

INFLE=PP${DATE}.TXT
in=$INDIR/$INFLE
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  if [ -d $INDIR ]; then
    echo; echo ${INDIR}:; ls $INDIR; echo
  fi
  exit 1
fi
out=MAP_${DOMAIN}_${RUN}_$(basename $in .TXT).ps
PDF=MAP_${DOMAIN}_${RUN}_$(basename $in .TXT).PDF

pscoast $range $size -X1.5 -Y5.2 -Di -W3 \
 -K -P >$out

# awk '{if($1!="#")print $1, $2}' $in

awk '{if($1!="#")print $1, $2}' $in |\
psxy $range $size -G255/0/0 -Sc0.005 -O -K >>$out

psbasemap $range $size $anot -O -K >>$out

xoffset=-1; yoffset=3.7

curdir1=$(pwd); now=$(date -R); host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${in} (${time})
0 0.75  9 0 1 LM INPUT:
0 0.60  9 0 1 LM INPUT:
0 0.45  9 0 1 LM OUTPUT: ${out} 
EOF

if [ -f $out ];then ps2pdfwr $out $PDF; rm -v $out; fi


echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
if [ -f $PDF ];then 
echo "OUTPUT : "
ls -lh --time-style=long-iso $PDF
fi
echo

echo "Done $0"

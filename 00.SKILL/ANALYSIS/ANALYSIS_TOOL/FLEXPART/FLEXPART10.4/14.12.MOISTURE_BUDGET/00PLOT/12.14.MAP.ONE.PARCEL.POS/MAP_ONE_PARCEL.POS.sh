#!/bin/bash
# Directory: /work09/am/FLEXPART_RUN/FLEX_PLOT
#

DATE=$1; RUN=$2
RUN=${RUN:-E0619_T02.00}
#Y=${DATE:0:4};M=${DATE:4:2};D=${DATE:6:2};H=${DATE:8:2}
#echo $Y $M $D $H

. ./gmtpar.sh

range=-R110/145/15/35; DOMAIN=ECS
size=-JM5
xanot=a10f2; yanot=a10f2
<<COMMENT
HH=$(printf %02d $H)
if [ $M -eq 1 ];  then MMM="JAN"; fi; if [ $M -eq 2 ];  then MMM="FEB"; fi
if [ $M -eq 3 ];  then MMM="MAR"; fi; if [ $M -eq 4 ];  then MMM="APR"; fi
if [ $M -eq 5 ];  then MMM="MAY"; fi; if [ $M -eq 6 ];  then MMM="JUN"; fi
if [ $M -eq 7 ];  then MMM="JUL"; fi; if [ $M -eq 8 ];  then MMM="AUG"; fi
if [ $M -eq 9 ];  then MMM="SEP"; fi; if [ $M -eq 10 ]; then MMM="OCT"; fi
if [ $M -eq 11 ]; then MMM="NOV"; fi; if [ $M -eq 12 ]; then MMM="DEC"; fi
COMMENT
anot=-B${xanot}/${yanot}:."${RUN}":WSne

gmtset HEADER_OFFSET		= -0.3

ROOTD=/work09/am/00.WORK/2022.ECS2022/12.14.FLEXPART_MOIST_BUDG/FLEXPART_MPI_RUN/ECS202206/${RUN}
if [ ! -d $ROOTD ];then echo NO SUCH DIR,$ROOTD;exit 1;fi
INDIR=${INDIR:-${ROOTD}/output_${RUN}}
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INFLE=partposit_\*
INLIST=$(ls $INDIR/$INFLE)

out=MAP_${DOMAIN}_${RUN}.ps
PDF=$(basename $out .ps).PDF

pscoast $range $size -X1.5 -Y5.2 -Dl -W1 \
 -K -P >$out

# awk '{if($1!="#")print $1, $2}' $in


for IN in $INLIST; do

echo $IN

nn=0; ne=10000; ns=2000
while [ $nn -le $ne ]; do
awk -v n=$nn '{if(NR==n && $1!="#")print $4, $5}' $IN |\
psxy $range $size -G255/0/0 -Sc0.01 -O -K >>$out

nn=$(expr $nn + $ns)

done #INLIST
done #nn

psbasemap $range $size $anot -O -K >>$out

xoffset=-1; yoffset=3.7

curdir1=$(pwd); now=$(date -R); host=$(hostname)

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INDIR: ${INDIR}
0 0.75  9 0 1 LM OUTPUT: ${out} 
EOF

if [ -f $out ];then ps2pdfwr $out $PDF; rm -v $out; fi


echo
echo "INPUT : "
echo $IN #ls -lh --time-style=long-iso $IN
echo
if [ -f $PDF ];then 
echo "OUTPUT : "
echo $PDF #ls -lh --time-style=long-iso $PDF
fi
echo

echo "Done $0"

import numpy as np
import matplotlib.pyplot as plt
m = 100
rng = np.random.default_rng(0)
x = rng.standard_normal(m)
x = (x - x.mean()) / x.std()
y = x + rng.random(m)
y = (y - y.mean()) / y.std()
 
X = np.array([x, y]).transpose()
m, n = X.shape
U, s, Vt = np.linalg.svd(X)
V = Vt.transpose()
 
fig, ax = plt.subplots(figsize=(8, 8))
ax.scatter(x, y, color='gray')
c = ["r", "b"]

for i in range(n):
   ax.quiver(s[i]*V[0, i], s[i]*V[1, i],
   units='xy', angles='xy', scale=8, color=c[i])
ax.set_aspect('equal')

FIG="SIMPLE_EXAMPLE_PYTHON_SVD.PDF"

plt.savefig(FIG)
print("FIG: "+FIG)


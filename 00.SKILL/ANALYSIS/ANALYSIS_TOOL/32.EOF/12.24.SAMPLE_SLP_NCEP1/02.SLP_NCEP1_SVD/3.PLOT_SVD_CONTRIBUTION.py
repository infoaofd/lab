import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

"""
月平均の海面気圧データに特異値分解を適用
"""

# 入力ファイルを開く
ds = xr.open_dataset("slp.mon.mean.nc")

# 1月を取り出して，平均と標準偏差を計算する
slp = ds.slp[0::12]
slp_clim = slp.mean(axis=0)
slp_stddev = slp.std(axis=0)

"""
緯度円の長さは極に近づくほど短くなるので，1点1点が代表する面積は緯度が
高くなるにつれて縮小する。

そのためデータには緯度の重みを掛ける必要がる。

緯度円の長さは緯度の余弦に比例するので，これを重みとして用いる。

共分散行列の各要素の単位は元のデータの単位の2乗になるので，
偏差行列には平方根が重みになる。
"""
wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

"""
whereメソッドで北緯20度より極よりのデータを選びます。 
既定では大きさはそのままで非数np.NaNが入るので，
drop=Trueで落とします。 
空間を1次元化したあと転置して各年の偏差を列ベクトルとする
行列にします。
"""
dslp = (slp - slp_clim) / slp_stddev
X = dslp * wgt
X = (X.where(ds.lat>20, drop=True)).data.reshape(X.shape[0],-1).transpose()

# 行列Xを特異値分解をする
U, s, V = np.linalg.svd(X)

# 寄与率を図示する
contrib = (s * s) / (s @ s) * 100

plt.rcParams["font.size"] = 15
fig, ax = plt.subplots(figsize=(8,8))
ax.plot(contrib[0:10])
ax.plot(contrib[0:10],'bo')
ax.set_title("contribution rate %", fontsize=20)

# 図のファイルへの書き出し
FIG="3.PLOT_SVD_CONTRIBUTION.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)



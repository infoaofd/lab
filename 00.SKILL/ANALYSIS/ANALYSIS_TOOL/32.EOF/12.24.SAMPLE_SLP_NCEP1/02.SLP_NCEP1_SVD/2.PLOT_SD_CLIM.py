import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds = xr.open_dataset("slp.mon.mean.nc")

# 1月を取り出して，平均と標準偏差を計算します。
slp = ds.slp[0::12]
slp_clim = slp.mean(axis=0)
slp_stddev = slp.std(axis=0)

# 描画のための関数を定義しておきます。 解析結果がndarrayになるので，経度と緯度も引数として与えます。
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
def plot_nps(lon, lat, data):
    datac, lonc = add_cyclic_point(data, lon)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, projection=ccrs.NorthPolarStereo(135))
    p = ax.contourf(lonc, lat, datac, transform=ccrs.PlateCarree())
    ax.set_extent([0, 359.999, 20, 90], ccrs.PlateCarree())
    fig.colorbar(p)
    ax.coastlines()

# 関数の呼び出し
plot_nps(ds.lon, ds.lat, slp_stddev)

# 図をファイルに書き出す
FIG="2.PLOT_SD_CLIM.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)



import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

"""
特異値分解した結果の何番目のモードを作図するか指定する
"""
IMODE=3

#　入力ファイルを開く
ds = xr.open_dataset("slp.mon.mean.nc")

# 1月を取り出して，平均と標準偏差を計算します。
slp = ds.slp[0::12]
slp_clim = slp.mean(axis=0)
slp_stddev = slp.std(axis=0)

# 描画のための関数を定義しておきます。 解析結果がndarrayになるので，経度と緯度も引数として与えます。
import cartopy.crs as ccrs
from cartopy.util import add_cyclic_point
def plot_nps(lon, lat, data):
    datac, lonc = add_cyclic_point(data, lon)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111, projection=ccrs.NorthPolarStereo(135))
    p = ax.contourf(lonc, lat, datac, transform=ccrs.PlateCarree())
    ax.set_extent([0, 359.999, 20, 90], ccrs.PlateCarree())
    fig.colorbar(p)
    ax.coastlines()

"""
緯度円の長さは極に近づくほど短くなりますので，
1点1点が代表する面積は緯度が高くなるにつれて縮小します。
そのためデータには緯度の重みを掛ける必要があります。
緯度円の長さは緯度の余弦に比例するので，
これを重みとして用います。
共分散行列では海面気圧偏差の2乗になるので，
偏差行列には平方根が重みになります。
"""
wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

"""
whereメソッドで北緯20度より極よりのデータを選びます。 
既定では大きさはそのままで非数np.NaNが入るので，
drop=Trueで落とします。 
空間を1次元化したあと転置して各年の偏差を列ベクトルとする
行列にします。
"""
dslp = (slp - slp_clim) / slp_stddev
X = dslp * wgt
X = (X.where(ds.lat>20, drop=True)).data.reshape(X.shape[0],-1).transpose()

# 行列Xを特異値分解をする
U, s, V = np.linalg.svd(X)

"""
行列dslpをVに射影することで各モードの空間パターンを得る
これを回帰と呼ぶことがある

Vの各行ベクトルが各モードの時系列を表している
  
v_i: Vの行ベクトル(第i行)
v_i=(v_i1 v_i2 ... v_in)
"""

D = (dslp.data.transpose() @ V.transpose()).transpose()

# 関数の呼び出し
plot_nps(ds.lon, ds.lat, D[IMODE-1])
"""
pythonの配列が0番から始まるので1を引いている
"""

# 図をファイルに書き出す
FIG="4.PLOT_MAP_SVD_M"+str(IMODE)+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)



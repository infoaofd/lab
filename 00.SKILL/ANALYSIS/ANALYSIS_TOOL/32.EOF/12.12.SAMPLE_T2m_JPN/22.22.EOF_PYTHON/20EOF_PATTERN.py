# -*- coding:utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("ERA5_T2m_120-160_20-50_DJF_ANO.nc")

import cartopy.crs as ccrs


def plot_map(lon, lat, M, data):
#    datac, lonc = add_cyclic_point(data, lon)
    fig = plt.figure(figsize=(8, 6))
    ax = fig.add_subplot(111,projection=ccrs.PlateCarree())
    p = ax.contourf(lon, lat, data, transform=ccrs.PlateCarree())
    ax.set_extent([120, 160, 20, 50], ccrs.PlateCarree())
    fig.colorbar(p)
    ax.coastlines()
    ax.set_title('ERA5 T2m DJF EOF MODE='+str(M))
    fig.savefig("20EOF_PATTERN_ERA5_T2m_DJF_EOF_M"+str(M)+".PDF")

dslp = ds.T2ADJF
print(dslp.shape)

#print(ds.lon)
#print(ds.lat)

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = X.data.reshape(X.shape[0],-1) #.transpose()
# Xの行=時刻, Xの列＝データ点
print(X.shape)

U, s, V = np.linalg.svd(X)
# U,Vは直交行列

M=1
VM=V[M-1,:]*s[M-1] 
# sを掛けて次元量に戻す
print(VM.shape)
D=VM.reshape([121,161])
D=-D #nclの計算と符号を合わせる
print(D.shape)
plot_map(ds.lon, ds.lat, M, D)

M=2
VM=V[M-1,:]*s[M-1]
# sを掛けて次元量に戻す
print(VM.shape)
D=VM.reshape([121,161])
D=-D #nclの計算と符号を合わせる
print(D.shape)
plot_map(ds.lon, ds.lat, M, D)

; 
; PREPROC

script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

INDIR="/work01/DATA/ERA5/JPN/MON/"
INFLE="ERA5_T2m_120-160_20-50.grib"
IN=INDIR+INFLE
a=addfile(IN,"r")

xIN=a->2T_GDS0_SFC_S123
time=a->initial_time0_hours
lat=a->g0_lat_1
lon=a->g0_lon_2

  month_abbr = (/"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", "Oct","Nov","Dec"/)
  utc_date = cd_calendar(time, 0)
  year   = tointeger(utc_date(:,0))    ; Convert to integer for
  month  = tointeger(utc_date(:,1))    ; use sprinti 
  day    = tointeger(utc_date(:,2))
  hour   = tointeger(utc_date(:,3))
  minute = tointeger(utc_date(:,4))
  second = utc_date(:,5)

  date_str = sprinti("%0.2iZ ", hour) + sprinti("%0.2i ", day) + \
              month_abbr(month) + " "  + sprinti("%0.4i", year)
dims=dimsizes(time)
NE=dims(0)-9
NS=NE-32*12+1
print("INPUT FILE: START="+date_str(NS)) 
print("INPUT FILE: END  ="+date_str(NE)) 

dims=dimsizes(time)
NE=dims(0)-9
NS=NE-32*12+1
y1=1991
y2=2022

xMON=xIN(NS:NE,:,:)
xDJF = month_to_season (xMON, "DJF")

xDJF!0="time"
xDJF!1="lat"
xDJF!2="lon"

T2DJF     = xDJF(time|:,lat|:,lon|:) 
T2ADJF=T2DJF
var   = dim_rmvmean_n(T2DJF,0)                    ; 時間平均を除去して偏差に
T2ADJF   = dtrend_n(var,False,0)                   ; トレンドを除く
T2ADJF!0="lat"
T2ADJF!1="lon"
T2ADJF!2="time"
copy_VarCoords(T2DJF,T2ADJF)
copy_VarMeta(T2DJF,T2ADJF)

;printVarSummary(T2DJF)

OFLE="ERA5_T2m_120-160_20-50_DJF_ANO.nc"
system("rm -vf "+OFLE)
out = addfile(OFLE,"c")
;out->T2DJF = T2DJF
out->T2ADJF = T2ADJF
printVarSummary(T2ADJF)

OFLE2="ERA5_T2m_120-160_20-50_DJF.nc"
system("rm -vf "+OFLE2)
out = addfile(OFLE2,"c")
out->T2DJF = T2DJF
print("")
print("OUT: " + OFLE)
print("OUT: " + OFLE2)
print("")

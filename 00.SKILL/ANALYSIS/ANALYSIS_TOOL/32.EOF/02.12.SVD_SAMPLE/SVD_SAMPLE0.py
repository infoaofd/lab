# linalgモジュールはLAとしてimportする
import numpy as np
import numpy.linalg as LA

#2次正方行列Aを定義する
a = np.array([[5, 2],[2, 5]])

# 2 次正方行列 U,Σ,V(転置行列)を求める
U, S, Vt = LA.svd(a, full_matrices=True)
print("左特異値ベクトルを列として並べた２次正方行列 U")
print(U)
print("特異値以外を0で埋めた２次正方行列 Σ")
print(np.diag(S))
print("右特異値ベクトルを列として並べた２次正方行列 V")
print(Vt)

#特異値分解の確認（U＊Σ＊Vt） 行列Aが算出される
print("固有値分解 確認 A = U x Σ x Vt")
print(np.dot(np.dot(U,np.diag(S)),Vt))


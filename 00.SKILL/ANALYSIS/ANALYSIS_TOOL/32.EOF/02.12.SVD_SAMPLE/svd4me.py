import numpy as np

 

def svd_w(A):

    """

        Return  r, urow, s, vrow

       

                r:    Rank of A

                urow: Left singular vectors as row vectors

                s:    Singular value in descending order

                vrow: Right sigular vectors as row vectors

 

    """

    u, s, vh = np.linalg.svd(A, full_matrices = False)

    r = np.linalg.matrix_rank(np.diag(s))

    M, N = A.shape

    urow = None

    vrow = None

    if r == 0:

        s = np.array([0])

        urow = np.full((1, M), 0.0)

        vrow = np.full((1, N), 0.0)

    else:

        s = s[:r]

        urow = u.T[:r]

        vrow = vh[:r]

 

    return r, urow, s, vrow

import numpy as np

from svd4me import svd_w

 

u1 = np.array([[1, 1, 1, 1]]).transpose()

u2 = np.array([[1, 1, -1, -1]]).transpose()

mu1 = 3.0

mu2 = 2.0

v1 = np.array([[1, 1, -1]])

v2 = np.array([[1, 0, 1]])

A = mu1 * u1 @ v1 + mu2 * u2 @ v2

print('A =\n', A)

 

r, urow, s, vrow = svd_w(A)

print('r = ', r)

print('urow = \n', urow)

print('s = \n', s)

print('vrow = \n', vrow)

 

m, n = A.shape

ckA = np.full((m, n), 0.0)

for i in range(r):

    ckA += s[i] * np.array([urow[i]]).transpose() @ \
                np.array([vrow[i]])

print('ckA =\n', ckA)

 

ckA = urow.transpose() @ np.diag(s) @ vrow

print('ckA =\n', ckA)

yyyy1=2006;yyyy2=2019; y=$yyyy1

mm1=06; dd1=01
#mm2=06; dd2=30
mm2=08; dd2=31

NCL=12.QVEC_AND_CNV_MSM.ncl
if [ ! -f $NCL ];then echo EEEEE NO SUCH FILE, $NCL;exit 1;fi

INROOT=/work02/DATA/MSM.NC/MSM-P/
if [ ! -d $INROOT ];then echo EEEEE NO SUCH DIR, $INROOT;exit 1;fi

while [ $y -le $yyyy2 ]; do

INDIR=/work02/DATA/MSM.NC/MSM-P/${y}
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR, $INDIR;exit 1;fi

ODIR=/work02/DATA/MSM.NC/Q-VECTOR/MSM-P/${y}
mkdir -vp $ODIR
FIGDIR=FIG_$(basename $NCL .ncl)/${y}
mkdir -vp $ODIR

start=${y}/${mm1}/${dd1}; end=${y}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${y}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  Y=${date_out:0:4}; MM=${date_out:4:2}; DD=${date_out:6:2}
  echo MMMMM $Y $MM $DD
  runncl.sh $NCL $Y $MM $DD $INDIR $ODIR $FIGDIR
  echo

  i=$(expr $i + 1)
done

  y=$(expr $y + 1)

done

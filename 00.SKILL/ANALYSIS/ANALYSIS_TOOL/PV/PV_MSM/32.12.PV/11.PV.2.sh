yyyy1=2021;yyyy2=2021; y=$yyyy1

mm1=06; dd1=01
#mm2=06; dd2=01
mm2=08; dd2=31

PY=12.PV_ISOBARIC_MSM.PY
if [ ! -f $PY ];then echo EEEEE NO SUCH FILE, $PY;exit 1;fi

INROOT=/work02/DATA/MSM.FLOAT/SPLIT_HOUR/MSM-P
if [ ! -d $INROOT ];then echo EEEEE NO SUCH DIR, $INROOT;exit 1;fi

while [ $y -le $yyyy2 ]; do

INDIR=$INROOT/${y}/
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR, $INDIR;exit 1;fi

ODIR=/work2/DATA/MSM.NC/MSM.PV/SPLIT_HOUR/MSM-P/${y}/
mkdir -vp $ODIR
FIGDIR=FIG_$(basename $PY .PY)/${y}
mkdir -vp $FIGDIR

start=${y}/${mm1}/${dd1}; end=${y}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${y}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  Y=${date_out:0:4}; MM=${date_out:4:2}; DD=${date_out:6:2}

H=0
while [ $H -le 21 ]; do
HH=$(printf %02d $H)
  echo MMMMM $Y $MM $DD $HH
  IN=$INDIR/MSM-P_SPLIT_${Y}${MM}${DD}${HH}.nc
  if [ ! -f $IN ];then echo EEEEE NO SUCH FILE, $IN;exit 1;fi
  python3 $PY --year $Y --month $MM --day $DD --hour $HH \
  --input_dir $INDIR --output_dir $ODIR --fig_dir $FIGDIR
  echo
H=$(expr $H + 3)
done #H
  i=$(expr $i + 1)
done #i

  y=$(expr $y + 1)
done #y

#!/bin/bash

PREFIX=$1
PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R1DAAV.ge.50PCNT}
#PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R1DAAV.le.50PCNT}
DTYPE=P # P or S

INFLE=02.MAKE_INFLE_${PREFIX}_${DTYPE}.TXT
if [ ! -f $INFLE ];then echo EEEEE NO SUCH FILE,$INFLE;exit 1;fi
echo MMMMM IN $INFLE; echo

NCL=14.QVEC_AND_CNV_MSM.ncl
if [ ! -f $NCL ];then echo EEEEE NO SUCH FILE, $NCL;exit 1;fi

ODIR=/work02/DATA/MSM.NC/Q-VECTOR/${PREFIX}
mkdir -vp $ODIR
FIGDIR=FIG_$(basename $0 .sh)
mkdir -vp $FIGDIR

while read BUF ; do
ary=(`echo $BUF`)   # �z��Ɋi�[
Y=${ary[0]}
MM=${ary[1]}
DD=${ary[2]}

echo "DEBUG Y=$Y MM=$MM DD=$DD"  # �ǉ�

./runncl.sh $NCL $Y $MM $DD $PREFIX

done < 02.TEST.TXT

#!/bin/bash

# /work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.12.COMPOSITE_Z
# 
PREFIX=$1
#PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R1DAAV.ge.50PCNT}
PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R1DAAV.le.50PCNT}
DTYPE=P # P or S

INFLE=02.MAKE_INFLE_${PREFIX}_${DTYPE}.TXT
if [ ! -f $INFLE ];then echo EEEEE NO SUCH FILE,$INFLE;exit 1;fi
echo MMMMM IN $INFLE; echo

NCL=14.QVEC_AND_CNV_MSM.ncl
if [ ! -f $NCL ];then echo EEEEE NO SUCH FILE, $NCL;exit 1;fi

ODIR=/work02/DATA/MSM.NC/Q-VECTOR/${PREFIX}
mkdir -vp $ODIR
FIGDIR=FIG_$(basename $0 .sh)
mkdir -vp $FIGDIR

FILES=()
while read BUF ; do
ary=(`echo $BUF`)   # �z��Ɋi�[
INDIR=${ary[0]}
Y=${ary[1]}
MM=${ary[2]}
DD=${ary[3]}

runncl.sh $NCL "$Y" "$MM" "$DD" "$INDIR" "$ODIR" "$FIGDIR"

done < "$INFLE"


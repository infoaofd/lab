!****************************************************************
!  geostorophic velocity
!   jy: number of stations
!   km: number of vertical layers
!   standard depth--->std in metre
!
!******************************************************************
      parameter(jy=6)
      parameter(km=1000)
      parameter(grav=980.0,pi=3.1415926536)
      parameter(rmiss=-99.99)
      parameter(cm2m=100.0)
      dimension t(jy,km),s(jy,km)
      dimension den(jy,km),std(km) !std(40)
      dimension alp(jy,km),dd(jy,km)
      dimension kbtm(jy)
      dimension v(jy,km)
      real*8 P8,T8,S8,RHO8
!
      open(1,file='T.txt',action="read")
      open(2,file='S.txt',action="read")

      open(21,file='density_mks.dat')
      open(22,file='density_plot_gmt.dat')
      open(23,file='specific_volume.dat')
      open(24,file='dyamic_depth.dat')


      open(31,file='current.dat')
      open(32,file='current_plot_gmt.dat')


      open(99,file='check.txt')


! Read T and S
      do k=1,km
        read(1,*) std(k), (t(j,k),j=1,jy)
        read(2,*) std(k), (s(j,k),j=1,jy)
        write(*,'(f7.1,12f7.2)') std(k), (t(j,k),s(j,k),j=1,jy)
      enddo !k
      print *,"Done reading."
!
      do j=1,jy
        do k=1,km
          if(t(j,k) .gt. 0.0 .and. s(j,k) .gt. 0.0)then
            P8=dble(std(k))*0.1d0
            T8=dble(t(j,k))
            S8=dble(s(j,k))
            call eqst2(P8,T8,S8,RHO8)
            den(j,k)=sngl(RHO8)
          else
            den(j,k)=rmiss
          endif
        enddo !k
      enddo !j


      write(*,*)'# density (kg/m3)'
      write(21,*)'# density(kg/m3)'
      do k=1,km
        write(*,'(f7.1,10f12.5)') std(k), (den(j,k),j=1,jy)
        write(21,'(f7.1,10f12.5)') std(k), (den(j,k),j=1,jy)
      enddo !k
      print *,"Done density."

                       
! convert unit of density into g/cm3
      do j=1,jy
        do k=1,km
          if(den(j,k) .ne. rmiss)then
            den(j,k)=den(j,k)/1000.
          endif
        enddo !k
      enddo !j

      do j=1,jy
        kbtm(j)=km
        do k=1,km
          if(den(j,k) .eq. rmiss)then
!            kbtm(j)=k
            kbtm(j)=k-1
            goto 100
          endif
        enddo
100     continue
      enddo !j

      do j=1,jy
        do k=1,km
          if(den(j,k) .ne. rmiss)then
            alp(j,k)=1./den(j,k)
          else
            alp(j,k)=rmiss
          endif
        enddo !k
      enddo !j

      do k=1,km
        std(k)=std(k)*100.
      enddo !k

      write(*,*)'Alpha'
      do k=1,km
        write(*,'(f9.1,5f12.5)')std(k), (alp(j,k),j=1,3) !jy)
      enddo !k
!

      do j=1,jy
        do k=1,km
          dd(j,k)=0.0
          v(j,k)=rmiss
        enddo !k

        dd(j,kbtm(j))=0. !am

        do  k=kbtm(j)-1,1,-1  !am
          dd(j,k)=dd(j,k+1)+(alp(j,k+1)+alp(j,k))/2.*(std(k+1)-std(k))
        enddo !k
      enddo !j

      do k=1,km
        write(23,'(f9.1,10f12.5)')std(k)/cm2m,(alp(j,k),j=1,jy)
        write(24,'(f9.1,10f12.5)')std(k)/cm2m,(dd(j,k),j=1,jy)
      enddo !k
      print *,"Done computing dynamic depth."

!
      fac=1.8486*60. ! For lat -> km

      do j=1,jy-1
        dis=40.0 !km
!        flatm=0.5*(rlat1+rlat2)*pi/180.
        flatm=33.0*pi/180.
        cor=2.*2.*pi/24./3600.*sin(flatm)

!        print *,'rlat1=',rlat1
!        print *,'rlat2',rlat2
        print *,'cor=',cor
!        print *,'fac=',fac
        print *,'dis=',dis

        dis=dis*1000.0*100.0 !cm

        do k=1,kbtm(j) !am
          v(j,k)=(dd(j+1,k)-dd(j,k))/cor/dis*grav
          print *,dd(j+1,k),dd(j,k),v(j,k)
        enddo !k

      enddo !j

      print *,"Done geostrophic velocity."
!
      do j=1,jy-1
        do k=1,kbtm(j) !km
          v(j,k)=v(j,k)-v(j,kbtm(j)) !am v(j,km)-v(j,k)
        enddo !k
      enddo !j
!
      write(31,'(A)')'# Geostrphic velocity'
      write(31,'(A)')'# depth(m), velocity(m/s)'
      do k=1,km
        write(31,'(f8.1,100e14.6)') std(k)/cm2m,(v(j,k)/cm2m,j=1,jy-1)
      enddo !k

! Print density and geostrophic velocity field for plotting
      do j=1,jy
        x=dis/1000./100.*float(j-1)

        do k=1,km
          if(den(j,k).ne.rmiss)then
            write(22,'(f10.1,f7.1,10f12.5)') x, std(k)/cm2m, 
     &     (den(j,k)-1.0)*1000.
          else
            write(22,'(f10.1,f7.1,10f12.5)') x, std(k)/cm2m, 
     &      rmiss
          endif
        enddo !k
      enddo !j
      close(22)

      do j=1,jy-1
        x=dis/1000./100.*(float(j-1)+0.5)
        do k=1,km
           if(v(j,k).ne.rmiss)then
            write(32,'(f10.1,f7.1,10f12.4)') x, std(k)/cm2m, v(j,k)/cm2m
          else
            write(32,'(f10.1,f7.1,10f12.4)') x, std(k)/cm2m, rmiss
          endif

        enddo !k
      enddo !j
      close(32)


!
      END


!***********************************************************************
! eqst2.f
! TASK
!j 状態方程式(IES80)により海水の密度を計算する。
! REMARK
!
!j All arguments must be DOUBLE PRECISION.
!
!j 入力データは,P [bar], T[deg.], S [psu]
!  圧力の単位がbarであることに注意! (1.0bar = 0.1*dbar)
!
! [USAGE]
!
!      Pressure=0.1*z
!      call eqst2(pressure,t,s,dens)
!
!      where z is depth in meters, t is temperature, s is salinity, and
!      dens is density in kg/m3.
!
!
!j 適用範囲
!     -2 < T < 40 [deg.]
!      0 < S < 42 [psu]
!      0 < P < 1000 [bar] = 10000 [dbar]
!
!j 水深[m]＝圧力[dbar]という関係を仮定している。これによる両者の誤差は
!j 1%以内(海洋観測指針)。
!    It assumes  no  pressure  variation along geopotential surfaces,
!  that  is,  depth  and  pressure  are interchangeable.
!
! REFERENCE
!j  気象庁編():海洋観測指針,428pp.
!   UNESCO (1981): Background Papers and Supporting Data on the 
!      International Equation of State of Seawater. Tech. Pap. Mar. 
!      Sci., 38, 192pp.
!***********************************************************************
     subroutine eqst2(P,T,S,RHO)
!------------------------- PARAMETERS ----------------------------------
      implicit none
      double precision b0,b1,b2,b3,b4, c0,c1,c2, d0
      double precision a0,a1,a2,a3,a4,a5
      double precision f0,f1,f2,f3, g0,g1,g2
      double precision i0,i1,i2, j0
      double precision m0,m1,m2
      double precision e0,e1,e2,e3,e4
      double precision h0,h1,h2,h3
      double precision k0,k1,k2

      parameter(b0= 8.24493d-1, c0=-5.72466d-3
     &         ,b1=-4.0899d-3 , c1= 1.0227d-4
     &         ,b2= 7.6438d-5 , c2=-1.6546d-6
     &         ,b3=-8.2467d-7
     &         ,b4= 5.3875d-9 , d0= 4.8314d-4)

      parameter(a0= 999.842594, a1= 6.793952d-2
     &         ,a2=-9.095290d-3,a3= 1.001685d-4
     &         ,a4=-1.120083d-6,a5= 6.536332d-9)

      parameter(f0= 54.6746    ,g0= 7.944d-2
     &         ,f1= -0.603459  ,g1= 1.6483d-2
     &         ,f2=  1.09987d-2,g2=-5.3009d-4
     &         ,f3= -6.1670d-5)

      parameter(i0= 2.2838d-3  ,j0= 1.91075d-4
     &         ,i1=-1.0981d-5
     &         ,i2=-1.6078d-6)

      parameter(m0=-9.9348d-7, m1= 2.0816d-8
     &         ,m2= 9.1697d-10)

      parameter(e0= 19652.21, e1= 148.4206
     &         ,e2=-2.327105, e3= 1.360477d-2
     &         ,e4=-5.155288e-5)

      parameter(h0= 3.239908,   h1= 1.43713d-3
     &         ,h2= 1.16092d-4, h3=-5.77905d-7)

      parameter(k0= 8.50935d-5, k1=-6.12293d-6
     &         ,k2= 5.2787d-8)

!-----------------------------------------------------------------------
!--------------------------- ARGUMENTS ---------------------------------
      double precision P,T,S,RHO
!------------------------- LOCAL VARIABLES -----------------------------
      integer k
      double precision rhow,rhop0,Kw,Aw,Bw,Kp0,A,B,Kp
!-----------------------------------------------------------------------
!j   以下の式番号はすべて海洋観測指針(p.92〜93)に記載されている式番号に
!j 対応する。


! Eq.(4)
        rhow=a0 + a1*t + a2*t**2 + a3*t**3 + a4*t**4 
     &       + a5*t**5
! Eq.(3)
        rhop0=rhow + (b0 + b1*t + b2*t**2 + b3*t**3 
     &             +  b4*t**4)*s
     &             + (c0 + c1*t + c2*t**2)*s**(1.5)
     &             + d0*s**2
! Eq.(9)
        Kw=e0 + e1*t + e2*t**2 + e3*t**3 + e4*t**4
! Eq.(10)
        Aw=h0 + h1*t + h2*t**2 + h3*t**3
! Eq.(11)
        Bw=k0 + k1*t + k2*t**2
! Eq.(6)
        Kp0=Kw + (f0 + f1*t + f2*t**2 + f3*t**3)*s
     &         + (g0 + g1*t + g2*t**2)*S**(1.5)
! Eq.(7)
        A=Aw + (i0 + i1*t + i2*t**2)*s + j0*S**(1.5)
! Eq.(8)
        B=Bw + (m0 + m1*t + m2*t**2)*s
! Eq.(5)
        Kp=Kp0 + A*p + B*p**2
! Eq.(1)
        rho=rhop0/(1.0d0 - p/Kp)

      return
      end


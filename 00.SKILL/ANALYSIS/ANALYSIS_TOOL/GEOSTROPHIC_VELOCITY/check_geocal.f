****************************************************************
*  geostorophic cal
*   number of station--->jy
*   standard depth--->std in metre
*
******************************************************************
      parameter(jy=2)
      parameter(km=12)
      parameter(grav=980.0,pi=3.1415926536)
      parameter(rmiss=-99.999)
      parameter(cm2m=100.0)
      dimension t(jy,km),s(jy,km)
      dimension den(jy,km),std(km) !std(40)
      dimension alp(jy,km),dd(jy,km)
      dimension v(jy,km)
      dimension kbtm(jy)
      real*8 P8,T8,S8,RHO8
c
      open(1,file='SampleTS.txt',action="read")
      open(2,file='Sample_current.dat')
      open(99,file='check.txt')


c
c ------> read density in g/cm3

c      read(1,70) ((den(j,k),j=1,jy),k=1,km)
      read(1,*)
      read(1,*) rlat1, rlon1
      read(1,*)
      read(1,*) rlat2, rlon2
      read(1,*)
      read(1,*)

      do k=1,km
        read(1,*) std(k), t(1,k),s(1,k),t(2,k),s(2,k)
        write(*,'(f7.1,12f7.2)') std(k), (t(j,k),s(j,k),j=1,jy)
      enddo !k
      print *,"Done reading."

!
      do j=1,jy
        do k=1,km
          P8=dble(std(k))*0.1d0
          T8=dble(t(j,k))
          S8=dble(s(j,k))
          call eqst2(P8,T8,S8,RHO8)
          den(j,k)=sngl(RHO8)
        enddo !k
      enddo !j
                       
! convert unit of density into g/cm3
      do j=1,jy
        do k=1,km
          if(t(j,k) .lt. 0.0)then
            den(j,k)=rmiss
          else
            den(j,k)=den(j,k)/1000.
          endif
        enddo !k
      enddo !j

      write(*,*)
      do k=1,km
        write(*,'(f7.1,5f12.5)') std(k), (den(j,k),j=1,jy)
      enddo !k

c
c    dynamic cal.
c
      do j=1,jy
        do k=1,km
          alp(j,k)=1./den(j,k)
        enddo !k
      enddo !j

      do k=1,km
        std(k)=std(k)*100.
      enddo !k

      write(*,*)'Alpha'
      do k=1,km
        write(*,'(f9.1,5f12.5)')std(k), (alp(j,k),j=1,jy)
      enddo !k
c
      do j=1,jy
        dd(j,km)=0. !am

        do  k=km-1,1,-1  !am
!          dd(j,k)=dd(j,k-1)+(alp(j,k-1)+alp(j,k))/2.*(std(k)-std(k-1))
          dd(j,k)=dd(j,k+1)+(alp(j,k+1)+alp(j,k))/2.*(std(k+1)-std(k))
        enddo !k
      enddo !j

      print *,"Done computing dynamic depth."

c
      fac=1.8486*60.

      do j=1,jy-1
!       fac=1.8486*60.
        dis=(rlat2-rlat1)*fac
        flatm=0.5*(rlat1+rlat2)*pi/180.
        cor=2.*2.*pi/24./3600.*sin(flatm)

        print *,'rlat1=',rlat1
        print *,'rlat2',rlat2
        print *,'cor=',cor
        print *,'fac=',fac
        print *,'dis=',dis

        dis=dis*1000.0*100.0 !cm

        do k=1,km
          v(j,k)=0.0
        enddo !k

        do k=1,km !19
          v(j,k)=(dd(j+1,k)-dd(j,k))/cor/dis*grav
          print *,dd(j+1,k),dd(j,k),v(j,k)
        enddo 

      enddo

      print *,"Done computing geostrophic velocity."
c
c------> We set 'v(j,19)...the lowest layer' as no-motion
      do 85 j=1,jy-1
      do 85 k=1,km
        v(j,k)=v(j,k) !am v(j,km)-v(j,k)
   85 continue
c

      write(2,'(A)')'# depth(m), velocity(m/s)'
      do k=1,km
        write(2,'(f8.1,100e14.6)') std(k)/cm2m,(v(j,k)/cm2m,j=1,jy-1)
      enddo !k
c
      END


************************************************************************
* eqst2.f
* TASK
cj 状態方程式(IES80)により海水の密度を計算する。
* REMARK
c
cj All arguments must be DOUBLE PRECISION.
c
cj 入力データは,P [bar], T[deg.], S [psu]
c  圧力の単位がbarであることに注意! (1.0bar = 0.1*dbar)
C
C [USAGE]
C
C      Pressure=0.1*z
C      call eqst2(pressure,t,s,dens)
C
C      where z is depth in meters, t is temperature, s is salinity, and
C      dens is density in kg/m3.
c
c
cj 適用範囲
c     -2 < T < 40 [deg.]
c      0 < S < 42 [psu]
c      0 < P < 1000 [bar] = 10000 [dbar]
c
cj 水深[m]＝圧力[dbar]という関係を仮定している。これによる両者の誤差は
cj 1%以内(海洋観測指針)。
c    It assumes  no  pressure  variation along geopotential surfaces,
c  that  is,  depth  and  pressure  are interchangeable.
c
* REFERENCE
cj  気象庁編():海洋観測指針,428pp.
*   UNESCO (1981): Background Papers and Supporting Data on the 
*      International Equation of State of Seawater. Tech. Pap. Mar. 
*      Sci., 38, 192pp.
************************************************************************
      subroutine eqst2(P,T,S,RHO)
*------------------------- PARAMETERS ----------------------------------
      implicit none
      double precision b0,b1,b2,b3,b4, c0,c1,c2, d0
      double precision a0,a1,a2,a3,a4,a5
      double precision f0,f1,f2,f3, g0,g1,g2
      double precision i0,i1,i2, j0
      double precision m0,m1,m2
      double precision e0,e1,e2,e3,e4
      double precision h0,h1,h2,h3
      double precision k0,k1,k2

      parameter(b0= 8.24493d-1, c0=-5.72466d-3
     &         ,b1=-4.0899d-3 , c1= 1.0227d-4
     &         ,b2= 7.6438d-5 , c2=-1.6546d-6
     &         ,b3=-8.2467d-7
     &         ,b4= 5.3875d-9 , d0= 4.8314d-4)

      parameter(a0= 999.842594, a1= 6.793952d-2
     &         ,a2=-9.095290d-3,a3= 1.001685d-4
     &         ,a4=-1.120083d-6,a5= 6.536332d-9)

      parameter(f0= 54.6746    ,g0= 7.944d-2
     &         ,f1= -0.603459  ,g1= 1.6483d-2
     &         ,f2=  1.09987d-2,g2=-5.3009d-4
     &         ,f3= -6.1670d-5)

      parameter(i0= 2.2838d-3  ,j0= 1.91075d-4
     &         ,i1=-1.0981d-5
     &         ,i2=-1.6078d-6)

      parameter(m0=-9.9348d-7, m1= 2.0816d-8
     &         ,m2= 9.1697d-10)

      parameter(e0= 19652.21, e1= 148.4206
     &         ,e2=-2.327105, e3= 1.360477d-2
     &         ,e4=-5.155288e-5)

      parameter(h0= 3.239908,   h1= 1.43713d-3
     &         ,h2= 1.16092d-4, h3=-5.77905d-7)

      parameter(k0= 8.50935d-5, k1=-6.12293d-6
     &         ,k2= 5.2787d-8)

*-----------------------------------------------------------------------
*--------------------------- ARGUMENTS ---------------------------------
      double precision P,T,S,RHO
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
      integer k
      double precision rhow,rhop0,Kw,Aw,Bw,Kp0,A,B,Kp
*-----------------------------------------------------------------------
cj   以下の式番号はすべて海洋観測指針(p.92〜93)に記載されている式番号に
cj 対応する。


c Eq.(4)
        rhow=a0 + a1*t + a2*t**2 + a3*t**3 + a4*t**4 
     &       + a5*t**5
c Eq.(3)
        rhop0=rhow + (b0 + b1*t + b2*t**2 + b3*t**3 
     &             +  b4*t**4)*s
     &             + (c0 + c1*t + c2*t**2)*s**(1.5)
     &             + d0*s**2
c Eq.(9)
        Kw=e0 + e1*t + e2*t**2 + e3*t**3 + e4*t**4
c Eq.(10)
        Aw=h0 + h1*t + h2*t**2 + h3*t**3
c Eq.(11)
        Bw=k0 + k1*t + k2*t**2
c Eq.(6)
        Kp0=Kw + (f0 + f1*t + f2*t**2 + f3*t**3)*s
     &         + (g0 + g1*t + g2*t**2)*S**(1.5)
c Eq.(7)
        A=Aw + (i0 + i1*t + i2*t**2)*s + j0*S**(1.5)
c Eq.(8)
        B=Bw + (m0 + m1*t + m2*t**2)*s
c Eq.(5)
        Kp=Kp0 + A*p + B*p**2
c Eq.(1)
        rho=rhop0/(1.0d0 - p/Kp)

      return
      end


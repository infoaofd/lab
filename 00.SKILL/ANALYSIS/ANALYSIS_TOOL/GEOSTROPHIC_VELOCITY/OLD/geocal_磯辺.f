****************************************************************
*  geostorophic cal.*
*   number of station--->jy
*   We use standard depth density(0-1000m=1,19th layer)
*   standard depth--->std in metre
*
*流速の定義方向を、どっちにしていたか忘れた。要確認！
*
*つまり、Stn(n)とStn(n+1)のどっちを右に見る方向を正としているか、
*
結果を見て確認しておくれ。
******************************************************************
      parameter(jy=75)
      dimension den(jy,19),std(40)
      dimension alp(jy,19),dd(jy,19),v(jy,19)
c
      data std/0.,10.,20.,30.,50.,75.,100.,125.,
     *  150.,200., 250.,300.,400.,500.,600.,700.,800.,
     *  900.,1000.,1100.,1200.,1300.,1400.,1500.,1750.,2000.,
     * 2500.,3000.,3500.,4000.,4500.,5000.,5500.,6000.,6500.,
     * 7000.,7500.,8000.,8500.,9000./
c
      open(1,file='woa94/sigma.dat')
      open(2,file='woa94/current.dat')
c
c ------> read density in g/cm3
      read(1,70) ((den(j,k),j=1,jy),k=1,km)
c
c    dynamic cal.
c
      do 81 j=1,jy
      do 81 k=1,19
   81 alp(j,k)=1./den(j,k)
c
      do 82 j=1,jy
      dd(j,1)=0.
      do 82 k=2,19
      dd(j,k)=dd(j,k-1)+(alp(j,k-1)+alp(j,k))/2.*(std(k)-std(k-1))
   82 continue
c
      pai=3.14159
      do 83 j=1,jy-1
      do 83 k=1,19
      fac=1.8486*60.
c----> latitude at (j-station)
      theta1=15.+float(j)/3.
c----> latitude at (j+1-station)
      theta2=15.+float(j+1)/3.
c----> dis=distance between stations in Km( We now set 1/3 degree)
      dis=fac/3.
      flatm=0.5*(theta1+theta2)*pai/180.
      cor=2.*2.*pai/24./3600.*sin(flatm)
      v(j,k)=(dd(j+1,k)-dd(j,k))/cor/dis
   83 continue
c
c------> We set 'v(j,19)...the lowest layer' as no-motion
      do 85 j=1,jy-1
      do 85 k=1,19
      v(j,k)=v(j,19)-v(j,k)
   85 continue
c
      write(2,110) ((v(j,k),k=1,19),j=1,jy)
  110 format(7e10.3)
c
      END

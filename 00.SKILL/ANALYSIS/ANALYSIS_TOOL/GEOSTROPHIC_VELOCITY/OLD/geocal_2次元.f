***********************************************************************
* geocal.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
* TASK
* METHOD
* SLAVE SUBROUTINE
* REMARK
*
*
*            |
*            |         V
*            |        ^
*            |        |
*            |
*           -+-+------+-----+-----+ J
*  We set the velocity of the 1st layer of input data(ADCP measured U
* and V) as reference velocity.
*
* REFERENCE
*  �C�m�ϑ��w�j(1990):�C�ے�,428pp.
***********************************************************************
      SUBROUTINE geocal
*-------------------------- PARAMETERS --------------------------------
      PARAMETER(PI=3.14159265358979)
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
      include 'param.h'
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
*----------------------------------------------------------------------
*
* CALCULATE SPECIFIC VOLUME, ALP
*
      do 100 k=1,ninpf
        do 100 j=1,nnout
          do 100 i=1,mmout
            rho = rhom(tmpout(i,j,k),salout(i,j,k))
            rho = rho*1.0e-3      ! kg/m3 -> g/cm3
            if(tmpout(i,j,k).ne.RMISS.and.salout(i,j,k).ne.RMISS)then
              alp(i,j,k) = 1.0/rho
            else
              rho = RMISS
              alp(i,j,k) = RMISS
            end if
*debug
*      if(j.eq.64)then
*        write(99,*)i,j,k,tmpout(i,j,k),salout(i,j,k),rho
*      end if
*debug end

100   continue

*
* CALCULATE DYNAMIC DEPTH, dd
*
      do 200 i=1,mmout
        do 200 j=1,nnout
          dd(i,j,1)=0.0
          do 210 k=2,ninpf
            dd(i,j,k)
     +   =  dd(i,j,k-1)+(alp(i,j,k-1)+alp(i,j,k))/2.0
     +     *(inpdep(k)-inpdep(k-1))
  210     continue
  200 continue

*
* Geostrophic Calculation
*
      do 300 k=2,NINPF
        do 310 j=1,nnout-1
          do 320 i=1,mmout-1
            theta1=RLATX(i)
            theta2=RLATX(i+1)
            flatm=0.5*(theta1+theta2)*PI/180.
            cor=2.*2.*pi/24./3600.*sin(flatm)
            if(alp(i,j,k).ne.RMISS.and.alp(i,j,k).ne.RMISS)then
              uout(i,j,k)=-(dd(i,j+1,k)-dd(i,j,k))/hyp/cor
              vout(i,j,k)= (dd(i+1,j,k)-dd(i,j,k))/hxp/cor
            else
              uout(i,j,k)=RMISS
              vout(i,j,k)=RMISS
            end if
            uout(i,ibmg+nnp,k)=uout(i,ibmg+nnp-1,k)
  320     continue
          vout(ilmg+mmp,j,k)=vout(ilmg+mmp-1,j,k)
  310   continue
  300 continue

*  We set the velocity of the 1st layer of input data(ADCP measured U
* and V) as reference velocity.
      do 85 i=1,mmout
      do 85 j=1,nnout

      do 85 k=2,NINPF
        if(alp(i,j,k).ne.RMISS)then
          uout(i,j,k)=uout(i,j,1)+uout(i,j,k)
          vout(i,j,k)=vout(i,j,1)+vout(i,j,k)
        else
          uout(i,j,k)=RMISS
          vout(i,j,k)=RMISS
        end if
   85 continue

      RETURN
      END

#!/bin/sh

gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 20
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 20
gmtset TICK_PEN 4
gmtset BASEMAP_TYPE PLAIN

IN=current_plot_gmt.dat
OUT=$(basename $IN .dat).ps
CPT=cpt.txt

echo
echo 'INPUT FILE NAME: ' $IN
echo 'OUTPUT FILE NAME: ' $OUT
echo

range=0/200/0/1000
reso=20/1
size=4/-5

awk '{if ($3 != "-99.9900" )print $1,$2,$3}' $IN | \
triangulate  \
 -R$range -I$reso -Gtmp.grd > /dev/null

makecpt -Cno_green -T-1.5/1.5/0.02 > $CPT

#awk '{if ($3 != "-99.9900" )print $1,$2,$3}' $IN | \
#psmask  -R$range -I$reso -JX${size}  \
#-X1.5 -Y3 \
#-P -K > $OUT

grdimage tmp.grd -R -JX$size \
 -C$CPT \
-X1.5 -Y3 \
-P -K > $OUT

# -O -K >> $OUT

grdcontour tmp.grd -R -JX \
 -A.2f12  -C.1 -W5 -G1/2  \
 -O -K >> $OUT

#psmask -C -O -K >> $OUT

psbasemap -R$range -JX$size \
 -Ba40f40:"Distance [km]":/a200f100:"depth [m]"::."":WsNe \
-O -K >> $OUT

psscale -D2/-.8/4/0.1h  -C$CPT -Ba.5f.1:"m/s": \
-O >> $OUT


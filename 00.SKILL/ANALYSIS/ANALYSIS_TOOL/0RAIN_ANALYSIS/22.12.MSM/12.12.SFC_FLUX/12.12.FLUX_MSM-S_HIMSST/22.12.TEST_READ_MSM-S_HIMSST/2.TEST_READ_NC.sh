#!/bin/bash
#
Y=$1;MM=$2;DD=$3
Y=${Y:-2024};MM=${MM:-07};DD=${DD:-25}
YMD=${Y}${MM}${DD}

INDIR1=/work02/DATA/MSM.NC/MSM-S/${Y}/
if [ ! -d $INDIR1 ];then echo NO SUCH DIR, $INDIR1; exit 1;fi
INFLE1=${MM}${DD}.nc
IN1=${INDIR1}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi

INDIR2=/work02/DATA/SST/HIMSST/NC_MSM-S_GRID/${Y}/
if [ ! -d $INDIR2 ];then echo NO SUCH DIR, $INDIR2; exit 1;fi
INFLE2=HIMSST_D${YMD}_MSM-S_G.nc
IN2=$INDIR2/$INFLE2
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

ODIR=/work02/DATA/MSM.NC/SFC_FLUX/${Y}/
OFLE=MSM_HIMSST_SFC_FLUX_COARE_${YMD}.nc

NML=$(basename $0 .sh)_NML.TXT
cat <<EOF >$NML
&PARA
INDIR1="$INDIR1",
INFLE1="$INFLE1",
INDIR2="$INDIR2",
INFLE2="$INFLE2",
ODIR="$ODIR",
OFLE="$OFLE",
&END
EOF

src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

#f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
#OPT=" -convert big_endian -assume byterecl"
#LOPT= #"-I/usr/local/stpk-0.9.20.0/include -L/usr/local/stpk-0.9.20.0/lib -lstpk"

export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:${LD_LIBRARY_PATH}:.
f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
LOPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

echo MMMMM Compiling ${src} ...
echo ${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEEE COMPILE ERROR!!!"; echo; echo TERMINATED.
exit 1
fi
rm -vf *.o

echo MMMMM LINKING ...
OBJ=$(basename $src .F90).o
echo ${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEE LINK ERROR!!!"; echo; echo TERMINATED.
exit 1
fi
echo MMMMM DONE COMPILE.

echo MMMMM ${exe} is running ...; echo
D1=$(date -R)

${exe} < ${NML}
if [ $? -ne 0 ]; then
echo; echo "EEEEE RUNTIME ERROR!!!"; echo; echo TERMINATED.
exit 1
fi
echo; echo "MMMMM Done ${exe}"; echo
cp -av ${src} $(basename $0) $ODIR
OUT=${ODIR}/${OFLE}
if [ -f $OUT ];then echo MMMMM OUT:$OUT;fi

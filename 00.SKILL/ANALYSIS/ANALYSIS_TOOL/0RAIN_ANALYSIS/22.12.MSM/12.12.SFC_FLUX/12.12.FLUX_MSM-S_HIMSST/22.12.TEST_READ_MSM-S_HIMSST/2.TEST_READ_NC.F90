PROGRAM TEST_READ

! netCDFライブラリで使用する変数や定数の設定ファイルを読み込む
use netCDF ! netCDF4
! include 'netcdf.inc' !netCDF3

character(len=500)::INDIR1, INFLE1, INDIR2, INFLE2, ODIR, OFLE
character(len=1000)::IN1, IN2, OUT
character(len=50)::varname

integer,parameter::IM=481, JM=505, NT=24

real,DIMENSION(IM,JM,NT)::rh,u,v,temp,sp
real::lon(IM), lat(JM), time(NT)
CHARACTER(LEN=100):: long_name,units

real,DIMENSION(IM,JM)::SSTC,SSTK

NAMELIST /PARA/ INDIR1, INFLE1, INDIR2, INFLE2, ODIR, OFLE

READ(5,NML=PARA)

PRINT '(A)','MMMMM READ MSM'

IN1=trim(INDIR1)//trim(INFLE1)
print '(a,a)','MMMMM INPUT: ',trim(IN1)

CALL READ_MSM_S_3D(IN1,'rh',IM,JM,NT,rh)
CALL READ_MSM_S_3D(IN1,'u' ,IM,JM,NT,u)
CALL READ_MSM_S_3D(IN1,'v' ,IM,JM,NT,v)
CALL READ_MSM_S_3D(IN1,'sp' ,IM,JM,NT,sp)
CALL READ_MSM_S_3D(IN1,'temp' ,IM,JM,NT,temp)

CALL READ_MSM_S_1D(IN1,'lon',IM,lon,long_name,units)
CALL READ_MSM_S_1D(IN1,'lat',JM,lat,long_name,units)
CALL READ_MSM_S_1D(IN1,'time',NT,time,long_name,units)
PRINT *

PRINT '(A)','MMMMM READ HIMSST'
IN2=trim(INDIR2)//trim(INFLE2)
print '(a,a)','MMMMM INPUT: ',trim(IN2)
CALL READ_HIMSST_2D(IN2,'SST' ,IM,JM, SSTC)

END PROGRAM TEST_READ


SUBROUTINE READ_HIMSST_2D(IN,varname,IM,JM,var)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::IM,JM
REAL,DIMENSION(IM,JM),INTENT(INOUT)::var
integer stat, ncid, varid, varid1
real::scale, offset

ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, var)
stat=NF90_CLOSE(ncid)!入力ファイル閉じる
print *
END SUBROUTINE READ_HIMSST_2D


SUBROUTINE READ_MSM_S_1D(IN,varname,NDIM,var,long_name,units)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::NDIM
REAL,DIMENSION(NDIM),INTENT(INOUT)::var
CHARACTER(LEN=*),INTENT(INOUT):: long_name,units

integer stat, ncid, varid, varid1
real::scale, offset
ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く
!print *,'nf90 open stat=',stat
!print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, var)
stat = nf90_get_att(ncid,varid,'long_name',long_name)
stat = nf90_get_att(ncid,varid,'units',units)
print '(a,i5,1x,a,1x,a)',trim(varname),varid,TRIM(long_name),TRIM(units)

END SUBROUTINE READ_MSM_S_1D


SUBROUTINE READ_MSM_S_3D(IN,varname,IM,JM,NT,var)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::IM,JM,NT
REAL,DIMENSION(IM,JM,NT),INTENT(INOUT)::var
integer stat, ncid, varid, varid1
real::scale, offset
integer(2)::varin(IM,JM,NT) ! short型の2バイト整数

ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く
!print *,'nf90 open stat=',stat
!print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
!print *,'nf90_get_att stat=',stat
stat = nf90_get_att(ncid,varid,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
print '(a,f10.5,a,g11.4)','scale_factor=',scale,' offset=',offset

var(:,:,:)=float(varin(:,:,:))*scale+offset

stat=NF90_CLOSE(ncid)!入力ファイル閉じる

print *
END SUBROUTINE READ_MSM_S_3D

#!/bin/bash
# Sun, 28 Jul 2024 15:41:36 +0900
# /work09/ma/16.TOOL/DATA_HANDLING/SST/HIMSST/HIMSST_TXT2NETCDF

function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

YMDHM1=$1; YMDHM1=${YMDHM1:-202407250000}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}


TIME=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}
TEXT="LHF ${HH1}:${MI1}UTC${DD1}${MMM1}${Y1}"
#CTL=$(basename $0 .sh).CTL
#if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

INDIR=/work02/DATA/MSM.NC/SFC_FLUX_KONDO/${Y1}/
INFLE=MSM_HIMSST_SFC_FLUX_KONDO_${Y1}${MM1}${DD1}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${Y1}${MM1}${DD1}_${HH1}${MI1}.PDF

# LONW= ;LONE= ; LATS= ;LATN=
# LEV=

#LEVS="0 500 50"
LEVS=" -levs 10 50 100 150 200 250 300 350 400 450 500"
KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->magenta'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=1
UNIT=W/m2

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set mpdset hires'
# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
#'set time ${TIME}'

'd LHF'
'set xlab off';'set ylab off'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
 x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

SEA=DJF
YS=1949; YE=2022
YSI=1866
INDIR=/work01/DATA/INDEX/ENSO
INFLE=SOI_0.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1; fi

ODIR=$INDIR
OFLE=SOI_DJF_${YS}-${YE}.nc
OUT=$ODIR/$OFLE

TMP=${ODIR}/$(basename $0 .sh)_TMP.nc
TSS=$(expr \( ${YS} - ${YSI} - 1 \) \* 12 + 1 )
TSE=$(expr \( ${YE} - ${YSI} \) \* 12 + 1 )
rm -vf $TMP
cdo seltimestep,${TSS}/${TSE} $IN $TMP

echo MMMMM SEASONAL MEAN $SEA
rm -vf $OUT
cdo timselmean,3,11,9 $TMP $OUT

<<COMMENT
cdo timselmean,nsets[,noffset[,nskip]] ifile ofile
COMMENT

#cdo -seasmean -selseas,DJF $IN $TMP
if [ ! -f $OUT ];then echo ERROR seasmean;exit 1; fi
echo

if [ -f $TMP ];then echo TMP: $TMP;fi 
if [ -f $OUT ];then echo OUT: $OUT;fi 

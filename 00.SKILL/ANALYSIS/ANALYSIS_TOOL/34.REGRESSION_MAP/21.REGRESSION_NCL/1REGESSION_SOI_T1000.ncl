; 
; 1REGESSION_SOI_T1000.ncl
; 
;
script_name  = get_script_name()
script=systemfunc("basename "+script_name+ " .ncl")
arg    = getenv("NCL_ARG_2")

INDIR1="/work01/DATA/INDEX/ENSO/"
INFLE1="SOI_DJF_1949-2022.nc"
IN1=INDIR1+INFLE1

INDIR2="/work01/DATA/NCEP1/MON/"
INFLE2="air.mon.mean_1000_DJF_ANO.nc"
IN2=INDIR2+INFLE2

OUT=script
TYP="PDF"
FIG=OUT+"."+TYP
a1=addfile(IN1,"r")
a2=addfile(IN2,"r")

print("MMMMM READ x")
x=a1->SOI_SIGNAL
x=-x
; Prolonged periods of negative SOI values coincide with 
; abnormally warm ocean waters across the eastern tropical 
; Pacific typical of El Nino episodes. 

print("MMMMM STANDARDIZE THE VALUES OF THE TIME DIMENSION AT ALL LAT/LON INDICES.")
xstan = dim_standardize_n_Wrap(x, 0, 0)
printVarSummary(xstan)

print("MMMMM READ y")
yin=a2->air
lat=a2->lat
lon=a2->lon
y=yin(lat|:,lon|:,time|:)

print("MMMMM REGRESSION COEFF xstan vs y")
rc    = regCoef(xstan,y)
rc!0="lat"
rc!1="lon"
rc&lat=lat
rc&lon=lon
rc&lat@units="degrees_north"
rc&lon@units="degrees_east"
printVarSummary(rc)


print("MMMMM PLOT")
  wks  = gsn_open_wks(TYP, OUT)        ; send graphics to PNG file
   
   res                       = True     
   res@gsnMaximize           = True             ; make large

   res@cnFillOn              = True             ; turn on color
   res@cnFillPalette         = "ViBlGrWhYeOrRe" ; set color map
   res@cnLinesOn             = False            ; turn off contour lines
   res@cnLineLabelsOn        = False            ; turn off contour line labels
 ;;res@cnFillMode            = "RasterFill"

   res@cnLevelSelectionMode  = "ManualLevels"   ; set manual contour levels
   res@cnMinLevelValF        =  -1.00           ; set min contour level
   res@cnMaxLevelValF        =   1.00           ; set max contour level
   res@cnLevelSpacingF       =   0.10           ; set contour interval

   res@mpFillOn              = False            ; turn off default background gray
   res@mpCenterLonF          = 180
   
   res@tiMainString          = ""
   plot = gsn_csm_contour_map_ce(wks,rc,res)    

print("")
print("MMMMM Done " + script_name)
print("MMMMM IN1: " + IN1)
print("MMMMM IN2: " + IN2)
print("MMMMM FIG: " + FIG)
print("")

! /work02/work01/110/work2/am/WRF.PRE/other.preprocessors/smooth_jcope2_twc_v0.2/src/srcbackup/20120227-154440
subroutine smooth_box(zi,nw,nt,im,jm,zo)
! Description:
!
! Author: am
!
! Host: aofd30
! Directory: /work2/am/12.Work11/44.SST_Smooth/12.Smooth_for_Run_S1/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 13:53 on 10-04-2011.

!  use
!  implicit none
  integer,intent(in)::im,jm
  real(8),intent(in)::zi(im,jm)
  integer,intent(in)::nw,nt
  real,intent(out)::zo(im,jm)
  real :: ztmp(im,jm)

  write(*,'(a)')'Subroutine: smooth_box'
!  write(*,*)''

!  do n=1,nm

  n=nw/2 * 2 + 1

  nh=n/2

  print *,"nw= ",nw
  print *,"nt= ",nt
!  print *,nh
!  print *,jm-nh
!  print *,im-nh
!  print *,jm
!  print *,im

  do j=1,jm
    do i=1,im
      zo(i,j)=sngl(zi(i,j))
    enddo !i
  enddo !j



  do j=1,jm
    do i=1,im
      ztmp(i,j)=sngl(zi(i,j))
    enddo !i
  enddo !j

  do n=1,nt

    do j=nh+1,jm-nh
      do i=nh+1,im-nh

        tmp=0.0
        nn=0
        do jj=-nh,nh
          do ii=-nh,nh

            if(ztmp(i+ii,j+jj) > -999.0)then
              nn=nn+1
              tmp=tmp+ztmp(i+ii,j+jj)

            endif

          enddo !ii
        enddo !jj

       w=1.0/(float(nn))
       zo(i,j)=tmp*w

       enddo !i
    enddo !j

    do j=1,jm
      do i=1,im
        ztmp(i,j)=zo(i,j)
      enddo !i
    enddo !j

  enddo !n

  write(*,'(a)')'Done subroutine smooth_box.'
  write(*,*) 
end subroutine smooth_box

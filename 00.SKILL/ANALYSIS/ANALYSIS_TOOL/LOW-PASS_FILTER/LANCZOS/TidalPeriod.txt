# Tidal Period (hr)
# �C�m�ϑ��w�j�\F.5(p.88)

Q1
360.0/13.3986609 ! deg/(deg/hr)
26.86835667      ! hr

O1
360.0/13.9430356 ! deg/(deg/hr)
25.81934166      ! hr

P1
360.0/14.9589314 ! deg/(deg/hr)
24.06589016      ! hr

K1
360.0/15.0410686 ! deg/(deg/hr)
23.93446965      ! hr

N2
360.0/28.4397295 ! deg/(deg/hr)
12.65834824      ! hr

M2
360./28.9841042 ! deg/(deg/hr)
12.42060122      ! hr

S2
360./30.0000000 ! deg/(deg/hr)
12.00000000      ! hr

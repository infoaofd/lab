#!/bin/sh

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16

if [ $# != 1 ]; then
  echo Error: Invalid number of arguments
  echo Usage: $0 INPUT_FILE
  echo Example: $0 weighting.txt
  echo
  exit 1
fi

IN=$1
if [ ! -f $IN ]; then
  echo
  echo Error: No input file.
  echo
  exit 1
fi

OUT=`echo $IN | sed -e "s/\.txt/.ps/"`

number_of_lines=`awk '{ nlines++ } END { print nlines }' $IN`

#RANGE=`awk '{if ($1 != "#") print $1, $2}' $IN | minmax -I2`
#echo $RANGE
RANGE=-R-270/270/-5/10
fac=1E-3
XLABEL=a100f50
YLABEL=a5f1
YTITLE="Weighting\030(x\030$fac)"
echo
pwd
echo
date
echo
echo "INPUT: " ${IN}
echo "OUTPUT: " ${OUT}
echo

# translate unit of time into day
# awk '{print $1*10/60/24, $2 }' $IN > $PROC

awk -v f=$fac '{if ($1 != "#") printf "%10.5f %10.5f\n", $1, $2/f}' \
 $IN | \
psxy -JX5/2 $RANGE -W3 \
  -B${XLABEL}:"# of terms":/${YLABEL}:${YTITLE}::."":WSne \
  -X1.5 -Y1.5 -P -K > $OUT
# psxy $IN -JX -R -Sc0.04 -G0 -O >> $OUT

title=""
# Print time, current working directory and output filename
currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
comment="$title"
pstext -JX6/1.2 -R0/1/0/1.2 -N -Y7.5 << EOF -O >> $OUT
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM ${IN}   ${OUT}
0 0.1   9 0 1 LM ${comment}
EOF


#!/bin/sh

#
# timesries
#

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16

if [ $# != 2 ]; then
  echo Error: Invalid number of arguments
  echo Usage: $0 Prefix_of_input_file File_No
  echo Example: $0 output/AS2_070706-23_Bin 01
  echo
  exit 1
fi

inhead=$1
file_no=$2

inext='.txt'

RANGE_EL=1/100/-10/10
size=5.5/1
XLABEL=a30f5
YLABEL=a10f1

OUT=`basename $inhead`

# Number of the bins to be plotted
no_of_file=8

temp_no=`expr $file_no + $no_of_file - 1`
last_file_no=`printf %02d $temp_no`
#echo $last_file_no


OUT=${OUT}${file_no}-${last_file_no}'.ps'

vlev=$file_no
IN=${inhead}${vlev}${inext}
echo INPUT:
echo $IN

awk '{if ($1 !="#") print $1, $2 }' $IN | \
  psxy -JX${size} -R$RANGE_EL \
  -B${XLABEL}:"day from Aug 1, 2007":/${YLABEL}:"[cms@+-1@+]":WSne \
  -W2/255/0/0 \
  -X1.5 -Y1 -P -K > $OUT
awk '{if ($1 !="#") print $1, $3 }' $IN | \
  psxy -JX${size} -R$RANGE_EL \
  -W2/0/0/255 \
  -O -K >> $OUT
pstext << END -R -JX -O -K >> $OUT
90 8 9 0 1 CM Bin${vlev}
END

CNT=`expr $file_no + 1`
while [ ${CNT} -le $last_file_no ]
do
  vlev=`printf %02d ${CNT}`
  IN=${inhead}${vlev}${inext}
  echo $IN

  awk '{if ($1 !="#") print $1, $2 }' $IN | \
  psxy -JX${size} -R$RANGE_EL \
  -B${XLABEL}:"day from Aug 1, 2007":/${YLABEL}:"[cms@+-1@+]":Wsne \
  -W2/255/0/0 \
  -Y1.2 -O -K >> $OUT
  awk '{if ($1 !="#") print $1, $3 }' $IN | \
  psxy -JX${size} -R$RANGE_EL \
  -W2/0/0/255 \
  -O -K >> $OUT
pstext << END -R -JX -O -K >> $OUT
90 8 9 0 1 CM Bin${vlev}
END
	CNT=$(expr ${CNT} + 1)
done



# Print time, current working directory and output filename
currentdir=`pwd`
if [ ${#currentdir} -gt 100 ]; then
  curdir1=${currentdir:1:100}
  curdir2=${currentdir:101}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
pstext -JX6/1.2 -R0/1/0/1.2 -N -Y1 << EOF -O >> $OUT
0 1.1   9 0 1 LM $0
0 0.95  9 0 1 LM ${now}
0 0.8   9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.5   9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM ${OUT}
0 0.15   9 0 1 LM red=eastward, blue=northward
EOF

echo 'OUTPUT:'
echo $OUT
exit 0

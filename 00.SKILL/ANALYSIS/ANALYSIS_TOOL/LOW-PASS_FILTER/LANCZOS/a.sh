#!/bin/sh

EXE="a.out"

if [ $# != 3 ]; then
  echo Error: Wrong number of arguments
  echo Usage: $0 PREFIX_OF_FILES NO_OF_FILES OUTPUT_FILE_NAME
  echo
  exit 1
fi

IN=$1
NO_FILES=$2
OUT=$3

${EXE} $IN $NO_FILES $OUT

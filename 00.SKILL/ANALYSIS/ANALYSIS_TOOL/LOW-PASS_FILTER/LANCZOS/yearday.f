C**********************************************************************
C yearday
C MODIFIED BY A.MANDA          3 Nov 2007
C TASK
C  For given year, month, and day, output a yearday
C
C  [INPUT]
C   IY
C   IM
C   ID
C  [OUTPUT]
C   YRDAY
C
C Example
C Sat Nov 03 15:25 2007
C /work1/aym/08.Work07/01.Research/02.Ibokisago/03.RDI_WH_Mooring/04.LowPassFilter/02.Lanczos_cosine
C [aym@oceani17<147>] yearday
C 
C Program yearday
C 
C Input year, month day:
C 2007 1 1
C Yearday =   1.0
C 
C Input year, month day:
C 2007 12 31
C Yearday = 365.0
C 
C Input year, month day:
C 2007 2 1
C Yearday =  32.0
C 
C Input year, month day:
C 2007 2 28
C Yearday =  59.0
C 
C Input year, month day:
C 2007 3 1
C Yearday =  60.0
C 
C Input year, month day:
C 2007 2 29
C Error in S/R yearday: Wrong input data
C day = 29, but IY does not indicate leap year.
C
***********************************************************************
C**********************************************************************
C
C Driver ruotine for tesing
C
C**********************************************************************
C      write(*,*)
C      write(*,'(A)')'Program yearday'
C      write(*,*)
C100   write(*,'(A)')'Input year, month day:'
C      read(*,*,err=8000)iy, im,id
C      call yearday(iy,im,id,yrday)
C      write(*,'(A,f5.1)')'Yearday = ',yrday
C      write(*,*)
C      goto 100
C
C      stop
C
C8000  write(*,'(A)')'Error: wrong input data'
C      write(*,'(A)')'Please input year, month, and day again.'
C      write(*,'(A)')'year(integer), month(integer), day(integer)'
C      goto 100
C
C      end

C**********************************************************************
C
C**********************************************************************
      subroutine yearday(IY,IM,ID,YRDAY)

      LOGICAL LEAPYR

      Juldy=NUMDAY(IY,IM,ID)-NUMDAY(IY,1,1) !Julian day

      if(leapyr(iy).eq. .false. .and. im.eq.2. and. id.eq.29)goto 8000
      YRDAY=float(Juldy)+1.0
      return

8000  write(*,'(A)')'Error in S/R yearday: Wrong input data'
      write(*,'(A)')'day = 29, but IY does not indicate leap year.'
      stop

      end

***********************************************************************
* NUMDAY.F
* CODED BY A.MANDA
* MODIFIED BY A.MANDA           1997. 
* TASK
*   RETURN THE NUMBER OF THE DAY FROM 1 JAN. AD.1 TO GIVEN DATE                        
* USAGE
*  [INPUT]
*    Y : YEAR (I), M : MONTH (I), D : DAY (I)
*  [OUTPUT]
*    NUMDAY (I)
*   I : INTEGER
* METHOD
* SLAVE FUNCTION(S)
*  NOFDAY
*  LEAPYR
* REMARK
* REFERENCE(S)                           
***********************************************************************
      INTEGER FUNCTION NUMDAY(Y,M,D)
*------------- COMMON VARIABLES SHARING OTHER ROUTINES ----------------
*----------------------------------------------------------------------
*--------------------------- PARAMTERS --------------------------------
      INTEGER Y,M,D
*----------------------------------------------------------------------
*--------- LIST OF THE VARIABLES USED ONLY IN THIS SUBROUTINE ---------
      INTEGER NOFDAY,DAYS(12),DY,DM
      LOGICAL LEAPYR
*----------------------------------------------------------------------

      DATA DAYS / 0,31,59,90,120,151,181,212,243,273,304,334/

      IF((Y.LT.1) .OR. (Y.GT.3000)) GOTO 90
      K=Y-1
      DY=365*K+(K/4)-(K/100)+(K/400)

      IF((D.LT.1) .OR. (D.GT.NOFDAY(Y,M))) GOTO 90
    
      DM=DAYS(M)
      IF(LEAPYR(Y) .AND. (M.GT.2)) DM=DM+1

      NUMDAY=DY+DM+D

      RETURN

90    NUMDAY=0

      END


***********************************************************************
* LEAPYR.F
* CODED BY A.MANDA
* MODIFIED BY A.MANDA           1997. 
* TASK
*  TEST ON A LEAP YEAR
* USAGE
*  [INPUT]
*    Y : YEAR (I)
*  [OUTPUT]
*    LEAPYR (L) (.TURE. OR .FALSE.)
*
* I : INTEGER , L : LOGICAL                         
*
* METHOD
* SLAVE SUBROUTINE(S)
* REMARK
* REFERENCE(S)                           
***********************************************************************
      LOGICAL FUNCTION LEAPYR(Y)
*------------- COMMON VARIABLES SHARING OTHER ROUTINES ----------------
*----------------------------------------------------------------------
*--------------------------- PARAMTERS --------------------------------
      INTEGER Y
*----------------------------------------------------------------------
*--------- LIST OF THE VARIABLES USED ONLY IN THIS SUBROUTINE ---------
*----------------------------------------------------------------------
      LEAPYR=.FALSE.
      IF(MOD(Y,4).NE.0) RETURN
      IF((MOD(Y,100).EQ.0).AND.(MOD(Y,400).NE.0)) RETURN
      LEAPYR=.TRUE.

      END

***********************************************************************
* NOFDAY.F
* CODED BY A.MANDA
* MODIFIED BY A.MANDA           1997. 
* TASK
*  TO RETURN NUMBER OF DAYS IN A GIVEN MONTH                         
* USAGE
*  [INPUT]
*    Y : YEAR (I) ,M : MONTH (I)
*  [OUTPUT]
*    NOFDAY : NUMBER OF THE DAYS IN A GIVEN MONTH
* METHOD
* SLAVE FUNCTION(S)
*  LEAPYR
* REMARK
* REFERENCE(S)                           
***********************************************************************
      INTEGER FUNCTION NOFDAY(Y,M)
*------------- COMMON VARIABLES SHARING OTHER ROUTINES ----------------
*----------------------------------------------------------------------
*--------------------------- PARAMTERS --------------------------------
      INTEGER Y,M
*----------------------------------------------------------------------
*--------- LIST OF THE VARIABLES USED ONLY IN THIS SUBROUTINE ---------
      INTEGER DAYM(12)
      LOGICAL LEAPYR
*----------------------------------------------------------------------
      DATA DAYM/31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 /

      IF((1.LE.M) .AND. (M.LE.12)) THEN
        NOFDAY=DAYM(M)
        IF(LEAPYR(Y) .AND. (M.EQ.2)) NOFDAY=29
      ELSE
        NOFDAY=0
      END IF

      END

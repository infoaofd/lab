#!/bin/sh

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16

if [ $# != 1 ]; then
  echo Error: Invalid number of arguments
  echo Usage: $0 INPUT_FILE
  echo Example: $0 frf.txt
  echo
  exit 1
fi

IN=$1
OUT=`echo $IN | sed -e "s/\.txt/.ps/"`

RANGE=0/120/-0.3/1.2
XLABEL=a24f6


echo
echo "INPUT: " ${IN}
echo "OUTPUT: " ${OUT}
echo

# translate unit of time into day
# awk '{print $1*10/60/24, $2 }' $IN > $PROC

awk '{if ($1 != "#") printf "%10.5f %10.5f\n", $1, $2}' $IN | \
psxy -JX5/4 -R$RANGE -W3 \
  -B${XLABEL}:"Period[hr]":/a0.5f0.1:"Freq. Response Factor"::."":WSne \
  -X1.5 -Y1.5 -P -K > $OUT

# vertical bars indicating tidal period 
psxy << END -JX -R -M -W1 -G0 -O -K >> $OUT
0 0
120 0
>
26.86835667 -0.5 #Q1
26.86835667 1.2
>
25.81934166 -0.5 #O1
25.81934166 1.2
>
24.06589016 -0.5 #P1
24.06589016 1.2
>
23.93446965 -0.5 #K1
23.93446965 1.2
>
12.65834824 -0.5 #N2
12.65834824 1.2
>
12.42060122 -0.5 #M2
12.42060122 1.2
>
12.00000000 -0.5 #S2
12.00000000 1.2
END

title=""
# Print time, current working directory and output filename
currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
comment="$title"
pstext -JX6/1.2 -R0/1/0/1.2 -N -Y7.5 << EOF -O >> $OUT
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM ${IN}   ${OUT}
0 0.1   9 0 1 LM ${comment}
EOF


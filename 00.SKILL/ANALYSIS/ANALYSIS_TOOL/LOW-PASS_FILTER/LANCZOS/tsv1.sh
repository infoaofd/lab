#!/bin/sh

#
# timesries
#

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 16

gmtset INPUT_DATE_FORMAT yyyy-mm-dd
#gmtset PLOT_DATE_FORMAT 'o yyyy'
#gmtset ANNOT_FONT_SIZE_PRIMARY +16p
#gmtset TIME_FORMAT_PRIMARY abbreviated CHAR_ENCODING ISOLatin1+
gmtset PLOT_DATE_FORMAT -o TIME_FORMAT_SECONDARY abbreviated ANNOT_FONT_SIZE_PRIMARY +12p
gmtset ANNOT_FONT_SIZE_SECONDARY +14p
gmtset TIME_LANGUAGE us

if [ $# != 1 ]; then
  echo Error: Invalid number of arguments
  echo Usage: $0 INPUT_FILE
  echo Example: $0 output/Amakusa071101Bin19.txt
  echo
  exit 1
fi

IN=$1


RANGE='2008-06-30T00:00:00/2008-07-30T23:00:00/-20/20'
SIZE=6T/2
YLABEL=a10f5

TMP=`basename $IN`
OUT=`echo $TMP | sed -e "s/\.txt/.ps/"`

echo INPUT:
echo $IN




awk '{if ($1 != "#" && $2 != 0.00000 ) \
printf "%04i%s%02i%s%02i%s%02i%s%02i%s%02i  %10.4f\n", \
2000+$8,"-",$9,"-",$10,"T",$11,":",$12,":",$13, $2 }' $IN | \
psxy -JX$SIZE -R$RANGE \
-Bsa1OS/WS -Bpa7Rf1d/${YLABEL}:"velocity [cm@+ @+s@+-1@+]":WSne \
-W4/255/0/0 \
-X1 -Y1 -P -K > $OUT

awk '{if ($1 != "#" && $3 != 0.00000 ) \
printf "%04i%s%02i%s%02i%s%02i%s%02i%s%02i  %10.4f\n", \
2000+$8,"-",$9,"-",$10,"T",$11,":",$12,":",$13, $3 }' $IN | \
psxy -JX$SIZE -R$RANGE \
-W4/0/0/255 \
-O -K >> $OUT


title="red=eastward, blue=northward"
# Print time, current working directory and output filename
currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
comment="$title"
pstext -JX6/1.2 -R0/1/0/1.2 -N -Y7.5 << EOF -O >> $OUT
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM ${IN}   ${OUT}
0 0.1   9 0 1 LM ${comment}
EOF

echo 'OUTPUT:'
echo $OUT
exit 0

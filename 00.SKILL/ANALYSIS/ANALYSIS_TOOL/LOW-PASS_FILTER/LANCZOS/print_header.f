C***********************************************************************
C print_header
Cj 出力ファイルに以下のヘッダ情報を印字する
cj
cj 作成日時
cj 現在作業しているディレクトリ名
cj ユーザー名
cj ホスト名
cj プログラム名
cj
cj 使用法
c   call print_header(iunit)
cj  iunit: 出力ファイルの機番(整数)
cj
C NOTE
cj このサブルーチンは, g77, ifortで動作することを確認している.
cj
cj 作成したデータが、何時、何処で、誰が、どのプログラムを使って作成した
cj か記録しておくと、後々作業をし直す必要が生じたときに役に立つ.
cj また、以前使ったプログラムを一部改変して利用するときなど、そのプログ
cj ラムを探すのに役に立つことが多い
C***********************************************************************
      subroutine print_header(iunit)

C------------------------- DECLARATION ---------------------------------
C      implicit none
C      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
C-------------------------- PARAMETERS ---------------------------------
C------------------------- COMMON VARIABLES ----------------------------
C----------------------------ARGUMENTS ---------------------------------
      integer iunit
C------------------------- LOCAL VARIABLES -----------------------------
C Print date, hostname, cwd, and username
      character dirinfo*144, userinfo*72, hostinfo*72
      INTEGER date_time(8)
      CHARACTER REAL_CLOCK(3)*12

      character progname*100
      integer idxpn, idx
      character shellinfo*80
C-----------------------------------------------------------------------
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C Get date, time, current directory, username, and hostname
C p.345 of for_lang.pdf (intel fortran user manual)
C DATE_AND_TIME is an intrisic S/R and works on most of Fotran 90 
C Compilers. g77 also supports this S/R.
      CALL DATE_AND_TIME (REAL_CLOCK (1), REAL_CLOCK (2), 
     $    REAL_CLOCK (3), date_time)
      call getenv("PWD", dirinfo)
      call getenv("USER", userinfo)

cj 使用しているシェルのチェック
cj 現在, sh, bash, csh, tcshのみに対応
      call getenv("SHELL",shellinfo)
      idxcsh=index(shellinfo,"/csh")
      idxtcsh=index(shellinfo,"/tcsh")
      idxsh=index(shellinfo,"/sh")
      idxbash=index(shellinfo,"/bash")

      if(idxcsh.ne.0.or.idxtcsh.ne.0)then
        call getenv("HOST", hostinfo) !csh, tcsh, etc.
      else if(idxsh.ne.0.or.idxbash.ne.0)then
        call getenv("HOSTNAME", hostinfo) !sh, bash, etc.
      end if

      write(iunit,'(a, 
     $ i2.2,a,i2.2,a,i4.4, a,i2.2,a,i2.2,a,i2.2, a,i3.2,a)')
     $  '# Date and time: ',
     $  date_time(2),'/',date_time(3),'/',date_time(1),
     $  ' at ',date_time(5),':',date_time(6),':',date_time(7),
     $  ' ',-date_time(4)/60,':00'
      write(iunit,'(A,A)')'# hostname: ',hostinfo

      idx=index(dirinfo,' ')
      if(idx.le.65)then
        write(iunit,'(A,A)')'# cwd: ',dirinfo
      else if(idx.ge.66.and.idx.lt.134)then
        write(iunit,'(A,A)')'# cwd: ',dirinfo(1:65)
        write(iunit,'(A,A)')'# ',dirinfo(66:133)
      else if(idx.ge.134)then
        write(iunit,'(A,A)')'# cwd: ',dirinfo(1:65)
        write(iunit,'(A,A)')'# ',dirinfo(66:133)
        write(iunit,'(A,A)')'# ',dirinfo(134:)
      end if
      write(iunit,'(A,A)')'# user: ',userinfo

C Get program name
      call getarg( 0, progname)
      idxpn=index(progname,' ')-1
C      write(*,'(A)')'Program name: '
C      write(*,'(A)')progname(1:idxpn)


      write(iunit,'(A,A)')'# program name: ',progname(1:idxpn)

CD      idxrn=index(run_name,' ')-1
CD      write(iunit,'(A,A)')'# run name: ',run_name(1:idxrn)
CD

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      return
      end


C***********************************************************************
C
C Note
C  Input file information
C Sat Nov 03 12:01 2007
C /work1/aym/08.Work07/01.Research/02.Ibokisago/03.RDI_WH_Mooring/01.Data
C [aym@oceani17<14>] head Amakusa071101Cond.txt
C "Series Data"
C "G:\0.FIELD_PROGRAM_DATA\01.Ariake\Fy2007\20071101Amakusa\04.RDI_WH_071102\_RDI_000.000"
C "Broadband 614.4 kHz"
C "Pings/Ens ="   120
C "Time/Ping = 00:05.00"
C "First Ensemble Date = 07/08/06"
C "First Ensemble Time = 11:44:48.42"
C "Ensemble Interval (s) = "      600.00
C "1st Bin Range (m) = "  2.70
C "Bin Size (m) ="        2.00
C***********************************************************************
C------------------------- DECLARATION ---------------------------------
C      implicit none
C      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
C      include 'comblk.h'
C-------------------------- PARAMETERS ---------------------------------
      parameter(MX=15000) !Maximum number of input data
C------------------------- COMMON VARIABLES ----------------------------
C      common //
C------------------------- LOCAL VARIABLES -----------------------------
C      double precision 
C      real
C      integer 
      character progname*72
      character infle_prefix*69,strm*144
      character infle*72, ofle*80,ofletmp*72
      character infle2*72, ofle2*72

      integer Nens(MX), yr(MX), mo(MX), day(MX), hour(MX), 
     $min(MX), sec(MX), milisec(MX)
      dimension etime(MX),pg4(MX)
      dimension VeCM(MX), VnCM(MX)
C Low-passed time series
      dimension Velp(MX), Vnlp(MX)
C-----------------------------------------------------------------------

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--

C Check number of command line arguments
      nargs = iargc() 
c One at a time, get an argument and write it out 
      if(nargs.lt.2)then
        goto 8000 ! Error handling
      end if
      call getarg( 0, progname)
      call getarg( 1, infle_prefix )
      call getarg( 2, strm)
      call getarg( 3, ofletmp)
      write(*,'(A)')progname
C      write(*,'(A)')'input: '
C      write(*,'(A)')infle

      read(strm, *)nofile

C z0: 1st bin range measured from the bottom (not transeducer)
C "1st Bin Range (m) = "  1.73 !2.70
      zxdcr=0.77  ! Height of the tranceducer
      z0=zxdcr + 1.73    !2.50 + 2.70

      write(*,*)'zxdcr: Height of the tranceducer'
      write(*,*)'z0: 1st bin range measured from the bottom (not transed
     $ucer)'
      write(*,*)'zxdcr=',zxdcr
      write(*,*)'z0=',z0

C Height above the bottom of the tranceducer = 2.50
C Needs to be checked (07/11/03)

! G:\09.Work08\01.Research\11.Ariake08SummerObsCamp\05.PlotRawData\01.MoorAS4\00.Data\_RDI_000.000
! Bin Size    (WS) 1.00 m
      dz=1.0
      write(*,*)'dz: Bin Size (m)'
      write(*,*)'dz=',dz

C OUTPUT FILE
      call system(
     $'if [ ! -d output ]; then 
     $   (echo "Create output directory"; mkdir -p output); 
     $ fi')

      idxo=index(ofletmp,'##')
      if(idxo.eq.0)goto 8040

      do n=1,nofile

        idx=index(infle_prefix,' ')

        infle=infle_prefix
        write(infle(idx:),'(I2.2)')n
        write(infle(idx+2:),'(A)')'.txt'

        open(10,file=infle,status='old',err=8010)

        write(ofletmp(idxo:idxo+1),'(I2.2)')n
        ofle="output/"//ofletmp
        open(20,file=ofle)
        call print_header(20)
        call print_ioinfo(20,infle,"input")

        if(n.eq.1.or.n.eq.nofile)then !c1
          write(*,'(A)')infle
          write(*,'(A)')ofle
        else if(n.eq.2)then
          write(*,'(A)')' ..... '
        end if !c1

C        if(n.eq.1) then
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C          call stdout('Header Information   ')
          call fileout(20,'#--------------------------------------------
     $   ')
          call fileout(20,'Header lines in the input file   ')

          do j=1,14
            read(10,'(A)')strm
C            call stdout(strm)
            call fileout(20,strm)
          end do !j
          call fileout(20,'#--------------------------------------------
     $   ')


C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
          write(strm,'(A,f6.3,A)') 'Distance between the bottom to the x
     $dcr, z0 = ',z0,' (m)     '
C          write(*,*)strm
          call fileout(20,'   ')
          call fileout(20,strm)
C
C Height of the bin measured from the bottom 
          z=z0 + dble(n-1)*dz
C
          write(20,'(A)')'# Height of the bin measured from the bottom, 
     $z'
          write(20,'(A,f10.3)')'# ',z
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--

        read(10,*)bin_inp !L15

        if(int(bin_inp).ne.n)then
          goto 8020
        end if

        read(10,*) !L16
        i=1
100     read(10,'(a)',END=9000,err=8030)STRM
          IF(STRM(1:1).EQ.'#')GOTO 100
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
          read(STRM,*,end=110,err=8030) 
     $Nens(i), yr(i), mo(i), day(i), hour(i), 
     $min(i), sec(i), milisec(i), Ve, Vn, pg4(i)
          goto 150

110       read(STRM,*)
     $Nens(i), yr(i), mo(i), day(i), hour(i), min(i), sec(i), milisec(i)
          Ve=0.0
          Vn=0.0
          Vmag=0.0
          Vdir=0.0
          pg4(i)=0.0
c          write(*,'(A)')'# Erroneous Data'
c          write(*,'(A)')infle
c          write(*,'(i5,f5.0,i3,f4.0,f4.0,i3,f4.0)')
c     $Nens(i), yr(i), mo(i), day(i), hour(i), min(i), sec(i)

150    iy=int(yr(i))
       im=int(mo(i))
       id=int(day(i))
       call yearday(IY,IM,ID,YRDAY)

C Set origin to Jul 1st
       call yearday(iy,7,1, Firstday)

       Etime(i) = YRDAY - Firstday
     $ + float(hour(i))/24. + float(min(i))/(24.*60.)
     $ + float(sec(i))/(24.*60.*60.)
          VeCm(i)=Ve/10. !mm/s -> cm/s
          VnCm(i)=Vn/10.
          VmagCm=Vmag/100.

          i=i+1
          goto 100
9000    NDAT=i-1
        close(10) 

        if(n.eq.1)then
          write(*,*) 'NDAT=', NDAT
        end if
C
C Low pass filter
C
        dt = 10./60. ! (hr)
        PRD1=60. ! cut-off period (hr)

        if(n.eq.1)then
          monitor=1
        else
          monitor=0
        end if
        call lanczos_cosine(VeCm,Velp,dt,MX,NDAT,PRD1, ntrim, monitor) 
        call lanczos_cosine(VnCm,Vnlp,dt,MX,NDAT,PRD1, ntrim, 0) 

          write(20,'(A)')'# Etime(day), Velp(cm/s), Vnlp(cm/s), Ve(cm/s), 
     $Vn(cm/s), pg4, Nens, yr, mo, day, hour, min, sec, milisec'
        do i=1+ntrim,NDAT-ntrim
          write(20,'(f15.8, 4f10.4, f5.0,1x,i5,1x,7i5)') 
     $    Etime(i), Velp(i), Vnlp(i), VeCm(i), VnCm(i), pg4(i),Nens(i), 
     $yr(i), mo(i), day(i), hour(i), min(i), sec(i), milisec(i)
        end do !i
        close(20)

      end do !n

      write(*,'(A)')'Program terminated normally.'
      stop


C
C Error handling
C
8000  write(*,'(A)')'Error: Wrong number of arguments.'
      write(*,'(A)')'Program terminated.'
      write(*,'(A)')'Program name:'
      write(*,'(A)')progname
      write(*,*)
      stop

8010  write(*,'(A)')'Error: No such file.'
      write(*,'(A)')infle
      write(*,*)
      stop

8020  write(*,'(A)')'Error: Bin number mismatch.'
      write(*,'(A)')infle
      write(*,'(A,I5)')'bin_inp = ',int(bin_inp)
      write(*,'(A,I5)')'n       = ',n
      write(*,'(A)')'Program terminated.'
      write(*,*)
      stop

8030  write(*,'(A)')'Error: Wrong format.'
      write(*,'(A)')infle
      write(*,'(A,I5)')'Ensumble number = ',Nens(i)
      write(*,'(A)')'Program terminated.'

      write(*,*)
      stop

8040  write(*,'(A)')'Error: Output file must contain ##.'
      write(*,'(A)')'Example: output/AS1_##.txt'
      write(*,'(A)')'Program terminated.'
      write(*,*)
      stop
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      end

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine stdout(strm)
      character strm*144
      idx=index(strm,"   ")
      if(idx.lt.73)then
        write(*,'(A)')strm(1:idx)
      else
        write(*,'(I2.2,3x,A)')1, strm(1:72)
        write(*,'(I2.2,3x,A)')2, strm(73:)
      end if
      return
      end

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      subroutine fileout(io,strm)
      integer io
      character strm*144

      if(io.lt.7)goto 8000
      idx=index(strm,"   ")
      if(idx.lt.73)then
        write(io,'(A,A)')'# ',strm(1:idx)
      else
        write(io,'(A,A)')'# 1 ', strm(1:71)
        write(io,'(A,A)')'# 2 ', strm(72:)
      end if
      return

8000  write(*,'(A)')'In fileout'
      write(*,'(A)')'Wrong file unit number.'
      stop

      end

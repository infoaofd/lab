***********************************************************************
* output.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
* REFERENCE                       
***********************************************************************
      SUBROUTINE OUTPUT(MMOUT,NNOUT,NOFLE)
*-------------------------- PARAMETERS --------------------------------
      INCLUDE 'param.h'
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
*[INPUT]
*[OUTPUT]
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
*----------------------------------------------------------------------
      do n=1,nofle
        OPEN(10,FILE=OFLE(n),STATUS='UNKNOWN')
        WRITE(10,'(A)')'#INTERPOLATED&SMOOTHED(MISSING VALUE = 999.0)'

        WRITE(10,'(A9,F7.2,A3)')'#DEPTH = ',OUTDEP(n),'[m]'
        WRITE(10,'(A23,I5,A5,I5)')'#NUMBER OF GRIDS , X = ',MMOUT
     +                           ,' Y = ',NNOUT
        WRITE(10,'(A33,I5,A6,I5)')'#NUMBER OF THE GRIDS(INNER), X = '
     +                            ,MMP,', Y = ',NNP
        WRITE(10,'(A)')'#NUMBER OF THE GRIDS OF MARGIN(L,R,B,T)'
        WRITE(10,'(4I5)')ILMG,IRMG,IBMG,ITMG
        WRITE(10,'(A16,F7.2,A4,A5,F7.2,A6,A6,F7.2,A4)')
     +  '#GRID SIZE, X = ',HXP,'[km]',',T = ',HTP,'[hour]',', Y = ',HYP
     +  ,'[km]'
        WRITE(10,'(A28,F7.3)')'#REFERENCE DATE OF T AXIS = ',ROBD
        WRITE(10,'(A)')'#LEFT[km],RIGHT[km],BOTTOM(T)[hour],TOP(T)[hour]
     +,BOTTOM(Y)[km],TOP(Y)[km]'
        WRITE(10,'(6F10.3)')XLFT,XRGT,TBTM,TTOP,YBTM,YTOP
        WRITE(10,'(A)')'#COORDINATE OF X(I=1,MMOUT)'
        WRITE(10,'(10F8.3)')(XOUT(I),I=1,MMOUT)
        WRITE(10,'(A)')'#COORDINATE OF T(J=1,NNOUT)'
        WRITE(10,'(10F8.3)')(TOUT(J),J=1,NNOUT)
        WRITE(10,'(A)')'#COORDINATE OF Y(J=1,NNOUT)'
        WRITE(10,'(10F8.3)')(YOUT(J),J=1,NNOUT)
*/// Modified in 2 June 1999///
        write(10,'(A)')'#latitude of each grid'
        write(10,'(10F8.4)')(XLAT(I),I=1,MMOUT)
*///
        WRITE(10,'(a1,a70)')'#',influ(n)
        DO 110 J=1,NNOUT
          WRITE(10,'(A6,I5)')'# J = ',J
          WRITE(10,'(7F10.3)')(UINP(I,J,n),I=1,MMOUT)
110     CONTINUE
        CLOSE(10)
      end do

      RETURN
      END


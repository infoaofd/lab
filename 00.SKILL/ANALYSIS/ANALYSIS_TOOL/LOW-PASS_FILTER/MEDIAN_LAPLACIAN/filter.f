***********************************************************************
* filter.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK
*  filtering the velocity field of the results of the diagnostic model.
* METHOD
* SLAVE SUBROUTINE
*
* REMARKS
* REFERENCE                          
***********************************************************************
*------------------------- PARAMETERS ---------------------------------
      include 'param.h'
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*----------------------- LIST OF VARIABLES ----------------------------
      CHARACTER STRM*100
*----------------------------------------------------------------------
*
* CONFIGURATION
*
      OPEN(7,FILE='config.txt',STATUS='UNKNOWN')
      read(7,*)
      read(7,'(i5)')nofle
      read(7,*)
      do 100 n=1,nofle
        read(7,*)
        READ(7,'(A70)')INFLU(n)
        READ(7,'(A70)')OFLE(n)
        write(*,'(A70)')INFLU(n)
        write(*,'(A70)')OFLE(n)
100   continue

c     --- times of filtering ---
      read(7,*)
      READ(7,'(I5)')ITIME
      WRITE(*,*)'USING LAPLACIAN AND FILTER FILTER ',itime,' TIME(S).'

      CLOSE(7)

* infle(a70)
* itime(i1)
* ofle(a70)

*
* READ DATA
*

      call input(MMOUT,NNOUT,NOFLE)

* sponge region
      is=ILMG
      ie=ILMG+MMP
      js=IBMG
      je=IBMG+NNP
*
* MEDIAN AND LAPLACIAN FILTER
*
      do 300 n=1,nofle
        do i=1,mmout
          do j=1,nnout
            up(i,j)=uinp(i,j,n)
          end do
        end do

        call medlpm(MMW,NNW,MMOUT,NNOUT,up,itime,RMISS
     &   ,is,ie,js,je)

      do i=1,mmout
        do j=1,nnout
          uinp(i,j,n)=up(i,j)
        end do
      end do

300   continue

*
* OUTPUT
*
      call output(MMOUT,NNOUT,NOFLE)

      STOP
      END

*------------------------- PARAMETERS ---------------------------------
      parameter(MXFLE=60,MXZ=28)
      PARAMETER(MMI=90,NNI=120,MMW=120,NNW=142)
      PARAMETER(CP=10.0)
      PARAMETER(ISTEPP=400,IPRP=1)
      PARAMETER(RMISS=999.0)

*----------------------------------------------------------------------

*------------------------ COMMON VARIABLES ----------------------------
      CHARACTER INFLU(mxfle)*70,OFLE(mxfle)*70

* --- VARIABLES TO OUTPUT GRIDDED DATA ---
      REAL OUTDEP(MXFLE),XOUT(MMW),TOUT(NNW),YOUT(NNW)
     +    ,UINP(MMW,NNW,MXFLE),UP(MMW,NNW)

      REAL XKM(MXFLE),THOUR(MXFLE),DEP(MXFLE),XLAT(MMW)

      INTEGER ILMG,IRMG,IBMG,ITMG

      REAL SOB,EOB,ROBD

      REAL HXP,HTP,HYP,HX,HT

      REAL XLFT,XRGT,TBTM,TTOP,YBTM,YTOP

      COMMON /FILES/INFLU,OFLE

* --- INPUT ---
      COMMON /INPINF/UINP,UP,MMP,NNP

* --- VARIABLES TO OUTPUT GRIDDED DATA ---
      COMMON /OUTINF/OUTDEP,XOUT,TOUT,YOUT

      COMMON /COORD/XKM,THOUR,DEP,XLAT

      COMMON /MARGIN/ILMG,IRMG,IBMG,ITMG

      COMMON /DATE/SOB,EOB,ROBD

      COMMON /GRID/NOZ,HXP,HTP,HYP,HX,HT

      COMMON /AXIS/XLFT,XRGT,TBTM,TTOP,YBTM,YTOP
*----------------------------------------------------------------------

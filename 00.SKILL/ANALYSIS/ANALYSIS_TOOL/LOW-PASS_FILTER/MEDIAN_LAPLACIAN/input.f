***********************************************************************
* input.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
* REFERENCE                       
***********************************************************************
      SUBROUTINE input(MMOUT,NNOUT,NOFLE)
*-------------------------- PARAMETERS --------------------------------
      INCLUDE 'param.h'
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
*[INPUT]

*[OUTPUT]
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
      CHARACTER DUM*80
*----------------------------------------------------------------------
      do n=1,nofle
        OPEN(10,FILE=INFLU(n),STATUS='UNKNOWN')
        READ(10,'(A)')DUM
        READ(10,'(A9,F7.2,A3)')DUM,OUTDEP(n),DUM
        READ(10,'(A23,I5,A5,I5)')DUM,MMOUT
     +                           ,DUM,NNOUT
        READ(10,'(A33,I5,A6,I5)')DUM,MMP,DUM,NNP
        READ(10,'(A)')DUM
        READ(10,'(4I5)')ILMG,IRMG,IBMG,ITMG
        READ(10,'(A16,F7.2,A4,A5,F7.2,A6,A6,F7.2,A4)')
     +  DUM,HXP,DUM,DUM,HTP,DUM,DUM,HYP
     +  ,DUM
        READ(10,'(A28,F7.3)')DUM,ROBD
        READ(10,'(A)')DUM
        READ(10,'(6F10.3)')XLFT,XRGT,TBTM,TTOP,YBTM,YTOP
        READ(10,'(A)')DUM
        READ(10,'(10F8.3)')(XOUT(I),I=1,MMOUT)
        READ(10,'(A)')DUM
        READ(10,'(10F8.3)')(TOUT(J),J=1,NNOUT)
        READ(10,'(A)')DUM
        READ(10,'(10F8.3)')(YOUT(J),J=1,NNOUT)
*/// Modified in 2 June 1999///
        READ(10,'(A25)')DUM
        READ(10,'(10F8.4)')(XLAT(I),I=1,MMOUT)
*///
        READ(10,'(a)')DUM
        DO 110 J=1,NNOUT
          READ(10,'(A6,I5)')DUM,Jdum
          READ(10,'(7F10.3)')(UINP(I,J,n),I=1,MMOUT)
110     CONTINUE
        CLOSE(10)
      end do

      RETURN
      END

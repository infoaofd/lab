gmtset MEASURE_UNIT INCH
gmtset BASEMAP_TYPE PLAIN
gmtset HEADER_FONT_SIZE 14
gmtset LABEL_FONT_SIZE 10

awk '{print $1, $2, $5}' 06-030g.dat | xyz2grd  -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -C0.2 -JM2 -W2 -Y7.5 -X1.5 -P -K > Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (030m)":  -O -K >> Sal_Jun.ps

awk '{print $1, $2, $5}' 06-050g.dat | xyz2grd  -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (050m)":  -O -K >> Sal_Jun.ps

awk '{print $1, $2, $5}' 06-075g.dat | xyz2grd -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (075m)":  -O -K >> Sal_Jun.ps

awk '{print $1, $2, $5}' 06-100g.dat | xyz2grd  -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y6 -X3 -P -K -O >> Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (100m)":  -O -K >> Sal_Jun.ps

awk '{print $1, $2, $5}' 06-125g.dat | xyz2grd  -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (125m)":  -O -K >> Sal_Jun.ps

awk '{print $1, $2, $5}' 06-150g.dat | xyz2grd -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> Sal_Jun.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> Sal_Jun.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> Sal_Jun.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."Sal_Jun (150m)":  -O  >> Sal_Jun.ps


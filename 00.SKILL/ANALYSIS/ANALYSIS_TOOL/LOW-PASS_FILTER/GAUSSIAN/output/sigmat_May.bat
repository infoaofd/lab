gmtset MEASURE_UNIT INCH
gmtset BASEMAP_TYPE PLAIN
gmtset HEADER_FONT_SIZE 14
gmtset LABEL_FONT_SIZE 10

xyz2grd 05-030g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -C0.2 -JM2 -W2 -Y7.5 -X1.5 -P -K > SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (030m)":  -O -K >> SigmaT_May.ps

xyz2grd 05-050g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -C0.2 -JM2 -W2 -Y-3 -P -K -O >> SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (050m)":  -O -K >> SigmaT_May.ps

xyz2grd 05-075g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -C0.2 -JM2 -W2 -Y-3 -P -K -O >> SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (075m)":  -O -K >> SigmaT_May.ps

xyz2grd 05-100g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -C0.2 -JM2 -W2 -Y6 -X3 -P -K -O >> SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (100m)":  -O -K >> SigmaT_May.ps

xyz2grd 05-125g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (125m)":  -O -K >> SigmaT_May.ps

xyz2grd 05-150g.dat -I0.5  -Gtmp.grd  -R122/131/26/34
grdcontour tmp.grd  -R -G0.8/5 -A0.2f8 -JM2 -W2 -Y-3 -P -K -O >> SigmaT_May.ps
grdcontour tmp.grd  -R -G0.8/5 -A1f8  -JM -W3  -P -K -O >> SigmaT_May.ps
psxy obs_line98.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
psxy obs_line99.dat -R -JM  -W4  -O -K >> SigmaT_May.ps
pscoast -R -G200 -W1 -JM -Di -Ba2f1WSne:."SigmaT_May (150m)":  -O  >> SigmaT_May.ps


***********************************************************************
* output.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
* REFERENCE                       
***********************************************************************
      SUBROUTINE OUTPUT(OUTFLE,NX,NY,RL,RR,RB,RT,D,MAXX,MAXY,TG,SG,RHOG
     +                 ,EFDKM,RKM)
*-------------------------- PARAMETERS --------------------------------
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
      CHARACTER OUTFLE*50
      INTEGER NX,NY
      REAL RL,RR,RB,RT,D
      INTEGER MAXX,MAXY
      REAL*8 TG(MAXX,MAXY),SG(MAXX,MAXY),RHOG(MAXX,MAXY)
*[INPUT]
* OUTFLE : OUTPUT FILE NAME 
* NX,NY : NUMBER OF THE GRIDS X AND Y DIRECTION, RESPECTIVELY.
* RB,RT,RL,RR : BOTTOM,UPPER,LEFT,RIGHT BOUNDS, RESPECTIVELY.
* D : GRID SIZE[degree]
* MAXX,MAXY : DIMENSION OF TG AND SG.
* TG,SG : GRIDDED TEMPERATURE AND SALINITY.

      REAL*8 RKM,EFDKM
* RKM,EFDKM : RADIUS AND E-FOLDING SCALE OF GAUSSIAN FILTER[km]

*[OUTPUT]
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
*----------------------------------------------------------------------

      OPEN(20,FILE='outcond.dat',STATUS='UNKNOWN')
      WRITE(20,'(4F10.3)')RL,RR,RB,RT
      WRITE(20,'(2I5)')NX,NY
      WRITE(20,'(2F10.5)')RKM,EFDKM
      WRITE(20,'(F10.5)')D
      close(20)

      OPEN(20,FILE=OUTFLE,STATUS='UNKNOWN')
      DO 100 I=1,NX+1
        DO 100 J=1,NY+1
          X=RL+FLOAT(I-1)*D
          Y=RB+FLOAT(J-1)*D
          if(tg(i,j).ne.999.0.and.sg(i,j).ne.999.0.and.rhog(i,j).ne.
     &    999.0)then
            WRITE(20,'(2F10.3,3F10.5)')X,Y,RHOG(I,J),TG(I,J),SG(I,J)
          end if
100   CONTINUE
      CLOSE(20)

      RETURN
      END

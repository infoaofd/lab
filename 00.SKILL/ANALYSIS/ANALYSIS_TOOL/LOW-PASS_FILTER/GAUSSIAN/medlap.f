***********************************************************************
* medlap.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
*  Spatial smoothing using Median filter & Laplacian filter 
*    
* METHOD
* SLAVE SUBROUTINE
* REMARK
*  3 by 3 grids domin.
*  Error must be "greater" than regular value!!!
* REFERENCE                       
***********************************************************************
      SUBROUTINE medlap(NMAX,KMAX,ix,jy,fncm,itime,error)
*-------------------------- PARAMETERS --------------------------------
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
      INTEGER NMAX,KMAX
      integer ix,jy
      real*8 fncm(NMAX,KMAX)
      integer itime
      real error
*[INPUT]
* itime : filter applying times.
* error : missing value(If fncm(i,j) equals to 'error', it is ignored).
*[OUTPUT]
*[INPUT & OUTPUT]
* fncm : input data(input) , smoothed data(output)

*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
      real*8 fnc(ix,jy),fm(9)
*----------------------------------------------------------------------
c
      do 45 n=1,itime
*      /// MEDIAN FILTER ///
       do 46 i=1,ix
       do 46 j=1,jy
         fnc(i,j)=fncm(i,j)
   46  continue
c
      do 50 i=2,ix-1
        do 50 j=2,jy-1
          if(fncm(i,j).eq.error) go to 50
          fm(1)=fnc(i,j)
          fm(2)=fnc(i+1,j)
          fm(3)=fnc(i-1,j)
          fm(4)=fnc(i,j+1)
          fm(5)=fnc(i,j-1)
          fm(6)=fnc(i+1,j+1)
          fm(7)=fnc(i-1,j+1)
          fm(8)=fnc(i+1,j-1)
          fm(9)=fnc(i-1,j-1)
          medi=5
          ind=0
          do 49 iii=1,9
            if(fm(iii).eq.error) ind=ind+1
   49     continue
          if(ind.eq.2.or.ind.eq.3) medi=4
          if(ind.eq.4.or.ind.eq.5) medi=3
          if(ind.eq.6) medi=2
          if(ind.ge.7) go to 50
          do 60 nnn=1,8
            do 70 nnnn=nnn+1,9
              if(fm(nnn).ge.fm(nnnn)) then
                dumy=fm(nnnn)
                fm(nnnn)=fm(nnn)
                fm(nnn)=dumy
              end if
   70       continue
   60     continue
          fncm(i,j)=fm(medi)
   50 continue
      WRITE(*,*)'USING MEDIAN FILTER ',itime,' TIME(S).'
*     /// LAPLACIAN FILTER ///
      do 51 i=1,ix
      do 51 j=1,jy
      fnc(i,j)=fncm(i,j)
   51 continue
      do 52 i=2,ix-1
      do 52 j=2,jy-1
        if(fncm(i,j).eq.error) go to 52
        a1=1.
        b1=1.
        a2=1.
        b2=1.
        if(fncm(i+1,j).eq.error) a1=0.
        if(fncm(i-1,j).eq.error) a2=0.
        if(fncm(i,j+1).eq.error) b1=0.
        if(fncm(i,j-1).eq.error) b2=0.
        fncm(i,j)=fnc(i,j)
     &     +0.2*(a1*(fnc(i+1,j)-fnc(i,j))-a2*(fnc(i,j)-fnc(i-1,j))
     &          +b1*(fnc(i,j+1)-fnc(i,j))-b2*(fnc(i,j)-fnc(i,j-1)))
   52 continue
c
      WRITE(*,*)'USING LAPLACIAN FILTER ',itime,' TIME(S).'

   45 continue


      return
      end


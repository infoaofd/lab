***********************************************************************
* grid.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK
*  HORIZONTAL GRIDDING BY GAUSSIAN FILTER.                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
*  Input data are made by itgdat.f(in ../JODC+KODC_DATA).
* REFERENCE                       
***********************************************************************
*-------------------------- PARAMETERS --------------------------------
      PARAMETER(MAXZ=9,MAXMON=12,MAXFLE=MAXMON*MAXZ)
      PARAMETER(MAXDAT=10000)
      PARAMETER(MAXX=120,MAXY=120)
      PARAMETER(ITIME=1)
* ITIME : MEDIAN AND LAPLACIAN FILTER APPLYING TIMES
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*----------------------------------------------------------------------
*------------------------ LIST OF VARIABLES ---------------------------
      CHARACTER INFLE(MAXFLE)*50,OUTFLE(MAXFLE)*50,BTHFLE*70
* BTHFLE : BATHYMETRIC DATA FILE

      REAL RLON(MAXDAT),RLAT(MAXDAT)
      REAL T(MAXDAT),S(MAXDAT),RHO(MAXDAT)
      INTEGER IDATE
* IDATE : DUMMY VARIABLE(NOT USED, ONLY READ).

      REAL R,EFOLD
* R : RADIUS OF THE GAUSSIAN FILTER.
* EFOLD : E-FOLDING SCALE OF GAUSSIAN FILTER.
*

      REAL*8 TG(MAXX,MAXY),SG(MAXX,MAXY),RHOG(MAXX,MAXY)
* TG : GRIDDED TEMPERATURE DATA, SG : GRIDDED SALINITY DATA

      REAL RMISS
* RMISS : MISSING VALUE.

      REAL*8 RKM,EFDKM
* RKM : RADIUS OF FILTER IN KM.
* EFDKM : E-FOLDING SCALE IN KM.

      REAL XB(MAXX),YB(MAXY),BTH(MAXX,MAXY)
* XB,YB : GRID POSITION OF X OF BATHMETRIC DATA, RESPECTIVELY.
* DTH : BATHYMETRY.
*----------------------------------------------------------------------
      OPEN(7,FILE='config.txt',STATUS='UNKNOWN')
      READ(7,*)
      READ(7,'(F10.5)')EFOLD
      READ(7,*)
      READ(7,'(F10.5)')R
      READ(7,*)
      READ(7,'(F10.5)')RMISS
      READ(7,*)
      READ(7,'(A70)')BTHFLE
      READ(7,*)
      READ(7,'(I5)')NFLE
      READ(7,*)
      DO 100 N=1,NFLE
        READ(7,'(A50)')INFLE(N)
        READ(7,'(A50)')OUTFLE(N)
100   CONTINUE      
      DO 110 I=1,NFLE
        WRITE(*,*)'INPUT FILE : '
        WRITE(*,*)INFLE(I)
        WRITE(*,*)'OUTPUT FILE : '
        WRITE(*,*)OUTFLE(I)
        WRITE(*,*)
110   CONTINUE
      WRITE(*,*)
      WRITE(*,*)'E-FOLDING SCALE OF GAUSSIAN FILTER = ',EFOLD,
     +'[degree]'
      WRITE(*,*)'RAIDIUS OF GAUSSIAN FILTER = ',R,'[degree]'
      WRITE(*,*)'MISSING VALUE IS SET TO BE ',RMISS

      OPEN(10,FILE=BTHFLE,STATUS='UNKNOWN')
      READ(10,'(4F10.3)')RL,RR,RB,RT
      READ(10,'(2I5)')NX,NY
      READ(10,'(2F10.5)')RKM,EFDKM
      READ(10,'(F10.5)')D
      DO 190 I=1,NX
        DO 190 J=1,NY
          READ(10,*)XB(I),YB(J),BTH(I,J)
190   CONTINUE
      CLOSE(10)

      DO 200 N=1,NFLE
        OPEN(10,FILE=INFLE(N),STATUS='UNKNOWN')
        WRITE(*,*)'NOW PROCCESSING : ',INFLE(N)
        READ(10,'(2I5)')NX,NY
        READ(10,'(4F10.3)')RL,RR,RB,RT
        READ(10,'(F10.5)')D
        READ(10,'(F10.5)')DEP
        READ(10,'(I10)')NDAT
        DO 300 I=1,NDAT
          READ(10,*)IDATE
          READ(10,*)RLON(I),RLAT(I)
          READ(10,*)T(I),S(I),RHO(I)
300     CONTINUE

      CALL GAUS2D(NX,NY,RL,RB,D,MAXDAT,NDAT,RLON,RLAT,T,MAXX,MAXY
     +           ,TG,EFOLD,R,RMISS,RKM,EFDKM,XB,YB,BTH)
      CALL GAUS2D(NX,NY,RL,RB,D,MAXDAT,NDAT,RLON,RLAT,S,MAXX,MAXY
     +           ,SG,EFOLD,R,RMISS,RKM,EFDKM,XB,YB,BTH)
      CALL GAUS2D(NX,NY,RL,RB,D,MAXDAT,NDAT,RLON,RLAT,RHO,MAXX,MAXY
     +           ,RHOG,EFOLD,R,RMISS,RKM,EFDKM,XB,YB,BTH)
c      CALL MEDLAP(MAXX,MAXY,NX,NY,TG,ITIME,RMISS)
c      CALL MEDLAP(MAXX,MAXY,NX,NY,SG,ITIME,RMISS)

      CALL OUTPUT(OUTFLE(N),NX,NY,RL,RR,RB,RT,D,MAXX,MAXY,TG,SG,RHOG
     +           ,EFDKM,RKM)
200   CONTINUE
      WRITE(*,*)
      STOP
      END


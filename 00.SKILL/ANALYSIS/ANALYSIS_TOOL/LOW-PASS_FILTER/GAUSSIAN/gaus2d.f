***********************************************************************
* gaus2d.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
* REFERENCE                       
***********************************************************************
      SUBROUTINE GAUS2D(NX,NY,RL,RB,D,MAXDAT,NDAT,RLON,RLAT,X,MAXX,MAXY
     +,Y,EFOLD,R,RMISS,EFDKM,RKM,XB,YB,BTH)
*-------------------------- PARAMETERS --------------------------------
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
*[INPUT]

      INTEGER NX,NY
* NX,NY : DIMENSIONS OF COASE FILED.

      REAL RL,RB
* RL,RB : LONGITUDE AND LATITUDE OF LEFT-BOTTOM CORNER(ORIGIN OF COASE
*         FILED).
      REAL D
* D : SIZE OF ONE MESH

      INTEGER MAXDAT,NDAT
* MAXDAT : DIMENSION OF INPUT DATA VARIABLES.
* NDAT : TOTAL NUMBER OF OBSERVATION STATION.

      REAL RLON(MAXDAT),RLAT(MAXDAT)
* RLON,RLAT : LONGITUDE AND LATITUDE OF EACH OBSERVATION STATION.

      REAL X(MAXDAT)
* X : INPUT DATA

      INTEGER MAXX,MAXY
* MAXX,MAXY : DIMENSION OF Y
      REAL*8 Y(MAXX,MAXY)
* Y : OUTPUT DATA

      REAL EFOLD,R
* EFOLD : E-FOLDING SCALE
* R : RADIUS OF THE FILTER

      REAL RMISS
* RMISS : MISSING VALUE.

      REAL XB(MAXX),YB(MAXY),BTH(MAXX,MAXY)
* XB,YB : X AND Y COORDINATE WHICH BATHYMETRIC DATA ARE GIVEN.
* BTH : BATHYMETRIC DATA

*[OUTPUT]
      REAL*8 RKM,EFDKM
* RKM : RADIUS OF FILTER IN KM.
* EFDKM : E-FOLDING SCALE IN KM.
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
      REAL G1D,RT,RR
* G1D : NUMBER OF THE GRID PER ONE DEGREE.
* RT,RR : UPPER AND RIGHT BOUNDS OF COASE FIELD.

      REAL*8 X1,X2,Y1,Y2,DIS
* X1,X2,Y1,Y2,DIS : FOR CALCULATION OF DISTANCE(SUBROUTINE DISTD CALLED 
*                   IN THIS ROUTINE NEEDS DOUBLE PRECISION VARIABLE.)

      REAL*8 GANMA
* GANMA : DENOMINATOR OF EXPONENTIAL FUNCTION IN WEIGHT OF FILTER.

      REAL*8 DENM(MAXX,MAXY)
* DENM : DENOMINATOR OF WEIGHT OF FILTER.
*----------------------------------------------------------------------
      G1D=1.0/D
      RR=RL+FLOAT(NX)*D
      RT=RB+FLOAT(NY)*D
      WRITE(*,*)'SIZE OF ONE MESH = ',D
      WRITE(*,*)'NUMBER OF GRID PER ONE DEGREE = ',G1D
      WRITE(*,*)'BOTTOM BOUND ',RB
      WRITE(*,*)'UPPER BOUND ',RT
      WRITE(*,*)'LEFT BOUND ',RL
      WRITE(*,*)'RIGHT BOUND ',RR
*     --- TRANSERATE UNITS OF RADIUS AND E-FOLDING SCALE INTO 
*         KILOMETERS ---
      X1=DBLE(RL+(RR-RL))
      Y1=DBLE(RB+(RT-RB))
      X2=DBLE(X1+R)
      Y2=DBLE(Y1+R)
      CALL DISTD(X1,Y1,X2,Y2,DIS)
      RKM=DIS
      WRITE(*,*)'RADIUS OF FILTER = ',RKM,'[km]'
      X2=DBLE(X1+EFOLD)
      Y2=DBLE(Y1+EFOLD)
      CALL DISTD(X1,Y1,X2,Y2,DIS)
      EFDKM=DIS
      WRITE(*,*)'E-FOLDING SCALE OF FILTER =',EFDKM,'[km]'
      GANMA=1.0/(EFDKM*EFDKM)

*
* SET WEIGHTS OF GAUSSIAN FILTER
*
*     --- Initailize Y AND DENM---
      DO 90 I=1,NX+1
        DO 90 J=1,NY+1
          Y(I,J)=0.0
          DENM(I,J)=0.0
90    CONTINUE
*     --- Set radius of filter in number of grid ---
      IR=R/D+1
      WRITE(*,*)'RADIUS OF FILTER = ',IR,' [number of grid]'
      WRITE(*,*)'NUMBER OF THE INPUT DATA,NDAT = ',NDAT
      DO 100 N=1,NDAT
*       --- Set the grid point which contains observation point. ---
        XLON=RLON(N)
        IF(RLON(N).LE.0.)XLON=RLON(N)+360.0
        YLAT=RLAT(N)
        I0=INT((XLON-RL)*G1D)+1
        J0=INT((YLAT-RB)*G1D)+1
        IF(I0.LT.1)I0=1
        IF(J0.LT.1)J0=1
        IF(I0.GT.NX)I0=NX
        IF(J0.GT.NY)J0=NY
*debug
*      write(*,*)i0,j0
*debug end
*       --- Set search range ---
        I1=I0-IR
        IF(I0.LE.IR)I1=1
        I2=I0+IR
        IF(I0.GE.NX-IR)I2=NX
        J1=J0-IR
        IF(J0.LE.IR)J1=1
        J2=J0+IR
        IF(J0.GE.NY-IR)J2=NY
*debug
*        write(*,*)i1,i2,j1,j2
*debug end
*       --- Calculate weights of filter ---
        X1=DBLE(RLON(N))
        Y1=DBLE(RLAT(N))
        DO 110 I=I1,I2
          DO 120 J=J1,J2
            X2=DBLE(RL+FLOAT(I-1)*D)
            Y2=DBLE(RB+FLOAT(J-1)*D)
            CALL DISTD(X1,Y1,X2,Y2,DIS)
*debug
*      write(*,*)dis
*debug end
            IF(DIS.GT.RKM)GOTO 120

            IF(X(N).EQ.RMISS)GOTO 120
            IF(BTH(I,J).EQ.0.0)GOTO 120
*debug
*       write(*,*) - ganma*dis*dis
*        write(*,*)i,j
*debug end
            DENM(I,J)=DENM(I,J)+DEXP(-GANMA*DIS*DIS)
            Y(I,J)=Y(I,J)+DBLE(X(N))*DEXP(-GANMA*DIS*DIS)
120       CONTINUE
110     CONTINUE
100   CONTINUE

*
* APPLY GAUSSIAN FILTER
*
      DO 200 I=1,NX
        DO 210 J=1,NY
          IF(DENM(I,J).gt.1.0e-5)THEN
            Y(I,J)=Y(I,J)/DENM(I,J)
          ELSE
            Y(I,J)=RMISS
          END IF
210     CONTINUE
200   CONTINUE

      RETURN
      END

***********************************************************************
* DISTD.F
* CODED BY H.HASE
* MODIFIED BY
* TASK       
*  CALCULATE DISTANCE BETWEEN TWO POINTS ON SPHERE.
* METHOD
* SLAVE SUBROUTINE(S)
* REMARK
* 1)
*              o(Ya,Xa)      N
*             /              ^
*            /               |
*           /                S
*          o(Yb,Xb)
*
* 2) Ya,Xa,Yb,Ya : DOUBLE PRECISION.
* REFERENCE(S)                           
***********************************************************************
      SUBROUTINE DISTD(YA,XA,YB,XB,STDIS)

*-------------------------- PARAMETERS --------------------------------
      parameter(pai=3.14159265359)
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
      REAL*8 YA,XA,YB,XB,STDIS
*[INPUT]
* YA,XA,YB,XB
*[OUTPUT]
* DIS : DISTANCE[km]
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------
      REAL*8 x1,x2,y1,y2,coal1,coal2,coal,alpa
     &                ,xrad,yrad
*----------------------------------------------------------------------

      xrad(x)=(90.-x)/180.*pai
      yrad(y)=y/180.*pai
      r=6370.
      x1    = xrad(xa)
      x2    = xrad(xb)
      y1    = yrad(ya)
      y2    = yrad(yb)
      coal1 = dsin(x1)*dsin(x2)*dcos(y1)*dcos(y2)
      coal2 = dsin(x1)*dsin(x2)*dsin(y1)*dsin(y2)+dcos(x1)*dcos(x2)
      coal  = coal1+coal2
      alpa  = dacos(coal)
      stdis = r*alpa

      RETURN
      end


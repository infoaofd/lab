# Kurihara filter

梅雨低気圧の環境場を調べたときに使った

[1]   Y. Kurihara and M.A. Bender, R.E. Tuley, and R.J. Ross, “Improvements in the GFDL hurricane prediction system,” Mon. Weather Rev., Vol. 123, pp. 2791–2801, 1995.　https://doi.org/10.1175/1520-0493(1995)123%3C2791:IITGHP%3E2.0.CO;2

[2]   H.J. Kwon, S.-H. Won, M.-H. Ahn, Ae-S. Suh, and H.-S. Chung, “GFDL-type typhoon initialization in MM5,” Mon. Weather Rev., Vol. 130, pp. 2966–2974, 2002. https://doi.org/10.1175/1520-0493(2002)130%3C2966:GTTIIM%3E2.0.CO;2

KURIHARA.F90

```fortran
SUBROUTINE KURIHARA(NX,NY,BUF,TMP,UNDEF)
IMPLICIT NONE

INTEGER :: NX, NY
REAL    :: BUF(NX,NY)
REAL    :: TMP(NX,NY)
REAL    :: UNDEF

REAL    :: WAVE(11)=(/2,3,4,2,5,6,7,2,8,9,2/)
INTEGER :: I, J, N
INTEGER :: IP, IM, JP, JM
REAL    :: PI, COEF

PI = 4.*ATAN(1.)

DO N = 1, 11
  COEF = 0.5/(1.0-COS(PI*2./WAVE(N)))
  TMP = UNDEF
  DO J = 1, NY
    DO I = 1, NX
!         IP = MIN(I+4,NX)
!         IM = MAX(I-4,1)
    IP = MIN(I+1,NX)
    IM = MAX(I-1,1)
    IF (BUF(I,J) < UNDEF+1) CYCLE
    IF (BUF(IP,J) < UNDEF+1) CYCLE
    IF (BUF(IM,J) < UNDEF+1) CYCLE
    TMP(I,J) = BUF(I,J) &
&            + COEF*(BUF(IM,J)+BUF(IP,J) - 2.*BUF(I,J))
    END DO !I
  END DO !J
  BUF = TMP

  IF (N > 4) CYCLE

  DO J = 1, NY
    DO I = 1, NX
    IP = MIN(I+1,NX)
    IM = MAX(I-1,1)
    IF (BUF(I,J) < UNDEF+1) CYCLE
    IF (BUF(IP,J) < UNDEF+1) CYCLE
    IF (BUF(IM,J) < UNDEF+1) CYCLE
    TMP(I,J) = BUF(I,J) &
             + COEF*(BUF(IM,J)+BUF(IP,J) - 2.*BUF(I,J))
    END DO !I
  END DO !J
  BUF = TMP

END DO !N

DO N = 1, 11

  COEF = 0.5/(1.0-COS(PI*2./WAVE(N)))
  TMP = UNDEF
  DO J = 1, NY !J
!       JP = MIN(J+4,NY)
!       JM = MAX(J-4,1)
    JP = MIN(J+1,NY)
    JM = MAX(J-1,1)
    DO I = 1, NX !I
      IF (BUF(I,J) < UNDEF+1) CYCLE
      IF (BUF(I,JP) < UNDEF+1) CYCLE
      IF (BUF(I,JM) < UNDEF+1) CYCLE
      TMP(I,J) = BUF(I,J) &
&              + COEF*(BUF(I,JM)+BUF(I,JP) - 2.*BUF(I,J))
    END DO !I
  END DO !J
  BUF = TMP

  IF (N > 4) CYCLE

  DO J = 1, NY
    JP = MIN(J+1,NY)
    JM = MAX(J-1,1)
    DO I = 1, NX
      IF (BUF(I,J) < UNDEF+1) CYCLE
      IF (BUF(I,JP) < UNDEF+1) CYCLE
      IF (BUF(I,JM) < UNDEF+1) CYCLE
      TMP(I,J) = BUF(I,J) &
     &         + COEF*(BUF(I,JM)+BUF(I,JP) - 2.*BUF(I,J))
    END DO !I
  END DO !J
  BUF = TMP

END DO !N

RETURN
END

```


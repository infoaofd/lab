***********************************************************************
* sigtp0.f
*
*  Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
*
* TASK                         
* METHOD
* SLAVE SUBROUTINE
* REMARK
*  PRESSURE = 0.0
* REFERENCE
cj �C�m�ϑ��w�j                       
***********************************************************************
      FUNCTION sigtp0(S,T,error)
*-------------------------- PARAMETERS --------------------------------
      PARAMETER(A0=999.842594,A1=6.793952E-2,A2=-9.095290E-3,
     &          A3=1.001685E-4,A4=-1.120083E-6,A5=6.536332E-9,
     &          B0=8.24493E-1,B1=-4.0899E-3,B2=7.6438E-5,
     &          B3=-8.2467E-7,B4=5.3875E-9,C0=-5.72466E-3,
     &          C1=1.0227E-4,C2=-1.6546E-6,D0=4.8314E-4)
*----------------------------------------------------------------------
*------------------------ COMMON VARIABLES ----------------------------
*----------------------------------------------------------------------
*---------------- DESCRIPTION OF ARGUMENTS  ---------------------------
      REAL S,T
*[INPUT]

*[OUTPUT]
*----------------------------------------------------------------------
*----------------- LOCAL VARIABLES IN THIS SUBROUTINE -----------------

      REAL RHOW,RHOZR
*----------------------------------------------------------------------

      RHOW=A0+A1*T+A2*T**2+A3*T**3+A4*T**4+A5*T**5
      RHOZR=RHOW+(B0+B1*T+B2*T**2+B3*T**3+B4*T**4)*S
     &     +(C0+C1*T+C2*T**2)*S**1.5+D0*S**2
      SIGtp0=RHOZR-1000.0

      if(t.eq.error.or.s.eq.error)then
      sigtp0=999.0
      end if

      RETURN
      END

#!/bin/sh

EXE="a.out"

if [ $# == 0 ]; then
  echo Error: No argument
  echo Usage: $0 FILENAME
  echo
  exit 1
fi

IN=$1

${EXE} $IN


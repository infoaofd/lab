C***********************************************************************
C
C
C
C***********************************************************************
C------------------------- DECLARATION ---------------------------------
C      implicit none
      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
      include 'param.h'
C-------------------------- PARAMETERS ---------------------------------
      PARAMETER(NTIDE=2,NFUNC=2*NTIDE+1)
C PI
      PARAMETER(PI=3.14159265358979323846D0)
C D2R: degree to radian
      PARAMETER(D2R=2.0D0*PI/360.0D0)
C R2D: radian to degree
      PARAMETER(R2D=180.0D0/PI)
C------------------------- COMMON VARIABLES ----------------------------
C      common //
C------------------------- LOCAL VARIABLES -----------------------------
C      double precision 
C      real
C      integer 
      character progname*70
      character infle*70,ofle*70,oflef*72,oflehit*72
      character tsfle(mxfle)*70
      CHARACTER STRM*100, strm2*100, fname*59
      dimension iyr(mxfle),imo(mxfle),idy(mxfle)
     $,ih(mxfle),im(mxfle),is(mxfle), etime(mxfle)
      dimension nz(mxfle)
      dimension z(mxfle,mz), t(mxfle,mz), s(mxfle,mz), sigma(mxfle,mz)

C Print date, hostname, cwd, and username
      character dirinfo*144, userinfo*72, hostinfo*72
      INTEGER date_time(8)
      CHARACTER REAL_CLOCK(3)*12

C For depth correction
      character slfile*80
      double precision u0, A1, A2, kp1, kp2
C SGMA must be in **radian**.
       double precision SGMA(NTIDE)
      dimension sealevel(mxfle) !Sealevel measured from the bottom
      dimension hab(mxfle,mz) ! Hight above the bottom
C For time series of estimated sea level
      character ts_sl_file*80
C For masking the redundant data above the sea surface
      character mask_file*80
C For 1-2-1 filter
      dimension tf(mxfle,mz), sf(mxfle,mz), sigmaf(mxfle,mz)
C For vertical interpolation
      dimension habit(mz) !Height above the bottom
      dimension tit(mxfle,mz),sit(mxfle,mz),sigmait(mxfle,mz)
C-----------------------------------------------------------------------

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--

C Check number of command line arguments
      nargs = iargc() 
c One at a time, get an argument and write it out 
      if(nargs.lt.2)then
        goto 8000 ! Error handling
      end if
      call getarg( 0, progname)
      call getarg( 1, infle )
      call getarg( 2, ofle)
      idx=index(ofle,'.txt')
      write(oflef(1:idx),'(A)') ofle(1:idx)
      write(oflef(idx:),'(A)') '_f.txt'

      write(oflehit(1:idx),'(A)') ofle(1:idx)
      write(oflehit(idx:),'(A)') '_hit.txt'

      write(*,'(A)')progname
      write(*,'(A)')'input: '
      write(*,'(A)')infle
      write(*,'(A)')'output: '
      write(*,'(A)')ofle
      write(*,'(A)')oflef
      write(*,'(A)')oflehit


C INPUT FILE
      open(10,file=infle,status='old')
C OUTPUT FILE
      open(20,file=ofle)
      open(21,file=oflef)
      open(22,file=oflehit) ! vertically interpolated data


C
C Read Data
C
      i=1
100   read(10,'(a100)',END=9000)STRM
        IF(STRM(1:1).EQ.'#')GOTO 100
        read(STRM,*) fname
        write(tsfle(i)(1:11),'(A11)') '../01.Data/'
        write(tsfle(i)(12:),'(A)') fname

CD        write(*,*)tsfle(i)

        i=i+1
        goto 100
9000  NDAT=i-1
      close(10) 
      write(*,*) 'NDAT=', NDAT
C      write(,'()')
C      write(*,*)

      do n=1,NDAT
        open(11,file=tsfle(n),err=8010)
CD
c        write(*,*)tsfle(n)
CD
        do i=1,12
          read(11,*) !skip header lines
        end do !i
        read(11,'(10x,i4,1x,i2,1x,i2)') iyr(n),imo(n),idy(n)
C                                       !year,month,day
        read(11,'(10x,i2,1x,i2,1x,i2)') ih(n),im(n),is(n)
C                                       !hour, minute, second

        do i=1,10
          read(11,*) !skip header lines
        end do !i

        k=1
200     read(11,'(a100)',END=250)strm2
          read(strm2,*)
     $    z(n,k), t(n,k), s(n,k), cond, ec25, den, sigma(n,k)
          k=k+1
        goto 200

250     nz(n)=k-1

        close(11)

      end do !n

C
C Depth correction
C
      slfile='HCSealevel070707.txt'
      write(*,'(A)')'Estimates of Sea Level:'
      write(*,'(A)')slfile
      open(13,file=slfile,err=8020)
      nhead = 12
      do i=1,nhead
        read(13,*)
      end do
      read(13,'(9x,f10.5)')u0
      read(13,'(9x,f10.5)')A1
      read(13,'(9x,f10.5)')A2
      read(13,'(9x,f10.5)')kp1
      read(13,'(9x,f10.5)')kp2
      close(13)
      write(*,'(A,f10.5)')'u0 =',u0
      write(*,'(A,f10.5)')'A1 =',A1
      write(*,'(A,f10.5)')'A2 =',A2
      write(*,'(A,f10.5)')'kp1=',kp1
      write(*,'(A,f10.5)')'kp2=',kp2
C Period
      T1=12.+25./60.
      T2=24.+50./60.
C
C Estimate heigh above the bottom using the result of harmonic analysis
C
      SGMA(1)=360.D0/T1*D2R !M2 (semi-diurnal)
      SGMA(2)=360.D0/T2*D2R !M1 (diurnal)

      do n=1,NDAT
        etime(n)=(dble(idy(n))-7.d0)*24.d0 + dble(ih(n)) 
     $      + (dble(im(n))/60.d0) + (dble(is(n))/3600.d0)

        do k=1,nz(n)
          sealevel(n)=u0
     $     + A1*cos(SGMA(1)*etime(n) - kp1*D2R)
     $     + A2*cos(SGMA(2)*etime(n) - kp2*D2R)
          hab(n,k)=sealevel(n) - z(n,k)
        end do !k
      end do !n

C
C Apply 1-2-1 filter
C
      call filter121(mxfle,mz,NDAT,nz,t,tf)
      call filter121(mxfle,mz,NDAT,nz,s,sf)
      call filter121(mxfle,mz,NDAT,nz,sigma,sigmaf)

C
C vertical interpolation
C
      dz=1.0
      nzit=15
      do k=1,nzit
        habit(k)=dble(k)*dz
      end do !k
      call vert_intpl2(NDAT,nz,nzit,hab,sf,habit,sit)
      call vert_intpl2(NDAT,nz,nzit,hab,tf,habit,tit)
      call vert_intpl2(NDAT,nz,nzit,hab,sigmaf,habit,sigmait)



CD     $(mxiout,mxlayer,iout,nofile,nz,habraw,veraw,vnraw,
CD     $habit,veit, vnit)

C
C Output Data
C

C
C Contour
C
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C   *************
      iunit=20
C   *************
      call print_header(iunit)

      write(iunit,'(A)')'# TIME-DEPTH PLOT OF RAW DATA'
      write(iunit,'(A)')'# etime, hab, z, t, s, sigma, yr, mo, dy, hr, mi, 
     $sec'

      do n=1,NDAT
        do k=1,nz(n)

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
          write(iunit,'(f12.6, f8.2,f7.1, f9.2, f10.3, f11.3, i5,5i3)')
     $etime(n)
     $   ,hab(n,k)
     $   ,z(n,k), t(n,k), s(n,k),sigma(n,k)
     $   ,iyr(n),imo(n),idy(n), ih(n),im(n),is(n)
        end do !k
      end do !n
      close(iunit)

C
C Contour of filtered data
C
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C   *************
      iunit=21
C   *************
      call print_header(iunit)
      write(iunit,'(A)')'# TIME-DEPTH PLOT OF FILTERED DATA'
      write(iunit,'(A)')'# etime, hab, z, t, s, sigma, yr, mo, dy, hr, mi, 
     $sec'

      do n=1,NDAT
        do k=1,nz(n)
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
          write(iunit,'(f12.6, f8.2,f7.1, f9.2, f10.3, f11.3, i5,5i3)')
     $etime(n)
     $   ,hab(n,k)
     $   ,z(n,k), tf(n,k), sf(n,k),sigmaf(n,k)
     $   ,iyr(n),imo(n),idy(n), ih(n),im(n),is(n)
        end do !k
      end do !n
      close(iunit)

C
C Contour of interpolated data
C
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C   *************
      iunit=22
C   *************
      call print_header(iunit)
      write(iunit,'(A)')'# TIME-DEPTH PLOT OF VERTICALLY INTERPOLATED DA
     $TA'
      write(iunit,'(A)')'# etime, hab, t, s, sigma, yr, mo, dy, hr, mi, 
     $sec'

      do n=1,NDAT
        do k=1,nzit !nz(n)
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
          write(iunit,'(f12.6, f8.2, f9.2, f10.3, f11.3, i5,5i3)')
     $etime(n)
     $   ,habit(k)
     $   ,tit(n,k), sit(n,k),sigmait(n,k)
     $   ,iyr(n),imo(n),idy(n), ih(n),im(n),is(n)
        end do !k
      end do !n
      close(iunit)

C
C Time series of sea level
C
      ts_sl_file='ts_sealevel.txt'
      write(*,'(A)')'time series of estimated sea level:'
      write(*,'(A)')ts_sl_file
      open(21,file=ts_sl_file)
      do n=1,NDAT
        write(21,'(f12.6,f7.2)')etime(n),sealevel(n)
      end do
      close(21)

C
C Mask data
C
      mask_file='mask.txt'
      write(*,'(A)')'Mask data:'
      write(*,'(A)')mask_file
      open(21,file=mask_file)
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      write(21,'(A)')'# For masking the redundant data above the sea sur
     $face'
      do n=1,NDAT
        write(21,'(f12.6,f7.2)')etime(n),sealevel(n)
      end do
      tmin=0
      hmax=15.
      write(21,*)etime(NDAT),hmax
      write(21,*)tmin,hmax
      write(21,*)tmin,sealevel(1)
      close(21)


      stop


C
C Error handling
C
8000  write(*,'(A)')'Error: Wrong number of arguments.'
      write(*,'(A)')'Program terminated.'
      write(*,*)
      stop

8010  write(*,'(A)')'Error: No such file.'
      write(*,'(A)')tsfle(n)
      write(*,'(A)')'Program terminated.'
      write(*,*)
      stop

8020  write(*,'(A)')'Error: No such file.'
      write(*,'(A)')slfile
      write(*,'(A)')'Program terminated.'
      write(*,*)
      stop



C---+----1----+----2----+----3----+----4----+----5----+----6----+----7--

      stop
      end

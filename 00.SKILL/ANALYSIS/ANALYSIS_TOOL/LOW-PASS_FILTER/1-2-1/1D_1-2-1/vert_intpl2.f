C***********************************************************************
C .f
C NOTE
C***********************************************************************
      subroutine vert_intpl2(NDAT,nz,nzit,hab,var,habit,varit)
C------------------------- DECLARATION ---------------------------------
C      implicit none
      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
      include 'param.h'
C-------------------------- PARAMETERS ---------------------------------
C      parameter()
C------------------------- COMMON VARIABLES ----------------------------
C      common //
C----------------------------ARGUMENTS ---------------------------------
      integer NDAT,nz(mxfle)
      integer nzit
C iout: # of data in time
C nofile: # of input files (= number of bins)
C nz: # of output layers
      dimension var(mxfle,mz)   !Raw data
      dimension hab(mxfle,mz)
      dimension varit(mxfle,mz) !Interpolation
      dimension habit(mz)
C------------------------- LOCAL VARIABLES -----------------------------
C-----------------------------------------------------------------------
      do i=1,NDAT

C N.B.
C    k: positive UPward (output data)
C    n: positive DOWNward (input data)
C    (confusing a little)

        do k=1,nzit
C Fill Missing values first
          varit(i,k)=RMISS

          do n=nz(i), 2, -1
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
            if(hab(i,n).le.habit(k).and.
     $      habit(k).lt.hab(i,n-1))then !c1
              v_up=var(i,n-1)
              v_lw=var(i,n)
              h_up=hab(i,n-1)
              h_lw=hab(i,n)

              if(v_up.ne.RMISS .and. v_lw.ne.RMISS)then !c2
C           Interpolation (v)
                 y2=v_up
                 y1=v_lw
                 x2=h_up
                 x1=h_lw
                 x=habit(k)
                 y = (y2-y1)/(x2-x1)*(x-x1)+y1
                 varit(i,k)=y
CD
                if(i.eq.50.and.k.eq.1)then
                  write(*,*)'DEBUG'
                  write(*,*)x1,x2
                  write(*,*)x
                  write(*,*)y1,y2
                  write(*,*)y
                end if
CD
              end if !c2

            end if !c1

          end do !n
        end do !k

      end do !i

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--

      return
      end

#!/bin/sh

gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16
gmtset ANOT_FONT_SIZE 16
gmtset TICK_PEN 4
gmtset BASEMAP_TYPE PLAIN

VAR=S

RANGE2=5.5/26.5/0/15

RANGE=6/26.05/0/15
SIZE=5/3.5

if [ $# == 0 ]; then
  echo Error: No argument
  echo Usage: $0 FILENAME
  echo
  exit 1
fi
IN=$1
IN_F=`echo $IN | sed -e "s/\.txt/_hit.txt/"`
OUT=`echo $IN | sed -e "s/\.txt/.ps/"`

if [ ! -f $IN ]; then
  echo Error: No Input file, $IN
  echo
  exit 1
fi
if [ ! -f $IN_F ]; then
  echo Error: No Input file, $IN_F
  echo
  exit 1
fi

OUT="${VAR}_"${OUT}

echo INPUT
echo $IN
echo $IN_F
echo OUTPUT
echo $OUT
echo

#----------------------------------------------------------------------
#
# Raw data
#
#makecpt -Csealand -T28.0/33.0/0.5 > cv_cpt.txt
makecpt -Csealand -T10.0/30.0/0.5 > cv_cpt.txt

awk '{if ($1 != "#") print $1, $2, $5}' $IN | \
blockmedian -I0.5/0.5 -R$RANGE2 > u.xyz

surface u.xyz -I0.1/0.1 -T1 -R$RANGE2 -Gu.grd
#psmask u.xyz -I0.1/1 -JX4.8/2.8163 -R \
# -Ba3f1:"hour":/a5f1:"height above the bottom [m]"::."Ariake Stn.4; 1600 23 Apr.-":WSNe \
# -X1.5 -Y6 -P -K \
# > $OUT
grdimage u.grd \
 -R$RANGE -JX$SIZE \
 -X1 -Y1 -P -K \
 -Ccv_cpt.txt \
 > $OUT
grdcontour u.grd -R -JX -A1f10 -C.5/255/255/255 -O -K  >> $OUT

#
# Masking the redundant data above the sea surface
#
IN2='mask.txt'
awk '{if ($1 != "#") print $1, $2}' $IN2 | \
psxy -R -JX -G255 -O -K >> $OUT

# Sealevel
IN3='ts_sealevel.txt'
awk '{if ($1 != "#") print $1, $2}' $IN3 | \
psxy -R -JX -W4 -O -K >> $OUT

pstext << END -R -JX -O -K \
-Ba3f1:"time [hr]":/a5f1:"Hight above the bottom [m]"::."Ariake Stn. P; Jul 7th, 2007":WSne \
 >> $OUT
10.5 24 16 0 5 CM SALINITY
END

#
# Measurement time
#
IN4='ts_sealevel.txt'
awk '{if ($1 != "#") print $1, 15.0}' $IN4 | \
psxy -JX -R -Si0.07 -G0 \
-Y0.07 \
-O -K >> $OUT

psscale -D5.2/2.2/4/0.2 -Ccv_cpt.txt -B2:saliniy:/::  -O -K \
 >> $OUT
#----------------------------------------------------------------------


#----------------------------------------------------------------------
#
# Interpolated data
#
awk '{if ($4 != "-999.000") print $1, $2, $4}' $IN_F | \
blockmedian -I0.5/0.5 -R$RANGE2 > u.xyz

surface u.xyz -I0.1/0.1 -T1 -R$RANGE2 -Gu.grd

grdimage u.grd \
 -R$RANGE -JX$SIZE \
 -Y5 -O -K \
 -Ccv_cpt.txt \
 >> $OUT
grdcontour u.grd -R -JX -A1f10 -C.5/255/255/255 -O -K  >> $OUT

#
# Masking the redundant data above the sea surface
#
IN2='mask.txt'
awk '{if ($1 != "#") print $1, $2}' $IN2 | \
psxy -R -JX -G255 -O -K >> $OUT

# Sealevel
IN3='ts_sealevel.txt'
awk '{if ($1 != "#") print $1, $2}' $IN3 | \
psxy -R -JX -W4 -O -K >> $OUT

pstext << END -R -JX -O -K \
-Ba3f1:"time [hr]":/a5f1:"Hight above the bottom [m]"::."Ariake Stn. P; Jul 7th, 2007":WSne \
 >> $OUT
10.5 24 16 0 5 CM SALINITY
END

#
# Measurement time
#
IN4='ts_sealevel.txt'
awk '{if ($1 != "#") print $1, 15.0}' $IN4 | \
psxy -JX -R -Si0.07 -G0 \
-Y0.07 \
-O -K >> $OUT

psscale -D5.2/2.2/4/0.2 -Ccv_cpt.txt -B2:saliniy:/::  -O -K \
 >> $OUT
#----------------------------------------------------------------------




currentdir=`pwd`
if [ ${#currentdir} -gt 100 ]; then
  curdir1=${currentdir:1:100}
  curdir2=${currentdir:101}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
pstext -JX6/1 -R0/1/0/1 -N -Y4.5 << EOF -O >> $OUT
0 0.9   9 0 1 LM $0 $@
0 0.75  9 0 1 LM ${now}
0 0.6   9 0 1 LM ${host}
0 0.45  9 0 1 LM ${curdir1}
0 0.3   9 0 1 LM ${curdir2}
0 0.15  9 0 1 LM ${IN}    ${IN_F}    ${OUT}
EOF

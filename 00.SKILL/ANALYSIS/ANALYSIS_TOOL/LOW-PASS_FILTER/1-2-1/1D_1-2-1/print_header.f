C***********************************************************************
C print_header
C NOTE
C***********************************************************************
      subroutine print_header(iunit)

C------------------------- DECLARATION ---------------------------------
C      implicit none
C      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
C-------------------------- PARAMETERS ---------------------------------
C------------------------- COMMON VARIABLES ----------------------------
C----------------------------ARGUMENTS ---------------------------------
      integer iunit
C------------------------- LOCAL VARIABLES -----------------------------
C Print date, hostname, cwd, and username
      character dirinfo*144, userinfo*72, hostinfo*72
      INTEGER date_time(8)
      CHARACTER REAL_CLOCK(3)*12

      character progname*100
      integer idxpn, idx
C-----------------------------------------------------------------------
C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
C Get date, time, current directory, username, and hostname
C p.345 of for_lang.pdf (intel fortran user manual)
C DATE_AND_TIME is an intrisic S/R and works on most of Fotran 90 
C Compilers. g77 also supports this S/R.
      CALL DATE_AND_TIME (REAL_CLOCK (1), REAL_CLOCK (2), 
     $    REAL_CLOCK (3), date_time)
      call getenv("PWD", dirinfo)
      call getenv("USER", userinfo)
      call getenv("HOST", hostinfo)

      write(iunit,'(a, 
     $ i2.2,a,i2.2,a,i4.4, a,i2.2,a,i2.2,a,i2.2, a,i3.2,a)')
     $  '# Date and time: ',
     $  date_time(2),'/',date_time(3),'/',date_time(1),
     $  ' at ',date_time(5),':',date_time(6),':',date_time(7),
     $  ' ',-date_time(4)/60,':00'
      write(iunit,'(A,A)')'# hostname: ',hostinfo

      idx=index(dirinfo,' ')
      if(idx.le.80)then
        write(iunit,'(A,A)')'# cwd: ',dirinfo
      else
        write(iunit,'(A,A)')'# cwd: ',dirinfo(1:72)
        write(iunit,'(A,A)')'# ',dirinfo(73:)
      end if
      write(iunit,'(A,A)')'# user: ',userinfo

C Get program name
      call getarg( 0, progname)
      idxpn=index(progname,' ')-1
C      write(*,'(A)')'Program name: '
C      write(*,'(A)')progname(1:idxpn)


      write(iunit,'(A,A)')'# program name: ',progname(1:idxpn)

CD      idxrn=index(run_name,' ')-1
CD      write(iunit,'(A,A)')'# run name: ',run_name(1:idxrn)
CD

C---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
      return
      end


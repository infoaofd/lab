program oisst_warm_wps

  ! constant output variables (need configure)
  integer, parameter :: version = 5  ! Format version (must =5 for WPS format) 
  integer, parameter :: nx = 1440    ! x-dimensions of 2-d array
  integer, parameter :: ny = 720     ! y-dimensions of 2-d array 
  integer, parameter :: iproj = 0    ! Code for projection of data in array: 
    !  0 = cylindrical equidistant  (either global or regional)
    !  1 = Mercator (regional only)
    !  3 = Lambert conformal conic (regional only)
    !  4 = Gaussian (global only!) 
    !  5 = Polar stereographic (regional only)
  real, parameter :: nlats = 0.  ! Number of latitudes north of equator (for Gaussian grids) 
  real, parameter :: xlvl     = 200100.   ! Vertical level of data in 2-d array (200100 for surface, 201300 for SLP)
  real, parameter :: startlat = -89.875 ! Lat of point in array indicated by startloc string
  real, parameter :: startlon =   0.25  ! Lon of point in array indicated by startloc string
  real, parameter :: deltalat = 0.25 ! Grid spacing, degrees 
  real, parameter :: deltalon = 0.25 ! Grid spacing, degrees 
  real, parameter :: dx = 0. ! x-grid spacing, km
  real, parameter :: dy = 0. ! y-grid spacing, km 
  real, parameter :: xlonc = 0.  ! Standard longitude of projection 
  real, parameter :: truelat1 = 0. ! True latitudes of projection 
  real, parameter :: truelat2 = 0.! True latitudes of projection 
  real, parameter :: earth_radius = 6367470. * .001  ! Earth radius, km 
  logical, parameter :: is_wind_grid_rel = .false. ! Flag indicating whether winds are    
    !  relative to source grid (TRUE) or  
    !  relative to earth (FALSE) 
  character(len=8), parameter  :: startloc = 'SWCORNER' ! Which point in array is given by  
    !  startlat/startlon; set either     
    !  to 'SWCORNER' or 'CENTER  ' 
  character(len=9), parameter  :: field = 'SST' ! Name of the field 
  character(len=25) :: units = 'K' ! Units of data 
  character(len=32) :: map_source = 'OISST' !  Source model / originating center 
  character(len=46) :: desc = 'SST' ! Short description of data 
  real, parameter :: sst_intv = 24. ! input SST interval hours

  ! dynamic output variables
  real :: xfcst   ! Forecast hour of data (hours from ifile1)
  real, dimension(nx, ny) :: isst1, isst2, osst ! The 2-d array holding the data 
  character(len=24) :: hdate ! Valid date for data YYYY:MM:DD_HH:00:00 

  ! other variables
  character(256) :: ifile1, ifile2, ofile
  character(80) :: cxfcst ! String of xfcst
  integer, parameter :: iunit1 = 10, iunit2 = 20, ounit = 100
  character(500)::out_check,out_ctl
  integer yyyy,mm,dd,hh
  character(3)::cmm(12)
  data cmm/'jan','feb','mar','apr','may','jun','jul','aug',&
&      'sep','oct','nov','dec'/

  real   ,parameter::fillvalue=-999.0

  ! interplation in time and extrapolation in space
  real,dimension(nx,ny) ::itsst
  integer,parameter :: nw=3

  real,dimension(nx,ny) ::smooth
  logical :: cyclic = .false.

  real,dimension(nx)::rlon
  real,dimension(ny)::rlat
  real,dimension(nx,ny)::land
  !-------------------------------------------

  namelist /para/ifile1,ifile2,ofile,hdate,cxfcst,&
& out_check,out_ctl,yyyy,mm,dd,hh, &
&  slon, elon,  slat, elat

  read(*,para)

  read(cxfcst, *) xfcst
  print *, trim(ofile)

  ! read input sst
  open(iunit1, file=ifile1, form='binary', access='sequential',action="read")
  open(iunit2, file=ifile2, form='binary', access='sequential',action="read")
  
  read(iunit1) isst1
  read(iunit2) isst2
   
  close(iunit1)
  close(iunit2)

  ! interpolate sst in time
  itsst(:,:) = (isst1(:,:)+273.15)*(sst_intv-xfcst)/sst_intv + (isst2(:,:)+273.15)*xfcst/sst_intv
  where(itsst <= -10.0) itsst = fillvalue
  
!  print *, isst1(1201,521), isst2(1201,521), itsst(1201,521) 

  ! Set land
  land(:,:)=0.0
  where(itsst == fillvalue)land=1.0

  ! Set lon and lat
  do i=1,nx
    rlon(i)=   0.125+0.25*float(i)
  enddo !i
  do j=1,ny
    rlat(j)= -89.875+0.25*float(j)
  enddo !j

  ! Smoothing
   call make_smooth(nx,ny,rlon,rlat, &
     &  slon, elon,  slat, elat,            &
     &                 itsst,smooth,land,cyclic)

  ! do extrapolation using spline technique

  call my_near_neighbor(smooth,osst,nx,ny,nw,fillvalue)

  ! write data
  open(ounit, file=ofile, form='unformatted', access='sequential')

  !  1) WRITE FORMAT VERSION 
  write(unit=ounit) version 

  !  2) WRITE METADATA
  write(unit=ounit) hdate, xfcst, map_source, field, & 
                    units, desc, xlvl, nx, ny, iproj 

  ! Cylindrical equidistant 
  if (iproj == 0) then 
        write(unit=ounit) startloc, startlat, startlon, & 
                          deltalat, deltalon, earth_radius 
  ! Mercator 
  else if (iproj == 1) then 
        write(unit=ounit) startloc, startlat, startlon, dx, dy, & 
                          truelat1, earth_radius 
  ! Lambert conformal 
  else if (iproj == 3) then 
        write(unit=ounit) startloc, startlat, startlon, dx, dy, & 
                          xlonc, truelat1, truelat2, earth_radius 
  ! Gaussian 
  else if (iproj == 4) then 
        write(unit=ounit) startloc, startlat, startlon, & 
      nlats, deltalon, earth_radius 
  ! Polar stereographic 
  else if (iproj == 5) then 
        write(unit=ounit) startloc, startlat, startlon, dx, dy, & 
                          xlonc, truelat1, earth_radius 
  end if 
     
  !  3) WRITE WIND ROTATION FLAG  
  write(unit=ounit) is_wind_grid_rel 
   
  !  4) WRITE 2-D ARRAY OF DATA 
  write(unit=ounit) osst
  
  close(ounit)

! Check data
  open(20, file=out_check, form='binary', access='sequential')
  write(20)osst
  close(20)

  open(20, file=out_ctl)
  write(20,'(A)')'DSET ^%y4%m2%d2_%h2.dat'
  write(20,'(A)')'TITLE NOAA OISST 1/4 degree using only &
&AVHRR downloaded from noaa.'
  write(20,'(A,f10.1)')'UNDEF ',fillvalue
  write(20,'(A)')'OPTIONS TEMPLATE big_endian'
  write(20,'(A)')'XDEF 1440 linear 0.125 0.25'
  write(20,'(A)')'YDEF  720 linear -89.875 0.25'
  write(20,'(A)')'ZDEF    1 linear 0 1'
  write(20,'(A,i2.2,A,i2.2,A3,i4.4,A)')&
&   'TDEF 11000 linear ',00,'Z',dd,cmm(mm),yyyy,' 6hr'
  write(20,'(A)')'VARS 1'
  write(20,'(A)')'sst 1 -999 sea surface temparature'
  write(20,'(A)')'ENDVARS'
  close(20)



end program oisst_warm_wps

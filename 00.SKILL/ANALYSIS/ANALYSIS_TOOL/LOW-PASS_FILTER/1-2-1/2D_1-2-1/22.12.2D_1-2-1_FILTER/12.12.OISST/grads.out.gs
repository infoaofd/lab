
function grads_out( args )
  _version='0.1'

*
* Decode options
*
  i = 1
  while( 1 )
    arg = subwrd( args, i )
    i = i + 1;
    if( arg = '' ); break; endif

    while( 1 )
      if( arg = '-i' ); input =  subwrd(args,i); i=i+1; break; endif
      if( arg = '-d' ); yyyymmdd = subwrd(args,i); i=i+1; break; endif
      if( arg = '-j' ); julday = subwrd(args,i); i=i+1; break; endif
      if( arg = '-o' ); output = subwrd(args,i); i=i+1; break; endif
      if( arg = '-q' ); flagquit=1; break; endif
      say 'syntax error : 'arg
*      usage()
      return
    endwhile
  endwhile

  if (input = '')
    say 'Please enter input file name'
    say 'Terminated.'
    say ''
    return
  endif

  if (yyyymmdd = '')
    say 'Please enter yyyymmdd'
    say 'Terminated.'
    say ''
    return
  endif

  if (julday = '')
    say 'Please enter julian day'
    say 'Terminated.'
    say ''
    return
  endif

  if (output = '')
    say 'Please enter output file name'
    say 'Terminated.'
    say ''
    return
  endif

*
*  OPEN CONTROL FILE
*

say; say 'Input: 'input ;say
'sdfopen 'input

say 'Date: 'yyyymmdd ;say


'set gxout fwrite'
'set UNDEF -999'      ;* <- GrADS 2.0.a6以降の場合あった方がいいかも

'set fwrite -be 'output ;* -be=big endian output

'set t ' julday

'set x 1 1440'
'set y 1 720'
'set z 1'

'd sst'

'disable fwrite'




say 'Output: 'output; say

if ( flagquit = 1)
 quit
endif

return

#!/bin/sh


syyyy=2014
smm=02
sdd=12

days=4

dh=6

exe=oisst.warm_wps.exe

para=para.$(basename $0).txt

# Smoothing area
slon=140.0
elon=180.0
slat=35.0
elat=39.0


indir=./OISST
outdir=./intm
if [ ! -d $outdir ]; then
  mkdir -vp $outdir
fi
outdir_check=./out.check
if [ ! -d $outdir_check ]; then
  mkdir -vp $outdir_check
fi

d=0
while [ $d -le $days ]; do

d1=$d
d2=$(expr $d1 + 1)

yyyymmdd1=$(date -d"${syyyy}${smm}${sdd} $d1 day" '+%Y%m%d')
yyyymmdd2=$(date -d"${syyyy}${smm}${sdd} $d2 day" '+%Y%m%d')

yyyy1=${yyyymmdd1:0:4}
mm1=${yyyymmdd1:4:2}
dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}

#echo $yyyymmdd1
#echo $yyyy1
#echo $mm1
#echo $dd1
#echo $yyyymmdd2
#echo

h=0
while [ $h -lt 24 ]; do

ifile1=${indir}/${yyyymmdd1}.grd
ifile2=${indir}/${yyyymmdd2}.grd
hh=$(printf "%02d" $h)
ofile="${outdir}/SST:${yyyy1}-${mm1}-${dd1}_${hh}"
hdate="${yyyy1}-${mm1}-${dd1}-${hh}:00:00"
cxfcst=${hh}
out_check=${outdir_check}/${yyyy1}${mm1}${dd1}_${hh}.dat
out_ctl=${outdir_check}/${yyyy1}${mm1}${dd1}.ctl

#echo ifile1="${ifile1}"
#echo ifile2="${ifile2}"
#echo ofile="${ofile}"
#echo hdate="${hdate}"
#echo xfcst="${xfcst}"
#echo 

now=$(date)
cwd=$(pwd)
host=$(hostname)
user=$(whoami)

cat <<EOF > $para
!
! namelist for ${exe}
!
! $host
! $cwd
! $user
! $now
&para
ifile1="${ifile1}",
ifile2="${ifile2}",
ofile="${ofile}",
hdate="${hdate}",
cxfcst="${cxfcst}",
out_check="${out_check}",
out_ctl="${out_ctl}",
yyyy=$yyyy1,
mm=$mm1,
dd=$dd1,
hh=$hh,
slon=${slon},
elon=${elon},
slat=${slat},
elat=${elat},
&end
EOF

h=$( expr $h + $dh )


$exe < $para


done # h

d=$(expr $d + 1)

done #d

exit 0

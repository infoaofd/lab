! /work09/am/00.WORK/2024.CYCLONE201402/12.OISST.Smooth.K.EXT/make_smooth.f90
! C:\Users\foome\lab\00.SKILL\ANALYSIS\ANALYSIS_TOOL\LOW-PASS_FILTER\1-2-1\2D_1-2-1\22.12.2D_1-2-1_FILTER\12.12.OISST
!
! 1-2-1 FILTER
!
subroutine make_smooth(nlon,nlat,rlon,rlat, &
     &  slon, elon,  slat, elat,            &
     &  sst,  smooth,land, cyclic)

      implicit none
      logical :: cyclic
      integer :: nlon, nlat
      real    :: rlon(nlon)
      real    :: rlat(nlat)
      real    :: sst(nlon,nlat)
      real    :: smooth(nlon,nlat)
      real    :: land(nlon,nlat)

      real   , dimension(:,:), allocatable :: buf

      integer :: num
      real    :: sum
      real    :: res1
      real    :: res2
      integer :: num_itr
      integer :: i, j, ii, jj, ic, jc, itr

      integer, parameter :: inc = 1
      integer, parameter :: max_itr = 200
      real   , parameter :: res_crt = 1.e-3

! ... copy sst to work array
      allocate(buf(nlon,nlat))
      buf = sst

      num_itr = 0
      res1 = 0
      res2 = 0

      do itr = 1, max_itr
      num_itr = num_itr + 1

      smooth = buf

      do j = 1, nlat
      do i = 1, nlon

      if (rlon(i) < slon) cycle
      if (rlon(i) > elon) cycle
      if (rlat(j) < slat) cycle
      if (rlat(j) > elat) cycle

      if (land(i,j) > 0.5) cycle

        num = 0
        sum = 0
        do jj = j-inc, j+inc
          if (jj < 1 .or. jj > nlat) cycle
          jc = jj

          do ii = i-inc, i+inc
            ic = ii

            if (ic < 1) then
              if (cyclic) then
                ic = ic + nlon
              else
                cycle
              end if
            end if

            if (ic > nlon) then
              if (cyclic) then
                ic = ic - nlon
              else
                cycle
              end if
            end if

            if (land(ic,jc) > 0.5) cycle

            num = num + 1
            sum = sum + buf(ic,jc)

          end do
        end do

        if (num > 1) then
          smooth(i,j) = sum/num
        end if

      end do
      end do

      res2 = 0
      num  = 0
      do j = 1, nlat
      do i = 1, nlon
      if (rlon(i) < slon) cycle
      if (rlon(i) > elon) cycle
      if (rlat(j) < slat) cycle
      if (rlat(j) > elat) cycle

      if (land(i,j) > 0.5) cycle

      res2 = res2 + (sst(i,j) - smooth(i,j))**2.0
      num  = num  + 1
      end do
      end do

      res2 = sqrt(res2/num)
      if ( abs(res2-res1) < res_crt ) exit
      if ( itr >= max_itr ) exit
      res1 = res2
      buf = smooth
      end do

      deallocate(buf)
      print *,' check : ',num_itr,res1,res2,res2-res1

!  write(*,'(a)')'Done subroutine make_smooth.'
!  write(*,*) 
end subroutine make_smooth

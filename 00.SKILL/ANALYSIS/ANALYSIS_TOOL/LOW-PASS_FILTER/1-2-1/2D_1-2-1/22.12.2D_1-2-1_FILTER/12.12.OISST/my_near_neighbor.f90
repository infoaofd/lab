subroutine my_near_neighbor &
(z,zout,nx,ny,nw,fillvalue)
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/WRF.Pre/OISST.Daily.Mean.Takatama
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 16:45 on 02-21-2014.

  integer,intent(in)::nx,ny,nw
  real,intent(in)::z(nx,ny),fillvalue
  real,intent(inout)::zout(nx,ny)


  zout(:,:)=z(:,:)

!  print *,im,jm,ni
  do j=1+nw,ny-nw
    do i=1+nw,nx-nw

      if(z(i,j) /= fillvalue)then
        zout(i,j)=z(i,j)
!               print *,bbb
        goto 100
      end if

      do n=1,nw

         do jj=-n,n
           do ii=-n,n
              
             if(z(i+ii,j+jj) /= fillvalue)then
               zout(i,j)=z(i+ii,j+jj)
!               print *,aaa
               goto 100
             end if

           enddo !ii
         enddo !jj

       enddo !n


100   continue

    end do ! i
  enddo ! j

end subroutine my_near_neighbor


!
! namelist for oisst.warm_wps.exe
!
! aofd165.fish.nagasaki-u.ac.jp
! /work1/am/WRF.Pre/OISST.Smooth.K.Ext
! am
! 2014年  3月  3日 月曜日 10:03:09 JST
&para
ifile1="./OISST/20140216.grd",
ifile2="./OISST/20140217.grd",
ofile="./intm/SST:2014-02-16_18",
hdate="2014-02-16-18:00:00",
cxfcst="18",
out_check="./out.check/20140216_18.dat",
out_ctl="./out.check/20140216.ctl",
yyyy=2014,
mm=02,
dd=16,
hh=18,
&end

#/bin/sh

syyyy=2014
smm=02
sdd=12
days=5

indir=/work4/DATA/SST/OISST/Daily.Mean
outdir=./OISST


d=0
while [ $d -le $days ]; do

yyyymmdd=$(date -d"${syyyy}${smm}${sdd} $d day" '+%Y%m%d')
yyyy=${yyyymmdd:0:4}

julday=$(date -d${yyyymmdd} +%j)

infle=sst.day.mean.${yyyy}.v2.nc
input=${indir}/${infle}
ofle=${yyyymmdd}.grd
output=${outdir}/${ofle}

if [ ! -d $outdir ]; then
  mkdir -vp $outdir
fi

if [ ! -f $input ]; then
  echo Error in $0 : No such file, $input
  exit 1
fi

grads -bcp "grads.out.gs -i ${input} -d ${yyyymmdd} -j ${julday} \
-o ${output} -q"

d=$(expr $d + 1)

done
exit 0


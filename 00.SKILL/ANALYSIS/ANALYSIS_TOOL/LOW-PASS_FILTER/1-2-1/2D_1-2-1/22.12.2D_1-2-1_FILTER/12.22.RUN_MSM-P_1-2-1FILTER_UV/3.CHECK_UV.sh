#!/bin/bash
# Wed, 16 Oct 2024 18:16:54 +0900
# /work09/$(whoami)/10.WORK/2024.HUMID_HEAT/12.12.ROMPS_HEAT_INDEX/22.12.MSM/12.12.TEST_READ_WRITE_MSM_S
# 
function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

YMDHM1=$1; YMDHM1=${YMDHM1:-202108140000}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}

LEV=$2; LEV=${LEV:-850}
max_itr=$3; max_itr=${max_itr:-10}

TIME=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}

INDIR1=/work02/DATA/MSM.NC/MSM-P/${Y1}
if [ ! -d $INDIR1 ];then echo EEEEE NO SUCH DIR,$INDIR1;exit 1;fi
INFLE1=${MM1}${DD1}.nc
IN1=${INDIR1}/${INFLE1}
if [ ! -f $IN1 ];then echo EEEEE NO SUCH FILE,$IN1;exit 1;fi

INDIR2=.
INFLE2=2.TEST_MSM-P_1-2-1_UV_CHECK_${Y1}${MM1}${DD1}_max_itr${max_itr}.nc
IN2=${INDIR2}/${INFLE2}
if [ ! -f $IN2 ];then echo EEEEE NO SUCH FILE,$IN2;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${LEV}_${YMDHM1}_max_itr${max_itr}.PDF

# NetCDF OUT
# https://gitlab.com/infoaofd/lab/-/tree/master/00.SKILL/00.TOOL/GRADS/GrADS_RECIPE/GrADS_NETCDF書き出し
#NC=$(basename $0 .sh).nc

LONW=123 ;LONE=131 ; LATS=30 ;LATN=35
# LEV=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}
'sdfopen ${IN1}';'sdfopen ${IN2}'

'q ctlinfo 1'; say sublin(result,1); say
'q ctlinfo 2'; say sublin(result,1); say

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=10.3
xmax = 1; ymax = 2
xwid = 5.0/xmax; ywid = 9./ymax
xmargin=0.5; ymargin=0.7

'cc';'set grads off';'set grid off'

ymap = 1; xmap = 1; nmap = 1
xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'set mpdraw off' ;# 'set mpdset worldmap'
'set xlint 1';'set ylint 1'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u.1,3);v.1 -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(u.1,3);v.1 -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

'set line 1 1 1'; 'draw shp JPN_adm1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2; 'draw string 'x' 'y' ${INFLE1}'
x=(xl+xr)/2; y=y+0.2; 'draw string 'x' 'y' ${INDIR1}'




ymap = 2; xmap = 1; nmap = 2
xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'set mpdraw off' ;# 'set mpdset worldmap'
'set xlint 1';'set ylint 1'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u.2,3);v.2 -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(u.2,3);v.2 -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

'set line 1 1 1'; 'draw shp JPN_adm1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2; 'draw string 'x' 'y' ${INFLE2}'
x=(xl+xr)/2; y=y+0.2; 'draw string 'x' 'y' ${INDIR2}'


# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=ytop+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

#say; say 'MMMMM NetCDF OUT'
#'define VAROUT = VAR'
#'set sdfwrite '
#'sdfwrite VAROUT'
#say 'MMMMM DONE.'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
ls -lh --time-style=long-iso $FIG
echo "OUTPUT : $FIG"
fi
echo

echo "DONE $(basename $0)."
echo

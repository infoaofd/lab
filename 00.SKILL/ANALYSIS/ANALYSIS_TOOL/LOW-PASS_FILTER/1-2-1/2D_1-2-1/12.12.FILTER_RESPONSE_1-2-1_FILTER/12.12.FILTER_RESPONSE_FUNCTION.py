import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import math

filt = np.array([0.25,0.5,0.25]) 
w, h=signal.freqz(filt)
print(w)

ftime=5
fig, ax = plt.subplots(figsize=(5, 4))
ax.plot(w/(2*math.pi), abs(h**ftime), label=str(ftime)+' pass')
ax.set_title("1-2-1 Filter "+str(ftime)+" pass", {"fontsize": 16} )
ax.set_xlabel("wavenumber", {"fontsize": 14})
ax.set_ylabel("Response", {"fontsize": 14})
ax.axhline(y=0.5, xmin=0, xmax=1,
            color='black',
            lw=1,
            ls='--',
            alpha=0.6)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+"_"+str(ftime)+"pass.PDF"
plt.subplots_adjust(left=0.15,bottom=0.15)

plt.savefig(FIG)
print("FIG: "+FIG)
print("")



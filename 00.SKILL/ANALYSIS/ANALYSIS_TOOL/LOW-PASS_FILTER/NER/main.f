C***********************************************************************
C .f
C TASK
C REMARK
C***********************************************************************
C------------------------- DECLARATION ---------------------------------
C      implicit none
C      implicit double precision(a-h,o-z)
C------------------------- INCLUDE FILE --------------------------------
C      include 'comblk.h'
C-------------------------- PARAMETERS ---------------------------------
      parameter(mdat=4000)
      parameter(mbin=25)
      parameter(nbin=25)
      parameter(isdat=13)
      parameter(iedat=86)

      parameter(PRD1=60.) ! 60.) (in terms of NUMBER OF DATA)
      parameter(rlambda=80.0,dratio=0.9)
      parameter(dt=30.0/60.0)
C------------------------- COMMON VARIABLES ----------------------------
C      common //
C------------------------- LOCAL VARIABLES -----------------------------
C      double precision 
      real mo(mdat) ! month
     $    ,dy(mdat) ! day
     $    ,yr(mdat) ! year
     $    ,hr(mdat) ! hour
     $    ,min(mdat) ! minute
     $    ,sec(mdat) ! second
     $    ,bv(mdat)  ! Battery voltage 
     $    ,sofs(mdat) ! speed of sound
     $    ,head(mdat) ! Heading
     $    ,pitch(mdat) ! Pitch
     $    ,roll(mdat)  ! Roll
     $    ,p(mdat) ! Pressure
     $    ,t(mdat)   ! water temperature
      integer code1(mdat),code2(mdat)

C data
      real dyelp(mdat),day_jul1(mdat)

C velocity
      real v1(mdat,mbin), v2(mdat,mbin)

      character infle*70, ofle*70

C temporal variable
       real ve(mdat), vn(mdat)

C low-passed velocity
      real velp(mdat), vnlp(mdat)
      real veflt(mdat,mbin), vnflt(mdat,mbin)
C-----------------------------------------------------------------------
      ndat=3911
      write(*,*)'ndat = ',ndat

C INPUT
      write(*,'(A)')'INPUT:'
      infle='../01.Plot_raw_rec/Aqdopp_07-2007_'
      do j=1,nbin
        write(infle(35:36),'(I2.2)')j
        infle(37:40)='.txt'
C Print input file name
        if(j.eq.1)then
          write(*,'(A)') infle
          write(*,'(A)')'.....'
        else if (j.eq.nbin)then
          write(*,'(A)') infle
        end if

        open(10,file=infle)
        do i=1,ndat
          read(10,'(F14.8,2F8.3,6F6.0)')
     $     day_jul1(i),v1(i,j),v2(i,j),mo(i),dy(i)
     $    ,yr(i),hr(i),min(i),sec(i)
        end do !** i **
        close(10)
      end do !** j **
C
C Print Filter Specification
      WRITE(*,*)'Filter Specification'
      WRITE(*,*)'Nearly equal ripple filter (Keiser and Reed)'
      WRITE(*,*)'cut-off period: ',PRD1,'(in terms of NUMBER OF DATA)'
      WRITE(*,*)'lambda = ',rlambda, '[dB]'
      WRITE(*,*)'dratio = ',DRATIO
C
      do j=1,nbin
C       Store velocity data in temporal variable
        do i=1,ndat
          ve(i)=v1(i,j)
          vn(i)=v2(i,j)
        end do

C       Filtering
         call NERLP(ve,velp,mdat,ndat,PRD1,rlambda,dratio,np)
         call NERLP(vn,vnlp,mdat,ndat,PRD1,rlambda,dratio,np)

        do i=1,ndat
          veflt(i,j)=velp(i)
          vnflt(i,j)=vnlp(i)
        end do
      end do !** j **

      write(*,'(A)')'OUTPUT:'
      ofle='Aqdopp_07-07-LP_'
      do j=1,nbin
        write(ofle(17:18),'(I2.2)')j
        ofle(19:22)='.txt'

C Print input file name
        if(j.eq.1)then
          write(*,'(A)') ofle
          write(*,'(A)')'.....'
        else if (j.eq.nbin)then
          write(*,'(A)') ofle
        end if

        open(20,file=ofle)
        do i=np+1,ndat-np
          write(20,'(F14.8,2F8.3,2x,2F8.3,6F6.0)')
     $     day_jul1(i),veflt(i,j),vnflt(i,j),v1(i,j),v2(i,j),mo(i),dy(i)
     $    ,yr(i),hr(i),min(i),sec(i)
        end do !** i **
        close(20)
      end do !** j **

      stop
      end

C[C:\WINDOWS\�޽�į��\Aquadopp_07-12-2005\Ar05S01.sen]
C  1   Month                            (1-12)
C  2   Day                              (1-31)
C  3   Year
C  4   Hour                             (0-23)
C  5   Minute                           (0-59)
C  6   Second                           (0-59)
C  7   Error code
C  8   Status code
C  9   Battery voltage                  (V)
C 10   Soundspeed                       (m/s)
C 11   Heading                          (degrees)
C 12   Pitch                            (degrees)
C 13   Roll                             (degrees)
C 14   Pressure                         (m)
C 15   Temperature                      (degrees C)
C 16   Analog input 1
C 17   Analog input 2

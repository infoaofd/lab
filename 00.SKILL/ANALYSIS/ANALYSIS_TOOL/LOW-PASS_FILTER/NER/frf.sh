#!/bin/sh

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16

IN=frf1.dat
OUT=frf1.ps
RANGE=0/4/-0.2/1.2

PROC=frf1_in_dy.dat

XLABEL=a1f0.25

echo
pwd
echo
date
echo
echo "INPUT: " ${IN}
echo "OUTPUT: " ${OUT}
echo

# translate unit of time into day
awk '{print $1*20/60/24, $2 }' $IN > $PROC

psxy $PROC -JX5/2 -R$RANGE -W3 \
  -B${XLABEL}:"day":/a0.5f0.1:"FRF"::."":WSne \
  -X1.5 -Y1.5 -P -K > $OUT
# psxy $IN -JX -R -Sc0.04 -G0 -O >> $OUT

currentdir=`pwd`
if [ ${#currentdir} -gt 100 ]; then
  curdir1=${currentdir:1:100}
  curdir2=${currentdir:101}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
pstext -JX6/1 -R0/1/0/1 -N -Y7.5 << EOF -O >> $OUT
0 0.9   9 0 1 LM $0
0 0.75  9 0 1 LM ${now}
0 0.6   9 0 1 LM ${host}
0 0.45  9 0 1 LM ${curdir1}
0 0.3   9 0 1 LM ${curdir2}
0 0.15  9 0 1 LM ${IN}
EOF

C
C See the end of this file for instruction.
C
      SUBROUTINE NERLP(X,Y,IDIM,NDATA,PRD1,LAMBDA,DRATIO,np1)

C     BAND-PASS FILTER USING NER KERNEL                                                 
C*****
      PARAMETER(MXTRM=24*100*2,MXTRM2=MXTRM/2)
      PARAMETER(NERDIM=MXTRM)
* NERDIM : THE DIMENSION OF THE COEFICIENTS OF THE NER FILTER
      PARAMETER(NDP=1)
                                                                                                                     
      DIMENSION X(IDIM),y(idim)
      DIMENSION FBK1(-MXTRM2:MXTRM2)
      REAL LAMBDA ,DRATIO

      REAL BK1(0:MXTRM2)
      REAL T1(8000),R1(8000)

    
      PI=4.0*atan(1.0)

*
* 1.APPLY LOW-PASS FILTER
*
                                                          
*--- SET THE WEIGHT OF FILTER ---
      BETA=1.0/PRD1
      DELTA=BETA*2.0*DRATIO
      CALL NERCOF(NERDIM,LAMBDA,BETA,DELTA,NP1,BK1)

C      WRITE(*,*)'HALF NUMBER OF THE TERMS OF THE WEIGHT OF THE BANDPASS 
C     +FILTER(1) : NP1'
C      write(*,'(a,i5)')'np1 = ',np1

      DO 110 I=0,NP1
        FBK1(I)=BK1(I+1)
110   CONTINUE
      DO 120 I=1,NP1
        FBK1(-I)=BK1(I+1)
120   CONTINUE
      DO 130 I=-NP1,NP1
        SOFW1=SOFW1+FBK1(I)
130   CONTINUE
      DO 140 I=-NP1,NP1
        FBK1(I)=FBK1(I)/SOFW1
140   CONTINUE
      DO 145 I=0,NP1
        BK1(I)=BK1(I)/SOFW1
145   CONTINUE

CD
CD Output weighting
CD
      open(70,file='Weighting.dat')
C      write(*,*)'Weighting.dat : the weight of the filter'
      do i=-np1,np1
        write(70,'(F15.7)')FBK1(i)
      end do
C     Sum of the weighting
      wsum=0.0
      do i=-np1,np1
        wsum=wsum+fbk1(i)
      end do
      write(70,*)'Sum of the weighting = ',wsum
C
C WEIGHTED RUNNNIG MEAN
C
      DO 150 J=NP1+1,NDATA-NP1
        SUM=0.0
        DO 160 I=-NP1,NP1
          SUM=SUM+FBK1(I)*X(J+I)
160     CONTINUE
        Y(J)=SUM
150   CONTINUE

*
* 3.SET ZEROS AT HEAD AND TAIL OF THE DATA
*
      DO 300 I=1,NP1
        Y(I)=0.0
        J=NDATA-I+1
        Y(J)=0.0
300    CONTINUE

      CLOSE(70)

*
* FILTER RESPONSE FACTOR
*
*--- LOW PASS FILTER 1 ---
      OPEN(70,FILE='frf1.dat')
      JMAX=INT(10.0*PRD1*NDP)
      JS=5*NDP
      DO 400 J=JS,JMAX
        T1(J)=FLOAT(J)/FLOAT(NDP)
        SUM=0.0
        DO 410 K=1,NP1
          RK=FLOAT(K)
          SUM=SUM+BK1(K)*COS(2.0*PI/T1(J)*RK)
410     CONTINUE
        R1(J)=BK1(0)+2.0*SUM
400   CONTINUE
      DO 420 J=JS,JMAX
        WRITE(70,'(2F12.5)')T1(J),R1(J)
420   CONTINUE        
      CLOSE(70)

      RETURN
      END                                                               

C***********************************************************************
      SUBROUTINE NERCOF(NERDIM,AL,BE,DE,NP,BK)                                    
C
C  THIS SUBROUTINE COMPUTES THE COEFFICIENTS OF A NEARLY EQUIRIPPLE
C LINEAR PHASE SMOOTHING FILTER WITH AN ODD NUMBER (2NP+1) OF TERMS 
* AND EVEN SYMMETRY.
*                                                     
*     INPUT :  NERDIM = THE DIMENSION OF THE COEFICIENTS OF THE FILTER.
*              AL = STOPBAND LOSS IN DB (LAMBDA)                        
C              BE = RELATIVE LOCATION OF IDEAL EDGE OF PASSBAND (BETA)  
C                   (I.E. BE = 2.0/PERIOD)                              
C              DE = RELATIVE WIDTH OF TRANSITION BAND (DELTA)           
C     OUTPUT : NP = NUMBER OF FILTER COEFFICIENT PAIRS (K=1,2,---,NP)   
C              BK = ARRAY OF FILTER COEFFICIENTS                        
C                   BK(1)=B0,BK(2)=B1,BK(3)=B2,---,BK(NP+1)=BNP         
C     TOTAL SPAN OF FILTER IS 2*NP INTERVALS                            
C*****                                                                  
      DIMENSION BK(NERDIM)                                                 
      PI=4.0*ATAN(1.0)                                                   
      FK=1.8445                                                         
      IF(AL.GE.21.0) FK=0.13927*(AL-7.95)                               
      ET=0.58417*((AL-21.0)**0.4)+0.07886*(AL-21.0)                     
      IF(AL.LT.21.0) ET=0.0                                             
      IF(AL.GT.50.0) ET=0.1102*(AL-8.7)                                 
      NP=INT((FK/(2.0*DE))+0.75)                                        
C*****                                                                  
C     TEST NP AGAINST DIMENSION LIMIT                                   
C*****                                                                  
*      IF(NP.GT.NERDIM-1) NP=NERDIM-1                                              
      FNP=FLOAT(NP)                                                     
      CALL INO(ET,FIA)                                                  
      DO 10 K=1,NP
      FLK=FLOAT(2*K-1)/2.
      GK=PI*FLOAT(K)                                                   
      GE=ET*SQRT(1.0-(FLOAT(K)/FNP)**2)                                 
      CALL INO(GE,E)                                                    
      BK(K)=(SIN(BE*GK)/GK)*(E/FIA)                                     
   10 CONTINUE                                                          
      BK(NP)=BK(NP)/2.0                                                 
      N=NP+1                                                            
      DO 20 I=2,N                                                       
      K=N-I+2                                                           
      J=K-1                                                             
   20 BK(K)=BK(J)                                                       
      BK(1)=BE                                                          
      RETURN                                                            
      END
                                                               
C***********************************************************************
      SUBROUTINE INO(X,S)                                               
C*****                                                                  
C     ROUTINE EVALUATES THE MODIFIED BESSEL FUNCTION OF ZEROTH ORDER AT 
C     REAL VALUES OF THE ARGUMENT                                       
C     INPUT :  X                                                        
C     OUTPUT : S                                                        
C*****                                                                  
      S=1.0                                                             
      DS=1.0                                                            
      D=0.0                                                             
    1 D=D+2.0                                                           
      DS=DS*X*X/(D*D)                                                   
      S=S+DS                                                            
      IF(DS.GT.0.2E-8*S) GO TO 1                                        
      RETURN                                                            
      END

C
C    This subroutine performs the data smoothing using the nearly equal 
C ripple filter developed by Kiser and Reed (1977).
C
C Description on Running Parameters
C
C       | -
C       | |epsiron
C      1+-----------------
C   F   | |              |
C   R   | -              |  
C   F   |              |-|-|delta
C       |                |
C       |                |
C      0+                ------------
C       |
C       +----------------+----------+--
C      0.0              beta       1.0
C             per unit frequency
C
C             FRF: Filter Responce Factor
C
C  Fig. 1. Definitions of beta, delta, and eps used in design of NER 
C          filter.
C
C Definitions of parameters
C     eps=10**(-lambda/20)
C     beta=1/(cut-off period)
C
C
C lambda:  STOP BAND TOLERANCE ON A LOGARITHMIC
C          SCALE IN TERMS OF dB(decibels) as LAMBDA = -20log10(EPS),
C          WHERE EPS IS A STOP BAND TOLERANCE.
C dratio : THE RATIO OF DELTA TO 2.0*BETA [DELTA=2.0*BETA*DRATIO],
C          WHERE DELTA IS A RELATIVE WIDTH OF TRANSITION BAND,
C          AND BETA IS A RELATIVE LOCATION OF IDEAL EDGE OF PASSBAND.  
C          0 < DRATIO < 1.
C          *** LARGER dratio gives SHARPER filter ***.
C
C Example (should be modified for your owen problem):
C LAMBDA,DRATIO: THE ARBITARY COEFFICIENT OF NER KERNEL
C 80.0 0.8
C
C Reference
C J. F. Kaiser and W. A. Reed. Data smoothing using low-pass digital
C    filters. Rev. Sci. Instrum., Vol. 48, No. 11, November 1977.

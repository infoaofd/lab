#!/bin/sh

#
# timesries
#

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16


inhead='Aqdopp_07-05-LP_' #
inext='.txt'
var='u_'

RANGE_EL=10/36/-0.4/0.4 #
size=5.5/2
XLABEL=a10f1
YLABEL=a0.2f0.1 #

vlev='01'
IN=${inhead}${vlev}${inext}
OUT=${inhead}${var}'L'${vlev}'.ps'

echo
echo INPUT:
echo $IN
echo OUTPUT:
echo $OUT

awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W4 -B${XLABEL}:"day from Jul 1, 2005":/${YLABEL}:"u [m/s]":WSne \
  -X1.5 -Y9 -P -K > $OUT

awk '{print $1, $4 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W1 \
  -O >> $OUT

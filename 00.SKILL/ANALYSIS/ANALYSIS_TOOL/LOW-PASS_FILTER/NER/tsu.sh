#!/bin/sh

#
# timesries
#

gmtset MEASURE_UNIT INCH
gmtset ANOT_FONT_SIZE 16
gmtset LABEL_FONT_SIZE 16
gmtset HEADER_FONT_SIZE 16


inhead='Aqdopp_07-06-LP_' #
inext='.txt'
var='u_'

RANGE_EL=10/36/-0.1/0.1 #
size=5.5/0.8
XLABEL=a10f1
YLABEL=a0.1f0.05g10 #

page='p1'
OUT=${inhead}${var}${page}'.ps'

echo INPUT:
vlev='01'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":WsNe \
  -X1.5 -Y9 -P -K > $OUT
#
vlev='02'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='03'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='04'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='05'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='06'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='07'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='08'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='09'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":Wsne \
  -X0 -Y-0.8 -O -K >> $OUT
#
vlev='10'
IN=${inhead}${vlev}${inext}
echo $IN
awk '{print $1, $2 }' $IN | psxy -JX${size} -R$RANGE_EL \
-W2 -B${XLABEL}:"day from Jul 1, 2006":/${YLABEL}:"u [m/s]":WSne \
  -X0 -Y-0.8 -O  >> $OUT
echo
echo OUTPUT:
echo $OUT

#awk '{print $1, $3 }' $IN | psxy -JX -R$RANGE_EL \
#-W3 -Ba0.5f0.125:"time [day]":/${YLABEL}:"elevation [m]":WSne \
#-Y-3 -O -K >> $OUT

#awk '{print $1, $4 }' $IN | psxy -JX -R$RANGE_EL \
#-W3 -Ba0.5f0.125:"time [day]":/${YLABEL}:"elevation [m]":WSne \
#-Y-3 -O >> $OUT


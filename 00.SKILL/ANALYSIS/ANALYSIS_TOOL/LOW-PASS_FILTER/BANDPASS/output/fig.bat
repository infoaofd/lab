#
# G N U P L O T
#
#
#
set autoscale
set term postscript
set output "raw.ps"
set title "1)INPUT DATA"
#set yrange [0:30]
set xlabel "T[3hour]"
set ylabel "[degree]"
plot "fku3h2-10ga.dat" using 3 w l

set output "bp.ps"
set title "4)BAND PASS FILTERED DATA"
#set yrange [-5:5]
set xlabel "T[3hour]"
set ylabel "[degree]"
plot "fku3h2-10ga.dat" using 4 w l

set output "low.ps"
set title "2) LOW PASS FILTERED DATA"
#set yrange [0:30]
set xlabel "T[hour]"
set ylabel "[degree]"
plot "fku3h2-10ga.dat" using 5 w l

set output "high.ps"
set title "3)REMAINDER OF HIGH PASS FILTER"
#set yrange [0:30]
set xlabel "T[hour]"
set ylabel "[degree]"
plot "fku3h2-10ga.dat" using 6 w l

set title
set xlabel
set ylabel
set term X11

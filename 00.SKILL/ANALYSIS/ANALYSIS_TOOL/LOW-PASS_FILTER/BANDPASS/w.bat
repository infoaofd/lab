set term postscript
set ylabel
set autoscale
set grid

set title "THE WEIGHT OF LOW PASS FILTER (1)"
set output "wl.ps"
plot "wl.dat" w l

set output "wh.ps"
set title "THE WEIGHT OF LOW PASS FILTER (2)"
plot "wh.dat" w l

set term X11
set title

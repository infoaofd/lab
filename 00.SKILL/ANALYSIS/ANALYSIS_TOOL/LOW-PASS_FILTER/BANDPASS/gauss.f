      SUBROUTINE GAUSS(X,Y,IDIM,NDATA,PRD1,PRD2,sllow,slhigh)                                                  

C     BAND-PASS FILTER USING GAUSSIAN KERNEL                                                 
C*****
      PARAMETER(MXTRM=24*100,MXTRM2=MXTRM/2)
      PARAMETER(DT=1.0/24.0)
      PARAMETER(NDP=10)
                                                                                                                     
      DIMENSION X(IDIM),y(idim)
     +         ,FBK1(-MXTRM2:MXTRM2),FBK2(-MXTRM2:MXTRM2)
      REAL slhigh(idim),sllow(idim)

      REAL BK1(0:MXTRM2),BK2(0:MXTRM2)
      REAL  T1(10*PRD1*NDP),T2(10*PRD2*NDP)
     +     ,R1(10*PRD1*NDP),R2(10*PRD2*NDP)  
*                                                                                                      
* -------+-------------------+---------> T
*       PRD1                PRD2

      NP1=INT(PRD1/2.0)
      NP2=INT(PRD2/2.0)
      WRITE(*,*)'GAUSSIAN KERNEL'
      WRITE(*,*)'HALF NUMBER OF THE TERMS OF THE WEIGHT OF THE BANDPASS 
     +FILTER : NP1,NP2'
      write(*,'(a,i5)')'np1 = ',np1,' np2 = ',np2
*debug
      open(70,file='wl.dat')
      open(80,file='wh.dat')
      write(*,*)'wl.dat : the weight of the low pass filter 1'
      write(*,*)'wh.dat : the weight of the low pass filter 2'
*debug end
      PI=4.0*ATAN(1.0)

*
* 1.APPLY LOW-PASS FILTER
*
                                                        
*--- SET THE WEIGHT OF FILTER (GMT3.0 Tech. Ref and Cookbook,p.69)---
      HWD1=FLOAT(NP1)/3.0
      DO 100 I=0,NP1
        RI=FLOAT(I)
        BK1(I)=EXP(-0.5*(RI/HWD1)**2)
100   CONTINUE

      DO 110 I=0,NP1
        FBK1(I)=BK1(I)
110   CONTINUE
      DO 120 I=1,NP1
        FBK1(-I)=BK1(I)
120   CONTINUE
      DO 130 I=-NP1,NP1
        SOFW1=SOFW1+FBK1(I)
130   CONTINUE
      DO 140 I=-NP1,NP1
        FBK1(I)=FBK1(I)/SOFW1
140   CONTINUE
      DO 145 I=0,NP1
        BK1(I)=BK1(I)/SOFW1
145   CONTINUE

*--- WEIGHTED RUNNNIG MEAN ---
      DO 150 J=NP1+1,NDATA-NP1
        SUM=0.0
        DO 160 I=-NP1,NP1
          SUM=SUM+FBK1(I)*X(J+I)
*debug
      if(j.eq.np1+1)then
         write(70,'(i5,e12.5)')i,fbk1(i)
      end if
*debug end

160     CONTINUE
        SLLOW(J)=SUM
150   CONTINUE

*
* 2.APPLY HIGH PASS FILTER
*
*--- SET THE WEIGHT OF FILTER (GMT3.0 Tech. Ref and Cookbook,p.69)---
      HWD2=FLOAT(NP2)/3.0
      DO 200 I=0,NP2
        RI=FLOAT(I)
        BK2(I)=EXP(-0.5*(RI/HWD2)**2)
200   CONTINUE

      DO 210 I=0,NP2
        FBK2(I)=BK2(I)
210   CONTINUE
      DO 220 I=1,NP2
        FBK2(-I)=BK2(I)
220   CONTINUE
      DO 230 I=-NP2,NP2
        SOFW2=SOFW2+FBK2(I)
230   CONTINUE
      DO 240 I=-NP2,NP2
        FBK2(I)=FBK2(I)/SOFW2
240   CONTINUE
      DO 245 I=0,NP2
        BK2(I)=BK2(I)/SOFW2
245   CONTINUE

*--- WEIGHTED RUNNNIG MEAN ---
      DO 250 J=NP1+NP2+1,NDATA-NP2-NP1
        SUM=0.0
        DO 260 I=-NP2,NP2
*debug
      if(j.eq.np1+np2+1)then
         write(80,'(i5,e12.5)')i,fbk2(i)
      end if
*debug end
          SUM=SUM+FBK2(I)*SLLOW(J+I)
260     CONTINUE
        SLHIGH(J)=SUM
        Y(J)=SLLOW(J)-SUM
250   CONTINUE

*
* 3.SET ZEROS AT HEAD AND TAIL OF THE DATA
*
      DO 300 I=1,NP1+NP2
	  Y(I)=0.0
	  J=NDATA-I+1
          Y(J)=0.0
300    CONTINUE

*debug
      CLOSE(70)
      CLOSE(80)
*debug end

*
* FILTER RESPONSE FACTOR
*
*--- LOW PASS FILTER 1 ---
      OPEN(70,FILE='frf1.dat')
      JMAX=INT(10.0*PRD1*NDP)
      JS=5*NDP
      DO 400 J=JS,JMAX
        T1(J)=FLOAT(J)/FLOAT(NDP)
        SUM=0.0
        DO 410 K=1,NP1
          RK=FLOAT(K)
          SUM=SUM+BK1(K)*COS(2.0*PI/T1(J)*RK)
410     CONTINUE
        R1(J)=BK1(0)+2.0*SUM
400   CONTINUE
      DO 420 J=1,JMAX
        WRITE(70,'(2F12.5)')T1(J),R1(J)
420   CONTINUE        
      CLOSE(70)
*--- LOW PASS FILTER 2 ---
      OPEN(80,FILE='frf2.dat')
      JMAX=INT(10.0*PRD2*NDP)
      JS=5*NDP
      DO 500 J=JS,JMAX
        T2(J)=FLOAT(J)/FLOAT(NDP)
        SUM=0.0
        DO 510 K=1,NP2
          RK=FLOAT(K)
          SUM=SUM+BK2(K)*COS(2.0*PI/T2(J)*RK)
510     CONTINUE
        R2(J)=BK2(0)+2.0*SUM
500   CONTINUE
      DO 520 J=1,JMAX
        WRITE(80,'(2F12.5)')T2(J),R2(J)
520   CONTINUE        
      CLOSE(80)

      RETURN                                                             
      END                                                               



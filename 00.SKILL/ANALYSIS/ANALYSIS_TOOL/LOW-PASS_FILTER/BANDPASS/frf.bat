set term post
set grid
set xlabel "T"
set ylabel "Filter Response Factor"

#
# FILTER 2
#
set output "frf1.ps"
plot "frf1.dat" w l

set xrange [0:150]
set output "frf1up.ps"
plot "frf1.dat" w l

set autoscale x

#
# FILTER 2
#
set output "frf2.ps"
plot "frf2.dat" w l

set xrange [0:300]
set output "frf2up.ps"
plot "frf2.dat" w l


set autoscale x
#
# BAND PASS FILTER 
#
set output "frfbp.ps"
plot "frfbp.dat" w l







***********************************************************************
* BP.F
* CODED BY A.MANDA
* MODIFIED BY A.MANDA           1998. 1
* TASK
*  BAND PASS FILTER                       
* METHOD
* SLAVE SUBROUTINE(S)
* REMARK
* 1)CUT-OFF PERIOD DEFINITION
*   --------+--------------------+-------------> T
*          PRD1                 PRD2
*
* REFERENCE(S)
* GMT3.0 REF. AND COOKBOOK
* KAISER,J.F. AND REED,W.A.(1977):DATA SMOOTHING USING LOW-PASS DIGITAL 
* FILTERS,REVIEW FO SCIENTIFIC INSTRUMENTAL,11,PP.1447-1457.                           
***********************************************************************
      PARAMETER(IDIM=24*365*2)
                                   
      DIMENSION SL1(IDIM),BSL1(IDIM)
      real sllow(idim),slhigh(idim)
      CHARACTER*20 DH1(IDIM)
      integer ND1
* ND1 : NUMBER OF THE INPUT DATA
      CHARACTER INFLE*40,OTFLE*40
      REAL PRD1,PRD2
      CHARACTER FTYPE*2
      REAL P
* P : ARBITALY COEF. OF COSINE-LANCZOS KERNEL
      REAL LAMBDA,DRATIO(2)
* LAMBDA,DRATIO : ARBITALY COEF. OF NER KERNEL 
* NER KERNEL = NEARLY EQUAL RIPPLE APPROXIMATION KERNEL(SEE. KAISER AND
* REED(1977)

* SET PARAMETERS FOR RUNNING THIS PROGRAM
      OPEN(7,FILE='iofile.txt')
	READ(7,*)
	READ(7,'(A40)')INFLE
	READ(7,*)
	READ(7,'(A40)')OTFLE
	READ(7,*)
	READ(7,'(I5)')ND1
	READ(7,*)
	READ(7,'(F10.2)')PRD1
	READ(7,*)
	READ(7,'(F10.2)')PRD2
        READ(7,*)
        READ(7,'(A2)')FTYPE
        READ(7,*)
        READ(7,'(F5.1)')P
        READ(7,*)
        READ(7,'(3F5.1)')LAMBDA,DRATIO(1),DRATIO(2)
*
* CHECKING THE PARAMETERS
*
        IF(P.LT.0.0.OR.P.GT.1.0)THEN
          WRITE(*,*)'P MUST BE BETWEEN 0 AND 1.'
          STOP
        END IF
        DO 100 I=1,2
        IF(DRATIO(I).LE.0.0.OR.DRATIO(I).GE.1.0)THEN
           WRITE(*,*)'DRATIO MUST BE BETWEEN 0 AND 1.'
        END IF
100     CONTINUE

*
* MONITORING THE PARAMETERS FOR RUNNIG THIS PROGRAM 
*
      WRITE(*,*)'INPUT FILE NAME  : ',INFLE
      WRITE(*,*)'OUTPUT FILE NAME : ',OTFLE
*
* READ INPUT DATA
*
      OPEN(10,FILE=INFLE)

      READ(10,*)
      READ(10,*)
      DO 110 I=1,ND1
        READ(10,1000) DH1(I),SL1(I)
110    CONTINUE
1000  FORMAT(A16,F10.2)
*
* --------+--------------------+-------------> T 
*        PRD1                  PRD2

*
* APPLY BAND PASS FILTER
*

      IF(FTYPE.EQ.'BC')THEN
*--- BOX CAR KERNEL ---
       CALL BPBOXC(SL1,BSL1,IDIM,ND1,PRD1,PRD2,SLLOW,slhigh)
      ELSE IF(FTYPE.EQ.'LC')THEN
*--- LANCZOS COSINE KERNEL ---
       CALL LANCOS(SL1,BSL1,IDIM,ND1,PRD1,PRD2,SLLOW,slhigh,P) 
      ELSE IF(FTYPE.EQ.'GA')THEN
*--- GAUSSIAN KERNEL ---
       CALL GAUSS(SL1,BSL1,IDIM,ND1,PRD1,PRD2,SLLOW,slhigh)
      ELSE IF(FTYPE.EQ.'NE')THEN
*--- NER KERNEL ---
       CALL NER(SL1,BSL1,IDIM,ND1,PRD1,PRD2,SLLOW,SLHIGH,LAMBDA
     +         ,DRATIO)
      ELSE       
        WRITE(*,*)'FILTER TYPE MUST BE SPECIFIED(IN iofile.txt).'
        STOP
      END IF
*
* OUTPUT BAND-PASSED DATA
*         

      OPEN(30,FILE=OTFLE)
      DO 120 I=1,ND1
        WRITE(30,'(A16,4F10.2)') DH1(I),SL1(I),BSL1(I),sllow(i),
     +                           slhigh(i)                             
120    continue

      CLOSE(10)
      CLOSE(30)
      STOP                                                              
      END                                                               



                                                               



      SUBROUTINE NER(X,Y,IDIM,NDATA,PRD1,PRD2,sllow,slhigh,LAMBDA
     +              ,DRATIO)                                                  

C     BAND-PASS FILTER USING NER KERNEL                                                 
C*****
      PARAMETER(MXTRM=24*100*2,MXTRM2=MXTRM/2)
      PARAMETER(NERDIM=MXTRM)
* NERDIM : THE DIMENSION OF THE COEFICIENTS OF THE NER FILTER
      PARAMETER(DT=1.0/24.0)
      PARAMETER(NDP=10)
                                                                                                                     
      DIMENSION X(IDIM),y(idim)
     +         ,FBK1(-MXTRM2:MXTRM2),FBK2(-MXTRM2:MXTRM2)
      REAL slhigh(idim),sllow(idim)
      REAL LAMBDA ,DRATIO(2)     

      REAL BK1(NERDIM),BK2(NERDIM)
      REAL  T1(10*PRD1*NDP),T2(10*PRD2*NDP)
     +     ,R1(10*PRD1*NDP),R2(10*PRD2*NDP),R12(10*PRD2*NDP)

    
      PI=4.0*atan(1.0)
*                                                                                                      
* -------+-------------------+---------> T
*       PRD1                PRD2
      WRITE(*,*)'///// NER KERNEL //////'
      WRITE(*,*)'CHARACTERISTICS OF THE FILTER'
      WRITE(*,*)'CUT-OFF PERIOD 1 : ',PRD1
      WRITE(*,*)'CUT-OFF PERIOD 2 : ',PRD2
      WRITE(*,*)'LAMBDA = ',LAMBDA, '[dB]'
      WRITE(*,*)'DRATIO(1) = ',DRATIO(1)
      WRITE(*,*)'DRATIO(2) = ',DRATIO(2) 
*debug
      open(70,file='wl.dat')
      open(80,file='wh.dat')
      write(*,*)'wl.dat : the weight of the low pass filter 1'
      write(*,*)'wh.dat : the weight of the low pass filter 2'
*debug end


*
* 1.APPLY LOW-PASS FILTER
*
                                                          
*--- SET THE WEIGHT OF FILTER ---
      BETA=1.0/PRD1
      DELTA=BETA*2.0*DRATIO(1)
      CALL NERCOF(NERDIM,LAMBDA,BETA,DELTA,NP1,BK1)

      WRITE(*,*)'HALF NUMBER OF THE TERMS OF THE WEIGHT OF THE BANDPASS 
     +FILTER(1) : NP1'
      write(*,'(a,i5)')'np1 = ',np1

      DO 110 I=0,NP1
        FBK1(I)=BK1(I+1)
110   CONTINUE
      DO 120 I=1,NP1
        FBK1(-I)=BK1(I+1)
120   CONTINUE
      DO 130 I=-NP1,NP1
        SOFW1=SOFW1+FBK1(I)
130   CONTINUE
      DO 140 I=-NP1,NP1
        FBK1(I)=FBK1(I)/SOFW1
140   CONTINUE
      DO 145 I=0,NP1
        BK1(I)=BK1(I)/SOFW1
145   CONTINUE


*--- WEIGHTED RUNNNIG MEAN ---
      DO 150 J=NP1+1,NDATA-NP1
        SUM=0.0
        DO 160 I=-NP1,NP1
          SUM=SUM+FBK1(I)*X(J+I)
*debug
      if(j.eq.np1+1)then
         write(70,'(i5,e12.5)')i,fbk1(i)
      end if
*debug end

160     CONTINUE
        SLLOW(J)=SUM
150   CONTINUE

*
* 2.APPLY HIGH PASS FILTER
*

*--- SET THE WEIGHT OF FILTER ---
      BETA=1.0/PRD2
      DELTA=BETA*2.0*DRATIO(2)
      CALL NERCOF(NERDIM,LAMBDA,BETA,DELTA,NP2,BK2)

      WRITE(*,*)'HALF NUMBER OF THE TERMS OF THE WEIGHT OF THE BANDPASS 
     +FILTER(2) : NP2'
      write(*,'(a,i5)')' np2 = ',np2
    
      DO 210 I=0,NP2
        FBK2(I)=BK2(I+1)
210   CONTINUE
      DO 220 I=1,NP2
        FBK2(-I)=BK2(I+1)
220   CONTINUE
      DO 230 I=-NP2,NP2
        SOFW2=SOFW2+FBK2(I)
230   CONTINUE
      DO 240 I=-NP2,NP2
        FBK2(I)=FBK2(I)/SOFW2
240   CONTINUE
      DO 245 I=0,NP2
        BK2(I)=BK2(I)/SOFW2
245   CONTINUE

*--- WEIGHTED RUNNNIG MEAN ---
      DO 250 J=NP1+NP2+1,NDATA-NP2-NP1
        SUM=0.0
        DO 260 I=-NP2,NP2
*debug
      if(j.eq.np1+np2+1)then
         write(80,'(i5,e12.5)')i,fbk2(i)
      end if
*debug end
          SUM=SUM+FBK2(I)*SLLOW(J+I)
260     CONTINUE
        SLHIGH(J)=SUM
        Y(J)=SLLOW(J)-SUM
250   CONTINUE

*
* 3.SET ZEROS AT HEAD AND TAIL OF THE DATA
*
      DO 300 I=1,NP1+NP2
	  Y(I)=0.0
	  J=NDATA-I+1
          Y(J)=0.0
300    CONTINUE

      CLOSE(70)
      CLOSE(80)

*
* FILTER RESPONSE FACTOR
*
*--- LOW PASS FILTER 1 ---
      OPEN(70,FILE='frf1.dat')
      JMAX=INT(10.0*PRD1*NDP)
      JS=5*NDP
      DO 400 J=JS,JMAX
        T1(J)=FLOAT(J)/FLOAT(NDP)
        SUM=0.0
        DO 410 K=1,NP1
          RK=FLOAT(K)
          SUM=SUM+BK1(K)*COS(2.0*PI/T1(J)*RK)
410     CONTINUE
        R1(J)=BK1(0)+2.0*SUM
400   CONTINUE
      DO 420 J=JS,JMAX
        WRITE(70,'(2F12.5)')T1(J),R1(J)
420   CONTINUE        
      CLOSE(70)
*--- LOW PASS FILTER 2 ---
      OPEN(80,FILE='frf2.dat')
      JMAX=INT(10.0*PRD2*NDP)
      JS=5*NDP
      DO 500 J=JS,JMAX
        T2(J)=FLOAT(J)/FLOAT(NDP)
        SUM=0.0
        DO 510 K=1,NP2
          RK=FLOAT(K)
          SUM=SUM+BK2(K)*COS(2.0*PI/T2(J)*RK)
510     CONTINUE
        R2(J)=BK2(0)+2.0*SUM
500   CONTINUE
      DO 520 J=JS,JMAX
        WRITE(80,'(2F12.5)')T2(J),R2(J)
520   CONTINUE        
      CLOSE(80)

*--- BAND PASS FILTER ---
      OPEN(90,FILE='frfbp.dat')
      JMAX=INT(10.0*PRD2*NDP)
      JS=5*NDP
      DO 600 J=JS,JMAX
        T2(J)=FLOAT(J)/FLOAT(NDP)
        SUM1=0.0
        SUM2=0.0
        DO 605 K=1,NP1
          RK=FLOAT(K)
          SUM1=SUM1+BK1(K)*COS(2.0*PI/T2(J)*RK)
605     CONTINUE        
        DO 610 K=1,NP2
          RK=FLOAT(K)
          SUM2=SUM2+BK2(K)*COS(2.0*PI/T2(J)*RK)
610     CONTINUE
        R12(J)=BK1(0)+2.0*SUM1 - (BK2(0)+2.0*SUM2)
600   CONTINUE
      DO 620 J=JS,JMAX
        WRITE(90,'(2F12.5)')T2(J),R12(J)
620   CONTINUE        
      CLOSE(90)

      RETURN                                                             
      END                                                               


C***********************************************************************
      SUBROUTINE NERCOF(NERDIM,AL,BE,DE,NP,BK)                                    
C*****                                                                  
C  THIS SUBROUTINE COMPUTES THE COEFFICIENTS OF A NEARLY EQUIRIPPLE
C LINEAR PHASE SMOOTHING FILTER WITH AN ODD NUMBER (2NP+1) OF TERMS 
* AND EVEN SYMMETRY.
*                                                     
*     INPUT :  NERDIM = THE DIMENSION OF THE COEFICIENTS OF THE FILTER.
*              AL = STOPBAND LOSS IN DB (LAMBDA)                        
C              BE = RELATIVE LOCATION OF IDEAL EDGE OF PASSBAND (BETA)  
C                   (I.E. BE = 2.0/PERIOD)                              
C              DE = RELATIVE WIDTH OF TRANSITION BAND (DELTA)           
C     OUTPUT : NP = NUMBER OF FILTER COEFFICIENT PAIRS (K=1,2,---,NP)   
C              BK = ARRAY OF FILTER COEFFICIENTS                        
C                   BK(1)=B0,BK(2)=B1,BK(3)=B2,---,BK(NP+1)=BNP         
C     TOTAL SPAN OF FILTER IS 2*NP INTERVALS                            
C*****                                                                  
      DIMENSION BK(NERDIM)                                                 
      PI=4.0*ATAN(1.0)                                                   
      FK=1.8445                                                         
      IF(AL.GE.21.0) FK=0.13927*(AL-7.95)                               
      ET=0.58417*((AL-21.0)**0.4)+0.07886*(AL-21.0)                     
      IF(AL.LT.21.0) ET=0.0                                             
      IF(AL.GT.50.0) ET=0.1102*(AL-8.7)                                 
      NP=INT((FK/(2.0*DE))+0.75)                                        
C*****                                                                  
C     TEST NP AGAINST DIMENSION LIMIT                                   
C*****                                                                  
*      IF(NP.GT.NERDIM-1) NP=NERDIM-1                                              
      FNP=FLOAT(NP)                                                     
      CALL INO(ET,FIA)                                                  
      DO 10 K=1,NP                                                      
      GK=PI*FLOAT(K)                                                   
      GE=ET*SQRT(1.0-(FLOAT(K)/FNP)**2)                                 
      CALL INO(GE,E)                                                    
      BK(K)=(SIN(BE*GK)/GK)*(E/FIA)                                     
   10 CONTINUE                                                          
      BK(NP)=BK(NP)/2.0                                                 
      N=NP+1                                                            
      DO 20 I=2,N                                                       
      K=N-I+2                                                           
      J=K-1                                                             
   20 BK(K)=BK(J)                                                       
      BK(1)=BE                                                          
      RETURN                                                            
      END                                                               
C***********************************************************************
      SUBROUTINE INO(X,S)                                               
C*****                                                                  
C     ROUTINE EVALUATES THE MODIFIED BESSEL FUNCTION OF ZEROTH ORDER AT 
C     REAL VALUES OF THE ARGUMENT                                       
C     INPUT :  X                                                        
C     OUTPUT : S                                                        
C*****                                                                  
      S=1.0                                                             
      DS=1.0                                                            
      D=0.0                                                             
    1 D=D+2.0                                                           
      DS=DS*X*X/(D*D)                                                   
      S=S+DS                                                            
      IF(DS.GT.0.2E-8*S) GO TO 1                                        
      RETURN                                                            
      END

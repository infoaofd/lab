# FILTER_RESPONS_FUNCTION_BOX_CAR.md

[[_TOC_]]

## 移動平均の応答特性

<img src="image-20241117154006182.png" alt="image-20241117154006182" style="zoom: 50%;" />

重み[1/3,1/3,1/3]のフィルタを**3回**かけた場合の応答関数 (横軸＝波数，縦軸＝応答関数)。応答関数が0の場合は，該当する波数のエネルギーはすべてカットされる。

<img src="image-20241117154029940.png" alt="image-20241117154029940" style="zoom:50%;" />

重み[1/3,1/3,1/3]のフィルタを**1回**かけた場合の応答関数。高波数側にノイズが残る。

## Pythonコード

```python
import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import math

filt = np.array([0.25,0.5,0.25]) 
filt = np.array([1/3,1/3,1/3]) 
w, h=signal.freqz(filt)
print(w)

ftime=1
fig, ax = plt.subplots(figsize=(5, 4))
ax.plot(w/(2*math.pi), abs(h**ftime), label=str(ftime)+' pass')
ax.set_title("1-2-1 Filter "+str(ftime)+" pass", {"fontsize": 16} )
ax.set_xlabel("wavenumber", {"fontsize": 14})
ax.set_ylabel("Response", {"fontsize": 14})
ax.axhline(y=0.5, xmin=0, xmax=1,
            color='black',
            lw=1,
            ls='--',
            alpha=0.6)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+"_"+str(ftime)+"pass.PDF"
plt.subplots_adjust(left=0.15,bottom=0.15)

plt.savefig(FIG)
print("FIG: "+FIG)
print("")
```



INDIR=/work02/DATA/MSM.NC/TOPO
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=TOPO.MSM_5K.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=$INDIR
OFLE=$(basename $IN .nc)_CUT_REGRID_OKINAWA.nc
OUT=$ODIR/$OFLE

LONW=126;LATS=25
NX=33;NY=31
DX=0.125; DY=0.1

cat > MYGRID.TXT << EOF
gridtype = lonlat
xsize    = $NX
ysize    = $NY
xunits = 'degree'
yunits = 'degree'
xfirst   = ${LONW}
xinc     = ${DX} 
yfirst   = ${LATS}
yinc     = ${DY}
EOF

rm -vf $OUT
cdo remapcon,MYGRID.TXT $IN $OUT
echo
echo IN: $IN
if [ -f $OUT ];then echo OUT: $OUT;fi



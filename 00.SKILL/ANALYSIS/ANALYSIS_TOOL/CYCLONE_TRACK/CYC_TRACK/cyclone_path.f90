Program cyclopath
! this program is for tracking one  cyclone among the extracted cyclone centers.
!!------------------  Detect based on two criterions ------------------!
!--    1. Within 6 hours, a cyclone could not pass over 5 degrees   --!
!                                                                     !
!--    2. One cyclone should last over 24 hours                     --!
!---------------------------------------------------------------------!
 

  use param
  integer iferr, iyy(nmax),imm(nmax),idd(nmax),ihh(nmax),mmddhh(nmax)
  integer iy,id,idmax,ih,nm,k


  character(len=11) ctime
  integer alltime(485,yr)
  real lon(nmax),lat(nmax),radius(nmax),slp(nmax)
  integer winstart,winend,counter,cyc
  integer :: mark(485,yr)
  character(len=500) :: CMD
  character(len=2) :: cycnum
  character(len=4) :: cycyear
  


  real :: clon(485,yr,cmax),clat(485,yr,cmax),crad(485,yr,cmax),cslp(485,yr,cmax)
  real dis(cmax),x(nmax),y(nmax),ratio
  integer rei(nmax),ret(nmax)

  type(cyclone) :: cl(25,yr)  ! 20 cyclones in one winter

  !real ang


   open(20,file='cyclone_center.asc')
   open(40,file='reformatted.asc')
   
   do n=1,nmax
    read(20,'(a,2f6.1,2f9.2)',iostat=iferr) ctime,lon(n),lat(n),radius(n),slp(n)
    if(iferr /= 0) exit

   read(ctime(1:4),"(i4)")iyy(n)
   read(ctime(5:6),"(i4)")imm(n)
   read(ctime(7:8),"(i4)")idd(n)
   read(ctime(10:11),"(i4)")ihh(n)
   enddo
   

   clon(:,:,:)=fillvalue
   clat(:,:,:)=fillvalue
   crad(:,:,:)=fillvalue
   cslp(:,:,:)=fillvalue
   mark(:,:)=0

   do k=1,yr  ! 8 winters
   winstart=startyear+k-1
   winend=winstart+1

   call filltime(winstart,winend,alltime(:,k))
!!!!!!!!!! Format the data
   !find which frame has cyclone center, mark the number if found
   do t=1,485   ! loop in one winter
   
 !     print *,alltime(t,k)
      do n=1,nmax
      if(iyy(n) >= winstart .and. iyy(n) <=winend) then
       ! print *,iyy(n),imm(n),idd(n),ihh(n)
        mmddhh(n)=iyy(n)*1000000+imm(n)*10000+idd(n)*100+ihh(n)
       ! print *,mmddhh(n)
        if( alltime(t,k) == mmddhh(n) ) then
      !  print *,alltime(t),mmddhh(n)
        mark(t,k)=mark(t,k)+1
        clon(t,k,mark(t,k))=lon(n)
        clat(t,k,mark(t,k))=lat(n)
        crad(t,k,mark(t,k))=radius(n)
        cslp(t,k,mark(t,k))=slp(n)
        endif


      endif   
      enddo ! n
     write(40,"(i10,i4)")alltime(t,k),mark(t,k)
    enddo ! t   one winter loop
  
   enddo ! winter  k 
   close(40)
    

!------------------  Detect based on two criterions ------------------!
!--    1. Within 6 hours, a cyclone could not pass over 5 degrees   --!
!                                                                     !
!--    2. One cyclone should last over 24 hours                     --!
!---------------------------------------------------------------------!

do k=1,yr
 
  irec=1
  cyc=1
  x(:)=0.0
  y(:)=0.0
    21 continue    
    do t=1,485
        if(mark(t,k) >=1)then     ! found start spots
             do i=1,mark(t,k)   !loop for each spot
             if ( clon(t,k,i) > -100.0 .and. clat(t,k,i) > -100.) then
              write(cycnum,'(i2.2)')cyc
              write(cycyear,'(i4.4)') startyear+k-1
              open(403,file="output/"//cycyear//cycnum//".asc")
              counter=1
              !if the location is same with the previous one, skip this frame
              write(403,"(i11,4f9.3)")  alltime(t,k) ,clon(t,k,i),clat(t,k,i),crad(t,k,i),&
       & cslp(t,k,i)
             

              x(counter)=clon(t,k,i)
              y(counter)=clat(t,k,i)
              ret(counter)=t
              rei(counter)=i
              do j=1,100                   ! no cyclone could exist 100 time steps
                                           ! Search in next frame
                                  
              if (mark(t+j,k) >=1)then
               do l=1,mark(t+j,k)
               x(counter+1)=clon(t+j,k,l)
               y(counter+1)=clat(t+j,k,l)
!AM  !!! 9.0 SHOULD BE ADJUSTED. !!!
               if( abs(x(counter+1)-x(counter)) <= 9.0 .and. abs(y(counter+1)-y(counter)) <=9.0) then  ! if smaller than max step, search next step
                  write(403,"(i11,4f9.3)")  alltime(t+j,k) ,clon(t+j,k,l),clat(t+j,k,l),crad(t+j,k,l),&
                  & cslp(t+j,k,l)
                 counter=counter+1
               ! record the used centers
                  ret(counter)=t+j
                  rei(counter)=l
                  goto 22
               else
                  goto 23
               endif    
               
              enddo !l
 
              else
               goto 23

              endif
              22 continue
             enddo !j

              23 close(403)
              if(counter>4)then
              cyc=cyc+1           ! jump to next cyclone
             ! set the located position to empty
             ! which can avoid it passing the criterion again
              do j=1,counter
               clon(ret(j),k,rei(j))=-999.0
               clat(ret(j),k,rei(j))=-999.0
              enddo
              goto 21
              endif
            endif
            enddo !i

        endif
     enddo
 
  
enddo

stop





   cl(:,:)%lt=0
   do k=1,yr
    cyc=1
     do t=1,485-1
     if(cyc>1 .and. t < irec) then
  !     print *,cyc-1,irec
      continue
     else

     if(mark(t,k) >= 1)then         ! set cyclone start spot
      cl(cyc,k)%st=t
      cl(cyc,k)%sn=alltime(t,k)
      cl(cyc,k)%lt=1
        do counter=1,1000              ! very large loop which can't be reached
           if(mark(t+counter,k) >= 1)then         ! set cyclone start spot



           cl(cyc,k)%lt=cl(cyc,k)%lt+1
           else    
             if(cl(cyc,k)%lt <= 4) then  ! whether cyclone can last over 24h
              exit                     ! exit if  not
             elseif(cl(cyc,k)%lt > 4)then
             print *,"This is No.",cyc,"cyclone in Year",k
          
             print *,"This cyclone lasts",real(cl(cyc,k)%lt)/4.,"days"
        
             print *,"Start from",cl(cyc,k)%sn


             call cycprint(alltime,cyc,k,mark,cl,clon,clat,crad,cslp) 
        
             cyc=cyc+1                ! record as one cyclone and jump to next
             irec=t+cl(cyc-1,k)%lt
           !  print *,cyc
             exit
            endif
          endif
       enddo
     endif

     endif
     enddo
   enddo


  



end program


subroutine filltime(winstart,winend,allti)

integer,intent(in) :: winstart,winend
integer,intent(inout) :: allti(485)

integer im,id,idd,ih,idmax,t,iday

  !!fillthetimeline
   t=1

  iy=winstart

   do im=11,12
     if (mod(iy,4) == 0) then
          idmax=29
      else
          idmax=28
      endif
       
      if (im==1 .or. im==3 .or. im==5 .or. im==7 .or. im==8 .or. im==10 .or. im==12)then
        iday=31
      else if(im==2)then
        iday=idmax
      else
        iday=30
      endif 
 
      do id=1,iday
       do ih=0,18,6
       allti(t)=ih+id*100+im*10000+iy*1000000
       t=t+1
       enddo
      enddo
    enddo


    iy=winend
     do im=1,2
     if (mod(iy,4) == 0) then
          idmax=29
      else
          idmax=28
      endif
       
      if (im==1 .or. im==3 .or. im==5 .or. im==7 .or. im==8 .or. im==10 .or. im==12)then
        iday=31
      else if(im==2)then
        iday=idmax
      else
        iday=30
      endif 
 
      do id=1,iday
       do ih=0,18,6
       allti(t)=ih+id*100+im*10000+iy*1000000
       t=t+1
       enddo
      enddo
    enddo




end subroutine

subroutine cycprint(alltime,cyc,k,mark,cl,clon,clat,crad,cslp) 
     use param
     integer,intent(in)::  alltime(485,yr),cyc,k,mark(485,yr)
     type(cyclone),intent(inout):: cl(25,yr)
     real,intent(in) :: clon(485,yr,cmax),clat(485,yr,cmax),crad(485,yr,cmax),cslp(485,yr,cmax)
     integer cmark,cc
     character(len=2) :: cycnum
     character(len=4) :: cycyear
  
             write(cycnum,'(i2.2)')cyc
             write(cycyear,'(i4.4)') startyear+k-1
             open(404,file="output/"//cycyear//cycnum//".asc")
             do i=0,cl(cyc,k)%lt-1
           
             if(mark(cl(cyc,k)%st+i,k)==1)then
                     cmark=mark(cl(cyc,k)%st+i,k)
             write(404,'(i11,3f7.1,f9.2,i4)') alltime(cl(cyc,k)%st+i,k),clon(cl(cyc,k)%st+i,k,cmark),&
                     &clat(cl(cyc,k)%st+i,k,cmark),&
                     &crad(cl(cyc,k)%st+i,k,cmark),cslp(cl(cyc,k)%st+i,k,cmark),cmark
       
             else
              do cmark=1,mark(cl(cyc,k)%st+i,k)
                write(404,'(i11,3f7.1,f9.2,i4)') alltime(cl(cyc,k)%st+i,k),clon(cl(cyc,k)%st+i,k,cmark),&
                     &clat(cl(cyc,k)%st+i,k,cmark),&
                     &crad(cl(cyc,k)%st+i,k,cmark),cslp(cl(cyc,k)%st+i,k,cmark),cmark
              enddo
             endif
             enddo
            
             close(404)

end subroutine



Program smooth_path

   integer iy,i
   character(len=4) cyear
   character(len=2) cnum
   real lon(1000),lat(1000),radius(1000),slp(1000)
   integer num,time(1000)

   open(20,file="output/allpath.asc")
   open(21,file="output/north.asc")
   open(22,file="output/middle.asc")
   open(23,file="output/south.asc")

   do iy=2003,2010  ! counter for winter
    
      do num=1,99  ! a big num

      write(cyear,'(i4.4)') iy    
      write(cnum,'(i2.2)') num

      open ( 201,file="output/"//cyear//cnum//".asc",status="old",err=100)
      do i=1,1000
      read(201,*,end=200) time(i),lon(i),lat(i),radius(i),slp(i)
      write(20,'(3f9.2,f10.2,i12)')lon(i),lat(i),radius(i),slp(i),time(i)
      if( lat(1) > 45.0 )then
         write(21,'(3f9.2,f10.2,i12)')lon(i),lat(i),radius(i),slp(i),time(i)
      elseif(lat(1) >= 35.0)then
         write(22,'(3f9.2,f10.2,i12)')lon(i),lat(i),radius(i),slp(i),time(i)
      else
         write(23,'(3f9.2,f10.2,i12)')lon(i),lat(i),radius(i),slp(i),time(i)
      endif


      enddo
200   write(20,'(i10)') 8888888888 ! end signal
      write(21,'(i10)') 8888888888 ! end signal
      write(22,'(i10)') 8888888888 ! end signal
      write(23,'(i10)') 8888888888 ! end signal
      close(201)
100   continue

      enddo  ! num

   enddo ! iy

     

end program
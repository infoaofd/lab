#!/bin/sh


rm *.o
ifort -c -CB -module ./ -o track.o cyclone_track.f90 -traceback
ifort -c -CB -module ./ -o param.o param.f90 -traceback
ifort -o ./track ./track.o ./param.o -module ./


./track
rm reformatted.asc
rm -r output/
mkdir output
ifort -c -CB -module ./ -o path.o cyclone_path.f90 -traceback
ifort -o ./path ./path.o ./param.o -module ./
./path
ifort -o ./smpath smooth_path.f90
./smpath

cd plot
sh plotpath.sh
cd ..


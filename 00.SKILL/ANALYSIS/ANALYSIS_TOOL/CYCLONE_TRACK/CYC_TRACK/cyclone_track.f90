
program cyclone_track

 ! modified

  use param
  character(len=4) :: cyear
  character(len=2) :: cmon
  character(len=2) :: cday
  character(len=2) :: chour
  character(len=500) :: A
  real,allocatable :: slp(:,:)
  integer iyy,imm,idd,idmax,iday,ihh,k
  character(len=3) :: cname
  !Cyclone center
  real,allocatable :: radius(:,:)
  integer,allocatable :: mark(:,:)

  type cyclone
    real lon,lat
    real slp
    integer i,j,t,m
  end type


  real :: lon(im),lat(jm)
  real :: angle(2),dangle ! angle of path when distance larger than 3


   open(30,file="cyclone_center.asc")
  

  allocate(slp(im,jm))
  allocate(radius(im,jm),mark(im,jm))


 do iyy=2003,2011

  
   do imm=1,12


      if (imm==1 .or. imm==2 .or. imm==11 .or. imm==12)then
      
      if (mod(iyy,4) == 0) then
          idmax=29
      else
          idmax=28
      endif
       
      if (imm==1 .or. imm==3 .or. imm==5 .or. imm==7 .or. imm==8 .or. imm==10 .or. imm==12)then
        iday=31
      else if(imm==2)then
        iday=idmax
      else
        iday=30
      endif 
      
      if ( iyy== 2003 .and. imm <= 3) cycle
     do idd=1,iday
       do ihh=0,18,6

          write(cmon,'(i2.2)') imm 
          write(cday,'(i2.2)') idd 
          write(cyear,'(i4.4)') iyy
          write(chour,'(i2.2)')ihh
         !!! READ SLP DATA 
          open(20,file="/misc/work2/ningz/processing/read_slp_FNL/output/slp"//cyear//cmon//cday//"_"//chour//".asc")
          do j=1,jm
           do i=1,im
           read(20,*) lon(i),lat(j),slp(i,j)
           enddo
          enddo
          close(20)



           !!! FIND all the cyclone centers
   call  cyceye(lon,lat,slp,mark)
   ! Mark=1 means the cyclone center

   !!! Find Effective circular disk
   call  cycdisk(lon,lat,mark,slp,radius)
  ! call  cycdisk(lon,lat,mark,slp,radius)
 
 
      do i=1,im
      do j=1,jm
      if(mark(i,j) ==1) then
      if( iyy+imm > 2005 .and. iyy+imm < 2022) then
       if( lon(i) <= ebd .and. lat(j) <=nbd) then
      write(30,'(a,2f6.1,2f9.2)')cyear//cmon//cday//"_"//chour,lon(i),lat(j),radius(i,j),&
              &slp(i,j)
       endif
      endif
      endif
      enddo
      enddo


       enddo
     enddo


     endif
  enddo

enddo



  close(30)   
 
  

end program cyclone_track

subroutine cyceye(lon,lat,slp,mark)
  ! This subroutine detect the cyclone center when the
  ! pressure difference between the center and all its
  ! adjacent grids is smaller than -0.5 hPa
  ! ( hayasaki and kawamura, 2012, SOLA)
  ! And, the center should also pass the identification
  ! of local maximum relative vorticity which filtered by
  ! 1-1-1 spatial-filter ( Flaounas, et al., 2014, Geosci. Model Dev.)
  use param
  real,intent(in) :: lon(im),lat(jm),slp(im,jm)
  integer,intent(inout) :: mark(im,jm)
  real,allocatable :: dslp(:) ! adjecent grids
  integer x,ii,jj,nm,k
  
  ! X: distance
  ! adjecent grids number: nm
  ! nm= (2X+1)^2 -1
  integer counter
  ! total number of adjecent grids
  x=ifilter
  nm=(2*x+1)**2-1
 
 ! print *,"RV threshold is",thres

  allocate(dslp(nm))
  mark(:,:)=0
  


     do i=x+1,im-x-1
      do j=x+1,jm-x-1
      if(lon(i) > wbd .and. lat(j) > sdb) then
      dslp(:)=0.0
       n=1
       do ii=i-x,i+x
        do jj=j-x,j+x
        if(ii==i .and. jj==j) cycle
        if(slp(ii,jj) > -100.) then
         dslp(n)=slp(i,j)-slp(ii,jj)
         endif
        n=n+1
        enddo
       enddo
       
   ! print *,n
!       print *,drv,dpslv
       counter=0
       do n=1,nm

   !     if (smrv(i,j,t,m,l) > thres .and. drv(n) > 0. .and.  dslp(n) < -0.5 ) then
        if ( slp(i,j) <= SLPthres .and.  dslp(n) <= -0.5 ) then ! 
        counter=counter+1
        endif
       enddo
 
  
   !    print *,counter
       if(counter == int(nm*2/3))then
        if(lon(i) <= ebd .and. lat(j) <= nbd) then
        mark(i,j)=1
        endif
       endif

     endif
     enddo
    enddo
   deallocate(dslp)

! open(202,file="mark.bin",form="unformatted")
! write(202)mark
! close(202)
 end subroutine cyceye



 subroutine cycdisk(lon,lat,mark,slp,radius)
 ! This subroutine find the cyclone effective circular
 ! Criterion:
 ! 1) the average of all grid in the disk are lower than threshold value
 ! 2) Radius reaches a pre-defined maximum
 ! 3) another larger maximum spot is found

 use param
 real,intent(in) :: lon(im),lat(jm)
 integer,intent(inout) :: mark(im,jm)
 real,intent(in) :: slp(im,jm)
 real,intent(out) :: radius(im,jm)
 real dis(im,jm)
 integer dl,disloop,k
 real c1,avrv
 integer maxi,maxj
  
 disloop=int(max_radius/disint) ! maximum loop times based on the max radius
 radius(:,:)=0.0
102 continue
 do i=1,im
  do j=1,jm
  ! when the center is find
  if (mark(i,j)==1) then
  ! compute the distance in degree
   do ii=1,im
    do jj=1,jm
    dis(ii,jj)=abs(sqrt((lon(ii)-lon(i))**2.0+(lat(jj)-lat(j))**2.0))
    enddo
   enddo
   dis(i,j)=0.0 !start point

   !!! loop for circular 
   do dl=1,disloop
   
   avrv=0.0 !reset every loop
   c1=0.0 

    do ii=1,jm
     do jj=1,jm
     if( dis(ii,jj) <= real(dl)*disint ) then
       
       ! if any surrounding spot is lower than center
       if (slp(ii,jj) < slp(i,j)) then
          mark(i,j)=0 ! set it non-center
          mark(ii,jj)=1 ! set it to be a new center 
          goto 102  !skip this non-center
       endif
       
       if(slp(ii,jj) > -100.) then
          avrv=avrv+slp(ii,jj)
          c1=c1+1.0
       endif
     endif
    enddo
   enddo
   
   ! Find the average value of this disk
   if (c1>0.0) then
     avrv=avrv/c1
   else
     avrv= SLPthres+1.0  ! larger than SLPthres
   endif

   if(avrv < SLPthres) then  ! Area Average lower 1005
     cycle  !go to the next loop
   else
     radius(i,j)=real(dl)*disint  ! if not pass the criterion 1, set the radius
    goto 101
   endif
 
   enddo ! loop circular
   !if reaches the maximum radius
   radius(i,j) = max_radius

  endif

101  CONTINUE
   enddo
 enddo

  where(mark==1 .and. radius<=1.25) radius=1.25
!  where(mark==1) radius=max_radius
!! remove any cyclone overlapping centers
! Important 
!print *,maxi,maxj

do i=1,im
 do j=1,jm
   if (mark(i,j) ==1) then
    do ii=1,im
    do jj=1,jm
    dis(ii,jj)=0.0
    if( ii/=i .and. j/=j) dis(ii,jj)=abs(sqrt((lon(ii)-lon(i))**2.0+(lat(jj)-lat(j))**2.0))
    if(dis(ii,jj) <= radius(i,j) .or. dis(ii,jj) <= radius(ii,jj)) then ! if dis
!            shorter than any radius of two cyclones
      if( radius(i,j) > radius(ii,jj)) mark(ii,jj) =0  ! remove smaller inner centers
      if( radius(i,j) < radius(ii,jj)) mark(i,j)=0 ! remove current center if overlapped
    endif
    enddo
   enddo
 
  endif
  enddo
 enddo



end subroutine cycdisk
  


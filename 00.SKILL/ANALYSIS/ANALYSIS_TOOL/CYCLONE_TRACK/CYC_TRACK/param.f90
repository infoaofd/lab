module param
 
   integer,parameter :: im=76,jm=50
   real,parameter :: grav=9.81,fillvalue=-999.9
   real,parameter :: d2s= 86400.0 ! 1 day = 86400 seconds
   real,parameter :: pi=3.1415926
   integer,parameter :: ifilter=1
   ! ifilter=1:  3x3 grid
   ! ifilter=2:  5x5
   ! ifilter=3:  7x7
   real,parameter :: thres=3.E-5,SLPthres=1010.  ! Relative vorticity/SLP threshold 
   real,parameter :: max_radius=10.0  ! maximum effective range in degrees
   real,parameter :: disint=0.5 ! interval for circular disk basically based on the data resolution

   real,parameter :: oradius=5.0   ! max distance before using angle criterion ! angle criterion has been abandoned
   integer,parameter :: iiiend=1 !  How many times to pass the filter (iiiend>1 only recommended for datasets of high resolution <50km)
   real,parameter :: wbd=120.0,ebd=160.0 ! western and eastern boundary for detecting cyclone
   real,parameter :: sbd=25.0,nbd=65.0  ! southern and northern boundary for detecting cyclone
   
 
   integer i,j,t,ka,kb,kc,irec,fil
   integer n,l,m
!!!!!! FOR Path

   integer,parameter :: nmax=10000
   integer,parameter :: yr=9
   integer,parameter :: startyear=2003
    integer,parameter :: cmax=100 ! max num of cyclone in one frame
       type cyclone
     integer st  ! start time in all time line
     integer sn   ! start time in cyclone center file
     integer :: lt   ! how long it exists
   endtype cyclone
   
end module

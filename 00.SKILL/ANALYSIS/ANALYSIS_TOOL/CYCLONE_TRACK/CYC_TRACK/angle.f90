program angle

! The angle criterion was about to ensure that cyclone would not turn around over 180deg;
! however, it was no longer needed for cyc_track.
! This is an example program for calculating angle using three points (i.e., consecutive cyclone centers).
!
implicit none
real x1,x2,x3
real y1,y2,y3
real A,B,C,D,E
integer i,j
real,parameter :: pi=3.141592653

x1=-3.8
y1=-1.8
x2=8.2
y2=-1.6
x3=5.1
y3=4.7

A=x1-x2
B=y1-y2

C=abs(atan2(B,A)/pi*180.)

A=x3-x2
B=y3-y2

D=abs(atan2(B,A)/pi*180.)

E=abs(360.0-C-D)


end program

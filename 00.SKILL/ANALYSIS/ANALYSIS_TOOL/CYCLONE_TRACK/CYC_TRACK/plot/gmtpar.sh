
#
# GMTのパラメータの設定 (version 4以上対象）
#
# 長さの単位はインチとする
gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 18
gmtset TICK_PEN 4
# 地図の縦横軸に縞々を使わない
gmtset BASEMAP_TYPE PLAIN
# 紙のサイズはA4
gmtset PAPER_MEDIA A4
# 地図の°の記号
gmtset DEGREE_SYMBOL = degree # Available for ver. 4 or later
# 空白文字の設定
sp="\040"  # White space
# =の記号
eq="\075"  # equal
# 温度の°の記号
deg="\260" #deg="\312"  # degree symbol

# 色のRGB値を設定 (線や点に色をつけるときRGB値を直接指定するより分かりやすい)
red="255/0/0"
green="0/255/0"
blue="0/0/255"
white="255/255/255"

# 数字の出力フォーマット(project, grd2xzy等で使う)
gmtset D_FORMAT %lg #デフォルト(規定値)
#  桁数を指定（例：123.45678)
#gmtset D_FORMAT %12.6f
#  状況に応じて
#gmtset D_FORMAT %12.5g

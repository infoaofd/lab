dset /raid2/iizuka/JRA55/bin/6hr/slp/slp.%y4%m2%d2%h2.bin
*options  byteswapped template
options  yrev template
undef 1.e30
title  OUTPUT FROM WRF V3.0.1.1 MODEL
xdef 288  linear 0 1.25
ydef 145  linear -90.0 1.25
zdef  14 levels  
                  1000 925 790 700 600 500 400 300 250 200
                   150 100  70  50  
tdef 65521 linear 00Z01jan1979 6hr
VARS    1
slp         0   0  Sea Levelp Pressure (hPa)
ENDVARS

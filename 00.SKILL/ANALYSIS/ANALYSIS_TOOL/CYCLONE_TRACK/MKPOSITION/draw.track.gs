* Script to draw a cyclone-track plot.
* Also assumes that a file has been opened (any file, doesn't matter);
* the set command doesn't work until a file has been opened.

function pstf (args)
fname=subwrd(args,1)

marktype = 3
marksize = 0.1
lcolor   = 1
lstyle   = 1
lthick   = 3
start    = 1
jump     = 1
deltax   = 0.1
deltay   = 0.1

Mon.01='Jan';Mon.02='Feb';Mon.03='Mar';Mon.04='Apr'
Mon.05='May';Mon.06='Jun';Mon.07='Jul';Mon.08='Aug'
Mon.09='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

* Read all data points
ret = read(fname)
rc  = sublin(ret,1)

if (ret='' | rc!=0)
  say 'Error : No file 'fname
  res = close(fname)
  return
endif

ii=0
while (rc = 0)
  ii=ii+1
  loc      = sublin(ret,2)
  num.ii   = subwrd(loc,3)
  year.ii  = subwrd(loc,4)
  mon.ii   = subwrd(loc,5)
  day.ii   = subwrd(loc,6)
  hour.ii  = subwrd(loc,7)
  lon.ii   = subwrd(loc,10)
  lat.ii   = subwrd(loc,11)
  slp.ii   = subwrd(loc,12)
  delp.ii  = subwrd(loc,13)
  ibomb.ii = subwrd(loc,14)
  ipmin.ii = subwrd(loc,15)

  ret      = read(fname)
  rc       = sublin(ret,1)
  loc      = sublin(ret,2)
endwhile
res = close(fname)
endrec = ii
*say ' file = ' fname
*say ' end_record = ' endrec
*
'query w2xy 'lon.start' 'lat.start
xprev = subwrd(result,3)
yprev = subwrd(result,6)
'set line 4 'lstyle' 'lthick
'draw mark 'marktype' 'xprev' 'yprev' 'marksize

next = start+jump
while (next <= endrec)
  'query w2xy 'lon.next' 'lat.next
   xnext = subwrd(result,3)
   ynext = subwrd(result,6)
  'set line 'lcolor' 'lstyle' 'lthick
  'draw line 'xprev' 'yprev' 'xnext' 'ynext

  if (ibomb.next != 0)
    'set line 2 'lstyle' 'lthick
    'draw mark 'marktype' 'xnext' 'ynext' 'marksize
  endif
  if (ipmin.next != 0)
    'set line 7 'lstyle' 'lthick
    'draw mark 'marktype' 'xnext' 'ynext' 'marksize
  endif

  next = next+jump
  xprev = xnext
  yprev = ynext
endwhile

return

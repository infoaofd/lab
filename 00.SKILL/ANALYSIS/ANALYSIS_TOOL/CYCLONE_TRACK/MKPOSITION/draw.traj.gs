* Script to draw a cyclone-track plot.
* Also assumes that a file has been opened (any file, doesn't matter);
* the set command doesn't work until a file has been opened.

function pstf (args)
fname=subwrd(args,1)

marktype = 3
marksize = 0.1
lcolor   = 2
lstyle   = 1
lthick   = 6
start    = 1
jump     = 1
deltax   = 0.1
deltay   = 0.1

Mon.01='Jan';Mon.02='Feb';Mon.03='Mar';Mon.04='Apr'
Mon.05='May';Mon.06='Jun';Mon.07='Jul';Mon.08='Aug'
Mon.09='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

* Read all data points
while (1)
  ret = read(fname)
  rc  = sublin(ret,1)

  if (rc > 0)
    if (rc = 2)
      say 'EOF : Finished reading 'filename
      break
    else
      say 'Error : while reading  'filname
      res = close(filename)
      break
    endif
  endif

  ii=0
  while (1)
    ii=ii+1
    loc      = sublin(ret,2)
    num.ii   = subwrd(loc,3)
    year.ii  = subwrd(loc,4)
    mon.ii   = subwrd(loc,5)
    day.ii   = subwrd(loc,6)
    hour.ii  = subwrd(loc,7)
    dlon.ii  = subwrd(loc,10)
    dlat.ii  = subwrd(loc,11)
    slp.ii   = subwrd(loc,12)
    delp.ii   = subwrd(loc,13)
    ibomb.ii   = subwrd(loc,14)
    ipmin.ii   = subwrd(loc,15)

    ret      = read(fname)
    rc       = sublin(ret,1)
    loc      = sublin(ret,2)
    flag     = subwrd(loc,1)
    say flag
    if (flag='' | rc!=0)
      break
    endif
  endwhile

  endrec = ii
  say ' file = ' fname
  say ' end_record = ' endrec
endwhile

ii = 1
while (ii <= endrec)
*while (ii <= 1)
  'clear'
   month=mon.ii
  'set time 'hour.ii'z'day.ii''Mon.month''year.ii
* 'set gxout shade2'
* 'd maskout(wspd.2,wspd.2-0)'
  'set gxout contour'
  'set cint 2'
  'd slp/100'
  'draw title 'hour.ii'Z 'day.ii' 'Mon.month' 'year.ii
*
  'query w2xy 'dlon.start' 'dlat.start
   xprev = subwrd(result,3)
   yprev = subwrd(result,6)
  'set line 4 'lstyle' 'lthick
  'draw mark 'marktype' 'xprev' 'yprev' 'marksize*2
*
   next = start+jump
   while (next <= endrec)
*
     'query w2xy 'dlon.next' 'dlat.next
      xnext = subwrd(result,3)
      ynext = subwrd(result,6)
     'set line 'lcolor' 'lstyle' 'lthick
     'draw line 'xprev' 'yprev' 'xnext' 'ynext

      if (next != ii)
        if (ibomb.next != 0)
          'set line 7 'lstyle' 'lthick
          'draw mark 'marktype' 'xnext' 'ynext' 'marksize*2
        else
          if (ipmin.next != 0)
            'set line 3 'lstyle' 'lthick
            'draw mark 'marktype' 'xnext' 'ynext' 'marksize*2
          else
            'set line 9 'lstyle' 'lthick
            'draw mark 'marktype' 'xnext' 'ynext' 'marksize
          endif
        endif
      else
          'set line 2 'lstyle' 'lthick
          'draw mark 'marktype' 'xnext' 'ynext' 'marksize*2
      endif
      next = next+jump
      xprev = xnext
      yprev = ynext
   endwhile
   say ' ii = 'ii 
  'q pos'
   ii=ii+1
endwhile

return

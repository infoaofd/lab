#!/bin/csh -f
# /raid11/iixxxx/old_raid5/cyclone/track
set FC = "ifort"
set OPT = "-CB -assume byterecl"

cd TRACK
/bin/rm *.txt

set DIR = "POSITION"

set year = 1979
set end_year = 2019

set nmon = (01)
foreach mon ($nmon)
/bin/cat "../$DIR/LOW${year}${mon}.txt" >> low.now.txt
end

while ($year <= $end_year)

@ year_m1 = $year - 1
@ year_p1 = $year + 1

set nmon = (02 03 04 05 06 07 08 09 10 11 12)
foreach mon ($nmon)
/bin/cat "../$DIR/LOW${year}${mon}.txt" >> low.now.txt
end

if ($year != $end_year) then
set nmon = (01)
foreach mon ($nmon)
/bin/cat "../$DIR/LOW${year_p1}${mon}.txt" >> low.now.txt
end
endif

#cp low.now.txt low.$year.txt
echo $year
cat > a.f << EOF
      program find_track
      implicit none
!======================================================================
!
!======================================================================

! ... domain
      real   , parameter :: slon = 120.0
      real   , parameter :: elon = 160.0
      real   , parameter :: slat =  20.0
      real   , parameter :: elat =  60.0

! ... radius to serach cyclones at next step
      real   , parameter :: def_rad = 600.0e3

! ... maximum distance of cyclone movement at next step
      real   , parameter :: def_dis = 1000.0e3

! ... increment time of data : unit is hour
      integer, parameter :: dt=6

! ... maximum cyclone number each sampling time
      integer, parameter :: max_cpd = 200

! ... maximum time dimension of data (1yr+leap+dec+jan)
      integer, parameter :: ntim    = 366*4 + 31*4 + 31*4

!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
      integer :: iyr, mon, idy, ihr
      integer :: ic, jc
      integer :: iover(99)
      real    :: lon1, lat1, slp1, topo1
      real    :: dlon, dlat, unow, vnow, uprv, vprv, dudt, dvdt
      real    :: pnext

      integer :: numc(ntim)
      integer :: id  (4,ntim)
      integer :: ilon(max_cpd,ntim)
      integer :: jlat(max_cpd,ntim)
      real    :: lon (max_cpd,ntim)
      real    :: lat (max_cpd,ntim)
      real    :: slp (max_cpd,ntim)
      real    :: topo(max_cpd,ntim)
      integer :: used(max_cpd,ntim)
      integer :: list(max_cpd,ntim)

      integer :: index(max_cpd)
      real    :: dist1(max_cpd)
      integer :: mhr
      real    :: dis
      real    :: dp
      real    :: radius, pi, deg2rad

      integer :: jd   (4,ntim)
      integer :: ipmin(ntim)
      integer :: ibomb(ntim)
      integer :: ip   (ntim)
      integer :: jp   (ntim)
      real    :: xlon (ntim)
      real    :: xlat (ntim)
      real    :: xslp (ntim)
      real    :: xdpdt(ntim)
      real    :: xspd (ntim)

      character(len=72) :: ifile = 'low.now.txt'
      character(len=72) :: ofile
      integer :: ier
      integer :: itim
      integer :: num
      integer :: num_cyc
      integer :: n, m, i, j, k
      integer :: iexist
      real    :: tmp
      real    :: xvec1, yvec1, avec1
      real    :: xvec2, yvec2, avec2
      real    :: angle

!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
      radius  = 6370.0e3
      pi      = 4.*atan(1.)
      deg2rad = pi/180.0

!----------------------------------------------------------------------
!     read cyclone position
!----------------------------------------------------------------------
      open(10,file=trim(ifile),form='formatted')
      jlat = 0
      ilon = 0
      lat  = 0
      lon  = 0
      slp  = 0
      itim = 0
      mhr  = -99
      n    = 0
      do
        read (10,*,iostat=ier)
     &    iyr,mon,idy,ihr,i,j,ic,jc,
     &    lat1, lon1, slp1, topo1
        if (ier /= 0) exit

        if (ihr /= mhr) then
          itim = itim + 1
          mhr  = ihr
          n    = 0
        end if

!       if (topo1 > 1000.0) cycle
!       if (slp1  > 1013.0) cycle
        n = n + 1

        id  (1,itim) = iyr
        id  (2,itim) = mon
        id  (3,itim) = idy
        id  (4,itim) = ihr
        numc  (itim) = n
        jlat(n,itim) = jc
        ilon(n,itim) = ic
        lat (n,itim) = lat1
        lon (n,itim) = lon1
        slp (n,itim) = slp1
        topo(n,itim) = topo1
      end do
      close(10)

!----------------------------------------------------------------------
!     serach the nearest cyclone position
!----------------------------------------------------------------------
      list = 0
      do n = 1, itim-1
        m = n + 1

        dist1 = radius*100.0
        index = 0
        do i = 1, numc(n)

! ... find the nearest cyclone position at next step
          do j = 1, numc(m)
            dis = sin(deg2rad*lat(j,m)) * sin(deg2rad*lat(i,n))
     &          + cos(deg2rad*lat(j,m)) * cos(deg2rad*lat(i,n)) 
     &          * cos(deg2rad*(lon(i,n)-lon(j,m)))
            dis = max(min(dis,1.0),-1.0) ! to remove numerical error
            dis = acos(dis)*radius
            dist1(j) = dis
          end do
          j = minloc(dist1,dim=1)
          dis = dist1(j)
          if (dis > def_rad) cycle
          index(i) = j
        end do

! ... find the nearest cyclone position at previous step
        do i = 1, numc(n)
          if (index(i) == 0) cycle
          j = index(i) ! id for next step
          dist1 = radius*100.0
          do k = 1, numc(n)
            dis = sin(deg2rad*lat(j,m)) * sin(deg2rad*lat(k,n))
     &          + cos(deg2rad*lat(j,m)) * cos(deg2rad*lat(k,n)) 
     &          * cos(deg2rad*(lon(k,n)-lon(j,m)))
            dis = max(min(dis,1.0),-1.0) ! to remove numerical error
            dis = acos(dis)*radius
            dist1(k) = dis
          end do
          k = minloc(dist1,dim=1)
          if (k /= i) index(i) = 0 ! not pair
        end do

        do i = 1, numc(n)
          list(i,n) = index(i)
        end do

        do i = 1, numc(n)
          if (list(i,n) == 0) cycle
          j = list(i,n)
!         write(*,66) i,lon(i,n),lat(i,n),slp(i,n),
!    &                j,lon(j,m),lat(j,m),slp(j,m)
66        format(2(1x,i2,3(1x,f6.1)))
        end do

      end do
!     stop

!----------------------------------------------------------------------
!     make cyclone track
!----------------------------------------------------------------------
      used = 0
      num_cyc = 0
      do n = 1, itim-1

        if (id(1,n) /= ${year}) exit

        do i = 1, numc(n)

          if (used(i,n) /= 0) cycle

! ... setp 1 : set initial position
          xslp  = 9000
          num = 1
          jd (1,num) = id  (1,n)
          jd (2,num) = id  (2,n)
          jd (3,num) = id  (3,n)
          jd (4,num) = id  (4,n)
          ip   (num) = ilon(i,n)
          jp   (num) = jlat(i,n)
          xlon (num) = lon (i,n)
          xlat (num) = lat (i,n)
          xslp (num) = slp (i,n)

! ... step 2 : seearch the nerarest point
          do m = n+1, n+1 ! search using nearest points
            j = list(i,m-1) ! j is the cyclone id for next step
            if (j == 0) exit
            if (used(j,m) /= 0) cycle

            num = num + 1
            jd (1,num) = id  (1,m)
            jd (2,num) = id  (2,m)
            jd (3,num) = id  (3,m)
            jd (4,num) = id  (4,m)
            ip   (num) = ilon(j,m)
            jp   (num) = jlat(j,m)
            xlon (num) = lon (j,m)
            xlat (num) = lat (j,m)
            xslp (num) = slp (j,m)
            used (j,m) = 1
            used (i,n) = 1
          end do
          if (num < 2) cycle

! ... step 3 : search the nerarest point based on the tranlation speed
          do m = n+2, itim
            if (num == 2) then
              dlon = (xlon(num)-xlon(num-1))
              dlat = (xlat(num)-xlat(num-1))
            else
              unow = (xlon(num)-xlon(num-1))
              vnow = (xlat(num)-xlat(num-1))
              uprv = (xlon(num-1)-xlon(num-2))
              vprv = (xlat(num-1)-xlat(num-2))
              dudt = (unow-uprv)/dt
              dvdt = (vnow-vprv)/dt
!             dlon = (unow + dudt*dt)
!             dlat = (vnow + dvdt*dt)
              dlon = (unow + uprv)*0.5
              dlat = (vnow + vprv)*0.5
            end if
!           dlon = (xlon(num)-xlon(num-1))
!           dlat = (xlat(num)-xlat(num-1))
            lon1 = xlon(num) + dlon*0.75
            lat1 = xlat(num) + dlat*0.75
!           lon1 = xlon(num) + dlon*0.5
!           lat1 = xlat(num) + dlat*0.5

            pnext = xslp(num) + (xslp(num)-xslp(num-1))*0.55
            dist1 = radius*100.0
            do k = 1, numc(m)
              if (used(k,m) /= 0) cycle
              dp  = slp(k,m) - pnext
              if (abs(dp) > 19.0) cycle
              dis = sin(deg2rad*lat(k,m)) * sin(deg2rad*lat1)
     &            + cos(deg2rad*lat(k,m)) * cos(deg2rad*lat1) 
     &            * cos(deg2rad*(lon1-lon(k,m)))
              dis = max(min(dis,1.0),-1.0) ! to remove numerical error
              dis = acos(dis)*radius
              dist1(k) = dis
            end do
            k = minloc(dist1,dim=1)
            dis = dist1(k)
            if (dis > def_rad) exit ! not find any cyclones

            dis = sin(deg2rad*lat(k,m)) * sin(deg2rad*xlat(num))
     &          + cos(deg2rad*lat(k,m)) * cos(deg2rad*xlat(num)) 
     &          * cos(deg2rad*(xlon(num)-lon(k,m)))
            dis = max(min(dis,1.0),-1.0) ! to remove numerical error
            dis = acos(dis)*radius
            if (dis > def_dis) exit ! not find any cyclones

            xvec1 = (lon1    -xlon(num))
            yvec1 = (lat1    -xlat(num))
            xvec2 = (lon(k,m)-xlon(num))
            yvec2 = (lat(k,m)-xlat(num))
            avec1 = sqrt(xvec1**2 + yvec1**2)
            avec2 = sqrt(xvec2**2 + yvec2**2)
            if (avec1*avec2 < 1.e-10) then
              angle = 0.0
            else
              angle = (xvec1*xvec2+yvec1*yvec2)/(avec1*avec2)
            end if
            angle = acos(angle)/deg2rad

            num = num + 1
            jd (1,num) = id  (1,m)
            jd (2,num) = id  (2,m)
            jd (3,num) = id  (3,m)
            jd (4,num) = id  (4,m)
            ip   (num) = ilon(k,m)
            jp   (num) = jlat(k,m)
            xlon (num) = lon (k,m)
            xlat (num) = lat (k,m)
            xslp (num) = slp (k,m)
            used (k,m) = 1
          end do
! ... end of search algorithm

! ... exclude stationaly cyclones
          tmp = 0
          do m = 1, num-1
            k = m+1
            dis = sin(deg2rad*xlat(m)) * sin(deg2rad*xlat(k))
     &          + cos(deg2rad*xlat(m)) * cos(deg2rad*xlat(k)) 
     &          * cos(deg2rad*(xlon(k)-xlon(m)))
            dis = max(min(dis,1.0),-1.0) ! to remove numerical error
            dis = acos(dis)*radius
            tmp = tmp + dis
          end do
          if (tmp <= 100.e3) then 
!           print *,'check:dis',tmp,xlon(1),xlon(num)
            cycle
          end if

! ... only cyclones passing the specified domain for more than one-day are saved
          iexist = 0
          do m = 1, num
            if (xlon(m) >= slon .and. 
     &          xlon(m) <= elon .and.
     &          xlat(m) >= slat .and.
     &          xlat(m) <= elat ) then
              iexist = iexist + 1
            end if
          end do
          if (iexist <= 0) cycle

! ... exclude cyclones with the lifetime less than 1 day
          if (num <= 4) cycle

! ... compute dpdt
          xdpdt = 0
          do m = 3, num-2
!           tmp = sin(60.0*deg2rad)/sin(xlat(m)*deg2rad)
            tmp = sin(45.0*deg2rad)/sin(xlat(m)*deg2rad)
            xdpdt(m) = -(xslp(m+2)-xslp(m-2))/(dt*4.0)*tmp
          end do

! ... compute translate speed
          xspd  = 0
          do m = 1, num-1
            dis = sin(deg2rad*xlat(m)) * sin(deg2rad*xlat(m+1))
     &          + cos(deg2rad*xlat(m)) * cos(deg2rad*xlat(m+1)) 
     &          * cos(deg2rad*(xlon(m+1)-xlon(m)))
            dis = max(min(dis,1.0),-1.0) ! to remove numerical error
            dis = acos(dis)*radius
            xspd(m) = dis/1000.0/dt
          end do

! ... serach time when cyclone takes min slp and max dp/dt
          ipmin = 0
          ibomb = 0
          j = minloc(xslp,dim=1)
          ipmin(j) = 1
          j = maxloc(xdpdt,dim=1)
          if (xdpdt(j) > 0) ibomb(j) = 1

! ... pick up only bomb
!         if (xdpdt(j) < 1.0) cycle
! ... check
!         if (xlat(j) < 30.0) then
!           print *,'',trim(ofile)
!         end if

! ... write track data
          num_cyc = num_cyc + 1
          write(ofile,'(a,i3.3,a)')
     &      'track.${year}.',num_cyc,'.txt'
          open (90,file=trim(ofile),form='formatted')
          do m = 1, num
            write(90,999)
     &        m,num,num_cyc,
     &        jd(1,m),jd(2,m),jd(3,m),jd(4,m),
     &        ip(m),jp(m),
     &        xlon(m),xlat(m),xslp(m),xdpdt(m),
     &        ibomb(m),ipmin(m)
          end do
          close(90)

        end do
      end do

!----------------------------------------------------------------------
!
!----------------------------------------------------------------------
      ofile = 'low.res.txt'
      open(80,file=trim(ofile),form='formatted')
      do i = 1, itim
        if (id(1,i) == ${year}) cycle
        m = 0
        do n = 1, numc(i)
          if (used(n,i) /= 0) cycle
          m = m + 1
        end do
        j = 0
        do n = 1, numc(i)
          if (used(n,i) /= 0) cycle
          j = j + 1
          write(80,888)
     &       id(1,i),id(2,i),id(3,i),id(4,i),
     &       m, j,
     &       ilon(n,i), jlat(n,i),
     &       lat (n,i), lon (n,i),
     &       slp (n,i), topo(n,i)
        end do
      end do
      close(80)

      stop
999   format(2(1x,i3),1x,i5,1x,
     &       i4.4,1x,i2.2,1x,i2.2,1x,i2.2,
     &       2(1x,i3),
     &       4(1x,f7.2), 
     &       2(1x,i1))
888   format(i4.4,1x,i2.2,1x,i2.2,1x,i2.2,
     &       2(1x,i3),
     &       2(1x,i3),
     &       3(1x,f6.1),
     &       1(1x,f6.1))
      end program find_track
EOF
$FC $OPT a.f && ./a.out
/bin/rm a.f a.out
/bin/mv low.res.txt low.now.txt

@ year = $year + 1
end

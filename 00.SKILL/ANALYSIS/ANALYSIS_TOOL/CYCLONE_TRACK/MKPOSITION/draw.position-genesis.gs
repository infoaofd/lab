* Script to draw a cyclone-track plot.
* Also assumes that a file has been opened (any file, doesn't matter);
* the set command doesn't work until a file has been opened.

function pstf (args)
fname=subwrd(args,1)

marktype = 3
marksize = 0.05
lcolor   = 2
lstyle   = 1
lthick   = 6
start    = 1
jump     = 1
deltax   = 0.1
deltay   = 0.1

Mon.01='Jan';Mon.02='Feb';Mon.03='Mar';Mon.04='Apr'
Mon.05='May';Mon.06='Jun';Mon.07='Jul';Mon.08='Aug'
Mon.09='Sep';Mon.10='Oct';Mon.11='Nov';Mon.12='Dec'

* Read all data points
ii=0
while (1)
  ret = read(fname)
  rc  = sublin(ret,1)

  if (ret='' | rc!=0)
    say 'EOF : Finished reading 'filename
    res = close(fname)
    break
  endif

  ii=ii+1
  loc   = sublin(ret,2)
  num   = subwrd(loc,3)
  year  = subwrd(loc,4)
  mon   = subwrd(loc,5)
  day   = subwrd(loc,6)
  hour  = subwrd(loc,7)
  lon   = subwrd(loc,10)
  lat   = subwrd(loc,11)
  slp   = subwrd(loc,12)
  delp  = subwrd(loc,13)
  ibomb = subwrd(loc,15)
  ipmin = subwrd(loc,16)

* if (ii = 1)
  if (ibomb = 1)
* if (ipmin = 1)
  'query w2xy 'lon' 'lat
   xprev = subwrd(result,3)
   yprev = subwrd(result,6)
  'set line 'lcolor' 'lstyle' 'lthick
  'draw mark 'marktype' 'xprev' 'yprev' 'marksize

    res = close(fname)
    break
  endif
*
  ii=ii+1
endwhile
endrec = ii

return

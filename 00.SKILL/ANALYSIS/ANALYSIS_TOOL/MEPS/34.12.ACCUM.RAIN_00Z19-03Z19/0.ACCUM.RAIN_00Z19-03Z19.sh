#!/bin/bash

INDIR="/work02/MEPS/2022/06/"

NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo ERROR in $NCL;exit 1;fi

DATE_OUT="2022/06/19 00:00:00"
#FHLIST="3"
FHLIST="9 15 21 27"
#FHLIST="21 27"

for FH in $FHLIST; do

if [ $FH -eq 3  ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 9  ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 15 ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 21 ];then FH_INFLE="FH18-33"; fi
if [ $FH -eq 27 ];then FH_INFLE="FH18-33"; fi

if [ $FH -eq 3  ];then DATE_INFLE=$(date -d"${DATE_OUT}  0 day ago"  +%Y%m%d%H); fi
if [ $FH -eq 9  ];then DATE_INFLE=$(date -d"${DATE_OUT}  6 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 15 ];then DATE_INFLE=$(date -d"${DATE_OUT} 12 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 21 ];then DATE_INFLE=$(date -d"${DATE_OUT} 18 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 27 ];then DATE_INFLE=$(date -d"${DATE_OUT}  1 day ago"  +%Y%m%d%H); fi

echo FH=$FH $DATE_INFLE $FH_INFLE

INFLE="Z__C_RJTD_${DATE_INFLE}0000_MEPS_GPV_Rjp_Lsurf_${FH_INFLE}_grib2.bin"
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE $INFLE; exit 1; fi

FHIN=$(printf %02d $FH)
FIG=MEPS_P3H_${DATE_INFLE}_FH${FHIN}

runncl.sh $NCL $INDIR $INFLE $FHIN $FH_INFLE $FIG
echo 
done



INDIR = getenv("NCL_ARG_2")
INFLE = getenv("NCL_ARG_3")
FH=toint(getenv("NCL_ARG_4"))
FH_INFLE=getenv("NCL_ARG_5")
FIG= getenv("NCL_ARG_6")
TYP="pdf"

IN=INDIR+INFLE

a=addfile(IN,"r")

print("")
print("MMMMM INDIR="+INDIR)
print("MMMMM INFLE="+INFLE)

P=925.0
IPRS=toint(P*100)
tmp=a->TMP_P1_L100_GLL0(0,:,{IPRS},0,0)

if (FH_INFLE .eq. "FH00-15")then
FT=tmp&forecast_time0
else
FT=tmp&forecast_time0
end if

dim=dimsizes(FT)

do i=0,dim(0)-1
if (FH .eq. FT(i))then
idx=i
print("FH="+FH+" i="+idx+" forecast_time="+FT(i))
print("")
end if
end do

acc3h=a->APCP_P11_L1_GLL0_acc3h(:,idx,{29:32},{127.5:131})


print("")
print("MMMMM INITIAL_TIME="+acc3h@initial_time)
print("MMMMM "+acc3h@type_of_statistical_processing)
print("MMMMM "+acc3h@statistical_process_duration)
print("MMMMM FORECAST TIME="+FT(idx))

wks = gsn_open_wks(TYP,FIG)             ; send graphics to PNG file

plot = new(21,graphic)                         ; create graphic array
ln = new(21,graphic)                         ; create graphic array

 res                      = True
 res@gsnPaperOrientation = "landscape"
 res@cnFillOn             = True         ; turn on color fill
res@cnFillPalette       = "prcp_1"
 res@gsnDraw              = False        ; do not draw picture
 res@gsnFrame             = False        ; do not advance frame
 res@gsnSpreadColors      = False        ; no longer automatic
 res@gsnAddCyclic = False
res@mpMinLatF =  29     ; 緯度の最小値
res@mpMaxLatF =  32     ; 緯度の最大値
res@mpMinLonF = 127.5     ; 経度の最小値
res@mpMaxLonF = 131     ; 経度の最大値


res@vpHeightF  = 0.3  

  res@cnLineThicknessF        = 0.2             ; 等値線の太さ
  res@cnLineColor        = "white"             ; 等値線の太さ
  res@cnLevelSelectionMode    = "ManualLevels"  ; 等値線の設定をマニュアルにする
  res@cnMinLevelValF  = 10                ; 等値線の最小値
  res@cnMaxLevelValF  = 130              ; 等値線の最大値
  res@cnLevelSpacingF = 10                ; 等値線の間隔
res@cnLineLabelsOn = False
res@lbLabelBarOn        = False           ; turn off individual cb's

res@mpDataBaseVersion = "HighRes"

plx1=(/128. , 129.34, 129.34, 128.,   128./)  
ply1=(/30.15, 30.15,  31.15,  31.15,  30.15/)  
lnres = True  

do n=0,20
res@gsnStringFontHeightF = 0.015
res@gsnLeftString="Ens # "+n
res@gsnCenterString=""
res@gsnRightString=""

plot(n)=gsn_csm_contour_map(wks,acc3h(n,:,:),res)
end do

lnres@gsLineColor = "black"  
do n=0,20
ln(n) = gsn_add_polyline(wks, plot(n), plx1, ply1, lnres)  
end do

plot@ln = ln
return(plot)

; draw panel with white space added
 resP                 = True
 resP@gsnPanelYWhiteSpacePercent = 5
 resP@gsnPanelXWhiteSpacePercent = 5
TITLE="MEPS P3H ending at forecast time    INIT:"+acc3h@initial_time+" FH="+FH
  resP@gsnPanelMainString = TITLE

  resP@gsnPanelLabelBar    = True                ; add common colorbar
  resP@lbTitleOn        = True
  resP@lbTitleString = "mm/3hr" ; title string
  resP@lbTitlePosition = "Right" ; title position
  resP@lbTitleFontHeightF= .012 ;Font size
  resP@lbTitleDirection = "Across" ; title direction

  resP@lbLabelFontHeightF  = 0.01               ; make labels smaller

resP@mpGridLatSpacingF = 0.5
resP@mpGridLatSpacingF = 0.5

 gsn_panel(wks,plot,(/4,6/),resP)

print("MMMMM FIG: "+FIG+"."+TYP)



; float APCP_P11_L1_GLL0_acc3h ( ensemble0, forecast_time1, lat_0, lon_0 )
; long_name :	Total precipitation
; units :	kg m-2
; _FillValue :	1e+20
; grid_type :	Latitude/longitude
; type_of_statistical_processing : Accumulation
; statistical_process_duration : 3 hours (ending at forecast time)

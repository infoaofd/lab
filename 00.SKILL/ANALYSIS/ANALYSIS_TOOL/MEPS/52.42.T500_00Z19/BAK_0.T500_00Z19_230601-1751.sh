#!/bin/bash

INDIR="/work02/MEPS/2022/06/"

NCL=$(basename $0 .sh).ncl
#NCL=CHK.FH.ncl
if [ ! -f $NCL ];then echo ERROR in $NCL;exit 1;fi

DATE_OUT="2022/06/19 00:00:00"
FHLIST="0"
#FHLIST="0 6 12 18 24"

for FH in $FHLIST; do

if [ $FH -eq 0  ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 6  ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 12 ];then FH_INFLE="FH00-15"; fi
if [ $FH -eq 18 ];then FH_INFLE="FH18-33"; fi
if [ $FH -eq 24 ];then FH_INFLE="FH18-33"; fi

if [ $FH -eq 0  ];then DATE_INFLE=$(date -d"${DATE_OUT}  0 day ago"  +%Y%m%d%H); fi
if [ $FH -eq 6  ];then DATE_INFLE=$(date -d"${DATE_OUT}  6 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 12 ];then DATE_INFLE=$(date -d"${DATE_OUT} 12 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 18 ];then DATE_INFLE=$(date -d"${DATE_OUT} 18 hour ago" +%Y%m%d%H); fi
if [ $FH -eq 24 ];then DATE_INFLE=$(date -d"${DATE_OUT}  1 day ago"  +%Y%m%d%H); fi

echo FH=$FH $DATE_INFLE $FH_INFLE

INFLE="Z__C_RJTD_${DATE_INFLE}0000_MEPS_GPV_Rjp_L-pall_${FH_INFLE}_grib2.bin"
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE $INFLE; exit 1; fi

FHIN=$(printf %02d $FH)

P=500
FIG=MEPS_GPH.T_${P}_${DATE_INFLE}_FH${FHIN}

runncl.sh $NCL $INDIR $INFLE $FHIN $FH_INFLE $P $FIG
echo 
done


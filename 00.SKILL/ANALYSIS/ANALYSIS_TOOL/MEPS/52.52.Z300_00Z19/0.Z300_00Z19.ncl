
INDIR = getenv("NCL_ARG_2")
INFLE = getenv("NCL_ARG_3")
FH=toint(getenv("NCL_ARG_4"))
FH_INFLE=getenv("NCL_ARG_5")
P=toint(getenv("NCL_ARG_6"))
FIG= getenv("NCL_ARG_7")
TYP="pdf"

IN=INDIR+INFLE

a=addfile(IN,"r")

print("")
print("MMMMM INDIR="+INDIR)
print("MMMMM INFLE="+INFLE)

p3=P
IPRS=toint(p3*100)
tmp=a->HGT_P1_L100_GLL0(0,:,{IPRS},0,0)

if (FH_INFLE .eq. "FH00-15")then
FT=tmp&forecast_time0
else
FT=tmp&forecast_time0
end if

dim=dimsizes(FT)

do i=0,dim(0)-1
if (FH .eq. FT(i))then
idx=i
print("FH="+FH+" i="+idx+" forecast_time="+FT(i))
print("")
end if
end do
LONW=125
LONE=140
LATS=28
LATN=43

GPH=a->HGT_P1_L100_GLL0(:,idx,{IPRS},{LATS:LATN},{LONW:LONE})
GPH=GPH/10.0

GPHav = dim_avg_n_Wrap(GPH,0)
printVarSummary(GPH)

GPHa=GPH
do n=0,20
GPHa(n,:,:)=GPH(n,:,:)-GPHav(:,:)
end do ;n


print("")
print("MMMMM INITIAL_TIME="+GPH@initial_time)
print("MMMMM FORECAST TIME="+FT(idx))
print("MMMMM P="+P)



wks = gsn_open_wks(TYP,FIG)             ; send graphics to PNG file
;gsn_define_colormap(wks,"CBR_coldhot")

plot = new(21,graphic)                         ; create graphic array
plot2 = new(21,graphic)                         ; create graphic array
ln = new(21,graphic)                         ; create graphic array

 res                      = True
 res@gsnPaperOrientation = "landscape"
 res@cnFillOn             = True         ; turn on color fill
 res@cnLinesOn	= False
res@cnFillPalette       = "cmp_flux"
 res@gsnDraw              = False        ; do not draw picture
 res@gsnFrame             = False        ; do not advance frame
 res@gsnSpreadColors      = False        ; no longer automatic
 res@gsnAddCyclic = False
res@mpMinLatF =  LATS     ; 緯度の最小値
res@mpMaxLatF =  LATN     ; 緯度の最大値
res@mpMinLonF = LONW     ; 経度の最小値
res@mpMaxLonF = LONE     ; 経度の最大値
res@mpGeophysicalLineColor       = "brown"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ

res@vpHeightF  = 0.3  

  res@cnLinesOn        = False             ; 等値線の太さ
  res@cnLevelSelectionMode    = "ManualLevels"  ; 等値線の設定をマニュアルにする
  res@cnMinLevelValF  = -2.0                ; 等値線の最小値
  res@cnMaxLevelValF  =  2.0              ; 等値線の最大値
  res@cnLevelSpacingF = 0.2                ; 等値線の間隔
res@lbLabelBarOn        = False           ; turn off individual cb's
res@cnLineLabelsOn = False
res@mpDataBaseVersion = "HighRes"

plx1=(/128. , 129.34, 129.34, 128.,   128./)  
ply1=(/30.15, 30.15,  31.15,  31.15,  30.15/)  
lnres = True  

do n=0,20
res@gsnStringFontHeightF = 0.015
res@gsnLeftString="Ens # "+n
res@gsnCenterString=""
res@gsnRightString=""

plot(n)=gsn_csm_contour_map(wks,GPHa(n,:,:),res)

 res2                      = True
res2@gsnDraw  = False
res2@gsnFrame = False
 res2@cnFillOn             = False         ; turn on color fill
 res2@cnLinesOn	= True
;  res2@cnLevelSelectionMode    = "ManualLevels"  ; 等値線の設定をマニュアルにする
;  res2@cnMinLevelValF  = 550.                ; 等値線の最小値
;  res2@cnMaxLevelValF  = 570.              ; 等値線の最大値
res2@cnLevelSpacingF = 2.                ; 等値線の間隔
res2@gsnLeftString=""
res2@gsnCenterString=""
res2@gsnRightString=""

res2@cnLineLabelsOn = True
res2@cnLineLabelInterval = 1
res2@cnLineLabelDensityF = 2.0

plot2(n)=gsn_csm_contour(wks,GPHav(:,:),res2)

overlay(plot(n),plot2(n))

end do

lnres@gsLineColor = "black"  
do n=0,20
ln(n) = gsn_add_polyline(wks, plot(n), plx1, ply1, lnres)  
end do

plot@ln = ln
return(plot)

; draw panel with white space added
 resP                 = True
 resP@gsnPanelYWhiteSpacePercent = 5
 resP@gsnPanelXWhiteSpacePercent = 5
TITLE="MEPS Z "+P+"  INIT:"+GPH@initial_time+" FH="+FH
  resP@gsnPanelMainString = TITLE

  resP@gsnPanelLabelBar    = True                ; add common colorbar
  resP@lbTitleOn        = True
  resP@lbTitleString = "dam" ; title string
  resP@lbTitlePosition = "Right" ; title position
  resP@lbTitleFontHeightF= .012 ;Font size
  resP@lbTitleDirection = "Across" ; title direction

  resP@lbLabelFontHeightF  = 0.01               ; make labels smaller

resP@mpGridLatSpacingF = 0.5
resP@mpGridLatSpacingF = 0.5

 gsn_panel(wks,plot,(/4,6/),resP)

print("MMMMM FIG: "+FIG+"."+TYP)

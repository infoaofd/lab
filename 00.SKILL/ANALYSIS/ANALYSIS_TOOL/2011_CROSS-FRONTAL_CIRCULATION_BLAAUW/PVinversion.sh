#!/bin/bash
#C:\Users\boofo\Dropbox\TOOLS_TEMP\2011_CROSS-FRONTAL_CIRCULATION_BLAAUW
MOD=PVinversion_modules.f90
SUB=PVinversion_numroutines.f90
SRC=PVinversion_main_v1.0.f90
EXE=PVinversion.EXE

OBJ="\
PVinversion_modules.o \
PVinversion_numroutines.o \
PVinversion_main_v1.0.o \
"

FC=ifort

$FC -c $MOD
if [ $? -ne 0 ];then echo COMPILE ERROR, $MOD;exit 1;fi
$FC -c $SUB
if [ $? -ne 0 ];then echo COMPILE ERROR, $SUB;exit 1;fi
$FC -c $SRC
if [ $? -ne 0 ];then echo COMPILE ERROR, $SRC;exit 1;fi

$FC $OBJ -o $EXE
if [ $? -ne 0 ];then echo LINK ERROR, $EXE;exit 1;fi

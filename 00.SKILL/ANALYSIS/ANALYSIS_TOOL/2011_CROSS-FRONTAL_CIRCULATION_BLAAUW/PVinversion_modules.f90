! PVinversion modules.f90
!C:\Users\boofo\Dropbox\TOOLS_TEMP\2011_CROSS-FRONTAL_CIRCULATION_BLAAUW
! -----------------------------------------------------------------------------------------------------------------------
! ----------------------------------------------------- Global Variables ------------------------------------------------
! -----------------------------------------------------------------------------------------------------------------------
MODULE Inversion_globalvar
IMPLICIT NONE
SAVE

! ----------------------------------------------------- User settings ----------------------------------------------------
! stratosphere ?, semi - geostrophic theory ?, print -to - file ?
LOGICAL :: stratos , SG , outputok =. TRUE.
! Configuration : Jet / front system , symmetric PV anomaly or imported PV field
CHARACTER (LEN =3) :: inimodel

!# gridpoints y and z, domain size y and z, timestep , endtime , print -to - file interval ,
INTEGER , PARAMETER :: ygrid =201 , zgrid =201 , domain_y =5d6 , domain_z =5e4 ,dt =300 , t1 =86400 , intervalit =3600
! Constants of nature
DOUBLE PRECISION , PARAMETER :: pi =3.14159 , g=9.81 , R =286.9 d0 , cp =1005. d0
! Coriolis parameter , surface temperature , static frequency reference atmosphere , surface pressure , height scale of
! troposphere - stratosphere transition
DOUBLE PRECISION , PARAMETER :: f=1.d-4, T0 =300. d0 , N=1.2d-2, p0 = 100000. d0 , alpha =5.d -4
! stretching deformation field ( A = -d(vg)/dy = constant )
DOUBLE PRECISION , PARAMETER :: Aconstant = 1.d -5
! Strength , position and structure of jet
DOUBLE PRECISION , PARAMETER :: Ujet0 =50.d0 , y0 =0.d0 , z0 =1.d4 , Yscale1 =5.d5 , Yscale2 =5.d5 , Zscale =6. d3
! Strength , position and structure of PV anomaly
DOUBLE PRECISION , PARAMETER :: PVano0 =2.d-4, PVy0 =0.d0 , PVz0 =1.d4 , PVYscale1 =5.d5 , PVYscale2 =5.d5 , PVZscale =5. d3
! ----------------------------------------------------------------------------------------------------------------------

! coordinates
DOUBLE PRECISION , DIMENSION ( ygrid ) :: y
DOUBLE PRECISION , DIMENSION ( zgrid ) :: z
! --- BASIC variables (print -to - file )
!-potential vorticity , geostrophic streamfunction , buoyancy , total potential temperature , geostrophic velocity ,
! frontogenetic forcing
!- ageostrophic streamfunction , ageostrophic velocity components , thermal wind balance , deformation field
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: PV , gphi , buoy , U, FF , phi , va , wa , Theta_bg ,TWB , A
! --- Additional variables ( not stored -to - file )
! Boundaries and PV components
DOUBLE PRECISION , DIMENSION (2, zgrid ) :: PVFF_lateral
DOUBLE PRECISION , DIMENSION ( ygrid ) :: PVFF_surface
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: PV_ts , PV_ano , gphi_ts , gphi_ano
! static , inertial and baroclinic frequencies and PVFF =PV*N^2 ( only for semigeostrophic case ) and vg=A*y
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: N2 , F2 , S2 , PVFF , vg

! --- Imported PV field parameters
! importing cross data sets
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: PVin , Ain , surfaceBCin , lateralBCin
! import dates
CHARACTER (13) , DIMENSION (16) :: darray
! import domain sizes y and z
INTEGER :: y_imp , z_imp

! --- Other
INTEGER :: i,j, k=0, it=0, countnegPV =0 !x,y, t_import ,t, # of negative PV
DOUBLE PRECISION :: dy , dz , beta , rhoJ , w !dy ,dz , beta =dy/dz , 2 optimal SOR parameters
DOUBLE PRECISION , DIMENSION (6) :: filedata_ini ! Write PV fields to file
DOUBLE PRECISION , DIMENSION (15) :: filedata_result ! Write from -PV - derived fields to file
END MODULE Inversion_globalvar



! -----------------------------------------------------------------------------------------------------------------------
! ------------------------------------------------- Supporting Functions ------------------------------------------------

! -----------------------------------------------------------------------------------------------------------------------
MODULE Inversion_support
USE Inversion_globalvar
USE nummethods
IMPLICIT NONE

CONTAINS
SUBROUTINE caseselect ( input )
IMPLICIT NONE

CHARACTER (LEN =*) , INTENT (IN) :: input

SELECT CASE ( input )
CASE ("jet")
inimodel = input
PRINT *, �fSimulation jet - front system chosen :�f
PRINT *, �f>> Symmetric jet structure is centered at (y,z) = (�f, y0 , �f,�f, z0 , �f) m�f
PRINT *, �f>> Horizontal length scale : yscale = �f, yscale1 , �f m�f
PRINT *, �f>> Vertical length scale : zscale = �f, zscale , �f m�f
PRINT *, �f>> Thermal front is in thermal wind balance with chosen jet structure �f
CASE ("ano")
inimodel = input
PRINT *, �fSimulation PV anomaly chosen �f
PRINT *, �f>> PV anomaly is centered at (y,z) = (�f, y0 , �f,�f, z0 , �f) m�f
PRINT *, �f>> Horizontal length scale : yscale = �f, yscale1 , �f m�f
PRINT *, �f>> Vertical length scale : zscale = �f, zscale , �f m�f
CASE ("imp")
inimodel = input
PRINT *, �fUNDER CONSTRUCTION !! Not user - friendly enough at the moment �f
PRINT *, �fPV field shall be imported from data files �f
PRINT *, �f>> Note that the following files are needed :�f
DO i=1, len( darray )
PRINT *, i, �fwref_param_date .txt �f
END DO
PRINT *, �f>> Where param is 60 for PV , 167 for surface temperature and 131 for jet stream on lateral boundaries �f

CASE ("QG")
SG =. FALSE .
CASE ("SG")
SG =. TRUE.

CASE ("y")
stratos =. TRUE.
CASE ("n")
stratos =. FALSE .
CASE DEFAULT
PRINT *, �fWrong input : please try again .�f
STOP
END SELECT
END SUBROUTINE caseselect

FUNCTION Data_in_date (no ,time)
IMPLICIT NONE

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: Data_in_date
CHARACTER (LEN =*) :: no , time

OPEN (25 , file=�f Wrefs /wref_ �f// no//�f_ �f// time // �f.txt �f)
DO j=1 ,81
IF (no == �f167 �f .AND. (j.NE .1)) THEN
Data_in_date (:,j) = 0. d0
ELSEIF (no == �f1311 �f) THEN
READ (25 , *) ( Data_in_date (i,j),i=1 ,2)
Data_in_date (3:101 ,j) = 0. d0
ELSE
READ (25 , *) ( Data_in_date (i,j),i=1 ,101)
ENDIF
END DO
CLOSE (25)
END FUNCTION Data_in_date

SUBROUTINE smoothing ( param )
IMPLICIT NONE

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: param
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: oldparam

oldparam = param
DO i=2, size(y) -1
DO j=2, size(z) -1
param (i,j) = ( oldparam (i-1,j+1)+ oldparam (i,j+1)+ oldparam (i+1,j+1)+ oldparam (i-1,j)+4. d0* oldparam (i,j)+ &
oldparam (i+1,j)+ oldparam (i-1,j -1)+ oldparam (i,j -1)+ oldparam (i+1,j -1))/12. d0
END DO
END DO
END SUBROUTINE smoothing

SUBROUTINE allowoutput ()
IMPLICIT NONE
IF (int(it/ intervalit ) == (it *1. d0/ intervalit )) THEN
outputok = .TRUE.
ELSE
outputok = . FALSE .
ENDIF
END SUBROUTINE allowoutput

! -------------------------------------------------------------------------
! ------------------------------ PV - inversion routines --------------------
! -------------------------------------------------------------------------

SUBROUTINE SORrelaxation_PVSG (PVlocal , PVFF_surfacelocal ,PVFF_lat ,func)
! This subroutine is called twice :
!- one time for total PV >> SORrelaxation_PVSG (PV , PVFF_surface +g/f, gphi )
!- one time for background PV >> SORrelaxation_PVSG (PV_bg ,g/f, gphi_bg )
IMPLICIT NONE
! extra iteration step for estimation phi including nonlinear terms
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (IN) :: PVlocal
DOUBLE PRECISION , DIMENSION ( ygrid ), INTENT (IN) :: PVFF_surfacelocal
DOUBLE PRECISION , DIMENSION (2, zgrid ), INTENT (IN) :: PVFF_lat

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (OUT) :: func

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: func_old
INTEGER :: ij , imax =5000
DOUBLE PRECISION :: err =1.d-4, anorm , anorm_old , r

! These relaxation parameters work best for the three configurations :
IF ( inimodel .NE.�fimp �f) THEN
r = 0.8 d0
ELSE
r = 0.2 d0
ENDIF

! initial streamfunction
func_old = 0. d0
DO i=1, size(y)
DO j=1, size(z)
PVFF(i,j) = PVlocal (i,j)*N**2
END DO
END DO
CALL SORrelaxation_BC (PVFF , PVFF_surfacelocal ,PVFF_lat ,func)

! SOR above is only QG >> test gphi field here before influence of nonlinear terms come into play !
! >> Solution for func is okay here ( what you expect from QG)
! CALL Testplotting (func - gphi_ts ) ! gphi_ano is what remains

! start iterating until nonlinear terms are also represented well enough
DO ij=1, imax
! correct func using r to avoid overestimation of func . The new func value is a factor r larger than func_old
IF (ij .NE. 1) THEN
func = func_old + r*( func - func_old )
ENDIF
! new forcing term including nonlinear terms
anorm = 0. d0
anorm_old = 0. d0
DO i=1, size(y)
DO j=1, size(z) -1
! Two important notes here
!1. second term is questionable >> use of PVFF_surface which is a parameter that does not change ( but is smallay ?)
!2. Lateral boundary conditions have been included for QG case . Also change this part ??????????????????????????
! >> probably not : lateral BC are arranged in SORrelaxation_BC with input of PVFF on ALL gridpoints !!
! >> PVFF comes from PV and need not to be changed ( except z =0) hereunder for guessing the nonlinear terms .
! >> nonlinear terms of PV only make a difference at center of domain !
IF (i==1) THEN
IF (j==1) THEN
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i+1,j)-func(i,j)-dy* PVFF_lat (1,j))/(.5 d0*dy **2))*(( func(i,j+1) -func(i,j)-dz* PVFF_surfacelocal (i))/(.5 d0**2)) + &
0. d0 !f *(( func (i+1,j +1) -func (i+1,j)-func (i,j+1) + func (i,j))/( dy*dz)) **2
ELSE
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i+1,j)-func(i,j)-dy* PVFF_lat (1,j))/(.5 d0*dy **2))*(( func(i,j+1) -2.d0*func(i,j)+func(i,j -1))/dz **2) + &
0. d0 !f *(( func (i+1,j +1) -func (i+1,j -1) -func (i,j+1) + func (i,j -1) ) /(2. d0*dy*dz)) **2
ENDIF
ELSEIF (i== size(y)) THEN
IF (j==1) THEN
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i-1,j)-func(i,j)+dy* PVFF_lat (2,j))/(.5 d0*dy **2))*(( func(i,j+1) -func(i,j)-dz* PVFF_surfacelocal (i))/(.5 d0**2)) + &
0. d0 !f*(( - func (i -1,j +1) + func (i -1,j)+ func (i,j +1) -func (i,j))/( dy*dz)) **2
ELSE
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i-1,j)-func(i,j)+dy* PVFF_lat (2,j))/(.5 d0*dy **2))*(( func(i,j+1) -2.d0*func(i,j)+func(i,j -1))/dz **2) + &
0. d0 !f*(( - func (i -1,j +1) + func (i -1,j -1) + func (i,j +1) -func (i,j -1)) /(2. d0*dy*dz))**2
ENDIF
ELSEIF (j==1 .AND. ((i.NE .1) .OR. (i.NE.size(y)))) THEN
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i+1,j) -2.d0*func(i,j)+func(i-1,j))/dy **2) *(( func(i,j+1) -func(i,j)-dz* PVFF_surfacelocal (i))/(.5 d0*dz **2))

f*(( PVFF_surfacelocal (i+1) -PVFF_surfacelocal (i -1))/(2. d0*dy))**2
!f *(( func (i+1,j +1) -func (i+1,j)-func (i -1,j+1) + func (i -1,j)) /(2. d0*dy*dz)) **2
ELSE
PVFF(i,j) = PVlocal (i,j)*N**2 - &
f*(( func(i+1,j) -2.d0*func(i,j)+func(i-1,j))/dy **2) *(( func(i,j+1) -2.d0*func(i,j)+func(i,j -1))/dz **2) + &
f*(( func(i+1,j+1) -func(i+1,j -1) -func(i-1,j+1)+func(i-1,j -1))/(4. d0*dy*dz))**2
ENDIF
anorm = anorm +abs(func(i,j)-func_old (i,j))
anorm_old = anorm_old + abs( func_old (i,j))
END DO
END DO
print *, ij , anorm / anorm_old , err
IF( anorm .LT. err* anorm_old ) RETURN

! remember some parameters
func_old = func
! new relaxation step
CALL SORrelaxation_BC (PVFF , PVFF_surfacelocal ,PVFF_lat ,func)
END DO
PAUSE �fimax exceeded in SORrelaxation_PVSG �f
END SUBROUTINE SORrelaxation_PVSG

SUBROUTINE SORrelaxation_BC (PVFF , PVFF_surface ,PVFF_lat ,func)
IMPLICIT NONE
! version where the estimated parameters are NONZERO on the surface boundary
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (IN) :: PVFF
DOUBLE PRECISION , DIMENSION ( ygrid ), INTENT (IN) :: PVFF_surface
DOUBLE PRECISION , DIMENSION (2, zgrid ), INTENT (IN) :: PVFF_lat
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (OUT) :: func

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: a,b,c,d,e,forc ,g

DO i=1, size(y)
DO j=1, size(z)
! Calculate coefficients of SG circulation equation
IF (i==1) THEN ! western lateral B.C.
IF (j==1) THEN ! lowerleftcorner of domain
c(i,j) = 2. d0*f**2* beta **2
d(i,j) = 0. d0
forc(i,j) = (dy **2)*PVFF(i,j) + 2. d0*f**2* beta **2* dz* PVFF_surface (i) + 2. d0*N**2* dy* PVFF_lat (1,j)
! ELSEIF (j== size (z)) THEN ! upperleftcorner of domain
! c(i,j) = 0. d0
! d(i,j) = 2. d0*f **2* beta **2
! forc (i,j) = (dy **2) * PVFF (i,j)
ELSE
c(i,j) = f**2* beta **2
d(i,j) = f**2* beta **2
forc(i,j) = (dy **2)*PVFF(i,j) + 2. d0*N**2* dy* PVFF_lat (1,j)
ENDIF
a(i,j) = 2. d0*N**2
b(i,j) = 0. d0
ELSEIF (i== size(y)) THEN ! eastern lateral B.C.
IF (j==1) THEN ! lowerrightcorner of domain
c(i,j) = 2. d0*f**2* beta **2
d(i,j) = 0. d0
forc(i,j) = (dy **2)*PVFF(i,j) + 2. d0*f**2* beta **2* dz* PVFF_surface (i) - 2. d0*N**2* dy* PVFF_lat (2,j)
! ELSEIF (j== size (z)) THEN ! upperrightcorner of domain
! c(i,j) = 0. d0
! d(i,j) = 2. d0*f **2* beta **2
! forc (i,j) = (dy **2) * PVFF (i,j)
ELSE
c(i,j) = f**2* beta **2
d(i,j) = f**2* beta **2
forc(i,j) = (dy **2)*PVFF(i,j) - 2. d0*N**2* dy* PVFF_lat (2,j)
ENDIF
a(i,j) = 0. d0
b(i,j) = 2. d0*N**2
ELSEIF (j==1 .AND. ((i.NE .1) .OR. (i.NE.size(y)))) THEN !on surface boundary ( except lateral borders )
a(i,j) = N**2
b(i,j) = N**2
c(i,j) = 2. d0*f**2* beta **2
d(i,j) = 0. d0
forc(i,j) = (dy **2)*PVFF(i,j) + 2. d0*f**2* beta **2* dz* PVFF_surface (i)
! ELSEIF (j== size (z) . AND . ((i.NE .1) .OR. (i.NE. size (y)))) THEN !on upper boundary ( except lateral borders )
! a(i,j) = N **2
! b(i,j) = N **2
! c(i,j) = 0. d0
! d(i,j) = 2. d0*f **2* beta **2
! forc (i,j) = (dy **2) * PVFF (i,j)
ELSE ! inner domain
a(i,j) = N**2
b(i,j) = N**2
c(i,j) = f**2* beta **2
d(i,j) = f**2* beta **2
forc(i,j) = (dy **2)*PVFF(i,j)
ENDIF
e(i,j) = -2.d0 *(N**2+f**2* beta **2)
g(i,j) = 0. d0
END DO
END DO

! use version 5 of SOR relaxation method including boundary problem
CALL SOR_5 (a,b,c,d,e,forc ,g,func ,w,ygrid , zgrid )
END SUBROUTINE SORrelaxation_BC

SUBROUTINE PV_circulation ( buoylocal ,Ulocal ,FFlocal , gphilocal )
IMPLICIT NONE

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (IN) :: gphilocal
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (OUT) :: buoylocal , Ulocal , FFlocal

! Calculate some fields and write resulting values to file
! geostrophic velocity
DO i=1, size(y)
IF (i==1) THEN
Ulocal (i ,:) = -( gphilocal (i+1 ,:) -gphilocal (i ,:))/dy
ELSEIF (i== size(y)) THEN
Ulocal (i ,:) = -( gphilocal (i ,:) -gphilocal (i -1 ,:))/dy
ELSE
Ulocal (i ,:) = -( gphilocal (i+1 ,:) -gphilocal (i -1 ,:))/(2. d0*dy)
ENDIF
END DO
! buoyancy
DO j=1, size(z)
IF (j==1) THEN
buoylocal (:,j) = f*( gphilocal (:,j+1) -gphilocal (:,j))/dz
ELSEIF (j== size(z)) THEN
buoylocal (:,j) = f*( gphilocal (:,j)-gphilocal (:,j -1))/dz
ELSE
buoylocal (:,j) = f*( gphilocal (:,j+1) -gphilocal (:,j -1))/(2. d0*dz)
ENDIF
END DO
! lateral conditions (to avoid lateral boundary problems !)
! buoylocal (1 ,:) = buoylocal (2 ,:)
! buoylocal ( size (y) ,:) = buoylocal ( size (y) -1 ,:)

! next : calculate new frontogenetic forcing
!Also , check thermal wind balance relation between Ug and buoy :
DO i=2, size(y) -1
DO j=1, size(z) -1
FFlocal (i,j) = Calc_FF2 ( buoylocal (i-1,j), buoylocal (i+1,j), A(i,j), dy)
IF (j.NE .1) TWB(i,j) = (-f*(U(i,j+1) -U(i,j -1))/(2. d0*dz))/(( buoy(i+1,j)-buoy(i-1,j))/(2. d0*dy))
! For case - study input
IF ( (z(j) >20000) .AND. (abs( FFlocal (i,j)) >0.5d -11) .AND. inimodel ==�fimp �f) THEN
FFlocal (i,j) = 0.5d -11
ELSEIF ( (z(j) <=1000) .AND. (abs( FFlocal (i,j)) >0.5d -11) .AND. inimodel ==�fimp �f) THEN
FFlocal (i,j) = 0. d0
ENDIF
END DO
END DO
END SUBROUTINE PV_circulation

SUBROUTINE new_frequencies ()
IMPLICIT NONE
! adjust the frequency parameters to the new U and buoy fields
! useful for input Sawyer - Eliassen equation
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: instabilitycheck

! Static frequency
DO j=1, size(z)
!N2 comes from isothemal ref . atmosphere + both perturbations ( buoy )
IF (j==1) THEN
N2(:,j) = N**2 + (buoy (:,j+1) -buoy (:,j))/dz
ELSEIF (j== size(z)) THEN
N2(:,j) = N**2 + (buoy (:,j)-buoy (:,j -1))/dz
ELSE
N2(:,j) = N**2 + (buoy (:,j+1) -buoy (:,j -1))/(2. d0*dz)
ENDIF
END DO

! Baroclinic and inertial frequency
DO i=1, size(y)
DO j=1, size(z)
IF (i==1 .OR. i== size(y)) THEN
S2(i,j) = 0. d0 !db /dy =0
F2(i,j) = f**2 ! dug /dy =0
ELSE
S2(i,j) = (buoy(i+1,j)-buoy(i-1,j))/(2. d0*dy)
F2(i,j) = f*(f - (U(i+1,j)-U(i-1,j))/(2. d0*dy))
ENDIF
! check if SGPV = N ^2* F^2-S^4 is greater than 0.
! Or else , Sawyer - Eliassen equation can not be solved
instabilitycheck = Calc_PV (N2(i,j),F2(i,j),S2(i,j)**2 ,y(i),z(j))
END DO
END DO
END SUBROUTINE new_frequencies

SUBROUTINE SORrelaxation (N2 ,F2 ,S2 ,FF ,phi)
IMPLICIT NONE
! simpler version where the estimated parameters are ZERO on the surface boundary
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (IN) :: N2 ,F2 ,S2 ,FF
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (OUT) :: phi

DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: a,b,c,d,e,forc ,g

DO i=1, size(y)
DO j=1, size(z)
! Calculate coefficients of SG circulation equation
a(i,j) = N2(i,j)
b(i,j) = N2(i,j)
c(i,j) = F2(i,j)*beta **2
d(i,j) = F2(i,j)*beta **2
e(i,j) = -2.d0 *( N2(i,j)+F2(i,j)*beta **2)
forc(i,j) = (dy **2)*FF(i,j)
g(i,j) = -.5d0*S2(i,j)*beta
END DO
END DO
CALL SOR_4 (a,b,c,d,e,forc ,g,phi ,w,ygrid , zgrid )
END SUBROUTINE SORrelaxation

SUBROUTINE ageo_circulation ()
IMPLICIT NONE
! Calculate ageostrophic wind components
!wa
DO i=2, size(y) -1
wa(i ,:) = -(phi(i+1 ,:) -phi(i -1 ,:))/(2. d0*dy)
END DO
!va
DO j=1, size(z) -1
IF (j==1) THEN
va(:,j) = (phi (:,j+1) -phi (:,j))/dz
ELSE
va(:,j) = (phi (:,j+1) -phi (:,j -1))/(2. d0*dz)
ENDIF
END DO
END SUBROUTINE ageo_circulation

SUBROUTINE results_to_file ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (6) :: filedata_ini
DOUBLE PRECISION , DIMENSION (15) :: filedata_result

DO i=1, size(y)
DO j=1, size(z) -1
! initial PV field
filedata_ini = (/ it *1.d0 , y(i)*1.d-3, z(j)*1.d-3, 1. d4*PV(i,j), 0.d0 , 0. d0 /)
CALL writetofile_small (14 , 1, filedata_ini )
! resulting field
filedata_result = (/ it *1.d0 , y(i)*1.d-3, z(j)*1.d-3, gphi(i,j), buoy(i,j), U(i,j), 1. d11*FF(i,j), &
phi(i,j), va(i,j), 1. d2*wa(i,j), Theta_bg (i,j)+buoy(i,j)*T0/g, TWB(i,j), 1. d4*N2(i,j), 1. d4*F2(i,j), 1. d4*S2(i,j)

CALL writetofile_large (15 , filedata_result )
END DO
! additional white space
WRITE (14 , �f( e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f)
WRITE (15 , �f( e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f)
END DO
! additional white space
WRITE (14 , �f( e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f)
WRITE (15 , �f(e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f)

! close data files
IF (it == t1) CLOSE (14)
IF (it == t1) CLOSE (15)
END SUBROUTINE results_to_file

SUBROUTINE writetofile_small (filename , interval , dataset )
! generating small datasets (6 columns ) used for vectorplots . Interval determines the
! separation of these vectors .
IMPLICIT NONE
INTEGER , INTENT (IN) :: filename , interval
DOUBLE PRECISION , DIMENSION (:) :: dataset
! Reduce the dataset such that only the vector arrows on certain gridpoints ( seperated by �finterval �f)
! are plotted !
IF ( (y(i)/ interval == int(y(i)/ interval )) .AND. (z(j)/ interval == int(z(j)/ interval )) ) THEN
WRITE (filename ,�f( e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f) dataset
ELSE
dataset (5:6) = 0 ! set length of vector arrows to zero (????)
WRITE (filename ,�f( e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f) dataset
END IF
END SUBROUTINE writetofile_small

SUBROUTINE writetofile_large (filename , dataset )
IMPLICIT NONE
INTEGER , INTENT (IN) :: filename
DOUBLE PRECISION , DIMENSION (:) :: dataset
WRITE (filename ,&
�f(e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8, e17 .8) �f) dataset
END SUBROUTINE writetofile_large

! --------------------- - - - FUNCTIONS - - - --------------------------
! INITIAL FUNCTIONS
FUNCTION Ujet ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: Ujet
! use circular jet !!
DO i=1, size(y)
DO j=1, size(z)
IF ((y(i)-y0) >=0) THEN
Ujet(i,j) = Ujet0 *exp ( -((y(i)-y0)/ Yscale1 )**2)*exp ( -((z(j)-z0)/ Zscale )**2)
ELSE
Ujet(i,j) = Ujet0 *exp ( -((y(i)-y0)/ Yscale2 )**2)*exp ( -((z(j)-z0)/ Zscale )**2)
ENDIF
END DO
END DO
END FUNCTION Ujet

FUNCTION ini_buoy ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: ini_buoy

! Solving TWB relation
DO i=1, size(y)
DO j=1, size(z)
IF ((y(i)-y0) >=0) THEN
ini_buoy (i,j) = &
(f*2. d0* Ujet0 *(z(j)-z0))/( Zscale **2)*exp ( -((z(j)-z0)/ Zscale )**2)* Yscale1 *.5 d0*sqrt(pi)*erf ((y(i)-y0)/ Yscale1 )
ELSE
ini_buoy (i,j) = &
(f*2. d0* Ujet0 *(z(j)-z0))/( Zscale **2)*exp ( -((z(j)-z0)/ Zscale )**2)* Yscale2 *.5 d0*sqrt(pi)*erf ((y(i)-y0)/ Yscale2 )
ENDIF
END DO
END DO
END FUNCTION ini_buoy

! TIME EVOLUTION FUNCTIONS

FUNCTION Calc_Mg (U, f, y)
IMPLICIT NONE
DOUBLE PRECISION :: Calc_Mg , U, f, y
! absolute momentum calculation used for calculating a new F2
Calc_Mg = U - f*y
END FUNCTION Calc_Mg

FUNCTION Calc_Q (U1 , U2 , A, f, T0 , g, dz)
IMPLICIT NONE
DOUBLE PRECISION :: Calc_Q , U1 , U2 , A, f, T0 , g, dz
! Frontogenetic forcing at new timestep
Calc_Q = -A*f*T0 *(U2 -U1)/(2. d0*g*dz)
END FUNCTION Calc_Q

FUNCTION Calc_FF (U1 , U2 , A, f, dz)
IMPLICIT NONE
DOUBLE PRECISION :: Calc_FF , U1 , U2 , A, f, dz
! Total forcing at new timestep
Calc_FF = A*f*(U2 -U1)/dz
! note : factor 2 in denumerator drops here
END FUNCTION Calc_FF

FUNCTION Calc_FF2 (b1 , b2 , A, dy)
IMPLICIT NONE
DOUBLE PRECISION :: Calc_FF2 , b1 , b2 , A, dy
! Total forcing at new timestep
Calc_FF2 = -2.d0*A*(b2 -b1)/(2. d0*dy)
! note : factor 2 in denumerator drops here
END FUNCTION Calc_FF2

FUNCTION Calc_PV (N_2 ,F_2 ,S_4 , y, z)
IMPLICIT NONE
DOUBLE PRECISION :: Calc_PV , N_2 , F_2 , S_4 , y, z
!q = N^2 F^2 - S^4
Calc_PV = N_2*F_2 - S_4
IF(Calc_PV <0) THEN
print *, �fcoord :�f, y, z, �fPV = �f, Calc_PV , �fCANCEL PROGRAM !!�f
print *, �f--------------------------------------------------�f
print *, �fN_eff ^2: �f, N_2
print *, �fF^2: �f, F_2
print *, �fS^4: �f, S_4
print *, �f--------------------------------------------------�f
print *, �fdb/dz: �f, N_2 -N**2
print *, �fdug/dy: �f, -(F_2 - f**2)/f
print *, �fdb/dy: �f, sqrt(S_4)
print *, �f--------------------------------------------------�f
ENDIF
END FUNCTION Calc_PV

! version 8: PV functions ---------------------------

! initial QGPV based on exponential structure of jet ( circular )
! using different tropopause formulation ( local1 )
FUNCTION ini_PVQG ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: ini_PVQG

DOUBLE PRECISION :: local1 , local2 , local3

! from analytic expression
DO i=1, size(y)
DO j=1, size(z)
IF ((y(i)-y0) >=0) THEN
local2 = -2.d0* Ujet0 *exp ( -((y(i)-y0)/ Yscale1 )**2)*exp ( -((z(j)-z0)/ Zscale )**2) *(y(i)-y0)/( Yscale1 **2) ! dug /dy
local3 = f* Ujet0 * Yscale1 *sqrt(pi)*erf ((y(i)-y0)/ Yscale1 )*exp ( -((z(j)-z0)/ Zscale )**2) *(1.d0 - &
2. d0 *((z(j)-z0)/ Zscale )**2) /( Zscale **2) !db/dz
ELSE
local2 = -2.d0* Ujet0 *exp ( -((y(i)-y0)/ Yscale2 )**2)*exp ( -((z(j)-z0)/ Zscale )**2) *(y(i)-y0)/( Yscale2 **2) ! dug /dy
local3 = f* Ujet0 * Yscale2 *sqrt(pi)*erf ((y(i)-y0)/ Yscale2 )*exp ( -((z(j)-z0)/ Zscale )**2) *(1.d0 - &
2. d0 *((z(j)-z0)/ Zscale )**2) /( Zscale **2) !db/dz
ENDIF
ini_PVQG (i,j) = (f/(N**2))* local3 - local2
END DO
END DO

END FUNCTION ini_PVQG

! initial SGPV based on exponential structure of jet
! using different tropopause formulation ( local1 )
FUNCTION ini_PVSG ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: ini_PVSG

DOUBLE PRECISION :: local2 , local3 , local4

! from analytic expression
DO i=1, size(y)
DO j=1, size(z)
IF ((y(i)-y0) >=0) THEN
local2 = -2.d0* Ujet0 *exp ( -((y(i)-y0)/ Yscale1 )**2)*exp ( -((z(j)-z0)/ Zscale )**2) *(y(i)-y0)/( Yscale1 **2) ! dug /dy
local3 = f* Ujet0 * Yscale1 *sqrt(pi)*erf ((y(i)-y0)/ Yscale1 )*exp ( -((z(j)-z0)/ Zscale )**2) *(1.d0 - &
2. d0 *((z(j)-z0)/ Zscale )**2) /( Zscale **2) !db/dz
local4 = 2. d0*f* Ujet0 *exp ( -((y(i)-y0)/ Yscale1 )**2) *(z(j)-z0)/( Zscale **2)*exp ( -((z(j)-z0)/ Zscale )**2) !db/dy
ELSE
local2 = -2.d0* Ujet0 *exp ( -((y(i)-y0)/ Yscale2 )**2)*exp ( -((z(j)-z0)/ Zscale )**2) *(y(i)-y0)/( Yscale2 **2) ! dug /dy
local3 = f* Ujet0 * Yscale2 *sqrt(pi)*erf ((y(i)-y0)/ Yscale2 )*exp ( -((z(j)-z0)/ Zscale )**2) *(1.d0 - &
2. d0 *((z(j)-z0)/ Zscale )**2) /( Zscale **2) !db/dz
local4 = 2. d0*f* Ujet0 *exp ( -((y(i)-y0)/ Yscale2 )**2) *(z(j)-z0)/( Zscale **2)*exp ( -((z(j)-z0)/ Zscale )**2) !db/dy
ENDIF
ini_PVSG (i,j) = (f/(N**2))* local3 - local2 - (1. d0 /(N**2))* local3 * local2 - (1. d0 /(f*N**2))* local4 **2
END DO
END DO
END FUNCTION ini_PVSG

FUNCTION ini_PV3 ()
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: ini_PV3

! from analytic expression
DO i=1, size(y)
DO j=1, size(z)
IF ((y(i)-y0) >=0) THEN
ini_PV3 (i,j) = PVano0 *exp ( -((y(i)-PVy0)/ PVYscale1 )**2)*exp ( -((z(j)-PVz0)/ PVZscale )**2)
ELSE
ini_PV3 (i,j) = PVano0 *exp ( -((y(i)-PVy0)/ PVYscale2 )**2)*exp ( -((z(j)-PVz0)/ PVZscale )**2)
ENDIF
END DO
END DO
END FUNCTION ini_PV3

SUBROUTINE smallest ( param )
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ), INTENT (IN) :: param
DOUBLE PRECISION :: minim

! minimal values
minim = param (1 ,1)
DO i=1, size(y)
DO j=1, size(z)
IF ( param (i,j) < minim ) minim = param (i,j)
END DO
END DO
print *, �fMinimal value SGPV: �f, minim
IF (minim <=0. d0) THEN
print *, �fPV has negative values somewhere . SGPV - inversion routine not expected to work.�f
ENDIF
END SUBROUTINE smallest

END MODULE Inversion_support



! -----------------------------------------------------------------------------------------------------------------------

! -------------------------------------------------- General subroutines -------------------------------------------

! -----------------------------------------------------------------------------------------------------------------------





MODULE Inversion_general
USE Inversion_support
IMPLICIT NONE

CONTAINS

SUBROUTINE Input_user ()
IMPLICIT NONE

CHARACTER (LEN =3) :: input1
CHARACTER (LEN =2) :: input2
CHARACTER (LEN =1) :: input3


PRINT *, �fWelcome user !�f
PRINT *, �fCurrent version : v1.0�f
PRINT *, �fThis program is used for simulating dynamical processes from a PV perspective �f
PRINT *, �fFor example : frontogenesis or tropopause fold evolution �f
PRINT *, �f------------------------------------------------------------�f

PRINT *, �fFirst , choose your initial problem to simulate :�f
PRINT *, �f1. Jet - front system (type "jet")�f
PRINT *, �f2. Symmetric PV anomaly (type "ano")�f
PRINT *, �f3. Import PV field (type "imp")�f
READ *, input1
PRINT *, �f�f
CALL caseselect ( input1 )
PRINT *, �f------------------------------------------------------------�f

PRINT *, �fAll other fields may be derived numerically using Piecewise PV - Inversion when the following 3 conditions are
�f
PRINT *, �f(I) A reference state has to be chosen for an atmosphere at rest �f
PRINT *, �f>> This program uses a linear stratified reference atmosphere where the static stability N^2 is constant .�f
PRINT *, �f>> N = �f, N, �f /s�f
PRINT *, �f�f
PRINT *, �f(II) A specific balance condition has to be chosen for the PV perturbations upon reference atmosphere �f
PRINT *, �f>> Please choose between : �f
PRINT *, �f1. Quasi - Geostrophic balance (typ "QG")�f
PRINT *, �f2. Semi - Geostrophic balance (typ "SG")�f
READ *, input2
PRINT *, �f�f
CALL caseselect ( input2 )
IF ( input1 .NE.�fimp �f) THEN
PRINT *, �f>> Would you like to include a tropopause ? (typ "y" or "n")�f
READ *, input3
PRINT *, �f�f
CALL caseselect ( input3 )
ENDIF
PRINT *, �f(III) The PV inversion problem has to be globally solved with proper boundary conditions �f
PRINT *, �f>> The PV inversion algorithm includes the surface and lateral boundary conditions �f
PRINT *, �f�f

PRINT *, �f------------------------------------------------------------�f
PRINT *, �f>> Starting simulation <<�f
PRINT *, �f------------------------------------------------------------�f
END SUBROUTINE Input_user

SUBROUTINE Initialisation ()
IMPLICIT NONE

! Open data file for storing initial PV perturbation values
OPEN (14 , file=�f Tropofold_QGPVinitial_ani .dat �f, ACTION =�fWRITE �f)
WRITE (14 , �f(a17 ,a17 ,a17 ,a17 ,a17 ,a17) �f) &
"  --- t (s) -- ", " -- y (km) -- ", " -- z (km) -- ", " PV (1 E12s ^ -4)", "  ------------ ", "  ------------ "

! ---open data file for storing resulting fields derived from PV perturbations
OPEN (15 , file=�f Tropofold_QGPVresult_ani .dat �f, ACTION =�fWRITE �f)
WRITE (15 , �f(a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17 ,a17) �f) "  --- t (s) -- ", " -- y (km) -- ", &
" -- z (km) -- ", " gPhi (m^2/s) ", " buoy (m/s^2) ", " - U  (m/s) - ", " - FF (/s^3) - ", " - Phi (/s) - ", &
" - va (m/s) - ", " - wa (m/s) - ", "  -Theta  (K)- ", "  --- TWB  ---  ", "  ------------ ", "  ------------ ", " 
-------- "

! -------------------- basic variables part I-------------------------
PV = 0. d0
PV_ts = 0. d0
PV_ano = 0. d0
gphi = 0. d0
gphi_ts = 0. d0
gphi_ano = 0. d0
buoy = 0. d0
U = 0. d0
FF = 0. d0
phi = 0. d0
va = 0. d0
wa = 0. d0
TWB = 1. d0
A = Aconstant

PVFF = 0. d0
N2 = N**2
S2 = 0. d0
F2 = f**2

! Calculate the optimal spectral radius for largest convergence in SOR
!>applying approximation based on ygrid / zgrid ratio
rhoJ = 1. d0 - (pi **2) /(2. d0* zgrid **2) + (pi **4) /(24. d0* zgrid **4)
! And the resulting over - relaxation parameter for faster convergence
w = 2. d0 /(1. d0 + sqrt (1.d0 -rhoJ **2))

! -------------------------------------------------------
! ---- initialisation depends on chosen configuration !----
! -------------------------------------------------------

SELECT CASE ( inimodel )
! ---------jet / front system case ------------
CASE ("jet")
dy = domain_y /( ygrid -1. d0)
dz = domain_z /( zgrid -1. d0)
y = ((/ (i,i=1, size(y)) /) -.5d0 *( ygrid +1. d0))*dy
z = ((/ (j,j=1, size(z)) /) -1.d0)*dz
beta = dy/dz

! stratification depending on tropopause or not
DO j=1, size(z)
! continuous tropopause included
IF ( stratos ) THEN
PV_ts (:,j) = N **2*(1.5 d0 *(1. d0+erf( alpha *(z(j)-z0)))) ! stratosphere PVanomaly
ELSE
PV_ts (:,j) = N**2
ENDIF
END DO

! initial values of perturbation fields
buoy = ini_buoy ()
U = Ujet ()
CALL new_frequencies () ! check for instability
! initial boundary conditions
PVFF_surface = buoy (: ,1)/f
PVFF_lateral (1 ,:) = -U(1 ,:)
PVFF_lateral (2 ,:) = -U(size(y) ,:)
IF (SG) THEN
PV_ano = ini_PVSG ()
ELSE
PV_ano = ini_PVQG ()
ENDIF

PV = PV_ano + PV_ts
! -------------PV anomaly case ----------------
CASE ("ano")
dy = domain_y /( ygrid -1. d0)
dz = domain_z /( zgrid -1. d0)
y = ((/ (i,i=1, size(y)) /) -.5d0 *( ygrid +1. d0))*dy
z = ((/ (j,j=1, size(z)) /) -1.d0)*dz
beta = dy/dz

! stratification depending on tropopause or not
DO j=1, size(z)
! continuous tropopause included
IF ( stratos ) THEN
PV_ts (:,j) = N **2*(1.5 d0 *(1. d0+erf( alpha *(z(j)-z0)))) ! stratosphere PVanomaly
ELSE
PV_ts (:,j) = N**2
ENDIF
END DO

! initial values of perturbation fields
PV_ano = ini_PV3 ()
! initial boundary conditions
PVFF_surface = 0. d0
PVFF_lateral (1 ,:) = 0. d0
PVFF_lateral (2 ,:) = 0. d0

PV = PV_ano + PV_ts
! --------------PV import case --------------------
CASE ("imp")
! read time_array
darray = (/ �f20011108 -0000 �f , �f20011108 -0600 �f , �f20011108 -1200 �f , �f20011108 -1800 �f ,&
�f20011109 -0000 �f , �f20011109 -0600 �f , �f20011109 -1200 �f , �f20011109 -1800 �f ,&
�f20011110 -0000 �f , �f20011110 -0600 �f , �f20011110 -1200 �f , �f20011110 -1800 �f ,&
�f20011111 -0000 �f , �f20011111 -0600 �f , �f20011111 -1200 �f , �f20011111 -1800 �f /)

! y_imp specifically chosen for 2001 nov case - study >> change later on
y_imp = sqrt ((2. d0*pi *6.371 d6 *(2.315 d1 /3.6 d2)) **2+(2. d0*pi*cos ((4.5 d1 /3.6 d2)*2. d0*pi) *6.371 d6 *(2.508 d1 /3.6 d2))**2)
z_imp = 4. e4
dy = y_imp /( ygrid -1. d0)
dz = z_imp /( zgrid -1. d0)
y = ((/ (i,i=1, size(y)) /) -.5d0 *( ygrid +1. d0))*dy
! yplot = ((/ (i,i=1, size (y)) /) -1. d0) *10. d0 ! alternative axis : put this in gnuplot script
z = ((/ (j,j=1, size(z)) /) -1.d0)*dz
beta = dy/dz

PVin = Data_in_date (�f601�f, darray (6))
! Ain = Data_in_date ( �f1341 �f , darray (1) )
surfaceBCin = Data_in_date (�f167�f, darray (6))
lateralBCin = Data_in_date ( �f1311 �f , darray (6))
DO i=1, size(y)
DO j=1, size(z)
!A(i,j) = dble ( Ain (i,j))
PV(i,j) = dble(PVin(i,j))
PVFF_surface (i) = dble( surfaceBCin (i ,1)/f)
PVFF_lateral (1,j) = dble( lateralBCin (1,j))
PVFF_lateral (2,j) = dble( lateralBCin (2,j))
! Some modifications on data
IF (PV(i,j) <0) print *, �fnegative value for PV(i,j). Coordinates �f, y(i), z(j), �fPV = �f, PV(i,j)
IF (PV(i,j) <0) PV(i,j) = 0. d0
IF (j==0) PVFF_lateral (:,j) = 0. d0 !on surface boundary : ug = 0 at lateral boundaries
END DO
END DO

! get rid of PV anomalies above 20 km
DO j=41 , size(z)
PV(:,j) = sum(PV(:,j))/size(y)
PVFF_lateral (:,j) = 0. d0
END DO

! Smoothing of noisy import fields by horizontal averaging
! CALL smoothing (A)

! calculating fields at timestep it
print *, �ftimestep = �f, darray (1) , �f. end time = �f, darray (it)
END SELECT

! -------------------- basic variables part II -------------------------
! information on PV , y and z needed here
DO i=1, size(y)
DO j=1, size(z) ! potential temperature profile for reference atmosphere only
Theta_bg (i,j) = T0 *( exp ((g*z(j))/(R*T0)))**(R/cp)
vg(i,j) = -A(i,j)*y(i)
IF (. NOT. SG) PVFF(i,j) = PV(i,j)*N**2
IF ((. NOT. SG) .AND. ( inimodel ==�fimp �f)) PVFF(i,j) = (PV(i,j)-f)*N**2
END DO
END DO

! minimal values
CALL smallest (PV+f)

PRINT *, �fInitialisation complete �f
END SUBROUTINE Initialisation



SUBROUTINE Initialisation_dt ()
IMPLICIT NONE

CALL allowoutput () ! check if results may be printed to file for this timestep

! -------------------------------------------------------
! ----PV adjustment depends on chosen configuration !----
! -------------------------------------------------------

SELECT CASE ( inimodel )
! ----------------------------------------------------jet / front system case --------------------------------
CASE ("jet")
print *, �ftimestep = �f, it , �ft1 = �f, t1

PVFF_lateral (1 ,:) = -U(1 ,:)
PVFF_lateral (2 ,:) = -U(size(y) ,:)
! ----------------------------------------------------PV anomaly case ----------------------------------------
CASE ("ano")
print *, �ftimestep = �f, it , �ft1 = �f, t1

PVFF_lateral (1 ,:) = -U(1 ,:)
PVFF_lateral (2 ,:) = -U(size(y) ,:)
! ----------------------------------------------------PV import case -----------------------------------------
CASE ("imp")
IF ( outputok ) k = (it)/(6* dt)

! print info to screen
IF (. NOT. outputok ) print *, �f------- time: �f, darray (k+1) , �f +�f, (it -k *21600) /(3600) , �fhours -------�f
IF ( outputok ) print *, �ftimestep = �f, darray (k+1) , �f. t1 = �f, darray (16)

! import new deformation field and boundary conditions every 6 hours ( when outputok = true )
IF ( outputok ) THEN
! Ain = Data_in_date ( �f1341 �f , darray (k))
surfaceBCin = Data_in_date (�f167�f, darray (k+1))
lateralBCin = Data_in_date ( �f1311 �f , darray (k+1))
DO i=1, size(y)
DO j=1, size(z)
!A = dble ( Ain )
PVFF_surface (i) = dble( surfaceBCin (i ,1)/f)
PVFF_lateral (1,j) = dble( lateralBCin (1,j))
PVFF_lateral (2,j) = dble( lateralBCin (2,j))
IF (PV(i,j) <0) print *, �fnegative value for PV(i,j): �f,PV(i,j)
IF (PV(i,j) <0) PV(i,j) = 0. d0
IF (j==0) PVFF_lateral (:,j) = 0. d0 !on surface boundary : ug = 0 at lateral boundaries
END DO
END DO

! apply smoothing on noisy fields
! CALL smoothing (A)

! Some modifications on data
DO j=41 , size(z) ! get rid of PV anomalies above 20 km
PV(:,j) = sum(PV(:,j))/size(y)
PVFF_lateral (:,j) = 0. d0
END DO
ENDIF
END SELECT

! -------------------- Some modifications -------------------------
DO i=1, size(y)
DO j=1, size(z)
! Some modifications on data
IF (( PV(i,j)+f) <0 .AND. SG) THEN ! nonlinear inversion equation only solvable for SGPV >=0
print *, �fcoord :�f, y(i), z(j), �fSGPV = �f, f+PV(i,j)
!PV(i,j) = 0. d0 ! solution for negative SGPV problem
ENDIF
vg(i,j) = -A(i,j)*y(i)
IF (. NOT. SG) PVFF(i,j) = PV(i,j)*N**2
IF ((. NOT. SG) .AND. ( inimodel ==�fimp �f)) PVFF(i,j) = (PV(i,j)-f)*N**2 ! take absolute vorticity instead of relative
END DO
END DO

! minimal values
CALL smallest (PV+f)
END SUBROUTINE Initialisation_dt
END MODULE Inversion_general

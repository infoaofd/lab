! PVinversion numroutines.f90
!!C:\Users\boofo\Dropbox\TOOLS_TEMP\2011_CROSS-FRONTAL_CIRCULATION_BLAAUW
MODULE nummethods
USE Inversion_globalvar
IMPLICIT NONE

CONTAINS
! ----------------------------------------------------------------------------------------------------------------
! ----------------------------------- Successive Over - Relaxation ---------------------------------------------------
! ----------------------------------------------------------------------------------------------------------------


! --- simplest form useful when parameter ( phi) is 0 on all boundaries . Iteration takes place on whole domain EXCEPT boundaries
SUBROUTINE SOR_4 (a,b,c,d,e,forc , g, phi , omega , gs1 , gs2)
IMPLICIT NONE
! Routine for SOR method in simplest form , based on given example in
!�fnumerical recipes �f H19 .
!No Chebyshev acceleration and odd - even ordening applied here .
! >> version 4: simple form that can be used if phi =0 on all boundaries
! input variables
INTEGER :: maxits , gs1 , gs2 ! size of grid space (ygrid , zgrid )
! relaxation parameter omega ( constant ), coefficients of equation a,b,c,d,e,f
! initial guess phi and error threshold value
DOUBLE PRECISION :: a(gs1 ,gs2),b(gs1 ,gs2),c(gs1 ,gs2),d(gs1 ,gs2) &
,e(gs1 ,gs2),forc(gs1 ,gs2), g(gs1 ,gs2), phi(gs1 ,gs2), err
! maximum iterations and threshold value
PARAMETER ( maxits =10000 , err =1.d -3)
! Remaining variables only used in subroutine
INTEGER :: i, ipass ,j,l,n
DOUBLE PRECISION :: anorm , anormf , omega ,resid , anormf_test

! compute initial norm of residual ( with threshold value err)
anormf = 0. d0
DO j=2,gs1 -1
DO l=2,gs2 -1
! assume here that initial value (of phi ) is zero and
! calculate norm of total forcing over whole grid space
anormf = anormf + abs(forc(j,l))
END DO
END DO

! Now iterate until the error between the total field that corresponds to forcing f
! and the numerical solution is less than ERR .
DO n=1, maxits
anorm =0. d0
DO j=2,gs1 -1
DO l=2, gs2 -1
resid = a(j,l)*phi(j+1,l)+b(j,l)*phi(j-1,l)+c(j,l)*phi(j,l+1)+d(j,l)*phi(j,l -1) &
+ e(j,l)*phi(j,l)-forc(j,l)+g(j,l)*( phi(j+1,l+1) -phi(j+1,l -1)+phi(j-1,l -1) -phi(j-1,l+1))
anorm = anorm +abs( resid )
! calculate new solution based on norm of residual
phi(j,l) = phi(j,l)-omega * resid /e(j,l)
END DO
END DO
! print *, n, anorm / anormf , err
IF( anorm .LT. err* anormf ) RETURN
END DO

PAUSE �fmaxits exceeded in SOR �f
END SUBROUTINE SOR_4

! --- Routine INCLUDING 3 or 4 boundary conditions ( surface and lateral boundaries and optional : upper boundary ).
! --- Iteration takes place on whole domain INCLUDING boundaries
SUBROUTINE SOR_5 (a,b,c,d,e,forc , g, phi , omega , gs1 , gs2)
IMPLICIT NONE
! Routine for SOR method in simplest form , based on given example in
!�fnumerical recipes �f H19 .
!No Chebyshev acceleration and odd - even ordening applied here .
! >> version 5: including lateral and surface and upper boundary conditions

! input variables
INTEGER :: maxits , gs1 , gs2 ! size of grid space (ygrid , zgrid )
! relaxation parameter omega ( constant ), coefficients of equation a,b,c,d,e,f
! initial guess phi and error threshold value
DOUBLE PRECISION :: a(gs1 ,gs2),b(gs1 ,gs2),c(gs1 ,gs2),d(gs1 ,gs2) &
,e(gs1 ,gs2),forc(gs1 ,gs2), g(gs1 ,gs2), phi(gs1 ,gs2), err
! maximum iterations and threshold value
PARAMETER ( maxits =100000 , err =1.d -3)
! Remaining variables only used in subroutine
INTEGER :: i, ipass ,j,l,n
DOUBLE PRECISION :: anorm , anormf , omega , resid

! compute initial norm of residual ( with threshold value err)
anormf = 0. d0
DO j=1, gs1
DO l=1,gs2 -1
! assume here that initial value (of phi ) is zero and
! calculate norm of total forcing over whole grid space
anormf = anormf + abs(forc(j,l))
END DO
END DO

! Now iterate until the error between the total field that corresponds to forcing f
! and the numerical solution is less than ERR .
DO n=1, maxits
anorm =0. d0
DO j=1, gs1
DO l=1,gs2 -1
IF (j==1) THEN !on western lateral boundary
IF (l==1) THEN ! lowerleftcorner of domain
resid = a(j,l)*phi(j+1,l)+c(j,l)*phi(j,l+1)+e(j,l)*phi(j,l)-forc(j,l)
! ELSEIF (l== gs2 ) THEN ! upperleftcorner of domain
! resid = a(j,l)* phi (j+1,l)+d(j,l)* phi (j,l -1) +e(j,l)*phi (j,l)-forc (j,l)
ELSE
resid = a(j,l)*phi(j+1,l)+c(j,l)*phi(j,l+1)+d(j,l)*phi(j,l -1)+e(j,l)*phi(j,l)-forc(j,l)
ENDIF
ELSEIF (j== gs1) THEN !on eastern lateral boundary
IF (l==1) THEN ! lowerrightcorner of domain
resid = b(j,l)*phi(j-1,l)+c(j,l)*phi(j,l+1)+e(j,l)*phi(j,l)-forc(j,l)
! ELSEIF (l== gs2 ) THEN ! upperrightcorner of domain
! resid = b(j,l)* phi (j -1,l)+d(j,l)* phi(j,l -1) +e(j,l)*phi (j,l)-forc (j,l)
ELSE
resid = b(j,l)*phi(j-1,l)+c(j,l)*phi(j,l+1)+d(j,l)*phi(j,l -1)+e(j,l)*phi(j,l)-forc(j,l)
ENDIF
ELSEIF (l==1 .AND. ((j.NE .1) .OR. (j.NE.gs1))) THEN !on surface boundary ( except lateral corners )
resid = a(j,l)*phi(j+1,l)+b(j,l)*phi(j-1,l)+c(j,l)*phi(j,l+1)+e(j,l)*phi(j,l)-forc(j,l)
! ELSEIF (l== gs2 . AND . ((j.NE .1) .OR. (j.NE.gs1 ))) THEN !on upper boundary ( except lateral corners )
! resid = a(j,l)* phi (j+1,l)+b(j,l)* phi (j -1,l)+d(j,l)*phi (j,l -1)+e(j,l)* phi (j,l)-forc (j,l)
ELSE ! inner domain
resid = a(j,l)*phi(j+1,l)+b(j,l)*phi(j-1,l)+c(j,l)*phi(j,l+1)+d(j,l)*phi(j,l -1)+e(j,l)*phi(j,l)-forc(j,l)
ENDIF
anorm = anorm +abs( resid )
! calculate new solution based on norm of residual
phi(j,l) = phi(j,l)-omega * resid /e(j,l)
END DO
END DO
! print *, n, anorm / anormf , err
IF( anorm .LT. err* anormf ) RETURN
END DO
PAUSE �fmaxits exceeded in SOR �f
END SUBROUTINE SOR_5

! ----------------------------------------------------------------------------------------------------------------
! -----------------------------------Runge - Kutta 4th order --------------------------------------------------------
! ----------------------------------------------------------------------------------------------------------------

! Central domain
SUBROUTINE rk4_PVfield ()
IMPLICIT NONE
! Given a parabolic (time - dependent ) p.d.e. solve it through Runge - Kutta
! for func from t0 till t1. The tendency of func equals rhs (i.e. the
! righthand side of function )

! The fourth order method consists of four phases
! After each phase the rhs of the p.d.e. needs to be updated .

! temporary
DOUBLE PRECISION , DIMENSION (ygrid , zgrid ) :: k1 , k2 , k3 , k4 , rhs , PV_old

PV_old = PV - PV_ts
! rhs at t0
rhs = Calc_PVrhs ( PV_old )

! first phase
k1 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVrhs ( PV_old +.5 d0*k1)

! second phase
k2 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVrhs ( PV_old +.5 d0*k2)

! third phase
k3 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVrhs ( PV_old +k3)

! fourth phase
k4 = dt*rhs

! calculate new function at t=t0+ tau =t1 using intermediate time evaluations
! given by the four k�fs
PV = PV_ts + PV_old + (1. d0 /6. d0)*( k1 +2. d0*k2 +2. d0*k3+k4)
END SUBROUTINE rk4_PVfield

! Surface boundary
SUBROUTINE rk4_PVsurface ()
IMPLICIT NONE
! Given a parabolic (time - dependent ) p.d.e. solve it through Runge - Kutta
! for func from t0 till t1. The tendency of func equals rhs (i.e. the
! righthand side of function )

! The fourth order method consists of four phases
! After each phase the rhs of the p.d.e. needs to be updated .

! temporary
DOUBLE PRECISION , DIMENSION ( ygrid ) :: k1 , k2 , k3 , k4 , rhs , PVFF_surface_old

PVFF_surface_old = PVFF_surface
! rhs at t0
rhs = Calc_PVFFsurfacerhs ( PVFF_surface_old )

! first phase
k1 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVFFsurfacerhs ( PVFF_surface_old +.5 d0*k1)

! second phase
k2 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVFFsurfacerhs ( PVFF_surface_old +.5 d0*k2)

! third phase
k3 = dt*rhs
! calculate new rhs for next phase
rhs = Calc_PVFFsurfacerhs ( PVFF_surface_old +k3)

! fourth phase
k4 = dt*rhs

! calculate new function at t=t0+ tau =t1 using intermediate time evaluations
! given by the four k�fs
PVFF_surface = PVFF_surface_old + (1. d0 /6. d0)*( k1 +2. d0*k2 +2. d0*k3+k4)
END SUBROUTINE rk4_PVsurface

! Function calculating right -hand - side of PV conservation equation
FUNCTION Calc_PVrhs (dPV)
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (: ,:) , INTENT (IN) :: dPV
DOUBLE PRECISION , DIMENSION (size(y),size(z)) :: Calc_PVrhs

DOUBLE PRECISION :: SGterm , QGterm

! q does not change on y- boundaries and at z =50 km. It does ! at z=0
!on surface z=0
DO i=2, size(y) -1
QGterm = -vg(i,j)*( dPV(i+1 ,1) -dPV(i -1 ,1))/(2. d0*dy)
SGterm = -va(i ,1) *( dPV(i+1 ,1) -dPV(i -1 ,1))/(2. d0*dy) - wa(i ,1) *( dPV(i ,2) -dPV(i ,1))/dz
IF (SG) THEN
Calc_PVrhs (i ,1) = QGterm + SGterm
ELSE
Calc_PVrhs (i ,1) = QGterm
ENDIF
END DO

DO i=2, size(y) -1
DO j=2, size(z) -1
QGterm = -vg(i,j)*( dPV(i+1,j)-dPV(i-1,j))/(2. d0*dy)
SGterm = -va(i,j)*( dPV(i+1,j)-dPV(i-1,j))/(2. d0*dy) - wa(i,j)*( dPV(i,j+1) -dPV(i,j -1))/(2. d0*dz)
IF (SG) THEN
Calc_PVrhs (i,j) = QGterm + SGterm
ELSE
Calc_PVrhs (i,j) = QGterm
ENDIF
END DO
END DO
END FUNCTION Calc_PVrhs

FUNCTION Calc_PVFFsurfacerhs ( PV_surface )
IMPLICIT NONE
DOUBLE PRECISION , DIMENSION (:) , INTENT (IN) :: PV_surface
DOUBLE PRECISION , DIMENSION (size(y)) :: Calc_PVFFsurfacerhs

DOUBLE PRECISION :: SGterm , QGterm

DO i=2, size(y) -1
QGterm = -vg(i ,1)
SGterm = -va(i ,1)
IF (SG) THEN
Calc_PVFFsurfacerhs (i) = ( QGterm + SGterm )*( PV_surface (i+1) -PV_surface (i -1))/(2. d0*dy)
ELSE
Calc_PVFFsurfacerhs (i) = QGterm *( PV_surface (i+1) -PV_surface (i -1))/(2. d0*dy)
ENDIF
END DO
END FUNCTION Calc_PVFFsurfacerhs
END MODULE nummethods

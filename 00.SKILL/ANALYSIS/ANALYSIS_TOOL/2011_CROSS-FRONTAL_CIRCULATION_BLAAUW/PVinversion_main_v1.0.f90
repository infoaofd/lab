! PVinversion main v1.0.f90
! This program is used for studying atmospheric dynamic processes using the concept of �fPV thinking �f
!C:\Users\boofo\Dropbox\TOOLS_TEMP\2011_CROSS-FRONTAL_CIRCULATION_BLAAUW
PROGRAM PV_inversion
USE Inversion_general
! Modules included in main_PV :
!- Inversion_globalvar
!- Inversion_support ( supporting subroutines and functions )
!- Nummethods ( SOR and rk4 methods )
 IMPLICIT NONE

! ---------------------------------------------------------------------------------------------
! ............................... Preparation .............................
! ---------------------------------------------------------------------------------------------

! Interaction with user who chooses the following :
!1. Choose reference atmosphere (a. Isothermal ( not included ), b. linear stratified )
!2. PV perturbations :
! Initial configuration : (a. Jet / front system , b. symmetric PV anomaly , c. PV imported )
! Balanced theory : (a. Quasi - geostrophic , b. semi - geostrophic )
! Include stratosphere (y/n)?
!3. Deformation field (a. Stretching deformation , other fields not included )
 CALL Input_user ()

! -----------------------------------------------------------------------------
! ------------------------------- Algorithm -----------------------------------

! -------------------------- Order of calculations : --------------------------
! Vg ,va ,wa --> PV --> gphi --> Ug , buoy , FF --> N2 ,F2 ,S2 --> phi --> va ,wa -->
! ----------------------------------------------------------------------------

DO it=0,t1 ,dt
IF (it ==0) THEN
! initialisation of variables depending on chosen model configuration
CALL Initialisation ()
ELSE
! Calculate new PV field at next timestep from PV conservation
! Using Runge - Kutta 4th order
CALL rk4_PVfield ()
! also on surface boundary
CALL rk4_PVsurface ()
! preparations for new time step (print -to - file settings , boundary conditions )
CALL Initialisation_dt ()
ENDIF

!PV - inversion (QG & SG) using Successive Over - Relaxation ( SOR)
SELECT CASE (SG)
CASE (. TRUE .) !Semi - geostrophic case
! Including surface and lateral boundary conditions
! Nonlinear inversion
IF ( inimodel .NE.�fimp �f) THEN
! >>> Perform PV - inversion for each anomaly separately and sum up gphi
! stratosphere anomaly
CALL SORrelaxation_PVSG (PV_ts , PVFF_surface *0.d0 , PVFF_lateral *0.d0 , gphi_ts )
! dynamic PV_anomaly
CALL SORrelaxation_PVSG (PV -PV_ts , PVFF_surface , PVFF_lateral , gphi_ano )
gphi = gphi_ano + gphi_ts
ELSE
! Imported SG anomalies cannot be separated : perform PV - inversion for whole field
CALL SORrelaxation_PVSG (PV -f, PVFF_surface , PVFF_lateral ,gphi)
ENDIF
CASE (. FALSE .) !Quasi - geostrophic case
! Linear inversion
! Including surface and lateral boundary conditions
CALL SORrelaxation_BC (PVFF , PVFF_surface , PVFF_lateral ,gphi)
END SELECT

! Calculate new geostrophic velocity , buoyancy and frontogenetic forcing fields from gphi
CALL PV_circulation (buoy ,U,FF ,gphi)

! New frequencies N^2, F^2, S^4 for SG case , for QG case they remain constant

IF (SG) CALL new_frequencies ()

! Use SOR again to determine ageostrophic streamfunction
! Linear operator for both QG and SG
! Simple zero - valued boundary conditions
CALL SORrelaxation (N2 ,F2 ,S2 ,FF ,phi)

! finally , determine ageostrophic velocity field
CALL ageo_circulation ()

! write resulting fields to file
IF ( outputok ) CALL results_to_file ()
END DO

! --------------End of program -------------------
END PROGRAM PV_inversion


! Made by: Marten Blaauw
! For comments / questions : mcblaauw@gmail .com
! Date of last update : 14 Oktober 2011

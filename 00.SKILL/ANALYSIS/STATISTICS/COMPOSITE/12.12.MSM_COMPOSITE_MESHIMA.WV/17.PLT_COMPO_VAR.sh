#!/bin/bash

LEV=$1
LEV=${LEV:-300}

INDIR=/work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.12.COMPOSITE_Z
RAIN=R1DAAV
PREFIX1=QFLUX.ge.50PCNT_${RAIN}.ge.50PCNT
PREFIX2=QFLUX.ge.50PCNT_${RAIN}.le.50PCNT
DTYPE=P
INFLE1=14.CDO_COMPO_VAR_${PREFIX1}_${DTYPE}.nc
INFLE2=14.CDO_COMPO_VAR_${PREFIX2}_${DTYPE}.nc

IN1=${INDIR}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi
IN2=${INDIR}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi


FIG=$(basename $0 .sh)_${RAIN}_${LEV}.PDF

TEXT1="Z ${LEV}hPa"
TEXT2="${RAIN} > 50% - ${RAIN} < 50%"
TEXT3="QFLUX > 50%"

GS=$(basename $0 .sh).GS


HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN1}'; 'sdfopen ${IN2}';say
'q ctlinfo 1';say 'MMMMM 'sublin(result,1);say
'q ctlinfo 2';say 'MMMMM 'sublin(result,1);say

lev=${LEV}

xmax = 1; ymax = 1
ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1
nmap=1; ymap=1; xmap=1
xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off'
'set xlint 10';'set ylint 5'
'set mpdset hires'

'd z.1'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1; 'draw string ' xx ' ' yy ' ${INFLE1}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INFLE2}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF


grads -bcp "$GS"
rm -vf $GS

echo; if [ -f $FIG ]; then echo "MMMMM FIG: $FIG"; fi



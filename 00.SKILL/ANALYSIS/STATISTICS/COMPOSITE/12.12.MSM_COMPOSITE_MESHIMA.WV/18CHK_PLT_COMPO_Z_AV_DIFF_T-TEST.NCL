INDIR="/work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.12.COMPOSITE_Z/"
RAIN="R1DAAV"
PREFIX1="QFLUX.ge.50PCNT_"+RAIN+".ge.50PCNT"
PREFIX2="QFLUX.ge.50PCNT_"+RAIN+".le.50PCNT"
DTYPE="P"
INFLE1A="12.CDO_COMPO_AVE_"+PREFIX1+"_"+DTYPE+".nc"
INFLE2A="12.CDO_COMPO_AVE_"+PREFIX2+"_"+DTYPE+".nc"
INFLE1V="14.CDO_COMPO_VAR_"+PREFIX1+"_"+DTYPE+".nc"
INFLE2V="14.CDO_COMPO_VAR_"+PREFIX2+"_"+DTYPE+".nc"
IN1A=INDIR+INFLE1A
IN2A=INDIR+INFLE2A
IN1V=INDIR+INFLE1A
IN2V=INDIR+INFLE2V

N1=295
N2=204

TYP="PDF"
FIG="18.CHK_prob"
FIGDIR="."

VAR="z"
LEV=500
f1a=addfile(IN1A,"r")
f2a=addfile(IN2A,"r")
f1v=addfile(IN1V,"r")
f2v=addfile(IN2V,"r")

a1=f1a->$VAR$(0,{LEV},:,:)
a2=f2a->$VAR$(0,{LEV},:,:)
v1=f1v->$VAR$(0,{LEV},:,:)
v2=f2v->$VAR$(0,{LEV},:,:)
lon=f1a->lon
lat=f1a->lat

sigr = 0.05; 
iflag=False; False indicates v1 /= v2

diff=a1-a2
copy_VarMeta(a1,diff)

prob = ttest(a1,v1,N1, a2, v2, N2, iflag, False) 
copy_VarMeta(a1,prob)
printVarSummary(prob)
wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=""
opt@gsnCenterString=""
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.07
opt@gsnLeftStringOrthogonalPosF=0.07
opt@gsnRightStringOrthogonalPosF=0.07
opt@gsnLeftStringFontHeightF=0.02
opt@gsnCenterStringFontHeightF=0.02
opt@gsnRightStringFontHeightF=0.02

opt@cnLevelSelectionMode = "AutomaticLevels"
;opt@cnMinLevelValF = -80.
;opt@cnMaxLevelValF =  80.
;opt@cnLevelSpacingF = 10.
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)

opt@pmLabelBarHeightF = 0.3
opt@pmLabelBarWidthF=0.03
opt@lbTitleString    = ""                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbOrientation = "vertical"
opt@lbLabelFontHeightF   = 0.015

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "HighRes" ;-- better map resolution

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpFillOn               = False
res@mpDataBaseVersion = "HighRes" ;-- better map resolution
res@mpMinLonF = 120     ; 経度の最小値
res@mpMaxLonF = 150    ; 経度の最大値
res@mpMinLatF =  21     ; 緯度の最小値
res@mpMaxLatF =  48     ; 緯度の最大値

res@vpWidthF      = 0.5  ; plot size (width)
;res@vpXF          = 0.121578   ; location of plot
;res@vpYF          = 0.60       ; location of plot

plot=gsn_csm_contour_map(wks,prob,res)

draw(plot)
frame(wks)         ; WorkStationの更新

print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)

print("MMMMM CRITICAL SIG LVL FOR R " +tostring (sigr))
print("MMMMM NUMBER OF SAMPLES: N1="+tostring(N1)+", N2="+tostring(N2))
print("MMMMM CHECK N1 & N2. THEY ARE HARD CODED IN NCL SCRIPT")

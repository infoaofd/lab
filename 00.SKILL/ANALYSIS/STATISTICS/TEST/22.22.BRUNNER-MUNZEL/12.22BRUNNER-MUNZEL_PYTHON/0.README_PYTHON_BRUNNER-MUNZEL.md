# PythonのScipyでBrunner-Munzel検定

## 概要

ノン[パラメトリック](http://d.hatena.ne.jp/keyword/%A5%D1%A5%E9%A5%E1%A5%C8%A5%EA%A5%C3%A5%AF)の検定では、Wilcoxon-Mann-WhitneyのU検定が非常に有名ですが、この検定は不当分散の場合には第一種の過誤を得る確率が5%にならない場合があり、適切な手法として使えません。 

Brunner-Munzel検定は、不当分散のときに使えるノン[パラメトリック](http://d.hatena.ne.jp/keyword/%A5%D1%A5%E9%A5%E1%A5%C8%A5%EA%A5%C3%A5%AF)検定です。ランクから、得られたデータの中央値に有意差があるか検出します。 

```python
from scipy import stats
```

```PYTHON
stats.brunnermunzel(x, y, alternative='two-sided', distribution='t', nan_policy='propagate')
```

`distribution`　p値の計算に t分布を使う（デフォルト）とき `'t'`，標準正規分布を使うとき `'normal'` を指定する。



```bash
$ python3
Python 3.9.13 (main, Oct 19 2022, 17:23:07) 
[GCC 11.2.0] :: Intel Corporation on linux
```

```python
>>> from scipy import stats
>>> x1 = [1,2,1,1,1,1,1,1,1,1,2,4,1,1]
>>> x2 = [3,3,4,3,1,2,3,1,1,5,4]
>>> w, p_value = stats.brunnermunzel(x1, x2)
>>> p_value
0.005786208666151538
```

https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.brunnermunzel.html




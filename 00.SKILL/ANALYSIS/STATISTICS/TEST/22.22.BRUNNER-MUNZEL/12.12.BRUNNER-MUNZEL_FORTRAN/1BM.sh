#!/bin/bash
#
# Tue, 06 Feb 2024 21:25:31 +0900
# /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/22.22.BRUNNER-MUNZEL
# BM.sh

EXE=$(basename $0 .sh).EXE
MAIN=$(basename $0 .sh).F90
OBJMAIN=$(basename $0 .sh).o

SUBLIST=" \
bm_permutation_stat.f  bm_test.f          combination.f    rank.f \
bm_permutation_test.f  calc_statistics.f  divide_groups.f"

FC="ifort"
DOPT="-traceback -CB -fpe0"

for SUB in $SUBLIST; do
echo $FC -c $DOPT $SUB
$FC -c $DOPT $SUB
done

OBJLIST=${SUBLIST//.f/.o}
for OBJ in $OBJLIST;do
if [ ! -f $OBJ ];then echo NO SUCH FILE, $OBJ; exit 1;fi
done 

$FC -c $MAIN
if [ ! -f $OBJMAIN ];then echo NO SUCH FILE, $OBJMAIN; exit 1;fi

$FC $OBJLIST $OBJMAIN -o $EXE
if [ ! -f $EXE ];then echo NO SUCH FILE, $EXE; exit 1;fi

${EXE}

echo "DONE $(basename $0)."
echo
#echo "INPUT : $IN"
#echo "OUTPUT: $OUT"
echo
exit 0



# THE FOLLOWING LINES ARE SAMPLES:
<<COMMENT


# INPUT FILE
IN=""
if [ ! -f $in ];then echo NO SUCH FILE, $IN; exit 1;fi
fi

# WHILE LOOP
IS=1; IE=3
I=$IS
while [ $I -le $IE ]; do

  I=$(expr $I + 1)
done

# FOR LOOP 
ALIST="a b c"
for A in $ALIST; do

done

# IF BLOCK
i=3
if [ $i -le 5 ]; then

else if  [ $i -le 2 ]; then

else

fi

# CREATING DIRECTORY
if [ ! -d $DIR ]; then mkdir -vp ${DIR}; fi

# CHECHING COMMAND LINE ARGUMENTS
if [ $# -lt 1 ]; then
  echo Error in $0 : No arugment
  echo Usage: $0 arg
  exit 1
fi

# CHECHING COMMAND LINE ARGUMENTS
if [ $# -lt 2 ]; then
  echo Error in $0 : Wrong number of arugments
  echo Usage: $0 arg1 arg2
  exit 1
fi

# HANDLING COMMAND LINE OPTIONS
CMDNAME=$(basename $0)
while getopts t: OPT; do
  case $OPT in
    "t" ) flagt="true" ; value_t="$OPTARG" ;;
     * ) echo "Usage $CMDNAME [-t VALUE] [file name]" 1>&2
  esac
done

value_t=${value_t:-"NOT_AVAILABLE"}

if [ $value_t = "foo" ]; then
  type="foo"
elif [ $value_t = "boo" ]; then
  type="boo"
else
  type=""
fi
shift $(expr $OPTIND - 1)

COMMENT

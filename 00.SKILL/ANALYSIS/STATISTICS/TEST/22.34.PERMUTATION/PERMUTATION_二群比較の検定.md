## Rによる二群比較の検定



## データ設定

```
(ins) > x<-c(289,269,296)
(ins) > y<-c(263,241,261)
(ins) > x
[1] 289 269 296
(ins) > y
[1] 263 241 261
```

## パッケージをインストール（まだインストールしていない場合）

```
#パッケージをインストール（まだインストールしていない場合）
> install.packages("exactRankTests")
> library("exactRankTests")
```

## 検定

```
# 以下、二群比較の検定
# 1. Permutation Test（並べ替え検定）
(ins) > perm.test(x, y, alternative = "greater", paired = FALSE, exact = TRUE)$p.value
[1] 0.05

# 2. Exact Wilcoxon rank sum test（マンホイットニーのU検定）
(ins) > wilcox.exact(x, y, alternative = "greater", paired = FALSE, exact = TRUE)$p.value
[1] 0.05

(ins) > t.test(x, y, alternative = "greater", var.equal = TRUE, paired = FALSE)$p.value
[1] 0.02518852

# 3. Welch's t-test
(ins) > t.test(x, y, alternative = "greater", var.equal = FALSE, paired = FALSE)$p.value
[1] 0.02574316


```

| alternative | character型。"two.sided"で両側検定、"greater"で右片側検定、"less"で左片側検定を指定できる。 |
| ----------- | ------------------------------------------------------------ |
|             |                                                              |

```
# lawstatパッケージをインストール（まだインストールしていない場合）
install.packages("lawstat")

# lawstatパッケージをロード
library(lawstat)
```

```
# 4. Brunner-Munzel test
brunnermunzel.test(x, y, alternative = "greater", perm = FALSE)$p.value

# 5. Permuted Brunner-Munzel Test（並べ替えBrunner-Munzel検定）
brunnermunzel.permutation.test(x, y, alternative = "greater")$p.value

```


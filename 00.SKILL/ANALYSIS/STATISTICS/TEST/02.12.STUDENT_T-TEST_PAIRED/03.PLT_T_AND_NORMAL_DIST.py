# /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/02.12.T-TEST_PAIRED

import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt

x = np.linspace(-6, 6, 100)

fig, ax = plt.subplots(1, 1)

ax.plot(x, st.t.pdf(x, 30), label='t-dist $n=30$')
ax.plot(x, st.norm.pdf(x, loc=0, scale=1), label='normal dist')
    
plt.xlim(-6, 6)
plt.ylim(0, 0.4)

plt.legend()
plt.show()

FIG="03.PLT_T_AND_NORMAL_DIST.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)

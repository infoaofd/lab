# Mann-Kendall using R

/work09/am/00.WORK/2023.KOUSA/12.12.TREND

統計解析ソフトＲを用いて, データの確率密度が正規分布でなく場合にトレンドの推定を行うMann-Kendall検定を行う。

Rについての概要は下記ウェブサイトなど参照

https://tomoecon.github.io/R_for_graduate_thesis/

Mann-Kendall検定の概要については下記ウェブサイトなど参照

https://qiita.com/MathGeek/items/0077e6c12140c3d6e9e8

### 入力ファイル確認

```
$ ls SPRING_SUM.csv
```

```
$ head SPRING_SUM.csv
year,x,y
1914,1,0
1915,2,5
1916,3,4
1917,4,3
1918,5,4
1919,6,5
1920,7,0
1921,8,3
1922,9,4
```



### R起動

統計解析用のソフトRを起動する

```bash
$ R
```

### パッケージのインストール

Rのトレンド推定用のパッケージ(trend)をインストールする。1回だけ行えばよい。時間かかる

```
install.packages("trend", dependencies = TRUE)
```

https://stats.biopapyrus.jp/r/basic/package.html



### trendパッケージのロード

```
(ins) > library(trend)
```



### 入力ファイル読み込み

```
(ins) > a=read.csv("SPRING_SUM.csv")
```

https://zenn.dev/eitsupi/articles/r-read-csv-2022#read.csv

```
(ins) > a$year
  [1] 1914 1915 1916 1917 1918 1919 1920 1921 1922 1923 1924 1925 1926 1927 1928
 [16] 1929 1930 1931 1932 1933 1934 1935 1936 1937 1938 1939 1940 1941 1942 1943
 [31] 1944 1945 1946 1947 1948 1949 1950 1951 1952 1953 1954 1955 1956 1957 1958
 [46] 1959 1960 1961 1962 1963 1964 1965 1966 1967 1968 1969 1970 1971 1972 1973
 [61] 1974 1975 1976 1977 1978 1979 1980 1981 1982 1983 1984 1985 1986 1987 1988
 [76] 1989 1990 1991 1992 1993 1994 1995 1996 1997 1998 1999 2000 2001 2002 2003
 [91] 2004 2005 2006 2007 2008 2009 2010 2011 2012 2013 2014 2015 2016 2017 2018
[106] 2019
(ins) > a$x
  [1]   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18
 [19]  19  20  21  22  23  24  25  26  27  28  29  30  31  32  33  34  35  36
 [37]  37  38  39  40  41  42  43  44  45  46  47  48  49  50  51  52  53  54
 [55]  55  56  57  58  59  60  61  62  63  64  65  66  67  68  69  70  71  72
 [73]  73  74  75  76  77  78  79  80  81  82  83  84  85  86  87  88  89  90
 [91]  91  92  93  94  95  96  97  98  99 100 101 102 103 104 105 106
(ins) > a$y
  [1]  0  5  4  3  4  5  0  3  4  0  2  5  2  5 13  5  3 12  6  0  8  5  8  4  5
 [26]  1  5  6 10  2  8  1  2  3  1  1  2  2 11 11  5 14  2  3  7  5  4  1  3  1
 [51]  3  4  9  3  7  9  2  1  2  3  4  6 13 11  7  5  5  9  3  3  6  2  1  4  7
 [76]  4  7  4  1  4  9  3  7  3  8  5 16 13 18  1 11  9  6  9  5  1  6  7  0  5
[101]  6  2  3  3  5  1
```



## Mann-kendallトレンド検定

```bash
(ins) > mk.test(a$y)

        Mann-Kendall trend test

data:  a$y
z = 1.4913, n = 106, p-value = 0.1359
alternative hypothesis: true S is not equal to 0
sample estimates:
           S         varS          tau 
5.440000e+02 1.325793e+05 1.022937e-01 
```

- p-value = 0.1359なので, **事前に**設定する危険率を14%以上としていれば有意な長期変化傾向があるといえる
- 統計量 S によって、Ｓ>0 のとき増加トレンド、Ｓ<0 のとき減少トレンドがあると判断できる。ここではS=5.44なので増加トレンドがあるといえる

- Kendall の順位相関係数, tauの値は=0.102



## 長期変化傾向の大きさの計算

```bash
(ins) > sens.slope(a$y,conf.level=0.9)

        Sen's slope

data:  a$count
z = 1.4913, n = 106, p-value = 0.1359
alternative hypothesis: true z is not equal to 0
90 percent confidence interval:
 0.00000000 0.03225806
sample estimates:
Sen's slope 
 0.01176471 
```

**傾き (長期変化傾向) = 0.012 [回数/年]** (100年で1回増す程度)

危険率10％での信頼限界は 0.00 < 長期変化傾向 < 0.032 (回数/年)



## 線形回帰

結果のチェックのため通常の線形回帰 (Ordinary Least Squares)を用いた場合の長期変化傾向を計算する。

```
(ins) > ans<-lm(y~x,data=a)
(ins) > ans

Call:
lm(formula = y ~ x, data = a)

Coefficients:
(Intercept)            x  
     4.0598       0.0181  

```

**傾き (長期変化傾向)=0.0181 [回/年]**, 切片=4.0598

```
(ins) > result<-lm(x~y,data=a)
(ins) > summary(result)

Call:
lm(formula = x ~ y, data = a)

Residuals:
    Min      1Q  Median      3Q     Max 
-51.465 -26.654  -0.335  24.336  57.543 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)    
(Intercept)  47.2046     5.0231   9.397 1.49e-15 ***
y             1.2520     0.8062   1.553    0.123    
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 30.54 on 104 degrees of freedom
Multiple R-squared:  0.02266,   Adjusted R-squared:  0.01327 
F-statistic: 2.412 on 1 and 104 DF,  p-value: 0.1235
```

p-value=0.1235　(危険率を13%以上としていれば有意)

相関係数の2乗=0.013なので相関係数~0.1


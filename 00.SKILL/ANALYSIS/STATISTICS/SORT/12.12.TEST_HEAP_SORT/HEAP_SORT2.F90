program main
  implicit none
  integer::i,N
  integer,allocatable::turn1(:),turn2(:)
  real,allocatable::a(:)
  real::t

  N=10
  allocate(a(N)); a=0.
  allocate(turn1(N),turn2(N)); turn1=0; turn2=0

  print '(A)','turn1(i),turn2(i),a(i)'
  do i=1,N
     call random_number(t)
     a(i)=t
     turn1(i)=i
     turn2(i)=10+i
     print '(i5,1x,i5,f7.3)',turn1(i),turn2(i),a(i)
  enddo
  write(6,*)

  call heapsort2(size(a),a,turn1,turn2)

  print '(A)','turn1(i),turn2(i),a(i)'
  do i=1,N
     print '(i5,1x,i5,f7.3)',turn1(i),turn2(i),a(i)
  enddo

  stop
end program main

subroutine heapsort2(n,array,turn1,turn2)
  implicit none
  integer,intent(in)::n
  integer,intent(out)::turn1(n),turn2(n)
  real,intent(inout)::array(n)

  integer::i,k,j,l,m1,m2
  real::t

  if(n.le.0)then
     write(6,*)"Error, at heapsort"; stop
  endif
  if(n.eq.1)return

  l=n/2+1
  k=n
  do while(k.ne.1)
     if(l.gt.1)then
        l=l-1
        t=array(l)
        m1=turn1(l)
        m2=turn2(l)
     else
        t=array(k)
        m1=turn1(k)
        m2=turn2(k)
        array(k)=array(1)
        turn1(k)=turn1(1)
        turn2(k)=turn2(1)
        k=k-1
        if(k.eq.1) then
           array(1)=t
           turn1(1)=m1
           turn2(1)=m2
           exit
        endif
     endif
     i=l
     j=l+l
     do while(j.le.k)
        if(j.lt.k)then
           if(array(j).lt.array(j+1))j=j+1
        endif
        if (t.lt.array(j))then
           array(i)=array(j)
           turn1(i)=turn1(j)
           turn2(i)=turn2(j)
           i=j
           j=j+j
        else
           j=k+1
        endif
     enddo
     array(i)=t
     turn1(i)=m1
     turn2(i)=m2
  enddo

  return
end subroutine heapsort2

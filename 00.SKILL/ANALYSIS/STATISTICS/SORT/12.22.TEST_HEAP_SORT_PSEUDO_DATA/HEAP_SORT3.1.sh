SRC=$(basename $0 .sh).F90
SUB=HEAPSORT3.1_SUB.F90
EXE=$(basename $SRC .F90).EXE

FC=gfortran
#DOPT=" -fbacktrace"
DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"

$FC $DOPT $SUB $SRC -o $EXE
if [ $? -ne 0 ];then EEEEE COMPILE ERROR;exit 1;fi

$EXE


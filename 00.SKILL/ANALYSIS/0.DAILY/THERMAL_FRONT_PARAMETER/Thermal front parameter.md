# Thermal front parameter

## 定義と解釈

http://www.zamg.ac.at/docu/Manual/SatManu/main.htm?/docu/Manual/SatManu/Basic/Parameters/TFP.htm



## GrADSスクリプト

https://www.villasmunta.it/OpenGrADS/TFP.htm



## 単純な勾配との比較

https://www.researchgate.net/figure/The-850-hPa-a-magnitude-of-the-gradient-b-thermal-front-parameter-and-c_fig2_329004675



## 計算上の注意点

GrADSのスクリプトで鉛直方向に平均をとってから計算していますが，1000hPaから自由対流高度 (LFC)の高度までとするのが一番よいと思います。

理由：
LFCよりも上方では, 気塊の浮力によって自動的に上昇流が起こるので，前線周辺の持ち上げで重要となるのはLFCまでであるから。

積分の際の上端の高度は，おおよその値でかまいません。

例えば1000hPaから持ち上げた場合に、おおよそ、

LFCが1000m程度でしたら、1000hPaから900hPaまで、

LFCが1500m程度でしたら、1000hPaから850hPaまで、

LFCが2000m程度でしたら、1000hPaから800hPaまで、

とするので良いと思います。
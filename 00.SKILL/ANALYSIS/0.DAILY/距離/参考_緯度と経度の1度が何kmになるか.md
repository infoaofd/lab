# 参考_緯度と経度の1度が何kmになるか

[~/TEACHING/Distance.Sphere]

[am@aofd165]

$ distance.earth.sh

    lat.[deg.] dy per 1deg[km] dx per 1deg[km]
      1.00     111.319         111.302
      2.00     111.319         111.251
      5.00     111.319         110.895
     10.00     111.319         109.628
     15.00     111.319         107.526
     20.00     111.319         104.605
     25.00     111.319         100.889
     30.00     111.319          96.405
     35.00     111.319          91.187
     40.00     111.319          85.275
     45.00     111.319          78.714
     50.00     111.319          71.554
     55.00     111.319          63.850
     60.00     111.319          55.659
     65.00     111.319          47.045
     70.00     111.319          38.073
     75.00     111.319          28.811
     80.00     111.319          19.330
     85.00     111.319           9.702


distance.earth.sh
```bash
#!/bin/bash

f90=ifort
src=$(basename $0 .sh).f90
opt="-CB -traceback"
exe==$(basename $0 .sh).exe

cat <<EOF >$src
program distance_earth
integer,parameter::nlat=19
real*8,dimension(nlat)::lat
real*8,parameter::pi=3.14159265358979d0
real*8,parameter::d2r=pi/180.d0
real*8,parameter::r0eth =  6378.1d3
real*8::dy,dx

integer i

data lat/1.d0, 2.d0,  5.d0, 10.d0, 15.d0, 20.d0, 25.d0, &
30.d0, 35.d0, 40.d0, 45.d0, 50.d0, 55.d0, 60.d0, 65.d0, &
70.d0, 75.d0, 80.d0, 85.d0/

print '(A)',"lat.[deg.]   dy per 1deg[km]  dx per 1deg[km]"

do i=1,nlat
   olat=lat(i)
   dy=2.0*pi*r0eth/360.0/1000.0
   dx=2.0*pi*(r0eth*cos(olat*d2r))/360.0/1000.0
   print '(f10.2, 2x, f10.3,6x,f10.3)',olat,dy,dx
end do

end program distance_earth

EOF

$f90 $opt $src -o $exe
if [ $? -eq 0 ]; then
  $exe
  exit 0
else
  echo Compile Error!
  exit 1
fi
EOF
```
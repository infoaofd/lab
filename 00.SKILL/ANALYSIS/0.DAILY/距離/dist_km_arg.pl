#!/usr/bin/perl -w

# /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/04.SOUNDING/22.12.SONDE/16.12.CSC_LFM/DISTANCE_PROGRAM/Perl_Script

# syntax: dist_km_arg.pl lon0 lat0 lon1 lat1

# TEST RESULT
# 
# https://keisan.casio.jp/exec/system/1257670779
# 130.00 30.00 131.00 31.00
# r=6378.137 km
# d=146.940104 km
#
# $ perl dist_km_arg.pl 130.00 30.00 131.00 31.00
# 146.8127
#
# 精度はそれほど良くないので精度が重要になる場合は注意
#
use strict;

$/ = '>';
my $total = 0;

my $lon0 = $ARGV[0];
my $lat0 = $ARGV[1];
my $lon1 = $ARGV[2];
my $lat1 = $ARGV[3];

$total = calculate($lon1, (90-$lat1), $lon0, (90-$lat0));

printf("%.4f\n",$total) if $total;



sub calculate {
   my ($lon0, $lat0, $lon1, $lat1) = @_;
   my $pi = atan2(1, 1) * 4;

   $lon0 *= ($pi/180);
   $lat0 *= ($pi/180);
   $lon1 *= ($pi/180);
   $lat1 *= ($pi/180);

   $lat0 = $pi/2 - $lat0;
   $lat1 = $pi/2 - $lat1;

   # Radius computation for geocentric latitude
   my $r1 = 6378.135;
	# equatorial radius (semi-major axis)
   	# 3443.917 for n-miles, 3963.189 for s-miles, or 6378.135 for km
   my $r2 = 6356.75;
	# polar radius (semi-minor axis)
   	# 3432.37 for n-miles, 3949.901 for s-miles, or 6356.75 for km
   my $avlat = ($lat0 + $lat1)/2;
   my $r0 = ($r1 * $r2) / (sqrt($r1 * $r1 - (cos($avlat) * cos($avlat) * ($r1 * $r1 - $r2 * $r2))));

   # Great circle arc distance calculation
   my $x = cos($lat0) * cos($lat1) * cos($lon0 - $lon1) + sin($lat0) * sin($lat1);

   return (atan2(sqrt(1 - $x**2), $x) * $r0);
} 

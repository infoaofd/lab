#!/bin/bash

YYYYMMDDHH=20160606
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )
echo $YMD1 $YMD2 $YMD3 $YMD4 $YMD5


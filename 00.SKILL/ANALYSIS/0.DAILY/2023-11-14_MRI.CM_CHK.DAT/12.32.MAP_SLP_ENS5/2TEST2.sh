#!/bin/bash

YYYYMMDDHH=20160606
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT=/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/
CTL1=${INROOT}/${YMD1}/sfc_snp_1hr.ncctl
CTL2=${INROOT}/${YMD2}/sfc_snp_1hr.ncctl
CTL3=${INROOT}/${YMD3}/sfc_snp_1hr.ncctl
CTL4=${INROOT}/${YMD4}/sfc_snp_1hr.ncctl
CTL5=${INROOT}/${YMD5}/sfc_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
if [ ! -f $CTL3 ];then echo NO SUCH FILE,$CTL3;exit 1;fi
if [ ! -f $CTL4 ];then echo NO SUCH FILE,$CTL4;exit 1;fi
if [ ! -f $CTL5 ];then echo NO SUCH FILE,$CTL5;exit 1;fi

echo $CTL1
echo $CTL2
echo $CTL3
echo $CTL4
echo $CTL5

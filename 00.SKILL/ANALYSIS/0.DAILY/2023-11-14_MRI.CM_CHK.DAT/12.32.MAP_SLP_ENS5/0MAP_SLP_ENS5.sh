#!/bin/bash

VAR=PSEA
YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT=/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/
CTL1=${INROOT}/${YMD1}/sfc_snp_1hr.ncctl
CTL2=${INROOT}/${YMD2}/sfc_snp_1hr.ncctl
CTL3=${INROOT}/${YMD3}/sfc_snp_1hr.ncctl
CTL4=${INROOT}/${YMD4}/sfc_snp_1hr.ncctl
CTL5=${INROOT}/${YMD5}/sfc_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
if [ ! -f $CTL3 ];then echo NO SUCH FILE,$CTL3;exit 1;fi
if [ ! -f $CTL4 ];then echo NO SUCH FILE,$CTL4;exit 1;fi
if [ ! -f $CTL5 ];then echo NO SUCH FILE,$CTL5;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'; 'open ${CTL3}'; 'open ${CTL4}'
'open ${CTL5}'

xmax = 2; ymax = 3 

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=1; ymargin=0.5

'cc'
'set vpage 0.0 8.5 0.0 11'
'set grid off';'set grads off'

nmap = 1
ymap = 1
while (ymap <= ymax)
xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 10';'set ylint 20'
'set map 1 1 1'
'set gxout contour';'set cint 4';'set ccolor 1';'set cthick 1'
'set clopts 1 1 0.05'
'd PSEA.'nmap'/100.0'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'


# TEXT
'set t 1';'q dims';TEMP=sublin(result,5);INITIME=subwrd(TEMP,6)
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' INIT:'INITIME


xmap=xmap+1
nmap=nmap+1
endwhile #xmap

ymap=ymap+1
endwhile #ymap

xx = 4.5; yy=ytop+0.5
'set strsiz 0.15 0.18'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' VALID: ${TIME}'; say 'VALID:  ${TIME}'

'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
xx = 0.2; yy=ytop+1
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

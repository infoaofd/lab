#!/bin/bash

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/
CTLA=atm_snp_1hr_glb.ncctl
CTL11=${INROOT1}/${YMD1}/$CTLA
CTL12=${INROOT1}/${YMD2}/$CTLA
CTL13=${INROOT1}/${YMD3}/$CTLA
CTL14=${INROOT1}/${YMD4}/$CTLA
CTL15=${INROOT1}/${YMD5}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
if [ ! -f $CTL12 ];then echo NO SUCH FILE,$CTL12;exit 1;fi
if [ ! -f $CTL13 ];then echo NO SUCH FILE,$CTL13;exit 1;fi
if [ ! -f $CTL14 ];then echo NO SUCH FILE,$CTL14;exit 1;fi
if [ ! -f $CTL15 ];then echo NO SUCH FILE,$CTL15;exit 1;fi

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
CTL22=${INROOT2}/${YMD2}/$CTLO
CTL23=${INROOT2}/${YMD3}/$CTLO
CTL24=${INROOT2}/${YMD4}/$CTLO
CTL25=${INROOT2}/${YMD5}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi
if [ ! -f $CTL22 ];then echo NO SUCH FILE,$CTL22;exit 1;fi
if [ ! -f $CTL23 ];then echo NO SUCH FILE,$CTL23;exit 1;fi
if [ ! -f $CTL24 ];then echo NO SUCH FILE,$CTL24;exit 1;fi
if [ ! -f $CTL25 ];then echo NO SUCH FILE,$CTL25;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL12}'; 'open ${CTL13}'; 'open ${CTL14}'
'open ${CTL15}'

'open ${CTL21}'; 'open ${CTL22}'; 'open ${CTL23}'; 'open ${CTL24}'
'open ${CTL25}'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS


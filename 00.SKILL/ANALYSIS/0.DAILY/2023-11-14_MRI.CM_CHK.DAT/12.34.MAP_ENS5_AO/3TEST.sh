#!/bin/bash

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/
CTLA=atm_snp_1hr.ncctl
CTL11=${INROOT1}/${YMD1}/$CTLA
CTL12=${INROOT1}/${YMD2}/$CTLA
CTL13=${INROOT1}/${YMD3}/$CTLA
CTL14=${INROOT1}/${YMD4}/$CTLA
CTL15=${INROOT1}/${YMD5}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
if [ ! -f $CTL12 ];then echo NO SUCH FILE,$CTL12;exit 1;fi
if [ ! -f $CTL13 ];then echo NO SUCH FILE,$CTL13;exit 1;fi
if [ ! -f $CTL14 ];then echo NO SUCH FILE,$CTL14;exit 1;fi
if [ ! -f $CTL15 ];then echo NO SUCH FILE,$CTL15;exit 1;fi

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
CTL22=${INROOT2}/${YMD2}/$CTLO
CTL23=${INROOT2}/${YMD3}/$CTLO
CTL24=${INROOT2}/${YMD4}/$CTLO
CTL25=${INROOT2}/${YMD5}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi
if [ ! -f $CTL22 ];then echo NO SUCH FILE,$CTL22;exit 1;fi
if [ ! -f $CTL23 ];then echo NO SUCH FILE,$CTL23;exit 1;fi
if [ ! -f $CTL24 ];then echo NO SUCH FILE,$CTL24;exit 1;fi
if [ ! -f $CTL25 ];then echo NO SUCH FILE,$CTL25;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL12}'; 'open ${CTL13}'; 'open ${CTL14}'
'open ${CTL15}'

xmax = 6; ymax = 2

ytop=7

xwid = 10.5/xmax; ywid = 5.0/ymax
xmargin=0.3; ymargin=0.5

'cc'
'set vpage 0.0 11 0.0 8.5'
'set grid off';'set grads off'

nmap = 1
ymap = 1
xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 30';'set ylint 10'
'set gxout contour';'set cint 20';'set ccolor 1';'set cthick 1'
'set clopts 1 1 0.05'
'set mpdraw on'; 'set map 1 1 1'
'd Z.'nmap


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'


# TEXT
'set t 1';'q dims';TEMP=sublin(result,5);INITIME=subwrd(TEMP,6)
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' INIT:'INITIME

xmap=xmap+1
nmap=nmap+1
endwhile #xmap

'allclose'



'open ${CTL21}'; 'open ${CTL22}'; 'open ${CTL23}'; 'open ${CTL24}'
'open ${CTL25}'

nmap = 1
ymap = 2
while (ymap <= ymax)

xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - 1.5; ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 30';'set ylint 10'
'set gxout contour';'set cint 2';'set ccolor 1';'set cthick 1'
'set clopts 1 1 0.05'
'set mpdraw on'; 'set map 1 1 1'
'd tos.'nmap


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'

xmap=xmap+1
nmap=nmap+1
endwhile #xmap

ymap=ymap+1
endwhile #ymap

xx = 4.5; yy=ytop+0.5
'set strsiz 0.15 0.18'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' VALID: ${TIME}'; say 'VALID:  ${TIME}'

'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
xx = 0.2; yy=ytop+1
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcl "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

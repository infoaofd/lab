# 対数測による風速の推定



## 対数測

$$
u(z)=\frac{u_*}{\kappa}\log\frac{z}{z_0}
$$

$\kappa$: カルマン定数 ($=0.4$), $u_*$: 摩擦速度, $z_0$: 粗度, $z$: 地表面からの高さ

$z=z_1$における$u(z_1)$の式と$z=z_2$における$u(z_2)$の式を辺々割ると,
$$
u(z_2)=u(z_1)\frac{\log\frac{z_2}{z_0}}{\log\frac{z_1}{z_0}}
$$
である。これより，高度$z_1$, $z=z_1$における風速$u(z_1)$, $z_2$の3つの値が分かれば，$z=z_2$における風速$u(z_2)$の値が推定できる。

### 注意

- **対数測は接地層と呼ばれる地表面近くの薄い層でしか使えない**。接地層の厚さは大気の乱れの程度によるが数十m程度である。

- 風速をベクトル量として扱う場合は，$u$, $v$それぞれの成分に対して対数測を適用すればよい。

## Fortranプログラム

### コンパイル例

```bash
$ ift
```

もしくは

```bash
$ source /opt/intel/oneapi/setvars.sh
```

```bash
$ ifort -o LOGLAW.EXE LOGLAW.F90
```



### 実行例

```bash
$ LOGLAW.EXE 10.0 20.0 5.0
z0=   0.05000
z1=  20.00000 u1=  10.00000
z2=   5.00000 u2=   7.68622
```



## プログラム

LOGLAW.F90

```fortran
!
! Estimate the near surface wind speed using log-law
!
! On-line Documents
! 
! iargc and getarg:
! http://hydro.iis.u-tokyo.ac.jp/~akira/page/Fortran/contents/io/general/arg.html
!
! Log-law of the vertical wind profile
! https://ocw.kyoto-u.ac.jp/wp-content/uploads/2021/03/2011_butsurikikougaku_11.pdf  
! Log-law
!   u(z)=kappa*log(z/z0)
! where, u: wind speed, z: altitude, z0: surface roughness
! 
integer :: i
character(len=1000),dimension(0:4) :: arg

call getarg(0,arg(0))

if( iargc() < 3 )then
print '(a)', 'MMMMM ERROR: Invalid number of command line arguments.'
print '(a,a,a)','MMMMM USAGE: ',trim(arg(0)),' u1 z1 z2 [z0]'
stop
endif

call getarg(1,arg(1))
read(arg(1),*) u1

call getarg(2,arg(2))
read(arg(2),*) z1

call getarg(3,arg(3))
read(arg(3),*) z2

z0=0.05

if (iargc()>=4)then
call getarg(3,arg(3))
read(arg(3),*) z0
end if

! ESTIAMTE u2 using log-law
u2=u1*log(z2/z0)/log(z1/z0) 

print '(2(a,f10.5))','z0=',z0
print '(2(a,f10.5))','z1=',z1,' u1=',u1
print '(2(a,f10.5))','z2=',z2,' u2=',u2

stop
end
!
! Table 1: surface roughness,z0 (p.21 of Takeuchi and Kondo, 1981)
!
! water surface (calm)  (0.1-10)*1E-5
!
! ice (smooth)          0.1*1E-4
!
! snow                  (0.5-10)*1E-4
!
! sand,desart           0.0003
!
! soil surface          0.001-0.01
!
! grass (0.02-0.1m)     0.003-0.01
!
! grass (0.25-1.0m)     0.04-0.10
!
! farmland              0.04-0.20
!
! orchard               0.5-1.0
!
! forest                1.0-6.0
!
! big city (Tokyo)      2.0
```


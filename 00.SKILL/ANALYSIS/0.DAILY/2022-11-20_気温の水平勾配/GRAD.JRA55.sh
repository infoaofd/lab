#!/bin/bash

DSET=JRA55

#Y=2020; MM=07; MMM=JUL; DD=03; HH=00
Y=2014; MM=04; MMM=APR; DD=04; HH=00
PLEV1=850
LONW=120; LONE=140
LATS=26 ; LATN=42
LONW=120; LONE=160
LATS=20 ; LATN=55

LONW0=115; LONE0=165; LATS0=15; LATN0=60

TIME=${HH}Z${DD}${MMM}${Y}

POW=5
FAC=1E${POW}
#SCL=10\`a-${POW}\`n
# https://www.sci.hokudai.ac.jp/grp/poc/top/old/software/other/grads_tips/index.htm

UNIT="${SCL} K/100 km"

KIND='-kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
VAR="GRAD T"
TITLE="${DSET} ${VAR} $TIME ${PLEV1}hPa"

INDIR=/work01/DATA/JRA55/6HR/PRS/${Y}
INFLE=anl_p125.011_tmp.2014040100_2014043018.nc
IN1=$INDIR/$INFLE
if [ ! -f $IN1 ]; then echo NO SUCH FILE, $IN1; echo; exit 1; fi

INDIR=/work01/DATA/JRA55/6HR/SFC/${Y}
INFLE=anl_surf125.002_prmsl.2014040100_2014043018.nc
IN2=$INDIR/$INFLE
if [ ! -f $IN2 ]; then echo NO SUCH FILE, $IN2; echo; exit 1; fi

PREFIX=$(basename $0 .sh)
FIG=${PREFIX}_${Y}${MM}${DD}_${HH}_${PLEV1}.pdf

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

# GRADS SCRIPT GOES HERE:
cat <<EOF>$GS
say

'sdfopen $IN1'
'sdfopen $IN2'


'q ctlinfo 1';say result
'q ctlinfo 2';say result


'set time ${TIME}'
'set lev $PLEV1'
'set lon $LONW0 $LONE0'; 'set lat $LATS0 $LATN0'


say '### GRAD'
'var=var11.1'

pi=3.14159265359
dtr=pi'/'180

r=6.371e6

dx '=' r '*cos(' dtr '*' lat ')*' dtr '*cdiff(' lon ',' x')'
dy '=' r '*' dtr '*cdiff(' lat ',' y ')'

dtdx '=cdiff(' var ',' x ')/' dx
dtdy '=cdiff(' var ',' y ')/' dy

grad '=mag(' dtdx ',' dtdy ')*$FAC'


say '### PLOT'
'cc'
'set grads off'; 'set grid off'
'set mpdset hires'
'set rgb 99 139 69 19'; 'set map 99 1 3'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'

'set vpage 0.0 8.5 0.0 11'

xs=1; xe=6
ys=3; ye=10
'set parea 'xs ' 'xe' 'ys' 'ye


'set xlab on'; 'set ylab on'
'set xlint 10'; 'set ylint 5'
'set hershey off'

say '### COLOR SHADE'
'color 0.5 5 0.5 $KIND'

#'d maskout(grad,p/100)' ;#-lev)'
'd grad'

'set xlab off'; 'set ylab off'


'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

x1=xr+0.4; x2=x1+0.1
y1=yb    ; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.12 -fh 0.16 -ft 4 -fs 2'

say '### SCALE & UNITS'
'set strsiz 0.12 0.16'; 'set string 1 c 4 0'
xx=(x1+x2)/2+0.2; yy=y2+0.2
'draw string ' xx ' ' yy ' ${UNIT}'

say '### CONTOUR T'
'set gxout contour'
'set ccolor 0'
'set cthick 5'
'set clab off';'set cint 2'
'set clskip 2'
'd var'
'set gxout contour'
'set ccolor 1'
'set cthick 1'
'set clab on';'set cint 2'
'set clskip 2'
'd var'

say '### CONTOUR SLP'
'set dfile 2'
'set z 1'
'set time $TIME'
'q dims';say result
'SLP=var2.2'
'set gxout contour'
'set ccolor 0'
'set cthick 5'
'set clab off';'set cint 4'
'set clskip 2'
'd SLP/100'
'set gxout contour'
'set ccolor 2'
'set cthick 2'
'set clab on';'set cint 4'
'set clskip 2'
'd SLP/100'



say '### TITLE'
'set strsiz 0.12 0.14'
'set string 1 c 3 0'
xx = (xl+xr)/2; yy=yt+0.2
'draw string ' xx ' ' yy ' ${TITLE}'

say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yy+0.5
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

say '$INFLE'
say
'gxprint $FIG'
say
'!ls -lh $FIG'
say
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

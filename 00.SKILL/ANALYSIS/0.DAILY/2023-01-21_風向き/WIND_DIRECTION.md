# 風向きを表す角度の変換

## 基礎知識

### 気象学で使う角度

風がどちらの方位から吹いてくるかを表している

方位→0度が北で時計回り

### 数学的な角度

ベクトルの先端の向き

角度→0度が東で反時計回り



## 変換式

単位を度として、気象学で使う角度をazm, 数学的な角度をdirとすると、

```
  dir = 270.0 - azm
```
で変換が可能 (p.???, Bluesten, 1996)。

### 例

!   0°(北）よりの風 -> 数学的な角度は270°

!  90°(東）よりの風 -> 数学的な角度は180°

## シェルスクリプト

サンプルシェルスクリプト

風向・風速→東西風, 南北風への分解を行う

ファイルの所在：[~/Dbox_bak/Tools/Scripts/Translate.Velocity]

$ cat trans.v.sh
```
#!/bin/bash

usage(){

cat << EOF

Usage: $0 <input>

<input>: input file name (e.g., wind.azm.txt)

EOF

}

input_file=$1

#if [ ! -f $input_file ]; then

#  echo No such file, $input_file

#  exit 1

#fi

output_file=$(basename $input_file .txt).out.txt

echo

echo Input:  $input_file

echo Output: $output_file

deg2rad=$(echo "scale=10; 3.1415926536/180.0" | bc)

#echo $deg2rad

cat << EOF > $output_file

# u(E-comp)    v(N-comp)      W(wind spd)  theta(wind dir)

EOF

awk -v d2r=$deg2rad '{if($1 != "#") printf "%10.5f  %10.5f %10.5f

%10.5f\n", \

  $1*cos((270.0 - $2)*d2r), $1*sin((270.0 - $2)*d2r), $1, $2 }' \

  $input_file >> $output_file

echo

echo Done. $(basename $0).

echo

exit 0
```
入力データファイル

$ cat wind.azm.txt
```bash
#wind speed, wind direction (azimuth in degree)

5.0    0.0

5.0   90.0

5.0  180.0

5.0  270.0

5.0  360.0

10.0   0.0

10.0  90.0

10.0 180.0

10.0 270.0

10.0 360.0
```
Fortran
変換プログラム:

kazemuki.f90 - 2011/06/14 0:33、A M (バージョン 1) 削除

1KB ダウンロード

az2dr.f90 - 2011/06/14 0:32、A M (バージョン 1) 削除

1KB ダウンロード

az2dr.f - 2011/06/14 0:32、A M (バージョン 1) 削除

1KB ダウンロード

makefile - 2011/06/14 0:33、A M (バージョン 1) 削除

3KB ダウンロード

作図用シェルスクリプト (GMT使用)

kazemuki.sh - 2011/06/14 0:33、A M (バージョン 2 / 旧バージョン) 削除

1KB ダウンロード

gmtpar.sh - 2011/07/20 18:00、A M (バージョン 1) 削除

1KB ダウンロード

note.sh - 2011/07/20 18:00、A M (バージョン 1) 削除

1KB ダウンロード

実行例

[Tue Jun 14 16:24:22 JST 2011]

[am@aofd30 processor=x86_64]

[~/Toolbox/Kazemuki]

$ make
```
if [ ! -d ../obj ]; then \

        mkdir -p ../obj ; \
    
        fi

ifort -c  -CB -traceback -fpe0  -module ../obj  -c -o ../obj/kazemuki.o kazemuki.f90

ifort -c -CB -traceback -fpe0  -module ../obj  -c -o ../obj/az2dr.o az2dr.f

ifort -o kazemuki ../obj/kazemuki.o ../obj/az2dr.o -module ../obj

real    0m0.081s

user    0m0.055s

sys     0m0.025s
```
[Tue Jun 14 16:24:23 JST 2011]

[am@aofd30 processor=x86_64]

[~/Toolbox/Kazemuki]

$ kazemuki > kazemuki.asc

[Tue Jun 14 16:24:33 JST 2011]

[am@aofd30 processor=x86_64]

[~/Toolbox/Kazemuki]

$ kazemuki.sh kazemuki.asc

Bash script ./kazemuki.sh starts.

Input : kazemuki.asc

Output : ./kazemuki.ps

-W3/0/0/255

pstext: Record 6 is incomplete (skipped)

Done ./kazemuki.sh.sh

kazemuki.ps - 2011/06/14 0:34、A M (バージョン 2 / 旧バージョン) 削除

38KB 表示 ダウンロード
PT EMAGRAM
=========================
P.d03.PT_EMAGRAM
============================-
[TOC]
  
Wed, 27 May 2020 13:12:15 +0900
calypso.bosai.go.jp
/work05/manda/WRF.POST/K17/GRADS.2004/P.d03.PT_EMAGRAM

```
srcdump.sh P.d03.PT_EMAGRAM.LOG ./P.d03.PT_EMAGRAM.sh P.d03.PT_EMAGRAM.GS P.d03.PT_EMAGRAM.f90 P.d03.PT_EMAGRAM_GMT.sh P.d03.PT_EMAGRAM.py
```
  
### HOW TO RUN
  
### INFO
**Machine info**
processor	: 15
model name	: Intel(R) Xeon(R) CPU E5-2690 0 @ 2.90GHz
MemTotal:       65988728 kB
  
### SOURCE FILES
- P.d03.PT_EMAGRAM.LOG
- ./P.d03.PT_EMAGRAM.sh
- P.d03.PT_EMAGRAM.GS
- P.d03.PT_EMAGRAM.f90
- P.d03.PT_EMAGRAM.py
  
#### P.d03.PT_EMAGRAM.LOG
```
Wed, 27 May 2020 13:12:12 +0900
calypso.bosai.go.jp
/work05/manda/WRF.POST/K17/GRADS.2004/P.d03.PT_EMAGRAM
./P.d03.PT_EMAGRAM.sh K17.R27.06.06 09Z05JUL2017 130.53 33.4

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
P.d03.PT_EMAGRAM.GS
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 112 2020-05-27 13:12 EPT.bin
-rw-rw-r-- 1 manda manda 112 2020-05-27 13:12 SEPT.bin
-rw-rw-r-- 1 manda manda 112 2020-05-27 13:12 PRESSURE.bin

Grid Analysis and Display System (GrADS) Version 2.1.1.b0
Copyright (C) 1988-2017 by George Mason University
GrADS comes with ABSOLUTELY NO WARRANTY
See file COPYRIGHT for more information

Config: v2.1.1.b0 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids,stn geotiff shapefile cairo
Issue 'q config' command for more detailed configuration information
GX Package Initialization: Size = 8.5 11 
Running in Batch mode
OPEN /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H.ctl
Notice: Implied interpolation for file /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H.ctl
 Interpolation will be performed on any data displayed from this file

OPEN /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H.ctl
Notice: Implied interpolation for file /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H.ctl
 Interpolation will be performed on any data displayed from this file


MAP EPT


EPT
dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H_%y4-%m2-%d2_%h2:%n2.dat

deepskyblue->lightcyan->wheat->orange->crimson
clevs= 342 344 346 348 350 352 354 356 358 360 362
ccols= 16 17 18 19 20 21 22 23 24 25 26 27

MARK

mark=2
size=0.15
color=1
long=130.53 -> x1=2.272 lati=33.4 -> y1=7.212

BOX


VPR EPT


EPT
dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H_%y4-%m2-%d2_%h2:%n2.dat

Default file number is: 1 
X is fixed     Lon = 130.528  X = 298
Y is fixed     Lat = 33.4015  Y = 420
Z is varying   Lev = 990 to 150   Z = 2 to 29
T is fixed     Time = 09Z05JUL2017  T = 16
E is fixed     Ens = 1  E = 1


VPR TK AND TD


TK
dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H_%y4-%m2-%d2_%h2:%n2.dat

Default file number is: 1 
X is fixed     Lon = 130.528  X = 298
Y is fixed     Lat = 33.4015  Y = 420
Z is varying   Lev = 990 to 700   Z = 2 to 18
T is fixed     Time = 09Z05JUL2017  T = 16
E is fixed     Ens = 1  E = 1


PRINT HEADER OF P.d03.PT_EMAGRAM_TMP.TXT


TEXTOUT
EPT


dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H_%y4-%m2-%d2_%h2:%n2.dat


LCL in hPa


dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H_%y4-%m2-%d2_%h2:%n2.dat

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LCL : INIT Z=0.5 km EPT=349.594 P=913.79
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LFC : INIT Z=0.5 km EPT=349.594 P=900.186
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

EL in hPa

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
LNB : INIT Z=0.5 km EPT=349.594 P=204.089
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


CAPE & CIN


dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H_%y4-%m2-%d2_%h2:%n2.dat

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CAPE : INIT Z=0.5 km CAPE=879.069
CIN  : INIT Z=0.5 km CIN =2.21094
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BINARY OUT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

dset /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H_%y4-%m2-%d2_%h2:%n2.dat

Default file number is: 1 
X is fixed     Lon = 130.528  X = 298
Y is fixed     Lat = 33.4015  Y = 420
Z is varying   Lev = 990 to 150   Z = 2 to 29
T is fixed     Time = 09Z05JUL2017  T = 16
E is fixed     Ens = 1  E = 1

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
nlev=28
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
GX Package Terminated 
-rw-rw-r-- 1 manda manda 337K 2020-05-27 13:12 P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017_SUB.eps
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
P.d03.PT_EMAGRAM.f90
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 1.9K 2020-05-27 13:12 P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.TXT
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
P.d03.PT_EMAGRAM.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-rw-rw-r-- 1 manda manda 1.9K 2020-05-27 13:12 ./P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.TXT
-rw-rw-r-- 1 manda manda 69205 2020-05-27 13:12 P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.png


[350.383 350.069 349.837 349.665 349.47  349.248 348.896 348.151 347.406
 346.522 344.584 342.957 341.637 340.376 339.27  337.842 338.413 339.013
 339.2   338.596 338.108 338.295 339.739 342.245 344.568 346.372 349.331
 357.924]
[0.997 0.994 0.991 0.988 0.985 0.982 0.979 0.976 0.973 0.97  0.964 0.958
 0.951 0.945 0.938 0.921 0.903 0.884 0.864 0.843 0.82  0.796 0.77  0.741
 0.709 0.673 0.631 0.581]
[990. 980. 970. 960. 950. 940. 930. 920. 910. 900. 880. 860. 840. 820.
 800. 750. 700. 650. 600. 550. 500. 450. 400. 350. 300. 250. 200. 150.]
EPT= 349.594  K
LCL= 0.975  IN EXNER
LFC= 0.97  IN EXNER
LNB= 0.635  IN EXNER
CAPE= 879  J/kg
CIN= 2  J/kg

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
CREATE DOC FILE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```

  
#### ./P.d03.PT_EMAGRAM.sh
```bash
#!/bin/bash

runname=$1
#runname=K17.R27.00.00
#runname=K17.R27.00.01
#runname=K17.R27.06.01

TIME0=$2 #00Z05JUL2017
CLON=$3 #130.53
CLAT=$4 #33.4

domain=d03


#if [ $# -lt 1 ]; then
#echo
#echo ERROR in $0: NO ARGUMENT
#echo USAGE $(basename $0) RUNNAME
#echo
#exit 1
#fi

indir_root=/work06/manda/ARWpost_K17_work06
indir=${indir_root}/ARWpost_${runname}

# MAP
LON0=130 #130.0
LON1=131.25 #131.0
LAT0=33 ##33   
LAT1=34 ##34 


# INITIAL HEIGHT OF A PARCEL
INIZ=0.5 #km

# FOR VERTICAL PROFILE & Y-AXIS OF PT-EMAGRAM
CLEVB=990
CLEVT=150
CLEVB_TK=$CLEVB
CLEVT_TK=700

INTERVAL=01H

#RECTANGLE
#lonw=130.48 #130.0
#lone=130.58 #131.0
#lats=33.28 #33   
#latn=33.38 #34   


fig=$(basename $0 .sh)_${runname}.${domain}_${CLON}_${CLAT}_${TIME0}_SUB.eps

ctl1=${indir}/${runname}.${domain}.basic_p.${INTERVAL}.ctl
ctl2=${indir}/${runname}.${domain}.cape3_z.${INTERVAL}.ctl

TMPDIR=$(basename $0 .sh)_${runname}.${domain}
mkdir -vp $TMPDIR

VAR=EPT
LEV=${INIZ}
LEVUNIT=km #hPa
TITLE="${VAR} (${LEV}${LEVUNIT})"
UNIT="[K]"

levs='342 362 2'
kind='deepskyblue->lightcyan->wheat->orange->crimson'
#kind='lightyellow->wheat->gold->orange->tomato->red->firebrick'
#kind="mistyrose->lightpink->mediumvioletred->blue->dodgerblue->aqua"
STEP_EPT=2

levs2=$levs #'7 20 1'
kind2=$kind

VAR2=SEPT
VRANGE="335 365"

VRANGE_TK="280 300"


GS=$(basename $0 .sh).GS
F90=$(basename $0 .sh).f90
EXE=$(basename $0 .sh).EXE
GMT=$(basename $0 .sh)_GMT.sh
PY=$(basename $0 .sh).py
LOG=$(basename $0 .sh).LOG
DOC=$(basename $0 .sh)_$(date +"%y%m%d_%H").md

# TEMPORALLY FILES
BIN1X=${VAR}.bin
BIN2X=${VAR2}.bin
BIN1Y=PRESSURE.bin
TMPTXT=$(basename $0 .sh)_TMP.TXT
rm -rf $TMPTXT
ASCOUT=$(basename $fig _SUB.eps).TXT



TIMESTAMP=$(date -R)
HOST=$(hostname)
CWD=$(pwd)
COMMAND="$0 $@"

echo $TIMESTAMP >$LOG
echo $HOST >>$LOG
echo $CWD  >>$LOG
echo $COMMAND>>$LOG
echo >>$LOG

cat <<EOF>$GS
'cc'

say 'OPEN ${ctl1}'
'open ${ctl1}'
say

say 'OPEN ${ctl2}'
'open ${ctl2}'
say

'set xlopts 1 3 0.12'
'set ylopts 1 3 0.12'


xmax=1
ymax=2

ytop=9

xwid=3.0/xmax
ywid=6./ymax
xmargin=1
ymargin=1


ymap=1

say
say 'MAP $VAR'
say

nmap=1
xmap=1
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'

'set dfile 2'
'q ctlinfo 2'
say
say ${VAR}
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${LON0} ${LON1}'
'set lat ${LAT0} ${LAT1}'
'set lev ${LEV}'
'color ${levs} -kind ${kind} -gxout shaded'

'set xlint 0.5'
'set ylint 0.5'

'set mpdset hires'
'd $VAR(time=${TIME0})'
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

x=xl ;# (xl+xr)/2
y=yt+0.2
'set strsiz 0.1 0.12'
'set string 1 l 3 0'
'draw string 'x' 'y' ${TITLE} ${UNIT}'
y = y+0.2
'draw string 'x' 'y' ${TIME0}'
y = y+0.2
'draw string 'x' 'y' ${runname}'

x1=xr+0.2
x2=x1+0.1
y1=yb ;#-0.5
y2=yt ;#+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${STEP_EPT} -ft 3 -line on -edge circle'

'set xlab off'
'set ylab off'


say
say 'MARK'
say
'markplot ${CLON} ${CLAT} -c 1 -m 2 -s 0.15' 

#say
#say 'LINE'
#say
#'trackplot ${CLON} ${CLATS} ${CLON} ${CLATN} -c 1 -l 1 -t 5' 

say
say 'BOX'
say
#'trackplot ${lonw} ${lats} ${lone} ${lats} -c 1 -l 1 -t 4' 
#'trackplot ${lone} ${lats} ${lone} ${latn} -c 1 -l 1 -t 4' 
#'trackplot ${lone} ${latn} ${lonw} ${latn} -c 1 -l 1 -t 4' 
#'trackplot ${lonw} ${latn} ${lonw} ${lats} -c 1 -l 1 -t 4' 



say
say 'VPR $VAR'
say

nmap=1
ymap=2
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'
'set xlab on'
'set ylab on'

'set dfile 1'
'q ctlinfo 1'
say
say ${VAR}
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev ${CLEVB} ${CLEVT}'
'q dims'
say result
'set cmark 0'
'set vrange $VRANGE'

'set xlint 5'
'set ylint 100'
'd ${VAR}(time=${TIME0})'

'set xlab off'
'set ylab off'
'set cmark 0'
'set vrange $VRANGE'
'd ${VAR2}(time=${TIME0})'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.1 0.12'

'set string 1 c 3 90'
x=xl-0.5 
y=(yb+yt)/2
'draw string 'x' 'y' P [hPa]'

x=xl ;# (xl+xr)/2
y=yt+0.2
'set string 1 l 3 0'
'draw string 'x' 'y' ${VAR} ${VAR2} ${UNIT} ${CLAT}N ${CLON}E'
y = y+0.2
'draw string 'x' 'y' ${TIME0}'

'set xlab off'
'set ylab off'

# Header
'set strsiz 0.1 0.12'
'set string 1 l 2'
'draw string 0.5 10.2 ${TIMESTAMP} ${HOST}'
'draw string 0.5 10 ${CWD}'
'draw string 0.5 9.8 ${COMMAND}'



say
say 'VPR TK AND TD'
say

nmap=3
xmap=2
ymap=2
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'
'set xlab on'
'set ylab on'

'set dfile 1'
'q ctlinfo 1'
say
say TK
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev ${CLEVB_TK} ${CLEVT_TK}'
'q dims'
say result
'set cmark 0'
'set vrange $VRANGE_TK'

'set xlint 5'
'set ylint 50'
'd tk(time=${TIME0})'

'set xlab off'
'set ylab off'
'set cmark 0'
'set vrange $VRANGE_TK'
'd td(time=${TIME0})+273.15'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.1 0.12'
'set string 1 c 3 90'
x=xl-0.5 
y=(yb+yt)/2
'draw string 'x' 'y' P [hPa]'

x=xl ;# (xl+xr)/2
y=yt+0.2
'set string 1 l 3 0'
'draw string 'x' 'y' T Td ${UNIT} ${CLAT}N ${CLON}E'
y = y+0.2
'draw string 'x' 'y' ${TIME0}'



'gxprint $fig'



say
say 'PRINT HEADER OF ${TMPTXT}'
say
ret = write ('${TMPTXT}','# ${runname}',append)
ret = write ('${TMPTXT}','# ${domain}',append)
ret = write ('${TMPTXT}','# ${TIME0}',append)
ret = write ('${TMPTXT}','# ${CLON}',append)
ret = write ('${TMPTXT}','# ${CLAT}',append)


say
say TEXTOUT
say '${VAR}'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev $INIZ'
'd ${VAR}'
line=sublin(result,2)
x=subwrd(line,4)
ret = write ('${TMPTXT}','LFC EPT= 'x,append)

say
say 'LCL in hPa'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say

'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev $INIZ'
'd LCL3P'
line=sublin(result,2)
LCL3P=subwrd(line,4)
ret = write ('${TMPTXT}','LCL PRS= 'LCL3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LCL : INIT Z=${INIZ} km EPT=' x ' P='LCL3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'


'd LFC3P'
line=sublin(result,2)
LFC3P=subwrd(line,4)

ret = write ('${TMPTXT}','LFC PRS= 'LFC3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LFC : INIT Z=${INIZ} km EPT=' x ' P='LFC3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'



say
say 'EL in hPa'
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev $INIZ'

'd EL3P'
line=sublin(result,2)
EL3P=subwrd(line,4)

ret = write ('${TMPTXT}','LNB PRS= 'EL3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LNB : INIT Z=${INIZ} km EPT=' x ' P='EL3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say

say
say 'CAPE & CIN'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev $INIZ'

'd cape'
line=sublin(result,2)
CAPEP=subwrd(line,4)


'd cin'
line=sublin(result,2)
CINP=subwrd(line,4)
ret = write ('${TMPTXT}','CAPE= 'CAPEP,append)
ret = write ('${TMPTXT}','CIN=  'CINP,append)
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'CAPE : INIT Z=${INIZ} km CAPE='CAPEP
say 'CIN  : INIT Z=${INIZ} km CIN ='CINP
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say


say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'BINARY OUT'
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'set dfile 1'
'q ctlinfo 1'
say
say sublin(result,1)
say
'set time ${TIME0}'
'set lon ${CLON}'
'set lat ${CLAT}' ;# ${CLATN}'
'set lev ${CLEVB} ${CLEVT}'
'q dims'
say result
line=sublin(result,4)

k1=subwrd(line,11)
k2=subwrd(line,13)
nlev=k2-k1+1
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'nlev='nlev
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
ret = write ('${TMPTXT}','nlev= 'nlev,append)

'set gxout fwrite'
'set fwrite -le ${BIN1X}'
'd ${VAR}.1(time=${TIME0})'
'disable fwrite'
'!ls -lh --time-style=long-iso ${BIN1X}'

'set gxout fwrite'
'set fwrite -le ${BIN2X}'
'd ${VAR2}.1(time=${TIME0})'
'disable fwrite'
'!ls -lh --time-style=long-iso ${BIN2X}'


'set gxout fwrite'
'set fwrite -le ${BIN1Y}'
'd pressure.1(time=${TIME0})'
'disable fwrite'
'!ls -lh --time-style=long-iso ${BIN1Y}'



'quit'
EOF

#/usr/local/grads-2_0_a8/bin/grads -bcl "${gs}"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "$GS"                                  |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG

grads -bcp "${GS}" 2>&1 |tee -a $LOG
if [ $? -ne 0 ]; then
echo
echo ERROR in grads -bcp "${GS}" 
exit 1
echo
fi

echo
ls -lh --time-style=long-iso $fig |tee -a $LOG
echo


cat <<EOF>$F90
INTEGER, PARAMETER :: nlon=1, nlat=1, ntim=1
integer nlev
REAL,allocatable,dimension(:,:,:,:) :: X1,X2,Y
real,allocatable,dimension(:)::exner
real,parameter::kappa=287.0/1004.0
real,parameter::p0=1000.
character(len=200)::STRM
real LON, LAT,EPT_LFC,LCL_P,LFC_P,LNB_P,LCL_EXNER,LFC_EXNER,LNB_EXNER
OPEN(11,FILE="${TMPTXT}",action="read")
read(11,*)
read(11,*)
read(11,*)
read(11,'(A)')STRM
read(STRM(3:),*)LON
read(11,'(A)')STRM
read(STRM(3:),*)LAT
read(11,'(A)')STRM
read(STRM(10:),*)EPT_LFC
read(11,'(A)')STRM
read(STRM(10:),*)LCL_P
read(11,'(A)')STRM
read(STRM(10:),*)LFC_P
read(11,'(A)')STRM
read(STRM(10:),*)LNB_P
read(11,'(A)')STRM
read(STRM(7:),*)CAPE
read(11,'(A)')STRM
read(STRM(7:),*)CIN
read(11,'(A)')STRM
read(STRM(7:),*)nlev
CLOSE(11)

allocate(X1(nlon, nlat, nlev, ntim),X2(nlon, nlat, nlev, ntim),&
Y(nlon, nlat, nlev, ntim))
allocate(exner(nlev))

OPEN(11, FILE="${BIN1X}", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) X1
CLOSE(11)

OPEN(11, FILE="${BIN2X}", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) X2
CLOSE(11)

OPEN(11, FILE="${BIN1Y}", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) Y
CLOSE(11)

LCL_EXNER=(LCL_P/p0)**kappa
LFC_EXNER=(LFC_P/p0)**kappa
LNB_EXNER=(LNB_P/p0)**kappa

OPEN(21, FILE="${ASCOUT}")
write(21,'(A)')'# $(date -R)'
write(21,'(A)')'# $(pwd)'
write(21,'(A)')'# $0 $@'
write(21,'(A)')'# $RUNNAME'
write(21,'(A)')'# $ctl1'
write(21,'(A)')'# $ctl2'
write(21,'(A)')'# $ctl3'
write(21,'(A)')'# TIME= $TIME0'
write(21,'(A)')'# LON= $CLON'
write(21,'(A)')'# LAT= $CLAT'
write(21,'(A,f10.3)')'# EPT_LFC= ',EPT_LFC
write(21,'(A,f10.3, i5)')'# LCL_P= ',LCL_EXNER, int(LCL_P)
write(21,'(A,f10.3, i5)')'# LFC_P= ',LFC_EXNER, int(LFC_P)
write(21,'(A,f10.3, i5)')'# LNB_P= ',LNB_EXNER, int(LNB_P)
write(21,'(A,i7)')'# CAPE= ',int(CAPE)
write(21,'(A,i7)')'# CIN=  ',int(CIN)

write(21,'(A)')&
'#   IDX   EPT       SEPT        EXNER   P      '

i=1
j=1
m=1
do k=1,nlev
exner(k)=(Y(i,j,k,m)/p0)**kappa
print '(i5,4f10.3)', k,exner(k),X1(i,j,k,m),X2(i,j,k,m),Y(i,j,k,m)
write(21,'(i5,4f10.3)') k,X1(i,j,k,m),X2(i,j,k,m),exner(k),Y(i,j,k,m)
end do
end
EOF

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "$F90"                                 |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
ifort -o $EXE -CB -traceback -fpe0 $F90 2>&1 |tee -a $LOG
if [ $? -eq 0 ]; then
$EXE
if [ $? -ne 0 ]; then
echo
echo ERROR in $EXE
exit 1
fi
fi
ls -lh --time-style=long-iso $ASCOUT |tee -a $LOG



cat <<EOF>$PY
# -*- coding: shift_jis -*-
"""
https://qiita.com/inashiro/items/c59e31b0f0557a7a8bca
"""

import numpy as np
import matplotlib.pyplot as plt

import subprocess
import os
import sys

infle="./$ASCOUT"
ext=".png"
#ext=".eps"

args = ['ls', '-lh', '--time-style=long-iso', infle]
res = subprocess.check_call(args)
print("")


idx, x1, x2, y1, y2  = \
 np.genfromtxt(infle, comments='#', unpack=True)


print("")
print(x1)
print(y1)
print(y2)


fig = plt.figure(figsize=(6, 6))
plt.subplots_adjust(left=0.15, right=0.85, bottom=0.08, top=0.78)

ax = fig.add_subplot(111)

tick =  [1, 0.970, 0.938, 0.903, 0.864, 0.820, 0.770, 0.709, 0.631, 0.581]
label = [1000, 900, 800, 700, 600, 500, 400, 300, 200, 150]


#ax.set(title='${TIME0} ${CLAT}N ${CLON}E')
ax.set(title='${runname} ${domain} ${TIME0} ${CLAT}N ${CLON}E',
       ylabel='P [hPa]', yticks=tick)


ax.set_yticklabels(label)

ax.plot(x1, y1, "-", color="g", label="EPT")
ax.plot(x2, y1, "-", color="r", label="SEPT")


ax.set_xlabel("[K]",fontsize=12)
ax.set_ylabel("P [hPa]",fontsize=12)
ax.set_xlim([335, 370])
ax.set_ylim([1.0, 0.58])
#ax.legend(bbox_to_anchor=(0, -0.1), loc='upper left', borderaxespad=0, fontsize=18)
ax.legend(loc="upper right")



# https://note.nkmk.me/python-grep-like/
with open(infle) as f:
    lines = f.readlines()
lines_strip = [line.strip() for line in lines]
l_XXX = [line for line in lines_strip if 'EPT_LFC' in line]
EPT=float(l_XXX[0].split()[2])
print('EPT=',EPT, ' K')

l_XXX = [line for line in lines_strip if 'LCL_P' in line]
LCL=float(l_XXX[0].split()[2])
print('LCL=',LCL, ' IN EXNER')
LCL_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'LFC_P' in line]
LFC=float(l_XXX[0].split()[2])
print('LFC=',LFC, ' IN EXNER')
LFC_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'LNB_P' in line]
LNB=float(l_XXX[0].split()[2])
print('LNB=',LNB, ' IN EXNER')
LNB_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'CAPE' in line]
CAPE=l_XXX[0].split()[2]
print('CAPE=',CAPE, ' J/kg')

l_XXX = [line for line in lines_strip if 'CIN' in line]
CIN=l_XXX[0].split()[2]
print('CIN=',CIN, ' J/kg')


xlg=360
ylg=0.69
ax.text(xlg, ylg, 'CAPE='+CAPE+" J/kg")
ylg=ylg+0.02
ax.text(xlg, ylg, 'CIN='+CIN+" J/kg")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LCL='+LCL_P+" hPa")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LFC='+LFC_P+" hPa")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LNB='+LNB_P+" hPa")


ax.vlines(EPT, LFC, LNB, colors="k", linestyle="dashed", label="")
#ax.axvline(x=EPT, ymin=LFC, ymax=LNB) #, LNB, LFC, c='skyblue')



# fig内でのaxes座標を取得，戻り値はBbox
ax_pos = ax.get_position()
# fig内座標でテキストを表示 Bboxは Bbox.x0, Bbox.x1, Bbox.y0, Bbox.y1で座標を取得できる

xh=ax_pos.x0+0.0
yh=ax_pos.y1+0.06

fig.text(xh, yh, "${runname} ${domain}")

yh=yh+0.03
fig.text(xh, yh, "${HOST}")

yh=yh+0.03
fig.text(xh, yh, "${CWD}")

yh=yh+0.03
fig.text(xh, yh, "${COMMAND}")

yh=yh+0.03
fig.text(xh, yh, "${TIMESTAMP}")



figfile= os.path.splitext(os.path.basename("$ASCOUT"))[0]+ext

fig.savefig( figfile )
#fig.savefig( figfile, facecolor=fig.get_facecolor())

args = ['ls', '-l', '--time-style=long-iso', figfile]
res = subprocess.check_call(args)
print("")

EOF

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "$PY"                                  |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
chmod u+x $PY
python ./$PY 2>&1 |tee -a $LOG
echo


which srcdump.sh
if [ $? -eq 0 ];then
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "CREATE DOC FILE"                      |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "PT EMAGRAM" >$DOC
echo "=========================">>$DOC
srcdump.sh $LOG $0 $GS $F90 $GMT $PY >> $DOC
echo
fi

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "REMOVE TEMP. FILES"                   |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
rm -fv $GS $F90 $EXE $GMT $PY $BIN1X $BIN2X $BIN1Y $TMPTXT 2>&1  |tee -a $LOG
echo

echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
echo "BACK UP TEMP. FILES"                  |tee -a $LOG
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" |tee -a $LOG
mv -fv $ASCOUT $LOG $TMPDIR 2>&1            |tee -a $LOG
cp -av $0 $TMPDIR 2>&1                      |tee -a $LOG
echo


echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo DOC: $DOC
echo LOG: $TMPDIR/$LOG
echo TXT: $TMPDIR$ASCOUT
echo eps: $fig
echo png: $(basename $ASCOUT .TXT).png
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
echo
exit 0


```

  
#### P.d03.PT_EMAGRAM.GS
```
'cc'

say 'OPEN /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H.ctl'
'open /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H.ctl'
say

say 'OPEN /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H.ctl'
'open /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H.ctl'
say

'set xlopts 1 3 0.12'
'set ylopts 1 3 0.12'


xmax=1
ymax=2

ytop=9

xwid=3.0/xmax
ywid=6./ymax
xmargin=1
ymargin=1


ymap=1

say
say 'MAP EPT'
say

nmap=1
xmap=1
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'

'set dfile 2'
'q ctlinfo 2'
say
say EPT
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130 131.25'
'set lat 33 34'
'set lev 0.5'
'color 342 362 2 -kind deepskyblue->lightcyan->wheat->orange->crimson -gxout shaded'

'set xlint 0.5'
'set ylint 0.5'

'set mpdset hires'
'd EPT(time=09Z05JUL2017)'
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

x=xl ;# (xl+xr)/2
y=yt+0.2
'set strsiz 0.1 0.12'
'set string 1 l 3 0'
'draw string 'x' 'y' EPT (0.5km) [K]'
y = y+0.2
'draw string 'x' 'y' 09Z05JUL2017'
y = y+0.2
'draw string 'x' 'y' K17.R27.06.06'

x1=xr+0.2
x2=x1+0.1
y1=yb ;#-0.5
y2=yt ;#+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

'set xlab off'
'set ylab off'


say
say 'MARK'
say
'markplot 130.53 33.4 -c 1 -m 2 -s 0.15' 

#say
#say 'LINE'
#say
#'trackplot 130.53  130.53  -c 1 -l 1 -t 5' 

say
say 'BOX'
say
#'trackplot     -c 1 -l 1 -t 4' 
#'trackplot     -c 1 -l 1 -t 4' 
#'trackplot     -c 1 -l 1 -t 4' 
#'trackplot     -c 1 -l 1 -t 4' 



say
say 'VPR EPT'
say

nmap=1
ymap=2
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'
'set xlab on'
'set ylab on'

'set dfile 1'
'q ctlinfo 1'
say
say EPT
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 990 150'
'q dims'
say result
'set cmark 0'
'set vrange 335 365'

'set xlint 5'
'set ylint 100'
'd EPT(time=09Z05JUL2017)'

'set xlab off'
'set ylab off'
'set cmark 0'
'set vrange 335 365'
'd SEPT(time=09Z05JUL2017)'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.1 0.12'

'set string 1 c 3 90'
x=xl-0.5 
y=(yb+yt)/2
'draw string 'x' 'y' P [hPa]'

x=xl ;# (xl+xr)/2
y=yt+0.2
'set string 1 l 3 0'
'draw string 'x' 'y' EPT SEPT [K] 33.4N 130.53E'
y = y+0.2
'draw string 'x' 'y' 09Z05JUL2017'

'set xlab off'
'set ylab off'

# Header
'set strsiz 0.1 0.12'
'set string 1 l 2'
'draw string 0.5 10.2 Wed, 27 May 2020 13:12:12 +0900 calypso.bosai.go.jp'
'draw string 0.5 10 /work05/manda/WRF.POST/K17/GRADS.2004/P.d03.PT_EMAGRAM'
'draw string 0.5 9.8 ./P.d03.PT_EMAGRAM.sh K17.R27.06.06 09Z05JUL2017 130.53 33.4'



say
say 'VPR TK AND TD'
say

nmap=3
xmap=2
ymap=2
xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

'set vpage 0.0 8.5 0.0 11.'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set grid off'
'set xlab on'
'set ylab on'

'set dfile 1'
'q ctlinfo 1'
say
say TK
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 990 700'
'q dims'
say result
'set cmark 0'
'set vrange 280 300'

'set xlint 5'
'set ylint 50'
'd tk(time=09Z05JUL2017)'

'set xlab off'
'set ylab off'
'set cmark 0'
'set vrange 280 300'
'd td(time=09Z05JUL2017)+273.15'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)

'set strsiz 0.1 0.12'
'set string 1 c 3 90'
x=xl-0.5 
y=(yb+yt)/2
'draw string 'x' 'y' P [hPa]'

x=xl ;# (xl+xr)/2
y=yt+0.2
'set string 1 l 3 0'
'draw string 'x' 'y' T Td [K] 33.4N 130.53E'
y = y+0.2
'draw string 'x' 'y' 09Z05JUL2017'



'gxprint P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017_SUB.eps'



say
say 'PRINT HEADER OF P.d03.PT_EMAGRAM_TMP.TXT'
say
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','# K17.R27.06.06',append)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','# d03',append)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','# 09Z05JUL2017',append)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','# 130.53',append)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','# 33.4',append)


say
say TEXTOUT
say 'EPT'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 0.5'
'd EPT'
line=sublin(result,2)
x=subwrd(line,4)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','LFC EPT= 'x,append)

say
say 'LCL in hPa'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say

'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 0.5'
'd LCL3P'
line=sublin(result,2)
LCL3P=subwrd(line,4)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','LCL PRS= 'LCL3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LCL : INIT Z=0.5 km EPT=' x ' P='LCL3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'


'd LFC3P'
line=sublin(result,2)
LFC3P=subwrd(line,4)

ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','LFC PRS= 'LFC3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LFC : INIT Z=0.5 km EPT=' x ' P='LFC3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'



say
say 'EL in hPa'
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 0.5'

'd EL3P'
line=sublin(result,2)
EL3P=subwrd(line,4)

ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','LNB PRS= 'EL3P,append)

say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'LNB : INIT Z=0.5 km EPT=' x ' P='EL3P
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say

say
say 'CAPE & CIN'
say
'set dfile 2'
'q ctlinfo 2'
say
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 0.5'

'd cape'
line=sublin(result,2)
CAPEP=subwrd(line,4)


'd cin'
line=sublin(result,2)
CINP=subwrd(line,4)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','CAPE= 'CAPEP,append)
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','CIN=  'CINP,append)
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'CAPE : INIT Z=0.5 km CAPE='CAPEP
say 'CIN  : INIT Z=0.5 km CIN ='CINP
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say


say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'BINARY OUT'
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
'set dfile 1'
'q ctlinfo 1'
say
say sublin(result,1)
say
'set time 09Z05JUL2017'
'set lon 130.53'
'set lat 33.4' ;# '
'set lev 990 150'
'q dims'
say result
line=sublin(result,4)

k1=subwrd(line,11)
k2=subwrd(line,13)
nlev=k2-k1+1
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
say 'nlev='nlev
say '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
ret = write ('P.d03.PT_EMAGRAM_TMP.TXT','nlev= 'nlev,append)

'set gxout fwrite'
'set fwrite -le EPT.bin'
'd EPT.1(time=09Z05JUL2017)'
'disable fwrite'
'!ls -lh --time-style=long-iso EPT.bin'

'set gxout fwrite'
'set fwrite -le SEPT.bin'
'd SEPT.1(time=09Z05JUL2017)'
'disable fwrite'
'!ls -lh --time-style=long-iso SEPT.bin'


'set gxout fwrite'
'set fwrite -le PRESSURE.bin'
'd pressure.1(time=09Z05JUL2017)'
'disable fwrite'
'!ls -lh --time-style=long-iso PRESSURE.bin'



'quit'
```

  
#### P.d03.PT_EMAGRAM.f90
```fortran
INTEGER, PARAMETER :: nlon=1, nlat=1, ntim=1
integer nlev
REAL,allocatable,dimension(:,:,:,:) :: X1,X2,Y
real,allocatable,dimension(:)::exner
real,parameter::kappa=287.0/1004.0
real,parameter::p0=1000.
character(len=200)::STRM
real LON, LAT,EPT_LFC,LCL_P,LFC_P,LNB_P,LCL_EXNER,LFC_EXNER,LNB_EXNER
OPEN(11,FILE="P.d03.PT_EMAGRAM_TMP.TXT",action="read")
read(11,*)
read(11,*)
read(11,*)
read(11,'(A)')STRM
read(STRM(3:),*)LON
read(11,'(A)')STRM
read(STRM(3:),*)LAT
read(11,'(A)')STRM
read(STRM(10:),*)EPT_LFC
read(11,'(A)')STRM
read(STRM(10:),*)LCL_P
read(11,'(A)')STRM
read(STRM(10:),*)LFC_P
read(11,'(A)')STRM
read(STRM(10:),*)LNB_P
read(11,'(A)')STRM
read(STRM(7:),*)CAPE
read(11,'(A)')STRM
read(STRM(7:),*)CIN
read(11,'(A)')STRM
read(STRM(7:),*)nlev
CLOSE(11)

allocate(X1(nlon, nlat, nlev, ntim),X2(nlon, nlat, nlev, ntim),&
Y(nlon, nlat, nlev, ntim))
allocate(exner(nlev))

OPEN(11, FILE="EPT.bin", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) X1
CLOSE(11)

OPEN(11, FILE="SEPT.bin", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) X2
CLOSE(11)

OPEN(11, FILE="PRESSURE.bin", ACCESS="direct", FORM="unformatted", &
RECL=4*nlon*nlat*nlev*ntim,action="read")
READ(11, REC=1) Y
CLOSE(11)

LCL_EXNER=(LCL_P/p0)**kappa
LFC_EXNER=(LFC_P/p0)**kappa
LNB_EXNER=(LNB_P/p0)**kappa

OPEN(21, FILE="P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.TXT")
write(21,'(A)')'# Wed, 27 May 2020 13:12:13 +0900'
write(21,'(A)')'# /work05/manda/WRF.POST/K17/GRADS.2004/P.d03.PT_EMAGRAM'
write(21,'(A)')'# ./P.d03.PT_EMAGRAM.sh K17.R27.06.06 09Z05JUL2017 130.53 33.4'
write(21,'(A)')'# '
write(21,'(A)')'# /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.basic_p.01H.ctl'
write(21,'(A)')'# /work06/manda/ARWpost_K17_work06/ARWpost_K17.R27.06.06/K17.R27.06.06.d03.cape3_z.01H.ctl'
write(21,'(A)')'# '
write(21,'(A)')'# TIME= 09Z05JUL2017'
write(21,'(A)')'# LON= 130.53'
write(21,'(A)')'# LAT= 33.4'
write(21,'(A,f10.3)')'# EPT_LFC= ',EPT_LFC
write(21,'(A,f10.3, i5)')'# LCL_P= ',LCL_EXNER, int(LCL_P)
write(21,'(A,f10.3, i5)')'# LFC_P= ',LFC_EXNER, int(LFC_P)
write(21,'(A,f10.3, i5)')'# LNB_P= ',LNB_EXNER, int(LNB_P)
write(21,'(A,i7)')'# CAPE= ',int(CAPE)
write(21,'(A,i7)')'# CIN=  ',int(CIN)

write(21,'(A)')&
'#   IDX   EPT       SEPT        EXNER   P      '

i=1
j=1
m=1
do k=1,nlev
exner(k)=(Y(i,j,k,m)/p0)**kappa
print '(i5,4f10.3)', k,exner(k),X1(i,j,k,m),X2(i,j,k,m),Y(i,j,k,m)
write(21,'(i5,4f10.3)') k,X1(i,j,k,m),X2(i,j,k,m),exner(k),Y(i,j,k,m)
end do
end
```

  
  
#### P.d03.PT_EMAGRAM.py
```python
# -*- coding: shift_jis -*-
"""
https://qiita.com/inashiro/items/c59e31b0f0557a7a8bca
"""

import numpy as np
import matplotlib.pyplot as plt

import subprocess
import os
import sys

infle="./P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.TXT"
ext=".png"
#ext=".eps"

args = ['ls', '-lh', '--time-style=long-iso', infle]
res = subprocess.check_call(args)
print("")


idx, x1, x2, y1, y2  =  np.genfromtxt(infle, comments='#', unpack=True)


print("")
print(x1)
print(y1)
print(y2)


fig = plt.figure(figsize=(6, 6))
plt.subplots_adjust(left=0.15, right=0.85, bottom=0.08, top=0.78)

ax = fig.add_subplot(111)

tick =  [1, 0.970, 0.938, 0.903, 0.864, 0.820, 0.770, 0.709, 0.631, 0.581]
label = [1000, 900, 800, 700, 600, 500, 400, 300, 200, 150]


#ax.set(title='09Z05JUL2017 33.4N 130.53E')
ax.set(title='K17.R27.06.06 d03 09Z05JUL2017 33.4N 130.53E',
       ylabel='P [hPa]', yticks=tick)


ax.set_yticklabels(label)

ax.plot(x1, y1, "-", color="g", label="EPT")
ax.plot(x2, y1, "-", color="r", label="SEPT")


ax.set_xlabel("[K]",fontsize=12)
ax.set_ylabel("P [hPa]",fontsize=12)
ax.set_xlim([335, 370])
ax.set_ylim([1.0, 0.58])
#ax.legend(bbox_to_anchor=(0, -0.1), loc='upper left', borderaxespad=0, fontsize=18)
ax.legend(loc="upper right")



# https://note.nkmk.me/python-grep-like/
with open(infle) as f:
    lines = f.readlines()
lines_strip = [line.strip() for line in lines]
l_XXX = [line for line in lines_strip if 'EPT_LFC' in line]
EPT=float(l_XXX[0].split()[2])
print('EPT=',EPT, ' K')

l_XXX = [line for line in lines_strip if 'LCL_P' in line]
LCL=float(l_XXX[0].split()[2])
print('LCL=',LCL, ' IN EXNER')
LCL_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'LFC_P' in line]
LFC=float(l_XXX[0].split()[2])
print('LFC=',LFC, ' IN EXNER')
LFC_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'LNB_P' in line]
LNB=float(l_XXX[0].split()[2])
print('LNB=',LNB, ' IN EXNER')
LNB_P=l_XXX[0].split()[3]

l_XXX = [line for line in lines_strip if 'CAPE' in line]
CAPE=l_XXX[0].split()[2]
print('CAPE=',CAPE, ' J/kg')

l_XXX = [line for line in lines_strip if 'CIN' in line]
CIN=l_XXX[0].split()[2]
print('CIN=',CIN, ' J/kg')


xlg=360
ylg=0.69
ax.text(xlg, ylg, 'CAPE='+CAPE+" J/kg")
ylg=ylg+0.02
ax.text(xlg, ylg, 'CIN='+CIN+" J/kg")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LCL='+LCL_P+" hPa")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LFC='+LFC_P+" hPa")
ylg=ylg+0.02
ax.text(xlg, ylg, 'LNB='+LNB_P+" hPa")


ax.vlines(EPT, LFC, LNB, colors="k", linestyle="dashed", label="")
#ax.axvline(x=EPT, ymin=LFC, ymax=LNB) #, LNB, LFC, c='skyblue')



# fig内でのaxes座標を取得，戻り値はBbox
ax_pos = ax.get_position()
# fig内座標でテキストを表示 Bboxは Bbox.x0, Bbox.x1, Bbox.y0, Bbox.y1で座標を取得できる

xh=ax_pos.x0+0.0
yh=ax_pos.y1+0.06

fig.text(xh, yh, "K17.R27.06.06 d03")

yh=yh+0.03
fig.text(xh, yh, "calypso.bosai.go.jp")

yh=yh+0.03
fig.text(xh, yh, "/work05/manda/WRF.POST/K17/GRADS.2004/P.d03.PT_EMAGRAM")

yh=yh+0.03
fig.text(xh, yh, "./P.d03.PT_EMAGRAM.sh K17.R27.06.06 09Z05JUL2017 130.53 33.4")

yh=yh+0.03
fig.text(xh, yh, "Wed, 27 May 2020 13:12:12 +0900")



figfile= os.path.splitext(os.path.basename("P.d03.PT_EMAGRAM_K17.R27.06.06.d03_130.53_33.4_09Z05JUL2017.TXT"))[0]+ext

fig.savefig( figfile )
#fig.savefig( figfile, facecolor=fig.get_facecolor())

args = ['ls', '-l', '--time-style=long-iso', figfile]
res = subprocess.check_call(args)
print("")

```

  

#!/bin/bash

runnames="\
K17.R27.00.06 \
"
#runnames="\
#K17.R27.00.00 \
#K17.R27.00.01 \
#K17.R27.06.01 \
#K17.R27.06.06 \
#"
CLON=130.53
CLAT=33.4

#LIST_TIME="\
#03Z05JUL2017 \
#"
LIST_TIME="\
03Z05JUL2017 \
"
#00Z05JUL2017 \
#06Z05JUL2017 \
#09Z05JUL2017 \

EXE=$(basename $0 .RUN.sh).sh

for runname in $runnames; do
for TIME in $LIST_TIME; do
$EXE $runname $TIME $CLON $CLAT
done
done


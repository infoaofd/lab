# -*- coding: shift_jis -*-
"""
https://qiita.com/inashiro/items/c59e31b0f0557a7a8bca
"""

import numpy as np
import matplotlib.pyplot as plt

import subprocess
import os
import sys

TIME="06Z05JUL2017"
RUNNAME1="K17.R27.00.00"
RUNNAME2="K17.R27.06.01"
LON="130.52"
LAT="33.3"
DOMAIN="d03"

infle1="./P.d03.EPT.VPRO_"+RUNNAME1+"."+DOMAIN+"_"+TIME+"_"+LON+"_"+LAT+".TXT"
infle2="./P.d03.EPT.VPRO_"+RUNNAME2+"."+DOMAIN+"_"+TIME+"_"+LON+"_"+LAT+".TXT"

args = ['ls', '-lh', '--time-style=long-iso', infle1]
res = subprocess.check_call(args)
print("")
args = ['ls', '-lh', '--time-style=long-iso', infle2]
res = subprocess.check_call(args)
print("")


idx, x1, x2, y1, y2  =  np.genfromtxt(infle1, comments='#', unpack=True)
idx, xx1, xx2, yy1, yy2  =  np.genfromtxt(infle2, comments='#', unpack=True)


print("")
print(x1)
print(y1)
print(y2)


fig = plt.figure(figsize=(6, 6))
ax = fig.add_subplot(111)

tick =  [1, 0.970, 0.938, 0.903, 0.864, 0.820, 0.770, 0.709, 0.631, 0.581]
label = [1000, 900, 800, 700, 600, 500, 400, 300, 200, 150]

ax.set(title=RUNNAME1+" "+RUNNAME2+" "+TIME+' '+LAT+'N'+' '+LON+'E',
       ylabel='P [hPa]', yticks=tick)

ax.set_yticklabels(label)

ax.set_xlabel("[K]")
ax.set_xlim([335, 370])
ax.set_ylim([1.0, 0.58])

ax.legend(loc="upper left")

ax.plot(x1, y1, "--", color="r", label="00.00")
ax.plot(x2, y1, "-", color="r", label="SEPT")

ax.plot(xx1, yy1, "--", color="g", label="06.01")
ax.plot(xx2, yy1, "-", color="g", label="06.01")


# https://note.nkmk.me/python-grep-like/
with open(infle1) as f:
    lines = f.readlines()
lines_strip = [line.strip() for line in lines]
l_XXX = [line for line in lines_strip if 'EPT_LFC' in line]
EPT=float(l_XXX[0].split()[2])
print('EPT=',EPT)

l_XXX = [line for line in lines_strip if 'LFC_P' in line]
LFC=float(l_XXX[0].split()[2])
print('LFC=',LFC)

l_XXX = [line for line in lines_strip if 'LNB_P' in line]
LNB=float(l_XXX[0].split()[2])
print('LNB=',LNB)

print(type(LNB))

ax.set_xlim([335, 370])
ax.set_ylim([1.0, 0.58])

ax.vlines(EPT, LFC, LNB, colors="r", linestyle="solid", label="")



with open(infle2) as f2:
    lines = f2.readlines()
lines_strip = [line.strip() for line in lines]
l_XXX = [line for line in lines_strip if 'EPT_LFC' in line]
EPT=float(l_XXX[0].split()[2])
print('EPT=',EPT)

l_XXX = [line for line in lines_strip if 'LFC_P' in line]
LFC=float(l_XXX[0].split()[2])
print('LFC=',LFC)

l_XXX = [line for line in lines_strip if 'LNB_P' in line]
LNB=float(l_XXX[0].split()[2])
print('LNB=',LNB)

print(type(LNB))

ax.set_xlim([335, 370])
ax.set_ylim([1.0, 0.58])

ax.vlines(EPT, LFC, LNB, colors="g", linestyle="solid", label="")





#figfile= os.path.splitext(os.path.basename("P.d03.EPT.VPRO_K17.R27.06.01.d03_"+TIME+"_130.52_33.3.TXT"))[0]+".png"
#figfile="TWO.png"
figfile=RUNNAME1+"_"+RUNNAME2+"_"+TIME+"_"+LON+"_"+LAT+".png"

fig.savefig( figfile, facecolor=fig.get_facecolor())

args = ['ls', '-l', '--time-style=long-iso', figfile]
res = subprocess.check_call(args)
print("")

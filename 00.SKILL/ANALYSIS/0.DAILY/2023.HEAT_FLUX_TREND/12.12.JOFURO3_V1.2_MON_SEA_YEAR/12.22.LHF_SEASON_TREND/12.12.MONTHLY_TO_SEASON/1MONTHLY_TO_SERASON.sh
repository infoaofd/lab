

VAR=LHF 

INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/LHF/MON/LHF.HOURS.SINCE.1800-01-01
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INLIST=$(ls $INDIR/J-OFURO3_*${VAR}*MON_????.nc)

TMP1=$(basename $0 .sh)_tmp1.nc
if [ -f $TMP1 ]; then rm -vf $TMP1; fi
cdo cat $INLIST $TMP1
if [ $? -ne 0 ];then echo ERROR: cdo cat;exit 1;fi



ODIR=$INDIR
SLIST="DJF MAM JJA SON"
YS=1989; YE=2021

for SEASON in $SLIST; do

OFLE=$ODIR/J-OFURO3_${VAR}_${SEASON}_${YS}-${YE}.nc
if [ -f $OFLE ]; then rm -vf $OFLE; fi

TMP2=$(basename $0 .sh)_tmp2.nc
if [ -f $TMP2 ]; then rm -vf $TMP2; fi

cdo -seasmean -select,season=${SEASON} ${TMP1} ${TMP2}
if [ $? -ne 0 ];then echo ERROR $TMP2; exit 1;fi

cdo selyear,${YS}/${YE} ${TMP2} ${OFLE}
if [ $? -ne 0 ];then echo ERROR $OFLE; exit 1;fi
#cp -av ${TMP2} ${OFLE}

if [ -f $OFLE ];then echo "MMMMM OUT:$OFLE"; fi

rm -v $TMP2; echo
done #SEASON

#rm -v $TMP1

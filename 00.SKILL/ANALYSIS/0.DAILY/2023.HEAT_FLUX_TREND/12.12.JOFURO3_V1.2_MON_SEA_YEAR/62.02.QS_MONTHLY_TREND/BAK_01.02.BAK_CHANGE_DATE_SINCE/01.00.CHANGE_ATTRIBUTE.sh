#/bin/bash

YS=1988
YE=2017

INDIR=/work01/DATA/JOFURO3/J-OFURO3/V1.1/MONTHLY/HR/LHF/
ODIR=/work01/DATA/JOFURO3/J-OFURO3/V1.1/MONTHLY/HR/LHF.BUG.FIX.DAYS.SINCE/
mkdir -vp $ODIR

YYYY=$YS
while [ $YYYY -le $YE ]; do

INFLE=$INDIR/J-OFURO3_LHF_V1.1_MONTHLY_HR_${YYYY}.nc
OFLE=$ODIR/J-OFURO3_LHF_V1.1_MONTHLY_HR_${YYYY}.nc

STR1="days since 2017-01-01"
STR2="days since ${YYYY}-01-01"

ncdump $INFLE  | sed -e "s/${STR1}/${STR2}/g" | ncgen -o $OFLE

echo
if [ -f $OFLE ]; then
echo $OFLE
fi
echo
ls -lh $OFLE
echo

YYYY=$(expr $YYYY + 1)
done


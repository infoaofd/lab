
Variable: f
Type: file
filename:	J-OFURO3_LHF_V1.1_MONTHLY_HR_2017
path:	/work01/DATA/JOFURO3/J-OFURO3/V1.1/MONTHLY/HR/LHF/J-OFURO3_LHF_V1.1_MONTHLY_HR_2017.nc
   file global attributes:
      name : J-OFURO3
      title : J-OFURO3 satellite-derived air-sea flux data (V1.1)
      institution : Institute for Space-Earth Environmental Research (ISEE), Nagoya University
      source : Multiple satellite data were used.
      history : Fortran90 netCDF library 2020-10-30
      reference : Tomita et al. 2019, https://doi.org/10.1007/s10872-018-0493-x
      Conversions : CF-1.7
      doi : 10.20783/DIAS.612
      comment : - J-OFURO web site: https://j-ofuro.isee.nagoya-u.ac.jp 
 - This is minor updated data set of J-OFURO3 V1.0.
 - Flux sign: positive upward (i.e. positive value means surface heat flux from ocean to atmosphere)
   dimensions:
      latitude = 720
      longitude = 1440
      time = 12
   variables:
      float latitude ( latitude )
         units :	degrees_north

      float longitude ( longitude )
         units :	degrees_east

      float time ( time )
         units :	days since 2017-01-01

      float LHF ( time, latitude, longitude )
         units :	W/m^2
         long_name :	Surface latent heat flux
         missing_value :	-9999
         _FillValue :	-9999


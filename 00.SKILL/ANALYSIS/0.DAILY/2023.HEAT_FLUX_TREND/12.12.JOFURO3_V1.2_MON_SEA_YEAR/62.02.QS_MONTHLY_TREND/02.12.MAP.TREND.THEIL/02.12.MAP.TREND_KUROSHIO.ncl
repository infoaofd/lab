begin
ABB="QS"
cmin=-3.
cmax=3.
cint=0.2
scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
arg    = getenv("NCL_ARG_2")

M=toint(arg)
MM=sprinti("%0.2i", M)
idx=M-1

MMM=(/"","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP", \
                    "OCT","NOV","DEC"/)
DSET="J-OFURO3"

lonw = 105.
lone = 160
lats = -10.
latn = 50.

prefix="J-OFURO3."+ABB+".TREND_KUROSHIO_"+MM

indir="/work01/DATA/J-OFURO3/V1.2_PRE/"+ABB+".HOURS.SINCE.1800-01-01"

FLIST1=indir+"/"+"J-OFURO3_"+ABB+"_V1.4.1_MON_19??.nc "
FLIST2=indir+"/"+"J-OFURO3_"+ABB+"_V1.4.1_MON_200?.nc "
FLIST3=indir+"/"+"J-OFURO3_"+ABB+"_V1.4.1_MON_201?.nc "
FLIST4=indir+"/"+"J-OFURO3_"+ABB+"_V1.4.1_MON_202?.nc "


FLIST=FLIST1+FLIST2+FLIST3+FLIST4

;print(FLIST)
f=systemfunc("ls "+FLIST)
;print(f)

a=addfiles(f,"r")
;print(a)


print("MMMMM READ DATA")
ListSetType (a, "cat") 
xMON=a[:]->QS


lat=a[0]->latitude ;g0_lat_1
lon=a[0]->longitude ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a[:]->time ; initial_time0_hours

;print(TIME)


print("MMMMM REORDER DATA")

ts = xMON(latitude|:, longitude|: ,time|idx::12)
ts!0="lat"
ts!1="lon"
ts!2="time"
ts&lat=lat
ts&lon=lon
ts&time=TIME(idx::12)


utc_date = ut_calendar(ts&time, 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
date_str = sprinti("%0.2i ", day) + \
         MMM(month) + " "  + sprinti("%0.4i", year)
 
print(date_str) 

;print(year)
time=year
dim=dimsizes(year)
nt=dim(0)
y1=year(0)
y2=year(nt-1)


printVarSummary(ts)

print("MMMMM REGRESSION")
trend           = regCoef(time,ts)
trend@units     = ts@units ;+"/decade"    
trend@long_name = "regression coefficient (trend)"
copy_VarCoords(ts(:,:,0), trend)                ; copy lat,lon coords
printVarSummary(trend)



print("MMMMM JUDGE SIGNIFICANCE")
;https://ccsr.aori.u-tokyo.ac.jp/~masakazu/memo/ncl/trend_signif.ncl
siglvl = 0.1 ;0.05
sighalf=siglvl/2.0
sigpct=siglvl*100
print("p-value (<"+siglvl+ "->significant at "+sigpct+"% level of falsely rejecting the null hypothesis, i.e., rcoef=0")
;;; t検定
  rstd    = reshape(trend@rstd,dimsizes(trend))   ; trend@rstdは1次元配列なのでtrendと同じ形に変形
  dof     = new(dimsizes(trend),integer)          ; 自由度の配列
  dof     = nt-2                      ; ここでは簡単のためどこでも年数とした。もちろん実際の解析では検討が必要。
  print(dof(0,0))
  cdl     = cdft_p(trend/rstd,dof)                ; t値(trend/rstd)からt分布の片側確率を計算
  cdl     = mask(cdl,ismissing(trend),False)      ; 陸地をマスキング
;;; cdl には，t分布を-∞から各地点のt値まで積分して得られる確率(0から1まで)が入る
cdl!0="lat"
cdl!1="lon"
cdl&lat=lat
cdl&lon=lon
printVarSummary(cdl)

trend=trend*tofloat(nt-1) ;単位を変換


print("PLOT")
FIG=prefix+"_"+y1+"-"+y2+"_sig"+sigpct
TYP="PDF"

wks = gsn_open_wks(TYP, FIG)


;gsn_define_colormap(wks,"BlueDarkRed18") ;ncl_default")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=DSET+" "+ABB
opt@gsnCenterString=""
opt@gsnRightString=MMM(M)+" "+tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@cnLevelSelectionMode = "ManualLevels"
opt@cnMinLevelValF = cmin
opt@cnMaxLevelValF = cmax
opt@cnLevelSpacingF = cint
opt@pmLabelBarHeightF = 0.08
opt@lbLabelFontHeightF=0.02
opt@pmLabelBarOrthogonalPosF = .10

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"

res@mpDataBaseVersion = "LowRes" ;-- better map resolution
;res@mpCenterLonF = 180
res@mpMinLonF = 105. ;-- min longitude
res@mpMaxLonF = 170. ;-- max longitude
res@mpMinLatF = -10. ;-- min latitude
res@mpMaxLatF =  50. ;-- max latitude

plot=gsn_csm_contour_map(wks,trend,res)



print("PLOT STATISTICAL SIGNIFICANCE")
  res2                 = True          ; 有意性のためのres
  res2@gsnDraw         = False         ; plotを描かない
  res2@gsnFrame        = False         ; WorkStationを更新しない
  ;;; あとでShadeLtGtContourを用いるため，あらゆるものをFalseにしておく
  res2@cnLinesOn       = False
  res2@cnLineLabelsOn  = False
  res2@cnFillOn        = False
  res2@cnInfoLabelOn   = False
  ;;; ShadeLtGtContourのために，等値線を念のため指定しておく
  res2@cnLevelSelectionMode = "ExplicitLevels"
  res2@cnLevels        = (/0.04,0.05,0.95,0.96/)
res2@cnFillScaleF   = 0.5       ;; シェード（パターン）の密度
res2@cnFillDotSizeF = 0.002      ;; 点描の際の点の大きさ

  dum  = gsn_csm_contour(wks,cdl,res2)           ; とりあえずcdlを描く
  dum  = ShadeLtGtContour(dum,0.05,17,0.95,17)
      ;;; 有意な地点に点々(17番)
      ;;; これは有意水準10％の両側検定に対応

  overlay(plot,dum)  ; 有意性を示したdumをplotに重ねる
  
  draw(plot)         ; ここでplotを描く
  frame(wks)         ; WorkStationの更新



txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

print("FIG: "+FIG+"."+TYP)
end

warning:NclGRIB: Unrecognized parameter table (center 98, subcenter 0, table 235), defaulting to NCEP operational table: variable names and units may be incorrect

Variable: f
Type: file
filename:	ERA5_EASIA_sea_surface_temperature_1982
path:	/work01/DATA/ERA5/EASIA/MON/SFC/LHF/ERA5_EASIA_sea_surface_temperature_1982.grib
   file global attributes:
   dimensions:
      initial_time0_hours = 12
      g0_lat_1 = 201
      g0_lon_2 = 201
   variables:
      float V_GRD_GDS0_SFC_S130 ( initial_time0_hours, g0_lat_1, g0_lon_2 )
 
            V_GRD_GDS0_SFC_S130

        center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	v-component of wind
         units :	m/s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	235
         parameter_number :	34
         forecast_time :	0
         forecast_time_units :	hours
         statistical_process_descriptor :	Average of forecast averages, forecasts at 24-hour intervals.
         statistical_process_duration :	12 hours (beginning at reference time at intervals of 24 hours)
         N :	<ARRAY of 12 elements>

      double initial_time0_hours ( initial_time0_hours )
         long_name :	initial time
         units :	hours since 1800-01-01 00:00

      double initial_time0_encoded ( initial_time0_hours )
         long_name :	initial time encoded as double
         units :	yyyymmddhh.hh_frac

      float g0_lat_1 ( g0_lat_1 )
         long_name :	latitude
         GridType :	Cylindrical Equidistant Projection Grid
         units :	degrees_north
         Dj :	0.25
         Di :	0.25
         Lo2 :	145
         La2 :	 0
         Lo1 :	95
         La1 :	50

      float g0_lon_2 ( g0_lon_2 )
         long_name :	longitude
         GridType :	Cylindrical Equidistant Projection Grid
         units :	degrees_east
         Dj :	0.25
         Di :	0.25
         Lo2 :	145
         La2 :	 0
         Lo1 :	95
         La1 :	50

      string initial_time0 ( initial_time0_hours )
         long_name :	Initial time of first record
         units :	mm/dd/yyyy (hh:mm)


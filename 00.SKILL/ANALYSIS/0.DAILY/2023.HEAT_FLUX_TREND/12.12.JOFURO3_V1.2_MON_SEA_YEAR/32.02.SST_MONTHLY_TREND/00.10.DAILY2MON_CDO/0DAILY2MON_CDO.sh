
VAR=SST

YS=1988; YE=2022
#YS=1989; YE=1989
Y=$YS

while [ $Y -le $YE ];do
INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/${VAR}
INFLE=J-OFURO_EM_${VAR}_V0.5.1_DAILY_${Y}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=$INDIR
OUT=$ODIR/$(basename $INFLE _DAILY_${Y}.nc)_MON_${Y}.nc

rm -vf $OUT
cdo monmean $IN $OUT

if [ -f $OUT ];then
echo MMMMM OUTPUT: $OUT
else
echo EEEEE NO SUCH FILE: $OUT
fi

Y=$(expr $Y + 1)
done #Y


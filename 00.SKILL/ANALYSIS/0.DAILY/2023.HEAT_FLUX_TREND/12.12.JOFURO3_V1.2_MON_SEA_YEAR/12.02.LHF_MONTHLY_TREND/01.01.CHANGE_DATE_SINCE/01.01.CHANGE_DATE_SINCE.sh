#/bin/bash

#YS=1988
YS=1999
YE=2022

INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/LHF
ODIR=/work01/DATA/J-OFURO3/V1.2_PRE/LHF.HOURS.SINCE.1800-01-01/
mkdir -vp $ODIR

YYYY=$YS
while [ $YYYY -le $YE ]; do

INFLE=$INDIR/J-OFURO3_LHF_HR_V1.4.1_MON_${YYYY}.nc
OFLE=$ODIR/J-OFURO3_LHF_V1.4.1_MON_${YYYY}.nc

STR1="days since ${yyyy}-01-01"
STR1="hours since 1800-01-01 00:00"

ncap2 -O -s \
'@units="hours since 1800-01-01 00:00}";time=udunits(time,@units);time@units=@units' \
$INFLE $OFLE


echo
if [ -f $OFLE ]; then
echo $OFLE
fi
echo
ls -lh $OFLE
echo

YYYY=$(expr $YYYY + 1)
done


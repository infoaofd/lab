begin

M=07
MM=sprinti("%0.2i", M)
idx=M-1

MMM=(/"","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP", \
                    "OCT","NOV","DEC"/)
DSET="J-OFURO3"
ABB="LHF"

lonw = 105.
lone = 145.
lats = 0.
latn = 50.

prefix="J-OFURO3.LHF.TREND."+MM


indir="/work01/DATA/J-OFURO3/V1.2_PRE/LHF/MON/LHF.HOURS.SINCE.1800-01-01"

FLIST1=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_19??.nc "
FLIST2=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_200?.nc "
FLIST3=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_201?.nc "
FLIST4=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_202?.nc "


FLIST=FLIST1+FLIST2+FLIST3 ;+FLIST4

;print(FLIST)
f=systemfunc("ls "+FLIST)
;print(f)

a=addfiles(f,"r")
;print(a)


print("### READ DATA")
ListSetType (a, "cat") 
xMON=a[:]->LHF
;xMON=-xMON ;POS UPWARD


lat=a[0]->latitude ;g0_lat_1
lon=a[0]->longitude ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a[:]->time ; initial_time0_hours

;print(TIME)


print("### REORDER DATA")

ts = xMON(latitude|:, longitude|: ,time|idx::12)
ts!0="lat"
ts!1="lon"
ts!2="time"
ts&lat=lat
ts&lon=lon
ts&time=TIME(idx::12)


utc_date = ut_calendar(ts&time, 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
date_str = sprinti("%0.2i ", day) + \
         MMM(month) + " "  + sprinti("%0.4i", year)
 
print(date_str) 

;print(year)
time=year
dim=dimsizes(year)
nt=dim(0)
y1=year(0)
y2=year(nt-1)


printVarSummary(ts)

print("### REGRESSION")
rc           = regCoef(time,ts)

rc=rc*tofloat(nt-1)
rc@units     = ts@units ;+"/decade"    
rc@long_name = "regression coefficient (trend)"
copy_VarCoords(ts(:,:,0), rc)                ; copy lat,lon coords
printVarSummary(rc)





print("SIGNIFICANCE")
copyatt(rc,ts)  ; copy other atts and cv's
tval = onedtond(rc@tval , dimsizes(rc))
df   = onedtond(rc@nptxy, dimsizes(rc)) - 2
b    = tval
b    = 0.5        ; b must be same size as tval (and df)
prob = betainc(df/(df+tval^2),df/2.0,b)

pval = rc
pval = (/prob/)  ; copy data
pval@long_name = "probability"



print("JUDGE SIGNIFICANCE")
siglvl = 0.05
sig = pval
ndim = dimsizes(pval)
nlat = ndim(0)
nlon = ndim(1)
do j=0,nlat-1
  do i=0,nlon-1
    if (pval(j,i).lt.siglvl) then
      sig(j,i) = 1.
    else
      sig(j,i) = 0.
    end if
  end do ;i
end do   ;j



print("PLOT")
FIG=prefix+"_"+y1+"-"+y2
TYP="png"

wks = gsn_open_wks(TYP, FIG)


;gsn_define_colormap(wks,"BlueDarkRed18") ;ncl_default")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=DSET+" "+ABB
opt@gsnCenterString=""
opt@gsnRightString=MMM(M)+" "+tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@cnLevelSelectionMode = "ManualLevels"
opt@cnMinLevelValF = -40.
opt@cnMaxLevelValF =  40.
opt@cnLevelSpacingF = 5
opt@pmLabelBarHeightF = 0.08
opt@lbLabelFontHeightF=0.02


res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=4
res@mpGeophysicalLineColor="saddlebrown"

res@mpDataBaseVersion = "MediumRes" ;-- better map resolution
res@mpMinLonF = 105. ;-- min longitude
res@mpMaxLonF = 145. ;-- max longitude
res@mpMinLatF =  0. ;-- min latitude
res@mpMaxLatF =  50. ;-- max latitude

plot1=gsn_csm_contour_map(wks,rc,res)




print("PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
; sgres@cnFillScaleF         = 0.6		; add extra density
sgres@cnFillDotSizeF       = 0.003

print("ACTIVATE IF GRAY SHADING FOR B&W PLOT")  
sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
sgres@cnLevels = 0.5	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = ""     ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString = ""    ; upper-left subtitle
sgres@gsnRightString = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,sig,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

overlay(plot1,sig_plot)



print("PLOT REGRESSION CONTOUR")
delete(res)
res=opt
res@cnFillOn     = False   ; turn on color fill
res@cnLinesOn    = True    ; turn off contour lines
res@gsnLeftString=""
res@gsnCenterString=""
res@gsnRightString=""

res@cnLineLabelsOn = True
res@cnLineLabelInterval = 5
res@cnLineLabelFontHeightF = 0.015
res@cnLineThicknessF = 2.0

;plot2=gsn_csm_contour(wks,rc,res)

;overlay(plot1,plot2)

draw(plot1)


txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

end

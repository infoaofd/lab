/work01/DATA/J-OFURO3/V1.2_PRE

$ ls /work01/DATA/J-OFURO3/V1.2_PRE/
0.README.TXT           LHF/  QS/   TA10/  ZIP/
J-OFURO3_V1.2 _PRE.md  QA/   SST/  WND/

$ ls LHF/|head -1
J-OFURO3_LHF_HR_V1.4.1_DAILY_1988.nc

$ ls QS|head -1
J-OFURO3_QS_HR_V1.4.1_DAILY_1988.nc

$ ls TA10|head -1
J-OFURO3_TA10_HR_V1.4.1_DAILY_1988.nc*

$ ls QA|head -1
J-OFURO3_QA_MULTI_V1.6.2_DAILY_1988.nc*

$ ls SST|head -1
J-OFURO_EM_SST_V0.5.1_DAILY_1988.nc

$ ls WND|head -1
J-OFURO3_WND_V0.6.6_DAILY_1988.nc

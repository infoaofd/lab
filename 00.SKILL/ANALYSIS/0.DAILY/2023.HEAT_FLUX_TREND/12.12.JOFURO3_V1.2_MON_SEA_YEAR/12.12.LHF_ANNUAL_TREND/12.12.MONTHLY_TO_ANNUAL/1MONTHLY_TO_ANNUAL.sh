echo MMMMM Assume an input dataset is a monthly mean time series. To compute the yearly mean from the correct
echo MMMMM weighted monthly mean use:
echo MMMMM cdo muldpm ifile tmpfile1
echo MMMMM cdo yearsum tmpfile1 tmpfile2
echo MMMMM cdo divdpy tmpfile2 ofile
echo MMMMM 
echo MMMMM Or all in one command line:
echo MMMMM cdo divdpy -yearsum -muldpm ifile ofile

VAR=LHF 

INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/LHF/MON/LHF.HOURS.SINCE.1800-01-01
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

ODIR=$INDIR
OFLE=$ODIR/J-OFURO3_${VAR}_YEARLY.nc
rm -vf $OFLE

INLIST=$(ls $INDIR/J-OFURO3_*${VAR}*MON_????.nc)
for IN in $INLIST;do
echo INPUT: $IN
done

TMP=$(basename $0 .sh)_tmp.nc
if [ -f $TMP ];then rm -vf $TMP;fi
cdo cat $INLIST $TMP
if [ $? -ne 0 ];then echo ERROR: cdo cat;exit 1;fi

cdo divdpy -yearsum -muldpm $TMP $OFLE
if [ $? -eq 0 ];then echo "OUT:$OFLE"; fi

#rm -v $TMP

   File format : NetCDF
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : unknown  unknown  v instant       1   1   1036800   1  F32  : -1            
   Grid coordinates :
     1 : lonlat                   : points=1036800 (1440x720)
                        longitude : 0.125 to 359.875 by 0.25 degrees_east  circular
                         latitude : -89.875 to 89.875 by 0.25 degrees_north
   Vertical coordinates :
     1 : surface                  : levels=1
   Time coordinate :  12 steps
     RefTime =  1800-01-01 00:00:00  Units = hours  Calendar = standard  Bounds = true
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-01-16 00:00:00  1988-02-15 00:00:00  1988-03-16 00:00:00  1988-04-15 12:00:00
  1988-05-16 00:00:00  1988-06-15 12:00:00  1988-07-16 00:00:00  1988-08-16 00:00:00
  1988-09-15 12:00:00  1988-10-16 00:00:00  1988-11-15 12:00:00  1988-12-16 00:00:00

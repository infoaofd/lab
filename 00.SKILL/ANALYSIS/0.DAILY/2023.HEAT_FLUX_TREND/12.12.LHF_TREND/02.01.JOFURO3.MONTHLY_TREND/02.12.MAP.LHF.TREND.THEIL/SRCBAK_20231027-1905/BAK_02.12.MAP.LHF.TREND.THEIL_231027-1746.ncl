begin

M=07
MM=sprinti("%0.2i", M)
idx=M-1

MMM=(/"","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP", \
                    "OCT","NOV","DEC"/)
DSET="J-OFURO3"
ABB="LHF"
sigpct=5

prefix="J-OFURO3.LHF.TREND_THEIL_sig"+sigpct+"_"+MM
sig1m=tofloat(100-sigpct)/100.0
print("sig1m="+sig1m)

indir="/work01/DATA/J-OFURO3/V1.2_PRE/LHF/MON/LHF.HOURS.SINCE.1800-01-01"

FLIST1=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_19??.nc "
FLIST2=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_200?.nc "
FLIST3=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_201?.nc "
FLIST4=indir+"/"+"J-OFURO3_LHF_V1.4.1_MON_202?.nc "

FLIST=FLIST1+FLIST2+FLIST3+FLIST4

f=systemfunc("ls "+FLIST)

a=addfiles(f,"r")


print("MMMMM READ DATA")
ListSetType (a, "cat") 
xMON=a[:]->LHF
;xMON=-xMON ;POS UPWARD


lat=a[0]->latitude  ;g0_lat_1
lon=a[0]->longitude ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a[:]->time ; initial_time0_hours


print("MMMMM REORDER DATA")
print("MMMMM x(NT,NY,MX): p=trend_manken(x,False,0)=> p(2,NY,MX)")
ts = xMON(time|idx::12,latitude|:, longitude|:)
ts!0="time"
ts!1="lat"
ts!2="lon"
ts&time=TIME(idx::12)
ts&lat=lat
ts&lon=lon


utc_date = ut_calendar(ts&time, 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
date_str = sprinti("%0.2i ", day) + \
         MMM(month) + " "  + sprinti("%0.4i", year)
 
print(date_str) 

time=year
dim=dimsizes(year)
nt=dim(0)
y1=year(0)
y2=year(nt-1)


printVarSummary(ts)
print("MMMMM TREND MANKEN")
print("MMMMM x(NT,NY,MX): p=trend_manken(x,False,0)=> p(2,NY,MX)")
manken=trend_manken(ts,False,0)
print("MMMMM p(2,NY,MX): p(0)=probability, p(1)=trend")
rc=manken(1,:,:)*tofloat(nt)
printVarSummary(rc)

rc@units     = ts@units ;+"/decade"    
rc@long_name = "trend"
rc!0="lat"
rc!1="lon"
rc&lat=lat
rc&lon=lon

print("MMMMM JUDGE SIGNIFICANCE")
;https://ccsr.aori.u-tokyo.ac.jp/~masakazu/memo/ncl/trend_signif.ncl
print(sigpct+"% level of falsely rejecting the null hypothesis, i.e., rcoef=0")

pval = manken(0,:,:) ;p-value
pval!0="lat"
pval!1="lon"
pval&lat=lat
pval&lon=lon

print("MMMMM PLOT")
FIG=prefix+"_"+y1+"-"+y2 ;+"_sig"+sigpct
TYP="PDF"

wks = gsn_open_wks(TYP, FIG)


;gsn_define_colormap(wks,"BlueDarkRed18") ;ncl_default")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=DSET+" "+ABB
opt@gsnCenterString=""
opt@gsnRightString=MMM(M)+" "+tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@cnLevelSelectionMode = "ManualLevels"
opt@cnMinLevelValF = -40.
opt@cnMaxLevelValF =  40.
opt@cnLevelSpacingF = 5
opt@pmLabelBarHeightF = 0.08
opt@lbLabelFontHeightF=0.02
opt@pmLabelBarOrthogonalPosF = .10

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@cnFillDrawOrder = "PreDraw"
res@mpFillOn                     = True     ; 地図を塗りつぶす
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpMinLonF = 105. ;-- min longitude
res@mpMaxLonF = 180. ;-- max longitude
res@mpMinLatF = -10. ;-- min latitude
res@mpMaxLatF =  50. ;-- max latitude

plot1=gsn_csm_contour_map(wks,rc,res)



print("PLOT STATISTICAL SIGNIFICANCE")
res2                 = True          ; 有意性のためのres
res2@gsnDraw         = False         ; plotを描かない
res2@gsnFrame        = False         ; WorkStationを更新しない
;;; あとでShadeLtGtContourを用いるため，あらゆるものをFalseにしておく
res2@cnLinesOn       = False
res2@cnLineLabelsOn  = False
res2@cnFillOn        = False
res2@cnInfoLabelOn   = False
res2@cnFillDrawOrder = "PostDraw"
;;; ShadeGtContourのために，等値線を念のため指定しておく
res2@cnLevelSelectionMode = "ExplicitLevels"
res2@cnLevels        = (/sig1m, 0.96/)

print(pval)
print("sig1m="+sig1m)
dum  = gsn_csm_contour(wks,pval,res2)           ; とりあえずcdlを描く
dum  = ShadeGtContour(dum,sig1m,17)
;;; sig1mよりp-valueが大きい地点に点を打つ
;;; これは有意水準sigpct％の両側検定に対応

overlay(plot1,dum)  ; 有意性を示したdumをplotに重ねるoverlay(plot1,sig_plot)

draw(plot1)

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

print("FIG: "+FIG+"."+TYP)
end

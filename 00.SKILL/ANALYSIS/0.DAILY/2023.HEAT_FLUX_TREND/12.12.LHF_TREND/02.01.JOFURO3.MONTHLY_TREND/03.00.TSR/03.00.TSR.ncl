;https://www.ncl.ucar.edu/Applications/Scripts/regress_1b.ncl
;http://www.stat.ucla.edu/~hqxu/stat105/pdf/ch11.pdf
;https://www.ncl.ucar.edu/Applications/Images/regress_1b_2_lg.png
begin

MM=07
MMm1=MM-1
istart=MM-1
istep=12

DSET="J-OFURO3"
ABB="LHF"
FLUXUP=0 ; 1=> (-1)*INPUT_DATA
UNIT="W/m2"

lonw = 125 ;127.
lone = 126 ;128.
lats = 25 ;27.
latn = 26 ;28.

prefix="J-OFURO3.LHF.TREND."+MM

indir="/work01/DATA/JOFURO3/J-OFURO3/V1.1/MONTHLY/HR/LHF.HOURS.SINCE.1800-01-01"

FLIST1=indir+"/"+"J-OFURO3_LHF_V1.1_MONTHLY_HR_19??.nc "
FLIST2=indir+"/"+"J-OFURO3_LHF_V1.1_MONTHLY_HR_200?.nc "
FLIST3=indir+"/"+"J-OFURO3_LHF_V1.1_MONTHLY_HR_201?.nc "

FLIST=FLIST1+FLIST2+FLIST3

f=systemfunc("ls "+FLIST)

print(f)

dim=dimsizes(f)
;print(dim)

ntim=dim(0)

it=0
a=addfile(f(it),"r")
printVarSummary(a)
xTMP=a->LHF
if(FLUXUP .ne. 0)then
xTMP=-xTMP 
end if

lat=a->latitude
lon=a->longitude

dim2=dimsizes(xTMP)

nlat=dim2(1)
nlon=dim2(2)

start=1988.0
finish=2017.0
npts=30

xMON=new((/npts,nlat,nlon/),typeof(xTMP))
printVarSummary(xMON)
printVarSummary(xTMP)

idx=0

do it=0,npts-1

a=addfile(f(it),"r")

delete(xTMP)

xTMP=a->LHF
if(FLUXUP .ne. 0)then
xTMP=-xTMP 
end if

xMON(it,:,:)=xTMP(MMm1,:,:)

end do


xMON!0="year"

xMON&year=fspan(start, finish, npts)

printVarSummary(xMON)


print("### AAV)")

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

x = xMON&year
y = wgt_areaave_Wrap(xMON(:, {lats:latn}, {lonw:lone}), wgty({lats:latn}), 1.0, 0)

print(y)

print("### CALCULATE THE REGRESSION COEFFICIENT (SLOPE)")
rc =  regline_stats(x,y) ; linear regression coef
;print(rc)

x@long_name = "" 
y@long_name = ABB 

rc    = regline(x, y)          ; slope           
rc@units = "degK/year"         
;print(rc)
pltarry   = new ( (/6,ntim/), typeof(y), y@_FillValue)

pltarry(0,:) = y                                ; use markers
pltarry(1,:) = rc@Yest                           ; regression values

pltarry(2,:) = rc@YMR025                         ; MR: mean response
pltarry(3,:) = rc@YMR975
pltarry(4,:) = rc@YPI025                         ; PI: prediction interval
pltarry(5,:) = rc@YPI975


TYP="png"
FIG=DSET+"."+ABB+".AAV."+"."+lats+"-"+latn+"_"+lonw+"-"+lone
wks  = gsn_open_wks(TYP,FIG)

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      


res                 = opt
res@xyDashPatterns      = (/0,0,1,1,1,1/) 
res@xyLineThicknesses   = (/2,3,2,2,2,2/)        
res@xyLineColors        = (/ "black", "black"\
                           , "black" ,"black" \
                           , "red"  , "red"  /)

res@tiMainString    = DSET+" "+ABB+" "+lats+"N-"+latn+"N "+lonw+"E-"+lone+"E"
res@tiYAxisString   = "["+UNIT+"]"                     
;res@xyLineThicknesses = 3
  
plot  = gsn_csm_xy (wks,x,pltarry(0:3,:),res)           ; create plot
          
draw(plot)




txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

print("")
print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm")
print(FIG+"."+TYP)
print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm")

if(FLUXUP .ne. 0)then
print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm")
print("POS UPWARD (THF ONLY)")
print("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm")
end if
end

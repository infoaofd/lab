#!/bin/bash
EXE=$(basename $0 .sh).ncl
if [ ! -f $EXE ];then echo NO SUCH FILE, $EXE; exit 1;fi

LONW=300; LATS=50
LONW=123; LATS=21
LONW=305; LATS=45

SEALIST="DJF MAM JJA SON"
SEALIST="DJF" # TEST
PRDLIST="1988-2004 2005-2021"
#PRDLIST="1988-2004" #TEST
RUNLIST="WND SST QA"
RUNLIST="RAW" #TEST

for SEA in $SEALIST; do
for PRD in $PRDLIST; do
for RUN in $RUNLIST; do
runncl.sh $EXE $SEA $PRD $RUN $LONW $LATS

done
done
done

exit 0
LONW=300
LATS=50
LONW=305
LATS=45

CLON=str(LONW)
CLAT=str(LATS)
PRD="1998-2004"
PRD="2005-2021"
INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/42.12.HISTOGRAM/12.12.MAKE_HISTO_DATA_1P/"
INFLE="J-OFURO3_COR3_DAILY_LHF_RAW_"+PRD+"_DJF_"+CLON+"_"+CLAT+".nc"
IN=INDIR+INFLE

import os
SCRNAME=os.path.splitext(os.path.basename(__file__))[0]
OUT=SCRNAME+"_J-OFURO3_COR3_DAILY_LHF_RAW_"+PRD+"_DJF_"+CLON+"_"+CLAT+"_LOG.PDF"

import netCDF4  
import numpy as np
import matplotlib.pyplot as plt

nc = netCDF4.Dataset(IN, "r")
x1=nc.variables["x1"][:]
plt.hist(x1, alpha=0.5, bins=20, range=(-400,400), 
color='b', label="LHF", log=True)

plt.savefig(OUT) 
print("")
print("MMMMM INDIR:  "+INDIR)
print("MMMMM INFLE:  "+INFLE)
print("MMMMM OUT: "+OUT)

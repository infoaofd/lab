LONW=123
LATS=21
CLON=str(LONW)
CLAT=str(LATS)

INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/42.12.HISTOGRAM/12.12.MAKE_HISTO_DATA_1P/"
INFLE1="J-OFURO3_COR3_DAILY_LHF_RAW_1988-2004_DJF_"+CLON+"_"+CLAT+".nc"
IN1=INDIR+INFLE1
INFLE2="J-OFURO3_COR3_DAILY_LHF_RAW_2005-2021_DJF_"+CLON+"_"+CLAT+".nc"
IN2=INDIR+INFLE2


import os
SCRNAME=os.path.splitext(os.path.basename(__file__))[0]
OUT=SCRNAME+"_J-OFURO3_COR3_DAILY_LHF_RAW_DJF_"+CLON+"_"+CLAT+".PDF"

import netCDF4  
import numpy as np
import matplotlib.pyplot as plt


nc1 = netCDF4.Dataset(IN1, "r")
LHF1=nc1.variables["x1"][:]
nc2 = netCDF4.Dataset(IN2, "r")
LHF2=nc2.variables["x1"][:]

bmin=-20
bmax=600
plt.hist(LHF1, alpha=0.5, bins=20, range=(bmin,bmax), 
color='b', label="1ST")
# log=True,
plt.hist(LHF2, alpha=0.5, bins=20, range=(bmin,bmax), 
color='r', label="2ND")

plt.savefig(OUT) 
print("")
print("MMMMM INDIR:  "+INDIR)
print("MMMMM INFLE1:  "+INFLE1)
print("MMMMM INFLE2:  "+INFLE2)
print("MMMMM OUT: "+OUT)

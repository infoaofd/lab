;/work09/am/00.WORK/2023.HEAT_FLUX_TREND/42.DIFF_15YEAR_PERIODS/22.12.WELCH_TEST
DSET="J-OFURO3"
ABB="LHF"

SEA = getenv("NCL_ARG_2"); DJF, MAM, JJA, SON
RUN = getenv("NCL_ARG_3"); RAW, WND, SST, QA

PRD1="1988-2004"
PRD2="2005-2021"
PREFIX="J-OFURO3_COR3_DAILY_"+ABB+"_"+RUN+"_"
INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/22.22.DIFF_SENSITIVITY_EXP/LHF/"
INFLE=PREFIX+SEA+".nc"
IN=INDIR+INFLE

scriptname = get_script_prefix_name()

FIGDIR="FIG_"+scriptname
system("mkdir -vp "+FIGDIR)
FIG="DIFF_"+PREFIX+SEA
TYP="PDF"

print("MMMMM READ INPUT DATA")
print("MMMMM INDIR "+INDIR)
print("MMMMM INFLE "+INFLE)
a=addfile(IN,"r")
diff=a->diffXY
diff=-diff ;
print("")

wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"BlueWhiteOrangeRed") ;"NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=DSET+" "+ABB+" "+RUN+" "+SEA
opt@gsnCenterString=""
opt@gsnRightString="("+ PRD2 +")" + "-" + "("+ PRD1 +")"
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.022
opt@gsnCenterStringFontHeightF=0.022
opt@gsnRightStringFontHeightF=0.022
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = -100.
;opt@cnMaxLevelValF =  100.
;opt@cnLevelSpacingF = 10.
opt@cnLevelSelectionMode = "ExplicitLevels"
opt@cnLevels = (/-50,-25,-20,-15,-10,-2.5,0,2.5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)


;opt@pmLabelBarHeightF = 0.41
opt@pmLabelBarWidthF=0.05
;opt@pmLabelBarParallelPosF = 0.48
;opt@lbTopMarginF=-0.01
;opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m~S~2~N~"                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
;opt@lbTitleDirection = "Across"             ; title direction
opt@lbOrientation = "vertical"

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpCenterLonF = 180

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"

plot=gsn_csm_contour_map(wks,diff,res)

draw(plot)         ; ここでplotを描く
frame(wks)         ; WorkStationの更新

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)


print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)

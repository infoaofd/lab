#!/bin/bash

function count_date(){
jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

tday=$( expr $jdend - $jdstart)

echo $start $end $tday
}

start0=1988/01/01
start1=1988/12/01
end1=2005/02/28
start2=2005/12/01
end2=2022/02/28
# 2022	���N	2

echo "FROM       TO         DAYS "
start=$start0; end=$start1; count_date $start $end
start=$start0; end=$end1;   count_date $start $end

start=$start0; end=$start2; count_date $start $end
start=$start0; end=$end2;   count_date $start $end


<<COMMENT
/work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/34.12.SELECT_SEA
$ cdo sinfo /work01/DATA/J-OFURO3/V1.2_PRE/32.JOFURO3_DECOMP_FLUX/34.12.SELECT_SEA/LHF/TMP_J-OFURO3_COR3_LHF_RAW_RUN_MERGE.nc |head -15
   File format : NetCDF4
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : unknown  unknown  v instant       1   1   1036800   1  F32  : -1            
   Grid coordinates :
     1 : lonlat                   : points=1036800 (1440x720)
                              lon : 0.125 to 359.875 by 0.25 degrees_east  circular
                              lat : -89.875 to 89.875 by 0.25 degrees_north
   Vertical coordinates :
     1 : surface                  : levels=1
   Time coordinate :
                             time : 12784 steps
     RefTime =  1800-01-01 00:00:00  Units = hours  Calendar = standard
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-01-01 00:00:00  1988-01-02 00:00:00  1988-01-03 00:00:00  1988-01-04 00:00:00
  1988-01-05 00:00:00  1988-01-06 00:00:00  1988-01-07 00:00:00  1988-01-08 00:00:00
COMMENT

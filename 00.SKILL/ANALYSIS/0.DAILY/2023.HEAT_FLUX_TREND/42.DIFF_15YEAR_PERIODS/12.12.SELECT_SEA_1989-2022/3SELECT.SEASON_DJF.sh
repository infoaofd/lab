
VAR=LHF
SEA=DJF

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/${VAR}
OROOT=/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/12.12.SELECT_SEA_1989-2022/${VAR}

mkd $OROOT

RD=0.README_$(basename $0 .sh).TXT
CWD=$(pwd)

<<COMMENT
   Time coordinate :  3159 steps
     RefTime =  1800-01-01 00:00:00  Units = hours  Calendar = standard
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-01-01 00:00:00  1988-01-02 00:00:00  1988-01-03 00:00:00  1988-01-04 00:00:00
.....
  2022-12-29 00:00:00  2022-12-30 00:00:00  2022-12-31 00:00:00
cdo    sinfo: Processed 1 variable over 3159 timesteps [0.09s 14MB].
COMMENT

#RLIST="RAW" # CLM WND SST QA"
#RLIST="CLM"
RLIST=" WND SST QA"

ODIR=${OROOT}/
mkd $ODIR; echo MMMMM OUTPUT DIR: $ODIR

for RUN in $RLIST; do
echo MMMMM INPUT DIR: $INROOT
INLIST=$(ls $INROOT/${RUN}_RUN/J-OFURO3_COR3_${VAR}_${RUN}_RUN_DAILY_*.nc)

TMP1=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_RUN_MERGE.nc

#echo MMMMM MERGETIME
#rm -vf $TMP1
#cdo mergetime $INLIST $TMP1
if [ -f $TMP1 ];then 
echo;echo MMMM MERGETIME: $TMP1
else
echo;echo EEEEE ;echo EEEEE ERROR MERGETIME: $TMP1;echo EEEEE; echo
exit 1
fi
echo

cat << EOF
MMMMM $ 94.TEST_JULDAY_DJF.sh 
MMMMM FROM       TO         DAYS 
MMMMM 1988/01/01 1988/12/01 335
MMMMM 1988/01/01 2005/02/28 6268
MMMMM 1988/01/01 2005/12/01 6544
MMMMM 1988/01/01 2022/02/28 12477
EOF

YLIST="1988 2005"; DY=16
#YLIST="1988"; DY=16
#YLIST="2005"; DY=16
for Y in $YLIST; do

YS=$Y; DY=16; YE=$(expr $YS + $DY)
if [ $YS -eq 1988 ];then IS=335;  IE=6268;  fi
if [ $YS -eq 2005 ];then IS=6544; IE=12478; fi

echo; echo MMMMM TRIM SELTIMESTEP FROM $YS TO $YE TIMESTEP $IS $IE
TMP2=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_TRIM_${YS}-${YE}.nc
rm -vf $TMP2

cdo seltimestep,${IS}/${IE} $TMP1 $TMP2
if [ -f $TMP2 ];then 
echo;echo MMMM TRIM SELTIMESTEP : $TMP2
else
echo;echo EEEEE ;echo EEEEE:TRIM SELTIMESTEP $TMP2;echo EEEEE; echo
exit 1
fi
echo

OFLE=J-OFURO3_COR3_DAILY_${VAR}_${RUN}_${YS}-${YE}_DJF.nc
OUT=$ODIR/$OFLE
rm -vf $OUT

echo MMMMM SELMON $SEA
TMP3=TMP_$(basename $0 .sh)_${SEA}.nc

if   [ $SEA = "DJF" ];then
cdo -selmon,12,1,2 $TMP2 $OUT
else
echo EEEEE $SEA NOT SUPPORTED.;exit 1
fi # SEA

if [ -f $OUT ];then 
echo MMMM OUT: $OUT
else
echo;echo EEEEE ;echo EEEEE ERROR : $OUT;echo EEEEE; echo
fi # OUT

done # Y

done # RUN


<<COMMENT

cdo mergetime *.nc outfile
should normally work without problems. There is one limitation, 
to sort the time steps mergetime has to open all input files at the same time. 
How many file can be opened depends on the operating system. 
On some system the limit is 256.
You can use cat if your input files are already sorted in time:

https://code.mpimet.mpg.de/boards/1/topics/908

COMMENT

<<COMMENT
I want to calculate the seasonal means between November to March 

Try the operator timselmean. Here is an example for a 10 year monthly mean dataset starting in January:
cdo timselmean,5,10,7 ifile ofile
5 - mean over 5 months (November to March)
10 - skip the first 10 months (January to October)
7 - skip 7 months between every 5 months interval (April to October)

https://code.mpimet.mpg.de/boards/1/topics/3225

COMMENT

VLIST="SST QA WND"
SEALIST="MAM JJA SON"
SEALIST="DJF" #
YLIST="1988 2005"
INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/12.14.SELECT_SEA_SST_QA_WND/

ODIR=OUT_$(basename $0 .sh)
mkd $ODIR

for VAR in $VLIST; do
for SEA in $SEALIST; do
for Y   in $YLIST;   do

OUT=$ODIR/$(basename $0 .sh)_${VAR}_${SEA}_${Y}.TXT

echo IN: $INDIR/J-OFURO3_*${VAR}*${Y}*${SEA}.nc
rm -vf $OUT
cdo showdate $INDIR/J-OFURO3_*${VAR}*${Y}*${SEA}.nc > $OUT

if [ -f $OUT ]; then echo $OUT; fi
echo

done #Y
done #SEA
done #VAR

;/work09/am/00.WORK/2023.HEAT_FLUX_TREND/42.DIFF_15YEAR_PERIODS/22.12.WELCH_TEST
DSET="J-OFURO3"
ABB="LHF"
SEA = getenv("NCL_ARG_2"); DJF, MAM, JJA, SON
RUN="RAW"
PRD1="1988-2004"
PRD2="2005-2021"

PREFIX="J-OFURO3_COR3_DAILY_"+ABB+"_"+RUN
INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/22.14.WELCH_TEST_ADJUST_DOF/"
INFLE=PREFIX+"_WELCH_ADJUST_"+PRD2+"-"+PRD1+"_"+SEA+"_TAV.nc"
IN=INDIR+INFLE
scriptname = get_script_prefix_name()

FIGDIR="FIG_"+scriptname
system("mkdir -vp "+FIGDIR)
FIG=PREFIX+"_WELCH_ADJUST_"+PRD2+"-"+PRD1+"_"+SEA+"_DIFF_TAV"
TYP="PDF"

print("MMMMM READ INPUT DATA")
print("MMMMM INDIR "+INDIR)
print("MMMMM INFLE "+INFLE)
a=addfile(IN,"r")
xAve=a->xAve
yAve=a->yAve

diff=yAve-xAve
copy_VarCoords(xAve,diff)

prob=a->prob
prob=1.0-prob
copy_VarCoords(xAve,prob)
print("")

wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=DSET+" "+ABB+" "+RUN+" "+SEA
opt@gsnCenterString=""
opt@gsnRightString="("+ PRD2 +")" + "-" + "("+ PRD1 +")"
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.022
opt@gsnCenterStringFontHeightF=0.022
opt@gsnRightStringFontHeightF=0.022
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = -100.
;opt@cnMaxLevelValF =  100.
;opt@cnLevelSpacingF = 10.
opt@cnLevelSelectionMode = "ExplicitLevels"
opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)


;opt@pmLabelBarHeightF = 0.41
opt@pmLabelBarWidthF=0.05
;opt@pmLabelBarParallelPosF = 0.48
;opt@lbTopMarginF=-0.01
;opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m~S~2~N~"                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
;opt@lbTitleDirection = "Across"             ; title direction
opt@lbOrientation = "vertical"

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpCenterLonF = 180

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"

plot=gsn_csm_contour_map(wks,diff,res)

print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
; sgres@cnFillScaleF         = 0.6		; add extra density
sgres@cnFillDotSizeF       = 0.002

sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
sgres@cnLevels = 0.5	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = ""     ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString = ""    ; upper-left subtitle
sgres@gsnRightString = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,prob,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

overlay(plot,sig_plot)

draw(plot)         ; ここでplotを描く

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.01,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.01,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.01,0.950,txres)

frame(wks)         ; WorkStationの更新

print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)

begin

load "./substring.ncl"

DSET="J-OFURO3"
ABB="WND"
UNIT="m/s"
RGN="GS"
lonw = -90.
lone = -30.
lats = -10.
latn = 50.

BIPEN    = getenv("NCL_ARG_2")
arg    = getenv("NCL_ARG_3")
M=toint(arg)
YS    = toint(getenv("NCL_ARG_4"))
YE    = toint(getenv("NCL_ARG_5"))

MM=sprinti("%0.2i", M)
idx=M-1

MMM=(/"","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP", \
                    "OCT","NOV","DEC"/)

prefix="J-OFURO3_"+ABB+"_TREND_"+RGN+"_"+BIPEN+"_"
BP=substring(BIPEN,4,5)

indir="/work01/DATA/J-OFURO3/V1.2_PRE/BIPEN/"+BIPEN

FLIST1=indir+"/"+"JOFURO3_V1.2_PRE_"+ABB+"_"+BIPEN+"_19??.nc "
FLIST2=indir+"/"+"JOFURO3_V1.2_PRE_"+ABB+"_"+BIPEN+"_20??.nc "

FLIST=FLIST1+FLIST2
f=systemfunc("ls "+FLIST)

dim=dimsizes(f)
ntim=dim(0)

it=0
a=addfile(f(it),"r")
xTMP=a->WND
lat=a->latitude
lon=a->longitude

dim2=dimsizes(xTMP)
nlat=dim2(1)
nlon=dim2(2)

print("MMMMM READ DATA")
xMON=new((/ntim,nlat,nlon/),typeof(xTMP))

do it=0,ntim-1
a=addfile(f(it),"r")
delete(xTMP)
xTMP=a->WND
xMON(it,:,:)=xTMP(0,:,:)
end do

xMON!0="year"
xMON!1="lat"
xMON!2="lon"
do n=0,ntim-1
xMON&year(n)=tofloat(YS+n)
end do ;n

print("MMMMM CLIMATOLOGY (TIME AVE)")
xAVE = dim_avg_n_Wrap(xMON,2)



print("MMMMM REORDER DATA")

ts = xMON(lat|:, lon|: ,year|:)
ts!0="lat"
ts!1="lon"
ts!2="year"
ts&lat=lat
ts&lon=lon
dim=dimsizes(xMON&year)
nt=dim(0)
y1=toint(xMON&year(0))
y2=toint(xMON&year(nt-1))

print("MMMMM REGRESSION")
trend           = regCoef(xMON&year,ts)
trend@units     = ts@units ;+"/decade"    
trend@long_name = "regression coefficient (trend)"
copy_VarCoords(ts(:,:,0), trend)                ; copy lat,lon coords



print("MMMMM JUDGE SIGNIFICANCE")
;https://ccsr.aori.u-tokyo.ac.jp/~masakazu/memo/ncl/trend_signif.ncl
siglvl = 0.1 ;0.05
sighalf=siglvl/2.0
sigpct=siglvl*100
print("p-value (<"+siglvl+ "->significant at "+sigpct+"% level of falsely rejecting the null hypothesis, i.e., rcoef=0")
;;; t検定
  rstd    = reshape(trend@rstd,dimsizes(trend))   ; trend@rstdは1次元配列なのでtrendと同じ形に変形
  dof     = new(dimsizes(trend),integer)          ; 自由度の配列
  dof     = nt-2                      ; ここでは簡単のためどこでも年数とした。もちろん実際の解析では検討が必要。
;  print(dof(0,0))
  cdl     = cdft_p(trend/rstd,dof)                ; t値(trend/rstd)からt分布の片側確率を計算
  cdl     = mask(cdl,ismissing(trend),False)      ; 陸地をマスキング
;;; cdl には，t分布を-∞から各地点のt値まで積分して得られる確率(0から1まで)が入る
cdl!0="lat"
cdl!1="lon"

cdl&lat=lat
cdl&lon=lon

trend=trend*(y2-y1+1.0) ;単位を変換


print("MMMMM PLOT")
print(y1+" "+y2)
FIG=prefix+tostring(y1)+"-"+tostring(y2)+"_sig"+sigpct
TYP="PDF"

wks = gsn_open_wks(TYP, FIG)


;gsn_define_colormap(wks,"BlueDarkRed18") ;ncl_default")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=DSET+" "+ABB+" "+MMM(M)+BP
opt@gsnCenterString=""
opt@gsnRightString=tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.02
opt@gsnCenterStringFontHeightF=0.02
opt@gsnRightStringFontHeightF=0.02
opt@lbTitleFontHeightF= .02 ;Font size
opt@cnLevelSelectionMode = "ManualLevels"
opt@cnMinLevelValF = -4.
opt@cnMaxLevelValF =  4.
opt@cnLevelSpacingF = 0.5
opt@pmLabelBarHeightF = 0.08

opt@pmLabelBarHeightF = 0.06
opt@pmLabelBarWidthF=0.7
opt@pmLabelBarOrthogonalPosF = .10
opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = UNIT               ; title string
opt@lbTitlePosition  = "Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbTitleDirection = "Across"             ; title direction

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"

res@mpDataBaseVersion = "LowRes" ;-- better map resolution
;res@mpCenterLonF = 180
res@mpMinLonF = lonw ;-- min longitude
res@mpMaxLonF = lone ;-- max longitude
res@mpMinLatF = lats ;-- min latitude
res@mpMaxLatF = latn ;-- max latitude

plot=gsn_csm_contour_map(wks,trend,res)



print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
  res2                 = True          ; 有意性のためのres
  res2@gsnDraw         = False         ; plotを描かない
  res2@gsnFrame        = False         ; WorkStationを更新しない
  ;;; あとでShadeLtGtContourを用いるため，あらゆるものをFalseにしておく
  res2@cnLinesOn       = False
  res2@cnLineLabelsOn  = False
  res2@cnFillOn        = False
  res2@cnInfoLabelOn   = False
  ;;; ShadeLtGtContourのために，等値線を念のため指定しておく
  res2@cnLevelSelectionMode = "ExplicitLevels"
  res2@cnLevels        = (/0.04,0.05,0.95,0.96/)
res2@cnFillScaleF   = 0.5       ;; シェード（パターン）の密度
res2@cnFillDotSizeF = 0.002      ;; 点描の際の点の大きさ

  dum  = gsn_csm_contour(wks,cdl,res2)           ; とりあえずcdlを描く
  dum  = ShadeLtGtContour(dum,0.05,17,0.95,17)
      ;;; 有意な地点に点々(17番)
      ;;; これは有意水準10％の両側検定に対応

  overlay(plot,dum)  ; 有意性を示したdumをplotに重ねる
  
  draw(plot)         ; ここでplotを描く
  frame(wks)         ; WorkStationの更新



txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

print("FIG: "+FIG+"."+TYP)
end

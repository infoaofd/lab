ABBLIST="LHF"

ys=1989; Y=$ys; ye=1989 #2022

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/

BLIST="07BP01"

for BIPEN in $BLIST; do
MM=${BIPEN:0:2}; BP=${BIPEN:4:2}

ODIR=. #$INROOT/$BIPEN; mkdir -vp $ODIR

for ABB in $ABBLIST; do
while [ $Y -le $ye ];do
INDIR=$INROOT/${ABB}
IN=$INDIR/*${ABB}*_DAILY*${Y}.nc
if [ ! -f $IN ];then echo NO SUCH FILR, $IN;exit 1;fi

OUT=$ODIR/${ABB}_${BIPEN}_${Y}.nc

if [ $MM = "01" ]; then DE3=31; fi
if [ $MM = "02" ]; then DE3=28; fi
if [ $MM = "03" ]; then DE3=31; fi
if [ $MM = "04" ]; then DE3=30; fi
if [ $MM = "05" ]; then DE3=31; fi
if [ $MM = "06" ]; then DE3=30; fi
if [ $MM = "07" ]; then DE3=31; fi
if [ $MM = "08" ]; then DE3=31; fi
if [ $MM = "09" ]; then DE3=30; fi
if [ $MM = "10" ]; then DE3=31; fi
if [ $MM = "11" ]; then DE3=30; fi
if [ $MM = "12" ]; then DE3=31; fi

if [ $BP = "01" ];then DS=1;DE=10; fi
if [ $BP = "02" ];then DS=11;DE=20; fi
if [ $BP = "03" ];then DS=21;DE=$DE3; fi

TMP=$(basename $0 .sh)_${Y}.nc
cdo -selyear,${Y} -selmon,${MM} -selday,${DS}/${DE} $IN $TMP
cdo timmean $TMP $OUT
Y=$(expr $Y + 1)
done #Y
done #ABB
done #BIPEN



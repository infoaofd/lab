#!/bin/bash

INROOT="/work01/DATA/ERA5/EASIA/6HR/SFC/SST"
OROOT="/work01/DATA/ERA5/EASIA/10DAY/SFC/SST"

/work03/am/mybin/mkd $OROOT

LOG=$(basename $0 .sh).LOG

BIPENLIST="06BP01/  06BP02/  06BP03/  07BP01/  07BP02/  07BP03/"

for BIPEN in $BIPENLIST;do

echo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
echo $BIPEN
echo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm

/work03/am/mybin/mkd $OROOT/$BIPEN

INLIST=$(ls $INROOT/$BIPEN/*.grib)

for IN in $INLIST; do

TMP=$(basename $IN .grib).grib
OUT=$OROOT/$BIPEN/$TMP

echo $IN
echo $OUT
echo

#cdo info $IN
#cdo timmean $IN $OUT

#cdo info $OUT >> $LOG

done
done


#!/bin/bash
ABBLIST="LHF" 
INROOT="/work01/DATA/J-OFURO3/V1.2_PRE/$ABB"
OROOT="/work01/DATA/J-OFURO3/V1.2_PRE/BIPEN/$ABB"

/work03/am/mybin/mkd $OROOT

LOG=$(basename $0 .sh).LOG
date -R > $LOG
echo   >> $LOG

BIPENLIST="07BP01/" 
#BIPENLIST="06BP01/  06BP02/  06BP03/  07BP01/  07BP02/  07BP03/"

for ABB in $ABBLIST;do
for BIPEN in $BIPENLIST;do

echo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
echo $BIPEN
echo mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm

mkdir -vp $OROOT/$BIPEN

INLIST=$(ls $INROOT/$BIPEN/*_DAILY_*.nc)

for IN in $INLIST; do

TMP=$(basename $IN .nc).nc
OUT=$OROOT/$BIPEN/$TMP

echo $IN
echo $IN >> $LOG
#echo $OUT
#echo

#cdo info $IN
cdo timmean $IN $OUT

cdo info $OUT >> $LOG

done #IN
done #BIPEN
done #ABB


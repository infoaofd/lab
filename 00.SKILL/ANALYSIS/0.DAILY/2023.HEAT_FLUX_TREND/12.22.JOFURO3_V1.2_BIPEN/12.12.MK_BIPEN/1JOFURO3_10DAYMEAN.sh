DSET="JOFURO3_V1.2_PRE"
ABBLIST="LHF QA QS SST TA10 WND"
#ABBLIST="TA10 WND"

ys=1988; Y=$ys; ye=2022

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/

#BLIST="06BP01 06BP02 06BP03  07BP02 07BP03 08BP01 08BP02 08BP03"
#BLIST="06BP01 06BP02 06BP03 07BP01 07BP02 07BP03 08BP01 08BP02 08BP03"
BLIST="07BP01"

for BIPEN in $BLIST; do

MM=${BIPEN:0:2}; BP=${BIPEN:4:2}

ODIR=$INROOT/BIPEN/${BIPEN}; mkdir -vp $ODIR

for ABB in $ABBLIST; do

Y=$ys
while [ $Y -le $ye ];do

INDIR=$INROOT/${ABB}
IN=$INDIR/*${ABB}*_DAILY*${Y}.nc
if [ ! -f $IN ];then echo NO SUCH FILR, $IN;exit 1;fi

OUT=$ODIR/${DSET}_${ABB}_${BIPEN}_${Y}.nc
rm -vf $OUT

if [ $MM = "01" ]; then DE3=31; fi
if [ $MM = "02" ]; then DE3=28; fi
if [ $MM = "03" ]; then DE3=31; fi
if [ $MM = "04" ]; then DE3=30; fi
if [ $MM = "05" ]; then DE3=31; fi
if [ $MM = "06" ]; then DE3=30; fi
if [ $MM = "07" ]; then DE3=31; fi
if [ $MM = "08" ]; then DE3=31; fi
if [ $MM = "09" ]; then DE3=30; fi
if [ $MM = "10" ]; then DE3=31; fi
if [ $MM = "11" ]; then DE3=30; fi
if [ $MM = "12" ]; then DE3=31; fi

if [ $BP = "01" ];then DS=1;DE=10; fi
if [ $BP = "02" ];then DS=11;DE=20; fi
if [ $BP = "03" ];then DS=21;DE=$DE3; fi

TMP=$(basename $0 .sh)_${Y}.nc
rm -vf $TMP

cdo -selyear,${Y} -selmon,${MM} -selday,${DS}/${DE} $IN $TMP
INFOTMP=$?
if [ $INFOTMP -ne 0 ];then echo EEEEE ERROR cdo seldate $Y $MM $DS $DE;fi

cdo timmean $TMP $OUT
INFOMEAN=$?
if [ $INFOMEAN -ne 0 ];then echo EEEEE ERROR cdo timemean $Y $MM $DS $DE; fi

if [ $INFOTMP -eq 0 ];then rm -vf $INFOTMP; fi
 
if [ -f $OUT ];then echo; echo MMMMM OUT: $OUT;echo ; fi

rm -vf $TMP

Y=$(expr $Y + 1)

done # Y
done # ABB
done # BIPEN



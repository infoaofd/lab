INDIR=/work01/DATA/JRA55C/CST
INFLE=ll125.C.081_land.1972110100_2012123121.nc
#INFLE=ll125.C.006_gp.1972110100_2012123121.nc
IN=${INDIR}/${INFLE}
OUT=JRA55C_125_LANDMASK.nc

rm -vf $OUT
cdo -expr,'topo = ((var81==0.0)) ? 1.0 : var81/0.0' $IN $OUT

if [ ! -f $OUT ];then ERROR topo;exit1 1;fi
cdo infon $OUT
echo OUTPUT: $OUT

# https://code.mpimet.mpg.de/boards/53/topics/10933#2-Mask-land-areas-missing-values-in-ocean-areas

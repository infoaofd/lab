dset /work09/am/00.WORK/2023.HEAT_FLUX_TREND/22.CHK_WIND_BIAS/12.24.AAV_JOF3_1988-2012/J-OFURO3_WND_V0.6.6_AAV_1988-2012.nc
title 
undef -9999
dtype netcdf
xdef 1 linear 0 1
ydef 1 linear 0 1
zdef 1 linear 0 1
tdef 300 linear 00Z16JAN1988 1mo
vars 1
WND=>wnd  0  t,y,x  J-OFURO3 WND V0.6.0
endvars


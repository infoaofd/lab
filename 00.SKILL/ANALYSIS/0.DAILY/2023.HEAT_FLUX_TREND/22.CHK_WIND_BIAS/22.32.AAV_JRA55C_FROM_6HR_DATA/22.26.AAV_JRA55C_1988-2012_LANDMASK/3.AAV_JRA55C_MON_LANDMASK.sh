INLIST=$(ls /work01/DATA/JRA55C/6HR/U10MAG_MON_MEAN/*u10magMonMean.*)

echo MMMMM MERGE TIME
TMP=$(basename $0 .sh)_MERGE_TIME.nc
cdo -f nc4 mergetime $INLIST $TMP 

echo MMMMM SELYEAR
YS=1988; YE=2012
TMP2=$(basename $0 .sh)_TMP_SELYEAR_${YS}-${YE}.nc
rm -vf $TMP2
cdo selyear,${YS}/${YE} $TMP $TMP2

echo MMMMM LAND MASK
MASK=JRA55C_125_LANDMASK.nc
if [ ! -f $MASK ];then echo NO SUCH FILE $MASK;exit 1;fi
TMP3=$(basename $0 .sh)_TMP_SELYEAR_${YS}-${YE}_MASK.nc
rm -vf $TMP3
cdo -mul $MASK $TMP2 $TMP3 

echo MMMMM AAV AVE
OFLE=JRA55C_u10magMonMean_from_6hr_AAV_LANDMASK_${YS}-${YE}.nc
cdo fldmean -sellonlatbox,-180,180,-70,70 -selyear,${YS}/${YE} $TMP3 $OFLE

rm -vf $TMP $TMP2 $TMP3
if [ -f $OFLE ];then echo; echo OUTPUT: $OFLE;ls -l $OFLE; fi
echo




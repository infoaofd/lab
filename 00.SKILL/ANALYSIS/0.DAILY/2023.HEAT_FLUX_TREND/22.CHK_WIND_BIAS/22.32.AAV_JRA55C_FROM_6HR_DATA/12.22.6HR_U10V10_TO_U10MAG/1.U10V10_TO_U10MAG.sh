YS=1973; YE=2012
Y=$YS

INDIR=/work01/DATA/JRA55C/6HR/U10V10_MERGE/
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
ODIR=/work01/DATA/JRA55C/6HR/U10MAG
if [ ! -d $ODIR ];then mkdir -vp $ODIR;exit 1;fi
rm -rf $ODIR/*

while [ $Y -le $YE ]; do
IN=$(ls $INDIR/*u10v10.${Y}010100_${Y}123118)
INFLE=$(basename $IN)

OFLE=${INFLE/u10v10/u10mag}
OUT=${ODIR}/${OFLE}

cdo expr,'U10MAG=sqrt(sqr(var33)+sqr(var34))' $IN $OUT
echo $OUT;echo

Y=$(expr $Y + 1)
done


YS=1973; YE=2012
Y=$YS

INDIR=/work01/DATA/JRA55C/6HR/U10MAG/
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
ODIR=/work01/DATA/JRA55C/6HR/U10MAG_MON_MEAN
if [ ! -d $ODIR ];then mkdir -vp $ODIR;fi
rm -rf $ODIR/*

while [ $Y -le $YE ]; do
IN=$(ls $INDIR/*_u10mag.${Y}010100_${Y}123118)
INFLE=$(basename $IN)

OFLE=${INFLE/u10mag/u10magMonMean}
OUT=${ODIR}/${OFLE}

cdo monmean $IN $OUT
if [ -f $OUT ];then echo OUTPUT: $OUT; fi
echo

Y=$(expr $Y + 1)
done


YS=1972; YE=2012
Y=$YS

INDIR=/work01/DATA/JRA55C/MON/SFC/ANL
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
ODIR=/work01/DATA/JRA55C/MON/SFC/U10V10_MERGE
if [ ! -d $ODIR ];then echo NO SUCH DIR,$ODIR;exit 1;fi


while [ $Y -le $YE ]; do
IN1=$(ls $INDIR/*ugrd*${Y}*)
IN2=$(ls $INDIR/*vgrd*${Y}*)
#echo $IN1
#echo $IN2
#echo

TMP=${IN1/ugrd/u10v10}
OFLE=$(basename $TMP)
OUT=$ODIR/$OFLE

cdo merge $IN1 $IN2 $OUT
echo $OUT

Y=$(expr $Y + 1)
done


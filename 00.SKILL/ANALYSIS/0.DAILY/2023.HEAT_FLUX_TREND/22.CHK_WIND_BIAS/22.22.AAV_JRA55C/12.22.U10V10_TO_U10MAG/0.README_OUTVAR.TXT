 var1

   File format : GRIB
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : unknown  unknown  v instant       1   1     41760   1  P16  : 1             
   Grid coordinates :
     1 : lonlat                   : points=41760 (288x145)
                              lon : 0 to 358.75 by 1.25 degrees_east  circular
                              lat : 90 to -90 by -1.25 degrees_north
   Vertical coordinates :
     1 : height                   : levels=1
                           height : 10 m
   Time coordinate :  unlimited steps
     RefTime =  2012-01-01 00:00:00  Units = hours  Calendar = proleptic_gregorian
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  2012-01-01 00:00:00  2012-02-01 00:00:00  2012-03-01 00:00:00  2012-04-01 00:00:00
  2012-05-01 00:00:00  2012-06-01 00:00:00  2012-07-01 00:00:00  2012-08-01 00:00:00
  2012-09-01 00:00:00  2012-10-01 00:00:00  2012-11-01 00:00:00  2012-12-01 00:00:00

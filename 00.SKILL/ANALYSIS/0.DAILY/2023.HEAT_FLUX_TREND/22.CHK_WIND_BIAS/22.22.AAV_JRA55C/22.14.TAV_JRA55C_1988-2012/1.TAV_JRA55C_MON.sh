INLIST=$(ls /work01/DATA/JRA55C/MON/SFC/U10MAG/*u10mag*)

echo MMMMM MERGE TIME
TMP=$(basename $0 .sh)_MERGE_TIME.nc
cdo -f nc4 mergetime $INLIST $TMP 

echo MMMMM SELYEAR
YS=1988; YE=2012
TMP2=$(basename $0 .sh)_TMP_SELYEAR_${YS}-${YE}.nc
rm -vf $TMP2
cdo selyear,${YS}/${YE} $TMP $TMP2

echo MMMMM TIME AVE
OFLE=JRA55C_WND_TAV_${YS}-${YE}.nc
cdo timmean $TMP2 $OFLE

if [ -f $OFLE ];then echo; echo OUTPUT: $OFLE;ls -l $OFLE; fi
echo




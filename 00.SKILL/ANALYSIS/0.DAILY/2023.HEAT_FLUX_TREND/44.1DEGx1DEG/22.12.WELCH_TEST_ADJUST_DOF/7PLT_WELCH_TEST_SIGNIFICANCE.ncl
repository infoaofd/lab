;/work09/am/00.WORK/2023.HEAT_FLUX_TREND/42.DIFF_15YEAR_PERIODS/22.12.WELCH_TEST
DSET="J-OFURO3"


VAR="LHF"
UNIT="W/m~S~2~N~"
SEA = getenv("NCL_ARG_2"); DJF, MAM, JJA, SON
RUN = getenv("NCL_ARG_3")
PRD1="1988-2004"
PRD2="2005-2021"

INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/44.1DEGx1DEG/22.12.WELCH_TEST_ADJUST_DOF/"+VAR+"/"

PREFIX="J-OFURO3_DAILY_"+VAR+"_"+RUN
INFLE=PREFIX+"_WELCH_ADJUST_"+PRD2+"-"+PRD1+"_"+SEA+"_TAV_1x1.nc"

IN=INDIR+INFLE
scriptname = get_script_prefix_name()

FIGDIR="FIG_"+scriptname
system("mkdir -vp "+FIGDIR)
FIG=PREFIX+"_WELCH_ADJUST_"+PRD2+"-"+PRD1+"_"+SEA+"_DIFF_TAV_1x1"
TYP="PDF"

print("MMMMM READ INPUT DATA")
print("MMMMM INDIR "+INDIR)
print("MMMMM INFLE "+INFLE)
a=addfile(IN,"r")
xAve=a->xAve
yAve=a->yAve

diff=yAve-xAve
copy_VarCoords(xAve,diff)

prob=a->prob
prob=1.0-prob
copy_VarCoords(xAve,prob)
print("")

wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")
plot = new(2,graphic)                 ; create graphical array
;sig_plot = new(2,graphic)            ; create graphical array

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=""
opt@gsnCenterString=DSET+" "+VAR+" "+RUN+" "+SEA+" ("+ PRD2 +")" + "-" + "("+ PRD1 +")"
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.07
opt@gsnLeftStringOrthogonalPosF=0.07
opt@gsnRightStringOrthogonalPosF=0.07
opt@gsnLeftStringFontHeightF=0.02
opt@gsnCenterStringFontHeightF=0.02
opt@gsnRightStringFontHeightF=0.02

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = -100.
;opt@cnMaxLevelValF =  100.
;opt@cnLevelSpacingF = 10.
opt@cnLevelSelectionMode = "ExplicitLevels"
opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)

opt@pmLabelBarHeightF = 0.3
opt@pmLabelBarWidthF=0.03
opt@lbTitleString    = UNIT                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbOrientation = "vertical"
opt@lbLabelFontHeightF   = 0.015

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpProjection = "Mollweide"
res@mpPerimOn = False ; Turns off the box perimeter.
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpCenterLonF = 120
res@mpCenterLatF = 10

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"

;res@vpWidthF      = 0.3  ; plot size (width)
;res@vpXF          = 0.121578   ; location of plot
;res@vpYF          = 0.60       ; location of plot

print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
sgres@cnFillDotSizeF       = 0.002

sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
sgres@cnLevels = 0.5	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = ""     ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString = ""    ; upper-left subtitle
sgres@gsnRightString = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,prob,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

plot(0)=gsn_csm_contour_map(wks,diff,res)
overlay(plot(0),sig_plot)


print("MMMMM PANEL2")
;res@vpYF          = 0.5       ; location of plot
res@mpCenterLonF = -60
res@gsnLeftString=""
res@gsnCenterString=""
res@gsnRightString=""
res@gsnCenterStringOrthogonalPosF=-0.07
res@gsnLeftStringOrthogonalPosF=-0.07
res@gsnRightStringOrthogonalPosF=-0.07

plot(1)=gsn_csm_contour_map(wks,diff,res)
sig_plot = gsn_csm_contour(wks,prob,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)
overlay(plot(1),sig_plot)


panel_res=True
panel_res@gsnFrame      = False 
panel_res@gsnPanelLabelBar   = True   ; common label bar
panel_res@pmLabelBarWidthF   = 0.5    ; label bar width
panel_res@pmLabelBarHeightF   = 0.05    ; label bar width
panel_res@pmLabelBarOrthogonalPosF = -0.04
panel_res@lbLabelFontHeightF   = 0.013
panel_res@lbTitleString    = UNIT                ; title string
panel_res@lbTitlePosition  = "Bottom"              ; title position
panel_res@lbTitleFontHeightF   = 0.015
panel_res@gsnPanelYF = (/0.1, 0.2, 0.2,0.2/) 
gsn_panel(wks, plot, (/ 2, 1 /), panel_res)

;txres=True
;txres@txFontHeightF = 0.015
;txres@txJust="CenterLeft"
;today = systemfunc("date -R")
;gsn_text_ndc(wks,today,  0.01,0.95,txres)
;cwd =systemfunc("pwd")
;gsn_text_ndc(wks,"Current dir: "+cwd,      0.01,0.97,txres)
;scriptname  = get_script_name()
;gsn_text_ndc(wks,"Script: "+scriptname, 0.01,0.99,txres)

frame(wks)         ; WorkStation�̍X�V

print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)

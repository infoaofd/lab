#!/bin/bash
EXE=$(basename $0 .sh).ncl
if [ ! -f $EXE ];then echo NO SUCH FILE, $EXE; exit 1;fi

SEALIST="DJF MAM JJA SON"
SEALIST="DJF" # TEST
PRD1="1988-2004"; PRD2="2005-2021"
VLIST="WND SST QA QS TA10"
VLIST="QA QS"

for SEA in $SEALIST; do
for VAR in $VLIST; do

runncl.sh $EXE $SEA $PRD1 $PRD2 $VAR

done
done

exit 0



# THE FOLLOWING LINES ARE SAMPLES:
<<COMMENT


# INPUT FILE
IN=""
if [ ! -f $in ];then echo NO SUCH FILE, $IN; exit 1
fi

# WHILE LOOP
IS=1; IE=3
I=$IS
while [ $I -le $IE ]; do

  I=$(expr $I + 1)
done

# FOR LOOP 
ALIST="a b c"
for A in $ALIST; do

done

# IF BLOCK
i=3
if [ $i -le 5 ]; then

else if  [ $i -le 2 ]; then

else

fi

# CREATING DIRECTORY
if [ ! -d $DIR ]; then mkdir -vp ${DIR}; fi

# CHECHING COMMAND LINE ARGUMENTS
if [ $# -lt 1 ]; then
  echo Error in $0 : No arugment
  echo Usage: $0 arg
  exit 1
fi

# CHECHING COMMAND LINE ARGUMENTS
if [ $# -lt 2 ]; then
  echo Error in $0 : Wrong number of arugments
  echo Usage: $0 arg1 arg2
  exit 1
fi

# HANDLING COMMAND LINE OPTIONS
CMDNAME=$(basename $0)
while getopts t: OPT; do
  case $OPT in
    "t" ) flagt="true" ; value_t="$OPTARG" ;;
     * ) echo "Usage $CMDNAME [-t VALUE] [file name]" 1>&2
  esac
done

value_t=${value_t:-"NOT_AVAILABLE"}

if [ $value_t = "foo" ]; then
  type="foo"
elif [ $value_t = "boo" ]; then
  type="boo"
else
  type=""
fi
shift $(expr $OPTIND - 1)

COMMENT

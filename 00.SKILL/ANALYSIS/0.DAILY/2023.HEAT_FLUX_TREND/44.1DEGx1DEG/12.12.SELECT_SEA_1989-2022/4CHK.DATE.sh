VLIST="LHF SST WND QA"
#VLIST="LHF"

RLIST="ORG RAW CLM WND SST QA"
#RLIST="ORG" # CLM WND SST QA"

SLIST="DJF MAM JJA SON"
#SLIST="MAM JJA SON"
#SLIST="DJF"
#SLIST="MAM"

YLIST="1988 2005"
INDIR=/work01/DATA/J-OFURO3/V1.2_PRE/44.1DEGx1DEG/12.12.SELECT_SEA_1989-2022/LHF

for VAR in $VLIST; do
for RUN in $RLIST; do
for SEA in $SLIST; do
for Y   in $YLIST; do

YS=$Y; DY=16; YE=$(expr $YS + $DY)


INFLE=J-OFURO3_DAILY_${VAR}_${RUN}_${YS}-${YE}_${SEA}_1x1.nc
IN=$INDIR/$INFLE
OUT=$(basename $IN .nc).TXT

echo MMMMM $IN

if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

echo MMMMM $IN >$OUT
cdo showdate $IN >> $OUT; echo >> $OUT

echo MMMMM $OUT; echo

done #Y
done #SEA
done #RUN
done #VAR

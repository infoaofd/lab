;/work09/am/00.WORK/2023.HEAT_FLUX_TREND/44.1DEGx1DEG/22.22.TAV_MAP_PLOT
DSET="J-OFURO3"

SEA = getenv("NCL_ARG_2"); DJF, MAM, JJA, SON
VAR = getenv("NCL_ARG_3");

PRD1="1988-2004"
PRD2="2005-2021"
INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/44.1DEGx1DEG/22.12.WELCH_TEST_ADJUST_DOF/"+VAR+"/"
PREFIX="J-OFURO3_DAILY_"+VAR
INFLE=PREFIX+"_WELCH_ADJUST_2005-2021-1988-2004_"+SEA+"_TAV_1x1.nc"
IN=INDIR+INFLE

scriptname = get_script_prefix_name()

FIGDIR="FIG_"+scriptname
system("mkdir -vp "+FIGDIR)
FIG=PREFIX+"_WELCH_ADJUST_2005-2021-1988-2004_"+SEA+"_TAV_1x1"
TYP="PDF"

print("MMMMM READ INPUT DATA")
print("MMMMM IN "+IN)
a=addfile(IN,"r")
xAve=a->xAve
yAve=a->yAve
print("")

wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"WhViBlGrYeOrRe") ;NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")
plot = new(2,graphic)                          ; create graphical array

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=VAR+" "+SEA
opt@gsnCenterString=""
opt@gsnRightString=PRD1
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.022
opt@gsnCenterStringFontHeightF=0.022
opt@gsnRightStringFontHeightF=0.022
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = 0.
;opt@cnMaxLevelValF = 350.
;opt@cnLevelSpacingF = 50.
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)


;opt@pmLabelBarHeightF = 0.41
opt@pmLabelBarWidthF=0.05
;opt@pmLabelBarParallelPosF = 0.48
;opt@lbTopMarginF=-0.01
;opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = ""                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
;opt@lbTitleDirection = "Across"             ; title direction
opt@lbOrientation = "vertical"

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpCenterLonF = 180

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"

plot(0)=gsn_csm_contour_map(wks,xAve,res)

res@gsnRightString=PRD2
plot(1)=gsn_csm_contour_map(wks,yAve,res)

panel_res                    = True   ; panel mods desired
panel_res@gsnFrame      = False      
panel_res@gsnPanelBottom     = 0.0    ; space for label bar
panel_res@gsnPanelTop        = 0.5    ; only panel on lower half of page
panel_res@gsnPanelLabelBar   = True   ; common label bar
panel_res@pmLabelBarWidthF   = 0.5    ; label bar width
panel_res@gsnPanelXF         = (/0.121578/)
gsn_panel(wks, plot, (/ 2, 1 /), panel_res)

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.01,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.01,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.01,0.950,txres)

frame(wks)         ; WorkStation�̍X�V

print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)
print("")
print("")

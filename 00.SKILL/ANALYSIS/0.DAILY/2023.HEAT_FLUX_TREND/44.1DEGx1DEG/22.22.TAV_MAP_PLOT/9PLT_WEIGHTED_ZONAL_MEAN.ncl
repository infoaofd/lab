;/work09/am/00.WORK/2023.HEAT_FLUX_TREND/42.DIFF_15YEAR_PERIODS/22.12.WELCH_TEST
DSET="J-OFURO3"
ABB="LHF"
;SEA="DJF"
;SEA="MAM"
;SEA="JJA"
SEA="SON"

RUN="RAW"
PRD1="1988-2004"
PRD2="2005-2021"
PREFIX="J-OFURO3_COR3_DAILY_"+ABB+"_"+RUN+"_"
INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/42.DIFF_15YEAR_PERIODS/22.12.WELCH_TEST/LHF/"
INFLE=PREFIX+SEA+"_WELCH_TEST.nc"
IN=INDIR+INFLE

scriptname = get_script_prefix_name()

FIGDIR="FIG_"+scriptname
system("mkdir -vp "+FIGDIR)
FIG=PREFIX+SEA+"_ZONAL_MEAN"
TYP="PDF"

print("MMMMM READ INPUT DATA")
print("MMMMM INDIR "+INDIR)
print("MMMMM INFLE "+INFLE)
a=addfile(IN,"r")
diff=a->diffXY
diff=-diff ;
lat=a->lat
lon=a->lon
print("")

print("MMMMM ZONAL MEAN")
zave = dim_avg_n(diff,1)
zave!0="lat"
zave&lat=lat
zave@_FillValue = -99999.

print("MMMMM REMOVE DATA IN POLAR REGION")
zave=where(abs(zave&lat).le.70., zave, zave@_FillValue)
printVarSummary(zave)

print("MMMMM MULTIPLY COSINE WEIGHT")
rad    = 4.0*atan(1.0)/180.0
clat   = cos(lat*rad)
printVarSummary(clat)
zave(:)=zave(:)*clat(:)

rad    = 4.0*atan(1.0)/180.0
wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=ABB+" "+RUN+" "+SEA
opt@gsnCenterString=""
opt@gsnRightString="("+ PRD2 +")" + "-" + "("+ PRD1 +")"
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.022
opt@gsnCenterStringFontHeightF=0.022
opt@gsnRightStringFontHeightF=0.022

res=opt
res@xyLineThicknesses = 1.0  ; 太さ
res@trXMaxF = 12.0  ; X軸の最大値
res@trXMinF = -2.0  ; X軸の最小値

plot=gsn_csm_xy(wks,zave,zave&lat,res) 

draw(plot)         ; ここでplotを描く
frame(wks)         ; WorkStationの更新

print("MMMMM FIGDIR "+FIGDIR)
print("MMMMM FIG "+FIG+"."+TYP)

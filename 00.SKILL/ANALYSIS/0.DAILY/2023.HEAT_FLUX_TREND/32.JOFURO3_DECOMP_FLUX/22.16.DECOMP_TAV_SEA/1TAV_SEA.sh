
VAR=LHF

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/OUT_1TAV_MON_ALLYEAR/${VAR}/TAV_MON

OROOT=/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/OUT_$(basename $0 .sh)/${VAR}

mkd $OROOT

RD=0.README_$(basename $0 .sh).TXT
CWD=$(pwd)

YS=1988; YE=2022

#RLIST="RAW" # CLM WND SST QA"
RLIST="RAW CLM WND SST QA"
SLIST="DJF MAM JJA SON"
#SLIST="DJF"

for RUN in $RLIST; do

for SEA in $SLIST; do

ODIR=${OROOT}/TAV_SEA
mkd $ODIR; echo OUTPUT DIR: $ODIR

INLIST=$(ls $INROOT/${RUN}_RUN/J-OFURO3_COR3_${VAR}_${RUN}_RUN_MON_*.nc)
I=1
for IN in $INLIST;do
if [ $I -eq 1 ];then echo INPUT: $IN;fi
if [ $I -eq 2 ];then echo READING ...;fi
I=$(expr $I + 1)
done

TMP1=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_RUN_MERGE.nc
rm -vf $TMP1

cdo mergetime $INLIST $TMP1
if [ -f $TMP1 ];then 
echo;echo MMMM MERGETIME: $TMP1
else
echo;echo EEEEE ;echo EEEEE ERROR MERGETIME: $IN;echo EEEEE; echo
fi

TMP2=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_RUN_${SEA}_NO_TRIM.nc
rm -vf $TMP2

OFLE=J-OFURO3_COR3_${VAR}_${RUN}_RUN_${SEA}.nc
OUT=$ODIR/$OFLE
rm -vf $OUT

IS=1; IE=$(expr $YE - $YS - 1)
if [ $SEA != "DJF" ];then
IS=2; IE=$(expr $IE + 1)
fi
echo; echo MMMMM $IS $IE; echo



if [ $SEA = "DJF" ];then

cdo timselmean,3,11,9 $TMP1 $TMP2

if [ -f $TMP2 ];then 
echo;echo MMMM TIMSELMEAN: $TMP2
else
echo;echo EEEEE ;echo EEEEE ERROR TIMSELMEAN: $TMP1;echo EEEEE; echo
fi

cdo seltimestep,$IS/$IE $TMP2 $OUT

fi

if [ $SEA = "MAM" ];then
cdo timselmean,3,2,9 $TMP1 $TMP2

if [ -f $TMP2 ];then 
echo;echo MMMM TIMSELMEAN: $TMP2
else
echo;echo EEEEE ;echo EEEEE ERROR TIMSELMEAN: $TMP1;echo EEEEE; echo
fi

cdo seltimestep,$IS/$IE $TMP2 $OUT

fi

if [ $SEA = "JJA" ];then
cdo timselmean,3,5,9 $TMP1 $TMP2

if [ -f $TMP2 ];then 
echo;echo MMMM TIMSELMEAN: $TMP2
else
echo;echo EEEEE ;echo EEEEE ERROR TIMSELMEAN: $TMP1;echo EEEEE; echo
fi

cdo seltimestep,$IS/$IE $TMP2 $OUT

fi

if [ $SEA = "SON" ];then
cdo timselmean,3,8,9 $TMP1 $TMP2

if [ -f $TMP2 ];then 
echo;echo MMMM TIMSELMEAN: $TMP2
else
echo;echo EEEEE ;echo EEEEE ERROR TIMSELMEAN: $TMP1;echo EEEEE; echo
fi

cdo seltimestep,$IS/$IE $TMP2 $OUT

fi

if [ -f $OUT ];then 
echo MMMM OUT: $OUT
echo MMMMM $IS $IE; echo
rm -vf $TMP1 $TMP2; echo
else
echo;echo EEEEE ;echo EEEEE ERROR : $TMP2;echo EEEEE; echo
fi

done # SEA

done # RUN

<<COMMENT

cdo mergetime *.nc outfile
should normally work without problems. There is one limitation, 
to sort the time steps mergetime has to open all input files at the same time. 
How many file can be opened depends on the operating system. 
On some system the limit is 256.
You can use cat if your input files are already sorted in time:

https://code.mpimet.mpg.de/boards/1/topics/908

COMMENT

<<COMMENT
I want to calculate the seasonal means between November to March 

Try the operator timselmean. Here is an example for a 10 year monthly mean dataset starting in January:
cdo timselmean,5,10,7 ifile ofile
5 - mean over 5 months (November to March)
10 - skip the first 10 months (January to October)
7 - skip 7 months between every 5 months interval (April to October)

https://code.mpimet.mpg.de/boards/1/topics/3225

COMMENT

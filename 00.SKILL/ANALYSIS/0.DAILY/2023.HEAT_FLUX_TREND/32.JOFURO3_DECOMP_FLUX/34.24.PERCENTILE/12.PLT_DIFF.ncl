; /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/32.22.FRACTION_SEA
begin

DSET="J-OFURO3"
ABB="LHF"
SEASON = getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
PCNT= getenv("NCL_ARG_4")

FIGDIR="FIG_DIFF_PCNT/"
system("mkdir -vp "+FIGDIR)
FIG=FIGDIR+"J-OFURO3.LHF.DIFF_PCNT_"+PCNT+"_"+SEASON+"_"+RUN
TYP="PDF"


INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/32.JOFURO3_DECOMP_FLUX/34.24.PERCENTILE/LHF/"
INFLE="DIFF_"+PCNT+"_J-OFURO3_COR3_DAILY_LHF_RAW_2005-1988_"+SEASON+".nc"

IN=INDIR+INFLE
a=addfile(IN,"r")


print("")
print("MMMMM READ DATA "+SEASON)
name_in_nc="LHF" ;;変数名の設定
print("MMMMM NAME IN NetCDF FILE="+name_in_nc)
xMON=a->$name_in_nc$


print("MMMMM PLOT "+SEASON)
wks = gsn_open_wks(TYP, FIG)
gsn_define_colormap(wks,"BlueWhiteOrangeRed") ; "cmp_b2r") ;ncl_default";BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=DSET+" "+ABB+" "+RUN+" "+SEASON+" DIFF"+PCNT+"%"
opt@gsnCenterString=""
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
opt@cnMinLevelValF = -50.
opt@cnMaxLevelValF =  50.
opt@cnLevelSpacingF = 5.
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-10,-5,-4,-3,-2,-1,-0.5,0,0.5,1,2,3,4,5,10/)

;opt@pmLabelBarHeightF = 0.41
opt@pmLabelBarWidthF=0.05
;opt@pmLabelBarParallelPosF = 0.48
;opt@lbTopMarginF=-0.01
;opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m~S~2~N~"                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
;opt@lbTitleDirection = "Across"             ; title direction
opt@lbOrientation = "vertical"

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines
res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"
res@mpCenterLonF = 180

plot=gsn_csm_contour_map(wks,xMON(0,:,:),res)

draw(plot)         ; ここでplotを描く

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)         ; WorkStationの更新

print("FIG: "+FIG+"."+TYP)
end


/work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/32.33.COARE30_ALL_RUN
1点のデータでテストする

---------------------------------------------------------------------
		SST		Qa		WND
---------------------------------------------------------------------
SST run 	Raw		Daily Clim.     Daily Clim.
Qa run		Daily clim.     Raw		Daily Clim.
WND run 	Daily clim.     Daily Clim.	Raw
---------------------------------------------------------------------

/work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/32.22.COARE30_CLM_RUN
$ ls ../22.12.DAILY_CLIM_SMTH/OUT_123_21/
DAILY_CLIM_SMO_1988-2022_123_21_QA.nc   DAILY_CLIM_SMO_1988-2022_123_21_TA10.nc
DAILY_CLIM_SMO_1988-2022_123_21_QS.nc   DAILY_CLIM_SMO_1988-2022_123_21_WND.nc
DAILY_CLIM_SMO_1988-2022_123_21_SST.nc

$ ncdump -h ../22.12.DAILY_CLIM_SMTH/OUT_123_21/DAILY_CLIM_SMO_1988-2022_123_21_QA.nc
 
netcdf DAILY_CLIM_SMO_1988-2022_123_21_QA {
dimensions:
        year_day = UNLIMITED ; // (366 currently)
        latitude = 1 ;
        longitude = 1 ;
variables:
        int year_day(year_day) ;
                year_day:long_name = "day of year" ;
                year_day:units = "ddd" ;
        double latitude(latitude) ;
                latitude:standard_name = "latitude" ;
                latitude:long_name = "latitude" ;
                latitude:units = "degrees_north" ;
                latitude:axis = "Y" ;
        double longitude(longitude) ;
                longitude:standard_name = "longitude" ;
                longitude:long_name = "longitude" ;
                longitude:units = "degrees_east" ;
                longitude:axis = "X" ;
        float QA_dc(year_day, latitude, longitude) ;
                QA_dc:long_name = "Daily Climatology: MULTI QA V1.6.2" ;
                QA_dc:units = "g/kg" ;
                QA_dc:information = "Raw daily averages across all years" ;
                QA_dc:smoothing = "None" ;
        float QA_sm(year_day, latitude, longitude) ;
                QA_sm:smoothing = "FFT: 2 harmonics were retained." ;
                QA_sm:information = "Smoothed daily climatological averages" ;
                QA_sm:units = "g/kg" ;
                QA_sm:long_name = "Daily Climatology: MULTI QA V1.6.2" ;

// global attributes:
                :creation_date = "Sat, 25 Nov 2023 20:39:48 +0900" ;
                :nhar_ = "NUMBER OF HARMONICS=2" ;
                :input_directory_ = "/work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/12.12.PICKUP_1PNT/OUT_123_21/QA/" ;
                :title = "QA: Daily Climatology: 1988-2022" ;
}


Sun, 26 Nov 2023 15:03:34 +0900
../12.12.PICKUP_1PNT/OUT_123_21 
LHF/
QA/
QS/
SST/
TA10/
WND/


netcdf J-OFURO3_LHF_DAILY_123_21_1988 {
dimensions:
	time = UNLIMITED ; // (366 currently)
	lon = 1 ;
	lat = 1 ;
variables:
	float time(time) ;
		time:standard_name = "time" ;
		time:units = "hours since 1800-01-01 00:00}" ;
		time:calendar = "standard" ;
		time:axis = "T" ;
	double lon(lon) ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:axis = "X" ;
	double lat(lat) ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:axis = "Y" ;
	float LHF(time, lat, lon) ;
		LHF:long_name = "J-OFURO3 LHF Preliminary V1.4.1" ;
		LHF:units = "W/m^2" ;
		LHF:_FillValue = -9999.f ;
		LHF:missing_value = -9999.f ;

// global attributes:
		:CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
		:Conventions = "CF-1.6" ;
		:history = "Sat Nov 25 19:02:19 2023: cdo -remapbil,lon=123_lat=21 /work01/DATA/J-OFURO3/V1.2_PRE/HOURS.SINCE.1800-01-01/LHF/J-OFURO3_LHF_HR_V1.4.1_DAILY_1988_18000101.nc OUT_123_21/LHF/J-OFURO3_LHF_DAILY_123_21_1988.nc\n",
			"Sat Nov 25 15:12:35 2023: ncap2 -O -s @units=\"hours since 1800-01-01 00:00}\";time=udunits(time,@units);time@units=@units /work01/DATA/J-OFURO3/V1.2_PRE/LHF/J-OFURO3_LHF_HR_V1.4.1_DAILY_1988.nc /work01/DATA/J-OFURO3/V1.2_PRE/HOURS.SINCE.1800-01-01/LHF/J-OFURO3_LHF_HR_V1.4.1_DAILY_1988_18000101.nc" ;
		:NCO = "netCDF Operators version 5.1.9 (Homepage = http://nco.sf.net, Code = http://github.com/nco/nco, Citation = 10.1016/j.envsoft.2008.03.004)" ;
		:units = "hours since 1800-01-01 00:00}" ;
		:CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
}

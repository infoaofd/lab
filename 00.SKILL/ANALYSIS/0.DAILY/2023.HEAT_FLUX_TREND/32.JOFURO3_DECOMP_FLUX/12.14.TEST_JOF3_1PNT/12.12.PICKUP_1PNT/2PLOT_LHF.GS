
'sdfopen OUT_123_21/LHF/J-OFURO3_LHF_DAILY_123_21_2000.nc'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
# 'color  -kind  -gxout shaded'

# 'set lon  '; 'set lat  '
# 'set lev '
# 'set time '
'set x 1';'set y 1'
'set t 1 365'
'set cmark 0'
'd LHF'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs  -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' '


# TEXT
x=xl ;# (xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' J-OFURO3_V1.2PRE LHF'

# HEADER
'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' 2PLOT_LHF.GS'
yy = yy+0.2
'draw string ' xx ' ' yy ' ./2PLOT_LHF.sh '
yy = yy+0.2
'draw string ' xx ' ' yy ' Sun, 26 Nov 2023 20:07:34 +0900'
yy = yy+0.2
'draw string ' xx ' ' yy ' localhost.localdomain'
yy = yy+0.2
'draw string ' xx ' ' yy ' /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/12.12.PICKUP_1PNT'

'gxprint J-OFURO3_LHF_DAILY_123_21_2000.PDF'
'quit'

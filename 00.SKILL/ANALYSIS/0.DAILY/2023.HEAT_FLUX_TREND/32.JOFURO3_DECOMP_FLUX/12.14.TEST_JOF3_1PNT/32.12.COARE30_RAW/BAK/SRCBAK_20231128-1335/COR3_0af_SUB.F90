! C:\Users\boofo\Dropbox\TOOLS_TEMP\COARE\COARE-algorithm-master\Fortran\COARE3.0\f90
function psiuo(zet)
    x=(1.-15.*zet)**.25 
    psik=2.*log((1.+x)/2.)+log((1.+x*x)/2.)-2.*atan(x)+2.*atan(1.) 
    x=(1.-10.15*zet)**.3333 
    psic=1.5*log((1.+x+x*x)/3.)-sqrt(3.)*atan((1.+2.*x)/sqrt(3.))+4.*atan(1.)/sqrt(3.) 
    f=zet*zet/(1+zet*zet) 
    psiuo=(1-f)*psik+f*psic                                                
    if(zet>0)then 
      c=min(50.,.35*zet) 
      psiuo=-((1+1.0*zet)**1.0+.667*(zet-14.28)/exp(c)+8.525)
    endif 
END FUNCTION psiuo 



function psit_30(zet)
    x=(1.-(15*zet))**.5 
    psik=2*log((1+x)/2) 
    x=(1.-(34.15*zet))**.3333 
    psic=1.5*log((1.+x+x*x)/3.)-sqrt(3.)*atan((1.+2.*x)/sqrt(3.))+4.*atan(1.)/sqrt(3.) 
    f=zet*zet/(1+zet*zet) 
    psit_30=(1-f)*psik+f*psic   
   
    if(zet>0)then 
      c=min(50.,.35*zet) 
      psit_30=-((1.+2./3.*zet)**1.5+.6667*(zet-14.28)/exp(c)+8.525)
   endif
end FUNCTION psit_30



function qsee(ts,Pa)
real :: ts,Pa
x=ts
p=Pa
es=6.112*exp(17.502*x/(x+240.97))*.98*(1.0007+3.46e-6*p)
qsee=es*621.97/(p-.378*es)
end function



function grv(lat)
real lat
gamma=9.7803267715
c1=0.0052790414
c2=0.0000232718
c3=0.0000001262
c4=0.0000000007
pi=3.141593

phi=lat*pi/180
x=sin(phi)
grv=gamma*(1+(c1*x**2)+(c2*x**4)+(c3*x**6)+(c4*x**8))
!print *,'grav=',grv,lat
end function grv


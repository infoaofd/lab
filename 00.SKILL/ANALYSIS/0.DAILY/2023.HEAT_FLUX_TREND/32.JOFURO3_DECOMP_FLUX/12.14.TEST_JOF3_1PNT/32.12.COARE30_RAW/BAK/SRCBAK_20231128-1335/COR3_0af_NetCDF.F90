! /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/32.10.TEST_READ_1PNT
! 7.TEST_READ_SIMPLIFY.F90

SUBROUTINE READ_NC(VNAME, IN, IM,JM,NM,var2d)
  USE netcdf
CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::IN
INTEGER,INTENT(IN)::IM,JM,NM
real,INTENT(INOUT)::var2d(IM,JM,NM)
INTEGER ncid, varid, status

PRINT '(A)',"MMMMM INPUT FILE : "
PRINT '(A)',TRIM(IN)

CALL CHECK( nf90_open(IN, nf90_nowrite, ncid) )

CALL CHECK( nf90_inq_varid(ncid, "time", varid) )
print *,"time varid", varid
CALL CHECK( nf90_get_var(ncid, varid, time) )

CALL CHECK( nf90_inq_varid(ncid, "lon", varid) )
print *,"lon varid", varid
CALL CHECK(  nf90_get_var(ncid, varid, lon) )

CALL CHECK( nf90_inq_varid(ncid, "lat", varid) )
print *,"lat varid", varid
CALL CHECK( nf90_get_var(ncid, varid, lat) )

CALL CHECK( nf90_inq_varid(ncid, TRIM(VNAME), varid) )
print *,TRIM(VNAME), varid
CALL CHECK( nf90_get_var(ncid, varid, var2d) )
print *,'MIN=',minval(var2d)
print *,'MAX=',maxval(var2d)

status = nf90_close(ncid)
print *
END SUBROUTINE READ_NC



SUBROUTINE WRITE_NC(VNAME, OUT, IM, JM, NM, var2d, INDIR, INFLE, ODIR, OFLE)
  USE netcdf
CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::OUT, INDIR, INFLE, ODIR, OFLE
INTEGER,INTENT(IN)::IM,JM,NM
real,INTENT(INOUT)::var2d(IM,JM,NM)
INTEGER ncido, varido, status

status=nf90_create( trim(OUT), NF90_HDF5, ncido)

!print '(A,A)','MMMMM DEFINE DIMENSIONS'
status=nf90_def_dim(ncido, 'time', NM, id_time_dim)
status=nf90_def_dim(ncido, 'lon', IM, id_lon_dim)
status=nf90_def_dim(ncido, 'lat', JM, id_lat_dim)

!print '(A,A)','MMMMM DEFINE VARIABLES'
status=nf90_def_var(ncido, 'time', NF90_INT, id_time_dim, id_time)
status=nf90_def_var(ncido, 'lat', NF90_DOUBLE, id_lat_dim, id_lat)
status=nf90_def_var(ncido, 'lon', NF90_DOUBLE, id_lon_dim, id_lon)

status=nf90_put_att(ncido, id_lat, 'units','degrees_north')
status=nf90_put_att(ncido, id_lon, 'units','degrees_east')

CALL CHECK( NF90_DEF_VAR(NCIDO, TRIM(VNAME), NF90_REAL, &
     (/ID_LAT_DIM, ID_LON_DIM, ID_TIME_DIM/), ID_VARO))
CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'units','W/m2') )
CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'_FillValue', -9999.) )
CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'missing_value', -9999.) )

! print '(A,A)','MMMMM GLOBAL ATTRIBUTES'
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_dir", LEN(TRIM(INDIR)),TRIM(INDIR))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_file", LEN(TRIM(INFLE)),TRIM(INFLE))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"output_dir", LEN(TRIM(ODIR)),TRIM(ODIR))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"output_file", LEN(TRIM(OFLE)),TRIM(OFLE))

status=nf90_enddef(ncido) 



! print '(A,A)','MMMMM WRITE VARIABLES'
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_TIME, time ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_LAT, lat ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_LON, lon ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_VARO, var2d ) )

CALL CHECK(NF90_CLOSE(NCIDO))

PRINT '(A)',"MMMMM OUTPUT FILE: "
PRINT '(A)',TRIM(OUT)

END SUBROUTINE WRITE_NC

SUBROUTINE CHECK( STATUS )
  USE netcdf
  INTEGER, INTENT (IN) :: STATUS
  IF(STATUS /= NF90_NOERR) THEN 
    PRINT '(A,A)','EEEEE ERROR ',TRIM(NF90_STRERROR(STATUS))
    STOP "ABNORMAL END"
  END IF
END SUBROUTINE CHECK


#!/bin/bash
#
# https://gitlab.com/infoaofd/lab/-/blob/master/TOOL/GNUPLOT/GNUPLOT_TSR/PL.TSR.SLP.sh
# 
# gnuplotで日付・時刻の書かれた時系列データを
# 二軸でグラフ描画し保存するシェルスクリプト
# https://hack-le.com/45576248-2/
#
# gnuplotのデータプロットで日付を扱う場合
# https://www.netplan.co.jp/archives/1625
#
# gnuplot で pdf が出力されない時: 最後にunset outputを指定する
# https://gordiustears.net/trouble-of-pdf-terminal-on-gnuplot/
#
# gnuplotでプロットなどの色をcolornameの指定で変更する
# https://yutarine.blogspot.com/2018/12/gnuplot-colorname.html
# 
INFLE1=$1; INFLE1=${INFLE:-test3_0_af_out.dat}

jwarm=0; jcool=0; Rl=0; Rs=0

FIG=$(basename $0 .sh)_$(basename $INFLE1 .dat)_JW${jwarm}_JC${jcool}_Rl${Rl}_Rs${Rs}.PDF
rm -vf $FIG
TITLE="${INFLE1} jwarm=${jwarm} jcool=${jcool} Rl=${Rl} Rs=${Rs}"
XLABEL=""
YLABEL="LHF [W/m2]"

echo MMMM PLOT USING GNUPLOT
gnuplot <<EOF
#set datafile separator ','
set xdata time
set timefmt "%Y%m%d%H%M%S"
set format x "%m/%d\n%H:%M"
set mxtics 2
set mytics 2
#set grid xtics ytics mxtics mytics
#set key outside

set title '$TITLE'
set terminal pdfcairo color enhanced font "Helvetica,12"
set xlabel '$XLABEL'
set ylabel '$YLABEL'
set output '$FIG'
p '$INFLE1' u 1:2 w l ti "jwarm=${jwarm} jcool=${jcool}" lt rgbcolor "orange" lw 5
set output
EOF
echo MMMMM DONE!
echo
if [ -f $FIG ];then echo MMMMM FIG: $FIG;fi


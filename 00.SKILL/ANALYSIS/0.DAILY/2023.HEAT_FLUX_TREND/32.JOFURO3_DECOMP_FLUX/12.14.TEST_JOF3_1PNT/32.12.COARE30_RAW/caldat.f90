subroutine caldat(JULIAN,IYYY,MONTH,DD) 
! Description:
!
! Author: am
!
! Host: aofd30
! Directory: /work2/am/12.Work11/75.Mitsui/52.Vertical_Profile/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 11:00 on 11-01-2011.

!  use
!  implicit none

!  write(*,'(a)')'Subroutine: caldat'
!  write(*,*)''
!***********************************************************************
!                                                                       
! ********** SUBROUTINE DESCRIPTION:                                    
!                                                                       
! GIVEN THE JULIAN DAY, RETURNS THE YEAR, MONTH AND DAY OF MONTH.       
!                                                                       
! ********** ORIGINAL AUTHOR AND DATE:                                  
!                                                                       
! PRESS,FLANNERY,TEUKOLSKY,VETTERLING 1986.                             
! NUMERICAL RECIPES                                                     
!                                                                       
! ********** REVISION HISTORY:                                          
!                                                                       
!                                                                       
! ********** ARGUMENT DEFINITIONS:                                      
!                                                                       
      INTEGER,intent(in):: JULIAN
      integer,intent(out)::IYYY,MONTH,DD 
!                                                                       
! NAME   IN/OUT DESCRIPTION                                             
!                                                                       
! JULIAN   I    THE JULIAN DAY                                          
! IYYY     O    THE YEAR                                                
! MONTH    O    THE MONTH (1 TO 12)                                     
! DD       O    THE DAY OF THE MONTH                                    
!                                                                       
! ********** COMMON BLOCKS:                                             
!                                                                       
! NONE                                                                  
!                                                                       
! ********** LOCAL PARAMETER DEFINITIONS:                               
!                                                                       
      INTEGER IGREG 
      PARAMETER (IGREG=2299161) 
!                                                                       
! ********** LOCAL VARIABLE DEFINITIONS:                                
!                                                                       
      INTEGER JALPHA,JA,JB,JC,JD,JE 
!                                                                       
! NAME   DESCRIPTION                                                    
!                                                                       
!                                                                       
! ********** OTHER ROUTINES AND FUNCTIONS CALLED:                       
!                                                                       
!                                                                       
!---+67--1----+----2----+----3----+----4----+----5----+----6----+----7--
!                                                                       
!                                                                       
      IF (JULIAN .GE. IGREG) THEN 
        JALPHA = INT(((JULIAN - 1867216) - 0.25)/36524.25) 
        JA = JULIAN + 1 + JALPHA - INT(0.25*JALPHA) 
      ELSE 
        JA = JULIAN 
      ENDIF 
      JB = JA + 1524 
      JC = INT(6680. + ((JB - 2439870) - 122.1)/365.25) 
      JD = 365*JC + INT(0.25*JC) 
      JE = INT((JB - JD)/30.6001) 
      DD = JB - JD - INT(30.6001*JE) 
      MONTH = JE - 1 
      IF (MONTH .GT. 12) MONTH = MONTH - 12 
      IYYY = JC - 4715 
      IF (MONTH .GT. 2) IYYY = IYYY - 1 
      IF (IYYY .LE. 0) IYYY = IYYY - 1 
      RETURN 
!  write(*,'(a)')'Done subroutine caldat.'
!  write(*,*) 
end subroutine caldat

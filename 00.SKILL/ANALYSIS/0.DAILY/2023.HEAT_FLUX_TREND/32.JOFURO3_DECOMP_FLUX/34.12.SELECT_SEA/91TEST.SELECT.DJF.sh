
VAR=LHF

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/${VAR}

OROOT=/work01/DATA/J-OFURO3/V1.2_PRE/32.JOFURO3_DECOMP_FLUX/34.12.SELECT_SEA/${VAR}

mkd $OROOT

RD=0.README_$(basename $0 .sh).TXT
CWD=$(pwd)

<<COMMENT
   Time coordinate :  3159 steps
     RefTime =  1800-01-01 00:00:00  Units = hours  Calendar = standard
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-01-01 00:00:00  1988-01-02 00:00:00  1988-01-03 00:00:00  1988-01-04 00:00:00
.....
  2022-12-29 00:00:00  2022-12-30 00:00:00  2022-12-31 00:00:00
cdo    sinfo: Processed 1 variable over 3159 timesteps [0.09s 14MB].
COMMENT

IS=$(expr 31 + 29 + 1 ); IE=$(expr 12784 - 31)

RLIST="RAW" # CLM WND SST QA"
#RLIST="RAW CLM WND SST QA"
#SLIST="DJF MAM JJA SON"
SLIST="DJF"

for RUN in $RLIST; do

for SEA in $SLIST; do

ODIR=${OROOT}/
mkd $ODIR; echo MMMMM OUTPUT DIR: $ODIR

INLIST=$(ls $INROOT/${RUN}_RUN/J-OFURO3_COR3_${VAR}_${RUN}_RUN_DAILY_*.nc)

TMP1=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_RUN_MERGE.nc

# echo MMMMM MERGETIME
# rm -vf $TMP1
# cdo mergetime $INLIST $TMP1
if [ -f $TMP1 ];then 
echo;echo MMMM MERGETIME: $TMP1
else
echo;echo EEEEE ;echo EEEEE ERROR MERGETIME: $TMP1;echo EEEEE; echo
exit 1
fi
echo

echo; echo MMMMM SELTIMESTEP $IS $IE
TMP2=$ODIR/TMP_J-OFURO3_COR3_${VAR}_${RUN}_RUN_${SEA}_TRIM.nc
rm -vf $TMP2
cdo seltimestep,${IS}/${IE} $TMP1 $TMP2
echo


if [ $SEA = "DJF" ];then
echo MMMMM SELMON $SEA
OFLE=J-OFURO3_COR3_DAILY_${VAR}_${RUN}_RUN_${SEA}.nc
OUT=$ODIR/$OFLE
rm -vf $OUT
cdo selmon,12,1,2 $TMP2 $OUT
fi # SEA



if [ -f $OUT ];then 
# rm -vf $TMP1 $TMP2; echo
echo MMMM OUT: $OUT
else
echo;echo EEEEE ;echo EEEEE ERROR : $TMP2;echo EEEEE; echo
fi # OUT

done # SEA

done # RUN

<<COMMENT

cdo mergetime *.nc outfile
should normally work without problems. There is one limitation, 
to sort the time steps mergetime has to open all input files at the same time. 
How many file can be opened depends on the operating system. 
On some system the limit is 256.
You can use cat if your input files are already sorted in time:

https://code.mpimet.mpg.de/boards/1/topics/908

COMMENT

<<COMMENT
I want to calculate the seasonal means between November to March 

Try the operator timselmean. Here is an example for a 10 year monthly mean dataset starting in January:
cdo timselmean,5,10,7 ifile ofile
5 - mean over 5 months (November to March)
10 - skip the first 10 months (January to October)
7 - skip 7 months between every 5 months interval (April to October)

https://code.mpimet.mpg.de/boards/1/topics/3225

COMMENT

; /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/32.22.FRACTION_SEA
begin

DSET="J-OFURO3"
ABB="LHF"
SEASON = getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
y1=1989
y2=2021
FIGDIR="FIG_DIFF_SEASON/"
system("mkdir -vp "+FIGDIR)
FIG=FIGDIR+"J-OFURO3.LHF.TREND_DIFF_"+SEASON+"_RAW-"+RUN
TYP="PDF"


INDIR="/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/32.15.TREND_SEA_NetCDF_OUT/"
prefix="J-OFURO3.LHF.TREND_"+SEASON+"_RAW"
INFLE=prefix+".nc"
IN0=INDIR+INFLE
a0=addfile(IN0,"r")

prefix="J-OFURO3.LHF.TREND_"+SEASON+"_"+RUN
INFLE=prefix+".nc"
IN=INDIR+INFLE
a=addfile(IN,"r")

print("")
print("MMMMM READ DATA "+SEASON)
name_in_nc="trend_RAW" ;;変数名の設定
print("MMMMM NAME IN NetCDF FILE="+name_in_nc)
xMON0=a0->$name_in_nc$

lat=a->lat
lon=a->lon
name_in_nc="trend_"+RUN ;;変数名の設定
print("MMMMM NAME IN NetCDF FILE="+name_in_nc)
xMON=a->$name_in_nc$

xFRAC=xMON0
; xMON0=正味
; xMON =気候値
; 正味>0のときxMON0-xMON = 偏差によって正味の変化が説明できる部分
; 正味<0のときxMON-xMON0 = 偏差によって正味の変化が説明できる部分

xFRAC=where( xMON0.gt.0.,xMON0-xMON, xMON-xMON0)
;xFRAC=where( xMON0.gt.-100.0 ,xFRAC, xMON0@_FillValue)


print("MMMMM PLOT "+SEASON)
wks = gsn_open_wks(TYP, FIG)
gsn_define_colormap(wks,"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=DSET+" "+ABB+" "+RUN+" "+SEASON
opt@gsnCenterString=""
opt@gsnRightString=tostring(y1)+"-"+tostring(y2)
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = -200.
;opt@cnMaxLevelValF =  200.
;opt@cnLevelSpacingF = 20.
opt@cnLevelSelectionMode = "ExplicitLevels"
opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)


opt@pmLabelBarHeightF = 0.08
opt@pmLabelBarWidthF=0.9
opt@pmLabelBarOrthogonalPosF = .10
opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m2"                ; title string
opt@lbTitlePosition  = "Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbTitleDirection = "Across"             ; title direction

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution

res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@cnFillMode = "CellFill"

plot=gsn_csm_contour_map(wks,xFRAC,res)

draw(plot)         ; ここでplotを描く
frame(wks)         ; WorkStationの更新

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)



print("FIG: "+FIG+"."+TYP)
end


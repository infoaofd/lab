$ ncdump -h /work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/32.15.TREND_SEA_NetCDF_OUT/J-OFURO3.LHF.TREND_DJF_QA.nc > 0NCDUMP_INPUT.TXT

netcdf J-OFURO3.LHF.TREND_DJF_QA {
dimensions:
	lat = 720 ;
	lon = 1440 ;
variables:
	double lat(lat) ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:axis = "Y" ;
	double lon(lon) ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:axis = "X" ;
	float trend_QA(lat, lon) ;
		trend_QA:_FillValue = -9999.f ;
}

;# /work09/am/00.WORK/2023.HEAT_FLUX_TREND/12.12.JOFURO3_V1.2_MON_SEA_YEAR/12.22.LHF_SEASON_TREND/12.22.TREND_SEASON
begin

DSET="J-OFURO3"
ABB="LHF"
MM=getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
REGION="GS"
M=toint(MM)
idx=M-1

MMM=(/"","JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP", \
                    "OCT","NOV","DEC"/)
DSET="J-OFURO3"
ABB="LHF"

FIGDIR="FIG_MON_"+REGION+"/"
system("mkdir -vp "+FIGDIR)
prefix="J-OFURO3.LHF.TREND"+"_"+REGION+"_"+RUN+"_"+MM


indir="/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/OUT_1TAV_MON_ALLYEAR/LHF/TAV_MON/"+RUN+"_RUN/"

FLIST1=indir+"/"+"J-OFURO3_COR3_LHF_"+RUN+"_RUN_MON_19??.nc "
FLIST2=indir+"/"+"J-OFURO3_COR3_LHF_"+RUN+"_RUN_MON_20??.nc "

FLIST=FLIST1+FLIST2

;print(FLIST)
f=systemfunc("ls "+FLIST)
;print(f)

a=addfiles(f,"r")
;print(a)


print("### READ DATA")
ListSetType (a, "cat") 
xMON=a[:]->LHF
;xMON=-xMON ;POS UPWARD


lat=a[0]->lat ;g0_lat_1
lon=a[0]->lon ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a[:]->time ; initial_time0_hours

;print(TIME)


print("### REORDER DATA")

ts = xMON(lat|:, lon|: ,time|idx::12)
ts!0="lat"
ts!1="lon"
ts!2="time"
ts&lat=lat
ts&lon=lon
ts&time=TIME(idx::12)


utc_date = ut_calendar(ts&time, 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
date_str = sprinti("%0.2i ", day) + \
         MMM(month) + " "  + sprinti("%0.4i", year)
 
print(date_str) 

;print(year)
time=year
dim=dimsizes(year)
nt=dim(0)
y1=year(0)
y2=year(nt-1)


printVarSummary(ts)

print("### REGRESSION")
rc           = regCoef(time,ts)

rc=rc*tofloat(nt-1)
rc@units     = ts@units ;+"/decade"    
rc@long_name = "regression coefficient (trend)"
copy_VarCoords(ts(:,:,0), rc)                ; copy lat,lon coords
printVarSummary(rc)





print("SIGNIFICANCE")
copyatt(rc,ts)  ; copy other atts and cv's
tval = onedtond(rc@tval , dimsizes(rc))
df   = onedtond(rc@nptxy, dimsizes(rc)) - 2
b    = tval
b    = 0.5        ; b must be same size as tval (and df)
prob = betainc(df/(df+tval^2),df/2.0,b)

pval = rc
pval = (/prob/)  ; copy data
pval@long_name = "probability"



print("MMMMM JUDGE SIGNIFICANCE")
;https://ccsr.aori.u-tokyo.ac.jp/~masakazu/memo/ncl/trend_signif.ncl
siglvl = 0.1 ;0.05
sigpct=siglvl*100
print("p-value (<"+siglvl+ "->significant at "+sigpct+"% level of falsely rejecting the null hypothesis, i.e., rcoef=0")

sig = pval
ndim = dimsizes(pval)
nlat = ndim(0)
nlon = ndim(1)
do j=0,nlat-1
  do i=0,nlon-1
    if (pval(j,i).lt.siglvl) then
      sig(j,i) = 1.
    else
      sig(j,i) = 0.
    end if
  end do ;i
end do   ;j



print("PLOT")
FIG=FIGDIR+prefix+"_"+y1+"-"+y2+"_sig"+sigpct
TYP="PDF"
lonw =  -90. ;GS
lone =  -30. 
lats =  -10. 
latn =   50. 

wks = gsn_open_wks(TYP, FIG)


;gsn_define_colormap(wks,"BlueDarkRed18") ;ncl_default")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=ABB+" "+RUN ;DSET+" "+ABB+" "+RUN
opt@gsnCenterString=""
opt@gsnRightString=MMM(M)+" "+tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@cnLevelSelectionMode = "ManualLevels"
opt@cnMinLevelValF = -80.
opt@cnMaxLevelValF =  80.
opt@cnLevelSpacingF = 10.

opt@pmLabelBarHeightF = 0.08
opt@pmLabelBarWidthF=0.9
opt@pmLabelBarOrthogonalPosF = .10
opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m2"                ; title string
opt@lbTitlePosition  = "Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbTitleDirection = "Across"             ; title direction


res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"

res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpMinLonF = lonw ;-- min longitude
res@mpMaxLonF = lone ;-- max longitude
res@mpMinLatF = lats ;-- min latitude
res@mpMaxLatF = latn ;-- max latitude

plot1=gsn_csm_contour_map(wks,rc,res)




print("PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
; sgres@cnFillScaleF         = 0.6		; add extra density
sgres@cnFillDotSizeF       = 0.003

print("ACTIVATE IF GRAY SHADING FOR B&W PLOT")  
sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
sgres@cnLevels = 0.5	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = ""     ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString = ""    ; upper-left subtitle
sgres@gsnRightString = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,sig,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

overlay(plot1,sig_plot)



print("PLOT REGRESSION CONTOUR")
delete(res)
res=opt
res@cnFillOn     = False   ; turn on color fill
res@cnLinesOn    = True    ; turn off contour lines
res@gsnLeftString=""
res@gsnCenterString=""
res@gsnRightString=""

res@cnLineLabelsOn = True
res@cnLineLabelInterval = 5
res@cnLineLabelFontHeightF = 0.015
res@cnLineThicknessF = 2.0

;plot2=gsn_csm_contour(wks,rc,res)

;overlay(plot1,plot2)

draw(plot1)


txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)

print("FIG: "+FIG+"."+TYP)
end

# /work09/am/00.WORK/2023.HEAT_FLUX_TREND/12.12.JOFURO3_V1.2_MON_SEA_YEAR/12.22.LHF_SEASON_TREND/12.22.TREND_SEASON

#NLIST="4TREND_MON_KUROSHIO.ncl 3TREND_MON_GS.ncl 2TREND_MON.ncl "
NLIST="2TREND_MON.ncl "
#NLIST="3TREND_MON_GS.ncl 4TREND_MON_KUROSHIO.ncl"
MLIST="01 02 03 04 05 06 07 08 09 10 11 12"
#MLIST="01"
RLIST="RAW SST WND QA"
#RLIST="RAW"

for NCL in $NLIST;do

if [ ! -f $NCL ];then echo NO SUCH FILE,$NCL;exit 1;fi

for MM in $MLIST;do

for RUN in $RLIST;do

runncl.sh $NCL $MM $RUN

done #RUN

done #SEASON

done #NCL


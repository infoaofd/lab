
VAR=LHF

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/${VAR}

OROOT=/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/OUT_$(basename $0 .sh)/${VAR}

mkd $OROOT

RD=0.README_$(basename $0 .sh).TXT
CWD=$(pwd)

#RLIST="RAW" # CLM WND SST QA"
RLIST="RAW CLM WND SST QA"

YS=1988; YE=2022
#YE=1988 #TMP

for RUN in $RLIST; do

ODIR=${OROOT}/TAV_MON/${RUN}_RUN
mkd $ODIR; echo OUTPUT DIR: $ODIR

Y=$YS
while [ $Y -le $YE ];do

INDIR=$INROOT/${RUN}_RUN/
INFLE=J-OFURO3_COR3_${VAR}_${RUN}_RUN_DAILY_$Y.nc
IN=$INDIR/$INFLE

if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

OFLE=J-OFURO3_COR3_${VAR}_${RUN}_RUN_MON_$Y.nc
OUT=$ODIR/$OFLE

cdo monmean $IN $OUT
if [ -f $OUT ];then echo OUT: $OUT; fi
echo 

Y=$(expr $Y + 1)

done # Y

done # RUN

<<COMMENT

The difference between the two is in the way they handle missing data. 
monmean ignores missing values, while monavg includes missing.
 Quoting the manual: "the mean of 1, 2, miss and 3 is (1+2+3)/3 = 2, 
whereas the average is (1+2+miss+3)/4 = miss/4 = miss" 
- see section 2.8 of the manual for more details.

https://stackoverflow.com/questions/28498686/how-to-calculate-monthly-data-from-a-daily-netcdf

COMMENT

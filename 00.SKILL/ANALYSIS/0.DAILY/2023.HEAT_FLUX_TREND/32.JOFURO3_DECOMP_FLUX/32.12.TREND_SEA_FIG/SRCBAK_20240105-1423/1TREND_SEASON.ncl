;# /work09/am/00.WORK/2023.HEAT_FLUX_TREND/12.12.JOFURO3_V1.2_MON_SEA_YEAR/12.22.LHF_SEASON_TREND/12.22.TREND_SEASON
begin

DSET="J-OFURO3"
ABB="LHF"
arg    = getenv("NCL_ARG_2")
SEASON=arg

prefix="J-OFURO3.LHF.TREND."+SEASON

indir="/work01/DATA/J-OFURO3/V1.2_PRE/LHF/MON/LHF.HOURS.SINCE.1800-01-01/"
infle="J-OFURO3_LHF_"+SEASON+"_1989-2021.nc"
in=indir+infle
a=addfile(in,"r")

print("MMMMM READ DATA")
xMON=a->LHF


lat=a->latitude ;g0_lat_1
lon=a->longitude ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a->time ; initial_time0_hours

;print(TIME)


print("MMMMM REORDER DATA")

ts = xMON(latitude|:, longitude|: ,time|:)
ts!0="lat"
ts!1="lon"
ts!2="time"
ts&lat=lat
ts&lon=lon
ts&time=TIME


utc_date = ut_calendar(ts&time, 0)

year   = tointeger(utc_date(:,0))    ; Convert to integer for
month  = tointeger(utc_date(:,1))    ; use sprinti 
day    = tointeger(utc_date(:,2))
date_str = sprinti("%0.2i ", day) + sprinti("%0.4i", year)
 
print(date_str) 

;print(year)
time=year
dim=dimsizes(year)
nt=dim(0)
print("nt="+nt)
y1=year(0)
y2=year(nt-1)


printVarSummary(ts)

print("MMMMM REGRESSION")
trend           = regCoef(time,ts)

trend@units     = ts@units ;+"/decade"    
trend@long_name = "regression coefficient (trend)"
copy_VarCoords(ts(:,:,0), trend)                ; copy lat,lon coords
printVarSummary(trend)
print(max(trend))
print(min(trend))

print("MMMMM JUDGE SIGNIFICANCE")
;https://ccsr.aori.u-tokyo.ac.jp/~masakazu/memo/ncl/trend_signif.ncl
siglvl = 0.1 ;0.05
sighalf=siglvl/2.0
sigpct=siglvl*100
copyatt(trend,ts)  ; copy other atts and cv's
tval = onedtond(trend@tval , dimsizes(trend))
df   = onedtond(trend@nptxy, dimsizes(trend)) - 2
b    = tval
b    = 0.5        ; b must be same size as tval (and df)
prob = betainc(df/(df+tval^2),df/2.0,b)

pval = trend
pval = (/prob/)  ; copy data
pval@long_name = "probability"

print("p-value (<"+siglvl+ "->significant at "+sigpct+"% level of falsely rejecting the null hypothesis, i.e., rcoef=0")
;;; t検定
sig = pval
ndim = dimsizes(pval)
nlat = ndim(0)
nlon = ndim(1)
do j=0,nlat-1
  do i=0,nlon-1
    if (pval(j,i).lt.siglvl) then
      sig(j,i) = 1.
    else
      sig(j,i) = 0.
    end if
  end do ;i
end do   ;j

trend=trend*tofloat(nt) ;単位を変換



print("MMMMM PLOT")
FIG=prefix+"_"+y1+"-"+y2+"_sig"+sigpct
TYP="PDF"

wks = gsn_open_wks(TYP, FIG)


gsn_define_colormap(wks,"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True

opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=DSET+" "+ABB+" "+SEASON
opt@gsnCenterString=""
opt@gsnRightString=tostring(y1)+"-"+tostring(y2)

opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

;opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
;opt@cnMinLevelValF = -100.
;opt@cnMaxLevelValF =  100.
;opt@cnLevelSpacingF = 10.
opt@cnLevelSelectionMode = "ExplicitLevels"
opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)


opt@pmLabelBarHeightF = 0.08
opt@pmLabelBarWidthF=0.9
opt@pmLabelBarOrthogonalPosF = .10
opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m2"                ; title string
opt@lbTitlePosition  = "Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbTitleDirection = "Across"             ; title direction

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"

res@mpDataBaseVersion = "LowRes" ;-- better map resolution
;res@mpMinLonF = 105. ;-- min longitude
;res@mpMaxLonF = 145. ;-- max longitude
;res@mpMinLatF =  0. ;-- min latitude
;res@mpMaxLatF =  50. ;-- max latitude

plot=gsn_csm_contour_map(wks,trend,res)


print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
; sgres@cnFillScaleF         = 0.6		; add extra density
sgres@cnFillDotSizeF       = 0.002

print("MMM ACTIVATE IF GRAY SHADING FOR B&W PLOT")  
sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
sgres@cnLevels = 0.5	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = ""     ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString = ""    ; upper-left subtitle
sgres@gsnRightString = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,sig,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

overlay(plot,sig_plot)

draw(plot)         ; ここでplotを描く
frame(wks)         ; WorkStationの更新

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)



print("FIG: "+FIG+"."+TYP)
end

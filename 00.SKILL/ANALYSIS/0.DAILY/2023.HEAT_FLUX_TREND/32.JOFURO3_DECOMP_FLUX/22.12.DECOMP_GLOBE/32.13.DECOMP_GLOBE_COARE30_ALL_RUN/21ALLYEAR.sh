EXE=2PLOT_LHF_ALL.sh
FIG=OUT_$(basename $EXE .sh)
mkdir $FIG

Y=1988
while [ $Y -le 2022 ]; do
$EXE $Y
mv -v 2PLOT_LHF_ALL_*.PDF $FIG
echo
Y=$(expr $Y + 1)
done


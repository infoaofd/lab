INTEGER FUNCTION JULDAY(IYYY,MONTH,DD) 

! Description:
!
! Author: am
!
! Host: aofd30
! Directory: /work2/am/12.Work11/75.Mitsui/52.Vertical_Profile/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 10:59 on 11-01-2011.

!                                                                       
! ********** SUBROUTINE DESCRIPTION:                                    
!                                                                       
! FINDS THE JULIAN DAY FROM A DATE.                                     
!                                                                       
! ********** ORIGINAL AUTHOR AND DATE:                                  
!                                                                       
! PRESS,FLANNERY,TEUKOLSKY,VETTERLING 1986.                             
! NUMERICAL RECIPES                                                     
!                                                                       
! ********** REVISION HISTORY:                                          
!                                                                       
!                                                                       
! ********** ARGUMENT DEFINITIONS:                                      
!                                                                       
      INTEGER,intent(inout):: IYYY,MONTH,DD
!                                                                       
! NAME   IN/OUT DESCRIPTION                                             
!                                                                       
! IYYY     I    YEAR                                                    
! MONTH    I    MONTH (1 TO 12)                                         
! DD       I    DAY OF MONTH                                            
! JULDAY   O    JULIAN DAY                                              
!                                                                       
! ********** COMMON BLOCKS:                                             
!                                                                       
! NONE                                                                  
!                                                                       
! ********** LOCAL PARAMETER DEFINITIONS:                               
!                                                                       
      INTEGER IGREG 
      PARAMETER (IGREG = 15 + 31*(10 + 12*1582)) 
!                                                                       
! ********** LOCAL VARIABLE DEFINITIONS:                                
!                                                                       
      INTEGER JY,JM,JA 
!                                                                       
! NAME   DESCRIPTION                                                    
!                                                                       
!                                                                       
! ********** OTHER ROUTINES AND FUNCTIONS CALLED:                       
!                                                                       
! INT    - INTRINSIC TRUNCATE                                           
!                                                                       
!---+67--1----+----2----+----3----+----4----+----5----+----6----+----7--
!                                                                       
      IF (IYYY .LT. 0) IYYY = IYYY + 1 
      IF (MONTH .GT. 2) THEN 
        JY = IYYY 
        JM = MONTH + 1 
      ELSE 
        JY = IYYY - 1 
        JM = MONTH + 13 
      ENDIF 
      JULDAY = INT(365.25*JY) + INT(30.6001*JM) + DD + 1720995 
      IF (DD + 31*(MONTH + 12*IYYY) .GE. IGREG) THEN 
        JA = INT(0.01*JY) 
        JULDAY = JULDAY + 2 - JA + INT(0.25*JA) 
      ENDIF 
      RETURN 

end function julday

#!/bin/bash

# Sun, 26 Nov 2023 20:04:07 +0900
# /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/12.14.TEST_JOF3_1PNT/12.12.PICKUP_1PNT

Y=$1; Y=${Y:-2022}; TIME="00Z01JAN$Y 00Z31DEC$Y"
VAR=LHF
LONW=123; LATS=21

INROOT=/work01/DATA/J-OFURO3/V1.2_PRE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/LHF

RUN=RAW; INDIR1=${INROOT}/${RUN}_RUN
INFLE1=J-OFURO3_COR3_LHF_${RUN}_RUN_DAILY_${Y}.nc

RUN=CLM; INDIR2=${INROOT}/${RUN}_RUN
INFLE2=J-OFURO3_COR3_LHF_${RUN}_RUN_DAILY_${Y}.nc

RUN=WND; INDIR3=${INROOT}/${RUN}_RUN
INFLE3=J-OFURO3_COR3_LHF_${RUN}_RUN_DAILY_${Y}.nc

RUN=SST; INDIR4=${INROOT}/${RUN}_RUN
INFLE4=J-OFURO3_COR3_LHF_${RUN}_RUN_DAILY_${Y}.nc

RUN=QA; INDIR5=${INROOT}/${RUN}_RUN
INFLE5=J-OFURO3_COR3_LHF_${RUN}_RUN_DAILY_${Y}.nc

IN1=${INDIR1}/${INFLE1}; if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi
IN2=${INDIR2}/${INFLE2}; if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi
IN3=${INDIR3}/${INFLE3}; if [ ! -f $IN3 ];then echo NO SUCH FILE,$IN3;exit 1;fi
IN4=${INDIR4}/${INFLE4}; if [ ! -f $IN4 ];then echo NO SUCH FILE,$IN4;exit 1;fi
IN5=${INDIR5}/${INFLE5}; if [ ! -f $IN5 ];then echo NO SUCH FILE,$IN5;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_JOF3_${VAR}_${LONW}_${LATS}_${Y}.PDF

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
UNIT="W/m\`a2\`n"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"
TEXT="J-OFURO3_V1.2PRE LHF RECOMPUTE COR3_0af"

cat << EOF > ${GS}

'sdfopen ${IN1}'; 'sdfopen ${IN2}'; 'sdfopen ${IN3}'; 'sdfopen ${IN4}'; 
'sdfopen ${IN5}'; 

xmax = 1; ymax = 1
ytop=9
xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grads off';'set grid off'

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW}';'set lat ${LATS}' ;# 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'
'set dfile 1'


'set vrange -50 600';'set ylint 100'
'set ccolor 16';'set cthick 5';'set cmark 0'
'd (LHF.3-LHF.2)+(LHF.4-LHF.2)+(LHF.5-LHF.2)+LHF.2' ;# ANO+CLM
'set xlab off';'set ylab off'

'set ccolor 1';'set cthick 2';'set cmark 0'
'd LHF.1' ;#RAW

'set ccolor 4';'set cthick 2';'set cmark 0'
'd LHF.2' ;#CLM

'set ccolor 3';'set cthick 2';'set cmark 0'
'd LHF.3' ;#WND

'set ccolor 2';'set cthick 2';'set cmark 0'
'd LHF.4' ;#SST

'set ccolor 9';'set cthick 2';'set cmark 0'
'd LHF.5' ;#QA

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=xl-0.7; y=(yb+yt)/2
'set strsiz 0.15 0.18'; 'set string 1 c 3 90'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5
'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

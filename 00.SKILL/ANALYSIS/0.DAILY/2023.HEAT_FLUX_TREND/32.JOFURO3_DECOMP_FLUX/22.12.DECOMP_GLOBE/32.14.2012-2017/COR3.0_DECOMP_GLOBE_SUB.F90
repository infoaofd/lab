! C:\Users\boofo\Dropbox\TOOLS_TEMP\COARE\COARE-algorithm-master\Fortran\COARE3.0\f90
function psiuo(zet)
    x=(1.-15.*zet)**.25 
    psik=2.*log((1.+x)/2.)+log((1.+x*x)/2.)-2.*atan(x)+2.*atan(1.) 
    x=(1.-10.15*zet)**.3333 
    psic=1.5*log((1.+x+x*x)/3.)-sqrt(3.)*atan((1.+2.*x)/sqrt(3.))+4.*atan(1.)/sqrt(3.) 
    f=zet*zet/(1+zet*zet) 
    psiuo=(1-f)*psik+f*psic                                                
    if(zet>0)then 
      c=min(50.,.35*zet) 
      psiuo=-((1+1.0*zet)**1.0+.667*(zet-14.28)/exp(c)+8.525)
    endif 
END FUNCTION psiuo 



function psit_30(zet)
    x=(1.-(15*zet))**.5 
    psik=2*log((1+x)/2) 
    x=(1.-(34.15*zet))**.3333 
    psic=1.5*log((1.+x+x*x)/3.)-sqrt(3.)*atan((1.+2.*x)/sqrt(3.))+4.*atan(1.)/sqrt(3.) 
    f=zet*zet/(1+zet*zet) 
    psit_30=(1-f)*psik+f*psic   
   
    if(zet>0)then 
      c=min(50.,.35*zet) 
      psit_30=-((1.+2./3.*zet)**1.5+.6667*(zet-14.28)/exp(c)+8.525)
   endif
end FUNCTION psit_30



function qsee(ts,Pa)
real :: ts,Pa
x=ts
p=Pa
es=6.112*exp(17.502*x/(x+240.97))*.98*(1.0007+3.46e-6*p)
qsee=es*621.97/(p-.378*es)
end function



function grv(lat)
real lat
gamma=9.7803267715
c1=0.0052790414
c2=0.0000232718
c3=0.0000001262
c4=0.0000000007
pi=3.141593

phi=lat*pi/180
x=sin(phi)
grv=gamma*(1+(c1*x**2)+(c2*x**4)+(c3*x**6)+(c4*x**8))
!print *,'grav=',grv,lat
end function grv



subroutine caldat(JULIAN,IYYY,MONTH,DD) 
      INTEGER,intent(in):: JULIAN
      integer,intent(out)::IYYY,MONTH,DD 
      INTEGER IGREG 
      PARAMETER (IGREG=2299161) 
!                                                                                              
!                                                                       
      INTEGER JALPHA,JA,JB,JC,JD,JE 
                                                                      
      IF (JULIAN .GE. IGREG) THEN 
        JALPHA = INT(((JULIAN - 1867216) - 0.25)/36524.25) 
        JA = JULIAN + 1 + JALPHA - INT(0.25*JALPHA) 
      ELSE 
        JA = JULIAN 
      ENDIF 
      JB = JA + 1524 
      JC = INT(6680. + ((JB - 2439870) - 122.1)/365.25) 
      JD = 365*JC + INT(0.25*JC) 
      JE = INT((JB - JD)/30.6001) 
      DD = JB - JD - INT(30.6001*JE) 
      MONTH = JE - 1 
      IF (MONTH .GT. 12) MONTH = MONTH - 12 
      IYYY = JC - 4715 
      IF (MONTH .GT. 2) IYYY = IYYY - 1 
      IF (IYYY .LE. 0) IYYY = IYYY - 1 
      RETURN 
end subroutine caldat



INTEGER FUNCTION JULDAY(IYYY,MONTH,DD)                                                                                                                                         
      INTEGER,intent(inout):: IYYY,MONTH,DD
                                                                     
      INTEGER IGREG 
      PARAMETER (IGREG = 15 + 31*(10 + 12*1582)) 
!                                                                                                 
!                                                                       
      INTEGER JY,JM,JA 
!                                                                                                                                            
      IF (IYYY .LT. 0) IYYY = IYYY + 1 
      IF (MONTH .GT. 2) THEN 
        JY = IYYY 
        JM = MONTH + 1 
      ELSE 
        JY = IYYY - 1 
        JM = MONTH + 13 
      ENDIF 
      JULDAY = INT(365.25*JY) + INT(30.6001*JM) + DD + 1720995 
      IF (DD + 31*(MONTH + 12*IYYY) .GE. IGREG) THEN 
        JA = INT(0.01*JY) 
        JULDAY = JULDAY + 2 - JA + INT(0.25*JA) 
      ENDIF 
      RETURN 

end function julday

Sat, 25 Nov 2023 14:56:50 +0900
2023-12-27_14-58

MMMMM
MMMMM DAILY CLIMATOLOGY
MMMMM
$ ls /work01/DATA/J-OFURO3/V1.2_PRE/DAILY_CLIM
DAILY_CLIM_SMO_1988-2022_QA.nc  DAILY_CLIM_SMO_1988-2022_SST.nc   DAILY_CLIM_SMO_1988-2022_WND.nc
DAILY_CLIM_SMO_1988-2022_QS.nc  DAILY_CLIM_SMO_1988-2022_TA10.nc

/work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/22.12.PREPROC/22.12.DAILY_CLIM_SMTH
$ ncdump -h /work01/DATA/J-OFURO3/V1.2_PRE/DAILY_CLIM/DAILY_CLIM_SMO_1988-2022_WND.nc

        year_day = UNLIMITED ; // (366 currently)
        latitude = 720 ;
        longitude = 1440 ;

MMMMM
MMMMM DAILY DATA
MMMMM
$ ls /work01/DATA/J-OFURO3/V1.2_PRE/HOURS.SINCE.1800-01-01
LHF/  QA/  QS/  SST/  TA10/  WND/

$ ncdump -h /work01/DATA/J-OFURO3/V1.2_PRE/HOURS.SINCE.1800-01-01/LHF/J-OFURO3_LHF_HR_V1.4.1_DAILY_1988
_18000101.nc 
netcdf J-OFURO3_LHF_HR_V1.4.1_DAILY_1988_18000101 {
dimensions:
        time = 366 ;
        latitude = 720 ;
        longitude = 1440 ;

$ ncdump -h /work01/DATA/J-OFURO3/V1.2_PRE/HOURS.SINCE.1800-01-01/LHF/J-OFURO3_LHF_HR_V1.4.1_DAILY_1989_18000101.nc 
netcdf J-OFURO3_LHF_HR_V1.4.1_DAILY_1989_18000101 {
dimensions:
        time = 365 ;


! C:\Users\boofo\Dropbox\TOOLS_TEMP\COARE\COARE-algorithm-master\Fortran\COARE3.0\f90
PROGRAM COR3_0af_JOF3
!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
!TOGA COARE BULK FLUX MODEL VERSION 3.0
!FOR J-OFURO3_V1.2PRE
!USES FOLLOWING SUBROUTINES:
! COR30A.F90
! SUB_COR3_0AF.F90
!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
use netcdf
use IO_INFO
use PARAM

IMPLICIT NONE

! FOR NetCDF FILE
real,ALLOCATABLE,DIMENSION(:,:,:) :: var2d
real,ALLOCATABLE,DIMENSION(:,:,:) :: QAI,QSI,SSTI,TA10I,WNDI !INPUT(RAW)
real,ALLOCATABLE,DIMENSION(:,:,:) :: SSTC,TA10C,WNDC,QAC     !INPUT(CLM)
real,ALLOCATABLE,DIMENSION(:,:,:) :: LHF,SHF !OUTPUT
real(8),ALLOCATABLE::lonI(:),latI(:)
INTEGER,ALLOCATABLE,DIMENSION(:)::timeI
INTEGER,PARAMETER::MV=5, MVO=2,MVCLM=4
CHARACTER(LEN=100)::VNAME(MV),VNAMEO(MVO),VCLM(MVCLM),VNAME_CLM,VRUN
DATA VNAME/"QA","QS","SST","TA10","WND"/
DATA VCLM/"SST","TA10","WND","QA"/
DATA VNAMEO/"LHF","SHF"/


! FOR COMPUTING SURFACE FLUXES
integer xin, ibg, indx(500), jdx(500)
real arrout(1000,13), qsx(500), tsx(500), locx(500), dt(500), hwave
real :: x(19), y(500), arnl(500), Hrain(500), hnet(500), hs(500), hl(500), tau(500),hl_webb(500)
real :: u,tsnk,ta,qa,rs,rl,org,lat,lon,msp

real :: al,cd,cdn_10,ce,cen_10,ch,chktime,chn_10,cpv,ctd1,ctd2
real :: grav
integer :: i,iday,ihr,imin,isec,iyr,l,le,mon
INTEGER::JLDY
INTEGER, EXTERNAL::JULDAY

REAL::dtime
real :: loc,lonx,newtime,q,qjoule,qr_out,qs,qsr,rain,rf
REAL::dqer
real :: rhoa,rich,rnl,rns,taub,time,intime,tkt,t,ts,tsea
real :: tsr,twave,usr,visa,wbar,wg,zo,zoq,zot
REAL::hlb,hsb
double precision :: jdy,st
real, external :: grv, qsee

INTEGER J,N

namelist /para/INDIR,INDCLM,ODIR,YI,NV,IM,JM,NM,NVO,VRUN

READ(5,para)

ALLOCATE(timeI(NM))
ALLOCATE(lonI(IM),latI(JM))
ALLOCATE(var2d(IM,JM,NM),QAI(IM,JM,NM),QSI(IM,JM,NM),SSTI(IM,JM,NM),&
TA10I(IM,JM,NM),WNDI(IM,JM,NM))
ALLOCATE(SSTC(IM,JM,NM),TA10C(IM,JM,NM),WNDC(IM,JM,NM),QAC(IM,JM,NM))
ALLOCATE(LHF(IM,JM,NM),SHF(IM,JM,NM))

WRITE(YYYY,'(I4)') YI
INFLE="J-OFURO3_"//TRIM(VNAME(1))//"_MULTI_V1.6.2_DAILY_"//YYYY//"_18000101.nc"
IN=TRIM(INDIR) // '/' // TRIM(VNAME(1)) // '/' //TRIM(INFLE)

print '(A)',trim(IN)

CALL READ_NC1FLT("time", IN, NM, timeI) !; print *,timeI;stop
CALL READ_NC1DBL("longitude", IN, IM, lonI)
CALL READ_NC1DBL("latitude", IN, JM, latI)

print '(A)','MMMMM';print '(A,A,A)','MMMMM READ DAILY RAW DATA';print '(A)','MMMMM';
DO I=1,NV
var2d=0.0
WRITE(YYYY,'(I4)') YI

IF(TRIM(VNAME(I))=="QA")&
INFLE="J-OFURO3_"//TRIM(VNAME(I))//"_MULTI_V1.6.2_DAILY_"//YYYY//"_18000101.nc"

IF(TRIM(VNAME(I))=="QS")&
INFLE="J-OFURO3_"//TRIM(VNAME(I))//"_HR_V1.4.1_DAILY_"//YYYY//"_18000101.nc"

IF(TRIM(VNAME(I))=="SST")&
INFLE="J-OFURO_EM_"//TRIM(VNAME(I))//"_V0.5.1_DAILY_"//YYYY//"_18000101.nc"

IF(TRIM(VNAME(I))=="TA10")&
INFLE="J-OFURO3_"//TRIM(VNAME(I))//"_HR_V1.4.1_DAILY_"//YYYY//"_18000101.nc"

IF(TRIM(VNAME(I))=="WND")&
INFLE="J-OFURO3_"//TRIM(VNAME(I))//"_V0.6.6_DAILY_"//YYYY//"_18000101.nc"

IN=TRIM(INDIR) // '/' // TRIM(VNAME(I)) // '/' //TRIM(INFLE)

CALL READ_NC2(VNAME(I), IN, IM,JM,NM,var2d)

IF(VNAME(I)=="QA")QAI=var2d;   IF(VNAME(I)=="QS")QSI=var2d
IF(VNAME(I)=="SST")SSTI=var2d; IF(VNAME(I)=="TA10")TA10I=var2d
IF(VNAME(I)=="WND")WNDI=var2d

END DO !I



print '(A)','MMMMM';print '(A,A,A)','MMMMM READ DAILY CLIMATOLOGY';print '(A)','MMMMM';
DO I=1,MVCLM
WRITE(YYYY,'(I4)') YI
INFCLM="DAILY_CLIM_SMO_1988-2022_"//TRIM(VCLM(I))//".nc"
INCLM=TRIM(INDCLM) // '/' //TRIM(INFCLM)
VNAME_CLM=TRIM(VCLM(I))//"_sm"
PRINT '(A)',TRIM(VNAME_CLM)
CALL READ_NC2_CLM(TRIM(VNAME_CLM), INCLM, IM,JM,NM,var2d)
IF(VCLM(I)=="SST") SSTC=var2d
IF(VCLM(I)=="TA10")TA10C=var2d
IF(VCLM(I)=="WND") WNDC=var2d
IF(VCLM(I)=="QA") QAC=var2d
END DO !I

!print '(A,A)','MMMMM COMPUTE FLUXES'
DO N=1,NM

ibg=N
JLDY=JULDAY(YI,1,1)+N-1
CALL CALDAT(JLDY,iyr,mon,iday)
ihr=12; imin=0; isec=0

PRINT '(A,A,A,I4,A,I4)','VRUN=',TRIM(VRUN),' YEAR=',YI,' DAY=',N

DO J=1,JM

DO I=1,IM

!PRINT '(A)','MMMMM SET DAILY CLIM'
 u=WNDC(I,J,N)
tsnk=SSTC(I,J,N)
ta=TA10C(I,J,N)
qa=sngl(QAC(I,J,N))
!PRINT '(A)','MMMMM SET RAW, ',TRIM(VRUN)
if(VRUN=="SST")tsnk=SSTI(I,J,N)
if(VRUN=="TA10")ta=TA10I(I,J,N)
if(VRUN=="WND")u=WNDI(I,J,N)
if(VRUN=="QA")qa=sngl(QAI(I,J,N))
if(VRUN=="RAW")then
tsnk=SSTI(I,J,N)
ta=TA10I(I,J,N)
u=WNDI(I,J,N)
qa=sngl(QAI(I,J,N))
end if !RAW

!qs=sngl(QSI(I,J,N))
!PRINT '(A)','MMMMM COMPUTE qs USING SST (qs=SATURATION SPECIFIC HUMIDITY)'
    qs=qsee(tsnk, P)
rs=0.0; rl=0.0; org=0.0
lat=sngl(latI(J)); lon=sngl(lonI(I))
msp=0.0

IF(u<=FILLVALUE .or. tsnk<=FILLVALUE .or. ta<=FILLVALUE .or. &
qa<=FILLVALUE .OR. QS<=FILLVALUE)THEN

LHF(I,J,N)=FILLVALUE
SHF(I,J,N)=FILLVALUE

CYCLE
END IF

!********   decode bulk met data ****
    if(ibg .eq. 1) ts=tsnk 
    tsea=tsnk !bulk sea surface temp
    t=ta !air temp
    qs=qsee(tsea, P) !bulk sea surface humidity
    q=qa !air humidity
    Rs=0.0 !downward solar flux !FORCED TO BE ZERO
    Rl=0.0 !doward IR flux      !FORCED TO BE ZERO
    rain=org !rain rate
    grav=grv(lat) !9.72 
    lonx=lon !longitude
   
!*****  variables for warm layer  ***
    time=((float(ihr*3600)+float(imin*60))/24.) /3600. 
    intime=time 
    loc=(lonx+7.5)/15 
    locx(ibg)=loc 
    Rnl=.97*(5.67e-8*(ts-dter*jcool+273.16)**4-Rl) !oceanic broadband emissivity=0.97
    arnl(ibg)=Rnl 
    Rns=.945*Rs !oceanic albedo=0.055 daily average
!*********   set condition dependent stuff ******
    Le=(2.501-.00237*tsea)*1e6 
    cpv=cpa*(1+0.84*q/1000) 
    rhoa=P*100/(Rgas*(t+tdk)*(1+0.61*q/1000)) 
    visa=1.326e-5*(1+6.542e-3*t+8.301e-6*t*t-4.84e-9*t*t*t) 
    Al=2.1e-5*(tsea+3.2)**0.79 

! PRINT '(A,i5)','WWWWW WARM LAYER ROUTINE IN cor3_0af.F90 WAS REMOVED SINCE JWARM IS 0.'
! PRINT '(A,i5)','WWWWW JWARM=',JWARM
    ts=tsea+dsea 
    ! qs=qsee(ts, P) 
    qsx(ibg)=qs 
    tsx(20)= 1. !ts 

    twave=b*u 
    hwave=a*u**2.*(1+.015*u)

!PRINT '(A)','MMMMM CREATE DATA FOR cor30a'
!PRINT *,'A',u, us, ts, t, qs, q, Rs, Rl, rain, zi,  P, zu, zt, zq, lat, jcool, jwave, twave, hwave
    x=(/u, us, ts, t, qs, q, Rs, Rl, rain, zi,  P, zu, zt, zq, lat, jcool, jwave, twave, hwave/)!set data for basic flux alogithm

!PRINT '(A)','MMMMM COMPUTE FLUXES USING COARE3.0'
    CALL cor30a(x,y) 

        hsb=y(1)                    !sensible heat flux W/m/m
        hlb=y(2)                    !latent
        taub=y(3)                   !stress
        zo=y(4)                     !vel roughness
        zot=y(5)                    !temp "
        zoq=y(6)                    !hum  "
        L=y(7)                      !Ob Length
        usr=y(8)                    !ustar
        tsr=y(9)                    !tstar
        qsr=y(10)                   !qstar  [g/g]
        dter=y(11)                  !cool skin delta T
        dqer=y(12)                  !cool skin delta q
        tkt=y(13)                   !thickness of cool skin
        RF=y(14)                    !rain heat flux
        wbar=y(15)                  !webb mean w     
        Cd=y(16)                    !drag @ zu
        Ch=y(17)                    !
        Ce=y(18)                    !Dalton
        Cdn_10=y(19)                !neutral drag @ 10 [includes gustiness]
        Chn_10=y(20)                !
        Cen_10=y(21)                !
        Wg=y(22) 

LHF(I,J,N)=hlb
SHF(I,J,N)=hsb

END DO !N
END DO !J
END DO !I

!print '(A,A)','MMMMM OUTPUT'
DO I=1,NVO
var2d=0.0
IF(I==1)var2d=LHF; IF(I==2)var2d=SHF
OFLE="J-OFURO3_COR3_"//TRIM(VNAMEO(I))//"_"//TRIM(VRUN)//"_RUN_DAILY_"//YYYY//".nc"
OUT=TRIM(ODIR) // '/' // TRIM(VNAMEO(I)) // '/' // TRIM(VRUN) // '_RUN/' //TRIM(OFLE)
PRINT '(A,A)','MMMMM OUTPUT: ',TRIM(OUT)
CALL WRITE_NC(VNAMEO(I), OUT, IM, JM, NM, var2d, timeI, latI, lonI, INDIR, INFLE, ODIR, OFLE)
END DO !I

END PROGRAM COR3_0af_JOF3

 !  jdy=x(xin,1) !time in the form YYYYMMDDHHSS.SS
 !  U=x(xin,2)  !true wind speed, m/s  etl sonic anemometer
 !  tsnk=x(xin,3) !sea snake temperature, C (0.05 m depth)
 !  ta=x(xin,4) !air temperature, C (z=14.5 m)
 !  qa=x(xin,5) !air specific humidity, g/kg (z=14.5  m)
 !  rs=x(xin,6) !downward solar flux, W/m^2 (ETL units)
 !  rl=x(xin,7) !downward IR flux, W/m^2 (ETL units)
 !  org=x(xin,8) !rainrate, mm/hr (ETL STI optical rain gauge, uncorrected)
 !  lat=x(xin,9) !latitude, deg  (SCS pcode)
 !  lon=x(xin,10) !longitude, deg (SCS pcode)
 !  msp=x(xin,11) !6-m deotg T from MSP, C    


!*********** basic specifications  *****
!	zu=			height of wind measurement
!	zt=			height of air temperature measurement
!	zq=			height of air humidity measurement
!	ts_depth	depth of water temperature measurement
!	jwarm=		0=no warm layer calc, 1 =do warm layer calc
!	jcool=		0=no cool skin calc, 1=do cool skin calc
!   jwave=      0= Charnock, 1=Oost et al, 2=Taylor and Yelland

!***********   input data **************
!	YYYYMMHHMMSS=		date in toga coare format, Y2K version
!	u=			wind speed (m/s), height zu
!	us=			surface current (m/s)
!	ts=			bulk surface sea temp (cent)
!	t=			air temp (cent), height zt
!	qs=			sea surface sat specific humidity (g/kg)
!	q=			air specific humidity (g/kg), height zq
!	Rs=			downward solar flux (w/m^2)
!	Rl=			downward IR flux (w/m^2)
!	zi=			inversion height (m)
!	P=			air pressure (mb)
!	rain=		rain rate (mm/hr)
!	lon=		longitude (deg E=+)
!	lat=		latitude (deg N=+)


!********** output data  ***************
!	hsb=			sensible heat flux (w/m^2)
!	hlb=			latent heat flux (w/m^2)
!	RF=			rain heat flux(w/m^2)
!	wbar=	   	webb mean w (m/s)
!	tau=			stress (nt/m^2)
!	zo=			velocity roughness length (m)
!	zot			temperature roughness length (m)
!	zoq=			moisture roughness length (m)
!	L=			Monin_Obukhov stability length
!	usr=			turbulent friction velocity (m/s), including gustiness
!	tsr			temperature scaling parameter (K)
!	qsr			humidity scaling parameter (g/g)
!	dter=			cool skin temperature depression (K)
!	dqer=			cool skin humidity depression (g/g)
!	tkt=			cool skin thickness (m)
!	Cd=			velocity drag coefficient at zu, referenced to u
!	Ch=			heat transfer coefficient at zt
!	Ce=			moisture transfer coefficient at zq
!	Cdn_10=			10-m velocity drag coeeficient, including gustiness
!	Chn_10=			10-m heat transfer coeeficient, including gustiness
!	Cen_10=			10-m humidity transfer coeeficient, including gustiness
!

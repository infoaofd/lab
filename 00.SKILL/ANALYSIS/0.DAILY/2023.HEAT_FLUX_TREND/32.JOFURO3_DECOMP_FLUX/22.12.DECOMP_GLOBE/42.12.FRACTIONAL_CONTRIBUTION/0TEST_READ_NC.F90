PROGRAM 
! Mon, 08 Jan 2024 10:51:36 +0900
! localhost.localdomain
! /work09/am/00.WORK/2023.HEAT_FLUX_TREND/32.JOFURO3_DECOMP_FLUX/22.12.DECOMP_GLOBE/42.12.FRACTIONAL_CONTRIBUTION

!IMPLICIT NONE
!
!CHARACTER INDIR*500,INFLE*500,IN*1000,ODIR*500,OFLE*500,&
!OUT*1000

!INTEGER
!INTEGER,ALLOCATABLE,DIMENSION(:)::
!REAL
!REAL,ALLOCATABLE,DIMENSION(:)::

!REAL,PARAMETER::UNDEF=

!NAMELIST /PARA/
!READ(*,NML=PARA)

!ALLOCATE()

PRINT *

IN=TRIM(INDIR)//'/'//TRIM(INFLE)

INFLE="J-OFURO3_"//TRIM(VNAME(1))//"_MULTI_V1.6.2_DAILY_"//YYYY//"_18000101.nc"
IN=TRIM(INDIR) // '/' // TRIM(VNAME(1)) // '/' //TRIM(INFLE)

print '(A)',trim(IN)

CALL READ_NC1FLT("time", IN, NM, timeI)


END PROGRAM

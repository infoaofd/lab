! C:\Users\boofo\Dropbox\TOOLS_TEMP\COARE\COARE-algorithm-master\Fortran\COARE3.0\f90

MODULE IO_INFO
CHARACTER(LEN=500)INDIR, INFLE, INDCLM, INFCLM
CHARACTER(LEN=500)ODIR, OFLE
CHARACTER(LEN=1000)IN*1000, INCLM, OUT*1000

REAL,PARAMETER::FILLVALUE=-9999.0
INTEGER::IM,JM,NM,NV,NVO
INTEGER::YI
CHARACTER(4)::YYYY
END MODULE IO_INFO



MODULE PARAM
REAL::zu=10. !15 !anemometer ht
REAL::zt=10. !15 !air T height
REAL::zq=10. !15 !humidity height
REAL::ts_depth=1.0 !.05 !bulk water temperature sensor depth, ETL seasnake&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
REAL::jcool=0 
REAL::jwarm=0 
REAL::jwave=0 
REAL::icount=1 
!MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
!*********************  housekeep variables  ********   
REAL::qcol_ac=0 
REAL::tau_ac=0 
REAL::jtime=0 
REAL::jamset=0 
REAL::tau_old=.06 
REAL::hs_old=10 
REAL::hl_old=100 
REAL::RF_old=0 
REAL::dsea=0 
REAL::dt_wrm=0 
REAL::tk_pwp=19 
REAL::fxp=.5 
REAL::q_pwp=0 
REAL::jump=1 
!*******************  set constants  ****************
REAL::tdk=273.16 
REAL::Rgas=287.1 
REAL::cpa=1004.67    
REAL::be=0.026 
REAL::cpw=4000 
REAL::rhow=1022 
REAL::visw=1e-6 
REAL::tcw=0.6 
REAL::dter=0.3 
REAL::a=.018 
REAL::b=.729 
!***********   set variables not in data base  ********
REAL::P=1008                      !air pressure
REAL::us=0                        !surface current
REAL::zi=600                     !inversion ht
!******************  setup read data loop  **********
REAL::didread=0
END MODULE PARAM


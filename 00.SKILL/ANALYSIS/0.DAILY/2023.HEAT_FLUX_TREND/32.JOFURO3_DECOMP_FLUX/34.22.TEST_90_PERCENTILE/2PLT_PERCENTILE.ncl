;# /work09/am/00.WORK/2023.HEAT_FLUX_TREND/12.12.JOFURO3_V1.2_MON_SEA_YEAR/12.22.LHF_SEASON_TREND/12.22.TREND_SEASON
begin

DSET="J-OFURO3"
ABB="LHF"
SEASON = getenv("NCL_ARG_2")
RUN = getenv("NCL_ARG_3")
PCNT= toint(getenv("NCL_ARG_4"))

AREA="GLOBE"

FIGDIR="FIG_"+PCNT+"PERCENTILE/"
system("mkdir -vp "+FIGDIR)
FIG=FIGDIR+"J-OFURO3_COR3_DAILY_"+AREA+"_"+PCNT+"PCNT_"+ABB+"_"+RUN+"_RUN_"+SEASON
TYP="PDF"

indir="/work01/DATA/J-OFURO3/V1.2_PRE/32.JOFURO3_DECOMP_FLUX/34.22.TEST_90_PERCENTILE/LHF/"
infle="J-OFURO3_COR3_DAILY_"+PCNT+"PCNT"+"_"+ABB+"_"+RUN+"_RUN_"+SEASON+".nc"
in=indir+infle
a=addfile(in,"r")

print("MMMMM READ DATA")
xMON=a->LHF

lat=a->lat ;g0_lat_1
lon=a->lon ;;g0_lon_2

rad    = 4.0*atan(1.0)/180.0
wgty=cos(lat*rad)
wgty!0="latitude"
wgty&latitude=lat

TIME=a->time ; initial_time0_hours

;print(TIME)

print("MMMMM REORDER DATA")

ts = xMON(lat|:, lon|: ,time|:)
ts!0="lat"
ts!1="lon"
ts!2="time"
ts&lat=lat
ts&lon=lon
ts&time=TIME


print("MMMMM PLOT")
wks = gsn_open_wks(TYP, FIG)
gsn_define_colormap(wks,"precip3_16lev"); "cmp_b2r") ;"NCV_jaisnd") ;BlueWhiteOrangeRed"); "ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      
opt@gsnLeftString=DSET+" "+ABB+" "+RUN+" "+PCNT+"% "+SEASON
opt@gsnCenterString=""
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.05
opt@gsnLeftStringOrthogonalPosF=0.05
opt@gsnRightStringOrthogonalPosF=0.05
opt@gsnLeftStringFontHeightF=0.025
opt@gsnCenterStringFontHeightF=0.025
opt@gsnRightStringFontHeightF=0.025
opt@lbTitleFontHeightF= .02 ;Font size
opt@lbLabelFontHeightF   = 0.01

opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
opt@cnMinLevelValF =  50.
opt@cnMaxLevelValF =  800.
opt@cnLevelSpacingF = 50.
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-200.,-100.,-50.,-40.,-30.,-20.,-10.,0.,10.,20.,30.,40.,50.,100.,200./)
;if (RUN .eq. "RAW")
;opt@cnLevels = (/-100.,-50.,-25.,-20.,-15.,-10.,-5.,0.,5.,10.,15.,20.,25.,50.,100./)
;end if

;opt@pmLabelBarHeightF = 0.41
opt@pmLabelBarWidthF=0.05
;opt@pmLabelBarParallelPosF = 0.48
;opt@lbTopMarginF=-0.01
;opt@lbLabelFontHeightF=0.015
opt@lbTitleString    = "W/m~S~2~N~"                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
;opt@lbTitleDirection = "Across"             ; title direction
opt@lbOrientation = "vertical"

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines
res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = True ;False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpDataBaseVersion = "LowRes" ;-- better map resolution
res@mpLandFillColor = "black"
res@mpCenterLonF = 180

plot=gsn_csm_contour_map(wks,ts(:,:,0),res)

draw(plot)         ; ここでplotを描く

txres=True
txres@txFontHeightF = 0.015
txres@txJust="CenterLeft"
today = systemfunc("date -R")
gsn_text_ndc(wks,today,  0.05,0.90,txres)
cwd =systemfunc("pwd")
gsn_text_ndc(wks,"Current dir: "+cwd,      0.05,0.925,txres)
scriptname  = get_script_name()
gsn_text_ndc(wks,"Script: "+scriptname, 0.05,0.950,txres)

frame(wks)         ; WorkStationの更新

print("FIG: "+FIG+"."+TYP)
end

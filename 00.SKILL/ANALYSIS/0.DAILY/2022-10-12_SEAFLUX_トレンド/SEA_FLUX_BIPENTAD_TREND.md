# SEAFLUX BIPENTAD TREND

https://seaflux.org/data-2

/work03/am/2022.HEAT_FLUX_TREND/04.01.SEAFLUX
2022-10-11_16-28

[[_TOC_]]

## TRIM REGION

CDO_TRIM_REGION.sh 

/work03/am/2022.HEAT_FLUX_TREND/04.01.SEAFLUX
2022-10-11_16-28

```bash
$ cd 20.12.CDO_TRIM_REGION/
```

/work03/am/2022.HEAT_FLUX_TREND/04.01.SEAFLUX/20.12.CDO_TRIM_REGION
2022-10-11_16-59

```bash
$ ls
```

0.README.TXT  CDO_TRIM_REGION.sh*

```bash
$ vi CDO_TRIM_REGION.sh 
```

```bash
$ CDO_TRIM_REGION.sh 
```

/work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988
2022-10-11_17-13

```bash
$ cdo sinfo SEAFLUX-0-50_90-160_D19880831_C20160824.nc
```

```bash
   File format : NetCDF4
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : unknown  SEAFLUX-OSB-CDR_V02R00_ATMOS_D1 v instant       1   1     56000   1  F32  : -1            
     2 : unknown  SEAFLUX-OSB-CDR_V02R00_ATMOS_D1 v instant       1   1     56000   1  F32  : -2            
     3 : unknown  SEAFLUX-OSB-CDR_V02R00_ATMOS_D1 v instant       1   1     56000   1  I8   : -3            
   Grid coordinates :
     1 : lonlat                   : points=56000 (280x200)
                              lon : 90.125 to 159.875 by 0.25 degrees_east
                              lat : 0.125 to 49.875 by 0.25 degrees_north
   Vertical coordinates :
     1 : surface                  : levels=1
   Time coordinate :  8 steps
     RefTime =  1988-01-01 00:00:00  Units = hours  Calendar = gregorian
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-08-31 01:30:00  1988-08-31 04:30:00  1988-08-31 07:30:00  1988-08-31 10:30:00
  1988-08-31 13:30:00  1988-08-31 16:30:00  1988-08-31 19:30:00  1988-08-31 22:30:00
cdo    sinfo: Processed 3 variables over 8 timesteps [0.07s 10MB].
```



## TIME AVE

CDO_TAV_BIPEN.sh

```
/work03/am/2022.HEAT_FLUX_TREND/04.01.SEAFLUX/20.22.CDO_TAV_BIPEN
2022-10-11_22-31
$ CDO_TAV_BIPEN.sh
```

```
$ tree /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160_BIPEN
/work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160_BIPEN
├── 06BP01
│   ├── 0.README.TXT
│   ├── SEAFLUX_1988_06_BP01_0-50_90-160.nc
│   └── SEAFLUX_1989_06_BP01_0-50_90-160.nc
├── 06BP02
│   ├── 0.README.TXT
│   ├── SEAFLUX_1988_06_BP02_0-50_90-160.nc
│   └── SEAFLUX_1989_06_BP02_0-50_90-160.nc
└── 06BP03
    ├── 0.README.TXT
    ├── SEAFLUX_1988_06_BP03_0-50_90-160.nc
    └── SEAFLUX_1989_06_BP03_0-50_90-160.nc
```

```
/work03/am/2022.HEAT_FLUX_TREND/04.01.SEAFLUX/20.22.CDO_TAV_BIPEN
2022-10-11_22-31
$ ncdump -h /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160_BIPEN/06BP01/SEAFLUX_1988_06_BP01_0-50_90-160.nc
netcdf SEAFLUX_1988_06_BP01_0-50_90-160 {
dimensions:
        time = UNLIMITED ; // (1 currently)
        bnds = 2 ;
        lon = 280 ;
        lat = 200 ;
variables:
        float time(time) ;
                time:standard_name = "time" ;
                time:long_name = "Center time of 3-hour period in elapsed-time format" ;
                time:bounds = "time_bnds" ;
                time:units = "hours since 1988-01-01 0:0:0" ;
                time:calendar = "gregorian" ;
                time:axis = "T" ;
        double time_bnds(time, bnds) ;
        float lon(lon) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "Center longitude of square grid cell" ;
                lon:units = "degrees_east" ;
                lon:axis = "X" ;
        float lat(lat) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "Center latitude of square grid cell" ;
                lat:units = "degrees_north" ;
                lat:axis = "Y" ;
        float surface_upward_sensible_heat_flux(time, lat, lon) ;
                surface_upward_sensible_heat_flux:standard_name = "surface_upward_sensible_heat_flux" ;
                surface_upward_sensible_heat_flux:long_name = "NOAA CDR of sensible heat flux" ;
                surface_upward_sensible_heat_flux:units = "W m-2" ;
                surface_upward_sensible_heat_flux:_FillValue = -9999.f ;
                surface_upward_sensible_heat_flux:missing_value = -9999.f ;
                surface_upward_sensible_heat_flux:cell_methods = "time: mean" ;
                surface_upward_sensible_heat_flux:Note = "Positive values represent heat gain to the atmosphere." ;
        float surface_upward_latent_heat_flux(time, lat, lon) ;
                surface_upward_latent_heat_flux:standard_name = "surface_upward_latent_heat_flux" ;
                surface_upward_latent_heat_flux:long_name = "NOAA CDR of latent heat flux" ;
                surface_upward_latent_heat_flux:units = "W m-2" ;
                surface_upward_latent_heat_flux:_FillValue = -9999.f ;
                surface_upward_latent_heat_flux:missing_value = -9999.f ;
                surface_upward_latent_heat_flux:cell_methods = "time: mean" ;
                surface_upward_latent_heat_flux:Note = "Positive values represent heat gain to the atmosphere." ;
        byte fill_missing_qc(time, lat, lon) ;
                fill_missing_qc:long_name = "Quality flag for missing data" ;
                fill_missing_qc:_FillValue = -127b ;
                fill_missing_qc:missing_value = -127b ;
                fill_missing_qc:cell_methods = "time: mean" ;
                fill_missing_qc:flag_values = 0b, 1b, 2b, 3b, 4b, 5b, 6b ;
                fill_missing_qc:flag_meanings = "observation unused_flag snow_ice land_mask lake_mask high_wind_speed no_flux" ;
                fill_missing_qc:comment = "0=observation, 1=unused, 2=snow or ice contamination, 3=over land, 4=over lake, 5=high wind speed, 6=fluxes unresolved" ;

// global attributes:
                :CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
                :Conventions = "CF-1.6" ;
                :source = "SEAFLUX-OSB-CDR_V02R00_ATMOS_D19880601_C20160616.nc, SEAFLUX-OSB-CDR_V02R00_SST_D19880601_C20160819.nc" ;
                :institution = "WHOI/PO: Physical Oceanography Department, Woods Hole Oceanographic Institution" ;
                :title = "NOAA Climate Data Record of Ocean Heat Fluxes" ;
                :Metadata_Conventions = "CF-1.6, Unidata Dataset Discovery v2.0.2, NOAA CDR v1.0, GDS v2.0" ;
                :standard_name_vocabulary = "CF Standard Name Table (v26, 08 November 2013)" ;
                :ID = "SEAFLUX-OSB-CDR_V02R00_FLUX_D19880601_C20160824.nc" ;
                :naming_authority = "gov.noaa.ncdc" ;
                :date_created = "2016-08-24T09:18:00Z" ;
                :license = "No constraints on data access or use." ;
                :summary = "SeaFlux Ocean Surface Bundle (OSB) Climate Data Record (CDR) of Ocean Heat Fluxes" ;
                :cdm_data_type = "Grid" ;
                :processing_level = "NOAA Level 4" ;
                :keywords = "EARTH SCIENCE > OCEANS > OCEAN HEAT BUDGET > HEAT FLUX, EARTH SCIENCE > OCEANS > OCEAN HEAT BUDGET > HEAT FLUX" ;
                :keywords_vocabulary = "NASA Global Change Master Directory (GCMD) Earth Science Keywords, Version 8.0" ;
                :creator_name = "Jeremiah Brown" ;
                :creator_url = "http://www.principalscientific.com" ;
                :creator_email = "Jeremiah@PrincipalScientific.com" ;
                :contributor_name = "Carol Anne Clayson, Jeremiah Brown" ;
                :contributor_role = "Principal Investigator, Processor and author of files to generate the OSB CDR" ;
                :geospatial_lat_min = -90. ;
                :geospatial_lat_max = 90. ;
                :geospatial_lon_min = 0. ;
                :geospatial_lon_max = 360. ;
                :geospatial_lat_units = "degrees_north" ;
                :geospatial_lat_resolution = 0.25f ;
                :geospatial_lon_units = "degrees_east" ;
                :geospatial_lon_resolution = 0.25f ;
                :time_coverage_start = "1988-06-01T01:30:00Z" ;
                :time_coverage_end = "1988-06-01T22:30:00Z" ;
                :time_coverage_duration = "P1D" ;
                :time_coverage_resolution = "PT3H" ;
                :cdr_program = "NOAA Climate Data Record for satellites, FY 2016." ;
                :cdr_variable = "surface_upward_sensible_heat_flux, surface_upward_latent_heat_flux" ;
                :metadata_link = "gov.noaa.ncdc:C00973" ;
                :product_version = "V02R00" ;
                :platform = "none" ;
                :sensor = "none" ;
                :spatial_resolution = "0.25 degree x 0.25 degree; equal-angle grid" ;
                :history = "Tue Oct 11 22:30:50 2022: cdo timmean SEAFLUX_0-50_90-160_198806_MRG_BP01.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160_BIPEN/06BP01/SEAFLUX_1988_06_BP01_0-50_90-160.nc\nTue Oct 11 22:30:49 2022: cdo mergetime /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880601_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880602_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880603_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880604_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880605_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880606_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880607_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880608_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880609_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880610_C20160824.nc SEAFLUX_0-50_90-160_198806_MRG_BP01.nc\nTue Oct 11 17:00:23 2022: cdo sellonlatbox,90,160,0,50 /work01/DATA/OHF_CDR_06-08/1988/SEAFLUX-OSB-CDR_V02R00_FLUX_D19880601_C20160824.nc /work01/DATA/OHF_CDR_NOAA/CUT_0-50_90-160/1988/SEAFLUX-0-50_90-160_D19880601_C20160824.nc" ;
                :CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
}
```


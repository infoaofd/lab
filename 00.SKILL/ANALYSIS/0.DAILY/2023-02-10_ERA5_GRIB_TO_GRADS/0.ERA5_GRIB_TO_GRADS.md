# ERA5 GRIB TO GRADS

## 1. grib2ctl

```
$ grib2ctl.pl /work01/DATA/ERA5/ECS/01HR/2021/08/ERA5_ECS_PRS_01HR_20210813.grib > ERA5_ECS_PRS_01HR.CTL
```



## 2. CTL書き変え

ERA5_ECS_PRS_01HR.CTLを書き変える

- dsetを書き変え

  dset /work01/DATA/ERA5/ECS/01HR/**%y4/%m2**/ERA5_ECS_PRS_01HR_**%y4%m2%d2**.grib

- optionsにtemplateを追加

  options yrev **template**

```
dset /work01/DATA/ERA5/ECS/01HR/%y4/%m2/ERA5_ECS_PRS_01HR_%y4%m2%d2.grib
index ^ERA5_ECS_PRS_01HR_20210813.grib.idx
undef 9.999E+20
title ERA5 PRS 1HR
*  produced by grib2ctl v0.9.12.6
dtype grib 255
options yrev template
ydef 81 linear 20.000000 0.25
xdef 161 linear 95.000000 0.250000
tdef 24 linear 00Z13aug2021 1hr
*  z has 16 levels, for prs
zdef 16 levels
1000 975 950 925 900 875 850 825 800 700 600 500 400 300 250 200
vars 6
PVprs 16 60,100,0 ** (profile) Potential vorticity [K m**2 kg**-1 s**-1]
Qprs 16 133,100,0 ** (profile) Specific humidity [kg kg**-1]
Tprs 16 130,100,0 ** (profile) Temperature [K]
Uprs 16 131,100,0 ** (profile) U velocity [m s**-1]
Vprs 16 132,100,0 ** (profile) V velocity [m s**-1]
Zprs 16 129,100,0 ** (profile) Geopotential [m**2 s**-2]
ENDVARS
```



## 3. gribmap

```
$ gribmap -e -i ERA5_ECS_PRS_01HR.CTL
```

**ポイント**: `-e`と`-i`オプションを付ける
gribmap: opening GRIB file /work01/DATA/ERA5/ECS/01HR/2021/08/ERA5_ECS_PRS_01HR_20210813.grib 
gribmap: reached end of files
gribmap: writing the GRIB1 index file (version 5) 



```
$ ls
```

0.README.TXT  ERA5_ECS_PRS_01HR.CTL  **ERA5_ECS_PRS_01HR_20210813.grib.idx**
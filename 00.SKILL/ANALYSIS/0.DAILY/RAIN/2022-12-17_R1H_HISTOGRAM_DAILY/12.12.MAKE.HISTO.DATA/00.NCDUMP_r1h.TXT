netcdf \0713 {
dimensions:
	lon = 481 ;
	lat = 505 ;
	time = 24 ;
variables:
	float lon(lon) ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:standard_name = "longitude" ;
	float lat(lat) ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:standard_name = "latitude" ;
	float time(time) ;
		time:long_name = "time" ;
		time:standard_name = "time" ;
		time:units = "hours since 2020-07-13 00:00:00+00:00" ;
	short r1h(time, lat, lon) ;
		r1h:scale_factor = 0.006116208155 ;
		r1h:add_offset = 200. ;
		r1h:long_name = "rainfall in 1 hour" ;
		r1h:units = "mm/h" ;
		r1h:standard_name = "rainfall_rate" ;
	short dswrf(time, lat, lon) ;
		dswrf:scale_factor = 0.0205 ;
		dswrf:add_offset = 665. ;
		dswrf:long_name = "Downward Short-Wave Radiation Flux" ;
		dswrf:units = "W/m^2" ;
		dswrf:standard_name = "surface_net_downward_shortwave_flux" ;

// global attributes:
		:Conventions = "CF-1.0" ;
		:history = "created by create_1daync_msm_r1h_dswrf.rb  2020-07-14" ;
}

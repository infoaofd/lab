; https://gitlab.com/infoaofd/lab/-/blob/master/NCL/NCL_TIPS.md

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
NML    = getenv("NCL_ARG_2")

INDIR=systemfunc("grep INDIR "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
INFLE=systemfunc("grep INFLE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
 ODIR=systemfunc("grep  ODIR "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
 OFLE=systemfunc("grep  OFLE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLONW=systemfunc("grep ALONW "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLONE=systemfunc("grep ALONE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLATS=systemfunc("grep ALATS "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLATN=systemfunc("grep ALATN "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
DOMAIN=systemfunc("grep DOMAIN "+NML+ "|cut -f2 -d'='|cut -f1 -d','")

ALONW=tofloat(CLONW) ; DOMAIN
ALONE=tofloat(CLONE)
ALATS=tofloat(CLATS)
ALATN=tofloat(CLATN)

IN=INDIR+"/"+INFLE
a=addfile(IN,"r")

r1h_in = short2flt( a->r1h(:,:,:) )  
lon = a->lon
lat_in = a->lat
time=a->time

lat=lat_in(::-1)
r1h=r1h_in(:,::-1,:)

;print(r1h@_FillValue)

LAT1  = ind(lat.ge.ALATS.and.lat.lt.ALATN)      
LON1  = ind(lon.ge.ALONW.and.lon.le.ALONE)   
r1h_cut=r1h(lat|LAT1,lon|LON1,time|:)
; https://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Examples/svd

lat_cut=lat(LAT1)
lon_cut=lon(LON1)


;printVarSummary(r1h)
;print("")
;printVarSummary(r1h_cut)

r1h_1d=ndtooned(r1h_cut)
;https://www.ncl.ucar.edu/Document/Functions/Built-in/ndtooned.shtml

OUT=ODIR+"/"+OFLE

dim=dimsizes(time)
utc_date0 = cd_calendar(time(0), 0)
utc_date1 = cd_calendar(time(dim(0)-1), 0)

y0  = tointeger(utc_date0(:,0))    ; Convert to integer for
m0  = tointeger(utc_date0(:,1))    ; use sprinti 
d0  = tointeger(utc_date0(:,2))
h0  = tointeger(utc_date0(:,3))
date0 = sprinti("%0.4i",y0)+sprinti("%0.2i",m0)+sprinti("%0.2i",d0)+\
sprinti("_%0.2i",h0)
y1  = tointeger(utc_date1(:,0))    ; Convert to integer for
m1  = tointeger(utc_date1(:,1))    ; use sprinti 
d1  = tointeger(utc_date1(:,2))
h1  = tointeger(utc_date1(:,3))
date1 = sprinti("%0.4i",y1)+sprinti("%0.2i",m1)+sprinti("%0.2i",d1)+\
sprinti("_%0.2i",h1)

r1h_1d@input_file=IN
r1h_1d@output_file=OUT
r1h_1d@cwd=systemfunc("pwd")
r1h_1d@now=systemfunc("date -R")
r1h_1d@cmd=scriptname
r1h_1d@time=date0+" "+date1
r1h_1d@domin=DOMAIN
r1h_1d@area=ALONW+" "+ALONE+" "+ALATS+" "+ALATN
;printVarSummary(r1h_1d)

system("rm -vf "+OUT)
setfileoption("nc","Format","LargeFile")
b=addfile(OUT,"c")

b->r1h_1d=r1h_1d



FIGDIR="FIG_R1H_CUT/"+DOMAIN
system("mkdir -vp "+FIGDIR)
FIGFLE=systemfunc("basename "+OFLE+" .nc")
FIG=FIGDIR+"/"+FIGFLE
TYP="pdf"

r1d=dim_sum_n(r1h_cut,2)
;https://www.ncl.ucar.edu/Document/Functions/Built-in/dim_sum_n.shtml
r1d!0="lat"
r1d&lat=lat_cut
r1d!1="lon"
r1d&lon=lon_cut

;printVarSummary(r1d)

wks = gsn_open_wks(TYP,FIG)

res = True
res@gsnAddCyclic = False
res@gsnDraw          = False                ; turn off draw and frame
res@gsnFrame         = False                ; b/c this is an overlay plot

opts = res

opts@mpMinLonF            = min(r1d&lon)               ; select a subregion
opts@mpMaxLonF            = max(r1d&lon)
opts@mpMinLatF            = min(r1d&lat)
opts@mpMaxLatF            = max(r1d&lat)

;opts@mpDataBaseVersion    = "HighRes" ;"MediumRes" ; fill land in gray and ocean in transparent
opts@mpFillOn = True ;False; 
;opts@mpFillColors = (/"background","transparent","gray","transparent"/)
;opts@mpFillDrawOrder = "PostDraw"

opts@gsnLeftString="MSM.MA"
opts@gsnCenterString=""
opts@gsnRightString=y0+"-"+m0+"-"+d0

opts@cnFillOn = True
opts@cnLinesOn            = False            ; turn off contour lines

opts@lbTitleString = "mm/h"
opts@lbTitlePosition = "bottom"
opts@lbTitleFontHeightF = 0.015

opts@pmLabelBarOrthogonalPosF = 0.1
opts@pmLabelBarHeightF = 0.08

COLORMAP="precip2_17lev"
gsn_define_colormap(wks,COLORMAP)

opts@cnLevelSelectionMode = "ExplicitLevels"
opts@cnLevels = (/20, 100, 125, 150, 175, 200, 225, 250/)

plot = gsn_csm_contour_map(wks,r1d,opts)

draw(plot)
frame(wks)


print("")
print("MMMMM INPUT:  "+IN)
print("MMMMM OUTPUT: "+OUT)
print("MMMMM FIG="+FIG+"."+TYP)
print("MMMMM DOMAIN: "+ALONW+" "+ALONE+" "+ALATS+" "+ALATN)
print("")

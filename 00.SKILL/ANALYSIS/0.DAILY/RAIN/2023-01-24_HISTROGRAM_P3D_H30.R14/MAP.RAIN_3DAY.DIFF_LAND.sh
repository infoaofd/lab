#!/bin/bash

usage(){
cat <<EOF

Usage: $0 -r <runname1> -s <runname2> -d <domain>

runname: run name (e.g., KE13.G.t1.OS.01.01)
domain : model domain (e.g., d02)

EOF
}

flagh=false
flagr=false
flagd=false

while getopts r:s:d:h OPT; do
  case $OPT in
    "r" ) flagr="true" ; runname1="$OPTARG" ;;
    "s" ) flagr="true" ; runname2="$OPTARG" ;;
    "d" ) flagd="true" ; domain="$OPTARG" ;;
    "h" ) flagh="true" ;;
     *  ) usage
  esac
done
if [ $flagh = "true" ]; then
usage
exit 0
fi

gs=$(basename $0 .sh).gs

yyyy=2018
mm=07
ds=05
de=08
dd=$(printf %d $ds)

hs=00
he=23

# FOR MAP
lonw=128.744  #127.5
lone=138.341  #132.5
lats=30.9605  #30
latn=37.4229  #34.5

# FOR AREA AVE
alonw=128.8  #127.5
alone=138.3  #132.5
alats=31.0   #30
alatn=37.4   #34.5

indir_root=/work06/manda/ARWpost
indir1=${indir_root}/ARWpost_${runname1}
indir2=${indir_root}/ARWpost_${runname2}

p_or_z=p

figdir=FIG_$(basename $0 .sh)
mkdir -vp $figdir

figfile=${figdir}/$(basename $0 .sh)_${domain}.${runname1}-${runname2}.eps



ctl1=${indir1}/${runname1}.${domain}.p.ctl
ctl2=${indir2}/${runname2}.${domain}.p.ctl

if [ ! -f $ctl1 ]; then
echo ERROR : NO SUCH FILE:
echo $ctl1
exit 1
fi
if [ ! -f $ctl2 ]; then
echo ERROR : NO SUCH FILE:
echo $ctl2
exit 1
fi

kind='darkblue->skyblue->white->orange->darkred'
clev='-200 -150 -100 -50 -5 5 50 100 150 200'

xcbar_para=' -edge circle -line on -ft 2'



export LANG=C
host=$(hostname)
cwd=$(pwd)
timestamp=$(date -R)



cat <<EOF>$gs
'cc'

'open ${ctl1}'
'open ${ctl2}'


yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif
if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif
if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif
if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif
if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif
if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif
if(mm='12');mmm='DEC';endif

dd=${dd}

#'set lon '${lonw}' '${lone}
#'set lat '${lats}' '${latn}
'set mpdset 'hires


xmax = 1 ;*5
ymax = 1 ;#4

xwid = 9.0/xmax
ywid = 4/ymax ;#5.5/ymax

nmap = 1
ymap = 1



while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

say 'A dd='dd
dp=dd+3
say 'B dp='dp

say
datetime1=00'Z'dd''mmm''yyyy
datetime2=00'Z'dp''mmm''yyyy


'set time 'datetime1
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t1=subwrd(line,9)
say dtcheck' 't1

'set time 'datetime2
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t2=subwrd(line,9)
say dtcheck' 't2

xs = 0.8 + (xwid+0.10)*(xmap-1)
xe = xs + xwid
ye = 7.0 - (ywid+0.30)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

if (ymap = ymax)
'set xlopts 1 2 0.12'
else
'set xlopts 1 2 0.0'
endif
if (xmap = 1)
'set ylopts 1 2 0.12'
else
'set ylopts 1 2 0.0'
endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='130 132 134 136 138'
'set xlevs 'xlevs
#'set xlint 2.5'
'set ylint 2'
'set grid off'


'color -levs ${clev} -gxout shaded -kind ${kind}'


'set t 't1
rb'='RAINC.1'+'RAINNC.1
'set t 't2
rf'='RAINC.1'+'RAINNC.1
rr1'='rf'-'rb

#'HGTp1=HGT-0.01'
'HGTp1=-XLAND+1.9'
rrland1'=maskout('rr1',HGTp1)'

'set t 't1
rb'='RAINC.2'+'RAINNC.2
'set t 't2
rf'='RAINC.2'+'RAINNC.2
rr2'='rf'-'rb

rrland2'=maskout('rr2',HGTp1)'

drr'='rr1'-'rr2
drrland'='rrland1'-'rrland2

'set mpdraw off'

'd 'drrland
#'d 'drr

'set mpdraw on'
'set map 1 1 1'
'draw shp JPN_adm1'

#'markplot 130.6655  33.4234 -c 1 -m 2 -s 0.1'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*
xx = xl+0.0
yy = yt+0.2
'set string 1 l 2 0'
'set strsiz 0.12 0.15'
if(dd < 10);do='0'dd;endif
if(dd >  9);do=dd   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


#title=hh'-'hp'UTC'
title=datetime1'-'datetime2' ${domain}' ;#do ;#hh'UTC'
'draw string 'xx' 'yy' 'title

xx=5.2
yy=7.5 ;#7.8
'set string 1 c 2 0'
'set strsiz 0.16 0.2'
#title='${dd}'mmm''yyyy
#'draw string 'xx' 'yy' 'title
'draw string 'xx' 'yy' ${runname1}-${runname2}'

xx1=xr+0.2
xx2=xr+0.3
yy1=yb
yy2=yt
'color -levs ${clev}  -gxout shaded -kind ${kind} -xcbar 'xx1' 'xx2' 'yy1' 'yy2' ${xcbar_para}'


say '=================================='
say ' AREA AVE'
say
say ' lonwa = $alonw   lonea = $alone'
say ' latsa = $alats   latna = $alatn'
say
say '=================================='

say 'WRITE AREA AVE OVER LAND CNTL'
say
rave'=aave(' rrland1',lon=${alonw},lon=${alone},lat=${alats},lat=${alatn})'
'd 'rave
raveout=subwrd(result,4)
raveoutint= math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

xx = xr-3.3
yy = yb+0.75
'set string 1 l 1 -1'

'draw string 'xx' 'yy' MEAN [${alonw}-${alone},${alats}-${alatn}]'

yy = yy-0.2
'draw string 'xx' 'yy' CNTL(L) 'raveoutint' mm'

say
say 'WRITE AREA AVE OVER LAND'
say
rave'=aave(' rrland2',lon=${alonw},lon=${alone},lat=${alats},lat=${alatn})'
'd 'rave
raveout=subwrd(result,4)
raveoutint= math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

#xx = xr-3
yy = yy-0.2
'set string 1 l 1 -1'
'draw string 'xx' 'yy' CMPR(L) 'raveoutint' mm'

say
say 'WRITE AREA AVE OVER LAND (DIFF)'
say
rave'=aave(' drrland',lon=${alonw},lon=${alone},lat=${alats},lat=${alatn})'
'd 'rave
raveout=subwrd(result,4)
raveoutint=math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

#xx = xr-3
yy = yy-0.2

'draw string 'xx' 'yy' D(L) 'raveoutint' mm'

#say
#say 'WRITE AREA MAX'
#say
#ramax'=amax(' rr ',lon=${lonw},lon=${lone},lat=${lats},lat=${latn})'
#'d 'ramax
#rmaxout=subwrd(result,4)
#rmaxoutint=math_nint(rmaxout)
#say 'rmaxoutint='rmaxoutint'mm'
#dlon=0.3
#texlon=${lonw}+0.3
#say 'texlon='texlon
#dlat=0.3
#texlat=${latn}-dlat*4.
#say 'texlat='texlat
#'q w2xy 'texlon' 'texlat
#x=subwrd(result,3)
#y=subwrd(result,6)
#'set string 1 l 1 0'
#'draw string 'x' 'y' MAX 'rmaxoutint' mm'
#
#say
#say 'PLOT LOCATION OF AREA MAX'
#say
#ramaxlocx'=amaxlocx(' rr ',lon=${lonw},lon=${lone},lat=${lats},lat=${latn})'
#'d 'ramaxlocx
#ramaxoutx=subwrd(result,4)
#say 'ramaxoutx='ramaxoutx
#
#ramaxlocy'=amaxlocy(' rr ',lon=${lonw},lon=${lone},lat=${lats},lat=${latn})'
#'d 'ramaxlocy
#ramaxouty=subwrd(result,4)
#say 'ramaxouty='ramaxouty
#
#'q gr2w 'ramaxoutx' 'ramaxouty
#say result
#ramaxlon=subwrd(result,3)
#ramaxlat=subwrd(result,6)
#'set line 3 1 7'
#'markplot 'ramaxlon' 'ramaxlat ' -m 6 -s 0.3 -c 3'
#say


'set parea off'
'set vpage off'
*
#if (nmap = 24); break; endif
if (nmap = 5); break; endif
nmap = nmap + 1
xmap = xmap + 1

#hh=hh+${dh}
dd=dd+1
say 'dd='dd

endwhile         ;* xmap
if (nmap = 5); break; endif
ymap = ymap + 1
endwhile         :* ymap




# Header
'set strsiz 0.08 0.1'
'set string 1 l 2'
'draw string 0.1 8.35 ${timestamp} ${host}'
'draw string 0.1 8.23 ${cwd} ${gs}'
'set strsiz 0.10 0.12'
'draw string 0.1 7.90 ${runname}'



#'print ${figfile}'
'gxprint ${figfile}'

say
say 'Fig file: ${figfile}'
say

quit
EOF

#/usr/local/grads-2_0_a8/bin/grads -bcl "${gs}"
grads -bcl "${gs}"

echo
echo "RUNNAME :${runname}"
echo "DOMAIN:  ${domain}"
echo
exit 0

EOF

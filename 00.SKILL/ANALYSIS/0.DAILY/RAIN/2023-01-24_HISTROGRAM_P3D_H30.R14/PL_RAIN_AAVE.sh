#!/bin/bash
# Description:
#
# Author: manda
#
# Host: calypso.bosai.go.jp
# Directory: /work05/manda/WRF.POST/H30/H30_1912/MAP.RAIN_3DAY.DIFF_LAND
#
# Revision history:
#  This file is created by /work05/manda//mybin/ngmt.sh at Fri, 20 Dec 2019 10:02:17 +0900.

. ./gmtpar.sh
echo "Bash script $0 starts."

OUT=$(basename $0 .sh).eps

RANGE=128.8E-138.3E,31.0N-37.4N

IN=$(basename $0 .sh)_IN.txt
cat <<EOF >$IN
# /work05/manda/WRF.POST/H30/H30_1912/MAP.RAIN_3DAY.DIFF_LAND/FIG_MAP.RAIN_3DAY.DIFF_LAND
# ${RANGE}
#
# EXP=1 H30.R14.00.00
# EXP=2 H30.R14.01.00
# EXP=3 H30.R14.00.06
# EXP=4 H30.R14.01.06
#
# EXP   AAVE      #DIFF
  1     253.63    #0
  2     252.96    # 0.66
  3     239.67    #13.96
  4     237.83    #15.80
EOF

GP=$(basename $0 .sh).GNU
cat <<EOF>$GP
set terminal postscript eps enhanced # 出力先をEPSに設定
# set terminal postscript enhanced color # カラー
set output '${OUT}'         # 出力ファイル名をtest.epsに設定

set tics font "Arial,40"   # 目盛りのフォントの変更
set xlabel font "Arial,50" # xlabelのフォントの変更
set ylabel font "Arial,50" # ylabelのフォントの変更
set zlabel font "Arial,50" # zlabelのフォントの変更
set title font "Arial,35"
#set key font "Arial,40"    # 凡例のフォントの変更

set xrange[0.3:4.8] # x軸の範囲を設定する
set yrange[230:260] # y軸の範囲を設定する

set size 1.2,1.5
set boxwidth 0.5 relative
set style fill solid border
set title "${TRANGE}\n${ARANGE}" offset 0,2.5
set ylabel 'P [mm/3-d]'
set ylabel offset -7,0
set xtics ('CNTL' 1, 'A80' 2, 'O80' 3, 'AO80' 4)
set xtics offset 0,graph -0.05
plot '${IN}' notitle with boxes

set output

EOF
# gnuplot
# グラフの保存
#   http://www.gnuplot-cmd.com/in-out/output.html
# 目盛見出しを任意の文字に変更する
#   http://www.gnuplot-cmd.com/axis/format.html#set-tics
# バッチ処理
#   http://hidehikomurao.blogspot.com/2015/08/gnuplot.html
# フォント設定
#   http://www.eng.kagawa-u.ac.jp/~haruna/memo/gnuplot/gnutips.html#font
# 棒グラフ
#   http://www.ss.scphys.kyoto-u.ac.jp/person/yonezawa/contents/program/gnuplot/bar-graph.html


gnuplot $GP

echo
echo "INPUT : "
ls -lh --time-style=long-iso $IN
echo "OUTPUT : "
ls -lh --time-style=long-iso $OUT
echo
rm -v $GP $IN
echo "Done $0"


#!/bin/bash

VER=.R12
#VER=
EXE=$(basename $0 .run.sh).sh
#EXE=$(basename $0 .run.sh)${VER}.sh

CASE=H30
RUN=R14
domain=d03 #d01 #d02

runname1=${CASE}.${RUN}.00.00

runnames2="\
${CASE}.R14.01.06 \
${CASE}.R14.01.06 \
${CASE}.R14.00.06 \
${CASE}.R14.01.00 \
${CASE}.R15.02.01.01 \
${CASE}.R15.02.00.01 \
${CASE}.R15.02.01.00 \
"
for runname2 in $runnames2; do

$EXE -r $runname1 -s $runname2 -d $domain

done





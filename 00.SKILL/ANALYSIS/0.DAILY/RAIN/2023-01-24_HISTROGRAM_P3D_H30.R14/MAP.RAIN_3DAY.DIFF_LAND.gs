'cc'

'open /work06/manda/ARWpost/ARWpost_H30.R14.00.00/H30.R14.00.00.d03.p.ctl'
'open /work06/manda/ARWpost/ARWpost_H30.R15.02.01.00/H30.R15.02.01.00.d03.p.ctl'


yyyy=2018

mm=07
if(mm='01');mmm='JAN';endif
if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif
if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif
if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif
if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif
if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif
if(mm='12');mmm='DEC';endif

dd=5

#'set lon '128.744' '138.341
#'set lat '30.9605' '37.4229
'set mpdset 'hires


xmax = 1 ;*5
ymax = 1 ;#4

xwid = 9.0/xmax
ywid = 4/ymax ;#5.5/ymax

nmap = 1
ymap = 1



while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

say 'A dd='dd
dp=dd+3
say 'B dp='dp

say
datetime1=00'Z'dd''mmm''yyyy
datetime2=00'Z'dp''mmm''yyyy


'set time 'datetime1
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t1=subwrd(line,9)
say dtcheck' 't1

'set time 'datetime2
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t2=subwrd(line,9)
say dtcheck' 't2

xs = 0.8 + (xwid+0.10)*(xmap-1)
xe = xs + xwid
ye = 7.0 - (ywid+0.30)*(ymap-1) ;*7.5 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

if (ymap = ymax)
'set xlopts 1 2 0.12'
else
'set xlopts 1 2 0.0'
endif
if (xmap = 1)
'set ylopts 1 2 0.12'
else
'set ylopts 1 2 0.0'
endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='130 132 134 136 138'
'set xlevs 'xlevs
#'set xlint 2.5'
'set ylint 2'
'set grid off'


'color -levs -200 -150 -100 -50 -5 5 50 100 150 200 -gxout shaded -kind darkblue->skyblue->white->orange->darkred'


'set t 't1
rb'='RAINC.1'+'RAINNC.1
'set t 't2
rf'='RAINC.1'+'RAINNC.1
rr1'='rf'-'rb

#'HGTp1=HGT-0.01'
'HGTp1=-XLAND+1.9'
rrland1'=maskout('rr1',HGTp1)'

'set t 't1
rb'='RAINC.2'+'RAINNC.2
'set t 't2
rf'='RAINC.2'+'RAINNC.2
rr2'='rf'-'rb

rrland2'=maskout('rr2',HGTp1)'

drr'='rr1'-'rr2
drrland'='rrland1'-'rrland2

'set mpdraw off'

'd 'drrland
#'d 'drr

'set mpdraw on'
'set map 1 1 1'
'draw shp JPN_adm1'

#'markplot 130.6655  33.4234 -c 1 -m 2 -s 0.1'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*
xx = xl+0.0
yy = yt+0.2
'set string 1 l 2 0'
'set strsiz 0.12 0.15'
if(dd < 10);do='0'dd;endif
if(dd >  9);do=dd   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


#title=hh'-'hp'UTC'
title=datetime1'-'datetime2' d03' ;#do ;#hh'UTC'
'draw string 'xx' 'yy' 'title

xx=5.2
yy=7.5 ;#7.8
'set string 1 c 2 0'
'set strsiz 0.16 0.2'
#title='5'mmm''yyyy
#'draw string 'xx' 'yy' 'title
'draw string 'xx' 'yy' H30.R14.00.00-H30.R15.02.01.00'

xx1=xr+0.2
xx2=xr+0.3
yy1=yb
yy2=yt
'color -levs -200 -150 -100 -50 -5 5 50 100 150 200  -gxout shaded -kind darkblue->skyblue->white->orange->darkred -xcbar 'xx1' 'xx2' 'yy1' 'yy2'  -edge circle -line on -ft 2'


say '=================================='
say ' AREA AVE'
say
say ' lonwa = 128.8   lonea = 138.3'
say ' latsa = 31.0   latna = 37.4'
say
say '=================================='

say 'WRITE AREA AVE OVER LAND CNTL'
say
rave'=aave(' rrland1',lon=128.8,lon=138.3,lat=31.0,lat=37.4)'
'd 'rave
raveout=subwrd(result,4)
raveoutint= math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

xx = xr-3.3
yy = yb+0.75
'set string 1 l 1 -1'

'draw string 'xx' 'yy' MEAN [128.8-138.3,31.0-37.4]'

yy = yy-0.2
'draw string 'xx' 'yy' CNTL(L) 'raveoutint' mm'

say
say 'WRITE AREA AVE OVER LAND'
say
rave'=aave(' rrland2',lon=128.8,lon=138.3,lat=31.0,lat=37.4)'
'd 'rave
raveout=subwrd(result,4)
raveoutint= math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

#xx = xr-3
yy = yy-0.2
'set string 1 l 1 -1'
'draw string 'xx' 'yy' CMPR(L) 'raveoutint' mm'

say
say 'WRITE AREA AVE OVER LAND (DIFF)'
say
rave'=aave(' drrland',lon=128.8,lon=138.3,lat=31.0,lat=37.4)'
'd 'rave
raveout=subwrd(result,4)
raveoutint=math_format('%.2f', raveout)
say 'raveoutint='raveoutint'mm'

#xx = xr-3
yy = yy-0.2

'draw string 'xx' 'yy' D(L) 'raveoutint' mm'

#say
#say 'WRITE AREA MAX'
#say
#ramax'=amax(' rr ',lon=128.744,lon=138.341,lat=30.9605,lat=37.4229)'
#'d 'ramax
#rmaxout=subwrd(result,4)
#rmaxoutint=math_nint(rmaxout)
#say 'rmaxoutint='rmaxoutint'mm'
#dlon=0.3
#texlon=128.744+0.3
#say 'texlon='texlon
#dlat=0.3
#texlat=37.4229-dlat*4.
#say 'texlat='texlat
#'q w2xy 'texlon' 'texlat
#x=subwrd(result,3)
#y=subwrd(result,6)
#'set string 1 l 1 0'
#'draw string 'x' 'y' MAX 'rmaxoutint' mm'
#
#say
#say 'PLOT LOCATION OF AREA MAX'
#say
#ramaxlocx'=amaxlocx(' rr ',lon=128.744,lon=138.341,lat=30.9605,lat=37.4229)'
#'d 'ramaxlocx
#ramaxoutx=subwrd(result,4)
#say 'ramaxoutx='ramaxoutx
#
#ramaxlocy'=amaxlocy(' rr ',lon=128.744,lon=138.341,lat=30.9605,lat=37.4229)'
#'d 'ramaxlocy
#ramaxouty=subwrd(result,4)
#say 'ramaxouty='ramaxouty
#
#'q gr2w 'ramaxoutx' 'ramaxouty
#say result
#ramaxlon=subwrd(result,3)
#ramaxlat=subwrd(result,6)
#'set line 3 1 7'
#'markplot 'ramaxlon' 'ramaxlat ' -m 6 -s 0.3 -c 3'
#say


'set parea off'
'set vpage off'
*
#if (nmap = 24); break; endif
if (nmap = 5); break; endif
nmap = nmap + 1
xmap = xmap + 1

#hh=hh+
dd=dd+1
say 'dd='dd

endwhile         ;* xmap
if (nmap = 5); break; endif
ymap = ymap + 1
endwhile         :* ymap




# Header
'set strsiz 0.08 0.1'
'set string 1 l 2'
'draw string 0.1 8.35 Fri, 20 Dec 2019 10:00:50 +0900 calypso.bosai.go.jp'
'draw string 0.1 8.23 /work05/manda/WRF.POST/H30/H30_1912/MAP.RAIN_3DAY.DIFF_LAND MAP.RAIN_3DAY.DIFF_LAND.gs'
'set strsiz 0.10 0.12'
'draw string 0.1 7.90 '



#'print FIG_MAP.RAIN_3DAY.DIFF_LAND/MAP.RAIN_3DAY.DIFF_LAND_d03.H30.R14.00.00-H30.R15.02.01.00.eps'
'gxprint FIG_MAP.RAIN_3DAY.DIFF_LAND/MAP.RAIN_3DAY.DIFF_LAND_d03.H30.R14.00.00-H30.R15.02.01.00.eps'

say
say 'Fig file: FIG_MAP.RAIN_3DAY.DIFF_LAND/MAP.RAIN_3DAY.DIFF_LAND_d03.H30.R14.00.00-H30.R15.02.01.00.eps'
say

quit

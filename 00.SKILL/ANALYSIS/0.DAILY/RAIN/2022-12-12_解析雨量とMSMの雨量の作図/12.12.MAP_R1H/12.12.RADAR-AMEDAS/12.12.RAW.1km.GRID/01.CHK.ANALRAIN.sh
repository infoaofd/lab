#!/bin/bash

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"
INDIR=/work02/DATA/RADAR_AMEDAS/2022/JJA
INFLE=3p-analrain_2022-08-31_2300utc.nc
IN=$INDIR/$INFLE
GS=$(basename $0 .sh).GS

cat << EOF > ${GS}

# ${NOW}
# ${HOST}
# ${CWD}

'sdfopen ${IN}'

'q ctlinfo'; say result

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

#!/bin/bash

YYYY=$1;M=$2;D=$3
YYYY=${YYYY:-2020};M=${M:-7};D=${D:-13}
MM=$(printf %02d $M); DD=$(printf %02d $D)

INDIR=/work02/DATA/RADAR_AMEDAS/$YYYY/JJA
INLIST=$(ls $INDIR/3p-analrain_${YYYY}-${MM}-${DD}_????utc.nc)

for INFLE in $INLIST; do
IN=$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1; fi
done #INFLE

ODIR=/work02/DATA/RADAR_AMEDAS/5km.GRID/$YYYY; mkd $ODIR

cat > MSM_GRID.TXT << EOF
gridtype = lonlat
xsize    = 481
ysize    = 505
xunits = 'degree'
yunits = 'degree'
xfirst   = 120
xinc     = 0.0625
yfirst   = 22.4
yinc     = 0.05
EOF

for INFLE in $INLIST; do
IN=$INFLE
OUT=$ODIR/$(basename $IN .nc)_5km.nc

echo MMMMM
echo INPUT: $IN

cdo remapcon,MSM_GRID.TXT $IN $OUT 

<<COMMENT
水平高解像度なファイルを低解像度なファイルへ変換
https://qiita.com/wm-ytakano/items/6d3ef4aa5d032c516162

remapping from fine to coarse grid
https://code.mpimet.mpg.de/boards/1/topics/562

The bilinar interpolation uses only 4 surrounding grid points. For the bicubic interpolation 16 surrounding points are used.
The conservative remapping with remapcon takes all source grid points into account!
COMMENT

echo OUTPUT: $OUT
echo MMMMM
echo

done #INFLE


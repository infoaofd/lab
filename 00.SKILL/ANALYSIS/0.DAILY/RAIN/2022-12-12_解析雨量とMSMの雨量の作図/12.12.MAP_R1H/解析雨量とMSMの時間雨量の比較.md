# 解析雨量とMSM (解析値)との時間雨量の比較

解析雨量＝降水レーダーを地上観測データ（アメダス）で補正したもの



## データの所在

### 解析雨量

```
/work02/DATA/RADAR_AMEDAS
```

### MSM (解析値)

```
/work01/DATA/MSM/MSM-S/r1h
```



## 作業用ディレクトリのコピー

```bash
$ cp -ar /work03/am/2022.NAKAMURO_FLUX/32.12.RAIN/ $HOME
```



## 生データの確認

### 解析雨量

#### 実行例

```bash
$ cd 12.12.MAP_R1H/12.12.RADAR-AMEDAS/12.12.RAW.1km.GRID
```

#### データの確認

```bash
$ 01.CHK.ANALRAIN.sh
```

```bash
$ ncdump -h /work02/DATA/RADAR_AMEDAS/2022/JJA/3p-analrain_2022-08-31_2300utc.nc
```

#### 作図

```bash
$ 41.R1H.PANEL.sh
```



### MSM (解析値)

```bash
$ cd 12.12.MAP_R1H/22.12.MSM.MA 
```

#### データの確認

```bash
$ 01.CHK.MSM.MA.RAIN.sh
```

#### 作図

```bash
$ 41.MSM_MA_R1H.PANEL.sh
```



## 解析雨量の補間

**解析雨量**(レーダーアメダス)の**空間解像度は1km**であり，**MSMの空間解像度** (**5km**)と異なる。

したがって，生データのままでは，2つのデータの差をとることができない。

そこで，cdoを用いて解析雨量のデータを補間することにより，解像度5kmのデータを作成する。

```bash
$ cd 12.12.MAP_R1H/12.12.RADAR-AMEDAS/20.12.HITPL.5km.GRID
```

### 補間

```bash
$ 12.RADAR_AMEDAS_HINTPL_5km.GRID.sh
```

1日分（24個のファイル）を一度に処理する。少々時間を要する。処理したい日数が多いときは，スクリプトを用いて自動化する

参考：日付処理

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

#### 作図

```bash
$ 41.R1H.5km.PANEL.sh
```


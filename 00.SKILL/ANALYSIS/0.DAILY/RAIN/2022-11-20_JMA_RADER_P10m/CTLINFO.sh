#!/bin/bash

# Tue, 23 Aug 2022 08:49:23 +0900
# calypso.bosai.go.jp
# /work04/manda/2022.06.ECS.OBS/34.00.CLOUD_HIMAWARI/12.00.CHK.RAW

usage(){
  echo USAGE GS.sh [-h] E1 E2 E3 AREA VAR ; exit 0
}

flagh="false"; flagd="false"
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
     *  ) usage
  esac
done
shift 0

#CTL=$(basename $0 .sh).CTL
GS=$(basename $0 .sh).GS
#
#FIG=$(basename $0 .sh).eps

# LONW= ;LONE=
# LATS= ;LATN=
# LEV=
# TIME=

TITLE=""

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

INDIR=/work04/manda/DATA/HIMAWARI/P-TREE_JAXA/CLP/010/202206/18/23
NC=NC_H08_20220618_2350_L2CLP010_FLDK.02401_02401.nc

cat << EOF > ${GS}

# Tue, 23 Aug 2022 08:49:23 +0900
# calypso.bosai.go.jp
# /work04/manda/2022.06.ECS.OBS/34.00.CLOUD_HIMAWARI/12.00.CHK.RAW

'sdfopen ${INDIR}/${NC}'

'q ctlinfo'; say result

'quit'

EOF

grads -bcp "$GS"

if [ $flagd != "yes" ]; then rm -vf ; fi

echo "DONE $0."
echo

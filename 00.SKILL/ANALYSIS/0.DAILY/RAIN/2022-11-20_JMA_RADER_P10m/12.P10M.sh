#!/bin/bash
usage(){
  echo USAGE $0 [-h] HH DD MM YYYY; exit 0
}

flagh="false"; flagd="false"
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
     *  ) usage
  esac
done
shift 0

CTL1=RADAR_10MIN.ctl
GS=$(basename $0 .sh).GS

LONW=127 ;LONE=131; LATS=29 ;LATN=32

YYYY=$1; MM=$2; DD=$3; HH=$4

YYYY=${YYYY:-2022}; MM=${MM:-06}; DD=${DD:-19}; HH=${HH:-00}; 

if [ $MM = 01 ]; then MMM=JAN; fi; if [ $MM = 02 ]; then MMM=FEB; fi
if [ $MM = 03 ]; then MMM=MAR; fi; if [ $MM = 04 ]; then MMM=APR; fi
if [ $MM = 05 ]; then MMM=MAY; fi; if [ $MM = 06 ]; then MMM=JUN; fi
if [ $MM = 07 ]; then MMM=JUL; fi; if [ $MM = 08 ]; then MMM=AUG; fi
if [ $MM = 09 ]; then MMM=SEP; fi; if [ $MM = 10 ]; then MMM=OCT; fi
if [ $MM = 11 ]; then MMM=NOV; fi; if [ $MM = 12 ]; then MMM=DEC; fi

TITLE=${HH}Z${DD}${MMM}${YYYY}

VAR1="rr.1/6.0"; VARS="P10m"
FIG=${VARS}_${YYYY}-${MM}-${DD}_${HH}.pdf

TITLE="$TIME P10m JMA-RADAR"

#LEVS="260 300 2"; FS=5; 
# KIND='blue->lime->yellow->orange->red->darkmagenta'
LEVS="-levs 0.5 1 2 3 4 5 6 7 8 9 10 11 12 13 14"; FS=1; 
KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'

LEVS2="0.1 0.2 0.02"; KIND2="(127,127,127,0)->(127,127,127,196)"; FS2=1

CLEV3H=5 ;#CLEV3W=1 ;#mm/10-min
# CLEV3H=1.67; #CLEV3W=0.333 #mm/hr

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

XLEFT=0.6; XWID=2.3; XMARGIN1=0.43 XMARGIN2=1.6
YTOP=9; YHGT=2; YHGT2=2; YMARGIN=0.7

cat << EOF > ${GS}

# Tue, 23 Aug 2022 08:56:40 +0900
# calypso.bosai.go.jp
# /work04/manda/2022.06.ECS.OBS/34.00.CLOUD_HIMAWARI/12.00.CHK.RAW


'set vpage 0.0 8.5 0.0 11'

'cc'; 'set grid off'; 'set grads off'


#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

'set xlevs 124 125 126 127 128 129 130';'set ylint 1'; 'set xlopts 1 3 0.1'; 'set ylopts 1 3 0.1'; 

'set rgb 99 78 53 36'
'set mpdraw on'; 'set mpdset hires';'set map 99 1 3'
'set datawarn off'

NN='00'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs=${XLEFT};xe=xs+${XWID}; ye = ${YTOP} ;ys=ye-${YHGT}

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME

'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

NN='10'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME

'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

NN='20'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME

'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

NN='30'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs=${XLEFT};xe=xs+${XWID}; ye = ${YTOP}-${YHGT}-${YMARGIN};ys=ye-${YHGT2}

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME

'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

NN='40'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME

'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL1}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

NN='50'; TIME='${HH}:'NN'Z${DD}${MMM}${YYYY}'
'set time 'TIME; say TIME

xs = xe + ${XMARGIN1}; xe=xs+${XWID} 

'set parea 'xs ' 'xe' 'ys' 'ye
say 'mmm xs='xs' xe='xe' ys='ys' ye='ye

'set z 1'

'set xlab on'; 'set ylab on'

'color ${LEVS} -kind ${KIND} -gxout grfill'
'd $VAR1'

'set xlab off';'set ylab off'

say 'MMMMM MASK OUT DATA MISSING AREA'
'set rgb 99 0 0 0 100'
'set line 99 1 1'
'q w2xy 127 30.5'; x1=subwrd(result,3); y1=subwrd(result,6)
'q w2xy 128 30.5'; x2=subwrd(result,3); y2=subwrd(result,6)
'q w2xy 128 32'; x3=subwrd(result,3); y3=subwrd(result,6)
'q w2xy 127 32'; x4=subwrd(result,3); y4=subwrd(result,6)
'q w2xy 127 30.5'; x5=subwrd(result,3); y5=subwrd(result,6)
'draw polyf 'x1' 'y1' 'x2' 'y2' 'x3' 'y3' 'x4' 'y4' 'x5' 'y5

# LEGEND COLOR BAR

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=${XLEFT}; x2=${XLEFT}+3*(${XMARGIN1}+${XWID})-2
y1=yb-0.5; y2=y1+0.1

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'

x=x2+0.1; y=(y1+y2)/2; 'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' P [mm/10-min]'


'set xlab on'; 'set ylab on'

xx = (xl+xr)/2; yy = yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.11 0.13'
'draw string 'xx' 'yy' 'TIME
'allclose'
#mmmmmm 



#MMMMMMMMMM
'open ${CTL}'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'

x1=${XLEFT}; x2=${XLEFT}+3*(${XWID}) ;#+${XMARGIN1})
y1=${YTOP}+0.55
xx=(x1+x2)/2; yy=y1
'set strsiz 0.15 0.17'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' $TITLE'


# HEADER
'set strsiz 0.11 0.13'; 'set string 1 l 3 0'
xx = 0.2; yy=${YTOP}+1.1; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'

#'q ctlinfo 1'; say result
'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
if [ $flagd != "true" ]; then rm -vf $GS; fi

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

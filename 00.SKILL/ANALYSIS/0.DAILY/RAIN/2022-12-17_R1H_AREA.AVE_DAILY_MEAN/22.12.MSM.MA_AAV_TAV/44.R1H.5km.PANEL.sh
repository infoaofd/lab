#!/bin/bash
# /work03/am/2022.06.ECS.OBS/32.00.RADAR_PRECIP/24.02.JMA_RADAR_P1H
#
#
gs=$(basename $0 .sh).gs

yyyy=$1; m=$2; d=$3
yyyy=${yyyy:-2020}; m=${m:-7}; d=${d:-13}

mm=$(printf %02d $m); dd=$(printf %02d $d); dp=$(expr $dd + 1)

hs=00; dh=1
UNIT=[mm/h]


FIGDIR=MSM.MA_5km_FIG; mkd $FIGDIR
FIG=${FIGDIR}/MSM.MA_5km_${yyyy}${mm}${dd}_R1H.pdf #.eps


ctl=41.MSM_MA_R1H.CTL
if [ ! -f $ctl ];then echo NO SUCH FILE,$ctl; exit 1; fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

lonw=127.5; lats=30; lone=132.5; latn=35


cat <<EOF>$gs
'cc'
'set rgb 99 78 53 36' ;# DARK BROWN

'open ${ctl}'


yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif



'set lon ${lonw} ${lone}'; 'set lat ${lats} ${latn}'
'q dims';say result
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='4 80 4'



xmax = 6; ymax = 4

xwid = 8.5/xmax; ywid = 5/ymax

nmap = 1; ymap = 1

hh=${hs}

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grid off'
'set map 99 1 1 1';'set mpdset hires'

datetime1=hh'Z'${dd}''mmm''yyyy

'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say dtcheck' 't1

hp=hh+1

xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.4)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.09'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.09'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.09'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130' ;# 132'
'set xlevs 'xlevs
'set ylint 1'


'color ' clevs ' -gxout shaded -kind ' kind

'd r1h'

# '04.02.FILL.MISSING.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.12
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


title=hh'-'hp'UTC${dd}'mmm''yyyy
'set string 1 l 1 0'; 'set strsiz 0.06 0.08'
'draw string 'xx' 'yy' 'title

'set parea off'; 'set vpage off'
*
if (nmap > 23); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+${dh}

say
endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 0.9 1 -edge circle -line on -fs 2 -ft 2' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 8.05 0.95 ${UNIT}';

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
'draw string 0.2 8.4 ${NOW}'; 'draw string 0.2 8.25 ${CWD}'
'draw string 0.2 8.1 ${CMD}'; 'draw string 0.2 7.9 ${FIG}'

'gxprint ${FIG}'

quit
EOF

grads -bcl "${gs}"
rm -vf $gs

if [ -f ${FIG} ]; then
echo; echo FIG: ${FIG};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${FIG}.;echo; exit 1
fi

exit 0

'cc'
'set rgb 99 78 53 36' ;# DARK BROWN

'open 41.RADAR_AMEDAS_5km.CTL'


yyyy=2020

mm=07
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif



'set lon 127.5 132.5'; 'set lat 30 35'
#'q dims';say result
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='4 80 4'



xmax = 6; ymax = 4

xwid = 8.5/xmax; ywid = 5/ymax

nmap = 1; ymap = 1

hh=00

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grid off'
'set map 99 1 1 1';'set mpdset hires'

datetime1=hh'Z'13''mmm''yyyy

'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say dtcheck' 't1

hp=hh+1

xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.4)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.09'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.09'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.09'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130' ;# 132'
'set xlevs 'xlevs
'set ylint 1'


'color ' clevs ' -gxout shaded -kind ' kind

'd P60'

# DOMAIN OF AREA AVERAGE
'trackplot 129.2 32 132.1 32 -c 1 -l 1 -t 1'
'trackplot 132.1 32 132.1 34.0 -c 1 -l 1 -t 1'
'trackplot 132.1 34.0 129.2 34.0 -c 1 -l 1 -t 1'
'trackplot 129.2 34.0 129.2 32 -c 1 -l 1 -t 1'

'markplot 128.3508 31.998 -c 2 -m 3 -s 0.05' ;# MESHIMA

# '04.02.FILL.MISSING.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.12
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


title=hh'-'hp'UTC13'mmm''yyyy
'set string 1 l 1 0'; 'set strsiz 0.06 0.08'
'draw string 'xx' 'yy' 'title

'set parea off'; 'set vpage off'
*
if (nmap > 23); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+1

say
endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 0.9 1 -edge circle -line on -fs 2 -ft 2' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 8.05 0.95 [mm/h]';

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
'draw string 0.2 8.4 Sat, 17 Dec 2022 17:56:26 +0900'; 'draw string 0.2 8.25 /work03/am/2022.NAKAMURO_FLUX/32.12.RAIN/22.12.AREA.AVE_DAILY_MEAN/12.12.RADAR-AMEDAS_AAV_TAV'
'draw string 0.2 8.1 ./44.R1H.5km.PANEL.sh 2020 07 13 129.2 132.1 32 34.0'; 'draw string 0.2 7.9 RADAR_AMEDAS_5km_FIG/RADAR_AMEDAS_5km_20200713_R1H.pdf'

'gxprint RADAR_AMEDAS_5km_FIG/RADAR_AMEDAS_5km_20200713_R1H.pdf'

quit

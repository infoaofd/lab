#!/bin/bash

cat <<EOF>r1h.ctl
dset output/%y4%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 1000  LINEAR 00:00Z01Jun2016 1hr
vars 1
rr 0 0 rainfall
endvars
EOF

datetime1=01Z19Jun2016
datetime2=00Z20Jun2016
figfile=R1D_20Jun2016.eps

cat <<EOF>r1d.gs
'open r1h.ctl'
'set lon 127.5 133.0'
'set lat  30.0  34.5'

'set time '${datetime1}
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t1=subwrd(line,9)
say dtcheck' 't1

'set time '${datetime2}
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t2=subwrd(line,9)
say dtcheck' 't2

r1d='sum(rr,t=' t1 ',t=' t2 ')'

'cc'
'set mpdset hires'
'set gxout shade2'

'd 'r1d

'cbarn'

'draw title '${datetime1}'-'${datetime2}

'gxprint ${figfile}'

say 'Fig file: ${figfile}'

quit
EOF

grads -bcp "r1d.gs"

exit 0

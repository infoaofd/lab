#!/bin/bash

exe=$(basename $0 .ALL.sh).sh

if [ ! -f $exe ]; then
  echo ERROR in $0 : No such file, $exe
  exit 1
fi

yyyymm=202206

sdate=18; edate=19

d=$sdate

while [ $d -le $edate ]; do
  dd=$(printf %02d $d)
  h=0
  while [ $h -le 23 ]; do
    hh=$(printf %02d $h)

    $exe  $yyyymm$dd $hh

    h=$(expr $h + 1)
  done

  d=$(expr $d + 1)
done

exit 0

#!/bin/bash

gs=$(basename $0 .sh).gs
figfile=$(basename $0 .sh).eps

ctl=$(basename $0 .sh).ctl
cat <<EOF>$ctl
dset output/%y4%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 1000  LINEAR 00:00Z01Jun2016 1hr
vars 1
rr 0 0 rainfall
endvars
EOF

export LANG=C
host=$(hostname)
cwd=$(pwd)
timestamp=$(date)

lonw=127.5
lone=132.5
lats=30
latn=34.5
datetime=0019JUN2016

cat <<EOF>$gs
'cc'

'open ${ctl}'

'set time ${datetime}'
'set lon '${lonw}' '${lone}
'set lat '${lats}' '${latn}
'set mpdset 'hires
'set gxout shade2'

xmax = 5
ymax = 1

xwid = 9.0/xmax
ywid = 5.5/ymax

nmap = 1
ymap = 1

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

xs = 0.8 + (xwid+0.10)*(xmap-1)
xe = xs + xwid
ye = 7.5 - (ywid+0.10)*(ymap-1)
ys = ye - ywid

if (ymap = ymax)
'set xlopts 1 2 0.14'
else
'set xlopts 1 2 0.0'
'set xlopts 1 2 0.14'
endif
if (xmap = 1)
'set ylopts 1 2 0.14'
else
'set ylopts 1 2 0.0'
endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='129 131'
'set xlevs 'xlevs
'set ylint 1'


'd 'rr

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*
xx = xl+0.0
yy = yt+0.15
'set string 1 l 2 0'
'set strsiz 0.15 0.15'
'draw string 'xx' 'yy' 'title

'set parea off'
'set vpage off'
*
if (nmap = 22); break; endif
nmap = nmap + 1
xmap = xmap + 1
endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap


# Header
'set strsiz 0.08 0.1'
'set string 1 l 2'
'draw string 0.1 8.35 ${timestamp}'
'draw string 0.1 8.23 ${host}'
'draw string 0.1 8.11 ${cwd}'
'draw string 0.1 7.99 ${gs}'



'gxprint ${figfile}'

say
say 'Fig file: ${figfile}'
say

quit
EOF

grads -bcl "${gs}"

exit 0

#!/bin/sh

exe=./runncl.sh

ncl=$(basename $0 .run.sh).ncl

if [ ! -f $ncl ]; then
  echo Error in $0 : No such file, $ncl
  exit 1
fi

if [ $# -ne 2 ]; then
  echo ERROR in $0: Wrong argument.
  echo
  echo Usage: $0 yyyymmdd hhmiss
  echo
  exit 1
fi

yyyymmdd=$1
hhmiss=$2

yyyy=${yyyymmdd:0:4}
  mm=${yyyymmdd:4:2}
  dd=${yyyymmdd:6:2}

  hh=${hhmiss:0:2}
  mi=${hhmiss:2:2}
  ss=${hhmiss:4:2}


rdir=/work04/manda/DATA/Radar-10min
indir=$rdir"/"$yyyy"/"$mm"/"$dd

outdir="Fig"
mkdir -vp $outdir

itmdir="intm.data"
mkdir -vp $itmdir

dirs="$rdir $indir $itmdir $outdir"
for dir in $dirs; do
if [ ! -d $dir ]; then
  echo Error in $0 : No such directory, $dir
  exit 1
fi
done

<<COMMENT
prefix="Z__C_RJTD_"
postfix="_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin"
input=${indir}/${prefix}${yyyy}${mm}${dd}${hh}${mi}${ss}${postfix}

inlist="$input"

for input in $inlist; do
if [ ! -f $input ]; then
  echo No such file, $input
  exit 1
fi
done

intmfile=${itmdir}/${yyyy}${mm}${dd}_${hh}${mi}${sc}_radar10m.bin

command="./jmaradar2bin $input   $intmfile"

echo
echo $command
echo
$command
if [ $? -ne 0 ]; then
  echo
  echo "Error in $0 while running the following:"
  echo "  $command"
  echo
  exit 1
fi
echo
echo "Input: $input"
COMMENT


echo
echo "Intermidiate file: $intmfile"
echo
ls -lh $intmfile
$exe $ncl "$yyyymmdd" "$hhmiss" "$intmfile" "$outdir"

echo
#rm -vf $intmfile

exit 0

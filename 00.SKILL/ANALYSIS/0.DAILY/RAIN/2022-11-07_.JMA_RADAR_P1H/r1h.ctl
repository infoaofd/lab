dset output/%y%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 8760  LINEAR 23:00Z19Jun2022 1hr
vars 1
rr 0 0 rainfall
endvars

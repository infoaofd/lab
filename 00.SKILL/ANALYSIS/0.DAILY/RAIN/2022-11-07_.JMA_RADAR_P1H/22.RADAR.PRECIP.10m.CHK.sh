#!/bin/sh

exe=./runncl.sh

ncl=$(basename $0 .sh).ncl

if [ ! -f $ncl ]; then
  echo Error in $0 : No such file, $ncl
  exit 1
fi

<<COMMENT
if [ $# -ne 2 ]; then
  echo ERROR in $0: Wrong argument.
  echo
  echo Usage: $0 yyyymmdd hhmiss
  echo
  exit 1
fi
COMMENT

yyyymmdd=$1; yyyymmdd=${yyyymmdd:-20220619}

hhmiss=$2; hhmiss=${hhmiss:-000000}

yyyy=${yyyymmdd:0:4}; mm=${yyyymmdd:4:2}; dd=${yyyymmdd:6:2}

  hh=${hhmiss:0:2}; mi=${hhmiss:2:2}; ss=${hhmiss:4:2}

rdir=/work01/DATA/JMA_RADAR_10m
indir=$rdir"/"$yyyy"/"$mm"/"$dd

outdir="Fig"
mkdir -vp $outdir

itmdir="intm.data"
mkdir -vp $itmdir

dirs="$rdir $indir $itmdir $outdir"
for dir in $dirs; do
if [ ! -d $dir ]; then
  echo Error in $0 : No such directory, $dir
  exit 1
fi
done

echo
echo "Intermidiate file: $intmfile"
echo
intmfile=${itmdir}/${yyyymmdd}_${hh}${mi}_radar10m.bin
if [ ! -f ${intmfile} ]; then
echo ERROR in $0 : NO SUC FILE, ${intmfile}; exit 1
fi

#ls -lh $intmfile
$exe $ncl "$yyyymmdd" "$hhmiss" "$intmfile" "$outdir"

echo
#rm -vf $intmfile

exit 0

#include <stdio.h>
#include "grib2.h"

int read_section6( FILE *fp )
{
  
  unsigned char buf[6];
  
  if( fread( buf, 1, 6, fp ) != 6 )
    return(0);
  
  if( buf[4] != 6 ) /* section number */
    return(0); 
  
  return(1);

}

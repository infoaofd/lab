#include <stdio.h>
#include "grib2.h"

int read_section0( FILE *fp )
{
  
  unsigned char buf[16];
  
  if( fread( buf, 1, 16, fp ) != 16 )
    return(0);
  
  if( buf[0]!='G' || buf[1]!='R' || buf[2]!='I' || buf[3]!='B' )
    return(0);
  
  if( buf[6] != 0 )  return(0); /* Discipline: Meteorological Product */
                                /* see Code Table 0.0 */
  
  if( buf[7] != 2 )  return(0); /* Edition Number: 2 */

  return(1);

}

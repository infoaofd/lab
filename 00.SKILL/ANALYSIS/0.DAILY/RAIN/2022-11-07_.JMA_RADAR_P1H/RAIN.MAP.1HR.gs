'cc'

'open RAIN.MAP.1HR.ctl'


yyyy=2017

mm=07
if(mm='01');mmm='JAN';endif
if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif
if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif
if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif
if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif
if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif
if(mm='12');mmm='DEC';endif



'set lon '130.4' '131.6
'set lat '33.2' '33.8
'set mpdset worldmap'



kind='white->lavender->cornflowerblue->dodgerblue->blue->yellow->orange->red->darkmagenta'
clevs='1 5 10 20 50 100'



xmax = 6 ;*5
ymax = 3

xwid = 9.0/xmax
ywid = 4/ymax
#ywid = 5.5/ymax

nmap = 1
ymap = 1



hh=00

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)


hp=hh+1


say
datetime1=hh'Z'05''mmm''yyyy
datetime2=hp'Z'05''mmm''yyyy

if (hh = 23)
datetime1=23'Z'05''mmm''yyyy
datetime2=00'Z'6''mmm''yyyy
endif

'set time 'datetime1
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t1=subwrd(line,9)
say dtcheck' 't1

'set time 'datetime2
'q dims'
line=sublin(result,5)
dtcheck=subwrd(line,6)
t2=subwrd(line,9)
say dtcheck' 't2

xs = 0.8 + (xwid+0.10)*(xmap-1)
xe = xs + xwid
ye = 7.0 - (ywid+0.30)*(ymap-1)
ys = ye - ywid

if (ymap = ymax)
'set xlopts 1 2 0.12'
else
'set xlopts 1 2 0.0'
endif
if (xmap = 1)
'set ylopts 1 2 0.12'
else
'set ylopts 1 2 0.0'
endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
'set xlevs 130.4 131'
'set ylevs 33.2 33.5 33.8'
'set grid off'



'color -levs ' clevs ' -gxout shaded -kind ' kind


'set t 't1
#r1h='sum(rr,t=' t1 ',t=' t2 ')'
'd 'rr

#'draw shp JPN_adm1'
'markplot 130.6655  33.4234 -c 1 -m 2 -s 0.1'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
*
xx = xl+0.0
yy = yt+0.1
'set string 1 l 2 0'
'set strsiz 0.08 0.1'
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


#title=hh'-'hp'UTC'
title=hh'UTC'
'draw string 'xx' 'yy' 'title

'set parea off'
'set vpage off'
*
#if (nmap = 24); break; endif
if (nmap = 18); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+1

endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap



xx=5.5
yy=7.3 ;#7.8
'set string 1 c 1 0'
'set strsiz 0.12 0.15'
title='RADAR AMeDAS 05'mmm''yyyy
'draw string 'xx' 'yy' 'title


xx=0.5
yy=7.6
'set string 1 l 1 0'
'set strsiz 0.10 0.12'
'draw string 'xx' 'yy' Shade=P (mm/h)'



'color -levs ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 2.0 2.1 -edge triangle -line on -ft 0.1'


# Header
'set strsiz 0.08 0.1'
'set string 1 l 2'
'draw string 0.1 8.40 Tue Jul 31 14:38:37 JST 2018'
'draw string 0.1 8.28 localhost'
'draw string 0.1 8.16 /work2/am/2017.KYUSHU-HOKUBU.HEAVY.RAIN/Radar.Precip.1hr.201707'
'draw string 0.1 8.04 RAIN.MAP.1HR.gs'



'gxprint RAIN.MAP.1HR_20170705.eps'

say
say 'Fig file: RAIN.MAP.1HR_20170705.eps'
say

quit

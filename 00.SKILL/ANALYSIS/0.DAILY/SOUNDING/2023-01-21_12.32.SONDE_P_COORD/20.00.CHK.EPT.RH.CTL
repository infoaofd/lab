dset ^OUT_SONDE_THERMO/ECS_A1_LINE_%y4-%m2-%d2_%h2.grd
options template
undef -999.9
title 2022-06_ECS A1 LINE
xdef 1 linear 0.0 2.5
ydef 1 linear 60.0 2.5
zdef 100 LEVELS
1000	990	980	970	960	950	940	930	920	910
900	890	880	870	860	850	840	830	820	810
800	790	780	770	760	750	740	730	720	710
700	690	680	670	660	650	640	630	620	610
600	590	580	570	560	550	540	530	520	510
500	490	480	470	460	450	440	430	420	410
400	390	380	370	360	350	340	330	320	310
300	290	280	270	260	250	240	230	220	210
200	190	180	170	160	150	140	130	120	110
100	90	80	70	60	50	40	30	20	10
tdef 300  linear 00z19jun2022 1hr
vars 14
h    100  99  Height               [m]
u    100  99  E-W Wind Speed     [m/s]
v    100  99  N-S Wind Speed     [m/s]
wr   100  99  Wind Relative        [s]
ws   100  99  Wind Speed         [m/s]
p    100  99  Air Pressure       [hpa]
t    100  99  Temprature           [K]
rh   100  99  Relative humid       [%]
pt   100  99  Potential Temprature [K]
wp   100  99  dewpoint Pressure  [hpa]  
ewo  100  99  equi
dt   100  99  Dewpoint depression  [K]
w    100  99  Mixing Ratio     [kg/kg]
ept  100  99  Equivalent potential temp [K]
endvars

! Output data
! $1=高度 $2=風向 $3=風速 $4=気圧 $5=気温 $6=湿度 $7=温位 $8=飽和水蒸気圧
! $9=露点温度での水蒸気圧 $10=露点温度 $11=露点温度における水蒸気量 $12=相当温位
!/work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT
      INTEGER,PARAMETER::VAR=14
      parameter(L=2490,P=3.1415926536*2.0)
      parameter NLEV=500 !200
      real x(var,NLEV) !500)
      integer n
      character csv*50,in*50,ODIR*100,out*150,OFLE*500
      character(len=150)::list,fmt
      namelist /para/nt,list,ODIR,fmt

      read(5,nml=para)
      print *,list

      open(1,file=list,action="read")
      read(1,*) !Skip comment line

      do 1 it=1,nt

        read(1,fmt) csv,in,n,out
        print *,trim(in),n,trim(out)

        do i=1,NLEV
          do j=1,var
            x(j,i)=-999.9
          enddo
        enddo

        open(2,file=in,action="read")

        OFLE=trim(ODIR)//"/"//trim(out)

        open(3,file=OFLE,access="direct",form="unformatted", &
&       recl=4*var*NLEV)

!       Input data
!       1高度 2風向 3風速 4気圧 5気温 6湿度

        do 10 i=1,n
          read(2,*) (x(j,i),j=1,6)

          if(-100.lt.x(5,i).and.x(5,i).le.100) then
            if(0.lt.x(4,i).and.x(4,i).lt.1100) then
              ! 温位
              x(7,i)=(x(5,i)+273.15)*(1000/x(4,i))**0.2859
              ! 飽和水蒸気圧  気温は摂氏
              x(8,i)=6.11*10**((7.5*x(5,i))/(237.3+x(5,i)))
            endif
          end if
         
          if(0.lt.x(6,i).and.x(6,i).le.101) then
            if(0.lt.x(4,i).and.x(4,i).lt.1100) then
              if(-999.ne.x(7,i).and.x(8,i).ne.-999) then   
                ! 現在の水蒸気圧
                x(9,i)=x(6,i)*x(8,i)/100
                ! 露点温度
                x(10,i)=273.2/(1-273.2*0.0001844*log(x(9,i)/6.11))-273.15
                ! 混合比
                x(11,i)=0.622*x(9,i)/(x(4,i)-x(9,i))
                ! 相当温位
                x(12,i)=x(7,i)*exp((x(11,i)*L)/(x(10,i)+273.15))
              endif
            endif
          endif
        
          if(0.le.x(2,i).and.x(2,i).le.360) then
            if(0.le.x(3,i).and.x(3,i).le.100) then
              x(13,i)=-(x(3,i)*cos(P*x(2,i)/360))
              x(14,i)=-(x(3,i)*sin(P*x(2,i)/360))
            endif
          endif

 10     continue

!DO k=1,499
! write(6,'(14f10.5)') (x(j,k),j=1,var)
!END DO

      write(3,rec=1) (x(1,j),j=1,NLEV),(x(13,j),j=1,NLEV),              &
                 (x(14,j),j=1,NLEV),(x(2,j),j=1,NLEV),(x(3,j),j=1,NLEV), &
                 (x(4,j),j=1,NLEV),(x(5,j),j=1,NLEV),(x(6,j),j=1,NLEV),  &
                 (x(7,j),j=1,NLEV),(x(8,j),j=1,NLEV),(x(9,j),j=1,NLEV),  &
                 (x(10,j),j=1,NLEV),(x(11,j),j=1,NLEV),(x(12,j),j=1,NLEV)
      close(3)

DO j=1,NLEV
 write(6,'(14f10.2)') x(1,j),x(4,j)
END DO

      close(2)

 1    continue

      stop
      end



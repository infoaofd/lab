#!/bin/bash
#
# Directory: /work03/am/2022.06.ECS.OBS/12.22.SONDE_3SHIP/12.12.OBS_POINT
#
. ./gmtpar.sh

range=127.5/130/30/31.5
size=JM5
xanot=a1f30m; yanot=a1f30m
anot=${xanot}/${yanot}WSne

in=2022.06.ECS.OBSP.TXT
if [ ! -f $in ]; then echo Error in $0 : No such file, $in; exit 1; fi
in2=FRONT_2022-06-19_00.TXT
if [ ! -f $in ]; then echo Error in $0 : No such file, $in; exit 1; fi

fig=$(basename $in .TXT).ps

pscoast -R$range -$size -JQ -W2 -Dh -P -K -X1 -Y6.5 >$fig

awk '{if($1!="#")print $1, $2}' $in2 |\
psxy -R$range -$size -W8t20_10:20 -O -K >>$fig

awk '{if($1!="#"&&$4=="N")print $2, $1}' $in |\
psxy -R$range -$size -Sc0.05 -G0 -O -K >> $fig

awk '{if($1!="#"&&$4=="K")print $2, $1}' $in |\
psxy -R$range -$size -St0.06 -G0 -O -K >> $fig

awk '{if($1!="#"&&$4=="S")print $2, $1}' $in |\
psxy -R$range -$size -Ss0.06 -G0  -O -K >> $fig

awk '{if($1!="#")print $2,$1," 8 0 1 LM ",$4$5}' $in |\
pstext -R$range -$size -D0.05/0.03 -W255 -O -K >>$fig

cat << END |psxy -R$range -$size -W2 -O -K >>$fig
124 28.2
130 31.0
END


psbasemap -R$range -$size -B$anot -O -K >>$fig

xoffset=; yoffset=3

export LANG=C

curdir1=$(pwd); now=$(date); host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${fig} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $fig
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${in} (${time})
0 0.90  9 0 1 LM OUTPUT: ${fig} (${timeo})
EOF

PDF=$(basename $fig .ps).pdf
ps2pdfwr $fig $PDF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $fig $PDF
echo

echo "Done $0"

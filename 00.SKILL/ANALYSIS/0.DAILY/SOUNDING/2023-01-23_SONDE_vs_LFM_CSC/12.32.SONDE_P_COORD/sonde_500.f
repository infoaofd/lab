c $1=高度 $2=風向 $3=風速 $4=気圧 $5=気温 $6=湿度 $7=温位 $8=飽和水蒸気圧
c $9=露点温度での水蒸気圧 $10=露点温度 $11=露点温度における水蒸気量 $12=相当温位
C /work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT
      parameter(L=2490,var=14,nt=17,P=6.28)
      real x(var,500)
      integer n
      character in*7,out*12

      open(1,file="list.dat")

      do 1 it=1,nt
         read(1,*) in,out,n

      do i=1,500
         do j=1,var
            x(j,i)=-999.9
         enddo
      enddo

      open(2,file=in)
      open(3,file=out,access="direct",form="unformatted",
     +    recl=4*var*500)


c     1高度 2風向 3風速 4気圧 5気温 6湿度

      do 10 i=1,n
        read(2,*) (x(j,i),j=1,6)

        if(-100.lt.x(5,i).and.x(5,i).le.100) then
         if(0.lt.x(4,i).and.x(4,i).lt.1100) then
c 温位
          x(7,i)=(x(5,i)+273.15)*(1000/x(4,i))**0.2859
c 飽和水上気圧  気温は摂氏
          x(8,i)=6.11*10**((7.5*x(5,i))/(237.3+x(5,i)))
         endif
        end if
         
        if(0.lt.x(6,i).and.x(6,i).le.101) then
         if(0.lt.x(4,i).and.x(4,i).lt.1100) then
          if(-999.ne.x(7,i).and.x(8,i).ne.-999) then   
c 現在の水上気圧
           x(9,i)=x(6,i)*x(8,i)/100
c 露点温度
           x(10,i)=273.2/(1-273.2*0.0001844*log(x(9,i)/6.11))-273.15
c 混合比
           x(11,i)=0.622*x(9,i)/(x(4,i)-x(9,i))
c 相当温位
           x(12,i)=x(7,i)*exp((x(11,i)*L)/(x(10,i)+273.15))
          endif
         endif
        endif
        
        if(0.le.x(2,i).and.x(2,i).le.360) then
           if(0.le.x(3,i).and.x(3,i).le.100) then
           x(13,i)=-(x(3,i)*cos(P*x(2,i)/360))
           x(14,i)=-(x(3,i)*sin(P*x(2,i)/360))
           endif
        endif
 10   continue

c      do 20 k=1,499
c      write(6,*) (x(j,k),j=1,var)
c 20   continue

      write(3,rec=1) (x(1,j),j=1,500),(x(13,j),j=1,500),
     +            (x(14,j),j=1,500),(x(2,j),j=1,500),(x(3,j),j=1,500),
     +            (x(4,j),j=1,500),(x(5,j),j=1,500),(x(6,j),j=1,500),
     +            (x(7,j),j=1,500),(x(8,j),j=1,500),(x(9,j),j=1,500),
     +            (x(10,j),j=1,500),(x(11,j),j=1,500),(x(12,j),j=1,500)
      close(3)
      close(2)

 1    continue
      stop
      end



#!/bin/bash
# /work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT
#
# Author: AM
#
# �����]���f�f�[�^����
#
#
# Host: aofd165.fish.nagasaki-u.ac.jp
# Directory: /work2/kunoki/to_manda_sensei/Tools

if [ $# -ne 2 ]; then
  echo Error in $0 : Wrong number of arugments
  echo Usage: $0 input output
  exit 1
fi

in=$1
out=$2

# awk : �K�v�ȃf�[�^�̔����o��
# $12=���x $10=���� $11=���� $21=�C�� $22=�C�� $23=���x 
#
# sort -n: ���בւ�����
# sort -nr: ���בւ��~��
# filter1d: ������

ZBOT=20 #50 #100 #20 #m
ZINT=20 #50 #100 #20 #m

# Using height as a vertical coordinate
#awk 'BEGIN{FS=","}{if (NR>7) print $12,$10,$11,$21,$22,$23}' $in|\
#sort -n | \
#filter1d -Fb${ZINT} -T${ZBOT}/15000/${ZINT} -N6/0 > $out

# Using pressure as a vertical coordinate
awk 'BEGIN{FS=","}{if (NR>7) print $21,$10,$11,$12,$22,$23}' $in|\
sort -n |\
filter1d -N6/0 -Fb10 -T480/1010/10 |
sort -nr > $out



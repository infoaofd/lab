
!/work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT

INTEGER,PARAMETER::VAR=15
parameter(L=2490,P=3.1415926536*2.0,d2r=3.1415926535897932/180.0)
parameter NLEV=100 !200
real x(var,NLEV) !500)
real ws(NLEV),dir(NLEV)
integer n

real mixr, lev !mixr=WV mix. ratio in g/kg. lev=Pressure in hPa

character csv*50,in*50,ODIR*100,out*150,OFLE*500,STN*3
real DIS
character(len=150)::list,fmt
namelist /para/nt,list,ODIR,fmt

read(5,nml=para)
print *,list

open(1,file=list,action="read")
read(1,*) !Skip comment line

do 1 it=1,nt

  read(1,fmt) STN,DIS,csv,in,n,out
  print *,trim(in),n,trim(out)

  do i=1,NLEV
    do j=1,var
      x(j,i)=-999.9
    enddo
  enddo

  open(2,file=in,action="read") !INPUT

  OFLE=trim(ODIR)//"/"//trim(out)
  open(3,file=OFLE) !OUTPUT

do 10 i=1,n
  read(2,*) (x(j,i),j=1,6)
! Input: 1気圧 2風向 3風速 4高度 5気温 6湿度

  if(-100.lt.x(5,i).and.x(5,i).le.100) then !CHECK TEMP
    if(0.lt.x(1,i).and.x(1,i).lt.1100) then !CHECK PRESSURE
      ! 温位
      x(7,i)=(x(5,i)+273.15)*(1000/x(1,i))**0.2859 !PT
      ! 飽和水蒸気圧  気温は摂氏
      x(8,i)=6.11*10**((7.5*x(5,i))/(237.3+x(5,i))) !SAT WV PRESS
    endif
  end if
 
  if(0.lt.x(6,i).and.x(6,i).le.101) then !CHECK RH
    if(0.lt.x(1,i).and.x(1,i).lt.1100) then !CHECK PRESSURE
      if(-999.ne.x(7,i).and.x(8,i).ne.-999) then   !CHECK PT & SAT.V.PRESS.
        ! 現在の水蒸気圧
        x(9,i)=x(6,i)*x(8,i)/100
        ! 露点温度
        x(10,i)=273.2/(1-273.2*0.0001844*log(x(9,i)/6.11))-273.15
        ! 混合比
        x(11,i)=0.622*x(9,i)/(x(1,i)-x(9,i))
        ! 相当温位
        tc=x(5,i);temp=x(5,i)+273.15; rh=x(6,i); lev=x(1,i) !PT=x(7,i); Qv=x(11,i); Td=x(10,i)
        es= 6.112*exp((17.67*tc)/(tc+243.5))   !Eq.10 of Bolton (1980)
        e=0.01*rh*es                           !Eq.4.1.5 (p. 108) of Emanuel (1994)
        td=(243.5*log(e/6.112))/(17.67-log(e/6.112)) !Inverting Eq.10 of Bolton since es(Td)=e
        dwpk= td+273.15
        Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56 !Eq.15 of Bolton (1980)
        mixr= 0.62197*(e/(lev-e))*1000.0 !Eq.4.1.2 (p.108) of Emanuel(1994) 
        TDL=temp*(1000/(lev-e))**0.2854*(temp/Tlcl)**(0.28*0.001*mixr) !Eq.24 of Bolton
        EPT=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))
        x(12,i)=EPT !BOLTON 1980
        !仮温位
        PT=x(7,i); Qv=x(11,i); x(15,i)=PT*(1.0+0.61*Qv)
      endif
    endif
  endif

  if(0.le.x(2,i).and.x(2,i).le.360) then !CHECK WIND DIRECTION
    if(0.le.x(3,i).and.x(3,i).le.150) then !CHECK WIND SPEED
      ws(i)=x(3,i); azm=x(2,i)
      dir(i) = (270.0 - azm) !*d2r
      u=ws(i)*cos(dir(i))
      v=ws(i)*sin(dir(i))
      x(13,i)=u; x(14,i)=v
    endif
  endif

! 計算結果
! $1=気圧 $2=風向 $3=風速 $4=高度 $5=気温 $6=湿度 $7=温位 $8=飽和水蒸気圧
! $9=露点温度での水蒸気圧 $10=露点温度 $11=露点温度における水蒸気量 $12=相当温位
! $13=u, $14=v, $15=仮温位
10 continue !i

  do i=1,n
    write(3,'(f7.2,f8.1,f7.2,f9.3,f8.2,f7.1,1X,f7.1,f7.2,1X,A,1x,A)')&
&   DIS,x(1,i),WS(I),DIR(I),x(12,i),x(6,i),x(4,i),x(15,i),trim(STN),trim(in)
  end do !i
  close(3)
!  print '(A,A)',trim(in)
!  DO j=1,n
!    print '(2f7.1,14f8.2)',x(1,j),x(4,j),x(5,j),x(6,j),x(12,j)
!  END DO
!  print *

  close(2)

1 continue

      stop
      end



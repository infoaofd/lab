c $1=高度 $2=風向 $3=風速 $4=気圧 $5=気温 $6=湿度 $7=温位 $8=飽和水蒸気圧
c $9=露点温度での水蒸気圧 $10=露点温度 $11=露点温度における水蒸気量 $12=相当温位

      parameter(L=2490,var=14,nt=17,P=6.28)
      real x(var,183),y(var,183)
      integer n
      character in*7,out*12

      open(1,file="list.dat")

      do 1 it=1,nt
         read(1,*) in,out,n

      do i=1,183
         do j=1,var
            x(j,i)=-999.9
            y(j,i)=-999.9
         enddo
      enddo

      open(2,file=in)
      open(3,file=out,access="direct",form="unformatted",
     +    recl=4*var*183)


c     4高度 2風向 3風速 1気圧 5気温 6湿度


c############
      do i=1,n
        read(2,*) (x(j,i),j=1,6)

      if (x(1,1).ne.100) then

        nm=(x(1,1)-100)/5

        do j=1,nm
         do k=1,14
            y(k,j)=-999.9
            end do
        end do

        do ii=1,n
           y(1,nm+ii)=x(1,ii)
           y(2,nm+ii)=x(2,ii)
           y(3,nm+ii)=x(3,ii)
           y(4,nm+ii)=x(4,ii)
           y(5,nm+ii)=x(5,ii)
           y(6,nm+ii)=x(6,ii)
        end do  

    
      else 
         do ii=1,183
           y(1,ii)=x(1,ii)
           y(2,ii)=x(2,ii)
           y(3,ii)=x(3,ii)
           y(4,ii)=x(4,ii)
           y(5,ii)=x(5,ii)
           y(6,ii)=x(6,ii)
          end do
      end if 
      end do

c###############

      do 10 i=1,183

        if(-100.lt.y(5,i).and.y(5,i).le.100) then
         if(0.lt.y(1,i).and.y(1,i).lt.1100) then
c 温位
          y(7,i)=(y(5,i)+273.15)*(1000/y(1,i))**0.2859
c 飽和水上気圧  気温は摂氏
          y(8,i)=6.11*10**((7.5*y(5,i))/(237.3+y(5,i)))
         endif
        end if
         
        if(0.lt.y(6,i).and.y(6,i).le.101) then
         if(0.lt.y(1,i).and.y(1,i).lt.1100) then
          if(-999.9.ne.y(7,i).and.y(8,i).ne.-999.9) then   
c 現在の水上気圧
           y(9,i)=y(6,i)*y(8,i)/100
c 露点温度
           y(10,i)=273.2/(1-273.2*0.0001844*log(y(9,i)/6.11))-273.15
c 混合比
           y(11,i)=0.622*y(9,i)/(y(1,i)-y(9,i))
c 相当温位
           y(12,i)=y(7,i)*exp((y(11,i)*L)/(y(10,i)+273.15))
          endif
         endif
        endif
        
        if(0.le.y(2,i).and.y(2,i).le.360) then
           if(0.le.y(3,i).and.y(3,i).le.100) then
           y(13,i)=-(y(3,i)*cos(P*y(2,i)/360))
           y(14,i)=-(y(3,i)*sin(P*y(2,i)/360))
           endif
        endif
 10   continue

c      do 20 k=1,183
c      write(6,*) (y(j,k),j=1,var)
c 20   continue
 
      write(3,rec=1) (y(1,j),j=1,183),(y(13,j),j=1,183),
     +            (y(14,j),j=1,183),(y(2,j),j=1,183),(y(3,j),j=1,183),
     +            (y(4,j),j=1,183),(y(5,j),j=1,183),(y(6,j),j=1,183),
     +            (y(7,j),j=1,183),(y(8,j),j=1,183),(y(9,j),j=1,183),
     +            (y(10,j),j=1,183),(y(11,j),j=1,183),(y(12,j),j=1,183)
      close(3)
      close(2)

 1    continue
      stop
      end




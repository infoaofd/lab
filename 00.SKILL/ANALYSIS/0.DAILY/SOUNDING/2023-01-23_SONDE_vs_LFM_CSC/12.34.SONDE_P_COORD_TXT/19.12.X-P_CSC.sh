#!/bin/bash
# Author: am
#
# Directory: /work03/am/2022.06.ECS.OBS/12.22.SONDE_3SHIP/12.34.SONDE_P_COORD_TXT
#
# This file is created by /work03/am/mybin/ngmt.sh at Sat, 21 Jan 2023 16:14:59 +0900.

. ./gmtpar.sh
gmtset ANNOT_FONT_SIZE_PRIMARY	= 12p ANNOT_OFFSET_PRIMARY	= 0.05i

range0=-20/165/500/1005; range=-2.5/160/500/1000
size=5/-2
xanot=a20f100:"km"; yanot=a100f50:"hPa":
anot=${xanot}:/${yanot}

INDIR=OUT_SONDE_THERMO
INLIST=$(ls $INDIR/ECS_A1*.TXT)
INMERGE=$(basename $0 .sh)_ECS_A1_LINE_2022-06-01.TXT
echo "# $0 $@" > $INMERGE
for IN in $INLIST; do
cat $IN >> $INMERGE
done #IN

OUT=$(basename $INMERGE .TXT).ps

XYZ=TEMP.XYZ; GRD=TEMP.GRD; CPT=TEMP.CPT
awk '{if($1!="#")print $1,$2,$5}' $INMERGE |\
blockmedian -R$range0 -I10/5 > $XYZ
surface $XYZ -R$range0 -I2/50 -G$GRD -T0.6
makecpt -Chot -I -T336/358/2 -Z >$CPT

grdimage $GRD -R$range -JX$size -C$CPT \
-K -X1.5 -Y7.8 -P >$OUT
psscale -D5.3/1/2/0.1 -C$CPT -E -B4:EPT:/:K: -O -K >> $OUT

grdcontour $GRD -R$range -JX$size -C2 \
-W2/100/100/100 -O -K >>$OUT

grdcontour $GRD -R$range -JX$size -A2f10 -L345.9/346.1 \
-W2/100/100/100 -G1/2 -O -K >>$OUT

grdcontour $GRD -R$range -JX$size -A2f10 -L347.9/348.1 \
-W8/100/100/100 -G1/2 -O -K >>$OUT

cat <<EOF |psxy -R -JX -W1/255/255/255 -G255 -O -K >>$OUT
-100 100
   0 100
   0 1000
-100 1000
-100 100
EOF
cat <<EOF |psxy -R -JX -W1/255/255/255 -G255 -O -K >>$OUT
142 100
200 100
200 1000
142 1000
142 100
EOF


awk '{if($2==980||$2==850||$2==700||$2==600) \
print $1,$2,-$4,$3/50}' $INMERGE |\
psxy -R -JX -Sv0.01/0.1/0.05n0.2 -G0 -O -K >>$OUT
# WIND DIRECTION SHOULD BE MULTIPLIED BY (-1) SINCE Y-AXIS IS
# REVERSED BY -JX OPTION.
psbasemap -JX -R$rannge -B${anot}WsNe -O -K >>$OUT

awk '{if($1!="#"&& $2==1000)print $1, 0}' $INMERGE |\
psxy -JX5/0.1 -R-2.5/160/0/1 -St0.2 -G0 -Y-0.1 -O -K >>$OUT

echo "0.7 0.5 0 0.4" |\
psxy -R0/1/0/1 -JX5/0.15 -Sv0.01/0.1/0.05n0.2 -G0 -O -K -Y-0.3 >>$OUT
echo "0.8 0.5 12 0 1 LM 20 m/s" |\
pstext -R -JX -O -K >>$OUT


rm -vf $XYZ $GRD $CPT



awk '{if($1!="#")print $1,$2,$6}' $INMERGE |\
blockmedian -R$range0 -I10/5 > $XYZ
surface $XYZ -Lu100 -R$range0 -I2/50 -G$GRD -T0.6
makecpt -Cdrywet -T70/100/5 -Z >$CPT

grdimage $GRD -R$range -JX$size -C$CPT \
-K -X0 -Y-2.5 -O -K >>$OUT
psscale -D5.3/1/2/0.1 -C$CPT -E -B5:RH:/:%: -O -K >> $OUT

#grdcontour $GRD -R$range -JX$size -C5 \
#-W2/100/100/100 -O -K >>$OUT
#grdcontour $GRD -R$range -JX$size -A5f10 -L89.9/96.1 \
#-W2/100/100/100 -G1/2 -O -K >>$OUT



awk '{if($1!="#")print $1,$2,$8}' $INMERGE |\
blockmean -R$range0 -I30/5 > $XYZ
surface $XYZ -R$range0 -I2/5 -G$GRD -T0.6

grdcontour $GRD -R$range -JX$size -C2 -A4f9 -W2/100/100/100 -O -K >>$OUT

cat <<EOF |psxy -R -JX -W1/255/255/255 -G255 -O -K >>$OUT
-100 100
   0 100
   0 1000
-100 1000
-100 100
EOF
cat <<EOF |psxy -R -JX -W1/255/255/255 -G255 -O -K >>$OUT
142 100
200 100
200 1000
142 1000
142 100
EOF

xanot=a20f100:""; yanot=a100f50:"hPa":
anot=${xanot}:/${yanot}
psbasemap -JX -R$rannge -B${anot}WsNe -O -K >>$OUT

awk '{if($1!="#"&& $2==1000)print $1, 0}' $INMERGE |\
psxy -JX5/0.1 -R-2.5/160/0/1 -St0.2 -G0 -Y-0.1 -O -K >>$OUT

rm -vf $XYZ $GRD $CPT



xoffset=-1; yoffset=5
curdir1=$(pwd); now=$(date -R); host=$(hostname)

time=$(ls -l ${IN} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${OUT} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${IN} (${time})
0 0.75  9 0 1 LM OUTPUT: ${OUT} (${timeo})
EOF





PDF=$(basename $OUT .ps).pdf; ps2pdfwr $OUT $PDF

echo; echo "INPUT : "
ls -lh --time-style=long-iso $IN
echo "OUTPUT : "
ls -lh --time-style=long-iso $OUT $PDF; echo


echo "Done $0"

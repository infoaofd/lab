#!/bin/bash
# /work04/manda/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU
# /work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT
cruise=ECS_A1_LINE
list_out=${cruise}.LIST_TMP.TXT

INDIR=CSV.EDIT
if [ ! -d $INDIR ]; then echo NO SUCH DIR, $INDIR; exit 1; fi

ODIR=OUT_$(basename $0 .RUN.sh)
# rm -rvf $ODIR
$(which mkd) $ODIR

inlist="\
F2022061900S1103613.edit.CSV
F2022061901S1103614.edit.CSV \
F2022061904S1103502.edit.CSV \
F2022061907S1101372.edit.CSV \
F2022061909S1102917.edit.CSV \
F2022061913S1102920.edit.CSV \
"

exe=16.04.PROC.MEISEI.P.COORD.sh
if [ ! -f $exe ];then echo Error in $0: No such file, $exe;exit 1;fi

echo "# ${cruise}" > ${list_out}

i=1

for input in $inlist; do

  out=$(printf %03d $i).dat
  echo "Input: ${input}      Output: ${out}"

  ${exe} ${INDIR}/$input ${ODIR}/$out
  i=$(expr $i + 1)

  if [ -f ${ODIR}/$out ]; then
    lines=$(wc -l ${ODIR}/$out |awk '{print $1}' )
    echo "${INDIR}/${input}  ${ODIR}/${out}  ${lines}  ${cruise}.grd" >> ${list_out}
  else
    echo Error in $input
  fi

done


echo
echo LIST OF OUTPUT FILES:
echo ${list_out}
echo


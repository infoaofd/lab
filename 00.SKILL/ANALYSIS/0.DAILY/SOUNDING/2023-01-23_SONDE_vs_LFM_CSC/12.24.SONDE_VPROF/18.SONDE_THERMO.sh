#/bin/bash
#w /work03/am/2022.06.ECS.OBS/12.00.SONDE_PREPROC/22.00.SONDE_2022-06_ECS_N-MARU/12.00.TEST.RH.EPT
NML="ECS_2022-06_A1LINE.NAMELIST.TXT"

ODIR=OUT_SONDE_THERMO

$(which mkd) $ODIR

cat <<EOF>$NML
&para
nt=5,
list="ECS_2022-06_A1_LINE.LIST.TXT",\
ODIR="$ODIR",
fmt="(A37,2x,A30,2x,i4,1x,A50)",
&end
EOF

EXE=sonde_thermo

if [ ! -f $EXE ];then NO SUCH FILE, $EXE; exit 1; fi

$EXE < $NML

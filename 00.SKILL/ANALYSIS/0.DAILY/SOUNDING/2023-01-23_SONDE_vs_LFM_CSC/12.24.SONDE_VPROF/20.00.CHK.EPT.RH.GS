 'reinit'

 'cc'
 'set grads off'


 'open 20.00.CHK.EPT.RH.CTL'

'set arrlab on'
'set xlopts 1 3 0.12'; 'set ylopts 1 3 0.16'

'set time 00z19Jun2022 13z19Jun2022'
'set lev 0 5000'; 'set ylint 1000'


'set parea 1.3 7.75 8.65 10.65'

say 'mmmmm RH'
'set gxout grfill'
'color -levs 95 100 -kind white->gray'
'd rh'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xr+0.15; x2=x1+0.1
y1=yb; y2=yt-0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs 1 -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.1
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' [%]'

'set xlab off';'set ylab off'

say 'mmmmm QV'
'set gxout contour'
'set cint 2'; 'set cmin 10'; 'set cmax 20'
'set clskip 2'
'd w*1000.'


'set parea 1.3 7.75 5.65 7.65'
'set xlab on';'set ylab on'
say 'mmmmm EPT'
'set gxout grfill'
'color 336 352 1 -kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta' ;#white->gold->orange->red'
'd ept'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xr+0.15; x2=x1+0.1
y1=yb; y2=yt-0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.1
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' [K]'

'set xlab off';'set ylab off'

 'close 1'


FIG='20.00.CHK.EPT.RH.PDF'
'gxprint 'FIG

say; say 'mmm FIG 'FIG; say
 


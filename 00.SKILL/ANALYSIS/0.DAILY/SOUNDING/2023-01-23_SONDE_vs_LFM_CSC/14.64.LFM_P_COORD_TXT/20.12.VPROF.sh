#!/bin/bash

# Fri, 20 Jan 2023 10:13:54 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/12.22.SONDE_3SHIP/12.22.TEST_PROC_SONDE

YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2022061901}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

# LONW= ;LONE= ; LATS= ;LATN=
#LEV="20 5000"
LEV="1000 500"
TIME=${HH}Z${DD}${MMM}${YYYY}
FH=FH00
CTL=20.00.LFM.EPT.RH.CTL
if [ ! -f  ];then echo ERROR in $: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_LFM_${FH}_${YYYY}-${MM}-${DD}_${HH}.pdf ;#eps

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='-kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

#'q dims';say result
#'q ctlinfo';say result

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 2
ytop=8.5; xwid = 3.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grads off';'set zlog on'
'set xlint 5';'set ylint 100';'set grid off'
'set lev ${LEV}'
'set time ${TIME}'

'q dims';say result

'set cmark 0';'set ccolor 2'
'set vrange 335 355'
'set xlpos 0 b'
'd ept'; say result

#'set xlab off';
'set ylab off'

'set vrange 70 100';'set cmark 0';'set ccolor 4'
'set xlpos 0 t'
'd rh'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



x=(xl+xr)/2; y=yt+0.5
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${HH}UTC${DD}${MMM}${YYYY}'

nmap = 2
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlab on';'set ylab on'

'set grads off';'set zlog on'
'set xlint 5';'set ylint 100';'set grid off'
'set lev ${LEV}'
#'set time ${TIME}'

'set cmark 0';'set ccolor 2'
'set vrange -40 40';'set xlint 20'
'd u'

'set vrange -40 40';'set cmark 0';'set ccolor 4'
'set xlpos 0 b'
'd v'


'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+1;   'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

<<COMMENT
echo; echo MMMMM BACK-UP SCRIPTS
ODIR= ; mkdir -vp $ODIR
TMP=TMP_$(basename $0)
echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
rm -f $TMP
echo MMMMM
COMMENT

echo
if [ -f $FIG ];then echo "OUTPUT : $FIG"; fi
echo

echo "DONE $0."
echo

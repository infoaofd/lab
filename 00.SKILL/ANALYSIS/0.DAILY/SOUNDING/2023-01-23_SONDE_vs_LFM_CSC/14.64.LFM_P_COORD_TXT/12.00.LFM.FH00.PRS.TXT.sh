NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo NO SUCH FILE, $NCL;exit 1;fi

# N3
SLON=128.03339
SLAT=30.15367
YYYY=2022
  MM=06
  DD=19
  HH=01
DIS=0.0
runncl.sh $NCL $SLON $SLAT $YYYY $MM $DD $HH $DIS

# N6
SLON=128.33431
SLAT=30.30233
YYYY=2022
  MM=06
  DD=19
  HH=04
DIS=33.328
runncl.sh $NCL $SLON $SLAT $YYYY $MM $DD $HH $DIS

# K7
SLON=128.66678
SLAT=30.44597
YYYY=2022
  MM=06
  DD=19
  HH=07
DIS=71.234
runncl.sh $NCL $SLON $SLAT $YYYY $MM $DD $HH $DIS

# S10
SLON=129.00081
SLAT=30.58406
YYYY=2022
  MM=06
  DD=19
  HH=09
DIS=106.763
runncl.sh $NCL $SLON $SLAT $YYYY $MM $DD $HH $DIS

# S13
SLON=129.33200
SLAT=30.72464
YYYY=2022
  MM=06
  DD=19
  HH=13
DIS=142.127
runncl.sh $NCL $SLON $SLAT $YYYY $MM $DD $HH $DIS

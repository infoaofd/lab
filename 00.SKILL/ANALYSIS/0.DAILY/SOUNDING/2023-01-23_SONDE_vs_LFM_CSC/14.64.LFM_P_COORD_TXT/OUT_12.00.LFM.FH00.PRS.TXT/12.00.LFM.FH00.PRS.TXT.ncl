load "./SUBSTRING.ncl"
; /work03/am/2022.NAKAMURO_FLUX/42.12.LFM.LA/18.02.LFM.FH00
DSET="LFM"
FH="FH00"

PREFIX="ECS_A1_LINE_"+FH

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")

SLON=tofloat(getenv("NCL_ARG_2"))
SLAT=tofloat(getenv("NCL_ARG_3"))
YYYY=getenv("NCL_ARG_4")
  MM=getenv("NCL_ARG_5")
  DD=getenv("NCL_ARG_6")
  HH=getenv("NCL_ARG_7")
  DIS=getenv("NCL_ARG_8")

INDIR="/work01/DATA/LFM_PROC_ROT_v6/FH00/"
INFLE="LFM_PRS_FH00_VALID_"+YYYY+"-"+MM+"-"+DD+"_"+HH+"_THERMO.nc"
IN=INDIR+INFLE

if (fileexists(IN) .eq. False) then
print("NO SUCH FILE,"+IN)
status_exit(1)
end if

ODIR="OUT_"+scriptname
system("mkdir -vp "+ODIR)
OFLE=ODIR+"/"+PREFIX+"_"+YYYY+"-"+MM+"-"+DD+"_"+HH+".TXT"


f=addfile(IN,"r")


T_ORG=f->temp(0,:,:,:)
RH_ORG=f->rh(0,:,:,:)
Z_ORG=f->z(0,:,:,:)
U_ORG=f->u(0,:,:,:)
V_ORG=f->v(0,:,:,:)
EPT_ORG=f->ept(0,:,:,:)

P1_ORG=f->p
P0_ORG=f->p

LAT_ORG=f->lat
LON_ORG=f->lon

lon=LON_ORG ;(:)
lat=LAT_ORG ;(::-1)

p0=P0_ORG(:)
p0@units="hPa"

temp=T_ORG ;(:,::-1,:)
rh=RH_ORG ;(:,::-1,:)
z=Z_ORG ;(:,::-1,:)
u=U_ORG ;(:,::-1,:)
v=V_ORG ;(:,::-1,:)
ept=EPT_ORG
p1=P1_ORG ;(:)
p1@units="hPa"

temp_s=linint2_points_Wrap(lon,lat,temp,False,SLON,SLAT,0)
rh_s=linint2_points_Wrap(lon,lat,rh,False,SLON,SLAT,0)
z_s=linint2_points_Wrap(lon,lat,z,False,SLON,SLAT,0)
u_s=linint2_points_Wrap(lon,lat,u,False,SLON,SLAT,0)
v_s=linint2_points_Wrap(lon,lat,v,False,SLON,SLAT,0)
ept_s=linint2_points_Wrap(lon,lat,ept,False,SLON,SLAT,0)

ITIME=temp@initial_time
print("ITIME="+ITIME)
FTIME=temp@forecast_time
FTIMEU=temp@forecast_time_units

MM=substring(ITIME,0,1)
DD=substring(ITIME,3,4)
YYYY=substring(ITIME,6,9)
HH=substring(ITIME,12,13)

month=toint(MM)
day=toint(DD)
year=toint(YYYY)
hour=toint(HH)
minute=0
second=0
units="hours since 2022-06-01 00:00:00"

stime = cd_inv_calendar(year,month,day,hour,minute,second,units, 0)
stime@units=units
time=stime

print("mmmmm SET TIME")


time= stime + FTIME
time!0="time"
time&time=time

utc_date = cd_calendar(time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd) +"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)

print("INIT_TIME="+YYYY+MM+DD+" "+HH)
print("FCST_TIME="+FTIME+" "+FTIMEU)
print("VALID_TIME="+VALID_TIME)


temp_out=temp_s(:,0)
temp_out@forecast_time0=FTIME
temp_out@valid_time=VALID_TIME
temp_out@level_type = "Isobaric surface (hPa)" 

rh_out=rh_s(:,0)
rh_out@forecast_time0=FTIME
rh_out@valid_time=VALID_TIME
rh_out@level_type = "Isobaric surface (hPa)" 

z_out=z_s(:,0)
z_out@forecast_time0=FTIME
z_out@valid_time=VALID_TIME
z_out@level_type = "Isobaric surface (hPa)" 

u_out=u_s(:,0)
u_out@forecast_time0=FTIME
u_out@valid_time=VALID_TIME
u_out@level_type = "Isobaric surface (hPa)" 

v_out=v_s(:,0)
v_out@forecast_time0=FTIME
v_out@valid_time=VALID_TIME
v_out@level_type = "Isobaric surface (hPa)" 

ept_out=ept_s(:,0)
ept_out@forecast_time0=FTIME
ept_out@valid_time=VALID_TIME
ept_out@level_type = "Isobaric surface (hPa)" 

r2d=180.0/3.141592653589793
dir_out=atan2(v_out,u_out)*r2d
mag_out=sqrt(u_out^2+v_out^2)

DIS_out=ept_out
DIS_out(:)=tofloat(DIS)

FH=sprinti("%0.2i", FTIME)

system("rm -vf "+OFLE)

header = (/\
"# Input: "+ IN, \
"# X P Wspd Wdir EPT RH Z"/)
hlist=[/header/]

; Create rows in table
alist= [/DIS_out(::-1),p1(::-1),mag_out(::-1),dir_out(::-1),ept_out(::-1),rh_out(::-1),z_out(::-1)/]

; Output the table
write_table(OFLE, "w", hlist, "%s")
write_table(OFLE, "a", alist, "%7.2f %8.1f %7.2f %9.3f %8.2f %7.1f %7.1f %s %s")

system("cp -a "+scriptname_in+" "+ODIR)
print("")
print("MMMMMMMMMMMMMMMMMMMMMM")
print("PRS OUT: "+OFLE)
print("MMMMMMMMMMMMMMMMMMMMMM")

#!/bin/bash

# Sat, 19 Nov 2022 15:06:58 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/18.12.SKEW-T/12.12.TEST.SKEW.T
# https://gitlab.com/infoaofd/lab/-/blob/master/MEETING/2021/2021-07-08/2021-07-08.md

YYYYMMDDHH=${YYYYMMDD:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

INDIR="/work01/DATA/LFM_PROC_ROT_v2/FH00/"
INFLE="LFM_PRS_FH00_VALID_${YYYY}-${MM}-${DD}_${HH}_THERMO.nc"
IN=$INDIR/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; echo;exit 1; fi

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

#CTL=$(basename $0 .sh).CTL
GS=$(basename $0 .sh).GS

LON1=128; LAT1=30
LEV="1000 300"

FIG=GRADS_SKEW_${LON1}_${LAT1}_${YYYY}-${MM}-${DD}_${HH}.pdf

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

# ${NOW}
# ${HOST}
# ${CWD}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'


'set lon ${LON1}'; 'set lat ${LAT1}'
'set lev ${LEV}'
'set time ${TIME}'

say 'MMMMM CHECK COORDINATES'
'q dims';say sublin(result,2);say sublin(result,3);say sublin(result,4)
say sublin(result,5)
say

say 'MMMMM DEWPOINT'
'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'         ;# Eq.10 of Bolton (1980)
'e=0.01*rh*es'                                 ;# Eq.4.1.5 (p. 108) of Emanuel (1994)
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))' ;# Inverting Eq.10 of Bolton since es(Td)=e
'dwpk=td+273.15'
say

say 'MMMMM WIND SPEED & DIRECTION'
'wspd=mag(u,v)'
'wdir=57.3*atan2(u,v)+180'

say 'MMMMM SKEW-T/LOG-P DIAGRAM'
rc=plotskew(tc,td,wspd,wdir)

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'



# TEXT
x=(xl+xr)/2; 
y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${LAT1}N ${LON1}E ${TIME}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.15
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.15
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.15
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.15
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'


$(cat /usr/local/grads-2.2.1/scripts/plotskew.gs)
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

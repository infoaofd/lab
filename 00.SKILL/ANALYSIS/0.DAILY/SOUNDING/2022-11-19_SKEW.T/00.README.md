
Skew-T Diagram
=======================

/work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/18.12.SKEW-T/12.12.TEST.SKEW.T  
Fri, 18 Nov 2022 18:13:15 +0900  

## NCL
SCRIPTS:  
- 00.TEST.SKEW-T.ncl
- skewt_func.ncl
- $NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl  

```bash
$ ncl 00.TEST.SKEW-T.ncl 

INPUT /work01/DATA/LFM_PROC_ROT_v2/FH00//LFM_PRS_FH00_VALID_2022-06-19_03_THERMO.nc

FIG SKEW_128_30_2022-06-19_03.pdf
```

## GrADS
SCRIPTS: 
- 02.TEST.SKEW-T.GRADS.sh
- /usr/local/grads-2.2.1/scripts/plotskew.gs

```bash
$ 02.TEST.SKEW-T.GRADS.sh 

Grid Analysis and Display System (GrADS) Version 2.2.1

MMMMM CHECK COORDINATES
X is fixed     Lon = 128  X = 161
Y is fixed     Lat = 30  Y = 191
Z is varying   Lev = 1000 to 300   Z = 1 to 12
T is fixed     Time = 00Z19JUN2022  T = 1

MMMMM DEWPOINT

MMMMM WIND SPEED & DIRECTION
MMMMM SKEW-T/LOG-P DIAGRAM
Drawing temperature sounding.
Drawing dewpoint sounding.
Drawing parcel path from surface upward.
Calculating precipitable water.
Calculating thermodynamic indices.
Drawing Wind Profile.
Drawing Hodograph.
Calculating Helicity & SR Helicity.
Done.
GX Package Terminated 

OUTPUT : 
-rw-r--r--. 1 am oc 57K 2022-11-19 15:29 GRADS_SKEW_128_30_2022-06-19_00.pdf

DONE ./02.TEST.SKEW-T.GRADS.sh.
```

; 
; 00.TEST.SKEW-T.ncl
; 
; Fri, 18 Nov 2022 18:15:56 +0900
; p5820.bio.mie-u.ac.jp
; /work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/18.12.SKEW-T/12.12.TEST.SKEW.T
; dewtemp_trh
;   http://www.ncl.ucar.edu/Document/Functions/Built-in/dewtemp_trh.shtml
; Dew Point Calculator
;   http://www.dpcalc.org/
; Skew-T
;   https://www.ncl.ucar.edu/Applications/skewt.shtml
; Missing values in NCL
;   http://www.ncl.ucar.edu/Document/Language/fillval.shtml

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "./skewt_func.ncl"

SCRIPT  = get_script_name()
ARG    = getenv("NCL_ARG_2")

YMDH="2022-06-19_03"
;LON1=129.5
;LAT1=30.0
LON1=128.0
LAT1=30.0

INDIR="/work01/DATA/LFM_PROC_ROT_v2/FH00/"
INFLE="LFM_PRS_FH00_VALID_"+YMDH+"_THERMO.nc"

IN=INDIR+"/"+INFLE
a=addfile(IN,"r")

time=a->time
lon=a->lon
lat=a->lat
p=a->p

t_in=a->temp
r_in=a->rh
u_in=a->u
v_in=a->v
z_in=a->z

xo=LON1
yo=LAT1
xi=lon
yi=lat
fi=t_in
t_1 = linint2_points(xi,yi,fi, False, xo,yo, 0)
fi=r_in
r_1 = linint2_points(xi,yi,fi, False, xo,yo, 0)
fi=u_in
u_1 = linint2_points(xi,yi,fi, False, xo,yo, 0)
fi=v_in
v_1 = linint2_points(xi,yi,fi, False, xo,yo, 0)
fi=z_in
z_1 = linint2_points(xi,yi,fi, False, xo,yo, 0)

tk=t_1(0,:,0)
tc=tk-273.15
rh=r_1(0,:,0)
u=u_1(0,:,0)
v=v_1(0,:,0)
z=z_1(0,:,0)
tdk  = dewtemp_trh(tk,rh)    ; dew pt temperature [K]
tdc  = tdk-273.15              ; [C]
wdir = wind_direction(u,v,0)
wspd = wind_speed(u,v)
;print(p+" "+z+" "+tk+" "+rh+" "+u+" "+v+" "+tdc+" "+wdir+" "+wspd)

p@_FillValue    = -999.0
wdir@_FillValue = -999.0
wspd@_FillValue = -999.0
   z@_FillValue    = -999.0
  rh@_FillValue    = -999.0
  tc@_FillValue    = -999.0000
   p@units = "hPa"
wdir@units = "deg"
wspd@units = "m/s"
   z@units = "m"
  rh@units = "%"
  tc@units = "C"


FIG="SKEW_"+LON1+"_"+LAT1+"_"+YMDH
TYP="pdf"
wks  = gsn_open_wks (TYP, FIG)

dataOpts           = True     ; options describing data and ploting
dataOpts@Wthin     = 1        ; plot every n-th wind barb
dataOpts@PrintZ           = True    ; do not print Z 
dataOpts@ThermoInfo       = True    ; print thermodynamic info
dataOpts@colTemperature  = "red"
dataOpts@colDewPt        = dataOpts@colTemperature
dataOpts@colWindP        = dataOpts@colTemperature
dataOpts@colDewPt        = dataOpts@colTemperature
dataOpts@linePatternDewPt = 1

skewtOpts                 = True
skewtOpts@DrawColAreaFill = True    ; default is False
skewtOpts@tiMainString    = ""
skewtOpts@DrawFahrenheit  = False   ; default is True
skewtOpts@DrawIsotherm      = False
skewtOpts@DrawIsobar        = False
skewtOpts@DrawMixRatio      = True
skewtOpts@DrawDryAdiabat    = True
skewtOpts@DrawMoistAdiabat  = True ; aka: saturation or pseudo adibat
skewtOpts@DrawWind          = True
skewtOpts@DrawStandardAtm   = False
skewtOpts@DrawColLine       = True
skewtOpts@DrawColAreaFill   = True 

skewt_bkgd = skewT_BackGround (wks, skewtOpts)
draw (skewt_bkgd)

dataOpts@xpWind    = 40.      ; new location for winds [default 45]
dataOpts@colTemperature  = "black"
dataOpts@colDewPt        = dataOpts@colTemperature
dataOpts@colWindP        = dataOpts@colTemperature

skewt_data = skewT_PlotData (wks, skewt_bkgd, p,tc,tdc,z \
                                , wspd,wdir, dataOpts)

;HEADER
txres=True
txres@txFontHeightF = 0.01
txres@txJust="CenterLeft"
NOW = systemfunc("date -R")
CWD=systemfunc("pwd")

gsn_text_ndc(wks,NOW,  0.2,0.99,txres)
gsn_text_ndc(wks,"Current dir: "+CWD, 0.2,0.97,txres)
gsn_text_ndc(wks,"Script: "+SCRIPT, 0.2,0.95,txres)
gsn_text_ndc(wks,"INDIR: "+INDIR, 0.2,0.93,txres)
gsn_text_ndc(wks,"INFLE: "+INFLE, 0.2,0.91,txres)
gsn_text_ndc(wks,"FIG: "+FIG+"."+TYP, 0.2,0.89,txres)

print("")
print("INPUT "+IN)
print("")
print("FIG " + FIG+"."+TYP)
print("")

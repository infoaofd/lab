
Variable: temp
Type: float
Total Size: 4262208 bytes
            1065552 values
Number of Dimensions: 4
Dimensions and sizes:	[time | 1] x [p | 12] x [lat | 316] x [lon | 281]
Coordinates: 
            time: [ 435.. 435]
            p: [300..1000]
            lat: [22.4..35]
            lon: [120..134]
Number Of Attributes: 12
  center :	Japanese Meteorological Agency - Tokyo (RSMC)
  production_status :	Operational products
  long_name :	Temperature
  units :	K
  grid_type :	Latitude/longitude
  parameter_discipline_and_category :	Meteorological products, Temperature
  parameter_template_discipline_category_number :	( 0, 0, 0, 0 )
  level_type :	Isobaric surface (Pa)
  forecast_time :	180
  forecast_time_units :	minutes
  initial_time :	06/19/2022 (00:00)
  _FillValue :	1e+20

Variable: rh
Type: float
Total Size: 4262208 bytes
            1065552 values
Number of Dimensions: 4
Dimensions and sizes:	[time | 1] x [p | 12] x [lat | 316] x [lon | 281]
Coordinates: 
            time: [ 435.. 435]
            p: [300..1000]
            lat: [22.4..35]
            lon: [120..134]
Number Of Attributes: 12
  center :	Japanese Meteorological Agency - Tokyo (RSMC)
  production_status :	Operational products
  long_name :	Relative humidity
  units :	%
  grid_type :	Latitude/longitude
  parameter_discipline_and_category :	Meteorological products, Moisture
  parameter_template_discipline_category_number :	( 0, 0, 1, 1 )
  level_type :	Isobaric surface (Pa)
  forecast_time :	180
  forecast_time_units :	minutes
  initial_time :	06/19/2022 (00:00)
  _FillValue :	1e+20

netcdf ETOPO1_CUT_LFM_PRS {
dimensions:
	lon = 281 ;
	lat = 316 ;
variables:
	double lon(lon) ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degree" ;
		lon:axis = "X" ;
	double lat(lat) ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degree" ;
		lat:axis = "Y" ;
	float HGT(lat, lon) ;
		HGT:_FillValue = -2.147484e+09f ;
		HGT:missing_value = -2.147484e+09f ;

// global attributes:
		:CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
		:Conventions = "CF-1.6" ;
		:script = "00.NCL.ETOPO1.CUT" ;
		:directory = "/work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/02.18.NCL.ETOPO1.CUT" ;
		:creation_date = "Tue, 20 Dec 2022 22:31:27 +0900" ;
		:history = "Wed Dec 21 08:18:25 2022: cdo -mul ETOPO1_TMP_LFM_PRS.nc -gec,0.1 ETOPO1_TMP_LFM_PRS.nc /work02/DATA/ETOPO1/ETOPO1_CUT_LFM_PRS.nc\n",
			"Wed Dec 21 08:18:23 2022: cdo remapcon,MYGRID.TXT /work02/DATA/ETOPO1/ETOPO1_CUT.nc ETOPO1_TMP_LFM_PRS.nc" ;
		:CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
}

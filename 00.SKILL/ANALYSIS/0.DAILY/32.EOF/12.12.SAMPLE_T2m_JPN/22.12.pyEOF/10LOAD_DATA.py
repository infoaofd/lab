from pyEOF import *
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


# load the DataArray
da = xr.open_dataset('ERA5_T2m_120-160_20-50_DJF_ANO.nc')["T2ADJF"]
print(da)

# create a mask
mask = da.sel(time=da.time[0])
mask = mask.where(mask<-999.).isnull().drop("time")

# get the DataArray with mask
da = da.where(mask)
da.sel(time=da.time[0]).plot()

FIG="10LOAD_DATA_ERA5_T2mA_DJF_CHECK.PDF"
plt.savefig(FIG)
print("")
print("FIG: "+FIG)
print("")



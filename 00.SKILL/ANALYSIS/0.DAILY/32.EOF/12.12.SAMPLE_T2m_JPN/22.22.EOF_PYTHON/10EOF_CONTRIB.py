import numpy as np
import matplotlib.pyplot as plt
import xarray as xr

ds=xr.open_dataset("ERA5_T2m_120-160_20-50_DJF_ANO.nc")

import cartopy.crs as ccrs

dslp = ds.T2ADJF

wgt = np.sqrt(np.abs(np.cos(np.deg2rad(ds.lat))))

X = dslp * wgt
X = (X.where(ds.lat<60, drop=True)).data.reshape(X.shape[0],-1).transpose()

U, s, V = np.linalg.svd(X)

contrib = (s * s) / (s @ s) * 100

x = list(range(1, 11))
fig, ax = plt.subplots(figsize=(8,8))
ax.plot(x,contrib[0:10])
ax.plot(x,contrib[0:10],'bo')
ax.set_title("contribution rate %", fontsize=14)
ax.grid()
fig.savefig("10EOF_CONTRIB_ERA5_T2m_DJF.PDF")

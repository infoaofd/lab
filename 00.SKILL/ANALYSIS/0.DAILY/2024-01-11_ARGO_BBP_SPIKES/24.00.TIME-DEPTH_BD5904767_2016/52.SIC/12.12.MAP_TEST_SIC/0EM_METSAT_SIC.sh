NCL=$(basename $0 .sh).ncl

if [ ! -f $NCL ];then echo NO SUCH FILE,$NCL;exit 1;fi

Y=$1; M=$2; D=$3

Y=${Y:-2017};M=${M:-08};D=${D:-01};
runncl.sh $NCL $Y $M $D

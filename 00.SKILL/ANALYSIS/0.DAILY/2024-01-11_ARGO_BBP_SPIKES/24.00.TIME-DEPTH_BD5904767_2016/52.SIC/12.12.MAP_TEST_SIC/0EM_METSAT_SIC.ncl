script_name  = get_script_name() ;スクリプトの名前を得る

Y  = getenv("NCL_ARG_2") 
M   = getenv("NCL_ARG_3")
D   = getenv("NCL_ARG_4")

YMD=Y+M+D

INDIR="/work01/DATA/SIC/EU_METSAT_SIC"
INFLE="ice_conc_sh_ease2-250_icdr-v2p0_"+YMD+"1200"

IN=INDIR+"/"+INFLE

a = addfile(IN+".nc","r")

ISIC=a->ice_conc(0,:,:)
SIC=tofloat(ISIC)*ISIC@scale_factor

SIC@lat2d = a->lat
SIC@lon2d = a->lon

TYP="PDF"
wks = gsn_open_wks(TYP,INFLE)       

res                     = True         
res@gsnMaximize         = True         

res@cnFillOn            = True         
res@cnFillPalette       = "MPL_jet"       
res@cnLinesOn           = False        
;res@cnFillMode          = "RasterFill" 
res@pmLabelBarWidthF    = 0.5          
res@pmLabelBarHeightF   = 0.05          
res@lbLabelFontHeightF  = .018         

res@trGridType = "TriangularMesh"    

res@gsnPolar   = "SH"
res@mpMaxLatF  = -60
;res@mpMinLatF  = -70
res@mpDataBaseVersion    = "HighRes"
res@tiMainString = Y+"-"+M+"-"+D

plot = gsn_csm_contour_map_polar(wks,SIC,res)   

FIG=INFLE+"."+TYP
print("")
print("FIG: " + FIG)

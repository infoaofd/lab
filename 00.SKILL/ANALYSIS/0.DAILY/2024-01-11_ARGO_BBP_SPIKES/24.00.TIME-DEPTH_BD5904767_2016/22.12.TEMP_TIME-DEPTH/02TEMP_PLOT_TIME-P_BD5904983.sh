#!/bin/bash

STN=BD5904983
IN=${STN}_TIME-P-TEMP.TXT
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

rm -fv .gmtdefaults4 .gmtdefaults4 
./gmtpar.sh

# echo MMMMM CHECK INPUT DATA
# awk '{if ($5!="nan" && $6!="nan" ) print $4,$5,$6}' $IN 

# echo MMMMM CHECK MIN AND MAX OF INPUT DATA
# awk '{if ($5!="nan" && $6!="nan" ) print $4,$5,$6}' $IN |minmax

echo MMMMM INTERPOLATE INPUT DATA
NC=$(basename $IN .TXT).nc
RANGE=366/600/0/1000
RANGE1=0/1581/0/500
RANGE2=0/1581/500/1000

awk '{if ($5!="nan" && $6!="nan" ) print $4,$5,$6}' $IN |\
surface -R$RANGE -G${NC} -I1/1 -T0.8
if [ $? -ne 0 ];then echo EEEEE ERROR in SURFACE COMMAND;exit 1;fi 
echo

echo MMMMM CHECK INTERPOLATED DATA
ncdump -h $NC

echo MMMMM MAKE COLOR PALETTE FILE
CPT=$(basename $0 .sh).CPT
makecpt -T-3.5/3.5/0.5 -Cno_green > $CPT

echo MMMMM PLOT
PS=$(basename $0 .sh).ps #_$(basename $IN .TXT).ps

echo MMMMM PLOT UPPER LAYER
gmtset ANNOT_FONT_SIZE_PRIMARY +14p LABEL_FONT_SIZE 12p
grdimage $NC -R$RANGE1 -JX6/-2.2 -C$CPT -X1.2 -Y5 -K -P >$PS
grdcontour $NC -R -JX -C0.5 -W1 -O -K >>$PS

psbasemap -R -JX -Ba60f30/a100f100:"Depth${sp}[m]":Wse \
-O -K >>$PS

gmtset PLOT_DATE_FORMAT o TIME_FORMAT_PRIMARY Character ANNOT_FONT_SIZE_PRIMARY +14p
#gmtset PLOT_DATE_FORMAT o TIME_FORMAT_PRIMARY abbreviated ANNOT_FONT_SIZE_PRIMARY +14p

rangeT="2016-01-01T00:00:00/2020-04-30T00:00:00"
psbasemap -JX -R$rangeT/0/2000 -Bpa3O/a10f5N -Bsa1Y/ -O -K >> $PS

echo MMMMM PLOT LOWER LAYER
gmtset LABEL_FONT_SIZE 12p ANNOT_FONT_SIZE_PRIMARY +14p
grdimage $NC -R$RANGE2 -JX6/-2.2 -C$CPT -Y-2.7 -O -K >>$PS
grdcontour $NC -R$RANGE2 -JX -C0.5 -Ba60f30/a500f100:"Depth${sp}[m]":Wsne -W1 -O -K >>$PS

psscale -D6.2/2.3/3.4/0.1 -C$CPT -E -B1:"":/:@+o@+C: -O -K >>$PS

XOFFSET=-1;YOFFSET=3
CWD=$(pwd); NOW=$(date -R)�GHOST=$(hostname)
#TIME=$(ls -l ${IN} | awk '{print $6, $7, $8}')
pstext -JX20/4 -R0/1/0/1.5 -N  -X${XOFFSET:-0} -Y${YOFFSET:-0} <<EOF \
-O >> $PS
0 1.5 12 0 1 LM $0 $@
0 1.4 12 0 1 LM ${NOW}
0 1.3 12 0 1 LM ${CWD}
0 1.2 12 0 1 LM Input: ${IN}
EOF
# x   y   size   angle   fontno   justify   text

rm -vf $NC $CPT

if [ -f $PS ];then 
PDF=$(basename $PS .ps ).PDF
ps2pdfwr $PS $PDF
rm -vf $PS
fi
echo
echo FIG: $PDF
echo
exit 0

# coding: utf-8
# 日本語を使いたい時はcoding: utf-8をファイルの先頭に入れる

"""
ライブラリのインポート
"""
import numpy as np # 数値計算のためのライブラリ
#import xarray as xr  #netCDF4を読み込むのに使用する
import netCDF4
import inspect
import sys #python上でLinuxコマンドを用いるためのライブラリ
# https://note.nkmk.me/python-command-line-arguments/
import pandas as pd
import datetime
from datetime import timedelta

#LISTNAME='BD5904983'
#LISTNAME='BD5904767_1'
LISTNAME='BD5904767_2'

INDIR="/work01/DATA/ARGO/"

#入力ファイルの一覧
INLIST='1FNAME_LIST_'+ LISTNAME +'.TXT'

# 読み込み用にファイルを開く
f = open(INLIST, 'r')

#入力ファイルを読み込んで行単位で分割してリストとして取得する
#入力ファイルの1行目は読み飛ばす
INFLE = f.readlines()[1:]
f.close() #ファイルを閉じる

NF=len(INFLE)

OFLE=LISTNAME+"_TIME-P-TEMP.TXT"
NF=len(INFLE)

with open(OFLE, "w") as f:

  """
  データの読み込み
  """
  for i in range(NF):
    IN=INDIR+"/"+INFLE[i].rstrip('\n')
    print(IN)
    nc = netCDF4.Dataset(IN,'r')

# 変数INに記憶されているファイル名のファイルを開く
# 開いたファイルのポインターはncとする
    lat=float(nc['LATITUDE'][0])
# ファイルncに含まれるLATITUDEという変数を変数latに代入する
    lon=float(nc['LONGITUDE'][0])
# ファイルncに含まれるLONGITUDEという変数を変数lonに代入する
#  print(str(lon)+" "+str(lat))
    TEMP=nc.variables['TEMP_DOXY']
    PRES=nc.variables['PRES']

# https://drive.google.com/drive/folders/16e3bE6jZyBKIEY-FRmzSKUTomAvxgDQp
    juld = float(nc.variables['JULD'][0])
    d=datetime.datetime(1950, 1, 1, 0, 0, 0, 0)+datetime.timedelta(days=juld)

    dt1=datetime.datetime(1950, 1, 1, 0, 0, 0, 0)
    dt2=datetime.datetime(2016, 1, 1, 0, 0, 0, 0)
    td = (dt2 - dt1)

    dout=d.strftime('%Y-%m-%dT%H:%M:%S')
    for k in range(TEMP.shape[1]):
      if  ( (not(np.isnan(TEMP[0][k]))) and (not (np.isnan(PRES[0][k]))) ):
        print('%s %d %d %f %f %f' % (dout, i+1, k, juld-td.days+1, PRES[0][k], TEMP[0][k]),file=f)

print("OUTPUT: "+OFLE)

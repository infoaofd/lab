# coding: utf-8
# 日本語を使いたい時はcoding: utf-8をファイルの先頭に入れる

"""
ライブラリのインポート
"""
import numpy as np # 数値計算のためのライブラリ
import xarray as xr  #netCDF4を読み込むのに使用する
import sys #python上でLinuxコマンドを用いるためのライブラリ
# https://note.nkmk.me/python-command-line-arguments/
import pandas as pd
import datetime

#LISTNAME='BD5904767_1'
LISTNAME='BD5904767_2'

INDIR="/work01/DATA/ARGO/"

#入力ファイルの一覧
INLIST='1FNAME_LIST_'+ LISTNAME +'.TXT'

# 読み込み用にファイルを開く
f = open(INLIST, 'r')

#入力ファイルを読み込んで行単位で分割してリストとして取得する
#入力ファイルの1行目は読み飛ばす
INFLE = f.readlines()[1:]
f.close() #ファイルを閉じる

NF=len(INFLE)

OUT=LISTNAME+"_LOC_MAP_POLAR_STEREO.PDF"

lat=[]
lon=[]
"""
データの読み込み
"""
for i in range(NF):
  IN=INDIR+"/"+INFLE[i].rstrip('\n')
  print(IN)
  nc = xr.open_dataset(IN) 
# 変数INに記憶されているファイル名のファイルを開く
# 開いたファイルのポインターはncとする
  lat.append( float(nc['LATITUDE'][0]) )
# ファイルncに含まれるLATITUDEという変数を変数latに代入する
  lon.append( float(nc['LONGITUDE'][0]) )
# ファイルncに含まれるLONGITUDEという変数を変数lonに代入する
#  print(str(lon)+" "+str(lat))

  julday = pd.to_datetime(nc['JULD'])
# ファイルncに含まれるJULDという変数を変数juldayに代入する
# JULDはJulian day （ユリウス日）の意味
# 日付を抜き出す
# https://note.nkmk.me/python-pandas-datetime-timestamp/
  date=julday[0]

  xr.Dataset.close(nc)


"""
地図に観測点の位置を示す点を打つ
https://qiita.com/earth06/items/d41b74f3e5504d5c74a3
"""
import cartopy.crs as ccrs
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(9,6))

ax=fig.add_subplot(1,2,2,projection=ccrs.SouthPolarStereo(central_longitude=180))
ax.coastlines(resolution='50m')

# 地図上に点を書く
for i in range(NF):
  ax.plot(lon[i],lat[i],'ro',markersize='4', transform=ccrs.PlateCarree())
# 正距円筒図法以外で作図する場合 transform=ccrs.PlateCarree()をつける
# https://yyousuke.github.io/matplotlib/cartopy.html

gl=ax.gridlines(draw_labels=True,linestyle='--',xlocs=plt.MultipleLocator(20)
             ,ylocs=plt.MultipleLocator(15))
gl.xlabel_style={'size':8,'color':'black'}
gl.ylabel_style={'size':8,'color':'black'}

ax.set_extent([-180,181,-90,-60],ccrs.PlateCarree())
plt.title(str(lat)+" "+str(lon))

plt.tight_layout()

import matplotlib.pyplot as plt
plt.savefig(OUT) #図をファイルに保存する

print("OUT: "+OUT)





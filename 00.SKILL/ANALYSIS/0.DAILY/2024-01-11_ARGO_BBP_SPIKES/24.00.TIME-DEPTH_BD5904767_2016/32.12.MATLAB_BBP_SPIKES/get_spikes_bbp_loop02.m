function [spikes, varargout] = get_spikes_bbp_loop(bbp1, p1, xerr, pr, max_iter)


%folder = 'C:\DATA\ARGO\';
%fileList = dir(fullfile(folder, '*.nc'));

%numFiles = length(fileList);

mylist = dir('C:\DATA\ARGO/*.nc');

n = size(mylist,1);

DEBUG = true;

%numFiles = 1

for i = 1:10

    fname = strcat('C:\DATA\ARGO\', mylist(i).name);
    fname

    %filename = fullfile(folder, fileList(j).name);
    
    ncid = netcdf.open(fname);
    
    try
    varid1 = netcdf.inqVarID(ncid, "BBP700");
    catch
    continue
    end
    bbp = netcdf.getVar(ncid, varid1);
    
    varid2 = netcdf.inqVarID(ncid, "PRES");
    p = netcdf.getVar(ncid, varid2);

    bbp1 = bbp(:, 1);
    p1 = p(:, 1);
    


end

end

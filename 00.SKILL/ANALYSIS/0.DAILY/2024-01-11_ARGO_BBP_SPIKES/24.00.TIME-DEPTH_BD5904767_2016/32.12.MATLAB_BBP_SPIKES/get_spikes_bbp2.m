function get_spikes_bbp(bbp, p)

% Get spikes in bbp signal

% Check input
if size(bbp,2) ~= 1; error('bbp is not a row vector (Nx1).'); end
if any(size(p) ~= size(bbp)); error('Input vectors are different size.'); end

% Prepare input
spikes = false(size(bbp, 1));

% Apply Hampel filter
xd = hampel(bbp, 15); % > 5 is good

% Get spikes
x_delta = bbp1 - xd;
spikes = logical(x_delta > 3 * median(abs(bbp1-median(bbp1))));

% Return output
if nargout == 2
  return p(spikes);
else
  return p(spikes), ~spikes;
end
end
# coding: utf-8
# 日本語を使いたい時はcoding: utf-8をファイルの先頭に入れる

"""
ライブラリのインポート
"""
import numpy as np # 数値計算のためのライブラリ
#import xarray as xr  #netCDF4を読み込むのに使用する
import netCDF4
import inspect
import sys #python上でLinuxコマンドを用いるためのライブラリ
# https://note.nkmk.me/python-command-line-arguments/
import pandas as pd
import datetime
from datetime import timedelta

INDIR="/work01/DATA/ARGO/"
INFLE=["BD5904983_001", "BD5904983_002", "BD5904983_003",
"BD5904983_004", "BD5904983_005", "BD5904983_006",
"BD5904983_007", "BD5904983_008", "BD5904983_009",
"BD5904983_010", "BD5904983_011", "BD5904983_012",
"BD5904983_013", "BD5904983_014", "BD5904983_015",
"BD5904983_016", "BD5904983_017", "BD5904983_018",
"BD5904983_019", "BD5904983_020", "BD5904983_021",
"BD5904983_022", "BD5904983_023", "BD5904983_024"]
NF=len(INFLE)

OFLE="BD5904983_TIME-P-CHLA.TXT"

with open(OFLE, "w") as f:

  """
  データの読み込み
  """
  for i in range(NF):
    IN=INDIR+"/"+INFLE[i]+".nc"
    print(IN)
    nc = netCDF4.Dataset(IN,'r')

# 変数INに記憶されているファイル名のファイルを開く
# 開いたファイルのポインターはncとする
    lat=float(nc['LATITUDE'][0])
# ファイルncに含まれるLATITUDEという変数を変数latに代入する
    lon=float(nc['LONGITUDE'][0])
# ファイルncに含まれるLONGITUDEという変数を変数lonに代入する
#  print(str(lon)+" "+str(lat))
    TEMP=nc.variables['CHLA']
    PRES=nc.variables['PRES']

# https://drive.google.com/drive/folders/16e3bE6jZyBKIEY-FRmzSKUTomAvxgDQp
    juld = float(nc.variables['JULD'][0])
    d=datetime.datetime(1950, 1, 1, 0, 0, 0, 0)+datetime.timedelta(days=juld)

    dt1=datetime.datetime(1950, 1, 1, 0, 0, 0, 0)
    dt2=datetime.datetime(2017, 1, 1, 0, 0, 0, 0)
    td = (dt2 - dt1)

    dout=d.strftime('%Y-%m-%dT%H:%M:%S')
    for k in range(TEMP.shape[1]):
      if  ( (not(np.isnan(TEMP[0][k]))) and (not (np.isnan(PRES[0][k]))) ):
        print('%s %d %d %f %f %f' % (dout, i+1, k, juld-td.days+1, PRES[0][k], TEMP[0][k]),file=f)

print("OUTPUT: "+OFLE)

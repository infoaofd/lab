
DSET=JRA55C
TYPE=surf125
VAR1=U10
VAR2=V10
VARGRIB1=033_ugrd
VNAME_IN_GRIB1=UGRD_GDS0_HTGL
VARGRIB2=034_vgrd
VNAME_IN_GRIB2=VGRD_GDS0_HTGL

VARO=U10V10

YS=1980; YE=2012

OPT1=merge

INFO=$(basename $0 .sh).LOG

INDIR=/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/24.22.SELMON.NC/OUT_SELMON_JRA55C.MON.SFC

ODIR="NC"
mkdir -vp  $ODIR
rm -v $ODIR/* &> $INFO

date -R >$INFO
pwd    >>$INFO
echo   >>$INFO

MS=1
M=$MS; ME=12 #$MS
while [ $M -le $ME ]; do 

MM=$(printf %02d $M)

OFLE=$ODIR/${DSET}.${VARO}.${YS}-${YE}_${MM}.nc
rm -vf $OFLE

INFLE1=$INDIR/JRA55C.U10.${YS}-${YE}_${MM}.nc
INFLE2=$INDIR/JRA55C.V10.${YS}-${YE}_${MM}.nc

echo "======================="
echo $INFLE1
echo $INFLE2
echo $OFLE
echo "======================="
echo "=======================" &>>$INFO
echo $INFLE1 &>>$INFO
echo $INFLE2 &>>$INFO
echo $OFLE &>>$INFO
echo "======================="&>>$INFO

if [ -f $OFLE ];then
echo "mmmmmmmmmmmmmmmmmmmmmmmmmmmmm"
echo rm -f $OFLE 
echo "mmmmmmmmmmmmmmmmmmmmmmmmmmmmm"

echo "mmmmmmmmmmmmmmmmmmmmmmmmmmmmm"&>>$INFO
rm -f $OFLE &>>$INFO
echo "mmmmmmmmmmmmmmmmmmmmmmmmmmmmm"&>>$INFO
fi

echo cdo $OPT1 $INFLE1 $INFLE2 $OFLE &>>$INFO
     cdo $OPT1 $INFLE1 $INFLE2 $OFLE &>>$INFO
if [ $? -ne 0 ]; then
echo
echo ERROR in $0
tail -f $INFO
echo
exit 1
fi

if [ -f $OFLE ]; then
echo "======================="
echo $OFLE
echo "======================="
echo "=======================" &>>$INFO
echo $OFLE&>>$INFO
echo "======================="&>>$INFO
cdo sinfo $OFLE 
#<<COMMENT
echo &>>$INFO
cdo sinfo $OFLE &>> $INFO
echo &>> $INFO
cdo showname $OFLE
cdo showname $OFLE &>> $INFO
echo &>>$INFO
#COMMENT
fi

echo
M=$(expr $M + 1)
done


#!/bin/bash

echo
HOGE=A_BB_CCC_DDDD_EEEEE
echo "HOGE = $HOGE"

echo
MOGE=${HOGE%_*} 
echo '${HOGE%_*} = '$MOGE

echo
MOGE=${HOGE%%_*} 
echo '${HOGE%%_*} = '$MOGE

echo
MOGE=${HOGE#*_}
echo '${HOGE#*_} = '$MOGE

echo
MOGE=${HOGE##*_}
echo '${HOGE##*_} = '$MOGE

echo
echo
echo

HOGE=A.BB.CCC.DDDD.EEEEE
echo "HOGE = $HOGE"

echo
MOGE=${HOGE%.*} 
echo '${HOGE%.*} = '$MOGE

echo
MOGE=${HOGE%%.*} 
echo '${HOGE%%.*} = '$MOGE

echo
MOGE=${HOGE#*.}
echo '${HOGE#*.} = '$MOGE

echo
MOGE=${HOGE##*.}
echo '${HOGE##*.} = '$MOGE


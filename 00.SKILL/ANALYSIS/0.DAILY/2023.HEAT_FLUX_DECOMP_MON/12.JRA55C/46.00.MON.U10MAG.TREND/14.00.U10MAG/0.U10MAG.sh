#!/bin/bash
DSET=JRA55
ABB=U10V10
ABBOUT=U10MAG
INROOT="/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/46.MON.U10MAG.TREND/12.00.MERGE.U10.V10"
OROOT="NC"

mkdir -vp $OROOT
rm -fv $OROOT/*

LOG=$(basename $0 .sh).LOG
date -R > $LOG
echo   >> $LOG

INLIST=$(ls $INROOT/NC/*)

for IN in $INLIST; do

TMP=$(basename $IN |sed -e s/${ABB}/${ABBOUT}/g)
OUT=$OROOT/$TMP

echo $IN
echo $OUT
echo

echo >> $LOG
echo >> $LOG
echo $IN >> $LOG
echo $OUT >> $LOG
echo >> $LOG

#<< COMMENT
echo "cdo expr,'U10MAG=sqrt(sqr(U10)+sqr(V10))'  $IN $OUT" &>>$LOG
cdo expr,'U10MAG=sqrt(sqr(U10)+sqr(V10))'  $IN $OUT


cdo sinfo $OUT &>> $LOG
echo
cdo showname $OUT &>> $LOG
#COMMENT

done


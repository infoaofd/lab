INDIR=/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/24.22.SELMON.NC/OUT_SELMON_JRA55C.MON.SFC

ODIR=NC
mkdir -vp $ODIR

M=1; ME=12
while [ $M -le $ME ];do
MM=$(printf %02d $M)

MM=$(printf %02d $M)

DSET=JRA55C; PERIOD="1980-2012"
INFLE1=${DSET}.TSK.${PERIOD}_${MM}.nc
INFLE2=${DSET}.SP.${PERIOD}_${MM}.nc
IN1=$INDIR/$INFLE1
IN2=$INDIR/$INFLE2
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi 
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi 

OFLE=${DSET}.TSK.SP.${PERIOD}_${MM}.nc
OUT=$ODIR/$OFLE

rm -vf $OUT
cdo -f nc4 merge $IN1 $IN2 $OUT
if [ $? -eq 0 ];then echo; echo $OUT; cdo showname $OUT ;fi 

M=$(expr $M + 1)
done

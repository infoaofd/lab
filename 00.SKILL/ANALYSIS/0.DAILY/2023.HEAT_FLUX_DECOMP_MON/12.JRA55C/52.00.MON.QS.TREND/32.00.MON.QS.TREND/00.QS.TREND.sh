EXE=$(basename $0 .sh).ncl
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE; exit 1;fi

M=1; ME=12
while [ $M -le $ME ];do
MM=$(printf %02d $M)
runncl.sh ${EXE} $MM
M=$(expr $M + 1)
done

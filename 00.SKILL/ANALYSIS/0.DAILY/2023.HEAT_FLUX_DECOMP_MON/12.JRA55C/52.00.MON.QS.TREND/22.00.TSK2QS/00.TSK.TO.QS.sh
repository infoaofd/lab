#!/bin/bash
DSET=JRA55C
ABB=TSK.SP
ABBOUT=QS
INDIR="/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/52.00.MON.QS.TREND/12.00.MERGE_Q2_SP/NC"
ODIR="NC"

FFILE=$(basename $0 .sh)_FORMULA.TXT
cat <<EOF > $FFILE
ES=6.112*exp((17.67*(TSK-273.15))/(TSK+243.5-273.15));
QS=0.622*ES/(SP/100.0-ES);
EOF
# Bolton (1980) modification of Tetens's (1930) formula. See Emanuel (1994)

mkdir -vp $ODIR

INLIST=$(ls $INDIR/*${ABB}*.nc)

for IN in $INLIST; do

if [ ! -f $IN ];then NO SUCH FILE,$IN;exit 1;fi

TMP=$(basename $IN |sed -e s/${ABB}/${ABBOUT}/g)
OUT=${ODIR}/$TMP

cdo exprf,$FFILE $IN $OUT
echo; echo $OUT
cdo showname $OUT 

done


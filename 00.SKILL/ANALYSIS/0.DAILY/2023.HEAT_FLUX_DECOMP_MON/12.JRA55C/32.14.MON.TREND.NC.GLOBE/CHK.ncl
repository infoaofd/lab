; /work03/am/2022.HEAT_FLUX_TREND/03.01.JRA55.TREND/08.00.MAP.BIPEN.TSK.TREND/04.00.MAP.BIPEN.1980_2020

begin

MM="01"
M=toint(MM)

DSET="ERA5"
ABB="TSK"
FLUXUP=0 ; 1=> (-1)*INPUT_DATA
SIGP=5 ;5 ;SIGNIFICANCE DOUBLE SIDED % 

start=1980
finish=2012
npts=toint(finish)-toint(start)+1

INDIR="/work03/am/2022.SST.TREND.ENERGY.BUDGET/32.12.ERA5.COARSE.MON/32.12.ALL/14.12.SELMON.SMOOTH/OUT_ERA5.MON.SFC"
INFLE=DSET+"."+ABB+"."+toint(start)+"-"+toint(finish)+"_"+MM+".grib"
IN=INDIR+"/"+INFLE

a=addfile(IN,"r")

print(a)

end

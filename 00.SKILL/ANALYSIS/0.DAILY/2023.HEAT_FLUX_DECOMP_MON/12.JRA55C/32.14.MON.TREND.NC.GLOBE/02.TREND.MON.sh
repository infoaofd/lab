#!/bin/sh

NCL=04.TREND.MON.ncl 

#VARLIST="DLR DSR LHF ULR USR SHF TSK"
#VARLIST="ULR USR" 
VARLIST="SHF LHF"

MS=1; ME=12
#MS=6; ME=6

for VAR in $VARLIST; do

M=$MS
while [ $M -le $ME ];do

MM=$(printf %02d $M)

runncl.sh $NCL $VAR $MM

M=$(expr $M + 1)
done #M

done #VAR

exit 0


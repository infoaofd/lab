***********************************************************************
* D:\16.00.TOOL\92.00.TOOLS_OLD_2021-09-09\0.TOOLS\TOOLBOX\OCEANOGRAPHY\FLUX_BULK_HIROSE
* LTNTHF.F
* CODED BY A.MANDA              1997.11
* MODIFIED BY A.MANDA           1997. 
* TASK
*   CALCULATE THE TIME SERIES OF LATENT HEAT FLUX                         
* METHOD
*   BULK METHOD(KONDO,1975)
* SLAVE SUBROUTINE(S)
* REMARK
* REFERENCE(S)
*  HIROSE,N.(1996):HEAT BUDGET IN THE JAPAN SEA.JOURNAL OF OCEANOGRAPHY
*  ,52,553-574.                           
***********************************************************************
      SUBROUTINE LTNTHF(NDAT,TS,TA,EA,PA,W,QE)
*------------- COMMON VARIABLES SHARING OTHER ROUTINES ----------------
* NONE.
*----------------------------------------------------------------------
*--------------------------- PARAMTERS --------------------------------
      INTEGER NDAT
      REAL TS(NDAT),TA(NDAT),EA(NDAT),PA(NDAT),W(NDAT)
      REAL QE(NDAT)

*[INPUT]
* NDAT : DIMENSION OF THE DATA (I)
* TS : SEA SURFACE TEMPERATURE(DEG. CELCIUS) (R)
* TA : AIR TEMPERATURE(DEG. CELCIUS) (R)
* W  : WIND SPEED(M/S) (R)
* EA : VAPOUR PRESSURE(hPa) (R)
* PA : AIR PRESSURE(hPa) (R)

*[OUTPUT]
* QE : LATENT HEAT FLUX(W/M**2) (R)
            
* R :REAL ,I : INTEGER
*----------------------------------------------------------------------
*--------- LIST OF THE VARIABLES USED ONLY IN THIS SUBROUTINE ---------
      REAL QA(NDAT),ES(NDAT),QS(NDAT),RHOA(NDAT),L,CE(NDAT)

* QA : THE SPECIFIC HUMIDITY
* ES : SATURATED VAPOR PRESSURE AT THE SEA SURFACE TEMPERATURE
* QS : SATURATED SPECIFIC HUMIDITY AT THE SEA SURFACE TEMPERATURE 
* RHOA : DENSITY OF AIR
* L : LATENT HEAT OF VAPORIZATION
* CE : DALTON NUMBER

*----------------------------------------------------------------------
      REAL EPS
* EPS : MOLECULAR WEIGHT RATIO OF WATER AND AIR (=0.62197)
      PARAMETER(EPS=0.62197)
* 
*     <<< CALCULATE THE LATENT HEAT FLUX >>>
      DO 100 I=1,NDAT

*        --- SPECIFIC HUMIDITY : QA ---
         QA(I)=EPS*EA(I)/PA(I)

*    
*        --- DENSITY OF AIR : RHOA ---
         RHOA(I)=(1.0 + QA(I))*(1.292 - 0.0044*TA(I))

*        --- SATURATED VAPOR PRESS. AT THE SEA SURFACE TEMP. : ES ---
*        -- GILL(1982)
         POW=(0.7859 + 0.03477*TS(I))/(1.0 + 0.00412*TS(I))
         ES(I)=10.0**POW
*         WRITE(*,*)'ES IS ESTIMATED BY GILL(1982).'
*        -- TETENS(1930)
*         POW=7.5*TS(I)/(237.3+TS(I))
*	   ES(I)=6.11*10**POW
*	   IF(I.EQ.1)THEN
*	     WRITE(*,*)'ES IS ESTIMATED BY TETENS(1930).'
*         END IF
*        --- SATURATED SPECIFIC HUMIDITY AT THE SEA SURFACE TEMP. QS ---
         QS(I)=EPS*ES(I)/PA(I)

*        --- DALTON NUMBER : CE ---
*        ... DETERMINE THE COEFICIENTS,AE,BE,PE,CSE ...
         IF(W(I).LT.0.3)THEN 
           W(I)=0.3
         END IF
*        .. 0.3 < W < 2.2 ..  
         IF(W(I).GE.0.3.AND.W(I).LT.2.2)THEN
            AE=0.0
            BE=1.23
            CSE=0.0
            PE=-0.16
         END IF
*        .. 2.2 < W < 5.0 ..  
         IF(W(I).GE.2.2.AND.W(I).LT.5.0)THEN
            AE=0.969
            BE=0.0521
            CSE=0.0
            PE=1.0
         END IF
*        .. 5.0 < W < 8.0 ..  
         IF(W(I).GE.5.0.AND.W(I).LT.8.0)THEN
            AE=1.18
            BE=0.01
            CSE=0.0
            PE=1.0
         END IF
*        .. 8.0 < W < 25.0 ..  
         IF(W(I).GE.8.0.AND.W(I).LT.25.0)THEN
            AE=1.196
            BE=0.008
            CSE=-0.0004
            PE=1.0
         END IF
*        .. 25.0 < W < 50.0 ..  
         IF(W(I).GE.25.0.AND.W(I).LT.50.0)THEN
            AE=1.68
            BE=-0.016
            CSE=0.0
            PE=1.0
         END IF

*        ... CASE 1 : NEUTRAL (TS = TA) ...  
            CE(I)
     +    = ( AE + BE*W(I)**PE + CSE*(W(I) - 8.0 )**2.0)*1.0E-3   

*        ... CASE 2 : NOT NEUTARAL (TS DOES NOT EQUALS TO TA.)
         IF(TS(I).NE.TA(I))THEN
           S0 = (TS(I) - TA(I))*W(I)**(-2.0)
           S = S0*ABS(S0)/(ABS(S0) + 0.01)

*          .. CASE 2 - 1 : STABLE ..
*          - FOR -3.3 < S < 0 - 
           IF(S0.GT.-3.3.AND.S0.LT.0.0)THEN
             CE(I)=CE(I)*(0.1 + 0.03*S + 0.9*EXP(4.8*S))
           ELSE IF(S0.LE.-3.3)THEN
             CE(I)=0.0
           END IF

*          .. CASE 2 - 2 : UNSTABLE ..
           IF(TS(I).GT.TA(I))THEN
             CE(I)=CE(I)*(1.0 + 0.63*SQRT(S))
           END IF

*          --- LATENT HEAT OF VAPORIZATION ---        
           L = 2.5008*1.0E6 - 2.3*1.0E3*TS(I)

*          --- LATENT HEAT FULX : QE ---        
          QE(I)=RHOA(I)*L*CE(I)*(QS(I) - QA(I))*W(I)
        END IF
100   CONTINUE

      RETURN
      END

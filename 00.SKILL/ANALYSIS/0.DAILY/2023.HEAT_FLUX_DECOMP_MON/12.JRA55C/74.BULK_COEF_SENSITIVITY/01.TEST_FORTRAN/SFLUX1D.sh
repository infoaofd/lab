#!/bin/bash
#
# Sat, 02 Sep 2023 19:25:58 +0900
# /work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/72.BULK_COEF_SENSITIVITY/01.TEST_FORTRAN
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
#nml=$(basename $0 .sh).nml
SUB="LHF1D.F90"

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"
LOPT="-I -L -l"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

echo; echo Created ${nml}.
ls -lh --time-style=long-iso ${nml}; echo


echo ${src}.; ls -lh --time-style=long-iso ${src}; echo

echo Compiling ${src} ...; echo
echo ${f90} ${DOPT} ${OPT} ${SUB} ${src} -o ${exe}; echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}

if [ $? -ne 0 ]; then
echo "EEEEE COMPILE ERROR!"
echo "EEEEE TERMINATED."; echo
exit 1
fi
echo "Done Compile."; echo
ls -lh ${exe}
echo

echo;echo ${exe} is running ...; echo

D1=$(date -R)
${exe}
if [ $? -ne 0 ]; then
echo;echo "EEEEE ERROR in $exe: RUNTIME ERROR!"
echo "EEEEE TERMINATED."; echo

D2=$(date -R)
echo "START: $D1 END:   $D2"
exit 1
fi
echo; echo "Done ${exe}";echo
D2=$(date -R)
echo "START: $D1 END:   $D2"

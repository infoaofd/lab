#!/bin/sh

NCL=00.BULK.COEF.MON.ncl

MS=1; ME=1

M=$MS
while [ $M -le $ME ];do

MM=$(printf %02d $M)

runncl.sh $NCL $MM

M=$(expr $M + 1)
done #M

exit 0


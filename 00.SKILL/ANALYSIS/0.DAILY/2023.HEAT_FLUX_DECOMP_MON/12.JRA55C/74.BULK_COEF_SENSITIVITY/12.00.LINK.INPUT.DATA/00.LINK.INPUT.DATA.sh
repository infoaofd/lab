DSET=JRA55C

#M=$1; M=${M:-1}

M=1; ME=12

while [ $M -le $ME ];do

MM=$(printf %02d $M)

INROOT=/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C

INLIST="\
${INROOT}/42.00.MON.LHF.TREND/NC/${DSET}.LHF.TREND.${MM}.nc \
${INROOT}/46.00.MON.U10MAG.TREND/16.00.MON.U10MAG.TREND/NC/${DSET}.U10MAG.TREND.${MM}.nc \
${INROOT}/48.00.MON.TSK.TREND/NC/${DSET}.TSK.TREND.${MM}.nc \
${INROOT}/50.00.MON.Q2.TREND/NC/${DSET}.Q2.TREND.${MM}.nc \
${INROOT}/52.00.MON.QS.TREND/32.00.MON.QS.TREND/NC/${DSET}.QS.TREND.${MM}.nc \
${INROOT}/72.00.MON.T2.TREND/NC/${DSET}.T2.TREND.${MM}.nc \
"

for IN in $INLIST;do

if [ -f $IN ]; then
echo; echo FOUND $IN
ln -fs $IN .
else
echo;echo "ERROR in $0 : NO SUCH FILE, $IN"; echo; exit 1
fi # IN

done # INLIST

M=$(expr $M + 1)
done # M


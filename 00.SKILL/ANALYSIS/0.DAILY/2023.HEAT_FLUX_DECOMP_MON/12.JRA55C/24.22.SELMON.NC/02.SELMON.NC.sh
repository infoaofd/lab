#/!bin/bash

DSET=JRA55C; DTR=MON; DLEV=SFC

ODIR="OUT_SELMON_${DSET}.${DTR}.${DLEV}"; mkd $ODIR

YS=1980; YE=2012
M=$1; M=${M:-7}; MM=$(printf %02d $M)

VAR=$2; VAR=${VAR:-TSK}


# INDIR=/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/22.12.REGRID.NCL/OUT_NC_SMOOTH_JRA55C
INDIR=/work03/am/2023.HEAT_FLUX_DECOMP_MON/12.JRA55C/22.12.REGRID.NCL/OUT_NC_SMOOTH_JRA55C
if [ ! -d ${INDIR} ];then 
echo $VAR; echo ERROR: NO SUCH DIR, $INDIR; echo; exit 1
fi

INFLE=$INDIR/${DSET}.MON.SFC.${VAR}.nc

OFLE=${ODIR}/${DSET}.${VAR}.${YS}-${YE}_${MM}.nc
echo; rm -vf $OFLE; echo

cdo selmon,$MM -selyear,${YS}/${YE} $INFLE $OFLE

echo; rm -vf $TMPFLE; echo

echo; echo; echo;
cdo sinfo $OFLE
cdo showname $OFLE
echo; echo "MMMMM"; echo OUTPUT: $OFLE; echo
ls -lh $OFLE 
echo "MMMMM";echo



# TREND_THEIL_SHEN_MANN-KENDALL

## SST_TREND_THEIL-SEN.ncl

```bash
(ncl_stable) 
$ ncl SST_TREND_THEIL-SEN.ncl 
it=0 yyyy=1982 INFLE: EAV_SST_1982.nc
it=35 yyyy=2017 INFLE: EAV_SST_2017.nc

Variable: trend
Type: double
Total Size: 518400 bytes
            64800 values
Number of Dimensions: 2
Dimensions and sizes:   [180] x [360]
Coordinates: 
Number Of Attributes: 5
  _FillValue :  -999000000
  nptxy :       <ARRAY of 64800 elements>
  rstd :        <ARRAY of 64800 elements>
  yintercept :  <ARRAY of 64800 elements>
  tval :        <ARRAY of 64800 elements>
mmmmmm Mann-Kendall trend significance and the Theil-Sen 
mmmmmm estimate of linear trend
 STARTED AT  Thu, 26 Oct 2023 13:11:16 +0900
 FINISHED AT Thu, 26 Oct 2023 13:11:20 +0900

Variable: trend
Type: double
Total Size: 518400 bytes
            64800 values
Number of Dimensions: 2
Dimensions and sizes:   [180] x [360]
Coordinates: 
Number Of Attributes: 7
  units :       K/36yr
  long_name :   SLOPE OF LINEAR TREND
  _FillValue :  -999000000
  nptxy :       <ARRAY of 64800 elements>
  rstd :        <ARRAY of 64800 elements>
  yintercept :  <ARRAY of 64800 elements>
  tval :        <ARRAY of 64800 elements>

Variable: p
Type: double
Total Size: 518400 bytes
            64800 values
Number of Dimensions: 2
Dimensions and sizes:   [lat | 180] x [lon | 360]
Coordinates: 
            lat: [-89.5..89.5]
            lon: [ 0.5..359.5]
Number Of Attributes: 3
  units :       percent
  long_name :   p-value
  _FillValue :  -999000000

FIG
-rw-rw-r-- 1 836K Oct 26 13:11 SST_TREND_JUL_EAV_THEIL-SEN1982-2017.pdf
```

SST_TREND_JUL_EAV_THEIL-SEN1982-2017.pdf

SST_TREND_THEIL-SEN.ncl

```

; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Examples/SST_trend

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

ys=1982
ye=2017
MM=07
MMM="JUL"

FIG="SST_TREND_"+MMM+"_EAV_THEIL-SEN"+ys+"-"+ye
TYPE="pdf"

indir="/work04/manda/2022.K17/00.02.K17.PAPER_FIG/16.00.EAV_MON_AVE_SST/OUT_EAV/"
f=addfile(indir+"/EAV_SST_1982.nc","r")
sst1=f->eavsst
lon=f->lon
lat=f->lat

dim=dimsizes(sst1)
nlat=dim(0)
nlon=dim(1)
;printVarSummary(sst1)
nt=ye-ys+1
sst=new((/nt,nlat,nlon/),typeof(sst1))

yyyy=ispan(ys,ye,1)

do it=0,nt-1

infle="EAV_SST_"+yyyy(it)+".nc"
if (it .eq. 0 .or. it .eq. nt-1) then
print("it="+it+" yyyy="+yyyy(it)+" INFLE: "+infle)
end if
f=addfile(indir+infle,"r")
;print("OPEN "+infle)
sst1=f->eavsst

sst(it,:,:)=sst1(:,:)

end do
;printVarSummary(sst)

trend   = regCoef_n(yyyy,sst,0,0)     ; 回帰係数の計算

printVarSummary(trend)

print("mmmmmm Mann-Kendall trend significance and the Theil-Sen ")
print("mmmmmm estimate of linear trend")

datetime1=systemfunc("date -R")
print(" STARTED AT  " + datetime1)

manken=trend_manken(sst,True,0)

datetime2=systemfunc("date -R")
print(" FINISHED AT " + datetime2)

;trend(:,:) = smth9_Wrap(p(1,:,:), 0.50, 0.25, False)
p=(1.0-manken(0,:,:))*100.
trend=manken(1,:,:)

trend       = trend*nt      ; C/yr -> C/(nt yr)

copy_VarAtts(sst,trend)
trend@long_name = "SLOPE OF LINEAR TREND"
trend@units     = "K/"+nt+"yr"
printVarSummary(trend)

copy_VarAtts(sst,p)
p@long_name = "p-value"
p@units     = "percent"
p!0     = "lat" 
p!1     = "lon"
p&lat   = lat
p&lon   = lon

printVarSummary(p)


 ;;; 格子情報等の付加
  trend!0     = "lat" 
  trend!1     = "lon"
  trend&lat   = lat
  trend&lon   = lon
  trend@unit  = "~S~O~N~C/"+nt+"yr"
;  copy_VarCoords(trend,cdl)

wks  = gsn_open_wks(TYPE,FIG)
plot  = new(2,graphic)     

 res                 = True         ; トレンドのためのres
  res@gsnDraw         = False        ; plotを描かない
  res@gsnFrame        = False        ; WorkStationを更新しない

  res@cnLinesOn       = False
  res@cnFillOn        = True
  res@cnInfoLabelOn   = False
  res@tiMainString    = MMM+" SST trend ("+ys+"-"+ye+") EAV (THEIL-SEN)"

;
res@cnFillPalette = (/\
"dodgerblue","lightskyblue","azure1","white","antiquewhite","khaki1","yellow","gold","orange","red","firebrick"/)
res@cnLevelSelectionMode = "ExplicitLevels"
res@cnLevels = (/-0.4,-0.2,0,0.2,0.4,0.6,0.8,1.0,1.2,2./)

;  res@cnLevelSelectionMode = "ManualLevels"
;  res@cnMinLevelValF  = -1
;  res@cnMaxLevelValF  =  1
;  res@cnLevelSpacingF =  0.2
;  res@cnFillPalette   = "BlWhRe"


  res@mpMaxLatF       =  85
  res@mpMinLatF       = -85
  res@mpCenterLonF    = 210
  res@mpFillDrawOrder       = "PostDraw"     ; draw map fill last
  res@mpLandFillColor = 1 ;164

  res@gsnRightString  = ""
  res@pmLabelBarOrthogonalPosF = 0.2        ; カラーバーの位置を微調整
  ;;; タイトルや軸の文字の大きさを設定
  res@tmYLLabelFontHeightF = 0.016
  res@tmXBLabelFontHeightF = 0.016
  res@tiMainFontHeightF    = 0.024
  res@lbLabelFontHeightF   = 0.016
  ;;; カラーバーにタイトルをつける
  res@lbTitleOn            = True
  res@lbTitleString        = trend@unit
  res@lbTitlePosition      = "Right"
  res@lbTitleDirection     = "Across" 
  res@lbTitleFontHeightF   = 0.016
  
  plot(0) = gsn_csm_contour_map(wks,trend,res)   ; trendを描いたものを一旦plotに収める

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF  = 0
res@cnMaxLevelValF  = 5
res@cnLevelSpacingF = 2.5
plot(1) = gsn_csm_contour_map(wks,p,res)

  resP    = True
 ;resP@gsnPanelMainString  = title              ; uncomment to add title
  resP@gsnMaximize         = True               ; make large
 ;resP@lbLabelStride       = 2                  ; force every other label 
 ;resP@lbLabelFontHeightF  = 0.0125             ; make labels smaller [0.2 default]

  gsn_panel(wks,plot,(/2,1/),resP)

  res2                 = True          ; 有意性のためのres
  res2@gsnDraw         = False         ; plotを描かない
  res2@gsnFrame        = False         ; WorkStationを更新しない
  ;;; あとでShadeLtGtContourを用いるため，あらゆるものをFalseにしておく
  res2@cnLinesOn       = False
  res2@cnLineLabelsOn  = False
  res2@cnFillOn        = False
  res2@cnInfoLabelOn   = False
  ;;; ShadeLtGtContourのために，等値線は指定しておく
  res2@cnLevelSelectionMode = "ExplicitLevels"
  res2@cnLevels        = (/0.024,0.025,0.975,0.976/)

;  dum  = gsn_csm_contour(wks,cdl,res2)           ; とりあえずcdlを描く
;  dum  = ShadeLtGtContour(dum,0.025,17,0.975,17)

;  dum  = ShadeLtGtContour(dum,0.025,6,0.975,17)
;      ;;; 0.025のコンターより下を6番のハッチ，0.975のコンターより上を点々(17番)
;      ;;; これは有意水準5％の両側検定に対応

;  overlay(plot,dum)  ; 有意性を示したdumをplotに重ねる
;   draw(plot)

frame(wks)

print("")
print("FIG")
system("ls -lh "+FIG+"."+TYPE)
print("")

end
```



## TREND_AAV_EAV_MON_SST_LR.ncl

```
$ ncl TREND_AAV_EAV_MON_SST_LR.ncl 
mmmmm READ DATA
it=0 yyyy=1982 INFLE: EAV_SST_1982.nc
it=35 yyyy=2017 INFLE: EAV_SST_2017.nc

Variable: sst
Type: double
Total Size: 18662400 bytes
            2332800 values
Number of Dimensions: 3
Dimensions and sizes:   [36] x [lat | 180] x [lon | 360]
Coordinates: 
            lat: [-89.5..89.5]
            lon: [ 0.5..359.5]
Number Of Attributes: 1
  _FillValue :  -999000000
mmmmm AAV
mmmmmm LR
tval= 2.44 pval= 0.02
mmmmmm M-K
probability=0.9822242017076082 trend=0.01760000469724357
```

SST_TREND_JUL_AAV_EAV_LR1982-2017.pdf



TREND_AAV_EAV_MON_SST_LR.ncl

```

; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Examples/SST_trend

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

ys=1982
ye=2017
MM=07
MMM="JUL"

LONW=120
LONE=140
LATS=22
LATN=42

FIG="SST_TREND_"+MMM+"_AAV_EAV_LR"+ys+"-"+ye
TYPE="pdf"

indir="/work04/manda/2022.K17/00.02.K17.PAPER_FIG/16.00.EAV_MON_AVE_SST/OUT_EAV/"
f=addfile(indir+"/EAV_SST_1982.nc","r")
sst1=f->eavsst
lon=f->lon
lat=f->lat

dim=dimsizes(sst1)
nlat=dim(0)
nlon=dim(1)
;printVarSummary(sst1)
nt=ye-ys+1
sst=new((/nt,nlat,nlon/),typeof(sst1))

yyyy=ispan(ys,ye,1)
iy=ispan(1,nt,1)



print("mmmmm READ DATA")

do it=0,nt-1

infle="EAV_SST_"+yyyy(it)+".nc"
if (it .eq. 0 .or. it .eq. nt-1) then
print("it="+it+" yyyy="+yyyy(it)+" INFLE: "+infle)
end if
f=addfile(indir+infle,"r")
;print("OPEN "+infle)
sst1=f->eavsst

sst(it,:,:)=sst1(:,:)

end do
printVarSummary(sst)

sst!0="year"
sst&year=yyyy


print("mmmmm AAV")
sstsub=sst({year|:},{lat |22:42},{lon |120:140})
xavg = wgt_areaave_Wrap(sstsub, 1.0, 1.0, 1)
xavg!0="year"
x=sst

 year  = yyyy
  nyrs  = dimsizes(year)

  xavg&year  = year

;************************************************
; Perform linear regression on annual means
;************************************************

  rc = regline_stats(year, xavg)                ; degC/year
  rc@long_name = "trend"
  rc@units     = "degC/year"

  nx   = dimsizes(xavg)
  pltarry      = new ( (/4,nx/), typeof(x))
  pltarry(0,:) = xavg                              ; use markers
  pltarry(1,:) = rc@Yest                           ; regression values
  pltarry(2,:) = rc@YMR025                         ; MR: mean response
  pltarry(3,:) = rc@YMR975
;  pltarry(4,:) = rc@YPI025                         ; PI: prediction interval
;  pltarry(5,:) = rc@YPI975

;************************************************
; create plot: use overlay approach
;************************************************
    wks  = gsn_open_wks(TYPE,FIG)             ; send graphics to PNG file
    
    res                     = True                   ; plot mods desired
  res@gsnDraw         = False         ; plotを描かない
  res@gsnFrame        = False         ; WorkStationを更新しない
    res@xyMarkLineModes     = (/"Lines  ","Lines" \  ; choose which have markers
                               ,"Lines"  ,"Lines" \ 
                               ,"Lines"  ,"Lines" /)
    res@xyMarkers           = 16                     ; choose type of marker 
    res@xyMarkerSizeF       = 0.0075                 ; Marker size (default 0.01)

    res@xyDashPatterns      = 0                      ; solid line 
   ;res@xyMonoDashPattern   = True
    res@xyLineThicknesses   = (/1,3,2,2/)        
    res@xyLineColors        = (/ "black", "black"\
                               , "blue" , "blue" \
                               /)
    res@tmYLFormat          = "f"                    ; not necessary but nicer labels 

  ;;res@trXMinF                =  min(year)
    res@trXMaxF                =  max(year)

    res@tiMainString           = "JUL SST "+LATS+"~F34~0~F~N-"+LATN+"~F34~0~F~N "+LONW+"~F34~0~F~E-"+LONE+"~F34~0~F~E"
;    plot                       = gsn_csm_xy (wks,year,pltarry(0:1,:),res) 

;---Make legend smaller and move into plot
    res@pmLegendDisplayMode    = "Always"            ; turn on legend
    res@pmLegendSide           = "Top"               ; Change location of 
    res@pmLegendParallelPosF   = .8                ; move units right
    res@pmLegendOrthogonalPosF = -0.29               ; move units down
    res@pmLegendWidthF         = 0.12                ; Change width and
    res@pmLegendHeightF        = 0.1               ; height of legend.
    res@lgPerimOn              = False               ; turn off/on box around
    res@lgLabelFontHeightF     = .015                ; label font height
    res@xyExplicitLegendLabels = (/"data"         , "regression"       \
                                  ,"5% response"  , "95% response"  \   
                                  /)  

    res@tiYAxisString        = "SST [~F34~0~F~C]"
    plot                       = gsn_csm_xy (wks,year,pltarry,res)           ; create plot


print("mmmmmm LR")
rc2 = regline(year, xavg)
tval=sprintf("%5.2f", rc@tval(1))
pval=sprintf("%5.2f", rc@pval(1))

print("tval="+tval+" pval="+pval)
trend=sprintf("%5.2f", rc2*(year(nyrs-1)-year(0))) +"~F34~0~F~C ("+tostring(nyrs)+"yrs)"

txres               = True                     ; text mods desired
txres@txFontHeightF = 0.02                     ; font smaller. default big
txres@txJust = "CenterLeft"
dum = gsn_add_text(wks,plot,"trend="+trend,1981.,27.1,txres) 
dum = gsn_add_text(wks,plot,"t="+tval,     1981.,26.98,txres) 
dum = gsn_add_text(wks,plot,"p="+pval,     1981.,26.86,txres) 

draw(plot)
frame(wks)

print("mmmmmm M-K")
opt = False
pt = trend_manken(xavg, opt, 0)
print("probability="+pt(0)+" trend="+pt(1))

end
```


load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"

begin

INDIR="/work04/manda/2022.K17/00.02.K17.PAPER_FIG/16.00.EAV_MON_AVE_SST/OUT_EAV/"

YYYY=1982
INFLE="EAV_SST_"+YYYY+".nc"

f=addfile(INDIR+INFLE,"r")
print(f)
lat=f->lat
lon=f->lon


files = systemfunc("ls "+INDIR+"/"+"EAV_SST*.nc")

fall=addfiles(files,"r")

sst=fall[:]->eavsst
;time=fall[:]->initial_time0_hours

printVarSummary (lat)
printVarSummary (lon)
printVarSummary (sst)



end


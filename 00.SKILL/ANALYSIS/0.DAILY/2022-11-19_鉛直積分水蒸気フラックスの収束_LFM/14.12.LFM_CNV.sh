#!/bin/bash

#MMMMMMMMMMMMMM INCLUDE ###################
INC=./06.LFM.MAKE.CTL.sh
. $INC
#MMMMMMMMMMMMMM INCLUDE ###################

FH=$1;
RECL1=$2;    LON1_1=$3;     LAT1_1=$4;    LON1_2=$5;    LAT1_2=$6;
PBOT=$7; PTOP=$8

YMDH=${YMDH:-2022061900}
YYYY=${YMDH:0:4}; MM=${YMDH:4:2}; DD=${YMDH:6:2}; HH=${YMDH:8:2}

echo MMMMMMM DATE ${YYYY} ${MM} ${DD} ${HH}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

FH=${FH:-00}

MODEL=LFM

LONW=122.0 ; LONE=134; LATS=24.5; LATN=35 #MAP AREA

VAR1='WVF(ALONG)'
# VAR1='mag(wvx.1,wvy.1)';#MAG
CMIN1=0.05; CMAX1=0.4; CINT1=0.05; CLEV1=0.1

VAR2=VPT.1; V2LEV=950; 
#CINT2M=1;CINT2C=1;CLEV2C="300 301 302 303 304 305 306 308 310 312 314 316 318"; CLEV2M=304
CINT2M=1;CINT2C=2;CLEV2C="300 304 308 312 316 320 324 328 332"; CLEV2M=304

SCLV=0.5; VUNIT="m/s" ;# WVF

VAR3=MAUL.1; V3LEV=850; 
RGB='set rgb 98 128 128 128 -100'
VAR4=RH; CLEV4="95"
XTITLE_CSC="Latitude North" ;#"Longitude East"
YTITLE_CSC="Pressure [hPa]" ;#"Longitude East"

VECUNIT="kg/m\`a2\`n/s"

LEVCSC1=1000; LEVCSC2=500; CLEVCSC=1

LEVCSC3=1000; LEVCSC4=900; CLEVCSC=1


echo MMMMMMM BOX
RECL1=${RECL1:-BOX1}
LON1_1=${LON1_1:-124}; LAT1_1=${LAT1_1:-26.7}
LON1_2=${LON1_2:-130}; LAT2B=${LAT1_2:-31.5}
PBTM=1000
PTOP=300
echo $LON1_1 $LAT1_1 $LON1_2 $LAT1_2
echo $PBTM $PTOP
echo

TIME="12Z18JUN2022 12Z19JUN2022"
YYYY=2022; MMM=JUN; DD=19; HH=00; DATE=${HH}UTC${DD}${MMM}${YYYY}

VECUNIT="kg/m\`a2\`n/s"

FIGDIR=FH${FH}; mkd $FIGDIR
FIG=${FIGDIR}/${MODEL}_FH${FH}_WVF_CNV_BOX.${RECL1}.pdf


HOST=$(hostname); CWD=$(pwd); TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo CREATE CTL FILE

CTL1=LFM.CTL; echo $CTL1

INDIR1=/work01/DATA/LFM_PROC_ROT_v2/FH${FH}/
INFLE_CHK=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_THERMO.nc
INFLE1=LFM_PRS_FH${FH}_VALID_%y4-%m2-%d2_%h2_THERMO.nc
DSET1=$INDIR1/${INFLE1}
DSET_CHK=$INDIR1/${INFLE_CHK}
if [ ! -f $DSET_CHK ]; then echo NO SUCH FILE, $DSET_CHK; echo; exit 1; fi

LFM_MAKE_CTL $CTL1 $DSET1
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM


GS=$(basename $0 .sh).GS

cat <<EOF>$GS

'open $CTL1' ;#'q ctlinfo'; say result

'cc'; 'set grid off'

say 'MMMMM SET FIGURE SIZE'
'set vpage 0.0 8.5 0.0 11.0'

xmax=2; ymax=2; xleft=0.2; ytop=9

xwid =  6/xmax; ywid =  5/ymax
xmargin=0.4; xmargin2=0.6; ymargin=1



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 1 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${HH}Z${DD}${MMM}${YYYY}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $V1LEV'
'q dims'; say sublin(result,4)

# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'

'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

say 'CONTOUR $VAR2 $V2LEV'
'set lev $V2LEV'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT2M'
'q dims'; say sublin(result,4)
'd $VAR2'

'set xlab off';'set ylab off'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint $CINT2M'
'd $VAR2'

'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV2M'
'd $VAR2'


say 'MMMMM WVF $V2LEV'
'set gxout vector'; 'set cthick 1'
'set arrscl 0.5 $SCLV'; 'set arrlab off'
'set lev $V2LEV'
'q dims'; say sublin(result,4)

'set ccolor 0'; 'set cthick 6'
'vec skip(wvx,25,25);wvy -SCL 0.5 $SCLV -P 20 20'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.8; yy=yb-0.3

'set ccolor 1'; 'set cthick 3'
'vec skip(wvx,25,25);wvy -SCL 0.5 $SCLV -P 'xx' 'yy' -SL ${VUNIT}'


say 'MMMMM BOX'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_1 -c 2 -l 1 -t 4'
'trackplot $LON1_2 $LAT1_1 $LON1_2 $LAT1_2 -c 2 -l 1 -t 4'
'trackplot $LON1_2 $LAT1_2 $LON1_1 $LAT1_2 -c 2 -l 1 -t 4'
'trackplot $LON1_1 $LAT1_2 $LON1_1 $LAT1_1 -c 2 -l 1 -t 4'

say 'MMMMM VECTOR $V3LEV'
'set gxout vector'
'set arrscl 0.5 $SCLV'; 'set arrlab off'
'set ccolor 0'; 'set cthick 6'
'set lev $V3LEV'
'q dims'; say sublin(result,4)

'set ccolor 14'; 'set cthick 2'
'vec skip(wvx,25,25);wvy -SCL 0.5 $SCLV -P 20 20'


say 'MMMMM CONTOUR $VAR2 $CLEV2M'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV2M'
'set lev $V2LEV'
'd $VAR2'

say 'MMMMM MAUL'
'set lev $V3LEV'
'q dims'; say sublin(result,4)
'set gxout grfill'
'$RGB' ; 'set ccolor 98';'set cthick 10'
'd ${VAR3}'

'set strsiz 0.1 0.12'; 'set string 1 c 3'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' ${DATE} (FH${FH})'
'set string 1 l 3'
xx=xl-0.3; yy=yy+0.18; 'draw string 'xx' 'yy' ${VAR2}(${V2LEV}) ${VAR3}(${V3LEV})'
XSLEFT=xl; YTTOP=yt



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 2 TIME SERIES MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

'set lon $LON1_1 $LON1_2'; 'set lat $LAT1_1 $LAT1_2'
'q dims';say sublin(result,2); ;say sublin(result,3)
'set time $TIME'
'q dims';say sublin(result,5)

say 'MMMMM VINT ${PBOT}-${PTOP}'
'set z 1 12'
'q dims';say sublin(result,4)

'set z 1';'IVX=0';'IVY=0'
'IVX=vint(lev(z=1), wvx, ${PTOP})'
'IVY=vint(lev(z=1), wvy, ${PTOP})'

say 'MMMMM DIV'
'DIV=hdivg(IVX,IVY)'

say 'MMMMM AAVE'
'set lon $LON1_1'; 'set lat $LAT1_1'
'CNV1A=-tloop(aave(DIV,lon=${LON1_1},lon=${LON1_2},lat=${LAT1_1},lat=${LAT1_2}))'
say



say 'MMMMM VINT ${PBOT}-900'
'set lon $LON1_1 $LON1_2'; 'set lat $LAT1_1 $LAT1_2'
'q dims';say sublin(result,2); ;say sublin(result,3)
'set time $TIME'
'q dims';say sublin(result,5)
'set z 1 5'
'q dims';say sublin(result,4)

'set z 1';'IVX=0';'IVY=0'
'IVX=vint(lev(z=1), wvx, 900)'
'IVY=vint(lev(z=1), wvy, 900)'

say 'MMMMM DIV'
'DIV=hdivg(IVX,IVY)'

say 'MMMMM AAVE'
'set lon $LON1_1'; 'set lat $LAT1_1'
'CNV2A=-tloop(aave(DIV,lon=${LON1_1},lon=${LON1_2},lat=${LAT1_1},lat=${LAT1_2}))'
say



say 'MMMMM VINT 900-${PTOP}'
'set lon $LON1_1 $LON1_2'; 'set lat $LAT1_1 $LAT1_2'
'q dims';say sublin(result,2); ;say sublin(result,3)
'set time $TIME'
'q dims';say sublin(result,5)
'set z 6 12'
'q dims';say sublin(result,4)

'set z 1';'IVX=0';'IVY=0'
'IVX=vint(lev(z=6), wvx, 300)'
'IVY=vint(lev(z=6), wvy, 300)'

say 'MMMMM DIV'
'DIV=hdivg(IVX,IVY)'

say 'MMMMM AAVE'
'set lon $LON1_1'; 'set lat $LAT1_1'
'CNV3A=-tloop(aave(DIV,lon=${LON1_1},lon=${LON1_2},lat=${LAT1_1},lat=${LAT1_2}))'
say



say 'MMMMMMMMMM PLOT DIV MMMMMMMMMMM'
ymap=1; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grads off'

'set xlab on'; 'set ylab on'
'set vrange -2.5 20';'set ylint 5'
'set cmark 0';'set ccolor 1';'set cthick 2'
'd CNV1A*3600' ;# kg/s->mm/h

'set xlab off'; 'set ylab off'
'set cmark 0';'set ccolor 4';'set cthick 2'
'd CNV2A*3600' ;# kg/s->mm/h

'set cmark 0';'set ccolor 3';'set cthick 2'
'd CNV3A*3600' ;# kg/s->mm/h

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' WVF CNV'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' ${RECL1} ${LON1_1}-${LON1_2},${LAT1_1}-${LAT1_2}'


'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.4; yy=(yt+yb)/2; 'draw string 'xx' 'yy' mm/h'

say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM HEADER MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
xx = XSLEFT - 0.2; yy=YTTOP-0.1
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
yy = yy+0.8; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${CMD} $@'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${TIMESTAMP}'

'gxprint $FIG'

'quit'
EOF


grads -bcp $GS
rm -vf $GS
if [ -f $FIG ];then echo; echo OUTPUT $FIG; echo; fi

echo MMMMM BACK-UP SCRIPTS
cp -av $0 $FIGDIR/00.$(basename $0 .sh)_BAK_$(date +"%Y-%m-%d_%H%M").sh
cp -av $INC $FIGDIR/00.$(basename $INC .sh)_BAK_$(date +"%Y-%m-%d_%H%M").sh
echo MMMMM

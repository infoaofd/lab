# commands02.txt
#
# Usage:
#  gnuplot < PLOT02.gnu
#
set term png
set output "PLOT02.png"
set xlabel "<---X--->"
set ylabel "<---Y--->"
set title "Data versus Piecewise Linear Interpolant"
set grid
set style data lines
plot "data02.txt" using 1:2 with points pt 7 ps 2 lc rgb "blue",\
     "INTPL02.TXT" using 1:2 lw 3 linecolor rgb "red"

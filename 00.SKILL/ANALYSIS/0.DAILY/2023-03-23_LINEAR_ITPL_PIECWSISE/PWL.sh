#!/bin/bash
#
# Wed, 22 Mar 2023 20:55:07 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/14.SREH.LFM/12.14.TEST_pwl_interp_1dD/12.14.TEST_pwl_interp_1dD_ONLY
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml
SUB="PWL_VALUE_1D.F90"

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "
IN=data02.txt
OUT=INTPL02.TXT
ND=18
NI=32
XI0=0
XIM=32

cat<<EOF>$nml
&para
ND=${ND}
NI=${NI}
XI0=${XI0}
XIM=${XIM}
IN="${IN}"
OUT="${OUT}"
&end
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} ${SUB} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
if [ -f ${OUT} ]; then echo; echo OUTPUT: ${OUT}; echo; fi

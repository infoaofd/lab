#!/bin/bash

GS=$(basename $0 .sh).GS

#YYYY=2009;MMM=AUG;MM=08;DD=28; HH=00
#YYYY=2009;MMM=AUG;MM=08;DD=31; HH=00
YYYY=2009;MMM=SEP;MM=09; DD=01; HH=00

DATE=${HH}Z${DD}${MMM}${YYYY}; YMD=${YYYY}${MM}${DD}

INDIR=/work02/DATA/ERA5.NWPAC/NC/SFC/$YYYY
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=ERA5_NWPAC_SFC_06HR_${YMD}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

FIG=$(basename $0 .sh)_${YMD}_${HH}.pdf

cat <<END >$GS
'sdfopen $IN'
'set time $DATE'

'cc';'set grid off'

'set time $DATE'
'set lon 120 180'; 'set lat   20  50'
'set rgb 99 80 80 80'
'set map 99 1 6'

say MMM GRADIENT
'SST=var34-273.5'

'pi=3.14159265359'
'dtr=pi/180'
'a=6.371e6'

'var=smth9(  SST )'

'dx = a * cos( dtr * lat )* dtr *cdiff( lon , x )'
'dy = a * dtr *cdiff( lat ,  y )'
'gradx=cdiff( var , x )/ dx'
'grady=cdiff( var , y )/ dy'

'grd=mag( gradx , grady )*100000'

'set gxout shaded'
'color 1 3 0.5 -kind white->wheat->gold->orange->red'
'set clab off'
'd grd'
'cbarn'

'set xlab off';'set ylab off'


'set cint 2'
'set gxout contour'
'set ccolor 1'
'set cthick 1';'set clab off'
'set cstyle 1'
'd SST' ;#
'draw title ERA5 $DATE'


'SLP=var151/100'
'set cint 2';'set clab off'
'set ccolor 0';'set cthick 6'
'd SLP'

'set cint 2';'set clab forced';'set clskip 4'
'set ccolor 3';'set cthick 3'
'd SLP'


'gxprint $FIG'

'quit'
END


grads -bcp $GS
#rm -vf $GS

if [ -f $FIG ];then
echo FIG: $FIG
fi


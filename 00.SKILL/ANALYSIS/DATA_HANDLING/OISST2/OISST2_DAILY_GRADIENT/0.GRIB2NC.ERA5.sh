INDIR=/work02/DATA/ERA5.NWPAC/SFC/2009
 ODIR=/work02/DATA/ERA5.NWPAC/NC/SFC/2009

mkd $ODIR

INLIST=$(ls $INDIR/*.grib)

for IN in $INLIST; do

OUT=$ODIR/$(basename $IN .grib).nc

echo IN:  $IN
echo cdo -f nc4 copy $IN $OUT
cdo -f nc4 copy $IN $OUT
echo OUT: $OUT
echo

done




#!/bin/bash

GS=$(basename $0 .sh).GS

YYYY=2009;MMM=AUG;DD=28
#YYYY=2009;MMM=SEP;DD=01
DATE=${DD}${MMM}${YYYY}

INDIR=/work01/DATA/SST/OISST2/A
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=sst.day.anom.${YYYY}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

INDIR2=/work01/DATA/SST/OISST2/R
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi
INFLE2=sst.day.mean.${YYYY}.nc
IN2=$INDIR2/$INFLE2
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

FIG=OISST_${DATE}.pdf

cat <<END >$GS
'sdfopen $IN'
'sdfopen $IN2'
'q ctlinfo 2'; say result

'cc'
'set gxout shade2'

#'q ctlinfo';say result
'rgbset2'

'set dfile 1'
'set time $DATE'
'set lon 120 180'; 'set lat   0  50'
'd anom.1'
'draw title OISST2 ANOM $DATE'

'cbarn'

'set dfile 2'
'set time $DATE'
'set lon 120 180'; 'set lat   0  50'
'set gxout contour'
'set ccolor 0';'set cthick 5'
'set clab off'
'set cint 1'
'd sst.2'

'set ccolor 1';'set cthick 1'
'set clab on'
'set cint 1';'set clskip 5'
'd sst.2'

'gxprint $FIG'

'quit'
END


grads -bcp $GS
#rm -vf $GS

if [ -f $FIG ];then
echo FIG: $FIG
fi


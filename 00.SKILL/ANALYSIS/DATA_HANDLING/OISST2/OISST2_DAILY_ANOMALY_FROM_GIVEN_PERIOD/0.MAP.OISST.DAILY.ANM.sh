#!/bin/bash

GS=$(basename $0 .sh).GS

YYYY=2009;MMM=AUG;MM=08;DD=28
#YYYY=2009;MMM=SEP;MM=09; DD=01
TAVP1=27AUG2009; TAVP2=03SEP2009

DATE=${DD}${MMM}${YYYY}; YMD=${YYYY}${MM}${DD}

INDIR=/work01/DATA/SST/OISST2/R
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=sst.day.mean.${YYYY}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

FIG=$(basename $0 .sh)_${YMD}.pdf

cat <<END >$GS
'sdfopen $IN'
'set time $DATE'
'tav=ave(sst,time=${TAVP1},time=${TAVP2})'
'anm=sst(time=${DATE})-tav'

'cc';'set grid off'
'set gxout shade2'


#'q ctlinfo';say result
'rgbset2'

'set dfile 1'
'set time $DATE'
'set lon 120 180'; 'set lat   20  50'
#'set cmin -3';'set cmax 3';'set cint 1'
'set clevs -2.5 -2 -1.5 -1 -0.5 0.5 1 1.5 2 2.5'
'd anm'
'draw title OISST2 $DATE'
'cbarn'

'set xlab off';'set ylab off'

'set time $DATE'
'set lon 120 180'; 'set lat   20  50'
'set gxout contour'
'set ccolor 0';'set cthick 5'
'set clab off'
'set cint 1'
'd sst'

'set ccolor 1';'set cthick 1'
'set clab on'
'set cint 1';'set clskip 5'
'd sst'

'gxprint $FIG'

'quit'
END


grads -bcp $GS
#rm -vf $GS

if [ -f $FIG ];then
echo FIG: $FIG
fi


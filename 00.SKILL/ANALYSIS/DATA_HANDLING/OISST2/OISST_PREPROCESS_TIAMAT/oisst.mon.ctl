dset    ^monthly/oisst.mon.%y4%m2.nc
dtype netcdf
title   daily-oi-v2
undef 32767
unpack scale_factor add_offset
options template
xdef  1440 linear   0.125  0.25
ydef   720 linear -89.875  0.25
zdef     1 levels   0.0
tdef  380 linear 00z01jan1982 1mo
*tdef  380 linear 00z01oct1981 1mo
vars  1
sst=>sst 0 t,y,x wind speed
endvars

dset ^CLM2-Filt/avhrr-only-v2.%y4%m2%d2.nc
title daily-oi-v2
dtype netcdf
*undef -999
undef 32767
unpack scale_factor add_offset
options template
xdef  1440 linear 0.125 0.25
ydef  720 linear -89.875 0.25
zdef    1 linear 1 1
tdef  365 linear 01jan0001 1dy
vars  1
sst=>sst 1 t,y,x wind speed
endvars

*/work05/manda/WRF.PRE/OISST/NetCDF/EOF
dset  ^amp.dat
options big_endian
undef  -9.99e33
xdef   1 linear 0.0 1.0
ydef   1 linear 0.0 1.0
zdef   1 linear 1.0 1.0
tdef    30 linear 00z1jan1982 1yr
vars     5
eof1  0 99 EOF1
eof2  0 99 EOF2
eof3  0 99 EOF3
eof4  0 99 EOF4
eof5  0 99 EOF5
endvars

#!/bin/csh -f
# /work05/manda/WRF.PRE/OISST/NetCDF/EOF
cat > a.f90 << EOF
      module oisst
! ======================================================================
!
! ======================================================================
      integer :: year0, s_year, e_year
      integer :: nlon, nlat, nlev, klev
      parameter (year0=1982, s_year=1982,e_year=2011)
      parameter (nmon=1)
      parameter (nyrs=e_year-s_year+1)
      parameter (ntim=nyrs*nmon)
      parameter (nlon=1440, nlat=720, nlev=1, klev=1)
      character(len=72) :: varname = 'sst'
      character(len=72) :: ifile='../oisst.winter.dat'
!     character(len=72) :: ifile='../oisst.subarctic-front.dat'
      end module oisst

      module gpcp
! ======================================================================
!
! ======================================================================
      integer :: year0, s_year, e_year
      integer :: nlon, nlat, nlev, klev
      parameter (year0=1979, s_year=1979,e_year=2009)
      parameter (nmon=1)
      parameter (nyrs=e_year-s_year+1)
      parameter (ntim=nyrs*nmon)
      parameter (nlon=144, nlat=72, nlev=1, klev=1)
      character(len=72) :: varname = 'precip'
      character(len=72) :: ifile='../GPCP/precip.mon.mean.nc'
      end module gpcp

      program eof
! ======================================================================
!     Program to compute EOF
! ======================================================================
      use oisst
      use svd

! ----------------------------------------------------------------------
!     dimensions for input data
!     ntim = length of data (in months)
! ----------------------------------------------------------------------

! ----------------------------------------------------------------------
!     the domain you want to analyse
! ----------------------------------------------------------------------
!     parameter (slon=140, elon=245, slat=25, elat= 60)
!     parameter (slon=145, elon=180, slat=25, elat= 50)
!     parameter (slon=115, elon=245, slat=25, elat= 60)
!     parameter (slon=127, elon=140, slat=35, elat= 45)
      parameter (slon=120, elon=270, slat=-20, elat= 60)

! ----------------------------------------------------------------------
!     neof = no. of eof's you want to write out
! ----------------------------------------------------------------------
      parameter(neof=5)

! ======================================================================
      real(8), allocatable :: dx(:,:), de(:), ds(:), du(:,:), dv(:,:)

      real(4) :: bufr  (nlon,nlat)
      real(4) :: rlon  (nlon)
      real(4) :: rlat  (nlat)
      real(4) :: rlev  (nlev)
      real(4) :: gmsk  (nlon,nlat)

      parameter(ngrid=nlon*nlat)
      integer :: msklon(ngrid)
      integer :: msklat(ngrid)
      real(4) :: xdata(ntim,ngrid)
!     real(4), allocatable :: xdata(:,:)

      real(4) :: amp(ntim)

      character(len=72) :: varname2
      character(len=72) :: ofile = 'mode.dat.nc'
      integer :: id(4)
      real(8) :: time

      real(4) :: pi, deg2rad, undef
      integer :: num

      pi = 4.*atan(1.)
      deg2rad = pi/180.
      undef = -9.99e33

! ----------------------------------------------------------------------
!     open files
! ----------------------------------------------------------------------
      nout  = 20
      open (nout,file='svd.txt', &
            form='formatted')

      ndamp = 40
      open (ndamp,file='amp.dat', &
            form='unformatted',access='direct',recl=4)

      nctl2 = 60
      open (nctl2,file='amp.ctl', &
            form='formatted')

      write(nctl2,'(a)') &
        'dset  ^amp.dat'
      write(nctl2,'(a)') &
        'options big_endian'
      write(nctl2,'(a)') &
        'undef  -9.99e33'
      write(nctl2,'(a)') &
        'xdef   1 linear 0.0 1.0'
      write(nctl2,'(a)') &
        'ydef   1 linear 0.0 1.0'
      write(nctl2,'(a)') &
        'zdef   1 linear 1.0 1.0'
      if (nmon == 1) then
      write(nctl2,'(a,i4,a,i4,a)') &
        'tdef  ',ntim,' linear 00z1jan',s_year,' 1yr'
      else
      write(nctl2,'(a,i4,a,i4,a)') &
        'tdef  ',ntim,' linear 00z1jan',s_year,' 1mo'
      end if
      write(nctl2,'(a,i4)') &
        'vars  ',neof
      do i = 1, neof
      write(nctl2,'(a,i1,a,i1)') &
        'eof',i,'  0 99 EOF',i
      end do
      write(nctl2,'(a)') &
        'endvars'

! ----------------------------------------------------------------------
!     set mask
! ----------------------------------------------------------------------
! ... oisst
      do i = 1, nlon
        rlon(i) = 0.125 + (i-1)*0.25
      end do
      do j = 1, nlat
        rlat(j) = -89.875 + (j-1)*0.25
      end do
      open (50,file=trim(ifile), &
     &         form='unformatted',access='direct',recl=nlon*nlat*4)
      jrec = 1
      read (50,rec=jrec) bufr
      close(50)

!     call netcdf_read_header_dimension(ifile, &
!    &     nlon,nlat,nlev,rlon,rlat,rlev)
!     call netcdf_read_data(ifile, &
!    &     varname,nlon,nlat,bufr,1,1)

      ngrid2=0
      do j = 1,nlat
      do i = 1,nlon
        gmsk(i,j) = 0.0
        if (rlon(i) < slon) cycle
        if (rlon(i) > elon) cycle
        if (rlat(j) < slat) cycle
        if (rlat(j) > elat) cycle
!       if (bufr(i,j) <  -9.99e20) cycle
        if (bufr(i,j) <  -99) cycle

        gmsk(i,j) = 1.0
        ngrid2 = ngrid2+1
        msklon(ngrid2)=i
        msklat(ngrid2)=j
      end do
      end do
 
! ----------------------------------------------------------------------
!     read data
! ----------------------------------------------------------------------
!     allocate(xdata(ntim,ngrid2))
      itim = 0
      do loop_year  = s_year, e_year
      do loop_month = 1, 1
!     do loop_month = 12, 14
!       irec = (loop_year-year0)*12 + loop_month
        irec = (loop_year-year0)*1 + loop_month
!       call netcdf_read_data(ifile, &
!              varname,nlon,nlat,bufr,klev,irec)
        open (50,file=trim(ifile), &
     &           form='unformatted',access='direct',recl=nlon*nlat*4)
        jrec = (irec-1)*4 + 3
        jrec = (irec-1)*3 + 1
        read (50,rec=jrec) bufr
        close(50)

        print *,'',loop_year,' ',loop_month

        itim = itim + 1
        do i = 1, ngrid2
	  xdata(itim,i)=0.0
        end do

        nv = 0
        do j = 1,nlat
        fac = cos(rlat(j)*deg2rad)
        fac = 1
        do i = 1,nlon
          if (gmsk(i,j) .ne. 0.0) then
            nv = nv + 1
            xdata(itim,nv) = bufr(i,j)/sqrt(fac)
	  end if
        end do
        end do
      end do
      end do

      write(nout,*)
      write(nout,*) ' data read : ',itim
      write(nout,*)

! ----------------------------------------------------------------------
!     anomaly
! ----------------------------------------------------------------------
      do i = 1, ngrid2
        do m = 1, nmon
          av = 0.0
          sd = 0.0
          do k = 1, nyrs
            j = (k-1)*nmon + m
            av = av + xdata(j,i)
            sd = sd + xdata(j,i)**2
          end do
          av = av/ntim
          sd = sd - ntim*av**2
          sd = sqrt(sd/ntim)
          do k = 1, nyrs
            j = (k-1)*nmon + m
!           xdata(j,i) = (xdata(j,i) - av)/sd
            xdata(j,i) = (xdata(j,i) - av)
          end do
        end do

!       call trend(xdata(1,i),ntim)
      end do

! ----------------------------------------------------------------------
!     singular value decomposition using double precision linpack
! ----------------------------------------------------------------------
      nra = ngrid2
      nca = ntim
      lda = ngrid
      ldu = ngrid
      ldv = ntim
      job = 11

      allocate(dx(ngrid2,ntim))
      allocate(de(ntim))
      allocate(ds(ngrid2))
      allocate(dv(ntim,ntim))
      allocate(du(ngrid2,ngrid2))
      do j=1,ntim
      do i=1,ngrid2
        dx(i,j) = xdata(j,i)
      end do
      end do
      call dsvdc(dx,ngrid2,ntim,ds,de,du,dv,job,info)

! ----------------------------------------------------------------------
! ... find partitioning of variance
! ----------------------------------------------------------------------
      call rnkeof(ds,ntim,irank)

      irank=0
      do n=1,ntim
        if (ds(n) .gt. tol) irank=irank+1
      end do

      var = 0.
      do i=1,irank
        var = var + ds(i)*ds(i)
      end do

      write(nout,*)
      do i=1,neof
        per = ds(i)*ds(i)/var
        write(nout,*) '    eof ',i,' : ',per*100.,' %'
      end do
      write(nout,*)
      write(nout,*) ' irank ',irank
      write(nout,*)

! ----------------------------------------------------------------------
!     write out
! ----------------------------------------------------------------------

      id(1) = 1; id(2) = 1; id(3) = 1; id(4) = 0
      call netcdf_write_header_dimension(ofile, &
           nlon,nlat,1,rlon,rlat,0.0,'eof', 'years', id)

      do n=1,neof
        av = 0.0
        sd = 0.0
        do i=1,ntim
          amp(i) = dv(i,n)*ds(n)
          av = av + amp(i)
          sd = sd + amp(i)**2.
        end do
        av = av/ntim
        sd = sd - ntim*av**2
        sd = sqrt(sd/ntim)

        do i=1,ntim
          namrec=(i-1)*neof + n
          write(ndamp,rec=namrec) amp(i)/sd
        end do
        
        bufr = -9.99e33
        do nv = 1, ngrid2
          i = msklon(nv)
          j = msklat(nv)
          bufr(i,j) = du(nv,n)*sd
        end do

        write(varname2,'(a,i1)') 'eof',n
        time = 0
        call netcdf_write_header_variable(ofile, &
            varname2, 'eof', 'no', 'real4', undef, 100., -100.)
        call netcdf_write_data(ofile, &
            varname2, nlon, nlat, bufr, 1, 1, time, undef)

      end do

      deallocate(dx,de,ds,du,dv)
      stop 'normal end'
      end program eof

      subroutine rnkeof(ds,ntim,irank)
! --------------------------------------------------------------------
!
! --------------------------------------------------------------------
      real(8) :: ds(ntim)
      real(8) :: tol=1.0d-7

      irank=0
      do n=1,ntim
        if (ds(n) .gt. tol) irank=irank+1
      end do

      return
      end subroutine rnkeof

      subroutine trend(x,n)
! --------------------------------------------------------------------
!     calculate and print the regression constant, regression
!     coefficient and r**2
! --------------------------------------------------------------------
      integer, intent(in) :: n
      real(4), intent(inout) :: x(n)

      nm =0
      sxy=0.0
      sx =0.0
      sy =0.0
      sx2=0.0
      sy2=0.0

      do i=1,n
        nm=nm+1
        sxy=sxy+i*x(i)
        sx =sx +i*1.0
        sy =sy +x(i)
        sx2=sx2+1.0*i*i
        sy2=sy2+x(i)*x(i)
      end do

      b=(nm*sxy-sx*sy)/(nm*sx2-sx*sx)
      a=sy/nm-b*sx/nm

      do i=1,n
        x(i)=x(i)-a-b*i
      end do

      r=(nm*sxy-sx*sy)**2/((nm*sx2-sx*sx)*(nm*sy2-sy*sy))
      write(6,3)a
      write(6,4)b
      write(6,5)r
 3    format(' regression constant:    ',f10.4)
 4    format(' regression coefficient: ',f10.4)
 5    format(' rsquared:               ',f10.7)
      return 
      end subroutine trend
EOF
unlimit
set FC = "gfortran"
set OPT = "-fconvert=big-endian"
set FC = "ifort"
set OPT = "-convert big_endian -assume byterecl -ftrapuv -std -fpe0 -traceback"
set NETCDF = "$home/local/netcdf/*.o -L/usr/local/lib -lnetcdf"
#$FC $OPT -c dsvdc.f90
$FC $OPT dsvdc.f90 a.f90 $NETCDF
/bin/rm svd.txt
./a.out
/bin/rm a.out a.f90
cat svd.txt

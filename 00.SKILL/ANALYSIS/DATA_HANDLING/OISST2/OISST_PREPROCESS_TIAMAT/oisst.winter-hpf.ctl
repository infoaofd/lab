dset    ^oisst.winter-hpf.dat
title   daily-oi-v2
options big_endian 
undef   -999.0
xdef  1440 linear   0.125  0.25
ydef   720 linear -89.875  0.25
zdef     1 levels   0.0
tdef  30 linear 00z01jan1981 1yr
*tdef  351 linear 00z01oct1981 1mo
vars  3
sst     0 99 relative humidity 850hPa :% 
smooth  0 99 relative humidity 850hPa :% 
fine    0 99 relative humidity 850hPa :% 
endvars

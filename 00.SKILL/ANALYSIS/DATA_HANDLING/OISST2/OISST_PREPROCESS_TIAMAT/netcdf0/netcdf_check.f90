!/raid7/manda/2024-08-22_BAK_WORK04/HOME_BAK_2022-01-08/local/netcdf0
!----------------------------------------------------------------------
! check NetCDF error
!----------------------------------------------------------------------
   subroutine netcdf_check (status, text)

   implicit none

!--- NetCDF ---
   include 'netcdf.inc'

!--- intent ---
   character(len=*), intent(in) :: text
   integer, intent(in) :: status

   if (status /= nf_noerr) then
     write(*,*) text
     write(*,*) nf_strerror(status)
     stop 'error'
   end if

   return
   end subroutine netcdf_check

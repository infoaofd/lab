!/raid7/manda/2024-08-22_BAK_WORK04/HOME_BAK_2022-01-08/local/netcdf0
#!/bin/csh -f

set NC_LIB = /usr/local/netcdf-3.6.3/lib
set NC_INC = /usr/local/netcdf-3.6.3/include

set FC = "ifort "
set OPT = "-auto  -ftrapuv -check all -std -fpe0 -traceback"
set OPT = " "

$FC $OPT -c -I$NC_INC *.f90 -L$NC_LIB -lnetcdf


!/raid7/manda/2024-08-22_BAK_WORK04/HOME_BAK_2022-01-08/local/netcdf0
!----------------------------------------------------------------------
! --- module for reading netCDF header
!----------------------------------------------------------------------
   module netcdf_read_mod
   implicit none

!--- NetCDF ---
   include 'netcdf.inc'

!--- USER may change ---
   character(len=4) :: tim_name='time'
   character(len=3) :: lon_name='lon'
   character(len=3) :: lat_name='lat'
!  character(len=3) :: lev_name='lev'
   character(len=5) :: lev_name='level'
!  character(len=5) :: lev_name='depth'

   end module netcdf_read_mod

!----------------------------------------------------------------------
! --- read netCDF header
!----------------------------------------------------------------------
   subroutine netcdf_read_header_dimension (file_name, &
  &           nlon,nlat,nlev,xlon,ylat,zlev)

   use netcdf_read_mod
   implicit none

!--- intent ---
   character(len=*), intent(in) :: file_name
   integer, intent(in) :: nlon
   integer, intent(in) :: nlat
   integer, intent(in) :: nlev
   real(4), intent(out) :: xlon(nlon)
   real(4), intent(out) :: ylat(nlat)
   real(4), intent(out) :: zlev(nlev)

!--- local ---
   integer :: status
   integer :: ncid
   integer :: varid
   integer :: lonid, latid, levid, timid

   integer :: ilon, ilat, ilev, itim
   character(len=128) :: text

!--- open file ---
   status = nf_open(file_name, nf_nowrite, ncid)
   call netcdf_check(status,'failed to open netcdf file')

!--- check global attribute ---
   text = ' '
   status = nf_get_att_text(ncid, nf_global, 'title', text)
!  call netcdf_check(status,'cannot read global attribute')
   write(*,*) trim(text)

!--- check x-axis ---
   status = nf_inq_dimid (ncid, lon_name, lonid)
   call netcdf_check(status,'cannot read x-axis 1')
   status = nf_inq_dimlen(ncid,  lonid, ilon)
   call netcdf_check(status,'cannot read x-axis 2')
   if (ilon /= nlon) then
     write(*,*) 'size of lon. dim. for parameter = ',nlon
     write(*,*) 'size of lon. dim. for netCDF    = ',ilon
     stop 'error : specified longitude dimension'
   end if
   status = nf_inq_varid (ncid, lon_name, varid)
   call netcdf_check(status,'cannot read x-axis 3')
   status = nf_get_var_real(ncid, varid, xlon)
   call netcdf_check(status,'cannot read x-axis 4')

!--- check y-axis ---
   status = nf_inq_dimid (ncid, lat_name, latid)
   call netcdf_check(status,'cannot read y-axis 1')
   status = nf_inq_dimlen(ncid,  latid, ilat)
   call netcdf_check(status,'cannot read y-axis 2')
   if (ilat /= nlat) then
     write(*,*) 'size of lat. dim. for parameter = ',nlat
     write(*,*) 'size of lat. dim. for netCDF    = ',ilat
     stop 'error : specified latitude dimension'
   end if
   status = nf_inq_varid (ncid, lat_name, varid)
   call netcdf_check(status,'cannot read y-axis 3')
   status = nf_get_var_real(ncid, varid, ylat)
   call netcdf_check(status,'cannot read y-axis 4')

!--- check z-axis ---
   status = nf_inq_dimid (ncid, lev_name, levid)
!  call netcdf_check(status,'cannot read z-axis 1')
   if (status /= nf_noerr) then
   lev_name = 'lev'
   status = nf_inq_dimid (ncid, lev_name, levid)
   end if
   if (status /= nf_noerr) then
   lev_name = 'level'
   status = nf_inq_dimid (ncid, lev_name, levid)
   end if
   if (status /= nf_noerr) then
   lev_name = 'depth'
   status = nf_inq_dimid (ncid, lev_name, levid)
   end if

   if (status == nf_noerr) then
     status = nf_inq_dimlen(ncid,  levid, ilev)
     call netcdf_check(status,'cannot read z-axis 2')
     if (ilev /= nlev) then
       write(*,*) 'size of lev. dim. for parameter = ',nlev
       write(*,*) 'size of lev. dim. for netCDF    = ',ilev
       stop 'error : specified vertical dimension'
     end if
     status = nf_inq_varid (ncid, lev_name, varid)
     call netcdf_check(status,'cannot read z-axis 3')
     status = nf_get_var_real(ncid, varid, zlev)
     call netcdf_check(status,'cannot read z-axis 4')
   else
     write(*,*) 'No size of lev. dim. for netCDF '
     zlev = 0.0
     print *,lev_name
     if (nlev > 1) stop 'lev_name may be wrong!'
   end if

   status = nf_inq_dimid (ncid, tim_name, timid)
   call netcdf_check(status,'cannot read t-axis 1')
   status = nf_inq_dimlen(ncid,  timid, itim)
   call netcdf_check(status,'cannot read t-axis 2')

!--- close file ---
   status = nf_close(ncid)
   call netcdf_check(status,'failed to close netcdf file')

   return
   end subroutine netcdf_read_header_dimension

!----------------------------------------------------------------------
! read netCDF file
!----------------------------------------------------------------------
   subroutine netcdf_read_data(file_name, varname, &
  &           nlon, nlat, buffer, irec_level, irec_time )

   use netcdf_read_mod
   implicit none

!--- intent ---
   character(len=*), intent(in) :: file_name
   character(len=*), intent(in) :: varname
   integer, intent(in) :: nlon
   integer, intent(in) :: nlat
   integer, intent(in) :: irec_time
   integer, intent(in) :: irec_level
   real(4), intent(out) :: buffer(nlon,nlat)

!--- local ---
   integer :: status
   integer :: ncid
   integer :: varid

   integer   , allocatable :: start(:)
   integer   , allocatable :: count(:)
   integer(1), allocatable :: buffer_i1(:,:)
   integer(2), allocatable :: buffer_i2(:,:)
   integer(4), allocatable :: buffer_i4(:,:)
   real(4)   , allocatable :: buffer_r4(:,:)
   real(8)   , allocatable :: buffer_r8(:,:)

   integer :: idims
   integer :: xtype
   real(4) :: scale, offset, missing_value

   character(len=80) :: long_name = ' '
   character(len=80) :: unit = ' '
   character(len=38), dimension(6) :: what_type = &
        (/ 'NCBYTE   (8 bit data                )',  &
           'NCCHAR   (character                 )',  &
           'NCSHORT  (16 bit integer            )',  &
           'NCLONG   (32 bit integer            )',  &
           'NCFLOAT  (32 bit IEEE floating-point)',  &
           'NCDOUBLE (64 bit IEEE floating-point)'/)

   logical, save :: first = .true.

!--- open file ---
   status = nf_open(file_name, nf_nowrite, ncid)
   call netcdf_check(status,'failed to open netcdf file')

!--- get ID ---
   status = nf_inq_varid(ncid, varname, varid)
   call netcdf_check(status,'cannot read var. id')

!--- check num. of dimensions ---
   status = nf_inq_varndims(ncid, varid, idims)
   call netcdf_check(status,'cannot read var. dims')

!--- get attribute ---
   status = nf_get_att_text(ncid, varid, 'long_name'    ,long_name)
   call netcdf_check(status,'cannot read var. long_name')

   status = nf_get_att_text(ncid, varid, 'units'        ,unit)
   call netcdf_check(status,'cannot read var. unit')

   status = nf_inq_vartype (ncid, varid, xtype)
   call netcdf_check(status,'cannot read variable type')

   status = nf_get_att_real(ncid, varid, 'scale_factor' ,scale)
!  call netcdf_check(status,'cannot read attribute')
   if (status /= nf_noerr) scale = 1.0

   status = nf_get_att_real(ncid, varid, 'add_offset'   ,offset)
!  call netcdf_check(status,'cannot read attribute')
   if (status /= nf_noerr) offset = 0.0

   status = nf_get_att_real(ncid, varid, 'missing_value',missing_value)
!  call netcdf_check(status,'cannot read attribute')
   if (status /= nf_noerr) missing_value = -9.99e33

   if (first) then
     first = .false.
     write(*,*) 'Variable name     =',trim(varname)
     write(*,*) 'Variable ID       =',varid
     write(*,*) 'num. of dimension =',idims
     write(*,*) 'long name         =',trim(long_name)
     write(*,*) 'units             =',trim(unit)
     write(*,*) 'type of variable  =',what_type(xtype)
     write(*,*) 'scale             =',scale
     write(*,*) 'offset            =',offset
     write(*,*) 'missing value     =',missing_value
   end if

!--- set count & start ---
   allocate(start(idims)) ; allocate(count(idims))
   select case (idims)
   case (3)
     count(1)=nlon  ; start(1)=1
     count(2)=nlat  ; start(2)=1
     count(3)=1     ; start(3)=irec_time
   case (4)
     count(1)=nlon  ; start(1)=1
     count(2)=nlat  ; start(2)=1
     count(3)=1     ; start(3)=irec_level
     count(4)=1     ; start(4)=irec_time
   case default
     stop 'num. of dimension is wrong'
   end select

!--- get data ---
   select case (xtype)
   case (nf_byte)
     allocate(buffer_i1(nlon,nlat))
     status = nf_get_vara_int1  (ncid, varid, start, count, buffer_i1)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_i1(:,:) * scale + offset
     where (buffer_i1 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_i1)
   case (nf_short)
     allocate(buffer_i2(nlon,nlat))
     status = nf_get_vara_int2  (ncid, varid, start, count, buffer_i2)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_i2(:,:) * scale + offset
     where (buffer_i2 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_i2)
   case (nf_int)
     allocate(buffer_i4(nlon,nlat))
     status = nf_get_vara_int   (ncid, varid, start, count, buffer_i4)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_i4(:,:) * scale + offset
     where (buffer_i4 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_i4)
   case (nf_real)
     allocate(buffer_r4(nlon,nlat))
     status = nf_get_vara_real  (ncid, varid, start, count, buffer_r4)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_r4(:,:) !  * scale + offset
     where (buffer_r4 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_r4)
   case (nf_double)
     allocate(buffer_r8(nlon,nlat))
     status = nf_get_vara_double(ncid, varid, start, count, buffer_r8)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_r8(:,:) * scale + offset
     where (buffer_r8 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_r8)
   case default
     allocate(buffer_r4(nlon,nlat))
     status = nf_get_vara_real  (ncid, varid, start, count, buffer_r4)
     call netcdf_check(status,'failed to read data')
     buffer(:,:) = buffer_r4(:,:) !  * scale + offset
     where (buffer_r4 == missing_value) 
       buffer(:,:) = -9.99e33
     end where
     deallocate(buffer_r4)
   end select

   deallocate(start)
   deallocate(count)

!--- close file ---
   status = nf_close(ncid)
   call netcdf_check(status,'failed to close netcdf file')

   return
   end subroutine netcdf_read_data

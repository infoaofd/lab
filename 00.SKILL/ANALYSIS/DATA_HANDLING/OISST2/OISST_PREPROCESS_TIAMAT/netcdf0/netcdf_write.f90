!/raid7/manda/2024-08-22_BAK_WORK04/HOME_BAK_2022-01-08/local/netcdf0
!----------------------------------------------------------------------
! --- write NetCDF header
!----------------------------------------------------------------------
   subroutine netcdf_write_header_dimension(file_name,   &
              nlon, nlat, nlev, xlon, ylat, zlev, &
              file_info, time_unit, id)

   implicit none

   include 'netcdf.inc'

!--- USER may change ---
   character(len=4) :: tim_name='time'
   character(len=3) :: lon_name='lon'
   character(len=3) :: lat_name='lat'
   character(len=3) :: lev_name='lev'
!  character(len=5) :: lev_name='level'
!  character(len=5) :: lev_name='depth'

!--- intent ---
   character(len=*), intent(in) :: file_name
   character(len=*), intent(in) :: file_info
   character(len=*), intent(in) :: time_unit
   integer, intent(in) :: nlon
   integer, intent(in) :: nlat
   integer, intent(in) :: nlev
   real(4), intent(in) :: xlon(nlon)
   real(4), intent(in) :: ylat(nlat)
   real(4), intent(in) :: zlev(nlev)
   integer, intent(in) :: id(3)

!--- local ---
   character(len=80) :: time_string
   integer :: ncid
   integer :: status
   integer :: varid
   integer :: lonid, latid, levid, timid

!--- open file ---
   status = nf_create(file_name, nf_clobber, ncid)
   call netcdf_check(status,'cannot create file')

!--- put global attributes ---
   status = nf_put_att_text(ncid, nf_global, 'title', &
                            len(trim(file_info)), trim(file_info))
   call netcdf_check(status,'cannot write attribute')

!--- define dimension ---
   status = nf_def_dim(ncid, tim_name, nf_unlimited, timid)
   call netcdf_check(status,'cannot define dimension')

   status = nf_def_dim(ncid, lon_name, nlon        , lonid)
   call netcdf_check(status,'cannot define dimension')

   status = nf_def_dim(ncid, lat_name, nlat        , latid)
   call netcdf_check(status,'cannot define dimension')

   status = nf_def_dim(ncid, lev_name, nlev        , levid)
   call netcdf_check(status,'cannot define dimension')

!--- define coordinates ---
   status = nf_def_var(ncid, tim_name, nf_real, 1, timid, varid)
   call netcdf_check(status,'cannot define variable')

   status = nf_def_var(ncid, lon_name, nf_real, 1, lonid, varid)
   call netcdf_check(status,'cannot define variable')

   status = nf_def_var(ncid, lat_name, nf_real, 1, latid, varid)
   call netcdf_check(status,'cannot define variable')

   status = nf_def_var(ncid, lev_name, nf_real, 1, levid, varid)
   call netcdf_check(status,'cannot define variable')

!--- put dimension attribute ---
   select case (trim(time_unit))
   case('secs')
     write (time_string,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     'secs since ',id(1),'-',id(2),'-',id(3),' 00:00:0.0'
   case('hours')
     write (time_string,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     'hours since ',id(1),'-',id(2),'-',id(3),' 00:00:0.0'
   case('days')
     write (time_string,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     'days since ',id(1),'-',id(2),'-',id(3),' 00:00:0.0'
   case('years')
     write (time_string,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     'years since ',id(1),'-',id(2),'-',id(3),' 00:00:0.0'
   case default
     write (time_string,'(a)') &
     'days since 0001-01-01 00:00:0.0'
   end select
   status = nf_put_att_text(ncid, timid, 'long_name', 4,'Time')
   call netcdf_check(status,'cannot write attribute')
   status = nf_put_att_text(ncid, timid, 'units'    ,  &
                           len(trim(time_string)), trim(time_string))
   call netcdf_check(status,'cannot write attribute')

   status = nf_put_att_text(ncid, lonid, 'long_name', 9,'Longitude')
   call netcdf_check(status,'cannot write attribute')
   status = nf_put_att_text(ncid, lonid, 'units'    ,12,'degrees_east')
   call netcdf_check(status,'cannot write attribute')

   status = nf_put_att_text(ncid, latid, 'long_name', 8,'Latitude')
   call netcdf_check(status,'cannot write attribute')
   status = nf_put_att_text(ncid, latid, 'units'    ,13,'degrees_north')
   call netcdf_check(status,'cannot write attribute')

!  status = nf_put_att_text(ncid, levid, 'long_name', 5,'Depth')
   status = nf_put_att_text(ncid, levid, 'long_name', 5,'Level')
   call netcdf_check(status,'cannot write attribute')
!  status = nf_put_att_text(ncid, levid, 'units'    , 1,'m')
   status = nf_put_att_text(ncid, levid, 'units'    , 3,'hPa')
   call netcdf_check(status,'cannot write attribute')
!  status = nf_put_att_text(ncid, levid, 'positive' , 4,'down')
!  status = nf_put_att_text(ncid, levid, 'positive' , 2,'up')
   call netcdf_check(status,'cannot write attribute')

!--- end define mode ---
   status = nf_enddef(ncid)
   call netcdf_check(status,'cannot finish define mode')

!--- put dimension ---
   status = nf_inq_varid   (ncid, lon_name, varid)
   call netcdf_check(status,'cannot read ID')
   status = nf_put_var_real(ncid, varid, xlon)
   call netcdf_check(status,'cannot write dimension')
   status = nf_inq_varid   (ncid, lat_name, varid)
   call netcdf_check(status,'cannot read ID')
   status = nf_put_var_real(ncid, varid, ylat)
   call netcdf_check(status,'cannot write dimension')
   status = nf_inq_varid   (ncid, lev_name, varid)
   call netcdf_check(status,'cannot read ID')
   status = nf_put_var_real(ncid, varid, zlev)
   call netcdf_check(status,'cannot write dimension')

!--- close file ---
   status = nf_close(ncid)
   call netcdf_check(status,'failed to close netcdf file')

   return
   end subroutine netcdf_write_header_dimension

!----------------------------------------------------------------------
! write NetCDF header
!----------------------------------------------------------------------
   subroutine netcdf_write_header_variable(file_name, &
              varname, long_name, unit, xtype, &
              missing_value, add_offset, scale_factor)

   implicit none

   include 'netcdf.inc'

!--- USER may change ---
   character(len=4) :: tim_name='time'
   character(len=3) :: lon_name='lon'
   character(len=3) :: lat_name='lat'
   character(len=3) :: lev_name='lev'
!  character(len=5) :: lev_name='level'
!  character(len=5) :: lev_name='depth'

!--- intent ---
   character(len=*), intent(in) :: file_name
   character(len=*), intent(in) :: varname
   character(len=*), intent(in) :: long_name
   character(len=*), intent(in) :: unit
   character(len=*), intent(in) :: xtype
   real(4), intent(in) :: missing_value

!--- local ---
   integer :: ncid
   integer :: status
   integer :: varid
   integer :: timid, lonid, latid, levid

   integer :: idims
   integer :: dimsize(4)
   real(4) :: add_offset
   real(4) :: scale_factor
 
!--- open file ---
   status = nf_open(file_name, nf_write, ncid)
   call netcdf_check(status,'failed to open netcdf file')

!--- start define mode ---
   status = nf_redef(ncid)
   call netcdf_check(status,'cannot start define mode')

!--- get num. of dimension
   status = nf_inq_ndims(ncid, idims)
   call netcdf_check(status,'cannot get ndims')

!--- define variables ---
   if (idims == 4) then
     status = nf_inq_dimid(ncid, lon_name, lonid)
     status = nf_inq_dimid(ncid, lat_name, latid)
     status = nf_inq_dimid(ncid, lev_name, levid)
     status = nf_inq_dimid(ncid, tim_name, timid)
     dimsize(1) = lonid
     dimsize(2) = latid
     dimsize(3) = levid
     dimsize(4) = timid
     select case (xtype)
     case('short')
       status = nf_def_var(ncid, varname, nf_short , 4, dimsize, varid)
     case('real4')
       status = nf_def_var(ncid, varname, nf_real  , 4, dimsize, varid)
     case('real8')
       status = nf_def_var(ncid, varname, nf_double, 4, dimsize, varid)
     case default
       status = nf_def_var(ncid, varname, nf_real  , 4, dimsize, varid)
     end select
   else if (idims == 3) then
     status = nf_inq_dimid(ncid, lon_name, lonid)
     status = nf_inq_dimid(ncid, lat_name, latid)
     status = nf_inq_dimid(ncid, tim_name, timid)
     dimsize(1) = lonid
     dimsize(2) = latid
     dimsize(3) = timid
     select case (xtype)
     case('short')
       status = nf_def_var(ncid, varname, nf_short , 3, dimsize, varid)
     case('real4')
       status = nf_def_var(ncid, varname, nf_real  , 3, dimsize, varid)
     case('real8')
       status = nf_def_var(ncid, varname, nf_double, 3, dimsize, varid)
     case default
       status = nf_def_var(ncid, varname, nf_real  , 3, dimsize, varid)
     end select
   else
     print *,' num. of dimension is wrong'
     stop
   end if
   call netcdf_check(status,'cannot define variable')

!--- put variable attribute ---
   status = nf_put_att_text(ncid, varid, 'long_name' ,  &
                            len(trim(long_name)), trim(long_name))
   call netcdf_check(status,'cannot write attribute')
 
   status = nf_put_att_text(ncid, varid, 'units'     ,  &
                            len(trim(unit)), trim(unit))
   call netcdf_check(status,'cannot write attribute')
 
   if (xtype == 'short') then
     status = nf_put_att_real(ncid, varid, 'add_offset'   ,  &
                              nf_real  , 1, add_offset)
     call netcdf_check(status,'cannot write attribute')

     status = nf_put_att_real(ncid, varid, 'scale_factor' ,  &
                              nf_real  , 1, scale_factor)
     call netcdf_check(status,'cannot write attribute')

     status = nf_put_att_int (ncid, varid, 'missing_value',  &
                              nf_short , 1, 32767)
     call netcdf_check(status,'cannot write attribute')
   else
     status = nf_put_att_real(ncid, varid, 'missing_value',  &
                              nf_real  , 1, missing_value)
     call netcdf_check(status,'cannot write attribute')
   end if

!--- end define mode ---
   status = nf_enddef(ncid)
   call netcdf_check(status,'cannot finish define mode')


!--- close file ---
   status = nf_close(ncid)
   call netcdf_check(status,'failed to close netcdf file')

   return
!--- print ---
   print *,'********************************'
   print *,'variable : ',trim(varname)
   print *,'********************************'
   print *,'   name          = ',trim(long_name)
   print *,'   unit          = ',trim(unit)
   print *,'   add_offset    = ',add_offset
   print *,'   scale_factor  = ',scale_factor
   print *,' '
 
   return
   end subroutine netcdf_write_header_variable

!----------------------------------------------------------------------
! --- write NetCDF 
!----------------------------------------------------------------------
   subroutine netcdf_write_data(file_name, varname, &
              nlon, nlat, buffer, irec_level, irec_time, time_now, &
              missing_value)

   implicit none

   include 'netcdf.inc'

!--- USER may change ---
   character(len=4) :: tim_name='time'
   character(len=3) :: lon_name='lon'
   character(len=3) :: lat_name='lat'
   character(len=3) :: lev_name='lev'
!  character(len=5) :: lev_name='level'
!  character(len=5) :: lev_name='depth'

!--- intent ---
   integer, intent(in) :: nlon
   integer, intent(in) :: nlat
   integer, intent(in) :: irec_time, irec_level
 
   real(4), intent(in) :: buffer(nlon,nlat)
   real(4), intent(in) :: missing_value
   real(8), intent(in) :: time_now

   character(len=*), intent(in) :: file_name
   character(len=*), intent(in) :: varname

!--- local ---
   integer :: ncid
   integer :: status
   integer :: varid

   integer, dimension(:), allocatable :: start
   integer, dimension(:), allocatable :: count
   integer(1), dimension(:,:), allocatable :: buffer_i1
   integer(2), dimension(:,:), allocatable :: buffer_i2
   integer(4), dimension(:,:), allocatable :: buffer_i4
   real(4),    dimension(:,:), allocatable :: buffer_r4
   real(8),    dimension(:,:), allocatable :: buffer_r8

   integer :: idims
   integer :: xtype
   integer :: i, j
   real(4) :: add_offset
   real(4) :: scale_factor
 
!--- open file ---
   status = nf_open(file_name, nf_write, ncid)
   call netcdf_check(status,'failed to open netcdf file')

!--- write time ---
   status = nf_inq_varid   (ncid, tim_name, varid)
   call netcdf_check(status,'cannot read ID')
!  status = nf_put_vara_real(ncid, varid, irec_time, 1, time_now)
   status = nf_put_vara_double(ncid, varid, irec_time, 1, time_now)
   call netcdf_check(status,'cannot write dimension')

!--- get num. of dimension
   status = nf_inq_ndims(ncid, idims)
   call netcdf_check(status,'cannot get ndims')

!-- get variable ID ---
   status = nf_inq_varid(ncid, varname, varid)
   call netcdf_check(status,'cannot start define mode')

!--- get type of variables
   status = nf_inq_vartype(ncid, varid, xtype)
   call netcdf_check(status,'cannot get type')

!-- get variable attributes ---
   if (xtype == 3) then
     status = nf_get_att_real(ncid, varid, 'add_offset', add_offset)
     call netcdf_check(status,'cannot get attribute')
     status = nf_get_att_real(ncid, varid, 'scale_factor', scale_factor)
     call netcdf_check(status,'cannot get attribute')
   end if

!--- write variable ---
   allocate(start(idims)) ; allocate(count(idims))
   if (idims == 4 ) then
     count(1)=nlon  ; start(1)=1
     count(2)=nlat  ; start(2)=1
     count(3)=1     ; start(3)=irec_level
     count(4)=1     ; start(4)=irec_time
   else if (idims == 3) then
     count(1)=nlon  ; start(1)=1
     count(2)=nlat  ; start(2)=1
     count(3)=1     ; start(3)=irec_time
   else
     print *,' num. of dimensions is wrong'
     stop
   end if

!     byte  : (-128 <--> 127)
!     short : (-32768 <--> 32767)
!     int4 : (-2147483648 <--> 2147483647)
   select case (xtype)
   case(3)
     allocate(buffer_i2(nlon,nlat))
     do j = 1, nlat
     do i = 1, nlon
       if (buffer(i,j) > missing_value ) then
         if (abs(buffer(i,j) - add_offset)/scale_factor >= 32760) then
           print *,varname
           print *,add_offset
           print *,scale_factor
           print *,buffer(i,j)
           print *,' scale_factor or add_offset should be changed '
           stop
         else
           buffer_i2(i,j) = nint((buffer(i,j) - add_offset)/scale_factor)
         end if
       else
         buffer_i2(i,j) = 32767
       end if
     end do
     end do
     status = nf_put_vara_int2  (ncid, varid, start, count, buffer_i2)
     call netcdf_check(status,'cannot write data')
     deallocate(buffer_i2)
   case(5)
     status = nf_put_vara_real  (ncid, varid, start, count, buffer)
     call netcdf_check(status,'cannot write data')
   case(6)
     buffer_r8(:,:) = buffer(:,:)
     status = nf_put_vara_double(ncid, varid, start, count, buffer_r8)
     call netcdf_check(status,'cannot write data')
   case default
     status = nf_put_vara_real  (ncid, varid, start, count, buffer)
     call netcdf_check(status,'cannot write data')
   end select

   deallocate(start)
   deallocate(count)

!--- close file ---
   status = nf_close(ncid)
   call netcdf_check(status,'failed to close netcdf file')

   return
   end subroutine netcdf_write_data

dset ^AVHRR/%y4/avhrr-only-v2.%y4%m2%d2.nc
dtype netcdf
*undef -9.99
*undef -999.9
undef 32767
unpack scale_factor add_offset
options template
xdef  1440 linear 0.125 0.25
ydef  720 linear -89.875 0.25
zdef    1 linear 1 1
tdef  387 linear 01jan2017 1dy
vars  1
sst=>sst 1 t,z,y,x wind speed
endvars

dset    ^oisst.subarctic-front.dat
title   daily-oi-v2
options big_endian template
undef   -999
xdef  1440 linear   0.125  0.25
ydef   720 linear -89.875  0.25
zdef     1 levels   0.0
*tdef  351 linear 00z01jan1982 1yr
tdef  360 linear 00z01jan1982 1mo
vars  4
sst   0 99 relative humidity 850hPa :% 
dtdy  0 99 relative humidity 850hPa :% 
grad  0 99 relative humidity 850hPa :% 
front 0 99 relative humidity 850hPa :% 
endvars

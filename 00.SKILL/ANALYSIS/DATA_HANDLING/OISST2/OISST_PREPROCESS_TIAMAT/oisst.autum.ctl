dset    ^oisst.autum.dat
title   daily-oi-v2
options big_endian template
undef   -999
xdef  1440 linear   0.125  0.25
ydef   720 linear -89.875  0.25
zdef     1 levels   0.0
tdef  30 linear 00z01apr1982 1yr
*tdef  351 linear 00z01oct1981 1mo
vars  3
sst   0 99 relative humidity 850hPa :% 
err   0 99 relative humidity 850hPa :% 
ice   0 99 relative humidity 850hPa :% 
endvars

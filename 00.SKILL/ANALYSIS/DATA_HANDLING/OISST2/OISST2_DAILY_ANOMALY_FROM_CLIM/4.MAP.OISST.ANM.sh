#!/bin/bash

GS=$(basename $0 .sh).GS

#YYYY=2009;MMM=AUG;DD=28
YYYY=2011;MMM=OCT;DD=25
DATE=${DD}${MMM}${YYYY}

INDIR=/work01/DATA/SST/OISST2/A
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=sst.day.anom.${YYYY}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

INDIR2=/work01/DATA/SST/OISST2/R
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi
INFLE2=sst.day.mean.${YYYY}.nc
IN2=$INDIR2/$INFLE2
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

FIG=OISST_${DATE}.pdf

KIND='-kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
LEVS='-levs -3.5 -3 -2.5 -2 -1.5 -1 -0.5 0.5 1 1.5 2 2.5 3 3.5'

cat <<END >$GS
'sdfopen $IN'
'sdfopen $IN2'; 'q ctlinfo 2'; say result

'cc'; 'set grid off'
'set gxout shade2'

#'q ctlinfo';say result
'set dfile 1'
'set time $DATE'
'set lon 120 270'; 'set lat   0  60'
'color $LEVS $KIND'
'set xlint 20';'set ylint 10'
'd anom.1'
'draw title OISST2 ANOM $DATE'

'cbarn'

'set xlab off';'set ylab off'

'set dfile 2'
'set time $DATE'
'set lon 120 270'; 'set lat   0  60'
'set gxout contour'
'set ccolor 0';'set cthick 5'
'set clab off'
'set cint 2'
'd sst.2'

'set ccolor 1';'set cthick 1'
'set clab on'
'set cint 2';'set clskip 5'
'd sst.2'

'gxprint $FIG'

'quit'
END


grads -bcp $GS
#rm -vf $GS

if [ -f $FIG ];then
echo FIG: $FIG
fi


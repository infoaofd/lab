#!/bin/bash

FH=00
YYYY=2022; MM=06; DD=18; HH=21
INDIR=/work01/DATA/LFM_HCUT.PDEL/FH${FH}
 ODIR=/work01/DATA/LFM_PROC_CUT/FH${FH}

mkd $ODIR

NCL=NCL.THERMO.ncl

MMDD=${MM}${DD}

 INFLE=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
 IN=$INDIR/${INFLE}
OUT=$ODIR/$(basename $IN .nc)_THEMO.nc

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN; echo; exit 1; fi

runncl.sh $NCL $IN $OUT

if [ $? -ne 0 ]; then
echo "mmmmmmmmmmmmmm"; echo ERROR IN $NCL; echo "mmmmmmmmmmmmmm"
fi
if [ -f $OUT ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT
echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT; echo "mmmmmmmmmmmmmm"
exit 1
fi


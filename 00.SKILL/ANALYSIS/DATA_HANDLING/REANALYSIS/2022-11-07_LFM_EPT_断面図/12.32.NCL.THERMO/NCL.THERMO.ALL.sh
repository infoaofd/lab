#!/bin/bash

FH=$1; FH=${FH:-00}

INDIR=/work01/DATA/LFM_HCUT.PDEL/FH${FH}
 ODIR=/work01/DATA/LFM_PROC_CUT/FH${FH}

mkd $ODIR

NCL=NCL.THERMO.ncl

INLIST=$(ls $INDIR/LFM_PRS_FH${FH}_VALID_*.nc) ;#echo $INLIST

for IN in $INLIST; do

OUT=$ODIR/$(basename $IN .nc)_THERMO.nc

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN; echo; exit 1; fi

runncl.sh $NCL $IN $OUT

if [ $? -ne 0 ]; then
echo "mmmmmmmmmmmmmm"; echo ERROR IN $NCL; echo "mmmmmmmmmmmmmm"
fi
if [ -f $OUT ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT
echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT; echo "mmmmmmmmmmmmmm"
exit 1
fi

done #IN

exit 0



INDIR="/work01/DATA/LFM_HCUT.PDEL/FH03"
INFLE="LFM_PRS_FH03_VALID_2022-06-18_03.nc"
IN=INDIR+"/"+INFLE

a=addfile(IN,"r")

temp=a->temp

print("INDIR="+INDIR)
print("INFLE="+INFLE)


utc_date = cd_calendar(temp&time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd) +"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)

print("VALID_TIME="+VALID_TIME)

;print(temp&time)



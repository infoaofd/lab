
Variable: a
Type: file
filename:	Z__C_RJTD_20220618000000_LFM_GPV_Rjp_L-pall_FH0600
path:	/work01/DATA/LFM/FH06/Z__C_RJTD_20220618000000_LFM_GPV_Rjp_L-pall_FH0600.grib2
   file global attributes:
   dimensions:
      lv_ISBL0 = 16
      lat_0 = 631
      lon_0 = 601
      lv_ISBL1 = 12
   variables:
      float TMP_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Temperature
         units :	K
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Temperature
         parameter_template_discipline_category_number :	( 0, 0, 0, 0 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float RH_P0_L100_GLL0 ( lv_ISBL1, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Relative humidity
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 0, 0, 1, 1 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float UGRD_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	U-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 2 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float VGRD_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	V-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 3 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float VVEL_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Vertical velocity (pressure)
         units :	Pa s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 8 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float HGT_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Geopotential height
         units :	gpm
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 5 )
         level_type :	Isobaric surface (Pa)
         forecast_time :	360
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float lv_ISBL1 ( lv_ISBL1 )
         long_name :	Isobaric surface
         units :	Pa

      float lat_0 ( lat_0 )
         long_name :	latitude
         grid_type :	Latitude/Longitude
         units :	degrees_north
         Dj :	0.04
         Di :	0.05
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6

      float lon_0 ( lon_0 )
         long_name :	longitude
         grid_type :	Latitude/Longitude
         units :	degrees_east
         Dj :	0.04
         Di :	0.05
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6

      float lv_ISBL0 ( lv_ISBL0 )
         long_name :	Isobaric surface
         units :	Pa


INPUT: /work01/DATA/LFM/FH06/Z__C_RJTD_20220618000000_LFM_GPV_Rjp_L-pall_FH0600.grib2


Variable: lv_ISBL0 (coordinate)
Type: float
Total Size: 64 bytes
            16 values
Number of Dimensions: 1
Dimensions and sizes:	[lv_ISBL0 | 16]
Coordinates: 
Number Of Attributes: 2
  long_name :	Isobaric surface
  units :	Pa
10000
15000
20000
25000
30000
40000
50000
60000
70000
80000
85000
90000
92500
95000
97500
100000


Variable: lv_ISBL1 (coordinate)
Type: float
Total Size: 48 bytes
            12 values
Number of Dimensions: 1
Dimensions and sizes:	[lv_ISBL1 | 12]
Coordinates: 
Number Of Attributes: 2
  long_name :	Isobaric surface
  units :	Pa
30000
40000
50000
60000
70000
80000
85000
90000
92500
95000
97500
100000

INPUT: /work01/DATA/LFM/FH06/Z__C_RJTD_20220618000000_LFM_GPV_Rjp_L-pall_FH0600.grib2

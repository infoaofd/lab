load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

INDIR="/work01/DATA/JRA55/MON/PRS/"
;INFLE="anl_p125.007_hgt.201101_201112"
;IN=INDIR+INFLE

INLIST=systemfunc("ls "+INDIR+"anl_p125.007_hgt.*")
a=addfiles(INLIST,"r")
;print(a)

hgt=a[:]->HGT_GDS0_ISBL_S123(:,{250},{0:80},{120:300})
time=a[:]->initial_time0_hours
YYYYMM  = cd_calendar(time,-1) 

;print(YYYYMM)

  ;;; 1982-2011年だけ取り出す
  t1      = ind(YYYYMM.eq.1982*100+1)
  t2      = ind(YYYYMM.eq.2011*100+12)
  hgt    := hgt(t1:t2,:,:,:)
  YYYYMM := YYYYMM(t1:t2)


clim = clmMonTLL(hgt)
printVarSummary(clim)
anom = calcMonAnomTLL(hgt,clim)
printVarSummary(anom)

YYYYMM1=2011*100+10
t1 = ind(YYYYMM.eq.YYYYMM1)
print("t1="+t1)

FIG="JRA55_HGT250_ANM.CLM_"+tostring(YYYYMM1)

  ;;; 以降，お絵かきの設定
  wks  = gsn_open_wks("pdf",FIG)

  res1                 = True 
  res1@gsnAddCyclic=False
  res1@gsnDraw         = False    ; plotを描かない
  res1@gsnFrame        = False    ; WorkStationを更新しない
  res1@cnFillOn        = True
  res1@cnLinesOn        = False
  res1@cnInfoLabelOn   = False
  res1@tiMainString    = "HGT ANM "+YYYYMM1
  res1@cnLevelSelectionMode = "ManualLevels"
  res1@cnMinLevelValF  = -100
  res1@cnMaxLevelValF  = 100
  res1@cnLevelSpacingF = 20
  res1@mpMaxLatF       =  80
  res1@mpMinLatF       = 0
  res1@mpMaxLonF       = 300
  res1@mpMinLonF       = 120

plot = gsn_csm_contour_map_ce(wks,anom(t1,:,:),res1)

  res2                 = True 
  res2@gsnDraw         = False    ; plotを描かない
  res2@gsnFrame        = False    ; WorkStationを更新しない
  res2@cnFillOn        = False
  res2@cnLinesOn        = True
  res2@cnInfoLabelOn   = False
  res2@tiMainString    = ""
;  res2@cnLevelSelectionMode = "ManualLevels"
;  res2@cnMinLevelValF  = 0
;  res2@cnMaxLevelValF  = 28
;  res2@cnLevelSpacingF = 2
res2@cnLineLabelInterval     = 5     ; 等値線のラベルをつける間隔(この場合5本ごと)
res2@cnLineLabelFontHeightF  = 0.008  ; ラベルの文字の大きさを変える
;res2@cnLineLabelDensityF     = 2.0   ; 同一等値線上のラベルの数を変える
res2@gsnLeftString  = ""   ; デフォルトは描画した変数の@long_name
res2@gsnRightString = ""  ; デフォルトは描画した変数の@units

plot2 = gsn_csm_contour(wks,clim(9,:,:),res2)

overlay(plot, plot2)

draw(plot)
frame(wks)

print("FIG: "+FIG)

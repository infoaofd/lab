#!/bin/bash

INDIR=/work01/DATA/NCEP2/MON/PRS
INFLE=hgt.mon.mean.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo ERROR in $GS: NO SUCH FILE,$IN;exit1;fi

GS=$(basename $0 .sh).GS

TIME=01OCT2011
TEXT="NECP2 HGT $TIME"

FIG=$(basename $0 .sh)_${TIME}.pdf ;#eps


# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='-kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

LONW=120;LONE=300;LATS=0;LATN=80
LEV=250


HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=11; xwid = 7.0/xmax; ywid = 9.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset mres';'set grads off';'set grid off'
'set xlint 20';'set ylint 10'

# 'set gxout shade2'
# 'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'

'd hgt'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

#x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=x2+0.1; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'



x=(xl+xr)/2 ;# TEXT
y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TEXT}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : $FIG"; ls -lh --time-style=long-iso $FIG; fi
echo


echo "DONE $0."
echo

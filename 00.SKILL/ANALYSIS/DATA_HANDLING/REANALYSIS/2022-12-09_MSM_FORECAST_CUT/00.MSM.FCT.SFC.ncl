load "./SUBSTRING.ncl"

STN="MESHIMA"
SLAT=31.9980555556
SLON=128.350833333

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
print("SCRIPT="+scriptname_in)
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")

IN  = getenv("NCL_ARG_2")
print("mmmmmmmmmmmmmmmmmm")
print("PRS IN="+IN)
print("mmmmmmmmmmmmmmmmmm")

ODIR_ROOT = getenv("NCL_ARG_3")

f=addfile(IN,"r")

T_ORG=f->TMP_P0_L103_GLL0_1
RH_ORG=f->RH_P0_L103_GLL0_1
U_ORG=f->UGRD_P0_L103_GLL0_1
V_ORG=f->VGRD_P0_L103_GLL0_1
P0_ORG=f->PRES_P0_L1_GLL0_1

LAT_ORG=f->lat_0
LON_ORG=f->lon_0
FTIME=f->forecast_time0

dim=dimsizes(FTIME)
NFCT=dim(0)
print("NFCT="+NFCT)
print("FTIME="+FTIME)

lon=LON_ORG(:)
lat=LAT_ORG(::-1)

temp=T_ORG(:,::-1,:)
rh=RH_ORG(:,::-1,:)
u=U_ORG(:,::-1,:)
v=V_ORG(:,::-1,:)
p0=P0_ORG(:,::-1,:)/100.0
p0@units="hPa"

temp_s=linint2_points_Wrap(lon,lat,temp,False,SLON,SLAT,0)
rh_s=linint2_points_Wrap(lon,lat,rh,False,SLON,SLAT,0)
u_s=linint2_points_Wrap(lon,lat,u,False,SLON,SLAT,0)
v_s=linint2_points_Wrap(lon,lat,v,False,SLON,SLAT,0)
p0_s=linint2_points_Wrap(lon,lat,p0,False,SLON,SLAT,0)
p0_s!0="forecast_time0"

ITIME=temp@initial_time
print("ITIME="+ITIME)

MM=substring(ITIME,0,1)
DD=substring(ITIME,3,4)
YYYY=substring(ITIME,6,9)
HH=substring(ITIME,12,13)

month=toint(MM)
day=toint(DD)
year=toint(YYYY)
hour=toint(HH)
minute=0
second=0
units="hours since 2020-05-01 00:00:00"

stime = cd_inv_calendar(year,month,day,hour,minute,second,units, 0)
stime@units=units
time=stime

print("mmmmm SET TIME")

do N=2,NFCT-1,3

time= stime + FTIME(N)
time!0="time"
time&time=time

utc_date = cd_calendar(time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd) +"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)

print("INIT_TIME="+YYYY+MM+DD+" "+HH)
print("FCST_TIME="+FTIME(N))
print("VALID_TIME="+VALID_TIME)

temp_out=temp_s(N,0)
temp_out@forecast_time0=FTIME(N)
temp_out@valid_time=VALID_TIME
temp_out@level_type = "Isobaric surface (hPa)" 

rh_out=rh_s(N,0)
rh_out@forecast_time0=FTIME(N)
rh_out@valid_time=VALID_TIME
rh_out@level_type = "Isobaric surface (hPa)" 

u_out=u_s(N,0)
u_out@forecast_time0=FTIME(N)
u_out@valid_time=VALID_TIME
u_out@level_type = "Isobaric surface (hPa)" 

v_out=v_s(N,0)
v_out@forecast_time0=FTIME(N)
v_out@valid_time=VALID_TIME
v_out@level_type = "Isobaric surface (hPa)" 

p0_out=p0_s(N,0)
p0_out@forecast_time0=FTIME(N)
p0_out@valid_time=VALID_TIME
p0_out@level_type = "Isobaric surface (hPa)"

;printVarSummary(temp_out)

FH=sprinti("%0.2i", FTIME(N))
ODIR=ODIR_ROOT+"FH"+FH
system("mkdir -vp "+ODIR)

OFLE=ODIR+"/"+YYYY+MM+DD+"_"+HH+"_FH"+FH+"_"+STN+"_SFC.nc"

system("rm -vf "+OFLE)

setfileoption("nc","Format","LargeFile")

a=addfile(OFLE,"c")

print(time)

a->time = time
a->p0   = p0_out
a->temp = temp_out
a->rh = rh_out
a->u = u_out
a->v = v_out


print("MMMMMMMMMMMMMMMMMMMMMM")
print("SFC OUT: "+OFLE)
print("MMMMMMMMMMMMMMMMMMMMMM")
print("")
print("")
print("")

;printVarSummary(temp_out)

end do ;N




;system("ncdump -h "+OUT)

print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("DONE SCRIPT "+scriptname_in)
print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("")
print("")
print("")

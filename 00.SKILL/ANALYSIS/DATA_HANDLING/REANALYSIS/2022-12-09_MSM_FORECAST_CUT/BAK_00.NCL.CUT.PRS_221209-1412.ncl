load "./SUBSTRING.ncl"

LONW=120.0
LONE=134.0
LATS=22.4
LATN=35.0

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
print("SCRIPT="+scriptname_in)
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")


IN  = getenv("NCL_ARG_2")

IN  = getenv("NCL_ARG_2")
print("mmmmmmmmmmmmmmmmmm")
print("PRS IN="+IN)
print("mmmmmmmmmmmmmmmmmm")

ODIR = getenv("NCL_ARG_3")

f=addfile(IN,"r")

T_ORG=f->TMP_P0_L100_GLL0
RH_ORG=f->RH_P0_L100_GLL0
U_ORG=f->UGRD_P0_L100_GLL0
V_ORG=f->VGRD_P0_L100_GLL0
W_ORG=f->VVEL_P0_L100_GLL0
Z_ORG=f->HGT_P0_L100_GLL0
P1_ORG=f->lv_ISBL1
LAT_ORG=f->lat_0
LON_ORG=f->lon_0
P0_ORG=f->lv_ISBL0

;printVarSummary(T2_ORG)



lon=LON_ORG({LONW:LONE})
lat=LAT_ORG({LATS:LATN})
p0=P0_ORG(:)
temp=T_ORG(:,{LATS:LATN},{LONW:LONE})
rh=RH_ORG(:,{LATS:LATN},{LONW:LONE})
u=U_ORG(:,{LATS:LATN},{LONW:LONE})
v=V_ORG(:,{LATS:LATN},{LONW:LONE})
w=W_ORG(:,{LATS:LATN},{LONW:LONE})
z=Z_ORG(:,{LATS:LATN},{LONW:LONE})
p1=P1_ORG(:)

;jprintVarSummary(lat)
;printVarSummary(lon)


print("mmmmm SET TIME")

ITIME=temp@initial_time
FTIME=temp@forecast_time

print("ITIME="+ITIME+" FTIME="+FTIME)

MM=substring(ITIME,0,1)
DD=substring(ITIME,3,4)
YYYY=substring(ITIME,6,9)
HH=substring(ITIME,12,13)
FH=toint(tofloat(FTIME)/60)

month=toint(MM)
day=toint(DD)
year=toint(YYYY)
hour=toint(HH)
minute=0
second=0
units="hours since 2022-06-01 00:00:00"

time = cd_inv_calendar(year,month,day,hour,minute,second,units, 0)
time@units=units

print("INIT_TIME="+YYYY+MM+DD+" "+HH)
print("FCST_TIME="+FH)

time=time + FH
time!0="time"
time&time=time

utc_date = cd_calendar(time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd) +"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)

print("VALID_TIME="+VALID_TIME)



print("")
print("mmmmm OUTPUT")

OUT=ODIR+"/"+"LFM_PRS_FH"+sprinti("%0.2i", FH)+"_VALID_"+VALID_TIME+".nc"

system("rm -vf "+OUT)

setfileoption("nc","Format","LargeFile")

a=addfile(OUT,"c")

a->time = time
a->p0   = p0
a->p1   = p1
a->lon  = lon 
a->lat  = lat 
a->u    = u
a->v    = v
a->w    = w
a->z    = z
a->temp = temp
a->rh   = rh

print("")
print("MMMMMMMMMMMMMMMMMMMMMM")
print("PRS OUT: "+OUT)
print("MMMMMMMMMMMMMMMMMMMMMM")
;system("ncdump -h "+OUT)

print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("DONE SCRIPT "+scriptname_in)
print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("")
print("")
print("")

#!/bin/bash

INDIR_ROOT=/work02/DATA/ERA5.KOIKE.LARGE
YS=1959; YE=2022; Y=$YS
MM=02; DS=11; DE=20

ODIR=/work02/DATA/ERA5.KOIKE.LARGE/10DAY.MEAN
mkd $ODIR

while [ $Y -le $YE ]; do

INDIR=$INDIR_ROOT/$Y

INLIST=$(ls $INDIR/ERA5*${Y}${MM}1[1-9]*.grib)
INLIST="$INLIST $(ls $INDIR/ERA5*${Y}${MM}20*.grib)"

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo INPUT:
for IN in $INLIST; do
if [ ! -f $IN ];then NO SUCH FILE, $IN; exit 1;fi
echo $IN
done 
echo

OFLE=ERA5_SLP_10DAY.MEAN_${Y}_${MM}_${DS}-${DE}.nc
rm -vf $ODIR/OFLE
OUT=$ODIR/$OFLE

<<COMMENT
http://kodama.fubuki.info/wiki/wiki.cgi/CDO/tips?lang=jp#11
Tips: 複数ファイルの時間平均
cdo mergetime hoge*.nc hoge_merge.nc
cdo timmean hoge_merge.nc hoge_mean.nc
パイプラインでは記述できないので注意。
COMMENT

MERGE=merge.nc #TEMPORALLY FILE
rm -vf $MERGE
cdo -f nc4 mergetime $INLIST $MERGE
cdo -f nc4 timmean $MERGE $OUT
echo
cdo sinfo $OUT


echo OUTPUT: $OUT
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo
echo
echo

Y=$(expr $Y + 1)
done

rm -vf $MERGE

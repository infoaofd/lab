#!/bin/bash

# Mon, 12 Dec 2022 13:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.SNOW_KOIKE/42.12.MAP.TSK/22.12.SNAPSHOT

DSET=ERA5; VAR=var151; VAROUT=SLP

YYYYMMDD=${YYYYMMDD:=20140214}
YYYY=${YYYYMMDD:0:4}; MM=${YYYYMMDD:4:2}; DD=${YYYYMMDD:6:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

DATE=${DD}${MMM}${YYYY}
HH1=00;HH2=21
TIME1=${HH1}Z${DD}${MMM}${YYYY};TIME2=${HH2}Z${DD}${MMM}${YYYY};

INDIR=/work02/DATA/ERA5.KOIKE/NC/2014
INFLE=${DSET}_JAPAN_SFC_03HR_${YYYY}${MM}${DD}.nc

IN=$INDIR/$INFLE

GS=$(basename $0 .sh).GS

FIG=${DSET}_SFC_${VAROUT}_DAYMEAN_${YYYY}${MM}${DD}.eps

# LONW= ;LONE= ; LATS= ;LATN=
# LEV=
# TIME=

LEVS="980 1040 4"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=2
UNIT=hPa

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

# ${NOW}
# ${HOST}
# ${CWD}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'
# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME1}'

'set xlint 20';'set ylint 10'
'TAV=ave($VAR,time=${TIME1},time=${TIME2})'
'd TAV/100.'

'set xlab off';'set ylab off'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.35; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.03; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



# TEXT
x=(xl+xr)/2; 
y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${DSET} ${VAROUT} ${HH1}-${HH2}Z${DATE}'

# HEADER
'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

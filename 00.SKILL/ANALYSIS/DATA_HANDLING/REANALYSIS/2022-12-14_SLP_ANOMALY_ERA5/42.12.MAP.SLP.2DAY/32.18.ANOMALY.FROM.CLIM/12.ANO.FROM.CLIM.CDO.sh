#!/bin/bash

<<COMMENT
https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html
netcdfファイルの演算（複数ファイル）

cdo sub HDC_21ka.nc HDC_0ka.nc HDC_21ka-0ka.nc

HDC_21ka.nc HDC_0ka.ncはそれぞれ(x,y)で定義されており、高さ方向と時間方向にデータは1つしかないとします。HDC_21ka.nc HDC_0ka.ncの差がHDC_21ka-0ka.ncに出力されます。
同じようにadd, mul, divができます。
COMMENT

INDIR1=/work02/DATA/ERA5.KOIKE.LARGE/2DAY.MEAN
INFLE1=ERA5_SLP_2DAY.MEAN_2014_02_14-15.nc
INDIR2=/work02/DATA/ERA5.KOIKE.LARGE/CLIM.MEAN
YS2=1959;YE2=2022;MM2=02;DS2=14;DE2=15
INFLE2=ERA5_CLIM.MEAN_${YS2}-${YE2}_${MM2}_${DS2}-${DE2}.nc

IN1=$INDIR1/$INFLE1
IN2=$INDIR2/$INFLE2

if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

ODIR=/work02/DATA/ERA5.KOIKE.LARGE/2DAY.ANOM.FROM.CLIM
OFLE=ERA5_SLP_2DAY.ANOM_${YS2}-${YE2}_${MM2}_${DS2}-${DE2}.nc
OUT=$ODIR/$OFLE
mkd $ODIR

cdo sub $IN1 $IN2 $OUT

cdo sinfo $OUT

echo MMMMMMMMMMMMMMMMMMMMMMMMMM
echo OUTPUT: $OUT
echo MMMMMMMMMMMMMMMMMMMMMMMMMM

#/bin/bash


EXE=GET.ERA5.RUN.sh
if [ ! -f $EXE ];then echo ERROR NO SUCH FILE,$EXE;echo;exit 1;fi

YS=1959;YE=2022;Y=$YS
MS=2;ME=2;M=$MS
DS=10;DE=20

while [ $Y -le $YE ]; do

MM=$(printf %02d $M)

DATE0=$( date -d"$Y/$MM/$DS " +%Y%m%d)
DATE1=$( date -d"$Y/$MM/$DE " +%Y%m%d)
#DATE1=$( date +%Y%m%d --date '1 day')
# dateコマンドで一日前の日付を取得する方法
# https://ex1.m-yabe.com/archives/2110

echo MMMMM DOWNLOADING $DATE0 - $DATE1
$EXE $DATE0 $DATE1
echo MMMMM DONE $DATE0 - $DATE1; echo

Y=$(expr $Y + 1)

done # Y


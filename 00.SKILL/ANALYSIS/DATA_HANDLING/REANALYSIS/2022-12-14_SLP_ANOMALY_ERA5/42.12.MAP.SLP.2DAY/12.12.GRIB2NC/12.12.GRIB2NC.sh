#!/bin/bash

YYYY=$1; M=$2; D=$3

YYYY=${YYYY:-2014};M=${M:-2};D=${D:-14}

MM=$(printf %02d $M)
DD=$(printf %02d $D)


DATE=${YYYY}${MM}${DD}

INDIR=/work02/DATA/ERA5.KOIKE.LARGE/$YYYY
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi

ODIR=/work02/DATA/ERA5.KOIKE.LARGE/NC/$YYYY
mkdir -vp $ODIR

<<COMMENT
INFLE=ERA5_JAPAN_PRS_03HR_${DATE}.grib
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1;fi
OFLE=$(basename $IN .grib).nc
OUT=$ODIR/$OFLE

cdo -f nc4 copy $IN $OUT

echo; echo MMMMM INPUT $IN
      echo MMMMM OUTPUT $OUT; echo
COMMENT

INFLE=ERA5_JAPAN_SFC_03HR_${DATE}.grib
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1;fi
OFLE=$(basename $IN .grib).nc
OUT=$ODIR/$OFLE
rm -vf $OUT

cdo -f nc4 copy $IN $OUT

echo; echo MMMMM INPUT $IN
      echo MMMMM OUTPUT $OUT; echo

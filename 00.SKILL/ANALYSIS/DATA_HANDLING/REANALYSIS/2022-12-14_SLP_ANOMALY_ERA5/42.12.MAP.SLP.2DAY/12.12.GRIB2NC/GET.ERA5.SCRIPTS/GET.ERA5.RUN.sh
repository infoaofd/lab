

EXE=GET.ERA5.SFC.sh
if [ ! -f $EXE ];then echo ERROR: NO SUCH FILE, $EXE;echo;exit1;fi


yyyymmdd1=$1; yyyymmdd2=$2
yyyymmdd1=${yyyymmdd1:-20140214}; yyyymmdd2=${yyyymmdd2:-20140215}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1};  end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  $EXE ${yyyy} ${mm} ${dd}

  i=$(expr $i + 1)
done

exit 0

#!/bin/bash

RA=ERA5; REGION=JAPAN; INTVL=03HR


LOG=$PWD/$(basename $0 .sh).LOG

date -R > $LOG; pwd    >> $LOG

README="00.README.TXT"


YYYY=$1; MM=$2; DD=$3

YYYY=${YYYY:-2014}; MM=${MM:-02}; DD=${DD:-14}

DIR=${YYYY}

mkdir -vp $DIR; cp -av $0 $DIR

CWD=$(pwd); CMD=$0

cd $DIR
pwd

date -R > ${README}; pwd >> ${README}; hostname >> ${README}
echo "MMMMM $YYYY $MM $DD   $CWD $CMD"


PY=ERA5.DL.PY

DTYP="SFC"
OUT=${RA}_${REGION}_${DTYP}_${INTVL}_${YYYY}${MM}${DD}.grib

cat <<EOF >$PY
import cdsapi

c = cdsapi.Client()

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'format': 'grib',
        'product_type': 'reanalysis',
        'variable': [
            'skin_temperature','sea_surface_temperature', 
            '10m_u_component_of_wind', '10m_v_component_of_wind', '2m_dewpoint_temperature',
            '2m_temperature', 'convective_rain_rate', 'convective_snowfall',
            'large_scale_rain_rate', 'large_scale_snowfall', 'mean_sea_level_pressure',
            'mean_surface_latent_heat_flux', 'mean_surface_net_long_wave_radiation_flux', 'mean_surface_net_short_wave_radiation_flux',
            'mean_surface_sensible_heat_flux', 'snow_depth', 'snowfall',
        ],
        'year': [
            '${YYYY}',
        ],
        'month': [
            '${MM}',
        ],
        'day': [
            '${DD}',
        ],
        'time': [
            '00:00', '03:00', '06:00',
            '09:00', '12:00', '15:00',
            '18:00', '21:00',
        ],
        'area': [
            80, 90, 20,
            150,
        ],
    },
    '${OUT}')
EOF

python3 $PY >> $LOG
rm -v $PY

cd $CWD
echo "NNNNN DONE $YYYY $MM $DD $CWD $CMD"; echo


#!/bin/bash

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

GS=$(basename $0 .sh).GS

INDIR=OUT_NC_MESHIMA
INFLE=$1
INFLE=${INFLE:-20220701_MESHIMA_MSM_HINTPL.nc}
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo ERROR: NO SUCH FILE, $IN; echo;exit1; fi

FIG=$(basename $INFLE .nc).pdf

LEV="1000 900"

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

'sdfopen ${IN}'

'set lev ${LEV}'

nmax=8; xmax=3; ymax=4

'cc'; 'set grid off'; 'set grads off'

xleft=0.5; ytop=9.4

xwid = 6.0/xmax; ywid = 10.0/ymax

xmargin=0.7; ymargin=0.7

# SET PAGE
'set vpage 0.0 8.5 0.0 11'


nmap=1; xmap=1; ymap=1

while ( nmap <= nmax )


mod=math_mod(nmap, xmax)

#say 'xs='xs; say 'xleft='xleft; say 'xwid='xwid; say 'xmargin='xmargin; 'xmap='xmap
#say 'ys='ys; say 'ytop='ytop; say 'ywid='ywid; say 'ymargin='ymargin; 'ymap='ymap


xs = xleft + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid


'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'

#say 'mod='mod'   nmap='nmap'   xmap='xmap'   ymap='ymap

'set xlevs 100 500 1000';'set ylint 50'

'set t 'nmap

'q dims'; TEMP=sublin(result,5); DTIME=subwrd(TEMP,6)

 'd z'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# TEXT
x=xl+0.2 ; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' 'DTIME


if ( mod = 0 )
xmap=1
ymap=ymap+1
else
xmap=xmap+1
endif

nmap=nmap+1

endwhile #nmap



# HEADER
'set strsiz 0.1 0.12'
'set string 1 l 3 0'
xx = 0.2
yy=9.8
'draw string ' xx ' ' yy ' $LAT $LON'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

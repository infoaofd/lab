#/bin/bash

INDIR=/work02/DATA/ERA5.KOIKE.LARGE/2DAY.MEAN
YS=1959; YE=2022
MM=02; DS=14; DE=15

ODIR=/work02/DATA/ERA5.KOIKE.LARGE/CLIM.MEAN
mkd $ODIR

INLIST=$(ls $INDIR/ERA5_SLP_2DAY.MEAN_????_${MM}_${DS}-${DE}.nc)

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo INPUT:
for IN in $INLIST; do
if [ ! -f $IN ];then NO SUCH FILE, $IN; exit 1;fi
echo $IN
done 
echo

OFLE=ERA5_SD.CLIM_${YS}-${YE}_${MM}_${DS}-${DE}.nc
OUT=$ODIR/$OFLE

<<COMMENT
http://kodama.fubuki.info/wiki/wiki.cgi/CDO/tips?lang=jp#11
Tips: 複数ファイルの時間平均
cdo mergetime hoge*.nc hoge_merge.nc
cdo timmean hoge_merge.nc hoge_mean.nc
パイプラインでは記述できないので注意。
COMMENT

MERGE=merge.nc #TEMPORALLY FILE
rm -vf $MERGE
cdo -f nc4 mergetime $INLIST $MERGE
cdo -f nc4 timstd $MERGE $OUT
echo
cdo sinfo $OUT


echo OUTPUT: $OUT
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo
echo
echo

rm -vf $MERGE

#!/bin/bash

# Mon, 12 Dec 2022 13:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.SNOW_KOIKE/42.12.MAP.TSK/22.12.SNAPSHOT

DSET=ERA5; VAR=var151; VAROUT=SLP
YY=2014;YS=1959;YE=2022;Y=$YS;MM=02;DS=10;DE=20

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

INDIR=/work02/DATA/ERA5.KOIKE.LARGE/1DAY.MEAN.EACH.YEAR/

INFLE_PREFIX=ERA5_SLP_1DAY.MEAN

while [ $Y -le $YE ];do
INFLE=${INFLE_PREFIX}_${Y}_${MM}_${DS}-${DE}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1; fi
Y=$(expr $Y + 1)
done 

CTLDIR=CTL; mkd $CTLDIR
Y=$YS
while [ $Y -le $YE ];do
CTL=${CTLDIR}/${Y}.CTL
cat <<EOF >$CTL
dset ${INDIR}/ERA5_SLP_1DAY.MEAN_${Y}_${MM}_${DS}-${DE}.nc
title ERA5 SFC DAILY MEAN
undef -9e+33
dtype netcdf
options yrev
xdef 361 linear 90 0.25
ydef 241 linear 20 0.25
zdef 1 linear 0 1
tdef 11 linear 00Z${DS}${MMM}${YY} 1DY
vars 17
var235=>var235  0  t,y,x  var235
var34=>var34  0  t,y,x  var34
var165=>var165  0  t,y,x  var165
var166=>var166  0  t,y,x  var166
var168=>var168  0  t,y,x  var168
var167=>var167  0  t,y,x  var167
var218=>var218  0  t,y,x  var218
var239=>var239  0  t,y,x  var239
var219=>var219  0  t,y,x  var219
var240=>var240  0  t,y,x  var240
var151=>var151  0  t,y,x  var151
var34_2=>var34_2  0  t,y,x  var34_2
var38=>var38  0  t,y,x  var38
var37=>var37  0  t,y,x  var37
var33=>var33  0  t,y,x  var33
var141=>var141  0  t,y,x  var141
var144=>var144  0  t,y,x  var144
endvars
EOF
Y=$(expr $Y + 1)
done 

GS=$(basename $0 .sh).GS

LONW=145 ;LONE=180 ; LATS=40 ;LATN=50
FIG=ERA5_SLP_1DAY.MEAN_${YS}-${YE}_${MM}_${DS}-${DE}_${LONW}-${LONE}_${LATS}-${LATN}.pdf

UNIT=hPa

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

ytop=9

xmax = 1; ymax = 1
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

nmap = 1; xmap = 1; ymap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'

ys=${YS}; ye=${YE}; year=${YS}

while (year <= ye)
CTL='${CTLDIR}/'year'.CTL'
say 'CTL='CTL

'open 'CTL

'set lon ${LONW}'; 'set lat ${LATS}'
'set time 00Z${DS}${MMM}${YY} 00Z${DE}${MMM}${YY}'

'AAV=0'
'AAV=tloop(aave(${VAR},lon=${LONW},lon=${LONE},lat=${LATS},lat=${LATN}))'

'set tlsupp year'
'set cmark 0'

'set vrange 970 1040'
if (yaer != $YY)
'set ccolor 1';'set cthick 1'
'set xlab off';'set ylab off'
'd $VAR/100.'
endif

#'q dims';say result

'allclose'
year=year+1; nmap=nmap+1
say
endwhile


CTL='${CTLDIR}/${YY}.CTL'
say 'CTL='CTL

'open 'CTL
'set lon ${LONW}'; 'set lat ${LATS}'
'set time 00Z${DS}${MMM}${YY} 00Z${DE}${MMM}${YY}'
'AAV=0'
'AAV=tloop(aave(${VAR},lon=${LONW},lon=${LONE},lat=${LATS},lat=${LATN}))'

'set vrange 970 1040'

'set xlab on';'set ylab on'
'set ylint 10'
'set cmark 0'
'set ccolor 2';'set cthick 10'
'd $VAR/100.'

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=xl-0.8; y=(yb+yt)/2
'set strsiz 0.12 0.15'; 'set string 1 c 4 90'
'draw string 'x' 'y' $VAROUT [${UNIT}]' 

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${DSET} ${VAROUT} ${LATS}N-${LATN}N ${LONW}E-${LONE}E'

# HEADER
'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'


'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
#rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

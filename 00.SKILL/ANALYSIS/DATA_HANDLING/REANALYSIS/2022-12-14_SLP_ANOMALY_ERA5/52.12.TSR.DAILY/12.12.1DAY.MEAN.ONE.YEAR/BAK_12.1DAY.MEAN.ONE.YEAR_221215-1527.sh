#!/bin/bash

INDIR_ROOT=/work02/DATA/ERA5.KOIKE.LARGE
Y=$1
Y=${Y:-2014}
MM=02; DS=10; DE=20

ODIR=/work02/DATA/ERA5.KOIKE.LARGE/1DAY.MEAN.EACH.YEAR
mkd $ODIR
OFLE=ERA5_SLP_1DAY.MEAN_${Y}_${MM}_${DS}-${DE}.nc
OUT=$ODIR/$OFLE
rm -vf $OUT


INDIR=$INDIR_ROOT/$Y 
INLIST=$(ls $INDIR/ERA5*${Y}*.grib)

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo INPUT:

for IN in $INLIST; do
if [ ! -f $IN ];then NO SUCH FILE, $IN; exit 1;fi
echo $IN
done 
echo

<<COMMENT
http://kodama.fubuki.info/wiki/wiki.cgi/CDO/tips?lang=jp#11
Tips: 複数ファイルの時間平均
cdo mergetime hoge*.nc hoge_merge.nc
cdo timmean hoge_merge.nc hoge_mean.nc
パイプラインでは記述できないので注意。
COMMENT

MERGE=merge.nc
rm -vf $MERGE
cdo -f nc4 mergetime $INLIST $MERGE
cdo -f nc4 -daymean $MERGE $OUT
rm -vf $MERGE

echo
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo OUTPUT: $OUT
cdo sinfo $OUT
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo
echo



rm -vf $MERGE

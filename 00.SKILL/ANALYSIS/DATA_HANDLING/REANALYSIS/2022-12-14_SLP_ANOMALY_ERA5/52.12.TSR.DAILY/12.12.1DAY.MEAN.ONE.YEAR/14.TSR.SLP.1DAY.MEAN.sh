#!/bin/bash

# Mon, 12 Dec 2022 13:40:20 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.SNOW_KOIKE/42.12.MAP.TSK/22.12.SNAPSHOT

DSET=ERA5; VAR=var151; VAROUT=SLP
Y=2014;YS=1959;YE=2022;MM=02;DS=10;DE=20

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

INDIR=/work02/DATA/ERA5.KOIKE.LARGE/1DAY.MEAN.EACH.YEAR/
INFLE=ERA5_SLP_1DAY.MEAN_${Y}_${MM}_${DS}-${DE}.nc

IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1; fi

TIME="00Z${DS}${MMM}${Y} 00Z${DE}${MMM}${Y}"
GS=$(basename $0 .sh).GS

LONW=145 ;LONE=180 ; LATS=40 ;LATN=50
FIG=ERA5_SLP_1DAY.MEAN_${Y}_${LONW}-${LONE}_${LATS}-${LATN}.pdf

UNIT=hPa

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'

'set lon ${LONW}'; 'set lat ${LATS}'
'set time ${TIME}'

'AAV=tloop(aave(${VAR},lon=${LONW},lon=${LONE},lat=${LATS},lat=${LATN}))'

'set cmark 0'
'd $VAR/100.'

#'q dims';say result
'set xlab off';'set ylab off'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=xl-1; y=(yb+yt)/2
'set strsiz 0.12 0.15'; 'set string 1 c 3 90'
'draw string 'x' 'y' $VAROUT [${UNIT}]' 

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${DSET} ${VAROUT} ${LATS}-${LATN} ${LONW}-${LONE}'

# HEADER
'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = 0.2; yy=yt+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

INDIR=/work01/DATA/NCEP2/MON/PRS
INFLE=hgt.mon.mean.nc
IN=$INDIR/$INFLE

GS=$(basename $0 .sh).GS

cat <<END>$GS
'sdfopen $IN'
'q ctlinfo';say result

'set time 01OCT1982'
'q dims'; say result
say

'set time 01OCT2011'
'q dims'; say result
say

'quit'
END

grads -bcp $GS
rm -vf $GS


# 0NCEP2_DAILY_ANOMALY.md
[[_TOC_]]

## 22.12.MAKE_1LAYER.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

print("MMMMM HGT")
VAR="hgt"
LEV=300

INDIR="/work01/DATA/NCEP2/6HR/1981-2023/1981-2017"
INLST=systemfunc("ls "+INDIR+"/"+VAR+"*.nc")  

a=addfiles(INLST,"r")
hgt=a[:]->hgt(:,{LEV},:,:)
lon=a[0]->lon
lat=a[0]->lat

printVarSummary(hgt)

OFLE="HGT_1981-2017_"+LEV+".nc"
system("rm -v "+OFLE)
f=addfile(OFLE,"c")
f->hgt=hgt

hgt&lon=lon
hgt&lat=lat

print("MMMMM OUTPUT: "+OFLE)

delete([/INLST,OFLE,a,f/])





end

```



## 22.14.MAKE_1LAYER.AIR.ncl
```bash
print("MMMMM air")
VAR="air"
LEV=925

INDIR="/work01/DATA/NCEP2/6HR/1981-2023/1981-2017"
INLST=systemfunc("ls "+INDIR+"/"+VAR+"*.nc")  

a=addfiles(INLST,"r")
print(a)

air=a[:]->air(:,{LEV},:,:)
;printVarSummary(hgt)

OFLE="AIR_1981-2017_"+LEV+".nc"
system("rm -v "+OFLE)

f=addfile(OFLE,"c")
f->air=air

air&lon=lon
air&lat=lat

print("MMMMM OUTPUT: "+OFLE)

delete([/INLST,OFLE,a,f/])

```



## 26.12.MAKE_DAYCLIM.HGT.200.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

print("MMMMM HGT")
VAR="hgt"
LEV=200

INFLE="HGT_1981-2017_"+LEV+".nc"
OFLE="HGT_1981-2017_ClmSmo"+LEV+".nc"

a=addfile(INFLE,"r")
hgt=a->hgt
lon=a->lon
lat=a->lat
time= a->time                     
 
hgt&lon=lon
hgt&lat=lat
hgt&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

print("MMMMM DAILY CLIM")
ClmDay    = clmDayTLL(hgt, yyyyddd)     ; daily climatology at each grid point                                      
printVarSummary(ClmDay)   

print("MMMMM RUNNING MEAN")
run=15
opt_runave_n=-15
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
ClmSmo=runave_n(ClmDay, run, opt_runave_n, dim)
copy_VarCoords(ClmDay,ClmSmo)
printVarSummary(ClmSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->hClmSmo=ClmSmo



print("MMMMM PLOT")

CLON=140 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=ClmSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_ClmDay_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Daily Clim"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 11500.; ;12330.
res@cnMaxLevelValF = 13000.; 12410.
res@cnLevelSpacingF =  100.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)

print("MMMMM OUTPUT: "+OFLE)
print("MMMMM FIG   : "+FIG)


end

```



## 26.22.MAKE_DAYCLIM.HGT.300.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="hgt"
LEV=300
print("MMMMM HGT "+LEV)

INFLE="HGT_1981-2017_"+LEV+".nc"
OFLE="HGT_1981-2017_ClmSmo"+LEV+".nc"

a=addfile(INFLE,"r")
hgt=a->hgt
lon=a->lon
lat=a->lat
time= a->time                     
 
hgt&lon=lon
hgt&lat=lat
hgt&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

print("MMMMM DAILY CLIM")
ClmDay    = clmDayTLL(hgt, yyyyddd)     ; daily climatology at each grid point                                      
printVarSummary(ClmDay)   

print("MMMMM RUNNING MEAN")
run=15
opt_runave_n=-15
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
ClmSmo=runave_n(ClmDay, run, opt_runave_n, dim)
copy_VarCoords(ClmDay,ClmSmo)
printVarSummary(ClmSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->hClmSmo=ClmSmo



print("MMMMM PLOT")

CLON=140 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=ClmSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_ClmDay_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Daily Clim"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 9000.; ;12330.
res@cnMaxLevelValF = 10000.; 12410.
res@cnLevelSpacingF =  50.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)

print("MMMMM OUTPUT: "+OFLE)
print("MMMMM FIG   : "+FIG)


end

```



## 26.32.MAKE_DAYCLIM_AIR.925.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="air"
LEV=925
print("MMMMM AIR "+LEV)

INFLE="AIR_1981-2017_"+LEV+".nc"
OFLE="AIR_1981-2017_ClmSmo"+LEV+".nc"

a=addfile(INFLE,"r")
air=a->air
lon=a->lon
lat=a->lat
time= a->time                     
 
air&lon=lon
air&lat=lat
air&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

print("MMMMM DAILY CLIM")
ClmDay    = clmDayTLL(air, yyyyddd)     ; daily climatology at each grid point                                      
printVarSummary(ClmDay)   

print("MMMMM RUNNING MEAN")
run=15
opt_runave_n=-15
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
ClmSmo=runave_n(ClmDay, run, opt_runave_n, dim)
copy_VarCoords(ClmDay,ClmSmo)
printVarSummary(ClmSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->airClmSmo=ClmSmo



print("MMMMM PLOT")

CLON=140 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=ClmSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_ClmDay_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Daily Clim"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 270.; ;12330.
res@cnMaxLevelValF = 300.; 12410.
res@cnLevelSpacingF =  2.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)



print("MMMMM OUTPUT: "+OFLE)

delete([/INFLE,OFLE,a,f/])


end

```



## 28.12.AIR_1LAYER.AIR_TARGET_YEAR.ncl
```bash
print("MMMMM air")
VAR="air"
LEV=925

TYEAR=2018
INDIR="/work01/DATA/NCEP2/6HR/1981-2023/2018-2023"
INLST=systemfunc("ls "+INDIR+"/"+VAR+"*"+TYEAR+"*.nc")  

a=addfiles(INLST,"r")
print(a)

air=a[:]->air(:,{LEV},:,:)
lon=a[0]->lon
lat=a[0]->lat
;printVarSummary(hgt)

OFLE="AIR_"+TYEAR+"_"+LEV+".nc"
system("rm -v "+OFLE)

f=addfile(OFLE,"c")
f->air=air

air&lon=lon
air&lat=lat

print("MMMMM OUTPUT: "+OFLE)

delete([/INLST,OFLE,a,f/])

```



## 28.32.AIR925_SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="air"
LEV=925
print("MMMMM AIR "+LEV)

INFLE="AIR_2018_"+LEV+"_DAILY.nc"
OFLE="AIR_2018_"+LEV+"_DAILY_SMO5DAY.nc"

a=addfile(INFLE,"r")
air=a->air
lon=a->lon
lat=a->lat
time= a->time                     
 
air&lon=lon
air&lat=lat
air&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

run=5
print("MMMMM "+run+"-DAY RUNNING MEAN")
opt_runave_n=0
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
airSmo=runave_n(air, run, opt_runave_n, dim)
copy_VarCoords(air,airSmo)
printVarSummary(airSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->airSmo=airSmo



print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.4i", yearC)+"-"+sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=airSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_Smoothd_Daily_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Smoothed Daily"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 270.
res@cnMaxLevelValF = 300.
res@cnLevelSpacingF =  2.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)



print("MMMMM OUTPUT: "+OFLE)
print("MMMMM FIG:    "+FIG)

delete([/INFLE,OFLE,a,f/])


end

```



## 28.42.AIR925_AnoSmo.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="air"
LEV=925
print("MMMMM AIR "+LEV)

yearI=2018
monthI=07
dayI=20

INFLE1="AIR_2018_"+LEV+"_DAILY_SMO5DAY.nc"
INFLE2="AIR_1981-2017_ClmSmo"+LEV+".nc"

a1=addfile(INFLE1,"r")
air1=a1->airSmo
lon=a1->lon
lat=a1->lat
time1= a1->time                     

air1&lon=lon
air1&lat=lat
air1&time=time1



a2=addfile(INFLE2,"r")
air2=a2->airClmSmo

air2&lon=lon
air2&lat=lat

print("MMMMM ANOMALY")
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

VOUT=air1(doyI,:,:)-air2(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)


print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

date_str = sprinti("%0.4i", yearI)+"-"+sprinti("%0.2i", monthI)+"-"+sprinti("%0.2i", dayI)

print("PLOT DATE="+date_str) 

VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_AnoSmo_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Daily Anom"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = -5.; ;12330.
res@cnMaxLevelValF = 5.; 12410.
res@cnLevelSpacingF =  0.5
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)

print("MMMMM FIG:    "+FIG)

end

```



## 30.12.HGT200_1LAYER_TARGET_YEAR.ncl
```bash
VAR="hgt"
LEV=200
print("MMMMM HGT "+LEV)

TYEAR=2018
INDIR="/work01/DATA/NCEP2/6HR/1981-2023/2018-2023"
INLST=systemfunc("ls "+INDIR+"/"+VAR+"*"+TYEAR+"*.nc")  

a=addfiles(INLST,"r")
print(a)

hgt=a[:]->hgt(:,{LEV},:,:)
lon=a[0]->lon
lat=a[0]->lat
;printVarSummary(hgt)

OFLE=VAR+"_"+TYEAR+"_"+LEV+".nc"
system("rm -vf "+OFLE)

f=addfile(OFLE,"c")
f->hgt=hgt

hgt&lon=lon
hgt&lat=lat

print("MMMMM OUTPUT: "+OFLE)

delete([/INLST,OFLE,a,f/])

```



## 30.14.HGT300_1LAYER_TARGET_YEAR.ncl
```bash
VAR="hgt"
LEV=300
print("MMMMM HGT "+LEV)

TYEAR=2018
INDIR="/work01/DATA/NCEP2/6HR/1981-2023/2018-2023"
INLST=systemfunc("ls "+INDIR+"/"+VAR+"*"+TYEAR+"*.nc")  

a=addfiles(INLST,"r")
print(a)

hgt=a[:]->hgt(:,{LEV},:,:)
lon=a[0]->lon
lat=a[0]->lat
;printVarSummary(hgt)

OFLE=VAR+"_"+TYEAR+"_"+LEV+".nc"
system("rm -vf "+OFLE)

f=addfile(OFLE,"c")
f->hgt=hgt

hgt&lon=lon
hgt&lat=lat

print("MMMMM OUTPUT: "+OFLE)

delete([/INLST,OFLE,a,f/])

```



## 30.16.UV200_1LAYER_TARGET_YEAR.ncl
```bash
VAR1="uwnd"
VAR2="vwnd"
LEV=200
print("MMMMM UV "+LEV)

TYEAR=2018
INDIR="/work01/DATA/NCEP2/6HR/1981-2023"
INLST1=systemfunc("ls "+INDIR+"/"+VAR1+"*"+TYEAR+"*.nc")  
INLST2=systemfunc("ls "+INDIR+"/"+VAR2+"*"+TYEAR+"*.nc")  

a1=addfiles(INLST1,"r")
a2=addfiles(INLST2,"r")
print(a1)

uwnd=a1[:]->uwnd(:,{LEV},:,:)
lon1=a1[0]->lon
lat1=a1[0]->lat
vwnd=a2[:]->vwnd(:,{LEV},:,:)
lon2=a2[0]->lon
lat2=a2[0]->lat

;printVarSummary(hgt)

OFLE=VAR1+"_"+TYEAR+"_"+LEV+".nc"
system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->uwnd=uwnd
delete([/f/])
print("MMMMM OUTPUT: "+OFLE)

OFLE=VAR2+"_"+TYEAR+"_"+LEV+".nc"
system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->vwnd=vwnd
delete([/f/])
print("MMMMM OUTPUT: "+OFLE)




```



## 30.18.UV200_1LAYER_DAYMEAN.sh
```bash
INFLE=uwnd_2018_200.nc
OFLE=uwnd_2018_200_DAILY.nc
cdo  daymean $INFLE $OFLE
if [ $? -eq 0 -a -f $OFLE ];then echo $OFLE; echo;fi
 
INFLE=vwnd_2018_200.nc
OFLE=vwnd_2018_200_DAILY.nc
cdo  daymean $INFLE $OFLE
if [ $? -eq 0 -a -f $OFLE ];then echo $OFLE; echo;fi
```



## 30.32.HGT200.SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="hgt"
LEV=200
print("MMMMM HGT "+LEV)

INFLE="hgt_2018_"+LEV+"_DAILY.nc"
OFLE="hgt_2018_"+LEV+"_DAILY_SMO5DAY.nc"

a=addfile(INFLE,"r")
hgt=a->hgt
lon=a->lon
lat=a->lat
time= a->time                     
 
hgt&lon=lon
hgt&lat=lat
hgt&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

run=5
print("MMMMM "+run+"-DAY RUNNING MEAN")
opt_runave_n=0
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
hgtSmo=runave_n(hgt, run, opt_runave_n, dim)
copy_VarCoords(hgt,hgtSmo)
printVarSummary(hgtSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->hgtSmo=hgtSmo



print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.4i", yearC)+"-"+sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=hgtSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_Smoothd_Daily_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Smoothed Daily"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 11500.; ;12330.
res@cnMaxLevelValF = 13000.; 12410.
res@cnLevelSpacingF =  100.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)



print("MMMMM OUTPUT: "+OFLE)
print("MMMMM FIG:    "+FIG)

delete([/INFLE,OFLE,a,f/])


end

```



## 30.34.HGT300.SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="hgt"
LEV=300
print("MMMMM HGT "+LEV)

INFLE="hgt_2018_"+LEV+"_DAILY.nc"
OFLE="hgt_2018_"+LEV+"_DAILY_SMO5DAY.nc"

a=addfile(INFLE,"r")
hgt=a->hgt
lon=a->lon
lat=a->lat
time= a->time                     
 
hgt&lon=lon
hgt&lat=lat
hgt&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

run=5
print("MMMMM "+run+"-DAY RUNNING MEAN")
opt_runave_n=0
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
hgtSmo=runave_n(hgt, run, opt_runave_n, dim)
copy_VarCoords(hgt,hgtSmo)
printVarSummary(hgtSmo)   

system("rm -vf "+OFLE)
f=addfile(OFLE,"c")
f->hgtSmo=hgtSmo



print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=23
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.4i", yearC)+"-"+sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

VOUT=hgtSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_Smoothd_Daily_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True

res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@tiMainString    = LEV+"hPa "+date_str+" Smoothed Daily"
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;sres@mpLandFillColor = 1
res@cnFillOn             = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 9000.; ;12330.
res@cnMaxLevelValF = 10000.; 12410.
res@cnLevelSpacingF =  50.
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "white"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)

;overlay(plot,plot1)
draw(plot)

frame(wks)



print("MMMMM OUTPUT: "+OFLE)
print("MMMMM FIG:    "+FIG)

delete([/INFLE,OFLE,a,f/])


end

```



## 30.36.UV200.SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="U"
LEV=200
print("MMMMM "+VAR+LEV)

INFLE1="uwnd_2018_"+LEV+"_DAILY.nc"
INFLE2="vwnd_2018_"+LEV+"_DAILY.nc"
INFLE1="uwnd_2018_"+LEV+"_DAILY.nc"
INFLE2="vwnd_2018_"+LEV+"_DAILY.nc"
OFLE1="uwnd_2018_"+LEV+"_DAILY_SMO5DAY.nc"
OFLE2="vwnd_2018_"+LEV+"_DAILY_SMO5DAY.nc"

a1=addfile(INFLE1,"r")
uwnd=a1->uwnd
lon=a1->lon
lat=a1->lat
time= a1->time                     
a2=addfile(INFLE2,"r")
vwnd=a2->vwnd
 
uwnd&lon=lon
uwnd&lat=lat
uwnd&time=time
vwnd&lon=lon
vwnd&lat=lat
vwnd&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

run=5
print("MMMMM "+run+"-DAY RUNNING MEAN")
opt_runave_n=0
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
uSmo=runave_n(uwnd, run, opt_runave_n, dim)
copy_VarCoords(uwnd,uSmo)
printVarSummary(uSmo)   
uSmo=runave_n(uwnd, run, opt_runave_n, dim)
vSmo=runave_n(vwnd, run, opt_runave_n, dim)

system("rm -vf "+OFLE1)
f1=addfile(OFLE1,"c")
f1->uSmo=uSmo
system("rm -vf "+OFLE2)
f2=addfile(OFLE2,"c")
f2->vSmo=vSmo



print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=20
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.4i", yearC)+"-"+sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

UOUT=uSmo(doyI,:,:)
UOUT!0="lat"
UOUT!1="lon"
UOUT&lat=lat
UOUT&lon=lon
VOUT=vSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
spd = sqrt(UOUT^2+VOUT^2)
copy_VarMeta(UOUT,spd)

FIG="UVsmo5day_"+LEV+"_"+yearI+monthI+dayI
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)
;cmap = read_colormap_file("sunshine_9lev")
gsn_define_colormap(wks,"sunshine_9lev")
res = True       ; plot mods desired
res@gsnFrame                = False
res@mpMinLatF = 0     ; 緯度の最小値
res@mpMaxLatF = 90     ; 緯度の最大値
res@mpCenterLonF = 120  ; 経度の中心
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@tiMainString    = "UV "+LEV+" "+yearI+monthI+dayI
res@gsnDraw         = False
res@gsnFrame        = False
;res@cnInfoLabelOrthogonalPosF = 0.15  ; move info label down
res@cnLinesOn = False
res@cnFillOn = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF  = 10.			; set the minimum contour level
res@cnMaxLevelValF  = 50.			; set the maximum contour level
res@cnLevelSpacingF      =  5.
plot = gsn_csm_contour_map(wks,spd,res)

vcres=True
vcres@gsnDraw         = False
vcres@gsnFrame        = False
vcres@gsnLeftString   = ""
vcres@gsnRightString  = ""
vcres@gsnCenterString = ""
vcres@vcRefAnnoOrthogonalPosF = -1.35   
vcres@vcRefMagnitudeF         = 100.0  
vcres@vcRefLengthF            = 0.045
vcres@vcGlyphStyle            = "FillArrow" 
vcres@vcMinDistanceF          = 0.017
vcres@vcFillArrowWidthF         = 0.05
vcres@vcFillArrowHeadXF         = 0.3      
;; 頭の長さ(外側)。ベクトルの長さに対する比で，0～2の範囲で与える。
; この比はベクトルの長さに対して不変である。
vcres@vcFillArrowHeadYF         = 0.2     
;; 頭の幅。ベクトルの長さに対する比で，0～1の範囲で与える。

vector = gsn_csm_vector(wks,UOUT,VOUT,vcres)

overlay(plot,vector)
draw(plot)
frame(wks)

frame(wks)



print("MMMMM OUTPUT: "+OFLE1)
print("MMMMM OUTPUT: "+OFLE2)
print("MMMMM FIG:    "+FIG)

end

```



## 32.12.Air925AnoSmo_HGT200_300Smo.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="Tano925_HGTsmo200_300"

yearI=2018
monthI=07
dayI=23
LEV=925

INFLE1="AIR_2018_"+LEV+"_DAILY_SMO5DAY.nc"
INFLE2="AIR_1981-2017_ClmSmo"+LEV+".nc"
INFLE3="hgt_2018_300_DAILY_SMO5DAY.nc"
INFLE4="HGT_1981-2017_ClmSmo300.nc"
INFLE5="hgt_2018_200_DAILY_SMO5DAY.nc"
INFLE6="HGT_1981-2017_ClmSmo200.nc"


a1=addfile(INFLE1,"r")
air1=a1->airSmo
lon=a1->lon
lat=a1->lat
time1= a1->time                     

air1&lon=lon
air1&lat=lat
air1&time=time1



a2=addfile(INFLE2,"r")
air2=a2->airClmSmo

air2&lon=lon
air2&lat=lat



a3=addfile(INFLE3,"r")
hgt3=a3->hgtSmo

a4=addfile(INFLE4,"r")
hgt4=a4->hClmSmo

a5=addfile(INFLE5,"r")
hgt5=a5->hgtSmo

a6=addfile(INFLE6,"r")
hgt6=a6->hClmSmo

print("MMMMM AIR ANOMALY")
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

VOUT=air1(doyI,:,:)-air2(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)


print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

date_str = sprinti("%0.4i", yearI)+"-"+sprinti("%0.2i", monthI)+"-"+sprinti("%0.2i", dayI)

print("PLOT DATE="+date_str) 

VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

opt = True       ; plot mods desired
opt@gsnDraw      =  False 
opt@gsnFrame     =  False
opt@gsnFrame                = False   
;opt@gsnMaximize = True



print("MMMMM AIR POSITIVE VALUE")
res=opt
res@mpProjection               = "Satellite"
res@mpCenterLonF               = CLON 
res@mpCenterLatF               = CLAT
res@mpSatelliteDistF           = 3.0 
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@mpGeophysicalLineColor       = "black"  ; 地図の線の色
res@mpGeophysicalLineThicknessF  = 2        ; 地図の線の太さ
res@mpOceanFillColor       = "gray97"            ; array index in color map
res@mpLandFillColor        = "tan"          ; for land, ocean, and inland h20
res@mpInlandWaterFillColor = "gray97"

res@tiMainString    = date_str
res@gsnLeftString    = ""
res@gsnCenterString    = ""
res@gsnRightString    = ""

;res@mpLandFillColor = 
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 2.; ;12330.
res@cnMaxLevelValF = 10.; 12410.
res@cnLevelSpacingF =  1
res@cnLineThicknessF = 2
res@cnLabelDrawOrder           = "PostDraw"
res@cnLineLabelFontColor="red"
res@cnLineLabelBackgroundColor="white"
res@cnLineColor = "red"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot = gsn_csm_contour_map(wks,VOUT,res)
delete(res)


print("MMMMM AIR NEGATIVE VALUE")
res=opt
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = -10; ;12330.
res@cnMaxLevelValF =  -2.; 12410.
res@cnLevelSpacingF =  1
res@cnLineThicknessF = 2
res@cnLineThicknessF = 2
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineLabelFontColor="blue"
res@cnLineColor = "blue"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot2 = gsn_csm_contour(wks,VOUT,res)
overlay(plot,plot2)



print("MMMMM hgtSmo 300")
delete([/VOUT,res/])
VOUT=hgt3(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)
res=opt
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 9250.
res@cnMaxLevelValF = 9330.
res@cnLevelSpacingF =  40.
res@cnLineLabelsOn = False
res@cnLineThicknessF = 3
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "green"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot3 = gsn_csm_contour(wks,VOUT,res)
overlay(plot,plot3)






print("MMMMM hgtClmSmo 300")
delete([/VOUT,res/])
VOUT=hgt4(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)
res=opt
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 9250.
res@cnMaxLevelValF = 9330.
res@cnLevelSpacingF =  40.
res@cnLineLabelsOn = False
res@cnLineThicknessF = 2
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "black"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot4 = gsn_csm_contour(wks,VOUT,res)
overlay(plot,plot4)



print("MMMMM hgtSmo 200")
delete([/VOUT,res/])
VOUT=hgt5(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)
res=opt
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 12330.
res@cnMaxLevelValF = 12410.
res@cnLevelSpacingF =  40.
res@cnLineLabelsOn = False
res@cnLineThicknessF = 3
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "orange"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot3 = gsn_csm_contour(wks,VOUT,res)
overlay(plot,plot3)



print("MMMMM hgtClmSmo 200")
delete([/VOUT,res/])
VOUT=hgt6(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)
res=opt
res@cnFillOn             = False
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 12330.
res@cnMaxLevelValF = 12410.
res@cnLevelSpacingF =  40.
res@cnLineLabelsOn = False
res@cnLineThicknessF = 2
res@cnLabelDrawOrder           = "PostDraw" 
res@cnLineColor = "black"
res@gsnLeftString   = ""
res@gsnCenterString = ""
res@gsnRightString  = ""
res@lbOrientation = "vertical"
res@pmLabelBarWidthF=0.05
plot6 = gsn_csm_contour(wks,VOUT,res)
overlay(plot,plot6)

draw(plot)


frame(wks)

print("MMMMM FIG:    "+FIG)

end

```



## 40.12.HGT200A.SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="hgt"
LEV=200
print("MMMMM HGT "+LEV)

yearI=2018
monthI=07
dayI=20

INFLE1="hgt_2018_"+LEV+"_DAILY_SMO5DAY.nc"
INFLE2="HGT_1981-2017_ClmSmo"+LEV+".nc"

a1=addfile(INFLE1,"r")
hgt1=a1->hgtSmo
lon=a1->lon
lat=a1->lat
time1= a1->time   

hgt1&lon=lon
hgt1&lat=lat
hgt1&time=time1

a2=addfile(INFLE2,"r")
hgt2=a2->hClmSmo
hgt2&lon=lon
hgt2&lat=lat

print("MMMMM ANOMALY")
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

VOUT=hgt1(doyI,:,:)-hgt2(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

date_str = sprinti("%0.4i", yearI)+"-"+sprinti("%0.2i", monthI)+"-"+sprinti("%0.2i", dayI)

print("PLOT DATE="+date_str) 

VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_AnoSmo_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)
gsn_define_colormap(wks,"BlueDarkRed18")
res = True
res@mpMinLatF = 0     ; 緯度の最小値
res@mpMaxLatF = 90     ; 緯度の最大値
res@mpCenterLonF = 120  ; 経度の中心
res@tiMainString = VAR+" anomaly "+LEV+"hPa "+date_str
res@gsnMaximize = True ; maxmize plot in frame
; add resources properties to get filled contour
res@cnFillOn = True ;-- turn off contour fill
res@cnLinesOn = False ;-- turn off contour lines
res@cnLineLabelsOn = False ;-- turn off line labels
res@cnLevelSelectionMode = "ManualLevels" ;-- set contour levels manually
res@cnMinLevelValF = -300. ;-- minimum contour level
res@cnMaxLevelValF = 300. ;-- maximum contour level
res@cnLevelSpacingF = 20. ;-- contour level spacing
;res@lbLabelStride = 4
res@lbBoxMinorExtentF = 0.15 ;-- decrease the height of the
;-- labelbar
res@tiMainFontHeightF = 0.02
plot = gsn_csm_contour_map(wks, VOUT, res)

;frame(wks)

print("MMMMM FIG:    "+FIG)

end

```



## 40.14.HGT300A.SMO5DAY.ncl
```bash
begin

setfileoption("nc","Format","LargeFile")

VAR="hgt"
LEV=300
print("MMMMM HGT "+LEV)

yearI=2018
monthI=07
dayI=20

INFLE1="hgt_2018_"+LEV+"_DAILY_SMO5DAY.nc"
INFLE2="HGT_1981-2017_ClmSmo"+LEV+".nc"

a1=addfile(INFLE1,"r")
hgt1=a1->hgtSmo
lon=a1->lon
lat=a1->lat
time1= a1->time   

hgt1&lon=lon
hgt1&lat=lat
hgt1&time=time1

a2=addfile(INFLE2,"r")
hgt2=a2->hClmSmo
hgt2&lon=lon
hgt2&lat=lat

print("MMMMM ANOMALY")
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

VOUT=hgt1(doyI,:,:)-hgt2(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

date_str = sprinti("%0.4i", yearI)+"-"+sprinti("%0.2i", monthI)+"-"+sprinti("%0.2i", dayI)

print("PLOT DATE="+date_str) 

VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
printVarSummary(VOUT)

LONLAT=tostring(CLON)+"_"+tostring(CLAT)

FIG=VAR+"_AnoSmo_"+LEV+"_"+date_str+"_"+LONLAT
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)
gsn_define_colormap(wks,"BlueDarkRed18")
res = True
res@mpMinLatF = 0     ; 緯度の最小値
res@mpMaxLatF = 90     ; 緯度の最大値
res@mpCenterLonF = 120  ; 経度の中心
res@tiMainString = VAR+" anomaly "+LEV+"hPa "+date_str
res@gsnMaximize = True ; maxmize plot in frame
; add resources properties to get filled contour
res@cnFillOn = True ;-- turn off contour fill
res@cnLinesOn = False ;-- turn off contour lines
res@cnLineLabelsOn = False ;-- turn off line labels
res@cnLevelSelectionMode = "ManualLevels" ;-- set contour levels manually
res@cnMinLevelValF = -300. ;-- minimum contour level
res@cnMaxLevelValF = 300. ;-- maximum contour level
res@cnLevelSpacingF = 20. ;-- contour level spacing
;res@lbLabelStride = 4
res@lbBoxMinorExtentF = 0.15 ;-- decrease the height of the
;-- labelbar
res@tiMainFontHeightF = 0.02
plot = gsn_csm_contour_map(wks, VOUT, res)

;frame(wks)

print("MMMMM FIG:    "+FIG)

end

```




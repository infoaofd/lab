begin

setfileoption("nc","Format","LargeFile")

VAR="U"
LEV=200
print("MMMMM "+VAR+LEV)

INFLE1="uwnd_2018_"+LEV+"_DAILY.nc"
INFLE2="vwnd_2018_"+LEV+"_DAILY.nc"
INFLE1="uwnd_2018_"+LEV+"_DAILY.nc"
INFLE2="vwnd_2018_"+LEV+"_DAILY.nc"
OFLE1="uwnd_2018_"+LEV+"_DAILY_SMO5DAY.nc"
OFLE2="vwnd_2018_"+LEV+"_DAILY_SMO5DAY.nc"

a1=addfile(INFLE1,"r")
uwnd=a1->uwnd
lon=a1->lon
lat=a1->lat
time= a1->time                     
a2=addfile(INFLE2,"r")
vwnd=a2->vwnd
 
uwnd&lon=lon
uwnd&lat=lat
uwnd&time=time
vwnd&lon=lon
vwnd&lat=lat
vwnd&time=time

TIME    = cd_calendar(time, 0)          ; type float 
year    = toint( TIME(:,0) )            ; toint strips meta data
month   = toint( TIME(:,1) )
day     = toint( TIME(:,2) ) 
                                        ; check for calendar attribute
if (isatt(TIME,"calendar")) then        ; default is gregorian
  year@calendar = TIME@calendar       
end if

ddd = day_of_year(year, month, day) 
if (isatt(year,"calendar")) then        ; default is gregorian
  ddd@calendar = year@calendar  
end if

yyyyddd = year*1000 + ddd               ; needed for input
if (isatt(ddd,"calendar")) then         ; default is gregorian
  yyyyddd@calendar = ddd@calendar  
end if

run=5
print("MMMMM "+run+"-DAY RUNNING MEAN")
opt_runave_n=0
dim=0
print("MMMMM run="+run+" opt_runave_n="+opt_runave_n+" dim="+dim)
uSmo=runave_n(uwnd, run, opt_runave_n, dim)
copy_VarCoords(uwnd,uSmo)
printVarSummary(uSmo)   
uSmo=runave_n(uwnd, run, opt_runave_n, dim)
vSmo=runave_n(vwnd, run, opt_runave_n, dim)

system("rm -vf "+OFLE1)
f1=addfile(OFLE1,"c")
f1->uSmo=uSmo
system("rm -vf "+OFLE2)
f2=addfile(OFLE2,"c")
f2->vSmo=vSmo



print("MMMMM PLOT")

CLON=120 ;220 ;270.0       ; choose center lon
CLAT=45 ;45 

yearI=2018
monthI=07
dayI=20
doyI=day_of_year(yearI,monthI,dayI)-1
doyI@units="days since "+tostring(yearI)+"-1-1"

utc_date = cd_calendar(doyI, 0)
yearC   = tointeger(utc_date(:,0))    ; Convert to integer for
monthC  = tointeger(utc_date(:,1))    ; use sprinti 
dayC    = tointeger(utc_date(:,2))
hourC   = tointeger(utc_date(:,3))

date_str = sprinti("%0.4i", yearC)+"-"+sprinti("%0.2i", monthC)+"-"+sprinti("%0.2i", dayC)

print("PLOT DATE="+date_str) 

UOUT=uSmo(doyI,:,:)
UOUT!0="lat"
UOUT!1="lon"
UOUT&lat=lat
UOUT&lon=lon
VOUT=vSmo(doyI,:,:)
VOUT!0="lat"
VOUT!1="lon"
VOUT&lat=lat
VOUT&lon=lon
spd = sqrt(UOUT^2+VOUT^2)
copy_VarMeta(UOUT,spd)

FIG="UVsmo5day_"+LEV+"_"+yearI+monthI+dayI
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)
;cmap = read_colormap_file("sunshine_9lev")
gsn_define_colormap(wks,"sunshine_9lev")
res = True       ; plot mods desired
res@gsnFrame                = False
res@mpMinLatF = 0     ; 緯度の最小値
res@mpMaxLatF = 90     ; 緯度の最大値
res@mpCenterLonF = 120  ; 経度の中心
res@mpPerimOn = False
res@mpOutlineOn  = True
res@mpGridLineDashPattern= 2 
res@tiMainString    = "UV "+LEV+" "+yearI+monthI+dayI
res@gsnDraw         = False
res@gsnFrame        = False
;res@cnInfoLabelOrthogonalPosF = 0.15  ; move info label down
res@cnLinesOn = False
res@cnFillOn = True
res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF  = 10.			; set the minimum contour level
res@cnMaxLevelValF  = 50.			; set the maximum contour level
res@cnLevelSpacingF      =  5.
plot = gsn_csm_contour_map(wks,spd,res)

vcres=True
vcres@gsnDraw         = False
vcres@gsnFrame        = False
vcres@gsnLeftString   = ""
vcres@gsnRightString  = ""
vcres@gsnCenterString = ""
vcres@vcRefAnnoOrthogonalPosF = -1.35   
vcres@vcRefMagnitudeF         = 100.0  
vcres@vcRefLengthF            = 0.045
vcres@vcGlyphStyle            = "FillArrow" 
vcres@vcMinDistanceF          = 0.017
vcres@vcFillArrowWidthF         = 0.05
vcres@vcFillArrowHeadXF         = 0.3      
;; 頭の長さ(外側)。ベクトルの長さに対する比で，0〜2の範囲で与える。
; この比はベクトルの長さに対して不変である。
vcres@vcFillArrowHeadYF         = 0.2     
;; 頭の幅。ベクトルの長さに対する比で，0〜1の範囲で与える。

vector = gsn_csm_vector(wks,UOUT,VOUT,vcres)

overlay(plot,vector)
draw(plot)
frame(wks)

frame(wks)



print("MMMMM OUTPUT: "+OFLE1)
print("MMMMM OUTPUT: "+OFLE2)
print("MMMMM FIG:    "+FIG)

end


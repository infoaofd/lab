#!/bin/bash

usage(){
  cat << EOF

  Usage $0 [-c <config_file>]

  config_file : configuration file name
EOF
}


# Default value
config_file=""
cruise="MandA2012Leg2"
stn=01
yyyy=2012
mm=06
dd=14
hh=23
lon=128
lat=28


# Decode option
flagc="false"
while getopts c: OPT; do
  case $OPT in
    "c" ) flagc="true" ; config_file="$OPTARG" ;;
     *  ) usage
  esac
done




exe=./msmstn



if [ ! -f $exe ]; then
  Error in $0 : No such file, $exe
fi

outdir=output
if [ ! -d $outdir ]; then
  mkdir -p $outdir
fi

namelist=$(basename $exe).namelist.txt



if [ -f ${config_file} ]; then
  echo "Config file: ${config_file}"
  a=()
  i=0
  while read line
  do
    a[$i]=$line
    echo ${a[$i]}

    i=$(expr $i + 1)
  done < $config_file
fi

cruise=${a[0]}
   stn=${a[1]}    
  yyyy=${a[2]}   
    mm=${a[3]}     
    dd=${a[4]}     
    hh=${a[5]}     
   lon=${a[6]}   
   lat=${a[7]}   



#lon=$(echo "scale=5; $lond + $lonm / 60.0" | bc)
#lat=$(echo "scale=5; $latd + $latm / 60.0" | bc)
#echo $lon
#echo $lat

#exit

dd1=$dd
dd2=$(expr $dd1 + 1)

cat << END > $namelist
&para
yyyy=${yyyy}
mm=${mm}
dd=${dd}
hh=${hh}
in_msmp1="/work2/kunoki/to_manda_sensei/Data/MSM/Original_data/Pressure/${mm}${dd1}.nc",
in_msmp2="/work2/kunoki/to_manda_sensei/Data/MSM/Original_data/Pressure/${mm}${dd2}.nc",
ofle="${outdir}/$(basename $exe).${cruise}.${stn}.txt",
lon=${lon}
lat=${lat}
&end
END

#in_msms1="/work2/kunoki/to_manda_sensei/Data/MSM/Original_data/Surface/${mm}${dd1}s.nc"
#in_msms2="/work2/kunoki/to_manda_sensei/Data/MSM/Original_data/Surface/${mm}${dd2}s.nc"

$exe < $namelist


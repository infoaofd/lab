#/bin/sh

write_config(){
cat << EOF > $config_file
${cruise}
${stn}
${yyyy}
${mm}
${dd}
${hh}
${lon}
${lat}
EOF
}

cruise=MandA2012Leg3


indir=/work2/kunoki/to_manda_sensei/Tools/MSM.profiles.at.Stations/output
outdir=output
prefix=msmstn



config_file=$(basename $0 .sh).config.txt
exe=msmstn.run.sh
log=$(basename $0 .sh).log
date > $log
hostname >> $log
pwd  >> $log
whoami >> $log
cat <<EOF >> $log
${cruise}
EOF


stn=01
yyyy=2012
mm=06
dd=15
hh=02
lon=129.607
lat=32.5414
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=02
yyyy=2012
mm=06
dd=15
hh=03
lon=129.654
lat=32.3677
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=03
yyyy=2012
mm=06
dd=15
hh=04
lon=129.836  
lat=32.1521
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=04
yyyy=2012
mm=06
dd=15
hh=05 
lon=129.921  
lat=32.0480
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=05
yyyy=2012
mm=06
dd=15
hh=06 
lon=130.056
lat=31.9070
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=06
yyyy=2012
mm=06
dd=15
hh=07 
lon=130.062 
lat=31.7205
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=07
yyyy=2012
mm=06
dd=15
hh=08
lon=130.050
lat=31.4864
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=08
yyyy=2012
mm=06
dd=15
hh=09
lon=130.053
lat=31.3434
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=09
yyyy=2012
mm=06
dd=15
hh=10 
lon=130.058  
lat=31.1561
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=10
yyyy=2012
mm=06
dd=15
hh=11
lon=130.073  
lat=30.9609
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=11
yyyy=2012
mm=06
dd=15
hh=12
lon=130.043
lat=30.7565
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=12
yyyy=2012
mm=06
dd=15
hh=13 
lon=129.855  
lat=30.5166
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=13
yyyy=2012
mm=06
dd=15
hh=14
lon=129.696  
lat=30.3081
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=14
yyyy=2012
mm=06
dd=15
hh=15
lon=129.653
lat=30.2938
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=15
yyyy=2012
mm=06
dd=15
hh=16
lon=129.532  
lat=30.1333
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=16
yyyy=2012
mm=06
dd=15
hh=17
lon=129.472  
lat=29.9550
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=17
yyyy=2012
mm=06
dd=15
hh=18
lon=129.473  
lat=29.7799
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=18
yyyy=2012
mm=06
dd=15
hh=20
lon=129.463  
lat=29.3908
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF


stn=19
yyyy=2012
mm=06
dd=15
hh=22
lon=129.370  
lat=29.9772
write_config
$exe -c ${config_file}
cat <<EOF >> $log
${indir}/${prefix}.${cruise}.${stn}.txt
${outdir}/${prefix}.${cruise}.${yyyy:2:2}${mm}${dd}${hh}.meter.grd
EOF

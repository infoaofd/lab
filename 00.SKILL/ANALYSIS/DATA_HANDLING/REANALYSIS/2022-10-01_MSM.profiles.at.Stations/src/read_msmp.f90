subroutine read_msmp(in_msmp, p)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:57 on 12-04-2012.

  use module_msmp_var

  include '/usr/local/include/netcdf.inc'

  character(len=*),intent(in) :: in_msmp
  type(msmp),intent(inout)::p


!  write(*,'(a)')'Subroutine: read_msmp'
!  write(*,*)''
  print *
  stat = nf_open(in_msmp, nf_nowrite, ncid) ! ファイルのopenとNetCDF ID(ncid)の取得
!  print *,'ncid= ',ncid
  if(stat == 0)then
    print '(A,A)','Open : ',in_msmp(1:lnblnk(in_msmp))
  else
    print '(A,A)','Error while opening ',in_msmp(1:lnblnk(in_msmp))
    print *,'status= ',stat
    stop
  endif

!==================
  p%var='lon'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%lon)
!  print *,'stat= ',stat



!==================
  p%var='lat'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%lat)
!  print *,'stat= ',stat
!  print *

!==================
  p%var='p'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%p)


!==================
  p%var='time'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%time)
!  print *,'stat= ',stat
!  print *


  call read_msmp_vars('z',p%z)
  call read_msmp_vars('w',p%w)
  call read_msmp_vars('u',p%u)
  call read_msmp_vars('v',p%v)
  call read_msmp_vars('temp',p%temp)
  call read_msmp_vars('rh',p%rh)

end subroutine read_msmp




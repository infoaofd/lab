!***********************************************************************
! bilinear.f90
! TASK
!  2D-bilinear interpolation
! http://imagingsolution.blog107.fc2.com/blog-entry-142.html
! http://www.library.cornell.edu/nr/bookfpdf/f3-6.pdf
!***********************************************************************
      subroutine bilinear(xa,ya, za, x, y, z)
!----------------- PARAMETERS AND COMMON VARIABLES ---------------------
!      implicit double precision(a-h,o-z)
!--------------------------- ARGUMENTS ---------------------------------
      real, intent(in) :: xa(4), ya(4), za(4)
      real, intent(in) :: x,y
      real, intent(out) :: z
!------------------------- LOCAL VARIABLES -----------------------------
      real t, u
!-----------------------------------------------------------------------
! Subscript, a indicates the input.
!
!
! For given xa(i),ya(i),za(i),x & y, z is estimated.
!
! (xa(4),ya(4),za(4))   (xa(3),ya(3),za(3))
!        +------------------+
!        |                  |
!        |    x             |
!        | (x,y,z)          |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        +------------------+
! (xa(1),ya(1),za(1))   (xa(2),ya(2),za(2))
!

      t=(x-xa(1))/(xa(2)-xa(1))
      u=(y-ya(1))/(ya(4)-ya(1))

      z =  (1.-t)*(1.-u)*za(1) &
     &   + t*(1.-u)*za(2) &
     &   + t*u*za(3) &
     &   + (1.-t)*u*za(4)

      return
      end
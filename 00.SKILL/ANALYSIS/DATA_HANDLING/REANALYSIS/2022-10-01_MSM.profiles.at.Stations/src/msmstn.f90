program msmstn
! Description:
!
! Author: AM
!
! Host: 133.45.210.165
! Directory: /work2/kunoki/to_manda_sensei/Tools/MSM.profiles.at.Stations
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 20:44 on 12-04-2012.
! 2014年  6月  2日 月曜日 15:48:02 JST
! [kunoki@aofd165 ~]$
! Interpolate MSM data in space and time for estimating the vertical
! profiles at an atmospheric souding location

  use module_main
!  use module_msms_var
  use module_msmp_var
  use module_inout

  include '/usr/local/include/netcdf.inc'

!  type(msms)::s1
!  type(msms)::s2
  type(msmp)::p1
  type(msmp)::p2
  type(msmp_vp)::vp1
  type(msmp_vp)::vp2
  type(msmp_stn)::stn
  real lon,lat
  real yyyy,mm,dd,hh

!  implicit none

!  namelist/para/yyyy, mm, dd, in_msms1, in_msms2, in_msmp1, in_msmp2, outdir, &
!&   lon, lat
  namelist/para/yyyy, mm, dd, hh, in_msmp1, in_msmp2, ofle, lon, lat


  write(*,'(a)')'Program msmstn starts.'

  read(*,nml=para)

!  print '(A,A)','Input: ',in_msms1(1:lnblnk(in_msms1))
!  print '(A,A)','Input: ',in_msms2(1:lnblnk(in_msms2))
  print '(A,A)','Input: ',in_msmp1(1:lnblnk(in_msmp1))
  print '(A,A)','Input: ',in_msmp2(1:lnblnk(in_msmp2))

!  call read_msms(in_msms1, s1)
!  call read_msms(in_msms1, s2)


  call read_msmp(in_msmp1, p1)
  call read_msmp(in_msmp2, p2)

  call interpolate_space(p1,vp1,lon,lat)
  call interpolate_space(p2,vp2,lon,lat)

  call interpolate_time(vp1,vp2,hh,stn)

  call write_stn(yyyy,mm,dd,hh,lon,lat,stn,ofle)

  write(*,'(a)')'Done program msmstn.'
  write(*,*) 
end program msmstn

subroutine read_msms(in_msms, s)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:57 on 12-04-2012.

  use module_msms_var

  include '/usr/local/include/netcdf.inc'

  character(len=*),intent(in) :: in_msms
  type(msms),intent(inout)::s


!  write(*,'(a)')'Subroutine: read_msms'
!  write(*,*)''
  print *
  stat = nf_open(in_msms, nf_nowrite, ncid) ! ファイルのopenとNetCDF ID(ncid)の取得
  if(stat == 0)then
    print '(A,A)','Open : ',in_msms(1:lnblnk(in_msms))
  else
    print '(A,A)','Error while opening ',in_msms(1:lnblnk(in_msms))
    print *,'status= ',stat
    stop
  endif


!==================
  s%var='lon'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%lon)
!  print *,'stat= ',stat



!==================
  s%var='lat'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%lat)
!  print *,'stat= ',stat
!  print *


!==================
  s%var='time'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%time)
!  print *,'stat= ',stat
!  print *

  call read_msms_vars('psea',s%psea)
  call read_msms_vars('sp',s%sp)
  call read_msms_vars('u',s%u)
  call read_msms_vars('v',s%v)
  call read_msms_vars('temp',s%temp)
  call read_msms_vars('rh',s%rh)
  call read_msms_vars('r1h',s%r1h)
  call read_msms_vars('ncld_upper',s%ncld_upper)
  call read_msms_vars('ncld_mid',s%ncld_mid)
  call read_msms_vars('ncld_low',s%ncld_low)
  call read_msms_vars('ncld',s%ncld)


end subroutine read_msms




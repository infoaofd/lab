!------------------------------------------------------------------------------
! This source code can be translated into TeX documentation using a Perl script
! called ProTex.
! ProTex is available via: 
!     http://gmao.gsfc.nasa.gov/software/protex/ .
! On-line manual of ProTex can be found at:
!     http://gmao.gsfc.nasa.gov/software/protex/doc/protex/ .
!
! Usage: protex src > out.tex
!   src: source file name(s)
!   out: output tex file name
! Type "protex -h" for help.
!
!------------------------------------------------------------------------------
!BOP
!
! !ROUTINE: print_header
!
! !DESCRIPTION: 出力ファイルに以下のヘッダ情報を印字する\\
! 作成日時 \\
! 現在作業しているディレクトリ名 \\
! ユーザー名 \\
! ホスト名 \\
! プログラム名 \\
! \\
! 使用法 \\
!   call print_header(iunit) \\
!  iunit: 出力ファイルの機番(整数) \\
!
! !INTERFACE:
subroutine print_header(iunit)
!
! !USES:
! include 
! use
!
!
! !INPUT PARAMETERS:
      integer iunit
! !OUTPUT PARAMETERS:
!
! !INPUT/OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:
      character(len=500) :: dirinfo, userinfo, hostinfo
      INTEGER date_time(8)
      CHARACTER REAL_CLOCK(3)*12

      character(len=500) :: progname,shellinfo
!
! !BUGS:  
! None known at this time.
!
! !SEE ALSO: 
!
! !SYSTEM ROUTINES: 
! None
!
! !FILES USED:  
! 
!
! !REVISION HISTORY: 
!   [Thu Aug 20 14:17:59 JST 2009]  - A. Manda - Initial version
!
! !REMARKS:
! このサブルーチンは, g77, ifortで動作することを確認している.
!
! 作成したデータが、何時、何処で、誰が、どのプログラムを使って作成した
! か記録しておくと、後々作業をし直す必要が生じたときに役に立つ.
! また、以前使ったプログラムを一部改変して利用するときなど、そのプログ
! ラムを探すのに役に立つことが多い
! !TO DO:
!
!EOP
!------------------------------------------------------------------------------
!---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
!BOC
! Get date, time, current directory, username, and hostname
! p.345 of for_lang.pdf (intel fortran user manual)
! DATE_AND_TIME is an intrisic S/R and works on most of Fotran 90 
! Compilers. g77 also supports this S/R.
      CALL DATE_AND_TIME (REAL_CLOCK (1), REAL_CLOCK (2), &
     &    REAL_CLOCK (3), date_time)
      call getenv("PWD", dirinfo)
      call getenv("USER", userinfo)

!j 使用しているシェルのチェック
!j 現在, sh, bash, csh, tcshのみに対応
      call getenv("SHELL",shellinfo)
      idxcsh=index(shellinfo,"/csh")
      idxtcsh=index(shellinfo,"/tcsh")
      idxsh=index(shellinfo,"/sh")
      idxbash=index(shellinfo,"/bash")

      if(idxcsh.ne.0.or.idxtcsh.ne.0)then
        call getenv("HOST", hostinfo) !csh, tcsh, etc.
      else if(idxsh.ne.0.or.idxbash.ne.0)then
        call getenv("HOSTNAME", hostinfo) !sh, bash, etc.
      endif

      write(iunit,'(a, &
     & i2.2,a,i2.2,a,i4.4, a,i2.2,a,i2.2,a,i2.2, a,i3.2,a)') &
     &  '# Date and time: ', &
     &  date_time(2),'/',date_time(3),'/',date_time(1), &
     &  ' at ',date_time(5),':',date_time(6),':',date_time(7), &
     &  ' ',-date_time(4)/60,':00'

      write(iunit,'(A,A)')'# hostname: ',hostinfo(1:lnblnk(hostinfo))

      write(iunit,'(A,A)')'# cwd: ',dirinfo(1:lnblnk(dirinfo))
      write(iunit,'(A,A)')'# user: ',userinfo(1:lnblnk(userinfo))

! Get program name
      call getarg( 0, progname)
      write(iunit,'(A,A)')'# program name: ',progname(1:lnblnk(progname))
      return

  end subroutine print_header
!EOC
!---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--

subroutine write_stn(yyyy,mm,dd,hh,lon,lat,stn,ofle)
! Description:
!
! Author: kunoki
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work2/kunoki/to_manda_sensei/Tools/MSM.profiles.at.Stations/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 16:48 on 06-03-2014.

  use module_msmp_var
!  implicit none
  real,intent(in)::yyyy,mm,dd,hh,lon,lat
  type(msmp_stn),intent(in)::stn
  character(len=*),intent(in)::ofle

!  write(*,'(a)')'Subroutine: write_stn'
!  write(*,*)''

  print *; print *,"write_stn: "
  print *,"Output: ",trim(ofle)

  open(20,file=ofle)
  write(20,'(A,f10.5)')'# lon= ',lon
  write(20,'(A,f10.5)')'# lat= ',lat
  write(20,'(A,f5.0)')'# yyyy= ',yyyy
  write(20,'(A,f3.0)')'# mm=   ',mm
  write(20,'(A,f3.0)')'# dd=   ',dd
  write(20,'(A,f6.2)')'# hh=   ',hh

  write(20,'(A)')'# p[hPa] z[m] u[m/s] v[m/s] temp[K] rh[%]'
  do k=1,ppres
    write(20,'(f5.0,f9.1, f7.2, f7.2, f8.3, f8.2)') &
&   stn%p(k), stn%z(k),stn%u(k),stn%v(k),stn%temp(k),stn%rh(k)
  enddo !k

  close(20)
!  write(*,'(a)')'Done subroutine write_stn.'
!  write(*,*) 
end subroutine write_stn

subroutine write_msms(yyyy,mm,dd, outdir, s, io, in_msms)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 17:05 on 12-05-2012.

!  use
!  implicit none
  use module_msms_var
  use module_inout

  integer,intent(in)::yyyy,mm,dd
  character(len=*),intent(in):: outdir
  type(msms),intent(in)::s
  type(inout),intent(in)::io
  character(len=*),intent(in)::in_msms
  integer :: hh

  character month*3, dat_msms*500

!  write(*,'(a)')'Subroutine: write_msms'
!  write(*,*)''

  time: do l=1,stime
    is=1
    ie=lnblnk(outdir)
    write(dat_msms(is:ie),'(A,A)')outdir(is:ie)
    is=ie+1
    ie=is
    write(dat_msms(is:ie),'(A,A)')'/'
    is=ie+1

    hh=l

    write(dat_msms(is:),'(A,i4.4,i2.2,i2.2,i2.2,A)')&
&  'MSMS',yyyy,mm,dd,hh,'.asc'
    print '(A,A)','Output: ',dat_msms(1:lnblnk(dat_msms))

    open(20,file=dat_msms)
    call print_header(20)
    write(20,'(A,A)')'# input: ',in_msms(1:lnblnk(in_msms))
    write(20,'(A,f11.5)')'# west= ',  s%lon(io%iw)
    write(20,'(A,f11.5)')'# east= ',  s%lon(io%ie)
    write(20,'(A,f11.5)')'# south= ', s%lat(io%js)
    write(20,'(A,f11.5)')'# north= ', s%lat(io%jn)

    write(20,'(A,A)')'# lon, lat, pesa(Pa), sp(Pa), u(m/s),&
& v(m/s), temp(K), rh(%), r1h(mm/h), ncld_upper(%), ncld_mid(%),&
& ncld_lower(%), ncld(%)'


    do j=io%js,io%jn,-1
      do i=io%iw, io%ie

        
        write(20,'(2f12.6,2f12.1,2f10.3,f10.3,f7.0,f7.0,4f7.0)')& 
&        s%lon(i),s%lat(j), &
&        s%psea(i,j,l),s%sp(i,j,l), s%u(i,j,l), s%v(i,j,l), &
&        s%temp(i,j,l),s%rh(i,j,l),s%r1h(i,j,l),s%ncld_upper(i,j,l), &
&        s%ncld_mid(i,j,l),s%ncld_low(i,j,l),s%ncld(i,j,l)

      enddo !i
    enddo !j
    close(20)
  enddo time

!  write(*,'(a)')'Done subroutine write_msms.'
!  write(*,*) 
end subroutine write_msms

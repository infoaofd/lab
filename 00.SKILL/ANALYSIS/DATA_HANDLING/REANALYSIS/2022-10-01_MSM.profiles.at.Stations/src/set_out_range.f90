subroutine set_out_range(east,west,south,north,s,io)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/tools/analysis/MSM/msm2txt/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 14:33 on 12-06-2012.

!  use
!  implicit none
  use module_msms_var
  use module_inout

  real,intent(in)::east,west,south,north
  type(msms),intent(in)::s
  type(inout),intent(inout)::io

!  write(*,'(a)')'Subroutine: set_out_range'
!  write(*,*)''

  do i=1,slon
    if( west  <= s%lon(i) .and. s%lon(i) <= west)then
      io%iw=i
    endif
  enddo

  do i=1,slon
    if( east  <= s%lon(i) .and. s%lon(i) <= east)then
      io%ie=i
    endif
  enddo

  do i=1,slat
    if( south  <= s%lat(i) .and. s%lat(i) <= south)then
      io%js=i
    endif
  enddo

  do i=1,slat
    if( north  <= s%lat(i) .and. s%lat(i) <= north)then
      io%jn=i
    endif
  enddo

!  print *,io%iw, io%ie, io%js, io%jn 
!  stop


!  write(*,'(a)')'Done subroutine set_out_range.'
!  write(*,*) 
end subroutine set_out_range

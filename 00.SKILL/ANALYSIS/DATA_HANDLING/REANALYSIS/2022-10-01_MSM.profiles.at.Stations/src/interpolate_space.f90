subroutine interpolate_space(p,vp,lon,lat)
! Description:
!
! Author: kunoki
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work2/kunoki/to_manda_sensei/Tools/MSM.profiles.at.Stations/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 11:34 on 06-03-2014.

  use module_main
  use module_msmp_var

  type(msmp),intent(in)::p
  type(msmp_vp),intent(inout)::vp
  real,intent(in)::lon,lat

  real xa(4), ya(4), za(4)
  real x,y
  real z

!  write(*,'(a)')'Program interpolate_space starts.'
!  write(*,*)''

  print *,"interpolate_space: ",lon,lat; print *

  idx=(lon-p%lon(1))/dlon
  jdx=plat-(lat-p%lat(plat))/dlat

  xa(1)=p%lon(idx);   ya(1)=p%lat(jdx+1)
  xa(2)=p%lon(idx+1); ya(2)=p%lat(jdx+1)
  xa(3)=p%lon(idx+1); ya(3)=p%lat(jdx)
  xa(4)=p%lon(idx);   ya(4)=p%lat(jdx)

  print *,xa(1),ya(1)
  print *,xa(2),ya(2)
  print *,xa(3),ya(3)
  print *,xa(4),ya(4)

  do n=1,ptime
!    print *,"interpolate_space: "; print *,p%time(n)

!   z
    do k=1,ppres
      za(1)=p%z(idx,   jdx+1, k, n)
      za(2)=p%z(idx+1, jdx+1, k, n)
      za(3)=p%z(idx+1, jdx,   k, n)
      za(4)=p%z(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%z(k,n))
!      print *,p%p(k),vp%z(k,n)
    enddo !k

!   u
    do k=1,ppres
      za(1)=p%u(idx,   jdx+1, k, n)
      za(2)=p%u(idx+1, jdx+1, k, n)
      za(3)=p%u(idx+1, jdx,   k, n)
      za(4)=p%u(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%u(k,n))
!      print *,p%p(k),vp%u(k,n)
    enddo !k

!   v
    do k=1,ppres
      za(1)=p%v(idx,   jdx+1, k, n)
      za(2)=p%v(idx+1, jdx+1, k, n)
      za(3)=p%v(idx+1, jdx,   k, n)
      za(4)=p%v(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%v(k,n))
!      print *,p%p(k),vp%v(k,n)
    enddo !k


!   temp
    do k=1,ppres
      za(1)=p%temp(idx,   jdx+1, k, n)
      za(2)=p%temp(idx+1, jdx+1, k, n)
      za(3)=p%temp(idx+1, jdx,   k, n)
      za(4)=p%temp(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%temp(k,n))
!      print *,p%p(k),vp%temp(k,n)
    enddo !k

!   rh
    do k=1,ppres
      za(1)=p%rh(idx,   jdx+1, k, n)
      za(2)=p%rh(idx+1, jdx+1, k, n)
      za(3)=p%rh(idx+1, jdx,   k, n)
      za(4)=p%rh(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%rh(k,n))
!      print *,p%p(k),vp%rh(k,n)
    enddo !k
  enddo !n

  do k=1,ppres
    vp%p(k)=p%p(k)
  enddo !k

  do n=1,ptime
    vp%time(n)=p%time(n)
!    print *,vp%time(n)
  enddo !n


!  write(*,'(a)')'Done program interpolate_space.'
!  write(*,*) 
end subroutine interpolate_space

subroutine interpolate_time(vp1,vp2,hh,stn)
! Description:
!
! Author: kunoki
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work2/kunoki/to_manda_sensei/Tools/MSM.profiles.at.Stations/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 14:14 on 06-03-2014.
  use module_main
  use module_msmp_var
  type(msmp_vp),intent(in)::vp1
  type(msmp_vp),intent(in)::vp2
  real,intent(in)::hh
  type(msmp_stn),intent(inout)::stn


!  implicit none

!  write(*,'(a)')'Subroutine: interpolate_time'
!  write(*,*)''
  print *,"interpolate_time: "; print *,hh

  if(hh<21.0)then
    ndx1=hh/3+1
    ndx2=ndx1+1
    print *,"interpolate_time: ",vp1%time(ndx1), vp1%time(ndx2)

    w1=(vp1%time(ndx2)-hh)/dtime
    w2=(hh-vp1%time(ndx1))/dtime

    do k=1,ppres
      stn%z(k)   =vp1%z(k,ndx1)*w1    + vp1%z(k,ndx2)*w2
      stn%u(k)   =vp1%u(k,ndx1)*w1    + vp1%u(k,ndx2)*w2
      stn%v(k)   =vp1%v(k,ndx1)*w1    + vp1%v(k,ndx2)*w2
      stn%temp(k)=vp1%temp(k,ndx1)*w1 + vp1%temp(k,ndx2)*w2
      stn%rh(k)  =vp1%rh(k,ndx1)*w1   + vp1%rh(k,ndx2)*w2
      if(vp1%rh(k,ndx1)<0.0 .or. vp1%rh(k,ndx2)<0)stn%rh(k)=rmiss
    enddo !k
  endif

  if(hh>=21.0)then

    ndx1=8
    ndx2=1
    print *,"interpolate_time: ",vp1%time(ndx1), vp2%time(ndx2)+24.0

    w1=(24.0-hh)/dtime
    w2=(hh-21.0)/dtime

    do k=1,ppres
      stn%z(k)   =vp1%z(k,ndx1)*w1    + vp2%z(k,ndx2)*w2
      stn%u(k)   =vp1%u(k,ndx1)*w1    + vp2%u(k,ndx2)*w2
      stn%v(k)   =vp1%v(k,ndx1)*w1    + vp2%v(k,ndx2)*w2
      stn%temp(k)=vp1%temp(k,ndx1)*w1 + vp2%temp(k,ndx2)*w2
      stn%rh(k)  =vp1%rh(k,ndx1)*w1   + vp2%rh(k,ndx2)*w2
      if(vp1%rh(k,ndx1)<0.0 .or. vp2%rh(k,ndx2)<0)stn%rh(k)=rmiss
    enddo !k
  endif

  print *,"interpolate_time: ",w1,w2,w1+w2

  do k=1,ppres
    stn%p(k)=vp1%p(k)
  enddo !k


!  write(*,'(a)')'Done subroutine interpolate_time.'
!  write(*,*) 
end subroutine interpolate_time

!------------------------------------------------------------------------------
! This source code can be translated into TeX documentation using a Perl script
! called ProTex.
! ProTex is available via: 
!     http://gmao.gsfc.nasa.gov/software/protex/ .
! On-line manual of ProTex can be found at:
!     http://gmao.gsfc.nasa.gov/software/protex/doc/protex/ .
!
! Usage: protex src > out.tex
!   src: source file name(s)
!   out: output tex file name
! Type "protex -h" for help.
!
!------------------------------------------------------------------------------
!BOP
!
! !ROUTINE: print_ioinfo
!
! !DESCRIPTION: 出力ファイルにヘッダ情報として、入出力ファイル名を印字する\\
! 使用法 \\
!   call print_ioinfo(iunit,infle,iokind) \\
!  iunit: 出力ファイルの機番(整数) \\
!  fle: 入出力ファイル名(文字型) \\
!  iokind: 入出力ファイルの種類(文字型): input or output \\
! \\
! !INTERFACE:
subroutine print_ioinfo(iunit,fle,iokind)
!
! !USES:
!
! !INPUT PARAMETERS:
      integer,intent(in):: iunit
      character,intent(in):: fle*(*), iokind*(*)
! !OUTPUT PARAMETERS:
!
! !INPUT/OUTPUT PARAMETERS:
!
! !LOCAL VARIABLES:

!
! !BUGS:  
!
! !SEE ALSO: 
!
! !SYSTEM ROUTINES: 
!
! !FILES USED:  
!
! !REVISION HISTORY: 
!   [Thu Aug 20 14:17:59 JST 2009]  - A. Manda - Initial version
!
! !REMARKS:
! このサブルーチンは, g77, ifortで動作することを確認している.
!
! 入出力に使ったファイル名を記録しておくと、後々作業をし直す必要
! が生じたときに役に立つ.
!
! !TO DO:
!
!EOP
!------------------------------------------------------------------------------
!---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
!BOC
      write(iunit,'(A,A,A,A)')'# ',iokind,': ',fle(1:lnblnk(fle))
      return
      end subroutine print_ioinfo
!EOC
!---+$|--1----+----2----+----3----+----4----+----5----+----6----+----7--
!------------------------------------------------------------------------------

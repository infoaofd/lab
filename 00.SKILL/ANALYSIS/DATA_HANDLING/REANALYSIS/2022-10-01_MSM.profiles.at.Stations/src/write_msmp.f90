subroutine write_msmp(yyyy,mm,dd, outdir, p)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 17:05 on 12-05-2012.

!  use
!  implicit none
  use module_msmp_var

  integer,intent(in)::yyyy,mm,dd
  character(len=*),intent(in):: outdir
  type(msmp),intent(in)::p

  character month*3, dat_msmp*500, ctl_msmp*500

!  write(*,'(a)')'Subroutine: write_msmp'
!  write(*,*)''

   is=1
   ie=lnblnk(outdir)
   write(dat_msmp(is:ie),'(A,A)')outdir(is:ie)
   is=ie+1
   ie=is
   write(dat_msmp(is:ie),'(A,A)')'/'
   is=ie+1
   write(dat_msmp(is:),'(A,i4.4,i2.2,i2.2,A)')'MSMP',yyyy,mm,dd,'.dat'
   print '(A,A)','Output: ',dat_msmp(1:lnblnk(dat_msmp))

   open(20,file=dat_msmp,form="unformatted")
   time: do l=1,ptime
     do k=1,ppres
       write(20)((p%z(i,j,k,l),   i=1,plon),j=1,plat)
     enddo
     do k=1,ppres
       write(20)((p%w(i,j,k,l),   i=1,plon),j=1,plat)
     enddo
     do k=1,ppres
       write(20)((p%u(i,j,k,l),   i=1,plon),j=1,plat)
     enddo
     do k=1,ppres
       write(20)((p%v(i,j,k,l),   i=1,plon),j=1,plat)
     enddo
     do k=1,ppres
       write(20)((p%temp(i,j,k,l),i=1,plon),j=1,plat)
     enddo
     do k=1,ppres
       write(20)((p%rh(i,j,k,l),  i=1,plon),j=1,plat)
     enddo

   enddo time
   close(20)

   if(mm == 1)month='Jan'
   if(mm == 2)month='Feb'
   if(mm == 3)month='Mar'
   if(mm == 4)month='Apr'
   if(mm == 5)month='May'
   if(mm == 6)month='Jun'
   if(mm == 7)month='Jul'
   if(mm == 8)month='Aug'
   if(mm == 9)month='Sep'
   if(mm == 10)month='Oct'
   if(mm == 11)month='Nov'
   if(mm == 12)month='Dec'

   is=1
   ie=lnblnk(outdir)
   write(ctl_msmp(is:ie),'(A,A)')outdir(is:ie)
   is=ie+1
   ie=is
   write(ctl_msmp(is:ie),'(A,A)')'/'
   is=ie+1
   write(ctl_msmp(is:),'(A,i4.4,i2.2,i2.2,A)')'MSMP',yyyy,mm,dd,'.ctl'
   print '(A,A)','Output: ',ctl_msmp(1:lnblnk(ctl_msmp))
   open(21,file=ctl_msmp)
   write(21,'(A,i4.4,i2.2,i2.2,A)')'DSET ^MSMP',yyyy,mm,dd,'.dat'
   write(21,'(A)')'OPTIONS sequential template yrev'
   write(21,'(A)')'UNDEF -999.9'
   write(21,'(A,i5,A,7f12.6)')'XDEF ',plon,' LEVELS'
   do i=1,plon
     write(21,'(f12.6)')p%lon(i)
   enddo
   write(21,'(A,i5,A,7f12.6)')'YDEF ',plat,' LEVELS'
   do i=plat,1,-1
     write(21,'(f12.6)')p%lat(i)
   enddo
   write(21,'(A,i5,A,7f9.1)')'ZDEF ',ppres,' LEVELS'
   do i=1,ppres
     write(21,'(f12.6)')p%p(i)
   enddo
   write(21,'(A,i5,A,i2.2,A3,i4.4,A)')'TDEF ',ptime,' linear 00Z',dd,month,yyyy,' 3HR'      
   write(21,'(A)')'VARS 6'
   write(21,'(A,i5,A,A,A)')'z ',ppres, ' 0 ',"geopotential height ","m"
   write(21,'(A,i5,A,A,A)')'w ',ppres, ' 0 ',"vertical velocity in p ","Pa/s"
   write(21,'(A,i5,A,A,A)')'u ',ppres, ' 0 ',"eastward component of wind ", "m/s"
   write(21,'(A,i5,A,A,A)')'v ',ppres, ' 0 ',"northward component of wind ","m/s"
   write(21,'(A,i5,A,A,A)')'temp ',ppres, ' 0 ',"air_temperature " ,"K"
   write(21,'(A,i5,A,A,A)')'rh ',ppres, ' 0 ',"relative_humidity ","%"
   write(21,'(A)')'ENDVARS'
   close(21)

!  write(*,'(a)')'Done subroutine write_msmp.'
!  write(*,*) 
end subroutine write_msmp

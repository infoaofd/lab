module module_msms_var
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:36 on 12-04-2012.

!  use
!  implicit none

!  write(*,'(a)')'Module: module_msms_var'
!  write(*,*)''
!  private
! ncid:ファイルのID番号、 varid: 変 数のID番号
  integer,public :: ncid,varid
  integer,public :: stat

  integer,public,parameter :: slon=481, slat=505, stime=24

  type, public :: msms
    character(len=50):: var
    real,dimension (slon) :: lon
    real,dimension (slat) :: lat
    real,dimension (stime) :: time

    real,dimension(slon,slat,stime) :: psea, sp, u, v, temp, &
&   rh, r1h, ncld_upper, ncld_mid, ncld_low, ncld

  end type msms


contains

subroutine read_msms_vars(varname, varout)

  include '/usr/local/include/netcdf.inc'

  character(len=*),intent(in)::varname
  real,dimension(:,:,:),intent(inout)::varout

  integer(2),dimension(slon,slat,stime)::varout_int

  integer,allocatable :: dimids(:),nshape(:)
  character(len=70):: err_message
  character(len=100) :: dimname !dimnameは各次元の名前。


!  print '(A)',varname(1:lnblnk(varname))
  ! ファイルID(ncid)からs%varで設定した変数のID(varid)を得る。
  sta = nf_inq_varid(ncid,varname,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
!  print *

  !  スケールファクターを得る。(*1)
  sta = nf_get_att_real(ncid,varid,'scale_factor',scale)
!  print *,'scale= ',scale
!  print *,'sta= ',sta
!  print *

  !  オフセットの 値を得る(*2)
  sta = nf_get_att_real(ncid,varid,'add_offset',offset)
!  print *,'offset=', offset
!  print *,'sta= ',sta
!  print *

! varidからndimsを得る。ndimsとは次元の数。二次元データなら2
  stat = nf_inq_varndims(ncid, varid, ndims)
!  print *,'ndims= ', ndims
!  print *

! varidからdimidsを得る。dimidsとはそれぞれの次元に対するID
  allocate(dimids(ndims))
  stat = nf_inq_vardimid(ncid, varid, dimids)
!  do i=1,ndims
!    write(6,'("dimids(",i2,")=",i9 )') i,dimids(i)
!  enddo
!  print *

  allocate(nshape(ndims))
  do i=1,ndims
!   dimidsからnshapeを得る。
!   nshapeとはそれぞれの次元に対する最大データ数 (格子数)
    stat = nf_inq_dimlen(ncid, dimids(i), nshape(i))
!    write(6,'("nshape(",i2,")=",i9 )') i,nshape(i)
  enddo !i
!  print *

  do i=1,ndims
    stat = nf_inq_dimname(ncid, dimids(i), dimname)
!    write(6,'("dimname(",i2,")=",1x,a23)') i,dimname
  enddo
!  print *

  print *,'Read ',varname(1:lnblnk(varname))
  stat = nf_get_var_int2(ncid, varid, varout_int)
  if(stat /= 0)then
    print *,'stat= ',stat
    print *,"Terminated."
    print *
    stop
  endif
  deallocate(dimids,nshape)

  varout(:,:,:)=float(varout_int(:,:,:))*scale + offset

end subroutine read_msms_vars

!  write(*,'(a)')'Done module module_msms_var.'
!  write(*,*) 
end module module_msms_var


! $ ncdump -h 1012.nc
! netcdf 1012 {
! dimensions:
!         lon = 481 ;
!         lat = 505 ;
!         time = 24 ;
!         ref_time = 8 ;
! variables:
!         float lon(lon) ;
!                 lon:long_name = "longitude" ;
!                 lon:units = "degrees_east" ;
!                 lon:standard_name = "longitude" ;
!         float lat(lat) ;
!                 lat:long_name = "latitude" ;
!                 lat:units = "degrees_north" ;
!                 lat:standard_name = "latitude" ;
!         float time(time) ;
!                 time:long_name = "time" ;
!                 time:standard_name = "time" ;
!                 time:units = "hours since 2012-10-12 00:00:00+00:00" ;
!         float ref_time(ref_time) ;
!                 ref_time:long_name = "forecaset reference time" ;
!                 ref_time:standard_name = "forecaset_reference_time" ;
!                 ref_time:units = "hours since 2012-10-12 00:00:00+00:00" ;
!         short psea(time, lat, lon) ;
!                 psea:scale_factor = 0.4587155879 ;
!                 psea:add_offset = 95000. ;
!                 psea:long_name = "sea level pressure" ;
!                 psea:units = "Pa" ;
!                 psea:standard_name = "air_pressure" ;
!         short sp(time, lat, lon) ;
!                 sp:scale_factor = 0.4587155879 ;
!                 sp:add_offset = 95000. ;
!                 sp:long_name = "surface air pressure" ;
!                 sp:units = "Pa" ;
!                 sp:standard_name = "surface_air_pressure" ;
!         short u(time, lat, lon) ;
!                 u:scale_factor = 0.006116208155 ;
!                 u:add_offset = 0. ;
!                 u:long_name = "eastward component of wind" ;
!                 u:units = "m/s" ;
!                 u:standard_name = "eastward_wind" ;
!         short v(time, lat, lon) ;
!                 v:scale_factor = 0.006116208155 ;
!                 v:add_offset = 0. ;
!                 v:long_name = "northward component of wind" ;
!                 v:units = "m/s" ;
!                 v:standard_name = "northward_wind" ;
!         short temp(time, lat, lon) ;
!                 temp:scale_factor = 0.002613491379 ;
!                 temp:add_offset = 255.4004974 ;
!                 temp:long_name = "temperature" ;
!                 temp:units = "K" ;
!                 temp:standard_name = "air_temperature" ;
!         short rh(time, lat, lon) ;
!                 rh:scale_factor = 0.002293577883 ;
!                 rh:add_offset = 75. ;
!                 rh:long_name = "relative humidity" ;
!                 rh:units = "%" ;
!                 rh:standard_name = "relative_humidity" ;
!         short r1h(time, lat, lon) ;
!                 r1h:scale_factor = 0.006116208155 ;
!                 r1h:add_offset = 200. ;
!                 r1h:long_name = "rainfall in 1 hour" ;
!                 r1h:units = "mm/h" ;
!                 r1h:standard_name = "rainfall_rate" ;
!         short ncld_upper(time, lat, lon) ;
!                 ncld_upper:scale_factor = 0.001666666591 ;
!                 ncld_upper:add_offset = 50. ;
!                 ncld_upper:long_name = "upper-level cloudiness" ;
!                 ncld_upper:units = "%" ;
!         short ncld_mid(time, lat, lon) ;
!                 ncld_mid:scale_factor = 0.001666666591 ;
!                 ncld_mid:add_offset = 50. ;
!                 ncld_mid:long_name = "mid-level cloudiness" ;
!                 ncld_mid:units = "%" ;
!         short ncld_low(time, lat, lon) ;
!                 ncld_low:scale_factor = 0.001666666591 ;
!                 ncld_low:add_offset = 50. ;
!                 ncld_low:long_name = "low-level cloudiness" ;
!                 ncld_low:units = "%" ;
!         short ncld(time, lat, lon) ;
!                 ncld:scale_factor = 0.001666666591 ;
!                 ncld:add_offset = 50. ;
!                 ncld:long_name = "cloud amount" ;
!                 ncld:units = "%" ;
!                 ncld:standard_name = "cloud_area_fraction" ;

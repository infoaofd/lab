#!/bin/bash

YYYY=$1; MMDD=$2

YYYY=${YYYY:-2022}; MMDD=${MMDD:-0705}

INDIR=/work01/DATA/MSM/MSM-S/$YYYY
INFLE=${MMDD}.nc

STN=MESHIMA; LAT=31.9980555556; LON=128.350833333

OFLE=${YYYY}${MMDD}_${STN}_$(basename $0 .sh).nc

ODIR=OUT_NC_${STN}
mkdir -vp $ODIR
OUT=${ODIR}/${OFLE}
rm -vf $OUT

# ある緯度経度地点における値をbilinear interpolationを用いて# 抽出する
# https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

# 女島 (長崎県)
# 北緯31度59分53秒  31+59/60.+53/3600.
# 東経128度21分03秒 128 + 21./60. + 3/3600.

cdo seltimestep,1,4,7,9,12,15,18,21 -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OUT

echo
echo $STN $LAT $LON
echo
echo INPUT: $INDIR/$INFLE
echo OUTPUT: $OUT



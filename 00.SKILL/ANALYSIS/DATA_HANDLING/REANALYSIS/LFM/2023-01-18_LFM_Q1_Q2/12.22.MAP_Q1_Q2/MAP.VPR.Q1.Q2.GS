
'open LFM.CTL'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 2

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=1; ymargin=1

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon 123 131'; 'set lat 27 33'
'set lev 1000 300'; 'set time 00Z19JUN2022'

say 'MMMMM HORI ADVCTION'
'pi=3.14159'; 'dtr=pi/180'  ;# DEGREE TO RADIAN
'a=6.37122e6'               ;# RADIUS OF THE EARTH
'L=2.5E6'                   ;# LATENT HEAT OF VAPORIZATION
'Cp=1004'                   ;# Specific heat at constant pressure
'kappa=0.286'               ;# kappa=Rd/Cp
'dy=cdiff(lat,y)*dtr*a'; 'dx=cdiff(lon,x)*dtr*a*cos(lat*dtr)' 
'dtdx=cdiff(temp,x)/dx' ; 'dtdy=cdiff(temp,y)/dy'
'dt2=3600*2'

'Q1hadv=Cp*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (temp(z+1)-temp(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define work = kappa*temp/(lev*100)'
'define Q1vadv = Cp*w*(dtdz-work)'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q1tend = Cp*(temp(t+1)-temp(t-1))/dt2'

say 'MMMMM Q1'
'define Q1=Q1tend+Q1hadv+Q1vadv'


'dtdx=cdiff(qv,x)/dx' ; 'dtdy=cdiff(qv,y)/dy'

'Q2hadv=L*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (qv(z+1)-qv(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define Q2vadv = L*w*dtdz'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q2tend = L*(qv(t+1)-qv(t-1))/dt2'

say 'MMMMM Q2'
'define Q2=-(Q2tend+Q2hadv+Q2vadv)'


nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shade2'; 'color -10 10 2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR
'set lev 850'; 'd Q1'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'
'set lev 850'; 'd vpt'

'trackplot 127 30 127.25 30 -c 1 -l 1 -t 4'
'trackplot 127.25 30 127.25 30.25 -c 1 -l 1 -t 4'
'trackplot 127.25 30.25 127 30.25 -c 1 -l 1 -t 4'
'trackplot 127 30.25 127 30 -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt+0.5

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q1 00Z19JUN2022 850'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color -10 10 2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR

'set lev 850'; 'd Q2'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'
'set lev 850'; 'd vpt'

'trackplot 127 30 127.25 30 -c 1 -l 1 -t 4'
'trackplot 127.25 30 127.25 30.25 -c 1 -l 1 -t 4'
'trackplot 127.25 30.25 127 30.25 -c 1 -l 1 -t 4'
'trackplot 127 30.25 127 30 -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q2 00Z19JUN2022 850'



nmap = 3
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

#'set gxout shade2'; 'color -10 10 2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR

'set lev 975 400'

'set lon 127';'set lat 30'
'Q1AV=aave(Q1,lon=127,lon=127.25,lat=30,lat=30.25)'
'Q2AV=aave(Q2,lon=127,lon=127.25,lat=30,lat=30.25)'

#'set lat 30 30.25'
#'Q1AVwe=ave(Q1,lon=127,lon=127.25)'
#'Q2AVwe=ave(Q2,lon=127,lon=127.25)'
#'set lon 127'
#'Q1AV=ave(Q1AVwe,lat=30,lat=30.25)'
#'Q2AV=ave(Q2AVwe,lat=30,lat=30.25)'

'set vrange -2 2';'set xlint 1';'set ylint 100'
'set ccolor 2';'set cthick 2';'set cstyle 1';'set cmark 0'
'd Q1AV'

'set xlab off';'set ylab off'

'set vrange -2 2';'set xlint 1';'set ylint 100'
'set ccolor 4';'set cthick 2';'set cstyle 1';'set cmark 0'
'd Q2AV'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' 00Z19JUN2022 30-30.25, 127-127.25'
y=y+0.2
'draw string 'x' 'y' Red=Q1 Blue=Q2'

x=(xl+xr)/2; y=yb-0.4
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' [K/s]'




'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' Q1.Q2.VPR.MAP850_2022-06-19_00.pdf'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ./MAP.VPR.Q1.Q2.sh '
yy = yy+0.2; 'draw string ' xx ' ' yy ' LFM.CTL'
yy = yy+0.2; 'draw string ' xx ' ' yy ' Sat, 14 Jan 2023 17:02:18 +0900'
yy = yy+0.2; 'draw string ' xx ' ' yy ' p5820.bio.mie-u.ac.jp'

'gxprint Q1.Q2.VPR.MAP850_2022-06-19_00.pdf'
'quit'

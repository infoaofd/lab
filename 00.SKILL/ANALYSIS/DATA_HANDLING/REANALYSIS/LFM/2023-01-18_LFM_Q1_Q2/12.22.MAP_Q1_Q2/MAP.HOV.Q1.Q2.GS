
'open LFM.CTL'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 3

ytop=9; xwid = 5.0/xmax; ywid = 4.0/ymax

xmargin=1; ymargin=0.6

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon 123 131'; 'set lat 27 33'
'set lev 1000 300'; 'set time 12Z18JUN2022 23Z19JUN2022'

say 'MMMMM HORI ADVCTION'
'pi=3.14159'; 'dtr=pi/180'  ;# DEGREE TO RADIAN
'a=6.37122e6'               ;# RADIUS OF THE EARTH
'L=2.5E6'                   ;# LATENT HEAT OF VAPORIZATION
'Cp=1004'                   ;# Specific heat at constant pressure
'kappa=0.286'               ;# kappa=Rd/Cp
'dy=cdiff(lat,y)*dtr*a'; 'dx=cdiff(lon,x)*dtr*a*cos(lat*dtr)' 
'dtdx=cdiff(temp,x)/dx' ; 'dtdy=cdiff(temp,y)/dy'
'dt2=3600*2'

'Q1hadv=Cp*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (temp(z+1)-temp(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define work = kappa*temp/(lev*100)'
'define Q1vadv = Cp*w*(dtdz-work)'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q1tend = Cp*(temp(t+1)-temp(t-1))/dt2'

say 'MMMMM Q1'
'define Q1=Q1tend+Q1hadv+Q1vadv'


'dtdx=cdiff(qv,x)/dx' ; 'dtdy=cdiff(qv,y)/dy'

'Q2hadv=L*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (qv(z+1)-qv(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define Q2vadv = L*w*dtdz'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q2tend = L*(qv(t+1)-qv(t-1))/dt2'

say 'MMMMM Q2'
'define Q2=-(Q2tend+Q2hadv+Q2vadv)'


say 'MMMMM TIME AVE'
'set time 00Z19JUN2022'; 'set lev 850'
'Q1TAV=ave(Q1,time=18Z18JUN2022,time=06Z19JUN2022)'
'Q2TAV=ave(Q2,time=18Z18JUN2022,time=06Z19JUN2022)'
'VTTAV=ave(vpt,time=18Z18JUN2022,time=06Z19JUN2022)'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shade2'; 'color -5 5 0.5 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR
'set time 00Z19JUN2022'; 'set lev 850'

'd Q1TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'

'd VTTAV'

'trackplot 126 29.75 126.25 29.75 -c 1 -l 1 -t 4'
'trackplot 126.25 29.75 126.25 30 -c 1 -l 1 -t 4'
'trackplot 126.25 30 126 30 -c 1 -l 1 -t 4'
'trackplot 126 30 126 29.75 -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt+0.5

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.01
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.08
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' Q2 850 18UTC18-06UTC19'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color -5 5 0.5 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR

'set time 00Z19JUN2022'; 'set lev 850'
'd Q2TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'
'set time 00Z19JUN2022'; 'set lev 850'
'd VTTAV'

'trackplot 126 29.75 126.25 29.75 -c 1 -l 1 -t 4'
'trackplot 126.25 29.75 126.25 30 -c 1 -l 1 -t 4'
'trackplot 126.25 30 126 30 -c 1 -l 1 -t 4'
'trackplot 126 30 126 29.75 -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.01
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.08
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' Q2 850 18UTC18-06UTC19'


say 'MMMMM HOVMOLLER'
'set lev 975 400';'set time 12Z18JUN2022 23Z19JUN2022'
'set lon 126';'set lat 29.75'
'Q1AV=tloop(aave(Q1,lon=126,lon=126.25,lat=29.75,lat=30))'
'Q2AV=tloop(aave(Q2,lon=126,lon=126.25,lat=29.75,lat=30))'
#'set lat 29.75 30'
#'Q1AVwe=ave(Q1,lon=126,lon=126.25)'
#'Q2AVwe=ave(Q2,lon=126,lon=126.25)'
#'set lon 126'
#'Q1AV=ave(Q1AVwe,lat=29.75,lat=30)'
#'Q2AV=ave(Q2AVwe,lat=29.75,lat=30)'


nmap = 3
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100';'set tlsupp year'
'set gxout shade2'; 'color -2 2 0.2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR
'd Q1AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q1 29.75-30, 126-126.25'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs  -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.02
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' '

nmap = 4
ymap = 3 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100'
'set gxout shade2'; 'color -2 2 0.2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson' ;# SET COLOR BAR
'd Q2AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.15
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q2 29.75-30, 126-126.25'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 5 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.02
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' K/s'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' Q1.Q2.HOV.MAP_126-126.25_29.75-30.pdf'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ./MAP.HOV.Q1.Q2.sh '
yy = yy+0.2; 'draw string ' xx ' ' yy ' LFM.CTL'
yy = yy+0.2; 'draw string ' xx ' ' yy ' /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/32.12.Q1_Q2/12.22.MAP_Q1_Q2'
yy = yy+0.2; 'draw string ' xx ' ' yy ' Mon, 16 Jan 2023 14:56:34 +0900'

'gxprint Q1.Q2.HOV.MAP_126-126.25_29.75-30.pdf'
'quit'

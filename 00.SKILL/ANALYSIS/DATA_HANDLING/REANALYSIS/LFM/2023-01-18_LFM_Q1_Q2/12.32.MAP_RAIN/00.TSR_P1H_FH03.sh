#!/bin/bash
# /work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/22.24.TSR_P1H_FH03
#MMMMMMMMMMMMMM INCLUDE ###################
INC=./12.12.LFM.CTL.FH03.sh
. $INC
#MMMMMMMMMMMMMM INCLUDE ###################

#YYYYMMDDHH=2011012100
#YYYY=; MM=; DD=; HH=

#if [  = "01" ]; then MMM="JAN"; fi; if [  = "02" ]; then MMM="FEB"; fi
#if [  = "03" ]; then MMM="MAR"; fi; if [  = "04" ]; then MMM="APR"; fi
#if [  = "05" ]; then MMM="MAY"; fi; if [  = "06" ]; then MMM="JUN"; fi
#if [  = "07" ]; then MMM="JUL"; fi; if [  = "08" ]; then MMM="AUG"; fi
#if [  = "09" ]; then MMM="SEP"; fi; if [  = "10" ]; then MMM="OCT"; fi
#if [  = "11" ]; then MMM="NOV"; fi; if [  = "12" ]; then MMM="DEC"; fi

DSET=FH03
BOX=${BOX:-BOX31};LON1W=${LON1W:-128};LON1E=${LON1E:-128.5};
                 LAT1S=${LAT1S:-30.5};LAT1N=${LAT1N:-31.0}
#BOX=${BOX:-BOX32};LON1W=${LON1W:-129};LON1E=${LON1E:-129.5};
#                   LAT1S=${LAT1S:-29.5} ;LAT1N=${LAT1N:-30.0}
#BOX=${BOX:-BOX33};LON1W=${LON1W:-127};LON1E=${LON1E:-127.5};
#                   LAT1S=${LAT1S:-29} ;LAT1N=${LAT1N:-29.5}

GS=$(basename $0 .sh).GS
CTL=LFM.SFC.FH03.CTL

BOX_SCRIPT=BOX.GS

TIME1=12Z18JUN2022; TIME2=23Z19JUN2022; TIME="$TIME1 $TIME2"
FIG=$(basename $0 .sh)_${BOX}_${LON1W}-${LON1E}_${LAT1S}_${LAT1N}.pdf

LONW=122.0 ; LONE=134; LATS=24.5; LATN=35 #MAP AREA

KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
CLEVS='50 300 50'; FS=1; UNIT="mm/36h"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}
'open ${CTL}'

ytop=9; xmargin=0.5; ymargin=0.5
xmax = 1; ymax = 2
xwid = 5.0/xmax; ywid = 5.0/ymax
'set vpage 0.0 8.5 0.0 11'
'cc';'set grads off';'set grid off'

say 'MMMMM MAP'

xmap = 1;ymap = 1;nmap = 1
xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
'color $CLEVS -gxout shaded -kind $KIND'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME1}'
'set xlevs 124 126 128 130 132';'set ylint 2'
'set mpdset hires'
'set xlopts 1 2 0.1'; 'set ylopts 1 2 0.1'
'r3h=sum(prc,time=${TIME1},time=${TIME2})'
'd r3h(time=${TIME1})/3'

'set xlab off';'set ylab off'

BOX($BOX,$LON1W,$LON1E,$LAT1S,$LAT1N)

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)
XLEFT=0.2; YUP=yt+0.5

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt-0.3 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 2 -line on -edge circle'
x=(x1+x2)/2+0.05; y=y2+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 2 0'
'draw string 'x' 'y' ${UNIT}'

'set strsiz 0.08 0.1'; 'set string 1 c 3 0' ;# TITLE
x=(xl+xr)/2 ;y=yt+0.15
'draw string 'x' 'y' ${DSET} (${LAT1S}-${LAT1N}), (${LON1W}-${LON1E})'


say;say;say
say 'MMMMM TSR'
xmap = 1;ymap = 2;nmap = 2
xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - 1
'set parea 'xs ' 'xe' 'ys' 'ye

'set lon ${LONW}'; 'set lat ${LATS}';'set z 1'
'set time ${TIME1} ${TIME2}'
'r1h=tloop(aave(prc,lon=${LON1W},lon=${LON1E},lat=${LAT1S},lat=${LAT1N}) )'

'set gxout line'
'set xlab on';'set ylab on';'set grid off'
'set cmark 0';'set ccolor 1';'set cthick 1'
#'set ylpos 0.0 l'
'set ylint 10'
'set vrange 0 35'
'd r1h/3'


'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)

'set strsiz 0.08 0.1'; 'set string 1 c 3 90'
x=xl-0.5 ;y=(yb+yt)/2
'draw string 'x' 'y' P [mm/h]'

'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.15
'draw string 'x' 'y' ${DSET} (${LAT1S}-${LAT1N}), (${LON1W}-${LON1E})'

'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = XLEFT; yy=YUP; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'

'gxprint ${FIG}'
'quit'
EOF

cat $BOX_SCRIPT >>$GS

grads -bcp "$GS"
#rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo; echo "MMMMM $BOX $LON1W $LON1E $LAT1S $LAT1N";echo

echo "DONE $0."
echo

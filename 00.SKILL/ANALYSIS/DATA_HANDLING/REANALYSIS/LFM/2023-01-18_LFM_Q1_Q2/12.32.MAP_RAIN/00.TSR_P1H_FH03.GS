'open LFM.SFC.FH03.CTL'

ytop=9; xmargin=0.5; ymargin=0.5
xmax = 1; ymax = 2
xwid = 5.0/xmax; ywid = 5.0/ymax
'set vpage 0.0 8.5 0.0 11'
'cc';'set grads off';'set grid off'

say 'MMMMM MAP'

xmap = 1;ymap = 1;nmap = 1
xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
'color 50 300 50 -gxout shaded -kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'

'set lon 122.0 134'; 'set lat 24.5 35'
# 'set lev '
'set time 12Z18JUN2022'
'set xlevs 124 126 128 130 132';'set ylint 2'
'set mpdset hires'
'set xlopts 1 2 0.1'; 'set ylopts 1 2 0.1'
'r3h=sum(prc,time=12Z18JUN2022,time=23Z19JUN2022)'
'd r3h(time=12Z18JUN2022)/3'

'set xlab off';'set ylab off'

BOX(BOX31,128,128.5,30.5,31.0)

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)
XLEFT=0.2; YUP=yt+0.5

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt-0.3 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 1 -ft 2 -line on -edge circle'
x=(x1+x2)/2+0.05; y=y2+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 2 0'
'draw string 'x' 'y' mm/36h'

'set strsiz 0.08 0.1'; 'set string 1 c 3 0' ;# TITLE
x=(xl+xr)/2 ;y=yt+0.15
'draw string 'x' 'y' FH03 (30.5-31.0), (128-128.5)'


say;say;say
say 'MMMMM TSR'
xmap = 1;ymap = 2;nmap = 2
xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - 1
'set parea 'xs ' 'xe' 'ys' 'ye

'set lon 122.0'; 'set lat 24.5';'set z 1'
'set time 12Z18JUN2022 23Z19JUN2022'
'r1h=tloop(aave(prc,lon=128,lon=128.5,lat=30.5,lat=31.0) )'

'set gxout line'
'set xlab on';'set ylab on';'set grid off'
'set cmark 0';'set ccolor 1';'set cthick 1'
#'set ylpos 0.0 l'
'set ylint 10'
'set vrange 0 35'
'd r1h/3'


'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)

'set strsiz 0.08 0.1'; 'set string 1 c 3 90'
x=xl-0.5 ;y=(yb+yt)/2
'draw string 'x' 'y' P [mm/h]'

'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.15
'draw string 'x' 'y' FH03 (30.5-31.0), (128-128.5)'

'set strsiz 0.12 0.14'; 'set string 1 l 3 0'
xx = XLEFT; yy=YUP; 'draw string ' xx ' ' yy ' 00.TSR_P1H_FH03_BOX31_128-128.5_30.5_31.0.pdf'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ./00.TSR_P1H_FH03.sh '
yy = yy+0.2; 'draw string ' xx ' ' yy ' /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/32.12.Q1_Q2/12.32.MAP_RAIN'
yy = yy+0.2; 'draw string ' xx ' ' yy ' LFM.SFC.FH03.CTL'

'gxprint 00.TSR_P1H_FH03_BOX31_128-128.5_30.5_31.0.pdf'
'quit'



function BOX(BOXNAME,LONW,LONE,LATS,LATN)

say 'MMMMM 'BOXNAME' 'LONW' 'LONE' 'LATS' 'LATN
'trackplot 'LONW' 'LATS' 'LONE' 'LATS' -c 0 -l 1 -t 4'
'trackplot 'LONE' 'LATS' 'LONE' 'LATN' -c 0 -l 1 -t 4'
'trackplot 'LONE' 'LATN' 'LONW' 'LATN' -c 0 -l 1 -t 4'
'trackplot 'LONW' 'LATN' 'LONW' 'LATS' -c 0 -l 1 -t 4'

'trackplot 'LONW' 'LATS' 'LONE' 'LATS' -c 1 -l 1 -t 1'
'trackplot 'LONE' 'LATS' 'LONE' 'LATN' -c 1 -l 1 -t 1'
'trackplot 'LONE' 'LATN' 'LONW' 'LATN' -c 1 -l 1 -t 1'
'trackplot 'LONW' 'LATN' 'LONW' 'LATS' -c 1 -l 1 -t 1'

return

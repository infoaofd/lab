#!/bin/bash

# Sat, 14 Jan 2023 13:26:03 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/32.12.Q1_Q2/12.12.TEST_OPEN_NC

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=123 ;LONE=131 ; LATS=27 ;LATN=33
BLONW=127; BLONE=127.25; BLATS=30; BLATN=30.25

LRANGE="1000 300"; MLEV=850
MTIME=${HH}Z${DD}${MMM}${YYYY}
TIME1=12Z18JUN2022; TIME2=23Z19JUN2022;TRANGE="$TIME1 $TIME2"
TAV1H=18; TAV1D=18; TAV2H=06; TAV2D=19
TAV1=${TAV1H}Z${TAV1D}JUN2022; TAV2=${TAV2H}Z${TAV2D}JUN2022

CTL=LFM.CTL
if [ ! -f  ];then echo ERROR in $: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=Q1.Q2.HOV.MAP_${BLONW}-${BLONE}_${BLATS}-${BLATN}.pdf ;#eps

LEVS="-5 5 0.5"
LEVS2="-2 2 0.2"
KIND='-kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson'
#KIND='-kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=5; UNIT=K/s
VRANGE="-2 2"; XLINT=1


HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 3

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=1; ymargin=1

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LRANGE}'; 'set time ${TRANGE}'

say 'MMMMM HORI ADVCTION'
'pi=3.14159'; 'dtr=pi/180'  ;# DEGREE TO RADIAN
'a=6.37122e6'               ;# RADIUS OF THE EARTH
'L=2.5E6'                   ;# LATENT HEAT OF VAPORIZATION
'Cp=1004'                   ;# Specific heat at constant pressure
'kappa=0.286'               ;# kappa=Rd/Cp
'dy=cdiff(lat,y)*dtr*a'; 'dx=cdiff(lon,x)*dtr*a*cos(lat*dtr)' 
'dtdx=cdiff(temp,x)/dx' ; 'dtdy=cdiff(temp,y)/dy'
'dt2=3600*2'

'Q1hadv=Cp*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (temp(z+1)-temp(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define work = kappa*temp/(lev*100)'
'define Q1vadv = Cp*w*(dtdz-work)'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q1tend = Cp*(temp(t+1)-temp(t-1))/dt2'

say 'MMMMM Q1'
'define Q1=Q1tend+Q1hadv+Q1vadv'


'dtdx=cdiff(qv,x)/dx' ; 'dtdy=cdiff(qv,y)/dy'

'Q2hadv=L*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (qv(z+1)-qv(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define Q2vadv = L*w*dtdz'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'
'define Q2tend = L*(qv(t+1)-qv(t-1))/dt2'

say 'MMMMM Q2'
'define Q2=-(Q2tend+Q2hadv+Q2vadv)'


say 'MMMMM TIME AVE'
'set time $MTIME'; 'set lev $MLEV'
'Q1TAV=ave(Q1,time=${TAV1},time=${TAV2})'
'Q2TAV=ave(Q2,time=${TAV1},time=${TAV2})'
'VTTAV=ave(vpt,time=${TAV1},time=${TAV2})'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shade2'; 'color ${LEVS} ${KIND}' ;# SET COLOR BAR
'set time $MTIME'; 'set lev $MLEV'

'd Q1TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'

'd VTTAV'

'trackplot $BLONW $BLATS $BLONE $BLATS -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATS $BLONE $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATN $BLONW $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONW $BLATN $BLONW $BLATS -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt+0.5

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.08
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q2 ${MLEV} ${TAV1H}UTC${TAV1D}-${TAV2H}UTC${TAV2D}'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set time $MTIME'; 'set lev $MLEV'
'd Q2TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'
'set time $MTIME'; 'set lev $MLEV'
'd VTTAV'

'trackplot $BLONW $BLATS $BLONE $BLATS -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATS $BLONE $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATN $BLONW $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONW $BLATN $BLONW $BLATS -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.08
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q2 ${MLEV} ${TAV1H}UTC${TAV1D}-${TAV2H}UTC${TAV2D}'


say 'MMMMM HOVMOLLER'
'set lev 975 400';'set time $TRANGE'
'set lon $BLONW';'set lat $BLATS'
'Q1AV=tloop(aave(Q1,lon=$BLONW,lon=$BLONE,lat=$BLATS,lat=$BLATN))'
'Q2AV=tloop(aave(Q2,lon=$BLONW,lon=$BLONE,lat=$BLATS,lat=$BLATN))'
#'set lat $BLATS $BLATN'
#'Q1AVwe=ave(Q1,lon=$BLONW,lon=$BLONE)'
#'Q2AVwe=ave(Q2,lon=$BLONW,lon=$BLONE)'
#'set lon $BLONW'
#'Q1AV=ave(Q1AVwe,lat=$BLATS,lat=$BLATN)'
#'Q2AV=ave(Q2AVwe,lat=$BLATS,lat=$BLATN)'


nmap = 3
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100'
'set gxout shade2'; 'color ${LEVS2} ${KIND}' ;# SET COLOR BAR
'd Q1AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q1 ${BLATS}-${BLATN}, ${BLONW}-${BLONE}'



nmap = 4
ymap = 3 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100'
'set gxout shade2'; 'color ${LEVS2} ${KIND}' ;# SET COLOR BAR
'd Q2AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Q2 ${BLATS}-${BLATN}, ${BLONW}-${BLONE}'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${HOST}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

#echo; echo MMMMM BACK-UP SCRIPTS
#ODIR= ; mkdir -vp $ODIR
#TMP=TMP_$(basename $0)
#echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
#echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
#echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
#BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
#rm -f $TMP
#echo MMMMM

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

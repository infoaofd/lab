#!/bin/bash

# Sat, 14 Jan 2023 13:26:03 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/32.12.Q1_Q2/12.12.TEST_OPEN_NC

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=123 ;LONE=131 ; LATS=27 ;LATN=33
BLONW=126; BLONE=126.25; BLATS=29.75; BLATN=30

LRANGE="1000 300"; MLEV=850
MTIME=${HH}Z${DD}${MMM}${YYYY}
TIME1=12Z18JUN2022; TIME2=23Z19JUN2022;TRANGE="$TIME1 $TIME2"
TAV1H=18; TAV1D=18; TAV2H=06; TAV2D=19
TAV1=${TAV1H}Z${TAV1D}JUN2022; TAV2=${TAV2H}Z${TAV2D}JUN2022

CTL=LFM.CTL
if [ ! -f  ];then echo ERROR in $: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=Qv.OMG.HOV.MAP_${BLONW}-${BLONE}_${BLATS}-${BLATN}.pdf ;#eps

LEVS1="4 18 1"; UNIT1=g/kg; FS1=2
LEVS1M="10 14 0.5"; UNIT1=g/kg; FS1=2
LEVS2="-3 3 0.3"; UNIT2=Pa/s; FS2=3
LEVS3="60 95 5"; UNIT3=%; FS3=2

KIND1='-kind maroon->saddlebrown->darkgoldenrod->khaki->white->palegreen->lightgreen->limegreen->darkgreen'
KIND2='-kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson'
KIND3="-kind white->antiquewhite->mistyrose->lightpink->mediumvioletred->navy->darkblue->blue->dodgerblue->aqua"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 3

ytop=9; xwid = 5.0/xmax; ywid = 4.0/ymax

xmargin=1; ymargin=0.6

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LRANGE}'; 'set time ${TRANGE}'

say 'MMMMM TIME AVE'
'set time $MTIME'; 'set lev $MLEV'
'Q1TAV=ave(qv,time=${TAV1},time=${TAV2})*1000.'
'Q2TAV=ave(w,time=${TAV1},time=${TAV2})'
'VTTAV=ave(vpt,time=${TAV1},time=${TAV2})'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set gxout shade2'; 'color ${LEVS1M} ${KIND1}' ;# SET COLOR BAR
'set time $MTIME'; 'set lev $MLEV'

'd Q1TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'

'd VTTAV'

'trackplot $BLONW $BLATS $BLONE $BLATS -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATS $BLONE $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATN $BLONW $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONW $BLATN $BLONW $BLATS -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt+0.5

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS1 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.01
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' ${UNIT1}'

x=(xl+xr)/2; y=yt+0.08
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' Qv${MLEV} ${TAV1H}UTC${TAV1D}-${TAV2H}UTC${TAV2D}'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color ${LEVS2} ${KIND2}' ;# SET COLOR BAR

'set time $MTIME'; 'set lev $MLEV'
'd Q2TAV'

'set xlab off';'set ylab off'

'set gxout contour';'set cint 1';'set cthick 1';'set ccolor 1'
'set clskip 2';'set clopts 1 1 0.06'
'set time $MTIME'; 'set lev $MLEV'
'd VTTAV'

'trackplot $BLONW $BLATS $BLONE $BLATS -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATS $BLONE $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONE $BLATN $BLONW $BLATN -c 1 -l 1 -t 4'
'trackplot $BLONW $BLATN $BLONW $BLATS -c 1 -l 1 -t 4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS2 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.01
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' ${UNIT2}'

x=(xl+xr)/2; y=yt+0.08
'set strsiz 0.06 0.08'; 'set string 1 c 2 0'
'draw string 'x' 'y' OMG${MLEV} ${TAV1H}UTC${TAV1D}-${TAV2H}UTC${TAV2D}'


say 'MMMMM HOVMOLLER'
'set lev 975 400';'set time $TRANGE'
'set lon $BLONW';'set lat $BLATS'
'Q1AV=tloop(aave(qv,lon=$BLONW,lon=$BLONE,lat=$BLATS,lat=$BLATN))*1000.'
'Q2AV=tloop(aave(w,lon=$BLONW,lon=$BLONE,lat=$BLATS,lat=$BLATN))'
'Q3AV=tloop(aave(rh,lon=$BLONW,lon=$BLONE,lat=$BLATS,lat=$BLATN))'



nmap = 3
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100';'set tlsupp year'
'set gxout shade2'; 'color ${LEVS1} ${KIND1}' ;# SET COLOR BAR
'd Q1AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' Qv ${BLATS}-${BLATN}, ${BLONW}-${BLONE}'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS1 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.01
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT1}'


nmap = 4
ymap = 3 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100'
'set gxout shade2'; 'color ${LEVS2} ${KIND2}' ;# SET COLOR BAR
'd Q2AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.15
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' OMG ${BLATS}-${BLATN}, ${BLONW}-${BLONE}'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS2 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.02
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT2}'


nmap = 4
ymap = 4 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid*2
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set ylint 100'
'set gxout shade2'; 'color ${LEVS3} ${KIND3}' ;# SET COLOR BAR
'd Q3AV'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.15
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' RH ${BLATS}-${BLATN}, ${BLONW}-${BLONE}'

x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS3 -ft 3 -line on -edge circle'
x=(x1+x2)/2+0.02; y=yt+0.02
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT3}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

#echo; echo MMMMM BACK-UP SCRIPTS
#ODIR= ; mkdir -vp $ODIR
#TMP=TMP_$(basename $0)
#echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
#echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
#echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
#BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
#rm -f $TMP
#echo MMMMM

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

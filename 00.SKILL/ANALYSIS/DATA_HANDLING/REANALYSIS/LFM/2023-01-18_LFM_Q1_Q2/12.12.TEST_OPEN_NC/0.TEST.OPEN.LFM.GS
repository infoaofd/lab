
'open LFM.CTL'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

'cc';'set mpdset hires';'set grads off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon 123 131'; 'set lat 27 33'
'set lev 850'; 'set time 00Z19JUN2022'

say 'MMMMM HORI ADVCTION'
'pi=3.14159'; 'dtr=pi/180'  ;# DEGREE TO RADIAN
'a=6.37122e6'               ;# RADIUS OF THE EARTH
'L=2.5E6'                   ;# LATENT HEAT OF VAPORIZATION

'dy=cdiff(lat,y)*dtr*a'; 'dx=cdiff(lon,x)*dtr*a*cos(lat*dtr)' 
'dtdx=cdiff(qv,x)/dx' ; 'dtdy=cdiff(qv,y)/dy'

'hadv=L*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (qv(z+1)-qv(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define vadv = L*w*dtdz'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'

#say 'MMMMM Q2'

'set gxout shade2'; 'color -10 10 2 -kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson' ;# SET COLOR BAR

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'd hadv'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 5 -ft 3 -line on -edge circle'
x=x2; y=(y1+y2)/2
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' HADV 00Z19JUN2022 850'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color -10 10 2 -kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson' ;# SET COLOR BAR

'd vadv'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 5 -ft 3 -line on -edge circle'
x=x2; y=(y1+y2)/2
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' K/s'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' VADV 00Z19JUN2022 850'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' TEST_2022-06-19_00.pdf'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ./0.TEST.OPEN.LFM.sh '
yy = yy+0.2; 'draw string ' xx ' ' yy ' LFM.CTL'
yy = yy+0.2; 'draw string ' xx ' ' yy ' Sat, 14 Jan 2023 14:54:17 +0900'
yy = yy+0.2; 'draw string ' xx ' ' yy ' p5820.bio.mie-u.ac.jp'

'gxprint TEST_2022-06-19_00.pdf'
'quit'

#!/bin/bash

# Sat, 14 Jan 2023 13:26:03 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/32.12.Q1_Q2/12.12.TEST_OPEN_NC

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=123 ;LONE=131 ; LATS=27 ;LATN=33
LEV="850" #1000 300"
TIME=${HH}Z${DD}${MMM}${YYYY}

CTL=LFM.CTL
if [ ! -f  ];then echo ERROR in $: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=TEST_${YYYY}-${MM}-${DD}_${HH}.pdf ;#eps

LEVS="-10 10 2"
#LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=5; UNIT=K/s

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

'cc';'set mpdset hires';'set grads off'
'set xlint 2';'set ylint 2'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

say 'MMMMM HORI ADVCTION'
'pi=3.14159'; 'dtr=pi/180'  ;# DEGREE TO RADIAN
'a=6.37122e6'               ;# RADIUS OF THE EARTH
'L=2.5E6'                   ;# LATENT HEAT OF VAPORIZATION

'dy=cdiff(lat,y)*dtr*a'; 'dx=cdiff(lon,x)*dtr*a*cos(lat*dtr)' 
'dtdx=cdiff(qv,x)/dx' ; 'dtdy=cdiff(qv,y)/dy'

'hadv=L*(u*dtdx+v*dtdy)' ;# HORIZONTAL ADVECTION

say 'MMMMM VERTICAL ADVCTION'
'define dtdz = (qv(z+1)-qv(z-1))/(lev(z+1)*100-lev(z-1)*100)'
'define vadv = L*w*dtdz'    ;# VERTICAL ADVECTION

say 'MMMMM TENDENCY'

#say 'MMMMM Q2'

'set gxout shade2'; 'color ${LEVS} ${KIND}' ;# SET COLOR BAR

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'd hadv'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2; y=(y1+y2)/2
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' HADV ${TIME} ${LEV}'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set xlab on';'set ylab on'

'set gxout shade2'; 'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'd vadv'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2; y=(y1+y2)/2
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' VADV ${TIME} ${LEV}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${HOST}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

#echo; echo MMMMM BACK-UP SCRIPTS
#ODIR= ; mkdir -vp $ODIR
#TMP=TMP_$(basename $0)
#echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
#echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
#echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
#BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
#rm -f $TMP
#echo MMMMM

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

#!/bin/bash

. ./gmtpar.sh

xrange=120/130; yrange=24.5/32; zrange=700/1000

FH=$1; FH=${FH:-FH00}; LEV=$2; LEV=${LEV:-975}

IN=${in:-LFM.${FH}_TEST_${LEV}.TXT}
if [ ! -f $IN ]; then echo Error in $0 : No such file, $IN; exit 1; fi

INDIR2=/work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/13.12.PRE_SST_ERA5
INFLE2=ERA5_ECS_SST_20220618.GRD
IN2=$INDIR2/$INFLE2
if [ ! -f $IN2 ]; then echo Error in $0 : No such file, $IN2; exit 1; fi

figdir="FIG"; mkd $figdir

FIG=${figdir}/$(basename $IN .TXT)_SST.ps #.ps

range=${xrange}/${yrange}
size=132.5/4.5
xanot=a2f1
yanot=a2f1
anot=${xanot}/${yanot}WsNe

CPT=$(basename $0.sh).CPT; makecpt -Cseis -T20/30/1 -I >$CPT
grdimage $IN2 -R$range -JQ$size -C$CPT -X1.5 -Y6 -P -K > $FIG

awk  '{if($1!="#" )print $5,$6}' $IN|\
psxy -R$range -JQ$size -Sc0.02 -G0 -O -K >> $FIG

pscoast -R$range -JQ -B$anot -Di -W3 -G200 -O -K >> $FIG



range=${xrange}/${zrange}
size=4.5/-1
xanot=a2f1; yanot=a100f50
anot=${xanot}:"Longitude":/${yanot}:"P${sp}[hPa]":WSne

awk  '{if($1!="#" )print $5,$7}' $IN|\
psxy -R$range -JX$size -Sc0.02 -G0 -X0 -Y-1.5 -O -K >> $FIG
psbasemap -R$range -JX -B$anot -O -K >> $FIG

xoffset=-0.5; yoffset=6; comment=

currentdir=`pwd`; now=`date -R`; host=`hostname`
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-0} << EOF -O >> $FIG
0 1.1 10 0 1 LM Indir2: ${INDIR2}
0 0.9 10 0 1 LM Infle2: ${INFLE2}
0 0.7 10 0 1 LM Input: ${IN}
0 0.5 10 0 1 LM ${currentdir}
0 0.3 10 0 1 LM $0 $@
0 0.1 10 0 1 LM ${FIG}
EOF

rm -f $CPT

echo Input : $IN; echo FIG : $FIG

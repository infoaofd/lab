module mod_input
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/TRAJECTORY/Cartesian.Forward.3D.Test01
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 16:26 on 01-15-2015.

use mod_velocity_data

contains

subroutine set_input_file

  character strm*500

  if(ni>mf)then
    print '(A)','Error in mod_velocity_data: number of velocity data files exceeds mf.'
    stop
  endif

  open(10,file=list_infile,action="read")
  do i=1,ni
    read(10,'(A)',err=9)infile_v(i)
    print '(A,A)','Input: ',trim(indir)//'/'//trim(infile_v(i))

    time_infile(i)=float(i-1)*dti
    print '(A,g14.6)','time[s]= ',time_infile(i)
    print *

  enddo !i
  close(10)
  print '(A)','Done subroutine set_input_file.'
  print *
  return

9 print '(A)','Error while reading list of velocity data files.'
  stop

end subroutine set_input_file



subroutine read_grads_ctl(input_file)
  character(len=*),intent(in)::input_file

  character(len=5000)::strm

  print '(A,A)','GrADS CTL File: ',trim(input_file)

  open(11,file=input_file,action="read")
  read(11,*)
  read(11,*)
  read(11,*)
  read(11,*)
  read(11,*)(xi(i),i=1,nx)
  read(11,*)
  read(11,*)(yi(i),i=1,ny)
  read(11,*)
  read(11,*)(zi(i),i=1,nz)
  close(11)

  do i=1,nx
    xi(i)=xi(i)*d2r !degree to radian
  enddo !i

  do j=1,ny
    yi(j)=yi(j)*d2r !degree to radian
  enddo !i

end subroutine



subroutine read_velocity(input_file,ui,vi,wi)

  character(len=*),intent(in)::input_file
  real,intent(inout),dimension(nx,ny,nz):: ui,vi,wi

  open(11,file=input_file,action="read",form="unformatted")

! LFM

! MSM: Order of reading variables in y direction is revered since 
!     the data in original netCDF file are stored southward from 
!     the northwest corner. 
  do k=1,nz
    read(11)((wi(i,j,k),   i=1,nx),j=1,ny) !LFM
!    read(11)((wi(i,j,k),   i=1,nx),j=ny,1,-1) !MSM
  enddo
  do k=1,nz
    read(11)((ui(i,j,k),   i=1,nx),j=1,ny) !LFM
!    read(11)((ui(i,j,k),   i=1,nx),j=ny,1,-1) !MSM
  enddo
  do k=1,nz
    read(11)((vi(i,j,k),   i=1,nx),j=1,ny) !LFM
!    read(11)((ui(i,j,k),   i=1,nx),j=ny,1,-1) !MSM
  enddo

  close(11)

  wi=wi/100.0 !Pa/s -> hPa/s

end subroutine

end module mod_input




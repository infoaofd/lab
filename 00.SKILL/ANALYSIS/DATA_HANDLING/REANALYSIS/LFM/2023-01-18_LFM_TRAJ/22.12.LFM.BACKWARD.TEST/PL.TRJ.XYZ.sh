#!/bin/bash

. ./gmtpar.sh

xrange=120/130; yrange=24.5/32; zrange=700/1000

FH=$1; FH=${FH:-FH00}; LEV=$2; LEV=${LEV:-975}

in=${in:-LFM.${FH}_TEST_${LEV}.TXT}
if [ ! -f $in ]; then echo Error in $0 : No such file, $in; exit 1; fi

figdir="FIG"; mkd $figdir

FIG=${figdir}/$(basename $in .TXT)_xyz.ps #.ps

range=${xrange}/${yrange}
size=132.5/4.5
xanot=a2f1
yanot=a2f1
anot=${xanot}/${yanot}WsNe

awk  '{if($1!="#" )print $5,$6}' $in|\
psxy -R$range -JQ$size -Sc0.02 -G0 -X1.5 -Y6 -P -K > $FIG

pscoast -R$range -JQ -B$anot -Di -W3 -G200 -O -K >> $FIG



range=${xrange}/${zrange}
size=4.5/-1
xanot=a2f1; yanot=a100f50
anot=${xanot}:"Longitude":/${yanot}:"P${sp}[hPa]":WSne

awk  '{if($1!="#" )print $5,$7}' $in|\
psxy -R$range -JX$size -Sc0.02 -G0 -X0 -Y-1.5 -O -K >> $FIG
psbasemap -R$range -JX -B$anot -O -K >> $FIG

xoffset=; yoffset=6
comment=
. ./note.sh

echo Input : $in; echo FIG : $FIG

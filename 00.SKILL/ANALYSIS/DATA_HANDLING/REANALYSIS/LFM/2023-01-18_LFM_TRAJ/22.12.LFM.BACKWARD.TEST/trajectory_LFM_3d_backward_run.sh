#!/bin/bash

ulimit -s unlimited

exe=$(basename $0 _run.sh)
if [ ! -f $exe ]; then Error in $0 : No such file, $exe exit 1; fi

FH=FH03; LEV=975 #850 #975
TRJ_RUN_NAME=LFM.${FH}_TEST_${LEV}

indir=/work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/12.22.PRE_LFM_NC_BIN/OUT_BIN/${FH}
if [ ! -d $indir ];then echo No such directory, $indir; exit 1; fi

namelist=namelist.${run_name}.txt
list_infile="list_infile_${run_name}.txt"


yr0=2022; mo0=06; dy0=18; hr0=03; mi0=00; sc0=00

cat << EOF > $namelist
&flow_field
indir="${indir}"
list_infile="${list_infile}"
ctl_file="${indir}/LFMP.ctl"
ni=23
nx=281
ny=316
nz=12
dti=3600
&end

&trajectory
x0=128 !125.0
y0=30
z0=${LEV}
yr0=${yr0}
mo0=${mo0}
dy0=${dy0}
hr0=${hr0}
mi0=${mi0}
sc0=${sc0}
t0=75600.0 !129600.0 !97200.0
dt=600.0
nmax=96
out_file="${TRJ_RUN_NAME}.TXT",
&end
EOF



first_input_file=${indir}/LFM.${FH}_${yr0}${mo0}${dy0}_${hr0}.dat
if [ ! -f $first_input_file ]; then
  echo Error in $0 : No such file, ${indir}/$first_input_file
  exit 1
fi

cat <<EOF >$list_infile
${first_input_file}
LFM.${FH}_20220618_04.dat
LFM.${FH}_20220618_05.dat
LFM.${FH}_20220618_06.dat
LFM.${FH}_20220618_07.dat
LFM.${FH}_20220618_08.dat
LFM.${FH}_20220618_09.dat
LFM.${FH}_20220618_10.dat
LFM.${FH}_20220618_11.dat
LFM.${FH}_20220618_12.dat
LFM.${FH}_20220618_13.dat
LFM.${FH}_20220618_14.dat
LFM.${FH}_20220618_15.dat
LFM.${FH}_20220618_16.dat
LFM.${FH}_20220618_17.dat
LFM.${FH}_20220618_18.dat
LFM.${FH}_20220618_19.dat
LFM.${FH}_20220618_20.dat
LFM.${FH}_20220618_21.dat
LFM.${FH}_20220618_22.dat
LFM.${FH}_20220618_23.dat
LFM.${FH}_20220619_00.dat
LFM.${FH}_20220619_01.dat
EOF



${exe} < $namelist






#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

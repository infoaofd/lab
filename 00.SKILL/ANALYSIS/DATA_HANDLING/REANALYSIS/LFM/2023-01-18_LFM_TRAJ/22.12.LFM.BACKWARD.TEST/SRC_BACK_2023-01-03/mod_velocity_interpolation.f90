module mod_velocity_interpolation
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/TRAJECTORY/Cartesian.Forward.Test02
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 09:58 on 01-09-2015.
use mod_velocity_data
use mod_input
contains

subroutine rkcoef(k, nc, u, v, w, dt, t, x, y, z)
  integer,intent(in)::nc
  real,intent(inout)::k(nc)
  real,intent(inout)::u,v,w
  real,intent(in)::dt,t,x,y,z

  call interpolate(t,x,y,z,u,v,w)

  k(1)=dt*u/(Re*cos(y))*(-1.0)
  k(2)=dt*v/Re*(-1.0)
  k(3)=dt*w*(-1.0)

end subroutine


subroutine set_input_velocity_field(t,tb,tf)
  real,intent(in)::t
  real,intent(inout):: tb,tf

  character(len=200)::fileb,filef
  character(len=500)::input_b,input_f
  real,save::tb_old=-1.E-32, tf_old=-1.E-32

  call neighboring_times(t, tb, tf, fileb, filef)

! ub,vb,wf
  if(tb_old/=tb)then
    input_b=trim(indir)//'/'//trim(fileb)
    call read_velocity(input_b,uib,vib,wib)
  endif

! uf,vf,wf
  if(tf_old/=tf)then
    input_f=trim(indir)//'/'//trim(filef)
    call read_velocity(input_f,uif,vif,wif)
  endif

  tb_old=tb
  tf_old=tf

end subroutine


subroutine interpolate(t,x,y,z,u,v,w)
  real,intent(in)::t,x,y,z
  real,intent(inout)::u,v,w

  real::tb,tf
  real::ub,uf,vb,vf,wb,wf

  call set_input_velocity_field(t,tb,tf)
  call interpolate_space(x,y,z,uib,vib,wib, ub,vb,wb)
  call interpolate_space(x,y,z,uif,vif,wif, uf,vf,wf)

  call interpolate_time(t,tb,tf,ub,uf,u)
  call interpolate_time(t,tb,tf,vb,vf,v)
  call interpolate_time(t,tb,tf,wb,wf,w)

end subroutine



subroutine neighboring_times(t, tb, tf, fileb, filef)
  real,intent(in)::t
  real,intent(inout)::tb,tf
  character(len=*),intent(inout)::fileb,filef

  tb=-1.E35
  tf=-1.E35

  do i=ni,2,-1

    if(t < time_infile(i) .and. t>=time_infile(i-1))then
      tb=time_infile(i-1)
      tf=time_infile(i)
      fileb=infile_v(i-1)
      filef=infile_v(i)
    endif

  enddo !i

  if(tb==-1.E35 .or. tf==-1.E35)then
    print *
    print '(A)','Error in neighboring_times (mod_velocity_interpolation): t is out of bounds'
    print '(A,g14.6)','t=',t
    print '(A,g14.6)','time_infile(1)=',time_infile(1)
    print '(A,g14.6)','time_infile(ni)=',time_infile(ni)
    print *
    stop -1
  endif

end subroutine



subroutine interpolate_space(x,y,z,ui,vi,wi,u,v,w)

  real,intent(in)::x,y,z
  real,intent(inout),dimension(nx,ny,nz)::ui,vi,wi
  real,intent(inout)::u,v,w


  call find_index(x,y,z,idx,jdx,kdx)

  ktop=kdx+1
  kbtm=kdx
  call bilinear(x,y,ui,idx,jdx,kbtm,ubtm)
  call bilinear(x,y,ui,idx,jdx,ktop,utop)
  call interpolate_z(z,zi(kbtm),zi(ktop),ubtm,utop,u)

  call bilinear(x,y,vi,idx,jdx,kbtm,vbtm)
  call bilinear(x,y,vi,idx,jdx,ktop,vtop)
  call interpolate_z(z,zi(kbtm),zi(ktop),vbtm,vtop,v)

  call bilinear(x,y,wi,idx,jdx,kbtm,wbtm)
  call bilinear(x,y,wi,idx,jdx,ktop,wtop)
  call interpolate_z(z,zi(kbtm),zi(ktop),wbtm,wtop,w)

end subroutine



subroutine find_index(x,y,z,idx,jdx,kdx)
  real,intent(in)::x,y,z
  integer,intent(inout)::idx,jdx,kdx

  idx=-999
  jdx=-999
  kdx=-999

  do i=1,nx-1
    if(x >=xi(i) .and. x<xi(i+1))then
      idx=i
    endif
  enddo !i
  if(idx==-999)then
    print *
    print '(A)','Error in find_index (mod_velocity_interpolation)'
    print '(A)','Invalid index in X direction.'
    print '(A,g14.6,3x,2g14.6)','x,xi(1),xi(nx)=',x,xi(1),xi(nx)
    print *
    stop -1
  endif

  do j=1,ny-1
    if(y >=yi(j) .and. y<yi(j+1))then
      jdx=j
    endif
  enddo !i
  if(jdx==-999)then
    print *
    print '(A)','Error in find_index (mod_velocity_interpolation)'
    print '(A)','Invalid index in Y direction.'
    print '(A,g14.6,3x,2g14.6)','y,yi(1),yi(ny)=',y,yi(1),yi(ny)
    print *
    stop -1
  endif

  do k=1,nz-1
    if(z <=zi(k) .and. z>zi(k+1))then
      kdx=k
    endif
  enddo !i
  if(kdx==-999)then
    print *
    print '(A)','Error in find_index (mod_velocity_interpolation)'
    print '(A)','Invalid index in Z direction.'
    print '(A,g14.6,3x,2g14.6)','z,zi(1),zi(nz)=',z,zi(1),zi(nz)
    print *
    stop -1
  endif


end subroutine



subroutine bilinear(x,y,ui,idx,jdx,kdx,us)
  real,intent(in)::x,y
  integer,intent(in)::idx,jdx,kdx
  real,intent(in)::ui(nx,ny,nz)
  real,intent(inout)::us

  real xa(4),ya(4),ua(4)
! Subscript, a indicates the input.
!
! us is estimated for given xa(i),ya(i),ua(i),x, and y.
!
! (xa(4),ya(4)), ua(4)   (xa(3),ya(3)), ua(3)
!        +------------------+
!        |                  |
!        |   us             |
!        |  (x,y)           |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        +------------------+
! (xa(1),ya(1)), ua(1)   (xa(2),ya(2)), ua(2)
! 
  i1=idx
  i2=idx+1
  j1=jdx
  j2=jdx+1

  kd=kdx

  xa(1)=xi(i1)
  xa(2)=xi(i2)
  xa(3)=xi(i2)
  xa(4)=xi(i1)

  ya(1)=yi(j1)
  ya(2)=yi(j1)
  ya(3)=yi(j2)
  ya(4)=yi(j2)

  ua(1)=ui(i1,j1,kd)
  ua(2)=ui(i2,j1,kd)
  ua(3)=ui(i2,j2,kd)
  ua(4)=ui(i1,j2,kd)

 t=(x-xa(1))/(xa(2)-xa(1))
 s=(y-ya(1))/(ya(4)-ya(1))

 us =  (1.-t)*(1.-s)*ua(1) &
&    + t*(1.-s)*ua(2) &
&    + t*s*ua(3) &
&    + (1.-t)*s*ua(4)

end subroutine



subroutine interpolate_time(t,tb,tf,ub,uf,u)
  real,intent(in)::t,tb,tf,ub,uf
  real,intent(inout)::u

  u=ub + (uf-ub)/(tf-tb)*(t-tb)

end subroutine



subroutine interpolate_z(z,zbtm,ztop,ubtm,utop,u)
  real,intent(in)::z,zbtm,ztop,ubtm,utop
  real,intent(inout)::u

  u=ubtm + (utop-ubtm)/(ztop-zbtm)*(z-zbtm)

end subroutine


end module

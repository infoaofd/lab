program trajectory_msm_3d_backward
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/TRAJECTORY/MSM.3D.Forward
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 11:25 on 01-20-2015.

  use mod_velocity_data
  use mod_velocity_interpolation

  character(len=500)::out_file

! Runge-Kutta method
  integer,parameter::nc=3
  real k1(nc),k2(nc),k3(nc),k4(nc)

  namelist/flow_field/indir,list_infile,ctl_file,ni,nx,ny,nz,dti
  namelist/trajectory/x0,y0,z0, yr0,mo0,dy0,hr0,mi0,sc0, t0,dt,nmax,&
& out_file

  read(*,nml=flow_field)
  allocate(xi(nx),yi(ny),zi(nz))
  allocate(uib(nx,ny,nz),vib(nx,ny,nz),wib(nx,ny,nz),&
& uif(nx,ny,nz),vif(nx,ny,nz),wif(nx,ny,nz))

  read(*,nml=trajectory)

  call read_grads_ctl(ctl_file) !in module mod_input
  call set_input_file !in module mod_velocity_data

  t=t0

! Note: x,y is in radians while computing backward trajectories
!       z is pressure in hPa.
  x=x0*d2r; y=y0*d2r; z=z0

  u=0;  v=0;  w=0
! u and v are in m/s.
! w is in hPa/s and positive downward.

  open(21,file=out_file)
  write(21,'(A,i4.4,1x,5(i2.2,1x))')'# Origin of time= ',yr0,mo0,dy0,hr0,mi0,sc0
  write(21,'(A,3g14.6)')'# x0,y0,z0= ',x0,y0,z0
  write(21,'(A,g14.6)')'# dt= ',dt
  write(21,'(A)')'# t, u, v, w, x, y, z'
  write(21,'(4g14.6,2g16.8,g14.6)')t, u, v, w, x*r2d, y*r2d, z

  print '(A)','# t,       u,        v,        w,        x,        y,        z'
  print '(4g10.2,2f11.4,f7.1)',   t, u, v, w, x*r2d, y*r2d, z

  do i=1,nmax

!   4th order Runge-Kutta method
    call rkcoef(k1, nc, u1, v1, w1, dt, t,        x,           y,           z)

    call rkcoef(k2, nc, u2, v2, w2, dt, t-dt*0.5, x+k1(1)*0.5, y+k1(2)*0.5, z+k1(3)*0.5)

    call rkcoef(k3, nc, u3, v3, w3, dt, t-dt*0.5, x+k2(1)*0.5, y+k2(2)*0.5, z+k2(3)*0.5)

    call rkcoef(k4, nc, u4, v4, w4, dt, t-dt,     x+k3(1),     y+k3(2),     z+k3(3)    )

    x=x+(k1(1)+2.0*(k2(1)+k3(1))+k4(1))/6.0
    u=u1

    y=y+(k1(2)+2.0*(k2(2)+k3(2))+k4(2))/6.0
    v=v1

    z=z+(k1(3)+2.0*(k2(3)+k3(3))+k4(3))/6.0
    w=w1

    t=t-dt

!   Note: Outputs of x and y are in degrees
    print '(4g10.2,2f11.4,f7.1)',   t, u, v, w, x*r2d, y*r2d, z
    write(21,'(4g14.6,2g16.8,g14.6)')t, u, v, w, x*r2d, y*r2d, z


  enddo !i

  print *
  print '(A,A)','Output file: ',trim(out_file)
  print *
  stop

end program

module mod_velocity_data
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/TRAJECTORY/Cartesian.Forward.Test03
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 10:08 on 01-09-2015.

!  use
!  implicit none


  character(len=500)::list_infile
  character(len=300)::indir

  integer,parameter::mf=1000
  integer::ni

  character(len=200)::infile_v(mf)
  real ::time_infile(mf)

  character(len=500)::ctl_file

  integer,public::nx,ny,nz
  real,allocatable,public:: xi(:),yi(:),zi(:)
! xi : Longitude
! yi : Latitude
! zi : Pressure [hPa]

  real,public:: dti !time increment of input data file

  real,allocatable,public,dimension(:,:,:)::uib,vib,wib,uif,vif,wif

  real,parameter,public::PI=3.141592653589793
  real,parameter,public::Re=6378.137*1.E3 ! Radius of the Earth
  real,parameter,public::d2r=PI/180.0, r2d=180.0/PI

end module mod_velocity_data

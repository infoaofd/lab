#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.fish.nagasaki-u.ac.jp
# Directory: /work1/am/TRAJECTORY/Runge.Kutta
#
# Revision history:
#  This file is created by /usr/local/mybin/nbscr.sh at 16:13 on 01-08-2015.

ulimit -s unlimited

exe=$(basename $0 _run.sh)
if [ ! -f $exe ]; then
  Error in $0 : No such file, $exe
  exit 1
fi

run_name=Baiu.Front.2011May.Stage2.Traj01

indir=/work1/am/TRAJECTORY/Preprocess.MSM/output
if [ ! -d $indir ]; then
  echo Error in $0 : No such directory, $indir
  exit 1
fi

namelist=${exe}.namelist.${run_name}.txt
list_infile="list_infile_${run_name}.txt"


yr0=2011
mo0=05
dy0=22
hr0=00
mi0=00
sc0=00

cat << EOF > $namelist
&flow_field
indir="${indir}"
list_infile="${list_infile}"
ctl_file="${indir}/MSMP.ctl"
ni=14
nx=241
ny=253
nz=16
dti=10800
&end

&trajectory
x0=128.0 !125.0
y0=28.0
z0=950.0
yr0=${yr0}
mo0=${mo0}
dy0=${dy0}
hr0=${hr0}
mi0=${mi0}
sc0=${sc0}
t0=129600.0 !97200.0
dt=600.0
nmax=72
out_file="${exe}.${run_name}.txt",
&end
EOF



first_input_file=${indir}/MSMP${yr0}${mo0}${dy0}_${hr0}.dat
if [ ! -f $first_input_file ]; then
  echo Error in $0 : No such file, ${indir}/$first_input_file
  exit 1
fi

cat <<EOF >$list_infile
${first_input_file}
MSMP20110522_03.dat
MSMP20110522_06.dat
MSMP20110522_09.dat
MSMP20110522_12.dat
MSMP20110522_15.dat
MSMP20110522_18.dat
MSMP20110522_21.dat
MSMP20110523_00.dat
MSMP20110523_03.dat
MSMP20110523_06.dat
MSMP20110523_09.dat
MSMP20110523_12.dat
MSMP20110523_15.dat
EOF



${exe} < $namelist






#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

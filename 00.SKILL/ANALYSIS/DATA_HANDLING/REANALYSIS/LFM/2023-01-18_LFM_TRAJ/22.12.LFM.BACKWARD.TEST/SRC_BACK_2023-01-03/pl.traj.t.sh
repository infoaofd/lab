#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.fish.nagasaki-u.ac.jp
# Directory: /work1/am/TRAJECTORY/Runge.Kutta
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 16:44 on 01-08-2015.

. ./gmtpar.sh
echo "Bash script $0 starts."

trange=-0.1/7.1
tanot=a2f1

urange=-60/60
uanot=a30f10

vrange=-60/60
vanot=a30f10

wrange=-0.2/0.2
wanot=a0.1f0.1

xrange=130/135
xanot=a1f1

yrange=30/35
yanot=a1f1

zrange=100/1000
zanot=a100f100


size=2.5/2.5
size_z=2.5/-2.5

s2h=3600

in=$1
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir="./fig/"
if [ ! -d ${figdir} ];then
  mkdir -p $figdir
fi
out=${figdir}$(basename $in .txt)_t.ps
echo Input : $in
echo Output : $out



range=$trange/$urange
anot=${tanot}:"t${sp}[h]":/${uanot}:"u${sp}[m/s]":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$2}' $in|\
psxy -R$range -JX$size -B$anot -Sc0.03 -G0 -X1.2 -Y8 -P -K > $out



range=$trange/$vrange
anot=${tanot}:"t${sp}[h]":/${vanot}:"v${sp}[m/s]":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$3}' $in|\
psxy -R$range -JX$size -B$anot -Sc0.03 -G0 -X0    -Y-3.5 -O -K >> $out



range=$trange/$wrange
anot=${tanot}:"t${sp}[h]":/${wanot}:"@~w@~${sp}[hPa/s]":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$4}' $in|\
psxy -R$range -JX$size -B$anot -Sc0.03 -G0 -X0    -Y-3.5 -O -K >> $out



range=$trange/$xrange
anot=${tanot}:"t${sp}[h]":/${xanot}:"Long.":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$5}' $in|\
psxy -R$range -JX$size -B$anot -Sc0.03 -G0 -X3.7  -Y7 -O -K >> $out



range=$trange/$yrange
anot=${tanot}:"t${sp}[h]":/${yanot}:"Lat.":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$6}' $in|\
psxy -R$range -JX$size -B$anot -Sc0.03 -G0 -X0   -Y-3.5 -O -K >> $out



range=$trange/$zrange
anot=${tanot}:"t${sp}[h]":/${zanot}:"p${sp}[hPa]":WSne

awk -v s2h=${s2h} '{if($1!="#")print $1/s2h,$7}' $in|\
psxy -R$range -JX$size_z -B$anot -Sc0.03 -G0 -X0   -Y-3.5 -O -K >> $out



xoffset=-3.7
yoffset=9.4
comment=
. ./note.sh

echo "Done $0"

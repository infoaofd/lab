&flow_field
indir="/work1/am/TRAJECTORY/Preprocess.MSM/output"
list_infile="list_infile_Baiu.Front.2011May.Stage2.Traj01.txt"
ctl_file="/work1/am/TRAJECTORY/Preprocess.MSM/output/MSMP.ctl"
ni=14
nx=241
ny=253
nz=16
dti=10800
&end

&trajectory
x0=128.0 !125.0
y0=28.0
z0=950.0
yr0=2011
mo0=05
dy0=22
hr0=00
mi0=00
sc0=00
t0=129600.0 !97200.0
dt=600.0
nmax=72
out_file="trajectory_msm_3d_backward.Baiu.Front.2011May.Stage2.Traj01.txt",
&end

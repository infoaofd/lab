#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.fish.nagasaki-u.ac.jp
# Directory: /work1/am/TRAJECTORY/Runge.Kutta
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 16:44 on 01-08-2015.

. ./gmtpar.sh
echo "Bash script $0 starts."

xrange=122/129
yrange=24/31
range=${xrange}/${yrange}
size=132.5/5
xanot=a1f1
yanot=a1f1
anot=${xanot}/${yanot}WSne

in=$1
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir="./fig/"
if [ ! -d ${figdir} ];then
  mkdir -p $figdir
fi
out=${figdir}$(basename $in .txt)_xy.ps

echo Input : $in
echo Output : $out

awk  '{if($1!="#" )print $5,$6}' $in|\
psxy -R$range -JQ$size -Sc0.02 -G0 -X1.5 -Y1.5 -P -K > $out

pscoast -R$range -JQ -B$anot -Di -W3 -G200 -O -K >> $out

xoffset=
yoffset=5.5
comment=
. ./note.sh

echo "Done $0.sh"

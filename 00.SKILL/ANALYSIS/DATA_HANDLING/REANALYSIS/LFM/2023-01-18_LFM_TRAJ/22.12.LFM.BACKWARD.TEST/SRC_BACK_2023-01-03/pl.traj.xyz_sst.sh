#!/bin/bash
# Description:
#
# Author: am
#
# Host: aofd165.fish.nagasaki-u.ac.jp
# Directory: /work1/am/TRAJECTORY/Runge.Kutta
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 16:44 on 01-08-2015.

. ./gmtpar.sh
echo "Bash script $0 starts."

sst_dir=/work1/am/SST.Plots/MGDSST
sst_file=mgdsst.20110522.txt

input_file_prefix=trajectory_msm_3d_backward.Baiu.Front.2011May
input_list="\
${input_file_prefix}.Stage1.Traj01.txt \
${input_file_prefix}.Stage2.Traj01.txt \
"

xrange=122/129
yrange=24/31
zrange=900/1000


figdir="./fig/"
if [ ! -d ${figdir} ];then
  mkdir -p $figdir
fi
out=${figdir}${input_file_prefix}_xyz_sst.ps



range=${xrange}/${yrange}
size=132.5/5
xanot=a1f1
yanot=a1f1
anot=${xanot}/${yanot}WsNe

input=${sst_dir}/${sst_file}
grd=$(basename ${input} .txt).grd

awk '{print $1,$2,$3-273.15}' $input |\
surface -R$range  -T0.5 -I0.03125 -G$grd
grdcontour $grd -R$range -JQ$size -W3 -C1 -A2f12 -G1/2 -L0/23 \
-Gl127.5/24/127.5/35 \
-X1.5 -Y4 -P -K > $out

grdcontour $grd -R$range -JQ$size -W6 -C1 -A2f12 -G1/2 -L23.9/24.1 \
-Gl127.5/24/127.5/35 \
-O -K >> $out

grdcontour $grd -R$range -JQ$size -W3 -C1 -A2f12 -G1/2 -L25/35 \
-Gl127.5/24/127.5/35 \
-O -K >> $out



for input in $input_list; do
if [ ! -f $input ]; then
  echo Error in $0 : No such file, $input
  exit 1
fi
echo Input : $input

awk  '{if($1!="#" )print $5,$6}' $input|\
psxy -R$range -JQ$size -Sc0.06 -G${white} -O -K >>$out

awk  '{if($1!="#" )print $5,$6}' $input|\
psxy -R$range -JQ$size -Sc0.03 -G0 -O -K >>$out

done

pscoast -R$range -JQ -B$anot -Di -W3 -G200 -O -K >> $out



range=${xrange}/${zrange}
size=5/-2
xanot=a1f1
yanot=a50f10
anot=${xanot}:"Longitude":/${yanot}:"P${sp}[hPa]":WSne

psbasemap -R$range -JX$size -B$anot -X0 -Y-2.5 -O -K >> $out

for input in $input_list; do
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $input
  exit 1
fi
echo Input : $input
awk  '{if($1!="#" )print $5,$7}' $input|\
psxy -R$range -JX$size -Sc0.02 -G0 -O -K >> $out

done

echo Output : $out

xoffset=
yoffset=8
comment=
in=$input_file_prefix
. ./note.sh

rm -vf $grd
echo "Done $0.sh"

# #!/bin/bash
# BACK UP of ./CHK.BIN.sh
# Mon, 02 Jan 2023 20:27:45 +0900
# /work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/12.22.PRE_LFM_NC_BIN
# CHK.BIN.sh
# 
#!/bin/bash

# Mon, 02 Jan 2023 19:54:39 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/12.22.PRE_LFM_NC_BIN

LEV=$1; LEV=${LEV:-850}
FH=$2; FH=${FH:-FH03}
YYYYMMDDHH=$3; YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

CTL=OUT_BIN/LFMP.ctl
if [ ! -f $CTL ];then echo ERROR in $: NO SUCH FILE,$CTL;exit1;fi

GS=$(basename $0 .sh).GS

# LONW= ;LONE= ; LATS= ;LATN=

FIGDIR=FIG_CHK; mkd $FIGDIR
FIG=${FIGDIR}/$(basename $0 .sh)_${FH}_${LEV}_${YYYY}-${MM}-${DD}_${HH}.pdf

LEVS="-10 10 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
FS=2; UNIT=Pa/s

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off'
'set xlint 2';'set ylint 2'
'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'd w'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u,20);v -SCL 0.5 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yb-0.8
'set cthick  3'; 'set ccolor  1'
'vec.gs skip(u,10);v -SCL 0.5 30 -P 'xx' 'yy' -SL m/s'



x=(xl+xr)/2 ;# TEXT
y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' BIN ${FH} ${HH}UTC${DD}${MMM}${YYYY} ${LEV}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo MMMMM BACK-UP SCRIPTS
ODIR=${FIGDIR} ; mkdir -vp $ODIR
TMP=TMP_$(basename $0)
echo "# #!/bin/bash" >   $TMP; echo "# BACK UP of $0" >>$TMP
echo "# $(date -R)" >>$TMP; echo "# $(pwd)"    >>$TMP
echo "# $(basename $0)">>$TMP; echo "# "       >>$TMP
BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
rm -f $TMP
echo MMMMM

echo
if [ -f $FIG ];then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

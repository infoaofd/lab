#!/bin/bash
# am@p5820
# /work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/12.22.PRE_LFM_NC_BIN
# 2023-01-02_18-00

EXE=$(basename $0 .sh).EXE
if [ ! -f $EXE ];then echo ERROR: NO SUCH FILE,$EXE; exit 1;fi

FH=$1; FH=${FH:-FH03}
yyyy=$2; mm=$3; dd=$4; hh=$5
yyyy=${yyyy:-2022}; mm=${mm:-06}; dd=${dd:-19}; hh=${hh:-00}

runname=LFM.${FH}
INDIR1="/work01/DATA/LFM_HCUT.PDEL/${FH}"
INFLE1="LFM_PRS_${FH}_VALID_${yyyy}-${mm}-${dd}_${hh}.nc"
IN1=$INDIR1/$INFLE1
if [ ! -f $IN1 ];then echo ERROR in $0: NO SUCH FILE,$IN1;exit 1;fi

iyyyy=2022; imm=06; idd=01
outdir=OUT_BIN/${FH}; mkd $outdir
OPREFIX=${runname}

mkd $outdir

namelist=NAMELIST.${runname}.TXT
cat <<EOF > $namelist
&para
iyyyy=${iyyyy}
imm=${imm}
idd=${idd}
yyyy=${yyyy}
mm=${mm}
dd=${dd}
hh=${hh}
in_msmp="${IN1}"
outdir="${outdir}"
OPREFIX="${OPREFIX}"
&end
EOF

echo ${EXE} < $namelist
${EXE} < $namelist

echo "Done $0"
echo





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

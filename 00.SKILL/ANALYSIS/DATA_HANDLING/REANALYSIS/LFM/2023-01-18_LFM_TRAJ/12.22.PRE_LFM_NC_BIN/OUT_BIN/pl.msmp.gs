#
function plmsm()

in='MSMP.ctl'

'allclose'

'open 'in
'c'

lev='950'
time='00Z22May2011'

'set lev 'lev
'set time 'time

'q file'
say result
'q dims'
say result
line=sublin(result,5)
hhddmmmyyyy=subwrd(line,6)
hh=substr(hhddmmmyyyy,1,2)
dd=substr(hhddmmmyyyy,4,2)
mmm=substr(hhddmmmyyyy,6,3)
if(mmm='APR')
 mm=04
endif
if(mmm='MAY')
 mm=05
endif
if(mmm='JUN')
 mm=06
endif
yyyy=substr(hhddmmmyyyy,9,4)
line=sublin(result,4)
press=subwrd(line,6)

'set mpdset hires'
'set gxout shade2'
'rgbset2'
'set clevs -2 -1 -0.5 -0.1 0.1 0.5 1 2'
'd w'
'cbarn'

'set gxout vector'
'd skip(u,10,10);skip(v,10,10)'

out=yyyy''mm''dd'_'hh'_'press'hPa.eps'
'print 'out


return

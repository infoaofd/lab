#!/bin/bash

. ./gmtpar.sh

xrange=120/130; yrange=24.5/32; zrange=700/1000

INDIR=/work03/am/2022.06.ECS.OBS/26.14.LFM.NCL.pt2/32.12.LFM_TRAJ/13.12.PRE_SST_ERA5
INFLE=ERA5_ECS_SST_20220618.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ]; then echo Error in $0 : No such file, $IN; exit 1; fi

figdir="FIG"; mkd $figdir

FIG=$(basename $IN .nc).ps #.ps

range=${xrange}/${yrange}
size=132.5/4.5
xanot=a2f1
yanot=a2f1
anot=${xanot}/${yanot}WsNe

XYZ=$(basename $IN .nc).TXT;GRD=$(basename $IN .nc).GRD

cdo gmtxyz $IN|awk '{if($1!="#" && $3>-999.) printf "%.4f %.4f %.2f\n", $1, $2, $3-273}' > $XYZ
 
xyz2grd -R$range -I0.25/0.25 -G$GRD $XYZ 

grdcontour $GRD -R$range -JQ$size -A1f12 -C1 -X1.5 -Y6 -P -K > $FIG

pscoast -R$range -JQ -B$anot -Di -W3 -G200 -O -K >> $FIG


xoffset=; yoffset=6
comment=
. ./note.sh

rm -f $XYZ #$GRD
echo
echo Input : $IN; echo FIG : $FIG
echo GRD: $GRD; echo

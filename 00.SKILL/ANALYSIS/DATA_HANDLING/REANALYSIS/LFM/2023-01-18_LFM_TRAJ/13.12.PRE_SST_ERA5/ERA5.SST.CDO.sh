#!/bin/bash
YYYY=2022;MM=06;DD=18;YMD=${YYYY}${MM}${DD}
INDIR=/work01/DATA/ERA5/ECS/01HR/$YYYY/$MM
INFLE=ERA5_ECS_SFC.FLUX_01HR_${YMD}.grib
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN; exit 1; fi
OUT=ERA5_ECS_SST_${YMD}.nc

cdo showname $IN; echo

TMP1=TMP1_$(basename $0 .sh).nc
TMP2=TMP2_$(basename $0 .sh).nc

cdo -f nc4 select,'param=34.128' $IN $TMP1
cdo -f nc4 timmean $TMP1 $OUT

rm -f $TMP1 #$TMP2

echo
cdo sinfo $OUT
cdo showname $OUT
ls -lh $OUT
echo





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

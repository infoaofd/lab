# Print time, current working directory and output filename

export LANG=C

currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
#comment=" "
time1=$(ls -l $IN | awk '{print $6, $7, $8}')
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-0} << EOF -O >> $FIG
0 0.7 10 0 1 LM Input: ${IN} (${time1})
0 0.5 10 0 1 LM ${currentdir}
0 0.3 10 0 1 LM $0 $@
0 0.1 10 0 1 LM ${FIG}
EOF
#  Output: ${out}

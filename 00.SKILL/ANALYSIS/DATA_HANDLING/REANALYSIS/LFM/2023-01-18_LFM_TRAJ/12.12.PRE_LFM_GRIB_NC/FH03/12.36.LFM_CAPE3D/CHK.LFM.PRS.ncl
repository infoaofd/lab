FH="FH03"
INDIR1="/work01/DATA/LFM_HCUT.PDEL/"+FH+"/"
INFLE1="LFM_PRS_"+FH+"_VALID_2022-06-19_03.nc"

IN=INDIR1+INFLE1

a=addfile(IN,"r")

temp=a->temp
rh=a->rh

printVarSummary(temp)
printVarSummary(rh)


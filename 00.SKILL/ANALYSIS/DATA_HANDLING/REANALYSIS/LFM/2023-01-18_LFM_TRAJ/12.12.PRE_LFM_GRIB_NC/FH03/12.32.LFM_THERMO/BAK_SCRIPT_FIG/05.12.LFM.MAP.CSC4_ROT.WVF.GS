
'open LFM.CTL' ;#'q ctlinfo'; say result
'open LFM_ROT_1.CTL'; 'open LFM_ROT_2.CTL'; 'open LFM_ROT_3.CTL'; 'open LFM_ROT_4.CTL'

'cc'; 'set grid off'



say 'MMMMM SET FIGURE SIZE'
'set vpage 0.0 11.5 0 8.5' ;#0.0 8.5 0 10.5'

xmax=2; ymax=2; xleft=0.2; ytop=7

xwid =  6/xmax; ywid =  5/ymax
xmargin=0.4; xmargin2=0.6; ymargin=1



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 1 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'

'set lon 122.0 134'; 'set lat 24.5 35'
'set time 16Z19JUN2022'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev '
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

say 'CONTOUR VPT.1 950'
'set lev 950'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint 1'
'q dims'; say sublin(result,4)
'd VPT.1'

'set xlab off';'set ylab off'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint 1'
'd VPT.1'

'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs 304'
'd VPT.1'


say 'MMMMM VECTOR 950'
'set gxout vector'; 'set cthick 1'
'set arrscl 0.5 40'; 'set arrlab off'
'set lev 950'
'q dims'; say sublin(result,4)

'set ccolor 0'; 'set cthick 6'
'vec skip(u,25,25);v -SCL 0.5 40 -P 20 20'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set ccolor 1'; 'set cthick 3'
'vec skip(u,25,25);v -SCL 0.5 40 -P 'xx' 'yy' -SL m/s'


say 'MMMMM LINE'
'trackplot 124 26.7 130 31.5 -c 1 -l 1 -t 4'
'trackplot 124.72 25.8 130.72 30.6 -c 1 -l 1 -t 4'
'trackplot 128.5 25.8 128.5 30.6 -c 1 -l 1 -t 4'
'trackplot 130.2 25.8 130.2 30.6 -c 1 -l 1 -t 4'


say 'MMMMM VECTOR 850'
'set gxout vector'
'set arrscl 0.5 40'; 'set arrlab off'
'set ccolor 0'; 'set cthick 6'
'set lev 850'
'q dims'; say sublin(result,4)

#'set ccolor 0'; 'set cthick 5'
#'vec skip(u,25,25);v -SCL 0.5 40 -P 20 20'
#'set rgb 97 120 120 120'

'set ccolor 14'; 'set cthick 2'
'vec skip(u,25,25);v -SCL 0.5 40 -P 20 20'


say 'MMMMM CONTOUR VPT.1 304'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs 304'
'set lev 950'
'd VPT.1'

say 'MMMMM MAUL'
'set gxout contour'; 'set cthick 10'; 
'set ccolor 98'
#'set clevs 1'
'set lev 850'
'q dims'; say sublin(result,4)
'd MAUL.1'

say 'NNNNN LINE NAME'
'q w2xy 124 26.7'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 
'set string 0 c 10'; 'draw string 'xx-0.05' 'yy-0.05' A'
'set string 1 c 3'; 'draw string 'xx-0.05' 'yy-0.05' A'

'q w2xy 124.72 25.8'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'set string 0 c 10'; 'draw string 'xx-0.05' 'yy-0.05' B'
'set string 1 c 3'; 'draw string 'xx-0.05' 'yy-0.05' B'

'q w2xy 128.5 25.8'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'set string 0 c 10'; 'draw string 'xx-0.05' 'yy-0.05' C'
'set string 1 c 3'; 'draw string 'xx-0.05' 'yy-0.05' C'

'q w2xy 130.2 25.8'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'set string 0 c 10'; 'draw string 'xx-0.05' 'yy-0.05' D'
'set string 1 c 3'; 'draw string 'xx-0.05' 'yy-0.05' D'


'set strsiz 0.1 0.12'; 'set string 1 c 3'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild 16UTC19JUN2022 (FH00)'
'set string 1 l 3'
xx=xl-0.3; yy=yy+0.22; 'draw string 'xx' 'yy' VPT.1(950) MAUL.1(850)'
XSLEFT=xl; YTTOP=yt



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 2 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time 16Z19JUN2022'
'set grads off'

line = A
lon1 = 124; lon2 = 130; lat1 = 26.7; lat2 = 31.5
YM=240
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev 1000 500' ;# 'set zlog on'

'collect 2 free'; 'collect 3 free'; 'collect 4 free'
'collect 5 free'; 'collect 6 free'; 'collect 7 free'; 
'collect 8 free'; 'collect 9 free' 


lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 2 gr2stn(VPT.1,'lon','lat')'
  'collect 3 gr2stn(MAUL.1,'lon','lat')'
  'collect 4 gr2stn(RH,'lon','lat')'
  'collect 5 gr2stn(wvxr.2,'lon','lat')'
  'collect 6 gr2stn(wvyr.2,'lon','lat')'
  'collect 7 gr2stn(dum0.2,'lon','lat')'
  lat = lat + 0.02
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=1; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

say; say 'MMMM WVF(ALONG) CROSS SECTION'
'set gxout shade2'
'color 0.05 0.4 0.05 -kind white->lightyellow->wheat->gold->orange->tomato->red->firebrick'
'd coll2gr(5,-u)' ;# ALONG
#'d mag(coll2gr(5,-u),coll2gr(6,-u))' ;# MAG

'set xlab off';'set ylab off'

'set gxout contour'
'set clab off'; 'set cint 2'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(2,-u)'

'set clab off'; 'set cint 2'; 'set ccolor 15'; 'set cthick 2'
'd coll2gr(2,-u)'

'set clab on'; 'set clevs 300 304 308 312 316 320 324 328 332'; 'set ccolor 15'; 'set cthick 2'; 'set clopts 15 2 0.07'
'd coll2gr(2,-u)'

say; say 'MMMMM WVF CROSS SECTION ALONG COMP.'
'set gxout vector'
'set cthick 5'; 'set ccolor  0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.7; yy=yb-0.27
'set cthick  2'; 'set ccolor  1';'set strsiz 0.08 0.1';'set string 1 c 2 0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 'xx' 'yy' -SL kg/m`a2`n/s'

say; say 'MMMMM WVF CROSS SECTION ACROSS COMP.'
'set gxout contour'
'set clab off'; 'set cint 0.05'; 'set ccolor 0'; 'set cthick 4'
'd coll2gr(6,-u)'
'set clab on'; 'set cint 0.05'; 'set cthick 1'
'set ccolor 2'; 'set clopts 2 2 0.07'
'd coll2gr(6,-u)'


say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set rgb 98 128 128 128 -75' ; 'set ccolor 98';'set cthick 10'
#'set cint '
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs 95'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs 95'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Valid 16UTC19JUN2022 (FH00)'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' A (' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' Latitude North'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' Pressure [hPa]'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 3 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time 16Z19JUN2022'
'set grads off'

line = B
lon1 = 124.72; lon2 = 130.72; lat1 = 25.8; lat2 = 30.6
YM=240
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev 1000 500' ;# 'set zlog on'

'collect 2 free'; 'collect 3 free'; 'collect 4 free'
'collect 5 free'; 'collect 6 free'; 'collect 7 free'; 

lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 2 gr2stn(VPT.1,'lon','lat')'
  'collect 3 gr2stn(MAUL.1,'lon','lat')'
  'collect 4 gr2stn(RH,'lon','lat')'
  'collect 5 gr2stn(wvxr.3,'lon','lat')'
  'collect 6 gr2stn(wvyr.3,'lon','lat')'
  'collect 7 gr2stn(dum0.3,'lon','lat')'
  lat = lat + 0.02
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=1; xmap=3

xs = xleft + (xwid+xmargin2)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

say; say 'MMMM WVF(ALONG) CROSS SECTION'
'set gxout shade2'
'color 0.05 0.4 0.05 -kind white->lightyellow->wheat->gold->orange->tomato->red->firebrick'
'd coll2gr(5,-u)' ;# ALONG
#'d mag(coll2gr(5,-u),coll2gr(6,-u))' ;# MAG

'set xlab off';'set ylab off'

say; say 'MMMM VPT.1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint 2'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(2,-u)'

'set clab off'; 'set cint 2'; 'set ccolor 15'; 'set cthick 2'
'd coll2gr(2,-u)'

'set clab on'; 'set clevs 300 304 308 312 316 320 324 328 332'; 'set ccolor 15'; 'set cthick 2'; 'set clopts 15 2 0.07'
'd coll2gr(2,-u)'



say; say 'MMMMM WVF CROSS SECTION ALONG COMP.'
'set gxout vector'
'set cthick 5'; 'set ccolor  0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.7; yy=yb-0.27
'set cthick  2'; 'set ccolor  1';'set strsiz 0.08 0.1';'set string 1 c 2 0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 'xx' 'yy' -SL kg/m`a2`n/s'

say; say 'MMMMM WVF CROSS SECTION ACROSS COMP.'
'set gxout contour'
'set clab off'; 'set cint 0.05'; 'set ccolor 0'; 'set cthick 4'
'd coll2gr(6,-u)'
'set clab on'; 'set cint 0.05'; 'set cthick 1'
'set ccolor 2'; 'set clopts 2 2 0.07'
'd coll2gr(6,-u)'

say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint '
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs 95'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs 95'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Valid 16UTC19JUN2022 (FH00)'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' B (' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' Latitude North'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' Pressure [hPa]'



say 'mmmmm COLOR BAR'
x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 1 -ft 3 -line on -edge circle'
xx=x1-0.05; yy=y2+0.15; 'set string 1 l 2 0';'set strsiz 0.08 0.1';
'draw string ' xx ' ' yy ' kg/m`a2`n/s'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 4 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time 16Z19JUN2022'
'set grads off'

line = C
lon1 = 128.5; lon2 = 128.5; lat1 = 25.8; lat2 = 30.6
YM=240
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev 1000 900' ;# 'set zlog on'

'collect 2 free'; 'collect 3 free'; 'collect 4 free'
'collect 5 free'; 'collect 6 free'; 'collect 7 free'; 

lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 2 gr2stn(VPT.1,'lon','lat')'
  'collect 3 gr2stn(MAUL.1,'lon','lat')'
  'collect 4 gr2stn(RH,'lon','lat')'
  'collect 5 gr2stn(wvxr.4,'lon','lat')'
  'collect 6 gr2stn(wvyr.4,'lon','lat')'
  'collect 7 gr2stn(dum0.4,'lon','lat')'
  lat = lat + 0.02
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=2; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid/2

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

say; say 'MMMM WVF(ALONG) CROSS SECTION'
'set gxout shade2'
'color 0.05 0.4 0.05 -kind white->lightyellow->wheat->gold->orange->tomato->red->firebrick'
'd coll2gr(5,-u)' ;# ALONG
#'d mag(coll2gr(5,-u),coll2gr(6,-u))' ;# MAG

'set xlab off';'set ylab off'

say; say 'MMMM VPT.1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint 2'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'

'set clab off'; 'set cint 2'; 'set ccolor 15'; 'set cthick 2'
'd coll2gr(2,-u)'

'set clab on'; 'set clevs 300 304 308 312 316 320 324 328 332'; 'set ccolor 15'; 'set cthick 2'; 'set clopts 15 2 0.07'
'd coll2gr(2,-u)'

say; say 'MMMMM WVF CROSS SECTION ALONG COMP.'
'set gxout vector'
'set cthick 5'; 'set ccolor  0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.7; yy=yb-0.27
'set cthick  2'; 'set ccolor  1';'set strsiz 0.08 0.1';'set string 1 c 2 0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 'xx' 'yy' -SL kg/m`a2`n/s'

say; say 'MMMMM WVF CROSS SECTION ACROSS COMP.'
'set gxout contour'
'set clab off'; 'set cint 0.05'; 'set ccolor 0'; 'set cthick 4'
'd coll2gr(6,-u)'
'set clab on'; 'set cint 0.05'; 'set cthick 1'
'set ccolor 2'; 'set clopts 2 2 0.07'
'd coll2gr(6,-u)'

say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint '
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs 95'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs 95'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Valid 16UTC19JUN2022 (FH00)'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' C (' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' Latitude North'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' Pressure [hPa]'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 5 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time 16Z19JUN2022'
'set grads off'

line = D
lon1 = 130.2; lon2 = 130.2; lat1 = 25.8; lat2 = 30.6
YM=240
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev 1000 900' ;# 'set zlog on'

'collect 2 free'; 'collect 3 free'; 'collect 4 free'
'collect 5 free'; 'collect 6 free'; 'collect 7 free'; 

lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 2 gr2stn(VPT.1,'lon','lat')'
  'collect 3 gr2stn(MAUL.1,'lon','lat')'
  'collect 4 gr2stn(RH,'lon','lat')'
  'collect 5 gr2stn(wvxr.5,'lon','lat')'
  'collect 6 gr2stn(wvyr.5,'lon','lat')'
  'collect 7 gr2stn(dum0.5,'lon','lat')'
  lat = lat + 0.02
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=2; xmap=3

xs = xleft + (xwid+xmargin2)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid/2

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

say; say 'MMMM WVF(ALONG) CROSS SECTION'
'set gxout shade2'
'color 0.05 0.4 0.05 -kind white->lightyellow->wheat->gold->orange->tomato->red->firebrick'
'd coll2gr(5,-u)' ;# ALONG
#'d mag(coll2gr(5,-u),coll2gr(6,-u))' ;# MAG

'set xlab off';'set ylab off'

say; say 'MMMM VPT.1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint 2'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'

'set clab off'; 'set cint 2'; 'set ccolor 15'; 'set cthick 2'
'd coll2gr(2,-u)'

'set clab on'; 'set clevs 300 304 308 312 316 320 324 328 332'; 'set ccolor 15'; 'set cthick 2'; 'set clopts 15 2 0.07'
'd coll2gr(2,-u)'



say; say 'MMMMM WVF CROSS SECTION ALONG COMP.'
'set gxout vector'
'set cthick 5'; 'set ccolor  0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.7; yy=yb-0.27
'set cthick  2'; 'set ccolor  1';'set strsiz 0.08 0.1';'set string 1 c 2 0'
'vec.gs skip(coll2gr(5,-u),25,1);coll2gr(7,-u) -SCL 0.5 0.5 -P 'xx' 'yy' -SL kg/m`a2`n/s'

say; say 'MMMMM WVF CROSS SECTION ACROSS COMP.'
'set gxout contour'
'set clab off'; 'set cint 0.05'; 'set ccolor 0'; 'set cthick 4'
'd coll2gr(6,-u)'
'set clab on'; 'set cint 0.05'; 'set cthick 1'
'set ccolor 2'; 'set clopts 2 2 0.07'
'd coll2gr(6,-u)'

say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint '
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs 95'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs 95'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Valid 16UTC19JUN2022 (FH00)'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' D (' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' Latitude North'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' Pressure [hPa]'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM HEADER MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
xx = XSLEFT - 0.2; yy=YTTOP-0.1
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
yy = yy+0.8; 'draw string ' xx ' ' yy ' FH00//LFM_FH00_061916_CSC4_ROT.WVF_A_B_C_D.pdf'
yy = yy+0.23; 'draw string ' xx ' ' yy ' /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/12.46.NCL.ROT_4LINE_NORMAL'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ./05.12.LFM.MAP.CSC4_ROT.WVF.sh 00 2022061916 A 124 26.7 130 31.5 B 124.72 25.8 130.72 30.6 C 128.5 25.8 128.5 30.6 D 130.2 25.8 130.2 30.6 00 2022061916 A 124 26.7 130 31.5 B 124.72 25.8 130.72 30.6 C 128.5 25.8 128.5 30.6 D 130.2 25.8 130.2 30.6'
yy = yy+0.23; 'draw string ' xx ' ' yy ' Mon, 19 Dec 2022 20:59:24 +0900'

'gxprint FH00//LFM_FH00_061916_CSC4_ROT.WVF_A_B_C_D.pdf'

'quit'

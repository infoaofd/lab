#!/bin/bash

FH=$1; FH=${FH:-FH00}
YYYYMMDDHH=$2; YYYYMMDDHH=${YYYYMMDDHH:-2022061900}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

INDIR=/work01/DATA/LFM_PROC_ROT_v3/$FH
INFLE=LFM_PRS_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_CAPE2D.nc

IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

GS=$(basename $0 .sh).GS
FIGDIR=CAPE2D/${FH}; mkd $FIGDIR; FIGFLE=$(basename $IN .nc).pdf; FIG=$FIGDIR/$FIGFLE

LEVS1="0 3 0.2";FS1=5
KIND1='white->lightcyan->deepskyblue->green->yellow->orange->red->crimson'

LEVS2="0 1 0.2";FS2=2
KIND2='white->lightcyan->deepskyblue->green->yellow->orange->red->crimson'

LEVS3="0 4 0.5";FS3=2
KIND3='white->lightcyan->deepskyblue->green->yellow->orange->red->crimson'

LEVS4="0 4 0.5";FS4=2
KIND4='white->lightcyan->deepskyblue->green->yellow->orange->red->crimson'

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 4; ymax = 1

ytop=9

xwid = 6.5/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

#while (ymap <= ymax)
#while (xmap <= xmax)


# SET PAGE
'set vpage 0.0 8.5 0.0 11'

'cc'
'set grads off';'set grid off'
'set mpdset hires'
'set xlevs 122 128';'set ylevs 26 30 34'
'set xlopts 1 2 0.09'; 'set ylopts 1 2 0.09'; 
'set map 1 1 1 '

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'



var='LFC'; unit='[km]'

'set gxout shade2'; 'color ${LEVS3} -kind ${KIND3}' ;# SET COLOR BAR

nmap = 1; xmap = 1; ymap = 1
xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'd 'var'/1000'

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr; y1=yb-0.3; y2=y1+0.07 ;# LEGEND COLOR BAR

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.07 -fh 0.1 -fs $FS3 -ft 3 -line on -edge circle'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.15; 'draw string 'x' 'y' 'var' 'unit
x=(xl+xr)/2; y=y+0.18; 'draw string 'x' 'y' ${TIME}'



var='LCL'; unit='[km]'

'set gxout shade2'; 'color ${LEVS4} -kind ${KIND4}' ;# SET COLOR BAR

nmap = 1; xmap = 2; ymap = 1
xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'd 'var'/1000'

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr; y1=yb-0.3; y2=y1+0.07 ;# LEGEND COLOR BAR

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.07 -fh 0.1 -fs $FS4 -ft 3 -line on -edge circle'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.15; 'draw string 'x' 'y' 'var' 'unit
x=(xl+xr)/2; y=y+0.18; 'draw string 'x' 'y' ${TIME}'



var='MCAPE'; unit='[kJ/kg]'

'set gxout shade2'; 'color ${LEVS1} -kind ${KIND1}' ;# SET COLOR BAR

nmap = 1; xmap = 3; ymap = 1
xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'd 'var'/1000.0'

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr; y1=yb-0.3; y2=y1+0.07 ;# LEGEND COLOR BAR

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -edge circle'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.15; 'draw string 'x' 'y' 'var' 'unit
x=(xl+xr)/2; y=y+0.18; 'draw string 'x' 'y' ${TIME}'



var='MCIN'; unit='[kJ/kg]'

'set gxout shade2'; 'color ${LEVS2} -kind ${KIND2}' ;# SET COLOR BAR

nmap = 1; xmap = 4; ymap = 1
xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'd 'var'/1000'

'q gxinfo' ;# GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr; y1=yb-0.3; y2=y1+0.07 ;# LEGEND COLOR BAR

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.07 -fh 0.1 -fs $FS2 -ft 3 -line on -edge circle'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.15; 'draw string 'x' 'y' 'var' 'unit
x=(xl+xr)/2; y=y+0.18; 'draw string 'x' 'y' ${TIME}'



'set strsiz 0.12 0.14'; 'set string 1 l 3 0' ;# HEADER
xx = 0.2; yy=ytop-0.8; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

LINE1="A"; LON1_1=124; LAT1_1=26.7; LON1_2=130; LAT1_2=31.5

LINE2="B"; LON2_1=124.72; LAT2_1=25.8; LON2_2=130.72; LAT2_2=30.6

#FH_LIST="03 06"
FH_LIST="00"

YYYY=2022; MM=06; MMM=JUN; DDLIST=19 #"18 19"

EXELIST="\
03.12.LFM.MAP.CSC2_DLFC.sh \
"

for FH in $FH_LIST; do

for EXE in $EXELIST; do

for DD in $DDLIST; do

if [ $DD == "18" ]; then
HHLIST="14 15 16 17 18 19 20 21 22 23"
elif [ $DD == "19" ]; then
#HHLIST="00 01 02 03 04 05 06 07 08 09 10 12"
HHLIST="00"
fi

for HH in $HHLIST; do

echo "NNNNN $EXE $YYYY $MM $DD $HH"
YMDH=${YYYY}${MM}${DD}${HH}
$EXE $FH ${YMDH} $LINE1 ${LON1_1} ${LAT1_1} ${LON1_2} ${LAT1_2} \
                 $LINE2 ${LON2_1} ${LAT2_1} ${LON2_2} ${LAT2_2} \

echo "NNNNN DONE $EXE  $YYYY $MM $DD $HH"
done #HHLIST

done #DDLIST
done #EXELIST

done #FH




EXELIST="\
"


#!/bin/bash

#FH_LIST="03 06"
FH_LIST="FH00"

YYYY=2022; MM=06; MMM=JUN; DDLIST="18 19"

EXELIST="\
02.24.LFM.MAP.CAPE.sh \
"

for FH in $FH_LIST; do

for EXE in $EXELIST; do

for DD in $DDLIST; do

if [ $DD == "18" ]; then
HHLIST="14 15 16 17 18 19 20 21 22 23"
elif [ $DD == "19" ]; then
HHLIST="00 01 02 03 04 05 06 07 08 09 10 11 12"
#HHLIST="00"
fi

for HH in $HHLIST; do

echo "NNNNN $EXE $YYYY $MM $DD $HH"

$EXE $FH $YYYY$MM$DD$HH

echo "NNNNN DONE $EXE  $YYYY $MM $DD $HH"
done #HHLIST

done #DDLIST
done #EXELIST

done #FH

#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

#MMMMMMMMMMMMMM INCLUDE ###################
. ./06.LFM.MAKE.CTL.sh
#MMMMMMMMMMMMMM INCLUDE ###################

FH=$1; YMDH=$2;
LINE1=$3;    LON1_1=$4;     LAT1_1=$5;    LON1_2=$6;    LAT1_2=$7
LINE2=$8;    LON2_1=$9;     LAT2_2=${10}; LON2_2=${11}; LAT2_2=${12}

FH=${FH:-FH03}; YMDH=${YMDH:-2022061900}
YYYY=${YMDH:0:4}; MM=${YMDH:4:2}; DD=${YMDH:6:2}; HH=${YMDH:8:2}

echo MMMMMMM DATE ${YYYY} ${MM} ${DD} ${HH}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

MODEL=LFM

LONW=122.0 ; LONE=134; LATS=24.5; LATN=35 #MAP AREA

echo MMMMMMM TRANSECT
LINE1=${LINE1:-A}
LON1_1=${LON1_1:-124}; LAT1_1=${LAT1_1:-26.7}
LON1_2=${LON1_2:-130}; LAT2B=${LAT1_2:-31.5}
echo $LON1_1 $LAT1_1 $LON1_2 $LAT1_2

LINE2=${LINE2:-B}
LON2_1=${LON2_1:-124.72}; LAT2_1=${LAT2_1:-25.8}
LON2_2=${LON2_2:-130.72}; LAT2_2=${LAT2_2:-30.6}
echo $LON2_1 $LAT2_1 $LON2_2 $LAT2_2


DLON=0.02; DLAT=$DLON

XM1=$(echo "scale=0; (${LON1_2}-${LON1_1})/${DLON}" | bc)
YM1=$(echo "scale=0; (${LAT1_2}-${LAT1_1})/${DLAT}" | bc)
echo ; echo MMMMMMMM XM1 = $XM1; echo MMMMMMMM YM1 = $YM1; echo

XM2=$(echo "scale=0; (${LON2_2}-${LON2_1})/${DLON}" | bc)
YM2=$(echo "scale=0; (${LAT2_2}-${LAT2_1})/${DLAT}" | bc)
echo ; echo MMMMMMMM XM2 = $XM2; echo MMMMMMMM YM2 = $YM2; echo

MMDD=${MM}${DD}

DATE=${HH}UTC${DD}${MMM}${YYYY}; TIME=${HH}Z${DD}${MMM}${YYYY}
VAR1=VPT; V1LEV=1000; CLEV1C="300 301 302 303 304 305 306 308 310 312 314 316 318"; CLEV2M=304
CINT1C=2

VAR2=EPT; V2LEV=1000; 
CMIN2M=320; CMAX2M=360; CINT2M=4;CINT2C=4;CLEV2MA="360"
CMIN2=334; CMAX2=360; CINT2=2; CLEV2=344

#CINT2M=1;CINT2C=2;CLEV2C="300 304 308 312 316 320 324 328 332"; CLEV2M=304

VAR3=MAUL; V3LEV=850; 
RGB='set rgb 98 128 128 128 -100'
VAR4=RH; CLEV4="95"
XTITLE_CSC="Latitude North" ;#"Longitude East"
YTITLE_CSC="Pressure [hPa]" ;#"Longitude East"

VAR5="analysed_sst.2-273"; CLEV5MA="24 26 28"; CLEV5M="23 25 27 29"; DATE5="00Z19JUN2022"
CMIN5=22; CMAX5=30; CINT5=1
DATE5="00Z19JUN2022"

VAR21=QV; V21LEV=975; V21FAC=1E3; CMIN21=10; CMAX21=22; CINT21=1; CLEV21M=18
KIND21="white->darkblue"

LEVCSC1=1000; LEVCSC2=500
CLEVCSC=1

LEVCSC3=1000; LEVCSC4=900
CLEVCSC=1


INDIR=/work01/DATA/LFM_PROC_ROT/FH${FH}
#INFLE=LFM_PRS_FH${FH}_PROC_V_${YYYY}-${MM}-${DD}_${HH}.nc
INFLE=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_THERMO.nc
IN=${INDIR}/${INFLE}

if [ ! -f $IN ]; then echo ERROR: NO SUCH FILE, $IN; exit 1; fi

FIGDIR=FH${FH}/$LINE; mkd $FIGDIR
FIG=${FIGDIR}/${MODEL}_FH${FH}_${MMDD}${HH}_CSC4_SST_EPT_${LINE1}_${LINE2}_${LINE3}_${LINE4}.pdf

SCLV=40
KIND2='lightcyan->cyan->mediumspringgreen->lawngreen->moccasin->orange->orangered->red->magenta'
KIND5='midnightblue->deepskyblue->green->wheat->orange->red->magenta'

HOST=$(hostname); CWD=$(pwd); TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo CREATE CTL FILE

CTL1=LFM.CTL; echo $CTL1

INDIR1=/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F/OUT_CAPE3D_NC4
INFLE_CHK=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_CAPE3D.nc
INFLE1=LFM_PRS_FH${FH}_VALID_%y4-%m2-%d2_%h2_CAPE3D.nc
DSET1=$INDIR1/${INFLE1}
DSET_CHK=$INDIR1/${INFLE_CHK}
if [ ! -f $DSET_CHK ]; then echo NO SUCH FILE, $DSET_CHK; echo; exit 1; fi

LFM_MAKE_CTL $CTL1 $DSET1
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM


CTL3=NAVO.CTL; echo $CTL3

GS=$(basename $0 .sh).GS

cat <<EOF>$GS

'open $CTL1' ;#'q ctlinfo'; say result
'open $CTL3' ;#'q ctlinfo'; say result

'cc'; 'set grid off'


say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 1 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

say 'MMMMM SET FIGURE SIZE'
'set vpage 0.0 11.5 0 8.5' ;#0.0 8.5 0 10.5'

xmax=2; ymax=2
xleft=0.2; ytop=7

xwid =  6/xmax; ywid =  5/ymax
xmargin=0.4; xmargin2=0.6; ymargin=1

ymap=1; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye



'set grads off'


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

'set gxout shade2'
'set z 1';'set time $DATE5'
'color $CMIN5 $CMAX5 $CINT5 -kind $KIND5'
'd $VAR5'

say 'mmmmm COLOR BAR'
'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 2 -ft 1 -line on -edge circle'
xx=x2+0.05; yy=y2; 'set string 1 l 3 0';'set strsiz 0.1 0.12'
'draw string ' xx ' ' yy '[C]'

'set xlab off'; 'set ylab off'


say 'CONTOUR $VAR2'
'set lev $L2LEV'
'set time ${TIME}'
'set gxout contour'
'set ccolor 0'; 'set cthick 3'
'set clab off'; 'set cint $CINT2M'
'q dims'; say sublin(result,4)
'd $VAR2'

'set ccolor 1';'set cthick 1'
'set clab off';'set cint $CINT2M';'set cmin $CMIN2M';'set cmax $CMAX2M'
'd $VAR2'

'set ccolor 1';'set cthick 1'
'set clab on';'set clevs $CLEV2MA';'set cmin $CMIN2M';'set cmax $CMAX2M'
'set clopts 1 2 0.05'
'd $VAR2'


'set time $TIME'

say 'MMMMM VECTOR $V2LEV'
'set gxout vector'; 'set cthick 1'
'set arrscl 0.5 $SCLV'; 'set arrlab off'
'set lev $V2LEV'
'q dims'; say sublin(result,4)

'set ccolor 0'; 'set cthick 6'
'vec skip(u,25,25);v -SCL 0.5 $SCLV -P 20 20'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set ccolor 1'; 'set cthick 3'
'vec skip(u,25,25);v -SCL 0.5 $SCLV -P 'xx' 'yy' -SL m/s'


say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 1'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 1'
'trackplot $LON3_1 $LAT3_1 $LON3_2 $LAT3_2 -c 1 -l 1 -t 1'
'trackplot $LON4_1 $LAT4_1 $LON4_2 $LAT4_2 -c 1 -l 1 -t 1'


say 'MMMMM VECTOR $V3LEV'
'set gxout vector'
'set arrscl 0.5 $SCLV'; 'set arrlab off'
'set ccolor 0'; 'set cthick 6'
'set lev $V3LEV'
'q dims'; say sublin(result,4)

#'set ccolor 0'; 'set cthick 5'
#'vec skip(u,25,25);v -SCL 0.5 $SCLV -P 20 20'
#'set rgb 97 120 120 120'

'set ccolor 97'; 'set cthick 2'
'vec skip(u,25,25);v -SCL 0.5 $SCLV -P 20 20'


say 'NNNNN LINE NAME'
'q w2xy ${LON1_1} ${LAT1_1}'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'draw string 'xx-0.05' 'yy-0.05' $LINE1'

'q w2xy ${LON2_1} ${LAT2_1}'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'draw string 'xx-0.06' 'yy-0.06' $LINE2'

'q w2xy ${LON3_1} ${LAT3_1}'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'draw string 'xx-0.06' 'yy-0.06' $LINE3'

'q w2xy ${LON4_1} ${LAT4_1}'; xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3'
'draw string 'xx-0.06' 'yy-0.06' $LINE4'


'set strsiz 0.1 0.12'; 'set string 1 c 3'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild $DATE (FH${FH})'
'set string 1 l 3'
xx=xl-0.3; yy=yy+0.18; 'draw string 'xx' 'yy' ${VAR1}(${V1LEV}) ${VAR2}(${V2LEV}) ${VAR3}(${V3LEV})'
XSLEFT=xl; YTTOP=yt



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 2 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time ${HH}Z${DD}${MMM}${YYYY}'
'set grads off'

line = $LINE1
lon1 = $LON1_1; lon2 = $LON1_2; lat1 = $LAT1_1; lat2 = $LAT1_2
YM=${YM1}
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev $LEVCSC1 $LEVCSC2' ;# 'set zlog on'

'collect 1 free'; 'collect 2 free'; 'collect 3 free'; 'collect 4 free'
lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 1 gr2stn($VAR1,'lon','lat')'
  'collect 2 gr2stn($VAR2,'lon','lat')'
  'collect 3 gr2stn($VAR3,'lon','lat')'
  'collect 4 gr2stn($VAR4,'lon','lat')'
  lat = lat + $DLAT
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=1; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

'set clab on'

'set cint $CLEVCSC'
'color $CMIN2 $CMAX2 $CINT2 -kind $KIND2'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'


say; say 'MMMM $VAR1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(1,-u)'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 1'; 'set cthick 2'
'd coll2gr(1,-u)'

'set clab on'; 'set clevs $CLEV1C'; 'set ccolor 1'; 'set cthick 2' 'set clopts 1 2 0.07'
'd coll2gr(1,-u)'



say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'$RGB' ; 'set ccolor 98';'set cthick 10'
#'set cint $CINT2'
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs $CLEV4'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs $CLEV4'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild $DATE (FH${FH})'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' 'line '(' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' $XTITLE_CSC'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' $YTITLE_CSC'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 3 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time ${HH}Z${DD}${MMM}${YYYY}'
'set grads off'

line = $LINE2
lon1 = $LON2_1; lon2 = $LON2_2; lat1 = $LAT2_1; lat2 = $LAT2_2
YM=${YM2}
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev $LEVCSC1 $LEVCSC2' ;# 'set zlog on'

'collect 1 free'; 'collect 2 free'; 'collect 3 free'; 'collect 4 free'
lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 1 gr2stn($VAR1,'lon','lat')'
  'collect 2 gr2stn($VAR2,'lon','lat')'
  'collect 3 gr2stn($VAR3,'lon','lat')'
  'collect 4 gr2stn($VAR4,'lon','lat')'
  lat = lat + $DLAT
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=1; xmap=3

xs = xleft + (xwid+xmargin2)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

'set clab on'

'color $CMIN2 $CMAX2 $CINT2 -kind $KIND2'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'


say; say 'MMMM $VAR1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(1,-u)'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 1'; 'set cthick 2'
'd coll2gr(1,-u)'

'set clab on'; 'set clevs $CLEV1C'; 'set ccolor 1'; 'set cthick 2' 'set clopts 1 2 0.07'
'd coll2gr(1,-u)'



say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint $CINT2'
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs $CLEV4'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs $CLEV4'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild $DATE (FH${FH})'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' 'line '(' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' $XTITLE_CSC'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' $YTITLE_CSC'



say 'mmmmm COLOR BAR'
x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 2 -ft 3 -line on -edge circle'
#xx=(x1+x2)/2; yy=y2+0.15; 'set string 1 c 3 0'
xx=x1; yy=y2; 'set string 1 c 3 0'
'draw string ' xx ' ' yy '[Pa/s]'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 4 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time ${HH}Z${DD}${MMM}${YYYY}'
'set grads off'

line = $LINE3
lon1 = $LON3_1; lon2 = $LON3_2; lat1 = $LAT3_1; lat2 = $LAT3_2
YM=${YM3}
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev $LEVCSC3 $LEVCSC4' ;# 'set zlog on'

'collect 1 free'; 'collect 2 free'; 'collect 3 free'; 'collect 4 free'
lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 1 gr2stn($VAR1,'lon','lat')'
  'collect 2 gr2stn($VAR2,'lon','lat')'
  'collect 3 gr2stn($VAR3,'lon','lat')'
  'collect 4 gr2stn($VAR4,'lon','lat')'
  lat = lat + $DLAT
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=2; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid/2

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

'set clab on'

'color $CMIN2 $CMAX2 $CINT2 -kind $KIND2'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'


say; say 'MMMM $VAR1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(1,-u)'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 1'; 'set cthick 2'
'd coll2gr(1,-u)'

'set clab on'; 'set clevs $CLEV1C'; 'set ccolor 1'; 'set cthick 2' 'set clopts 1 2 0.07'
'd coll2gr(1,-u)'



say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint $CINT2'
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs $CLEV4'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs $CLEV4'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild $DATE (FH${FH})'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' 'line '(' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' $XTITLE_CSC'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' $YTITLE_CSC'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM 5 CROSS SECTION MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
'set xlab on'; 'set ylab on'

'set time ${HH}Z${DD}${MMM}${YYYY}'
'set grads off'

line = $LINE4
lon1 = $LON4_1; lon2 = $LON4_2; lat1 = $LAT4_1; lat2 = $LAT4_2
YM=${YM4}
say lon1' 'lon2' 'lat1' 'lat2

'set x 1'; 'set y 1'
'set lev $LEVCSC3 $LEVCSC4' ;# 'set zlog on'

'collect 1 free'; 'collect 2 free'; 'collect 3 free'; 'collect 4 free'
lat = lat1
while (lat <= lat2)
  lon = lon1 + (lon2-lon1)*(lat-lat1) / (lat2-lat1)
  'collect 1 gr2stn($VAR1,'lon','lat')'
  'collect 2 gr2stn($VAR2,'lon','lat')'
  'collect 3 gr2stn($VAR3,'lon','lat')'
  'collect 4 gr2stn($VAR4,'lon','lat')'
  lat = lat + $DLAT
endwhile
'set x 1 'YM
'set xaxis 'lat1' 'lat2

say 'MMMMM SET FIGURE SIZE'
ymap=2; xmap=3

xs = xleft + (xwid+xmargin2)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid/2

'set parea 'xs ' 'xe' 'ys' 'ye



'set xlint 1'; 'set xlevs 25 26 27 28 29 30 31 32 33 34 35 36'; 'set ylint 50'

'set clab on'

'color $CMIN2 $CMAX2 $CINT2 -kind $KIND2'
'd coll2gr(2,-u)'

'set xlab off'; 'set ylab off'


say; say 'MMMM $VAR1 CROSS SECTION'
'set gxout contour'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 0'; 'set cthick 6'
'd coll2gr(1,-u)'

'set clab off'; 'set cint $CINT1C'; 'set ccolor 1'; 'set cthick 2'
'd coll2gr(1,-u)'

'set clab on'; 'set clevs $CLEV1C'; 'set ccolor 1'; 'set cthick 2' 'set clopts 1 2 0.07'
'd coll2gr(1,-u)'



say; say 'MMMM MAUL CROSS SECTION'
'set gxout grfill'
'set ccolor 98';'set cthick 10'
#'set cint $CINT2'
'd coll2gr(3,-u)'



say; say 'MMMM RH CROSS SECTION'
'set gxout contour'
'set clevs $CLEV4'; 'set ccolor 0';' set cthick 5';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'
'set clevs $CLEV4'; 'set ccolor 4';' set cthick 2';'set clopts 4 2 0.07'
'd coll2gr(4,-u)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' Vaild $DATE (FH${FH})'
xx=(xl+xr)/2; yy=yy+0.18; 'draw string 'xx' 'yy' 'line '(' lat1 'N,' lon1 'E)-(' lat2 ',' lon2 'E)'
xx=(xl+xr)/2; yy=yb-0.3; 'draw string 'xx' 'yy' $XTITLE_CSC'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' $YTITLE_CSC'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM HEADER MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'
xx = XSLEFT - 0.2; yy=YTTOP+0.1
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
yy = yy+0.8; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${CMD} $@'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${TIMESTAMP}'



'gxprint $FIG'

'quit'
EOF


grads -bcl $GS
rm -vf $GS
if [ -f $FIG ];then echo; echo OUTPUT $FIG; echo; fi

; 球面三角法をもちいた線分と緯線（緯度が等しい線）がなす角度
; https://keisan.casio.jp/exec/system/1257670779
; 球面三角法を用いているので若干の誤差はあるが，数100kmぐらいであれば問題にならない

LON1B=124
LAT1B=26.7
LON2B=130
LAT2B=31.5

;LON1="124"; LAT1="26.7"; LON2="130"; LAT2="31.5"
;LON1S= getenv("NCL_ARG_5")
;LAT1S= getenv("NCL_ARG_6")
;LON2S= getenv("NCL_ARG_7")
;LAT2S= getenv("NCL_ARG_8")
;LON1=tofloat(LON1S)
;LAT1=tofloat(LAT1S)
;LON2=tofloat(LON2S)
;LAT2=tofloat(LAT2S)


PI=3.141592653589793
D2R=PI/180.0
x1=LON1*D2R
y1=LAT1*D2R
x2=LON2*D2R
y2=LAT2*D2R
dx=x2-x1
ARCTAN=atan2(sin(dx), cos(y1)*tan(y2)-sin(y1)*cos(dx))
PHI_A=PI/2.0 - ARCTAN

PHI_D=PHI_A/D2R   ; [DEGREE]
THETA=PHI_D*D2R  ; ANGLE MEASURED FROM THE LATITUDE LINE [RADIAN]

print("mmmmm LON1="+LON1S+" LAT1="+LAT1S+" LON2="+LON2S+" LAT2="+LAT2S)
print("mmmmm ANGLE="+PHI_D)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
ur=u
vr=v
ur@transect_lon1=LON1S
ur@transect_lat1_=LAT1S
ur@transect_lon2_=LON2S
ur@transect_lat2_=LAT2S
ur@transect_angle=PHI_D

vr@transect_lon1_=LON1S
vr@transect_lat1_=LAT1S
vr@transect_lon2_=LON2S
vr@transect_lat2_=LAT2S
vr@transect_angle=PHI_D

ur=   u*cos(THETA) + v*sin(THETA)
vr= - u*sin(THETA) + v*cos(THETA)

print("mmmmm mmmmm ROTATE U & V")
print("mmmmm LON1="+LON1S+" LAT1="+LAT1S+" LON2="+LON2S+" LAT2="+LAT2S)
print("mmmmm ANGLE="+PHI_D)

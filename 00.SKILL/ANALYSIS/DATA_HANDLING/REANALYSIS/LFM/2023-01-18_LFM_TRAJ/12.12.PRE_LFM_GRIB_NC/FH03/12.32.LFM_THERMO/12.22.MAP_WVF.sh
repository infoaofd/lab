#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/13.12.NCL.ROT.CAPE.ASCUL

#MMMMMMMMMMMMMM INCLUDE ###################
. ./12.12.LFM.MAKE.CTL.sh
#MMMMMMMMMMMMMM INCLUDE ###################

FH=$1; FH=${FH:-FH00}
YYYYMMDDHH=$2; YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

GS=$(basename $0 .sh).GS

VAR1=WVF; LEV1=950; LEV2=800;
VAR3=VPT; LEV3=950; CLEV3=304
VARLIST="$VAR1"

VTHD=0.1; VSKP=20

FIG=LFM_${FH}_${VARLIST}_${LEV1}_${LEV2}_${YYYY}-${MM}-${DD}_${HH}.pdf

CTL2=LFM_PROC_ROT_v5_THERMO.CTL
INDIR2=/work01/DATA/LFM_PROC_ROT_v5/${FH}
INFLE2=LFM_PRS_${FH}_VALID_%y4-%m2-%d2_%h2_THERMO.nc
DSET2=$INDIR2/$INFLE2
LFM_MAKE_CTL_2 $CTL2 $DSET2

LONW=122.5 ; LONE=131.5; LATS=25; LATN=32 #MAP AREA

#LEVS1="-levs 0 0.1 0.2 0.3 0.4 0.5 0.6"
LEVS1="-levs 0.1 0.15 0.2 0.25 0.3 0.35 0.4"
KIND1='-kind white->khaki->orange->red'
FS1=2; UNIT1="kg/m\`a2\`n/s"

LEVS2=$LEVS1 #"-levs 0 0.05 0.1 0.15 0.2 0.25 0.3"
KIND2=$KIND1 #'-kind white->khaki->orange->red'
FS2=2; UNIT2="kg/m\`a2\`n/s"

echo MMMMMMM TRANSECT
LINE1=${LINE1:-A}
LON1_1=${LON1_1:-124}; LAT1_1=${LAT1_1:-26.7}
LON1_2=${LON1_2:-130}; LAT1_2=${LAT1_2:-31.5}
echo $LON1_1 $LAT1_1 $LON1_2 $LAT1_2

LINE2=${LINE2:-B}
LON2_1=${LON2_1:-124.72}; LAT2_1=${LAT2_1:-25.8}
LON2_2=${LON2_2:-130.72}; LAT2_2=${LAT2_2:-30.6}
echo $LON2_1 $LAT2_1 $LON2_2 $LAT2_2

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL2}'

xmax = 2; ymax = 1
ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE
'cc'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set rgb 99 78 53 36'
'set mpdset hires';'set map 99 1 3 1'
'set grads off';'set grid off'
'set xlint 2';'set ylint 1'
'set xlopts 1 3 0.1'; 'set ylopts 1 3 0.1'; 
'set gxout shade2'
'color ${LEVS1} ${KIND1}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV1}'; 'set time ${TIME}'

'd wvm'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR1} ${TIME} ${LEV1}hPa ${FH}'
x=(xl+xr)/2; y=y+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR3} ${TIME} ${LEV3}hPa ${FH}'

x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -edge circle'
x=x2-0.7; y=y1-0.3; 'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT1}'

'set lev ${LEV1}'; 'set time ${TIME}'
'set gxout vector';'set xlab off';'set ylab off'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(maskout(wvx,wvm-${VTHD}),${VSKP});wvy -SCL 0.5 0.5 -P 20 20 -SL m/s'

xx=xr-0.8; yy=yb-1
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(maskout(wvx,wvm-${VTHD}),${VSKP});wvy -SCL 0.5 0.5 -P 'xx' 'yy' -SL ${UNIT1}'


'set lev ${LEV3}'; 'set time ${TIME}'
'set xlab off';'set ylab off'
'set gxout contour'
'set clevs ${CLEV3}';'set clab off'
'set cthick 5';'set ccolor 0'
'd vpt'
'set clevs ${CLEV3}';'set clab on'
'set cthick 1';'set ccolor 1';'set clopts 1 1 0.08'
'd vpt'
'set xlab on';'set ylab on'

say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 0 -l 1 -t 4'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 2'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 0 -l 1 -t 4'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 2'


nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV2}'; 'set time ${TIME}'

'set gxout shade2'
'color ${LEVS2} ${KIND2}' ;# SET COLOR BAR
'd wvm'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR1} ${TIME} ${LEV2}hPa ${FH}'
x=(xl+xr)/2; y=y+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR3} ${TIME} ${LEV3}hPa ${FH}'

x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS2 -ft 3 -line on -edge circle'
x=x2-0.7; y=y1-0.3; 'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT2}'



'set lev ${LEV2}'; 'set time ${TIME}'
'set gxout vector';'set xlab off';'set ylab off'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(maskout(wvx,wvm-${VTHD}),${VSKP});wvy -SCL 0.5 0.5 -P 20 20 -SL m/s'

xx=xr-0.8; yy=yb-1
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(maskout(wvx,wvm-${VTHD}),${VSKP});wvy -SCL 0.5 0.5 -P 'xx' 'yy' -SL ${UNIT2}'


'set lev ${LEV3}'; 'set time ${TIME}'
'set xlab off';'set ylab off'
'set gxout contour'
'set clevs ${CLEV3}';'set clab off'
'set cthick 5';'set ccolor 0'
'd vpt'
'set clevs ${CLEV3}';'set clab on'
'set cthick 1';'set ccolor 1';'set clopts 1 1 0.08'
'd vpt'
'set xlab on';'set ylab on'

say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 0 -l 1 -t 4'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 2'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 0 -l 1 -t 4'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 2'


#x=(xl+xr)/2 ;# TEXT
#y=yt+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${TEXT}'

'set strsiz 0.1 0.12'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR1}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR2}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

FH=00
#FH=03
INDIR='/work01/DATA/LFM_PROC_CUT/FH'FH

INFLE='LFM_PRS_FH' FH '_PROC_V_2022-06-19_00.nc'
INFLE='LFM_PRS_FH' FH '_PROC_V_2022-06-18_23.nc'

IN=INDIR'/'INFLE

LON=122; LAT=23.6
VAR="ept" ;#"u" ;"v"' "w" ;"u" ;"temp"; "rh" ;"vpt"; "pt"

'sdfopen 'IN
'q ctlinfo'; say result
say; say INFLE

'set lon 'LON; 'set lat 'LAT; 'set lev 1000 300'
#'set time 05Z20JUN2022'
'set t 1';'q dims';say result

'set gxout shaded'
'set grid off';'set grads off'
'cc'
#'set mpdset hires'

if (VAR = 'qv')
'd 'VAR'*1000'
else
'd 'VAR
endif

'draw title 'LON' 'LAT' 'INFLE

FIG=INFLE'_VPR_'VAR'.eps'
'gxprint 'FIG

say; say "FIG: "FIG

'quit'


#
FC	= gfortran
#FFLAGS	= -assume byterecl
FFLAGS	= 
NC	= /usr/local/
NCINC	= -I$(NC)/include
NCLIB	= -L$(NC)/lib -lnetcdf

OBJ	= ncio.o  gettime.o

.SUFFIXES: .f90 .f .F90 .F .o

.f90.o	:
	$(FC) $(FFLAGS) $(NCINC) -c $<

program: $(OBJ)
#$(FC) $(FFLAGS) $(NCINC) -c $(OBJ)

clean:
	/bin/rm *.o *.mod


# D:\TOOLS_200804\TOOLBOX\NetCDF\netcdf_Iizuka
#!/bin/csh -f


set NC_LIB = /usr/local/lib
set NC_INC = /usr/local/include

set FC = "ifort"
set OPT = "-auto  -ftrapuv -check all -std -fpe0 -traceback"
set OPT = " "

$FC $OPT -c -I$NC_INC *.f90 -L$NC_LIB -lnetcdf

# D:\TOOLS_200804\TOOLBOX\NetCDF\netcdf_Iizuka

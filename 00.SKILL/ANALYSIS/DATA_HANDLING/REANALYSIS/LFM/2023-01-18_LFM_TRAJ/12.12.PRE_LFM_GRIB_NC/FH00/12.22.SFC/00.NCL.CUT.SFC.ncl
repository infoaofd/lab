load "./SUBSTRING.ncl"

; # /work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/02.16.NCL.HCUT.PDEL

LONW=120.0
LONE=134.0
LATS=22.4
LATN=35.0

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
print("SCRIPT="+scriptname_in)
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")

IN  = getenv("NCL_ARG_2")
print("mmmmmmmmmmmmmmmmmm")
print("SFC IN="+IN)
print("mmmmmmmmmmmmmmmmmm")

ODIR = getenv("NCL_ARG_3")

f=addfile(IN,"r")

LON_ORG=f->lon_0
LAT_ORG=f->lat_0

T_ORG=f->TMP_P0_L103_GLL0
RH_ORG=f->RH_P0_L103_GLL0
U_ORG=f->UGRD_P0_L103_GLL0
V_ORG=f->VGRD_P0_L103_GLL0
PS_ORG=f->PRES_P0_L1_GLL0
SLP_ORG=f->PRMSL_P0_L101_GLL0


;printVarSummary(T_ORG)
;printVarSummary(RH)
;printVarSummary(U)
;printVarSummary(V)
;printVarSummary(PS)
;printVarSummary(SLP)

lon=LON_ORG({LONW:LONE})
lat=LAT_ORG({LATS:LATN})

tmp_temp=T_ORG({LATS:LATN},{LONW:LONE})
tmp_rh=RH_ORG({LATS:LATN},{LONW:LONE})
tmp_u=U_ORG({LATS:LATN},{LONW:LONE})
tmp_v=V_ORG({LATS:LATN},{LONW:LONE})
tmp_sp=PS_ORG({LATS:LATN},{LONW:LONE})
tmp_slp=SLP_ORG({LATS:LATN},{LONW:LONE})

; ADD DIMENSION OF TIME
dim=dimsizes(tmp_temp)

temp=new( (/1,dim(0),dim(1)/),typeof(tmp_temp) )
temp(0,:,:)=tmp_temp(:,:)

rh=new( (/1,dim(0),dim(1)/),typeof(tmp_rh) )
rh(0,:,:)=tmp_rh(:,:)

u=new( (/1,dim(0),dim(1)/),typeof(tmp_u) )
u(0,:,:)=tmp_u(:,:)

v=new( (/1,dim(0),dim(1)/),typeof(tmp_v) )
v(0,:,:)=tmp_v(:,:)

sp=new( (/1,dim(0),dim(1)/),typeof(tmp_sp) )
sp(0,:,:)=tmp_sp(:,:)

slp=new( (/1,dim(0),dim(1)/),typeof(tmp_slp) )
slp(0,:,:)=tmp_slp(:,:)



print("mmmmm SET TIME")

ITIME=temp@initial_time
FTIME=temp@forecast_time

print("ITIME="+ITIME+" FTIME="+FTIME)

MM=substring(ITIME,0,1)
DD=substring(ITIME,3,4)
YYYY=substring(ITIME,6,9)
HH=substring(ITIME,12,13)
FH=toint(tofloat(FTIME)/60)

month=toint(MM)
day=toint(DD)
year=toint(YYYY)
hour=toint(HH)
minute=0
second=0
units="hours since 2022-06-01 00:00:00"

time = cd_inv_calendar(year,month,day,hour,minute,second,units, 0)
time@units=units

print("INIT_TIME="+YYYY+MM+DD+" "+HH)
print("FCST_TIME="+FH)

time=time + FH
time!0="time"
time&time=time

utc_date = cd_calendar(time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd)+"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)
print("VALID_TIME="+VALID_TIME)
time@valid_time=VALID_TIME

lon!0="lon"
lon&lon=lon
lon@units = "degrees_east"
lon@grid_type = "Latitude/Longitude"
lon@long_name = "longitude"

lat!0="lat"
lat&lat=lat
lat@units = "degrees_north"
lat@grid_type = "Latitude/Longitude"
lat@long_name = "latitude"

temp!0="time"
temp&time=time
temp!1="lat"
temp&lat=lat
temp!2="lon"
temp&lon=lon

copy_VarCoords(temp,rh)
copy_VarCoords(temp,u)
copy_VarCoords(temp,v)
copy_VarCoords(temp,sp)
copy_VarCoords(temp,slp)

print("")
print("mmmmm OUTPUT")
OUT=ODIR+"/"+"LFM_SFC_FH"+sprinti("%0.2i", FH)+"_VALID_"+VALID_TIME+".nc"

system("rm -vf "+OUT)

setfileoption("nc","Format","LargeFile")

a=addfile(OUT,"c")

dimNames = (/"time", "lat", "lon"/)
dimSizes = (/1,dim(0),dim(1)/)
dimUnlim = (/True,False,False/)  

setfileoption(a,"DefineMode",True)
fatts = True
fatts@creation_date = systemfunc("date -R")  ;; dateコマンドの戻り値
fatts@directory=systemfunc("pwd")
fatts@script = scriptname ;; dateコマンドの戻り値

fileattdef(a,fatts)

filedimdef(a,dimNames,dimSizes,dimUnlim)

filevardef(a,"time",typeof(time),getvardims(time))
filevardef(a,"lat",typeof(lat),getvardims(lat))
filevardef(a,"lon",typeof(lon),getvardims(lon))
filevardef(a,"temp",typeof(u),getvardims(temp))
filevardef(a,"rh",typeof(u),getvardims(rh))
filevardef(a,"u",typeof(u),getvardims(u))
filevardef(a,"v",typeof(v),getvardims(v))
filevardef(a,"sp",typeof(sp),getvardims(sp))
filevardef(a,"slp",typeof(slp),getvardims(slp))

a->time = time
a->lon  = lon 
a->lat  = lat 
a->u    = u
a->v    = v
a->temp = temp
a->rh   = rh 
a->sp   = sp
a->slp  = slp


print("MMMMMMMMMMMMMMMMMMMMMM")
print("SFC OUT: "+OUT)
print("MMMMMMMMMMMMMMMMMMMMMM")
;system("ncdump -h "+OUT)
print("")

print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("DONE SCRIPT "+scriptname_in)
print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("")
print("")
print("")

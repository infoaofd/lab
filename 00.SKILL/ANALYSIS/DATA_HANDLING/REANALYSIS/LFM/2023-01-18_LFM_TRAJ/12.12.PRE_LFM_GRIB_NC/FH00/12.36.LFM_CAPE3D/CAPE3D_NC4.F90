PROGRAM CAPE3D
! Mon, 26 Dec 2022 18:24:27 +0900
! p5820.bio.mie-u.ac.jp
! /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

use netcdf

IMPLICIT NONE

INTEGER::I3DFLAG=1,TER_FOLLOW=1

CHARACTER INDIR1*500,INFLE1*500,IN1*1000,INDIR2*500,INFLE2*500,IN2*1000,&
INDIR3*500,INFLE3*500,IN3*1000
CHARACTER ODIR*500, OFLE*500, OUT*1000
CHARACTER PSAFILE*500 

INTEGER MIY,MJX,MKZH ! DIMENSIONS OF DATA (X,Y, AND Z DIRECTIONS)

INTEGER ncid1,ncid2,ncid3,stat,varid !NC4 INPUT VAR

REAL,ALLOCATABLE,DIMENSION(:)::lon, lat
REAL,ALLOCATABLE,DIMENSION(:)::p_in
REAL,ALLOCATABLE,DIMENSION(:,:)::HGT 
REAL,ALLOCATABLE,DIMENSION(:,:,:  )::sp_in, sphpa
REAL,ALLOCATABLE,DIMENSION(:,:,:,:)::temp_in,qv_in,z_in

REAL(8)::CMSG=-999.9D0
REAL::FILLVALUE=1.E20, EPS=1.E-4

REAL(8)::TIME
REAL(8),ALLOCATABLE,DIMENSION(:,:)::TER !ter=HGT
REAL(8),ALLOCATABLE,DIMENSION(:,:)::SFP
REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::PRS,TMK,QVP,GHT
REAL(8),ALLOCATABLE,DIMENSION(:,:,:)::CAPE,CIN,DLFC,DLCL
REAL,   ALLOCATABLE,DIMENSION(:,:,:)::CAPEO,CINO,DLFCO,DLCLO

INTEGER :: NCIDO, VARIDO, NF_GLOBAL !NC4 OUTPUT VAR
INTEGER :: LON_DIM_ID, LAT_DIM_ID, LEVEL_DIM_ID, TIME_DIM_ID
INTEGER :: LON_ID, LAT_ID, LEVEL_ID, TIME_ID
INTEGER :: nf_put_att_text, status
INTEGER::I,J,K, IDX, JDX



NAMELIST /PARA/INDIR1,INFLE1,INDIR2,INFLE2,INDIR3,INFLE3,PSAFILE,&
ODIR,OFLE,MIY,MJX,MKZH
READ(*,NML=PARA)

ALLOCATE(lon(MIY),lat(MJX))
ALLOCATE(p_in(MKZH),HGT(MIY,MJX),sp_in(MIY,MJX,1), sphpa(MIY,MJX,1))
ALLOCATE(temp_in(MIY,MJX,MKZH,1),qv_in(MIY,MJX,MKZH,1),z_in(MIY,MJX,MKZH,1))
ALLOCATE(PRS(MIY,MJX,MKZH),TMK(MIY,MJX,MKZH),QVP(MIY,MJX,MKZH),GHT(MIY,MJX,MKZH))
ALLOCATE(TER(MIY,MJX),SFP(MIY,MJX))
ALLOCATE(CAPE(MIY,MJX,MKZH),CIN(MIY,MJX,MKZH),&
DLFC(MIY,MJX,MKZH),DLCL(MIY,MJX,MKZH))
ALLOCATE(CAPEO(MIY,MJX,MKZH),CINO(MIY,MJX,MKZH),&
DLFCO(MIY,MJX,MKZH),DLCLO(MIY,MJX,MKZH))

IN1=TRIM(INDIR1)//'/'//TRIM(INFLE1)
IN2=TRIM(INDIR2)//'/'//TRIM(INFLE2)
IN3=TRIM(INDIR3)//'/'//TRIM(INFLE3)
OUT=TRIM(ODIR)//'/'//TRIM(OFLE)

!stat=nf_open(IN1, nf_nowrite, ncid1)
call check( nf90_open( trim( IN1 ), nf90_nowrite, ncid1) )
call check( nf90_open( trim( IN2 ), nf90_nowrite, ncid2) )
call check( nf90_open( trim( IN3 ), nf90_nowrite, ncid3) )

call check( nf90_inq_varid(ncid1, "time", varid))
call check( nf90_get_var(ncid1, varid, TIME) )

call check( nf90_inq_varid(ncid1, "lon", varid))
call check( nf90_get_var(ncid1, varid, lon) )

call check( nf90_inq_varid(ncid1, "lat", varid))
call check( nf90_get_var(ncid1, varid, lat) )

call check( nf90_inq_varid(ncid1, "p", varid))
call check( nf90_get_var(ncid1, varid, p_in) )

call check( nf90_inq_varid(ncid1, "temp", varid))
call check( nf90_get_var(ncid1, varid, temp_in) )

call check( nf90_inq_varid(ncid1, "qv", varid))
call check( nf90_get_var(ncid1, varid, qv_in) )

call check( nf90_inq_varid(ncid1, "z", varid))
call check( nf90_get_var(ncid1, varid, z_in) )

call check( nf90_inq_varid(ncid2, "sp", varid))
call check( nf90_get_var(ncid2, varid, sp_in) )

call check( nf90_inq_varid(ncid3, "HGT", varid))
call check( nf90_get_var(ncid3, varid, HGT) )

where (HGT <= 0.0)
HGT=0.0
endwhere

TER=HGT !REAL -> DOUBLE
DO K=1,MKZH
PRS(:,:,K)=p_in(K) !1D->3D
end do !k

IDX=175; JDX=195 ! FOR CHECKING
print '(A,12f5.0)','MMM PRS ',(PRS(IDX,JDX,k),k=1,MKZH)

DO K=1,MKZH
TMK(:,:,K)=temp_in(:,:,K,1) !REAL -> DOUBLE; REMOVE TIME DIMENSION
QVP(:,:,K)=  qv_in(:,:,K,1)
GHT(:,:,K)=   z_in(:,:,K,1)
END DO !K

DO J=1,MJX
DO I=1,MIY
!IF(ABS(sp_in(i,j,1)-FillValue)<EPS)THEN
IF(sp_in(i,j,1)<2.E6)THEN
sphpa(i,j,1)=sp_in(i,j,1)/100.0  !Pa->hPa
ELSE
sphpa(i,j,1)=FillValue
END IF
END DO !I
END DO !J

SFP(:,:)  =  sphpa(:,:,1) !REAL->DOUBLE

print '(a,12f10.1)', 'MMM PRS ',(PRS(IDX,JDX,k),k=1,MKZH)
print '(a,12f10.1)', 'MMM TMK ',(TMK(IDX,JDX,k),k=1,MKZH)
print '(a,12f10.6)', 'MMM QVP ',(QVP(IDX,JDX,k),k=1,MKZH)
print '(a,12f10.1)', 'MMM GHT ',(GHT(IDX,JDX,k),k=1,MKZH)
print '(a,12f10.1)', 'MMM TER ',TER(IDX,JDX)
print '(a,12f10.1)', 'MMM SFP ',SFP(IDX,JDX)
print '(2(a,f10.3))','MMM LON ',LON(IDX),' LAT ',LAT(JDX)
print '(2(a,I5))'   ,'MMM IDX ',IDX,     ' LAT ',JDX



print *; print '(a)','WWW'; print '(a)','WWW CAPE, CIN, DLFC'

CALL DCAPECALC3D(PRS,TMK,QVP,GHT,TER,SFP,CAPE,CIN,DLFC,&
       DLCL,CMSG,MIY,MJX,MKZH,I3DFLAG,TER_FOLLOW,PSAFILE)

!      SUBROUTINE DCAPECALC3D(PRS,TMK,QVP,GHT,TER,SFP,CAPE,CIN,DLFC,
!     +DLCL,CMSG,MIY,MJX,MKZH,I3DFLAG,TER_FOLLOW,PSAFILE)
!
print '(a)','WWW';print *

!DOUBLE -> REAL
CAPEO=SNGL(CAPE); CINO=SNGL(CIN); DLFCO=SNGL(DLFC); DLCLO=SNGL(DLCL)

!PUT FILLVALUE
where (ABS(CAPE-CMSG)<EPS); CAPEO=FILLVALUE; endwhere

where (ABS(CINO-CMSG)<EPS); CINO=FILLVALUE; endwhere

where (ABS(DLFCO-CMSG)<EPS); DLFCO=FILLVALUE; endwhere

where (ABS(DLCL-CMSG)<EPS); DLCLO=FILLVALUE; endwhere

print '(a,12f7.1)','NNN CAPE ',(CAPE(IDX,JDX,k),k=1,MKZH)
print '(a,12f7.1)','NNN CIN  ',(CIN(IDX,JDX,k),k=1,MKZH)
print '(a,12f7.1)','NNN DLFC ',(DLFC(IDX,JDX,k),k=1,MKZH)
print '(a,12f7.1)','NNN DLCL ',(DLCL(IDX,JDX,k),k=1,MKZH)

! 出力ファイルを開く
CALL CHECK( NF90_CREATE( TRIM(OUT), NF90_HDF5, NCIDO) )

! 次元を定義する
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lon', MIY, LON_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lat', MJX, LAT_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'p',  MKZH, LEVEL_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'time', NF90_UNLIMITED, TIME_DIM_ID) )

! 変数を定義する
CALL CHECK( NF90_DEF_VAR(NCIDO, 'time', NF90_DOUBLE, TIME_DIM_ID, TIME_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'p',    NF90_REAL, LEVEL_DIM_ID, LEVEL_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lat',  NF90_REAL, LAT_DIM_ID, LAT_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lon',  NF90_REAL, LON_DIM_ID, LON_ID) )

! 変数に attributionをつける
CALL CHECK( NF90_PUT_ATT(NCIDO, LON_ID,  'units','degrees_east') )
CALL CHECK( NF90_PUT_ATT(NCIDO, LAT_ID,  'units','degrees_north') )
CALL CHECK( NF90_PUT_ATT(NCIDO, LEVEL_ID,'units','hPa') )
CALL CHECK( NF90_PUT_ATT(NCIDO, TIME_ID, 'units', 'hours since 2022-06-01 00:00:00'))

! GLOBAL ATTRIBUTES
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_dir1", LEN(TRIM(INDIR1)),TRIM(INDIR1))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_file", LEN(TRIM(INFLE1)),TRIM(INFLE1))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_dir2", LEN(TRIM(INDIR2)),TRIM(INDIR2))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_file2",LEN(TRIM(INFLE2)),TRIM(INFLE2))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_dir3", LEN(TRIM(INDIR3)),TRIM(INDIR3))
STATUS=NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_file3",LEN(TRIM(INFLE3)),TRIM(INFLE3))

! define モードを終了する。
CALL CHECK( NF90_ENDDEF(NCIDO) )

CALL CHECK( NF90_PUT_VAR(NCIDO, TIME_ID, time ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, LEVEL_ID, p_in ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, LAT_ID, lat ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, LON_ID, lon ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "DLFC", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, DLFCO ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "DLCL", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, DLCLO ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "CAPE", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','J/kg') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, CAPEO ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "CIN", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','J/kg') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, CINO ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "temp", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','K') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, temp_in ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "qv", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','kg/kg') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, qv_in ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "z", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEVEL_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, z_in ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "sp", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','hPa') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, sphpa ) )

CALL CHECK( NF90_DEF_VAR(NCIDO, "HGT", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, HGT ) )

! FILE を閉じる
CALL CHECK(NF90_CLOSE(NCID1));CALL CHECK(NF90_CLOSE(NCID2));
CALL CHECK(NF90_CLOSE(NCID3));
CALL CHECK(NF90_CLOSE(NCIDO))

print '(a,a)','NNN ODIR: ',TRIM(ODIR)
print '(a,a)','NNN OFLE: ',TRIM(OFLE)

contains
SUBROUTINE CHECK( STATUS )
  INTEGER, INTENT (IN) :: STATUS
  IF(STATUS /= NF90_NOERR) THEN 
    PRINT '(A,A)','EEEEE ERROR ',TRIM(NF90_STRERROR(STATUS))
    STOP "ABNORMAL END"
  END IF
END SUBROUTINE CHECK

END PROGRAM

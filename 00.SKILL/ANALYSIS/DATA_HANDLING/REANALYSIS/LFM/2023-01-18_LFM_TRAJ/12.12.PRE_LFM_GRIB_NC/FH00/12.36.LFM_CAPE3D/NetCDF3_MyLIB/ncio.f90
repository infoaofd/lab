! # D:\TOOLS_200804\TOOLBOX\NetCDF\netcdf_Iizuka

      module ncio_mod
!======================================================================
!     module for netcdf io
!======================================================================
      implicit none

! ... netcdf
      include 'netcdf.inc'

! ... 
      character(len=120) :: t_name = 'time'
      character(len=120) :: t_long = 'Time'
      character(len=120) :: t_unit = 'days'
      character(len=120) :: calendar = 'standard' ! = 'gregorian'
!     character(len=120) :: calendar = 'gregorian'
!     character(len=120) :: calendar = 'julian'
!     character(len=120) :: calendar = 'noleap'
!     character(len=120) :: calendar = '360_day'
!     character(len=120) :: calendar = 'none'

      character(len=120) :: x_name = 'lon'
      character(len=120) :: x_long = 'Longitude'
      character(len=120) :: x_unit = 'degrees_east'

      character(len=120) :: y_name = 'lat'
      character(len=120) :: y_long = 'Latitude'
      character(len=120) :: y_unit = 'degrees_north'

      character(len=120) :: z_name = 'level'
      character(len=120) :: z_long = 'Level'
      character(len=120) :: z_unit = 'm'
      character(len=120) :: updown = 'down'

      end module ncio_mod

      subroutine check_nc(ier,file,name,text)
!======================================================================
!
!======================================================================
      implicit none
      integer :: ier
      character(len=*), intent(in) :: file, name, text

      if (ier /= 0) then
        write(*,*) ' file : ',trim(file)
        write(*,*) ' var  : ',trim(name)
        write(*,*) ' msg  : ',trim(text)
        stop
      end if
      
      end subroutine check_nc

      subroutine get_nc_time(file)
!======================================================================
!
!======================================================================
      use ncio_mod
      implicit none

      character(len=*), intent(in) :: file

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: length
      integer :: i1, i2, i3, i4, i5, i6, i7
      character(len=120) :: text = ' '
      character(len=120) :: name = ' '
      character(len=120) :: cal = ' '
      character(len=4) :: cyear
      character(len=2) :: cmonth, cday, chour, cminute, csecond
      integer :: iyear, imonth, iday, ihour, iminute, isecond

      name = t_name

! ... open file
      ier = nf_open(file, nf_nowrite, ncid)
      call check_nc(ier,file,name,'cannot open file : get_nc_time')

! ... get variable id
      ier = nf_inq_dimid(ncid, name, varid)
      call check_nc(ier,file,name,'cannot get varid')

! ... get attribution
      ier = nf_inq_attlen(ncid, varid, 'units', length)
      call check_nc(ier,file,name,'cannot get units length')
      ier = nf_get_att_text(ncid, varid, 'units', text(1:length))
      call check_nc(ier,file,name,'cannot get units')

! ... get units
      if (text(1:10) == 'days since') then
        t_unit = 'days'
      else if (text(1:11) == 'hours since') then
        t_unit = 'hours'
      else if (text(1:13) == 'minutes since') then
        t_unit = 'minutes'
      else if (text(1:13) == 'seconds since') then
        t_unit = 'seconds'
      else
        write(*,*) 'time unit : ',trim(text)
        write(*,*) 'time unit in nc is not supported!'
        stop
      end if

! ... get base time
      i1  = scan(trim(text),'since') + 8
      i2  = scan(trim(text(i1:length)),'-') + i1
      i3  = scan(trim(text(i2:length)),'-') + i2
      i4  = scan(trim(text(i3:length)),' ') + i3
      i5  = scan(trim(text(i4:length)),':') + i4
      i6  = scan(trim(text(i5:length)),':') + i5
      i7  = scan(trim(text(i6:length)),' ') + i6
      if (i7 == i6) i7 = length + 2
      cyear   = text(i1:i2-2)
      cmonth  = text(i2:i3-2)
      cday    = text(i3:i4-2)
      chour   = text(i4:i5-2)
      cminute = text(i5:i6-2)
      csecond = text(i6:i7-2)
      read(cyear  ,*) iyear
      read(cmonth ,*) imonth
      read(cday   ,*) iday
      read(chour  ,*) ihour
      read(cminute,*) iminute
      read(csecond,*) isecond

!     write(6,'(i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a,i2.2)') &
!    & iyear,'/',imonth,'/',iday,'@',ihour,':',iminute,':',isecond

! ... get attribution
      ier = nf_inq_attlen(ncid, varid, 'calendar', length)
!     call check_nc(ier,file,name,'cannot get calendar length')

      if (ier /= 0) then
        cal = 'standard'
      else
        ier = nf_get_att_text(ncid, varid, 'calendar', text(1:length))
        call check_nc(ier,file,name,'cannot get calendar')

        select case(text(1:length))
        case ('standard')
          cal = 'greoglian'
        case ('gregolian')
          cal = 'greoglian'
        case ('noleap')
          cal = '365_day'
        case ('365_day')
          cal = '365_day'
        case ('all_leap')
          cal = '366_day'
        case ('366_day')
          cal = '366_day'
        case ('julian')
          cal = 'julian'
        case ('none')
          cal = 'none'
        case default
          write(*,*) ' calendar = ',text(1:length),' is not supported!'
          stop
        end select
      end if

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,name,'cannot close file')

      end subroutine get_nc_time

      subroutine read_nc_dim(file, name, ndim, data)
!======================================================================
!
!======================================================================
      use ncio_mod
      implicit none

! ... intent
      character(len=*), intent(in) :: file
      character(len=*), intent(in) :: name
      integer, intent(in)  :: ndim
      real   , intent(inout) :: data(ndim)

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: xtype
      integer :: dimlen
      integer(2), allocatable :: ibuf2(:)
      integer   , allocatable :: ibuf4(:)
      real(4)   , allocatable :: rbuf4(:)
      real(8)   , allocatable :: rbuf8(:)
      real(4) :: offset, scale, missing_value

! ... open file
      ier = nf_open(file, nf_nowrite, ncid)
      call check_nc(ier,file,name,'cannot open file : read dim')

! ... get variable id
      ier = nf_inq_dimid(ncid, name, varid)
      call check_nc(ier,file,name,'cannot get varid')

! ... get variable dimension length
      ier = nf_inq_dimlen(ncid, varid, dimlen)
      call check_nc(ier,file,name,'cannot get dimlen')

! ... check dimension size
      if (dimlen /= ndim) then
        write(*,*) 'variable : ',trim(name)
        write(*,*) 'dim in nc : ',dimlen,' is different from ',ndim
        stop
      end if

! ... get variable id
      ier = nf_inq_varid(ncid, name, varid)
      call check_nc(ier,file,name,'cannot get varid')

! ... get variable type
      ier = nf_inq_vartype(ncid, varid, xtype)
      call check_nc(ier,file,name,'cannot get varriable type')

! ... get variable
      select case (xtype)
      case (nf_float)
        allocate(rbuf4(ndim))
        ier = nf_get_var_real(ncid, varid, rbuf4)
        call check_nc(ier,file,name,'cannot get var')
        data = rbuf4
        deallocate(rbuf4)
      case (nf_double)
        allocate(rbuf8(ndim))
        ier = nf_get_var_double(ncid, varid, rbuf8)
        call check_nc(ier,file,name,'cannot get var')
        data = rbuf8
        deallocate(rbuf8)
      case default
        write(*,*) 'variable : ',trim(name)
        write(*,*) 'dim in nc : ',xtype,' is not supported!'
        stop
      end select

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,name,'cannot close file')

      return
      end

      subroutine read_nc_data(file, name, &
     &           nlon, nlat, data, lev, itim, undef)
!======================================================================
!
!======================================================================
      use ncio_mod
      implicit none

! ... intent
      character(len=*), intent(in) :: file
      character(len=*), intent(in) :: name
!     character(len=*), intent(in) :: long
!     character(len=*), intent(in) :: unit
      integer, intent(in)  :: nlon, nlat, lev, itim
      real   , intent(in)  :: undef
      real   , intent(out) :: data(nlon,nlat)

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: xtype
      integer :: ndims
      integer :: nsize

      integer   , allocatable :: start(:)
      integer   , allocatable :: count(:)
      integer(2), allocatable :: ibuf2(:,:)
      real(4)   , allocatable :: rbuf4(:,:)
      real(8)   , allocatable :: rbuf8(:,:)
      real(4) :: offset, scale, missing_value

      character(len=45), dimension(6) :: type_kind = &
     &  (/ 'byte   (fill_byte   = -127                  )',  &
     &     'char   (fill_char   = 0                     )',  &
     &     'short  (fill_short  = -32767                )',  &
     &     'int    (fill_int    = -2147483647           )',  &
     &     'float  (fill_float  = 9.9692099683868690e+36)',  &
     &     'double (fill_double = 9.9692099683868690e+36)'/)

! ... open file
      ier = nf_open(file, nf_nowrite, ncid)
      call check_nc(ier,file,name,'cannot open file : read var')

! ... get variable id
      ier = nf_inq_varid(ncid, name, varid)
      call check_nc(ier,file,name,'cannot get varid')

! ... get variable type
      ier = nf_inq_vartype(ncid, varid, xtype)
      call check_nc(ier,file,name,'cannot get variable type')

! ... get long name
!     ier = nf_get_att_text (ncid, varid, 'long_name', long)
!     call check_nc(ier,file,name,'cannot get long name')

! ... get units
!     ier = nf_get_att_text (ncid, varid, 'units', unit)
!     call check_nc(ier,file,name,'cannot get units')

! ... get offset & scale
      select case (xtype)
      case (nf_short)
        ier = nf_get_att_real (ncid, varid, 'add_offset'  , offset)
!       call check_nc(ier,file,name,'cannot get add_offset')
        if (ier /= 0) offset = 0.0
        ier = nf_get_att_real (ncid, varid, 'scale_factor', scale)
!       call check_nc(ier,file,name,'cannot get scale_factor')
        if (ier /= 0) scale = 1.0
      case default
        offset = 0.0
        scale  = 1.0
      end select

! ... get undefined value
      ier = nf_get_att_real (ncid, varid, 'missing_value', missing_value)
      if (ier /= 0) then
      ier = nf_get_att_real (ncid, varid, 'fill_value', missing_value)
      end if
      if (ier /= 0) then
        select case (xtype)
        case(nf_short)
          missing_value = 32767
        case(nf_real)
          missing_value = 9.9692099683868690e+36
        case default
          write(*,*) 'type : ',type_kind(xtype)
          call check_nc(ier,file,name,'cannot get missing_value')
        end select
      end if

! ... get num. of dimension
      ier = nf_inq_varndims(ncid, varid, ndims)
      call check_nc(ier,file,name,'cannot get varndims')

! ... set count & start ---
      select case (ndims)
      case (2)
        nsize = 2
      case (3)
        nsize = 3
      case (4)
        nsize = 4
      case default
        write(*,*) 'num of dimension = ',ndims,' for ',trim(name)
        stop '1d variable is not supported'
      end select

      allocate(start(nsize)) ; allocate(count(nsize))

      select case (nsize)
      case (2)
        start(1) = 1   ; count(1) = nlon
        start(2) = 1   ; count(2) = nlat
      case (3)
        start(1) = 1   ; count(1) = nlon
        start(2) = 1   ; count(2) = nlat
        start(3) = itim; count(3) = 1
      case (4)
        start(1) = 1   ; count(1) = nlon
        start(2) = 1   ; count(2) = nlat
        start(3) = lev ; count(3) = 1
        start(4) = itim; count(4) = 1
      case default
        write(*,*) 'num of dimension = ',nsize,' for ',trim(name)
        stop '1d variable is not supported'
      end select

! ... get variable
      select case (xtype)
      case (nf_short)
        allocate(ibuf2(nlon,nlat))
        ier = nf_get_vara_int2(ncid, varid, start, count, ibuf2)
        call check_nc(ier,file,name,'cannot get vara_int2')

        where (ibuf2 == missing_value)
          data = undef
        else where
          data = ibuf2 * scale + offset
        end where
        deallocate(ibuf2)
        
      case (nf_real)
        allocate(rbuf4(nlon,nlat))
        ier = nf_get_vara_real(ncid, varid, start, count, rbuf4)
        call check_nc(ier,file,name,'cannot get vara_real')

        where (rbuf4 == missing_value)
          data = undef
        else where
          data = rbuf4
        end where
        deallocate(rbuf4)
        
      case (nf_double)
        allocate(rbuf8(nlon,nlat))
        ier = nf_get_vara_double(ncid, varid, start, count, rbuf8)
        call check_nc(ier,file,name,'cannot get vara_double')

        where (rbuf8 == missing_value)
          data = undef
        else where
          data = rbuf8
        end where
        deallocate(rbuf8)

      case default
        write(*,*) 'variable : ',trim(name)
        write(*,*) 'cannot read data type : ',type_kind(xtype)
        stop
      end select

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,name,'cannot close file')

      return
      end

      subroutine def_nc_file(file,title, &
     &                       nlon,nlat,nlev,rlon,rlat,rlev, &
     &                       time_unit,id)
!======================================================================
!     write dimension
!======================================================================
      use ncio_mod
      implicit none

! ... intent
      character(len=*), intent(in) :: file
      character(len=*), intent(in) :: title
      character(len=*), intent(in) :: time_unit
      integer, intent(in) :: nlon, nlat, nlev
      integer, intent(in) :: id(4)
      real   , intent(in) :: rlon(nlon), rlat(nlat), rlev(nlev)

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: tid, xid, yid, zid
      real(4)   , allocatable :: rbuf4(:)
      real(8)   , allocatable :: rbuf8(:)

! ... open file
      ier = nf_create(file, nf_clobber, ncid)
      call check_nc(ier,file,title,'cannot open file : write dim')

! ... write file attribution
      ier = nf_put_att_text(ncid, nf_global, 'title', &
     &                      len_trim(title), trim(title))
      call check_nc(ier,file,title,'cannot put title')

! ... define time unit
      select case (trim(time_unit))
        case('secs')
          write (t_unit,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     &    'secs since ',id(1),'-',id(2),'-',id(3),' 00:00:00'
        case('hours')
          write (t_unit,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     &    'hours since ',id(1),'-',id(2),'-',id(3),' 00:00:00'
        case('days')
          write (t_unit,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     &    'days since ',id(1),'-',id(2),'-',id(3),' 00:00:00'
        case('years')
          write (t_unit,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     &    'years since ',id(1),'-',id(2),'-',id(3),' 00:00:00'
        case default
          write (t_unit,'(a,i4.4,a,i2.2,a,i2.2,a)') &
     &    'days since ',id(1),'-',id(2),'-',id(3),' 00:00:00'
      end select

! ... define dimension : t
      ier = nf_def_dim(ncid, t_name, nf_unlimited, tid)
      call check_nc(ier,file,t_name,'cannot define dim')
      ier = nf_def_var(ncid, t_name, nf_real, 1, tid, varid)
      call check_nc(ier,file,t_name,'cannot define varid')
      ier = nf_put_att_text(ncid, varid, 'long_name', &
     &                      len_trim(t_long), trim(t_long))
      call check_nc(ier,file,t_name,'cannot define long_name')
      ier = nf_put_att_text(ncid, varid, 'units', &
     &                      len_trim(t_unit), trim(t_unit))
      call check_nc(ier,file,t_name,'cannot define units')
      ier = nf_put_att_text(ncid, varid, 'calendar', &
     &                      len_trim(calendar), trim(calendar))
      call check_nc(ier,file,t_name,'cannot define calendar')

! ... define dimension : x
      ier = nf_def_dim(ncid, x_name, nlon, xid)
      call check_nc(ier,file,x_name,'cannot define dim')
      ier = nf_def_var(ncid, x_name, nf_real, 1, xid, varid)
      call check_nc(ier,file,x_name,'cannot define varid')
      ier = nf_put_att_text(ncid, varid, 'long_name', &
     &                      len_trim(x_long), trim(x_long))
      call check_nc(ier,file,x_name,'cannot define long_name')
      ier = nf_put_att_text(ncid, varid, 'units', &
     &                      len_trim(x_unit), trim(x_unit))
      call check_nc(ier,file,x_name,'cannot define units')

! ... define dimension : y
      ier = nf_def_dim(ncid, y_name, nlat, yid)
      call check_nc(ier,file,y_name,'cannot define dim')
      ier = nf_def_var(ncid, y_name, nf_real, 1, yid, varid)
      call check_nc(ier,file,y_name,'cannot define varid')
      ier = nf_put_att_text(ncid, varid, 'long_name', &
     &                      len_trim(y_long), trim(y_long))
      call check_nc(ier,file,y_name,'cannot define long_name')
      ier = nf_put_att_text(ncid, varid, 'units', &
     &                      len_trim(y_unit), trim(y_unit))
      call check_nc(ier,file,y_name,'cannot define units')

! ... define dimension : z
      ier = nf_def_dim(ncid, z_name, nlev, zid)
      call check_nc(ier,file,z_name,'cannot define dim')
      ier = nf_def_var(ncid, z_name, nf_real, 1, zid, varid)
      call check_nc(ier,file,z_name,'cannot define varid')
      ier = nf_put_att_text(ncid, varid, 'long_name', &
     &                      len_trim(z_long), trim(z_long))
      call check_nc(ier,file,z_name,'cannot define long_name')
      ier = nf_put_att_text(ncid, varid, 'units', &
     &                      len_trim(z_unit), trim(z_unit))
      call check_nc(ier,file,z_name,'cannot define units')

!     ier = nf_put_att_text(ncid, varid, 'positive', &
!    &                      len_trim(updown), trim(updown))
!     call check_nc(ier,file,z_name,'cannot define +/-')

! ... end define mode
      ier = nf_enddef(ncid)

! ... write dimension : x
      allocate(rbuf4(nlon))
      rbuf4 = rlon
      ier = nf_inq_varid(ncid, x_name, varid)
      call check_nc(ier,file,x_name,'cannot get varid')
      ier = nf_put_var_real(ncid, varid, rbuf4)
      call check_nc(ier,file,x_name,'cannot put var')
      deallocate(rbuf4)

! ... write dimension : y
      allocate(rbuf4(nlat))
      rbuf4 = rlat
      ier = nf_inq_varid(ncid, y_name, varid)
      call check_nc(ier,file,y_name,'cannot get varid')
      ier = nf_put_var_real(ncid, varid, rbuf4)
      call check_nc(ier,file,y_name,'cannot put var')
      deallocate(rbuf4)

! ... write dimension : z
      allocate(rbuf4(nlev))
      rbuf4 = rlev
      ier = nf_inq_varid(ncid, z_name, varid)
      call check_nc(ier,file,z_name,'cannot get varid')
      ier = nf_put_var_real(ncid, varid, rbuf4)
      call check_nc(ier,file,z_name,'cannot put var')
      deallocate(rbuf4)

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,title,'cannot close file')

      return
      end

      subroutine def_nc_var(file, &
     &                      name,long,unit,xyzt,&
     &                      xtype,offset,scale,undef)
!======================================================================
!     write variable attribution
!======================================================================
      use ncio_mod
      implicit none

! ... intent
      character(len=*), intent(in) :: file
      character(len=*), intent(in) :: name
      character(len=*), intent(in) :: long
      character(len=*), intent(in) :: unit
      character(len=*), intent(in) :: xtype
      character(len=*), intent(in) :: xyzt
      real    :: offset, scale, undef

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: nsize
      integer :: tid, xid, yid, zid
      integer, allocatable :: size(:)
      real(4) :: offset4, scale4, undef4

! ... set constants
      offset4 = offset
      scale4  = scale
      undef4  = undef

! ... open file
      ier = nf_open(file, nf_write, ncid)
      call check_nc(ier,file,name,'cannot open file : write var def')

! ... start define mode
      ier = nf_redef(ncid)

! ... define dimension of variable
      if (trim(xyzt) == 'xyzt') then
        nsize = 4
      else
        nsize = 3
      end if

      allocate(size(nsize))

      select case (nsize)
      case (3)
        ier = nf_inq_dimid(ncid, x_name, xid)
        call check_nc(ier,file,x_name,'cannot get dimid')
        ier = nf_inq_dimid(ncid, y_name, yid)
        call check_nc(ier,file,y_name,'cannot get dimid')
        ier = nf_inq_dimid(ncid, t_name, tid)
        call check_nc(ier,file,t_name,'cannot get dimid')
        size(1) = xid
        size(2) = yid
        size(3) = tid
        select case (trim(xtype))
        case ('short')
          ier = nf_def_var(ncid, name, nf_short, 3, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        case ('real4')
          ier = nf_def_var(ncid, name, nf_real , 3, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        case default
          ier = nf_def_var(ncid, name, nf_real , 3, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        end select
      case (4)
        ier = nf_inq_dimid(ncid, x_name, xid)
        call check_nc(ier,file,x_name,'cannot get dimid')
        ier = nf_inq_dimid(ncid, y_name, yid)
        call check_nc(ier,file,y_name,'cannot get dimid')
        ier = nf_inq_dimid(ncid, z_name, zid)
        call check_nc(ier,file,z_name,'cannot get dimid')
        ier = nf_inq_dimid(ncid, t_name, tid)
        call check_nc(ier,file,t_name,'cannot get dimid')
        size(1) = xid
        size(2) = yid
        size(3) = zid
        size(4) = tid
        select case (trim(xtype))
        case ('short')
          ier = nf_def_var(ncid, name, nf_short, 4, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        case ('real4')
          ier = nf_def_var(ncid, name, nf_real , 4, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        case default
          ier = nf_def_var(ncid, name, nf_real , 4, size, varid)
          call check_nc(ier,file,name,'cannot define varid')
        end select
      case default
        stop 'num. of dimension is wrong'
      end select
      deallocate(size)

! ... write variable attribution
      ier = nf_put_att_text(ncid, varid, 'long_name', &
     &                      len_trim(long), trim(long))
      call check_nc(ier,file,name,'cannot define long_name')
      ier = nf_put_att_text(ncid, varid, 'units', &
     &                      len_trim(unit), trim(unit))
      call check_nc(ier,file,name,'cannot define units')

      select case (xtype)
      case ('short')
        ier = nf_put_att_real(ncid, varid, 'add_offset', &
     &                        nf_real, 1, offset4)
        call check_nc(ier,file,name,'cannot define add_offset')
        ier = nf_put_att_real(ncid, varid, 'scale_factor', &
     &                        nf_real, 1, scale4)
        call check_nc(ier,file,name,'cannot define scale_factor')
        ier = nf_put_att_int(ncid, varid, 'missing_value', &
     &                        nf_short, 1, 32767)
        call check_nc(ier,file,name,'cannot define missing_value')
      case ('real4')
        ier = nf_put_att_real(ncid, varid, 'missing_value', &
     &                        nf_real, 1, undef4)
        call check_nc(ier,file,name,'cannot define missing_value')
      case default
        ier = nf_put_att_real(ncid, varid, 'missing_value', &
     &                        nf_real, 1, undef4)
        call check_nc(ier,file,name,'cannot define missing_value')
      end select

! ... end define mode
      ier = nf_enddef(ncid)

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,name,'cannot close file')

      return
      end

      subroutine write_nc_data(file, name, xyzt, &
     &                         nlon, nlat, data, lev, itim, time, undef)
!======================================================================
!     write variable attribution
!======================================================================
      use ncio_mod
      implicit none

! ... intent
      character(len=*), intent(in) :: file
      character(len=*), intent(in) :: name
      character(len=*), intent(in) :: xyzt
      integer, intent(in) :: nlon, nlat, lev, itim
      real   , intent(in) :: data(nlon,nlat)
      real   , intent(in) :: undef
      real(8), intent(in) :: time

! ... local
      integer :: ier
      integer :: ncid
      integer :: varid
      integer :: xtype
      integer :: nsize

      real    :: val
      real    :: offset, scale
      real(4) :: offset4, scale4

      integer   , allocatable :: start(:)
      integer   , allocatable :: count(:)
      integer(2), allocatable :: ibuf2(:,:)
      real(4)   , allocatable :: rbuf4(:,:)
      real(8)   , allocatable :: rbuf8(:,:)

      integer :: i, j

! ... open file
      ier = nf_open(file, nf_write, ncid)
      call check_nc(ier,file,name,'cannot open file : write var')

! ... write time
      ier = nf_inq_varid(ncid, t_name, varid)
      call check_nc(ier,file,t_name,'cannot get varid')
!     ier = nf_put_vara_real(ncid, varid, itim, 1, time)
      ier = nf_put_vara_double(ncid, varid, itim, 1, time)
      call check_nc(ier,file,t_name,'cannot put var')

! ... get variable ID 
      ier = nf_inq_varid(ncid, name, varid)
      call check_nc(ier,file,name,'cannot get varid')

! ... get variable type
      ier = nf_inq_vartype(ncid, varid, xtype)
      call check_nc(ier,file,name,'cannot get variable type')

! ... get scale and offset
      if (xtype == nf_short) then
        ier = nf_get_att_real(ncid, varid, 'add_offset'  , offset4)
        call check_nc(ier,file,name,'cannot get add_offset')
        ier = nf_get_att_real(ncid, varid, 'scale_factor', scale4)
        call check_nc(ier,file,name,'cannot get scale_factor')
        offset = offset4
        scale  = scale4
      else
        offset = 0.0
        scale  = 1.0
      end if

! ... write variable
      if (trim(xyzt) == 'xyzt') then
        nsize = 4
      else
        nsize = 3
      end if

      allocate(start(nsize)) ; allocate(count(nsize))

      select case (nsize)
      case (3)
        start(1) = 1   ; count(1) = nlon
        start(2) = 1   ; count(2) = nlat
        start(3) = itim; count(3) = 1
      case (4)
        start(1) = 1   ; count(1) = nlon
        start(2) = 1   ; count(2) = nlat
        start(3) = lev ; count(3) = 1
        start(4) = itim; count(4) = 1
      case default
        stop 'num. of dimension is wrong'
      end select

      select case (xtype)
      case(nf_short)
        allocate(ibuf2(nlon,nlat))
        do j = 1, nlat
        do i = 1, nlon
          if (data(i,j) == undef) then
            ibuf2(i,j) = 32767
          else
            val = ( data(i,j) - offset )/scale
            if (abs(val) >= 32767) then
              write(*,*) 'name   = ',name
              write(*,*) 'scale  = ',scale
              write(*,*) 'offset = ',offset
              write(*,*) 'undef  = ',undef
              write(*,*) 'val    = ',val
              write(*,*) 'data   = ',data(i,j)
              stop
            else
              ibuf2(i,j) = nint(val)
            end if
          end if
        end do
        end do

        ier = nf_put_vara_int2(ncid, varid, start, count, ibuf2)
        call check_nc(ier,file,name,'cannot put vara_int2')
        deallocate(ibuf2)

      case(nf_real)
        allocate(rbuf4(nlon,nlat))
        rbuf4 = data
        ier = nf_put_vara_real(ncid, varid, start, count, rbuf4)
        call check_nc(ier,file,name,'cannot put vara_real')
        deallocate(rbuf4)

      case(nf_double)
        allocate(rbuf8(nlon,nlat))
        rbuf8 = data
        ier = nf_put_vara_real(ncid, varid, start, count, rbuf8)
        call check_nc(ier,file,name,'cannot put vara_real')
        deallocate(rbuf8)

      case default
        allocate(rbuf4(nlon,nlat))
        rbuf4 = data
        ier = nf_put_vara_real(ncid, varid, start, count, rbuf4)
        call check_nc(ier,file,name,'cannot put vara_real')
        deallocate(rbuf4)

      end select
      deallocate(start, count)

! ... close file
      ier = nf_close(ncid)
      call check_nc(ier,file,name,'cannot close file')

      return
      end

#!/bin/bash

#MMMMMMMMMMMMMM INCLUDE ###################
. ./06.LFM.MAKE.CTL.sh

#MMMMMMMMMMMMMM INCLUDE ###################



FH=$1; YMDH=$2; 
LINE1=$3; LON1_1=$4; LAT1_1=$5; LON1_2=$6; LAT1_2=$7
LINE2=$8; LON2_1=$9; LAT2_2=${10}; LON2_2=${11}; LAT2_2=${12}

YMDH=${YMDH:-2022061900}
YYYY=${YMDH:0:4}; MM=${YMDH:4:2}; DD=${YMDH:6:2}; HH=${YMDH:8:2}

echo MMMMMMM DATE ${YYYY} ${MM} ${DD} ${HH}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

FH=${FH:-00}

MODEL=LFM

LONW=122.0 ; LONE=134; LATS=24.5; LATN=35 #MAP AREA


MMDD=${MM}${DD}

DATE=${HH}UTC${DD}${MMM}${YYYY}

VAR1=WVFMAG; UNIT1=kg/m2/s; V1LEV=950; CMIN1=0.05; CMAX1=0.5; CINT1=0.05; CLEV1=0.1
VAR2=VPT; UNIT2=K; V2LEV=950; CMIN2=290; CMAX2=310; CINT2=1; CLEV2M=304
VAR3=MAUL; V3LEV=950

VAR4=WVFMAG; UNIT4=kg/m2/s; V4LEV=850; CMIN4=0.03; CMAX4=0.3; CINT4=0.03; CLEV4=0.1
VAR5=VPT; UNIT5=g/kg; V5LEV=850; CMIN5=290; CMAX5=310; CINT5=1; CLEV5M=310
VAR6=MAUL; V6LEV=850

SCLV=0.5 ;# WVF

FIGDIR=FH${FH}; mkd $FIGDIR
FIG=${FIGDIR}/${MODEL}_FH${FH}_${MMDD}${HH}_${LINE1}_${LINE2}_WVF_ROT.pdf

KIND='white->lightcyan->cyan->mediumspringgreen->lawngreen->moccasin->orange->orangered->red->magenta'


HOST=$(hostname); CWD=$(pwd); TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo CREATE CTL FILE

CTL1=LFM.CTL; echo $CTL1

INDIR1=/work01/DATA/LFM_PROC_ROT_v3/FH${FH}/
INFLE_CHK=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_THERMO.nc
INFLE1=LFM_PRS_FH${FH}_VALID_%y4-%m2-%d2_%h2_THERMO.nc
DSET1=$INDIR1/${INFLE1}
DSET_CHK=$INDIR1/${INFLE_CHK}
if [ ! -f $DSET_CHK ]; then echo NO SUCH FILE, $DSET_CHK; echo; exit 1; fi

LFM_MAKE_CTL $CTL1 $DSET1
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

echo NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
echo CREATE CTL FILE

CTL2=LFM_ROT_1.CTL; echo $CTL2

INDIR2=/work01/DATA/LFM_PROC_ROT_v2/FH${FH}
INFLE_CHK=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_ROT_UV_${LON1_1}-${LAT1_1}-${LON1_2}-${LAT1_2}.nc
INFLE2=LFM_PRS_FH${FH}_VALID_%y4-%m2-%d2_%h2_ROT_UV_${LON1_1}-${LAT1_1}-${LON1_2}-${LAT1_2}.nc

if [ ! -f $INDIR2/${INFLE_CHK} ]; then echo NO SUCH FILE, $INDIR2/${INFLE_CHK}; echo; exit 1; fi

DSET2=$INDIR2/${INFLE2}

LFM_CTL_ROTATED_COORD $CTL2 $DSET2
echo NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN



GS=$(basename $0 .sh).GS

cat <<EOF>$GS

'open $CTL1' ;#'q ctlinfo'; say result
'open $CTL2' ;#'q ctlinfo'; say result
'cc'; 'set grid off'

say 'MMMMM SET FIGURE SIZE'
'set vpage 0.0 8.5 0 10.5'

xmax=2; ymax=2
xleft=0.5; ytop=8

xwid =  6/xmax; ywid =  5/ymax
xmargin=1;    ymargin=1



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM MAP 1 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grads off'


say 'SHADE $VAR1'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${HH}Z${DD}${MMM}${YYYY}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $V1LEV'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.12'; 'set xlint 4'
'set ylopts 1 3 0.12'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

'set gxout shade2'
'color $CMIN1 $CMAX1 $CINT1 -kind $KIND'
'd mag(wvx,wvy)'

'set xlab off'; 'set ylab off'


say 'CONTOUR $VAR2 $V2LEV'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT2'
'set lev $V2LEV'
'q dims'; say sublin(result,4)
'd $VAR2'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint $CINT2'
'set lev $V2LEV'
'd $VAR2'



'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3


say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 1'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 1'

say 'CONTOUR $VAR2 $CLEV2M'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV2M'
'set lev $V2LEV'
'd $VAR2'

say 'MMMMM MAUL'
'set gxout contour'; 'set cthick 10'; 
'set rgb 98 128 0 128 -200'; 'set ccolor 98'
#'set clevs 1'
'set lev $V3LEV'
'q dims'; say sublin(result,4)
'd ${VAR3}'

say 'mmmmm COLOR BAR'
x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
xx=(x1+x2)/2; yy=y2+0.15; 'set string 1 c 3 0'
'draw string ' xx ' ' yy '$VAR1 [$UNIT1]'

say 'MMMMM VECTOR $V1LEV'
'set gxout vector'; 'set cthick 6'; 'set ccolor 0'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V1LEV'
'q dims'; say sublin(result,4)
'd skip(wvx,25,25);wvy'

'set gxout vector'; 'set cthick 2'; 'set ccolor 1'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V1LEV'
'q dims'; say sublin(result,4)
'd skip(wvx,25,25);wvy'


say 'mmmmm PANEL TITLE'
'set strsiz 0.1 0.12'; 'set string 1 l 3'
xx=xl; yy=yt+0.15; 'draw string 'xx' 'yy' ${DATE} FH${FH} ${V1LEV}hPa'

say 'mmmmm SAVE COORDINATES OF TOP-LEFT CORNER FOR HEADER'
XSLEFT=xl; YTTOP=yt



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM MAP 2 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grads off'

say 'SHADE $VAR4'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${HH}Z${DD}${MMM}${YYYY}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $V4LEV'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.12'; 'set xlint 4'
'set ylopts 1 3 0.12'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

'set gxout shade2'
'color $CMIN4 $CMAX4 $CINT4 -kind $KIND'
'd mag(wvx,wvy)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set xlab off'; 'set ylab off'


say 'CONTOUR $VAR5 $V5LEV'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT5'
'set lev $V5LEV'
'q dims'; say sublin(result,4)
'd $VAR5'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint $CINT5'
'set lev $V5LEV'
'd $VAR5'


say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 1'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 1'


say 'CONTOUR $VAR5 $CLEV5M'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV5M'
'set lev $V5LEV'
'd $VAR5'

say 'MMMMM MAUL'
'set gxout contour'; 'set cthick 10'; 
'set rgb 98 128 0 128 -200'; 'set ccolor 98'
#'set clevs 1'
'set lev $V6LEV'
'q dims'; say sublin(result,4)
'd ${VAR6}'

say 'mmmmm COLOR BAR'
x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
xx=(x1+x2)/2; yy=y2+0.15; 'set string 1 c 3 0'
'draw string ' xx ' ' yy '$VAR1 [$UNIT1]'

say 'MMMMM VECTOR $V4LEV'
'set gxout vector'; 'set cthick 6'; 'set ccolor 0'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V4LEV'
'q dims'; say sublin(result,4)
'd skip(wvx,25,25);wvy'

'set gxout vector'; 'set cthick 2'; 'set ccolor 1'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V4LEV'
'q dims'; say sublin(result,4)
'd skip(wvx,25,25);wvy'

say 'mmmmm PANEL TITLE'
'set strsiz 0.1 0.12'; 'set string 1 l 3'
xx=xl; yy=yt+0.15; 'draw string 'xx' 'yy' ${DATE} FH${FH} ${V4LEV}hPa'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM MAP 3 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=2; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grads off'


say 'SHADE $VAR1'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${HH}Z${DD}${MMM}${YYYY}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $V1LEV'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.12'; 'set xlint 4'
'set ylopts 1 3 0.12'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

'set gxout shade2'
'color $CMIN1 $CMAX1 $CINT1 -kind $KIND'
'd mag(wvxr.2,wvyr.2)'

'set xlab off'; 'set ylab off'


say 'CONTOUR $VAR2 $V2LEV'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT2'
'set lev $V2LEV'
'q dims'; say sublin(result,4)
'd $VAR2'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint $CINT2'
'set lev $V2LEV'
'd $VAR2'



'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3


say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 1'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 1'

say 'CONTOUR $VAR2 $CLEV2M'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV2M'
'set lev $V2LEV'
'd $VAR2'

say 'MMMMM MAUL'
'set gxout contour'; 'set cthick 10'; 
'set rgb 98 128 0 128 -200'; 'set ccolor 98'
#'set clevs 1'
'set lev $V3LEV'
'q dims'; say sublin(result,4)
'd ${VAR3}'

say 'mmmmm COLOR BAR'
x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
xx=(x1+x2)/2; yy=y2+0.15; 'set string 1 c 3 0'
'draw string ' xx ' ' yy '$VAR1 [$UNIT1]'

say 'MMMMM VECTOR $V1LEV'
'set gxout vector'; 'set cthick 6'; 'set ccolor 0'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V1LEV'
'q dims'; say sublin(result,4)
'd skip(wvxr.2,25,25);wvyr.2'

'set gxout vector'; 'set cthick 2'; 'set ccolor 1'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V1LEV'
'q dims'; say sublin(result,4)
'd skip(wvxr.2,25,25);wvyr.2'


say 'mmmmm PANEL TITLE'
'set strsiz 0.1 0.12'; 'set string 1 l 3'
xx=xl; yy=yt+0.15; 'draw string 'xx' 'yy' ROT $LINE1 ${DATE} FH${FH} ${V1LEV}hPa'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM MAP 4 MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=2; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set grads off'

say 'SHADE $VAR4'

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${HH}Z${DD}${MMM}${YYYY}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $V4LEV'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.12'; 'set xlint 4'
'set ylopts 1 3 0.12'; 'set ylint 2'


'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

'set gxout shade2'
'color $CMIN4 $CMAX4 $CINT4 -kind $KIND'
'd mag(wvxr.2,wvyr.2)'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set xlab off'; 'set ylab off'


say 'CONTOUR $VAR5 $V5LEV'
'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT5'
'set lev $V5LEV'
'q dims'; say sublin(result,4)
'd $VAR5'

'set ccolor 1'; 'set cthick 1'
'set clab off'; 'set cint $CINT5'
'set lev $V5LEV'
'd $VAR5'


say 'MMMMM LINE'
'trackplot $LON1_1 $LAT1_1 $LON1_2 $LAT1_2 -c 1 -l 1 -t 1'
'trackplot $LON2_1 $LAT2_1 $LON2_2 $LAT2_2 -c 1 -l 1 -t 1'


say 'CONTOUR $VAR5 $CLEV5M'
'set clopts 1 2 0.07'
'set gxout contour'
'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set clevs $CLEV5M'
'set lev $V5LEV'
'd $VAR5'

say 'MMMMM MAUL'
'set gxout contour'; 'set cthick 10'; 
'set rgb 98 128 0 128 -200'; 'set ccolor 98'
#'set clevs 1'
'set lev $V6LEV'
'q dims'; say sublin(result,4)
'd ${VAR6}'

say 'mmmmm COLOR BAR'
x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
xx=(x1+x2)/2; yy=y2+0.15; 'set string 1 c 3 0'
'draw string ' xx ' ' yy '$VAR1 [$UNIT1]'

say 'MMMMM VECTOR $V4LEV'
'set gxout vector'; 'set cthick 6'; 'set ccolor 0'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V4LEV'
'q dims'; say sublin(result,4)
'd skip(wvxr.2,25,25);wvyr.2'

'set gxout vector'; 'set cthick 2'; 'set ccolor 1'
'set arrscl 0.5 $SCLV' ;# 'set arrlab off'
'set lev $V4LEV'
'q dims'; say sublin(result,4)
'd skip(wvxr.2,25,25);wvyr.2'

say 'mmmmm PANEL TITLE'
'set strsiz 0.1 0.12'; 'set string 1 l 3'
xx=xl; yy=yt+0.15; 'draw string 'xx' 'yy' ROT $LINE1 ${DATE} FH${FH} ${V4LEV}hPa'



say 'mmmmm HEADER'
xx = XSLEFT; yy = YTTOP
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
yy = yy+0.6; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${CMD} $@'
yy = yy+0.23; 'draw string ' xx ' ' yy ' ${TIMESTAMP}'



'gxprint $FIG'

'quit'
EOF


grads -bcp $GS
rm -vf $GS
if [ -f $FIG ];then echo; echo OUTPUT $FIG; echo; fi

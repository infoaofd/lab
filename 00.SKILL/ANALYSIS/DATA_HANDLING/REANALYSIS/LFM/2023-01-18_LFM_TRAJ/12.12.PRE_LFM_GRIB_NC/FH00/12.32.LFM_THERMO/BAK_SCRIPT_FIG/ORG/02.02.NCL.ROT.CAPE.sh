#!/bin/bash

FH=00
YYYY=2022; MM=06; DD=19; HH=00
INDIR=/work01/DATA/LFM_HCUT.PDEL/FH${FH}

 ODIR=/work01/DATA/LFM_PROC_ROT_v3/FH${FH}

echo "MMMMM LINE B"
#LON1B=127; LAT1B=30; LON2B=126; LAT2B=30
LON1B=124; LAT1B=26.7; LON2B=130; LAT2B=31.5
echo "MMMMM $LON1B $LAT1B $LON2B $LAT2B"

LON1=$LON1B; LAT1=$LAT1B; LON2=$LON2B; LAT2=$LAT2B

mkd $ODIR

NCL=$(basename $0 .sh)
if [ ! -f $NCL ];then echo NO SUCH FILE,$NCL;exit 0;fi

MMDD=${MM}${DD}

 INFLE=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
 IN=$INDIR/${INFLE}
OUT1=$ODIR/$(basename $IN .nc)_CAPE.nc
OUT2=$ODIR/$(basename $IN .nc)_ROT_UV_${LON1}-${LAT1}-${LON2}-${LAT2}.nc

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN; echo; exit 1; fi

runncl.sh $NCL $IN $OUT1 $OUT2  $LON1 $LAT1 $LON2 $LAT2

if [ $? -ne 0 ]; then
echo "mmmmmmmmmmmmmm"; echo ERROR IN $NCL; echo "mmmmmmmmmmmmmm"
fi
if [ -f $OUT1 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT1; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT1; echo "mmmmmmmmmmmmmm"
exit 1
fi

if [ -f $OUT2 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT2; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT2; echo "mmmmmmmmmmmmmm"
exit 1
fi

cp -av $0 $ODIR
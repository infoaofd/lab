! D:\TOOLS_200804\TOOLBOX\NetCDF\netcdf_Iizuka

!----------------------------------------------------------------------
   subroutine gettime (y, m, d, ld)
     integer :: md(1:12) = (/31,28,31,30,31,30,31,31,30,31,30,31/)
     integer :: y, m, d, ld

     md(2) = 28 + leap(y) ! Add 1 for Leap.
     ld    = yday(y) + SUM( md(1:m-1) ) + d

!    ld    = ld - 2400000.5
     if (y > 1800) ld = ld + 2
!    print *,'',yday(y), SUM( md(1:m-1) ), d, ld

     return

     contains

!----------------------------------------------------------------------
     integer function leap(x)
       integer :: x
       if ( mod(x, 400) == 0) then
         leap = 1
       else if ( mod(x, 100) == 0) then
         leap = 0
       else if ( mod(x, 4) == 0) then
         leap = 1
       else
         leap = 0
       end if
     end function leap


!----------------------------------------------------------------------

     function yday(x)  result(days)
       integer :: x, days
       days = 365*(x-1) + int(x-1)/4 - int(x-1)/100 + int(x-1)/400
     end function yday

   end subroutine gettime

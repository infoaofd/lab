#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/13.12.NCL.ROT.CAPE.ASCUL

FH=$1; FH=${FH:-FH00}
YYYY=$2; MM=$3; DD=$4; HH=$5
YYYY=${YYYY:-2022}; MM=${MM:-06}; DD=${DD:-19}; HH=${HH:-00}

INDIR=/work01/DATA/LFM_HCUT.PDEL/${FH}        # PRS
INDIR2=/work01/DATA/LFM_HCUT.SFC.HINTPL/${FH} # SFC
INDIR3=/work02/DATA/ETOPO1                      # TOPOGRAPHY
 ODIR=/work01/DATA/LFM_PROC_ROT_v5/${FH}

echo "MMMMM LINE B"
#LON1B=127; LAT1B=30; LON2B=126; LAT2B=30
LON1B=124; LAT1B=26.7; LON2B=130; LAT2B=31.5
echo "MMMMM $LON1B $LAT1B $LON2B $LAT2B"

LON1=$LON1B; LAT1=$LAT1B; LON2=$LON2B; LAT2=$LAT2B

mkd $ODIR

NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo NO SUCH FILE,$NCL;exit 0;fi

TMP=TMP_$(basename $0)
echo "# $(date -R)" >$TMP; echo "# $(pwd)" >>$TMP
echo "# $(basename $0)">>$TMP; echo "# ">>$TMP
BAK=$ODIR/$0
cat $TMP $0 > $BAK
ls $BAK; head -3 $BAK
rm -f $TMP

TMP=TMP_$(basename $NCL)
echo "; $(date -R)" >$TMP; echo "; $(pwd)" >>$TMP
echo "; $(basename $NCL)">>$TMP; echo "; ">>$TMP
BAK=$ODIR/$NCL
cat $TMP $NCL > $BAK
ls $BAK; head -3 $BAK
rm -f $TMP


MMDD=${MM}${DD}

 INFLE=LFM_PRS_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
 IN=$INDIR/${INFLE}
 IN2=$INDIR2/LFM_SFC_HINTPL_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
 IN3=$INDIR3/ETOPO1_CUT_LFM_PRS.nc
OUT1=$ODIR/$(basename $IN .nc)_THERMO.nc
OUT2=$ODIR/$(basename $IN .nc)_ROT_UV_${LON1}-${LAT1}-${LON2}-${LAT2}.nc
OUT3=$ODIR/$(basename $IN .nc)_CAPE2D.nc

if [ ! -f $IN  ]; then echo NO SUCH FILE, $IN ; echo; exit 1; fi
if [ ! -f $IN2 ]; then echo NO SUCH FILE, $IN2; echo; exit 1; fi
if [ ! -f $IN3 ]; then echo NO SUCH FILE, $IN3; echo; exit 1; fi

runncl.sh $NCL $IN $IN2 $IN3 $OUT1 $OUT2 $OUT3  $LON1 $LAT1 $LON2 $LAT2

if [ $? -ne 0 ]; then
echo "mmmmmmmmmmmmmm"; echo ERROR IN $NCL; echo "mmmmmmmmmmmmmm"
fi
if [ -f $OUT1 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT1; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT1; echo "mmmmmmmmmmmmmm"
exit 1
fi

if [ -f $OUT2 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT2; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT2; echo "mmmmmmmmmmmmmm"
exit 1
fi

if [ -f $OUT3 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT3; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT3; echo "mmmmmmmmmmmmmm"
exit 1
fi

echo "NNNNNNNNNNNNNNN"; echo DONE $FH $YYYY $MM $DD $HH; echo "NNNNNNNNNNNNNNN";echo

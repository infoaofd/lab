#!/bin/bash

# LONW=120.0 ;LONE=134.0 ;LATS=22.4; LATN=35.0

FHLIST=FH00
FHLIST="FH03 FH06"

for FH in $FHLIST; do
INDIR=/work01/DATA/LFM_HCUT.PDEL/${FH}
INLIST=$(ls $INDIR/LFM_SFC_${FH}_VALID_????-??-??_??.nc)

ODIR=/work01/DATA/LFM_HCUT.SFC.HINTPL/$FH; mkd $ODIR

cat > MYGRID.TXT << EOF
gridtype = lonlat
xsize    = 281
ysize    = 316
xunits = 'degree'
yunits = 'degree'
xfirst   = 120
xinc     = 0.05
yfirst   = 22.4
yinc     = 0.04
EOF

for IN in $INLIST; do
TMP=$(basename $IN)
OUT=$ODIR/$(echo $TMP|sed -e "s/SFC/SFC_HINTPL/g")
echo MMMMM INPUT:  $IN
echo MMMMM OUTPUT: $OUT
echo MMMMM cdo remapcon,MYGRID.TXT INPUT OUTPUT
cdo remapcon,MYGRID.TXT $IN $OUT
echo

done #IN
done #FH

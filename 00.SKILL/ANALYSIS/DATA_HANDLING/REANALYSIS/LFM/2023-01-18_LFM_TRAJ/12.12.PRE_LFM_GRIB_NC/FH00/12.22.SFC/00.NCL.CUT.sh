#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/02.16.NCL.HCUT.PDEL

FH=$1 #00
FH=${FH:-00}

INDIR=/work01/DATA/LFM/FH${FH}
INLIST1=$(ls $INDIR/*surf*grib2)
INLIST2=$(ls $INDIR/*pall*grib2)

LONW=120.0 ;LONE=134.0 ;LATS=22.4; LATN=35.0
PLIST="30000 40000 50000 60000 70000 80000 85000 90000 92500 95000 97500 100000"

ODIR=/work01/DATA/LFM_HCUT.PDEL/FH${FH}; mkdir -vp $ODIR

RDM=${ODIR}/00.README.TXT
date -R  > $RDM
pwd     >> $RDM
echo $0 >> $RDM
echo $LONW $LONE $LATS $LATN >> $RDM
echo $PLIST >> $RDM
echo >>$RDM

###<<COMMENT
##IN=/work01/DATA/LFM/FH${FH}/Z__C_RJTD_20220619000000_LFM_GPV_Rjp_Lsurf_FH${FH}00.grib2

for IN in $INLIST1; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

runncl.sh 00.NCL.CUT.SFC.ncl $IN $ODIR

done # IN
###COMMENT


<<COMMENT
#IN=/work01/DATA/LFM/FH${FH}/Z__C_RJTD_20220619000000_LFM_GPV_Rjp_L-pall_FH${FH}00.grib2
NCL=00.NCL.CUT.PRS.ncl
#NCL=00.NCL.CUT.PRS_ORG_221107-1220.ncl

for IN in $INLIST2; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

runncl.sh $NCL $IN $ODIR

done 
COMMENT

echo
cp -a $0 $ODIR
cp -av /work01/DATA/LFM/*.pdf $ODIR

load "./SUBSTRING.ncl"

; # /work03/am/2022.06.ECS.OBS/26.12.LFM.NCL/02.16.NCL.HCUT.PDEL

LONW=120.0
LONE=134.0
LATS=22.4
LATN=35.0
PTOP=30000.
PBOT=100000.

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")
print("SCRIPT="+scriptname_in)
print("MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM")


IN  = getenv("NCL_ARG_2")

IN  = getenv("NCL_ARG_2")
print("mmmmmmmmmmmmmmmmmm")
print("PRS IN="+IN)
print("mmmmmmmmmmmmmmmmmm")

ODIR = getenv("NCL_ARG_3")

f=addfile(IN,"r")

T_ORG=f->TMP_P0_L100_GLL0
RH_ORG=f->RH_P0_L100_GLL0
U_ORG=f->UGRD_P0_L100_GLL0
V_ORG=f->VGRD_P0_L100_GLL0
W_ORG=f->VVEL_P0_L100_GLL0
Z_ORG=f->HGT_P0_L100_GLL0
P1_ORG=f->lv_ISBL1
LAT_ORG=f->lat_0
LON_ORG=f->lon_0
P0_ORG=f->lv_ISBL0

;printVarSummary(T2_ORG)


; CUT HORIZONTALLY
lon=LON_ORG({LONW:LONE})
lat=LAT_ORG({LATS:LATN})
p0=P0_ORG(:)
p1=P1_ORG(:)
;MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
p=p1/100.0 ;hPa
;MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

tmp_temp=T_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})
tmp_rh=RH_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})
tmp_u=U_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})
tmp_v=V_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})
tmp_w=W_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})
tmp_z=Z_ORG({PTOP:PBOT},{LATS:LATN},{LONW:LONE})



; ADD DIMENSION OF TIME
dim=dimsizes(tmp_temp)

temp=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_temp) )
temp(0,:,:,:)=tmp_temp(:,:,:)

rh=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_rh) )
rh(0,:,:,:)=tmp_rh(:,:,:)

u=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_u) )
u(0,:,:,:)=tmp_u(:,:,:)

v=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_v) )
v(0,:,:,:)=tmp_v(:,:,:)

w=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_w) )
w(0,:,:,:)=tmp_w(:,:,:)

z=new( (/1,dim(0),dim(1),dim(2)/),typeof(tmp_z) )
z(0,:,:,:)=tmp_z(:,:,:)



;jprintVarSummary(lat)
;printVarSummary(lon)


print("mmmmm SET TIME")

ITIME=temp@initial_time
FTIME=temp@forecast_time

print("ITIME="+ITIME+" FTIME="+FTIME)

MM=substring(ITIME,0,1)
DD=substring(ITIME,3,4)
YYYY=substring(ITIME,6,9)
HH=substring(ITIME,12,13)
FH=toint(tofloat(FTIME)/60)

month=toint(MM)
day=toint(DD)
year=toint(YYYY)
hour=toint(HH)
minute=0
second=0
units="hours since 2022-06-01 00:00:00"

time = cd_inv_calendar(year,month,day,hour,minute,second,units, 0)
time@units=units

print("INIT_TIME="+YYYY+MM+DD+" "+HH)
print("FCST_TIME="+FH)

time=time + FH
time!0="time"
time&time=time

utc_date = cd_calendar(time, 0)
year_cd   = tointeger(utc_date(:,0))
month_cd  = tointeger(utc_date(:,1))
day_cd    = tointeger(utc_date(:,2))
hour_cd   = tointeger(utc_date(:,3))
minute_cd = tointeger(utc_date(:,4))

VALID_TIME=sprinti("%0.4i", year_cd) +"-"  \
          +sprinti("%0.2i", month_cd)+"-" \
          +sprinti("%0.2i", day_cd)  +"_" \
          +sprinti("%0.2i", hour_cd)

print("VALID_TIME="+VALID_TIME)

p!0="p"
p&p=p
p@units = "hPa"
p@long_name = "Isobaric surface" 

lon!0="lon"
lon&lon=lon
lon@units = "degrees_east"
lon@grid_type = "Latitude/Longitude"
lon@long_name = "longitude"

lat!0="lat"
lat&lat=lat
lat@units = "degrees_north"
lat@grid_type = "Latitude/Longitude"
lat@long_name = "latitude"

temp!0="time"
temp&time=time
temp!1="p"
temp&p=p ;hPa
temp!2="lat"
temp&lat=lat
temp!3="lon"
temp&lon=lon

copy_VarCoords(temp,rh)
copy_VarCoords(temp,u)
copy_VarCoords(temp,v)
copy_VarCoords(temp,w)
copy_VarCoords(temp,z)


print("")
print("mmmmm OUTPUT")

OUT=ODIR+"/"+"LFM_PRS_FH"+sprinti("%0.2i", FH)+"_VALID_"+VALID_TIME+".nc"

system("rm -vf "+OUT)

a=addfile(OUT,"c")

setfileoption("nc","Format","LargeFile")

dimNames = (/"time","p", "lat","lon"/)
dimSizes = (/1,dim(0),dim(1),dim(2)/)
dimUnlim = (/True,False,False,False/)  

setfileoption(a,"DefineMode",True)
fatts = True
fatts@creation_date = systemfunc("date -R")  ;; dateコマンドの戻り値
fatts@directory=systemfunc("pwd")
fatts@script = scriptname ;; dateコマンドの戻り値

fileattdef(a,fatts)

filedimdef(a,dimNames,dimSizes,dimUnlim)

filevardef(a,"time",typeof(time),getvardims(time))
filevardef(a,"p",typeof(p),getvardims(p))
filevardef(a,"lat",typeof(lat),getvardims(lat))
filevardef(a,"lon",typeof(lon),getvardims(lon))
filevardef(a,"temp",typeof(u),getvardims(temp))
filevardef(a,"rh",typeof(u),getvardims(rh))
filevardef(a,"u",typeof(u),getvardims(u))
filevardef(a,"v",typeof(v),getvardims(v))
filevardef(a,"w",typeof(w),getvardims(w))
filevardef(a,"z",typeof(z),getvardims(z))


a->time = time
a->p   = p
a->lon  = lon
a->lat  = lat
a->temp = temp
a->rh   = rh
a->u    = u
a->v    = v
a->w    = w
a->z    = z


print("")
print("MMMMMMMMMMMMMMMMMMMMMM")
print("PRS OUT: "+OUT)
print("MMMMMMMMMMMMMMMMMMMMMM")
;system("ncdump -h "+OUT)

print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("DONE SCRIPT "+scriptname_in)
print("NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN")
print("")
print("")
print("")

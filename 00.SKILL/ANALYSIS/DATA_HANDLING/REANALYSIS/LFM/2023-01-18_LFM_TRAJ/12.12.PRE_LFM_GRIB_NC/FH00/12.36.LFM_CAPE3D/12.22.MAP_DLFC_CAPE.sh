#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

#MMMMMMMMMMMMMM INCLUDE ###################
. ./12.12.LFM.MAKE.CTL.sh
#MMMMMMMMMMMMMM INCLUDE ###################

FH=$1; FH=${FH:-FH00}
YYYYMMDDHH=$2; YYYYMMDDHH=${YYYYMMDDHH:=2022061902}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

GS=$(basename $0 .sh).GS

VAR1=DLFC; VAR2=CAPE; VAR3=VPT; VARLIST=${VAR1}_${VAR2}
LEV=950; LEV2=975
FIG=LFM_${FH}_${VARLIST}_${LEV}_${YYYY}-${MM}-${DD}_${HH}.pdf

CTL1=LFM_PROC_ROT_v5_CAPE3D.CTL
INDIR1=/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F/OUT_CAPE3D_NC4
INFLE1=LFM_PRS_${FH}_VALID_%y4-%m2-%d2_%h2_CAPE3D.nc
DSET1=$INDIR1/$INFLE1
LFM_MAKE_CTL $CTL1 $DSET1

CTL2=LFM_PROC_ROT_v5_THERMO.CTL
INDIR2=/work01/DATA/LFM_PROC_ROT_v5/${FH}
INFLE2=LFM_PRS_${FH}_VALID_%y4-%m2-%d2_%h2_THERMO.nc
DSET2=$INDIR2/$INFLE2
LFM_MAKE_CTL_2 $CTL2 $DSET2

LONW=122.5 ; LONE=131.5; LATS=25; LATN=32 #MAP AREA

LEVS1="-levs 0 100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500"
KIND1='-kind crimson->red->orange->yellow->greenyellow->lightcyan->white'
FS1=5; UNIT1="[m]"

LEVS2="-levs 0 100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500"
KIND2='-kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS1=5; UNIT2="[J/kg]"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'

xmax = 2; ymax = 1
ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE
'cc'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set rgb 99 78 53 36'
'set mpdset hires';'set map 99 1 3 1'
'set grads off';'set grid off'
'set xlint 2';'set ylint 1'
'set xlopts 1 3 0.1'; 'set ylopts 1 3 0.1'; 
'set gxout shade2'
'color ${LEVS1} ${KIND1}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

'd DLFC'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR1} ${TIME} ${LEV}hPa ${FH}'
x=(xl+xr)/2; y=y+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR3} ${TIME} ${LEV2}hPa ${FH}'

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -edge circle'
x=x2+0.1; y=y1; 'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT1}'

'set lev ${LEV2}'; 'set time ${TIME}'
'set xlab off';'set ylab off'
'set gxout contour'
'set clevs 300 301 302 303 304 305';'set clab off'
'set cthick 5';'set ccolor 0'
'd vpt.2'
'set clevs 300 301 302 303 304 305';'set clab on'
'set cthick 1';'set ccolor 1';'set clopts 1 1 0.08'
'd vpt.2'
'set xlab on';'set ylab on'



nmap = 2
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 0.4 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set mpdset hires'; 'set grads off';'set grid off'
'set xlint 2';'set ylint 1'
'set xlopts 1 2 0.1'; 'set ylopts 1 2 0.1'; 
'set gxout shade2'
'color ${LEVS2} ${KIND2}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'; 'set time ${TIME}'

'd CAPE'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yt+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR2} ${TIME} ${LEV}hPa ${FH}'
x=(xl+xr)/2; y=y+0.2; 'set strsiz 0.10 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${VAR3} ${TIME} ${LEV2}hPa ${FH}'

x1=xl; x2=xr-0.7; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT2}'

'set lev ${LEV2}'; 'set time ${TIME}'
'set xlab off';'set ylab off'
'set gxout contour'
'set clevs 300 301 302 303 304 305';'set clab off'
'set cthick 5';'set ccolor 0'
'd vpt.2'
'set clevs 300 301 302 303 304 305';'set clab on'
'set cthick 1';'set ccolor 1';'set clopts 1 1 0.08'
'd vpt.2'
'set xlab on';'set ylab on'



#x=(xl+xr)/2 ;# TEXT
#y=yt+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${TEXT}'

'set strsiz 0.1 0.12'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR1}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR2}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo

echo "DONE $0."
echo

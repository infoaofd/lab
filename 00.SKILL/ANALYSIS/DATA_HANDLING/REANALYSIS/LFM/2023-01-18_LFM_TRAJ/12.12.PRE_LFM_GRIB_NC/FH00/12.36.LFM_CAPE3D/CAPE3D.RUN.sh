#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

FH_LIST="FH00 FH03" ;#FH_LIST="00"

YYYY=2022; MM=06; MMM=JUN; DDLIST="18 19" ;#18

EXELIST="\
CAPE3D_NC4.sh \
"

for EXE in $EXELIST; do
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE; exit 1;fi

for FH in $FH_LIST; do

for DD in $DDLIST; do

if [ $DD == "18" ]; then
HHLIST="14 15 16 17 18 19 20 21 22 23"
elif [ $DD == "19" ]; then
HHLIST="00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15"
#HHLIST="02"
fi

for HH in $HHLIST; do

echo "NNNNN $EXE $FH $YYYY $MM $DD $HH"

$EXE $FH $YYYY $MM $DD $HH

echo "NNNNN DONE $EXE  $YYYY $MM $DD $HH"
done #HHLIST

done #DDLIST

done #FH

done #EXELIST

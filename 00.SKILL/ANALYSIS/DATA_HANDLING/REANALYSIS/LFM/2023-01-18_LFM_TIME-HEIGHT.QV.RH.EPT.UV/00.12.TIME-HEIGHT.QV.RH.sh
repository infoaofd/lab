#!/bin/bash

#MMMMMMMMMMMMMM INCLUDE ###################
INC=./06.LFM.MAKE.CTL.sh
. $INC
#MMMMMMMMMMMMMM INCLUDE ###################

FH=$1
BOX=$2; LON1W=$3; LON1E=$4; LAT1S=$5; LAT1N=$6
YMDH_MAP=$7; YMDH1=$8; YMDH2=$9

echo MMMMMMM DATE ${YYYY} ${MM} ${DD} ${HH}

FH=${FH:-00}
YMDH1=${YMDH1:-12Z18JUN2022}; YMDH2=${YMDH2:-23Z19JUN2022}
YMDH_MAP=${YMDH_MAP:-00Z19JUN2022}
TIME1S=${TIME1S:-00Z19JUN2022}; TIME1E=${TIME1E:-03Z19JUN2022};
TIME2S=${TIME2S:-03Z19JUN2022}; TIME2E=${TIME2E:-06Z19JUN2022};

#BOX=${BOX:-BOX3};LON1W=${LON1W:-128.5};LON1E=${LON1E:-129};
#                 LAT1S=${LAT1S:-30};LAT1N=${LAT1N:-30.5}
#BOX=${BOX:-BOX4};LON1W=${LON1W:-127.5};LON1E=${LON1E:-128};
#                 LAT1S=${LAT1S:-29.5};LAT1N=${LAT1N:-30}
#BOX=${BOX:-BOX5};LON1W=${LON1W:-128};LON1E=${LON1E:-128.5};
#                 LAT1S=${LAT1S:-30};LAT1N=${LAT1N:-30.5}
BOX=${BOX:-BOX6};LON1W=${LON1W:-128.5};LON1E=${LON1E:-129};
                 LAT1S=${LAT1S:-30.25};LAT1N=${LAT1N:-30.75}


MODEL=LFM
LEV1=850; LEV2=500; LEV3=300
VAR1=RH;CINT1=2
VAR2=RH;LEVCSC="1000 300";CINT2=2
LEVS2="60 95 5"; UNIT2="%"
KIND2="-kind white->antiquewhite->mistyrose->lightpink->mediumvioletred->navy->darkblue->blue->dodgerblue->aqua"
YTITLE_CSC="Pressure [hPa]" ;#"Longitude East"
VAR3=QV;CINT3=2;CINT3B=1

FIGDIR=FH${FH}; mkd $FIGDIR
FIG=${FIGDIR}/${MODEL}_FH${FH}_${VAR1}_${BOX}_${LON1W}-${LON1E}_${LAT1S}_${LAT1N}.pdf

VSCL=0.25; VSCM=20

RGB2='99 100 100 100' ;# GRAY
RGB3='98 0 0 255'     ;# BLUE
RGB4='97 102 204 51'  ;# GREEN

HOST=$(hostname); CWD=$(pwd); TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
echo CREATE CTL FILE

CTL1=LFM.CTL; echo $CTL1

INDIR1=/work01/DATA/LFM_PROC_ROT_v2/FH${FH}/
INFLE1=LFM_PRS_FH${FH}_VALID_%y4-%m2-%d2_%h2_THERMO.nc
DSET1=$INDIR1/${INFLE1}

LFM_MAKE_CTL $CTL1 $DSET1
echo MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

CTL2=42.R3H.PANEL.CTL

GS=$(basename $0 .sh).GS

cat <<EOF>$GS

'open $CTL1' ;#'q ctlinfo'; say result
'open $CTL2' ;#'q ctlinfo'; say result

'cc'; 'set grid off'
'set rgb $RGB3'; 'set rgb $RGB4'

say 'MMMMM SET FIGURE SIZE'
'set vpage 0.0 11.5 0 8.5' ;#0.0 8.5 0 10.5'

xmax=3; ymax=2; xleft=0.5; ytop=10

xwid =  6/xmax; ywid =  5/ymax
xleft2=0.8; xwid2=  5; ywid2=2.5

xmargin=0.5; xmargin2=0.8; ymargin=0.8

say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMM 1 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=1

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'

'set dfile 1'
'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${YMDH_MAP}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $LEV1'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'

'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

say 'COLOR $VAR1 $V1LEV'
'set lev $LEV1'
'color -gxout shaded ${LEVS2} ${KIND2}'
'd ${VAR1}'

'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

say 'VECTOR $VAR3 $LEV1'
'set lev $LEV1'
'set ccolor 0'; 'set cthick 5'; 'set string 1 c 3 0'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 20 20 -SL m/s'

'set lev $LEV1'
'set ccolor 1'; 'set cthick 2'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 'xx' 'yy' -SL m/s'

say 'CONTOUR $VAR3 $LEV1'
'q dims'; say sublin(result,4)
'set lev $LEV1'
'set gxout contour'
'set ccolor 0'; 'set cthick 2'
'set clab off'; 'set cint $CINT3'
'd ${VAR3}*1000'

'set xlab off';'set ylab off'

'set ccolor 99'; 'set cthick 1'; 'set rgb $RGB2'
'set clab on'; 'set cint $CINT3';'set clskip 2';'set clopts 99 2 0.06'
'd ${VAR3}*1000'

'set dfile 2';'set gxout contour'
'r3h=sum(rr,time=$TIME1S,time=$TIME1E)'
'set ccolor 0';'set cthick 2';'set clevs 50';
'd r3h'
'set ccolor 98';'set cthick 1';'set clevs 50'
'd r3h'
'set dfile 2';'set gxout contour'

'r3h=sum(rr,time=$TIME2S,time=$TIME2E)'
'set ccolor 0';'set cthick 2';'set clevs 50'
'd r3h'
'set ccolor 97';'set cthick 1';'set clevs 50'
'd r3h'

'set dfile 1'

'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx=xl; yy=yt+0.14; 'draw string 'xx' 'yy' ${YMDH_MAP} ${LEV1}hPa'

say 'MMMMM $BOX ${LON1W} ${LON1E} ${LAT1S} ${LAT1N}'
'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 0 -l 1 -t 4'

'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 2 -l 1 -t 2'


# Header
'set strsiz 0.1 0.12'; 'set string 1 l 2'
x=xleft;y=yt+0.3;'draw string 'x' 'y' ${NOW}'; x=xleft;y=y+0.2;'draw string 'x' 'y'  ${FIG}'
x=xleft;y=y+0.2;'draw string 'x' 'y' ${CWD}'; x=xleft;y=y+0.2;'draw string 'x' 'y'  ${CMD}'

say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMM 2 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=2

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'set dfile 1'
'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${YMDH_MAP}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $LEV2'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'

'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

say 'COLOR $VAR1 $V1LEV'
'set lev $LEV2'
'color -gxout shaded ${LEVS2} ${KIND2}'
'd ${VAR1}'

'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set lev $LEV2'
'set ccolor 0'; 'set cthick 5'; 'set string 1 c 3 0'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 20 20 -SL m/s'

'set lev $LEV2'
'set ccolor 1'; 'set cthick 2'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 'xx' 'yy' -SL m/s'

say 'CONTOUR $VAR3 $LEV2'
'q dims'; say sublin(result,4)
'set lev $LEV2'
'set gxout contour'
'set ccolor 0'; 'set cthick 3'
'set clab off'; 'set cint $CINT3'
'd ${VAR3}*1000'

'set xlab off';'set ylab off'

'set ccolor 99'; 'set cthick 1';'set rgb $RGB2'
'set clab on'; 'set cint $CINT3';'set clskip 2';'set clopts 99 2 0.06'
'd ${VAR3}*1000'

'set dfile 2';'set gxout contour'
'r3h=sum(rr,time=$TIME1S,time=$TIME1E)'
'set ccolor 0';'set cthick 2';'set clevs 50'
'd r3h'
'set ccolor 98';'set cthick 1';'set clevs 50'
'd r3h'

'set dfile 2';'set gxout contour'
'r3h=sum(rr,time=$TIME2S,time=$TIME2E)'
'set ccolor 0';'set cthick 2';'set clevs 50'
'd r3h'
'set ccolor 97';'set cthick 1';'set clevs 50'
'd r3h'

'set dfile 1'

'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx=xl; yy=yt+0.14; 'draw string 'xx' 'yy' ${YMDH_MAP} ${LEV2}hPa'

say 'MMMMM $BOX ${LON1W} ${LON1E} ${LAT1S} ${LAT1N}'
'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 0 -l 1 -t 4'

'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 2 -l 1 -t 2'



say; say; say 'MMMMMMMMMMMMMMMMMMMMMMMMMMMM 3 MAP MMMMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=1; xmap=3

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'set dfile 1'
'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time ${YMDH_MAP}'
'q dims'; say result; line=sublin(result,5); datetime=subwrd(line,6)
'set lev $LEV2'
'q dims'; say sublin(result,4)


# 78 53 36 ;#DARK BROWN
'set mpdset hires'; 'set rgb 99 160 82 45'; 'set map 99 1 2'
'set xlopts 1 3 0.1'; 'set xlevs 122 125 128 131'
'set ylopts 1 3 0.1'; 'set ylint 2'

'set xlab on'; 'set xlevs 122 125 128 131'; 'set ylab on'

say 'COLOR $VAR1 $LEV3'
'set lev $LEV3'
'color -gxout shaded ${LEVS2} ${KIND2}'
'd ${VAR1}'

'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
xx=xr-0.55; yy=yb-0.3

'set lev $LEV3'
'set ccolor 0'; 'set cthick 5'; 'set string 1 c 3 0'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 20 20 -SL m/s'

'set lev $LEV3'
'set ccolor 1'; 'set cthick 2'
'vec skip(u,25,25);v -SCL $VSCL $VSCM -P 'xx' 'yy' -SL m/s'

say 'CONTOUR $VAR3 $LEV3'
'q dims'; say sublin(result,4)
'set lev $LEV3'
'set gxout contour'
'set ccolor 0'; 'set cthick 2'
'set clab off'; 'set cint $CINT3B'
'd ${VAR3}*1000'

'set xlab off';'set ylab off'

'set ccolor 99'; 'set cthick 1';'set rgb $RGB2'
'set clab on'; 'set cint $CINT3B';'set clskip 1';'set clopts 99 2 0.06'
'd ${VAR3}*1000'

'set dfile 2';'set gxout contour'
'r3h=sum(rr,time=$TIME1S,time=$TIME1E)'
'set ccolor 0';'set cthick 2';'set clevs 50'
'd r3h'
'set ccolor 98';'set cthick 1';'set clevs 50'
'd r3h'

'r3h=sum(rr,time=$TIME2S,time=$TIME2E)'
'set ccolor 0';'set cthick 2';'set clevs 50'
'd r3h'
'set ccolor 97';'set cthick 1';'set clevs 50'
'd r3h'

'set dfile 1'

'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx=xl; yy=yt+0.14; 'draw string 'xx' 'yy' ${YMDH_MAP} ${LEV3}hPa'

say 'MMMMM $BOX ${LON1W} ${LON1E} ${LAT1S} ${LAT1N}'
'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 0 -l 1 -t 4'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 0 -l 1 -t 4'

'trackplot ${LON1W} ${LAT1S} ${LON1E} ${LAT1S} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1S} ${LON1E} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1E} ${LAT1N} ${LON1W} ${LAT1N} -c 2 -l 1 -t 2'
'trackplot ${LON1W} ${LAT1N} ${LON1W} ${LAT1S} -c 2 -l 1 -t 2'



say; say; say 'MMMMMMMMMMMMMMMMMMMMM 3 TIME-HEIGHT MMMMMMMMMMMMMMMMMMMMMMMMMM'

ymap=2; xmap=1

xs = xleft2 + (xwid+xmargin)*(xmap-1); xe = xs + xwid2
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid2
'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'

'set xlab on'; 'set ylab on'

say 'COLOR $VAR2 $LEVCSC'
'set time ${YMDH1} ${YMDH2}'; 'set lev ${LEVCSC}'
'set ylint 100'
'set x 1';'set y 2'
'V2BOX=tloop(aave($VAR2,lon=${LON1W},lon=${LON1E},lat=${LAT1S},lat=${LAT1N}))'

'color -gxout shaded ${LEVS2} ${KIND2}'
'd V2BOX'

'set xlab on'; 'set ylab on'

say 'CONTOUR $VAR3 $LEVCSC'
'V3BOX=tloop(aave($VAR3,lon=${LON1W},lon=${LON1E},lat=${LAT1S},lat=${LAT1N}))'
'q dims'; say sublin(result,4)

'set gxout contour'
'set ccolor 0'; 'set cthick 5'
'set clab off'; 'set cint $CINT3B'
'd V3BOX*1000'

'set xlab off';'set ylab off'

'set ccolor 1'; 'set cthick 1'
'set clab on'; 'set cint $CINT3B';'set clskip 2'
'd V3BOX*1000'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
xx=(xl+xr)/2; yy=yt+0.14; 'draw string 'xx' 'yy' (${LAT1S}-${LAT1N})-(${LON1W}-${LON1E})'

'set strsiz 0.1 0.12'; 'set string 1 c 3 90'
xx=xl-0.5; yy=(yt+yb)/2; 'draw string 'xx' 'yy' $YTITLE_CSC'

say 'mmmmm COLOR BAR'
x1=xr+0.1; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs 1 -ft 3 -line on -edge circle'
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx=(x1+x2)/2; yy=y2+0.14; 'draw string 'xx' 'yy' $UNIT2'


'gxprint $FIG'

'quit'
EOF


grads -bcp $GS
rm -vf $GS
if [ -f $FIG ];then echo; echo OUTPUT $FIG; echo; fi

echo MMMMM BACK-UP SCRIPTS
cp -av $0 $FIGDIR/00.$(basename $0 .sh)_BAK_$(date +"%Y-%m-%d_%H%M").sh
cp -av $INC $FIGDIR/00.$(basename $INC .sh)_BAK_$(date +"%Y-%m-%d_%H%M").sh
echo MMMMM

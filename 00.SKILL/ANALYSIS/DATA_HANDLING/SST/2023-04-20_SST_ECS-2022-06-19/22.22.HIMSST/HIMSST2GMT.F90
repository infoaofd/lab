PROGRAM HIMSST2GMT
! Thu, 20 Apr 2023 10:51:23 +0900
! p5820.bio.mie-u.ac.jp

!IMPLICIT NONE
!
CHARACTER INDIR*500,INFLE*500,IN*1000,ODIR*500,OFLE*500,&
OUT*1000

INTEGER,PARAMETER::NX=800,NY=600
!INTEGER,ALLOCATABLE,DIMENSION(:)::
REAL,DIMENSION(NX,NY)::VAR
REAL::LON(NX),LAT(NY)
REAL::LONW,LONE,LATS,LATN
!REAL,ALLOCATABLE,DIMENSION(:)::

!REAL,PARAMETER::UNDEF=

NAMELIST /PARA/INDIR,INFLE,ODIR,OFLE,LONW,LONE,LATS,LATN,DX,DY
READ(*,NML=PARA)

!ALLOCATE()

PRINT *

IN=TRIM(INDIR)//'/'//TRIM(INFLE)
OUT=TRIM(ODIR)//'/'//TRIM(OFLE)

DO I=1,NX
LON(I)=100.05+0.1*FLOAT(I-1)
END DO !I
DO J=1,NY
LAT(J)=0.05+0.1*FLOAT(J-1)
END DO !I

IW=(LONW-100.05)/DX+1; IE=(LONE-100.05)/DX+2
JS=(0.05+LATS)/DY+1; JN=(0.05+LATN)/DY+2

PRINT *,LON(IW),LON(IE)
PRINT *,LAT(JS),LAT(JN)


IUNIT=11
call read_sst( IN, VAR, IUNIT )
! 要素番号の小さいものが地球の南西端 (経度 0, 緯度 -89.875) に入り, 配列
! 第一要素は東向き, 第二要素は北向きに格子データが格納されて出力される.
! 未定義値は 999.0, 海氷データは 888.0 という値で格納される.

OPEN(21,FILE=OUT)
DO J=JS,JN
DO I=IW,IE
IF(VAR(I,J)<800.0)THEN
WRITE(21,'(f9.3,f8.3,f7.2)')LON(I),LAT(J),VAR(I,J)
ENDIF
END DO !I
END DO !J

CLOSE(21)
!OPEN(21,FILE=OUT,FORM='UNFORMATTED',ACCESS='DIRECT',&
!RECL=4*NX*NY)

END PROGRAM HIMSST2GMT



subroutine read_sst( fname, sst, funit )
! himsst データを読み込むためのルーチン
! 読み込み用データは 800 x 600 で固定.
! これは, himsst のフォーマットからくるものである.
  implicit none
  integer, parameter :: nx=800  ! 経度方向の格子点数
  integer, parameter :: ny=600   ! 緯度方向の格子点数
  character(*), intent(in) :: fname  ! 読み込む mgdsst ルーチン
  real, intent(inout), dimension(nx,ny) :: sst  ! 読み込んだ SST データ [K].
                              ! ただし, 999.0 が未定義, 888.0 が海氷.
  integer, intent(in), optional :: funit   ! ファイルユニット番号.
                       ! デフォルト = 11.
  character(3), dimension(nx,ny) :: sstc
  character(10) :: formatt
  integer :: j, k, unitn

  if(present(funit))then
     unitn=funit
  else
     unitn=11
  end if

  formatt='(800a3)' !'(1440a3)'


  call read_file_text( trim(fname), nx, ny, sstc, 1, trim(formatt),  &
  &                    funit )
  print '(A)','DONE read_file_text.'



!  open(unit=12,file=trim(dat_list(i)),status='old')
!     read(12,*) sstc(1,1)
  do k=1,ny
!     read(12,formatt) (sstc(j,k),j=1,nx)
     do j=1,nx
!        print *,k,j
        if(sstc(j,k)=='888')then
           sst(j,ny-k+1)=263.15
        else if(sstc(j,k)=='999')then
           sst(j,ny-k+1)=999.0
        else
!
!           print *,j,k,sstc(j,k)
           read(sstc(j,k),*) sst(j,ny-k+1)
           sst(j,ny-k+1)=sst(j,ny-k+1)/10.0
!           write(*,*) j,k,sstc(j,k),sst(j,k)
        end if
!        tmp(j,ny-k+1)=sst(j,k)
!        sst(j,k)=tmp(j,k)
     end do
  end do

end subroutine



subroutine read_file_text( fname, nx, ny, val, skip, forma, funit )
! テキスト形式のデータを読み込むルーチン
! forma と nx, ny の関係は 4x3 の i3 (空白込み) の行列データの場合,
! forma = '(3a3)' という form で読み込むなら, nx = 3, ny = 4
! forma = '(a)' という form で読み込むなら, nx = 1, ny = 4 となる.
  implicit none
  character(*), intent(in) :: fname  ! 読み込むテキストファイル
  integer, intent(in) :: nx  ! 読み込むファイルの列数
  integer, intent(in) :: ny  ! 読み込むファイルの行数
  character(*), intent(inout) :: val(nx,ny)  ! 文字列データにも対応するため, 文字列で結果を返す.
  integer, intent(in), optional :: skip  ! 行頭何行を読み飛ばすか. default は 0.
  character(*), intent(in), optional :: forma  ! read のフォーマット '(f4.1)' など. デフォルトでは read のデフォルトフォーマット使用.
  integer, intent(in), optional :: funit   ! ファイルユニット番号.
                       ! デフォルト = 11.
  integer :: i, j, unitn
  character(100) :: dummy

  if(present(funit))then
     unitn=funit
  else
     unitn=11
  end if

  open(unit=unitn, file=trim(fname), status='old')
  if(present(skip))then
     do i=1,skip
        read(unitn,*) dummy
        print *,trim(dummy)
     end do
  end if

  if(present(forma))then
     do j=1,ny
        read(unitn,forma) (val(i,j),i=1,nx)
     end do
  else
     do j=1,ny
        read(unitn,*) (val(i,j),i=1,nx)
     end do
  end if
  close(unit=unitn)
end subroutine

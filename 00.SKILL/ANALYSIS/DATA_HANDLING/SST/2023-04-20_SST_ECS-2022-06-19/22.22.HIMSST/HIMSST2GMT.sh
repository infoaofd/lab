#!/bin/bash
#
# Thu, 20 Apr 2023 10:51:23 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/22.12.HIMSST
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -convert big_endian -assume byterecl"
LOPT= #"-I/usr/local/stpk-0.9.20.0/include -L/usr/local/stpk-0.9.20.0/lib -lstpk"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

YMD=20220619
REGION=ECS
LONW=105
LONE=132
LATS=20
LATN=36
INDIR=/work01/DATA/SST/HIMSST
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1;fi
INFLE=him_sst_pac_D${YMD}.txt
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1;fi
ODIR=OUT_$(basename $0 .sh);mkd $ODIR
OFLE=HIMSST_${REGION}_D${YMD}_GMT.TXT
cat<<EOF>$nml
&para
INDIR="$INDIR"
INFLE="$INFLE"
ODIR="$ODIR"
OFLE="$OFLE"
LONW=${LONW}
LONE=${LONE}
LATS=${LATS}
LATN=${LATN}
DX=0.1
DY=0.1
&end
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/stpk-0.9.20.0/lib

echo MMMMM Compiling ${src} ...
echo ${f90} ${OPT} ${DOPT} -c ${src}
${f90} ${OPT} ${DOPT} -c ${src}
if [ $? -ne 0 ]; then
echo; echo "EEEEE COMPILE ERROR!!!"; echo; echo TERMINATED.
exit 1
fi

echo MMMMM LINKING ...
OBJ=$(basename $src .F90).o
echo ${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEE LINK ERROR!!!"; echo; echo TERMINATED.
exit 1
fi


echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo; echo "EEEEE RUNTIME ERROR!!!"; echo; echo TERMINATED.
exit 1
fi

echo; echo "Done ${exe}"; echo
D2=$(date -R)
echo "START: $D1   END:   $D2"
echo ODIR: $ODIR
echo OFLE: $OFLE

#!/bin/bash
# Description:
#
# Author: am
#
# Host: p5820.bio.mie-u.ac.jp
# Directory: /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/32.12.iQUAM/12.12.GET.INFO.iQUAM
#
# Revision history:
#  This file is created by /work03/am/mybin/nbscr.sh at 16:35 on 04-20-2023.
exe=$(basename $0 .sh).ncl
if [ ! -f $exe  ];then echo EEEEE NO SUCH FILE,$exe;exit 1;fi

INDIR=/work01/DATA/iQUAM_SST
INFLE=202206-STAR-L2i_GHRSST-SST-iQuam-V2.10-v01.0-fv03.0.nc
IN=$INDIR/$INFLE
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR,$INDIR;exit 1;fi
if [ ! -f $IN  ];then echo EEEEE NO SUCH FILE,$IN;exit 1;fi


runncl.sh $exe $INDIR $INFLE

echo "Done $0"
echo





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

#!/bin/bash
#
# BASH SCRIPT FOR GMT4 
#
# Directory: /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/12.12.CALIB
# Wed, 19 Apr 2023 21:09:49 +0900.
#
. ./gmtpar.sh

range=127.9/129.6/30.0/31.3
size=M4
xanot=a30mf15m
yanot=a30mf15m
anot=${xanot}/${yanot}WSne

IN1=CUT.iQUAM.ECS.2022-06-19_EDIT.TXT
if [ ! -f $IN1 ]; then echo Error in $0 : No such file, $IN1; exit 1; fi
IN2=CUT.iQUAM.ECS.2022-06-20_EDIT.TXT
if [ ! -f $IN2 ]; then echo Error in $0 : No such file, $IN2; exit 1; fi

#figdir="FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
figdir=.
OUT=${figdir}/$(basename $0 .sh)_$(basename $IN1 .TXT)_$(basename $IN2 .TXT).ps

#awk '{if($1!="#")print $1,$2,$3)}' $IN1

CPT=$(basename $0 .sh)_CPT2.TXT
makecpt -Cjet -T19/30/0.5 -Z >$CPT

awk '{if($1!="#")print $1,$2,$3}' $IN1|\
psxy -R$range -J$size -Sc0.07 -C$CPT -K -X1.5 -Y6.0 -P >$OUT

awk '{if($1!="#")print $1,$2,$3}' $IN2|\
psxy -R$range -J$size -Sc0.07 -C$CPT -O -K >>$OUT

pscoast -R -JM -B$anot -Df -W2 -G200 -O -K >> $OUT

psscale -D4.2/1.8/3.4/0.1 -C$CPT -E -B1:SST:/:@+o@+C: -O -K >>$OUT

psxy <<EOF -R -JM -W1 -A -O -K >>$OUT
128.00 30.15
129.34 30.15
129.34 31.15
128.00 31.15
128.00 30.15
EOF



range2=115/132/22/36
anot2=a4f2/a4f2WSne
awk '{if($1!="#")print $1,$2,$3}' $IN1|\
psxy -R$range2 -J$size -Sc0.05 -C$CPT -Y-4.5 -O -K >>$OUT

awk '{if($1!="#")print $1,$2,$3}' $IN2|\
psxy -R$range2 -J$size -Sc0.05 -C$CPT -O -K >>$OUT

pscoast -R -JM -B$anot2 -Di -W2 -G200 -O -K >> $OUT

psscale -D4.8/1.8/3.4/0.1 -C$CPT -E -B1:SST:/:@+o@+C: -O -K >>$OUT

psxy <<EOF -R -JM -W1 -A -O -K >>$OUT
128.00 30.15
129.34 30.15
129.34 31.15
128.00 31.15
128.00 30.15
EOF

xoffset=-1; yoffset=8.5

curdir1=$(pwd); now=$(date -R); host=$(hostname)

time=$(ls -l ${IN} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${OUT} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${IN} (${time})
0 0.75  9 0 1 LM OUTPUT: ${OUT} (${timeo})
EOF

PDF=$(basename $OUT .ps).PDF
ps2pdfwr $OUT $PDF
if [ $? -eq 0 ];then rm -vf $OUT; fi

echo; echo "INPUT : "
ls -lh --time-style=long-iso $IN
echo "OUTPUT : "
ls -lh --time-style=long-iso $PDF; echo

echo "Done $0"

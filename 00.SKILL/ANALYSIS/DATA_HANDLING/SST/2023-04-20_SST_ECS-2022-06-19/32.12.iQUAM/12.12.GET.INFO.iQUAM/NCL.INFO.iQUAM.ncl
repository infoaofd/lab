; 
; NCL.INFO.iQUAM.ncl
; 
; Thu, 20 Apr 2023 16:33:50 +0900
; p5820.bio.mie-u.ac.jp
; /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/32.12.iQUAM/12.12.GET.INFO.iQUAM
; am
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

;LOG=script+".LOG"

;NOW=systemfunc("date '+%y%m%d_%H%M' ")
;print("NOW="+NOW)
; LOG=script+NOW+".LOG"
HOST=systemfunc("hostname")
CWD=systemfunc("pwd")

INDIR = getenv("NCL_ARG_2")
INFLE = getenv("NCL_ARG_3")
IN=INDIR+"/"+INFLE

a=addfile(IN,"r")

print(a)



;FINFO=systemfunc("ls -lh --time-style=long-iso "+script_name)
;LOG_HEADER = (/"# "+ NOW, "# "+ HOST, "# "+ CWD, "# "+ FINFO /)
;hlist=[/LOG_HEADER/]
;write_table(LOG, "w", hlist, "%s")



print("")
print("Done " + script_name)
print("")
;print("LOG: "+LOG)
;print("")

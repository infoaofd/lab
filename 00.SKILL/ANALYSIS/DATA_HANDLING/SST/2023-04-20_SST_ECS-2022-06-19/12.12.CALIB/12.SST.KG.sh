#!/bin/bash
# Description:
#
# Author: am
#
# Host: p5820.bio.mie-u.ac.jp
# Directory: /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST
#
# Revision history:
#  This file is created by /work03/am/mybin/nbscr.sh at 18:21 on 04-19-2023.

VAR=SST

INDIR=/work01/DATA/IN_SITU/2022.06.ECS_SHIP/2023-04-19_EDIT_2022-06_VESSEL_MOUNT_TSR_RAW_DATA/FOR_SST/K
INFLE=KAGOSHIMA_2022-06-SHIP.csv
IN=${INDIR}/${INFLE}
ODIR=OUT_$(basename $0 .sh); mkd $ODIR
OFLE=$(basename $INFLE .csv)_${VAR}.TXT
OUT=${ODIR}/${OFLE}

# CALIBRATION
INCALIB=SST_INTAKE_CALIBRATION_2022-06_ECS.TXT
#KAGOSHIMA-MARU y = 0.9357x + 1.7637 R2 = 0.9657
a=0.9357
b=1.7637

if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
if [ ! -f $IN    ];then echo NO SUCH FILE,$IN;exit 1;fi

echo "# $(pwd)"> $OUT
echo "# $(date -R)">>$OUT
echo "# $(basename $0)">>$OUT
echo "# INDIR: $INDIR">>$OUT
echo "# INFLE: $INFLE">>$OUT
echo "# ODIR: $ODIR">>$OUT
echo "# OFLE: $OFLE">>$OUT
echo "# INCALIB: $INCALIB">>$OUT
echo "# a = $a">>$OUT
echo "# b = $b">>$OUT
echo "# DATE TIME (UTC) LAT LON SST(CALIB) SST(INTAKE)">>$OUT
awk -v a=$a, -v b=$b, -F, '{if($1!="#"&&NR>4)\
printf "%s %s %s %s %s %10.4f %7.2f\n",
$3,substr($4,1,2),substr($4,4,6),\
substr($5,1,3),substr($5,5,6),a*$18+b,$18}' $IN >>$OUT


echo "MMMMM INDIR: $INDIR"
echo "MMMMM INFLE: $INFLE"
echo "MMMMM ODIR: $ODIR"
echo "MMMMM OFLE: $OFLE"




#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

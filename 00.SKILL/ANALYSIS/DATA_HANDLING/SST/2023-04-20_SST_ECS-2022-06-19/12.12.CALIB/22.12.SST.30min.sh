#!/bin/bash
# Description:
#
# Author: am
#
# Host: p5820.bio.mie-u.ac.jp
# Directory: /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/12.12.CALIB
#
# Revision history:
#  This file is created by /work03/am/mybin/nbscr.sh at 20:53 on 04-19-2023.

INDIR1=OUT_12.SST.NG; INFLE1=NAGSAKI_2022-06-SHIP_SST.TXT
IN1=$INDIR1/$INFLE1
if [ ! -d $INDIR1 ];then echo NO SUCH DIR,$INDIR1;exit 1;fi
if [ ! -f $IN1 ];then echo NO SUCH FILE,$INFLE1;exit 1;fi
INDIR2=OUT_12.SST.KG; INFLE2=KAGOSHIMA_2022-06-SHIP_SST.TXT
IN2=$INDIR2/$INFLE2
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi
if [ ! -f $IN2 ];then echo NO SUCH FILE,$INFLE2;exit 1;fi
INDIR3=OUT_12.SST.SS; INFLE3=SEISUI_2022-06-SHIP_SST.TXT
IN3=$INDIR3/$INFLE3
if [ ! -d $INDIR3 ];then echo NO SUCH DIR,$INDIR3;exit 1;fi
if [ ! -f $IN3 ];then echo NO SUCH FILE,$INFLE3;exit 1;fi

ODIR=OUT_$(basename $0 .sh); mkd $ODIR
OFLE=$(basename $0 .sh).TXT
OUT=${ODIR}/${OFLE}

echo "# $(pwd)" > $OUT
echo "# $(basename $0)" >> $OUT
echo "# $INDIR1" >> $OUT
echo "# $INFLE1" >> $OUT
echo "# $INDIR2" >> $OUT
echo "# $INFLE2" >> $OUT
echo "# $INDIR3" >> $OUT
echo "# $INFLE3" >> $OUT
echo "# LON LAT SST(CALIB) DATE_TIME (UTC)">>$OUT

SHIP=NG
awk -v S=$SHIP '{if( ($1 !="#")&&((NR-12)%30==0)) \
printf "%8.4f %7.4f %8.3f  %s  %s\n", $4+$5/60,$2+$3/60,$6,$1,S}' $IN1 >> $OUT

SHIP=KG
awk -v S=$SHIP '{if( ($1 !="#")&&((NR-12)%30==0)) \
printf "%8.4f %7.4f %8.3f  %s  %s\n", $4+$5/60,$2+$3/60,$6,$1,S}' $IN2 >> $OUT

SHIP=SS
awk -v S=$SHIP '{if( ($1 !="#")&&((NR-12)%30==0)) \
printf "%8.4f %7.4f %8.3f  %s  %s\n", $4+$5/60,$2+$3/60,$6,$1,S}' $IN3 >> $OUT


echo ODIR: $ODIR
echo OFLE: $OFLE





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

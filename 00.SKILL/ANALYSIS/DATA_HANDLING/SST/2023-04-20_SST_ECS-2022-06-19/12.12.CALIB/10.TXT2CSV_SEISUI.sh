#!/bin/bash

INDIR=/work01/DATA/IN_SITU/2022.06.ECS_SHIP/2023-04-19_EDIT_2022-06_VESSEL_MOUNT_TSR_RAW_DATA/FOR_SST/S
INFLE=SEISUI_2022-06-SHIP.txt
IN=$INDIR/$INFLE

ODIR=OUT_$(basename $0 .sh);mkd $ODIR
OFLE=$(basename $INFLE .txt).csv
OUT=$ODIR/$OFLE

echo "# $(pwd)" >$OUT
echo "# $(basename $0)" >>$OUT
echo "# INDIR: $INDIR"  >>$OUT
echo "# INFLE: $INFLE" >>$OUT

awk '{if(NR<=4)print $0}' $IN >> $OUT
awk '{if(NR>4) printf "%s %s,%s,%s,%s\n", $1, $2,$5,$6,$12}' $IN >> $OUT
#awk 'BEGIN{OFS=","}{if(NR>4) print $1 $2,$5,$6,$12}' $IN >> $OUT

echo "MMMMM INDIR: $INDIR"
echo "MMMMM INFLE: $INFLE"
echo "MMMMM ODIR: $ODIR"
echo "MMMMM OFLE: $OFLE"


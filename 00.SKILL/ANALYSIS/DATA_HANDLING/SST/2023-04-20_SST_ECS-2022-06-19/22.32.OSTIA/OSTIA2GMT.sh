#!/bin/bash

YMD=20220619
REGION=ECS
LONW=105; LONE=132
LATS=20 ; LATN=36

src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

<<COMMENT
f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"
COMMENT

f90=gfortran
DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
NCDIR=/usr/local/netcdf-c-4.8.0
LOPT=" -L$NCDIR/lib -lnetcdff -I${NCDIR}/include"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${NCDIR}

# OpenMP
#OPT2=" -fopenmp "

INDIR=/work01/DATA/SST/OSTIA
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1;fi
INFLE=${YMD}120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1;fi
ODIR=OUT_$(basename $0 .sh);mkd $ODIR
OFLE=OSTIA_${REGION}_D${YMD}_GMT.TXT
cat<<EOF>$nml
&para
INDIR="$INDIR"
INFLE="$INFLE"
ODIR="$ODIR"
OFLE="$OFLE"
LONW=${LONW}
LONE=${LONE}
LATS=${LATS}
LATN=${LATN}
DX=0.05
DY=0.05
&end
EOF


echo; echo Created ${nml}.
ls -lh --time-style=long-iso ${nml}; echo


echo ${src}.; ls -lh --time-style=long-iso ${src}; echo

echo Compiling ${src} ...; echo

${f90} ${OPT} ${src} -c ${LOPT}
if [ $? -ne 0 ];then echo EEEEE COMPILE ERROR ${src};exit 1;fi

OBJ=$(basename $src .F90).o
${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
if [ $? -ne 0 ];then echo EEEEE LINK ERORR ${LOPT};exit 1;fi

echo "Done Compile."; echo
ls -lh ${exe}
echo

echo;echo ${exe} is running ...

D1=$(date -R)

${exe} < ${nml}
if [ $? -ne 0 ]; then
echo;echo "EEEEE ERROR in $exe: RUNTIME ERROR!"
echo "EEEEE TERMINATED."; echo

D2=$(date -R)
echo "START: $D1 END:   $D2"
exit 1
fi
echo; echo "Done ${exe}";echo
D2=$(date -R)
echo "START: $D1 END:   $D2"

echo; echo ODIR: $ODIR
echo OFLE: $OFLE; echo

netcdf \20220619120000-UKMO-L4_GHRSST-SSTfnd-OSTIA-GLOB-v02.0-fv02.0 {
dimensions:
	time = 1 ;
	lat = 3600 ;
	lon = 7200 ;
variables:
	int time(time) ;
		time:long_name = "reference time of sst field" ;
		time:standard_name = "time" ;
		time:axis = "T" ;
		time:units = "seconds since 1981-01-01 00:00:00" ;
		time:comment = "" ;
	float lat(lat) ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:valid_min = -90.f ;
		lat:valid_max = 90.f ;
		lat:axis = "Y" ;
		lat:comment = " Latitude geographical coordinates,WGS84 projection" ;
	float lon(lon) ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:valid_min = -180.f ;
		lon:valid_max = 180.f ;
		lon:axis = "X" ;
		lon:comment = " Longitude geographical coordinates,WGS84 projection" ;
	short analysed_sst(time, lat, lon) ;
		analysed_sst:long_name = "analysed sea surface temperature" ;
		analysed_sst:standard_name = "sea_surface_foundation_temperature" ;
		analysed_sst:units = "kelvin" ;
		analysed_sst:coordinates = "lon lat" ;
		analysed_sst:_FillValue = -32768s ;
		analysed_sst:add_offset = 273.15f ;
		analysed_sst:scale_factor = 0.01f ;
		analysed_sst:valid_min = -300s ;
		analysed_sst:valid_max = 4500s ;
		analysed_sst:source = "AVHRR18_G-NAVO-L2P-V1.0, AVHRR19_G-NAVO-L2P-V1.0, AVHRR_SST_METOP_B-OSISAF-L2P-V1.0, VIIRS_NPP-OSPO-L2P-V2.3, AMSR2-REMSS-L2P-V07.2, GOES13-OSISAF-L3C-V1.0, SEVIRI_SST-OSISAF-L3C-V1.0, OSISAF_ICE, NCEP_ICE" ;
		analysed_sst:reference = "Good S, Fiedler E, Mao C, Martin MJ, Maycock A, Reid R, Roberts-Jones J, Searle T, Waters J, While J, Worsfold M. The Current Configuration of the OSTIA System for Operational Production of Foundation Sea Surface Temperature and Ice Concentration Analyses. Remote Sensing. 2020; 12(4):720 https://doi.org/10.3390/rs12040720" ;
		analysed_sst:comment = " OSTIA foundation SST" ;
	short analysis_error(time, lat, lon) ;
		analysis_error:long_name = "estimated error standard deviation of analysed_sst" ;
		analysis_error:standard_name = "sea_surface_foundation_temperature standard_error" ;
		analysis_error:units = "kelvin" ;
		analysis_error:coordinates = "lon lat" ;
		analysis_error:_FillValue = -32768s ;
		analysis_error:add_offset = 0.f ;
		analysis_error:scale_factor = 0.01f ;
		analysis_error:valid_min = 0s ;
		analysis_error:valid_max = 32767s ;
		analysis_error:comment = " OSTIA foundation SST analysis standard deviation error" ;
	byte sea_ice_fraction(time, lat, lon) ;
		sea_ice_fraction:long_name = "sea ice area fraction" ;
		sea_ice_fraction:standard_name = "sea_ice_area_fraction" ;
		sea_ice_fraction:units = "1" ;
		sea_ice_fraction:coordinates = "lon lat" ;
		sea_ice_fraction:_FillValue = -128b ;
		sea_ice_fraction:add_offset = 0.f ;
		sea_ice_fraction:scale_factor = 0.01f ;
		sea_ice_fraction:valid_min = 0b ;
		sea_ice_fraction:valid_max = 100b ;
		sea_ice_fraction:source = "EUMETSAT OSI-SAF" ;
		sea_ice_fraction:comment = " Sea ice area fraction" ;
	byte mask(time, lat, lon) ;
		mask:long_name = "land sea ice lake bit mask" ;
		mask:coordinates = "lon lat" ;
		mask:_FillValue = -128b ;
		mask:valid_min = 1b ;
		mask:valid_max = 31b ;
		mask:flag_masks = 1b, 2b, 4b, 8b, 16b ;
		mask:flag_meanings = "water land optional_lake_surface sea_ice optional_river_surface" ;
		mask:source = "NAVOCEANO_landmask_v1.0 EUMETSAT_OSI-SAF_icemask ARCLake_lakemask" ;
		mask:comment = " Land/ open ocean/ sea ice /lake mask" ;

// global attributes:
		:Conventions = "CF-1.4, ACDD-1.3" ;
		:title = "Global SST & Sea Ice Analysis, L4 OSTIA, 0.05 deg daily (METOFFICE-GLO-SST-L4-NRT-OBS-SST-V2)" ;
		:summary = "A merged, multi-sensor L4 Foundation SST product" ;
		:references = "Good S, Fiedler E, Mao C, Martin MJ, Maycock A, Reid R, Roberts-Jones J, Searle T, Waters J, While J, Worsfold M. The Current Configuration of the OSTIA System for Operational Production of Foundation Sea Surface Temperature and Ice Concentration Analyses. Remote Sensing. 2020; 12(4):720 https://doi.org/10.3390/rs12040720" ;
		:institution = "UKMO" ;
		:history = "Created from sst.nc; obs_anal.nc; seaice.nc" ;
		:comment = "WARNING Some applications are unable to properly handle signed byte values. If values are encountered > 127, please subtract 256 from this reported value" ;
		:license = "These data are available free of charge under the CMEMS data policy" ;
		:id = "OSTIA-UKMO-L4-GLOB-v2.0" ;
		:naming_authority = "org.ghrsst" ;
		:product_version = "3.5" ;
		:uuid = "536d4865-f5a8-45b2-806f-1f1db491069a" ;
		:gds_version_id = "2.4" ;
		:netcdf_version_id = "4.1" ;
		:date_created = "20220620T063541Z" ;
		:start_time = "20220619T000000Z" ;
		:time_coverage_start = "20220619T000000Z" ;
		:stop_time = "20220620T000000Z" ;
		:time_coverage_end = "20220620T000000Z" ;
		:file_quality_level = 3 ;
		:source = "AVHRR18_G-NAVO-L2P-V1.0, AVHRR19_G-NAVO-L2P-V1.0, AVHRR_SST_METOP_B-OSISAF-L2P-V1.0, VIIRS_NPP-OSPO-L2P-V2.3, AMSR2-REMSS-L2P-V07.2, GOES13-OSISAF-L3C-V1.0, SEVIRI_SST-OSISAF-L3C-V1.0, OSISAF_ICE, NCEP_ICE" ;
		:platform = "NOAA-18, NOAA-19, MetOpB, NPP, GCOM-W, GOES13, MSG4, DMSP-F17, DMSP-F15" ;
		:sensor = "AVHRR, VIIRS, AMSR2, GOES_IMAGER, SEVIRI, SSMIS, SSM/I" ;
		:Metadata_Conventions = "Unidata Observation Dataset v1.0" ;
		:metadata_link = "http://podaac.jpl.nasa.gov/ws/metadata/dataset?format=gcmd&shortName=UKMO-L4HRfnd-GLOB-OSTIA" ;
		:keywords = "Oceans > Ocean Temperature > Sea Surface Temperature" ;
		:keywords_vocabulary = "NASA Global Change Master Directory (GCMD) Science Keywords" ;
		:standard_name_vocabulary = "NetCDF Climate and Forecast (CF) Metadata Convention" ;
		:westernmost_longitude = -180.f ;
		:easternmost_longitude = 180.f ;
		:southernmost_latitude = -90.f ;
		:northernmost_latitude = 90.f ;
		:spatial_resolution = "0.05 degree" ;
		:geospatial_lat_units = "degrees_north" ;
		:geospatial_lat_resolution = 0.05f ;
		:geospatial_lon_units = "degrees_east" ;
		:geospatial_lon_resolution = 0.05f ;
		:acknowledgment = "Please acknowledge the use of these data with the following statement: These data were provided by GHRSST, Met Office and CMEMS" ;
		:creator_name = "Met Office as part of CMEMS" ;
		:creator_email = "servicedesk.cmems@mercator-ocean.eu" ;
		:creator_url = "http://marine.copernicus.eu" ;
		:project = "Group for High Resolution Sea Surface Temperature" ;
		:publisher_name = "GHRSST Project Office" ;
		:publisher_url = "http://www.ghrsst.org" ;
		:publisher_email = "ghrsst-po@nceo.ac.uk" ;
		:processing_level = "L4" ;
		:cdm_data_type = "grid" ;
}

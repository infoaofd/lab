#!/bin/bash
#
# BASH SCRIPT FOR GMT4 
#
# Directory: /work03/am/2022.06.ECS.OBS/0.ECS2022.06_SUMMARY_2023-02-21/06.SHIP/12.12.SST/12.12.CALIB
# Wed, 19 Apr 2023 21:09:49 +0900.
#
. ./gmtpar.sh

DSET=OSTIA
YMD=20220619
REGION=ECS
INDIR=OUT_OSTIA2GMT #OUT_$(basename $0 .sh);mkd $ODIR
INFLE=OSTIA_${REGION}_D${YMD}_GMT.TXT
IN=$INDIR/$INFLE
if [ ! -f $IN ]; then echo Error in $0 : No such file, $IN; exit 1; fi

range=127.9/129.6/30.0/31.3
size=M4
xanot=a30mf15m; yanot=a30mf15m
TITLE="${DSET}${sp}${YMD}"
anot=${xanot}/${yanot}:.${TITLE}:WSne


#figdir="FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
figdir=.
OUT=${figdir}/$(basename $0 .sh)_$(basename $IN .TXT).ps

#awk '{if($1!="#")print $1,$2,$3)}' $IN1

CPT=$(basename $0 .sh)_CPT.TXT
#makecpt -Cjet -T23/28/0.5 -Z >$CPT

GRD=$(basename $0 .sh)_GRD.nc
awk '{if($1!="#")print $1,$2,$3}' $IN|\
surface -R$range -I0.02/0.02 -T0.8 -G$GRD

<<COMMENT
MXY=$(basename $0 .sh)_MXY.TXT
cat <<EOF>$MXY
128.0 30.1
129.4 30.1
129.4 31.2
128.0 31.2
EOF
COMMENT

<<COMMENT
MSK=$(basename $0 .sh)_MSK.nc
grdmask $MXY -R$range -I0.02/0.02 -NNaN/NaN/1 -G$MSK -V
COMMENT

<<COMMENT
BLK=$(basename $0 .sh)_BLK.TXT
awk '{if($1!="#")print $1,$2,$3}' $IN|\
blockmedian -R$range -I0.02/0.02 -V > $BLK
grdmask $BLK -R$range -I0.02/0.02 -NNaN/NaN/1 -S20k -G$MSK -V
COMMENT

<<COMMENT
MSKGRD=$(basename $0 .sh)_MSKGRD.nc
grdmath $GRD $MSK OR = $MSKGRD
COMMENT

gmtset HEADER_FONT_SIZE	= 18p
gmtset HEADER_OFFSET	= -0.2

grdimage $GRD -R$range -J$size  -C$CPT  -K -X1.5 -Y6.0 -P >$OUT

grdcontour $GRD -R$range -J$size -W2 -A1f10 -C1 -O -K >>$OUT

pscoast -R -JM -B$anot -Df -W2 -G200 -O -K >> $OUT

psscale -D4.2/1.8/3.4/0.1 -C$CPT -E -B1:SST:/:@+o@+C: -O -K >>$OUT

range2=115/131/22/35
anot2=a10f5/a5f5WSne
GRD2=$(basename $0 .sh)_GRD2.nc
awk '{if($1!="#")print $1,$2,$3}' $IN|\
surface -R$range2 -I0.02/0.02 -T0.8 -G$GRD2
grdimage $GRD2 -R$range2 -JM4  -C$CPT -O -K -Y-4.5  >>$OUT

grdcontour $GRD2 -R$range2 -J$size -W2 -A1f10 -C1 -O -K >>$OUT

pscoast -R -JM -B$anot2 -Dh -W2 -G200 -O -K >> $OUT

psscale -D4.2/1.8/3.4/0.1 -C$CPT -E -B1:SST:/:@+o@+C: -O -K >>$OUT


xoffset=-1; yoffset=8.5

curdir1=$(pwd); now=$(date -R); host=$(hostname)

time=$(ls -l ${IN} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${OUT} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${IN} (${time})
0 0.75  9 0 1 LM OUTPUT: ${OUT} (${timeo})
EOF

PDF=$(basename $OUT .ps).PDF
ps2pdfwr $OUT $PDF
if [ $? -eq 0 ];then rm -vf $OUT; fi

rm -fv $GRD $GRD2 $BLK $MSK $MSKGRD

echo; echo "INPUT : "
ls -lh --time-style=long-iso $IN
echo "OUTPUT : "
ls -lh --time-style=long-iso $PDF; echo

echo "Done $0"

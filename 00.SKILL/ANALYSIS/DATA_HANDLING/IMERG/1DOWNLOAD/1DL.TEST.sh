
INLIST="\
3B-HHR-L.MS.MRG.3IMERG.20220619-S000000-E002959.0000.V06C.HDF5 \
"

for INFLE in $INLIST; do
wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies \
 https://gpm1.gesdisc.eosdis.nasa.gov/data/GPM_L3/GPM_3IMERGHHL.06/\
2022/170/\
${INFLE}
done


CTL='./TOPO.MSM_5K.CTL'

LONW=125 ;LONE=142 ; LATS=30 ;LATN=45

LEVS='0 1 0.1'
KIND=" -kind white->white->lightgreen->limegreen->darkgreen->khaki->darkgoldenrod->saddlebrown->maroon"

'open 'CTL
'cc'
'set vpage 0.0 8.5 0.0 11'

xmax = 1; ymax = 1
ytop=9
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'set rgb 99 0 255 0'
'set mpdset hires' ;#worldmap'
#'set mpdset worldmap'
'set map 99 1 2 1'
'set xlopts 1 3 0.15'; 'set ylopts 1 3 0.15'

'set grid off';'set grads off'

'set lon 'LONW ' 'LONE; 'set xlint 10'
'set lat 'LATS ' 'LATN; 'set ylint 5'

'set t 1'

#'set gxout contour'
#'set clopts 1 2 0.2'
#'set cthick 1'
#'set ccolor 1'
#'set cint 10'
#'set cmin 50'
#'set cmax 150'
#'d topo'

'color 'LEVS ' 'KIND ' -gxout shaded'
'd topo/1000'


# LEGEND COLOR BAR
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
x1=xl; x2=xr-0.5; y1=yb-0.8; y2=y1+0.1

'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' km'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' km'

'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' MSM Topography'

OFLE='1.MSM_TOPO.PDF'
'gxprint 'OFLE
say
say 'OUTPUT: 'OFLE

quit

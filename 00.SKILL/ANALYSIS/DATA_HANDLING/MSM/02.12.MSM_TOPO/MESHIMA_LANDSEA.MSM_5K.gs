CTL='./LANDSEA.MSM_5K.CTL'

LONW=128.3  ;LONE=128.5
LATS=31.9   ;LATN=32.1

#LONW=120  ;LONE=150
#LATS=22.4 ;LATN=47.6

LEVS='0 1 0.1'
KIND='-kind midnightblue->deepskyblue->green->wheat->orange->red->magenta'


'open 'CTL

say 
say result
say

'cc'
'set rgb 99 0 255 0'
'set mpdset worldmap'; 'set map 99 1 2 1'
'set xlopts 1 2 0.08'; 'set ylopts 1 2 0.08'

'set grid off';'set grads off'

#'set xlab on'; 'set ylab on'

'set lon 'LONW ' 'LONE; 'set xlint 0.1'
'set lat 'LATS ' 'LATN; 'set ylint 0.1'

#'set xlopts 0 5 0.2'
#'set ylopts 0 5 0.2'


#'set lon 'LONW ' 'LONE; 'set xlint 5'
#'set lat 'LATS ' 'LATN; 'set ylint 5'

'set t 1'

#水域0, 陸域1 とした海陸の割合
#https://akyura.sakura.ne.jp/study/GrADS/Command/graph_type.html
#'set gxout grfill'
'color 'LEVS ' 'KIND ' -gxout grfill'
'd landsea'
'cbarn'

'draw title MESHIMA LANDSEA MSM'

OFLE='MESHIMA_LANDSEA.MSM_5K.PDF'
#OFLE='LANDSEA.MSM_5K.PDF'
'gxprint 'OFLE
say
say 'OUTPUT: 'OFLE

quit

# 1時間間隔
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/01.BASH/LINUX_DATE/0.LINUX_DATE.md

#!/bin/bash

# 日付の処理
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

EXE=$(basename $0 _RUN.sh).sh
if [ ! -f $EXE ];then echo ERROR: NO SUCH FILE, $EXE; exit 1; fi

yyyymmdd1=$1; yyyymmdd2=$2
yyyymmdd1=${yyyymmdd1:-20240723}; yyyymmdd2=${yyyymmdd2:-20240727}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}
jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do

  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  h=0;he=23;dh=3

  while [ $h -le $he ]; do
    hh=$(printf %02d $h)
    $EXE ${yyyy}${mm}${dd}${hh}
    h=$(expr $h + $dh)
  done #h
  
  i=$(expr $i + 1)
done #i

exit 0


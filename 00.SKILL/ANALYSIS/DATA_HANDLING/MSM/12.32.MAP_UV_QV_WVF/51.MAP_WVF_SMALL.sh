#!/bin/bash

# Tue, 18 Jun 2024 08:23:07 +0900
# /work09/am/00.WORK/2024.06.OKINAWA_RAIN/22.12.MSM/12.22.PT

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-2024072300}

LONW=125 ;LONE=142 ; LATS=30 ;LATN=45
LEV=975

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

FIGDIR=FIG_$(basename $0 .sh);mkd $FIGDIR
#CTL=$(basename $0 .sh).CTL
#if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

INDIR=/work02/DATA/MSM.NC/MSM-P/${YYYY}
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=${MM}${DD}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=${FIGDIR}/$(basename $0 .sh)_${YYYYMMDDHH}.PDF

TEXT="QV UV $TIME ${LEV}hPa MSM"
LEVS="14 21 1"
KIND="white->antiquewhite->mistyrose->lightpink->mediumvioletred->navy->darkblue->blue->dodgerblue->aqua"

FS=2
UNIT=g/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grads off';'set grid off'
# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
#'q dims';say result;'quit '

'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))*1000'
'rho=lev*100/287/temp'
'WVX=rho*QV*u';'WVY=rho*QV*v'

'set map 3 1 3';'set mpdset hires'
'set grid off'
'set xlint 10';'set ylint 5'
'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(WVX,15);WVY -SCL 0.5 300 -P 20 20 -SL g/m2/s'
'set xlab off';'set ylab off'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yb-0.35
'set cthick  4'; 'set ccolor  1'
'vec.gs skip(WVX,15);WVY -SCL 0.5 300 -P 'xx' 'yy' -SL g/m2/s'

#'VMAG=mag(WVX,WVY)'
#'set gxout contour'
#'set ccolor 0';'set cthick 10'
#'set clevs 150';'set clab off'
#'d VMAG'

#'set ccolor 1';'set cthick 5'
#'set cstyle 3'
#'set clevs 150'
#'d VMAG'


# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.8; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

# /work09/am/00.WORK/2024.06.OKINAWA_RAIN/12.12.CHART
#INDIR=FIG_FOR_ANIM
INDIR=FIG_31.MAP_WVF_SMALL

#LAST=$INDIR/Z__C_RJTD_2024061423.pdf
GIF=$(basename $0 .sh)_20240723-0727.GIF
#ROTATE="-rotate -90"

convert -layers optimize -loop 0 -delay 50 -density 144 $ROTATE $INDIR/*.PDF $GIF

#convert -layers optimize -loop 0 -delay 50 -density 144 $ROTATE $INDIR/*.pdf -delay 150 -density 144 $ROTATE $LAST $GIF
 
echo OUT: $GIF

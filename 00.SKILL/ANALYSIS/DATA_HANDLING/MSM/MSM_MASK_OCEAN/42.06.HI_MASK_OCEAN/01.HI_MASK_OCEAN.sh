#!/bin/bash
# https://code.mpimet.mpg.de/boards/53/topics/10933

TOPOIN=TOPO.MSM_5K_INVERT_LAT.nc
TOPOOUT=TOPO.MSM_5K_INVERT_LAT_ZERO_ONE.nc
cdo -expr,'topo = ((topo>1.0)) ? 1.0 : topo/0.0' $TOPOIN $TOPOOUT

echo;echo MMMMM TOPOIN: $TOPOIN
echo;echo MMMMM TOPOOUT: $TOPOOUT;echo

function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

YMDHM1=$1; YMDHM1=${YMDHM1:-202408170600}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}
THREADS=$2; THREADS=${THREADS:-30}

INDIR=/work04/DATA/MSM_HEAT_INDEX_LU2022/OpenMP/${Y1}
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi
INFLE=MSM_HEAT_INDEX_LU2022_${Y1}${MM1}${DD1}_THREADS_${THREADS}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=/work04/DATA/MSM_HEAT_INDEX_LU2022/OpenMP/MASKOCEAN/${Y1}
mkd $ODIR
OUT=$ODIR/MSM_HEAT_INDEX_LU2022_${Y1}${MM1}${DD1}_MASKOCN.nc
cdo -mul  $IN $TOPOOUT $OUT

if [ -f $OUT ];then echo;echo MMMMM OUT: $OUT;echo;fi


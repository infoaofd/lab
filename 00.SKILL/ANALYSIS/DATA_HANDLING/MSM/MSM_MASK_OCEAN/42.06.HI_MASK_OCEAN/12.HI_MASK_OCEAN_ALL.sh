#!/bin/bash
# https://code.mpimet.mpg.de/boards/53/topics/10933

TOPOIN=TOPO.MSM_5K_INVERT_LAT.nc
TOPOOUT=TOPO.MSM_5K_INVERT_LAT_ZERO_ONE.nc
cdo -expr,'topo = ((topo>1.0)) ? 1.0 : topo/0.0' $TOPOIN $TOPOOUT

echo;echo MMMMM TOPOIN: $TOPOIN
echo;echo MMMMM TOPOOUT: $TOPOOUT;echo

YS=2006;YE=2024;Y=$YS

while [ $Y -le $YE ];do


INDIR=/work04/DATA/MSM_HEAT_INDEX_LU2022/OpenMP/${Y1}
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi
INFLE=MSM_HEAT_INDEX_LU2022_${Y1}${MM1}${DD1}_THREADS_${THREADS}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=/work04/DATA/MSM_HEAT_INDEX_LU2022/OpenMP/MASKOCEAN/${Y1}
mkd $ODIR
OUT=$ODIR/MSM_HEAT_INDEX_LU2022_${Y1}${MM1}${DD1}_MASKOCN.nc
cdo -mul  $IN $TOPOOUT $OUT

if [ -f $OUT ];then echo;echo MMMMM OUT: $OUT;echo;fi

Y=$(expr $Y + 1)
done #Y

#!/bin/sh

exe=./runncl.sh

ncl=$(basename $0 .run.sh).ncl

if [ ! -f $ncl ]; then

  echo Error in $0 : No such file, $ncl

  exit 1

fi

if [ $# -ne 1 ]; then

  echo ERROR in $0: Wrong argument.

  echo

  echo Usage: $0 yyyymmdd

  echo

fi

yyyymmdd0=$1

yyyy0=${yyyymmdd0:0:4}

  mm0=${yyyymmdd0:4:2}

  dd0=${yyyymmdd0:6:2}

indir=/work05/manda/DATA/MSM

outdir="FIG"

mkdir -vp $outdir

indirp0=$indir/MSM-P/${yyyy0}

indirs0=$indir/MSM-S/${yyyy0}

yyyymmdd1=$(date -d"$yyyymmdd0"-1days '+%Y%m%d')

yyyy1=${yyyymmdd1:0:4}

  mm1=${yyyymmdd1:4:2}

  dd1=${yyyymmdd1:6:2}

indirp1=$indir/MSM-P/${yyyy1}

indirs1=$indir/MSM-S/${yyyy1}

dirs="$indirs0 $indirp0 $indirs1 $indirp1 $outdir"

for dir in $dirs; do

if [ ! -d $dir ]; then

  echo Error in $0 : No such directory, $dir

  exit 1

fi

done

inlist="\
${indirs0}/${mm0}${dd0}.nc  ${indirp0}/${mm0}${dd0}.nc \
${indirs1}/${mm1}${dd1}.nc  ${indirp1}/${mm1}${dd1}.nc"

for input in $inlist; do

if [ ! -f $input ]; then

  echo No such file, $input

  exit 1

fi

done

export LANG=C

$exe $ncl "$yyyymmdd0" "$yyyymmdd1" "$indir" "$outdir"

exit 0

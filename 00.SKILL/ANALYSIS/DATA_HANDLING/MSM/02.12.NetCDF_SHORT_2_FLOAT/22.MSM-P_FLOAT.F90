PROGRAM TEST_READ
! C:\Users\foome\lab\00.SKILL\ANALYSIS\DATA_HANDLING\MSM\02.14.MSM_READ_WRITE\12.22.MSM-P

! netCDFライブラリで使用する変数や定数の設定ファイルを読み込む
use netCDF ! netCDF4
! include 'netcdf.inc' !netCDF3

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname
integer,parameter::IM=241, JM=253, KM=16, NT=8

real,DIMENSION(IM,JM,KM,NT)::z !double
real,DIMENSION(IM,JM,KM,NT):: u, v, temp, rh !short
real::lon(IM), lat(JM), p(KM)
real::time(NT)
! real(8)::time(NT)
CHARACTER(LEN=40):: name_lon,units_lon, name_lat,units_lat, name_lev,units_lev, &
name_time,units_time

REAL,PARAMETER::FILLVALUE=-999.9

NAMELIST /PARA/ INDIR, INFLE, ODIR, OFLE

READ(5,NML=PARA)

PRINT '(A)','MMMMM INPUT MSM-P'

IN=trim(INDIR)//trim(INFLE)
print '(a,a)','MMMMM INPUT: ',trim(IN)

CALL READ_MSM_P_4D_DtoS(IN,'z' ,IM,JM,KM,NT,z)
CALL READ_MSM_P_4D(IN,'u'   ,IM,JM,KM,NT,   u)
CALL READ_MSM_P_4D(IN,'v'   ,IM,JM,KM,NT,   v)
CALL READ_MSM_P_4D(IN,'temp',IM,JM,KM,NT,temp)
CALL READ_MSM_P_4D(IN,'rh'  ,IM,JM,KM,NT,  rh)

CALL READ_MSM_P_1D(IN,'lon',IM,lon,name_lon,units_lon)
CALL READ_MSM_P_1D(IN,'lat',JM,lat,name_lat,units_lat)
CALL READ_MSM_P_1D(IN,'p',  KM,p  ,name_lev,units_lev)
CALL READ_MSM_P_1D(IN,'time',NT,time,name_time,units_time)
PRINT *

!print *,'AAA',trim(units_time)

PRINT '(A)','MMMMM OUTPUT'
CALL WRITE_MSM_P(INDIR,INFLE,ODIR,OFLE,IM,JM,KM,NT, z, u, v, temp, rh, lon, lat, p, time, &
FILLVALUE, &
name_lon,units_lon, &
name_lat,units_lat, &
name_lev,units_lev, &
name_time,units_time)

END PROGRAM TEST_READ

SUBROUTINE CHECK( STATUS )
use netCDF ! netCDF4
  INTEGER, INTENT (IN) :: STATUS
  IF(STATUS /= NF90_NOERR) THEN 
    PRINT '(A,A)','EEEEE ERROR ',TRIM(NF90_STRERROR(STATUS))
    STOP "ABNORMAL END"
  END IF
END SUBROUTINE CHECK

SUBROUTINE READ_MSM_P_1D(IN,varname,NDIM,var,long_name,units)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::NDIM
REAL,DIMENSION(NDIM),INTENT(INOUT)::var
CHARACTER(LEN=*),INTENT(INOUT):: long_name,units

integer stat, ncid, varid, varid1
real::scale, offset
ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, var)
stat = nf90_get_att(ncid,varid,'long_name',long_name)
stat = nf90_get_att(ncid,varid,'units',units)
print '(a,i5,1x,a,1x,a)',trim(varname),varid,TRIM(long_name),TRIM(units)
stat=NF90_CLOSE(ncid)
END SUBROUTINE READ_MSM_P_1D

SUBROUTINE READ_MSM_P_4D(IN,varname,IM,JM,KM,NT,var)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::IM,JM,KM,NT
REAL,DIMENSION(IM,JM,KM,NT),INTENT(INOUT)::var
integer stat, ncid, varid, varid1
real::scale, offset
integer(2)::varin(IM,JM,KM,NT) ! short型の2バイト整数

ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く
!print *,'nf90 open stat=',stat
!print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
!print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
!print *,'nf90_get_att stat=',stat
stat = nf90_get_att(ncid,varid,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
!print '(a,f10.5,a,g11.4)','scale_factor=',scale,' offset=',offset

var(:,:,:,:)=float(varin(:,:,:,:))*scale+offset

stat=NF90_CLOSE(ncid)!入力ファイル閉じる

!print *
END SUBROUTINE READ_MSM_P_4D



SUBROUTINE READ_MSM_P_4D_DtoS(IN,varname,IM,JM,KM,NT,var)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::IM,JM,KM,NT
REAL,DIMENSION(IM,JM,KM,NT),INTENT(INOUT)::var
integer stat, ncid, varid, varid1
real::scale, offset
real(8)::varin(IM,JM,KM,NT) ! DOUBLE PRECISIONt型の8バイト実数

ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
!print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, varin)
!stat = nf90_get_att(ncid,varid,'scale_factor',scale)
!stat = nf90_get_att(ncid,varid,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
!print '(a,f10.5,a,g11.4)','scale_factor=',scale,' offset=',offset

var(:,:,:,:)=sngl(varin(:,:,:,:)) !*scale+offset

stat=NF90_CLOSE(ncid)!入力ファイル閉じる

!print *
END SUBROUTINE READ_MSM_P_4D_DtoS



SUBROUTINE WRITE_MSM_P(INDIR,INFLE,ODIR,OFLE,IM,JM,KM,NT, z, u, v, temp, rh, lon, lat, p, time, &
FILLVALUE, &
name_lon,units_lon, &
name_lat,units_lat, &
name_lev,units_lev, &
name_time,units_time)

use netcdf

CHARACTER(len=500),INTENT(IN)::INDIR,INFLE,ODIR,OFLE
CHARACTER*2000::OUT
INTEGER,INTENT(IN)::IM,JM,KM,NT
REAL,INTENT(IN),DIMENSION(IM,JM,KM,NT)::z,u,v,temp,rh
REAL,INTENT(IN)::lon(IM),lat(JM),p(KM)
REAL,INTENT(IN)::time(NT)
REAL,INTENT(IN)::FILLVALUE
!REAL(8),INTENT(IN)::time(NT)
character(len=40)::units_lon,units_lat,units_lev, units_time
character(len=40)::name_lon,name_lat,name_lev,name_time

INTEGER :: NCIDO, VARIDO !NC4 OUTPUT VAR
INTEGER :: LON_DIM_ID, LAT_DIM_ID, LEV_DIM_ID,TIME_DIM_ID
INTEGER :: LON_ID, LAT_ID, LEV_ID, TIME_ID
INTEGER STATUS

OUT=trim(ODIR)//'/'//trim(OFLE)

! 出力ファイルを開く
!print '(A,A)','MMMMM OPEN ',TRIM(OUT)
CALL CHECK( NF90_CREATE( TRIM(OUT), NF90_HDF5, NCIDO) )

! 次元を定義する
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lon', IM, LON_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lat', JM, LAT_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'p',   KM, LEV_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'time', NF90_UNLIMITED, TIME_DIM_ID) )
!print '(A,1X,A)','MMMMM def_dim'

! 変数を定義する
CALL CHECK( NF90_DEF_VAR(NCIDO, 'time', NF90_REAL, TIME_DIM_ID, TIME_ID) )
!CALL CHECK( NF90_DEF_VAR(NCIDO, 'time', NF90_DOUBLE, TIME_DIM_ID, TIME_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lat',  NF90_REAL, LAT_DIM_ID, LAT_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lon',  NF90_REAL, LON_DIM_ID, LON_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'p'  ,  NF90_REAL, LEV_DIM_ID, LEV_ID) )
!print '(A,1X,A)','MMMMM def_var'

! 変数に attributionをつける
CALL CHECK( NF90_PUT_ATT(NCIDO, LON_ID,  'units', units_lon) )
CALL CHECK( NF90_PUT_ATT(NCIDO, LON_ID,  'long_name ', name_lon) )
!print '(A,1X,A)',trim(name_lon),trim(units_lon)
CALL CHECK( NF90_PUT_ATT(NCIDO, LAT_ID,  'units', units_lat) )
CALL CHECK( NF90_PUT_ATT(NCIDO, LAT_ID,  'long_name ', name_lat) )
!print '(A,1X,A)',trim(name_lat), trim(units_lat)
CALL CHECK( NF90_PUT_ATT(NCIDO, LEV_ID,  'units', units_lev) )
CALL CHECK( NF90_PUT_ATT(NCIDO, LEV_ID,  'long_name ', name_lev) )
!print '(A,1X,A)',trim(name_lev), trim(units_lev)
CALL CHECK( NF90_PUT_ATT(NCIDO, TIME_ID, 'units', units_time) )
CALL CHECK( NF90_PUT_ATT(NCIDO, TIME_ID,  'long_name ', name_time) )
!print '(A,1X,A)',trim(name_time), trim(units_time)
!print '(A,1X,A)','MMMMM attribution'

! GLOBAL ATTRIBUTES
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"output_dir", TRIM(ODIR)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"output_file",TRIM(OFLE)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"input_dir", TRIM(INDIR)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"input_file",TRIM(INFLE)) )
!print '(A,1X,A)','MMMMM global attribution'

! define モードを終了する。
CALL CHECK( NF90_ENDDEF(NCIDO) )
!print '(A,1X,A)','MMMMM define'

CALL CHECK( NF90_PUT_VAR(NCIDO, TIME_ID, time ) )
!print *,'put_var time'
CALL CHECK( NF90_PUT_VAR(NCIDO, LEV_ID, p ) )
!print '(A,1X,A)','MMMMM put_var p'
CALL CHECK( NF90_PUT_VAR(NCIDO, LAT_ID, lat ) )
!print '(A,1X,A)','MMMMM put_var lat'
CALL CHECK( NF90_PUT_VAR(NCIDO, LON_ID, lon ) )
!print '(A,1X,A)','MMMMM put_var lon'

CALL CHECK( NF90_DEF_VAR(NCIDO, "z", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEV_DIM_ID, TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, z ) )
!print '(A,1X,A)','MMMMM put_var z'

CALL CHECK( NF90_DEF_VAR(NCIDO, "u", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEV_DIM_ID, TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m/s') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, u ) )
!print '(A,1X,A)','MMMMM put_var u'

CALL CHECK( NF90_DEF_VAR(NCIDO, "v", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEV_DIM_ID, TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','m/s') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, v ) )
!print '(A,1X,A)','MMMMM put_var v'

CALL CHECK( NF90_DEF_VAR(NCIDO, "temp", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEV_DIM_ID, TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','K') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, temp ) )
!print '(A,1X,A)','MMMMM put_var temp'

CALL CHECK( NF90_DEF_VAR(NCIDO, "rh", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,LEV_DIM_ID, TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','%') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, rh ) )
!print '(A,1X,A)','MMMMM put_var rh'

CALL CHECK(NF90_CLOSE(NCIDO))
!print '(A,1X,A)','MMMMM CLOSE'

END SUBROUTINE WRITE_MSM_P

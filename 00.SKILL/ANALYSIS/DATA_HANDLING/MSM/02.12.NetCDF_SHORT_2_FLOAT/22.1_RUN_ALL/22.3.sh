#!/bin/bash
# C:\Users\foome\lab\00.SKILL\ANALYSIS\DATA_HANDLING\MSM\02.14.MSM_READ_WRITE\12.22.MSM-P

Y=$1;MM=$2;DD=$3
Y=${Y:-2020};MM=${MM:-07};DD=${DD:-03}
YMD=${Y}${MM}${DD}

INDIR=/work02/DATA/MSM.NC/MSM-P/${Y}/
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1;fi
INFLE=${MM}${DD}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1;fi

ODIR=/work02/DATA/MSM.FLOAT/MSM-P/${Y}
mkd $ODIR
OFLE=${MM}${DD}.nc
OUT=$ODIR/$OFLE

PASS=5 # REPEAT TIME
#ODIR=/work02/DATA/MSM_1-2-1FILTER/MSM-P/${PASS}PASS/${Y}/
#if [ ! -d $ODIR ];then mkdir -vp $ODIR; fi
#OFLE=MSM-P_UV_1-2-1_UV_${YMD}.nc

NML=$(basename $0 .sh)_NML.TXT
cat <<EOF >$NML
&PARA
INDIR="$INDIR",
INFLE="$INFLE",
ODIR="$ODIR",
OFLE="$OFLE",
&END
EOF

src=22.MSM-P_FLOAT.F90 #$(basename $0 .sh).F90
exe=$(basename $src .F90).exe
nml=$(basename $0 .sh).nml

#f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
#OPT=" -convert big_endian -assume byterecl"
#LOPT= #"-I/usr/local/stpk-0.9.20.0/include -L/usr/local/stpk-0.9.20.0/lib -lstpk"

export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:${LD_LIBRARY_PATH}:.
f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
LOPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

<<COMMENT
echo MMMMM Compiling ${src} ...
echo ${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEEE COMPILE ERROR!!!"; echo; echo TERMINATED.
rm -vf *.o
exit 1
fi

echo MMMMM LINKING ...
OBJ=$(basename $src .F90).o
echo ${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEE LINK ERROR!!!"; echo; echo TERMINATED.
exit 1
fi
rm -v *.o
echo MMMMM DONE COMPILE.
COMMENT

echo MMMMM ${exe} is running ...; echo
D1=$(date -R)

${exe} < ${NML}
if [ $? -ne 0 ]; then
echo; echo "EEEEE RUNTIME ERROR!!!"; echo; echo TERMINATED.
#rm -vf ${exe}
exit 1
fi
#rm -vf ${exe}

echo; echo "MMMMM Done ${exe}"; echo
if [ -f $OUT ];then echo MMMMM OUT: $OUT;fi

# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/01.BASH/LINUX_DATE/0.LINUX_DATE.md
# yyyy年mm1月dd1日からyyyy2年mm2月dd2日までループさせる

EXE=$(basename $0 .RUN.sh).sh
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE;exit 1;fi

yyyy1=2020; yyyy2=2020; y=$yyyy1
mm1=06; dd1=01
mm2=08; dd2=31

while [ $y -le $yyyy2 ]; do

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy1}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  $EXE $yyyy $mm $dd

  i=$(expr $i + 1)
done

  y=$(expr $y + 1)

done
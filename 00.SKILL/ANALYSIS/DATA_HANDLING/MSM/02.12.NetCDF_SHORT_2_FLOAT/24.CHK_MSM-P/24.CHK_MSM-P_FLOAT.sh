#!/bin/bash
# Wed, 29 Jan 2025 11:13:01 +0900
# /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/62.12.COMPOSITE/12.12.R1DAAV/02.12.QFLUX.ge.50PCNT

function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

VAR=$1; VAR=${VAR:-z}

YMDHM1=$2; YMDHM1=${YMDHM1:-202007030000}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}


TIME=${HH1}Z${DD1}${MMM1}${Y1}
echo $TIME #;exit

CTL1=MSM-P_F.CTL #$(basename $0 .sh).CTL
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi

CTL2=MSM-P.CTL #$(basename $0 .sh).CTL
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi

GS=$(basename $0 .sh).GS; FIG=$(basename $0 .sh)_${Y1}${MM1}${DD1}_${HH1}_${VAR}.PDF

# NetCDF OUT
# https://gitlab.com/infoaofd/lab/-/tree/master/00.SKILL/00.TOOL/GRADS/GrADS_RECIPE/GrADS_NETCDF書き出し
#NC=$(basename $0 .sh).nc

# LONW= ;LONE= ; LATS= ;LATN=
LEV=300

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
# KIND='midnightblue->deepskyblue->paleturquoise->white->orange->red->magenta'
# FS=2
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'q ctlinfo 1';say sublin(result,1);say
'open ${CTL2}';'q ctlinfo 2';say sublin(result,1);say

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 2
xwid = 5.0/xmax; ywid = 8.0/ymax
xmargin=0.5; ymargin=0.5
'cc';'set grads off';'set grid off'

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

# 'color ${LEVS} -kind ${KIND} -gxout shaded'
'set dfile 1'
# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'set gxout shaded'
'q dims';say result
'd ${VAR}.1'
'cbarn'
'set gxout contour';'set ccolor 1'
'd ${VAR}.1'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
Y0=yt

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=xl; y=yt+0.2
'draw string 'x' 'y' ${Y1}${MM1}${DD1}_${HH1}'



ymap = 2
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set dfile 2'
# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'set gxout shaded'
'd ${VAR}.2'
'cbarn'
'set gxout contour';'set ccolor 1'
'd ${VAR}.2'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=xl; y=yt+0.2
'draw string 'x' 'y' ${Y1}${MM1}${DD1}_${HH1}'



# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=Y0+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
#yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
#yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

#say; say 'MMMMM NetCDF OUT'
#'define VAROUT = VAR'
#'set sdfwrite '
#'sdfwrite VAROUT'
#say 'MMMMM DONE.'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

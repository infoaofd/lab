#!/bin/bash
# C:\Users\foome\lab\00.SKILL\ANALYSIS\DATA_HANDLING\MSM\02.14.MSM_READ_WRITE\12.22.MSM-P
# CHECK float or double 

INROOT=/work02/DATA/MSM.NC/MSM-P/
YS=2006;YE=2024;Y=$YS

while [ $Y -le $YE ]; do
INDIR=$INROOT/$Y
IN=$INDIR/0601.nc
VARTYPE=$(ncdump -h $IN |grep "time(time)")
echo $Y $VARTYPE
Y=$(expr $Y + 1)
done #Y

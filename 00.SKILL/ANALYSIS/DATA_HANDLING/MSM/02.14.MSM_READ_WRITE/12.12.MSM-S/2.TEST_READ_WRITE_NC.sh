#!/bin/bash
# /work09/ma/10.WORK/2024.HUMID_HEAT/12.12.ROMPS_HEAT_INDEX/22.12.MSM/12.12.TEST_READ_WRITE_MSM_S

Y=$1;MM=$2;DD=$3
Y=${Y:-2024};MM=${MM:-08};DD=${DD:-17}
YMD=${Y}${MM}${DD}

INDIR=/work02/DATA/MSM.NC/MSM-S/${Y}/
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1;fi
INFLE=${MM}${DD}.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1;fi

ODIR=.
OFLE=$(basename $0 .sh)_CHECK_${YMD}.nc

<<COMMENT
ODIR=/work02/DATA/MSM_HI_LU2022/${Y}/
if [ ! -d $ODIR ];then mkdir -vp $ODIR; fi
OFLE=MSM_HI_LU2022_${YMD}.nc
COMMENT

NML=$(basename $0 .sh)_NML.TXT
cat <<EOF >$NML
&PARA
INDIR="$INDIR",
INFLE="$INFLE",
ODIR="$ODIR",
OFLE="$OFLE",
&END
EOF

src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

#f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
#OPT=" -convert big_endian -assume byterecl"
#LOPT= #"-I/usr/local/stpk-0.9.20.0/include -L/usr/local/stpk-0.9.20.0/lib -lstpk"

export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:${LD_LIBRARY_PATH}:.
f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
LOPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

echo MMMMM Compiling ${src} ...
echo ${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
${f90} ${OPT} ${DOPT} -c ${src} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEEE COMPILE ERROR!!!"; echo; echo TERMINATED.
rm -vf *.o
exit 1
fi

echo MMMMM LINKING ...
OBJ=$(basename $src .F90).o
echo ${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
${f90} ${OPT} ${OBJ} -o ${exe} ${LOPT}
if [ $? -ne 0 ]; then
echo; echo "EEEE LINK ERROR!!!"; echo; echo TERMINATED.
exit 1
fi
rm -v *.o
echo MMMMM DONE COMPILE.

echo MMMMM ${exe} is running ...; echo
D1=$(date -R)

${exe} < ${NML}
if [ $? -ne 0 ]; then
echo; echo "EEEEE RUNTIME ERROR!!!"; echo; echo TERMINATED.
rm -vf ${exe}
exit 1
fi
rm -vf ${exe}

echo; echo "MMMMM Done ${exe}"; echo
#cp -av ${src} $(basename $0) $ODIR
OUT=${ODIR}/${OFLE}
if [ -f $OUT ];then echo MMMMM OUT: $OUT;fi

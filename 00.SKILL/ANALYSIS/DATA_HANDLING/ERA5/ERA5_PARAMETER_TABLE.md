# ERA5 PARAMETER TABLE

https://codes.ecmwf.int/grib/param-db/ 

| Table ID      | Parameter ID | CDO    | Name                                       | VAR NAME             | Unit       |
| ------------- | ------------ | ------ | ------------------------------------------ | -------------------- | ---------- |
| 170, 128, 180 | 143          | var134 | Convective precipitation                   |                      | m          |
| 228           | 218          | var218 | Convective rain rate                       |                      | kg m-2 s-1 |
| 170, 128, 180 | 142          | var142 | Large-scale precipitation                  |                      | m          |
| 228           | 219          | var219 | Large scale rain rate                      |                      | kg m-2 s-1 |
| 235           | 33           |        | Mean surface sensible heat flux            | U_GRD_GDS0_SFC_ave1h |            |
| 235           | 34           |        | Mean surface latent heat flux              | V_GRD_GDS0_SFC_ave1h |            |
| 235           | 37           |        | Mean surface net short-wave radiation flux | MNTSF_GDS0_SFC_ave1h |            |
| 235           | 38           |        | Mean surface net long-wave radiation flux  | SGCVV_GDS0_SFC_ave1h |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |
|               |              |        |                                            |                      |            |

​        'convective_available_potential_energy', 'convective_inhibition', 'convective_precipitation',
​        'convective_rain_rate', 'gravity_wave_dissipation', 'large_scale_precipitation',
​        'large_scale_rain_rate',
​        '10m_u_component_of_wind', '10m_v_component_of_wind', '2m_dewpoint_temperature',
​        '2m_temperature', 'mean_sea_level_pressure', 'mean_surface_latent_heat_flux',
​        'mean_surface_sensible_heat_flux', 'sea_surface_temperature', 'surface_pressure',

# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TIPS.md

CTL=ERA5.CTL
OUT='ERA5_ECS_20220618_21_P_CONV.pdf'
#OUT='ERA5_ECS_20220618_23-19_00_P_CONV.pdf'
#OUT='ERA5_ECS_20220619_00-06_P_CONV.pdf'

'open 'CTL
'q ctlinfo';say result

'R=var218(time=21Z18JUN2022)'
#'R=sum(var218,time=23Z18JUN2022,time=00Z19JUN2022)'
#'R=sum(var218,time=00Z19JUN2022,time=06Z19JUN2022)'
'R=R*3600' ;#mm/s->mm/h

'cc'
'set gxout shaded'
'set lat 20 39'
KIND='(255,255,255)->(245,245,245)->(175,237,237)->(152,251,152)->(67,205,128)->(59,179,113)->(250,250,210)->(255,255,0)->(255,164,0)->(255,0,0)->(205,55,0)->(199,20,133)->(237,130,237)->(255,0,255)'
#'color 0.2 1 0.2 -kind 'KIND
'color 1 20 1 -kind 'KIND
'set mpdset hires'
'set grid off';'set grads off'
'd R'

'trackplot 128   30   128.5 30   -c 1 -l 1 -t 1'
'trackplot 128.5 30   128.5 30.5 -c 1 -l 1 -t 1'
'trackplot 128.5 30.5 128   30.5 -c 1 -l 1 -t 1'
'trackplot 128   30.5 128   30   -c 1 -l 1 -t 1'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)
x1=xl; x2=xr-1; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 5 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 4 0'; 'set strsiz 0.12 0.14'
'draw string 'x' 'y' [mm/hr]'


'draw title 'OUT

'gxprint 'OUT

say 'OUT: 'OUT

'quit'


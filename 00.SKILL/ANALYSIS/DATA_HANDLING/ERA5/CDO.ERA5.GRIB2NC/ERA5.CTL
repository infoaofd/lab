# /work02/work01/DATA/ERA5/ECS/01HR/NC
dset ^ERA5_ECS_SFC.FLUX_01HR_%y4%m2%d2_PRECIP.nc
title ERA5 RAIN
options template yrev
undef 9.96921e+36
dtype netcdf
xdef 93 linear 110 0.25
ydef 121 linear 20 0.25
zdef 1 linear 0 1
tdef 240 linear 00Z15JUN2022 60mn
vars 4
var218=>var218  0  t,y,x  var218
var142=>var142  0  t,y,x  var142
var219=>var219  0  t,y,x  var219
var134=>var134  0  t,y,x  var134
endvars

#var218 Convective rain rate [kg m-2 s-1]
#var142 Large-scale precipitation [m]
#var219 Large scale rain rate [kg m-2 s-1]
#var134 Convective precipitation [m]
#
#[kg m-2 s-1]=[1 mm/s]


INDIR=/work02/work01/DATA/ERA5/ECS/01HR/2022/06
DATE=$1; DATE=${DATE:-20220619}
INFLE=ERA5_ECS_SFC.FLUX_01HR_${DATE}.grib
IN=$INDIR/$INFLE
ODIR=.
OFLE=$(basename $IN .grib)_PRECIP.nc
OUT=$ODIR/$OFLE

var1=var134 ;v1='Convective precipitation [m]'
var2=var218 ;v2='Convective rain rate [kg m-2 s-1]'
var3=var142 ;v3='Large-scale precipitation [m]'
var4=var219 ;v4='Large scale rain rate [kg m-2 s-1]'


cdo -f nc4 select,name=$var1,$var2,$var3,$var4 $IN $OUT


echo
echo $var1 $v1
echo $var2 $v2
echo $var3 $v3
echo $var4 $v4
echo
echo "[kg m-2 s-1]=[1 mm/s]"
echo
echo IN: $IN
echo OUT: $OUT

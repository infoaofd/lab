# https://github.com/lucas-uw/WRF-tools
# -*- coding: utf-8 -*-

import numpy as np
import xarray as xr
import matplotlib
import matplotlib.pyplot as plt
import cartopy
import cartopy.crs as ccrs
import WRFDomainLib
import matplotlib.ticker as mticker

C="K17"; R="R27"
RUNNAME=C+"."+R
TITLE=RUNNAME

DEMFile = 'DemoData/ETOPO1.0_1degree.nc'
WPSFile = 'namelist.wps.'+C+"."+R #'DemoData/namelist.wps.2010Nash'
SSTDIR="/work04/manda/2022.K17/00.02.K17.PAPER_FIG/30.02.WRF.DOMAIN.SST/20.02.K17.DOMAIN.SST/12.00.NAVO.SST.FILL.NAN/"
SSTFLE="20170705-NAVO-FILLNAN_SST.nc"
 
FIG=RUNNAME+"_WRF_DOMAIN_SST.eps"
#FIG=RUNNAME+"_WRF_DOMAIN_SST.png"

DEMDs = xr.open_dataset(SSTDIR+SSTFLE)
#DEMDs = xr.open_dataset(DEMFile)
dem = DEMDs['SST'].values
#dem = DEMDs['DEM'].values
dem_lat = DEMDs['lat'].values
dem_lon = DEMDs['lon'].values

dem_lons, dem_lats = np.meshgrid(dem_lon, dem_lat)

#print(dem)

#for i in np.arange(dem.shape[0]):
#    for j in np.arange(dem.shape[1]):
#        if dem[i,j]<0:
#            dem[i,j]=0

#SSTs = xr.open_dataset(SSTDIR+SSTFLE)
#SST = SSTs['analysed_sst'].values
#SST_lat = SSTs['lat'].values
#SST_lon = SSTs['lon'].values
#
#SST_lons, SST_lats = np.meshgrid(SST_lon, SST_lat)
#
#for i in np.arange(SST.shape[0]):
#    for j in np.arange(SST.shape[1]):
#        if SST[i,j]<0:
#            SST[i,j]=0
#
wpsproj, latlonproj, corner_lat_full, corner_lon_full, length_x, length_y = WRFDomainLib.calc_wps_domain_info(WPSFile)

cmap1 = matplotlib.cm.jet #terrain
vmin = 273 #0
vmax = 304
#vmax = 3000

fig1 = plt.figure(figsize=(6,8))
ax1 = plt.subplot(1, 1, 1, projection=wpsproj)

ax1.pcolormesh(dem_lons, dem_lats, dem, cmap=cmap1, vmin=vmin, vmax=vmax, alpha=1, transform=ccrs.PlateCarree(), zorder=0)

#print(corner_lon_full)

# d01
corner_x1, corner_y1 = WRFDomainLib.reproject_corners(corner_lon_full[0,:], corner_lat_full[0,:], wpsproj, latlonproj)
ax1.set_xlim([corner_x1[0]-length_x[0]/15, corner_x1[3]+length_x[0]/15])
ax1.set_ylim([corner_y1[0]-length_y[0]/15, corner_y1[3]+length_y[0]/15])

# d01 box
ax1.add_patch(matplotlib.patches.Rectangle((corner_x1[0], corner_y1[0]),  length_x[0], length_y[0], 
                                    fill=None, lw=1, edgecolor='white', zorder=2))
ax1.text(corner_x1[0]+length_x[0]*0.05, corner_y1[0]+length_y[0]*0.95, 'D01',
        fontweight='bold', size=9, color='white', zorder=2)

# d02 box
corner_x2, corner_y2 = WRFDomainLib.reproject_corners(corner_lon_full[1,:], corner_lat_full[1,:], wpsproj, latlonproj)
ax1.add_patch(matplotlib.patches.Rectangle((corner_x2[0], corner_y2[0]),  length_x[1], length_y[1], 
                                    fill=None, lw=1, edgecolor='gold', zorder=2))
ax1.text(corner_x2[0]+length_x[1]*0.05, corner_y2[0]+length_y[1]*0.9, 'D02',
        fontweight='bold', size=9, color='gold', zorder=2)

# d03 box
corner_x3, corner_y3 = WRFDomainLib.reproject_corners(corner_lon_full[2,:], corner_lat_full[2,:], wpsproj, latlonproj)
ax1.add_patch(matplotlib.patches.Rectangle((corner_x3[0], corner_y3[0]),  length_x[2], length_y[2],
                                    fill=None, lw=1, edgecolor='cyan', zorder=2))
ax1.text(corner_x3[0]+length_x[2]*0.1, corner_y3[0]+length_y[2]*0.9, 'D03', va='top', ha='left',
        fontweight='bold', size=9, color='cyan', zorder=2)

# LONGITUDE TICK MARKS
degree_sign= u'\N{DEGREE SIGN}E'
xaxlon=120; xaxlat=19.5; xaxtext=str(xaxlon)+degree_sign
xlon, ylat = wpsproj.transform_point(xaxlon, xaxlat, latlonproj)
ax1.text(xlon, ylat, xaxtext, size=11, ha='center')

xaxlon=130; xaxlat=20; xaxtext=str(xaxlon)+degree_sign
xlon, ylat = wpsproj.transform_point(xaxlon, xaxlat, latlonproj)
ax1.text(xlon, ylat, xaxtext, size=11, ha='center')

xaxlon=140; xaxlat=19.5; xaxtext=str(xaxlon)+degree_sign
xlon, ylat = wpsproj.transform_point(xaxlon, xaxlat, latlonproj)
ax1.text(xlon, ylat, xaxtext, size=11, ha='center')

# decorations
ax1.coastlines('50m', linewidth=0.8)
#ax1.add_feature(cartopy.feature.OCEAN, edgecolor='k', facecolor='lightblue', zorder=1)
#ax1.add_feature(cartopy.feature.LAKES, edgecolor='k', facecolor='lightblue', zorder=1)
ax1.add_feature(cartopy.feature.LAND, edgecolor='k', facecolor='k', zorder=1)
#states = cartopy.feature.NaturalEarthFeature(category='cultural', scale='50m', facecolor='none',
#                             name='admin_1_states_provinces_shp')
#ax1.add_feature(states, linewidth=0.5)

gl = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=False, linestyle='--', alpha=1)
gl.top_labels = False #True
gl.bottom_labels = False #True #False
gl.left_labels = True
gl.right_labels = False #True
gl.xlocator = matplotlib.ticker.FixedLocator(np.arange(0,360,10))
gl.ylocator = matplotlib.ticker.FixedLocator(np.arange(0,81,10))

ax1.set_title(TITLE, size=16)



cbar_ax = fig1.add_axes([0.2, 0.15, 0.6, 0.02])
fig1.colorbar(matplotlib.cm.ScalarMappable(cmap=cmap1, norm=matplotlib.colors.Normalize(vmin=vmin, vmax=vmax)),
              cax=cbar_ax, ticks=np.arange(0, vmax+1, 5), orientation='horizontal')
cbar_ax.tick_params(labelsize=12)
cbar_ax.text((vmin+vmax)/2, 200, '[K]', ha='right', va='center', size=12)

fig1.savefig(FIG, dpi=600)

#plt.show()
plt.close()
del(fig1)

print(""); print("FIG: "+FIG); print("")


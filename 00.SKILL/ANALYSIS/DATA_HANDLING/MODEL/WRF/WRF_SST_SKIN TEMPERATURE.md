## WRF SKIN TEMPERATURE SST

Aug 14, 2021

https://forum.mmm.ucar.edu/threads/using-skin-temperature-as-sst.10636/post-22454



Dear all, I have read somewhere that SKIN TEMPERATURE can be used to update SST. I have the SKIN TEMPERATIRE from ECMWF dataset and I want to update SST using this data. Is there a convenient way to do it , like adding something in the WRF namelist.input file? I know how to update the SST (sst_update = 1) but I don't know how to force the model to read SKIN TEMPERATURE in place of SST. Or should we go through the standard WPS processing to generate the required SST files (note sure how to do it)? Could you please help?





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Aug 23, 2021](https://forum.mmm.ucar.edu/threads/using-skin-temperature-as-sst.10636/post-22537)

- 
- [#2](https://forum.mmm.ucar.edu/threads/using-skin-temperature-as-sst.10636/post-22537)

Hi,
Do you have both SST and SKIN TEMPERATURE in your ECMWF input files? If you only have SKIN TEMPERATURE, the WPS program will just use skin temperature. However, I may need to give you additional information.

You may also benefit from the information in these 2 previous forum posts.
https://forum.mmm.ucar.edu/phpBB3/viewtopic.php?f=32&t=9973&p=20071&hilit=sst_update+skin+temperature#p20071
https://forum.mmm.ucar.edu/phpBB3/viewtopic.php?f=48&t=8113&p=20024&hilit=sst_update+skin+temperature#p20024



#### [francescomaicu](https://forum.mmm.ucar.edu/members/francescomaicu.3673/)

##### Member



- [Jan 29, 2021](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20020)

- 
- [#1](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20020)

Dear WRF Supporters,
I'm trying to go a little bit into the detail of the SKINTEMP and SST.
I run WRF with NCEP GDAS data.
Once I download the grib files I run two times ungrib, first with Vtable.GFS, then with Vtable.SST.
The point is that both SKINTEMP and SST are derived (I guess looking at the Vtable) from the same field in the input grib files. Am I correct?
Then running metgrid, the two variables appear different because the interpolation/masking are different in the metgrid.tbl.
If I add the only line of Vtable.SSt in Vtable.GFS and run ungrib, the intermediate files do not contain the SST, but only the SKINTEMP.
Why this happen?





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Feb 1, 2021](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20058)

- 
- [#2](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20058)

Hi,
The Vtable.SST table is meant to be run with an additional source of SST data. Most of the large-scale models include an SST or SKINTEMP component, but the data are very coarse. This is okay as long as you aren't running for more than about a week. If running a week or longer, you likely want to obtain SST data from another source that will have higher temporal and spatial resolution. The 6th slide on [this presentation](https://www2.mmm.ucar.edu/wrf/users/tutorial/202101/werner_data_util_pp.pdf) provides some links to SST fields, if you're interested. For those, you would use Vtable.SST. You will still need to run ungrib separately - once for the NCEP GDAS data, and again for the SST data. You can see examples for running it like this [here](https://www2.mmm.ucar.edu/wrf/OnLineTutorial/CASES/SST/index.php).





[F](https://forum.mmm.ucar.edu/members/francescomaicu.3673/)

#### [francescomaicu](https://forum.mmm.ucar.edu/members/francescomaicu.3673/)

##### Member



- [Feb 2, 2021](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20071)

- 
- [#3](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20071)

Hi,
thank you for the comprehensive answer.
I agree with you regarding the length of the simulation and the opportunity to change to different SST datasets.
My concern is that if I use GDAS or GFS data, both SKINTEMP and SST are derived from the TMP variable in the grib files, so their content is the same except for the interpolation/masking ....
In this case I can avoid to ungrib separately the SST, and METGRID will generate only the SKINTEMP variable.
Finally REAL, will produce (I ask the SST_update) the wrflowinp file with the SST, even though SST is not in the metgrid files.
This is good, but I guess a little bit confusing at a first look. By the way, do you think this procedure is correct if I run a 4 days long simulation.
Things are different with ECMWF grib files which contain both SSTK (sea surface temp) and SKT (skin temp), so I run ungrib just once.
By the way, in the Vtable.ECMWF, it is clear that SST and SKINTEMP are extracted from two different fields, even though SKT has metgrid description as Sea-surface temperature.
The result is that in the metgrid files I find both SKINTEMP and SST, and interestingly they are different since their difference is not zero on the ocean.
Thank you!





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Feb 2, 2021](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20084)

- 
- [#4](https://forum.mmm.ucar.edu/threads/merge-vtable-gfs-and-vtable-sst.9973/post-20084)

Hi,
Thank you for the explanation. It is perfectly fine that the SST field is derived from SKINTEMP; however, if you are not using high-resolution time-varying SST data input, there really is no need to use the sst_update option. If you are only running for 4 days, it shouldn't be necessary. You can read more about the sst_update option in our [Users' Guid](https://www2.mmm.ucar.edu/wrf/users/docs/user_guide_v4/v4.2/users_guide_chap5.html#sst_update)



#### [kunaldayal](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

##### New member



- [Jul 30, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13589)

- 
- [#1](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13589)

Hi,

I am running long-term wind simulations using the WRF model and I am using the following settings for SST update:

&time_control:
\- auxinput4_inname = 'wrflowinp_d<domain>',
\- auxinput4_interval = 360, 360, 360,

&physics:
\- sst_update = 1

And I have "avhrr-only-v2.20171231.nc" files for each of the days for the SST data from Optimum Interpolation Sea Surface Temperature (OISST) at 0.25º × 0.25º resolution (https://www.ncei.noaa.gov/data/sea-surface-temperature-optimum-interpolation/access/avhrr-only/) in the wrf/run folder where the real.exe and wrf.exe are located.

Is this sufficient setting for sst_update or do I have to specify the filename and locations in the namelist.input file?

Appreciate your assistance and advice.

Regards
Kunal





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Jul 31, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13635)

- 
- [#2](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13635)

Hi,
You will need to input your SST data during the WPS process. Take a look at the [SST Example from our WRF Online Tutorial](http://www2.mmm.ucar.edu/wrf/OnLineTutorial/CASES/SST/index.php) for information on how to do this. Because your SST data are in netcdf format, however, you will need to write them to intermediate format in order for metgrid to incorporate the data. To do so, you can follow the instructions in [Chapter 3 of the WRF Users' Guide](http://www2.mmm.ucar.edu/wrf/users/docs/user_guide_v4/v4.1/users_guide_chap3.html#_Writing_Meteorological_Data). You can also find an example [script for writing a netcdf file to intermediate format here](http://www2.mmm.ucar.edu/wrf/users/special_code.html). Just note that this script was provided to us from a user who wrote it specifically for their application, so you will need to make modifications for your own use.





[K](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

#### [kunaldayal](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

##### New member



- [Aug 4, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13837)

- 
- [#3](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13837)

Hi,

I was able to convert the SST data from NetCDF to grib2 format using CDO.

I was also able to perform ungrib on the converted SST data using ungrib.exe.

But, ungrib created input SST files which have zero (0) size whereas the converted SST in grib2 format had approximately 4.8 MB size per day.

I am sure something is wrong that's why the SST files have zero size?

Appreciate your assistance and advice.

Regards
Kunal





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Aug 5, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13848)

- 
- [#4](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13848)

Kunal,
It is recommended to convert the files from NetCDF format, to intermediate format, instead of trying to convert them to grib2 format. Can you give this a try and see if you have better luck that way? I provided some linkable resources in the previous message I sent. Thanks.





[K](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

#### [kunaldayal](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

##### New member



- [Aug 5, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13861)

- 
- [#5](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13861)

Hi,

I take note and I will try your recommendation.

I have a few other questions as well:

If my input atmospheric data, for instance, NCEP-FNL or others already have SST temperature for each time record do I still have to use SST data from another source?

Or just the following settings should suffice?
"
&time_control:
\- auxinput4_inname = 'wrflowinp_d<domain>',
\- auxinput4_interval = 360, 360, 360,
&physics:
\- sst_update = 1
"
Appreciate your advice.

Regards
Kunal





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Aug 5, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13865)

- 
- [#6](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13865)

Hi,
If you are planning to run simulations that go out for more than about a week, it's advised to use an outside source for SST data. Most datasets (for e.g., GFS) typically come with an SST field, but they are usually coarse and not reliable for an extended time. If you are not doing long simulations, then it's okay to skip the outside SST source. In that case, you don't even need to specify anything extra in the namelist.input file - so you don't need to add sst_update, and it's corresponding settings.





[K](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

#### [kunaldayal](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

##### New member



- [Aug 5, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13867)

- 
- [#7](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-13867)

Hi,

I take note and thank you for the information.

I will get back to you after trying the mentioned recommendations for the long simulations using SST.

Regards
Kunal





[K](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

#### [kunaldayal](https://forum.mmm.ucar.edu/members/kunaldayal.281/)

##### New member



- [Aug 29, 2019](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-14108)

- 
- [#8](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-14108)

Hi,

I managed to locate high-resolution SST data from RTG-SST_HR at 0.083 deg x 0.083 deg resolution in Grib format for my simulations.

Thank you.

Regards
Kunal





[J](https://forum.mmm.ucar.edu/members/julianrvogel.3147/)

#### [julianrvogel](https://forum.mmm.ucar.edu/members/julianrvogel.3147/)

##### New member



- [Jan 29, 2021](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20024)

- 
- [#9](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20024)

Hi,

This is already helping me a lot. But I have 2 follow-up questions on SST in WRF:

1.) What is the best practise, if no external SST is available for 3 month simulations? Better use the GFS-FNL or is it no good at all?

2.) According to my default Vtable.GFS, the SST field of my GFS-FNL data is not used. Even for short term simulations, where the SST field does not have to be updated, how is the skin temperature of water bodies initialized? Do I have to add the SST entry in the Vtable.GFS? Or is it determined another way?

I appreciate any help.

Best regards,
Julian





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Feb 1, 2021](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20059)

- 
- [#10](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20059)

Julian,

\1) Have you checked the sources mentioned on slide #6 in [this presentation](https://www2.mmm.ucar.edu/wrf/users/tutorial/202101/werner_data_util_pp.pdf) for SST data?

\2) You do not need to add SST into the Vtable. The model will use SKINTEMP for the values of SST.





[J](https://forum.mmm.ucar.edu/members/julianrvogel.3147/)

#### [julianrvogel](https://forum.mmm.ucar.edu/members/julianrvogel.3147/)

##### New member



- [Feb 2, 2021](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20076)

- 
- [#11](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20076)

Hi kwerner,

thanks a lot for your quick and helpful reply.

In your presentation slides, the first two links under SST do not work for me, as the servers are unavailable for a while already. I didn't know the last link which works well. I will give it a try.

Best regards,
Julian





[K](https://forum.mmm.ucar.edu/members/kwerner.49/)

#### [kwerner](https://forum.mmm.ucar.edu/members/kwerner.49/)

##### Administrator

**Staff member**



- [Feb 2, 2021](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20085)

- 
- [#12](https://forum.mmm.ucar.edu/threads/re-enquiry-about-sst-update-in-wrf.8113/post-20085)

Julian,
Great! And thanks for letting me know about the other 2 links. I'll check into that.
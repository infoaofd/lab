#!/bin/bash


RUNNAME=RW3A.00.04.05.05.0000.01
INDIR=/work00/DATA/HD04/RW3A.00.04/01HR/ARWpost_${RUNNAME}
PREFIX=${RUNNAME}.d01.basic_p.01HR_
ODIR=OUT_$(basename $0 .sh)/${RUNNAME}
mkdir -vp $ODIR

TINT_UNIT="hour"
START="2021/08/12 00:00:00"

F90=CONV.STRAT.F90
if [ ! -f $F90 ];then echo NO SUCH FILE, $F90; exit 1; fi
EXE=$(basename $F90 .F90).EXE
ifort -o $EXE $F90
if [ $? -ne 0 ];then echo COMPILE ERROR, $F90; exit 1; fi


N=00; NH=24

while [ $N -le $NH ]; do

NML=namelist.txt
# https://hydrocoast.jp/index.php?fortran/namelist%E3%81%AE%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95


NN=$(printf %02d $N)
DATE=$(date -d"$START $N $TINT_UNIT" +"%Y-%m-%d_%H:00")
#DATE=$(date -d"$START $N $TINT_UNIT" +"%Y-%m-%d_%H:00:00")

INFLE=${PREFIX}${DATE}.dat
OFLE=CONV_STRAT_${RUNNAME}_${DATE}.dat

if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1; fi
if [ ! -f $INDIR/$INFLE ];then echo NO SUCH FILE, $INFLE; exit 1; fi

echo "&para" > $NML
echo INDIR=\"$INDIR\", >> $NML
echo INFLE=\"$INFLE\", >> $NML
echo ODIR=\"$ODIR\", >> $NML
echo OFLE=\"$OFLE\", >> $NML
echo "&end" >> $NML

$EXE < $NML

N=$(expr $N + 1)

done


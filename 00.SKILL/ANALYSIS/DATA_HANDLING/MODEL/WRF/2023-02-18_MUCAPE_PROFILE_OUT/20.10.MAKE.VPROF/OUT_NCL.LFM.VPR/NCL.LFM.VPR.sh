#!/bin/bash

FH=FH00
YYYY=2022; MM=06; DD=19; HH=00

INDIR1=/work01/DATA/LFM_HCUT.PDEL/${FH}
INDIR2=/work01/DATA/LFM_HCUT.SFC.HINTPL/${FH}

 ODIR=OUT_$(basename $0 .sh)

#LAT1=30.29889; LON1=128.16494
#LON1=129.75; LAT1=29.75
LON1=${LON1:-128.75}; LAT1=${LAT1:-30.5}

mkd $ODIR

NCL=NCL.LFM.VPR.ncl

MMDD=${MM}${DD}

INFLE1=LFM_PRS_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
IN1=$INDIR1/${INFLE1}
INFLE2=LFM_SFC_HINTPL_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc
IN2=$INDIR2/${INFLE2}

OUT1=$ODIR/$(basename $IN1 .nc)_VPR_${LON1}_${LAT1}.TXT

if [ ! -f $IN1 ]; then echo NO SUCH FILE, $IN1; echo; exit 1; fi
if [ ! -f $IN2 ]; then echo NO SUCH FILE, $IN2; echo; exit 1; fi

runncl.sh $NCL $IN1 $IN2 $OUT1 $LON1 $LAT1

if [ $? -ne 0 ]; then
echo "mmmmmmmmmmmmmm"; echo ERROR IN $NCL; echo "mmmmmmmmmmmmmm"
fi
if [ -f $OUT1 ]; then
echo "MMMMMMMMMMMMMMM"; echo OUTPUT: $OUT1; echo "MMMMMMMMMMMMMMM"
#ncdump -h $OUT; echo
else
echo "mmmmmmmmmmmmmm"; echo NO SUCH FILE, $OUT1; echo "mmmmmmmmmmmmmm"
exit 1
fi

cp -av $0 $ODIR

module entraining_CAPE
! C:\Users\atmos\Dropbox\TOOLS_TEMP\E-CAPE_Ito
CONTAINS

subroutine calc_initial_parcel_ml1000m( &
     &km, pres_e_input, hgt_e_input, tem_e_input, rh_e_input, undef, &
     &hgt_p, tem_p, qv_p, errorflag)

  implicit none

  integer, intent(IN)  :: km
  real(4), intent(IN)  :: pres_e_input(km)
  real(4), intent(IN)  :: hgt_e_input(km)
  real(4), intent(IN)  :: tem_e_input(km)
  real(4), intent(IN)  :: rh_e_input(km)
  real(4), intent(IN)  :: undef
  real(4), intent(OUT) :: hgt_p, tem_p, qv_p
  integer, intent(OUT) :: errorflag

  integer :: k, kmark1, kmark2, kpar

  real(8) :: Ph2o, Ph2osatu, qv_satu
  real(8) :: lapse, power, sfcp, topp
  real(8) :: p_layer, calc_theta, total_theta, total_qv, total_hgt
  real(8) :: zpar, pres_e_int0, tem0, qv0
  real(8) :: del_z, int_z, z_ratio, pres_correct

  real(8), allocatable :: pres_e(:), hgt_e(:), tem_e(:), rh_e(:)
  real(8), allocatable :: qv_e(:), vtem_e(:), pres_e_diff(:)

  real(8), parameter :: layer= 1000.d0, pres_ref= 1000.d2
  real(8), parameter :: Ra= 287.04d0, Rv= 461.4d0, Cva= 719.d0, Cvv= 1418.d0
  real(8), parameter :: e= Ra/Rv, g= 9.80665d0
  real(8), parameter :: &
       &coef1= 53.67957d0, coef2= -6743.769d0  , coef3= -4.8451d0

  allocate(pres_e(km))
  allocate(hgt_e(km))
  allocate(tem_e(km))
  allocate(rh_e(km))
  allocate(qv_e(km))
  allocate(vtem_e(km))
  allocate(pres_e_diff(km))

  do k= 1, km
     
     pres_e(k)= dble(pres_e_input(k))
     hgt_e(k) = dble(hgt_e_input(k))
     tem_e(k) = dble(tem_e_input(k))
     rh_e(k)  = dble(rh_e_input(k))

  end do

  !environmental profile
  do k= 1, km

     Ph2osatu = dexp(coef1+coef2/tem_e(k)+coef3*dlog(tem_e(k))) ![hPa]
     Ph2o     = Ph2osatu*rh_e(k)/100.d0 ![hPa]
     qv_e(k)  = e*Ph2o/(pres_e(k)-Ph2o)
     vtem_e(k)= tem_e(k)*(1.d0+qv_e(k)/e)/(1.d0+qv_e(k))

  end do

  pres_e= 100.d0*pres_e

  do k= 2, km

     lapse= (vtem_e(k-1)-vtem_e(k))/(hgt_e(k)-hgt_e(k-1))
     power= g/(Ra*lapse)
     pres_e_diff(k)= pres_e(k-1)*((vtem_e(k)/vtem_e(k-1))**power)&
          &         -pres_e(k)
     
  end do

  do k= 1, km
     if (hgt_e(k) >= 0.d0) then
        kmark1= k
        exit
     end if
  end do
  
  if (kmark1 == 1) then

     lapse= (vtem_e(kmark1)-vtem_e(kmark1+1))/(hgt_e(kmark1+1)-hgt_e(kmark1))
     power= g/(Ra*lapse)
     sfcp = pres_e(kmark1)*((1.d0+lapse*hgt_e(kmark1)/vtem_e(kmark1))**power)

  else

     lapse= (vtem_e(kmark1-1)-vtem_e(kmark1))/(hgt_e(kmark1)-hgt_e(kmark1-1))
     power= g/(Ra*lapse)
     sfcp = pres_e(kmark1)*((1.d0+lapse*hgt_e(kmark1)/vtem_e(kmark1))**power)
     
  end if

  kmark2= 0
  do k= kmark1, km
     if (hgt_e(k) > layer) then
        kmark2= k-1
        exit
     end if
  end do

  if (kmark2 < kmark1) then

     errorflag= 1
     hgt_p= undef
     tem_p= undef
     qv_p = undef

     write(*, '(a)') &
          &"ERROR @ subroutine 'calc_initial_parcel_ml1000m'."
     write(*, '(a)') ' profile data is not suitable for the calculation.'

     deallocate(pres_e)
     deallocate(hgt_e)
     deallocate(tem_e)
     deallocate(rh_e)
     deallocate(qv_e)
     deallocate(vtem_e)
     deallocate(pres_e_diff)

     return

  end if

  lapse= (vtem_e(kmark2)-vtem_e(kmark2+1))/(hgt_e(kmark2+1)-hgt_e(kmark2))
  power= g/(Ra*lapse)
  topp = pres_e(kmark2)&
       &*((1.d0-lapse*(layer-hgt_e(kmark2))/vtem_e(kmark2))**power)

  !determine initial parcel
  if (kmark1 == kmark2) then

     zpar= hgt_e(kmark1)
     qv0 = qv_e(kmark1)
     tem0= tem_e(kmark1)

  else

     total_theta= 0.d0
     total_qv   = 0.d0
     total_hgt  = 0.d0

     do k= kmark1, kmark2

        if (k == kmark1) then

           p_layer= sfcp-0.5d0*(pres_e(k)+pres_e(k+1))

           power     = (Ra/(Ra+Cva))*(1.d0+(Rv/Ra-(Rv+Cvv)/(Ra+Cva))*qv_e(k))
           calc_theta= tem_e(k)*((pres_ref/pres_e(k))**power)

           total_theta= total_theta+calc_theta*p_layer
           total_qv   = total_qv   +qv_e(k)   *p_layer
           total_hgt  = total_hgt  +hgt_e(k)  *p_layer

        else if (k == kmark2) then

           p_layer= 0.5d0*(pres_e(k-1)+pres_e(k))-topp

           power     = (Ra/(Ra+Cva))*(1.d0+(Rv/Ra-(Rv+Cvv)/(Ra+Cva))*qv_e(k))
           calc_theta= tem_e(k)*((pres_ref/pres_e(k))**power)

           total_theta= total_theta+calc_theta*p_layer
           total_qv   = total_qv   +qv_e(k)   *p_layer
           total_hgt  = total_hgt  +hgt_e(k)  *p_layer

        else

           p_layer= 0.5d0*(pres_e(k-1)-pres_e(k+1))

           power     = (Ra/(Ra+Cva))*(1.d0+(Rv/Ra-(Rv+Cvv)/(Ra+Cva))*qv_e(k))
           calc_theta= tem_e(k)*((pres_ref/pres_e(k))**power)

           total_theta= total_theta+calc_theta*p_layer
           total_qv   = total_qv   +qv_e(k)   *p_layer
           total_hgt  = total_hgt  +hgt_e(k)  *p_layer

        end if

     end do

     zpar= total_hgt/(sfcp-topp)
     qv0 = total_qv /(sfcp-topp)

     do k= 1, km-1
        if (zpar >= hgt_e(k) .and. zpar < hgt_e(k+1)) then
           kpar= k
           exit
        end if
     end do

     del_z= zpar-hgt_e(kpar)
     int_z= hgt_e(kpar+1)-hgt_e(kpar)

     z_ratio= del_z/int_z

     pres_correct= pres_e_diff(kpar+1)*z_ratio
     
     lapse= (vtem_e(kpar)-vtem_e(kpar+1))/int_z
     power= g/(Ra*lapse)
     
     pres_e_int0= pres_e(kpar)*((1.d0-lapse*del_z/vtem_e(kpar))**power)&
          &      -pres_correct

     power= (Ra/(Ra+Cva))*(1.d0+(Rv/Ra-(Rv+Cvv)/(Ra+Cva))*qv0)
     tem0 = (total_theta/(sfcp-topp))*((pres_e_int0/pres_ref)**power)

  end if

  hgt_p= real(zpar)
  tem_p= real(tem0)
  qv_p = real(qv0)

  errorflag= 0

  deallocate(pres_e)
  deallocate(hgt_e)
  deallocate(tem_e)
  deallocate(rh_e)
  deallocate(qv_e)
  deallocate(vtem_e)
  deallocate(pres_e_diff)

  return
end subroutine calc_initial_parcel_ml1000m



!mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm
!                                                                   !
! CAPE is calculated using Romps and Kuang (2010)'s parcel model.   !
!                                                                   !
! Initial conditions (height, T, Qv) of a lifted parcel are needed. !
!                                                                   !
!mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm

subroutine calc_CAPE_Romps_and_Kuang_2010_no_fusion_total_fallout(&
     &hgt_p, tem_p, qv_p, &
     &km_input, pres_e_input, hgt_e_input, tem_e_input, rh_e_input, undef, &
     &w, dt, entrainment_rate, maxlev, &
     &cape_output, cin_output, lcl_output, lfc_output, el_output, &
     & MZO, Z_PC, P_PC, TVE_PC, TV_PC, BUOY_PC)

  implicit none

  integer, intent(IN)  :: km_input
  real(4), intent(IN)  :: hgt_p, tem_p, qv_p
  real(4), intent(IN)  :: pres_e_input(km_input)
  real(4), intent(IN)  :: hgt_e_input(km_input)
  real(4), intent(IN)  :: tem_e_input(km_input)
  real(4), intent(IN)  :: rh_e_input(km_input)
  real(4), intent(IN)  :: undef
  real(4), intent(OUT) :: cape_output
  real(4), intent(OUT) :: cin_output
  real(4), intent(OUT) :: lcl_output
  real(4), intent(OUT) :: lfc_output
  real(4), intent(OUT) :: el_output

  INTEGER,INTENT(INOUT)::MZO
  REAL,INTENT(INOUT),DIMENSION(MZO)::Z_PC, P_PC, TVE_PC, TV_PC, BUOY_PC

  !----- vertical velocity of parcel [m/s] -----
  real(8), intent(IN)  :: w

  !----- time step [s] -----
  real(8), intent(IN)  :: dt
  
  !----- entrainment rate [%/km] -----
  real(8), intent(IN)  :: entrainment_rate

  !----- maximum level for lifting -----
  real(8), intent(IN)  :: maxlev

  integer :: k, km, kmark1, kmark2, kpar, t, count
  integer :: kk, kk_lcl, kk_el, kk_lfc
  integer, parameter :: max_ite= 30

  real(8) :: cape, cin, lcl, lfc, el
  real(8) :: Ph2o, Ph2osatu, qv_satu
  real(8) :: lapse, power
  real(8) :: entrate_lamda, entrate_epsilon, zpar, psi
  real(8) :: del_pres_e_int, tem_e_int, rh_e_int, qv_e_int
  real(8) :: rho_e_int, vtem_e_int, Rm_e_int, Cvm_e_int, Cpm_e_int
  real(8) :: pres_e_int0, tem0, rho0, qv0, ql0
  real(8) :: pres_e_int1, tem1, rho1, qv1, ql1
  real(8) :: Rm, Cvm, Cpm, Le, vtem, pres_correct
  real(8) :: evap, evap_tmp, evap_max, evap_min, zpar0, fl
  real(8) :: del_z, int_z, z_ratio, buoy_accum_min

  real(8), allocatable :: pres_e(:), hgt_e(:), tem_e(:), rh_e(:)
  real(8), allocatable :: qv_e(:), rho_e(:), vtem_e(:)
  real(8), allocatable :: Rm_e(:), Cvm_e(:), Cpm_e(:)
  real(8), allocatable :: pres_e_diff(:)
  real(8), allocatable :: buoy(:), zrel(:), zrel_vtem(:), buoy_accum(:)

  real(8), parameter :: pres_ref= 1000.d2
  real(8), parameter :: Ra= 287.04d0, Rv= 461.4d0
  real(8), parameter :: Cva= 719.d0, Cvv= 1418.d0, Cl= 4216.d0
  real(8), parameter :: Ttrip= 273.16d0, E0v= 2.374d6
  real(8), parameter :: e= Ra/Rv, g= 9.80665d0
  real(8), parameter :: &
       &coef1= 53.67957d0, coef2= -6743.769d0  , coef3= -4.8451d0
  real(8), parameter :: eps= 1.d-9

  zpar= dble(hgt_p)
  tem0= dble(tem_p)
  qv0 = dble(qv_p)

  if (zpar < dble(hgt_e_input(1))) then

     km= km_input+1

     allocate(pres_e(km))
     allocate(hgt_e(km))
     allocate(tem_e(km))
     allocate(rh_e(km))
     allocate(qv_e(km))
     allocate(rho_e(km))
     allocate(vtem_e(km))
     allocate(Rm_e(km))
     allocate(Cvm_e(km))
     allocate(Cpm_e(km))
     allocate(pres_e_diff(km))

     do k= 1, km_input
     
        pres_e(k+1)= dble(pres_e_input(k))
        hgt_e(k+1) = dble(hgt_e_input(k))
        tem_e(k+1) = dble(tem_e_input(k))
        rh_e(k+1)  = dble(rh_e_input(k))

     end do

     hgt_e(1)= zpar
     tem_e(1)= tem_e(2)-(hgt_e(2)-zpar)&
          &            *(tem_e(3)-tem_e(2))&
          &            /(hgt_e(3)-hgt_e(2))
     rh_e(1) = rh_e(2)

     Ph2osatu = dexp(coef1+coef2/tem_e(2)+coef3*dlog(tem_e(2))) ![hPa]
     Ph2o     = Ph2osatu*rh_e(2)/100.d0 ![hPa]
     qv_e(2)  = e*Ph2o/(pres_e(2)-Ph2o)
     vtem_e(2)= tem_e(2)*(1.d0+qv_e(2)/e)/(1.d0+qv_e(2))

     Ph2osatu = dexp(coef1+coef2/tem_e(3)+coef3*dlog(tem_e(3))) ![hPa]
     Ph2o     = Ph2osatu*rh_e(3)/100.d0 ![hPa]
     qv_e(3)  = e*Ph2o/(pres_e(3)-Ph2o)
     vtem_e(3)= tem_e(3)*(1.d0+qv_e(3)/e)/(1.d0+qv_e(3))

     lapse    = (vtem_e(2)-vtem_e(3))/(hgt_e(3)-hgt_e(2))
     power    = g/(Ra*lapse)
     pres_e(1)= pres_e(2)*((1.d0+lapse*(hgt_e(2)-zpar)/vtem_e(2))**power)

  else

     km= km_input

     allocate(pres_e(km))
     allocate(hgt_e(km))
     allocate(tem_e(km))
     allocate(rh_e(km))
     allocate(qv_e(km))
     allocate(rho_e(km))
     allocate(vtem_e(km))
     allocate(Rm_e(km))
     allocate(Cvm_e(km))
     allocate(Cpm_e(km))
     allocate(pres_e_diff(km))

     do k= 1, km_input
     
        pres_e(k)= dble(pres_e_input(k))
        hgt_e(k) = dble(hgt_e_input(k))
        tem_e(k) = dble(tem_e_input(k))
        rh_e(k)  = dble(rh_e_input(k))

     end do

  end if

!  PRINT '(A)','ENVIRONMENTAL PROFILE'
  do k= 1, km

     Ph2osatu = dexp(coef1+coef2/tem_e(k)+coef3*dlog(tem_e(k))) ![hPa]
     Ph2o     = Ph2osatu*rh_e(k)/100.d0 ![hPa]
     qv_e(k)  = e*Ph2o/(pres_e(k)-Ph2o)
     Rm_e(k)  = Ra+qv_e(k)*(Rv-Ra)
     Cvm_e(k) = Cva+qv_e(k)*(Cvv-Cva)
     Cpm_e(k) = Rm_e(k)+Cvm_e(k)
     rho_e(k) = (100.d0*pres_e(k))/(Rm_e(k)*tem_e(k))
     vtem_e(k)= tem_e(k)*(1.d0+qv_e(k)/e)/(1.d0+qv_e(k))

  end do

  pres_e= 100.d0*pres_e

  do k= 2, km

     lapse= (vtem_e(k-1)-vtem_e(k))/(hgt_e(k)-hgt_e(k-1))
     power= g/(Ra*lapse)
     pres_e_diff(k)= pres_e(k-1)*((vtem_e(k)/vtem_e(k-1))**power)&
          &         -pres_e(k)
     
  end do
  
  do k= 1, km-1
     if (zpar >= hgt_e(k) .and. zpar < hgt_e(k+1)) then
        kpar= k
        exit
     end if
  end do

  del_z= zpar-hgt_e(kpar)
  int_z= hgt_e(kpar+1)-hgt_e(kpar)

  z_ratio= del_z/int_z

  pres_correct= pres_e_diff(kpar+1)*z_ratio
     
  lapse= (vtem_e(kpar)-vtem_e(kpar+1))/int_z
  power= g/(Ra*lapse)
     
  pres_e_int0= pres_e(kpar)*((1.d0-lapse*del_z/vtem_e(kpar))**power)&
       &      -pres_correct

  Ph2osatu= dexp(coef1+coef2/tem0+coef3*dlog(tem0))
  qv_satu = e*Ph2osatu/(pres_e_int0/100.d0-Ph2osatu)

  if (qv0 > qv_satu) qv0= qv_satu

  Rm  = Ra+qv0*(Rv-Ra)

  rho0= pres_e_int0/(Rm*tem0)

  zpar0= zpar

  allocate(buoy      (2*int((maxlev-zpar0)/(w*dt))))
  allocate(zrel      (2*int((maxlev-zpar0)/(w*dt))))
  allocate(buoy_accum(2*int((maxlev-zpar0)/(w*dt))))
  
  ql0 = 0.d0
  
  evap= 0.d0

  fl= 0.d0

  kk= 1 !!!!! 0
  count= 0

  entrate_lamda= w*1.d-3*dlog(1.d0+entrainment_rate/100.d0) ![/s]

!  PRINT '(A)','CALCULATE PARCEL PROFILE BY EULER METHOD'
  do

     Rm = Ra+qv0*(Rv-Ra)-ql0*Ra
     Cvm= Cva+qv0*(Cvv-Cva)+ql0*(Cl-Cva)
     Cpm= Rm+Cvm
     Le = (Cvv+Rv-Cl)*(tem0-Ttrip)+E0v+Rv*Ttrip

     do k= 1, km-1
        if (zpar >= hgt_e(k) .and. zpar < hgt_e(k+1)) then
           kpar= k
           exit
        end if
     end do

     del_z= zpar-hgt_e(kpar)
     int_z= hgt_e(kpar+1)-hgt_e(kpar)

     z_ratio= del_z/int_z

     pres_correct= pres_e_diff(kpar+1)*z_ratio
     
     lapse= (vtem_e(kpar)-vtem_e(kpar+1))/int_z
     power= g/(Ra*lapse)
     
     pres_e_int0= pres_e(kpar)*((1.d0-lapse*del_z/vtem_e(kpar))**power)&
          &      -pres_correct

     tem_e_int= tem_e(kpar)+(tem_e(kpar+1)-tem_e(kpar))*z_ratio

     rh_e_int= rh_e(kpar)+(rh_e(kpar+1)-rh_e(kpar))*z_ratio

     Ph2osatu  = dexp(coef1+coef2/tem_e_int+coef3*dlog(tem_e_int)) ![hPa]
     Ph2o      = rh_e_int*Ph2osatu/100.d0 ![hPa]
     qv_e_int  = e*Ph2o/(pres_e_int0/100.d0-Ph2o)
     Rm_e_int  = Ra+qv_e_int*(Rv-Ra)
     Cvm_e_int = Cva+qv_e_int*(Cvv-Cva)
     Cpm_e_int = Rm_e_int+Cvm_e_int
     rho_e_int = pres_e_int0/(Rm_e_int*tem_e_int)
     vtem_e_int= tem_e_int*(1.d0+qv_e_int/e)/(1.d0+qv_e_int)

     vtem= tem0*(1.d0+qv0/e)/(1.d0+qv0)

     kk= kk+1
     
     buoy(kk) = g*(vtem-vtem_e_int)/vtem_e_int
     zrel(kk) = zpar-zpar0

!    COPY VERTICAL PROFILE DATA
     Z_PC(KK-1)=zpar0+zrel(kk)
     P_PC(KK-1)=pres_e_int0
     TVE_PC(KK-1)=vtem_e_int
     TV_PC(KK-1)=vtem
     BUOY_PC(KK-1)=buoy(kk)
     !print '(A,i3,1x,f5.0,2f7.2,f10.4)','kk,z,vtem,vtem_e_int, buoy,=',kk,zpar0+zrel(kk),vtem,vtem_e_int,buoy(kk) !DEBUG

!     PRINT '(A)','MMMMM BUOY'
     if (kk > 1 .and. buoy(kk)*buoy(kk-1) < 0.d0) then

        kk= kk+1

        buoy(kk)= buoy(kk-1)
        zrel(kk)= zrel(kk-1)

        buoy(kk-1)= 0.
        zrel(kk-1)= zrel(kk-2)&
             &     +(zrel(kk)-zrel(kk-2))&
             &     *buoy(kk-2)/(buoy(kk-2)-buoy(kk))

     end if

!     PRINT '(A)','MMMMM LIFTING CONDENSATION LEVEL'
     if (count == 1) then 
        kk_lcl= kk
        lcl   = zrel(kk)+zpar0
     ELSE !!!!
       kk_lcl=1 !!!!!!
     end if
     
     del_pres_e_int= -pres_e_int0*g/(Ra*vtem_e_int)

     entrate_epsilon= rho0*entrate_lamda ![kg/m^3s]
     
     psi=   (1.d0/pres_e_int0)&
          &*(-(Cvm/Cpm)*w*del_pres_e_int&
          &-entrate_epsilon*Rm_e_int*(tem_e_int-tem0)&
          &+(Rm/Cpm)*entrate_epsilon*Cpm_e_int*(tem_e_int-tem0)&
          &+(Rm/Cpm)*0.5d0*entrate_epsilon*(w**2.d0))

     rho1= rho0+( entrate_epsilon*(1.d0-(rho0/rho_e_int))&
          &      -rho0*psi)*dt
     
     qv1 = qv0+(1.d0/rho0)*entrate_epsilon*(qv_e_int-qv0)*dt
     
     ql1 = ql0+(1.d0/rho0)*(-entrate_epsilon*ql0)*dt

     tem1= tem0+(1.d0/(rho0*Cpm))*( w*del_pres_e_int&
          &                        +entrate_epsilon*Cpm_e_int*(tem_e_int-tem0)&
          &                        +0.5d0*entrate_epsilon*(w**2.d0))*dt

     zpar= zpar+w*dt

     do k= 1, km-1
        if (zpar >= hgt_e(k) .and. zpar < hgt_e(k+1)) then
           kpar= k
           exit
        end if
     end do

     del_z= zpar-hgt_e(kpar)
     int_z= hgt_e(kpar+1)-hgt_e(kpar)

     z_ratio= del_z/int_z

     pres_correct= pres_e_diff(kpar+1)*z_ratio
     
     lapse= (vtem_e(kpar)-vtem_e(kpar+1))/int_z
     power= g/(Ra*lapse)
     
     pres_e_int1= pres_e(kpar)*((1.d0-lapse*del_z/vtem_e(kpar))**power)&
          &      -pres_correct
     
     Ph2osatu= dexp(coef1+coef2/tem1+coef3*dlog(tem1)) 

     qv_satu = e*Ph2osatu/(pres_e_int1/100.d0-Ph2osatu)

     if (qv1 > qv_satu) then

        evap    = -rho1*(qv1-qv_satu)/dt

        evap_min= 2.d0*evap
        evap_max= 0.d0

       
        fl= -evap

        do t= 1, max_ite


           psi=   (1.d0/pres_e_int0)&
                &*(-(Cvm/Cpm)*w*del_pres_e_int&
                &+Rv*Tem0*evap&
                &-entrate_epsilon*Rm_e_int*(tem_e_int-tem0)&
                &-(Rm/Cpm)*Le*evap&
                &+(Rm/Cpm)*entrate_epsilon*Cpm_e_int*(tem_e_int-tem0)&
                &+(Rm/Cpm)*0.5d0*entrate_epsilon*(w**2.d0))

           rho1= rho0+(-fl&
                &      +entrate_epsilon*(1.d0-(rho0/rho_e_int))&
                &      -rho0*psi)*dt

           qv1 = qv0+(1.d0/rho0)*( evap&
                &                 +fl*qv0&
                &                 +entrate_epsilon*(qv_e_int-qv0))*dt

           ql1 = ql0+(1.d0/rho0)*(-evap&
                &                 -fl&
                &                 +fl*ql0&
                &                 -entrate_epsilon*ql0)*dt

           tem1= tem0+(1.d0/(rho0*Cpm))*( w*del_pres_e_int&
                &                        -Le*evap&
                &                        +entrate_epsilon*Cpm_e_int&
                &                                        *(tem_e_int-tem0)&
                &                        +0.5d0*entrate_epsilon*(w**2.d0))*dt

           Ph2osatu= dexp(coef1+coef2/tem1+coef3*dlog(tem1)) 

           qv_satu = e*Ph2osatu/(pres_e_int1/100.d0-Ph2osatu)

           if (dabs(qv1/qv_satu-1.d0) < eps) then
              goto 100
           else if (qv1 < qv_satu) then
              evap_tmp= evap
              evap    = 0.5d0*(evap+evap_max)
              evap_min= evap_tmp
           else
              evap_tmp= evap
              evap    = 0.5d0*(evap+evap_min)
              evap_max= evap_tmp
           end if

           fl= -evap

        end do

     end if
100  continue

     if (zpar >= hgt_e(km) .or. zpar >= maxlev) exit
     
     rho0= rho1
     qv0 = qv1
     ql0 = ql1
     tem0= tem1
     
  end do

!  PRINT *,'lcl=',lcl
  lcl_output= real(lcl+1.E-6)
!  PRINT *,'lcl_output=',lcl_output

  do k= kk, kk_lcl, -1
     if (buoy(k) >= 0.d0) then !Equilibrium Level
        kk_el= k
        el   = zrel(k)+zpar0
        goto 200
     end if
  end do

  cape_output= 0.
  cin_output = undef
  lfc_output = undef
  el_output  = undef
  return


200 continue

  el_output= real(el)

  buoy_accum(1)= 0.d0
  do k= 2, kk
     del_z= zrel(k)-zrel(k-1)
     buoy_accum(k)= buoy_accum(k-1)+0.5d0*del_z*(buoy(k-1)+buoy(k))
  end do

  count= 0
  do k= kk_lcl, kk_el
     count= count+1
     if (count == 1) then
        buoy_accum_min= buoy_accum(k)
        kk_lfc= k
     else if (buoy_accum(k) < buoy_accum_min) then
        buoy_accum_min= buoy_accum(k)
        kk_lfc= k
     end if
  end do

  cape= buoy_accum(kk_el)-buoy_accum_min
  cin = -buoy_accum_min
  lfc = zrel(kk_lfc)+zpar0

  cape_output= real(cape)
  cin_output = real(cin)
  lfc_output = real(lfc)

  deallocate(pres_e)
  deallocate(hgt_e)
  deallocate(tem_e)
  deallocate(rh_e)
  deallocate(qv_e)
  deallocate(rho_e)
  deallocate(vtem_e)
  deallocate(Rm_e)
  deallocate(Cvm_e)
  deallocate(Cpm_e)
  deallocate(pres_e_diff)
  deallocate(buoy)
  deallocate(zrel)
  deallocate(buoy_accum)
  
  return
end subroutine calc_CAPE_Romps_and_Kuang_2010_no_fusion_total_fallout

end module entraining_CAPE

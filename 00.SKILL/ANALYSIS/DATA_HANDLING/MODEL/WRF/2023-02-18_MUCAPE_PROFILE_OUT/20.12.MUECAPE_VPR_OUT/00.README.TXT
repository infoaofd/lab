/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F
Mon, 26 Dec 2022 18:23:39 +0900

/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/15.12.ECAPE_TEST
2023-01-15_08-34

/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/15.12.ECAPE/15.12.MUECAPE_TEST
2023-01-30_17-51


/work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/15.12.ECAPE/15.14.MUECAPE_W_DT_10
2023-02-16_16-40

-------- Forwarded Message --------
Subject:	Re: 検討のお願い
Date:	Thu, 9 Feb 2023 12:00:55 +0900
From:	栃本英伍 <e_tochi@mri-jma.go.jp>
To:	Satoshi IIZUKA <iizuka@bosai.go.jp>
CC:	末木 健太 <ksueki@mri-jma.go.jp>


飯塚様
（Cc：末木様）

栃本です。
少々立て込んでおりまして、遅くなってしまい申し訳ありません。

E-CAPEの計算プログラムをお送りします。
module_entraining_CAPE.f90が実際のE-CAPEの計算プログラムになっています。
メインプログラムの参考として、メソ解析のモデル面データを使用したものを添付します。

いくつかパラメータがあります。

wp: パーセルを上昇させる速度
Entrainment_rate: エントレインメント率
Dt: 時間刻み

Sueki and Niino (2016)やTochimoto et al. (2019)では、上昇速度1m/s, 時間刻み1/sですが、
計算が重たいので、それぞれ10倍くらいにしたほうがいいかもしれません。
結果はあまり変わらなかったです。

何か不明点がありましたら、お知らせください。
＊末木さん、補足があればお願いします。

よろしくお願い致します。

# Gribに慣れる

[[_TOC_]]

## Gribとは

大気・海洋のデータ形式の一つ。世界の気象機関のデータのやり取りに使われている。

grib1とgrib2でバージョンが2種類あるが，互換性がなくソフト(アプリ)は個別に作成されている。そのため, **自分がgrib1かgirib2のどちらのデータを使っているのか注意する**必要がある。



## 参考資料

気象データを解く  
http://blog.syo-ko.com/?eid=1228  

eCcodes HOME  
https://confluence.ecmwf.int/display/ECC 

wgrib 使用方法  
http://www.hysk.sakura.ne.jp/Linux_tips/how2use_wgrib  

cdo入門  
https://gitlab.com/infoaofd/lab/-/blob/master/CDO/00.CDO%E5%85%A5%E9%96%80.md

gradsでgribファイルを読む  
https://ccsr.aori.u-tokyo.ac.jp/~obase/grib2.html



## Gribファイルの中身

Gribファイルには，あらかじめ定められた並びにしたがって1バイト単位でデータが収納されている。Gribファイルに関する文書では，1バイトのことを1オクテット (octet)と呼んでいることが多い。オクテットは，8組の意味であるので，8オクテットとは1ビットの情報8組ということである。1バイト=8ビットであるので，1オクテット＝1バイトとなる。

### 中身をのぞいてみる

#### 1バイト単位で表示

試みにgribファイルの中身を1バイト単位で表示させてみる。linuxのodというコマンドを使うと，ファイルに保存されているデータを1バイト単位で表示させることができる。

ここでは，大気再解析データのERA5を例として用いる。ファイルの容量が大きいので，headコマンドを使って，odコマンドの先頭3行のみを表示させている。

```bash
$ cd /work02/DATA/ERA5.KOIKE.LARGE/2014
```

```bash
$ ls -lh ERA5_JAPAN_SFC_03HR_20140214.grib 
-rw-r--r--. 1 am 23M 12月 12 18:42 ERA5_JAPAN_SFC_03HR_20140214.grib
```

```bash
$ od -tx1 -Ax ERA5_JAPAN_SFC_03HR_20140214.grib|head -3 
000000 47 52 49 42 02 a8 1e 01 00 00 34 80 62 91 ff 80
000010 eb 01 00 00 0e 02 0e 00 00 01 00 00 00 00 00 00
000020 15 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

一番左の列はデータのアドレスを表す。アドレスとはファイル上でデータが記憶されているファイル先頭からの位置を，バイト単位で表した数値のことである。

- `-tx1`オプションは，1バイト単位で16進数で表示させるという意味である。

- `-Ax`オプションは，データのアドレスを16進数で表示させるという意味である。

一番左にあるアドレスを表す列を除くと，16列単位でデータが表示されていることが分かる。16進数の一桁は 1, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, Fの15文字で表現する。16進数の10は10進数の16である。このため，一番左のアドレスを表す列は16進数で10単位で増えている。

10進, 2進, 16進数の対応関係を表にまとめておく。



| 10進数 | 2進数 | 16進数 |
| ------ | ----- | ------ |
| 0      | 0000  | 0      |
| 1      | 0001  | 1      |
| 2      | 0010  | 2      |
| 3      | 0011  | 3      |
| 4      | 0100  | 4      |
| 5      | 0101  | 5      |
| 6      | 0110  | 6      |
| 7      | 0111  | 7      |
| 8      | 1000  | 8      |
| 9      | 1001  | 9      |
| 10     | 1010  | A      |
| 11     | 1011  | B      |
| 12     | 1100  | C      |
| 13     | 1101  | D      |
| 14     | 1110  | E      |
| 15     | 1111  | F      |

これを見ると，0000から1111までの4桁の2進数は，1桁の16進数で表現できることが分かる。

同様にして考えると，00000000～11111111の8桁の2進数は，00～FFの2桁の16進数で表現できることが分かる。詳しくは下記を参照のこと。

https://uquest.tktk.co.jp/embedded/learning/lecture01.html  
https://www.yamanjo.net/knowledge/data/data_04.html    

1バイト=8ビットなので，1バイトを意味する00000000～11111111の8桁の2進数は00～FFの2桁の16進数で表すこともできる。このため，バイナリデータを表現する際に，2進数のかわりに16進数が良く用いられる。

さて，odコマンドを使って表示させた上のgribファイルの先頭の4バイトは，16進数で47, 52, 49, 42となっている。

気象データ用のGRIBファイルの国際規約(FM92 GRIB)によると, 先頭4バイトは文字をasciiコードと呼ばれる数字で表したものと決められている。asciiコード表という数字と文字の対応関係を示した表  
 https://www.k-cube.co.jp/wakaba/server/ascii_code.html   
を見ると，47=G, 52=R, 49=I, 42=Bとなっており，このファイルの先頭4バイトはGRIBという文字列を現わしていることになる。

これ以降もデータの仕様書 (データの並べ方について説明した書類)を参照すれば, データを解読することができ，gribファイルを読み込むプログラムを自作することもできる（下記参照）。

#### gribファイルの仕様

FM92 GRIBの解説 (説明が丁寧) 
https://qiita.com/e_toyoda/items/ce7497e1a633b16f1ff1

MSM (メソスケールモデル)    
http://www.jmbsc.or.jp/jp/online/file/f-online10200.html  
https://www.data.jma.go.jp/suishin/shiyou/pdf/no12601

JRA55 (全球再解析)    
http://www.jmbsc.or.jp/jp/offline/reference/format_jra55.pdf



#### gribファイルを読み解いた例

仕様書からデータの構造を読み解き，0から気象庁MSMデータの変換用プログラムを作成した例が下記に記載されている。

気象データを解く  
http://blog.syo-ko.com/?eid=1228

「気象データを解く」に記載された作業を自分で行うことでgribファイルに通暁することができるが，これ以上ここでは深入りしない。もし業務などで必要が生じたら，ここでの学習内容をおさらいしてほしい。

ここではgribファイルになじむことを目的として，少しだけではあるがgribファイルを読むためのプログラムを自作してみる。

下記のプログラムは，gribファイルの一番最初の4オクテット (4バイト)を読み込んで，そのデータを画面に表示させるC言語のプログラムである。

**READ_SECTION0_GRIB.c** 

```c
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, char **argv){

  char infile[100];
  FILE *fp;

  unsigned char buf[4];

  if(argc < 2){
    printf("ERROR: NO INPUT FILE NAME\n");
    exit(1);
  }

  strcpy(infile,argv[1]); ///argv[1]=>infle///

  ///OPEN INPUT FILE///
  fp = fopen(infile,"rb");
  if(fp == NULL){
    fprintf(stderr,"\nCANNOT OPEN %s\n",infile);
    exit(1);
  }else{
  fprintf(stderr,"\nOPEN %s\n",infile);
  }

  fread( buf, 1, 4, fp );
  buf[4] = '\0';
  
  printf("\nbuf = %s\n",buf);

  fclose(fp);

}
```

```bash
$ gcc -o READ_SECTION0_GRIB.EXE READ_SECTION0_GRIB.c 
```

```bash
$ READ_SECTION0_GRIB.EXE /work02/DATA/ERA5.KOIKE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib
```

```bash
OPEN /work02/DATA/ERA5.KOIKE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib

buf = GRIB
```



## Grib用のコマンド

以降はLinuxコマンドとして提供されているgrib用アプリを活用する。

### grib_dump

`grib_dump`はヨーロッパ中期予報センター(ECMWF)で作成された，gribファイルの内容を表示するアプリである。netCDF用のncdumpと同様のことができる。

```bash
$ /usr/local/eccodes-2.21.0/bin/grib_dump  /work02/DATA/ERA5.K
OIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib |head
```

```bash
***** FILE: /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib 
#==============   MESSAGE 1 ( length=174110 )              ==============
GRIB {
  editionNumber = 1;
  table2Version = 128;
  # European Centre for Medium-Range Weather Forecasts (common/c-1.table)  
  centre = 98;
  generatingProcessIdentifier = 145;
  # Skin temperature  (K)  (grib1/2.98.128.table)  
  indicatorOfParameter = 235;
```

このソフトは，eCcodesと呼ばれるライブラリの一部として提供されている。

```bash
$ ls /usr/local/eccodes-2.21.0
```

```bash
bin/  include/  lib/  lib64/  share/
```

```bash
$ ls /usr/local/eccodes-2.21.0/bin/grib_dump -lh
```

```bash
-rwxr-xr-x. 1 root 254K  5月 12  2021 /usr/local/eccodes-2.21.0/bin/grib_dump*
```

eCcodesに関するマニュアル等は下記のサイトから取得できる  
https://confluence.ecmwf.int/display/ECC  
(アクセスに時間がかかることがあるので注意)

grib_dumpを頻繁に使う場合，$HOME/.bashrcに下記の1行を加えておくとよい。

```bash
export PATH=$PATH:/usr/local/eccodes-2.21.0/bin/
```



### wgrib

米国海洋大気庁 (NOAA)で作成されたgrib用のアプリである。

オプション無しで実行すると，変数の名称や，日付，高度などデータの概要が表示される。

```
$ wgrib /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib|head -3
```

```
Undefined parameter table (center 98-0 table 235), using NCEP-opn
1:0:d=14021400:SKT:kpds5=235:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
2:174120:d=14021400:SSTK:kpds5=34:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
3:270720:d=14021400:10U:kpds5=165:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
```

d=14021400 (2014年02月14日00時)

SKT: 表皮温度 (surface skin temperature)

type=analysis (データの種類は再解析(予報値ではない))



#### wgribの使用例

##### バイナリファイルへの変換

```bash
$ wgrib INFLE | grep ":MSL:" | wgrib INFLE -i -grib -o OFLE
```

入力ファイルINFLEに含まれるMSLという名称の変数を，プレーンバイナリファイルとして，OFLEに書き出す。プレーンバイナリファイルなので，Fortranで読み書きできる。また，適切なgrads用コントロール(CTL)ファイル を作成すれば，出力ファイルをgradsで作図することができる。

**使用例**

入力ファイルと出力ファイルの名前を変数に代入しておく。

```bash
$ INFLE=/work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR
_20140214.grib
```

```bash
$ OFLE=MSL.bin
```

```bash
$ echo $INFLE; echo $OFLE
/work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib
MSL.bin
```

wgribでデータを変換

```bash
$ wgrib $INFLE | grep ":MSL:" | wgrib $INFLE -i -grib -o $OFLE
```

```
Undefined parameter table (center 98-0 table 235), using NCEP-opn
11:1663680:d=14021400:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
28:4633200:d=14021403:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
45:7602720:d=14021406:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
62:10572240:d=14021409:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
79:13541760:d=14021412:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
96:16511280:d=14021415:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
113:19480800:d=14021418:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
130:22450320:d=14021421:MSL:kpds5=151:kpds6=1:kpds7=0:TR=0:P1=0:P2=0:TimeU=1:sfc:anl:type=analysis:NAve=0
```

出力ファイルの確認

```bash
$ ll $OFLE
-rw-r--r--. 1 am 1.4M 2022-12-14 14:49 MSL.bin
```

wgribを使うとさらに色々なことができる。詳しくは下記のサイトを参照のこと。  
http://www.hysk.sakura.ne.jp/Linux_tips/how2use_wgrib  



#### 注意

**girb2**形式のファイルにはwgrib2を用いる。**wgribとwgrib2は**名前は似ているが**全く別のアプリ**で互換性はない。　　

wgrib2の使用法については下記を参照のこと。  
http://www.hysk.sakura.ne.jp/Linux_tips/how2use_wgrib2  



### cdo

独国Max-plank研究所で作成されたアプリである。多彩な機能を有し操作が容易であることから，使用者が増えている。

- データ形式の変換
- データの内挿
- 簡単なデータ解析

が行える。**cdoはgrib, grib2, netCDFの各ファイル形式に対応している**。

**注**: 気象庁のgrib2ファイルに関してはファイルの一番最初に記憶されている変数しか認識しないという問題があるようである。確認に使用したcdoとその関連ライブラリのバージョンは下記の通り

```
cdo version 1.9.10 
CDI library version : 1.9.10
cgribex library version : 1.9.5
ecCodes library version : 2.21.0
NetCDF library version : 4.8.0 of Apr 27 2021 14:47:11 $
hdf5 library version : 1.8.22
exse library version : 1.4.2
FILE library version : 1.9.1
```



cdoの使用例を下記に示す。

#### 変数名の表示

```bash
$ cdo showname /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_S
FC_03HR_20140214.grib
```

```
 var235 var34 var165 var166 var168 var167 var218 var239 var219 var240 var151 var34 var38 var37 var33 var141 var144
cdo    showname: Processed 17 variables [0.00s 8340KB].
```

ERA5データを入力データとして使った場合，cdoは変数の識別番号のみを表示する (varの後の数字が識別番号である)。ERA5データの変数の識別番号と変数名の対応については，下記を参照のこと。  
https://apps.ecmwf.int/codes/grib/param-db


#### 時刻の表示

```BASH
$ cdo showtime /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_S
FC_03HR_20140214.grib
```

```
 00:00:00 03:00:00 06:00:00 09:00:00 12:00:00 15:00:00 18:00:00 21:00:00
cdo    showtime: Processed 17 variables over 8 timesteps [0.01s 8340KB].
```

#### ファイルの概要表示

```bash
$ cdo sinfo /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_
03HR_20140214.grib
```

```bash
   File format : GRIB
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 235.128       
     2 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 34.128        
     3 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 165.128       
     4 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 166.128       
     5 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 168.128       
     6 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 167.128       
     7 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 218.228       
     8 : ECMWF    unknown  v accum         1   1     87001   1  P16  : 239.128       
     9 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 219.228       
    10 : ECMWF    unknown  v accum         1   1     87001   1  P16  : 240.128       
    11 : ECMWF    unknown  v instant       1   1     87001   1  P16  : 151.128       
    12 : ECMWF    unknown  v avg           1   1     87001   1  P16  : 34.235        
    13 : ECMWF    unknown  v avg           1   1     87001   1  P16  : 38.235        
    14 : ECMWF    unknown  v avg           1   1     87001   1  P16  : 37.235        
    15 : ECMWF    unknown  v avg           1   1     87001   1  P16  : 33.235        
    16 : ECMWF    unknown  v instant       1   1     87001   1  P24  : 141.128       
    17 : ECMWF    unknown  v accum         1   1     87001   1  P16  : 144.128       
   Grid coordinates :
     1 : lonlat                   : points=87001 (361x241)
                              lon : 90 to 180 by 0.25 degrees_east
                              lat : 80 to 20 by -0.25 degrees_north
   Vertical coordinates :
     1 : surface                  : levels=1
   Time coordinate :  unlimited steps
     RefTime =  2014-02-14 00:00:00  Units = hours  Calendar = proleptic_gregorian
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  2014-02-14 00:00:00  2014-02-14 03:00:00  2014-02-14 06:00:00  2014-02-14 09:00:00
  2014-02-14 12:00:00  2014-02-14 15:00:00  2014-02-14 18:00:00  2014-02-14 21:00:00
cdo    sinfo: Processed 17 variables over 8 timesteps [0.05s 8340KB].
```

#### gribをnetCDFに変換

変換したいgribファイル

```bash
$ ls -lh /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib
```

```bash
-rw-rw-r--. 1 koike 23M 12月 12 18:42 /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib
```

変換

```BASH
$ cdo -f nc4 copy /work02/DATA/ERA5.KOIKE.LARGE/2014/ERA5_JAPAN_SFC_03HR_20140214.grib E
RA5_JAPAN_SFC_03HR_20140214.nc
```

変換後のnetCDFファイル

```BASH
$ ll ERA5_JAPAN_SFC_03HR_20140214.nc 
-rw-r--r--. 1 am 46M 2022-12-14 23:39 ERA5_JAPAN_SFC_03HR_20140214.nc
```

**netCDFに変換すると**，**ファイル容量が倍程度増えている**のに注意。

さらに詳しい使用法に関しては，下記を参照のこと。

CDO入門  
https://gitlab.com/infoaofd/lab/-/blob/master/CDO/00.CDO%E5%85%A5%E9%96%80.md  



### NCL

NCAR Command languge (NCL)は，NCAR (アメリカ大気研究センター)で作成されているデータ解析用のプログラミング言語である(現在開発が止まり，pythonへの移行がすすんでいる)。

高品質の図を作成できるだけでなく，データ解析のための関数が数多く準備されており，本格的な研究に使用されている。NCLはGribファイルを簡単に読み込むことができる。NCLもCDO同様, **netCDF, grib1, grib2の各ファイル形式に対応している**。今のところNCLを使ったデータの読み込みで問題が生じたことは無い。NCLはgribファイルの読み込みはできるが，書き込みができない。

次のスクリプトを使うと，データが収納されている配列名や配列の次元が画面に書き出される。

CHK.ERA5.ncl

```
INDIR="/work02/DATA/ERA5.KOIKE/2014"
INFLE="ERA5_JAPAN_SFC_03HR_20140214.grib"
IN=INDIR+"/"+INFLE

a=addfile(IN,"r")

print(a)
```

```
$ ncl CHK.ERA5.ncl 
```

注意：ERA5データの変数名には誤りがあるので注意する。ダウンロードした際に指定した変数とスクリプトの出力結果を比較すると誤りかどうかは割とすぐわかる。



## gribをGrADSで読む

ファイルの変換をせずに，gribファイルをGrADSで読むことが出来る。変換によって別途ファイルが作成されないため，ディスク容量を節約できる。また，プレーンバイナリより高速で読み込みが行われる（多くの場合netCDFよりも速い）。

手順は,

1. CTLファイルの雛形作成 (grib2ctl.plかg2ctlを使う)
2. マップファイルの作成 (gribmapを使う)
3. 必要に応じてCTLファイルの修正 (手作業)

となる。

詳細は下記参照のこと

https://ccsr.aori.u-tokyo.ac.jp/~obase/grib2.html  
https://ccsr.aori.u-tokyo.ac.jp/~kimoto/index.files/gribgrads.pdf

**注意**：gradsのCTLファイルのtemplateやyrevなどを自分で書き加える必要がある場合がある。



## データ処理の手順

### 作図のみ

単に作図するだけなら，ファイル形式を変換する必要はない。GrADSかNCLで直接作図できる。

GrADSを用いる場合は，grib2ctl.plとgribmapを使用する。

pythonもgribファイルの読み書き用のライブラリが用意されているのでファイル形式の変換は不要である(ここでは解説しない)。

### 解析

#### 簡単な解析

平均を取るなどの簡単な解析であれば，GrADSかCDOを用いればよい。GrADSは計算に時間を要する場合があるので，その場合CDOを用いる。

#### 詳しい解析

NCL, FORTRANもしくはpythonやC言語を用いる。

NCLは解析用の関数がいくつも用意されているので，活用できないかまず検討してみる。下記を参照のこと。  
http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php  
https://sites.google.com/site/tips4ncl/    
https://www.ncl.ucar.edu/Document/Functions/list_type.shtml  
https://gitlab.com/infoaofd/lab/-/blob/master/NCL/NCL_QUICK_REF.md


NCLでも難しい場合やNCLでは時間が掛かりすぎる場合，FORTRANを使用する。NCLの関数の使用法がどうしても分からない場合もFORTRANを使用する（使用法を調べるのに四苦八苦するよりも往々にして作業時間が短くなる場合がある）。

FORTRANを使用する場合，まずgribファイルをnetCDFかプレーンバイナリに変換する。

### デバッグ

スクリプトの誤りのことをバグ (bug)といい，バグを直すことをデバッグ (debug)という。GrADSやCDOを用いた場合，誤操作の原因を見つける方法が非常に限定されるので，計算結果に問題があっても，スクリプトの修正が困難になる場合がある。

GrADSのスクリプトを修正する場合，エラーメッセージを見ながら，途中経過を作図することで，誤りの原因を特定していく。デバッグの例については下記を参照のこと。   
https://gitlab.com/infoaofd/lab/-/blob/master/DEGBUG/BASICS/22.12.GRADS_DEBUG.md

修正が困難な場合，NCLやpythonのスクリプト, もしくはFORTRANのプログラムを新規に作った方が作業時間を短縮できることが多い。  

その他のデバッグの手法や例については，下記に追記していく。

https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG


#!/bin/sh

#
# レーダーの10分雨量から1時間雨量を計算する(前1時間雨量ではないので注意)
#

rdir=/work01/DATA/JMA_RADAR_10m
nx=2560; ny=3360
x0=118.006250; y0=20.004167
dx=0.012500; dy=0.008333



if [ $# -ne 2 ]; then
  echo ERROR in $0: Wrong argument.
  echo
  echo Usage: $0 yyyymmdd hh
  echo
  exit 1
fi

yyyymmdd_i=$1
hh_i=$2

ss="00"

if [ $hh_i -lt 0 -o $hh_i -gt 23 ]; then
  echo
  echo ERROR in $0
  echo "hh must be 0 <= hh <= 23."
  echo
  exit 1
fi

yyyymmdda[0]=${yyyymmdd_i}
hha[0]=$(printf %02d $(expr ${hh_i}) )
mia[0]="10"

yyyymmdda[1]=${yyyymmdd_i}
hha[1]=${hha[0]}; mia[1]="20"

yyyymmdda[2]=${yyyymmdd_i}
hha[2]=${hha[0]}; mia[2]="30"

yyyymmdda[3]=${yyyymmdd_i}
hha[3]=${hha[0]}; mia[3]="40"

yyyymmdda[4]=${yyyymmdd_i}
hha[4]=${hha[0]}; mia[4]="50"

yyyymmdda[5]=${yyyymmdd_i}
hha[5]=$(printf %02d $(expr ${hh_i} + 1)); mia[5]="00"

if [ $hh_i = "23" ]; then
  yyyymmdda[5]=$(date -d"${yyyymmdd_i}"+1days '+%Y%m%d')
  hha[5]="00"; mia[5]="00"
fi

outdir="Fig"
mkdir -vp $outdir

itmdir="intm.data"
mkdir -vp $itmdir

dirs="$rdir $itmdir $outdir"

prefix="Z__C_RJTD_"
postfix="_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin"


cwd=$(pwd)
i=0
while [ $i -le 5 ]; do

yyyymmdd=${yyyymmdda[$i]}
    yyyy=${yyyymmdd:0:4}
      mm=${yyyymmdd:4:2}
      dd=${yyyymmdd:6:2}
      hh=${hha[$i]}
      mi=${mia[$i]}
echo "$yyyy $mm $dd $hh $mi"

indir=$rdir"/"$yyyy"/"$mm"/"$dd

if [ ! -d $indir ]; then
  echo
  echo Error in $0 : No such directory, $indir
  echo
  exit 1
fi

yyyymmddhhmiss=${yyyy}${mm}${dd}${hh}${mi}${ss}
input[$i]=${indir}/${prefix}${yyyymmddhhmiss}${postfix}

if [ ! -f ${input[$i]} ]; then

for f in ${indir}/${prefix}${yyyymmddhhmiss}_RDR_JMAGPV__grib2.tar; do

  echo
  echo "Going to ${indir} ..."
  echo
  cd ${indir}
  tar -xvf  ${f}
  if [ $? -ne 0 ]; then
     echo
     echo ERROR in $0 while running tar command.
     echo tar file: ${f}
     echo
     echo "Going back to ${cwd} ..."
     echo
     cd   ${cwd}
     echo "ABNORMAL END"
     echo
     exit 1
  fi
done

echo
echo "Going back to ${cwd} ..."
echo
cd   ${cwd}

fi

if [ ! -f ${input[$i]} ]; then
  echo
  echo Error in $0 : No such file, ${input[$i]}
  echo
  exit 1
fi

intmfile[$i]=${itmdir}/${yyyy}${mm}${dd}_${hh}${mi}${sc}_radar10m.bin

command="./jmaradar2bin ${input[$i]}   ${intmfile[$i]}"

echo mmmmmmmmmmmmmmmmmmmm; echo $command; echo mmmmmmmmmmmmmmmmmmmm
$command
if [ $? -ne 0 ]; then
  echo
  echo "Error in $0 while running the following:"
  echo "  $command"; echo; exit 1
fi
echo; echo; echo; 
i=$(expr $i + 1)
done #i



i=0
echo
while [ $i -le 5 ]; do
echo "Input file: ${input[$i]}"
i=$(expr $i + 1)
done #i

i=0
echo
while [ $i -le 5 ]; do
echo "Intermidiate file: ${intmfile[$i]}"
i=$(expr $i + 1)
done #i
echo



exe=10min_to_1hour
if [ ! -f $exe ]; then
  echo
  echo ERROR in $0: No such file, $exe
  echo
  exit 1
fi
namelist="namelist.${exe}.txt"

outdir="output"
mkdir -vp $outdir

output="${outdir}/${yyyymmdd_i}_${hh_i}.bin"
cat <<EOF >$namelist
&para
im=${nx},
jm=${ny},
input0="${intmfile[0]}",
input1="${intmfile[1]}"
input2="${intmfile[2]}"
input3="${intmfile[3]}"
input4="${intmfile[4]}"
input5="${intmfile[5]}"
output="${output}"
&end
EOF

echo MMMMMMMMMMMMMMMMM; echo $namelist; echo MMMMMMMMMMMMMMMMM
cat $namelist; echo; echo ; echo

command="$exe < $namelist"
echo
echo $command
echo
$exe < $namelist
if [ $? -ne 0 ]; then
  echo
  echo "Error in $0 while running the following:"
  echo "  $command"
  echo
  exit 1
fi



yyyy=${yyyymmdd_i:0:4}
  mm=${yyyymmdd_i:4:2}
  dd=${yyyymmdd_i:6:2}
  hh=${hh_i}

if [ $mm = "01" ]; then mmm="Jan"; fi
if [ $mm = "02" ]; then mmm="Feb"; fi
if [ $mm = "03" ]; then mmm="Mar"; fi
if [ $mm = "04" ]; then mmm="Apr"; fi
if [ $mm = "05" ]; then mmm="May"; fi
if [ $mm = "06" ]; then mmm="Jun"; fi
if [ $mm = "07" ]; then mmm="Jul"; fi
if [ $mm = "08" ]; then mmm="Aug"; fi
if [ $mm = "09" ]; then mmm="Sep"; fi
if [ $mm = "10" ]; then mmm="Oct"; fi
if [ $mm = "11" ]; then mmm="Nov"; fi
if [ $mm = "12" ]; then mmm="Dec"; fi

ctl=r1h.ctl
cat <<EOF>$ctl
dset ${outdir}/%y%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef ${nx} LINEAR ${x0} ${dx}
ydef ${ny} LINEAR ${y0} ${dy}
zdef 1  LEVELS 1000
tdef 8760  LINEAR ${hh}:00Z${dd}${mmm}${yyyy} 1hr
vars 1
rr 0 0 rainfall
endvars
EOF

echo
echo "Output time: ${yyyymmdd_i} ${hh_i}"
echo
echo
echo GrADS control file, $ctl
echo
cat $ctl
echo

exit 0

#!/bin/bash

#
#3時間雨量のプロット
#
gs=$(basename $0 .sh).gs

yyyy=2022; mm=06; dd=19;   dp=$(expr $dd + 1)

hs=00; dh=3

figfile=$(basename $0 .sh)_${yyyy}${mm}${dd}.pdf #.eps


ctl=$(basename $0 .sh).CTL
cat <<EOF>$ctl
# $(date -R)
# $0
dset output/%y4%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 1000  LINEAR 00:00Z01Jun2022 1hr
vars 1
rr 0 0 rainfall
endvars
EOF



HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

lonw=126; lats=29; lone=133; latn=34.5

cat <<EOF>$gs
'cc'

'open ${ctl}'


yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif



'set lon '${lonw}' '${lone}; 'set lat '${lats}' '${latn}
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->yellow->orange->red->darkmagenta'
clevs='1 10 20 50 100 150 200 300'



xmax = 6; ymax = 2

xwid = 9.0/xmax; ywid = 5.5/ymax

nmap = 1; ymap = 1

hh=${hs}

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)


hp=hh+3
say
datetime1=hh'Z'${dd}''mmm''yyyy
datetime2=hp'Z'${dd}''mmm''yyyy

if (hh = 21)
datetime1=21'Z'${dd}''mmm''yyyy
datetime2=00'Z'${dp}''mmm''yyyy
endif

'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say dtcheck' 't1

'set time 'datetime2
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t2=subwrd(line,9)
say dtcheck' 't2

xs = 0.8 + (xwid+0.10)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.20)*(ymap-1); ys = ye - ywid

if (ymap = ymax)
'set xlopts 1 2 0.14'
else
'set xlopts 1 2 0.0'
'set xlopts 1 2 0.14'
endif
if (xmap = 1)
'set ylopts 1 2 0.14'
else
'set ylopts 1 2 0.0'
endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='125 127 129 131'
'set xlevs 'xlevs
'set ylint 1'



'color -levs ' clevs ' -gxout shaded -kind ' kind



r3h='sum(rr,t=' t1 ',t=' t2 ')'
'd 'r3h
#'draw shp JPN_adm1'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.15
'set string 1 l 2 0'; 'set strsiz 0.12 0.15'
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


title=hh'-'hp'UTC${dd}'mmm''yyyy
'draw string 'xx' 'yy' 'title

'set parea off'; 'set vpage off'
*
if (nmap = 22); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+${dh}

endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color -levs ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 0.5 0.7 -edge triangle -line on -ft 2' 

# Header
'set strsiz 0.1 0.12'
'set string 1 l 2'
'draw string 0.2 8.4 ${NOW}'; 'draw string 0.2 8.2 ${HOST}'
'draw string 0.2 8.0 ${CWD}'; 'draw string 0.2 7.8 ${CMD}'

'gxprint ${figfile}'

quit
EOF

grads -bcl "${gs}"
rm -vf $gs

if [ -f ${figfile} ]; then
echo; echo FIG: ${figfile};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${figfile}.;echo; exit 1
fi

exit 0

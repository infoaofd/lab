#!/bin/bash

exe=$(basename $0 .ALL.sh).sh

if [ ! -f $exe ]; then
  echo ERROR in $0 : No such file, $exe
  exit 1
fi

yyyymm=202306; sdate=1; edate=1

cd 10min.to.1hr
pwd
source /opt/intel/oneapi/setvars.sh
make
if [ -f 10min_to_1hour ];then
cp -v 10min_to_1hour ..
cd ..
else
echo ERROR CANNOT COMPILE 10min_to_1hour.;cd ..;exit 1
fi
pwd

d=$sdate

while [ $d -le $edate ]; do
  dd=$(printf %02d $d)
  h=0
  while [ $h -le 23 ]; do
    hh=$(printf %02d $h)

    $exe  $yyyymm$dd $hh

    h=$(expr $h + 1)
  done

  d=$(expr $d + 1)
done

exit 0

#!/bin/sh

exe=10min_to_1hour

im=960
jm=1200

timelist="
2012061502 \
2012061503 \
2012061504 \
2012061505 \
2012061506 \
2012061507 \
2012061508 \
2012061509 \
2012061510 \
2012061511 \
2012061512 \
2012061513 \
"
indir=../output.bin
outdir="output"
mkdir -vp $outdir

namelist=$exe.namelist

for time in $timelist; do
  tt=$(expr ${time:8:2})

  if [ $tt -lt 10 ]; then
    tt2=$(expr ${time:9:1} - 1 )
    time1=${time:0:8}0$(printf %01d $tt2)
  elif [ $tt -ge 10 ]; then
    tt2=$(expr ${time:8:2} - 1 )
    time1=${time:0:8}$(printf %02d $tt2)
  fi

#  echo $time1
  i=0
  while [ $i -le 4 ]; do
    iout=$(expr $i + 1 )
    input[$i]=$time1$(printf %01d $iout)0

    i=$(expr $i + 1)
  done
  input[5]=${time}00

cat <<EOF>$namelist
&para
im=${im},
jm=${jm},
EOF
  i=0
  while [ $i -le 5 ]; do
    echo  input${i}=\"${indir}/${input[$i]}.bin\" >> $namelist
    i=$(expr $i + 1)
  done
  output=${outdir}/${time}.1hrr.grd
  echo output=\"$output\" >> $namelist
  echo "&end" >> $namelist

  $exe < $namelist

done

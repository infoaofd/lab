# ヒストグラム用データ作成_RADAR_AMADAS

/work03/am/2022.MESHIMA.WV/32.12.RAIN/32.12.R1H_HISTOGRAM_DAILY/12.14.MAKE.HISTO.DATA.2

[[_TOC_]]

## やりたいことをはっきりさせる

### 目的をはっきりさせる

今の場合やりたいことは，下記の通り。

指定された領域で1時間降水量の99.9パーセンタイル値（上位0.1%の値）を抽出する



### 作業内容をはっきりさせる

目的をかなえるために必要な作業は以下の通り

- 指定した領域における1時間当毎の格子データを24時間分まとめた配列を作る
- 大きい順に並べ替える
- 上位5%の値を抜き出す

文字で分かりにくいところは，作業内容を示す模式図を作る。（今は割愛）



## 雛形となるプログラムの内容把握

### 34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL

#### 緯度に関するデータの反転

```
lat=lat_in(::-1)
r1h=r1h_in(:,::-1,:)
```

北→南を南→北に反転する



```
LAT1  = ind(lat.ge.ALATS.and.lat.lt.ALATN)      
LON1  = ind(lon.ge.ALONW.and.lon.le.ALONE)   
```

括弧内で指定した条件を満たす配列要素番号をLAT1, LON1に書き込む

### テストしてみる

```
$ cp 34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL TEMP.NCL
`34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL' -> `TEMP.NCL'
```

TEMP.NCL

```bash
LAT1  = ind(lat.ge.ALATS.and.lat.lt.ALATN) 
LON1  = ind(lon.ge.ALONW.and.lon.le.ALONE) 

print(LON1)
print(LAT1)
```

LON1とLAT1の値を書き出してみる

同様の作業を繰り返してプログラムの動作を把握する。



```
$ cp 32.RADAR-AMEDAS.R1H_MAKE.HISTO.sh 36.SELECT_95PCNT.sh
`32.RADAR-AMEDAS.R1H_MAKE.HISTO.sh' -> `36.SELECT_95PCNT.sh'
```



## プログラムの理解の仕方

プログラムを**印刷**して，繰り返し読む

上記の動作チェックを繰り返しながら，プログラムの動作を把握する

細かい場所で分からないところがあった場合，より大きなプログラムのブロックで動作が確認できないか試してみる（自分で準備したテストデータに対して予想した通りの計算結果がでているか確認する）。



## プログラムの書き換え

### バックアップ

書きかえる前のプログラムのバックアップを必ず取る

```bash
$ bak.sh 34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL
'34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL' -> 'BAK_34.RADAR-AMEDAS.R1H_MAKE.HISTO_230714-1517.NCL'
~~~~~~~~~~~~~~~~~~~~~ ORG ~~~~~~~~~~~~~~~~~~~~~
-rw-r--r--. 1 am oc 4.3K Jul 14 15:00 34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

~~~~~~~~~~~~~~~~~~~~~ BAK ~~~~~~~~~~~~~~~~~~~~~
-rw-r--r--. 1 am oc 4.3K Jul 14 15:00 BAK_34.RADAR-AMEDAS.R1H_MAKE.HISTO_230714-1517.NCL
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
```



```bash
/work03/am/2022.MESHIMA.WV/32.12.RAIN/32.12.R1H_HISTOGRAM_DAILY/12.14.MAKE.HISTO.DATA.2
$ mv 34.RADAR-AMEDAS.R1H_MAKE.HISTO.NCL 36.SELECT_95PCNT.NCL
```



### ndtooned

```
r1h_1d=ndtooned(r1h_cut)

printVarSummary(r1h_cut)
print("")
printVarSummary(r1h_1d)
; https://www.ncl.ucar.edu/Document/Functions/Built-in/ndtooned.shtml
exit
```

```
$ 36.SELECT_95PCNT.sh 

Variable: r1h_cut
Type: float
Total Size: 176640 bytes
            44160 values
Number of Dimensions: 3
Dimensions and sizes:   [lat | 40] x [lon | 46] x [TIME | 24]
Coordinates: 
            lat: [33.95..  32]
            lon: [129.25..132.0625]
            TIME: [1594598400..1594681200]
Number Of Attributes: 6
  long_name :   60-minutes precipitation
  units :       1e-3 meter
  _FillValue_original : -32768
  _FillValue :  -32768
  missing_value_original :      -32768
  missing_value :       -32768


Variable: r1h_1d
Type: float
Total Size: 176640 bytes
            44160 values
Number of Dimensions: 1
Dimensions and sizes:   [44160]
Coordinates: 
Number Of Attributes: 1
  _FillValue :  -32768
```



### 36.SELECT_95PCNT.NCL(主要部の抜粋)

```bash
lat_cut=lat(LAT1) ;指定した領域でカット
lon_cut=lon(LON1) ;指定した領域でカット

r1h_1d=ndtooned(r1h_cut) ;多次元配列を1次元配列

r1d_sort=r1h_1d
qsort(r1d_sort)          ;小さい順に並べ替え

dim=dimsizes(r1h_1d)     ;配列の要素数を調べる
n=dim(0)                 ;配列要素数をnに代入する
print(n)
;do i=n-500,n
;print(i+" "+r1d_sort(i))
;end do ;i

n999=toint(n*0.999)        ;上位0.1%の配列要素数の指定。小数点以下切り捨て
print(n999)

print(r1d_sort(n999))     ;上位0.1%の値を画面に書き出す
print(r1d_sort(n-1))        ;最大値を画面に書き出す

r1h999=r1d_sort(n999)
```



### 計算に使用するファイル

36.SELECT_95PCNT.sh

36.SELECT_95PCNT.NCL

; https://gitlab.com/infoaofd/lab/-/blob/master/NCL/NCL_TIPS.md

scriptname_in = getenv("NCL_ARG_1")
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
NML    = getenv("NCL_ARG_2")

INDIR=systemfunc("grep INDIR "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
INFLE=systemfunc("grep INFLE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
 ODIR=systemfunc("grep  ODIR "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
 OFLE=systemfunc("grep  OFLE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLONW=systemfunc("grep ALONW "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLONE=systemfunc("grep ALONE "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLATS=systemfunc("grep ALATS "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
CLATN=systemfunc("grep ALATN "+NML+ "|cut -f2 -d'='|cut -f1 -d','")
DOMAIN=systemfunc("grep DOMAIN "+NML+ "|cut -f2 -d'='|cut -f1 -d','")

ALONW=tofloat(CLONW) ; DOMAIN
ALONE=tofloat(CLONE)
ALATS=tofloat(CLATS)
ALATN=tofloat(CLATN)

INLIST=systemfunc("ls "+INDIR+"/"+INFLE)
a=addfiles(INLIST,"r")

r1h_in = short2flt( a[:]->P60 )
;printVarSummary(r1h_in)

lon = a[0]->lon
lat_in = a[0]->lat
time=a[0]->TIME

lat=lat_in(::-1)
r1h=r1h_in(:,::-1,:)

LAT1  = ind(lat.ge.ALATS.and.lat.lt.ALATN) 
LON1  = ind(lon.ge.ALONW.and.lon.le.ALONE) 
r1h_cut=r1h(lat|LAT1,lon|LON1,TIME|:) ;指定した領域でカット
; https://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Examples/svd

lat_cut=lat(LAT1) ;指定した領域でカット
lon_cut=lon(LON1) ;指定した領域でカット

r1h_1d=ndtooned(r1h_cut) ;多次元配列を1次元配列

r1d_sort=r1h_1d
qsort(r1d_sort)          ;小さい順に並べ替え

dim=dimsizes(r1h_1d)     ;配列の要素数を調べる
n=dim(0)                 ;配列要素数をnに代入する
print(n)
;do i=n-500,n
;print(i+" "+r1d_sort(i))
;end do ;i

n999=toint(n*0.999)        ;上位0.1%の配列要素数の指定。小数点以下切り捨て
print(n999)

print(r1d_sort(n999))     ;上位0.1%の値を画面に書き出す
print(r1d_sort(n-1))        ;最大値を画面に書き出す

r1h999=r1d_sort(n999)

OUT=ODIR+"/"+OFLE

dim=dimsizes(time)
utc_date0 = cd_calendar(time(0), 0)
utc_date1 = cd_calendar(time(dim(0)-1), 0)

y0  = tointeger(utc_date0(:,0))    ; Convert to integer for
m0  = tointeger(utc_date0(:,1))    ; use sprinti 
d0  = tointeger(utc_date0(:,2))
h0  = tointeger(utc_date0(:,3))
date0 = sprinti("%0.4i",y0)+sprinti("%0.2i",m0)+sprinti("%0.2i",d0)+\
sprinti("_%0.2i",h0)
y1  = tointeger(utc_date1(:,0))    ; Convert to integer for
m1  = tointeger(utc_date1(:,1))    ; use sprinti 
d1  = tointeger(utc_date1(:,2))
h1  = tointeger(utc_date1(:,3))
date1 = sprinti("%0.4i",y1)+sprinti("%0.2i",m1)+sprinti("%0.2i",d1)+\
sprinti("_%0.2i",h1)

r1h_1d@input_file=INFLE
r1h_1d@output_file=OUT
r1h_1d@cwd=systemfunc("pwd")
r1h_1d@now=systemfunc("date -R")
r1h_1d@cmd=scriptname
r1h_1d@time=date0+" "+date1
r1h_1d@domin=DOMAIN
r1h_1d@area=ALONW+" "+ALONE+" "+ALATS+" "+ALATN

system("rm -vf "+OUT)
setfileoption("nc","Format","LargeFile")
b=addfile(OUT,"c")

b->r1h999=r1h999



FIGDIR="FIG_R1H_CUT/"+DOMAIN
system("mkdir -vp "+FIGDIR)
FIGFLE=systemfunc("basename "+OFLE+" .nc")
FIG=FIGDIR+"/"+FIGFLE
TYP="pdf"

wks = gsn_open_wks(TYP,FIG)

res = True
res@gsnDraw          = False                ; turn off draw and frame
res@gsnFrame         = False                ; b/c this is an overlay plot

plot = gsn_histogram(wks,ndtooned(r1h_1d),False)

draw(plot)
frame(wks)


print("")
print("MMMMM INPUT:  "+INFLE)
print("MMMMM OUTPUT: "+OUT)
print("MMMMM FIG="+FIG+"."+TYP)
print("MMMMM DOMAIN: "+ALONW+" "+ALONE+" "+ALATS+" "+ALATN)
print("")

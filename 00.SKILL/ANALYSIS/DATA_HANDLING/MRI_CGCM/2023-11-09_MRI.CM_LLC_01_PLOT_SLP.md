# 気象研結合モデルの可視化

GrADSというソフトを使って, 海面気圧の分布を作図する

## 0MAP_SLP_ENS5.sh

### 使用例

時刻指定しない（デフォルト設定）

```
$ 0MAP_SLP_ENS5.sh 

OUTPUT : 
-rw-r--r--. 1 am oc 152K 2023-11-09 15:27 0MAP_SLP_ENS5_PSEA_2016060500.PDF
```

時刻指定する

```
$ 0MAP_SLP_ENS5.sh 2016060612

OUTPUT : 
-rw-r--r--. 1 am oc 143K 2023-11-09 15:28 0MAP_SLP_ENS5_PSEA_2016060612.PDF
```

### スクリプト

```bash
#!/bin/bash

VAR=PSEA
YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT=/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/
CTL1=${INROOT}/${YMD1}/sfc_snp_1hr.ncctl
CTL2=${INROOT}/${YMD2}/sfc_snp_1hr.ncctl
CTL3=${INROOT}/${YMD3}/sfc_snp_1hr.ncctl
CTL4=${INROOT}/${YMD4}/sfc_snp_1hr.ncctl
CTL5=${INROOT}/${YMD5}/sfc_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
if [ ! -f $CTL3 ];then echo NO SUCH FILE,$CTL3;exit 1;fi
if [ ! -f $CTL4 ];then echo NO SUCH FILE,$CTL4;exit 1;fi
if [ ! -f $CTL5 ];then echo NO SUCH FILE,$CTL5;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'; 'open ${CTL3}'; 'open ${CTL4}'
'open ${CTL5}'

xmax = 2; ymax = 3 

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=1; ymargin=0.5

'cc'
'set vpage 0.0 8.5 0.0 11'
'set grid off';'set grads off'

nmap = 1
ymap = 1
while (ymap <= ymax)
xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 10';'set ylint 20'
'set map 1 1 1'
'set gxout contour';'set cint 4';'set ccolor 1';'set cthick 1'
'set clopts 1 1 0.05'
'd PSEA.'nmap'/100.0'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'


# TEXT
'set t 1';'q dims';TEMP=sublin(result,5);INITIME=subwrd(TEMP,6)
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' INIT:'INITIME


xmap=xmap+1
nmap=nmap+1
endwhile #xmap

ymap=ymap+1
endwhile #ymap

xx = 4.5; yy=ytop+0.5
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' ${TIME}'

'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
xx = 0.2; yy=ytop+1
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo
```



試作品１

```
#!/bin/bash

YYYYMMDDHH=20160606
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )
echo $YMD1 $YMD2 $YMD3 $YMD4 $YMD5
```

試作品２

```
#!/bin/bash

YYYYMMDDHH=20160606
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT=/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/
CTL1=${INROOT}/${YMD1}/sfc_snp_1hr.ncctl
CTL2=${INROOT}/${YMD2}/sfc_snp_1hr.ncctl
CTL3=${INROOT}/${YMD3}/sfc_snp_1hr.ncctl
CTL4=${INROOT}/${YMD4}/sfc_snp_1hr.ncctl
CTL5=${INROOT}/${YMD5}/sfc_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
if [ ! -f $CTL3 ];then echo NO SUCH FILE,$CTL3;exit 1;fi
if [ ! -f $CTL4 ];then echo NO SUCH FILE,$CTL4;exit 1;fi
if [ ! -f $CTL5 ];then echo NO SUCH FILE,$CTL5;exit 1;fi

echo $CTL1
echo $CTL2
echo $CTL3
echo $CTL4
echo $CTL5
```

試作品３

```
#!/bin/bash

VAR=PSEA
YYYYMMDDHH=2016061000
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT=/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/
CTL1=${INROOT}/${YMD1}/sfc_snp_1hr.ncctl
CTL2=${INROOT}/${YMD2}/sfc_snp_1hr.ncctl
CTL3=${INROOT}/${YMD3}/sfc_snp_1hr.ncctl
CTL4=${INROOT}/${YMD4}/sfc_snp_1hr.ncctl
CTL5=${INROOT}/${YMD5}/sfc_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi
if [ ! -f $CTL3 ];then echo NO SUCH FILE,$CTL3;exit 1;fi
if [ ! -f $CTL4 ];then echo NO SUCH FILE,$CTL4;exit 1;fi
if [ ! -f $CTL5 ];then echo NO SUCH FILE,$CTL5;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'; 'open ${CTL3}'; 'open ${CTL4}'
'open ${CTL5}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=1; ymargin=0.5

'cc'
'set vpage 0.0 8.5 0.0 11'
'set grid off';'set grads off'

nmap = 1
ymap = 1
xmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 10';'set ylint 20'
'set map 1 1 1'
'set gxout contour';'set cint 4';'set ccolor 1';'set cthick 1'
'set clopts 1 1 0.05'
'd PSEA.'nmap'/100.0'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# LEGEND COLOR BAR
#x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
#x=xr; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 r 3 0'
#'draw string 'x' 'y' ${UNIT}'


# TEXT
'set t 1';'q dims';TEMP=sublin(result,5);INITIME=subwrd(TEMP,6)
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' INIT:'INITIME


xx = 4.5; yy=ytop+0.5
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' ${TIME}'

'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
xx = 0.2; yy=ytop+1
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo
```



## 演習

６月３日から１０日まで、１日におきに海面気圧の分布図を書く

１００年天気図の天気図と並べて比較したパワーポイントの資料を作成する

http://agora.ex.nii.ac.jp/digital-typhoon/weather-chart/



## GrADSのコントールファイルについての資料

https://gitlab.com/infoaofd/lab/-/blob/master/FORTRAN/PROGRAM_2022/F_05_SP02_READ_WRF.md?ref_type=heads#ctl%E3%83%95%E3%82%A1%E3%82%A4%E3%83%AB

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TURORIAL_06_BINARY_IO.md?ref_type=heads

https://akyura.sakura.ne.jp/study/GrADS/kouza/grads.html



## GrADSの初歩

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TUTORIAL_01_NCEP1_OISST.md?ref_type=heads



## GrADSの使用法についての資料

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TURORIAL_06_BINARY_IO.md?ref_type=heads



## LINUX日付処理

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/01.BASH/LINUX_DATE.md?ref_type=heads
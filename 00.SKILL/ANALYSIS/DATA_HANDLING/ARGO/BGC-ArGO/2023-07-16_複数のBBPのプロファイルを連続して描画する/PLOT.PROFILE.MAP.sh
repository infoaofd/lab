#!/bin/bash

EXE=PLOT.PROFILE.MAP.py

if [ ! -f $EXE ]; then echo ERROR NO SUCH FILE,$EXE; exit 1;fi
# EXE (=PLOT.PROFILE.MAP.py)が無かったらエラーメッセージを表示
# して終了

INDIR=/work01/DATA/ARGO # 入力ファイルのあるディレクトリ名
#INFLE=BR5905194_070.nc  # 入力ファイル名
INFLE=BD2902092_037.nc

ODIR=OUT_$(basename $0 .sh) #出力ディレクトリ名
mkdir -vp $ODIR #出力ディレクトリがなければ新規作成する

OFLE=$(basename $INFLE .nc).PDF #出力ファイル名

echo python3 $EXE $INDIR $INFLE $ODIR $OFLE
python3 $EXE $INDIR $INFLE $ODIR $OFLE #pythonスクリプトの実行
if [ $? -eq 0 ];then
echo INDIR: $INDIR #入力ディレクトリ名の画面表示
echo INFLE: $INFLE #入力ファイル名の画面表示
echo ODIR: $ODIR #出力ディレクトリ名の画面表示
echo OFLE: $OFLE #出力ファイル名の画面表示
else
echo EEEEE ABNORMAL END! #pythonスクリプトが異常終了した場合
echo EEEEE INDIR: $INDIR #入力ディレクトリ名の画面表示
echo EEEEE INFLE: $INFLE #入力ファイル名の画面表示
fi

# MESHIMA_MIMIC_CORR

[[_TOC_]]

## 目的

MIMICの可降水量と女島の水蒸気量の相関係数（一点相関）の分布



## データフォーマット

```
/work03/2021/nakamuro/34.12.PW/22.22.PW.MIMIC_NCL_GRADS
$ ncdump -h /work03/2021/nakamuro/34.12.PW/22.22.PW.MIMIC_NCL_GRADS/OUT_0.MIMIC_NCL_GRADS/comp20200713.040000_GRADS.nc >0.NCDUMP.MIMIC.1H.TXT
```

```bash
$ cat 0.NCDUMP.MIMIC.1H.TXT 
netcdf comp20200713.040000_GRADS {
dimensions:
        lat = 721 ;
        lon = 1440 ;
variables:
        float tpw(lat, lon) ;
                tpw:least_significant_digit = 1. ;
                tpw:units = "mm" ;
        float lat(lat) ;
                lat:units = "degrees_north" ;
        float lon(lon) ;
                lon:units = "degrees_east" ;
}
```



/work03/2021/nakamuro/34.12.PW/22.22.PW.MIMIC_NCL_GRADS

**全部の日付変換**



## データの切り出し

### CDOを使った切り出しの方法

https://gitlab.com/infoaofd/lab/-/blob/master/CDO/00.CDO%E5%85%A5%E9%96%80.md?ref_type=heads#selllonlatbox



/work03/am/2022.MESHIMA.WV/52.12.PW/52.10.MIMIC_CUT

```
/work03/am/2022.MESHIMA.WV/52.12.PW/52.10.MIMIC_CUT
$ 52.10.MIMIC_CUT.sh 
mkdir: ディレクトリ `OUT_52.10.MIMIC_CUT' を作成しました
cdo    sellonlatbox: Processed 1038240 values from 1 variable over 1 timestep [0.06s 13MB].
IN: comp20200602.000000_GRADS.nc
OUT: comp20200602.000000_GRADS_CUT.nc
```

```bash
$ ncdump -h OUT_52.10.MIMIC_CUT/comp20200602.000000_GRADS_CUT.nc 
netcdf comp20200602.000000_GRADS_CUT {
dimensions:
        lon = 61 ;
        lat = 81 ;
variables:
        float lon(lon) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
                lon:axis = "X" ;
        float lat(lat) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
                lat:axis = "Y" ;
        float tpw(lat, lon) ;
                tpw:units = "mm" ;
                tpw:least_significant_digit = 1. ;

// global attributes:
                :CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
                :Conventions = "CF-1.6" ;
                :history = "Fri Jul 28 16:56:38 2023: cdo sellonlatbox,120,135,20,40 /work03/am/2022.MESHIMA.WV/52.12.PW/22.22.PW.MIMIC_NCL_GRADS/OUT_0.MIMIC_NCL_GRADS/comp20200602.000000_GRADS.nc OUT_52.10.MIMIC_CUT/comp20200602.000000_GRADS_CUT.nc" ;
                :CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
}
```



## 読み込み

### Fortranでの読み込み

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/ANALYSIS/DATA_HANDLING/2022-12-07_NETCDF%E3%81%AB%E6%85%A3%E3%82%8C%E3%82%8B/NetCDF%E3%81%AB%E6%85%A3%E3%82%8C%E3%82%8B.md#fortran%E3%81%A7%E3%81%AE%E8%AA%AD%E3%81%BF%E8%BE%BC%E3%81%BF



### ひな型

注：可降水量データの読み込み部分のみ

READ_MIMIC_CUT.sh

```bash
#!/bin/bash
#
# Fri, 04 Aug 2023 16:59:37 +0900
# /work03/am/2022.MESHIMA.WV/52.12.PW/52.11.READ_MIMIC_CUT_TEST
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"
LOPT="-L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "

# OpenMP
#OPT2=" -fopenmp "

INDIR="/work03/2021/nakamuro/52.12.PW/52.10.MIMIC_CUT/OUT_52.10.MIMIC_CUT"
ODIR=OUT_$(basename $0)
mkdir -vp $ODIR

echo ${src}.; ls -lh --time-style=long-iso ${src}; echo

echo Compiling ${src} ...; echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}; echo

${f90} ${DOPT} ${OPT} ${OPT2} ${src} -o ${exe} ${LOPT}

if [ $? -ne 0 ]; then
echo "EEEEE COMPILE ERROR!"
echo "EEEEE TERMINATED."; echo
exit 1
fi
echo "Done Compile."; echo
ls -lh ${exe}
echo

echo;echo ${exe} is running ...; echo

D1=$(date -R)
${exe}
if [ $? -ne 0 ]; then
echo;echo "EEEEE ERROR in $exe: RUNTIME ERROR!"
echo "EEEEE TERMINATED."; echo

D2=$(date -R)
echo "START: $D1 END:   $D2"
exit 1
fi
echo; echo "Done ${exe}";echo
D2=$(date -R)
echo "START: $D1 END:   $D2"

```

READ_MIMIC_CUT.F90

```fortran
PROGRAM READ_MIMIC_CUT
! Fri, 04 Aug 2023 16:59:37 +0900
! p5820.bio.mie-u.ac.jp
! /work03/am/2022.MESHIMA.WV/52.12.PW/52.11.READ_MIMIC_CUT_TEST

! NetCDF読み方
! https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/ANALYSIS/DATA_HANDLING/2022-12-07_NETCDF%E3%81%AB%E6%85%A3%E3%82%8C%E3%82%8B/NetCDF%E3%81%AB%E6%85%A3%E3%82%8C%E3%82%8B.md#fortran%E3%81%A7%E3%81%AE%E8%AA%AD%E3%81%BF%E8%BE%BC%E3%81%BF

include 'netcdf.inc'

!IMPLICIT NONE

CHARACTER INDIR*500,INFLE*500,IN*1000,ODIR*500,OFLE*500,OUT*1000

CHARACTER CDATE*8,CTIME*6

INTEGER,PARAMETER::IM=61,JM=81,KM=3*3*31*24
REAL,DIMENSION(KM)::tpw1d 
REAL,DIMENSION(IM,JM)::tpw2d
REAL,DIMENSION(IM,JM,KM)::tpw3d 
REAL,DIMENSION(KM)::qv1d

!REAL,PARAMETER::UNDEF=

INTEGER stat,varid

!NAMELIST /PARA/
!READ(*,NML=PARA)

!ALLOCATE()

!PRINT *

INDIR="/work03/2021/nakamuro/52.12.PW/52.10.MIMIC_CUT/OUT_52.10.MIMIC_CUT"

!
![1] 可降水量(tpw)の読み込み
!
tpw3d=0.0
it=0
YEAR: do i=2020,2022
MONTH: do j=6,8
if(j==6)jd=30
if(j==7 .or. j==8)jd=31
DAY: do k=1,jd
HOUR: do l=0,23

it=it+1

write(CDATE(1:4),'(I4.4)')i
write(CDATE(5:6),'(I2.2)')j
write(CDATE(7:8),'(I2.2)')k
write(CTIME(1:2),'(I2.2)')l
write(CTIME(3:6),'(I4.4)')0

INFLE='comp'//CDATE//'.'//CTIME//'_GRADS_CUT.nc'

IN=TRIM(INDIR)//'/'//TRIM(INFLE)

stat = nf_open(IN, nf_nowrite, ncid)

stat = nf_inq_varid(ncid, 'tpw', varid)
print *,stat,varid,trim(INFLE)

tpw2d=0.0
stat = nf_get_var_real(ncid, varid, tpw2d)

tpw3d(:,:,it)=tpw2d(:,:)

stat = nf_close(ncid)

end do HOUR
end do DAY
end do MONTH
end do YEAR
nd=id

!
! [2]　水蒸気量の読み込み
!
!データを読み込む変数をいまqv1dとする

!
! [3] 各地点(i,j)ごとに相関係数を計算
!
do j=1,JM
do i=1,IM

tpw1d(:)=tpw3d(i,j,:)

!qvとtpw1dの相関係数を計算
!計算された相関係数の値をcorr1dとする
! https://gitlab.com/infoaofd/lab/-/blob/master/FORTRAN/PROGRAM_2022/F_06_COPRELATION_MAP.md

!計算された相関係数を2次元配列に代入
!corr2d(i,j)=corr1d

end do !i
end do !j

END PROGRAM

```



## 相関係数の計算部分

### プログラムのひな型

```fortran
! Z0は１次元の配列 (ある特定の点の時間変化)
! zは３次元の配列 (Z(i,j,k)で, iを経度, jを緯度, kを時刻と仮定)
! CORRはZ0(:)とZ(i,j,:)の相関係数を表す２次元配列 (CORR(i,j)でiを経度, jを緯度と仮定)

Z0_AV=SUM(Z0)/FLOAT(NM)
Z0_AN(:)=Z0(:)-Z0_AV

do j=1,JM
do i=1,IM

Z1(:)=Z(I,J,:)

Z1_AV=SUM(Z1)/FLOAT(NM)
Z1_AN(:)=Z1(:)-Z1_AV

Z0_DOT_Z1 = DOT_PRODUCT(Z0_AN, Z1_AN)
Z0MAG2 = DOT_PRODUCT(Z0_AN, Z0_AN)
Z1MAG2 = DOT_PRODUCT(Z1_AN, Z1_AN)
Z0MAG=SQRT(Z0MAG2)
Z1MAG=SQRT(Z1MAG2)

CORR(i,j)=Z0_DOT_Z1/(Z0MAG*Z1MAG)

!print *,I,J,Z0_DOT_Z1,Z0MAG,Z1MAG,CORR(i,j)

end do !i
end do !j
```



## 結果の出力部分

### プログラムのひな型

```fortran
open(21,file=OFLE,form="unformatted",access="direct",&
recl=isize)
irec=1
write(21,rec=irec)CORR
close(21)

print *;print '(A,A)','OUTPUT: ',trim(OFLE)
```



## 上達のためのポイント

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**。

ポイントは次の3つである。

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。エラーの原因だけでなく，**考えうる解決策**が記載されていることも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーの場所が特定できれば，エラーの修正の大部分は完了したと考えてもよいほどである。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても頭に入らないものは印刷してから読む。

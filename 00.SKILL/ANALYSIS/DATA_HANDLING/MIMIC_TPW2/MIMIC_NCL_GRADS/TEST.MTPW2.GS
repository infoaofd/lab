
'open MTPW2.CTL'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off'
'set xlint 2';'set ylint 2'

'set gxout shade2'
# 'color  ' ;# SET COLOR BAR

# 'set lon  '; 'set lat  '
# 'set lev '
# 'set time Z'

'd tpwGrid'
'cbarn'

'set xlab off';'set ylab off'

'gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

#x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs  -ft 3 -line on -edge circle'
#x=x2+0.1; y=y1
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' '



#x=(xl+xr)/2 ;# TEXT
#y=yt+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' '

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' .nc.pdf'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ./TEST.MTPW2.sh '
yy = yy+0.2; 'draw string ' xx ' ' yy ' MTPW2.CTL'
yy = yy+0.2; 'draw string ' xx ' ' yy ' Fri, 12 May 2023 17:51:08 +0900'
yy = yy+0.2; 'draw string ' xx ' ' yy ' p5820.bio.mie-u.ac.jp'

'gxprint .nc.pdf'
'quit'

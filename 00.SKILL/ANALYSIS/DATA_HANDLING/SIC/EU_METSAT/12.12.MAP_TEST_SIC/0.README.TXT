/work03/am/2022.BGC_ARGO_SO/22.00.TIME-DEPTH_201701-08/32.SIC/12.12.MAP_TEST_SIC
Tue, 05 Sep 2023 16:33:52 +0900

http://osisaf.met.no/quicklooks/reprocessed/ice/conc-cont-reproc/v2p0/2017/08/


        float lon(yc, xc) ;
                lon:units = "degrees_east" ;
                lon:long_name = "longitude coordinate" ;
                lon:standard_name = "longitude" ;
        int ice_conc(time, yc, xc) ;
                ice_conc:_FillValue = -32767 ;
                ice_conc:long_name = "fully filtered concentration of sea ice using atmospheric correction of brightness temperatures and open water filters" ;
                ice_conc:standard_name = "sea_ice_area_fraction" ;
                ice_conc:units = "%" ;
                ice_conc:valid_min = 0 ;
                ice_conc:valid_max = 10000 ;
                ice_conc:grid_mapping = "Lambert_Azimuthal_Grid" ;
                ice_conc:coordinates = "time lat lon" ;
                ice_conc:ancillary_variables = "total_standard_error status_flag" ;
                ice_conc:scale_factor = 0.01 ;
                ice_conc:comment = "this field is the primary sea ice concentration estimate for this climate data record" ;


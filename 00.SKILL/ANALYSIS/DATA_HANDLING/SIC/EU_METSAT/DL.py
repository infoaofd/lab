"""
https://cds.climate.copernicus.eu/cdsapp#!/dataset/satellite-sea-ice-concentration?tab=overview

Sea ice concentration daily gridded data from 1979 to present derived from satellite observations
WARNING: 2021-03-05 - Because of an outage of SSMIS satellite data in February 2021, the EUMETSAT OSI SAF sea ice concentration ICDR product will have missing files for 2021-02-20 and 2021-02-21. For more details, see announcement on C3S User Forum.

"""

import cdsapi

c = cdsapi.Client()

c.retrieve(
    'satellite-sea-ice-concentration',
    {
        'version': 'v2',
        'variable': 'all',
        'format': 'zip',
        'region': 'southern_hemisphere',
        'cdr_type': 'icdr',
        'year': '2017',
        'month': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08',
        ],
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
        'origin': 'eumetsat_osi_saf',
    },
    'EUMETSAT_DAILY_SIC_SH_201701-201708.zip')

#!/bin/bash

# /work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.12.COMPOSITE_Z
# 
PREFIX=$1
#PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R3HMAX.ge.50PCNT}
PREFIX=${PREFIX:-QFLUX.ge.50PCNT_R3HMAX.le.50PCNT}
DTYPE=P # P or S

INFLE=02.MAKE_INFLE_${PREFIX}_${DTYPE}.TXT
if [ ! -f $INFLE ];then echo EEEEE NO SUCH FILE,$INFLE;exit 1;fi
echo MMMMM IN $INFLE; echo

OUT=$(basename $0 .sh)_${PREFIX}_${DTYPE}.nc

FILES=()
while IFS= read -r LINE; do
    FILES+=("$LINE")
done < "$INFLE"

rm -vf $OUT
cdo ensmean "${FILES[@]}" "$OUT"
if [ -f $OUT ];then echo MMMMM OUT $OUT;fi

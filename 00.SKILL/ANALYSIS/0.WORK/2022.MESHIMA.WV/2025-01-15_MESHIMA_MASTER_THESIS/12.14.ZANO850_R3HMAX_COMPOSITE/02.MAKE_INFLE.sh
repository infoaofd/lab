#!/bin/bash

# /work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.12.COMPOSITE_Z
# 02.MAKE_INFLE.sh

PREFIX=QFLUX.ge.50PCNT_R3HMAX.ge.50PCNT
#PREFIX=QFLUX.ge.50PCNT_R3HMAX.le.50PCNT
DTYPE=P # P or S

IN=N-KYUSHU_${PREFIX}.txt
if [ ! -f $INFLE ];then echo EEEEE NO SUCH FILE,$INFLE;exit 1;fi

INDIR=/work02/DATA/MSM.NC/SPLIT_HOUR/MSM-P/ZANO
if [ ! -d $INDIR ];then echo EEEEE NO SUCH FILE,$INDIR;exit 1;fi

OUT=$(basename $0 .sh)_${PREFIX}_${DTYPE}.TXT
rm -vf $OUT

echo MMMMM IN: $IN

YMD=();
N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y[$N]=${ary[0]}
  M[$N]=${ary[1]}
  D[$N]=${ary[2]}
  H[$N]=${ary[3]}

  N=$(expr $N + 1 )
done < $IN

rm -vf $OUT
touch $OUT

NM1=$(expr $N - 1)
I=0
while [ $I -le $NM1 ];do #I
YYYY=${Y[$I]}
MM=$(printf %02d ${M[$I]})
DD=$(printf %02d ${D[$I]})
HH=$(printf %02d ${H[$I]})


MD=${MM}${DD}
echo $MD
CFILE=${YYYY}/MSM-P_${YYYY}${MD}${HH}_ZANO850.nc
IP1=$(expr $I + 1)
#echo "$IP1 $INDIR/$CFILE \\"
echo "$INDIR/$CFILE" >>$OUT
I=$(expr $I + 1)

done #I

if [ -f $OUT ];then echo MMMMM OUT: $OUT;fi

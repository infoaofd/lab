;/work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/22.12.R3HMAX_COMPOSITE/12.12.Z_R3HMAX_COMPOISITE
INDIR="./"
VAR="ZANO850"
RAIN="R3HMAX"
PREFIX1="QFLUX.ge.50PCNT_"+RAIN+".ge.50PCNT"
PREFIX2="QFLUX.ge.50PCNT_"+RAIN+".le.50PCNT"
DTYPE="P"
INFLE1A="12.CDO_COMPO_AVE_"+VAR+"_"+PREFIX1+"_"+DTYPE+".nc"
INFLE2A="12.CDO_COMPO_AVE_"+VAR+"_"+PREFIX2+"_"+DTYPE+".nc"
INFLE1V="14.CDO_COMPO_VAR_"+VAR+"_"+PREFIX1+"_"+DTYPE+".nc"
INFLE2V="14.CDO_COMPO_VAR_"+VAR+"_"+PREFIX2+"_"+DTYPE+".nc"
IN1A=INDIR+INFLE1A
IN2A=INDIR+INFLE2A
IN1V=INDIR+INFLE1V
IN2V=INDIR+INFLE2V

N1=245. ;NUMBER OF SAMPLES IN CLUSTER 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
N2=171. ;NUMBER OF SAMPLES IN CLUSTER 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
VAR="zano"
LEV=850

scriptname_in = get_script_name()
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
FIG=scriptname+"_"+RAIN+"_"+VAR+tostring(LEV)
TYP="PDF"
FIGDIR="."

ODIR=scriptname+"_"+RAIN+"/"
system("mkdir -vp "+ODIR)
OFLE=scriptname+"_"+RAIN+"_"+VAR+tostring(LEV)+".nc"
OUT=ODIR+OFLE

f1a=addfile(IN1A,"r")
f2a=addfile(IN2A,"r")
f1v=addfile(IN1V,"r")
f2v=addfile(IN2V,"r")

a1=f1a->$VAR$(0,:,:)
a2=f2a->$VAR$(0,:,:)
v1=f1v->$VAR$(0,:,:)
v2=f2v->$VAR$(0,:,:)
lon=f1a->lon
lat=f1a->lat
printMinMax(v1,0)
printMinMax(v2,0)

sigr = 0.05; 
iflag=False; False indicates v1 /= v2

diff=a1-a2
copy_VarMeta(a1,diff)

prob = ttest(a1,v1,N1, a2, v2, N2, iflag, False) 
alpha=100.0*(1.0-prob) ;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
;!!!!!!!! alpha = 100.*(1. - prob) 
;!!!!!!!! A significance of 0.05 returned by ttest would yield 
;!!!!!!!! 95% for alpha.

copy_VarMeta(a1,alpha)
wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"NCV_blu_red") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=""
opt@gsnCenterString=""
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.07
opt@gsnLeftStringOrthogonalPosF=0.07
opt@gsnRightStringOrthogonalPosF=0.07
opt@gsnLeftStringFontHeightF=0.02
opt@gsnCenterStringFontHeightF=0.02
opt@gsnRightStringFontHeightF=0.02

opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
opt@cnMinLevelValF = -30.
opt@cnMaxLevelValF =  30.
opt@cnLevelSpacingF = 5.
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)

opt@pmLabelBarHeightF = 0.03
opt@pmLabelBarWidthF=0.5
opt@lbTitleOn        = True
opt@lbTitleString = "m" ; title string
opt@lbTitlePosition = "Right" ; title position
opt@lbTitleFontHeightF= .012 ;Font size
opt@lbTitleDirection = "Across" ; title direction
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbOrientation = "horizontal";"vertical"
opt@pmLabelBarOrthogonalPosF = .15

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "HighRes" ;-- better map resolution

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpFillOn               = False
res@mpDataBaseVersion = "HighRes" ;-- better map resolution
res@mpMinLonF = 120     ; 経度の最小値
res@mpMaxLonF = 150     ; 経度の最大値
res@mpMinLatF =  22     ; 緯度の最小値
res@mpMaxLatF =  48     ; 緯度の最大値

res@vpWidthF      = 0.5  ; plot size (width)
;res@vpXF          = 0.121578   ; location of plot
;res@vpYF          = 0.60       ; location of plot

print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
sgres@cnFillDotSizeF       = 0.002

sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
;sgres@cnLevels = 0.5	; nly set one level
sgres@cnLevels = 95.0	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = str_upper(VAR)+tostring(LEV)      ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString   = ""    ; upper-left subtitle
sgres@gsnRightString  = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,alpha,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

plot=gsn_csm_contour_map(wks,diff,res)
overlay(plot,sig_plot)

print("MMMMM PLOT CONTOUR")
cres                      = True		; significance
cres@gsnDraw              = False		; draw plot
cres@gsnFrame             = False		; advance frome
cres@cnFillDrawOrder      = "PostDraw"
cres@cnInfoLabelOn        = False		; turn off info label
cres@cnLinesOn            = True		; draw contour lines
cres@cnLineLabelsOn       = True		; draw contour labels
cres@cnLevelSelectionMode = "AutomaticLevels";"ManualLevels" ;
;cres@cnMinLevelValF = -30.
;cres@cnMaxLevelValF =  30.
;cres@cnLevelSpacingF = 5.
cres@cnLineColor = "green"; "purple"
cres@cnLineLabelFontColor = "green"; "purple"
cres@cnLineThicknessF = 2.0
plot2=gsn_csm_contour(wks,a2,cres)
overlay(plot,plot2)

draw(plot)

xh=0.1   ;Left of header lines
yh=0.9   ;Top of header lines
dyh=0.02 ;Line spacing
fh=0.01  ;Font Height

txres               = True
txres@txFontHeightF = fh
txres@txJust="CenterLeft"
today = systemfunc("date -R")
cwd =systemfunc("pwd")
TEXT=PREFIX1+" - "+PREFIX2
gsn_text_ndc(wks,today,  xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,"Current dir: "+cwd,      xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,TEXT,xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,"OUT: "+FIG+"."+TYP, xh,yh,txres)

frame(wks)         ; WorkStationの更新



print("MMMMM ODIR "+ODIR)
print("MMMMM OFLE "+OFLE)
; 同名のファイルが存在していたらまず消去
system("/bin/rm -vf "+OUT)

; 出力ファイルを開く
out = addfile (OUT, "c")

print("MMMMM CREATE NETCDF")
; 出力データの次元の設定
dimx   = dimsizes(a1)
nlat   = dimx(0)
mlon   = dimx(1)

; ファイル属性(ファイルに関する情報)の設定
filAtt = 0
filAtt@title =RAIN+" AVE P-VALUE OF WELCH TEST"
filAtt@input_directory = INDIR
filAtt@creation_date = systemfunc("date -R")
filAtt@CRITICAL_SIG_LVL = tostring(sigr)
filAtt@NUMBER_OF_SAMPLE_N1 = tostring(N1)
filAtt@NUMBER_OF_SAMPLE_N2 = tostring(N2)

fileattdef( out, filAtt )           ; copy file attributes  

setfileoption(out,"DefineMode",True)

;変数の設定
; 次元
dimNames = (/"lat", "lon"/)  
dimSizes = (/nlat,  mlon/) 
dimUnlim = (/False, False/)   
filedimdef(out,dimNames,dimSizes,dimUnlim)

filevardef(out, "lat", typeof(lat), getvardims(lat)) 
filevardef(out, "lon", typeof(lon), getvardims(lon))
filevardef(out, "a1",  typeof(a1),  getvardims(a1))    
filevardef(out, "a2",  typeof(a2),  getvardims(a2))    
filevardef(out, "alpha",  typeof(alpha),  getvardims(alpha))    

; 属性
filevarattdef(out,"lat"  ,lat) ; copy lat attributes
filevarattdef(out,"lon" ,lon)  ; copy lon attributes
filevarattdef(out,"a1", a1)
filevarattdef(out,"a2", a2)
filevarattdef(out,"alpha", alpha)
                
; 変数の値の書き出し
out->lat  = (/lat/)
out->lon  = (/lon/)
out->a1   = (/a1/)
out->a2   = (/a2/)
out->alpha   = (/alpha/)
print("MMMMM")
print("MMMMM OFLE "+OFLE)
print("MMMMM")



print("")
print("MMMMM FIG "+FIG+"."+TYP)
print("")
print("MMMMM CRITICAL SIG LVL FOR R " +tostring (sigr))
print("MMMMM NUMBER OF SAMPLES: N1="+tostring(N1)+", N2="+tostring(N2))
print("MMMMM CHECK N1 & N2. THEY ARE HARD CODED IN NCL SCRIPT")

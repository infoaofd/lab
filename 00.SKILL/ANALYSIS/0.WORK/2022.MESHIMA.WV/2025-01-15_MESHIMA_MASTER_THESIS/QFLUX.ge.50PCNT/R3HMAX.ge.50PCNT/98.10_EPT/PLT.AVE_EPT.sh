#!/bin/bash

INDIR=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT
INFLE=02.CDO_COMPOSITE_R3HMAX.ge.50PCNT_P.nc
IN=${INDIR}/${INFLE}

GS=$(basename $0 .sh).GS

LEV=850

LEVS="320 356 3"
KIND="-kind midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson"
FS=1
UNIT=K

TEXT1="R3HMAX.ge.50PCNT EPT${LEV}"
TEXT2="RAINDAY QFLUX.ge.50PCNT"

FIG=$(basename $INFLE P.nc)EPT${LEV}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1; xmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set lev ${LEV}'
'set t 1'


# CALC Equivalent Potential Temeprature
'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'
'e=0.01*rh*es'
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))'

'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56'

'mixr= 0.62197*(e/(${LEV}-e))*1000'
'TDL=temp*pow(1000/(${LEV}-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'

'EPT=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))'

#'set gxout shaded'
#'d EPT'
#'cbarn'


# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
'd EPT'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)


# LEGEND COLOR BAR
x1=1.75; x2=5.75; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.25
'set strsiz 0.17 0.2'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.35
'draw string 'x' 'y' ${TEXT2}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1
'draw string ' xx ' ' yy ' ${IN}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'


'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo


#!/bin/bash

INDIR=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT
INFLE1=02.CDO_COMPOSITE_R3HMAX.ge.50PCNT_P.nc
INFLE2=03.CDO_COMPOSITE_R3HMAX.le.50PCNT_P.nc

IN1=${INDIR}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi

IN2=${INDIR}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

GS=$(basename $0 .sh).GS

LEV=850

LEVS="320 356 3"
KIND="-kind midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson"
FS=1
UNIT=K

TEXT1="R3HMAX.ge.50PCNT - R3HMAX.le.50PCNT EPT${LEV}"
TEXT2="R3HMAX.ge.50PCNT EPT${LEV}"
TEXT3="RAINDAY QFLUX.ge.50PCNT"

FIG=$(basename $0 .sh)_${LEV}hPa.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}

'sdfopen ${IN1}'; 'sdfopen ${IN2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap=1; ymap=1; xmap=1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set dfile 1'
'set t 1'; 'set lev ${LEV}'

'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'
'e=0.01*rh*es'
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))'
'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56'
'mixr= 0.62197*(e/(${LEV}-e))*1000'
'TDL=temp*pow(1000/(${LEV}-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'
'EPT1=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))'


'set dfile 2'
'set t 1'; 'set lev ${LEV}'
'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'
'e=0.01*rh*es'
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))'
'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56'
'mixr= 0.62197*(e/(${LEV}-e))*1000'
'TDL=temp*pow(1000/(${LEV}-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'
'EPT2=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))'


'EPT=EPT1-EPT2'


# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
'd EPT1'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)


# LEGEND COLOR BAR
x1=1.75; x2=5.75; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# SET CONTOUR
'set gxout contour'
'set clab off' ;#LABEL OFF
'set cstyle 1'
'set cthick 10';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 2'
'set cmin -6'
'set cmax 6'
'd EPT'

'set clab off'
'set cstyle 1'
'set clopts 1 1 0.09'
'set cthick 3'
'color -6 6 2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson -gxout contour'
'd EPT'

'set clab on'
'set cstyle 1'
'set clopts 1 1 0.09'
'set cthick 5'
'color -6 6 2 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson -gxout contour'
'd EPT'


# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.13 0.16'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.28
'draw string 'x' 'y' ${TEXT2}'
x=(xl+xr)/2; y=y+0.28
'draw string 'x' 'y' ${TEXT3}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1
'draw string ' xx ' ' yy ' ${IN1}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${IN2}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'


'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo





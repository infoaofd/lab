#!/bin/bash

# /work09/am/00.WORK/2022.MESHIMA.WV/12.18.MSM_VORTICITY/22.32.SPLIT_TIME_COMPOSITE
# 01.MAKE_INFLE.sh


#IN=N-KYUSHU_QFLUX.ge.50PCNT_R3HMAX.ge.50PCNT.txt
IN=N-KYUSHU_QFLUX.ge.50PCNT_R3HMAX.le.50PCNT.txt
if [ ! -f $INFLE ];then echo EEEEE NO SUCH FILE,$INFLE;exit 1;fi

INDIR=/work01/DATA/MSM/MSM-P/SPLIT_TIME
#INDIR=/work01/DATA/MSM/MSM-S/SPLIT_TIME
if [ ! -d $INDIR ];then echo EEEEE NO SUCH FILE,$INDIR;exit 1;fi

OUT=$(basename $IN .txt)_INLIST_P.TXT
#OUT=$(basename $IN .txt)_INLIST_S.TXT

echo MMMMM IN: $IN

YMD=();
N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y[$N]=${ary[0]}
  M[$N]=${ary[1]}
  D[$N]=${ary[2]}
  H[$N]=${ary[3]}

  N=$(expr $N + 1 )
done < $IN

rm -vf $OUT
touch $OUT

NM1=$(expr $N - 1)
I=0
while [ $I -le $NM1 ];do #I
MM=$(printf %02d ${M[$I]})
DD=$(printf %02d ${D[$I]})
HH=$(printf %02d ${H[$I]})

YMDH=${Y[$I]}${MM}${DD}${HH}
echo $YMDH
CFILE=${Y[$I]}/MSM-P_SPLIT_${YMDH}.nc
#CFILE=${Y[$I]}/MSM-S_SPLIT_${YMDH}.nc
IP1=$(expr $I + 1)
#echo "$IP1 $INDIR/$CFILE \\"
echo "$INDIR/$CFILE \\" >>$OUT
I=$(expr $I + 1)

done #I

echo MMMMM OUT: $OUT

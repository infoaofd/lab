#!/bin/bash

INDIR=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT
INFLE1=02.CDO_COMPOSITE_R3HMAX.ge.50PCNT_P.nc
INFLE2=03.CDO_COMPOSITE_R3HMAX.le.50PCNT_P.nc

IN1=${INDIR}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi

IN2=${INDIR}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

GS=$(basename $0 .sh).GS

LEV=700

Rd=287

LEVS="0 300 30"
KIND="-kind maroon->saddlebrown->darkgoldenrod->khaki->white->palegreen->lightgreen->limegreen->darkgreen"
FS=1
UNIT=g/m2/s

TEXT1="R3HMAX.ge.50PCNT - R3HMAX.le.50PCNT QFLUX${LEV}"
TEXT2="R3HMAX.le.50PCNT QFLUX${LEV}"
TEXT3="RAINDAY QFLUX.ge.50PCNT"

FIG=$(basename $0 .sh)_${LEV}hPa.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}

'sdfopen ${IN1}'; 'sdfopen ${IN2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap=1; ymap=1; xmap=1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set dfile 1'
'set t 1'; 'set lev ${LEV}'
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'q = 0.62197*(e/(lev-e))'
'qv = (q/(1-q))'

'tv=(1+0.61*qv)*temp'

'row=${LEV}*100/(${Rd}*tv)'

'WVX1=row*qv*u*1000'; 'WVY1=row*qv*v*1000'
'VMAG1=mag(WVX1,WVY1)'


'set dfile 2'
'set t 1'; 'set lev ${LEV}'
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'q = 0.62197*(e/(lev-e))'
'qv = (q/(1-q))'

'tv=(1+0.61*qv)*temp'

'row=${LEV}*100/(${Rd}*tv)'

'WVX2=row*qv*u*1000'; 'WVY2=row*qv*v*1000'
'VMAG2=mag(WVX2,WVY2)'


'VMAG=VMAG1-VMAG2'


# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
'd VMAG2'


# wind vector
'set gxout vector'
'set cthick 12'; 'set ccolor  0'
'vec.gs skip(WVX2,25);WVY2 -SCL 0.5 200 -P 20 20 -SL ${UNIT}'

'set cthick  5'; 'set ccolor  1'
'vec.gs skip(WVX2,25);WVY2 -SCL 0.5 200 -P 20 20 -SL ${UNIT}'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)


# LEGEND COLOR BAR
x1=1.0; x2=5.0; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# wind vector label
xx=x2+1.25; yy=(y1+y2)/2
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(WVX2,25);WVY2 -SCL 0.5 200 -P 'xx' 'yy' -SL ${UNIT}'

# SET CONTOUR
'set gxout contour'
'set clab off' ;#LABEL OFF
'set cstyle 1'
'set cthick 10';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 10'   ;#CONTOUR INTERVAL
'set cmin -100'
'set cmax 100'
'd VMAG'

'set clab off'
'set cstyle 1'
'set clopts 1 1 0.09'
'set cthick 3'
'color -100 100 10 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson -gxout contour'
'd VMAG'

'set clab on'
'set cstyle 1'
'set clopts 1 1 0.09'
'set cthick 5'
'color -100 100 10 -kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson -gxout contour'
'd VMAG'


# TEXT
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.13 0.16'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.28
'draw string 'x' 'y' ${TEXT2}'
x=(xl+xr)/2; y=y+0.28
'draw string 'x' 'y' ${TEXT3}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1
'draw string ' xx ' ' yy ' ${IN1}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${IN2}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'


'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo



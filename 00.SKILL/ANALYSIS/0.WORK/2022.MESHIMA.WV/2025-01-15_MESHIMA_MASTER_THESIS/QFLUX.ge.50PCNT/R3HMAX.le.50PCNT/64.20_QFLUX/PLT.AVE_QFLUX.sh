#!/bin/bash

INDIR=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT
INFLE=03.CDO_COMPOSITE_R3HMAX.le.50PCNT_P.nc
IN=${INDIR}/${INFLE}

GS=$(basename $0 .sh).GS

LEV=700

Rd=287

LEVS="0 300 30"
KIND="-kind maroon->saddlebrown->darkgoldenrod->khaki->white->palegreen->lightgreen->limegreen->darkgreen"
FS=1
UNIT=g/m2/s

TEXT1="R3HMAX.le.50PCNT QFLUX${LEV}"
TEXT2="RAINDAY QFLUX.ge.50PCNT"

FIG=$(basename $INFLE P.nc)QFLUX${LEV}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1; ymap = 1; xmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set lev ${LEV}'

'set t 1'

'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'q = 0.62197*(e/(lev-e))'
'qv = (q/(1-q))'

'tv=(1+0.61*qv)*temp'

'row=${LEV}*100/(${Rd}*tv)'

'WVX=row*qv*u*1000'; 'WVY=row*qv*v*1000'
'VMAG=mag(WVX,WVY)'

#'set gxout shaded'
#'d VMAG'
#'cbarn'


# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
'd VMAG'


# wind vector
'set gxout vector'
'set cthick 12'; 'set ccolor  0'
'vec.gs skip(WVX,25);WVY -SCL 0.5 200 -P 20 20 -SL ${UNIT}'

'set cthick  5'; 'set ccolor  1'
'vec.gs skip(WVX,25);WVY -SCL 0.5 200 -P 20 20 -SL ${UNIT}'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)


# LEGEND COLOR BAR
x1=1.0; x2=5.0; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# wind vector label
xx=x2+1.25; yy=(y1+y2)/2
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(WVX,25);WVY -SCL 0.5 200 -P 'xx' 'yy' -SL ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.25
'set strsiz 0.17 0.2'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.35
'draw string 'x' 'y' ${TEXT2}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1
'draw string ' xx ' ' yy ' ${IN}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'


'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo





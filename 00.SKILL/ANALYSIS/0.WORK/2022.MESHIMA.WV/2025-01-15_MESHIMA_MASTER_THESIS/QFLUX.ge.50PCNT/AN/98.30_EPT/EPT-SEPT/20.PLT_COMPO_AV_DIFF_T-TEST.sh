#!/bin/bash
# /work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/12.12.R1DAAV/12.34.EPT_COMPOSITE

BLEV=950; ULEV=600
VARNAME=EPT${BLEV}-SEPT${ULEV}

var=${VAR:-b}

RAIN=R3HMAX

INROOT=./
PREFIX=18.COMPO_AV_DIFF_T-TEST.NCL_${RAIN}
INDIR=$INROOT/${PREFIX}
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;fi
INFLE=${PREFIX}_${VARNAME}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;fi

FIG=$(basename $0 .sh)_${RAIN}_${VARNAME}.PDF

TEXT1="${VARNAME}"
TEXT2="${RAIN} > 50% - ${RAIN} < 50%"
TEXT3="RAINDAY   QFLUX > 50%"

GS=$(basename $0 .sh).GS

LEVS="-5 5 0.5"
KIND="-kind midnightblue->deepskyblue->paleturquoise->white->orange->red->crimson"
FS=2; UNIT=K

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}

'sdfopen ${IN}'
'q ctlinfo 1';say 'MMMMM 'sublin(result,1);say

'set t 1'

xmax = 1; ymax = 1
ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1
nmap=1; ymap=1; xmap=1
xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off'
'set xlint 10';'set ylint 5'
'set mpdset hires'

'diff=a1-a2'

# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
#'d diff'
'd maskout(diff,alpha-95)'
'set xlab off';'set ylab off'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)


# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set gxout shaded' ;#SHADE NOT SIGNIFICANT
'set rgb 98 255 255 255   0'
'set rgb 99 0 0 0 90'
'set clevs 0 95'
'set ccols 99   99  98'
#'d maskout(alpha,95-alpha)'

'set gxout contour';# CONTOUR
'set clab off' ;#LABEL OFF
'set cstyle 1'; 'set cthick 12';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 4'; 'set cmin -21'; 'set cmax 9'
'd a2'
'set clab on'; 'set cstyle 1'; 'set cthick 5'
'set clskip 1'
'set ccolor 3';#GREEN
'set cint 4'; 'set cmin -21'; 'set cmax 9'
'd a2'


'set gxout contour';# CONTOUR
'set clab off' ;#LABEL OFF
'set cstyle 1'; 'set cthick 12';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 4';'set cmin -21'; 'set cmax 9'
'd a1'
'set clab on'; 'set cstyle 1'; 'set cthick 5'
'set clskip 1'
'set ccolor 2';#RED
'set cint 4'; 'set cmin -21'; 'set cmax 9'
'd a1'

# TEXT
x=(xl+xr)/2; y=yt+0.19
'set strsiz 0.15 0.19'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=y+0.35; 'draw string 'x' 'y' ${TEXT2}'
x=(xl+xr)/2; y=y+0.25; 'draw string 'x' 'y' ${TEXT3}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+1.1; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF


grads -bcp "$GS"
rm -vf $GS

echo; if [ -f $FIG ]; then echo "MMMMM FIG: $FIG"; fi



#!/bin/bash
# Thu, 26 Dec 2024 16:51:29 +0900
# /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/62.14.COMPOSITE_WVF/12.12.WVF_PROJECTION

function YYYYMMDDHH(){
YYYY=${YMDH:0:4}; MM=${YMDH:4:2}; DD=${YMDH:6:2}; HH=${YMDH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

ANG=30
# PHI=ANG*3.141592653589793/180.0 #2pi rad =360 deg -> 1 deg= pi/180.0 deg
#PHI=$(echo "1+1" | bc)
PHI=$(echo "scale=7;${ANG}*3.141592653590/180.0" | bc -l | xargs printf "%.5f\n")
echo "ANG=${ANG} PHI=${PHI}"

YMDH1=$1; YMDH1=${YMDHM1:-2021081202}
YMDH=$YMDH1
YYYYMMDDHH $YMDH
MMM1=$MMM
Y1=${YYYY};MM1=${MM};DD1=${DD};HH1=${HH}
#echo $Y1 $MM1 $DD1 $HH1 $MMM1; exit 0
TIME=${HH1}Z${DD1}${MMM1}${Y1}
#echo $TIME;exit 0

CTL=MSM-P.CTL
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

GS=$(basename $0 .sh).GS; FIG=$(basename $0 .sh).PDF

# NetCDF OUT
# https://gitlab.com/infoaofd/lab/-/tree/master/00.SKILL/00.TOOL/GRADS/GrADS_RECIPE/GrADS_NETCDF書き出し
NCX=MSM_WVRX_${YYYY}-${MM}-${DD1}_${HH}_PROJ${ANG}.nc
NCY=MSM_WVRY_${YYYY}-${MM}-${DD1}_${HH}_PROJ${ANG}.nc

# LONW= ;LONE= ; LATS= ;LATN=
LEV=850
FIG=MSM_WVF_${YYYY}-${MM}-${DD1}_${HH}_PROJ${ANG}_LEV${LEV}.PDF

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
# KIND='midnightblue->deepskyblue->paleturquoise->white->orange->red->magenta'
# FS=2
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 1
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grads off';'set grid off'

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'

'set x 1 241'; 'set y 1 253'; 'set z 1 12'
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'
'VT=temp*(1.0 + 0.61*QV)'
'R=287'
'RHO=lev*100/(R*temp)'

'WVX=RHO*QV*u'; 'WVY=RHO*QV*v'
#'WVX=u'; 'WVY=v'

'WVRX=WVX*'math_cos(${PHI})'+WVY*'math_sin(${PHI})
'WVRY=-WVX*'math_sin(${PHI})'+WVY*'math_cos(${PHI})

'set x 1 241'; 'set y 1 253'; 'set z 1 12'
say; say 'MMMMM NetCDF OUT'
'define WVRX = WVRX'; 'set sdfwrite $NCX'; 'sdfwrite WVRX'
say 'MMMMM NetCDF FILE = $NCX'
'define WVRY = WVRY'; 'set sdfwrite $NCY'; 'sdfwrite WVRY'
say 'MMMMM NetCDF FILE = $NCY'

'set lev ${LEV}'
'set lon 128 129'; 'set lat 29 30'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(WVX,1);WVY -SCL 0.5 0.2 -P 20 20 -SL kg/m2/s'
'set xlab off'; 'set ylab off'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65; yy=yb-0.35
'set cthick  2'; 'set ccolor  1'
'vec.gs skip(WVX,1);WVY -SCL 0.5 0.2 -P 'xx' 'yy' -SL kg/m2/s'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(WVRX,1);WVRY -SCL 0.5 0.2 -P 20 20 -SL kg/m2/s'
'set xlab off'; 'set ylab off'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.5; yy=yb-0.35
'set cthick  3'; 'set ccolor  2'
'vec.gs skip(WVRX,1);WVRY -SCL 0.5 0.2 -P 'xx' 'yy' -SL kg/m2/s'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
# x=xl ;# (xl+xr)/2; y=yt+0.2
#'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

'quit'
EOF

grads -bcp "$GS"
#rm -vf $GS

if [ -f $FIG ]; then echo "FIG : $FIG"; fi
echo

#!/bin/bash

INDIR=/work09/2021/nakamuro/RAINDAY/12.12.WVF_PROJECTION
INFLE1=MSM_WVRX_2021-08-12_02_PROJ30.nc
INFLE2=MSM_WVRY_2021-08-12_02_PROJ30.nc

IN1=${INDIR}/${INFLE1}
IN2=${INDIR}/${INFLE2}

LEV=850

GS=$(basename $0 .sh).GS; FIG=$(basename $0 .sh).PDF

cat << EOF > ${GS}

'sdfopen ${IN1}'; 'sdfopen ${IN2}'

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 1
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set grads off';'set grid off'

'set dfile 1'
'WVRX=WVRX'

'set dfile 2'
'WVRY=WVRY'

'set lev ${LEV}'
'set lon 128 129'; 'set lat 29 30'

'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(WVRX,1);WVRY -SCL 0.5 0.2 -P 20 20 -SL kg/m2/s'
'set xlab off'; 'set ylab off'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.5; yy=yb-0.35
'set cthick  3'; 'set ccolor  2'
'vec.gs skip(WVRX,1);WVRY -SCL 0.5 0.2 -P 'xx' 'yy' -SL kg/m2/s'


'gxprint ${FIG}'

'quit'

EOF


grads -bcp "$GS"
#rm -vf $GS

if [ -f $FIG ]; then echo "FIG : $FIG"; fi
echo




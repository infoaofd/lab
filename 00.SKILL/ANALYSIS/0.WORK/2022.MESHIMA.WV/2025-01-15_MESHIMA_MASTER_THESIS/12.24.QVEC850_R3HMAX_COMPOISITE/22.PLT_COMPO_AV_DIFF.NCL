;/work09/am/00.WORK/2022.MESHIMA.WV/2025-01-15_MESHIMA_MASTER_THESIS/32.12.COMPOSITE/22.12.R3HMAX_COMPOSITE/12.12.Z_R3HMAX_COMPOISITE

RAIN="R3HMAX"
VAR="CNVQ"
LEV="700"
INDIR="18.COMPO_AV_DIFF_T-TEST.NCL_R3HMAX/"
INFLE="18.COMPO_AV_DIFF_T-TEST.NCL_"+RAIN+"_"+VAR+LEV+".nc"
IN=INDIR+INFLE
if (.not. fileexists(IN)) then
print("EEEEE NO SUCH FILE, "+IN)
exit
end if

scriptname_in = get_script_name()
scriptname=systemfunc("basename " + scriptname_in + " .ncl")
FIG=scriptname+"_"+RAIN+"_"+VAR+tostring(LEV)
TYP="PDF"
FIGDIR="."

ODIR=scriptname+"_"+RAIN+"/"
system("mkdir -vp "+ODIR)
OFLE=scriptname+"_"+RAIN+"_"+VAR+LEV+".nc"
OUT=ODIR+OFLE

f=addfile(IN,"r")
a1=f->a1 ;$VAR$(0,{LEV},:,:)
a2=f->a2 ;$VAR$(0,{LEV},:,:)
alpha=f->alpha ;$VAR$(0,{LEV},:,:)
lon=f->lon
lat=f->lat

sigr = 0.05; 
iflag=False; False indicates v1 /= v2

diff=a1-a2
copy_VarMeta(a1,diff)
wks = gsn_open_wks(TYP, FIGDIR+"/"+FIG)
gsn_define_colormap(wks,"NCV_jaisnd") ;"ncl_default") ;testcmap") ;BlueDarkRed18") ;")

opt=True
opt@gsnDraw       = False     
opt@gsnFrame      = False      

opt@gsnLeftString=""
opt@gsnCenterString=""
opt@gsnRightString=""
opt@gsnCenterStringOrthogonalPosF=0.07
opt@gsnLeftStringOrthogonalPosF=0.07
opt@gsnRightStringOrthogonalPosF=0.07
opt@gsnLeftStringFontHeightF=0.02
opt@gsnCenterStringFontHeightF=0.02
opt@gsnRightStringFontHeightF=0.02

opt@cnLevelSelectionMode = "ManualLevels" ;"AutomaticLevels"
opt@cnMinLevelValF = -1.E-14
opt@cnMaxLevelValF =  1.E-14
opt@cnLevelSpacingF = 1.E-15
;opt@cnLevelSelectionMode = "ExplicitLevels"
;opt@cnLevels = (/-50,-25,-20,-15,-10,-5,0,5,10,15,20,25,50/)
;opt@cnLevels = (/-100,-40,-30,-20,-10,-5,0,5,10,20,30,40,100/)

opt@pmLabelBarHeightF = 0.3
opt@pmLabelBarWidthF=0.03
opt@lbTitleString    = ""                ; title string
opt@lbTitlePosition  = "Top" ;Right"              ; title position
opt@lbTitleFontHeightF= 0.015               ; make title smaller
opt@lbOrientation = "vertical"
opt@lbLabelFontHeightF   = 0.015

res=opt
res@cnFillOn     = True   ; turn on color fill
res@cnLinesOn    = False    ; turn off contour lines

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="saddlebrown"
res@mpDataBaseVersion = "HighRes" ;-- better map resolution

res@cnFillDrawOrder      = "PreDraw"  ; draw contours first
res@cnFillMode = "CellFill"
res@cnMissingValFillColor = "lightgray"

res@gsnAddCyclic = False
res@mpGeophysicalLineThicknessF=1
res@mpGeophysicalLineColor="black"
res@mpFillOn               = False
res@mpDataBaseVersion = "HighRes" ;-- better map resolution
res@mpMinLonF = 125     ; 経度の最小値
res@mpMaxLonF = 135     ; 経度の最大値
res@mpMinLatF =  28     ; 緯度の最小値
res@mpMaxLatF =  36     ; 緯度の最大値

res@vpWidthF      = 0.5  ; plot size (width)
;res@vpXF          = 0.121578   ; location of plot
;res@vpYF          = 0.60       ; location of plot

print("MMMMM PLOT STATISTICAL SIGNIFICANCE")
sgres                      = True		; significance
sgres@gsnDraw              = False		; draw plot
sgres@gsnFrame             = False		; advance frome
sgres@cnInfoLabelOn        = False		; turn off info label
sgres@cnLinesOn            = False		; draw contour lines
sgres@cnLineLabelsOn       = False		; draw contour labels
sgres@cnFillDotSizeF       = 0.002

sgres@cnFillOn = True
sgres@cnFillColors = (/"transparent","transparent"/)	; choose one color for our single cn level
sgres@cnLevelSelectionMode = "ExplicitLevels"	; set explicit contour levels
;sgres@cnLevels = 0.5	; only set one level
sgres@cnLevels = 95.0	; only set one level
sgres@lbLabelBarOn = False

sgres@tiMainString = str_upper(VAR)+tostring(LEV)      ; title
sgres@gsnCenterString = ""  ; subtitle
sgres@gsnLeftString   = ""    ; upper-left subtitle
sgres@gsnRightString  = ""   ; upper-right subtitle

sig_plot = gsn_csm_contour(wks,alpha,sgres)

res3 = True
res3@gsnShadeFillType = "pattern"
res3@gsnShadeHigh     = 17
sig_plot = gsn_contour_shade(sig_plot,-999.,0.5,res3)

plot=gsn_csm_contour_map(wks,diff,res)

overlay(plot,sig_plot)
draw(plot)

xh=0.1   ;Left of header lines
yh=0.9   ;Top of header lines
dyh=0.02 ;Line spacing
fh=0.01  ;Font Height

txres               = True
txres@txFontHeightF = fh
txres@txJust="CenterLeft"
today = systemfunc("date -R")
cwd =systemfunc("pwd")
;TEXT=PREFIX1+" - "+PREFIX2
gsn_text_ndc(wks,today,  xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,"Current dir: "+cwd,      xh,yh,txres)
;yh=yh-dyh
;gsn_text_ndc(wks,TEXT,xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,"OUT: "+FIG+"."+TYP, xh,yh,txres)

frame(wks)         ; WorkStationの更新

print("")
print("MMMMM FIG "+FIG+"."+TYP)

yyyy1=2020;yyyy2=2023; y=$yyyy1

mm1=06; dd1=01
mm2=08; dd2=31

while [ $y -le $yyyy2 ]; do

INDIR=/work02/DATA/MSM.NC/Q-VECTOR/MSM-P/${y}
 ODIR=/work02/DATA/MSM.NC/Q-VECTOR/DAY_MEAN/${y}
mkdir -vp $ODIR

start=${y}/${mm1}/${dd1}; end=${y}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${y}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  IN=$INDIR/QVEC_${y}${mm}${dd}.nc
  if [ ! -f $IN ];then echo EEEEE NO SUCH FILE, $IN;fi
  OUT=${ODIR}/QVEC_DAYMEAN_${y}${mm}${dd}.nc
  rm -vf $OUT
  cdo daymean $IN $OUT
  if [ -f $OUT ];then echo MMMMM OUT: $OUT;fi
  echo
  i=$(expr $i + 1)
done

  y=$(expr $y + 1)

done



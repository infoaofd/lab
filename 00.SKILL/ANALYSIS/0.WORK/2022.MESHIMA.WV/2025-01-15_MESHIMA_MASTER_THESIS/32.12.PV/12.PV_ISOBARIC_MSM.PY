# -*- coding: shift-jis -*-
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import argparse
import os

# 引数の処理
parser = argparse.ArgumentParser(description="Calculate Potential Vorticity")
parser.add_argument("--year", type=int, required=True, help="Year of the input data")
parser.add_argument("--month", type=int, required=True, help="Month of the input data")
parser.add_argument("--day", type=int, required=True, help="Day of the input data")
parser.add_argument("--hour", type=int, required=True, help="Hour of the input data")
parser.add_argument("--input_dir", type=str, required=True, help="Directory of the input NetCDF files")
parser.add_argument("--output_dir", type=str, required=True, help="Directory to save the output files")
parser.add_argument("--fig_dir", type=str, required=True, help="Directory to save the figure files")
parser.add_argument("--level", type=int, default=300, help="Pressure level for plotting")

args = parser.parse_args()

# 定数
g=9.81
Cp = 1000.4  # 比熱
Rd = 287.05  # 乾燥空気の気体定数 [J/(kg・K)]
P0 = 1000.0*100.0  # 基準気圧 [Pa]

# 入力ファイルのパス作成
input_file = os.path.join(args.input_dir, f"MSM-P_SPLIT_{args.year}{args.month:02d}{args.day:02d}{args.hour:02d}.nc")
data = xr.open_dataset(input_file)

# 変数の取得
time = data["time"].values
lon = data["lon"].values  # 経度
lat = data["lat"].values  # 緯度
lev = data["p"].values  # 気圧面 [hPa]
u = data["u"].values   # 東西風 [m/s]
v = data["v"].values   # 南北風 [m/s]
T = data["temp"].values  # 温度 [K]
#P = lev[None, :, None, None] * 100  # 気圧 [Pa] (3D化)
lev_expanded = lev.reshape((1, 16, 1, 1))  # (1, 16, 1, 1) にリシェイプ

# T.shape に合わせてブロードキャスト
P = np.broadcast_to(lev_expanded, T.shape)  # (1, 16, 253, 241) にブロードキャスト
P=P*100.0
# 温位の計算
print("T.shape="+str(T.shape))
print("max T="+str(np.max(T)))
print("min T="+str(np.min(T)))
print("P.shape="+str(P.shape))
print("Max P:", np.max(np.abs(P)))
print("Min P:", np.min(np.abs(P)))

theta = T * (P0 / P)** (Rd / Cp)
print("theta.shape="+str(theta.shape))
print("Max theta:", np.max(np.abs(theta)))

# コリオリパラメータ
f = 2 * 7.2921e-5 * np.sin(np.radians(lat))  # [1/s]

# 緯度・経度方向の間隔（ゼロ除去）
dlat = np.radians(np.gradient(lat))
dlon = np.radians(np.gradient(lon))

# 地球半径
Re = 6.371e6  # [m]

# 相対渦度 ζ
dv_dx = np.gradient(v, axis=-1) / (Re * np.cos(np.radians(lat[:, None]))) / np.where(dlon == 0, np.nan, dlon)
du_dy = np.gradient(u, axis=-2) / Re / np.where(dlat[:, None] == 0, np.nan, dlat[:, None])
zeta = dv_dx - du_dy  # [1/s]

# θの気圧微分（ゼロ除去）
lev_expanded = lev.reshape((1, 16, 1, 1))  # Reshape to match the vertical dimension of theta
# Explicit finite difference method for pressure gradient (dtheta/dp)
dtheta_dp = np.gradient(theta, lev_expanded.squeeze() * 100, axis=1, edge_order=2)  # [K/Pa]
# 何も指定していない場合ではedge_order=1になっていて，境界部分の微分は1次の前方/後方差分を計算する．
# オプションでedge_order=2を明記すると，境界部分の微分は2次の前方/後方差分を計算する．

print("Max lev_expanded:", np.max(lev_expanded))
print("Max dtheta_dp:", np.max(dtheta_dp))

# 渦位
f_expanded = f[None, None, :, None]  # Reshape f
print("f_expanded"+str(f_expanded.shape))

# dtheta_dpが-999.9の場合、PVも-999.9に設定
PV = -g * (zeta + f_expanded) * dtheta_dp
PV[dtheta_dp == -999.9] = -999.9  # dtheta_dpが-999.9の箇所に対応するPVも-999.9に設定
print("Max PV", np.max(PV))

# NaN の処理（ゼロ割りが発生していた場合）
PV = np.nan_to_num(PV, nan=0.0)

# NetCDF に保存
output_nc = os.path.join(args.output_dir, f"PV_MSM-P_{args.year}{args.month:02d}{args.day:02d}{args.hour:02d}.nc")
pv_ds = xr.Dataset({
    "PV": (("time", "p", "lat", "lon"), PV)
}, coords={
    "time": time,  # 時刻の次元を拡張
    "p": lev,
    "lat": lat,
    "lon": lon
})

# PVにmissing_valueを設定
pv_ds["PV"].attrs["missing_value"] = -999.9

pv_ds.to_netcdf(output_nc)

# プロットする気圧面（LEV）を引数から取得、デフォルトは500 hPa
level_index = np.argmin(np.abs(lev - args.level))

# 図の保存
output_file = os.path.join(args.fig_dir, f"PV{args.level}_{args.year}{args.month:02d}{args.day:02d}{args.hour:02d}.pdf")
plt.figure(figsize=(8, 5))
print("level_index="+str(level_index)+" args.level="+str(args.level))
plt.contourf(lon, lat, PV[0, level_index], levels=5, cmap='gist_ncar')  # 選択された気圧面
plt.colorbar(label='Potential Vorticity (PVU)')
plt.xlabel('Longitude')
plt.ylabel('Latitude')
plt.title(f'Potential Vorticity at {args.level} hPa - {args.year}-{args.month:02d}-{args.day:02d} {args.hour:02d}:00')

# PDFの左上に情報を追加
plt.text(0.05, 0.95, f"Current directory: {os.getcwd()}", ha='left', va='top', fontsize=8, transform=plt.gca().transAxes)
plt.text(0.05, 0.90, f"Input file directory: {args.input_dir}", ha='left', va='top', fontsize=8, transform=plt.gca().transAxes)
plt.text(0.05, 0.85, f"NetCDF output file: {os.path.basename(output_nc)}", ha='left', va='top', fontsize=8, transform=plt.gca().transAxes)
plt.text(0.05, 0.80, f"Script name: {os.path.basename(__file__)}", ha='left', va='top', fontsize=8, transform=plt.gca().transAxes)
plt.text(0.05, 0.75, f"PDF output file: {os.path.basename(output_file)}", ha='left', va='top', fontsize=8, transform=plt.gca().transAxes)

plt.savefig(output_file, format='pdf')
plt.close()

# 終了メッセージ表示
print(f"Input file: {input_file}")
print(f"NetCDF output file: {output_nc}")
print(f"PDF output file: {output_file}")
print()  # 空行を出力

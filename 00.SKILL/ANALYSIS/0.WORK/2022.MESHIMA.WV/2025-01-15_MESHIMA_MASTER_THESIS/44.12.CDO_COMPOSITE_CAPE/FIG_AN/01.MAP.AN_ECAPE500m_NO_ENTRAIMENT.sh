#!/bin/bash
P_INIT=500m

TEXT1="CAPE_AN $P_INIT RH"
TEXT2="R3HMAX.ge.50PCNT - R3HMAX.le.50PCNT"
TEXT3="RAINDAY QFLUX.ge.50PCNT"

LONW=125;LONE=133;LATS=29;LATN=35
DOMAIN=${LONW}-${LONE}_${LATS}-${LATN}

INDIR1=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT/ECAPE.vs.CAPE/VERTICAL_SECTION/44.12.CDO_COMPOSITE_CAPE/R3HMAX.ge.50PCNT
INFLE1=14.22.CDO_COMPOSITE_R3HMAX.ge.50PCNT_CAPE.nc
IN1=${INDIR1}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi

INDIR2=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT/ECAPE.vs.CAPE/VERTICAL_SECTION/44.12.CDO_COMPOSITE_CAPE/R3HMAX.le.50PCNT
INFLE2=24.22.CDO_COMPOSITE_R3HMAX.le.50PCNT_CAPE.nc
IN2=${INDIR2}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).pdf ;#eps

LEVS="-240 240 40" 
KIND="-kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta"
FS=2
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}
'sdfopen ${IN1}'; 'sdfopen ${IN2}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set mpdset hires'; 'set grads off'; 'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set t 1'
'CAPE1=CAPE'

'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set t 1'
'CAPE2=CAPE'

'CAPE=CAPE1-CAPE2'


# SET COLOR BAR
#'set gxout shaded'
'color ${LEVS} ${KIND} -gxout shaded'
'd CAPE'
#'cbarn'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt


# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.18
'set strsiz 0.1 0.12'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.25
'draw string 'x' 'y' ${TEXT2}'
x=(xl+xr)/2; y=y+0.25
'draw string 'x' 'y' ${TEXT3}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=YUP+1.1
'draw string ' xx ' ' yy ' ${IN1}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${IN2}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo



#!/bin/bash

# /work09/am/00.WORK/2022.MESHIMA.WV/12.18.MSM_VORTICITY/22.32.SPLIT_TIME_COMPOSITE
# 01.MAKE_INFLE.sh

LAGHR=3
#LAGPM=MINUS
LAGPM=PLUS
LAG=${LAGPM}${LAGHR}


INDIR1=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT/ANYPLACE-ALTITUDE/R3HMAX.le.50PCNT
INFLE1=N-KYUSHU_QFLUX.ge.50PCNT_R3HMAX.le.50PCNT_${LAG}.txt
IN=${INDIR1}/${INFLE1}
if [ ! -f $IN ];then echo EEEEE NO SUCH FILE,$IN;exit 1;fi

INDIR2=/work02/DATA/MSM.NC/MSM_CUT_125-133_29-35/OUT_0.ECAPE_NO_ENTRAINMENT_500m
if [ ! -d $INDIR2 ];then echo EEEEE NO SUCH DIR,$INDIR2;exit 1;fi

OUT=$(basename $INFLE1 .txt)_INLIST_CAPE.TXT

echo MMMMM IN: $IN

YMD=();
N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y[$N]=${ary[0]}
  M[$N]=${ary[1]}
  D[$N]=${ary[2]}
  H[$N]=${ary[3]}

  N=$(expr $N + 1 )
done < $IN

rm -vf $OUT
touch $OUT

NM1=$(expr $N - 1)
I=0
while [ $I -le $NM1 ];do #I
YYYY=${Y[$I]}
MM=${M[$I]}
DD=${D[$I]}
HH=${H[$I]}

echo $YYYY $MM $DD $HH
CFILE=ECAPE_RH_125-133_29-35_${YYYY}-${MM}-${DD}_${HH}_500m_NO_ENTRAINMENT.nc

IP1=$(expr $I + 1)
#echo "$IP1 $INDIR/$CFILE \\"
echo "$INDIR2/$CFILE \\" >>$OUT
I=$(expr $I + 1)

done #I

echo MMMMM OUT: $OUT

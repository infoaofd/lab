#!/bin/bash

#NUM=02; LAG=MINUS6
#NUM=12; LAG=MINUS3
NUM=32; LAG=PLUS3

P_INIT=500m

TEXT1="$LAG ECAPE_AN $P_INIT RH"
TEXT2="R3HMAX.ge.50PCNT - R3HMAX.le.50PCNT"
TEXT3="RAINDAY QFLUX.ge.50PCNT"

LONW=125;LONE=133;LATS=29;LATN=35
DOMAIN=${LONW}-${LONE}_${LATS}-${LATN}

INDIR1=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT/ECAPE.vs.CAPE/VERTICAL_SECTION/34.12.CDO_COMPOSITE_ECAPE/R3HMAX.ge.50PCNT
INFLE1=14.${NUM}.CDO_COMPOSITE_R3HMAX.ge.50PCNT_${LAG}_ECAPE_RH500m.nc
IN1=${INDIR1}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1;fi

INDIR2=/work09/2021/nakamuro/RAINDAY/N-KYUSHU_R3HMAX/QFLUX.ge.50PCNT/ECAPE.vs.CAPE/VERTICAL_SECTION/34.12.CDO_COMPOSITE_ECAPE/R3HMAX.le.50PCNT
INFLE2=24.${NUM}.CDO_COMPOSITE_R3HMAX.le.50PCNT_${LAG}_ECAPE_RH500m.nc
IN2=${INDIR2}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1;fi

# 850hPaのQFLUXに平行かつ女島を通過する線分
lon1=126.1135789099; lat1=30.8138610227; #西端の経度・緯度
lon2=131.8146305342; lat2=33.6971859662;   #東端の経度・緯度

# MESHIMA
RLON=128.350833333; RLAT=31.9980555556

GS=$(basename $0 .sh)_${LAG}.GS

FIG=$(basename $0 .sh)_${LAG}.pdf ;#eps

LEVS="-120 120 20" 
KIND="-kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta"
FS=2
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}
'sdfopen ${IN1}'; 'sdfopen ${IN2}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set mpdset hires'; 'set grads off'; 'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1'

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set t 1'
'ECAPE1=CAPE'

'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set t 1'
'ECAPE2=CAPE'

'ECAPE=ECAPE1-ECAPE2'


# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'
'd ECAPE'


# SET CONTOUR
'set gxout contour'
'set clab off' ;#LABEL OFF
'set cstyle 1'
'set cthick 12';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 20'   ;#CONTOUR INTERVAL
'set cmin 100'
'set cmax 400'
'd ECAPE2'

'set clab on'
'set cstyle 1'
'set cthick 5'
'set ccolor 1';#BLACK
'set cint 20'
'set cmin 100'
'set cmax 400'
'd ECAPE2'


# plot line
#'trackplot $lon1 $lat1 $lon2 $lat2 -c 4 -l 1 -t 7'
#'markplot $lon1 $lat1 -c 4 -m 3 -s 0.1'
#'markplot $lon2 $lat2 -c 4 -m 3 -s 0.1'

# PLOT MESHIMA POSITION
'markplot ${RLON} ${RLAT} -m 3 -s 0.15 -c 1'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt


# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 2 -line on -edge circle'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'


# TEXT
x=(xl+xr)/2; y=yt+0.18
'set strsiz 0.1 0.12'; 'set string 1 c 4 0'
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2; y=y+0.25
'draw string 'x' 'y' ${TEXT2}'
x=(xl+xr)/2; y=y+0.25
'draw string 'x' 'y' ${TEXT3}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=YUP+1.1
'draw string ' xx ' ' yy ' ${IN1}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${IN2}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF



grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo



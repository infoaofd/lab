#!/bin/bash

#NUM=02; LAG=MINUS6
#NUM=12; LAG=MINUS3
NUM=32; LAG=PLUS3

P_INIT=500m

TEXT1="$LAG ECAPE $P_INIT RH"
TEXT2="RAINDAY QFLUX.ge.50PCNT R3HMAX.ge.50PCNT"

LONW=125;LONE=133;LATS=29;LATN=35
DOMAIN=${LONW}-${LONE}_${LATS}-${LATN}

IN=14.${NUM}.CDO_COMPOSITE_R3HMAX.ge.50PCNT_${LAG}_ECAPE_RH500m.nc
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

GS=$(basename $0 .sh)_${LAG}.GS

FIG=$(basename $0 .sh)_${LAG}.pdf ;#eps

LEVS="100 300 20"
KIND='-kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=2
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"



cat << EOF > ${GS}
'sdfopen ${IN}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set t 1'

'q dims';say result
'set mpdset worldmap'
'd CAPE.1'
'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.18
'draw string 'x' 'y' ${TEXT1}'
x=(xl+xr)/2 ;y=y+0.25
'draw string 'x' 'y' ${TEXT2}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS $CTL

if [ -f $FIG ]; then echo "FIG: $FIG"; fi


echo "DONE $0."
echo

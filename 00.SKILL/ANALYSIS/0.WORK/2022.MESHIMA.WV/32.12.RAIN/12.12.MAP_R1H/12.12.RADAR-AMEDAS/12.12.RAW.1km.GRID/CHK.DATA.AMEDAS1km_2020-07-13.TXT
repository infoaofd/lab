netcdf \3p-analrain_2020-07-13_0900utc {
dimensions:
	LON = 2560 ;
	LAT = 3360 ;
	TIME = 1 ;
variables:
	double LON(LON) ;
		LON:units = "degrees_east" ;
		LON:long_name = "Longitude" ;
	double LAT(LAT) ;
		LAT:units = "degrees_north" ;
		LAT:long_name = "Latitude" ;
	double TIME(TIME) ;
		TIME:units = "seconds since 1970-01-01 00:00:00 +0:00" ;
		TIME:long_name = "Time" ;
	short P60(TIME, LAT, LON) ;
		P60:long_name = "60-minutes precipitation" ;
		P60:units = "1e-3 meter" ;
		P60:missing_value = -32768s ;
		P60:scale_factor = 0.01f ;
		P60:add_offset = 0.f ;

// global attributes:
		:Convention = "COARDS" ;
		:history = "created by jmapp2nc version 2.0" ;
		:comment = "Analized precipitation by JMA" ;
}

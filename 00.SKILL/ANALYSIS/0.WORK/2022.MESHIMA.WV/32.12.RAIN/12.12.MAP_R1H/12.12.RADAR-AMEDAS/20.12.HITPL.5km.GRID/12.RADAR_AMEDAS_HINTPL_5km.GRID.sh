#!/bin/bash

#YYYY=$1;M=$2;D=$3
#YYYY=${YYYY:-2020};M=${M:-7};D=${D:-13}
#MM=$(printf %02d $M); DD=$(printf %02d $D)

# 日付の処理
# YYYY年MM1月DD1日からYYYY2年MM2月DD2日までループさせる

if [ $# -ne 2 ]; then
  echo
  echo Error in $0 : Wrong arguments.
  echo $0 YYYYMMDD1 YYYYMMDD2
  echo
  exit 1
fi

YYYYMMDD1=$1; YYYYMMDD2=$2

YYYY1=${YYYYMMDD1:0:4}; MM1=${YYYYMMDD1:4:2}; DD1=${YYYYMMDD1:6:2}
YYYY2=${YYYYMMDD2:0:4}; MM2=${YYYYMMDD2:4:2}; DD2=${YYYYMMDD2:6:2}

start=${YYYY1}/${MM1}/${DD1}; end=${YYYY2}/${MM2}/${DD2}



jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${YYYY1}/${MM1}/${DD1} ${i}day" +%Y%m%d)
  YYYY=${date_out:0:4}; MM=${date_out:4:2}; DD=${date_out:6:2}


  INDIR=/work02/DATA/RADAR_AMEDAS/$YYYY/JJA
  INLIST=$(ls $INDIR/3p-analrain_${YYYY}-${MM}-${DD}_????utc.nc)

  for INFLE in $INLIST; do
  IN=$INFLE
  if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1; fi
  done #INFLE

  ODIR=/work02/DATA/RADAR_AMEDAS/5km.GRID/$YYYY; mkd $ODIR

cat > MSM_GRID.TXT << EOF
gridtype = lonlat
xsize    = 481
ysize    = 505
xunits = 'degree'
yunits = 'degree'
xfirst   = 120
xinc     = 0.0625
yfirst   = 22.4
yinc     = 0.05
EOF

  for INFLE in $INLIST; do
  IN=$INFLE
  OUT=$ODIR/$(basename $IN .nc)_5km.nc

  echo MMMMM
  echo INPUT: $IN

  cdo remapcon,MSM_GRID.TXT $IN $OUT 

<<COMMENT
水平高解像度なファイルを低解像度なファイルへ変換
https://qiita.com/wm-ytakano/items/6d3ef4aa5d032c516162

remapping from fine to coarse grid
https://code.mpimet.mpg.de/boards/1/topics/562

The bilinar interpolation uses only 4 surrounding grid points. For the bicubic interpolation 16 surrounding points are used.
The conservative remapping with remapcon takes all source grid points into account!
COMMENT

  echo OUTPUT: $OUT
  echo MMMMM
  echo

  done #INFLE


  i=$(expr $i + 1)
done


exit 0

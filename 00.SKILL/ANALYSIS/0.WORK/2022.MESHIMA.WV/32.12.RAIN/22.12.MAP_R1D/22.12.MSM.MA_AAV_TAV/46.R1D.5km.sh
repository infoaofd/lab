#!/bin/bash
# /work03/am/2022.06.ECS.OBS/32.00.RADAR_PRECIP/24.02.JMA_RADAR_P1H
#
#
gs=$(basename $0 .sh).gs
DSET="MSM.MA"

YYYY=$1; MM=$2; DD=$3
YYYY=${YYYY:-2020}; MM=${MM:-07}; DD=${DD:-13}

ALONW=$4; ALONE=$5; ALATS=$6; ALATN=$7 ;#AREA AVERAGE
ALONW=${ALONW:-129.2}; ALONE=${ALONE:-132.1}
ALATS=${ALATS:-32}; ALATN=${ALATN:-34.0}

# printfでも"08"や"09"を有効にする
# https://rcmdnk.com/blog/2019/09/09/computer-bash-zsh/
MM=${MM#0}
DD=${DD#0}

MM=$(printf %02d $MM); DD=$(printf %02d $DD)
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

echo $YYYY $MM $DD $MMM

LONW=127.5; LATS=30; LONE=132.5; LATN=35 ;#MAP RANGE

PLON=128.3508; PLAT=31.998 ;# MESHIMA (128+21/60+3/3600, 31+59/60+53/3600)

hs=00; dh=1
UNIT=[mm/h]

FIGDIR=MSM.MA_5km_FIG; mkd $FIGDIR
FIG=${FIGDIR}/MSM.MA_5km_${YYYY}${MM}${DD}_R1D.pdf #.eps


ctl=41.MSM_MA_R1H.CTL
if [ ! -f $ctl ];then echo NO SUCH FILE,$ctl; exit 1; fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

TIME0=00Z${DD}${MMM}${YYYY}; TIME1=23Z${DD}${MMM}${YYYY}; 

cat <<EOF>$gs
'cc'
'set rgb 99 78 53 36' ;# DARK BROWN

'open ${ctl}'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
#'q dims';say result
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='-levs 20 100 125 150 175 200 225 250'

'set time $TIME0'
say 'MMMMM ACCUMULATED RAIN FROM ${TIME0} TO ${TIME1}'
'TAV=sum(r1h,time=${TIME0},time=${TIME1})'


xmax = 1; ymax = 1
xwid = 6/xmax; ywid = 6/ymax
xmap=1; nmap = 1; ymap = 1

'set grid off'
'set map 99 1 1 1';'set mpdset hires'


xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.4)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.15'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.15'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.15'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130 132' ;# 132'
'set xlevs 'xlevs
'set ylint 1'


'color ' clevs ' -gxout shaded -kind ' kind

'd TAV'

# '04.02.FILL.MISSING.GS'

# DOMAIN OF AREA AVERAGE
'trackplot $ALONW $ALATS $ALONE $ALATS -c 1 -l 1 -t 1'
'trackplot $ALONE $ALATS $ALONE $ALATN -c 1 -l 1 -t 1'
'trackplot $ALONE $ALATN $ALONW $ALATN -c 1 -l 1 -t 1'
'trackplot $ALONW $ALATN $ALONW $ALATS -c 1 -l 1 -t 1'

'markplot $PLON $PLAT -c 2 -m 3 -s 0.07' ;# MESHIMA

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

'set lon ${ALONW}'; 'set lat ${ALATS}'; 
'AAV=aave(TAV,lon=${ALONW},lon=${ALONE},lat=${ALATS},lat=${ALATN})'
'd AAV'
line=sublin(result,1); AAVOUT=subwrd(line,4)
say 'NNNNN AREA AVERAGE = 'AAVOUT' ${ALONW}-${ALONE}, ${ALATS}-${ALATN}'

'set strsiz 0.12 0.14'; 'set string 1 c 2'
x=(xl+xr)/2; y=yt+0.22;'draw string 'x' 'y ' ${DSET} ${DD}${MMM}${YYYY} 'AAVOUT '[mm]' 
x=(xl+xr)/2; y=y+0.30; 'draw string 'x' 'y ' AREA AVERAGE ${ALATS}N-${ALATN}N, ${ALONW}E-${ALONE}E' 

x1=xl-0.5; x2=xr-0.7; y1=yb-0.7; y2=y1+0.1
'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs 1 -ft 1' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
x=x2+0.05; y=(y1+y2)/2;'draw string 'x' 'y ' ${UNIT}' 

x1=xr+0.6; x2=x1+0.1; y1=yb; y2=yt-0.3
'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs 1 -ft 1' 
'set strsiz 0.1 0.12'; 'set string 1 c 2'
x=(x1+x2)/2; y=y2+0.2;'draw string 'x' 'y ' ${UNIT}' 

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
x=0.2; y=yt+0.9
         'draw string 'x' 'y ' ${FIG}' 
y=y+0.2; 'draw string 'x' 'y ' ${CMD}' 
y=y+0.2; 'draw string 'x' 'y ' ${CWD}' 
y=y+0.2; 'draw string 'x' 'y ' ${NOW}' 

'gxprint ${FIG}'

say
say 'DAILY_MEAN ${YYYY} ${MM} ${DD} 'AAVOUT' [mm] ${DSET} ${ALATS}N-${ALATN}N, ${ALONW}E-${ALONE}E $0'
say 
quit
EOF

grads -bcp "${gs}"
#rm -vf $gs

if [ -f ${FIG} ]; then
echo; echo FIG: ${FIG};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${FIG}.;echo; exit 1
fi

exit 0

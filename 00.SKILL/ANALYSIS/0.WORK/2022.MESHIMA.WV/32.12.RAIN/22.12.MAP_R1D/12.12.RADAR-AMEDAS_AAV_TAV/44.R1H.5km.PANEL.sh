#!/bin/bash
# /work03/am/2022.06.ECS.OBS/32.00.RADAR_PRECIP/24.02.JMA_RADAR_P1H
#
#
gs=$(basename $0 .sh).gs

yyyy=$1; mm=$2; dd=$3 # DATE
yyyy=${yyyy:-2020}; mm=${mm:-07}; dd=${dd:-13}

ALONW=$4; ALONE=$5; ALATS=$6; ALATN=$7 ;#DOMAIN OF AREA AVERAGE
ALONW=${ALONW:-129.2}; ALONE=${ALONE:-132.1}
ALATS=${ALATS:-32}; ALATN=${ALATN:-34.0}

LONW=127.5; LATS=30; LONE=132.5; LATN=35 ;#MAP RANGE

PLON=128.3508; PLAT=31.998 ;# MESHIMA (128+21/60+3/3600, 31+59/60+53/3600)


# printfでも"08"や"09"を有効にする
# https://rcmdnk.com/blog/2019/09/09/computer-bash-zsh/
mm=${mm#0}
dd=${dd#0}

mm=$(printf %02d $mm); dd=$(printf %02d $dd); dp=$(expr $dd + 1)

#echo $mm $dd $dp


hs=00; dh=1
UNIT=[mm/h]

FIGDIR=RADAR_AMEDAS_5km_FIG; mkd $FIGDIR
FIG=${FIGDIR}/RADAR_AMEDAS_5km_${yyyy}${mm}${dd}_R1H.pdf #.eps

#echo $FIG


ctl=41.RADAR_AMEDAS_5km.CTL
if [ ! -f $ctl ];then echo NO SUCH FILE,$ctl; exit 1; fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"



cat <<EOF>$gs

'cc'
'set rgb 99 78 53 36' ;# DARK BROWN

'open ${ctl}'


yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif


'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
#'q dims';say result
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='4 80 4'


xmax = 6; ymax = 4

xwid = 8.5/xmax; ywid = 5/ymax

nmap = 1; ymap = 1

hh=${hs}

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grid off'
'set map 99 1 1 1';'set mpdset hires'

datetime1=hh'Z'${dd}''mmm''yyyy

'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say dtcheck' 't1

hp=hh+1

xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.4)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.09'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.09'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.09'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130' ;# 132'
'set xlevs 'xlevs
'set ylint 1'


'color ' clevs ' -gxout shaded -kind ' kind

'd P60'

# DOMAIN OF AREA AVERAGE
'trackplot $ALONW $ALATS $ALONE $ALATS -c 1 -l 1 -t 1'
'trackplot $ALONE $ALATS $ALONE $ALATN -c 1 -l 1 -t 1'
'trackplot $ALONE $ALATN $ALONW $ALATN -c 1 -l 1 -t 1'
'trackplot $ALONW $ALATN $ALONW $ALATS -c 1 -l 1 -t 1'

'markplot $PLON $PLAT -c 2 -m 3 -s 0.05' ;# MESHIMA

# '04.02.FILL.MISSING.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.12
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


title=hh'-'hp'UTC${dd}'mmm''yyyy
'set string 1 l 1 0'; 'set strsiz 0.06 0.08'
'draw string 'xx' 'yy' 'title

'set parea off'; 'set vpage off'
*
if (nmap > 23); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+${dh}

say
endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 0.9 1 -edge circle -line on -fs 2 -ft 2' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 8.05 0.95 ${UNIT}';

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
'draw string 0.2 8.4 ${NOW}'; 'draw string 0.2 8.25 ${CWD}'
'draw string 0.2 8.1 ${CMD}'; 'draw string 0.2 7.9 ${FIG}'

'gxprint ${FIG}'

quit
EOF



grads -bcl "${gs}"
#rm -vf $gs

if [ -f ${FIG} ]; then
echo; echo FIG: ${FIG};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${FIG}.;echo; exit 1
fi

exit 0

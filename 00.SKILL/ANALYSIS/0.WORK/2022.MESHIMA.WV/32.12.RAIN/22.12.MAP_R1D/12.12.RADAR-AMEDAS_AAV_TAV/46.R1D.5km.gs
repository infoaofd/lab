'cc'
'set rgb 99 78 53 36' ;# DARK BROWN

'open 41.RADAR_AMEDAS_5km.CTL'

'set lon 127.5 132.5'; 'set lat 30 35'
#'q dims';say result
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='-levs 20 100 125 150 175 200 225 250'

'set time 00Z30JUN2020'
say 'MMMMM ACCUMULATED RAIN FROM 00Z30JUN2020 TO 23Z30JUN2020'
'TAV=sum(P60,time=00Z30JUN2020,time=23Z30JUN2020)'


xmax = 1; ymax = 1
xwid = 6/xmax; ywid = 6/ymax
xmap=1; nmap = 1; ymap = 1

'set grid off'
'set map 99 1 1 1';'set mpdset hires'


xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.4)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.15'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.15'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.15'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130 132' ;# 132'
'set xlevs 'xlevs
'set ylint 1'


'color ' clevs ' -gxout shaded -kind ' kind

'd TAV'

# '04.02.FILL.MISSING.GS'

# DOMAIN OF AREA AVERAGE
'trackplot 129.2 32 132.1 32 -c 1 -l 1 -t 1'
'trackplot 132.1 32 132.1 34.0 -c 1 -l 1 -t 1'
'trackplot 132.1 34.0 129.2 34.0 -c 1 -l 1 -t 1'
'trackplot 129.2 34.0 129.2 32 -c 1 -l 1 -t 1'

'markplot 128.3508 31.998 -c 2 -m 3 -s 0.07' ;# MESHIMA

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

'set lon 129.2'; 'set lat 32'; 
'AAV=aave(TAV,lon=129.2,lon=132.1,lat=32,lat=34.0)'
'd AAV'
line=sublin(result,1); AAVOUT=subwrd(line,4)
say 'NNNNN AREA AVERAGE = 'AAVOUT' 129.2-132.1, 32-34.0'

'set strsiz 0.12 0.14'; 'set string 1 c 2'
x=(xl+xr)/2; y=yt+0.22;'draw string 'x' 'y ' RADAR-AMEDAS 30JUN2020 'AAVOUT '[mm]' 
x=(xl+xr)/2; y=y+0.30; 'draw string 'x' 'y ' AREA AVERAGE 32N-34.0N, 129.2E-132.1E' 

x1=xl-0.5; x2=xr-0.7; y1=yb-0.7; y2=y1+0.1
'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs 1 -ft 1' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
x=x2+0.05; y=(y1+y2)/2;'draw string 'x' 'y ' [mm/h]' 

x1=xr+0.6; x2=x1+0.1; y1=yb; y2=yt-0.3
'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs 1 -ft 1' 
'set strsiz 0.1 0.12'; 'set string 1 c 2'
x=(x1+x2)/2; y=y2+0.2;'draw string 'x' 'y ' [mm/h]' 

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
x=0.2; y=yt+0.9
         'draw string 'x' 'y ' RADAR_AMEDAS_5km_FIG/RADAR_AMEDAS_5km_20200630_R1D.pdf' 
y=y+0.2; 'draw string 'x' 'y ' ./46.R1D.5km.sh 2020 06 30 129.2 132.1 32 34.0' 
y=y+0.2; 'draw string 'x' 'y ' /work03/2021/nakamuro/32.12.RAIN/22.12.MAP_R1D/12.12.RADAR-AMEDAS_AAV_TAV' 
y=y+0.2; 'draw string 'x' 'y ' Tue, 11 Jul 2023 18:07:50 +0900' 

'gxprint RADAR_AMEDAS_5km_FIG/RADAR_AMEDAS_5km_20200630_R1D.pdf'

say
#say 'DAILY_MEAN 2020 06 30 'AAVOUT' [mm] RADAR-AMEDAS 32N-34.0N, 129.2E-132.1E ./46.R1D.5km.sh'
say 'DAILY_MEAN,DATE=,2020,06,30, AAV=,'AAVOUT',[mm], RADAR-AMEDAS,32N-34.0N, 129.2E-132.1E ./46.R1D.5km.sh'
say 
quit

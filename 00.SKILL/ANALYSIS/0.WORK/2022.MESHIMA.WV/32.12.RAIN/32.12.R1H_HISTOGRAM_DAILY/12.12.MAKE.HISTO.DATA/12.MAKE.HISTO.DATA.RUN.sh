#!/bin/bash

EXE1=22.MSM.MA.R1H_MAKE.HISTO.sh
EXE2=32.RADAR-AMEDAS.R1H_MAKE.HISTO.sh

if [ ! -f $EXE1 ]; then echo ERROR: NO SUCH FILE, $EXE1; exit 1; fi
if [ ! -f $EXE2 ]; then echo ERROR: NO SUCH FILE, $EXE2; exit 1; fi


# DOMAIN OF AREA AVERAGE
DOMAIN="N-KYUSHU01"
ALONW=${ALONW:-129.2}; ALONE=${ALONE:-132.1}
ALATS=${ALATS:-32}; ALATN=${ALATN:-34.0}
AAV_DOMAIN="$ALONW $ALONE $ALATS $ALATN"

# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

yyyymmdd1=$1; yyyymmdd2=$2
yyyymmdd1=${yyyymmdd1:-20200713}; yyyymmdd2=${yyyymmdd2:-20200713}; 

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1};  end=${yyyy2}/${mm2}/${dd2}


jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

TEXTOUT=${DSET}_AAV_TAV_${yyyymmdd1}-${yyyymmdd2}_${ALONW}-${ALONE}_${ALATS}-${ALATN}.TXT
rm -vf $TEXTOUT; touch $TEXTOUT

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  YYYY=${date_out:0:4}; MM=${date_out:4:2}; DD=${date_out:6:2}

  $EXE1 ${YYYY} ${MM} ${DD} ${DOMAIN} ${AAV_DOMAIN}
  if [ $? -ne 0 ];then echo ;echo ERROR in $EXE1; echo; exit 1; fi

  $EXE2 ${YYYY} ${MM} ${DD} ${DOMAIN} ${AAV_DOMAIN}
  if [ $? -ne 0 ];then echo ;echo ERROR in $EXE2; echo; exit 1; fi

  i=$(expr $i + 1)
done

exit 0

import netCDF4  
import numpy as np
import matplotlib.pyplot as plt
import os

DOMAIN="N-KYUSHU01"

#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
YMD="20210831"
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

#MSM=>1
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
INDIR1="/work03/2021/nakamuro/32.12.RAIN/32.12.R1H_HISTOGRAM_DAILY/12.12.MAKE.HISTO.DATA/NC_HISTO/"+DOMAIN+"/MSM"
INFLE1=YMD+"_"+DOMAIN+"_HISTO_MSM.MA.nc"
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

IN1=INDIR1+"/"+INFLE1

# RADAR-AMEDAS=>1
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
INDIR2="/work03/2021/nakamuro/32.12.RAIN/32.12.R1H_HISTOGRAM_DAILY/12.12.MAKE.HISTO.DATA/NC_HISTO/"+DOMAIN+"/RADAR_AMEDAS"
INFLE2=YMD+"_"+DOMAIN+"_HISTO_RADAR-AMEDAS.nc"
#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

IN2=INDIR2+"/"+INFLE2

THOLD=0.3 #THRESHOLD=0.3mm/d


OUTDIR="HISTO"
os.makedirs(OUTDIR,exist_ok=True)
OUTFLE="HISTO_"+"N-KYUSHU02"+"_"+YMD+".pdf"
OUT=OUTDIR+"/"+OUTFLE


nc1 = netCDF4.Dataset(IN1, "r")
r1h_1=nc1.variables["r1h_1d"][:]

print(np.shape(r1h_1))

r1h_1_thold=r1h_1[r1h_1>THOLD]


plt.hist(r1h_1_thold, alpha=0.5, bins=15, range=(0.5,5), 
color='b', log=True, label="MSM", ec='white')


nc2 = netCDF4.Dataset(IN2, "r")
r1h_2=nc2.variables["r1h_1d"][:]

r1h_2_thold=r1h_2[r1h_2>THOLD]

plt.hist(r1h_2_thold, alpha=0.5, bins=15, range=(0.5,5), 
color='white', log=True, label="RADAR-AMeDAS", ec='r')

plt.xlabel("hourly rainfall [mm/h]")
plt.ylabel("frequency")
plt.title(YMD+" N-KYUSHU")

plt.legend(loc="upper right", fontsize=13)

plt.savefig(OUT) 
print("")


print("MMMMM INPUT:  "+IN1)
print("MMMMM INPUT:  "+IN2)
print("MMMMM OUTPUT: "+OUT)


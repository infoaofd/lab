import netCDF4  
import numpy as np
import matplotlib.pyplot as plt

DOMAIN="N-KYUSHU01"
INDIR1="/work03/am/2022.NAKAMURO_FLUX/32.12.RAIN/32.12.R1H_HISTOGRAM_DAILY/12.22.MERGE.P1H.DATA/OUT_MERGE"
INFLE1="MSM_P1H_MERGE_TEST.nc"
IN1=INDIR1+"/"+INFLE1

# RADAR-AMEDAS=>1
INDIR2=INDIR1
INFLE2="RADAR_P1H_MERGE_TEST.nc"

IN2=INDIR2+"/"+INFLE2

THOLD=5 #THRESHOLD=5mm/d


OUT="HISTO_MERGE_TEST"+DOMAIN+".pdf"

nc1 = netCDF4.Dataset(IN1, "r")
r1h_1=nc1.variables["r1h_1d"][:]
r1h_1_thold=r1h_1[r1h_1>THOLD]
plt.hist(r1h_1_thold, alpha=0.5, bins=15, range=(5,80), 
color='b', log=True, label="MSM")


nc2 = netCDF4.Dataset(IN2, "r")
r1h_2=nc2.variables["r1h_1d"][:]
r1h_2_thold=r1h_2[r1h_2>THOLD]
plt.hist(r1h_2_thold, alpha=0.5, bins=15, range=(5,80), 
color='r', log=True, label="RADAR-AMeDAS")

plt.xlabel("P [mm/h]")
plt.ylabel("Frequency")
plt.title(DOMAIN)

plt.legend(loc="upper right", fontsize=13)

plt.savefig(OUT) 
print("")
print("MMMMM INPUT:  "+IN1)
print("MMMMM OUTPUT: "+OUT)

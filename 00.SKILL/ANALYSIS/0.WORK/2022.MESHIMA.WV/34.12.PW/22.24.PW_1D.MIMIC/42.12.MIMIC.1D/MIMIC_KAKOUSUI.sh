ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

INDIR="/work03/2021/nakamuro/34.12.PW/12.02.PW.DATA_1D/OUT_0.MIMIC_NC"
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi

GS=$(basename $0 .sh).GS

CTL="MIMIC.1D.CTL"

YYYYMMDD=$1
YYYYMMDD=${YYYYMMDD:-20210810}

YYYY=${YYYYMMDD:0:4}; MM=${YYYYMMDD:4:2}; DD=${YYYYMMDD:6:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${DD}${MMM}${YYYY}

FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR
FIGFLE=$(basename $0 .sh)_${YYYY}${MM}${DD}_Avg_1D.pdf
#FIGFLE=$(basename $0 .sh).eps

FIG=${FIGDIR}/${FIGFLE}


LONW=120;LONE=135 ; LATS=22.5 ;LATN=38

# 水の密度が1[kg/m^3]だったら、[kg/m^2]は[mm]と同じ
# gitlab.com/infoaofd/lab/-/blob/master/METEO/THERMO/%E8%92%B8%E7%99%BA%E3%81%A8%E6%BD%9C%E7%86%B1%E3%81%AE%E9%96%A2%E4%BF%82.md 
UNIT="mm"
#UNIT="kg/m^2"

KIND='white->lightcyan->deepskyblue->greenyellow->yellow->wheat->orange->tomato->red->crimson->mediumvioletred'
LEVS='10 70 10'


#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

cat <<EOF>$CTL
dset OUT_0.MIMIC_NC/MIMIC_PW_avg_1D_%y4%m2%d2.nc
title MIMIC 1D MEAN
dtype netcdf
undef 1.e20
options template
xdef 1440 linear -180 0.25
ydef  720 linear  -90 0.25
zdef    1 linear 0 1
tdef 99999 linear 00Z01JUN2020 1dy
vars 1
tpw=>tpw 1 y,x PW mm
endvars

EOF


cat <<EOF>$GS

'open ${CTL}'

'sdfopen ${INDIR}/MIMIC_PW_avg_1D_${YYYY}${MM}${DD}.nc'

'cc'
'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set gxout shaded'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd tpw'
'cbarn'
'gxprint ${FIG}'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


exit 0

#!/bin/bash

NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo NO SUCH FILE, $NCL; exit 1; fi


# 日付の処理
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

if [ $# -ne 2 ]; then
  echo
  echo Error in $0 : Wrong arguments.
  echo $0 YYYYMMDD1 YYYYMMDD2
  echo
  exit 1
fi


start=$1
end=$2

YYYYMMDD1=$1
YYYYMMDD2=$2

YYYY1=${YYYYMMDD1:0:4}
  MM1=${YYYYMMDD1:4:2}
  DD1=${YYYYMMDD1:6:2}

YYYY2=${YYYYMMDD2:0:4}
  MM2=${YYYYMMDD2:4:2}
  DD2=${YYYYMMDD2:6:2}

start=${YYYY1}/${MM1}/${DD1}
  end=${YYYY2}/${MM2}/${DD2}

#echo $start
#echo $end


jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

#echo "nday=" $nday



i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${YYYY1}/${MM1}/${DD1} ${i}day" +%Y%m%d)
  YYYY=${date_out:0:4}
  MM=${date_out:4:2}
  DD=${date_out:6:2}

INDIR=/work01/DATA/MIMIC_TPW2
#YYYY=2022; MM=06; DD=02

ODIR=OUT_$(basename $0 .sh); mkdir -vp $ODIR
FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR

INLIST=$(ls $INDIR/*${YYYY}${MM}${DD}*nc)

echo $INLIST

for IN in $INLIST; do

echo MMMMM $IN

OFLE=$(basename $IN .nc)_GRADS.nc
FIGFLE=$(basename $IN .nc)

OUT=$ODIR/$OFLE
FIG=$FIGDIR/$FIGFLE

#echo MMMMM $OUT
#echo MMMMM $FIG

runncl.sh $NCL $IN $OUT $FIG

done


 i=$(expr $i + 1)
done

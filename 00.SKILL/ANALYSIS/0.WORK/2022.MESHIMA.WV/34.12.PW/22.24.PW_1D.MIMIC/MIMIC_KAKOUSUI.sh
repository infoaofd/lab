ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

INDIR="/work03/2021/nakamuro/34.12.PW/22.24.PW_1D.MIMIC/OUT_0.MIMIC_NC"
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi

GS=$(basename $0 .sh).GS

CTL="MIMIC.1D.CTL"

YYYYMMDD=$1
YYYYMMDD=${YYYYMMDD:-20210810}

YYYY=${YYYYMMDD:0:4}; MM=${YYYYMMDD:4:2}; DD=${YYYYMMDD:6:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${DD}${MMM}${YYYY}
echo ${TIME0}


FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR
FIGFLE=$(basename $0 .sh)_${YYYY}${MM}${DD}_Avg_1D.pdf
#FIGFLE=$(basename $0 .sh).eps

FIG=${FIGDIR}/${FIGFLE}


LONW=120;LONE=135 ; LATS=22.5 ;LATN=38

# 水の密度が1[kg/m^3]だったら、[kg/m^2]は[mm]と同じ
# gitlab.com/infoaofd/lab/-/blob/master/METEO/THERMO/%E8%92%B8%E7%99%BA%E3%81%A8%E6%BD%9C%E7%86%B1%E3%81%AE%E9%96%A2%E4%BF%82.md 
UNIT="mm"
#UNIT="kg/m^2"

KIND='white->lightcyan->deepskyblue->greenyellow->yellow->wheat->orange->tomato->red->crimson->mediumvioletred'
LEVS='10 70 10'


#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
cat <<EOF>$GS

'open ${CTL}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'sdfopen ${INDIR}/MIMIC_PW_avg_1D_${YYYY}${MM}${DD}.nc'

'q ctlinfo'; say result

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
'set time $TIME0'

#'set t 1'

'q dims'; say result

'cc'

'set mpdraw on'; 'set mpdset hires'
'set xlint 2'; 'set ylint 1'


say '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd tpw'

#女島の位置に点を打つ(128+21/60+3/3600, 31+59/60+53/3600)
'markplot 128.3508 31.998 -c 0 -m 2 -s 0.10'

say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.8; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off';'set ylab off'

say '### TITLE'
'q dims'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'MIMIC' '1D_avg' '${TIME0}' '



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint ${FIG}'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


exit 0

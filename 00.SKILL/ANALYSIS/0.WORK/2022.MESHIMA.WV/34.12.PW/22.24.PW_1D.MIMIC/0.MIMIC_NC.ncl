; 
; TEST.MTPW2.ncl
; 
; Fri, 21 Apr 2023 08:48:15 +0900
; p5820.bio.mie-u.ac.jp
; /work03/am/2022.MESHIMA.WV/52.12.PW/12.12.MIMIC_PTW2/12.12.TEST.MIMIC_TPW2
; am
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

;IN    = getenv("NCL_ARG_2")
;OUT   = getenv("NCL_ARG_3")


;ファイル名の設定 ;yyyymmddを随時変更する
IN  = systemfunc("ls /work01/DATA/MIMIC_TPW2/*20210810*nc")
OUT = "OUT_0.MIMIC_NC/MIMIC_PW_avg_1D_20210810.nc"


; ファイルを開く
;a=addfile(IN,"r")
a=addfiles(IN,"r")
;print(a)


; ファイル数の長さを持つ次元を左端に加える
;http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Functions/ListSetType
ListSetType(a,"join")


; 変数の代入
tpw=a[:]->tpwGrid
lon=a[:]->lonArr
lat=a[:]->latArr
lon@units="degrees_east"
lat@units="degrees_north"


; 座標などの設定
tpw!0="time"
tpw!1="lat"
tpw!2="lon"
tpw&lat=lat(0,:)
tpw&lon=lon(0,:)
tpw&lat@units="degrees_north"
tpw&lon@units="degrees_east"

lon!0="lon"
lat!0="lat"

printVarSummary(tpw)
printVarSummary(lon)
printVarSummary(lat)
;print(tpw@_FillValue)


;可降水量の日平均を算出
tpw_avg_1D = dim_avg_n_Wrap(tpw,0)
tpw_avg_1D@_FillValue=1.e20
printVarSummary(tpw_avg_1D)


;可降水量の日平均をnetCDFファイルで出力
;http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Functions/addfile


;上書き保存をする
;デフォルトでは同じ名前のファイルは作成できない
system("rm -fv "+OUT)

out = addfile(OUT,"c")
out->tpw = tpw_avg_1D


; 入出力ファイル名を画面に書き出す。
print("")
print("Done " + script_name)
print("")
print("IN: "+IN)
print("")
print("OUT:"+OUT)
print("")


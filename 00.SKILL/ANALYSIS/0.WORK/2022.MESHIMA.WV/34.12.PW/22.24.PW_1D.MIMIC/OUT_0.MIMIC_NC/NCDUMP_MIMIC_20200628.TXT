netcdf MIMIC_PW_avg_1D_20200628 {
dimensions:
	lat = 721 ;
	lon = 1440 ;
variables:
	float tpw(lat, lon) ;
		tpw:_FillValue = 1.e+20f ;
		tpw:units = "mm" ;
		tpw:least_significant_digit = 1. ;
		tpw:average_op_ncl = "dim_avg_n over dimension(s): time" ;
	float lat(lat) ;
		lat:units = "degrees_north" ;
	float lon(lon) ;
		lon:units = "degrees_east" ;
}

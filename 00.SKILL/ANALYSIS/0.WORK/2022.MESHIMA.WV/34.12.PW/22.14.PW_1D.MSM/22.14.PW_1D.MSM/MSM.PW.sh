#!/bin/bash

# Fri, 16 Jun 2023 17:12:16 +0900
# /work03/am/2022.MESHIMA.WV/52.12.PW/32.12.MSM.PW

YYYYMMDD=$1;YYYYMMDD=${YYYYMMDD:=20220815}
YYYY=${YYYYMMDD:0:4}; MM=${YYYYMMDD:4:2}; DD=${YYYYMMDD:6:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=120;LONE=135 ; LATS=22.5 ;LATN=38
TIME=${DD}${MMM}${YYYY}
T1=00Z${DD}${MMM}${YYYY};T2=21Z${DD}${MMM}${YYYY}
LEV="1000 300"

#CTL=MSM-P.CTL
#if [ ! -f $CTL ];then echo ERROR in $: NO SUCH FILE,$CTL;exit1;fi
NC=/work01/DATA/MSM/MSM-P/${YYYY}/${MM}${DD}.nc
if [ ! -f $NC ];then echo ERROR in $: NO SUCH FILE, $NC;exit1;fi

GS=$(basename $0 .sh).GS

FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR
NCODIR=NC_$(basename $0 .sh); mkdir -vp $NCODIR

FIGFLE=PW.MSM.${YYYY}${MM}${DD}.pdf ;#eps
NCOFLE=PW.MSM.${YYYY}${MM}${DD}.nc 

FIG=$FIGDIR/$FIGFLE
NCOUT=$NCODIR/$NCOFLE

LEVS="10 70 10"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->lightcyan->deepskyblue->greenyellow->yellow->wheat->orange->tomato->red->crimson->mediumvioletred'
FS=2
UNIT="mm"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

#'open ${CTL}'
'sdfopen ${NC}'
#'q ctlinfo';say result

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${T1} ${T2}'
#'q dims';say result

# 混合比の計算式
# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TUTORIAL_04_MET_VARS.md
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'
# 'QV=QV*1000' ;#g/kg => kg/kg

# vintの使い方 
# https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A4%CB%A4%C4%A4%A4%A4%C6%A4%CE%A5%E1%A5%E2

'set time ${T1}'
'q dims';line=sublin(result,5);say line
ts=subwrd(line,9)
te=ts+8

say ts' 'te

'set t 'ts; 'q dims';line=sublin(result,5);say subwrd(line,9)

'set z 1'
'PW=vint(lev(z=1),QV,300)'
'PWTAV=PW'

t=ts+1; say t
while ( t < te )

'set t 't; say t
'PW=vint(lev(z=1),QV,300)'
'PWTAV = PWTAV + PW'
 t=t+1
endwhile

#'PW1Davg=PWTAV'
'PW1Davg=PWTAV/8'

# MSMの相対湿度は300hPaまでしかデータがないことに注意

# 可降水量の時間平均
#'set time ${T1}'
#'q dims';say result
#'PW1Davg=ave(PW, t=${T1}, t=${T2})'

'set xlint 2';'set ylint 2'
#'set gxout shade2'
'color ${LEVS} -kind ${KIND} -gxout shaded ' ;# SET COLOR BAR

#'d PW'
'd PW1Davg'

#'set gxout contour'
#'d PW1Davg'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



x=(xl+xr)/2 ;y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' MSM PW 1D_avg ${TIME}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

'set sdfwrite ${NCOUT}'
'sdfwrite PW1Davg'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "FIG: "; ls -lh --time-style=long-iso $FIG; fi
if [ -f $NCOUT ]; then echo "NC:  "; ls -lh --time-style=long-iso $NCOUT; fi
echo


echo "DONE $0."
echo

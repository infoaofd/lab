#!/bin/bash

NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo NO SUCH FILE, $NCL; exit 1; fi

INDIR=/work01/DATA/MIMIC_TPW2
YYYY=2022; MM=06; DD=02

ODIR=OUT_$(basename $0 .sh); mkdir -vp $ODIR
FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR

INLIST=$(ls $INDIR/*${YYYY}${MM}${DD}*nc)

echo $INLIST

for IN in $INLIST; do

echo MMMMM $IN

OFLE=$(basename $IN .nc)_GRADS.nc
FIGFLE=$(basename $IN .nc)

OUT=$ODIR/$OFLE
FIG=$FIGDIR/$FIGFLE

echo MMMMM $OUT
echo MMMMM $FIG

runncl.sh $NCL $IN $OUT $FIG

done

; 
; TEST.MTPW2.ncl
; 
; Fri, 21 Apr 2023 08:48:15 +0900
; p5820.bio.mie-u.ac.jp
; /work03/am/2022.MESHIMA.WV/52.12.PW/12.12.MIMIC_PTW2/12.12.TEST.MIMIC_TPW2
; am
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

;LOG=script+".LOG"

;NOW=systemfunc("date '+%y%m%d_%H%M' ")
;print("NOW="+NOW)
; LOG=script+NOW+".LOG"
HOST=systemfunc("hostname")
CWD=systemfunc("pwd")

IN    = getenv("NCL_ARG_2")
OUT   = getenv("NCL_ARG_3")
FIG   = getenv("NCL_ARG_4")

; ファイル名設定
;IN="comp20220619.000000.nc"
;OUT="PW_20220619_FOR_GRADS.nc"

; ファイルを開く
a=addfile(IN,"r")
;print(a)

;FIG=systemfunc("basename "+IN+" .nc")
TYP="pdf"

;　変数の代入
tpw=a->tpwGrid
lon=a->lonArr
lat=a->latArr
lon@units="degrees_east"
lat@units="degrees_north"

; 座標などの設定
tpw!0="lat"
tpw!1="lon"
tpw&lon=lon
tpw&lat=lat
tpw&lat@units="degrees_north"
tpw&lon@units="degrees_east"


printVarSummary(tpw)
print(tpw@_FillValue)

; ここから作図の部分。変換には直接関係ない
res=True
wks = gsn_open_wks(TYP,FIG)

  res@vpWidthF = 0.6
  res@vpHeightF = 0.6
  res@gsnLeftString   = ""               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = ""

  res@gsnDraw         = False        ; plotを描かない
  res@gsnFrame        = False        ; WorkStationを更新しない
  res@cnLinesOn       = False
  res@cnFillOn        = True
  res@cnInfoLabelOn   = False

  res@cnLevelSelectionMode = "ManualLevels"
  res@cnMinLevelValF  = 0 
  res@cnMaxLevelValF  = 70
  res@cnLevelSpacingF = 5 
  res@cnFillPalette   = "CBR_wet"

  res@mpMinLonF       = 100
  res@mpMaxLonF       = 160
  res@mpMinLatF       = -10
  res@mpMaxLatF       =  50
  res@mpCenterLonF = 140  ; 経度の中心

;; res@gsnMaximize        = True 

  res@gsnRightString  = ""
  res@pmLabelBarOrthogonalPosF = 0.1        ; カラーバーの位置を微調整
  ;;; タイトルや軸の文字の大きさを設定
  res@tmYLLabelFontHeightF = 0.016
  res@tmXBLabelFontHeightF = 0.016
  res@tiMainFontHeightF    = 0.024
  res@lbLabelFontHeightF   = 0.016
  ;;; カラーバーにタイトルをつける
  res@lbTitleOn            = True
  res@lbTitleString        = tpw@units
  res@lbTitlePosition      = "Right"
  res@lbTitleDirection     = "Across" 
  res@lbTitleFontHeightF   = 0.016
  res@pmLabelBarHeightF = 0.05
plot = gsn_csm_contour_map_ce(wks,tpw(:,:),res)

draw(plot)
frame(wks)

;FINFO=systemfunc("ls -lh --time-style=long-iso "+script_name)
;LOG_HEADER = (/"# "+ NOW, "# "+ HOST, "# "+ CWD, "# "+ FINFO /)
;hlist=[/LOG_HEADER/]
;write_table(LOG, "w", hlist, "%s")
; ここまで作図の部分。変換には直接関係ない


; grads用netCDFファイルの出力
; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?%E3%83%87%E3%83%BC%E3%82%BF%E3%81%AE%E8%AA%AD%E3%81%BF%E6%9B%B8%E3%81%8D
print("MMMMM OUTPUT")
out = addfile(OUT,"c")
out->tpw = tpw



; 入出力ファイル名を画面に書き出す。
print("")
print("Done " + script_name)
print("IN: "+IN)
print("FIG:"+FIG+"."+TYP)
print("")
print("OUT:"+OUT)
print("")
;print("LOG: "+LOG)
;print("")

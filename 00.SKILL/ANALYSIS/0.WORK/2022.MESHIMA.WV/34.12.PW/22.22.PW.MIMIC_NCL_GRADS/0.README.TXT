/work03/am/2022.MESHIMA.WV/12.12.TEST.MIMIC_TPW2
Fri, 21 Apr 2023 08:43:32 +0900



GRADSでの作図法
実際にはGrADSスクリプトを作成した方がよい

am@p5820
/work03/am/2022.MESHIMA.WV/52.12.PW/12.12.MIMIC_PTW2/12.22.MIMIC_NCL_GRADS
2023-05-31_16-34
$ grads -bcp
Note: -c option was specified, but no command was provided

Grid Analysis and Display System (GrADS) Version 2.2.1
Copyright (C) 1988-2018 by George Mason University
GrADS comes with ABSOLUTELY NO WARRANTY
See file COPYRIGHT for more information

Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile
Issue 'q config' and 'q gxconfig' commands for more detailed configuration information
GX Package Initialization: Size = 8.5 11 
Running in Batch mode
ga-> sdfopen PW_20220619_FOR_GRADS.nc 
Scanning self-describing file:  PW_20220619_FOR_GRADS.nc
SDF file has no discernable time coordinate -- using default values.
SDF file PW_20220619_FOR_GRADS.nc is open as file 1
LON set to 0 360 
LAT set to -90 90 
LEV set to 0 0 
Time values set: 1:1:1:0 1:1:1:0 
E set to 1 1 
ga-> d tpw
Contouring: 10 to 100 interval 10 
ga-> gxprint PW_20220619_FOR_GRADS.PDF
Created PDF file PW_20220619_FOR_GRADS.PDF
ga-> set lon 100 160
LON set to 100 160 
ga-> set lat -20 60
LAT set to -20 60 
ga-> set gxout shaded
ga-> d tpw
Contouring: 10 to 100 interval 10 
ga-> cbarn
ga->  gxprint PW_20220619_FOR_GRADS.PDF
Created PDF file PW_20220619_FOR_GRADS.PDF
ga-> cc
ga-> d tpw
Contouring: 10 to 100 interval 10 
ga-> cbarn
ga-> gxprint PW_20220619_FOR_GRADS.PDF
Created PDF file PW_20220619_FOR_GRADS.PDF

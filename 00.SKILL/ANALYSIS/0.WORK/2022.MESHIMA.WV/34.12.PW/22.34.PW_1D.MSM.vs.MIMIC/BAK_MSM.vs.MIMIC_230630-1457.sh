#!/bin/bash

# Fri, 16 Jun 2023 17:12:16 +0900
# /work03/am/2022.MESHIMA.WV/52.12.PW/32.12.MSM.PW

YYYYMMDD=$1; YYYYMMDD=${YYYYMMDD:=20210810}
YYYY=${YYYYMMDD:0:4}; MM=${YYYYMMDD:4:2}; DD=${YYYYMMDD:6:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

HHLIST="00 03 06 09 12 15 18 21"


for HH in $HHLIST; do


LONW=120;LONE=135 ; LATS=22.5 ;LATN=38
TIME=${HH}Z${DD}${MMM}${YYYY}
LEV="1000 300"

NC1=/work01/DATA/MSM/MSM-P/${YYYY}/${MM}${DD}.nc
if [ ! -f $NC1 ];then echo ERROR in $: NO SUCH FILE, $NC1;exit1;fi

INDIR="/work03/2021/nakamuro/34.12.PW/22.22.PW.MIMIC_NCL_GRADS/OUT_0.MIMIC_NCL_GRADS"
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi
NC2=${INDIR}/comp${YYYY}${MM}${DD}.${HH}0000_GRADS.nc
if [ ! -f $NC2 ];then echo ERROR in $: NO SUCH FILE, $NC2;exit1;fi

GS=$(basename $0 .sh).GS

FIGDIR=FIG_$(basename $0 .sh); mkdir -vp $FIGDIR
FIGFLE=PW.MSM-MIMIC.${YYYY}${MM}${DD}_${HH}.pdf ;#eps
FIG=${FIGDIR}/${FIGFLE}


KIND='-kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
LEVS='-20 20 1'
FS=5
UNIT="mm"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${NC1}'; 'sdfopen ${NC2}'; 

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off'


'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'
'set time ${TIME}'
'q dims';say result

'set dfile 1'
# 混合比の計算式
# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TUTORIAL_04_MET_VARS.md
'tc=(temp.1-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh.1) + pow((2.5+0.007*tc)*(1-0.01*rh.1),3) + (15.9+0.117*tc)*pow((1-0.01*rh.1),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'
# 'QV=QV*1000' ;#g/kg => kg/kg

# vintの使い方 
# https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A4%CB%A4%C4%A4%A4%A4%C6%A4%CE%A5%E1%A5%E2

'set z 1'
'PWMSM=vint(lev(z=1),QV,300)'
# MSMの相対湿度は300hPaまでしかデータがないことに注意


'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set t 1';'set z 1'
'q dims';say result
# 内挿 (regridding)
# https://seesaawiki.jp/ykamae_grads-note/d/GrADS2%A4%DE%A4%C8%A4%E1#content_3
'MIRGRD=lterp(tpw.2(t=1), PWMSM(t=1), bilin)'

'PWDIFF=PWMSM-MIRGRD'

'set xlint 2';'set ylint 2'
'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'd PWDIFF' ;#MIRGRD'

'set xlab off';'set ylab off'

'set gxout contour'
'set cint 10';'set clab off'
'set cthick 10';'set ccolor 0'
'd PWMSM'

'set cint 10';'set clab on';'set clskip 2'
'set cthick 1';'set ccolor 1'
'd PWMSM'

'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -edge circle'
x=x2+0.1; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



x=(xl+xr)/2 ;y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' PW MSM-MIMIC ${HH}UTC${DD}${MMM}${YYYY}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = yt+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


echo "DONE $0."
echo


done #HH


exit 0 

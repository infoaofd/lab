ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

INDIR="../22.22.PW.MIMIC_NCL_GRADS/OUT_0.MIMIC_NCL_GRADS"
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR;exit 1;fi

GS=$(basename $0 .sh).GS

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2021081000}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}
HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${HH}Z${DD}${MMM}${YYYY}

FIG=$(basename $0 .sh)_${YYYY}${MM}${DD}_${HH}.pdf
#FIG=$(basename $0 .sh).eps


LONW=120;LONE=135 ; LATS=22.5 ;LATN=38
UNIT="mm" ;"kg/m^2"
# 

KIND='white->lightcyan->deepskyblue->greenyellow->yellow->wheat->orange->tomato->red->crimson->mediumvioletred'
LEVS='10 70 10'

#MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM

cat <<EOF>$GS

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye


'sdfopen ${INDIR}/comp${YYYY}${MM}${DD}.${HH}0000_GRADS.nc'

'q ctlinfo'; say result

'set lon $LONW $LONE'; 'set lat $LATS $LATN'
#'set time $TIME0'

'set t 1'

'q dims'; say result

'cc'

'set mpdraw on'; 'set mpdset hires'
'set xlint 2'; 'set ylint 1'


say '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd tpw'

say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.8; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off';'set ylab off'

say '### TITLE'
'q dims'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'MIMIC $TIME0' kakousui_CNTL01'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG



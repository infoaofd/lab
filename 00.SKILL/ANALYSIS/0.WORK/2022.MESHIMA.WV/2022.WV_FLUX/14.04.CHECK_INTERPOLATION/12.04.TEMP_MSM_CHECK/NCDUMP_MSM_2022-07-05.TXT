netcdf \0705 {
dimensions:
	lon = 241 ;
	lat = 253 ;
	p = 16 ;
	time = 8 ;
variables:
	float lon(lon) ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:standard_name = "longitude" ;
	float lat(lat) ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:standard_name = "latitude" ;
	float p(p) ;
		p:long_name = "pressure level" ;
		p:standard_name = "air_pressure" ;
		p:units = "hPa" ;
	float time(time) ;
		time:long_name = "time" ;
		time:standard_name = "time" ;
		time:units = "hours since 2022-07-05 00:00:00+00:00" ;
	double z(time, p, lat, lon) ;
		z:long_name = "geopotential height" ;
		z:units = "m" ;
		z:standard_name = "geopotential_height" ;
	double w(time, p, lat, lon) ;
		w:long_name = "vertical velocity in p" ;
		w:units = "Pa/s" ;
		w:standard_name = "lagrangian_tendency_of_air_pressure" ;
	short u(time, p, lat, lon) ;
		u:scale_factor = 0.006116208155 ;
		u:add_offset = 0. ;
		u:long_name = "eastward component of wind" ;
		u:units = "m/s" ;
		u:standard_name = "eastward_wind" ;
	short v(time, p, lat, lon) ;
		v:scale_factor = 0.006116208155 ;
		v:add_offset = 0. ;
		v:long_name = "northward component of wind" ;
		v:units = "m/s" ;
		v:standard_name = "northward_wind" ;
	short temp(time, p, lat, lon) ;
		temp:scale_factor = 0.002613491379 ;
		temp:add_offset = 255.4004974 ;
		temp:long_name = "temperature" ;
		temp:units = "K" ;
		temp:standard_name = "air_temperature" ;
	short rh(time, p, lat, lon) ;
		rh:scale_factor = 0.002293577883 ;
		rh:add_offset = 75. ;
		rh:long_name = "relative humidity" ;
		rh:units = "%" ;
		rh:standard_name = "relative_humidity" ;

// global attributes:
		:Conventions = "CF-1.0" ;
		:history = "created by create_1daync_msm_p.rb  2022-07-06" ;
}

GS=$(basename $0 .sh).GS

YYYY=2020; MM=07; MMM=JUL; 
DDS=01; DDE=10
HH=00
LONW=128.3; LATS=32
LEV=1000

DATES=${DDS}${MMM}${YYYY}
DATEE=${DDE}${MMM}${YYYY}
FIG=$(basename $0 .sh)_${LEV}_${DATES}.eps

cat <<EOF>$GS
'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon $LONW'
'set lat $LATS'
'set lev $LEV'
'set time 00Z$DATES 00Z$DATEE'

'q dims'
say result

'tc=(temp-273.15)'

'es= 6.112*exp((17.67*tc)/(tc+243.5))' ;# Eq.10 of Bolton (1980)

'e=0.01*rh*es'               ;# Eq.4.1.5 (p. 108) of Emanuel (1994)

'mixr= 0.62197*(e/(lev-e))*1000'   ;# Eq.4.1.2 (p.108) of Emanuel(1994) 


'cc'

'set vrange 5 20'
'd mixr'

'draw title QV${LEV} ${LATS}N ${LONW}E'
'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"

ls -lh $FIG


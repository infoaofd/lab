'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon 138.5'
'set lat 35.5'
'set lev 850'
'set time 00Z13FEB2014 00Z15FEB2014'

'q dims'
say result

'cc'
'set mpdset hires'
#'rgbset2'
#'set cmin -10';'set cmax 10'
'set vrange -5 5'
'd temp-273.15'

'draw title T850 35.5N 138.5E'
'gxprint PL.MSM_T850_TSR_T850_13FEB2014.eps'

'quit'

GS=$(basename $0 .sh).GS

YYYY=2014; MM=02; MMM=FEB; 
DDS=13; DDE=15
HH=00
LONW=138.5; LATS=35.5
LEV=850

DATES=${DDS}${MMM}${YYYY}
DATEE=${DDE}${MMM}${YYYY}
FIG=$(basename $0 .sh)_T${LEV}_${DATES}.eps

cat <<EOF>$GS
'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon $LONW'
'set lat $LATS'
'set lev $LEV'
'set time 00Z$DATES 00Z$DATEE'

'q dims'
say result

'cc'
'set mpdset hires'
#'rgbset2'
#'set cmin -10';'set cmax 10'
'set vrange -5 5'
'd temp-273.15'

'draw title T${LEV} ${LATS}N ${LONW}E'
'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"

ls -lh $FIG


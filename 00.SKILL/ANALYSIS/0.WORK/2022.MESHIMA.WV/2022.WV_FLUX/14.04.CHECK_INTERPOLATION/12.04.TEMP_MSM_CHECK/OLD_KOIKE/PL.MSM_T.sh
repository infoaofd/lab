GS=$(basename $0 .sh).GS

YYYY=2014; MM=02; MMM=FEB; DD=14; HH=00
LONW=137; LONE=141
LATS=34;  LATN=38
LEV=850

DTIME=${HH}Z${DD}${MMM}${YYYY}
FIG=$(basename $0 .sh)_T${LEV}_${DTIME}.eps

cat <<EOF>$GS
'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $DTIME'

'q dims'
say result

'cc'
'set mpdset hires'
#'rgbset2'
#'set cmin -10';'set cmax 10'
'd temp-273.15'

'markplot 138.5 35.6 -c 2 -m 3 -s 0.25'

'draw title T${LEV} $DTIME'
'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"

ls -lh $FIG


#'open MSM-P.CTL'
'sdfopen /work01/DATA/MSM/MSM-P/2022/0705.nc'
'q ctlinfo'
say result

'set lon 128.350833'
'set lat 31.998056'
'set lev 1000'
'set time 00Z05JUL2022 21Z05JUL2022'

'q dims'
say result

'tc=(temp-273.15)'

'es= 6.112*exp((17.67*tc)/(tc+243.5))' ;# Eq.10 of Bolton (1980)

'e=0.01*rh*es'               ;# Eq.4.1.5 (p. 108) of Emanuel (1994)

'mixr= 0.62197*(e/(lev-e))*1000'   ;# Eq.4.1.2 (p.108) of Emanuel(1994) 


'cc'

'set vrange 10 32'
'd tc'

'draw title SDFOPEN T1000 31.998056N 128.350833E'
'gxprint MSM_TEMP_CHK2_1000_05JUL2022_05JUL2022.eps'


'quit'

#!/bin/bash

#-----------------------------------------------------------------
# Set up parameters
#
prefix=MESHIMA2022
traj=0000
#prefix=YAMAGATA2013
#traj=115581

in_dir_trajectory=.
in_file_trajectory=${in_dir_trajectory}/${prefix}_${traj}.csv

in_dir_msm_p=/work01/DATA/MSM/MSM-P
in_dir_msm_s=/work01/DATA/MSM/MSM-S

yr0=2022
mo0=07
dy0=01
hr0=00

out_dir_stn=output/${prefix}_${traj}
#-----------------------------------------------------------------



export PATH=$PATH:.

exe=$(basename $0 _run.sh)
if [ ! -f $exe ]; then
  echo Error in $0: No such file, $0
  exit 1
fi


namelist=${exe}.namelist.txt


if [ ! -f $in_file_trajectory ]; then
  echo
  echo Error in $0: No such file:
  echo $in_file_trajectory
  echo
  exit 1
fi

if [ ! -d $in_dir_msm_p ]; then
  echo
  echo Error in $0: No such directory, $in_dir_msm_p
  echo
  exit 1
fi

if [ ! -d $in_dir_msm_s ]; then
  echo
  echo Error in $0: No such directory, $in_dir_msm_s
  echo
  exit 1
fi


mkdir -vp $out_dir_stn



cat <<EOF >$namelist
&input
prefix="${prefix}_${traj}",
in_file_trajectory="${in_file_trajectory}",
in_dir_msm_p="${in_dir_msm_p}",
in_dir_msm_s="${in_dir_msm_s}",
yr0=${yr0}
mo0=${mo0}
dy0=${dy0}
hr0=${hr0}
&end

&output
out_dir_stn="${out_dir_stn}"
&end
EOF

$exe < $namelist

echo
echo Directory of the output files: ${out_dir_stn}
echo

echo
echo Done ${exe}.
echo

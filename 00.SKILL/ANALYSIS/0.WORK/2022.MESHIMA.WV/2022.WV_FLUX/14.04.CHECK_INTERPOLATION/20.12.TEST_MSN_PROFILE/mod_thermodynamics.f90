module mod_thermodynamics
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/Sato.Kodama.Manda2015/MSM.Profiles.Trajectory
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 13:22 on 01-23-2015.

  use mod_parameter
!  implicit none

contains

subroutine potential_temperature(pt, tk, p)
!
  implicit none

  real,intent(out)::pt
! pt: potential temperature (kelvin)

  real,intent(in)::tk,p
! tk: absolute temperature (kelvin)
! p : pressure (hPa)
  real,parameter::  kappa=287.0/1004.0
! Rd=287 J K-1 kg-1
! Cp=1004 J K-1 kg-1

  real,parameter::p0=1000.0
! p0: pressue (hPa)

! Shimizus
  real,parameter::L=2.5*1.e6
  real,parameter::Rv=461.0

  pt=tk*(p0/p)**kappa

end subroutine potential_temperature



subroutine virtual_potential_temperature(vpt, tk, p, rh)
!
  implicit none

  real,intent(out)::vpt
! pt: virtual potential temperature (kelvin)

   real,intent(in)::tk,p,rh
! tk: absolute temperature (kelvin)
! p : pressure (hPa)
! rh: Relative Humidity (%)

  real,parameter::  kappa=287.0/1004.0
! Rd=287 J K-1 kg-1
! Cp=1004 J K-1 kg-1

  real,parameter::p0=1000.0
! p0: pressue (hPa)

  real pt !Potential Temp.
  real tc !air temp. in Celsius
  real es !saturated water vapor pressure
  real e  !water vapor pressure
  real w  !Mixing Ratio

  if(RH <= 0.0)then 
    vpt=-999.
    return
  endif

  pt=tk*(p0/p)**kappa

! saturated water vapor pressure by Tetens (1930)'s formulae
  tc=tk-273.15
  es=611.0/100.*10**((7.5*tc)/(237.3+tc)) ![hPa] 

  e=rh/100*es

! water vapor mixing ratio in kg/kg
  w=0.622*e/(p-e)

  vpt=pt*(1.0 + 0.61*w)

end subroutine virtual_potential_temperature



subroutine water_vapor(w, q, tk, p, rh)
!
  implicit none

  real,intent(out)::w,q
! w: water vapor mixing ratio in kg/kg
! q: specific humidity in kg/kg

   real,intent(in)::tk,p,rh
! tk: absolute temperature (kelvin)
! p : pressure (hPa)
! rh: Relative Humidity (%)

  real es !saturated water vapor pressure
  real e  !water vapor pressure

  real tc !air temperature in Celsius

  if(RH <= 0.0)then 
    w=-999.
    q=-999.
    return
  endif

! saturated water vapor pressure by Tetens (1930)'s formulae
  tc=tk-273.15
  es=611.0/100.*10**((7.5*tc)/(237.3+tc)) ![hPa] 

  e=rh/100*es

  w=0.622*e/(p-e)

  q=w/(1.0+w)

end subroutine water_vapor


subroutine ept_bolton80_RH(ept, tk, p, RH)
! Author: am

! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 11:29 on 11-02-2012.
!
! References:
! Bolton, D., 1980: The computation of equivalent potential
!   Temperature, Monthly Weather Review, 108, 1046-1053.
!  url: http://www.rsmas.miami.edu/users/pzuidema/Bolton.pdf
!
!  "The most accurate formula to data is from Bolton (1980).
!  It computes EPT to accuracy, relative to numerical 
!  solutions of the governing equation, better than 0.02 K."
!                                     Davies-Jones (MWR, 2009)
  implicit none

  real,intent(out)::ept
! ept: equivalent potential temperature (kelvin)

  real,intent(in)::tk,p,RH
! tk: absolute temperature (kelvin)
! p : pressure (hPa)
! RH : relative humidity (%)

 real r,u,td
! r : water-vapor mixing ratio (g/kg)
! u : relative humidity (%)

  real,parameter::p0=1000.0
! p0: pressue (hPa)

! Shimizus
  real,parameter::L=2.5*1.e6
  real,parameter::Rv=461.0
! L : latent heat
! Rv: gas constant

 real tl, e
! tl: Absolute temperature at lifting condensation level (LCL)
!     (kelvin)
!  e: water vapor pressure (hPa)

  real pt
! pt : potential temparature
  real pow,arg1,arg2,exp1, denom
  real A, es
  real numer, C !

  if(RH <= 0.0)then 
    ept=-999.
    return
  endif

    u=RH

    denom=1.0/(tk - 55.0) - log(u/100.0)/2840.0
    tl = 1.0/denom + 55.0

    A = (tk- 273.2)/(273.2*tk)
    es = 6.11*exp(A*L/Rv)  
    e = u * es * 0.01
    r = (0.622 * e/ (p - e))*1.E3 ! mixing ratio g/kg
    
  pow=0.2854*(1.0 - 0.28*0.001*r)

! Eq.(43) of B80.
  arg1 = 3.376/tl - 0.00254
  arg2 = r*(1.0 + 0.81 * 1.0E-3*r)

  exp1=exp( arg1 * arg2 )

  pt=tk*(p0/p)**pow

  ept=tk*(p0/p)**pow * exp1


! Eq. (6.3) of Davies-Jones (MWR, 2009)
!  numer = (2.771*1.E6 - 1109.0*(tl - 273.15))*r*1.E-3
!  denom = 1005.7*tl
!  ept=pt*exp(numer/denom)


! Eq. (2.5) of Davies-Jones (MWR, 2009)
! ept = pt*exp((2.690*1.E6 * 1.0E-3 * r)/(1005.7*tl) )
!  write(*,'(a)')'Done subroutine ept_b80.'
!  write(*,*) 
end subroutine ept_bolton80_RH

!
! Check result
!
! http://hurri.kean.edu/~yoh/calculations/diagnostic/
!
! P=900 mb
! T=300 K
! r=23.22014 g/kg
! pt=309.1584
! tl=298.16249 K
! ept=380.47828 K
!
!
! [2012年 11月  2日 金曜日 18:06:36 JST]
! [am@aofd30 processor=x86_64]
! [/work1/am/WRF/Test_EPT_Bolton/src]
! $ run_ept_bolton80 < in.txt
! Program run_ept_bolton80 starts.
!
! Reading input data from the standard input using namelist.
! tk =    300.0000      [K]
! p =    900.0000      [hPa]
! r =    23.22014      [g/kg]
! td =   -999.0000      [K]
! u =   -999.0000      [%]
! fillvalue =   -999.0000
! ! NB:
! ! If td or u equals fillvalue, it is not used for
! ! computation of equivalent potential temperature.
!
! Mixing ratio, r is used.
!
! ept =    380.4848      [K]
!
!Done program run_ept_bolton80.


! p=975 hPa
! tk=290 K
! r=10 g/kg
! ept=320.71316 K
!
! [2012年 11月  2日 金曜日 18:09:37 JST]
! [am@aofd30 processor=x86_64]
! [/work1/am/WRF/Test_EPT_Bolton/src]
! $ run_ept_bolton80 < in.txt
! Program run_ept_bolton80 starts.
! 
!  Reading input data from the standard input using namelist.
!  tk =    290.0000      [K]
!  p =    975.0000      [hPa]
!  r =    10.00000      [g/kg]
!  td =   -999.0000      [K]
!  u =   -999.0000      [%]
!  fillvalue =   -999.0000
!  ! NB:
!  ! If td or u equals fillvalue, it is not used for
!  ! computation of equivalent potential temperature.
! 
!  Mixing ratio, r is used.
! 
!  ept =    320.7140      [K]
! 
! Done program run_ept_bolton80.


! p=500 hPa
! tk=275 K
! r = 2 g/kg
! ept=342.47232 K
![2012年 11月  2日 金曜日 18:12:13 JST]
![am@aofd30 processor=x86_64]
![/work1/am/WRF/Test_EPT_Bolton/src]
!$ run_ept_bolton80 < in.txt
!Program run_ept_bolton80 starts.
!
! Reading input data from the standard input using namelist.
! tk =    275.0000      [K]
! p =    500.0000      [hPa]
! r =    2.000000      [g/kg]
! td =   -999.0000      [K]
! u =   -999.0000      [%]
! fillvalue =   -999.0000
! ! NB:
! ! If td or u equals fillvalue, it is not used for
! ! computation of equivalent potential temperature.
!
! Mixing ratio, r is used.
!
! ept =    342.4699      [K]
!
!Done program run_ept_bolton80_RH

end module

module mod_timer

integer:: yr0,mo0,dy0,hr0,mi0,sc0 !Origin of time
real(8):: origin_day

contains

subroutine set_time_origin(yr0,mo0,dy0,hr0,mi0,sc0,origin_day)
  integer,intent(in):: yr0,mo0,dy0,hr0,mi0,sc0
  real(8),intent(out)::origin_day
  origin_day= dble(julday(yr0,mo0,dy0)) &
&           + dble(hr0)/24.d0+dble(mi0)/1440.d0+dble(sc0)/(86400.d0)

  return
end subroutine


subroutine date_time(t,origin_day,yr,mo,dy,hr,mi,sc)
  implicit none
  real,intent(in)::t
  real(8),intent(in)::origin_day
  integer,intent(inout)::yr,mo,dy,hr,mi,sc
  integer julian_day
  real(8):: elapsed_day, timed, r_time_hr, r_time_mi, r_time_sc
  real(8),parameter::eps=1.D-6

  elapsed_day=origin_day+dble(t)/86400.d0+eps
  julian_day=int(elapsed_day)

! year, month, day
  call caldat(julian_day,yr,mo,dy)
  timed=dble(julian_day)

! hour
  r_time_hr=(elapsed_day-timed)*24.d0
  hr=int(r_time_hr)

! minute
  r_time_mi=(r_time_hr-dble(hr))*60.d0
  mi=int(r_time_mi)

! seccond
  r_time_sc=int((r_time_mi-dble(mi))*60.d0)
  sc=int(r_time_sc)

end subroutine date_time



double precision function elapsed_time&
& (yr0,mo0,dy0,hr0,mi0,sc0, yr,mo,dy,hr,mi,sc)
  integer,intent(in)::yr0,mo0,dy0,hr0,mi0,sc0, yr,mo,dy,hr,mi,sc

  integer julian0, julian
  double precision,parameter::d2h=24.d0, m2h=1.d0/60.d0, s2h=1.d0/3600.d0
  double precision timeh0, timeh

  julian0=julday(yr0,mo0,dy0)
  julian =julday(yr ,mo ,dy )

  timeh0=dble(julian0)*d2h + dble(hr0) + dble(mi0)*m2h + dble(sc0)*s2h
  timeh =dble(julian )*d2h + dble(hr ) + dble(mi )*m2h + dble(sc )*s2h

  elapsed_time=timeh-timeh0

! https://sites.google.com/site/afcanalysis/home/time/eltime
end function


INTEGER FUNCTION JULDAY(IYYY,MONTH,DD) 

      INTEGER:: IYYY,MONTH,DD
!                                                                       
! NAME   IN/OUT DESCRIPTION                                             
!                                                                       
! IYYY     I    YEAR                                                    
! MONTH    I    MONTH (1 TO 12)                                         
! DD       I    DAY OF MONTH                                            
! JULDAY   O    JULIAN DAY                                              
!                                                                       
      INTEGER IGREG 
      PARAMETER (IGREG = 15 + 31*(10 + 12*1582)) 
      INTEGER JY,JM,JA 
!                                                                       
      IF (IYYY .LT. 0) IYYY = IYYY + 1 
      IF (MONTH .GT. 2) THEN 
        JY = IYYY 
        JM = MONTH + 1 
      ELSE 
        JY = IYYY - 1 
        JM = MONTH + 13 
      ENDIF 
      JULDAY = INT(365.25*JY) + INT(30.6001*JM) + DD + 1720995 
      IF (DD + 31*(MONTH + 12*IYYY) .GE. IGREG) THEN 
        JA = INT(0.01*JY) 
        JULDAY = JULDAY + 2 - JA + INT(0.25*JA) 
      ENDIF 
      RETURN 

end function julday



subroutine caldat(JULIAN,IYYY,MONTH,DD) 
      INTEGER,intent(in):: JULIAN
      integer,intent(out)::IYYY,MONTH,DD 

! NAME   IN/OUT DESCRIPTION                                             
!                                                                       
! JULIAN   I    THE JULIAN DAY                                          
! IYYY     O    THE YEAR                                                
! MONTH    O    THE MONTH (1 TO 12)                                     
! DD       O    THE DAY OF THE MONTH                                    
!                                                                       
      INTEGER IGREG 
      PARAMETER (IGREG=2299161) 
      INTEGER JALPHA,JA,JB,JC,JD,JE 
!                                                                       
      IF (JULIAN .GE. IGREG) THEN 
        JALPHA = INT(((JULIAN - 1867216) - 0.25)/36524.25) 
        JA = JULIAN + 1 + JALPHA - INT(0.25*JALPHA) 
      ELSE 
        JA = JULIAN 
      ENDIF 
      JB = JA + 1524 
      JC = INT(6680. + ((JB - 2439870) - 122.1)/365.25) 
      JD = 365*JC + INT(0.25*JC) 
      JE = INT((JB - JD)/30.6001) 
      DD = JB - JD - INT(30.6001*JE) 
      MONTH = JE - 1 
      IF (MONTH .GT. 12) MONTH = MONTH - 12 
      IYYY = JC - 4715 
      IF (MONTH .GT. 2) IYYY = IYYY - 1 
      IF (IYYY .LE. 0) IYYY = IYYY - 1 
      RETURN 
end subroutine caldat

end module

module mod_msmp_var
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:36 on 12-04-2012.

!  use
!  implicit none

!  write(*,'(a)')'Module: module_msmp_var'
!  write(*,*)''
!  private
! ncid:ファイルのID番号、 varid: 変 数のID番号
  integer,public :: ncid,varid
  integer,public :: stat

  integer,public,parameter :: plon=241, plat=253, ptime=8, ppres=16
  real,public,parameter :: dlon=0.1250, dlat=0.1000
  real,public,parameter :: dtime=3.0

! 3D grid data at the pressure level
! MSM gridded data
  type, public :: msmp
    character(len=50):: var
    real,dimension (plon) :: lon
    real,dimension (plat) :: lat
    real,dimension (ppres) :: p
    real,dimension (ptime) :: time

    real,dimension(plon, plat, ppres, ptime) :: z, w, u, v, temp, rh

  end type msmp

! Station data as functions of time and pressure level
! Interpolated in space on each pressure level
  type, public :: msmp_vp
    character(len=50):: var
    real,dimension (ppres) :: p
    real,dimension (ptime) :: time

    real,dimension(ppres, ptime) :: z, w, u, v, temp, rh

  end type msmp_vp

! Station data at pressure levels
! Interpolated both space in time
  type, public :: msmp_stn
    character(len=50):: var
    real,dimension (plon) :: lon
    real,dimension (plat) :: lat
    real,dimension (ppres) :: p
    real,dimension (ptime) :: time

    real,dimension(ppres) :: z, w, u, v, temp, rh, &
    pt,vpt,ept, wmix,q

  end type msmp_stn



contains

subroutine read_msmp_vars(varname, varout)

  include 'netcdf.inc'

  character(len=*),intent(in)::varname
  real,dimension(:,:,:,:),intent(inout)::varout

  integer(2),dimension(plon,plat,ppres,ptime)::varout_int
  real(8),dimension(plon,plat,ppres,ptime)::varout_dbl

  integer,allocatable :: dimids(:),nshape(:)
  character(len=70) :: err_message
  character(len=100) :: dimname !dimnameは各次元の名前。

!  print '(A)',varname(1:lnblnk(varname))
  ! ファイルID(ncid)からs%varで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,varname,varid)
!  print *,'ncid=', ncid
!  print *,'varid=', varid
!  print *,'stat= ', stat
!  print *

  !  スケールファクターを得る。(*1)
  stat = nf_get_att_real(ncid,varid,'scale_factor',scale)
!  print *,'scale= ',scale
!  print *,'stat= ',stat
!  print *

  !  オフセットの 値を得る(*2)
  stat = nf_get_att_real(ncid,varid,'add_offset',offset)
!  print *,'offset=', offset
!  print *,'stat= ',stat
!  print *

! varidからndimsを得る。ndimsとは次元の数。二次元データなら2
  stat = nf_inq_varndims(ncid, varid, ndims)
!  print *,'stat=',stat
!  print *,'ndims= ', ndims
!  print *

! varidからdimidsを得る。dimidsとはそれぞれの次元に対するID
  allocate(dimids(ndims))
  stat = nf_inq_vardimid(ncid, varid, dimids)
!  print *,'stat= ',stat
!  print *,'ndims= ', ndims
!  print *,dimids
!  do i=1,ndims
!    write(6,'("dimids(",i3,")=",i9 )') i,dimids(i)
!  enddo
!  print *

  allocate(nshape(ndims))
  do i=1,ndims
!   dimidsからnshapeを得る。
!   nshapeとはそれぞれの次元に対する最大データ数 (格子数)
    stat = nf_inq_dimlen(ncid, dimids(i), nshape(i))
!    print *,'stat=',stat
!    print *,nshape
!    write(6,'("nshape(",i3,")=",i9 )') i,nshape(i)
  enddo !i
!  print *

  do i=1,ndims
    stat = nf_inq_dimname(ncid, dimids(i), dimname)
!    print *,'stat=',stat
!    print *,'dimname=',dimname
!    write(6,'("dimname(",i3,")=",1x,a23)') i,dimname
  enddo
!  print *

!  print *,'Read ',varname(1:lnblnk(varname))

  if(varname == 'u' .or. varname == 'v' .or. varname == 'temp' &
  .or. varname == 'rh')then

    stat = nf_get_var_int2(ncid, varid, varout_int)

  else if (varname == 'z' .or. varname == 'w')then

    stat = nf_get_var_double(ncid, varid, varout_dbl)

  endif

  if(stat /= 0)then
    print *,'stat= ',stat
    print *,"Terminated."
    print *
    stop
  endif

  if(varname == 'temp')then
    print *,'temp: scale=',scale,' offset=',offset
  end if

  if(varname == 'u' .or. varname == 'v' .or. varname == 'temp' &
  .or. varname == 'rh')then

    varout(:,:,:,:)=float(varout_int(:,:,:,:))*scale + offset

  else if (varname == 'z' .or. varname == 'w')then

    varout(:,:,:,:)=sngl(varout_dbl(:,:,:,:))

  endif

  deallocate(dimids,nshape)


end subroutine

end module

! $ ncdump ~/Data/MSM/MSM-P/1012.nc -h
! netcdf 1012 {
! dimensions:
!         lon = 241 ;
!         lat = 253 ;
!         p = 16 ;
!         time = 8 ;
! variables:
!         float lon(lon) ;
!                 lon:long_name = "longitude" ;
!                 lon:units = "degrees_east" ;
!                 lon:standard_name = "longitude" ;
!         float lat(lat) ;
!                 lat:long_name = "latitude" ;
!                 lat:units = "degrees_north" ;
!                 lat:standard_name = "latitude" ;
!         float p(p) ;
!                 p:long_name = "pressure level" ;
!                 p:standard_name = "air_pressure" ;
!                 p:units = "hPa" ;
!         float time(time) ;
!                 time:long_name = "time" ;
!                 time:standard_name = "time" ;
!                 time:units = "hours since 2012-10-12 00:00:00+00:00" ;
!         double z(time, p, lat, lon) ;
!                 z:long_name = "geopotential height" ;
!                 z:units = "m" ;
!                 z:standard_name = "geopotential_height" ;
!         double w(time, p, lat, lon) ;
!                 w:long_name = "vertical velocity in p" ;
!                 w:units = "Pa/s" ;
!                 w:standard_name = "lagrangian_tendency_of_air_pressure" ;
!         short u(time, p, lat, lon) ;
!                 u:scale_factor = 0.006116208155 ;
!                 u:add_offset = 0. ;
!                 u:long_name = "eastward component of wind" ;
!                 u:units = "m/s" ;
!                 u:standard_name = "eastward_wind" ;
!         short v(time, p, lat, lon) ;
!                 v:scale_factor = 0.006116208155 ;
!                 v:add_offset = 0. ;
!                 v:long_name = "northward component of wind" ;
!                 v:units = "m/s" ;
!                 v:standard_name = "northward_wind" ;
!         short temp(time, p, lat, lon) ;
!                 temp:scale_factor = 0.002613491379 ;
!                 temp:add_offset = 255.4004974 ;
!                 temp:long_name = "temperature" ;
!                 temp:units = "K" ;
!                 temp:standard_name = "air_temperature" ;
!         short rh(time, p, lat, lon) ;
!                 rh:scale_factor = 0.002293577883 ;
!                 rh:add_offset = 75. ;
!                 rh:long_name = "relative humidity" ;
!                 rh:units = "%" ;
!                 rh:standard_name = "relative_humidity" ;
! 
! // global attributes:
!                 :Conventions = "CF-1.0" ;
!                 :history = "created by create_1daync_msm_p.rb  2012-10-14" ;
! }

module mod_interpolate
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/Sato.Kodama.Manda2015/MSM.Profiles.Trajectory
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 13:14 on 01-23-2015.

use mod_parameter

contains

subroutine interpolate_space_2d(s,sp,lon,lat,ierr)

  use mod_msms_var

  type(msms),intent(in)::s
  type(msms_ts),intent(inout)::sp
  real,intent(in)::lon,lat
  integer,intent(inout)::ierr

  real xa(4), ya(4), za(4)
  real x,y
  real z

  idx=(lon-s%lon(1))/sdlon
  jdx=slat-(lat-s%lat(slat))/sdlat

  if(idx <= 1 .or. idx >= slon .or. jdx <= 1 .or. jdx >= slat)then
    ierr=1
    return
  end if

  xa(1)=s%lon(idx);   ya(1)=s%lat(jdx+1)
  xa(2)=s%lon(idx+1); ya(2)=s%lat(jdx+1)
  xa(3)=s%lon(idx+1); ya(3)=s%lat(jdx)
  xa(4)=s%lon(idx);   ya(4)=s%lat(jdx)

  print *,xa(1),ya(1)
  print *,xa(2),ya(2)
  print *,xa(3),ya(3)
  print *,xa(4),ya(4)

  time_loop: do n=1,stime
!   u
    za(1)=s%u(idx,   jdx+1, n)
    za(2)=s%u(idx+1, jdx+1, n)
    za(3)=s%u(idx+1, jdx,   n)
    za(4)=s%u(idx,   jdx,   n)

    call bilinear(xa,ya, za, lon, lat, sp%u(n))

!   v
    za(1)=s%v(idx,   jdx+1, n)
    za(2)=s%v(idx+1, jdx+1, n)
    za(3)=s%v(idx+1, jdx,   n)
    za(4)=s%v(idx,   jdx,   n)

    call bilinear(xa,ya, za, lon, lat, sp%v(n))

!   temp
    za(1)=s%temp(idx,   jdx+1, n)
    za(2)=s%temp(idx+1, jdx+1, n)
    za(3)=s%temp(idx+1, jdx,   n)
    za(4)=s%temp(idx,   jdx,   n)
    call bilinear(xa,ya, za, lon, lat, sp%temp(n))

!   rh
    za(1)=s%rh(idx,   jdx+1, n)
    za(2)=s%rh(idx+1, jdx+1, n)
    za(3)=s%rh(idx+1, jdx,   n)
    za(4)=s%rh(idx,   jdx,   n)

    call bilinear(xa,ya, za, lon, lat, sp%rh(n))

!   sp (surface pressure)
    za(1)=s%sp(idx,   jdx+1, n)
    za(2)=s%sp(idx+1, jdx+1, n)
    za(3)=s%sp(idx+1, jdx,   n)
    za(4)=s%sp(idx,   jdx,   n)

    call bilinear(xa,ya, za, lon, lat, sp%sp(n))

!   ncld (cloud amount in %)
    za(1)=s%ncld(idx,   jdx+1, n)
    za(2)=s%ncld(idx+1, jdx+1, n)
    za(3)=s%ncld(idx+1, jdx,   n)
    za(4)=s%ncld(idx,   jdx,   n)

    call bilinear(xa,ya, za, lon, lat, sp%ncld(n))

  enddo time_loop

  do n=1,stime
    sp%time(n)=s%time(n)
  enddo !n

end subroutine



subroutine interpolate_time_2d(sp1,sp2,hr,mi,sc,stn)
  use mod_msms_var
  type(msms_ts),intent(in)::sp1
  type(msms_ts),intent(in)::sp2
  integer,intent(in)::hr,mi,sc
  real hh
  type(msms_stn),intent(inout)::stn

  hh=float(hr)+float(mi)/60.0+float(sc)/3600.0
  print *,"interpolate_time_2d: ",hh

  if(hh<23.0)then
    ndx1=hh+1
    ndx2=ndx1+1
    print *,"interpolate_time_2d: sp1,sp1,",sp1%time(ndx1), sp1%time(ndx2)

    w1=(sp1%time(ndx2)-hh)/sdtime
    w2=(hh-sp1%time(ndx1))/sdtime

    stn%u   =sp1%u(ndx1)*w1    + sp1%u(ndx2)*w2
    stn%v   =sp1%v(ndx1)*w1    + sp1%v(ndx2)*w2
    stn%temp=sp1%temp(ndx1)*w1 + sp1%temp(ndx2)*w2
    stn%rh  =sp1%rh(ndx1)*w1   + sp1%rh(ndx2)*w2
    stn%sp  =sp1%sp(ndx1)*w1   + sp1%sp(ndx2)*w2
    stn%ncld=sp1%ncld(ndx1)*w1 + sp1%ncld(ndx2)*w2

    if(sp1%rh(ndx1)<0.0 .or. sp2%rh(ndx2)<0)stn%rh=rmiss

  endif

  if(hh>=23.0)then

    ndx1=24
    ndx2=1
    print *,"interpolate_time_2d: sp1,sp2",sp1%time(ndx1), sp2%time(ndx2)+24.0

    w1=(24.0-hh)/sdtime
    w2=(hh-21.0)/sdtime

    stn%u   =sp1%u(ndx1)*w1    + sp2%u(ndx2)*w2
    stn%v   =sp1%v(ndx1)*w1    + sp2%v(ndx2)*w2
    stn%temp=sp1%temp(ndx1)*w1 + sp2%temp(ndx2)*w2
    stn%rh  =sp1%rh(ndx1)*w1   + sp2%rh(ndx2)*w2
    stn%sp  =sp1%sp(ndx1)*w1   + sp2%sp(ndx2)*w2
    stn%ncld=sp1%ncld(ndx1)*w1 + sp1%ncld(ndx2)*w2
  endif

  stn%w=0.0 !Dummy variable since omega is always zero at the surface

  print *,"interpolate_time_2d: ",w1,w2,w1+w2

end subroutine



subroutine interpolate_space(p,vp,lon,lat,ierr)

  use mod_msmp_var

  type(msmp),intent(in)::p
  type(msmp_vp),intent(inout)::vp
  real,intent(in)::lon,lat
  integer,intent(inout)::ierr

  real xa(4), ya(4), za(4)
  real x,y
  real z

!  print *,"interpolate_space: ",lon,lat; print *

  idx=(lon-p%lon(1))/dlon
  jdx=plat-(lat-p%lat(plat))/dlat

  if(idx <= 1 .or. idx >= plon .or. jdx <= 1 .or. jdx >= plat)then
    ierr=1
    return
  end if

  xa(1)=p%lon(idx);   ya(1)=p%lat(jdx+1)
  xa(2)=p%lon(idx+1); ya(2)=p%lat(jdx+1)
  xa(3)=p%lon(idx+1); ya(3)=p%lat(jdx)
  xa(4)=p%lon(idx);   ya(4)=p%lat(jdx)

  print *,xa(1),ya(1)
  print *,xa(2),ya(2)
  print *,xa(3),ya(3)
  print *,xa(4),ya(4)

  do n=1,ptime
!    print *,"interpolate_space: "; print *,p%time(n)

!   z
    do k=1,ppres
      za(1)=p%z(idx,   jdx+1, k, n)
      za(2)=p%z(idx+1, jdx+1, k, n)
      za(3)=p%z(idx+1, jdx,   k, n)
      za(4)=p%z(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%z(k,n))
!      print *,p%p(k),vp%z(k,n)
    enddo !k

!   u
    do k=1,ppres
      za(1)=p%u(idx,   jdx+1, k, n)
      za(2)=p%u(idx+1, jdx+1, k, n)
      za(3)=p%u(idx+1, jdx,   k, n)
      za(4)=p%u(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%u(k,n))
!      print *,p%p(k),vp%u(k,n)
    enddo !k

!   v
    do k=1,ppres
      za(1)=p%v(idx,   jdx+1, k, n)
      za(2)=p%v(idx+1, jdx+1, k, n)
      za(3)=p%v(idx+1, jdx,   k, n)
      za(4)=p%v(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%v(k,n))
!      print *,p%p(k),vp%v(k,n)
    enddo !k

!   w (=omega)
    do k=1,ppres
      za(1)=p%w(idx,   jdx+1, k, n)
      za(2)=p%w(idx+1, jdx+1, k, n)
      za(3)=p%w(idx+1, jdx,   k, n)
      za(4)=p%w(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%w(k,n))
!      print *,p%p(k),vp%v(k,n)
    enddo !k

!   temp
    do k=1,ppres
      za(1)=p%temp(idx,   jdx+1, k, n)
      za(2)=p%temp(idx+1, jdx+1, k, n)
      za(3)=p%temp(idx+1, jdx,   k, n)
      za(4)=p%temp(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%temp(k,n))
!      print *,p%p(k),vp%temp(k,n)
    enddo !k

!   rh
    do k=1,ppres
      za(1)=p%rh(idx,   jdx+1, k, n)
      za(2)=p%rh(idx+1, jdx+1, k, n)
      za(3)=p%rh(idx+1, jdx,   k, n)
      za(4)=p%rh(idx,   jdx,   k, n)

      call bilinear(xa,ya, za, lon, lat, vp%rh(k,n))
!      print *,p%p(k),vp%rh(k,n)
    enddo !k
  enddo !n

  do k=1,ppres
    vp%p(k)=p%p(k)
  enddo !k

  do n=1,ptime
    vp%time(n)=p%time(n)
!    print *,vp%time(n)
  enddo !n

end subroutine



subroutine interpolate_time(vp1,vp2,hr,mi,sc,stn)
  use mod_msmp_var
  type(msmp_vp),intent(in)::vp1
  type(msmp_vp),intent(in)::vp2
  integer,intent(in)::hr,mi,sc
  real hh
  type(msmp_stn),intent(inout)::stn

  hh=float(hr)+float(mi)/60.0+float(sc)/3600.0
!  print *,"interpolate_time: ",hh

  if(hh<21.0)then
    ndx1=hr/3+1
    ndx2=ndx1+1
!    print *,"interpolate_time: vp1,vp1,",vp1%time(ndx1), vp1%time(ndx2)

    w1=(vp1%time(ndx2)-hh)/dtime
    w2=(hh-vp1%time(ndx1))/dtime

    do k=1,ppres
      stn%z(k)   =vp1%z(k,ndx1)*w1    + vp1%z(k,ndx2)*w2
      stn%u(k)   =vp1%u(k,ndx1)*w1    + vp1%u(k,ndx2)*w2
      stn%v(k)   =vp1%v(k,ndx1)*w1    + vp1%v(k,ndx2)*w2
      stn%w(k)   =vp1%w(k,ndx1)*w1    + vp1%w(k,ndx2)*w2
      stn%temp(k)=vp1%temp(k,ndx1)*w1 + vp1%temp(k,ndx2)*w2
      stn%rh(k)  =vp1%rh(k,ndx1)*w1   + vp1%rh(k,ndx2)*w2
      if(vp1%rh(k,ndx1)<0.0 .or. vp2%rh(k,ndx2)<0)stn%rh(k)=rmiss
    enddo !k
  endif

  if(hh>=21.0)then

    ndx1=8
    ndx2=1
!    print *,"interpolate_time: vp1,vp2",vp1%time(ndx1), vp2%time(ndx2)+24.0

    w1=(24.0-hh)/dtime
    w2=(hh-21.0)/dtime

    do k=1,ppres
      stn%z(k)   =vp1%z(k,ndx1)*w1    + vp2%z(k,ndx2)*w2
      stn%u(k)   =vp1%u(k,ndx1)*w1    + vp2%u(k,ndx2)*w2
      stn%v(k)   =vp1%v(k,ndx1)*w1    + vp2%v(k,ndx2)*w2
      stn%w(k)   =vp1%w(k,ndx1)*w1    + vp2%w(k,ndx2)*w2
      stn%temp(k)=vp1%temp(k,ndx1)*w1 + vp2%temp(k,ndx2)*w2
      stn%rh(k)  =vp1%rh(k,ndx1)*w1   + vp2%rh(k,ndx2)*w2
      if(vp1%rh(k,ndx1)<0.0 .or. vp2%rh(k,ndx2)<0)stn%rh(k)=rmiss
    enddo !k
  endif

!  print *,"interpolate_time: ",w1,w2,w1+w2

  do k=1,ppres
    stn%p(k)=vp1%p(k)
  enddo !k

end subroutine



subroutine bilinear(xa,ya, za, x, y, z)

! http://imagingsolution.blog107.fc2.com/blog-entry-142.html
! http://www.library.cornell.edu/nr/bookfpdf/f3-6.pdf
  real, intent(in) :: xa(4), ya(4), za(4)
  real, intent(in) :: x,y
  real, intent(out) :: z
  real t, u
!-----------------------------------------------------------------------
! Subscript "a" indicates the input.
!
!
! For given xa(i),ya(i),za(i),x & y, z is estimated.
!
! (xa(4),ya(4),za(4))   (xa(3),ya(3),za(3))
!        +------------------+
!        |                  |
!        |    x             |
!        | (x,y,z)          |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        |                  |
!        +------------------+
! (xa(1),ya(1),za(1))   (xa(2),ya(2),za(2))
!

      t=(x-xa(1))/(xa(2)-xa(1))
      u=(y-ya(1))/(ya(4)-ya(1))

      z =  (1.-t)*(1.-u)*za(1) &
     &   + t*(1.-u)*za(2) &
     &   + t*u*za(3) &
     &   + (1.-t)*u*za(4)

      return
end subroutine

end module

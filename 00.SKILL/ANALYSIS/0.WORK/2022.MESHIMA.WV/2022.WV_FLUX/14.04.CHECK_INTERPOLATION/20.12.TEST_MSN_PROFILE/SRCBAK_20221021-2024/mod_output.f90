module mod_output
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/Sato.Kodama.Manda2015/MSM.Profiles.Trajectory
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 13:28 on 01-23-2015.

!  use
!  implicit none

contains

subroutine write_stn(eltime, yr,mo,dy,hr,mi,sc,lon,lat, ss,stn, alt, &
&  in_file_trajectory, prefix, out_dir_stn)

  use mod_msmp_var
  use mod_msms_var

  double precision, intent(in)::eltime
  integer,intent(in)::yr,mo,dy,hr,mi,sc
  real,intent(in)::lon,lat
  type(msms_stn),intent(in)::ss
  type(msmp_stn),intent(in)::stn
  real,intent(in)::alt ! Altitude of bird
  character(len=*),intent(in)::in_file_trajectory, prefix, out_dir_stn
  character(len=600)::ofle

  is=1;    ie=lnblnk(out_dir_stn)
  write(ofle(is:ie),'(A)')trim(out_dir_stn)
  is=ie+1; ie=is
  write(ofle(is:ie),'(A)')'/'
  is=ie+1;   ie=is+lnblnk(prefix)
  write(ofle(is:ie),'(A)')trim(prefix)
  is=ie;   ie=is
  write(ofle(is:ie),'(A)')'_'
  is=ie+1; ie=is+3
  write(ofle(is:ie),'(i4.4)')yr
  is=ie+1; ie=is+1
  write(ofle(is:ie),'(i2.2)')mo
  is=ie+1; ie=is+1
  write(ofle(is:ie),'(i2.2)')dy
  is=ie+1;   ie=is
  write(ofle(is:ie),'(A)')'_'
  is=ie+1; ie=is+1
  write(ofle(is:ie),'(i2.2)')hr
  is=ie+1; ie=is+1
  write(ofle(is:ie),'(i2.2)')mi
  is=ie+1; ie=is+1
  write(ofle(is:ie),'(i2.2)')sc
  is=ie+1;   ie=is+3
  write(ofle(is:ie),'(A)')'.txt'

  print '(A,A)',"Output: ",trim(ofle)
  print *

  open(20,file=ofle)
  write(20,'(A,A)')'# trajectory= ',trim(in_file_trajectory)
  write(20,'(A,f10.5)')'# lon= ',lon
  write(20,'(A,f10.5)')'# lat= ',lat
  write(20,'(A,f12.5,1x,i4.4,5(1x,i2.2))')"# ",eltime, yr, mo, dy, hr, mi, sc
  write(20,'(A,f10.1)')"# alt",alt
  write(20,'(A)')'# p[hPa] z[m] u[m/s] v[m/s] temp[K] rh[%] &
pt [K] vpt[K] ept[K] w[kg/kg] q[kg/kg] omega[Pa/s]'

  write(20,'(f9.2,f9.1,2f9.3,f10.3,f7.1,3f10.3,2f11.5,g12.4)') &
ss%sp, 0.0, ss%u, ss%v, ss%temp, ss%rh, &
ss%pt, ss%vpt, ss%ept, &
ss%wmix, ss%q, ss%w

  do k=1,ppres
  write(20,'(f9.2,f9.1,2f9.3,f10.3,f7.1,3f10.3,2f11.5,g12.4)') &
stn%p(k), stn%z(k), stn%u(k), stn%v(k), stn%temp(k), stn%rh(k), &
stn%pt(k), stn%vpt(k), stn%ept(k), &
stn%wmix(k), stn%q(k), stn%w(k)
  enddo !k

  close(20)
end subroutine write_stn

end module

program msm_profiles_trajectory
! Description:
!
! Author: AM
!
! Host: 133.45.210.165
! Directory: /work1/am/Sato.Kodama.Manda2015/MSM.Profiles.Trajectory
!
! Interpolate MSM data in space and time for estimating the vertical
! profiles at a parcel location

  use mod_msms_var
  use mod_msmp_var
  use mod_input
  use mod_output
  use mod_interpolate
  use mod_thermodynamics
  use mod_timer

  include 'netcdf.inc'

! MSM surface data
  type(msms)::s1, s2
  type(msms_ts)::sp1,sp2
  type(msms_stn)::ss

! MSM pressure level data
  type(msmp)::p1, p2
  type(msmp_vp)::vp1, vp2
  type(msmp_stn)::stn

! Date and time
  integer yr,mo,dy,hr,mi,sc, julian, yrp1,mop1,dyp1
  integer:: yr_old=-9999, mo_old=-9999, dy_old=-9999
  character cyr*4,cmo*2,cdy*2

! Geographic coordinate
  real lon,lat,alt

  double precision eltime !elapsed time

! Input and output
  character(len=200)::prefix
  character(len=300)::in_file_trajectory, in_dir_msm_p, in_dir_msm_s
  character(len=600):: in_msms1, in_msms2, in_msmp1, in_msmp2
  character(len=300)::out_dir_stn

  character(500)::strm

  namelist/input/prefix,in_file_trajectory, in_dir_msm_p, in_dir_msm_s,&
&                yr0,mo0,dy0,hr0
  namelist/output/out_dir_stn

  read(*,nml=input)
  read(*,nml=output)

  open(10,file=in_file_trajectory,action="read")
  print '(A,A)','Open ',trim(in_file_trajectory)

  mi0=0
  sc0=0

  time_loop: do
    read(10,'(A)',end=999)strm
    if(strm(1:1) == "#")cycle

    call read_trajectory(strm,ptt,yr,mo,dy,hr,mi,sc,lon,lat,alt)

    eltime=elapsed_time(yr0,mo0,dy0,hr0,mi0,sc0, yr,mo,dy,hr,mi,sc)

    if(yr/=yr_old .or. mo/=mo_old .or. dy/=dy_old)then
      write(cyr(1:4),'(i4.4)')int(yr)
      write(cmo(1:2),'(i2.2)')int(mo)
      write(cdy(1:2),'(i2.2)')int(dy)

      in_msms1=trim(in_dir_msm_s)//'/'//cyr//'/'//cmo//cdy//'.nc'
      call read_msms(in_msms1, s1)
      s1%sp=s1%sp*0.01 !Pa -> hPa

      in_msmp1=trim(in_dir_msm_p)//'/'//cyr//'/'//cmo//cdy//'.nc'
      call read_msmp(in_msmp1, p1)

      julian=julday(yr,mo,dy)
      call caldat(julian+1,yrp1,mop1,dyp1)
      write(cyr(1:4),'(i4.4)')int(yrp1)
      write(cmo(1:2),'(i2.2)')int(mop1)
      write(cdy(1:2),'(i2.2)')int(dyp1)
      in_msms2=trim(in_dir_msm_s)//'/'//cyr//'/'//cmo//cdy//'.nc'
      call read_msms(in_msms2, s2)
      s2%sp=s2%sp*0.01 !Pa -> hPa
      in_msmp2=trim(in_dir_msm_p)//'/'//cyr//'/'//cmo//cdy//'.nc'
      call read_msmp(in_msmp2, p2)

      print '(i4.4,5(1x,i2.2),2f10.4,f7.1)',yr,mo,dy,hr,mi,sc,lon,lat,p
      print '(A,A,A)',trim(in_msms1),' ',trim(in_msms2)
      print '(A,A,A)',trim(in_msmp1),' ',trim(in_msmp2)

    endif
    yr_old=yr; mo_old=mo; dy_old=dy

    ierr=0 ! Check if the particle is out of bounds

!   Surface data
    call interpolate_space_2d(s1,sp1,lon,lat,ierr)
    if(ierr==1) cycle

    call interpolate_space_2d(s2,sp2,lon,lat,ierr)
    print *,'sp2%sp',sp2%sp
    if(ierr==1) cycle

    call interpolate_time_2d(sp1,sp2,hr,mi,sc,ss)



!   Pressure level data
    call interpolate_space(p1,vp1,lon,lat,ierr)
    if(ierr==1) cycle
    call interpolate_space(p2,vp2,lon,lat,ierr)
    if(ierr==1) cycle
    call interpolate_time(vp1,vp2,hr,mi,sc,stn)


    call potential_temperature(ss%pt, ss%temp, ss%sp)

    call virtual_potential_temperature(ss%vpt, ss%temp, ss%sp, ss%rh)
    call ept_bolton80_RH(ss%ept, ss%temp, ss%sp, ss%rh)
    call water_vapor(ss%wmix, ss%q, ss%temp, ss%sp, ss%rh)

    do k=1,ppres
      call potential_temperature(stn%pt(k), stn%temp(k), stn%p(k))
      call virtual_potential_temperature(stn%vpt(k), stn%temp(k), stn%p(k), stn%rh(k))
      call ept_bolton80_RH(stn%ept(k), stn%temp(k), stn%p(k), stn%rh(k))
      call water_vapor(stn%wmix(k), stn%q(k), stn%temp(k), stn%p(k), stn%rh(k))
    enddo !k

!   Output
    call write_stn(eltime, yr,mo,dy,hr,mi,sc,lon,lat, ss,stn, alt, &
&    in_file_trajectory,prefix,out_dir_stn)

  enddo time_loop

999 continue

  print *
  print *,"Origin of time for computing elapsed time:"
  print *,yr0,mo0,dy0,hr0
  print *
end program

module mod_input
! Description:
!
! Author: am
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work1/am/Sato.Kodama.Manda2015/MSM.Profiles.Trajectory
!
! Revision history:
! Created by /usr/local/mybin/nff.sh at 13:26 on 01-23-2015.

!  use
!  implicit none

contains

subroutine read_trajectory(strm,ptt,yr,mo,dy,hr,mi,sc,lon,lat,alt)
  character(len=*),intent(in):: strm
  integer,intent(inout):: yr,mo,dy,hr,mi,sc
  real,intent(inout):: lon,lat,alt

  read(strm,*)ptt,yr,mo,dy,hr,mi,sc,lat,lon,alt
!  read(strm,*)eltime,yr,mo,dy,hr,mi,sc,u,v,w,lon,lat,p
end subroutine



subroutine read_msmp(in_msmp, p)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:57 on 12-04-2012.

  use mod_msmp_var

  include 'netcdf.inc'

  character(len=*),intent(in) :: in_msmp
  type(msmp),intent(inout)::p


!  write(*,'(a)')'Subroutine: read_msmp'
!  write(*,*)''
  print *
  stat = nf_open(in_msmp, nf_nowrite, ncid) ! ファイルのopenとNetCDF ID(ncid)の取得
!  print *,'ncid= ',ncid
  if(stat == 0)then
    print '(A,A)','Open : ',in_msmp(1:lnblnk(in_msmp))
  else
    print '(A,A)','Error while opening ',in_msmp(1:lnblnk(in_msmp))
    print *,'status= ',stat
    stop
  endif

!==================
  p%var='lon'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%lon)
!  print *,'stat= ',stat



!==================
  p%var='lat'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%lat)
!  print *,'stat= ',stat
!  print *

!==================
  p%var='p'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%p)


!==================
  p%var='time'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,p%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',p%var(1:lnblnk(p%var))
  stat = nf_get_var_real(ncid, varid, p%time)
!  print *,'stat= ',stat
!  print *


  call read_msmp_vars('z',p%z)
  call read_msmp_vars('w',p%w)
  call read_msmp_vars('u',p%u)
  call read_msmp_vars('v',p%v)
  call read_msmp_vars('temp',p%temp)
  call read_msmp_vars('rh',p%rh)

end subroutine read_msmp



subroutine read_msms(in_msms, s)
! Description:
!
! Author: share
!
! Host: aofd31.fish.nagasaki-u.ac.jp
! Directory: /home/share/Data/121012_Jikken4/MSM/src
!
! Revision history:
!  This file is created by /usr/local/mybin/nff.sh at 21:57 on 12-04-2012.

  use mod_msms_var

  include 'netcdf.inc'

  character(len=*),intent(in) :: in_msms
  type(msms),intent(inout)::s


!  write(*,'(a)')'Subroutine: read_msms'
!  write(*,*)''
  print *
  stat = nf_open(in_msms, nf_nowrite, ncid) ! ファイルのopenとNetCDF ID(ncid)の取得
  if(stat == 0)then
    print '(A,A)','Open : ',in_msms(1:lnblnk(in_msms))
  else
    print '(A,A)','Error while opening ',in_msms(1:lnblnk(in_msms))
    print *,'status= ',stat
    stop
  endif


!==================
  s%var='lon'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%lon)
!  print *,'stat= ',stat



!==================
  s%var='lat'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%lat)
!  print *,'stat= ',stat
!  print *


!==================
  s%var='time'
!==================
  ! ファイルID(ncid)からvarで設定した変数のID(varid)を得る。
  stat = nf_inq_varid(ncid,s%var,varid)
!  print *,'varid=', varid
!  print *,'sta= ', sta
  print *,'Read ',s%var(1:lnblnk(s%var))
  stat = nf_get_var_real(ncid, varid, s%time)
!  print *,'stat= ',stat
!  print *

  call read_msms_vars('psea',s%psea)
  call read_msms_vars('sp',s%sp)
  call read_msms_vars('u',s%u)
  call read_msms_vars('v',s%v)
  call read_msms_vars('temp',s%temp)
  call read_msms_vars('rh',s%rh)
  call read_msms_vars('r1h',s%r1h)
  call read_msms_vars('ncld_upper',s%ncld_upper)
  call read_msms_vars('ncld_mid',s%ncld_mid)
  call read_msms_vars('ncld_low',s%ncld_low)
  call read_msms_vars('ncld',s%ncld)


end subroutine read_msms




end module

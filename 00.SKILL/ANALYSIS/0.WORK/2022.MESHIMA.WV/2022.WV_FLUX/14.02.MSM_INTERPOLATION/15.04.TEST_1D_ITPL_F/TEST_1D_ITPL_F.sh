#!/bin/bash
#
# Sat, 01 Oct 2022 05:56:28 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/15.04.TEST_1D_ITPL_F
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
OPT= #" -L. -lncio_test -O2 "
OPT2= #" -fopenmp "
DOPT= #" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"


INDIR=.
INFLE=TEST_INPUT.txt
 ODIR=.
 OFLE=TEST_OUTPUT.txt
XO=100

cat <<EOF> $nml
&para
INDIR="$INDIR"
INFLE="$INFLE"
ODIR="$ODIR"
OFLE="$OFLE"
XO=$XO
&end
EOF

cat <<EOF>$INDIR/$INFLE
4
50   20 
300  18
500  16
1000 13
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

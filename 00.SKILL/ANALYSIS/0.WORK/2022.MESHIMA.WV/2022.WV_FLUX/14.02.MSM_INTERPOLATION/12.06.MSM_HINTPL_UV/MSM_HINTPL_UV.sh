#!/bin/bash

YYYY=2022; MMDD=0705
INDIR=/work01/DATA/MSM/MSM-P/$YYYY
INFLE=${MMDD}.nc

STN=MESHIMA; LAT=31.9980555556; LON=128.350833333

OFLE=${YYYY}${MMDD}_${STN}_$(basename $0 .sh).nc

# ある緯度経度地点における値をbilinear interpolationを用いて# 抽出する
# https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

# 女島 (長崎県)
# 北緯31度59分53秒  31+59/60.+53/3600.
# 東経128度21分03秒 128 + 21./60. + 3/3600.

cdo -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OFLE

echo
echo $STN $LAT $LON
echo
echo INPUT: $INDIR/$INFLE
echo OUTPUT: $OFLE

# ncdump -h $OFLE


#/bin/bash

INDIR=/work01/DATA/MSM/MSM-P/2022
INFLE=0705.nc

<<COMMENT
# https://code.mpimet.mpg.de/projects/cdo/wiki/Tutorial
# 
# Display variables of a file
# cdo -showname <infile>
#
# Display number of timesteps of a file
# cdo -ntime <infile>

# Display Information about the underlying grid: griddes does the following:
# cdo -griddes tsurf.nc
COMMENT

echo
cdo showname $INDIR/$INFLE
echo
cdo ntime $INDIR/$INFLE
#echo
#cdo griddes $INDIR/$INFLE

echo INPUT: $INDIR/$INFLE

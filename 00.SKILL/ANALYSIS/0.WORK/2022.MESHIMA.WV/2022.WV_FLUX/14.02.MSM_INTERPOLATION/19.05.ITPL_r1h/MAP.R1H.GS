INDIR='/work01/DATA/MSM/MSM-S/r1h/2021'
INFLE='0814.nc'
IN=INDIR'/'INFLE

'sdfopen 'IN

'set time 01Z14AUG2021'
*時刻を指定

'cc'
'set grads off';'set grid off'
*白紙の状態にする

'set gxout shaded'
*図の種類を指定
'set mpdset hires'
*地図の種類を高解像度のものに変更

'd r1h'
*r1h(=1時間降水量)で平面図を作図

'cbarn'
*色見本を追加

'q dims'
STR=sublin(result,5);TIME=subwrd(STR,6)
*作図対象としている時刻の情報を取得する
*/work03/2021/nakamuro/2022_PROGRAM/2022-12-08_NETCDF_EXERCISE/CHECK_DATA/CHK.R1H_RESULT.GS　でresultの内容を確認できる

*resultの内容
*val=Default file number is: 1 
*X is varying   Lon = 120 to 150   X = 1 to 481
*Y is varying   Lat = 22.4 to 47.6   Y = 1 to 505
*Z is fixed     Lev = 0  Z = 1
*T is fixed     Time = 01Z14AUG2021  T = 1
*E is fixed     Ens = 1  E = 1


'draw title 'TIME
*図の上に時刻を表示

'gxprint R1H.2021-08-14_01.eps'
*図をファイルに書き出す

'quit'

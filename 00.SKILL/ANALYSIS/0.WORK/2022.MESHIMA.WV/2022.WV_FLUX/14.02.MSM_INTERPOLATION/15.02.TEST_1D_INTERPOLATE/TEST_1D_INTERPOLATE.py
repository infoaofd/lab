import metpy.interpolate as mi
#import metpy as mi
import numpy as np

x = np.array([1., 2., 3., 4.])
y = np.array([2., 4., 6., 8.])
x_interp = np.array([2.5, 3.5])
y_interp=mi.interpolate_1d(x_interp, x, y)

print("x")
print(x)
print("y")
print(y)
print("x_interp")
print(x_interp)
print("y_interp")
print(y_interp)


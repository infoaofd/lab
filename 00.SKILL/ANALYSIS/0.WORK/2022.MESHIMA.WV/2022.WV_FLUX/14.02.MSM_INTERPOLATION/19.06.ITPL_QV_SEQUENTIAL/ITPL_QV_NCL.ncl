; NCL tips
; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Top

begin

YYYYMMDD  = getenv("NCL_ARG_2")

INDIR="/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.08.MSM_HINTPL_SEQUENTIAL/OUT_NC_MESHIMA"
INFLE=YYYYMMDD+"_MESHIMA_MSM_HINTPL.nc"


IN=INDIR+"/"+INFLE

f=addfile(IN,"r")
;print(f)

rh=short2flt(f->rh)
temp=short2flt(f->temp)

rh_short=f->rh
temp_short=f->temp

z=f->z
p=f->p
time=f->time
lon=f->lon
lat=f->lat


;printVarSummary(rh)
;printVarSummary(rh_short)
;printVarSummary(temp)
;printVarSummary(temp_short)

dims=dimsizes(temp)
;print(dims)
nt=dims(0)
np=dims(1)
nlat=dims(2)
nlon=dims(3)

zOBS=100.; fspan(100.,200.,np)

pin=new((/nt,np,nlat,nlon/),typeof(p))
copy_VarMeta(temp,pin)
;printVarSummary(pin)

pin=conform(temp,p,1)
;do n=0,nt-1
;pin(n,:,0,0)=p(:)
;end do
;print(pin)

;printVarSummary(z)


Qv = mixhum_ptrh (pin, temp, rh, 1)
copy_VarMeta(z,Qv)
Qv@long_name="Vapor mixing ratio"
Qv@standard_name="Qv"
Qv@units="kg/kg"

QvITP=Qv(:,0,0,0)
QvITP@z=zOBS
delete(QvITP@p)
;printVarSummary(QvITP)

z1D=z(0,:,0,0)
;printVarSummary(z1D)


date=cd_calendar(time, 0)
year   = tointeger(date(:,0))
month  = tointeger(date(:,1))
day    = tointeger(date(:,2))
hour   = tointeger(date(:,3))
date_str = sprinti("%0.4i ", year) + \
sprinti("%0.2i ", month) + \ 
sprinti("%0.2i ", day) + \ 
sprinti("%0.2i ", hour) 

do n=0,nt-1
;print("TIME_UTC="+date_str(n)) 
Qv1D=Qv(n,:,0,0)
; VERTICAL INTERPOLATION TO FIND Qv at zOBS
QvITP(n) = linint1_n_Wrap (z1D,Qv1D, False, zOBS, 0, 0)
end do


print("TIME_UTC    ZOBS    QvITP") 
do n=0,nt-1
;do k=0,2
;print("p="+p(k)+" z="+z(n,k,0,0)+" Qv="+Qv(n,k,0,0))
;end do ;k

print(date_str(n)+" "+zOBS +" "+QvITP(n))

end do ;n 

;printVarSummary(QvITP)



end



Variable: z
Type: double
Total Size: 1024 bytes
            128 values
Number of Dimensions: 4
Dimensions and sizes:	[time | 8] x [p | 16] x [lat | 1] x [lon | 1]
Coordinates: 
            time: [ 0..21]
            p: [1000..100]
            lat: [31.9980555556..31.9980555556]
            lon: [128.350833333..128.350833333]
Number Of Attributes: 3
  standard_name :	geopotential_height
  long_name :	geopotential height
  units :	m

Variable: QvITP
Type: float
Total Size: 32 bytes
            8 values
Number of Dimensions: 1
Dimensions and sizes:	[time | 8]
Coordinates: 
            time: [ 0..21]
Number Of Attributes: 7
  z :	100
  lon :	128.350833333
  lat :	31.9980555556
  _FillValue :	9.96921e+36
  standard_name :	Qv
  long_name :	Vapor mixing ratio
  units :	kg/kg

Variable: z1D
Type: double
Total Size: 128 bytes
            16 values
Number of Dimensions: 1
Dimensions and sizes:	[p | 16]
Coordinates: 
            p: [1000..100]
Number Of Attributes: 6
  lon :	128.350833333
  lat :	31.9980555556
  time :	 0
  units :	m
  long_name :	geopotential height
  standard_name :	geopotential_height
TIME_UTC=2022 06 20 00 
p=1000 z=45.19390087876351 Qv=0.0162888
p=975 z=265.7548613685057 Qv=0.0156723
p=950 z=491.0460069928404 Qv=0.0147512
zOBS=100 QvITP=0.0161356

TIME_UTC=2022 06 20 03 
p=1000 z=53.35712833110792 Qv=0.0161909
p=975 z=273.7615891812018 Qv=0.0154926
p=950 z=499.2299616073087 Qv=0.0141972
zOBS=100 QvITP=0.0160174

TIME_UTC=2022 06 20 06 
p=1000 z=50.58370335366151 Qv=0.0163913
p=975 z=271.0426974884273 Qv=0.0153961
p=950 z=496.6777121811294 Qv=0.0148006
zOBS=100 QvITP=0.016144

TIME_UTC=2022 06 20 09 
p=1000 z=48.44124656848633 Qv=0.0160798
p=975 z=269.0878793142707 Qv=0.015971
p=950 z=495.0796951285577 Qv=0.015756
zOBS=100 QvITP=0.0160528

TIME_UTC=2022 06 20 12 
p=1000 z=43.12858763956448 Qv=0.0169471
p=975 z=264.2200004551691 Qv=0.0159223
p=950 z=490.1723864340149 Qv=0.0150579
zOBS=100 QvITP=0.0166925

TIME_UTC=2022 06 20 15 
p=1000 z=39.38097066775212 Qv=0.0177031
p=975 z=261.3043089277009 Qv=0.0177597
p=950 z=488.5855354675712 Qv=0.0166225
zOBS=100 QvITP=0.0177171

TIME_UTC=2022 06 20 18 
p=1000 z=13.42223071960364 Qv=0.0183205
p=975 z=236.0295381779334 Qv=0.0169334
p=950 z=463.2042868700028 Qv=0.0155312
zOBS=100 QvITP=0.0179758

TIME_UTC=2022 06 20 21 
p=1000 z=13.09177475547333 Qv=0.0179473
p=975 z=235.441071268409 Qv=0.0176062
p=950 z=462.6464250380913 Qv=0.0169986
zOBS=100 QvITP=0.0178625


Variable: QvITP
Type: float
Total Size: 32 bytes
            8 values
Number of Dimensions: 1
Dimensions and sizes:	[time | 8]
Coordinates: 
            time: [ 0..21]
Number Of Attributes: 8
  time :	 0
  lat :	31.9980555556
  lon :	128.350833333
  units :	kg/kg
  long_name :	Vapor mixing ratio
  standard_name :	Qv
  z :	100
  _FillValue :	9.96921e+36

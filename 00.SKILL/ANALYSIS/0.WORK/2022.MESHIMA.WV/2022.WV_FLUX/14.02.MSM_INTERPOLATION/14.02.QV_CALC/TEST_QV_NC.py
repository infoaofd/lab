"""
NetCDF
https://qiita.com/roy29fuku/items/6ff09dd254302dcebb8d

Python-NetCDF
https://mizu.bosai.go.jp/wiki2/wiki.cgi?page=Python%2DNetCDF%B2%F2%C0%CF+%28%A5%C7%A1%BC%A5%BF%B2%F2%C0%CF%CA%D4%29

MetPy
https://www.dpac.dpri.kyoto-u.ac.jp/enomoto/pymetds/index.html

Xarray
https://qiita.com/fujiisoup/items/0d71995e54055e9708fc
https://astropengu.in/posts/29/
"""

import netCDF4
import sys
from metpy.units import units
import metpy.calc as mpcalc
import numpy as np
import xarray as xr

if len(sys.argv) < 4:
   print ("WRONG ARGUMENT: INDIR INFLE OFLE")
   exit(1)

INDIR = sys.argv[1]
INFLE = sys.argv[2]
OFLE  = sys.argv[3]

ds = xr.open_dataset(INDIR+"/"+INFLE)

qv=mpcalc.mixing_ratio_from_relative_humidity(ds.p*units.hPa, ds.temp, ds.rh)

ds['qv']=qv

#print(qv)

ds.to_netcdf(OFLE)

print("")
print("INDIR: "+INDIR)
print("")
print("INFLE: "+INFLE)
print("")
print("OUTPUT: "+OFLE)
print("")

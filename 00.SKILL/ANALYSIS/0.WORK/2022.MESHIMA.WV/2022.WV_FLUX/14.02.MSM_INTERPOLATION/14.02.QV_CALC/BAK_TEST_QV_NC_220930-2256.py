"""
NetCDF
https://qiita.com/roy29fuku/items/6ff09dd254302dcebb8d

Python-NetCDF
https://mizu.bosai.go.jp/wiki2/wiki.cgi?page=Python%2DNetCDF%B2%F2%C0%CF+%28%A5%C7%A1%BC%A5%BF%B2%F2%C0%CF%CA%D4%29

MetPy
https://www.dpac.dpri.kyoto-u.ac.jp/enomoto/pymetds/index.html
"""

import netCDF4
import sys
#import inspect
import metpy.calc
import numpy as np
import xarray as xr

if len(sys.argv) < 3:
   print ("WRONG ARGUMENT: INDIR INFLE")
   exit(1)

INDIR = sys.argv[1]
INFLE    = sys.argv[2]   
IN=INDIR+"/"+INFLE
ds = xr.open_dataset(IN)

print(ds)

tabs=ds.temp[0,:,0,0]
relhum=ds.rh[0,:,0,0]
pres=ds.p
qv=metpy.calc.mixing_ratio_from_relative_humidity(pres, tabs, relhum)

print(qv)


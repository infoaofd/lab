#!/bin/bash
#
# Sat, 01 Oct 2022 06:56:48 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/16.02.TEST_READ_MSM_F
#
src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort

OPT="-L/usr/local/netcdf-4.3.3.1/lib -lnetcdf"

OPT= #" -L. -lncio_test -O2 "
OPT2= #" -fopenmp "
DOPT= #" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"



cat<<EOF>$nml
&para
&end
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

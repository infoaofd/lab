/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.08.MSM_HINTPL_SEQUENTIAL
2022-10-28_18-46

指定した緯度・経度における変数の値を抜き出す。

使用法

am@p5820
/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.08.MSM_HINTPL_SEQUENTIAL
2022-10-28_18-46
$ MSM_HINTPL.RUN.sh 20200601 20200602
MSM_HINTPL.sh 2020 0601
MSM_HINTPL.sh 2020 0602

am@p5820
/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.08.MSM_HINTPL_SEQUENTIAL
2022-10-28_18-46
$ MSM_HINTPL.sh 2020 0601
mkdir: ディレクトリ `OUT_NC_MESHIMA' を作成しました
cdo    remapbil: Bilinear weights from lonlat (241x253) to lonlat (1x1) grid
cdo    remapbil: Processed 46827264 values from 6 variables over 8 timesteps [2.70s 11MB].

MESHIMA 31.9980555556 128.350833333

INPUT: /work01/DATA/MSM/MSM-P/2020/0601.nc
OUTPUT: OUT_NC_MESHIMA/20200601_MESHIMA_MSM_HINTPL.nc



MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
MSM_HINTPL.RUN.sh
はそのままでは動かない。

    50  i=0
    51  while [ $i -le $nday ]; do
    52    date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
    53    yyyy=${date_out:0:4}
    54    mm=${date_out:4:2}
    55    dd=${date_out:6:2}
    56 
    57    echo $EXE ${yyyy} ${mm}${dd}
    58 
    59    i=$(expr $i + 1)

57行目のechoを削除せよ。
MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM



参考資料
yyyy年mm1月dd1日からyyyy2年mm2月dd2日までループさせる
https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

#!/bin/bash

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

GS=$(basename $0 .sh).GS

INFLE=$1
INFLE=${INFLE:-20220701_MESHIMA_MSM-S_HINTPL.nc}
INDIR=OUT_NC_MESHIMA
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo ERROR: NO SUCH FILE, $IN; echo;exit 0;fi
FIG=$(basename $INFLE .nc).eps

# LEV="1000 900"

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

'sdfopen ${IN}'

'set z 1'

'cc'; 'set grid off'; 'set grads off'


xleft=1; ytop=8

xmax=1; ymax=3
xwid = 6.0/xmax; ywid = 10.0/ymax
xmargin=1; ymargin=0.7

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

xmap=1;ymap=1
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


'set parea 'xs ' 'xe' 'ys' 'ye

'set xlint 1';'set ylint 0.5'

'set t 1 8'

'q dims'; TEMP=sublin(result,5); DTIME=subwrd(TEMP,6)

 'd sp/100.'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# TEXT
x=xl ; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' SURFACE PRESSURE [hPa]'


# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=9.5
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' $INFLE'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

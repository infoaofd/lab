# MSM地表面データの抜き出し

指定された緯度・経度の点の値を抜き出す  

```
$ pwd
```

/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.12.MSM-S_HINTPL_SEQUENTIAL



## 実行例

```
$ MSM-S_HINTPL.sh
```

`OUT_NC_MESHIMA/20220705_MESHIMA_MSM-S_HINTPL.nc' を削除しました  
cdo(1) remapbil: Process started  
cdo(1) remapbil: Bilinear weights from lonlat (481x505) to lonlat (1x1) grid  
cdo(1) remapbil: Processed 69956640 values from 12 variables over 24 timesteps.  
cdo    seltimestep: Processed 96 values from 12 variables over 22 timesteps [0.70s 15MB].  

MESHIMA 31.9980555556 128.350833333  

INPUT: /work01/DATA/MSM/MSM-S/2022/0705.nc  
OUTPUT: OUT_NC_MESHIMA/20220705_MESHIMA_MSM-S_HINTPL.nc  



```
$ cdo showtime OUT_NC_MESHIMA/20220705_MESHIMA_MSM-S_HINTPL.nc
```

 **00:00:00 03:00:00 06:00:00 08:00:00 11:00:00 14:00:00 17:00:00 20:00:00**  
cdo    showtime: Processed 12 variables over 8 timesteps [0.00s 9252KB].  

```
$ cdo showname OUT_NC_MESHIMA/20220705_MESHIMA_MSM-S_HINTPL.nc 
```

 **psea sp u v temp rh r1h ncld_upper ncld_mid ncld_low ncld dswrf**  
cdo    showname: Processed 12 variables [0.00s 9224KB].  



```
$ MSM-S_HINTPL.sh 2022 0701
```

cdo(1) remapbil: Process started
cdo(1) remapbil: Bilinear weights from lonlat (481x505) to lonlat (1x1) grid
cdo(1) remapbil: Processed 69956640 values from 12 variables over 24 timesteps.
cdo    seltimestep: Processed 96 values from 12 variables over 22 timesteps [1.82s 15MB].

MESHIMA 31.9980555556 128.350833333

INPUT: /work01/DATA/MSM/MSM-S/**2022**/**0701**.nc
OUTPUT: OUT_NC_MESHIMA/20220701_MESHIMA_MSM-S_HINTPL.nc



```
$ MSM-S_HINTPL.RUN.sh 20220701 20220703
```

```
$ ll OUT_NC_MESHIMA/
```

合計 12K  
-rw-r--r--. 1 am 4.0K 2022-11-30 22:55 20220701_MESHIMA_MSM-S_HINTPL.nc  
-rw-r--r--. 1 am 4.0K 2022-11-30 22:55 20220702_MESHIMA_MSM-S_HINTPL.nc  
-rw-r--r--. 1 am 4.0K 2022-11-30 22:55 20220703_MESHIMA_MSM-S_HINTPL.nc  



## スクリプト

### MSM-S_HINTPL.sh

```
#!/bin/bash

YYYY=$1; MMDD=$2

YYYY=${YYYY:-2022}; MMDD=${MMDD:-0705}

INDIR=/work01/DATA/MSM/MSM-S/$YYYY
INFLE=${MMDD}.nc

STN=MESHIMA; LAT=31.9980555556; LON=128.350833333

OFLE=${YYYY}${MMDD}_${STN}_$(basename $0 .sh).nc

ODIR=OUT_NC_${STN}
mkdir -vp $ODIR
OUT=${ODIR}/${OFLE}
rm -vf $OUT

# ある緯度経度地点における値をbilinear interpolationを用いて# 抽出する
# https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

# 女島 (長崎県)
# 北緯31度59分53秒  31+59/60.+53/3600.
# 東経128度21分03秒 128 + 21./60. + 3/3600.

cdo seltimestep,1,4,7,9,12,15,18,21 -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OUT

echo
echo $STN $LAT $LON
echo
echo INPUT: $INDIR/$INFLE
echo OUTPUT: $OUT
```



### MSM-S_HINTPL.RUN.sh

```
#!/bin/bash

EXE=MSM-S_HINTPL.sh

if [ ! -f $EXE ]; then
echo ERROR: NO SUCH FILE, $EXE; exit 1
fi

# 日付の処理
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

if [ $# -ne 2 ]; then
  echo
  echo Error in $0 : Wrong arguments.
  echo $0 yyyymmdd1 yyyymmdd2
  echo
  exit 1
fi

start=$1
end=$2

yyyymmdd1=$1
yyyymmdd2=$2

yyyy1=${yyyymmdd1:0:4}
  mm1=${yyyymmdd1:4:2}
  dd1=${yyyymmdd1:6:2}

yyyy2=${yyyymmdd2:0:4}
  mm2=${yyyymmdd2:4:2}
  dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}
  end=${yyyy2}/${mm2}/${dd2}

#echo $start
#echo $end


jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

#echo "nday=" $nday

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}
  mm=${date_out:4:2}
  dd=${date_out:6:2}

  $EXE ${yyyy} ${mm}${dd}

  i=$(expr $i + 1)
done

exit 0
```



### CHK_MSM-S_HINTPL.sh

```
#!/bin/bash

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

GS=$(basename $0 .sh).GS

INFLE=$1
INFLE=${INFLE:-20220705_MESHIMA_MSM-S_HINTPL.nc}
INDIR=OUT_NC_MESHIMA
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo ERROR: NO SUCH FILE, $IN; ehco;exit 0;fi
FIG=$(basename $INFLE .nc).eps

# LEV="1000 900"

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

'sdfopen ${IN}'

'set z 1'

'cc'; 'set grid off'; 'set grads off'


xleft=1; ytop=8

xmax=1; ymax=3
xwid = 6.0/xmax; ywid = 10.0/ymax
xmargin=1; ymargin=0.7

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

xmap=1;ymap=1
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid


'set parea 'xs ' 'xe' 'ys' 'ye

'set xlint 1';'set ylint 0.5'

'set t 1 8'

'q dims'; TEMP=sublin(result,5); DTIME=subwrd(TEMP,6)

 'd sp/100.'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# TEXT
x=xl ; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' SURFACE PRESSURE [hPa]'


# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=9.5
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' $INFLE'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo
```


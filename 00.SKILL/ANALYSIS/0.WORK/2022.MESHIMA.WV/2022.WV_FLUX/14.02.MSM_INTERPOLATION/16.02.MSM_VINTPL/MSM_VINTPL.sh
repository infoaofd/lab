#!/bin/bash

YYYY=2022; MMDD=0601
INDIR=/work01/DATA/MSM/MSM-P/$YYYY
INFLE=${MMDD}.nc

OFLE=${YYYY}${MMDD}_${STN}_$(basename $0 .sh).nc




cdo -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OFLE

echo
echo $STN $LAT $LON
echo
echo INPUT: $INDIR/$INFLE
echo OUTPUT: $OFLE

# ncdump -h $OFLE


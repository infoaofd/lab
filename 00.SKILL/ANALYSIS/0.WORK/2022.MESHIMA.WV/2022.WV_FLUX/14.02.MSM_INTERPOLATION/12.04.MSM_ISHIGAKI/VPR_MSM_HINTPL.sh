#!/bin/bash

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

GS=$(basename $0 .sh).GS

INFLE=$1
INFLE=${INFLE:-20220705_ISHIGAKI_MSM_HINTPL.nc}

FIG=VPR_$(basename $INFLE .nc).eps

LEV="1000 700"

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->deepskyblue->lightcyan->white->orange->red->crimson'
# FS=2
# UNIT=

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Fri, 30 Sep 2022 10:15:30 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.NAKAMURO_FLUX/14.02.TEST_CDO/12.02.MSM_HINTPL

'sdfopen ${INFLE}'

'set lev ${LEV}'

nmax=8; xmax=3; ymax=4

'cc'; 'set grid off'; 'set grads off'

xleft=0.5; ytop=9

xwid = 6.0/xmax; ywid = 10.0/ymax

xmargin=0.7; ymargin=0.7

# SET PAGE
'set vpage 0.0 8.5 0.0 11'


nmap=1; xmap=1; ymap=1

while ( nmap <= nmax )

mod=math_mod(nmap, xmax)

#say 'xs='xs; say 'xleft='xleft; say 'xwid='xwid; say 'xmargin='xmargin; 'xmap='xmap
#say 'ys='ys; say 'ytop='ytop; say 'ywid='ywid; say 'ymargin='ymargin; 'ymap='ymap


xs = xleft + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid


'set parea 'xs ' 'xe' 'ys' 'ye

#say 'mod='mod'   nmap='nmap'   xmap='xmap'   ymap='ymap

'set xlevs 10 15 20 25';'set ylint 50'

'set t 'nmap

'q dims'; TEMP=sublin(result,5); DTIME=subwrd(TEMP,6)

# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TUTORIAL_04_MET_VARS.md#%E6%B0%B4%E8%92%B8%E6%B0%97%E6%B7%B7%E5%90%88%E6%AF%94-water-vapor-mixing-ratio

'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'
'QV=QV*1000.0'

 'd QV'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# TEXT
x=xl+0.2 ; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' 'DTIME


if ( mod = 0 )
xmap=1
ymap=ymap+1
else
xmap=xmap+1
endif

nmap=nmap+1

endwhile #nmap



# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=9.5
'draw string ' xx ' ' yy ' $LAT $LON'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

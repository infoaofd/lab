#!/bin/bash

EXE=ITPL_SFC-PRS_NCL.ncl

if [ ! -f $EXE ]; then
echo ERROR: NO SUCH FILE, $EXE; exit 1
fi

# 日付の処理
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

if [ $# -ne 2 ]; then
  echo
  echo Error in $0 : Wrong arguments.
  echo $0 yyyymmdd1 yyyymmdd2
  echo
  exit 1
fi

start=$1
end=$2

yyyymmdd1=$1
yyyymmdd2=$2

yyyy1=${yyyymmdd1:0:4}
  mm1=${yyyymmdd1:4:2}
  dd1=${yyyymmdd1:6:2}

yyyy2=${yyyymmdd2:0:4}
  mm2=${yyyymmdd2:4:2}
  dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}
  end=${yyyy2}/${mm2}/${dd2}

STN=MESHIMA
ODIR=OUT_$(basename $0 .sh)
mkdir -vp $ODIR


jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

#echo "nday=" $nday

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}
  mm=${date_out:4:2}
  dd=${date_out:6:2}

  OFLE=QV_UV_${STN}_${yyyy}${mm}${dd}.TXT
  OUT=${ODIR}/$OFLE
  rm -vf $OUT
# runncl.sh $EXE ${yyyy}${mm}${dd}
 runncl.sh $EXE ${yyyy}${mm}${dd} > $OUT

  if [ -f $OUT ]; then
    echo OUTPUT: $OUT
  else
    echo ERROR in $EXE : ${yyyy}${mm}${dd}
  fi

  i=$(expr $i + 1)
done

exit 0


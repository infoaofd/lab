; NCL tips
; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Top

begin

YYYYMMDD  = getenv("NCL_ARG_2")

INDIR1="/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.08.MSM_HINTPL_SEQUENTIAL/OUT_NC_MESHIMA"
INFLE1=YYYYMMDD+"_MESHIMA_MSM_HINTPL.nc"

INDIR2="/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.12.MSM-S_HINTPL_SEQUENTIAL/OUT_NC_MESHIMA"
INFLE2=YYYYMMDD+"_MESHIMA_MSM-S_HINTPL.nc"


IN1=INDIR1+"/"+INFLE1
IN2=INDIR2+"/"+INFLE2

print("MMMMM READ PRESSURE LEVEL DATA")
f1=addfile(IN1,"r")

rh=short2flt(f1->rh)
temp=short2flt(f1->temp)
z=f1->z
p=f1->p
time=f1->time
lon=f1->lon
lat=f1->lat

print("MMMMM READ SURFACE DATA")
f2=addfile(IN2,"r")

sp=short2flt(f2->sp)/100.
t2=short2flt(f2->temp)
rh2=short2flt(f2->rh)


dims=dimsizes(temp)
;print(dims)
nt=dims(0)
np=dims(1)
nlat=dims(2)
nlon=dims(3)

zOBS=100.

pin=new((/nt,np,nlat,nlon/),typeof(p))
copy_VarMeta(temp,pin)

pin=conform(temp,p,1)


Qv = mixhum_ptrh (pin, temp, rh, 1)
copy_VarMeta(z,Qv)
Qv@long_name="Vapor mixing ratio"
Qv@standard_name="Qv"
Qv@units="kg/kg"

Q2=mixhum_ptrh (sp, t2, rh2, 1)

QvITP=new((/nt/),typeof(Qv))
QvITP@z=zOBS
QvITP@lon=lon
QvITP@lat=lat
QvITP@_FillValue=Qv@_FillValue
QvITP@long_name=Qv@long_name
QvITP@units=Qv@units
;printVarSummary(QvITP)


date=cd_calendar(time, 0)
year   = tointeger(date(:,0))
month  = tointeger(date(:,1))
day    = tointeger(date(:,2))
hour   = tointeger(date(:,3))
date_str = sprinti("%0.4i ", year) + \
sprinti("%0.2i ", month) + \ 
sprinti("%0.2i ", day) + \ 
sprinti("%0.2i ", hour) 

z1D=new((/np+1/),typeof(z))
p1D=new((/np+1/),typeof(p))
Qv1D=new((/np+1/),typeof(Qv))

Qv1D!0=Qv!1
z1D!0=z!1
p1D!0=pin!1
;printVarSummary(z1D)
;printVarSummary(Qv1D)

pref=new((/np+1/),typeof(p))
pref(0)=1005
pref(1:np)=p


print("TIME_UTC      ZOBS    QvITP") 

do n=0,nt-1

z1D(0)=2.0
z1D(1:np)=z(0,:,0,0)

p1D(0)=(/sp(n,0,0)/)
p1D(1:np)=pin(0,:,0,0)

Qv1D(0)=(/Q2(n,0,0)/)
Qv1D(1:np)=(/Qv(n,:,0,0)/)

; VERTICAL INTERPOLATION TO FIND Qv at zOBS
QvITP(n) = linint1_n_Wrap (z1D,Qv1D, False, zOBS, 0, 0)

;do k=0,2; np-1
;print("p="+p1D(k)+" z="+z1D(k)+" Qv1D="+Qv1D(k))
;end do ;k
print(date_str(n)+" "+zOBS +" "+QvITP(n))
;print("")
end do ;n 

;printVarSummary(QvITP)



end


#!/bin/bash

YYYY=$1
M=$2

YYYY=${YYYY:-2020}
M=${M:-5}
MM=$(printf %02d $M)

INDIR=/work01/DATA/MSM.GRIB/${YYYY}/${MM}

INLIST1=$(ls $INDIR/*Lsurf*FH00-15*_grib2.bin)
INLIST2=$(ls $INDIR/*L-pall*FH00-15*_grib2.bin)

#INLIST1=/work01/DATA/MSM.GRIB/2020/05/Z__C_RJTD_20200531000#000_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin

#INLIST2=/work01/DATA/MSM.GRIB/2020/05/Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin


ODIR=/work01/DATA/MSM.FCT/; mkdir -vp $ODIR

RDM=${ODIR}/00.README.TXT
date -R  > $RDM
pwd     >> $RDM
echo $0 >> $RDM
echo $LONW $LONE $LATS $LATN >> $RDM


for IN in $INLIST1; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

echo runncl.sh 00.MSM.FCT.SFC.ncl $IN $ODIR

runncl.sh 00.MSM.FCT.SFC.ncl $IN $ODIR

done # IN


for IN in $INLIST2; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

echo runncl.sh 00.MSM.FCT.PRS.ncl $IN $ODIR

runncl.sh 00.MSM.FCT.PRS.ncl $IN $ODIR

done # IN



echo
cp -a $0 $ODIR
cp -av /work01/DATA/LFM/*.pdf $ODIR

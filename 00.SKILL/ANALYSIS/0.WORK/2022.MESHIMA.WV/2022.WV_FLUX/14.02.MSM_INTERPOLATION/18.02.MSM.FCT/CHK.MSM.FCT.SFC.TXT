fatal:syntax error: line 3 in file CHK.MSM.FCT.sh before or near # 
#
^

fatal:error in statement

Variable: f
Type: file
filename:	Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin
path:	/work01/DATA/MSM.GRIB/2020/05/Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin
   file global attributes:
   dimensions:
      lat_0 = 505
      lon_0 = 481
      forecast_time0 = 15
   variables:
      float TMP_P0_L103_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Temperature
         units :	K
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Temperature
         parameter_template_discipline_category_number :	( 0, 0, 0, 0 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float RH_P0_L103_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Relative humidity
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 0, 0, 1, 1 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float UGRD_P0_L103_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	U-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 2 )
         level_type :	Specified height level above ground (m)
         level :	10
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float VGRD_P0_L103_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	V-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 3 )
         level_type :	Specified height level above ground (m)
         level :	10
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float PRES_P0_L1_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 0 )
         level_type :	Ground or water surface
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float PRMSL_P0_L101_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure reduced to MSL
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 1 )
         level_type :	Mean sea level (Pa)
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float TCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Total cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 1 )
         level_type :	Ground or water surface
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float LCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Low cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 3 )
         level_type :	Ground or water surface
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float MCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Medium cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 4 )
         level_type :	Ground or water surface
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float HCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         generating_process_type :	Initialization
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	High cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 5 )
         level_type :	Ground or water surface
         forecast_time :	0
         forecast_time_units :	hours
         initial_time :	05/31/2020 (00:00)

      float TMP_P0_L103_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Temperature
         units :	K
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Temperature
         parameter_template_discipline_category_number :	( 0, 0, 0, 0 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         initial_time :	05/31/2020 (00:00)

      float RH_P0_L103_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Relative humidity
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 0, 0, 1, 1 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         initial_time :	05/31/2020 (00:00)

      float UGRD_P0_L103_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	U-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 2 )
         level_type :	Specified height level above ground (m)
         level :	10
         initial_time :	05/31/2020 (00:00)

      float VGRD_P0_L103_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	V-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 3 )
         level_type :	Specified height level above ground (m)
         level :	10
         initial_time :	05/31/2020 (00:00)

      float PRES_P0_L1_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 0 )
         level_type :	Ground or water surface
         initial_time :	05/31/2020 (00:00)

      float PRMSL_P0_L101_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure reduced to MSL
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 1 )
         level_type :	Mean sea level (Pa)
         initial_time :	05/31/2020 (00:00)

      float TCDC_P0_L1_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Total cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 1 )
         level_type :	Ground or water surface
         initial_time :	05/31/2020 (00:00)

      float LCDC_P0_L1_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Low cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 3 )
         level_type :	Ground or water surface
         initial_time :	05/31/2020 (00:00)

      float MCDC_P0_L1_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Medium cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 4 )
         level_type :	Ground or water surface
         initial_time :	05/31/2020 (00:00)

      float HCDC_P0_L1_GLL0_1 ( forecast_time0, lat_0, lon_0 )
         generating_process_type :	Forecast
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	High cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 5 )
         level_type :	Ground or water surface
         initial_time :	05/31/2020 (00:00)

      float APCP_P8_L1_GLL0_acc1h ( forecast_time0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Total precipitation
         units :	kg m-2
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 8, 0, 1, 8 )
         level_type :	Ground or water surface
         type_of_statistical_processing :	Accumulation
         statistical_process_duration :	1 hours (ending at forecast time)
         initial_time :	05/31/2020 (00:00)

      float DSWRF_P8_L1_GLL0_avg1h ( forecast_time0, lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Downward short-wave radiation flux
         units :	W m-2
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Short wave radiation
         parameter_template_discipline_category_number :	( 8, 0, 4, 7 )
         level_type :	Ground or water surface
         type_of_statistical_processing :	Average
         statistical_process_duration :	1 hours (ending at forecast time)
         initial_time :	05/31/2020 (00:00)

      integer forecast_time0 ( forecast_time0 )
         long_name :	Forecast offset from initial time
         units :	hours

      float lat_0 ( lat_0 )
         long_name :	latitude
         grid_type :	Latitude/Longitude
         units :	degrees_north
         Dj :	0.05
         Di :	0.0625
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6

      float lon_0 ( lon_0 )
         long_name :	longitude
         grid_type :	Latitude/Longitude
         units :	degrees_east
         Dj :	0.05
         Di :	0.0625
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6


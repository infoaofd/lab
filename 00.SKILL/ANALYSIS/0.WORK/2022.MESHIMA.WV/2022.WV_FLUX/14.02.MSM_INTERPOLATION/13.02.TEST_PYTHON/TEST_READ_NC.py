"""
NetCDF
https://qiita.com/roy29fuku/items/6ff09dd254302dcebb8d

Python-NetCDF
https://mizu.bosai.go.jp/wiki2/wiki.cgi?page=Python%2DNetCDF%B2%F2%C0%CF+%28%A5%C7%A1%BC%A5%BF%B2%F2%C0%CF%CA%D4%29

"""

import netCDF4
import sys
import inspect

if len(sys.argv) < 3:
   print ("WRONG ARGUMENT: INDIR INFLE")
   exit(1)

INDIR = sys.argv[1]
INFLE    = sys.argv[2]   

nc = netCDF4.Dataset(INDIR+"/"+INFLE,"r")

lon=nc.variables['lon'][:]
lat=nc.variables['lat'][:]
prs=nc.variables['p'][:]
temp=nc.variables['temp'][:]
rh=nc.variables['rh'][:]
z=nc.variables['z'][:]
u=nc.variables['u'][:]
v=nc.variables['v'][:]

print("LON")
print(lon)
print("LAT")
print(lat)
print("PRS")
print(prs)


"""
if VAR in a:
  print (VAR+" is available.")
else :
  print (VAR+" is unavailable.")
"""


Variable: z
Type: double
Total Size: 1024 bytes
            128 values
Number of Dimensions: 4
Dimensions and sizes:	[time | 8] x [p | 16] x [lat | 1] x [lon | 1]
Coordinates: 
            time: [ 0..21]
            p: [1000..100]
            lat: [31.9980555556..31.9980555556]
            lon: [128.350833333..128.350833333]
Number Of Attributes: 3
  standard_name :	geopotential_height
  long_name :	geopotential height
  units :	m

Variable: z1D
Type: double
Total Size: 128 bytes
            16 values
Number of Dimensions: 1
Dimensions and sizes:	[p | 16]
Coordinates: 
            p: [1000..100]
Number Of Attributes: 6
  lon :	128.350833333
  lat :	31.9980555556
  time :	 0
  units :	m
  long_name :	geopotential height
  standard_name :	geopotential_height
TIME_UTC=2022 06 19 00 
p=1000 z=87.91259431670493 u=0.746177
p=975 z=308.8059475921091 u=1.36391
p=950 z=534.4662749630523 u=0.201835
zOBS=100 uITP=0.77998 vITP=3.46725 wdir=192.678 wspd=3.55389

TIME_UTC=2022 06 19 03 
p=1000 z=76.42637552321548 u=-2.82569
p=975 z=297.3569355018855 u=-2.30581
p=950 z=523.3660630992152 u=-3.03976
zOBS=100 uITP=-2.79724 vITP=1.93981 wdir=124.74 wspd=3.40403

TIME_UTC=2022 06 19 06 
p=1000 z=66.79851500612605 u=-1.30887
p=975 z=287.202305179472 u=-1.98165
p=950 z=512.6147829636649 u=-2.40979
zOBS=100 uITP=-1.34568 vITP=6.31165 wdir=167.964 wspd=6.45351

TIME_UTC=2022 06 19 09 
p=1000 z=58.0623132862016 u=-2.04893
p=975 z=278.8798415722554 u=0.409786
p=950 z=505.6385846755989 u=3.10703
zOBS=100 uITP=-1.91439 vITP=5.49644 wdir=160.797 wspd=5.82028

TIME_UTC=2022 06 19 12 
p=1000 z=59.95653393399129 u=-3.44343
p=975 z=280.6959809229294 u=-2.66055
p=950 z=506.0700817535024 u=0.489297
zOBS=100 uITP=-3.40059 vITP=8.36584 wdir=157.879 wspd=9.03057

TIME_UTC=2022 06 19 15 
p=1000 z=54.87058692003961 u=-1.5107
p=975 z=276.1207793707413 u=1.26606
p=950 z=502.5858890492939 u=5.87768
zOBS=100 uITP=-1.35876 vITP=7.11533 wdir=169.189 wspd=7.24391

TIME_UTC=2022 06 19 18 
p=1000 z=48.14997306126673 u=6.01223
p=975 z=268.7248425213093 u=7.98165
p=950 z=494.5622210146445 u=8.62997
zOBS=100 uITP=6.12 vITP=3.22367 wdir=242.222 wspd=6.91711

TIME_UTC=2022 06 19 21 
p=1000 z=42.65816454297755 u=4.74006
p=975 z=263.4170929511735 u=6.47095
p=950 z=489.2599657778394 u=7.5474
zOBS=100 uITP=4.83478 vITP=3.83209 wdir=231.599 wspd=6.16928


GS=$(basename $0 .sh).GS

#---------------------------------
# ここの設定気をつけること！
#YYYY=2020; MM=07; MMM=JUL; 
YYYY=2022; MM=08; MMM=AUG; 
DDS=07; DDE=08
HHS=15; HHE=12
#---------------------------------
LONW=128.3; LATS=32
# 
# Geological coordinates (from Wikipedia)
# 北緯31度59分53秒   31.9981 
# 東経128度21分03秒 128.3508
LEV=1000

DATES=${DDS}${MMM}${YYYY}
DATEE=${DDE}${MMM}${YYYY}
FIG=$(basename $0 .sh)_${LEV}_${DATES}_${DATEE}.eps
BIN=$(basename $0 .sh)_${LEV}_${DATES}_${DATEE}.BIN
BINT=$(basename $0 .sh)_T_${LEV}_${DATES}_${DATEE}.BIN
#BINP=$(basename $0 .sh)_P_${LEV}_${DATES}_${DATEE}.BIN
BINR=$(basename $0 .sh)_RH_${LEV}_${DATES}_${DATEE}.BIN

cat <<EOF>$GS
'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon $LONW'
'set lat $LATS'
'set lev $LEV'
'set time ${HHS}Z$DATES ${HHE}Z$DATEE'

'q dims'
say result

'tc=(temp-273.15)'

'es= 6.112*exp((17.67*tc)/(tc+243.5))' ;# Eq.10 of Bolton (1980)

'e=0.01*rh*es'               ;# Eq.4.1.5 (p. 108) of Emanuel (1994)

'mixr= 0.62197*(e/(lev-e))*1000'   ;# Eq.4.1.2 (p.108) of Emanuel(1994) 


'cc'

'set vrange 5 20'
'd mixr'

'draw title QV${LEV} ${LATS}N ${LONW}E'
'gxprint $FIG'

'set gxout fwrite'
'set fwrite -be ${BIN}'
'd mixr'
 'disable fwrite'

'set gxout fwrite'
'set fwrite -be ${BINT}'
'd tc'
 'disable fwrite'

'set gxout fwrite'
'set fwrite -be ${BINR}'
'd rh'
'quit'
EOF

grads -bcp "$GS"

ls -lh $FIG
ls -lh $BIN $BINT $BINR




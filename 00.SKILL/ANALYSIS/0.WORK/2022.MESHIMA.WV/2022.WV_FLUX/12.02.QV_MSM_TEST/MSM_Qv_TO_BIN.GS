'open MSM-P.CTL'
'q ctlinfo'
say result

'set lon 128.3'
'set lat 32'
'set lev 1000'
'set time 03Z18JUN2022 00Z21JUN2022'

'q dims'
say result

'tc=(temp-273.15)'

'es= 6.112*exp((17.67*tc)/(tc+243.5))' ;# Eq.10 of Bolton (1980)

'e=0.01*rh*es'               ;# Eq.4.1.5 (p. 108) of Emanuel (1994)

'mixr= 0.62197*(e/(lev-e))*1000'   ;# Eq.4.1.2 (p.108) of Emanuel(1994) 


'cc'

'set vrange 5 20'
'd mixr'

'draw title QV1000 32N 128.3E'
'gxprint MSM_Qv_TO_BIN_1000_18JUN2022_21JUN2022.eps'

'set gxout fwrite'
'set fwrite -be MSM_Qv_TO_BIN_1000_18JUN2022_21JUN2022.BIN'
'd mixr'
 'disable fwrite'

'set gxout fwrite'
'set fwrite -be MSM_Qv_TO_BIN_T_1000_18JUN2022_21JUN2022.BIN'
'd tc'
 'disable fwrite'

'set gxout fwrite'
'set fwrite -be MSM_Qv_TO_BIN_RH_1000_18JUN2022_21JUN2022.BIN'
'd rh'
'quit'

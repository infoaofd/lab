#!/bin/bash

FH=$1
FH=${FH:-00}

YYYY=$2; MM=$3; DD=$4
YYYY=${YYYY:-2022}; MM=${MM:-06}; DD=${DD:-19}

# 女島 (長崎県)
# 北緯31度59分53秒  31+59/60.+53/3600.
# 東経128度21分03秒 128 + 21./60. + 3/3600.

STN=MESHIMA; LAT=31.9980555556; LON=128.350833333

INDIR=/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/12.12.LFM-P.CUT/LFM-P_CUT/FH${FH}


for i in $(seq -w 0 23); do

INFLE=LFM_PRS_FH${FH}_VALID_${YYYY}-${MM}-${DD}_${i}.nc

OFLE=LFM_PRE_FH${FH}_${STN}_${YYYY}${MM}${DD}${i}.nc

#echo "$OFLE"
#echo "$INFLE"
#echo "$i"


ODIR=LFM-P_OUT_NC_${STN}
mkdir -vp $ODIR
OUT=${ODIR}/${OFLE}
rm -vf $OUT


# ある緯度経度地点における値をbilinear interpolationを用いて# 抽出する
# https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

cdo -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OUT


echo
echo $STN $LAT $LON
echo
echo INPUT : $INDIR/$INFLE
echo OUTPUT: $OUT


done

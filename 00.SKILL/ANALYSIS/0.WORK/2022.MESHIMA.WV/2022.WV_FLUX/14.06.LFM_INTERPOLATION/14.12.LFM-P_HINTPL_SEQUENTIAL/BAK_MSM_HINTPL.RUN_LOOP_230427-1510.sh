#!/bin/bash


# 日付の処理
# https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

if [ $# -ne 2 ]; then
  echo
  echo Error in $0 : Wrong arguments.
  echo $0 yyyymmdd1 yyyymmdd2
  echo
  exit 1
fi

start=$1
end=$2

yyyymmdd1=$1
yyyymmdd2=$2

yyyy1=${yyyymmdd1:0:4}
  mm1=${yyyymmdd1:4:2}
  dd1=${yyyymmdd1:6:2}

yyyy2=${yyyymmdd2:0:4}
  mm2=${yyyymmdd2:4:2}
  dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}
  end=${yyyy2}/${mm2}/${dd2}

#echo $start
#echo $end


jsstart=$(date -d${start} +%s)
jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400)
jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

#echo "nday=" $nday

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}
  mm=${date_out:4:2}
  dd=${date_out:6:2}


  INDIR=/work01/DATA/MSM/MSM-P/$yyyy
  INFLE=${mm}${dd}.nc

  STN=MESHIMA; LAT=31.9980555556; LON=128.350833333

  OFLE=${yyyy}${mm}${dd}_${STN}_$(basename $0 .RUN_LOOP.sh).nc

  ODIR=OUT_NC_${STN}
  mkdir -vp $ODIR
  OUT=${ODIR}/${OFLE}
  rm -vf $OUT

  # ある緯度経度地点における値をbilinear interpolationを用いて# 抽出する
  # https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

  # 女島 (長崎県)
  # 北緯31度59分53秒  31+59/60.+53/3600.
  # 東経128度21分03秒 128 + 21./60. + 3/3600.

  cdo -remapbil,lon=${LON}_lat=${LAT} $INDIR/$INFLE $OUT

  echo
  echo $STN $LAT $LON
  echo
  echo INPUT: $INDIR/$INFLE
  echo OUTPUT: $OUT


  i=$(expr $i + 1)
done

exit 0


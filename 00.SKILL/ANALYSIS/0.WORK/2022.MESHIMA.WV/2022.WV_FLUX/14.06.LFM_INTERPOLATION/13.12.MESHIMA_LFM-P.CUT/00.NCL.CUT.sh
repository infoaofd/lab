#!/bin/bash

FH=$1
FH=${FH:-00}

#INDIR=/work01/DATA/LFM/FH${FH}
#INLIST=$(ls $INDIR/*pall*grib2)

INDIR=/work02/LFM2020-2022JJA/JJA
INLIST=$(ls $INDIR/*pall*grib2*bin)


# 女島 (長崎県)
# 北緯31度59分53秒  31+59/60.+53/3600.
# 東経128度21分03秒 128 + 21./60. + 3/3600.

#LONW=120.0 ;LONE=134.0 ;LATS=22.4; LATN=35.0
LONW=128.350833333 ;LONE=128.350833333 ;LATS=31.9980555556; LATN=31.9980555556

ODIR=/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/13.12.MESHIMA_LFM-P.CUT/LFM-P_OUT_NC_MESHIMA/FH${FH}; mkdir -vp $ODIR
RDM=${ODIR}/00.README.TXT
date -R  > $RDM
pwd     >> $RDM
echo $0 >> $RDM
echo $LONW $LONE $LATS $LATN >> $RDM


###<<COMMENT
#IN=/work01/DATA/LFM/FH${FH}/Z__C_RJTD_20220619230000_LFM_GPV_Rjp_L-pall_FH${FH}00.grib2

for IN in $INLIST; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

runncl.sh 00.NCL.CUT.PRS.ncl $IN $ODIR

done 
###COMMENT

echo
cp -a $0 $ODIR
#cp -av /work01/DATA/LFM/*.pdf $ODIR

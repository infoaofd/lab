
Variable: a
Type: file
filename:	Z__C_RJTD_20220618000000_LFM_GPV_Rjp_Lsurf_FH0300
path:	/work01/DATA/LFM/FH03/Z__C_RJTD_20220618000000_LFM_GPV_Rjp_Lsurf_FH0300.grib2
   file global attributes:
   dimensions:
      lat_0 = 1261
      lon_0 = 1201
   variables:
      float TMP_P0_L103_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Temperature
         units :	K
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Temperature
         parameter_template_discipline_category_number :	( 0, 0, 0, 0 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float RH_P0_L103_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Relative humidity
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 0, 0, 1, 1 )
         level_type :	Specified height level above ground (m)
         level :	1.5
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float UGRD_P0_L103_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	U-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 2 )
         level_type :	Specified height level above ground (m)
         level :	10
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float VGRD_P0_L103_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	V-component of wind
         units :	m s-1
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Momentum
         parameter_template_discipline_category_number :	( 0, 0, 2, 3 )
         level_type :	Specified height level above ground (m)
         level :	10
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float PRES_P0_L1_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 0 )
         level_type :	Ground or water surface
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float PRMSL_P0_L101_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Pressure reduced to MSL
         units :	Pa
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Mass
         parameter_template_discipline_category_number :	( 0, 0, 3, 1 )
         level_type :	Mean sea level (Pa)
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float TCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Total cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 1 )
         level_type :	Ground or water surface
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float LCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Low cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 3 )
         level_type :	Ground or water surface
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float MCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Medium cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 4 )
         level_type :	Ground or water surface
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float HCDC_P0_L1_GLL0 ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	High cloud cover
         units :	%
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Cloud
         parameter_template_discipline_category_number :	( 0, 0, 6, 5 )
         level_type :	Ground or water surface
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float APCP_P8_L1_GLL0_acc ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Total precipitation
         units :	kg m-2
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Moisture
         parameter_template_discipline_category_number :	( 8, 0, 1, 8 )
         level_type :	Ground or water surface
         type_of_statistical_processing :	Accumulation
         statistical_process_duration :	initial time to forecast time
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float DSWRF_P8_L1_GLL0_avg30min ( lat_0, lon_0 )
         center :	Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :	Operational products
         long_name :	Downward short-wave radiation flux
         units :	W m-2
         _FillValue :	1e+20
         grid_type :	Latitude/longitude
         parameter_discipline_and_category :	Meteorological products, Short wave radiation
         parameter_template_discipline_category_number :	( 8, 0, 4, 7 )
         level_type :	Ground or water surface
         type_of_statistical_processing :	Average
         statistical_process_duration :	30 minutes (ending at forecast time)
         forecast_time :	180
         forecast_time_units :	minutes
         initial_time :	06/18/2022 (00:00)

      float lat_0 ( lat_0 )
         long_name :	latitude
         grid_type :	Latitude/Longitude
         units :	degrees_north
         Dj :	0.02
         Di :	0.025
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6

      float lon_0 ( lon_0 )
         long_name :	longitude
         grid_type :	Latitude/Longitude
         units :	degrees_east
         Dj :	0.02
         Di :	0.025
         Lo2 :	150
         La2 :	22.4
         Lo1 :	120
         La1 :	47.6


INPUT: /work01/DATA/LFM/FH03/Z__C_RJTD_20220618000000_LFM_GPV_Rjp_Lsurf_FH0300.grib2

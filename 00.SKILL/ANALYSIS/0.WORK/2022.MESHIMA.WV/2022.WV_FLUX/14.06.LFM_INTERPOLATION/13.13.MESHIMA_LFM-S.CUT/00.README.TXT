/work01/DATA/LFM/FH03
2022-11-28_15-32
$ wgrib2 Z__C_RJTD_20220619230000_LFM_GPV_Rjp_Lsurf_FH0300.grib2
1.1:0:d=2022061923:PRMSL:mean sea level:180 min fcst:
1.2:0:d=2022061923:PRES:surface:180 min fcst:
1.3:0:d=2022061923:UGRD:10 m above ground:180 min fcst:
1.4:0:d=2022061923:VGRD:10 m above ground:180 min fcst:
1.5:0:d=2022061923:TMP:1.5 m above ground:180 min fcst:
1.6:0:d=2022061923:RH:1.5 m above ground:180 min fcst:
1.7:0:d=2022061923:APCP:surface:0-3 hour acc fcst:
1.8:0:d=2022061923:TCDC:surface:180 min fcst:
1.9:0:d=2022061923:LCDC:surface:180 min fcst:
1.10:0:d=2022061923:MCDC:surface:180 min fcst:
1.11:0:d=2022061923:HCDC:surface:180 min fcst:
1.12:0:d=2022061923:DSWRF:surface:150-180 min ave fcst:

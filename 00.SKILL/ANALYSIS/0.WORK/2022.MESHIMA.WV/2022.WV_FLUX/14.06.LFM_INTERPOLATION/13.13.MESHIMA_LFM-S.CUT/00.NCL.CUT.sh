#!/bin/bash

FH=$1
FH=${FH:-03}

#INDIR=/work01/DATA/LFM/FH${FH}
#INLIST=$(ls $INDIR/*surf*grib2)

#INDIR=/work02/LFM2020-2022JJA/JJA
#INLIST=$(ls $INDIR/*surf*grib2*bin)

INDIR=/work02/LFM2020-2022JJA/tmp
INLIST=$(ls $INDIR/*surf*FH${FH}*grib2*bin)


LONW=128.350833333 ;LONE=128.350833333 ;LATS=31.9980555556 ;LATN=31.9980555556

ODIR=/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/13.13.MESHIMA_LFM-S.CUT/LFM-S_OUT_NC_MESHIMA/FH${FH}; mkdir -vp $ODIR
RDM=${ODIR}/00.README.TXT
date -R  > $RDM
pwd     >> $RDM
echo $0 >> $RDM
echo $LONW $LONE $LATS $LATN >> $RDM


### <<COMMENT
### IN=/work01/DATA/LFM/FH${FH}/Z__C_RJTD_20220619230000_LFM_GPV_Rjp_Lsurf_FH${FH}00.grib2

for IN in $INLIST; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

runncl.sh 00.NCL.CUT.SFC.ncl $IN $ODIR

done # IN
### COMMENT


echo
cp -a $0 $ODIR
cp -av /work01/DATA/LFM/*.pdf $ODIR

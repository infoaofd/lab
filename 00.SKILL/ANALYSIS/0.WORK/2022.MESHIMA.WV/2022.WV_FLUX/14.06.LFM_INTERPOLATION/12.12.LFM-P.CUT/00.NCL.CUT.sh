#!/bin/bash

FH=$1
FH=${FH:-06}

INDIR=/work01/DATA/LFM/FH${FH}
INLIST=$(ls $INDIR/*pall*grib2)

LONW=120.0 ;LONE=134.0 ;LATS=22.4; LATN=35.0

ODIR=/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/12.12.LFM-P.CUT/LFM-P_CUT/FH${FH}; mkdir -vp $ODIR
RDM=${ODIR}/00.README.TXT
date -R  > $RDM
pwd     >> $RDM
echo $0 >> $RDM
echo $LONW $LONE $LATS $LATN >> $RDM


###<<COMMENT
#IN=/work01/DATA/LFM/FH${FH}/Z__C_RJTD_20220619230000_LFM_GPV_Rjp_L-pall_FH${FH}00.grib2

for IN in $INLIST; do

if [ ! -f $IN ]; then echo NO SUCH FILE, $IN ; echo; fi

runncl.sh 00.NCL.CUT.PRS.ncl $IN $ODIR

done 
###COMMENT

echo
cp -a $0 $ODIR
cp -av /work01/DATA/LFM/*.pdf $ODIR

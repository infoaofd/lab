; NCL tips
; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Top

begin

FH=getenv("NCL_ARG_2")

YYYYMMDD  = getenv("NCL_ARG_3")

HH  = getenv("NCL_ARG_4")

INDIR1="/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/13.12.MESHIMA_LFM-P.CUT/LFM-P_OUT_NC_MESHIMA/"+FH
INFLE1="LFM_PRS_"+FH+"_MESHIMA_"+YYYYMMDD+HH+".nc"

INDIR2="/work03/2021/nakamuro/2022.NAKAMURO_FLUX/14.06.LFM_INTERPOLATION/13.13.MESHIMA_LFM-S.CUT/LFM-S_OUT_NC_MESHIMA/"+FH
INFLE2="LFM_SFC_"+FH+"_MESHIMA_"+YYYYMMDD+HH+".nc"

IN1=INDIR1+"/"+INFLE1
IN2=INDIR2+"/"+INFLE2

;print(IN1)
;print(IN2)


;print("MMMMM READ SURFACE DATA")
f2=addfile(IN2,"r")

sp  = new((/1/),float)
t2  = new((/1/),float)
rh2 = new((/1/),float)

sp=f2->sp
sp=sp/100 ;[Pa]から[hPa]にする
t2=f2->temp
rh2=f2->rh

;printVarSummary(sp)
;printVarSummary(t2)
;printVarSummary(rh2)

;print(sp(0)+" "+t2(0)+" "+rh2(0))
 
Q2=mixhum_ptrh (sp, t2, rh2, 1)

Q2!0="p" ;Q2の0次元目に【p】という名前を付ける
sp!0="p" ;spの0次元目に【p】という名前を付ける

;print(sp(0)+" "+t2(0)+" "+rh2(0)+" "+Q2(0))


;print("MMMMM READ PRESSURE LEVEL DATA")
f1=addfile(IN1,"r")

rh    = new((/12/),float)
temp  = new((/16/),float)
z     = new((/16/),float)
p0p   = new((/16/),float)
p1p   = new((/12/),float)


rh=f1->rh
temp=f1->temp
z=f1->z
p0p=f1->p0 ;100hPaまで
p0p=p0p/100 ;[Pa]から[hPa]にする
p1p=f1->p1 ;300hPaまで
p1p=p1p/100 ;[Pa]から[hPa]にする
time=f1->time

temp300=temp(4:15)
temp300=temp300(::-1)

z300=z(4:15)
z300=z300(::-1)

p1p=p1p(::-1)

rh=rh(::-1)

temp300!0="p"
temp300&p=p1p ;p1pの配列が、temp300のpの配列に入る
 
z300!0="p"
z300&p=p1p ;p1pの配列が、z300のpの配列に入る
 
p1p!0="p"
p1p&p=p1p ;p1pの配列が、p1pのpの配列に入る
 
rh!0="p"
rh&p=p1p ;p1pの配列が、rhのpの配列に入る

;do k=0,2
;print(p1p(k)+" "+z300(k)+" "+temp300(k)+" "+rh(k))
;end do ;k

Qv = mixhum_ptrh (p1p, temp300, rh, 1)
copy_VarMeta(temp300,Qv)
Qv@long_name="Vapor mixing ratio"
Qv@standard_name="Qv"
Qv@units="kg/kg"

;printVarSummary(p1p)
;printVarSummary(temp300)
;printVarSummary(rh)
;printVarSummary(Qv)

;do k=0,2
;print(p1p(k)+" "+temp300(k)+" "+rh(k)+" "+Qv(k))
;end do ;k


date=cd_calendar(time, 0)
year   = tointeger(date(:,0))
month  = tointeger(date(:,1))
day    = tointeger(date(:,2))
hour   = tointeger(date(:,3))
date_str = sprinti("%0.4i ", year) + \
sprinti("%0.2i ", month) + \ 
sprinti("%0.2i ", day) + \ 
sprinti("%0.2i ", hour)

year_str = sprinti("%0.4i",year)
month_str = sprinti("%0.2i",month)
day_str = sprinti("%0.2i",day)
hour_str = sprinti("%0.2i",hour)

;print("DATE  = "+date_str)
;print("YEAR  = "+year_str)
;print("MONTH = "+month_str)
;print("DAY   = "+day_str)
;print("HOUR  = "+hour_str)


dims = dimsizes(Qv) ;Qvの次元の大きさをdimsに返す
np = dims(0) ;気圧のデータ

z1D=new((/np+1/),typeof(z300))
p1D=new((/np+1/),typeof(p1p))
Qv1D=new((/np+1/),typeof(Qv))

;printVarSummary(z1D)
;printVarSummary(p1D)
;printVarSummary(Qv1D)

z1D(0)=2.0
z1D(1:12)=z300(:)
;print(z1D(:))


;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
if(z1D(0).lt.z1D(1)) then
;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


z1D13  = new((/13/),typeof(z1D))
p1D13  = new((/13/),typeof(p1D))
Qv1D13 = new((/13/),typeof(z1D))

z1D13(0)=2.0
z1D13(1:12)=z300(:)

p1D13(0)=sp(0)
p1D13(1:12)=p1p(:)

Qv1D13(0)=Q2(0)
Qv1D13(1:12)=Qv(:)

z1D13!0="p"
p1D13!0="p"
Qv1D13!0="p"


zOBS=100.0
; VERTICAL INTERPOLATION TO FIND Qv at zOBS
QvITP = linint1_n_Wrap (z1D13, Qv1D13, False, zOBS, 0, 0)


;print("MMMMM VERTICAL PROFILE")
;do k=0,12
;print("z1D13("+k+")="+z1D13(k)+"   p1D13("+k+")="+p1D13(k)+"   Qv1D13("+k+")="+Qv1D13(k))
;end do


;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
else
;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


z1D12=new((/12/),typeof(z1D))
p1D12=new((/12/),typeof(p1D))
Qv1D12=new((/12/),typeof(Qv1D))

z1D12(0)=2.0
z1D12(1:11)=z300(1:11)

p1D12(0)=sp(0)
p1D12(1:11)=p1p(1:11)

Qv1D12(0)=Q2(0)
Qv1D12(1:11)=Qv(1:11)

z1D12!0="p"
p1D12!0="p"
Qv1D12!0="p"


zOBS=100.0
; VERTICAL INTERPOLATION TO FIND Qv at zOBS
QvITP = linint1_n_Wrap (z1D12, Qv1D12, False, zOBS, 0, 0)


;print("MMMMM VERTICAL PROFILE")
;do k=0,11
;print("z1D12("+k+")="+z1D12(k)+"   p1D12("+k+")="+p1D12(k)+"   Qv1D12("+k+")="+Qv1D12(k))
;end do


;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
end if
;NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


;print("MMMMM RESULT")
print("FH=,"+FH+", DATE=,"+year_str+","+month_str+","+day_str+","+hour_str+", zOBS=,"+zOBS+", Qv=,"+QvITP)
;print("END")
;print("")


end

; NCL tips
; http://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/index.php?Top

begin

INDIR="/work03/am/2022.NAKAMURO_FLUX/14.02.TEST_INTERPOLATION/12.02.MSM_HINTPL"
INFLE="20220705_MESHIMA_MSM_HINTPL.nc"

IN=INDIR+"/"+INFLE

f=addfile(IN,"r")
;print(f)

rh=short2flt(f->rh)
temp=short2flt(f->temp)

rh_short=f->rh
temp_short=f->temp

z=f->z
p=f->p
time=f->time
lon=f->lon
lat=f->lat


printVarSummary(rh)
printVarSummary(rh_short)
;printVarSummary(temp)
;printVarSummary(temp_short)

dims=dimsizes(temp)
;print(dims)
nt=dims(0)
np=dims(1)
nlat=dims(2)
nlon=dims(3)

pin=new((/nt,np,nlat,nlon/),typeof(p))
copy_VarMeta(temp,pin)
printVarSummary(pin)

pin=conform(temp,p,1)
;do n=0,nt-1
;pin(n,:,0,0)=p(:)
;end do

print(pin)

Qv = mixhum_ptrh (pin, temp, rh, 1)

date=cd_calendar(time, 0)
year   = tointeger(date(:,0))
month  = tointeger(date(:,1))
day    = tointeger(date(:,2))
hour   = tointeger(date(:,3))
date_str = sprinti("%0.4i ", year) + \
sprinti("%0.2i ", month) + \ 
sprinti("%0.2i ", day) + \ 
sprinti("%0.2i ", hour) 
 
do n=0,nt-1
print("TIME_UTC="+date_str(n)) 
print("p="+p(:)+" Qv="+Qv(n,:,0,0))
print("")
end do

end


#!/bin/bash

YYYYMMDDHH=$1
INDIR=$2

YYYYMMDDHH=${YYYYMMDD:=20200713}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

INDIR=${INDIR:-/work01/DATA/MSM/MSM-P/${YYYY}}
if [ ! -d $INDIR ];then echo NO SUCH DIR, $INDIR; exit 1;fi

TIME=${HH}Z${DD}${MMM}${YYYY}

INFLE=${MM}${DD}.nc; IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN; exit 1;fi

GS=$(basename $0 .sh).GS

LONW=120 ;LONE=132 ; LATS=24 ;LATN=35
LEV=975

FIG=MSM_QV_UV_${LEV}_${YYYY}${MM}${DD}.pdf


LEVS="13 20 1" #LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND="-kind white->antiquewhite->mistyrose->lightpink->mediumvioletred->navy->darkblue->blue->dodgerblue->aqua"
FS=1
UNIT=g/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

# ${NOW}
# ${HOST}
# ${CWD}

'sdfopen ${IN}'

'set vpage 0.0 8.5 0.0 11'
'cc'
'set rgb 99 0 255 0'
'set mpdset hires'; 'set map 99 1 2 1'
'set xlopts 1 2 0.08';'set ylopts 1 2 0.08';
'set xlevs 120 125 130';'set ylint 2'

ytop=10
xmax = 3; ymax = 4
xwid = 5/xmax; ywid = 8.0/ymax
xmargin=0.5; ymargin=0.3


t=0
nmap = 1; ymap = 1
while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grads off';'set grid off'
'set xlab on';'set ylab on'



# WATER VAPOR MIXING RATIO
# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TUTORIAL_04_MET_VARS.md

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${LEV}'

if (t<8) ;###
t=t+1 ;# FOR SNAPSHOTS
'set t 't

'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'

'QVOUT=QV'
'UOUT=u'; 'VOUT=v'

else ;###


'set t 1 8'
'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'

'set t 1'
'QVOUT=ave(QV,t=1,t=8)'
'UOUT=ave(u,t=1,t=8)'; 'VOUT=ave(v,t=1,t=8)'; 

endif ;###


xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set t 't
if (nmap=9)
timeout=substr(timeout,4,9)
else
'q dims';line=sublin(result,5);timeout=subwrd(line,6)
endif
say 'MMMMM 'timeout

'color ${LEVS} ${KIND} -gxout shaded' ;# SET COLOR BAR

'd QVOUT*1000.'

# 白抜きベクトル＋図の右下に凡例
# https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/0.GRADS_TIPS.md
'set gxout vector'
'set cthick 6'; 'set ccolor  0'
'vec.gs skip(u,15);v -SCL 0.5 30 -P 20 20 -SL m/s'

'q gxinfo' ;## GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

xx=6.8; yy=3.05
'set cthick  2'; 'set ccolor  1'
if(nmap = 1)
'vec.gs skip(u,15);v -SCL 0.5 30 -P 'xx' 'yy' -SL m/s'
else
'vec.gs skip(u,15);v -SCL 0.5 30 -P 20 20 -SL m/s'
endif

# 女島の位置に点を打つ (128+21/60+3/3600, 31+59/60+53/3600)
'markplot 128.3508 31.998 -c 2 -m 3 -s 0.05'

say
x=xl; y=yt+0.1
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' 'timeout' ${LEV}hPa'

nmap=nmap+1
xmap=xmap+1
endwhile ;#xmap

if(nmap >= 9)
break
endif

ymap=ymap+1
endwhile ;#ymap

# LEGEND COLOR BAR
x1=2; x2=6; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 2 -line on -edge circle'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'



# TEXT
#x=xl ;# (xl+xr)/2; 
#y=yt+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=10.3
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

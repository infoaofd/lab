#!/bin/bash
P_INIT=500m; TEXT="$P_INIT"
LONW=128; LONE=132.5; LATS=31; LATN=35 #MAP REGION
ALATS=32.7; ALATN=34.0; ALONW=129.2; ALONE=132.1 # NORTHERN KYUSHU

LEVS1="50 500 50"; LEVS2="150 1500 150"
KIND='-kind white->lavender->cornflowerblue->lime->yellow->orange->red->magenta'
FS=4
UNIT=J/kg


# 日付の処理
yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20210812}; yyyymmdd2=${yyyymmdd2:-20210812}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do

  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  YYYY=${date_out:0:4}; MM=${date_out:4:2}; DD=${date_out:6:2}

# printfでも"08"や"09"を有効にする
# https://rcmdnk.com/blog/2019/09/09/computer-bash-zsh/
MM=${MM#0}; DD=${DD#0}

MM=$(printf %02d $MM)
if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi
if [ $MM = "08" ]; then MMM="AUG"; fi

DD=$(printf %02d $DD)

INDIR1=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/OUT_ECAPE500m_NC4_RH/KYUSHU
if [ ! -d $INDIR1 ];then echo NO SUCH DIR,$INDIR1;exit 1;fi

INDIR2=/work02/DATA/MSM.NC/OUT_0.ECAPE_NO_ENTRAINMENT_500m/KYUSHU
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi

INDIR3="/work02/DATA/RADAR_AMEDAS/5km.GRID_P3H/${YYYY}"
if [ ! -d $INDIR3 ];then echo NO SUCH DIR,$INDIR3;exit 1;fi


CTL1=$(basename $0 .sh)_ECAPE.CTL
cat <<EOF>$CTL1
dset $INDIR1/ECAPE_RH_%y4-%m2-%d2_%h2_500m_0.0.nc
title ECAPE
undef 1e+20
dtype netcdf
options template yrev
xdef 37 linear 128 0.125
ydef 41 levels 31  31.1  31.2  31.3  31.4  31.5  31.6  31.7  31.8  31.9  32  32.1  
32.2  32.3  32.4  32.5  32.6  32.7  32.8  32.9  33  33.1  33.2  33.3  
33.4  33.5  33.6  33.7  33.8  33.9  34  34.1  34.2  34.3  34.4  34.5  
34.6  34.7  34.8  34.9  35
zdef 16 levels 1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100
tdef 100000 linear 00Z01JUN2020 180mn
vars 9
DLFC=>dlfc  0  t,y,x  DLFC
DLCL=>dlcl  0  t,y,x  DLCL
CAPE=>cape  0  t,y,x  CAPE
CIN=>cin  0  t,y,x  CIN
temp=>temp  16  t,z,y,x  temp
qv=>qv  16  t,z,y,x  qv
z=>z  16  t,z,y,x  z
sp=>sp  0  t,y,x  sp
HGT=>hgt  0  t,y,x  HGT
endvars
EOF

CTL2=$(basename $0 .sh)_CAPE.CTL
cat <<EOF>$CTL2
dset $INDIR2/ECAPE_%y4-%m2-%d2_%h2_500m_NO_ENTRAINMENT.nc
title ECAPE
undef 1e+20
dtype netcdf
options template yrev
xdef 37 linear 128 0.125
ydef 41 levels 31  31.1  31.2  31.3  31.4  31.5  31.6  31.7  31.8  31.9  32  32.1  
32.2  32.3  32.4  32.5  32.6  32.7  32.8  32.9  33  33.1  33.2  33.3  
33.4  33.5  33.6  33.7  33.8  33.9  34  34.1  34.2  34.3  34.4  34.5  
34.6  34.7  34.8  34.9  35
zdef 16 levels 1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100
tdef 100000 linear 00Z01JUN2020 180mn
vars 9
DLFC=>dlfc  0  t,y,x  DLFC
DLCL=>dlcl  0  t,y,x  DLCL
CAPE=>cape  0  t,y,x  CAPE
CIN=>cin  0  t,y,x  CIN
temp=>temp  16  t,z,y,x  temp
qv=>qv  16  t,z,y,x  qv
z=>z  16  t,z,y,x  z
sp=>sp  0  t,y,x  sp
HGT=>hgt  0  t,y,x  HGT
endvars
EOF


  h=0;he=21;dh=3

  while [ $h -le $he ]; do
    HH=$(printf %02d $h)
#   echo ${YYYY} ${MM} ${DD} ${HH}


INFLE1=ECAPE_RH_${YYYY}-${MM}-${DD}_${HH}_${P_INIT}_0.0.nc
IN1=$INDIR1/$INFLE1
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi

INFLE2=ECAPE_${YYYY}-${MM}-${DD}_${HH}_${P_INIT}_NO_ENTRAINMENT.nc
IN2=$INDIR2/$INFLE2
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

INFLE3="P3H_${YYYY}-${MM}-${DD}_${HH}00utc_5km.nc"
IN3=${INDIR3}/${INFLE3}
if [ ! -f $IN3 ];then echo EEEEE NO SUCH FILE,${IN3};exit 1;fi

TIME=${HH}Z${DD}${MMM}${YYYY}

GS=$(basename $0 .sh).GS

FIGDIR=$(basename $0 .sh)_FIG/${YYYY}; mkd $FIGDIR
FIG=${FIGDIR}/$(basename $0 .sh)_${YYYY}-${MM}-${DD}_${HH}.pdf 

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE
xmax = 2; ymax = 1
ytop=9.5; xwid = 8/xmax; ywid = 3.5/ymax
xmargin=0; ymargin=0.4

'open $CTL1'; say; 'q ctlinfo 1'; say sublin(result,1); say
'open $CTL2'; say; 'q ctlinfo 2'; say sublin(result,1); say
'sdfopen $IN3';    'q ctlinfo 3'; say sublin(result,1); say

'cc';'set grid off';'set grads off'

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 0.2 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

#'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';
'set mpdset hires' ;#worldmap'

'set gxout shade2'
'color ${LEVS1} ${KIND}' ;# SET COLOR BAR

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims'; say 'MMMMM 'result

'd CAPE.1'
'set xlab off';'set ylab off'

'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'

'set dfile 3'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims'; say 'MMMMM 'result
'set gxout contour'
'set ccolor 0';'set cthick 8'
'set clevs 50 100 200';'set clab off'
'd P60.3';# 3hour precipitation
'set ccolor 9';'set cthick 3'
'set clevs 50 100 200';'set clab on'
'd P60.3';# 3hour precipitation

# MAX AND ITS LOCATION
'set lon $LONW'; 'set lat $LATS'
'd amax(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3MAX=subwrd(result,4);say 'MMMMM P3MAX='P3MAX

'd amaxlocx(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3X=subwrd(result,4)
'set x 'P3X; 'q dims'; LIN=sublin(result,2);P3LON=subwrd(LIN,6)
say 'MMMMM P3LON='P3LON

'd amaxlocy(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3Y=subwrd(result,4)
'set y 'P3Y; 'q dims'; LIN=sublin(result,3);P3LAT=subwrd(LIN,6)
say 'MMMMM P3LAT='P3LAT

'markplot 'P3LON' 'P3LAT' -c 1 -m 3 -s 0.04'
'q w2xy 'P3LON' 'P3LAT
xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 l 4 0'
'draw string 'xx+0.02' 'yy+0.05' 'math_int(P3MAX)

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

# LEGEND COLOR BAR
x1=xl-0.25; x2=xr-0.45; y1=yb-0.7; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

# TITLE
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.18
'draw string 'x' 'y' ECAPE ${TIME}'
x=(xl+xr)/2 ;y=y+0.25
'draw string 'x' 'y' ${TEXT}'



nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 0.0 +(xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

#'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';
'set mpdset worldmap'

'set gxout shade2'
'color ${LEVS2} ${KIND}' ;# SET COLOR BAR

'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims'; say 'MMMMM 'result
'set xlab on';'set ylab on'
'd CAPE.2'
'set xlab off';'set ylab off'

'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'

'set dfile 3'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims'; say 'MMMMM 'result
'set gxout contour'
'set ccolor 0';'set cthick 8'
'set clevs 50 100 200';'set clab off'
'd P60.3';# 3hour precipitation
'set ccolor 9';'set cthick 3'
'set clevs 50 100 200';'set clab on'
'd P60.3';# 3hour precipitation

# MAX AND ITS LOCATION
'set lon $LONW'; 'set lat $LATS'
'd amax(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3MAX=subwrd(result,4);say 'MMMMM P3MAX='P3MAX

'd amaxlocx(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3X=subwrd(result,4)
'set x 'P3X; 'q dims'; LIN=sublin(result,2);P3LON=subwrd(LIN,6)
say 'MMMMM P3LON='P3LON

'd amaxlocy(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3Y=subwrd(result,4)
'set y 'P3Y; 'q dims'; LIN=sublin(result,3);P3LAT=subwrd(LIN,6)
say 'MMMMM P3LAT='P3LAT

'markplot 'P3LON' 'P3LAT' -c 1 -m 3 -s 0.04'
'q w2xy 'P3LON' 'P3LAT
xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 l 4 0'
'draw string 'xx+0.02' 'yy+0.05' 'math_int(P3MAX)

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

# LEGEND COLOR BAR
x1=xl-0.25; x2=xr-0.45; y1=yb-0.7; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

# LEGEND COLOR BAR
x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

# TITLE
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.18
'draw string 'x' 'y' CAPE ${TIME}'
x=(xl+xr)/2 ;y=y+0.25
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.2; 
yy = yt+0.9; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"

if [ -f $FIG ]; then echo "FIG: $FIG"; fi

echo "DONE $0."
echo


    h=$(expr $h + $dh)
  done #h
  
  i=$(expr $i + 1)
done #i
rm -vf $GS $CTL1 $CTL2


exit 0




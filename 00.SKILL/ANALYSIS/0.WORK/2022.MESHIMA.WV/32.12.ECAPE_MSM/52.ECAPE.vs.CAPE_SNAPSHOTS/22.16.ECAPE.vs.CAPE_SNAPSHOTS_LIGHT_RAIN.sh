#!/bin/bash

EXE=22.12.ECAPE.vs.CAPE_SNAPSHOTS_2.sh
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE;exit 1;fi

INDIR=22.00.AVE_R1D.ge.1mm_QFLUX.ge.ave
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INFLE=N-KYUSHU_R3HMAX.le.ave.txt
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=$(basename $0 .sh)_OUT
mkdir -vp $ODIR

N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y=${ary[0]}
  M=${ary[1]}
  D=${ary[2]}
  H=${ary[3]}

  MM=$(printf %02d $M)
  DD=$(printf %02d $D)
  H=${H#0}
  HH=$(printf %02d $H)

  YMDH=${Y}${MM}${DD}${HH}

  $EXE $YMDH $ODIR
 
  N=$(expr $N + 1 )
done < $IN

cp -vp $0 $EXE $ODIR

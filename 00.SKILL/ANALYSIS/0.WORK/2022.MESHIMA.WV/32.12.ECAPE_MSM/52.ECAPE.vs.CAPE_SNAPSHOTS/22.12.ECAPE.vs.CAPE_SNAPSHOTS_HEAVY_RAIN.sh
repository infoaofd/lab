#!/bin/bash

EXE=
if [ ! -d $EXE ];then echo NO SUCH DIR,$EXE;exit 1;fi

INDIR=13.12.AVE_R1D.ge.1mm_QFLUX.ge.ave
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INFLE=N-KYUSHU_R3HMAX.ge.ave.txt
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
ODIR=$(basename $0 .sh)_OUT

N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y=${ary[0]}
  M=${ary[1]}
  D=${ary[2]}
  H=${ary[3]}

  MM=$(printf %02d $M)
  DD=$(printf %02d $D)
  HH=$(printf %02d $H)

  YMDH=${Y}${MM}${DD}${HH}

  $EXE $YMDH $YMDH $ODIR 
 
  N=$(expr $N + 1 )
done < $TMPFLE


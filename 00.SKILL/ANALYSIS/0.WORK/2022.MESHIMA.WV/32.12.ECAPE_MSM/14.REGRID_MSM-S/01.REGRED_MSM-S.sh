
LONW=128;LATS=31
NX=37;NY=41
DX=0.125; DY=0.1

cat > MYGRID.TXT << EOF
gridtype = lonlat
xsize    = $NX
ysize    = $NY
xunits = 'degree'
yunits = 'degree'
xfirst   = ${LONW}
xinc     = ${DX} 
yfirst   = ${LATS}
yinc     = ${DY}
EOF

yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20240614}; yyyymmdd2=${yyyymmdd2:-20240614}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}


INDIR=/work02/DATA/MSM.NC/MSM-S/$yyyy
INFLE=$mm${dd}.nc
IN=$INDIR/$INFLE

ODIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/REGRID_SFC/$yyyy
mkdir -p $ODIR
OFLE=MSM-S_CUT_REGRID_${yyyy}${mm}${dd}.nc
OUT=$ODIR/$OFLE

if [ ! -f $IN ];then echo EEEEEE NO SUCH FILE,$IN; exit 1;fi

rm -vf $OUT
# 気圧面データと格子間隔をそろえる
cdo remapcon,MYGRID.TXT $IN $OUT

echo MMMMM OUT: $OUT

  i=$(expr $i + 1)
done #i

exit 0




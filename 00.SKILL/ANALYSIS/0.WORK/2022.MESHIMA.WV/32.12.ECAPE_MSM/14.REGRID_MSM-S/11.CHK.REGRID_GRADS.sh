#!/bin/bash

# Sat, 22 Jun 2024 17:19:19 +0900
# /work09/ma/00.WORK/2024.06.OKINAWA_RAIN/22.12.MSM/32.12.CAPE/14.REGRID_MSM-S

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-2024061400}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

#CTL=$(basename $0 .sh).CTL
#if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

INDIR=/work02/DATA/MSM.NC/MSM-S/CUT_OKINAWA/${YYYY}
INFLE=MSM-S_${YYYY}${MM}${DD}_CUT.nc
IN1=${INDIR}/${INFLE}
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi

INDIR=/work02/DATA/MSM.NC/MSM-S/CUT_OKINAWA_REGRID/${YYYY}
INFLE=MSM-S_CUT_REGRID_${YYYY}${MM}${DD}.nc
IN2=${INDIR}/${INFLE}
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${YYYY}${MM}${DD}.PDF

LONW=126 ;LONE=130 ; LATS=25 ;LATN=28
# LEV=

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
# FS=2
# UNIT=

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN1}';'sdfopen ${IN2}'

xmax = 1; ymax = 2

ytop=9.5

xwid = 5.0/xmax; ywid = 7.0/ymax
xmargin=1; ymargin=0.8

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'cc'

nmap = 1; ymap = 1
while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
# 'color ${LEVS} -kind ${KIND} -gxout shaded'

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
# 'set time ${TIME}'
'set ccolor 1';'set cthick 1'
'set cint 0.5';'set clab forced'
'set mpdraw on'
'set mpdset hires';'set map 1 1 3'
'set grid off'
'd psea.'nmap'/100'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' FILE 'nmap

xmap=xmap+1
nmap=nmap+1
endwhile

ymap=ymap+1
endwhile

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'



# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=ytop+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

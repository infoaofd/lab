#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/15.12.ECAPE_TEST
# /work09/am/00.WORK/2022.ECS2022/26.LFM.3/22.16.ECAPE500m.RH.ECMWF_2
# /work09/ma/00.WORK/2024.06.OKINAWA_RAIN/22.12.MSM/32.12.CAPE/32.ECAPE_RH_500m

P_INIT=500m
# ELIST="00"  #ENTRAINMENT_RATE %/km

EXE=ECAPE${P_INIT}_NC4_RH.sh
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE; exit 1;fi

YS=2023;YE=2020
Y=$YS

while [ $Y -ge $YE ];do

yyyymmdd1=${Y}0601; yyyymmdd2=${Y}0930

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

h=0
while [ $h -le 23 ];do

hh=$(printf %02d $h)

$EXE $yyyy $mm $dd $hh
# $EXE $FH $YYYY $MM $DD $HH $ERATE

h=$(expr $h + 3)
done #h

  i=$(expr $i + 1)
done #i

Y=$(expr $Y - 1)
done #Y


#!/bin/bash
P_INIT=500m;TEXT="ECAPE $P_INIT RH"

INDIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/OUT_ECAPE500m_NC4_RH/KYUSHU
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2021081200}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

INFLE=ECAPE_RH_${YYYY}-${MM}-${DD}_${HH}_${P_INIT}_0.0.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

CTL=$(basename $0 .sh).CTL
cat<<EOF>$CTL
dset /work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/OUT_ECAPE500m_NC4_RH/KYUSHU/ECAPE_RH_%y4-%m2-%d2_%h2_500m_0.0.nc
title ECAPE
undef 1e+20
dtype netcdf
options template yrev
xdef 37 linear 128 0.125
ydef 41 levels 31  31.1  31.2  31.3  31.4  31.5  31.6  31.7  31.8  31.9  32  32.1  
32.2  32.3  32.4  32.5  32.6  32.7  32.8  32.9  33  33.1  33.2  33.3  
33.4  33.5  33.6  33.7  33.8  33.9  34  34.1  34.2  34.3  34.4  34.5  
34.6  34.7  34.8  34.9  35
zdef 16 levels 1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100
tdef 100000 linear 00Z01JUN2006 180mn
vars 9
DLFC=>dlfc  0  t,y,x  DLFC
DLCL=>dlcl  0  t,y,x  DLCL
CAPE=>cape  0  t,y,x  CAPE
CIN=>cin  0  t,y,x  CIN
temp=>temp  16  t,z,y,x  temp
qv=>qv  16  t,z,y,x  qv
z=>z  16  t,z,y,x  z
sp=>sp  0  t,y,x  sp
HGT=>hgt  0  t,y,x  HGT
endvars
EOF

TIME=${HH}Z${DD}${MMM}${YYYY}

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_${YYYY}-${MM}-${DD}_${HH}.pdf ;#eps

LEVS="100 800 50"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=4
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 1; ymax = 1

ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
#'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

#'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims';say result
'set mpdset worldmap'
'd CAPE.1'
'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.18
'draw string 'x' 'y' ECAPE ${TIME}'
x=(xl+xr)/2 ;y=y+0.25
'draw string 'x' 'y' ${TEXT}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS $CTL

if [ -f $FIG ]; then echo "FIG: $FIG"; fi


echo "DONE $0."
echo


LONW=128;LONE=132.5;LATS=31;LATN=35
#LONW=126;LONE=130;LATS=25;LATN=28
LTYP="P S"
# LTYP="P"

yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20240614}; yyyymmdd2=${yyyymmdd2:-20240614}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

for TYP in $LTYP; do

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}


INDIR=/work02/DATA/MSM.NC/MSM-${TYP}/$yyyy
INFLE=$mm${dd}.nc
IN=$INDIR/$INFLE

ODIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/MSM-${TYP}//$yyyy
mkdir -p $ODIR
OFLE=MSM-${TYP}_${yyyy}${mm}${dd}_CUT.nc
OUT=$ODIR/$OFLE

if [ ! -f $IN ];then echo EEEEEE NO SUCH FILE,$IN; exit 1;fi

rm -vf $OUT
# ある緯度経度領域内のみ切り出す
cdo sellonlatbox,$LONW,$LONE,$LATS,$LATN $IN $OUT

echo MMMMM OUT: $OUT

  i=$(expr $i + 1)
done #i

done #TYP
exit 0




# /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/32.12.ECAPE_MSM/16.SPLIT_HOUR
# 01.SPLIT_HOUR_P.sh
LONW=126;LONE=130;LATS=25;LATN=28

yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20240614}; yyyymmdd2=${yyyymmdd2:-20240614}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}


INDIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/MSM-P/$yyyy
if [ ! -d $INDIR ];then echo EEEEEE NO SUCH DIR,$INDIR; exit 1;fi
INFLE=MSM-P_${yyyy}${mm}${dd}_CUT.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo EEEEEE NO SUCH FILE,$IN; exit 1;fi

ODIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/CUT_N-KYUSHU_SPLIT/MSM-P/$yyyy
mkdir -p $ODIR
OFLE=MSM-P_SPLIT_${yyyy}${mm}${dd}
OUT=$ODIR/$OFLE

rm -vf $OUT
# 時間でわける
cdo splithour $IN $OUT

H=0
while [ $H -le 23 ];do
if [ $(( $H % 3 )) -ne 0 ]; then
HH=$(printf %02d $H)
rm -f $OUT${HH}.nc
fi
H=$(expr $H + 1)
done #H

OLIST=$(ls $OUT*)
for O in $OLIST;do
echo MMMMM OUT: $O
done

  i=$(expr $i + 1)
done #i

exit 0




#!/bin/bash
P_INIT=500m;TEXT="CAPE $P_INIT"

INDIR=/work02/DATA/MSM.NC/OUT_0.ECAPE_NO_ENTRAINMENT_500m
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
echo INDIR: $INDIR
YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2024061400}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

INFLE=ECAPE_${YYYY}-${MM}-${DD}_${HH}_${P_INIT}_NO_ENTRAINMENT.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

CTL=$(basename $0 .sh).CTL
cat<<EOF>$CTL
dset $INDIR/ECAPE_%y4-%m2-%d2_%h2_${P_INIT}_NO_ENTRAINMENT.nc
title ECAPE
undef 1e+20
dtype netcdf
options template yrev
xdef 33 linear 126 0.125
ydef 31 levels 25 25.1 25.2 25.3 25.4 25.5 25.6 25.7
 25.8 25.9 26 26.1 26.2 26.3 26.4 26.5 26.6 26.7
 26.8 26.9 27 27.1 27.2 27.3 27.4 27.5 27.6 27.7
 27.8 27.9 28
zdef 16 levels 1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100
tdef 100000 linear 00Z01JUN2006 180mn
vars 9
DLFC=>dlfc  0  t,y,x  DLFC
DLCL=>dlcl  0  t,y,x  DLCL
CAPE=>cape  0  t,y,x  CAPE
CIN=>cin  0  t,y,x  CIN
temp=>temp  16  t,z,y,x  temp
qv=>qv  16  t,z,y,x  qv
z=>z  16  t,z,y,x  z
sp=>sp  0  t,y,x  sp
HGT=>hgt  0  t,y,x  HGT
endvars
EOF

TIME=${HH}Z${DD}${MMM}${YYYY}

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_${YYYY}-${MM}-${DD}_${HH}.pdf ;#eps

LEVS="100 1800 100"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=3
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'
'q dims'say result
'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 2

ytop=9; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
#'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

#'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'd CAPE.1'
'set xlab off';'set ylab off'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
x=(xl+xr)/2 ;y=yt+0.18
'draw string 'x' 'y' ECAPE ${TIME}'
x=(xl+xr)/2 ;y=y+0.25
'draw string 'x' 'y' ${TEXT}'

'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.7; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS $CTL

if [ -f $FIG ]; then echo "FIG: $FIG"; fi


echo "DONE $0."
echo

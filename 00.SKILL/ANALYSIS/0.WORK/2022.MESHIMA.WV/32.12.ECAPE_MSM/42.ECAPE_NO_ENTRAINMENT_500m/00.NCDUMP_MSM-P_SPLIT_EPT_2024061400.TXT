netcdf MSM-P_SPLIT_EPT_2024061400 {
dimensions:
	time = UNLIMITED ; // (1 currently)
	p = 16 ;
	lat = 31 ;
	lon = 33 ;
variables:
	float time(time) ;
		time:standard_name = "time" ;
		time:long_name = "time" ;
		time:units = "hours since 2024-06-14 00:00:00+00:00" ;
		time:calendar = "standard" ;
		time:axis = "T" ;
	float p(p) ;
		p:standard_name = "air_pressure" ;
		p:long_name = "Isobaric surface" ;
		p:units = "hPa" ;
		p:positive = "down" ;
		p:axis = "Z" ;
	float lat(lat) ;
		lat:grid_type = "Latitude/Longitude" ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:axis = "Y" ;
	float lon(lon) ;
		lon:grid_type = "Latitude/Longitude" ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:axis = "X" ;
	float temp(time, p, lat, lon) ;
		temp:_FillValue = 1.e+20f ;
		temp:standard_name = "air_temperature" ;
		temp:long_name = "temperature" ;
		temp:units = "K" ;
	float rh(time, p, lat, lon) ;
		rh:_FillValue = 1.e+20f ;
		rh:standard_name = "relative_humidity" ;
		rh:long_name = "relative humidity" ;
		rh:units = "%" ;
	float qv(time, p, lat, lon) ;
		qv:long_name = "Water vapor mixing ratio" ;
		qv:standard_name = "qvapor" ;
		qv:units = "kg/kg" ;
		qv:_FillValue = 9.96921e+36f ;
	float u(time, p, lat, lon) ;
		u:_FillValue = 1.e+20f ;
		u:standard_name = "eastward_wind" ;
		u:long_name = "eastward component of wind" ;
		u:units = "m/s" ;
	float v(time, p, lat, lon) ;
		v:_FillValue = 1.e+20f ;
		v:standard_name = "northward_wind" ;
		v:long_name = "northward component of wind" ;
		v:units = "m/s" ;
	double w(time, p, lat, lon) ;
		w:standard_name = "lagrangian_tendency_of_air_pressure" ;
		w:long_name = "vertical velocity in p" ;
		w:units = "Pa/s" ;
	double z(time, p, lat, lon) ;
		z:standard_name = "geopotential_height" ;
		z:long_name = "geopotential height" ;
		z:units = "m" ;
	float ept(time, p, lat, lon) ;
		ept:units = "K" ;
		ept:long_name = "Equivalent potential temperature" ;
		ept:standard_name = "EPT" ;
		ept:_FillValue = 1.e+20f ;

// global attributes:
		:script = "01.COMP_EPT_QV_P.ncl" ;
		:directory = "/work09/ma/00.WORK/2024.06.OKINAWA_RAIN/22.12.MSM/32.12.CAPE/18.COMP_EPT_QV" ;
		:creation_date = "Sun, 23 Jun 2024 10:11:18 +0900" ;
data:

 time = 0 ;

 p = 1000, 975, 950, 925, 900, 850, 800, 700, 600, 500, 400, 300, 250, 200, 
    150, 100 ;

 lat = 28, 27.9, 27.8, 27.7, 27.6, 27.5, 27.4, 27.3, 27.2, 27.1, 27, 26.9, 
    26.8, 26.7, 26.6, 26.5, 26.4, 26.3, 26.2, 26.1, 26, 25.9, 25.8, 25.7, 
    25.6, 25.5, 25.4, 25.3, 25.2, 25.1, 25 ;

 lon = 126, 126.125, 126.25, 126.375, 126.5, 126.625, 126.75, 126.875, 127, 
    127.125, 127.25, 127.375, 127.5, 127.625, 127.75, 127.875, 128, 128.125, 
    128.25, 128.375, 128.5, 128.625, 128.75, 128.875, 129, 129.125, 129.25, 
    129.375, 129.5, 129.625, 129.75, 129.875, 130 ;
}

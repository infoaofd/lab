#!/bin/bash
P_INIT=950; P_RH=850; RH_THD=95
EPT_CNT=349

YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2022061900}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=127 ;LONE=130 ; LATS=29 ;LATN=31.5
TIME=${HH}Z${DD}${MMM}${YYYY}

CTL1=LFM_ECAPE_E00.CTL; CTL2=LFM_ECAPE_E05.CTL
CTL3=LFM_ECAPE_E10.CTL; CTL4=LFM_ECAPE_E20.CTL
if [ ! -f $CTL1 ];then echo ERROR in $CTL1: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL2 ];then echo ERROR in $CTL2: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL3 ];then echo ERROR in $CTL3: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL4 ];then echo ERROR in $CTL4: NO SUCH FILE,;exit1;fi

CTL5=LFM00.CTL
if [ ! -f $CTL5 ];then echo ERROR in $CTL5: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_${YYYY}-${MM}-${DD}_${HH}_EPT${P_INIT}-${EPT_CNT}K_RH${P_RH}hPa-${RH_THD}pcnt.pdf ;#eps

LEVS="100 700 100"
LEVS2="50 350 50"
KIND='-kind white->wheat->gold->orange->red->maroon'
FS=2
UNIT=J/kg
TEXT1="E=0%/km";  TEXT2="E=5%/km"
TEXT3="E=10%/km"; TEXT4="E=20%/km"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}'; 'open ${CTL3}'; 'open ${CTL4}'
'open ${CTL5}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 2

ytop=9.5; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'; 'set time ${TIME}'
'color ${LEVS} ${KIND}'
'd CAPE.1'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set xlab off';'set ylab off'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_INIT}'
'set time ${TIME}'
'q dims'; say '$CTL2'; say result

'set gxout contour';'set cthick 5';'set ccolor 1'
'set clopts 1 2 0.08'
'set clevs 349'
'd ept.5'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_RH}'
'set time ${TIME}'
'set gxout shaded'
'set clevs ${RH_THD}'

'color 95 105 10 -kind (0,0,0,0)->(0,0,150,30)->(0,0,100,30)->(0,0,50,30)'
'd rh.5'

'set gxout contour';'set cthick 2';'set ccolor 4'
'set clopts 4 2 0.08'
'set clevs 95'
'd rh.5'

'02.04.OBS.BOX.DASH.GS'

x=(xl+xr)/2 ;# TEXT
y=yt+0.15
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT1}'



nmap = 2
ymap = 2 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set mpdset hires';'set grads off';'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set dfile 2'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'
'set time ${TIME}'

'set xlab on';'set ylab on'

'd CAPE.2'
'set xlab off';'set ylab off'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_INIT}'
'set time ${TIME}'
'q dims'; say '$CTL2'; say result

'set gxout contour';'set cthick 5';'set ccolor 1'
'set clopts 1 2 0.08'
'set clevs 349'
'd ept.5'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_RH}'
'set time ${TIME}'
'set gxout shaded'
'set clevs ${RH_THD}'

'color 95 105 10 -kind (0,0,0,0)->(0,0,150,30)->(0,0,100,30)->(0,0,50,30)'
'd rh.5'

'set gxout contour';'set cthick 2';'set ccolor 4'
'set clopts 4 2 0.08'
'set clevs 95'
'd rh.5'

'02.04.OBS.BOX.DASH.GS'


x=(xl+xr)/2 ;# TEXT
y=yt+0.15
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT2}'



nmap = 3
ymap = 1 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set mpdset hires';'set grads off';'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set dfile 3'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'
'set time ${TIME}'

'set xlab on';'set ylab on'

'd CAPE.3'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set xlab off';'set ylab off'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_INIT}'
'set time ${TIME}'
'q dims'; say '$CTL2'; say result

'set gxout contour';'set cthick 5';'set ccolor 1'
'set clopts 1 2 0.08'
'set clevs 349'
'd ept.5'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_RH}'
'set time ${TIME}'
'set gxout shaded'
'set clevs ${RH_THD}'

'color 95 105 10 -kind (0,0,0,0)->(0,0,150,30)->(0,0,100,30)->(0,0,50,30)'
'd rh.5'

'set gxout contour';'set cthick 2';'set ccolor 4'
'set clopts 4 2 0.08'
'set clevs 95'
'd rh.5'

'02.04.OBS.BOX.DASH.GS'

x=(xl+xr)/2 ;# TEXT
y=yt+0.15
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT3}'



nmap = 4
ymap = 2 ;#while (ymap <= ymax)
xmap = 2 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set mpdset hires';'set grads off';'set grid off'
'set xlint 1';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS2} ${KIND}' ;# SET COLOR BAR

'set dfile 4'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1'
'set time ${TIME}'

'set xlab on';'set ylab on'

'd CAPE.4'

'q gxinfo' ;#GET COORDINATES OF 4 CORNERS
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set xlab off';'set ylab off'


'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_INIT}'
'set time ${TIME}'
'q dims'; say '$CTL2'; say result

'set gxout contour';'set cthick 5';'set ccolor 1'
'set clopts 1 2 0.08'
'set clevs 349'
'd ept.5'

'set dfile 5'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set lev ${P_RH}'
'set time ${TIME}'
'set gxout shaded'
'set clevs ${RH_THD}'

'color 95 105 10 -kind (0,0,0,0)->(0,0,150,30)->(0,0,100,30)->(0,0,50,30)'
'd rh.5'

'set gxout contour';'set cthick 2';'set ccolor 4'
'set clopts 4 2 0.08'
'set clevs 95'
'd rh.5'

'02.04.OBS.BOX.DASH.GS'

x=(xl+xr)/2 ;# TEXT
y=yt+0.15
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT4}'


'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL1} ${CTL2}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS

<<COMMENT
echo; echo MMMMM BACK-UP SCRIPTS
ODIR= ; mkdir -vp $ODIR
TMP=TMP_$(basename $0)
echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
rm -f $TMP
echo MMMMM
COMMENT

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


echo "DONE $0."
echo

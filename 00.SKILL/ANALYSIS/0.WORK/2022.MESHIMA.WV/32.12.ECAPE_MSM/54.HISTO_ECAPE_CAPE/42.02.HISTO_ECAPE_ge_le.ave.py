# coding: utf-8
import netCDF4  
import numpy as np
import matplotlib.pyplot as plt

PERIOD="2020-2023"
MMM="JJA"
VAR="ECAPE RH 500m"
ALONW="129.2" ;ALONE="132.1" ; ALATS="32.7" ;ALATN="34.0"
INDIR="/work02/DATA/MSM.NC/ECAPE_SELLONLATBOX_129.2-132.1_32.7-34.0/0.MERGETIME/"
INFLE1="ECAPE_ge.ave_129.2-132.1_32.7-34.0_MERGETIME.nc"
IN1=INDIR+INFLE1

INFLE2="ECAPE_le.ave_129.2-132.1_32.7-34.0_MERGETIME.nc"
IN2=INDIR+INFLE2

DOMAIN="N-KYUSHU ("+ALATS+"-"+ALATN+"N "+ALONW+"-"+ALONE+"E)"

nc1 = netCDF4.Dataset(IN1, "r")
a1=nc1.variables["CAPE"][:]
b1 = a1.ravel() #多次元のリストを1次元のリストにして返します
nc2 = netCDF4.Dataset(IN2, "r")
a2=nc2.variables["CAPE"][:]
b2 = a2.ravel() #多次元のリストを1次元のリストにして返します

print("")
print("MMMMM INPUT:  "+IN1)
print("MMMMM B MAX = "+str(max(b1)))
print("MMMMM INPUT:  "+IN2)
print("MMMMM B MAX = "+str(max(b2)))

bmin=100; bmax=1500
bin_width = 50
num_bin_real=(bmax-bmin)/bin_width
print("MMMMM num_bin_real= "+str(num_bin_real))
num_bins = int(num_bin_real)
print("MMMMM bmin="+str(bmin)+" bmax="+str(bmax))
print("MMMMM bin_width="+str(bin_width)+" num_bins="+str(num_bins))

weights = np.ones_like(b1) / len(b1)*100
plt.hist(b1, bins=num_bins, weights=weights,range=(bmin,bmax), color='red', alpha=0.8,edgecolor="gray")

weights = np.ones_like(b2) / len(b2)*100
plt.hist(b2, bins=num_bins, weights=weights,range=(bmin,bmax), color='blue',alpha=0.8,edgecolor="gray")
plt.xlim(bmin, bmax) # x軸の範囲
#plt.ylim(0, 15)    # y軸の範囲
plt.yscale('log')
plt.ylim(0.01, 20)    # y軸の範囲

#plt.hist(b, alpha=0.5, bins=6, range=(20,80), color='b')
#plt.hist(b, alpha=0.5, bins=8, range=(40,80), color='b', log=True) #, label=MMM)

#r1h_1_thold=r1h_1[r1h_1>THOLD]
#plt.hist(r1h_1_thold, alpha=0.5, bins=15, range=(5,80), 
#color='b', log=True, label="MSM")

plt.xlabel("ECAPE [J/kg]")
plt.ylabel("Relative Frequency [%]")
plt.title(VAR+" "+DOMAIN+" "+MMM+" "+PERIOD)

#plt.legend(loc="upper right", fontsize=13)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)
print("")



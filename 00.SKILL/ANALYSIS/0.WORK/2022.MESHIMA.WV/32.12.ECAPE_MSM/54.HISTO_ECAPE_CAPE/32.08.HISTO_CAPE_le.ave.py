# coding: utf-8
import netCDF4  
import numpy as np
import matplotlib.pyplot as plt

PERIOD="2020-2023"
MMM="JJA"
VAR="CAPE 500m"
INDIR="/work02/DATA/MSM.NC/ECAPE_SELLONLATBOX_129.2-132.1_32.7-34.0/0.MERGETIME/"
INFLE="CAPE_le.ave_129.2-132.1_32.7-34.0_MERGETIME.nc"
IN=INDIR+INFLE

ALONW="129.2" ;ALONE="132.1" ; ALATS="32.7" ;ALATN="34.0"
DOMAIN="N-KYUSHU ("+ALATS+"-"+ALATN+"N "+ALONW+"-"+ALONE+"E)"

nc1 = netCDF4.Dataset(IN, "r")
a=nc1.variables["CAPE"][:]
b = a.ravel() #�������̃��X�g��1�����̃��X�g�ɂ��ĕԂ��܂�
print("")
print("MMMMM INPUT:  "+IN)
print("MMMMM B MAX = "+str(max(b)))

bmin=100; bmax=3600
bin_width = 125
num_bin_real=(bmax-bmin)/bin_width
print("MMMMM num_bin_real= "+str(num_bin_real))
num_bins = int(num_bin_real)
print("MMMMM bmin="+str(bmin)+" bmax="+str(bmax))
print("MMMMM bin_width="+str(bin_width)+" num_bins="+str(num_bins))

weights = np.ones_like(b) / len(b)*100
plt.hist(b, bins=num_bins, weights=weights,range=(bmin,bmax), color='b')
plt.xlim(bmin,bmax) # x���͈̔�
#plt.ylim(0, 15)    # y���͈̔�
plt.yscale('log')
plt.ylim(0.01, 20)    # y���͈̔�


#plt.hist(b, alpha=0.5, bins=6, range=(20,80), color='b')
#plt.hist(b, alpha=0.5, bins=8, range=(40,80), color='b', log=True) #, label=MMM)

#r1h_1_thold=r1h_1[r1h_1>THOLD]
#plt.hist(r1h_1_thold, alpha=0.5, bins=15, range=(5,80), 
#color='b', log=True, label="MSM")

plt.xlabel("CAPE [J/kg]")
plt.ylabel("Relative Frequency [%]")
plt.title(VAR+" "+DOMAIN+" "+MMM+" "+PERIOD)

#plt.legend(loc="upper right", fontsize=13)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)
print("")


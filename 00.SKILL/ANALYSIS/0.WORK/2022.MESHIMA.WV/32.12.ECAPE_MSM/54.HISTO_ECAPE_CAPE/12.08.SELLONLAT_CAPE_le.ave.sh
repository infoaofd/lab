
ALATS=32.7; ALATN=34.0; ALONW=129.2; ALONE=132.1 # NORTHERN KYUSHU

GROUP=le.ave
INDIR=22.00.AVE_R1D.ge.1mm_QFLUX.ge.ave

if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR,$INDIR;exit 1;fi
INFLE=N-KYUSHU_R3HMAX.${GROUP}.txt
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo EEEEE NO SUCH FILE,$IN;exit 1;fi

INDIR2=/work02/DATA/MSM.NC/OUT_0.ECAPE_NO_ENTRAINMENT_500m/KYUSHU/
if [ ! -d $INDIR2 ];then echo EEEEE NO SUCH DIR,$INDIR2;exit 1;fi

ODIR=/work02/DATA/MSM.NC/ECAPE_SELLONLATBOX_129.2-132.1_32.7-34.0/CAPE_${GROUP}
mkd $ODIR

N=0
while read BUF ; do
  ary=(`echo $BUF`)   # �z��Ɋi�[
  Y=${ary[0]}
  M=${ary[1]}
  D=${ary[2]}
  H=${ary[3]}

  MM=$(printf %02d $M)
  DD=$(printf %02d $D)
  H=${H#0}
  HH=$(printf %02d $H)

# ECAPE_RH_2021-09-30_21_500m_0.0.nc
  INFLE2=ECAPE_${Y}-${MM}-${DD}_${HH}_500m_NO_ENTRAINMENT.nc
  IN2=$INDIR2/$INFLE2
  if [ ! -f $IN2 ];then echo EEEEE NO SUCH FILE, $IN2;fi
  TMP=CAPE_${ALONW}-${ALONE}_${ALATS}-${ALATN}_${Y}-${MM}-${DD}_${HH}.nc
  OUT=$ODIR/CAPE_${ALONW}-${ALONE}_${ALATS}-${ALATN}_${Y}-${MM}-${DD}_${HH}.nc

  echo INPUT: $IN2
  cdo sellonlatbox,${ALONW},${ALONE},${ALATS},${ALATN} $IN2 $TMP
  cdo selvar,CAPE $TMP $OUT
  if [ ! -f $OUT ];then EEEEE NO SUCH FILE, $OUT;exit 1;fi
  rm -vf $TMP
  echo
 
  N=$(expr $N + 1 )
done < $IN

echo ; echo MMMMM ODIR: $ODIR; echo


NCL=$(basename $0 .sh).ncl
if [ ! -f $NCL ];then echo EEEEE NO SUCH FILE,$NCL;exit 1;fi

yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20240614}; yyyymmdd2=${yyyymmdd2:-20240614}

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

INDIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/CUT_N-KYUSHU_SPLIT/MSM-P/$yyyy

if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR,$INDIR;exit 1;fi
ODIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU/EPT_QV_N-KYUSHU_SPLIT/MSM-P/$yyyy
mkdir -p $ODIR

H=0
while [ $H -le 23 ];do

HH=$(printf %02d $H)

INFLE=MSM-P_SPLIT_${yyyy}${mm}${dd}${HH}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo EEEEE NO SUCH FILE,$IN; exit 1;fi

OFLE=MSM-P_SPLIT_EPT_${yyyy}${mm}${dd}${HH}.nc
OUT=$ODIR/$OFLE

rm -vf $OUT
echo IN: $IN
runncl.sh ${NCL} ${yyyy} ${mm} ${dd} $IN $OUT
if [ -f $OUT ];then echo OUT: $OUT;fi
echo

H=$(expr $H + 3)
done #H

  i=$(expr $i + 1)
done #i

exit 0


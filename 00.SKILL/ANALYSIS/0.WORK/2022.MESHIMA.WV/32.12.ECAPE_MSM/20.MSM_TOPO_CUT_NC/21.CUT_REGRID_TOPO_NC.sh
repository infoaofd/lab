
INDIR=/work02/DATA/MSM.NC/TOPO
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=TOPO.MSM_5K.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

ODIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU
mkd $ODIR
OFLE=$(basename $IN .nc)_CUT_REGRID_KYUSHU.nc
OUT=$ODIR/$OFLE

LONW=128;LATS=31
NX=37;NY=41
DX=0.125; DY=0.1

cat > MYGRID.TXT << EOF
gridtype = lonlat
xsize    = $NX
ysize    = $NY
xunits = 'degree'
yunits = 'degree'
xfirst   = ${LONW}
xinc     = ${DX} 
yfirst   = ${LATS}
yinc     = ${DY}
EOF

rm -vf $OUT
cdo remapcon,MYGRID.TXT $IN $OUT
echo
echo IN: $IN
if [ -f $OUT ];then echo OUT: $OUT;fi



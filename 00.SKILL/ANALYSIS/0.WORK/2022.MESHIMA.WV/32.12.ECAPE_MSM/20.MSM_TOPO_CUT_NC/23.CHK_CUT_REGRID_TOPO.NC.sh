#!/bin/bash

# Sun, 23 Jun 2024 11:23:54 +0900
# /work09/ma/00.WORK/2024.06.OKINAWA_RAIN/22.12.MSM/32.12.CAPE/20.MSM_TOPO_CUT_NC

INDIR=/work02/DATA/MSM.NC/MSM_CUT_N-KYUSHU
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=TOPO.MSM_5K_CUT_REGRID_KYUSHU.nc
IN=${INDIR}/${INFLE}
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

TEXT="MSM TOPO NetCDF CUT REGRID 0.125deg x 0.1deg"

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh).PDF

# LONW= ;LONE= ; LATS= ;LATN=
# LEV=

LEVS=" -levs 1 10 20 30 40 50 100 150 200 250 300 350 400 500"
KIND=" -kind white->lightgreen->limegreen->darkgreen->khaki->darkgoldenrod->saddlebrown->maroon"
FS=1
UNIT=m

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout grfill'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
# 'set time ${TIME}'

'set mpdset hires';'set map 1 1 1'
'set xlint 0.5';'set ylint 0.5'
'set grid off'
'set mpdset worldmap'
'd HGT'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

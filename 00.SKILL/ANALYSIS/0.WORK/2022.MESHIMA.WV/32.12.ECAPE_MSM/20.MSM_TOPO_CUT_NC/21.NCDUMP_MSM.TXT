netcdf MSM-P_SPLIT_2023060100 {
dimensions:
	time = UNLIMITED ; // (1 currently)
	lon = 33 ;
	lat = 46 ;
	p = 16 ;
variables:
	float time(time) ;
		time:standard_name = "time" ;
		time:long_name = "time" ;
		time:units = "hours since 2023-06-01 00:00:00+00:00" ;
		time:calendar = "standard" ;
		time:axis = "T" ;
	float lon(lon) ;
		lon:standard_name = "longitude" ;
		lon:long_name = "longitude" ;
		lon:units = "degrees_east" ;
		lon:axis = "X" ;
	float lat(lat) ;
		lat:standard_name = "latitude" ;
		lat:long_name = "latitude" ;
		lat:units = "degrees_north" ;
		lat:axis = "Y" ;
	float p(p) ;
		p:standard_name = "air_pressure" ;
		p:long_name = "pressure level" ;
		p:units = "hPa" ;
		p:positive = "down" ;
		p:axis = "Z" ;
	double z(time, p, lat, lon) ;
		z:standard_name = "geopotential_height" ;
		z:long_name = "geopotential height" ;
		z:units = "m" ;
	double w(time, p, lat, lon) ;
		w:standard_name = "lagrangian_tendency_of_air_pressure" ;
		w:long_name = "vertical velocity in p" ;
		w:units = "Pa/s" ;
	short u(time, p, lat, lon) ;
		u:standard_name = "eastward_wind" ;
		u:long_name = "eastward component of wind" ;
		u:units = "m/s" ;
		u:add_offset = 0. ;
		u:scale_factor = 0.006116208155 ;
	short v(time, p, lat, lon) ;
		v:standard_name = "northward_wind" ;
		v:long_name = "northward component of wind" ;
		v:units = "m/s" ;
		v:add_offset = 0. ;
		v:scale_factor = 0.006116208155 ;
	short temp(time, p, lat, lon) ;
		temp:standard_name = "air_temperature" ;
		temp:long_name = "temperature" ;
		temp:units = "K" ;
		temp:add_offset = 255.4004974 ;
		temp:scale_factor = 0.002613491379 ;
	short rh(time, p, lat, lon) ;
		rh:standard_name = "relative_humidity" ;
		rh:long_name = "relative humidity" ;
		rh:units = "%" ;
		rh:add_offset = 75. ;
		rh:scale_factor = 0.002293577883 ;

// global attributes:
		:CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
		:Conventions = "CF-1.0" ;
		:history = "Fri Jun 28 11:16:09 2024: cdo splithour /work02/DATA/MSM.NC/MSM-P/CUT_KYUSHU/2023/MSM-P_20230601_CUT.nc /work02/DATA/MSM.NC/MSM-P/CUT_KYUSHU_SPLIT/2023/MSM-P_SPLIT_20230601\n",
			"Fri Jun 28 10:18:43 2024: cdo sellonlatbox,128,132,30,34.5 /work02/DATA/MSM.NC/MSM-P/2023/0601.nc /work02/DATA/MSM.NC/MSM-P/CUT_KYUSHU/2023/MSM-P_20230601_CUT.nc\n",
			"created by create_1daync_msm_p.rb  2023-06-02" ;
		:CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
data:

 time = 0 ;

 lon = 128, 128.125, 128.25, 128.375, 128.5, 128.625, 128.75, 128.875, 129, 
    129.125, 129.25, 129.375, 129.5, 129.625, 129.75, 129.875, 130, 130.125, 
    130.25, 130.375, 130.5, 130.625, 130.75, 130.875, 131, 131.125, 131.25, 
    131.375, 131.5, 131.625, 131.75, 131.875, 132 ;

 lat = 34.5, 34.4, 34.3, 34.2, 34.1, 34, 33.9, 33.8, 33.7, 33.6, 33.5, 33.4, 
    33.3, 33.2, 33.1, 33, 32.9, 32.8, 32.7, 32.6, 32.5, 32.4, 32.3, 32.2, 
    32.1, 32, 31.9, 31.8, 31.7, 31.6, 31.5, 31.4, 31.3, 31.2, 31.1, 31, 30.9, 
    30.8, 30.7, 30.6, 30.5, 30.4, 30.3, 30.2, 30.1, 30 ;

 p = 1000, 975, 950, 925, 900, 850, 800, 700, 600, 500, 400, 300, 250, 200, 
    150, 100 ;
}

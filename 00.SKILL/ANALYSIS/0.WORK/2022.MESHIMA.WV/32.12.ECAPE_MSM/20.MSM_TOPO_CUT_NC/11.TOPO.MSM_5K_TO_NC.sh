INDIR=/work02/DATA/MSM.NC/TOPO
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INFLE=TOPO.MSM_5K
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi

TEXT="MSM TOPO BIG ENDIAN PLAIN BINARY"
ODIR=$INDIR
OFLE=TOPO.MSM_5K.nc
OUT=$ODIR/$OFLE

CTL=$(basename $0 .sh).CTL
cat <<EOF>$CTL
DSET $IN
TITLE TOPO.MSM_5K
UNDEF	-9.99E33
OPTIONS	big_endian yrev
XDEF 481 LINEAR 120.0 0.0625
YDEF 505 LINEAR 22.4 0.05
ZDEF 1 LEVELS 1000 
TDEF  1000 LINEAR 00z01mar1994 12hr
VARS	1
HGT 0 99 TOPOGRAPHY m
ENDVARS

* https://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/etc/README%ef%bc%88%e6%94%af%e6%8f%b4%e3%82%bb%e3%83%b3%e3%82%bf%e3%83%bc%ef%bc%89_201406.txt
*  (2) 座標系
*    等緯度経度格子座標系です。
*
*    (b) メソ数値予報モデルGPV
*       格子数：  東西方向   481    南北方向   505
*      格子間隔：東西方向 0.0625度  南北方向 0.05度
*     先頭の格子点：北緯47.6度  東経120度 
EOF


LEVS=" -levs 1 10 50 100 200 300 400 500 1000 1500 2000 2500 3000 3500 4000"
KIND=" -kind white->white->lightgreen->limegreen->darkgreen->khaki->darkgoldenrod->saddlebrown->maroon"
FS=1
UNIT=m

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh).PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat<<EOF>$GS
'open $CTL'
#'q dims';say result

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
# 'set time ${TIME}'

'set mpdset hires';'set map 1 1 1'
'set xlint 5';'set ylint 5'
'set grid off'
'set mpdset worldmap'
'd HGT'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

say 'MMMMM NETCDF OUT'
'set x 1 481'; 'set y 1 505'; 'set z 1'; 'set t 1'
'define HGT = HGT'
'set sdfwrite -3dt -nc4 $OUT'
'sdfwrite HGT'

'quit'
EOF


grads -bcp $GS
rm -vf $GS
if [ -f $FIG ];then echo FIG: $FIG;fi
if [ -f $OUT ];then echo OUT: $OUT;fi


load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"

begin

INTVL="6HR"
ABB="SST"
VAR="sea_surface_temperature"
UNIT="(C)"
ABB2="T2MCD"

YYYY="1982"
MM="07"
BIPEN="01"

LATS=75.
LATN=82.
LONW=20.
LONE=75.

indir="/work01/DATA/ERA5/EASIA/6HR/SFC/"+ABB

minlat             =  70.                ; min lat to mask
maxlat             =  90.                ; max lat to mask

infle="ERA5_EASIA_"+VAR+"_"+INTVL+"_"+YYYY+MM+"_"+BIPEN+".grib"

f=indir+"/"+infle

a=addfile(f,"r")


print("### READ "+ABB)
system("date -R")

print(a)

;T2M=a->
;lat=a->g0_lat_1
;time=T2M&initial_time0_hours

system("date -R")



end


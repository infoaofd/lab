#!/bin/bash
# Description:
#
# Author: ma
#
# Host: localhost.localdomain
# Directory: /work09/ma/10.WORK/2024.HUMID_HEAT/12.12.ROMPS_HEAT_INDEX/12.14.HEAT_INDEX_TABLE_FIG08_LU_ROMPS_2022
#
# Revision history:
#  This file is created by /work09/am/mybin/ngmt.sh at Wed, 16 Oct 2024 11:57:22 +0900.

. ./gmtpar.sh
gmtset ANNOT_FONT_SIZE_PRIMARY 10p

range=300/330/40/100
size=-JX5/-3
xanot=a2f2:"Temperature${sp}[K]":
yanot=a5f5:"RH${sp}[%]":
anot=${xanot}/${yanot}WsNe

IN=HI_INDEX_TABLE.TXT
if [ ! -f $IN ]; then echo EEEEE NO SUCH FILE, $IN; exit 1; fi
GRD=$(basename $IN .TXT).GRD
out=$(basename $IN .TXT)_K.ps; FIG=$(basename $out .ps).PDF

awk '{print $1,$2*100,$3}' $IN|surface -R$range -I0.5/1 -T1 -G$GRD

CPTFILE=CPTFILE.TXT; CPT=hot
makecpt -C$CPT -T300/460/1 -I -Z >$CPTFILE

grdimage $GRD -R$range $size -B$anot -C$CPTFILE -X1.5 -Y2 -K -P > $out

psscale -D5.2/1.5/3/0.08 -C$CPTFILE -Ba20f10::/:K: -O -K  >>$out

xoffset=-1; yoffset=3
curdir1=$(pwd); now=$(date -R); host=$(hostname)
time=$(ls -l --time-style=long-iso  ${IN} | awk '{print $6, $7}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${IN} (${time})
0 0.90  9 0 1 LM FIG: ${FIG}
EOF

if [ -f $IN ];then echo INPUT: $IN;fi
if [ -f $out ];then ps2pdfwr $out $FIG ; rm -vf $out ; fi
if [ -f $FIG ];then echo FIG: $FIG ; fi


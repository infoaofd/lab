#!/bin/bash
#
# Sat, 12 Oct 2024 17:12:50 +0900
# localhost.localdomain
# /work09/ma/00.WORK/2024.HUMID_HEAT/12.12.ROMPS_HEAT_INDEX/12.12.TEST_HEAT_INDEX_PROGRAM
#
src=$(basename $0 .sh).F90
if [ ! -f $src ];then EEEEE NO SUCH FILE,$src;exit 1;fi
SUB="heatindex.F90"
if [ ! -f $SUB ];then EEEEE NO SUCH FILE,$SUB;exit 1;fi
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/opt/intel/oneapi/vpl/2022.2.5/lib:/opt/intel/oneapi/tbb/2021.7.1/env/../lib/intel64/gcc4.8:/opt/intel/oneapi/mpi/2021.7.1//libfabric/lib:/opt/intel/oneapi/mpi/2021.7.1//lib/release:/opt/intel/oneapi/mpi/2021.7.1//lib:/opt/intel/oneapi/mkl/2022.2.1/lib/intel64:/opt/intel/oneapi/itac/2021.7.1/slib:/opt/intel/oneapi/ippcp/2021.6.2/lib/intel64:/opt/intel/oneapi/ipp/2021.6.2/lib/intel64:/opt/intel/oneapi/dnnl/2022.2.1/cpu_dpcpp_gpu_dpcpp/lib:/opt/intel/oneapi/debugger/2021.7.1/gdb/intel64/lib:/opt/intel/oneapi/debugger/2021.7.1/libipt/intel64/lib:/opt/intel/oneapi/debugger/2021.7.1/dep/lib:/opt/intel/oneapi/dal/2021.7.1/lib/intel64:/opt/intel/oneapi/compiler/2022.2.1/linux/lib:/opt/intel/oneapi/compiler/2022.2.1/linux/lib/x64:/opt/intel/oneapi/compiler/2022.2.1/linux/lib/oclfpga/host/linux64/lib:/opt/intel/oneapi/compiler/2022.2.1/linux/compiler/lib/intel64_lin:/opt/intel/oneapi/ccl/2021.7.1/lib/cpu_gpu_dpcpp:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT= #" -fpp -CB -traceback -fpe0 -check all"
OPT=" -r8 " #-fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "


# OpenMP
#OPT2=" -fopenmp "

echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo
ls -lh ${exe}
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf $exe $nml
D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf $exe $nml
D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"

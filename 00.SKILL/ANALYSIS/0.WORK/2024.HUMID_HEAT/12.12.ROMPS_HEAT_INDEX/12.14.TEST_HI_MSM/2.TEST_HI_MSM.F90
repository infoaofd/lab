PROGRAM TEST_READ

! netCDFライブラリで使用する変数や定数の設定ファイルを読み込む
use netCDF ! netCDF4
use mod_heatindex
! include 'netcdf.inc' !netCDF3

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname
integer,parameter::IM=481, JM=505, NT=24

real,DIMENSION(IM,JM,NT)::rh,temp,HI
real(8)::rh8,temp8,HI8

real::lon(IM), lat(JM)
real::time(NT)
! real(8)::time(NT)
CHARACTER(LEN=40):: name_lon,units_lon, &
name_lat,units_lat,&
name_time,units_time
REAL,PARAMETER::FILLVALUE=-999.9

NAMELIST /PARA/ INDIR, INFLE, ODIR, OFLE

READ(5,NML=PARA)

PRINT '(A)','MMMMM INPUT MSM-S'

IN=trim(INDIR)//trim(INFLE)
print '(a,a)','MMMMM INPUT: ',trim(IN)

CALL READ_MSM_S_3D(IN,'rh',IM,JM,NT,rh)
CALL READ_MSM_S_3D(IN,'temp' ,IM,JM,NT,temp)

CALL READ_MSM_S_1D(IN,'lon',IM,lon,name_lon,units_lon)
CALL READ_MSM_S_1D(IN,'lat',JM,lat,name_lat,units_lat)
CALL READ_MSM_S_1D(IN,'time',NT,time,name_time,units_time)
!CALL READ_MSM_S_1D_R8(IN,'time',NT,time,name_time,units_time)
PRINT *

PRINT '(A)','MMMMM HEAT INDEX'
DO n=1,NT
DO J=1,JM
DO I=1,IM
temp8=dble(temp(i,j,n))
rh8=dble(rh(i,j,n)/100.0)
if(temp8 > 200. .and. temp8 < 500. .and. rh8 > -1. .and. rh8 < 105.)then
HI8=heatindex(temp8,rh8)
!print '(f7.1,f7.3,f7.1)',sngl(temp8)-273.15,sngl(rh8),sngl(HI8)-273.15
HI(i,j,n)=sngl(HI8)
else
HI(i,j,n)=FILLVALUE
end if
END DO !I
END DO !J
END DO !n

PRINT '(A)','MMMMM OUTPUT'
CALL WRITE_MSM_S(INDIR,INFLE,ODIR,OFLE,IM,JM,NT, HI, lon, lat, time, &
FILLVALUE, &
name_lon,units_lon, &
name_lat,units_lat, &
name_time,units_time)

END PROGRAM TEST_READ

SUBROUTINE CHECK( STATUS )
use netCDF ! netCDF4
  INTEGER, INTENT (IN) :: STATUS
  IF(STATUS /= NF90_NOERR) THEN 
    PRINT '(A,A)','EEEEE ERROR ',TRIM(NF90_STRERROR(STATUS))
    STOP "ABNORMAL END"
  END IF
END SUBROUTINE CHECK

SUBROUTINE READ_MSM_S_1D(IN,varname,NDIM,var,long_name,units)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::NDIM
REAL,DIMENSION(NDIM),INTENT(INOUT)::var
CHARACTER(LEN=*),INTENT(INOUT):: long_name,units

integer stat, ncid, varid, varid1
real::scale, offset
ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, var)
stat = nf90_get_att(ncid,varid,'long_name',long_name)
stat = nf90_get_att(ncid,varid,'units',units)
print '(a,i5,1x,a,1x,a)',trim(varname),varid,TRIM(long_name),TRIM(units)
stat=NF90_CLOSE(ncid)
END SUBROUTINE READ_MSM_S_1D

SUBROUTINE READ_MSM_S_1D_R8(IN,varname,NDIM,var,long_name,units)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::NDIM
REAL(8),DIMENSION(NDIM),INTENT(INOUT)::var
CHARACTER(LEN=*),INTENT(INOUT):: long_name,units

integer stat, ncid, varid, varid1
real::scale, offset
ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, var)
stat = nf90_get_att(ncid,varid,'long_name',long_name)
stat = nf90_get_att(ncid,varid,'units',units)
print '(a,i5,1x,a,1x,a)',trim(varname),varid,TRIM(long_name),TRIM(units)
stat=NF90_CLOSE(ncid)
END SUBROUTINE READ_MSM_S_1D_R8


SUBROUTINE READ_MSM_S_3D(IN,varname,IM,JM,NT,var)
use netCDF ! netCDF4
CHARACTER(LEN=*),INTENT(IN)::IN
CHARACTER(LEN=*),INTENT(IN)::varname
INTEGER,INTENT(IN)::IM,JM,NT
REAL,DIMENSION(IM,JM,NT),INTENT(INOUT)::var
integer stat, ncid, varid, varid1
real::scale, offset
integer(2)::varin(IM,JM,NT) ! short型の2バイト整数

ncid=0;stat = nf90_open(IN, nf90_nowrite, ncid) ! ファイルを開く
!print *,'nf90 open stat=',stat
!print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
!print *,'nf90_get_att stat=',stat
stat = nf90_get_att(ncid,varid,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
print '(a,f10.5,a,g11.4)','scale_factor=',scale,' offset=',offset

var(:,:,:)=float(varin(:,:,:))*scale+offset

stat=NF90_CLOSE(ncid)!入力ファイル閉じる

print *
END SUBROUTINE READ_MSM_S_3D



SUBROUTINE WRITE_MSM_S(INDIR,INFLE,ODIR,OFLE,IM,JM,NT, HI, lon, lat, time, &
FILLVALUE, &
name_lon,units_lon, &
name_lat,units_lat, &
name_time,units_time)

use netcdf

CHARACTER(len=500),INTENT(IN)::INDIR,INFLE,ODIR,OFLE
CHARACTER*2000::OUT
INTEGER,INTENT(IN)::IM,JM,NT
REAL,INTENT(IN)::HI(IM,JM,NT)
REAL,INTENT(IN)::lon(IM),lat(JM)
REAL,INTENT(IN)::time(NT)
REAL,INTENT(IN)::FILLVALUE
!REAL(8),INTENT(IN)::time(NT)
character(len=40)::units_lon,units_lat, units_time
character(len=40)::name_lon,name_lat,name_time

INTEGER :: NCIDO, VARIDO !NC4 OUTPUT VAR
INTEGER :: LON_DIM_ID, LAT_DIM_ID, TIME_DIM_ID
INTEGER :: LON_ID, LAT_ID, TIME_ID
INTEGER STATUS

OUT=trim(ODIR)//'/'//trim(OFLE)

! 出力ファイルを開く
print '(A,A)','MMMMM OPEN ',TRIM(OUT)
CALL CHECK( NF90_CREATE( TRIM(OUT), NF90_HDF5, NCIDO) )

! 次元を定義する
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lon', IM, LON_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'lat', JM, LAT_DIM_ID) )
CALL CHECK( NF90_DEF_DIM(NCIDO, 'time', NF90_UNLIMITED, TIME_DIM_ID) )
print *,'def_dim'

! 変数を定義する
CALL CHECK( NF90_DEF_VAR(NCIDO, 'time', NF90_REAL, TIME_DIM_ID, TIME_ID) )
!CALL CHECK( NF90_DEF_VAR(NCIDO, 'time', NF90_DOUBLE, TIME_DIM_ID, TIME_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lat',  NF90_REAL, LAT_DIM_ID, LAT_ID) )
CALL CHECK( NF90_DEF_VAR(NCIDO, 'lon',  NF90_REAL, LON_DIM_ID, LON_ID) )
print *,'def_var'

! 変数に attributionをつける
CALL CHECK( NF90_PUT_ATT(NCIDO, LON_ID,  'units', units_lon) )
print *,trim(units_lon)
CALL CHECK( NF90_PUT_ATT(NCIDO, LAT_ID,  'units', units_lat) )
print *,trim(units_lat)
CALL CHECK( NF90_PUT_ATT(NCIDO, TIME_ID, 'units', units_time) )
print *,trim(units_time)
print *,'attribution'

! GLOBAL ATTRIBUTES
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"output_dir", TRIM(ODIR)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"output_file",TRIM(OFLE)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"input_dir", TRIM(INDIR)) )
CALL CHECK( NF90_PUT_ATT(NCIDO,NF90_GLOBAL,"input_file",TRIM(INFLE)) )
print *,'global attribution'

! define モードを終了する。
CALL CHECK( NF90_ENDDEF(NCIDO) )
print *,'define'

CALL CHECK( NF90_PUT_VAR(NCIDO, TIME_ID, time ) )
print *,'put_var time'
CALL CHECK( NF90_PUT_VAR(NCIDO, LAT_ID, lat ) )
print *,'put_var lat'
CALL CHECK( NF90_PUT_VAR(NCIDO, LON_ID, lon ) )
print *,'put_var lon'

CALL CHECK( NF90_DEF_VAR(NCIDO, "Heat_Index", NF90_REAL, &
     (/LON_DIM_ID,LAT_DIM_ID,TIME_DIM_ID/), VARIDO))
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO,'units','K') )
CALL CHECK( NF90_PUT_ATT(NCIDO, VARIDO, "_FillValue", FILLVALUE) )
CALL CHECK( NF90_PUT_VAR(NCIDO, VARIDO, HI ) )
print *,'put_var HI'

CALL CHECK(NF90_CLOSE(NCIDO))

END SUBROUTINE WRITE_MSM_S

script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

yearday=31+28+31+30+31+18

INDIR1="/work01/DATA/ERA5/ECS_PS/01DY_FLUX_ANOMALY/QVAPOR/"
INFLE1="ERA5_ECS_PS_QVAPOR_01DY_2022.nc"
INDIR2="/work01/DATA/ERA5/ECS_PS/DAILY_CLIM/"
INFLE2="DAILY_CLIM_SMO_1992-2022_qvapor.nc"
IN1=INDIR1+INFLE1
IN2=INDIR2+INFLE2

a1=addfile(IN1,"r")
a2=addfile(IN2,"r")
VARNAME1="qvapor"
RAW=a1->$VARNAME1$
RAW!1="lat"
RAW!2="lon"

VARNAME2="qvapor_sm"
CLM=a2->$VARNAME2$
CLM!1="lat"
CLM!2="lon"

lon=a1->g0_lon_2
lat=a1->g0_lat_1
RAW&lat=lat
RAW&lon=lon
CLM&lat=lat
CLM&lon=lon

printVarSummary(RAW)

time    = a1->initial_time0_hours                        ; time:units = "hours since 1-1-1 00:00:0.0"                               
   TIME    = cd_calendar(time, 0)          ; type float 
   year    = toint( TIME(:,0) )
   month   = toint( TIME(:,1) )
   day     = toint( TIME(:,2) ) 
   ddd     = day_of_year(year, month, day) 
   yyyyddd = year*1000 + ddd                  
;print(yyyyddd)

ANO = calcDayAnomTLL (RAW, yyyyddd, CLM)


print("MMMMM PLOTTING")
; 作図開始

yyyy=tostring(year(yearday))
mm=sprinti("%02d",month(yearday))
dd=tostring(day(yearday))

month_abbr = (/"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", \
                    "Oct","Nov","Dec"/)
mmm=month_abbr(month(yearday))
date=dd+mmm+yyyy

FIG=script+"_"+yyyy+"-"+mm+"-"+dd
TYP="PDF"


wks = gsn_open_wks(TYP, FIG)
;作図するファイルを開く
gsn_define_colormap(wks,"NCV_jaisnd") ;"WhiteBlueGreenYellowRed")


res=True
; resmpの情報をresに引き継ぐ
res@gsnDraw  = False ;don't draw
res@gsnFrame = False ;don't advance frame

res@cnFillOn = True
; Trueの場合, 色で塗分けする
res@cnLinesOn = False
; Falseの場合, 等値線を書かない
res@gsnAddCyclic = False
;地球一周分のデータでない場合Falseにする

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = -3.
res@cnMaxLevelValF =  3.
res@cnLevelSpacingF = 0.5

res@pmLabelBarHeightF = 0.03
res@pmLabelBarOrthogonalPosF = .15
res@cnFillDrawOrder = "PreDraw"
res@pmLabelBarWidthF=0.6
res@lbTitleOn        = True
res@lbTitleString = "g/kg" ;"W/m~S~2~N~"
res@lbTitlePosition = "Right" ; title position
res@lbTitleFontHeightF= .02 ;Font size
res@lbTitleDirection = "Across" ; title direction

resmp=res
; 地図の描画の設定に関する情報を記載する変数(resmp)を用意する
resmp@mpDataBaseVersion="HighRes"
resmp@mpFillOn               = True    ;地図を塗りつぶす
resmp@mpLandFillColor = "black"
resmp@mpMinLonF = 120     ; 経度の最小値
resmp@mpMaxLonF = 150     ; 経度の最大値
resmp@mpMinLatF =   5     ; 緯度の最小値
resmp@mpMaxLatF =  35     ; 緯度の最大値
resmp@tiMainString    = ""  ; タイトルの文字列
resmp@gsnLeftString   = "QV 2m" ; 小タイトル(左側)の文字列
resmp@gsnCenterString = ""  ; 小タイトル(左側)の文字列
resmp@gsnRightString  = date  ; 小タイトル(右側)の文字列

ANOPLOT=ANO(yearday,:,:)*1000.0
copy_VarMeta(ANO(yearday,:,:),ANOPLOT)
plot=gsn_csm_contour_map(wks,ANOPLOT,resmp)

res@cnFillOn = False
res@cnLinesOn = True
;res@cnLevelSelectionMode = "ManualLevels"
;res@cnMinLevelValF = 5.
;res@cnMaxLevelValF = 25.
;res@cnLevelSpacingF = 1.
res@cnLevelSelectionMode = "AutomaticLevels"
res@tiMainString    = ""  ; タイトルの文字列
res@gsnLeftString   = ""  ; 小タイトル(左側)の文字列
res@gsnCenterString = ""  ; 小タイトル(左側)の文字列
res@gsnRightString  = ""  ; 小タイトル(右側)の文字列
CLMPLOT=CLM(yearday,:,:)*1000
copy_VarMeta(CLM(yearday,:,:),CLMPLOT)
plot2=gsn_csm_contour(wks,CLMPLOT,res)
overlay(plot,plot2)

draw(plot)
frame(wks)
print("MMMMM IN1 : "+IN1)
print("MMMMM IN1 : "+IN2)
print("MMMMM FIG: "+FIG+"."+TYP)

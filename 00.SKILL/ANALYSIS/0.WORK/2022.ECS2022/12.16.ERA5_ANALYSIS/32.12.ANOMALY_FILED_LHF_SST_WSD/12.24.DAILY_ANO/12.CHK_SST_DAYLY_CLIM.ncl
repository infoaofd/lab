; 
; NCL_MAP_SST_LHF.sh
;
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

   month_abbr = (/"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", \
                    "Oct","Nov","Dec"/)

HOST=systemfunc("hostname")
CWD=systemfunc("pwd")

;arg    = getenv("NCL_ARG_2")

INDIR="/work01/DATA/ERA5/ECS_PS/DAILY_CLIM/"
INFLE="DAILY_CLIM_SMO_1992-2022_SST.nc"
IN=INDIR+INFLE
a=addfile(IN,"r")

yearday=31+28+31+30+31+17
sst=a->SST_sm(yearday,:,:)
lon=a->longitude
lat=a->latitude
sst!0="lat"
sst!1="lon"
sst&lon=lon
sst&lat=lat

;printVarSummary(sst)

print("MMMMM PLOTTING")
; 作図開始

FIG="5.CHK_SST_DAYLY_CLIM_0617"
TYP="pdf"
; 図のファイル名とファイルの種類(pdf)を指定

wks = gsn_open_wks(TYP, FIG)
;作図するファイルを開く
gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")


res=True
; resmpの情報をresに引き継ぐ
res@gsnDraw  = False ;don't draw
res@gsnFrame = False ;don't advance frame

res@cnFillOn = True
; Trueの場合, 色で塗分けする
res@cnLinesOn = False
; Falseの場合, 等値線を書かない
res@gsnAddCyclic = False
;地球一周分のデータでない場合Falseにする

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 280.
res@cnMaxLevelValF = 305.
res@cnLevelSpacingF = 1.

res@pmLabelBarHeightF = 0.03
res@pmLabelBarOrthogonalPosF = .15
res@cnFillDrawOrder = "PreDraw"
res@pmLabelBarWidthF=0.6
res@lbTitleOn        = True
res@lbTitleString = "K" ;"W/m~S~2~N~"
res@lbTitlePosition = "Right" ; title position
res@lbTitleFontHeightF= .02 ;Font size
res@lbTitleDirection = "Across" ; title direction

resmp=res
; 地図の描画の設定に関する情報を記載する変数(resmp)を用意する
resmp@mpDataBaseVersion="HighRes"
resmp@mpFillOn               = True    ;地図を塗りつぶす
resmp@mpLandFillColor = "black"
resmp@mpMinLonF = 120     ; 経度の最小値
resmp@mpMaxLonF = 150     ; 経度の最大値
resmp@mpMinLatF =   5     ; 緯度の最小値
resmp@mpMaxLatF =  35     ; 緯度の最大値

plot=gsn_csm_contour_map(wks,sst,resmp)

draw(plot)
frame(wks)
;print("MMMMM IN : "+IN)
print("MMMMM FIG: "+FIG+"."+TYP)

#!/bin/bash
P_INIT=500m

YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2022061916}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=122 ;LONE=130 ; LATS=25 ;LATN=31.5
TIME=${HH}Z${DD}${MMM}${YYYY}

CTL1=LFM_ECAPE_E00.CTL; 
CTL2=LFM00.CTL
#CTL2=LFM_ECAPE_E05.CTL; CTL3=LFM_ECAPE_E10.CTL; CTL4=LFM_ECAPE_E20.CTL
if [ ! -f $CTL1 ];then echo ERROR in $CTL1: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL2 ];then echo ERROR in $CTL2: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL3 ];then echo ERROR in $CTL3: NO SUCH FILE,;exit1;fi
if [ ! -f $CTL4 ];then echo ERROR in $CTL4: NO SUCH FILE,;exit1;fi

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh)_${YYYY}-${MM}-${DD}_${HH}.pdf ;#eps

LEVS="100 550 50"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind white->tan->gold->orange->red->maroon'
FS=2
UNIT=J/kg
TEXT1="ECAPE RH";  TEXT2="ECAPE E=5%/km"
TEXT3="ECAPE E=10%/km"; TEXT4="ECAPE E=20%/km"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}'; 'open ${CTL2}' 
#'q ctlinfo 1'; say result
;# 'open ${CTL3}'; 'open ${CTL4}'

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 2

ytop=9.5; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims';say result
'd CAPE.1'
#'d temp.1'
'set xlab off';'set ylab off'

'set dfile 2'
'set lev 850'
'set gxout contour'
'set ccolor 0';'set cthick 6'; 'set clevs 90';'set clab off'
'd rh.2'
'set gxout contour'
'set ccolor 1';'set cthick 2'; 'set clevs 90'
'set clab on'; 'set clopts 1 1 0.07'
'd rh.2'

say '### OBS BOX'; '02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

x1=xr+0.2; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.12 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2 ;# TEXT
y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT1}'



'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL1} ${CTL2}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS

<<COMMENT
echo; echo MMMMM BACK-UP SCRIPTS
ODIR= ; mkdir -vp $ODIR
TMP=TMP_$(basename $0)
echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
rm -f $TMP
echo MMMMM
COMMENT

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


echo "DONE $0."
echo

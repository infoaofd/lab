#!/bin/bash
#
# Mon, 26 Dec 2022 18:24:27 +0900
# p5820.bio.mie-u.ac.jp
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F

P_INIT=500m

FH=$1; FH=${FH:-FH00}
YYYY=$2; MM=$3; DD=$4; HH=$5; ENTRAINMENT_RATE=$6
YYYY=${YYYY:-2022}; MM=${MM:-06}; DD=${DD:-19}; HH=${HH:-00}

ENTRAINMENT_RATE=${ENTRAINMENT_RATE:-0.0}
W=10.0;DT=10.0 ;MAXLEV=10000.0

SRC=$(basename $0 .sh).F90
SUBLIST="module_calc_entraining_CAPE.f90" ;#MyCAPE.f
OBJ="module_calc_entraining_CAPE.o $(basename $SRC .F90).o"

exe=$(basename $SRC .F90).exe
nml=$(basename $0 .sh).nml

f90=gfortran
OPT1="-O2"
#OPT1="-g3 -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"

#f90=ifort
#OPT1="-traceback -fpe0 -CB"

NCDIR=/usr/local/netcdf-c-4.8.0
OPT2=" -L$NCDIR/lib -lnetcdff -I${NCDIR}/include"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${NCDIR}



INDIR1="/work01/DATA/LFM_PROC_ROT/${FH}"
INFLE1="LFM_PRS_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_THERMO.nc"
IN1=${INDIR1}/${INFLE1}

if [ ! -f $IN1 ];then echo NO SUCH FILE, $IN1; exit 1; fi
INDIR2="/work01/DATA/LFM_HCUT.SFC.HINTPL/${FH}"
INFLE2="LFM_SFC_HINTPL_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}.nc"
IN2=${INDIR2}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE, $IN2; exit 1; fi

INDIR3="/work02/DATA/ETOPO1"
INFLE3="ETOPO1_CUT_LFM_PRS.nc"
IN3=${INDIR3}/${INFLE3}
if [ ! -f $IN3 ];then echo NO SUCH FILE, $IN3; exit 1; fi

ODIR=OUT_$(basename $0 .sh); mkd $ODIR
OFLE=LFM_PRS_${FH}_VALID_${YYYY}-${MM}-${DD}_${HH}_CAPE${P_INIT}_${ENTRAINMENT_RATE}.nc

cat<<EOF>$nml
&para
INDIR1="$INDIR1"
INFLE1="$INFLE1"
INDIR2="$INDIR2"
INFLE2="$INFLE2"
INDIR3="$INDIR3"
INFLE3="$INFLE3"
PSAFILE="$PSAFILE"
ODIR="${ODIR}"
OFLE="${OFLE}"
MIY=281
MJX=316
MKZH=12
entrainment_rate=${ENTRAINMENT_RATE}
w=${W}
maxlev=${MAXLEV}
dt=${DT}
Z_P=500.0
&end
EOF

echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${SRC} ${SUBLIST}
echo
ls -lh --time-style=long-iso ${SRC} ${SUBLIST}
echo

echo Compiling ${src} ${SUBLIST}  ...
echo
${f90} ${OPT1} ${SUBLIST} -c
if [ $? -ne 0 ];then echo COMPILE ERROR ${SUBLIST};exit 1;fi
${f90} ${OPT1} ${SRC} ${OPT2}  -c
if [ $? -ne 0 ];then echo COMPILE ERROR ${SRC};exit 1;fi
${f90} ${OPT1} ${OBJ} ${OPT2} -o ${exe}
if [ $? -ne 0 ];then echo LINK ERORR ${SUBLIST};exit 1;fi

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)
echo ENTRAINMENT_RATE $ENTRAINMENT_RATE
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "MMMMM ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo MMMMM TERMINATED.
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"
exit 1
fi
echo
echo "MMMMM Done ${exe}"
echo
D2=$(date -R)
echo "START: $D1"
echo "END:   $D2"

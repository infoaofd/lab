program main_entraining_cape
USE entraining_cape

implicit none
integer::i,j,h,imx,jmx,imn,jmn,n,lag,hmn,hmx,tmn,tmx,t,hmx2,hmx3,iyear,hh,hhmx,hhmn,hhmx2,nk,iflag,syear,eyear,hmx4,mmonth,errorflag
parameter(lag=40,jmn=1,jmx=40,imn=1,imx=40,hmn=1,hmx=80,hmx3=79,tmn=1,hmx2=80,syear=2021,eyear=2021,hmx4=80)
real::u(imx,jmx,hmx),v(imx,jmx,hmx),tmp(imx,jmx,hmx),spfh(imx,jmx,hmx),hgt(imx,jmx,hmx),tp,pp,rp,rh2(hmx4),hgt2(hmx4),tt(hmx4),r(hmx4),rd,pi,sig,caped,tob,new_p(hmx4),new_tmp(imx,jmx,hmx4),new_sp(imx,jmx,hmx4),dp,rh1(imx,jmx,hmx)
real,dimension(imx,jmx)::flon,flat,lcl,lfc,el,cin,ecape
real,dimension(imx,jmx,hmx)::eptemp,qvapor,pt,pres
real,dimension(hmx)::p2
real(8)::w,dt,entrainment_rate,maxlev
real::eptmax,hgt_p,tem_p,qv_p,undef,cape_output,lcl_output,el_output,lfc_output,p0,cin_output,cp
real::e,alpha,x,r1,beta,r2,r3,r4,r5,r6,r7,rw,epsilon,es,qv,qs,tlcl
character::year*4,month*2,imonth*3,region*3
      dp=25
      rd=287.
      cp=1004.5
      pi=atan(-1.)
      sig=0.
      nk=1
      region='JPN'
      p0=1000.e2
      w=1.
      dt=1.
      entrainment_rate=0.
      maxlev=20000.
      undef=-999.

      r1=643.
      r2=5.1959
      r3=3.1473172
      r4=2.95944e-3
      r5=4.191398e-4
      r6=1.829924e-7
      r7=8.243516e-8
      rw=461.

!      print*, 'please input APR or MAY'
!      read(5,*) iyear
!      read(*,*) month
!      write(6,*) month
!      write(year,'(i4)') iyear
      !write(month,'(i3)') imonth

      
!      do h=hmn,hmx4
!       new_p(h)=1000.-dp*(h-1)
!      enddo
      
      do mmonth=5,5

       if(mmonth.eq.1) then
       month='01'
       tmx=4*31
       elseif(mmonth.eq.2) then
       month='02'
       tmx=4*28
       elseif(mmonth.eq.3) then
       month='03'
       tmx=4*31
       elseif(mmonth.eq.4) then
       month='04'
       tmx=4*30
       elseif(mmonth.eq.5) then
       month='05'
       tmx=4*31
       elseif(mmonth.eq.6) then
       month='06'
       tmx=4*30
       elseif(mmonth.eq.7) then
       month='07'
       tmx=4*31
       elseif(mmonth.eq.8) then
       month='08'
       tmx=4*31
       elseif(mmonth.eq.9) then
       month='09'
       tmx=4*30
       elseif(mmonth.eq.10) then
       month='10'
       tmx=4*31
       elseif(mmonth.eq.11) then
       month='11'
       tmx=4*30
       elseif(mmonth.eq.0) then
       month='12' 
       tmx=4*31
       endif
     

      do iyear=syear,eyear
      write(6,*) iyear,mmonth

      if(mod(iyear,4).eq.0.and.mmonth.eq.2) then
       tmx=4*29
      elseif(mmonth.eq.2) then
       tmx=4*28
      endif

       write(year,'(i4)') iyear
!      open(50,file='../data/uwind1000_0_JRA55.'
!     + //year//'.'//month//'.data',
!     + status='old',form='unformatted')
!      open(51,file='../data/vwind1000_0_JRA55.'
!     + //year//'.'//month//'.data',
!     + status='old',form='unformatted')

!      open(52,file='../data/'//region//'/NEW_TMP1000_0_JRA55_'//region//'.'//year//'.'//month//'.data',status='old',form='unformatted')
!      open(53,file='../data/'//region//'/NEW_SPFH1000_0_JRA55_'//region//'.'//year//'.'//month//'.data',status='old',form='unformatted')
!      open(54,file='../data/'//region//'/NEW_HGT1000_0_JRA55_'//region//'.'//year//'.'//month//'.data',status='old',form='unformatted')

      open(52,file='fortbin-201909220000.dat',status='old',form='unformatted')
      open(56,file='muecape_erate0_fortbin-201909220000.dat',status='unknown',form='unformatted')
      write(6,*) './fortbin-201909220000.dat'
      do t=1,1


      !do i = 1, imx
      ! do j = 1, jmx
      !!   read(52) flat(i,j)
      !  write(6,*) 'flat', flat(i,j)
      ! enddo
      !enddo

       
       read(52) ((flat(i,j),i=imn,imx),j=jmn,jmx)
       read(52) ((flon(i,j),i=imn,imx),j=jmn,jmx)
       ! write(6,*) 'flat', flat(20,20)
       ! write(6,*) 'flon', flon(20,20)
      !do h=hmn,hmx
       read(52) (((hgt(i,j,h),i=imn,imx),j=jmn,jmx),h=hmn,hmx)
       ! write(6,*) 'hgt', hgt(20,20,h)
      !enddo
      !  write(6,*) 'hgt', hgt(20,20,40)
      !do h=hmn,hmx
       read(52) (((pres(i,j,h),i=imn,imx),j=jmn,jmx),h=hmn,hmx)
      !enddo
        write(6,*) 'pres', pres(20,20,1)
 
      !do h=hmn,hmx
       read(52) (((pt(i,j,h),i=imn,imx),j=jmn,jmx),h=hmn,hmx)
      !enddo
        write(6,*) 'pt', pt(20,20,40)

      !do h=hmn,hmx
       read(52) (((qvapor(i,j,h),i=imn,imx),j=jmn,jmx),h=hmn,hmx)
      !enddo
        write(6,*) 'qv', qvapor(20,20,40)


! calculate RH



do h=hmn,hmx
 do j=jmn,jmx
  do i=imn,imx
   !pt(i,j,h)=tmp(i,j,h)*(p0/p(h))**(rd/cp)
   tmp(i,j,h)=pt(i,j,h)/((p0/pres(i,j,h))**(rd/cp))
  enddo
 enddo
enddo

!calcu_relative humidity

do h=hmx2+1,hmx
 rh1(i,j,h)=0.
enddo


        do h=1,hmx
          do j=1,jmx
            do i=1,imx
             x=0.1*(tmp(i,j,h)-453.)
             alpha=(r1-tmp(i,j,h))/tmp(i,j,h)
             beta=r3-r4*x+r5*(x**2)-r6*(x**3)+r7*(x**4)
             epsilon=r2-alpha*beta
             es=133.3224*(10.**epsilon)
             qs=es/(es+(pres(i,j,h)-es)*(rw/rd))
             rh1(i,j,h)=qvapor(i,j,h)/qs*100.
             e=pres(i,j,h)*qvapor(i,j,h)/(0.62197+qvapor(i,j,h))
             tlcl=2840./(3.5*log(tmp(i,j,h))-log(e/100)-4.805)+55.
             eptemp(i,j,h)=tmp(i,j,h)*(p0/pres(i,j,h))**(0.2854*(1.-0.28*qvapor(i,j,h)))&
                        *exp((3376./tlcl-2.54)*qvapor(i,j,h)*(1.+0.81*qvapor(i,j,h)))
             enddo
            enddo 
          enddo

! calculate initial parcel parameters
write(6,*) 'CALC ECAPE! year, month', iyear, mmonth
 do i=imn,imx
  do j=jmn,jmx
   do h=hmn,hmx-1
     tt(h)=tmp(i,j,h+1)
     rh2(h)=rh1(i,j,h+1)
     hgt2(h)=hgt(i,j,h+1)
     p2(h) = pres(i,j,h+1)/100.
     !write(6,*) tt(h)
   enddo
   eptmax = 0.
    do h = hmn,hmx-1
     if(hgt2(h).le.3000.and.eptmax.lt.eptemp(i,j,h)) then
      eptmax = eptemp(i,j,h)
      hgt_p = hgt2(h)
      tem_p = tt(h)
      qv_p = qvapor(i,j,h)
     endif
    enddo
    !write(6,*) 'input parcel', tt(1),rh2(1),hgt2(1),p2(1)
    ! call calc_initial_parcel_ml1000m(hmx3,p2,hgt2,tt,rh2,undef,hgt_p,tem_p, qv_p,errorflag)
    write(6,*) 'parcel property', hgt_p,tem_p,qv_p,eptmax
     call calc_CAPE_Romps_and_Kuang_2010_no_fusion_total_fallout(hgt_p, tem_p, qv_p, hmx3, p2, hgt2, tt, rh2, undef,w, dt, entrainment_rate, maxlev,cape_output, cin_output, lcl_output, lfc_output, el_output)
     ecape(i,j)=cape_output
     cin(i,j)=cin_output
     lcl(i,j)=lcl_output
     lfc(i,j)=lfc_output
     el(i,j)=el_output
     write(6,*) 'ECAPE=', cape_output
  enddo
 enddo
 

write(56) ((ecape(i,j),i=imn,imx),j=jmn,jmx)
write(56) ((cin(i,j),i=imn,imx),j=jmn,jmx)
write(56) ((lcl(i,j),i=imn,imx),j=jmn,jmx)
write(56) ((lfc(i,j),i=imn,imx),j=jmn,jmx)
write(56) ((el(i,j),i=imn,imx),j=jmn,jmx)

  enddo !tloop

 enddo ! month loop

enddo !year loop



endprogram main_entraining_cape

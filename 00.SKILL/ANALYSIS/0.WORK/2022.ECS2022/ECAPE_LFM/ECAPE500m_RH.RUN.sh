#!/bin/bash

# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/14.12.CAPE3D.F
# /work03/am/2022.06.ECS.OBS/26.16.LFM.NCL.pt3/15.12.ECAPE_TEST
# /work09/am/00.WORK/2022.ECS2022/26.LFM.3/22.16.ECAPE500m.RH.ECMWF_2

P_INIT=500m

ELIST="00"  #ENTRAINMENT_RATE %/km

FH_LIST="FH00" ;# FH03" ;#FH_LIST="00"

YYYY=2022; MM=06; MMM=JUN; DDLIST="19"

EXE=ECAPE${P_INIT}_NC4_RH.sh
if [ ! -f $EXE ];then echo NO SUCH FILE,$EXE; exit 1;fi

for FH in $FH_LIST; do

for ERATE in $ELIST; do

for DD in $DDLIST; do

if [ $DD == "18" ]; then
#HHLIST="20 21 22 23"
HHLIST="23"
elif [ $DD == "19" ]; then
HHLIST="19 20" # 04 05 06 12 13"
HHLIST="16"
fi

for HH in $HHLIST; do

echo "NNNNN $EXE $FH $YYYY $MM $DD $HH"

$EXE $FH $YYYY $MM $DD $HH $ERATE

echo "NNNNN DONE $EXE  $YYYY $MM $DD $HH"
done #HHLIST

done #DDLIST

done #ELIST

done #FH


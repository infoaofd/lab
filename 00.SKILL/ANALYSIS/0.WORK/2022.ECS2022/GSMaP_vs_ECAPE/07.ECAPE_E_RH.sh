#!/bin/bash

YYYYMMDDHH=$1;YYYYMMDDHH=${YYYYMMDDHH:=2022061807}
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

LONW=122 ;LONE=130 ; LATS=25 ;LATN=31.5
TIME=${HH}Z${DD}${MMM}${YYYY}

VAR1=CAPE.1
CTL1=07.LFM_ECAPE_E_RH.CTL
if [ ! -f $CTL1 ];then echo NO SUCH FILE $CTL1;exit 1; fi;
VAR2=rh.2
CTL2=06.LFM00.CTL
if [ ! -f $CTL2 ];then echo NO SUCH FILE $CTL2;exit 1; fi;

GS=$(basename $0 .sh).GS

FIGDIR=FIG/${YYYY}-${MM}-${DD}_${HH}; mkd $FIGDIR
FIG=${FIGDIR}/${YYYY}-${MM}-${DD}_${HH}_$(basename $0 .sh).pdf ;#eps


LEVS="100 550 50"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='-kind white->tan->gold->orange->red->maroon'
FS=2
TEXT1="ECAPE"

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}' 

'set vpage 0.0 8.5 0.0 11' ;# SET PAGE

xmax = 2; ymax = 2

ytop=9.5; xwid = 6.0/xmax; ywid = 5.0/ymax

xmargin=0.7; ymargin=0.7

nmap = 1
ymap = 1 ;#while (ymap <= ymax)
xmap = 1 ;#while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc';'set mpdset hires';'set grads off';'set grid off'
'set xlevs 122 124 126 128 130';'set ylint 1'
'set xlopts 1 2 0.1';'set ylopts 1 2 0.1';

'set gxout shade2'
'color ${LEVS} ${KIND}' ;# SET COLOR BAR

'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set z 1' ;# lev ${LEV}'
'set time ${TIME}'
'q dims';say result
'd ${VAR1}'
'set xlab off';'set ylab off'

'set dfile 2'
'set gxout contour'; 'set cthick 1'
'set clab on'; 'set clopts 1 1 0.07'
'set clevs 90'
'set lev 850'
'd ${VAR2}'

say '### OBS BOX'; '19.OBS.BOX.GS'

'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4);xr=subwrd(line3,6)
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
YUP=yt

x1=xr+0.35; x2=x1+0.1; y1=yb; y2=yt ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 3 -line on -edge circle'
x=(x1+x2)/2; y=yt+0.2
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xl-0.3; x2=xr+0.15; y1=yb-0.5; y2=y1+0.1 ;# LEGEND COLOR BAR
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.08 -fh 0.1 -fs $FS -ft 3 -line on -edge circle'
x=x1+(x2-x1)/2; y=y2-0.4
'set strsiz 0.08 0.1'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

x=(xl+xr)/2 ;# TEXT
y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TIME} ${TEXT1}'



'set strsiz 0.08 0.1'; 'set string 1 l 3 0' ;#HEADER
xx = 0.2; 
yy = YUP+0.5; 'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"; rm -vf $GS

<<COMMENT
echo; echo MMMMM BACK-UP SCRIPTS
ODIR= ; mkdir -vp $ODIR
TMP=TMP_$(basename $0)
echo "# #!/bin/bash"      >$TMP; echo "# BACK UP of $0" >>$TMP
echo "# $(date -R)"     >>$TMP; echo "# $(pwd)"        >>$TMP
echo "# $(basename $0)">>$TMP; echo "# "               >>$TMP
BAK=$ODIR/$0; cat $TMP $0 > $BAK; ls $BAK
rm -f $TMP
echo MMMMM
COMMENT

echo
if [ -f $FIG ]; then echo "OUTPUT : "; ls -lh --time-style=long-iso $FIG; fi
echo


echo "DONE $0."
echo

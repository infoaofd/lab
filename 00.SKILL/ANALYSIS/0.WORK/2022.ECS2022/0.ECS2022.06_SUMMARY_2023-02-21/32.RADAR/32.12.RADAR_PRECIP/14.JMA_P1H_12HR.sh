#!/bin/bash
# /work09/am/00.WORK/2022.ECS2022/0.ECS2022.06_SUMMARY_2023-02-21/32.RADAR/32.12.RADAR_PRECIP/42.12.JMA_RADAR_P1H
# https://static.cld.navitime.jp/passstorage/html/common/202102/rain_indication.html
gs=$(basename $0 .sh).gs

yyyy=2022; mm=06; dd=$1; dd=${dd:-18}; hh=$2; hh=${hh:-21}

hs=$hh; dh=1
UNIT=[mm/h]

# LEGEND
DX=50       ;# DISTANCE IN km
RLAT=31.4; LON1=129.1
R=6371     ;#RADIUS OF THE EARTH IN km
PI=3.141592653589793; R2D=57.29577951308

figfile=$(basename $0 .sh)_${yyyy}${mm}${dd}_${hh}.PDF #.eps


ctl=$(basename $0 .sh).CTL
cat <<EOF>$ctl
# $(date -R)
# $0
dset output/%y4%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 1000  LINEAR 00:00Z01Jun2022 1hr
vars 1
rr 0 0 rainfall
endvars
EOF
#KIND='-kind white->dodgerblue->lime->gold->red->magenta'
KIND='-kind (255,255,255)->(245,245,245)->(175,237,237)->(152,251,152)->(67,205,128)->(59,179,113)->(250,250,210)->(255,255,0)->(255,164,0)->(255,0,0)->(205,55,0)->(199,20,133)->(237,130,237)->(255,0,255)'
CLEVS='-levs 2 5 10 20 30 40 50 60 70 80'

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

lonw=127.75; lone=129.75; lats=30; latn=31.5

cat <<EOF>$gs
'cc'
'open ${ctl}'

yyyy=${yyyy}; mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif

a.1="(a)"; a.2="(b)"; a.3="(c)"; a.4="(d)"; a.5="(e)"; a.6="(f)"; a.7="(g)"; 
a.8="(h)"; a.9="(g)"; a.10="(i)"; a.11="(j)"; a.12="(k)"


'set lon '${lonw}' '${lone}; 'set lat '${lats}' '${latn}
'set mpdset 'hires

xmax = 4; ymax = 3

xwid = 8.5/xmax; ywid = 5.5/ymax

i = 1; ymap = 1

datetime1=${hs}Z${dd}''mmm''yyyy
'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say 'MMMMM t1= 't1; say dtcheck
hh=substr(dtcheck,1,2); dd=substr(dtcheck,4,2)
mmm=substr(dtcheck,6,3);yyyy=substr(dtcheck,9,4)
if (hh=23)
dd2=dd+1; hh2=0
endif
dd2=dd; hh2=hh+1

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grid off'

'set t 't1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say 'MMMMM t1= 't1; say dtcheck
hh=substr(dtcheck,1,2); dd=substr(dtcheck,4,2)
mmm=substr(dtcheck,6,3);yyyy=substr(dtcheck,9,4)

xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.40)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.09'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.09'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.09'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
#xlevs='122 124 126 128 130' ;# 132'
#'set xlevs 'xlevs
'set xlint 1';'set ylint 1'

#say 'UNDEF'
#'set gxout grfill'
#'set clevs -1000 -998'
#'set rgb 99 40 40 40'
#'set ccols 0 99 0'
#'d const(rr, -999, -u)'


'color $CLEVS $KIND -gxout shaded'


say 'P1H'
'd 'rr

'04.02.FILL.MISSING.GS'
'02.04.OBS.BOX.DASH.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl-0.27; yy = yt+0.12

hh2=hh+1
if(hh2<9)then
hh2='0'hh2
endif
if(hh<23)
title=a.i'  'hh'-'hh2'UTC'dd''mmm''yyyy
endif
if(hh=23)
dd2=dd+1
title=a.i'  'hh'UTC'dd'-00UTC'dd2''mmm''yyyy
endif
'set string 1 l 1 0'; 'set strsiz 0.08 0.1'
'draw string 'xx' 'yy' 'title

say 'MMMMM DISTANCE LEGEND MMMMM'
DLON=${DX}/(${R}*math_cos(${RLAT}*${PI}/180))*${R2D}
LON1=${LON1}; LON2=LON1+DLON; LAT=${RLAT}
'trackplot ' LON1 ' ' LAT ' ' LON2 ' ' LAT ' -c 1 -l 1 -t 3'
CLON=(LON1+LON2)/2; CLAT=LAT-0.075
'q w2xy 'CLON' 'CLAT
xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.08 0.1'; 'set string 1 c 1'
'draw string 'xx' 'yy' ${DX} km'

'set parea off'; 'set vpage off'
*
if (i > 23); break; endif
i = i + 1
xmap = xmap + 1

t1=t1+1

endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color $CLEVS $KIND -gxout shaded -xcbar 2.5 8 0.8 0.9 -edge circle -line on -fs 1 -ft 2' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 8.05 0.85 ${UNIT}';

# Header
'set strsiz 0.08 0.1'; 'set string 1 l 2'
'draw string 0.2 8.5 ${NOW}'; 'draw string 0.2 8.35 ${HOST}'
'draw string 0.2 8.1 ${CWD}'; 'draw string 0.2 7.95 ${CMD}'

'gxprint ${figfile}'

quit
EOF

/work09/am/mybin/grads -bcl "${gs}"
rm -vf $gs $ctl

if [ -f ${figfile} ]; then
echo; echo FIG: ${figfile};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${figfile}.;echo; exit 1
fi

exit 0

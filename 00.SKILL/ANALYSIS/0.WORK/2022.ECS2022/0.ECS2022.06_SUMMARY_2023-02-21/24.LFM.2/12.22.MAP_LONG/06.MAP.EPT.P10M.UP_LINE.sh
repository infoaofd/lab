#!/bin/bash

gs=$(basename $0 .sh).gs

yyyy=2022; mm=06; dd=$1; dd=${dd:-18}
hs=$2; hs=${hs:-23}

   dp=$(expr $dd + 1)

dh=1

#lonw=124; lone=130; lats=26.5;  latn=31.5 #lonw=126; lats=28; lone=131; latn=32
lonw=126.5; lone=129.7; lats=29;  latn=31.5

LEV1=$2;LEV1=${LEV1:-975}

POW=4; FAC=1E${POW}; FACOUT=10\`a-${POW}\`n
POW2=4; FAC2=1E${POW2}

UNIT2="[K]"
KIND2='-kind lightcyan->cyan->mediumspringgreen->lawngreen->moccasin->orange->orangered->red->magenta'
LEVS2='334 360 2'

VAR6=UV; VSCL=30; VSKP=10; VECUNIT="m/s"

#RGB='set rgb 98 0 0 0 80' #MAUL 

figfile=$(basename $0 .sh)_${yyyy}${mm}${dd}_${hs}_${LEV1}.pdf


CTL1=RADAR_10MIN.ctl
if [ ! -f $CTL1 ];then echo ERROR: NO SUCH FILE, $CTL1; exit 1; fi

CTL2=LFM00.CTL
if [ ! -f $CTL2 ];then echo ERROR: NO SUCH FILE, $CTL2; exit 1; fi

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"


cat <<EOF>$gs
'cc';'set datawarn off'

'open ${CTL1}'; 'open ${CTL2}'

yyyy=${yyyy}; mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif; if(mm='03');mmm='MAR';endif
if(mm='04');mmm='APR';endif; if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif; if(mm='09');mmm='SEP';endif
if(mm='10');mmm='OCT';endif; if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif

'set vpage 0.0 11.0 0.0 8.5'

'set lon '${lonw}' '${lone}; 'set lat '${lats}' '${latn}
'set mpdset hires'
'set grid off';'set grads off'

xmax = 4; ymax = 2; xwid = 9.0/xmax; ywid = 5.5/ymax
nmax=6
nmap = 1; ymap = 1

hh=${hs}; dd=${dd}; dp=dd+1

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

say 'dd='dd' hh='hh
if (hh > 23)
hh=hh-24
dd=dd+1
endif

if (hh < 10)
hh='0'hh
endif


hp=hh+1; hm=hh

datetime1=hh'Z'dd''mmm''yyyy; datetime2=hp'Z'dd''mmm''yyyy
datetimem=hm'Z'dd''mmm''yyyy

if (hh = 23)
datetime1='23Z' dd '' mmm ''yyyy; datetime2='00Z' dp ''mmm''yyyy
endif

'set time 'datetime1; 'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say; say dtcheck' t1='t1

'set time 'datetime2; 'q dims'; line=sublin(result,5); 
dtcheck=subwrd(line,6); t2=subwrd(line,9)


xs = 0.3 + (xwid+0.50)*(xmap-1); xe = xs + xwid
ye = 6 - (ywid+0.20)*(ymap-1); ys = ye - ywid

'set xlab on';'set ylab on';'set xlopts 1 2 0.08';'set ylopts 1 2 0.08'

'set parea 'xs ' 'xe' 'ys' 'ye; 'set grads off'
'set xlint 1'; 'set ylint 1'
'set rgb 50 127 73 45' ;# red : 127 Green : 73 Blue : 45
'set map 50 1 1'

'set dfile 2'
'set lev $LEV1'
'set time 'datetime1
'set gxout shade2'
'color $LEVS2 $KIND2'
'd ept.2'

if(nmap = nmax)
'xcbar 10 10.1 6 8 -edge circle -line on -ft 2 -fs 4 -fw 0.1 -fh 0.12' 
'set strsiz 0.1 0.12'; 'set string 1 l 3'; 'draw string 10.1 8.1 ${UNIT2}';
endif

'set xlab off';'set ylab off'

'02.04.OBS.BOX.DASH.GS' ;#'02.02.OBS.BOX.GS' ;# say 'MMMMM OBSERVATION AREA'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

'set lev $LEV1';'set time 'datetime1
'set ccolor 0'; 'set cthick 6'
'set line 1 1 2'
'vec skip(u.2,${VSKP},${VSKP});v.2 -SCL 0.5 $VSCL -P 20 20'

xx=xr-1; yy=yb-0.2
'set ccolor 1'; 'set cthick 3';'set strsiz 0.08 0.1'
'vec skip(u.2,${VSKP},${VSKP});v.2 -SCL 0.5 $VSCL -P 'xx' 'yy' -SL $VECUNIT'


#'set dfile 1';'set z 1';'set time 'datetime1
#'set time 'datetime1; 'q dims'; line=sublin(result,5)
#dtcheck=subwrd(line,6); t1=subwrd(line,9)
#say 'RRRRRR P10M 'dtcheck' 't1
#
#'set gxout contour'; 'set clevs 30';' set cthick 2'
#'set rgb 99 0 255 255'
#'set ccolor 99'
#'set clab off'; 'd rr.1'



'set dfile 2'
'set time 'datetime2
'set lev ${LEV1}'
say 'CCCCCC CNV 'datetime2' LEV '${LEV1}
'CNV=-hdivg(u.2,v.2)*${FAC2}'
'set gxout contour'
'set ccolor 0';'set cthick 6';'set cstyle 1'
'set clevs 1 4'
'set clab off'
#'d CNV'

'set ccolor 14';'set cthick 2';'set cstyle 3'
'set clevs 2'
'set clopts 14 1 0.06';'set clab off'
#'d CNV'



'set dfile 2';'set lev $LEV1';'set time 'datetime1
'set gxout contour'
'set ccolor 0';'set cthick 6';'set cstyle 1'
'set cint 1'; 'set clab off'
'd vpt.2'
'set ccolor 1';'set cthick 2';'set cstyle 1'
'set clevs 295 296 297 298 299 300 302 303 304 305 306 307 308'
'set clopts 1 2 0.06';'set clab off'
'd vpt.2'

'set ccolor 1';'set cthick 4';'set cstyle 1'
'set clevs 301'
'set clopts 1 3 0.06';'set clab on'
'd vpt.2'

rc = gsfallow("on")
obsp( datetime1 )

'06.LINE_LEGEND.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.15
'set string 1 l 2 0'; 'set strsiz 0.1 0.12'

title=hh'UTC'dd''mmm''yyyy' ${LEV1}hPa'

'draw string 'xx' 'yy' 'title

if (nmap = 22); break; endif
nmap = nmap + 1;xmap = xmap + 1

hh=hh+${dh}

endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

# Header
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 0.2 8.4 ${NOW}'; 'draw string 0.2 8.2 ${CWD}'; 'draw string 0.2 8.0 ${CMD}'

'gxprint ${figfile}'

quit

$(cat 06.PLOT.OBSP.N2-N12.GS)

EOF

grads -bcl "${gs}"; #rm -vf $gs
rm -vf $gs

if [ -f ${figfile} ]; then
echo; echo FIG: ${figfile};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${figfile}.;echo; exit 1
fi

exit 0

# https://www.jma.go.jp/jma/kishou/know/radar/kaisetsu.html
#���˖�	���m�����ˎs�i���˖����ʒn��C�ۊϑ����j	33��15��09��	134��10��38��	207.0	24.0	5355.0
#����	���ꌧ�_��s�i�ҐU�R�j	33��26��04��	130��21��25��	983.2	17.0	5365.0
#��q��	���������F�ьS�i����q�j30��38��23��	130��58��45��	302.5	24.0	5365.0
#����	�������������s�i�{�����j28��23��39��	129��33��07��	318.6	24.7	5350.0
#����	���ꌧ���s�i�����j	26��09��12��	127��45��54��	208.4	21.9	5355.0
#�Ί_��	���ꌧ�Ί_�s�i���Γo�x�j24��25��36��	124��10��56��	533.5	17.5	5350.0
COLOR=120/36/122; WHITE=255/255/255
echo "33 26 04 130 21 25" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.1 -G$WHITE -O -K >>$fig
echo "33 26 04 130 21 25" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.07 -G$COLOR -O -K >>$fig

echo "30 38 23 130 58 45" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.1 -G$WHITE -O -K >>$fig
echo "30 38 23 130 58 45" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.07 -G$COLOR -O -K >>$fig

echo "28 23 39 129 33 07" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.1 -G$WHITE -O -K >>$fig
echo "28 23 39 129 33 07" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.07 -G$COLOR -O -K >>$fig

echo "26 09 12 127 45 54" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.1 -G$WHITE -O -K >>$fig
echo "26 09 12 127 45 54" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.07 -G$COLOR -O -K >>$fig

echo "24 25 36 124 10 56" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.1 -G$WHITE -O -K >>$fig
echo "24 25 36 124 10 56" |awk '{print $4+$5/60+$6/3600, $1+$2/60+$3/3600}'|\
psxy -R -JM -Sc0.07 -G$COLOR -O -K >>$fig

#!/bin/bash
#
#/work09/am/00.WORK/2022.ECS2022/0.ECS2022.06_SUMMARY_2023-02-21/04.SOUNDING/22.12.SONDE/22.14.STN/00.12.OBS_POINT
#
. ./gmtpar.sh
gmtset ANOT_FONT_SIZE 12
gmtset HEADER_FONT_SIZE 16;gmtset HEADER_OFFSET 0.05

range1=120/141/19/34; range1P=121/140/20/34
range2=127.9/129.5/30/31.3
size=JM4
xanot=a30mf10m; yanot=a30mf10m
anot=${xanot}/${yanot}WSne

in=2022.06.ECS.OBSP.TXT
if [ ! -f $in ]; then echo Error in $0 : No such file, $in; exit 1; fi

fig=$(basename $0 .sh)_$(basename $in .TXT).ps


echo MMMMM LARGE MAP
DSET=
YMD=20220619; DATE=19JUN2022
REGION=ECS_PS
INDIR=OUT_MGDSST2GMT #OUT_$(basename $0 .sh);mkd $ODIR
INFLE=MGDSST_${REGION}_D${YMD}_GMT.TXT
IN=$INDIR/$INFLE
if [ ! -f $IN ]; then echo Error in $0 : No such file, $IN; exit 1; fi

CPT=MGDSST_CPT.TXT; rm -vf $CPT
if [ ! -f $CPT ]; then makecpt -Cjet -T18/31/0.5 -Z >$CPT; fi

GRD=$(basename $0 .sh)_GRD.nc
awk '{if($1!="#")print $1,$2,$3}' $IN|\
surface -R$range1 -I0.02/0.02 -T0.8 -G$GRD

grdimage $GRD -R$range1P -$size  -C$CPT  -K \
-K -X1 -Y3 >$fig
grdcontour $GRD -R$rangeP -$size -W2 -C1 -O -K >>$fig
grdcontour $GRD -R$rangeP -$size -W2 -L23.9/24.1 -G2/2 -A1f10 -O -K >>$fig
grdcontour $GRD -R$rangeP -$size -W2 -L25.9/26.1 -G2/2 -A1f10 -O -K >>$fig
rm -vf $GRD

pscoast -R$range1P -$size -G0 -Di \
-Ba5f1/a5f1:.${DSET}${sp}${DATE}:WsNe \
-O -K >>$fig

psxy <<EOF -R -$size -W6/255/255/255 -A -O -K >>$fig
128 30.15
129.34 30.15
129.34 31.15
128 31.15
128 30.15
EOF
psxy <<EOF -R -$size -W5 -A -O -K >>$fig
128 30.15
129.34 30.15
129.34 31.15
128 31.15
128 30.15
EOF

. ./MAP_OBSP_SST_LS_RADAR_SITE.sh

psscale -D2/-0.2/3/0.1h -B2f1::/:"@+o@+C": -C$CPT -O -K >>$fig

echo MMMMM CLOSE UP
pscoast -R$range2 -$size -W2 -Dh \
-L129.1/30.1/30.1/30k \
-O -K -Y-0.3 -X5 >>$fig

awk '{if($1!="#"&&$4=="N")print $2, $1}' $in |\
psxy -R$range -$size -Sc0.06 -G0 -O -K >> $fig

awk '{if($1!="#"&&$4=="K")print $2, $1}' $in |\
psxy -R$range -$size -St0.07 -G0 -O -K >> $fig

awk '{if($1!="#"&&$4=="S")print $2, $1}' $in |\
psxy -R$range -$size -Ss0.07 -G0  -O -K >> $fig

awk '{if($1!="#")print $2,$1," 9 0 1 LM ",$4$5}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig

psxy <<EOF -R -$size -W3 -A -O -K >>$fig
128 30.15
129.34 30.15
129.34 31.15
128 31.15
128 30.15
EOF
psbasemap -R$range -$size -B$anot \
-O -K >>$fig



xoffset=-5; yoffset=5

export LANG=C

curdir1=$(pwd); now=$(date); host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${fig} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $fig
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${in} (${time})
0 0.90  9 0 1 LM OUTPUT: ${fig} (${timeo})
EOF

PDF=$(basename $fig .ps).pdf
ps2pdfwr $fig $PDF
if [ $? -eq 0 ];then rm -vf $fig; fi
echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $PDF
echo

echo "Done $0"

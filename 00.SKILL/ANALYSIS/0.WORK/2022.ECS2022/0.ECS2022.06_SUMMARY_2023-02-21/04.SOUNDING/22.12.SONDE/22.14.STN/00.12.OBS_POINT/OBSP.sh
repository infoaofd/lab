#!/bin/bash
# Description:
# Host: p5820.bio.mie-u.ac.jp
# Directory: /work03/am/2022.06.ECS.OBS/12.22.SONDE_3SHIP/12.12.OBS_POINT
#
# Revision history:
#  This file is created by /work03/am/mybin/nbscr.sh at 19:49 on 01-17-2023.

echo "Shell script, $(basename $0) starts."

INDIR1=/work01/DATA/IN_SITU/2022.06.ECS.SONDE/NAGASAKI/N
INDIR2=/work01/DATA/IN_SITU/2022.06.ECS.SONDE/KAGOSHIMA/01_KZ
INDIR3=/work01/DATA/IN_SITU/2022.06.ECS.SONDE/SEISUI/S

if [ ! -d $INDIR1 ];then echo ERROR: NO SUCH DIR, $INDIR1;exit 1;fi
if [ ! -d $INDIR2 ];then echo ERROR: NO SUCH DIR, $INDIR2;exit 1;fi
if [ ! -d $INDIR3 ];then echo ERROR: NO SUCH DIR, $INDIR3;exit 1;fi

INDLIST="$INDIR1 $INDIR2 $INDIR3"
OFLE=2022.06.ECS.OBSP.TXT
CMD="$0 $@"; CWD=$(pwd); NOW=$(date -R)
echo "# $CWD" > $OFLE
echo "# $CMD" >> $OFLE
echo "# $NOW" >> $OFLE

echo "# N $INDIR1" >> $OFLE
echo "# K $INDIR2" >> $OFLE
echo "# S $INDIR3" >> $OFLE

for INDIR in $INDLIST; do
echo MMMMMMMMMM; echo MMMMMMMMMM $INDIR
INLIST=$(ls $INDIR/*.CSV)

STN=0
for INFLE in $INLIST; do
LINE=$(awk -F, '{if(NR==1) print $5, $6, $10, $11}' $INFLE)
LAT=$(awk -F, '{if(NR==1) print $10}' $INFLE)
LON=$(awk -F, '{if(NR==1) print $11}' $INFLE)
DATE=$(awk -F, '{if(NR==1) print $5}' $INFLE)
TIME=$(awk -F, '{if(NR==1) print $6}' $INFLE)

echo $INFLE
DTIME=${DATE}T${TIME}
TIMEO=$(echo $DTIME | sed -e "s/\//-/g")
if [ $INDIR = $INDIR1 ]; then SHIP=N; fi
if [ $INDIR = $INDIR2 ]; then SHIP=K; fi
if [ $INDIR = $INDIR3 ]; then SHIP=S; fi

STN=$(expr $STN + 1)
echo "$LAT $LON $TIMEO $SHIP ${STN} $INFLE" >> $OFLE
done # INFLE

done #INDIR

echo "OUTPUT: $OFLE"
echo





#
# The following lines are samples:
#

# Sample for handling options

# CMDNAME=$0
# Ref. https://sites.google.com/site/infoaofd/Home/computer/unix-linux/script/tips-on-bash-script/hikisuuwoshorisuru
#while getopts t: OPT; do
#  case  in
#    "t" ) flagt="true" ; value_t="" ;;
#     * ) echo "Usage nbscr.sh [-t VALUE] [file name]" 1>&2
#  esac
#done
#
#value_t=NOT_AVAILABLE
#
#if [  = "foo" ]; then
#  type="foo"
#elif [  = "boo" ]; then
#  type="boo"
#else
#  type=""
#fi
#shift 0




# Sample for checking arguments

#if [ $# -lt 1 ]; then
#  echo Error in $0 : No arugment
#  echo Usage: $0 arg
#  exit 1
#fi

#if [ $# -lt 2 ]; then
#  echo Error in $0 : Wrong number of arugments
#  echo Usage: $0 arg1 arg2
#  exit 1
#fi



# Sample for checking input file

#in=""
#if [ ! -f $in ]; then
#  echo Error in $0 : No such file, $in
#  exit 1
#fi



# Sample for creating directory

#dir=""
#if [ ! -d $dir ]; then
#  mkdir -p ${dir}
#  echo Directory, ${dir} created.
#fi

#out=$(basename $in .asc).ps

#echo Input : $in
#echo Output : $out



# Sample of do-while loop

#i=1
#n=3
#while [ $i -le $n ]; do
#  i=$(expr $i + 1)
#done



# Sample of for loop

# list_item="a b c"
#for item in list_item
#do
#  echo $item
#done



# Sample of if block

#i=3
#if [ $i -le 5 ]; then
#
#else if  [ $i -le 2 ]; then
#
#else
#
#fi

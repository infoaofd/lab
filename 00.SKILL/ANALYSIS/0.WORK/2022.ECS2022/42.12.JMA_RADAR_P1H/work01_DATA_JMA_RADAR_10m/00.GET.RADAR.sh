ys=2022; ye=2022
ms=6; me=6
ds=18; de=19

y=$ys
while [ $y -le $ye ]; do

m=$ms
while [ $m -le $me ]; do

mm=$(printf "%02d" $m)

d=$ds
while [ $d -le $de ]; do

dd=$(printf "%02d" $d)

wget -r -l 1 http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/jma-radar/synthetic/original/${y}/${mm}/${dd}/

d=$(expr $d + 1)
done #d

m=$(expr $m + 1)
done #m

y=$(expr $y + 1)
done #y

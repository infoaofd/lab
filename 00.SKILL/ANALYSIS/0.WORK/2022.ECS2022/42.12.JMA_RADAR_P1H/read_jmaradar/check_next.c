#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "grib2.h"

void check_next(int cur_pos,int *n, unsigned char *data, int length, 
		short VMAX){

  if(data[cur_pos+1] <= VMAX){
    return;
  }

  *n = *n + 1;

  if(cur_pos + *n > length){
    return;
  }

  check_next(cur_pos+1,n,data,length,VMAX);    


}

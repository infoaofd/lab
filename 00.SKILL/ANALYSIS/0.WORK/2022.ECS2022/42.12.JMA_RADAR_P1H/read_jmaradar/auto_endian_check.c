#include<stdio.h>

int auto_endian_check(){

  
  union{ int i; char c[4]; } u;  
  /* endian check */
  u.i = 1;
  if( u.c[0] == 1 ){ /* little endian */
    return 1;
  }
  else{ /* big endian */
    return 0;
  }  
  
}

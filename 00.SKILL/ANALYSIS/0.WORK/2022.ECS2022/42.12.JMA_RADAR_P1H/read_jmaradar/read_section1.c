/*
 * Convert JMA-MSM GPV to NetCDF based on COARDS convention.
 * by Takeshi Maesaka <maesaka@bosai.go.jp>
 *
 * $Log: read_section1.c,v $
 * Revision 1.1.1.1  2006/03/01 10:09:22  msaka
 *
 *
 */
 
#include <stdio.h>
#include "grib2.h"

int read_section1( FILE *fp, struct grib2_section1 *s1 )
{
  
  unsigned char buf[21];
  
  if( fread( buf, 1, 21, fp ) != 21 )
    return(0);

  if( buf[4] != 1 ) /* section number */
    return(0);

  if( swapping( buf+5, it_short ) != 34 ) /* Organization: Tokyo */
    return(0);
  
  s1->iyear = swapping( buf+12, it_short );
  s1->imon  = buf[14];
  s1->iday  = buf[15];
  s1->ihour = buf[16];
  s1->imin  = buf[17];
  s1->isec  = buf[18];

  printf("year %d \n",s1->iyear);
  printf("mon  %d \n",s1->imon);
  printf("day %d \n",s1->iday);
  printf("hour %d \n",s1->ihour);
  printf("min %d \n",s1->imin);
  
  return(1);

}

program ten2one
! Description:
!
! Host: aofd165.fish.nagasaki-u.ac.jp
! Directory: /work2/kunoki/to_manda_sensei/Tools/JMA_radar/10min.to.1hr
!
! ifortの場合以下のようにオプション-assume bytereclを付けてコンパイルすること。 
! $ ifort -assume byterecl hoge.f90
! こうしないと、"recl"がbyte単位ではなく、word単位になってしまう
!
!  use
!  implicit none
  real,allocatable:: rr(:,:),rr1(:,:)
  integer im,jm
  character(len=500)::input0, input1, input2, input3, input4, input5
  character(len=500)::output

  namelist /para/im,jm,input0, input1, input2, input3, input4, input5, output

  read(*,nml=para)

  print '(A,A)','Input: ',trim(input0)
  print '(A,A)','Input: ',trim(input1)
  print '(A,A)','Input: ',trim(input2)
  print '(A,A)','Input: ',trim(input3)
  print '(A,A)','Input: ',trim(input4)
  print '(A,A)','Input: ',trim(input5)
  print '(A,A)','Output: ',trim(output)
  print *

  allocate(rr(im,jm),rr1(im,jm))

  rr(:,:)=0.0
  open(10,file=input0,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  open(10,file=input1,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  open(10,file=input2,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  open(10,file=input3,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  open(10,file=input4,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  open(10,file=input5,form="unformatted",access="direct",recl=4*im*jm,action="read")
  read(10,rec=1)rr1
  rr(:,:)=rr(:,:)+rr1(:,:)
  close(10)

  rr(:,:)=rr(:,:)/6.0

  open(10,file=output,form="unformatted",access="direct",recl=4*im*jm)
  write(10,rec=1)rr
  close(10)
 
  
end program ten2one

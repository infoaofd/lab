#!/bin/bash
# /work03/am/2022.06.ECS.OBS/32.00.RADAR_PRECIP/24.02.JMA_RADAR_P1H
#
#
gs=$(basename $0 .sh).gs

yyyy=2022; mm=06; dd=16;   dp=$(expr $dd + 1)

hs=00; dh=1
UNIT=[mm/h]

figfile=$(basename $0 .sh)_${yyyy}${mm}${dd}.pdf #.eps


ctl=$(basename $0 .sh).CTL
cat <<EOF>$ctl
# $(date -R)
# $0
dset output/%y4%m2%d2_%h2.bin
OPTIONS TEMPLATE
undef -999.9
xdef 2560 LINEAR 118.006250 0.012500
ydef 3360 LINEAR 20.004167 0.008333
zdef 1  LEVELS 1000
tdef 1000  LINEAR 00:00Z01Jun2022 1hr
vars 1
rr 0 0 rainfall
endvars
EOF



HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

lonw=126; lats=29; lone=132; latn=33 #34.5
#lonw=128; lats=29; lone=132; latn=32 #34.5

cat <<EOF>$gs
'cc'

'open ${ctl}'


yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif



'set lon '${lonw}' '${lone}; 'set lat '${lats}' '${latn}
'set mpdset 'hires

kind='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta->magenta'
clevs='4 80 4'



xmax = 6; ymax = 4

xwid = 8.5/xmax; ywid = 5.5/ymax

nmap = 1; ymap = 1

hh=${hs}

while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

'set grid off'


datetime1=hh'Z'${dd}''mmm''yyyy

'set time 'datetime1
'q dims'; line=sublin(result,5)
dtcheck=subwrd(line,6); t1=subwrd(line,9)
say dtcheck' 't1

hp=hh+1

xs = 0.5 + (xwid+0.4)*(xmap-1); xe = xs + xwid
ye = 7.5 - (ywid+0.20)*(ymap-1); ys = ye - ywid

#if (ymap = ymax)
'set xlopts 1 1 0.09'
#else
#'set xlopts 1 2 0.0'
'set xlopts 1 1 0.09'
#endif
#if (xmap = 1)
'set ylopts 1 1 0.09'
#else
#'set ylopts 1 2 0.0'
#endif

'set vpage 0.0 11.0 0.0 8.5'
'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'
'set font 4'
xlevs='126 128 130' ;# 132'
'set xlevs 'xlevs
'set ylint 1'

#say 'UNDEF'
#'set gxout grfill'
#'set clevs -1000 -998'
#'set rgb 99 40 40 40'
#'set ccols 0 99 0'
#'d const(rr, -999, -u)'


'color ' clevs ' -gxout shaded -kind ' kind


say 'P1H'
'd 'rr

'04.02.FILL.MISSING.GS'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx = xl+0.0; yy = yt+0.12
if(d < 10);dd='0'd;endif
if(d >  9);dd=d   ;endif
if(hh < 10 & hh > 0);hh='0'hh;endif
if(hh >  9);hh=hh   ;endif
if(hp < 10 & hp > 0);hp='0'hp;endif
if(hp >  9);hp=hp   ;endif


title=hh'-'hp'UTC${dd}'mmm''yyyy
'set string 1 l 1 0'; 'set strsiz 0.08 0.1'
'draw string 'xx' 'yy' 'title

'set parea off'; 'set vpage off'
*
if (nmap > 23); break; endif
nmap = nmap + 1
xmap = xmap + 1

hh=hh+${dh}

endwhile         ;* xmap
ymap = ymap + 1
endwhile         :* ymap

'color ' clevs ' -gxout shaded -kind ' kind ' -xcbar 3 8 1 1.1 -edge circle -line on -fs 2 -ft 2' 
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 8.05 1.05 ${UNIT}';

# Header
'set strsiz 0.1 0.12'; 'set string 1 l 2'
'draw string 0.2 8.4 ${NOW}'; 'draw string 0.2 8.2 ${HOST}'
'draw string 0.2 8.0 ${CWD}'; 'draw string 0.2 7.8 ${CMD}'

'gxprint ${figfile}'

quit
EOF

grads -bcl "${gs}"
rm -vf $gs

if [ -f ${figfile} ]; then
echo; echo FIG: ${figfile};echo
else
echo ; echo ERROR in $0: NO SUCH FILE, ${figfile}.;echo; exit 1
fi

exit 0

#!/bin/bash

MM2MMM(){
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}
#RLONW=129.9 ;RLONE=130.0; RLATS=32.58;RLATN=32.68 # T00.01
RLONW=127 ;RLONE=131; RLATS=30;RLATN=33.5 
YYYYMMDDHH1=${YYYYMMDDHH1:-2021081200}
YYYYMMDDHH2=${YYYYMMDDHH2:-2021081500}

YYYY1=${YYYYMMDDHH1:0:4}; MM=${YYYYMMDDHH1:4:2}; DD1=${YYYYMMDDHH1:6:2}; HH1=${YYYYMMDDHH1:8:2}
MM2MMM 
MM1=$MM; MMM1=$MMM
YYYY2=${YYYYMMDDHH2:0:4}; MM=${YYYYMMDDHH2:4:2}; DD2=${YYYYMMDDHH2:6:2}; HH2=${YYYYMMDDHH2:8:2}
MM2MMM 
MM2=$MM; MMM2=$MMM

TIME1=${HH1}Z${DD1}${MMM1}${YYYY1}; TIME2=${HH2}Z${DD2}${MMM2}${YYYY2}
echo $TIME1 $TIME2
LEV1=0.5; LEV2=3

EXP1=0000; EXP2=0802
EXP1NAME=CNTL; EXP2NAME=CLIM
GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_RW3A.00.04.05.05_01_${EXP1}-${EXP2}.PDF

KIND='darkblue->blue->skyblue->white->gold->red->firebrick'
VAR=MS03; LEVS='-2 2 0.1'; UNIT="[10\`a-3\`ns\`a-1\`n]"; FS=5


PREFIX=42.MAP_TAV_MS03
NCE1U1=${PREFIX}_${EXP1}_U${LEV1}.nc; NCE1U2=${PREFIX}_${EXP1}_U${LEV2}.nc
NCE1V1=${PREFIX}_${EXP1}_V${LEV1}.nc; NCE1V2=${PREFIX}_${EXP1}_V${LEV2}.nc

NCE2U1=${PREFIX}_${EXP2}_U${LEV1}.nc; NCE2U2=${PREFIX}_${EXP2}_U${LEV2}.nc
NCE2V1=${PREFIX}_${EXP2}_V${LEV1}.nc; NCE2V2=${PREFIX}_${EXP2}_V${LEV2}.nc

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'cc';'set grid off';'set grads off'
'set vpage 0.0 8.5 0.0 11'

say;say '01 ${NCE1U1}';'sdfopen ${NCE1U1}'
say;say '02 ${NCE1U2}';'sdfopen ${NCE1U2}'
say;say '03 ${NCE1V1}';'sdfopen ${NCE1V1}'
say;say '04 ${NCE1V2}';'sdfopen ${NCE1V2}'
say;say '05 ${NCE2U1}';'sdfopen ${NCE2U1}'
say;say '06 ${NCE2U2}';'sdfopen ${NCE2U2}'
say;say '07 ${NCE2V1}';'sdfopen ${NCE2V1}'
say;say '08 ${NCE2V2}';'sdfopen ${NCE2V2}'
say

UE1L1=UAV1.1; UE1L2=UAV2.2
VE1L1=VAV1.3; VE1L2=VAV2.4

UE2L1=UAV1.5; UE1L2=UAV2.6
VE2L1=VAV1.7; VE1L2=VAV2.8

say; say 'MMMMM MS03'
'rdz=1.0/( (${LEV2}-${LEV1})*1000 ) '
'USE1=(UAV1.1-UAV2.2)*rdz*1000'
'VSE1=(VAV1.3-VAV2.4)*rdz*1000'
'MSE1=mag(USE1,VSE1)'

'USE2=(UAV1.5-UAV2.6)*rdz*1000'
'VSE2=(VAV1.7-VAV2.8)*rdz*1000'
'MSE2=mag(USE2,VSE2)'


xmax = 1; ymax = 1
ytop=9
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set xlopts 1 3 0.15'; 'set ylopts 1 3 0.15'
'set parea 'xs ' 'xe' 'ys' 'ye
'set ylint 1'; 'set xlint 1'

'set mpdraw on'; 'set mpdset worldmap'; 'set rgb 99 66 19 0';'set map 99 1 3'
'set xlint 1'; 'set ylint 1'
'color ${LEVS} -kind ${KIND} -gxout shaded'
#'d maskout(TAV1,xland-1.5)'
'd MSE1-MSE2'

'set xlab off';'set ylab off'

'set xlab off';'set ylab off'
'set gxout contour';'set cthick 6';'set ccolor 0';'set cint 1';'set clab off'
'd MSE2'
'set gxout contour';'set cthick 3';'set ccolor 1';'set cint 1';'set clab on'
'set clopts 1 4 0.13';'set clskip 2'
'd MSE2'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl-0.5; x2=xr-0.2; y1=yb-0.7; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs $FS -ft 4 -fw 0.15 -fh 0.17' 
'set strsiz 0.15 0.17'; 'set string 1 l 4'
x=x2+0.05; y=(y1+y2)/2;'draw string 'x' 'y ' ${UNIT}' 

x1=xr+0.9; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs $FS -ft 4 -fw 0.15 -fh 0.17' 
'set strsiz 0.15 0.17'; 'set string 1 c 4'
x=(x1+x2)/2*1.05; y=y2+0.2;'draw string 'x' 'y ' ${UNIT}' 

'set strsiz 0.18 0.2'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=y+0.3
'draw string 'x' 'y' ${VAR} ${EXP1NAME}-${EXP2NAME}' ;#'${RLATS}-${RLATN}N,${RLONW}-${RLONE}E'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=y+0.27; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ];then echo FIG: $FIG; fi

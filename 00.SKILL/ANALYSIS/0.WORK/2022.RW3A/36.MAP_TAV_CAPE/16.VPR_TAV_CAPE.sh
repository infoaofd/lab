#!/bin/bash

MM2MMM(){
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}
#RLONW=129.9 ;RLONE=130.0; RLATS=32.58;RLATN=32.68 # T00.01
RLONW=127 ;RLONE=131; RLATS=30;RLATN=33.5 
YYYYMMDDHH1=${YYYYMMDDHH1:-2021081200}
YYYYMMDDHH2=${YYYYMMDDHH2:-2021081500}

YYYY1=${YYYYMMDDHH1:0:4}; MM=${YYYYMMDDHH1:4:2}; DD1=${YYYYMMDDHH1:6:2}; HH1=${YYYYMMDDHH1:8:2}
MM2MMM 
MM1=$MM; MMM1=$MMM
YYYY2=${YYYYMMDDHH2:0:4}; MM=${YYYYMMDDHH2:4:2}; DD2=${YYYYMMDDHH2:6:2}; HH2=${YYYYMMDDHH2:8:2}
MM2MMM 
MM2=$MM; MMM2=$MMM

TIME1=${HH1}Z${DD1}${MMM1}${YYYY1}; TIME2=${HH2}Z${DD2}${MMM2}${YYYY2}
echo $TIME1 $TIME2
LEV=0.5

EXP=$1; EXP=${EXP:-0000}
INDIR=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
cp -av $CTL .
CTL1=$CTL;EXP1=$EXP
if [ $EXP = "0000" ];then EXP1NAME=CNTL;fi
if [ $EXP = "0802" ];then EXP1NAME=CLIM;fi

#EXP=0802
#INDIR=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/RW3A.00.04.05.05.${EXP}.01
#CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
#if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
#CTL2=$CTL;EXP2=$EXP; EXP2NAME=CLIM

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_RW3A.00.04.05.05.${EXP1}.01_${RLONW}_${RLATS}_${LEV}.PDF

KIND='white->lightcyan->deepskyblue->yellow->orange->red->purple'
LEVS='100 1000 100'
VAR=CAPE; UNIT=[J/kg]; FS=2

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

say;'open ${CTL1}';say;#'open ${CTL2}';say

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'set lev ${LEV1} ${LEV2}'
'set lon ${RLONW} ${RLONE}';'set lat ${RLATS} ${RLATN}'
'set time ${TIME1}'
say 'MMMMM TAV';'!date -R'
'TAV1=ave(cape.1,time=${TIME1},time=${TIME2})';'!date -R'
say 'MMMMM TAV';'!date -R'
'TAV2=ave(cape.2,time=${TIME1},time=${TIME2})';'!date -R'

'set lon ${RLONW}';'set lat ${RLATS}'
say 'MMMMM AAV'
'AAV1=aave(TAV1,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN})'
say 'MMMMM AAV'
'AAV2=aave(TAV2,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN})'

'set xlopts 1 3 0.15'; 'set ylopts 1 3 0.15'
'set parea 'xs ' 'xe' 'ys' 'ye
'set ylint 1'; 'set xlint 1'

'set gxout line'
'set xlopts 1 3 0.15'; 'set ylopts 1 3 0.15'
'set vrange $TMIN $TMAX'
'set ccolor 9'; 'set cthick 6'; 'set cstyle 1'; 'set cmark 0'
'd AAVE2'

'set xlab off';'set ylab off'

'set vrange $TMIN $TMAX'
'set ccolor 3'; 'set cthick 6'; 'set cstyle 1'; 'set cmark 0'
'd AAVE1'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl-1; x2=xr; y1=yb-0.7; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs $FS -ft 4 -fw 0.15 -fh 0.17' 
'set strsiz 0.15 0.17'; 'set string 1 l 4'
x=x2+0.05; y=(y1+y2)/2;'draw string 'x' 'y ' ${UNIT}' 

x1=xr+0.9; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2' -edge circle -line on -fs 1 -ft 4 -fw 0.15 -fh 0.17' 
'set strsiz 0.15 0.17'; 'set string 1 c 4'
x=(x1+x2)/2*1.05; y=y2+0.2;'draw string 'x' 'y ' ${UNIT}' 

'set strsiz 0.18 0.2'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=y+0.3
'draw string 'x' 'y' ${VAR} ${LEV}km ${EXP1NAME}' ;#'${RLATS}-${RLATN}N,${RLONW}-${RLONE}E'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=y+0.27; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

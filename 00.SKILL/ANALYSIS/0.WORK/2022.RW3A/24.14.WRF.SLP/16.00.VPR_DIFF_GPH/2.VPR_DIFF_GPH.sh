HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2021081200}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}
HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${HH}Z${DD}${MMM}${YYYY}

REGION=WEST; LONW=127; LONE=128; LATS=29.5;  LATN=30.5 #WEST
#REGION=EAST; LONW=129.5; LONE=130.5; LATS=29.5;  LATN=30.5 #EAST

LEV1=1000 ; LEV2=500
TMIN=-6;TMAX=6
UNIT="DIFF GPH [m]"
UNIT2="P [hPa]"

FIG=$(basename $0 .sh)_$TIME0_${REGION}_${LONW}_${LATS}.pdf

INROOT=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/01HR
RUN1=0000; RUN2=0802
CTL1=$INROOT/ARWpost_RW3A.00.04.05.05.${RUN1}.01/RW3A.00.04.05.05.${RUN1}.01.d01.basic_p.01HR.ctl
CTL2=$INROOT/ARWpost_RW3A.00.04.05.05.${RUN2}.01/RW3A.00.04.05.05.${RUN2}.01.d01.basic_p.01HR.ctl
if [ ! -f $CTL1 ]; then echo EEEEE NO SUCH FILE, $CTL1;exit 1;fi
if [ ! -f $CTL2 ]; then echo EEEEE NO SUCH FILE, $CTL2;exit 1;fi

cat <<EOF>$GS
say; 'open $CTL1';say; 'open $CTL2';say

'set lev $LEV1 $LEV2'; 'set time $TIME0'

'cc'; 'set grid off'; 'set grads off'

'set vpage 0.0 8.5 0.0 11'

xs=2; xe=6; ys=3; ye=8
'set parea 'xs ' 'xe' 'ys' 'ye


'set xlab on'; 'set ylab on'
'set xlint 2'; 'set ylint 100'

'set time $TIME0';'set lev ${LEV1} ${LEV2}'
'set lon ${LONW}'; 'set lat ${LATS}'

say 'MMMMM AAV'
'AAVE1=1000*aave(height.1,lon=${LONW},lon=${LONE},lat=${LATS},lat=${LATN})'
say 'MMMMM AAV'
'AAVE2=1000*aave(height.2,lon=${LONW},lon=${LONE},lat=${LATS},lat=${LATN})'

'set gxout line'
'set ccolor 1'; 'set cthick 10'
'set cstyle 1'; 'set cmark 0'
'set vrange $TMIN $TMAX'
'set xlopts 1 3 0.15'; 'set ylopts 1 3 0.15'

'd AAVE1-AAVE2'
'trackplot 0 500 0 1000 -c 1 -l 3 -t 1'


'set xlab off'; 'set ylab off'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

say '###  UNIT'
'set strsiz 0.12 0.16'; 'set string 1 c 4 0'
xx=(xl+xr)/2; yy=yb-0.5
'draw string ' xx ' ' yy ' ${UNIT}'

'set strsiz 0.12 0.16'
'set string 1 c 4 90'
yy=(yb+yt)/2; xx=xl-0.8
'draw string ' xx ' ' yy ' ${UNIT2}'

say '### TITLE'
'set strsiz 0.12 0.14'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=yt+0.25; 'draw string 'x' 'y' $TIME0 DIFF GPH ${REGION}'
y=y+0.25; 'draw string 'x' 'y' ${LATS}-${LATN}N,${LONW}-${LONE}'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG




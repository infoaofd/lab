ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2021081200}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}
HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${HH}Z${DD}${MMM}${YYYY}; TIME1=01Z12AUG2021

FIG=$(basename $0 .sh)_${YYYYMMDDHH}.pdf

LONW=126; LONE=131.5; LATS=29;  LATN=33
WREGION=WEST; LONW2=127; LONE2=128; LATS2=29.5;  LATN2=30.5 #WEST
EREGION=EAST; LONW3=129.5; LONE3=130.5; LATS3=29.5;  LATN3=30.5 #EAST
CREGION=CENTER; LONW4=128.25;LONE4=129.25;LATS4=29.5;LATN4=30.5

press1=1010;press2=1012

UNIT="m/s"

KIND='blue->dodgerblue->skyblue->white->yellow->red->magenta'
LEVS='-8 8 1'
INROOT=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj
RUN1=0000; RUN2=0802
CTL1=$INROOT/RW3A.00.04.05.05.${RUN1}.01/RW3A.00.04.05.05.${RUN1}.01.d01.traj.10MN.ctl
CTL2=$INROOT/RW3A.00.04.05.05.${RUN2}.01/RW3A.00.04.05.05.${RUN2}.01.d01.traj.10MN.ctl
if [ ! -f $CTL1 ]; then echo EEEEE NO SUCH FILE, $CTL1;exit 1;fi
if [ ! -f $CTL2 ]; then echo EEEEE NO SUCH FILE, $CTL2;exit 1;fi

cat <<EOF>$GS
say; 'open $CTL1';say; 'open $CTL2';say

#'q ctlinfo'; say result

'set lon $LONW $LONE'; 'set lat $LATS $LATN'

'set time $TIME0'
#'q dims'; say result

'cc';'set grads off';'set grid off'

'set vpage 0.0 11.0 0.0 8.5'

xmax = 1; ymax = 1
xwid = 6/xmax; ywid = 6/ymax
xmap=1; nmap = 1; ymap = 1
xleft=0.7; ytop=9.5
xs = xleft + (xwid+0.4)*(xmap-1); xe = xs + xwid 
ye = ytop - (ywid+0.4)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'set time $TIME0'; 'set z 1'
say 'MMMMM TAV U10'
'U1TAV=ave(U10.1,time=${TIME0},time=${TIME1})'
say 'MMMMM TAV V10'
'V1TAV=ave(V10.1,time=${TIME0},time=${TIME1})'
say 'MMMMM TAV U10'
'U2TAV=ave(U10.2,time=${TIME0},time=${TIME1})'
say 'MMMMM TAV V10'
'V2TAV=ave(V10.2,time=${TIME0},time=${TIME1})'

'set mpdraw on';'set mpdset hires'
'set rgb 50 127 73 45' ;# red : 127 Green : 73 Blue : 45
'set map 50 1 5'

'set xlab on'; 'set ylab on'
'set xlopts  1 3 0.18'; 'set ylopts  1 3 0.18'
'set ylint 1'; 'set xlint 1'

'color ${LEVS} -kind ${KIND} -gxout shaded'
'd maskout(V1TAV-V2TAV,xland-1.5)'

'set cthick 10'; 'set ccolor  0'
'vec.gs skip(U1TAV,50,50);V1TAV -SCL 0.7 30 -P 20 20 -SL m/s'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.8; yy=yb-0.45
'set cthick  5'; 'set ccolor  1'
'set strsiz 0.15 0.18';'set string 1 c 4'
'vec.gs skip(U1TAV,50,50);V1TAV -SCL 0.7 30  -P 'xx' 'yy' -SL m/s'


say 'MMMMM TAV SLP'
'SLPS1=ave(slp.1,time=${TIME0},time=${TIME1})'
say 'MMMMM TAV SLP'
'SLPS2=ave(slp.2,time=${TIME0},time=${TIME1})'

'set z 1';'set gxout contour'
'set cmax 1030'; 'set cmin 800'
'set cint 1'; 'set clskip 2'
'set clopts 3 2 0.13'; 'set ccolor 3'; 'set cthick 3'
'd maskout(SLPS1,xland-1.5)'

'set gxout contour'
'set clevs ${press1} ${press2}'
'set clopts 3 4 0.13'
'set ccolor 3'; 'set cthick 8'
'd maskout(SLPS1,xland-1.5)'



'set gxout contour'
'set cmax 1030'; 'set cmin 800'; 'set cint 1'
'set clskip 2'
'set clopts 9 4 0.13'
'set ccolor 9'; 'set cthick 3'
'd maskout(SLPS2,xland-1.5)'

'set clevs ${press1} ${press2}'
'set ccolor 9'; 'set cthick 8'
'd maskout(SLPS2,xland-1.5)'



say 'MMMMM COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1; y1=yb-1; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.15 -fh 0.18 -fs 2 -ft 5 -line on -edge square -lc 0'
'set strsiz 0.15 0.18'
x=x2+0.5; y=(y1+y2)*0.5;'set string 1 c 3 0'; 
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.5; x2=x1+0.1; y1=yb; y2=yt-0.3
'xcbar 'x1' 'x2' 'y1' 'y2' -fw 0.15 -fh 0.18 -fs 2 -ft 5 -line on -edge square -lc 0' 
'set strsiz 0.15 0.17'; 'set string 1 c 4'
x=(x1+x2)/2*1.05; y=y2+0.2;'draw string 'x' 'y ' ${UNIT}' 

say 'MMMMM $WREGION'
'trackplot ${LONW2} ${LATN2} ${LONW2} ${LATS2} -c 1  -l 2 -t 10'
'trackplot ${LONW2} ${LATN2} ${LONE2} ${LATN2} -c 1  -l 2 -t 10'
'trackplot ${LONE2} ${LATS2} ${LONE2} ${LATN2} -c 1  -l 2 -t 10'
'trackplot ${LONE2} ${LATS2} ${LONW2} ${LATS2} -c 1  -l 2 -t 10'

say 'MMMMM $EREGION'
'trackplot ${LONW3} ${LATN3} ${LONW3} ${LATS3} -c 1  -l 1 -t 10'
'trackplot ${LONW3} ${LATN3} ${LONE3} ${LATN3} -c 1  -l 1 -t 10'
'trackplot ${LONE3} ${LATS3} ${LONE3} ${LATN3} -c 1  -l 1 -t 10'
'trackplot ${LONE3} ${LATS3} ${LONW3} ${LATS3} -c 1  -l 1 -t 10'

say 'MMMMM $CREGION'
'trackplot ${LONW4} ${LATN4} ${LONW4} ${LATS4} -c 1  -l 3 -t 6'
'trackplot ${LONW4} ${LATN4} ${LONE4} ${LATN4} -c 1  -l 3 -t 6'
'trackplot ${LONE4} ${LATS4} ${LONE4} ${LATN4} -c 1  -l 3 -t 6'
'trackplot ${LONE4} ${LATS4} ${LONW4} ${LATS4} -c 1  -l 3 -t 6'

say 'MMMMM TITLE'
#'q dims'; say result

'set strsiz 0.2 0.22'; 'set string 1 c 5 0'
x=(xl+xr)/2; y=yt+0.25; 'draw string 'x' 'y' SLP '
'set strsiz 0.18 0.2'; 'set string 1 c 4 0'
x=(xl+xr)/2; y=y+0.3; 'draw string 'x' 'y' ${TIME0}-${TIME1}'


say 'MMMMM HEADER'
'set strsiz 0.12 0.14'; 'set string 1 l 2 0'
xx = 0.2; yy=y+0.4; 'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'

'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG




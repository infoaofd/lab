# coding: utf-8
#! /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
import netCDF4  
import numpy as np
import matplotlib.pyplot as plt
import os
from scipy.stats import skew

# 欠損値や異常値を処理する関数
def read_and_clean_nc(filepath, var_name, tol=1e-3, extreme_value=1e20):
    with netCDF4.Dataset(filepath, "r") as nc_file:
        var = nc_file.variables[var_name]
        data = var[:]
        fill_value = var.getncattr("_FillValue")
        
        # FillValueのマスキング
        mask_fill_value = np.abs(data - fill_value) < tol
        data[mask_fill_value] = np.nan

        # 極端な外れ値をNaNに置き換える
        mask_extreme_values = np.abs(data) > extreme_value
        data[mask_extreme_values] = np.nan

        # NaNを削除し、平坦化（1次元化）
        cleaned_data = data[~np.isnan(data)].flatten()
        
        # データ検証
        print(f"File: {filepath}")
        print(f"Variable: {var_name}")
        print(f"Number of valid data points: {len(cleaned_data)}")
        print(f"Last 10 values: {cleaned_data[-10:-1]}")
        print("")
        
        return cleaned_data

# メイン処理
PERIOD = "2021-08-11_00:00 2021-08-12_00:00"
VAR = "W (0-1 km)"       
ALONW = 128.0
ALONE = 129.0
ALATS = 25.0
ALATN = 32.0

INROOT = "/work04/manda/RW3A.ARWpost/10MN/W_HISTO/"

RUN1 = "0000"
RUN2 = "0802"

INDIR1 = INROOT + "RW3A.00.04.05.05." + RUN1 + ".01/"
INFLE1 = "RW3A.00.04.05.05." + RUN1 + ".01.d01.traj.10MN_WOUT.nc"
IN1 = INDIR1 + INFLE1

INDIR2 = INROOT + "RW3A.00.04.05.05." + RUN2 + ".01/"
INFLE2 = "RW3A.00.04.05.05." + RUN2 + ".01.d01.traj.10MN_WOUT.nc"
IN2 = INDIR2 + INFLE2

DOMAIN = f"{ALATS}-{ALATN}N {ALONW}-{ALONE}E"

bmin = -2
bmax = 2
bin_width = 0.05
num_bin_real = (bmax - bmin) / bin_width
print(f"MMMMM num_bin_real= {num_bin_real}")
num_bins = int(np.floor(num_bin_real))  # より適切な切り捨て方法
print(f"MMMMM bmin={bmin} bmax={bmax}")
print(f"MMMMM bin_width={bin_width} num_bins={num_bins}")

# 入力ファイル1の読み込みとクリーニング
b1 = read_and_clean_nc(IN1, "WOUT")

# 入力ファイル2の読み込みとクリーニング
b2 = read_and_clean_nc(IN2, "WOUT")

# ヒストグラム作成
weights2 = np.ones_like(b2) / b2.size * 100
plt.hist(b2, bins=num_bins, weights=weights2, range=(bmin, bmax), color='blue', alpha=0.8, edgecolor="gray")

weights1 = np.ones_like(b1) / b1.size * 100
plt.hist(b1, bins=num_bins, weights=weights1, range=(bmin, bmax), color='white', alpha=0.8, edgecolor="red")

# 平均、標準偏差、歪度、中央値の計算
mean_b1, std_b1, skew_b1, median_b1 = np.mean(b1), np.std(b1), skew(b1), np.median(b1)
mean_b2, std_b2, skew_b2, median_b2 = np.mean(b2), np.std(b2), skew(b2), np.median(b2)

# Bootstrapによる信頼区間
# Run1の信頼区間 (4.58, 5.16)
# Run2の信頼区間 (3.82, 4.40)
ci_b1 = (4.58, 5.16)
ci_b2 = (3.82, 4.40)

# グラフに統計情報を追加
plt.text(0.05, 0.95, f"CNTL Mean: {mean_b1:.2f}\nMedian: {median_b1:.2f}\nStd: {std_b1:.2f}\nSkew: {skew_b1:.2f}", 
         transform=plt.gca().transAxes, fontsize=10, color='red', ha='left', va='top', bbox=dict(facecolor='white', alpha=0.7))
plt.text(0.05, 0.75, f"CLIM Mean: {mean_b2:.2f}\nMedian: {median_b2:.2f}\nStd: {std_b2:.2f}\nSkew: {skew_b2:.2f}", 
         transform=plt.gca().transAxes, fontsize=10, color='blue', ha='left', va='top', bbox=dict(facecolor='white', alpha=0.7))

# 信頼区間の表示
plt.text(0.05, 0.65, f"CNTL 95% CI: ({ci_b1[0]:.2f}, {ci_b1[1]:.2f})", 
         transform=plt.gca().transAxes, fontsize=10, color='red', ha='left', va='top', bbox=dict(facecolor='white', alpha=0.7))
plt.text(0.05, 0.55, f"CLIM 95% CI: ({ci_b2[0]:.2f}, {ci_b2[1]:.2f})", 
         transform=plt.gca().transAxes, fontsize=10, color='blue', ha='left', va='top', bbox=dict(facecolor='white', alpha=0.7))

# グラフの設定
plt.xlim(bmin, bmax)  # x軸の範囲
plt.xlabel("W [m/s]")
plt.yscale('log')
plt.ylim(0.01, 50)    # y軸の範囲
plt.ylabel("Relative Frequency [%]")
plt.title(f"{VAR} {DOMAIN}\n\n{PERIOD}\n{RUN1} {RUN2}")

# ファイル名の指定
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = f"{fn_}_{RUN1}_{RUN2}.PDF"
plt.savefig(FIG)
print(f"FIG: {FIG}")

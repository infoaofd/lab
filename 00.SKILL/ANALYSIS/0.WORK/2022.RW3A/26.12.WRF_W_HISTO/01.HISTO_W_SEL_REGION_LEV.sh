#!/bin/bash
# /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/22.12.CONV.STRAT/12.94.BRAUN2010_AAV_N_KYUSHU
# 2024-05-09_20-09
ulimit -s unlimited
LEV=01km
RUNNAME=$1;RUNNAME=${RUNNAME:-0000}
RUNNO=$2;RUNNO=${RUNNO:-01}
EXP=${EXP:-RW3A.00.04.05.05.${RUNNAME}.${RUNNO}}

EXP=${EXP:-RW3A.00.04.05.05.${RUNNAME}.01}
# PERIOD
iy1=2021; im1=8; id1=11; ih1=00; in1=0
#iy2=2021; im2=8; id2=11; ih2=00; in2=10
iy2=2021; im2=8; id2=12; ih2=00; in2=0

slon=128.0;elon=129.;slat=25.0;elat=32.0

DT_FILE=600.0
imax=599; jmax=599; kmax=30; KZMAX=61

# FOR XLONG,XLAT,U,V,WZ
EXP=RW3A.00.04.05.05.${RUNNAME}.01
INROOT=/work04/$(whoami)/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/
INDIR=$INROOT/${EXP}
if [ ! -d $INDIR ];then EEEEE NO SUCH DIR, $INDIR;exit 1;fi
RUN=${EXP}.d01.traj.10MN

ODIR=/work04/$(whoami)/RW3A.ARWpost/10MN/W_HISTO/${EXP}/
mkdir -vp $ODIR

README=$ODIR/0.README_$(basename $0).sh
echo >> $README
echo $(pwd) >> $README
echo $0 >>$README



SRC=$(basename $0 .sh).F90
SUB="$(basename $0 .sh)_DATE.F90 $(basename $0 .sh)_SUB.F90  \
$(basename $0 .sh)_OUT.F90"

# https://atmarkit.itmedia.co.jp/flinux/rensai/linuxtips/248chngext.html
for fname in $SUB; do
OBJ="${OBJ} ${fname%.F90}.o"
done
OBJ="${OBJ} $(basename $SRC .F90).o"

exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

#export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -O2 -fpp -convert big_endian -assume byterecl "

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
#OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "

#TIAMAT
NOPT=" -L/usr/local/netcdf-f-4.5.3/lib -lnetcdff -I/usr/local/netcdf-f-4.5.3/include"
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/netcdf-f-4.5.3/lib
# OpenMP
#OPT2=" -fopenmp "

cat<<EOF>$nml
&para
INDIR="$INDIR",
RUN="$RUN",
ODIR="$ODIR",
iy1=$iy1,
im1=$im1
id1=$id1
ih1=$ih1
in1=$in1
iy2=$iy2,
im2=$im2
id2=$id2
ih2=$ih2
in2=$in2
DT_FILE=$DT_FILE
slon=$slon
elon=$elon
slat=$slat
elat=$elat
imax=$imax, 
jmax=$jmax, 
kmax=$kmax,
KZMAX=$KZMAX
&end
EOF

echo MMMMM Compiling ${SRC} ${SUB}  ...
echo
${f90} ${OPT} ${SUB} -c ${NOPT}
if [ $? -ne 0 ];then echo;echo EEEEE COMPILE ERROR ${SUB};exit 1;fi
${f90} ${OPT}  ${SRC} -c ${NOPT}
if [ $? -ne 0 ];then echo;echo EEEEE COMPILE ERROR ${SRC};exit 1;fi
${f90} ${OPT}  ${OBJ} -o ${exe} ${NOPT}
if [ $? -ne 0 ];then echo;echo EEEEE LINK ERORR ${OBJ};exit 1;fi

rm -vf *.o *.mod
echo; echo MMMMM COMPILE DONE!; echo

echo
echo MMMMM ${exe} is RUNNING ...
echo
D1=$(date -R)
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf $exe $nml
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
D2=$(date -R)
rm -vf $exe $nml

cp -a $0 $ODIR
echo ;echo "START: $D1"; echo "END:  $D2";echo

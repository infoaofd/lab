# ! /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
import netCDF4
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.stats import skew

# 入力ディレクトリとファイルパス
INROOT = "/work04/manda/RW3A.ARWpost/10MN/W_HISTO/"

RUN1 = "0000"
RUN2 = "0802"

INDIR1 = INROOT + "RW3A.00.04.05.05." + RUN1 + ".01/"
INFLE1 = "RW3A.00.04.05.05." + RUN1 + ".01.d01.traj.10MN_WOUT.nc"
IN1 = INDIR1 + INFLE1
print("IN1 : " + IN1)
print("")

INDIR2 = INROOT + "RW3A.00.04.05.05." + RUN2 + ".01/"
INFLE2 = "RW3A.00.04.05.05." + RUN2 + ".01.d01.traj.10MN_WOUT.nc"
IN2 = INDIR2 + INFLE2
print("IN2 : " + IN2)

# データを読み込み、欠損値と異常値を処理する関数
def read_and_clean_nc(filepath, var_name, tol=1e-3, extreme_value=1e20):
    with netCDF4.Dataset(filepath, "r") as nc_file:
        var = nc_file.variables[var_name]
        data = var[:]
        fill_value = var.getncattr("_FillValue")
        
        # FillValueのマスキング
        mask_fill_value = np.abs(data - fill_value) < tol
        data[mask_fill_value] = np.nan

        # 極端な外れ値をNaNに置き換える
        mask_extreme_values = np.abs(data) > extreme_value
        data[mask_extreme_values] = np.nan

        # NaNを削除し、平坦化（1次元化）
        cleaned_data = data[~np.isnan(data)].flatten()
        
        # データ検証
        print(f"File: {filepath}")
        print(f"Variable: {var_name}")
        print(f"Number of valid data points: {len(cleaned_data)}")
        print(f"First 10 valid values: {cleaned_data[:10]}")
        print("")
        
        return cleaned_data

# WOUTデータの読み込みと処理
data1 = read_and_clean_nc(IN1, "WOUT")
data2 = read_and_clean_nc(IN2, "WOUT")

# 平均、中央値、標準偏差、歪度を計算
def calculate_stats(data):
    mean_value = np.mean(data)  # 平均
    median_value = np.median(data)  # 中央値
    std_dev = np.std(data)  # 標準偏差
    skewness = skew(data)   # 歪度
    return mean_value, median_value, std_dev, skewness

mean1, median1, std_dev1, skewness1 = calculate_stats(data1)
mean2, median2, std_dev2, skewness2 = calculate_stats(data2)

# モニターに値を表示
print(f"Dataset 1: Mean = {mean1:.2e}, Median = {median1:.2e}, Std Dev = {std_dev1:.2e}, Skewness = {skewness1:.2f}")
print(f"Dataset 2: Mean = {mean2:.2e}, Median = {median2:.2e}, Std Dev = {std_dev2:.2e}, Skewness = {skewness2:.2f}")
print("")

# データをリストに格納
data = [data1, data2]

# ボックスプロット作成
plt.figure(figsize=(8, 6))

# ボックスプロットの設定
positions = [1, 2]
boxplot = plt.boxplot(
    data,
    positions=positions,
    patch_artist=True,
    boxprops=dict(facecolor="skyblue", color="black"),
    vert=True,  # 縦向きのボックスプロット
    showmeans=True,  # 平均を表示
    showfliers=False  # 外れ値を表示しない
)

# 中央値をプロット（赤い線を引く）
for i, dataset in enumerate(data, start=1):
    median_value = np.median(dataset)
    plt.scatter(
        i,
        median_value,
        color="red",
        s=50,
        zorder=3,
        label="Median" if i == 1 else ""
    )

# グラフ設定
plt.title("Boxplot of WOUT", fontsize=14)
plt.ylabel("WOUT Values", fontsize=12)
plt.xticks(positions, ["Dataset 1", "Dataset 2"])

# 標準偏差、歪度、平均、中央値をテキストとして追加
# y_max1 と y_max2 を微調整
y_max1 = np.max(data1) * 1.2  # データ1の最大値の1.2倍の位置にテキスト
y_max2 = np.max(data2) * 1.2  # データ2の最大値の1.2倍の位置にテキスト

# データ1の統計量
plt.text(1.05, y_max1, f"Mean: {mean1:.2e}\nMedian: {median1:.2e}\nStd Dev: {std_dev1:.2e}\nSkewness: {skewness1:.2f}", color="black", fontsize=12, ha="left", va="bottom")
# データ2の統計量
plt.text(2.05, y_max2, f"Mean: {mean2:.2e}\nMedian: {median2:.2e}\nStd Dev: {std_dev2:.2e}\nSkewness: {skewness2:.2f}", color="black", fontsize=12, ha="left", va="bottom")

# 凡例を追加
plt.legend(loc="upper right")

# 出力ファイル名をスクリプト名から生成
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + "_" + RUN1 + "_" + RUN2 + ".PDF"

# プロットを保存
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

from scipy.stats import ks_2samp

# コルモゴロフ–スミルノフ検定
ks_statistic, p_value = ks_2samp(data1, data2)

# 結果を表示
print(f"Kolmogorov-Smirnov Test Statistic: {ks_statistic:.3f}")
print(f"P-value: {p_value:.3e}")

# 検定結果の解釈
if p_value < 0.05:
    print("The data sets are likely to come from different distributions (reject H0).")
else:
    print("The data sets are likely to come from the same distribution (fail to reject H0).")

# ! /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
import netCDF4
import numpy as np

# 入力ファイルのパス
INFILE = "/work04/manda/RW3A.ARWpost/10MN/W_HISTO/RW3A.00.04.05.05.0000.01/RW3A.00.04.05.05.0000.01.d01.traj.10MN_WOUT.nc"

# ファイルを開き、情報を確認
with netCDF4.Dataset(INFILE, "r") as nc_file:
    var = nc_file.variables["WOUT"]
    
    # データ型とFillValueを確認
    data_type = var.dtype
    fill_value = var.getncattr("_FillValue")
    
    print(f"Data type of 'WOUT': {data_type}")
    print(f"Fill value of 'WOUT': {fill_value}")
    
    # データ内にFillValueが存在するかを確認
    data = var[:]
    fill_value_count = np.sum(data == fill_value)
    print(f"Number of FillValue occurrences in 'WOUT': {fill_value_count}")
    
    # サンプルデータの最初の10個を表示
    print("First 10 values in 'WOUT':")
    print(data[:10])



# coding: utf-8
# ! /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
import netCDF4  
import numpy as np
import matplotlib.pyplot as plt
import os

# 欠損値や異常値を処理する関数
def read_and_clean_nc(filepath, var_name, tol=1e-3, extreme_value=1e20):
    with netCDF4.Dataset(filepath, "r") as nc_file:
        var = nc_file.variables[var_name]
        data = var[:]
        fill_value = var.getncattr("_FillValue")
        
        # FillValueのマスキング
        mask_fill_value = np.abs(data - fill_value) < tol
        data[mask_fill_value] = np.nan

        # 極端な外れ値をNaNに置き換える
        mask_extreme_values = np.abs(data) > extreme_value
        data[mask_extreme_values] = np.nan

        # NaNを削除し、平坦化（1次元化）
        cleaned_data = data[~np.isnan(data)].flatten()
        
        # データ検証
        print(f"File: {filepath}")
        print(f"Variable: {var_name}")
        print(f"Number of valid data points: {len(cleaned_data)}")
        print(f"Last 10 values: {cleaned_data[-10:-1]}")
        print("")
        
        return cleaned_data

# メイン処理
PERIOD = "2021-08-11_00:00 2021-08-12_00:00"
VAR = "W (0-1 km)"       
ALONW = 128.0
ALONE = 129.0
ALATS = 25.0
ALATN = 32.0

INROOT = "/work04/manda/RW3A.ARWpost/10MN/W_HISTO/"

RUN1 = "0000"
RUN2 = "0802"

INDIR1 = INROOT + "RW3A.00.04.05.05." + RUN1 + ".01/"
INFLE1 = "RW3A.00.04.05.05." + RUN1 + ".01.d01.traj.10MN_WOUT.nc"
IN1 = INDIR1 + INFLE1

INDIR2 = INROOT + "RW3A.00.04.05.05." + RUN2 + ".01/"
INFLE2 = "RW3A.00.04.05.05." + RUN2 + ".01.d01.traj.10MN_WOUT.nc"
IN2 = INDIR2 + INFLE2

DOMAIN = f"{ALATS}-{ALATN}N {ALONW}-{ALONE}E"

bmin = -0.5
bmax = 0.5
bin_width = 0.01
num_bin_real = (bmax - bmin) / bin_width
print(f"MMMMM num_bin_real= {num_bin_real}")
num_bins = int(np.floor(num_bin_real))  # より適切な切り捨て方法
print(f"MMMMM bmin={bmin} bmax={bmax}")
print(f"MMMMM bin_width={bin_width} num_bins={num_bins}")

# 入力ファイル1の読み込みとクリーニング
b1 = read_and_clean_nc(IN1, "WOUT")

# 入力ファイル2の読み込みとクリーニング
b2 = read_and_clean_nc(IN2, "WOUT")

# ヒストグラム作成
weights2 = np.ones_like(b2) / b2.size * 100
plt.hist(b2, bins=num_bins, weights=weights2, range=(bmin, bmax), color='blue', alpha=0.8, edgecolor="gray")

weights1 = np.ones_like(b1) / b1.size * 100
plt.hist(b1, bins=num_bins, weights=weights1, range=(bmin, bmax), color='white', alpha=0.8, edgecolor="red")

plt.xlim(bmin, bmax)  # x軸の範囲
plt.xlabel("W [m/s]")
plt.ylabel("Relative Frequency [%]")
plt.title(f"{VAR} {DOMAIN}\n\n{PERIOD}\n{RUN1} {RUN2}")

# ファイル名の指定
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = f"{fn_}_{RUN1}_{RUN2}.PDF"
plt.savefig(FIG)
print(f"FIG: {FIG}")

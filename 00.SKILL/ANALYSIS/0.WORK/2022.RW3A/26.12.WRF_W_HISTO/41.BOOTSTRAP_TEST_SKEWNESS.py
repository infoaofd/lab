# ! /work04/$(whoami)/2022.RW3A/26.12.WRF_W_HISTO/02.01.HISTO_W_SEL_REGION_LEV
import netCDF4
import matplotlib.pyplot as plt
import numpy as np
import os
from scipy.stats import skew

# 入力ディレクトリとファイルパス（既存の部分）
INROOT = "/work04/manda/RW3A.ARWpost/10MN/W_HISTO/"
RUN1 = "0000"
RUN2 = "0802"
INDIR1 = INROOT + "RW3A.00.04.05.05." + RUN1 + ".01/"
INFLE1 = "RW3A.00.04.05.05." + RUN1 + ".01.d01.traj.10MN_WOUT.nc"
IN1 = INDIR1 + INFLE1
INDIR2 = INROOT + "RW3A.00.04.05.05." + RUN2 + ".01/"
INFLE2 = "RW3A.00.04.05.05." + RUN2 + ".01.d01.traj.10MN_WOUT.nc"
IN2 = INDIR2 + INFLE2

# データを読み込み、欠損値と異常値を処理する関数（既存の部分）
def read_and_clean_nc(filepath, var_name, tol=1e-3, extreme_value=1e20):
    with netCDF4.Dataset(filepath, "r") as nc_file:
        var = nc_file.variables[var_name]
        data = var[:]
        fill_value = var.getncattr("_FillValue")
        mask_fill_value = np.abs(data - fill_value) < tol
        data[mask_fill_value] = np.nan
        mask_extreme_values = np.abs(data) > extreme_value
        data[mask_extreme_values] = np.nan
        cleaned_data = data[~np.isnan(data)].flatten()
        return cleaned_data

# データ読み込み
data1 = read_and_clean_nc(IN1, "WOUT")
data2 = read_and_clean_nc(IN2, "WOUT")

# 歪度を計算する関数（既存の部分）
def calculate_stats(data):
    skewness = skew(data)
    return skewness

# ブートストラップ法による信頼区間の計算
def bootstrap_skew(data, num_iterations=10000, alpha=0.05):
    # ブートストラップサンプルを生成し、歪度を計算
    skewness_samples = []
    n = len(data)
    for _ in range(num_iterations):
        sample = np.random.choice(data, size=n, replace=True)
        skewness_samples.append(calculate_stats(sample))
    
    # 信頼区間を計算
    skewness_samples = np.array(skewness_samples)
    lower_bound = np.percentile(skewness_samples, 100 * alpha / 2)
    upper_bound = np.percentile(skewness_samples, 100 * (1 - alpha / 2))
    
    return lower_bound, upper_bound, skewness_samples

# データ1とデータ2の歪度の信頼区間を計算
lower1, upper1, skew_samples1 = bootstrap_skew(data1)
lower2, upper2, skew_samples2 = bootstrap_skew(data2)

# 歪度の推定値を計算
skewness1 = calculate_stats(data1)
skewness2 = calculate_stats(data2)

# 結果を表示
print(f"Dataset 1: Skewness Estimate: {skewness1:.2f}, Confidence Interval: ({lower1:.2f}, {upper1:.2f})")
print(f"Dataset 2: Skewness Estimate: {skewness2:.2f}, Confidence Interval: ({lower2:.2f}, {upper2:.2f})")

# 歪度の差を検定
if (upper1 < lower2) or (upper2 < lower1):
    print("The skewness of the two datasets is significantly different.")
else:
    print("There is no significant difference in skewness between the two datasets.")

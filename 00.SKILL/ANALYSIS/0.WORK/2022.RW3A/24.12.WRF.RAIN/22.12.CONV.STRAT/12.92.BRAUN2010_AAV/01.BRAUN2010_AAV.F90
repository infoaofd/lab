PROGRAM BRAUN2010
! Sat, 04 May 2024 12:01:28 +0900
! /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

integer :: iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2
integer::isec=0
real    :: slon, elon, slat, elat
real :: DT_FILE  ![sec]
real   , parameter :: daysec = 86400.0 ! [sec]
REAL, PARAMETER::UNDEF=1.e30
real(8) :: jd_beg, jd_end, jd
real,allocatable,dimension(:,:)::rlon,rlat,mask
real,allocatable,dimension(:,:)::RAINRNC
real,allocatable,dimension(:,:,:)::W,QCLOUD
real,allocatable,dimension(:,:)::RTYPE,RTYPE1
real,allocatable,dimension(:,:)::RC,RS, RCTSUM, RSTSUM

REAL NAAV !NUMBER OF THE GRIDS FOR AREA AVERAGE

integer::imax, jmax, kmax

CHARACTER(LEN=500)::INDIR0,RUN0,INDIR,RUN, ODIR
CHARACTER(LEN=1000)::OUTTSR, OUTTSUM

namelist /PARA/INDIR0,RUN0,INDIR,RUN,ODIR,iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2,&
DT_FILE,slon, elon, slat, elat,  imax, jmax, kmax, OUTTSR, OUTTSUM

READ(5,NML=PARA)

allocate(rlon(imax,jmax),rlat(imax,jmax),mask(imax,jmax),&
RAINRNC(imax,jmax))
allocate(RTYPE(imax,jmax),RTYPE1(imax,jmax))
allocate(W(imax,jmax,kmax),QCLOUD(imax,jmax,kmax))
allocate(RS(imax,jmax),RC(imax,jmax))
allocate(RSTSUM(imax,jmax),RCTSUM(imax,jmax))

IU1=31
OPEN(IU1,FILE=TRIM(OUTTSR),form='unformatted',access='direct',recl=4)

IU2=32
OPEN(IU2,FILE=TRIM(OUTTSUM),form='unformatted',access='direct',recl=imax*jmax*4)
RSTSUM=0.0; RCTSUM=0.0

print '(A,4f7.3)','MMMMM SET JULIAN DAY '
call date2jd(iy1,im1,id1,ih1,in1,isec,jd_beg)
call date2jd(iy2,im2,id2,ih2,in2,isec,jd_end)

print '(A,4f7.3)','MMMMM SET DOMAIN ',slon, elon, slat, elat
jd = jd_beg
call jd2date(iyr,mon,idy,ihr,imin,isec,jd)
CALL READ_WRF_LON_LAT(INDIR0,RUN0,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,rlon,rlat)
mask(:,:)=0.
NAAV=0.
DO J=1,jmax
DO I=1,imax
if(rlon(i,j)>=slon .and. rlon(i,j)<=elon .and. &
   rlat(i,j)>=slat .and. rlat(i,j)<=elat)then
   mask(i,j)=1.0
   NAAV=NAAV+1.0
end if
! if(mask(i,j)==1.0)print '(2(3(f7.3,1x),2x))',slon,rlon(i,j),elon,slat,rlat(i,j),elat
END DO !I
END DO !J
RNAAV=1.0/NAAV
PRINT *

print '(A)','MMMMM TIME LOOP'
jd = jd_beg
ITREC=0
RSAC=0.0; RCAC=0.0 !ACCUMLATED RAIN

MAIN_TIME_LOOP: DO 

call jd2date(iyr,mon,idy,ihr,imin,isec,jd)

CALL READ_WRF(INDIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,RAINRNC,W,QCLOUD,UNDEF)

RTYPE1=UNDEF
CALL CHK_P1H(imax,jmax,RAINRNC,RTYPE1)
CALL CHK_TEXTURE(imax,jmax,RAINRNC,UNDEF,RTYPE1)
CALL CHK_W(imax,jmax,kmax,w,UNDEF,RTYPE1)
CALL CHK_QCLOUD(imax,jmax,kmax,QCLOUD,UNDEF,RTYPE1)
CALL CHK_STRAT(imax,jmax,RAINRNC,UNDEF,RTYPE1)

RTYPE=RTYPE1

CALL SEPARATE_RAIN_TYPE(imax,jmax,UNDEF,RTYPE,RAINRNC,RS,RC)
!DO J=1,jmax
!DO I=1,imax
!if(mask(i,j)==1.0)THEN !.and.RTYPE(i,j)/=UNDEF)then
!print '(2(f7.3,1x),2x,F6.1,g11.3)',rlon(i,j),rlat(i,j),rainrnc(I,J),RTYPE(I,J)
!end if
!END DO !I
!END DO !J
!CALL BINOUT_RTYPE(ODIR,RUN,iyr,mon,idy,ihr,imin, &
!&            imax,jmax,rlon,rlat,RTYPE,RS,RC)

CALL AAV_RS_RC(imax,jmax,UNDEF,RS,RC,RSAC,RCAC,mask,RNAAV,IU1,ITREC)

RSTSUM=RSTSUM+RS; RCTSUM=RCTSUM+RC

jd = jd + (dt_file/daysec)

IF (jd > jd_end) EXIT
END DO MAIN_TIME_LOOP

IREC=1
WRITE(IU2,rec=IREC)RSTSUM
IREC=IREC+1
WRITE(IU2,rec=IREC)RCTSUM

END PROGRAM BRAUN2010

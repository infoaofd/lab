#!/bin/bash

#YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-2021081201}
RUN1=$1; RUN1=${RUN1:-RW3A.00.04.05.05.0000.01};RUN1NAME=CNTL
RUN2=$2; RUN2=${RUN2:-RW3A.00.04.05.05.0802.01};RUN2NAME=CLIM
RECT=0 #PLOT RECTANGLE (1) OR NOT
ALONW=130.15 ;ALONE=131.2
ALATS=32.8   ;ALATN=33.3

RUN=${RUN1}; CTL1=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL1
dset /work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/01HR/ARWpost_${RUN}/RTYPE/0RAIN_TSUM_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   1 linear 00Z12AUG2021      60MN      
VARS   2
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
ENDVARS
EOF

RUN=${RUN2}; CTL2=$(basename $0 .sh)_${RUN}.CTL
cat <<EOF>$CTL2
dset /work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/01HR/ARWpost_${RUN}/RTYPE/0RAIN_TSUM_MASK_${RUN}.d01.basic_p.01HR.dat
options  byteswapped
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   1 levels  1000
tdef   1 linear 00Z12AUG2021      60MN      
VARS   2
RS     1  0  HOURLY RAIN STRATIFORM
RC     1  0  HOURLY RAIN CONVECTIVE
ENDVARS
EOF

<<COMMENT
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
TIME=${HH}Z${DD}${MMM}${YYYY}
COMMENT

GS=$(basename $0 .sh).GS
FIGDIR=FIG_$(basename $0 .sh); mkd $FIGDIR
if [ $RECT -eq 1 ];then
FIG=$(basename $0 .sh)_${RUN1}-${RUN2}_RECT.PDF
else
FIG=$(basename $0 .sh)_${RUN1}-${RUN2}.PDF
fi

LONW=128 ;LONE=132 ; LATS=30 ;LATN=34

LEVS1="0 1000 100"
KIND1='-kind (255,255,255)->(245,245,245)->(175,237,237)->(152,251,152)->(67,205,128)->(59,179,113)->(250,250,210)->(255,255,0)->(255,164,0)->(255,0,0)->(205,55,0)->(199,20,133)->(237,130,237)->(255,0,255)'
FS1=2

LEVS2="-350 350 50"
KIND2='-kind cyan->blue->palegreen->azure->papayawhip->gold->red->magenta'
FS2=2
UNIT="mm"



HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

say;'open ${CTL1}';say;'open ${CTL2}';say

'set vpage 0.0 11 0.0 8.5'
xmax = 3; ymax = 2; xleft=0.4; ytop=7.2
xwid = 6.0/xmax; ywid = 5.0/ymax
xmargin=0.7; ymargin=0.6
xmargin2=0.6

'cc';'set grid off';'set grads off'
'set lon ${LONW} ${LONE}';'set lat $LATS $LATN'
'set mpdset hires';'set xlint 2';'set ylint 1'
'set xlopts 1 3 0.1';'set ylopts 1 3 0.1'

say; say 'MMMMM CONVECTIVE ${RUN1NAME}'

xmap=1; ymap=1; nmap=3
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS1} ${KIND1} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RC.1'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN1NAME} CONVECTIVE'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say; say 'MMMMM CONVECTIVE ${RUN2NAME}'
xmap=2; ymap=1; nmap=2
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS1} ${KIND1} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RC.2'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4); yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN2NAME} CONVECTIVE'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say;say 'MMMMM COLOR BAR VERTICAL'
x1=xr+0.3; x2=x1+0.1; y1=yt-2*(yt-yb)-ymargin; y2=yt
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=yt+0.1
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

say; say 'MMMMM CONVECTIVE ${RUN1NAME}-${RUN2NAME}'
xmap=3; ymap=1; nmap=2
xs = xleft + xmargin2 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS2} ${KIND2} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RC.1-RC.2'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN1NAME}-${RUN2NAME} CONVECTIVE'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say;say 'MMMMM COLOR BAR VERTICAL'
x1=xr+0.3; x2=x1+0.1; y1=yt-2*(yt-yb)-ymargin; y2=yt
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS2 -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=yt+0.1
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
x=xleft; y=ytop+0.45
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${RUN1}-${RUN2}'


say; say 'MMMMM STRATIFORM ${RUN1NAME}'

xmap=1; ymap=2; nmap=4
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS1} ${KIND1} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RS.1'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN1NAME} STRATIFORM'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say;say 'MMMMM COLOR BAR HORIZONTAL'
x1=xleft+2*xmargin; x2=x1+2*(xr-xl)+xmargin2; y1=yb-0.6; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS1 -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


say; say 'MMMMM STRATIFORM ${RUN2NAME}'
xmap=2; ymap=2; nmap=5
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS1} ${KIND1} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RS.2'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN2NAME} STRATIFORM'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say; say 'MMMMM STRATIFORM ${RUN1NAME}-${RUN2NAME}'
xmap=3; ymap=2; nmap=6
xs = xleft + xmargin2 +  (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye ;# SET PAGE
'color ${LEVS2} ${KIND2} -gxout shaded' ;# SET COLOR BAR
'set t 1'
'd RS.1-RS.2'
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)
x=(xs+xe)/2; y=yt+0.15
'set strsiz 0.1 0.13'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RUN1NAME}-${RUN2NAME} STRATIFORM'
if (rect=1) 
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATS} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONE} ${ALATN} ${ALONW} ${ALATN} -c 1 -l 1 -t 6'
'trackplot ${ALONW} ${ALATN} ${ALONW} ${ALATS} -c 1 -l 1 -t 6'
endif

say;say 'MMMMM COLOR BAR HORIZONTAL'
x1=xleft+2*xmargin; x2=x1+2*(xr-xl)+xmargin2; y1=yb-1.2; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS2 -ft 3 -line on -lc 0 -edge square'
x=x2+0.05; y=(y1+y2)/2
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'


# HEADER
'set strsiz 0.08 0.1'; 'set string 1 l 3 0'
xx = 0.1; yy=ytop+0.35;'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.15;'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.15;'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.15;'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.15;'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcl "$GS"
rm -vf $GS $CTL1 $CTL2

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

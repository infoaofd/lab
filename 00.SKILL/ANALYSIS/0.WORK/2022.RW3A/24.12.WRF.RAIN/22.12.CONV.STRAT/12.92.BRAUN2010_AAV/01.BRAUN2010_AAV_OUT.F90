
SUBROUTINE AAV_RS_RC(imax,jmax,UNDEF,RS,RC,RSAC,RCAC,mask,RNAAV,IU,ITREC)
integer,intent(in):: imax, jmax, IU
integer,intent(inout):: ITREC
REAL,intent(in)::UNDEF, RNAAV
real,intent(in),dimension(imax,jmax)::RS, RC, mask
REAL,intent(inout)::RSAC,RCAC
REAL RSAAV, RCAAV

RSAAV=SUM(mask*RS)*RNAAV
RSAC=RSAC+RSAAV
if(RSAAV<0.0)RSAAV=UNDEF

RCAAV=SUM(mask*RC)*RNAAV
RCAC=RCAC+RCAAV
if(RCAAV<0.0)RCAAV=UNDEF

ITREC=ITREC+1
WRITE(IU,REC=ITREC)RSAAV
ITREC=ITREC+1
WRITE(IU,REC=ITREC)RCAAV
ITREC=ITREC+1
WRITE(IU,REC=ITREC)RSAC
ITREC=ITREC+1
WRITE(IU,REC=ITREC)RCAC

!PRINT '(A,i3,1x,i5,2g11.3,2x,2f10.0)','MMMMM AAV_RS_RC ',IU,ITREC,RSAAV, RCAAV, RSAC,RCAC
PRINT *,'MMMMM AAV_RS_RC ',IU,ITREC,RSAAV, RCAAV, RSAC,RCAC
END



SUBROUTINE BINOUT_RTYPE(ODIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,rlon,rlat,RTYPE,RS,RC)

character(len=*),intent(in):: ODIR,RUN
integer,intent(in):: iyr, mon, idy, ihr, imin
integer,intent(in):: imax, jmax
real,intent(in),dimension(imax,jmax)::rlon,rlat,RTYPE,RS,RC
character(len=1000) :: OFLE

logical :: yes
integer :: ier, irec
integer :: i, j, k

write(OFLE,'(a,a,a,a,i4.4,a,i2.2,a,i2.2,a,i2.2,a,i2.2,a)') &
trim(ODIR),'/',TRIM(RUN),'_RTYPE_', &
iyr,'-',mon,'-',idy,'_',ihr,'_',imin,'.dat'

inquire(file=trim(OFLE),exist=yes)
if (yes) then
  print *;print '(A,A)','MMMMM DELETE ',trim(OFLE)
  CALL SYSTEM('rm -vf '//TRIM(OFLE)); print *
end if

print '(A,A)','MMMMM OUT ',trim(OFLE)

open (10,file=trim(OFLE),form='unformatted',&
      access='direct',recl=imax*jmax*4)

irec=1
WRITE (10,rec=irec) ((rlat(i,j),i=1,imax),j=1,jmax)

irec=irec+1
WRITE (10,rec=irec) ((rlon(i,j),i=1,imax),j=1,jmax)

irec=irec+1
WRITE (10,rec=irec) ((RTYPE(i,j),i=1,imax),j=1,jmax)

irec=irec+1
WRITE (10,rec=irec) ((RS(i,j),i=1,imax),j=1,jmax)

irec=irec+1
WRITE (10,rec=irec) ((RC(i,j),i=1,imax),j=1,jmax)
!DO J=1,jmax
!DO I=1,imax
!print '(g11.3)',RTYPE(I,J)
!END DO !I
!END DO !J

write(*,*);print '(A)','MMMMM BINOUT_RTYPE END'; PRINT *

close(10)

return
end

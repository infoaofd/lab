"""
https://doku-pro.com/matplotlib-bar-chart/
/work04/manda/2022.RW3A/24.12.WRF.RAIN/12.02.3DAY_ACCUMRATED_00Z12-00Z15
https://docs.google.com/presentation/d/11EU2Msv91vD6piGdLYbraRdKlXnl4gYJ/edit#slide=id.p10
実験番号	3日雨量
		 (九州領域平均)
		CNTL		CLIM (8月中旬)
01 		289 		263 
02 		269 		241
03 		296 		261
平均 	285 		255
"""

import matplotlib.pyplot as plt #モジュールのインポート
import numpy as np #numpyのインポート

fig, ax = plt.subplots() #グラフを定義

label = ['1', '2', '3'] #x軸用のラベル
y = [289,269,296] #y軸用のデータ
y2 = [263,241,261]    #y軸用のデータ2
x = np.arange(len(label)) #numpyでラベルの数のアレイを作る
width = 0.35 #幅を変数に格納

ax.set_ylim(200, 320)
ax.set_xlabel("Run number", fontsize=16)
ax.set_ylabel("Accumulated precipitation [mm/3-day]", fontsize=14)
ax.bar(x-width/2, y,  width,  color='lime', label="CNTL")   #widthの半分の値を引くと、左側に
ax.bar(x+width/2, y2,  width,  color='magenta', label="CLIM") #widthの半分の値を足すと、右側に

ax.set_xticks(x) #ラベルの数を指定、アレイを渡す
ax.set_xticklabels(label) #ラベルの表示を変更
plt.legend()

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
fig.savefig(FIG)
print("FIG: "+FIG)
print("")

# /work09/am/00.WORK/2022.RW3A/24.16.WRF.MAUL/12.12.CSC/32.12.ROTATE_WIND/14.12.MAP_UV_RAIN_LINE
function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}
EXP=$1;EXP=${EXP:-0000}
if [ $EXP -eq "0000" ];then EXPNAME=CNTL;fi
if [ $EXP -eq "0702" ];then EXPNAME=0702;fi

ALONW=130.06; ALATS=32.71; ALONE=130.67; ALATN=32.94
TLONW=129.9; TLONE=130.0; TLATS=32.58; TLATN=32.68

LONW=129.75; LONE=131.05; LATS=32.25;  LATN=33.25

INROOT=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj
if [ ! -d $INROOT ];then EEEEE NO SUCH DIR,$INROOT;exit 1;fi

INDIR=$INROOT/RW3A.00.04.05.05.${EXP}.01
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR,$INDIR;exit 1;fi
CTL=$INDIR/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo EEEEE NO SUCH FILE,$CTL;exit 1;fi
cp -av $CTL .
CTL1=$CTL

YMDHM1=$2; YMDHM1=${YMDHM1:-202108120600}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}
YMDHM2=$3; YMDHM2=${YMDHM2:-202108120700}
YMDHM=$YMDHM2
YYYYMMDDHHMI $YMDHM
Y2=${YYYY};MM2=${MM};MMM2=${MMM};DD2=${DD};HH2=${HH};MI2=${MI}

TIME1=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}
TIME2=${HH2}:${MI2}Z${DD2}${MMM2}${Y2}

FIG=$(basename $0 .sh)_${EXP}_${Y1}${MM1}${DD1}${HH1}-${Y2}${MM2}${DD2}${HH2}.pdf


KIND='(255,255,255)->(245,245,245)->(175,237,237)->(152,251,152)->(67,205,128)->(59,179,113)->(250,250,210)->(255,255,0)->(255,164,0)->(255,0,0)->(205,55,0)->(199,20,133)->(237,130,237)->(255,0,255)'
LEVS='0 80 5'; UNIT="mm/h"

ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

cat <<EOF>$GS

'open $CTL1'
#'q ctlinfo'; say result

'set lon $LONW $LONE'; 'set lat $LATS $LATN';'set z 1'
'set time ${TIME1}'

'cc'

'set vpage 0.0 8.5 0 10.5'; 'set grid off'
xleft=1.; ytop=8
xmax=1; ymax=1
xwid =  6/xmax; ywid =  5/ymax; xmargin=0.6; ymargin=0.5

ymap=1; xmap=1
xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set mpdraw on'; 'set mpdset worldmap'; 'set map 1 1 2'
'set ylint 0.5'; 'set xlint 0.5'

say  'MMMMM COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'q dims'; say 'MMMMM $TIME0 'sublin(result,5)
'd RAINNC(time=${TIME2})-RAINNC(time=${TIME1})'

'q gxinfo'
line3=sublin(result,3); xl=subwrd(line3,4); xr=subwrd(line3,6)
line4=sublin(result,4); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-2; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'
x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'; 'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'; 'set ylab off'

#say 'MMMMM LINE'
'trackplot ${ALONW} ${ALATS} ${ALONE} ${ALATN} -c 1 -l 1 -t 8'
'set strsiz 0.16 0.19'; 'set string 2 c 4 0'
'q w2xy ${ALONW} ${ALATS}'; xx=subwrd(result,3); yy=subwrd(result,6)
'draw string 'xx-0.1' 'yy+0.1' A1'
'q w2xy ${ALONE} ${ALATN}'; xx=subwrd(result,3); yy=subwrd(result,6)
'draw string 'xx-0.1' 'yy+0.1' A2'

#'trackplot ${BLONW} ${BLATS} ${BLONE} ${BLATN} -c 4 -l 1 -t 8'
#
#say 'MMMMM RECTANGLE DOMAIN FOR TSR CAPE'
#say '/work09/am/00.WORK/2022.RW3A/26.12.ECAPE_WRF/12.22.ECAPE_500m/12.12.TEST_ECAPE'
#'trackplot ${RLONW} ${RLATS} ${RLONE} ${RLATS} -c 1 -l 1 -t 6'
#'trackplot ${RLONE} ${RLATS} ${RLONE} ${RLATN} -c 1 -l 1 -t 6'
#'trackplot ${RLONE} ${RLATN} ${RLONW} ${RLATN} -c 1 -l 1 -t 6'
#'trackplot ${RLONW} ${RLATN} ${RLONW} ${RLATS} -c 1 -l 1 -t 6'
#
#'trackplot ${RLONW2} ${RLATS2} ${RLONE2} ${RLATS2} -c 1 -l 1 -t 6'
#'trackplot ${RLONE2} ${RLATS2} ${RLONE2} ${RLATN2} -c 1 -l 1 -t 6'
#'trackplot ${RLONE2} ${RLATN2} ${RLONW2} ${RLATN2} -c 1 -l 1 -t 6'
#'trackplot ${RLONW2} ${RLATN2} ${RLONW2} ${RLATS2} -c 1 -l 1 -t 6'

say 'MMMMM RECTANGLE DOMAIN FOR BACK TRAJ'
say '/work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.22.RW3A.TRAJ.FWD.10MIN.01'
'trackplot ${TLONW} ${TLATS} ${TLONE} ${TLATS} -c 1 -l 1 -t 6'
'trackplot ${TLONE} ${TLATS} ${TLONE} ${TLATN} -c 1 -l 1 -t 6'
'trackplot ${TLONE} ${TLATN} ${TLONW} ${TLATN} -c 1 -l 1 -t 6'
'trackplot ${TLONW} ${TLATN} ${TLONW} ${TLATS} -c 1 -l 1 -t 6'

'set gxout vector'
'set lev 950'
'set gxout vector'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u,10);v -SCL 0.5 20 -P 20 20 -SL m/s'

xx=x2+1.5; yy=yb-0.35
'set cthick 3'; 'set ccolor  1'
'vec.gs skip(u,10);v -SCL 0.5 20 -P 'xx' 'yy' -SL m/s'

'set lev 800'
'set cthick 10'; 'set ccolor  0'
'vec.gs skip(u,10);v -SCL 0.5 20 -P 20 20 -SL m/s'

xx=x2+0.5; yy=yb-0.35
'set cthick 5'; 'set ccolor  9'
'vec.gs skip(u,10);v -SCL 0.5 20 -P 20 20 -SL m/s'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

ctime1=subwrd(result,3)  
hh=substr(ctime1,1,2); dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) ; year=substr(ctime1,9,4)  

x=(xl+xr)/2; y=yt+0.25
'set strsiz 0.15 0.18'; 'set string 1 c 4 0'
'draw string 'x' 'y' '${TIME1}-${TIME2}' $EXPNAME'

x=(xl+xr)/2; y=y+0.35
'set strsiz 0.15 0.18'; 'set string 2 c 3 0'
'draw string 'x' 'y' ${ALONW} ${ALATS} ${ALONE} ${ALATN}'
x=(xl+xr)/2; y=y+0.3
'set strsiz 0.15 0.18'; 'set string 4 c 3 0'
'draw string 'x' 'y' ${BLONW} ${BLATS} ${BLONE} ${BLATN}'
x=(xl+xr)/2; y=y+0.3
'set strsiz 0.15 0.18'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RLONW} ${RLATS} ${RLONE} ${RLATN}'
x=(xl+xr)/2; y=y+0.3
'set strsiz 0.15 0.18'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${RLONW2} ${RLATS2} ${RLONE2} ${RLATN2}'

say '### HEADER'
'set strsiz 0.12 0.14'; 'set string 1 l 2 0'
xx = 0.2; yy=y+0.35; 'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

if [ -f $FIG ];then
FIGDIR=FIG_$(basename $0 .sh)
mkd $FIGDIR
cp -av $FIG $FIGDIR
echo; echo MMMMM $FIG ;echo
fi


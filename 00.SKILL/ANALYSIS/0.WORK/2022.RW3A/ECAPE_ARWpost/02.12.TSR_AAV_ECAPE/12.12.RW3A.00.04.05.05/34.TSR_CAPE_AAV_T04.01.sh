#!/bin/bash

MM2MMM(){
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}
RLONW=130.6 ;RLONE=130.7; RLATS=33.1;RLATN=33.2 # T04.01
LEV=$1; LEV=${LEV:-500m}
YYYYMMDDHH1=$2; YYYYMMDDHH1=${YYYYMMDDHH1:-2021081223}
YYYYMMDDHH2=$3; YYYYMMDDHH2=${YYYYMMDDHH2:-2021081300}

VNAME=ECAPE; VRANGE="0 600"; YLINT="100"; UNIT=[J/kg]

YYYY1=${YYYYMMDDHH1:0:4}; MM=${YYYYMMDDHH1:4:2}; DD1=${YYYYMMDDHH1:6:2}; HH1=${YYYYMMDDHH1:8:2}
MM2MMM 
MM1=$MM; MMM1=$MMM
YYYY2=${YYYYMMDDHH2:0:4}; MM=${YYYYMMDDHH2:4:2}; DD2=${YYYYMMDDHH2:6:2}; HH2=${YYYYMMDDHH2:8:2}
MM2MMM 
MM2=$MM; MMM2=$MMM

TIME1=${HH1}Z${DD1}${MMM1}${YYYY1}; TIME2=${HH2}Z${DD2}${MMM2}${YYYY2}

EXP=0000
INDIR=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/10MN/ECAPE${LEV}_RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/ECAPE_RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.CTL
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP1=$EXP; CTL1=$CTL

EXP=0802
INDIR=/work04/manda/ARWpost_work04_2021-03-20/RW3A.ARWpost/10MN/ECAPE${LEV}_RW3A.00.04.05.05.${EXP}.01
CTL=$INDIR/ECAPE_RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.CTL
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP2=$EXP; CTL2=$CTL

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_RW3A.00.04.05.05.${EXP1}-${EXP2}.01_${RLONW}_${RLATS}_${LEV}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'set lon ${RLONW}';'set lat ${RLATS}'
'set time ${TIME1} ${TIME2}'
'AAV1=tloop(aave(cape.1,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN}))'
'AAV2=tloop(aave(cape.2,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN}))'

# BLUE FIRST
'set grid off';'set grads off'
'set gxout line';'set ccolor 4';'set cmark 0';'set cthick 5'
'set vrange $VRANGE';'set ylint $YLINT'
'set xaxis 0 60 10'
'd AAV2'
'set xlab off';'set ylab off'

# RED SECOND
'set gxout line';'set ccolor 2';'set cmark 0';'set cthick 5'
'set vrange $VRANGE'
'd AAV1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=(xl+xr)/2; y=yb-0.5
'set strsiz 0.12 0.15'; 'set string 1 c 4 0'
'draw string 'x' 'y' Mintues from $TIME1'

x=xl-0.7; y=(yt+yb)/2
'set strsiz 0.12 0.15'; 'set string 1 c 4 90'
'draw string 'x' 'y' $VNAME ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=xl+0.05; y=yt+0.2
'set string 2 l 3 0'; 'draw string 'x' 'y' ${EXP1}'
x=x+0.7
'set string 4 l 3 0'; 'draw string 'x' 'y' ${EXP2}'

'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=y+0.24
'draw string 'x' 'y' ${LEV} ${RLONW}-${RLONE},${RLATS}-${RLATN}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=y+0.27; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

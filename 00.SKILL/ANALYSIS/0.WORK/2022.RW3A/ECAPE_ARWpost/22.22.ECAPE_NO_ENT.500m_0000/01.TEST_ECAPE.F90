PROGRAM TEST_ECAPE
! Sat, 04 May 2024 12:01:28 +0900
! /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

USE entraining_cape

integer :: iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2
integer::isec=0
real    :: slon, elon, slat, elat
real :: DT_FILE  ![sec]
real   , parameter :: daysec = 86400.0 ![sec]
REAL, PARAMETER::UNDEF=1.e30
real(8) :: jd_beg, jd_end, jd
real,allocatable,dimension(:,:)::rlon,rlat,mask
real,allocatable,dimension(:)::Z ![m]
real,allocatable,dimension(:,:,:)::QVAPOR, pressure, tk, theta, EPT, RH
real,allocatable,dimension(:,:)::HGT

REAL ALONW, ALONE, ALATS, ALATN !X-AXIS OF ROTATED CORRDINATE

REAL NAAV !NUMBER OF THE GRIDS FOR AREA AVERAGE

integer::imax, jmax, kmax, KZMAX

INTEGER,allocatable,dimension(:)::KOUT
REAL,allocatable,dimension(:)::ZOUT
INTEGER::KZOUT

CHARACTER(LEN=500)::INDIR,RUN, ODIR
REAL:: CAPE_OUTPUT, CIN_OUTPUT, LCL_OUTPUT, DLFC_OUTPUT, EL_OUTPUT
REAL,allocatable,dimension(:,:)::CAPE,CIN,LCL,DLFC,EL

!FOR ENTRAINMENT CAPE
REAL,ALLOCATABLE,DIMENSION(:)::TT,RH2,z2,P2,EPT2,QV2
REAL EPTMAX, QV_P, TEM_P, Z_P, P_P, EPT_P, RH_P
REAL(8) entrainment_rate,w,maxlev,dt !FOR ENTRAINMENT CAPE

namelist /PARA/INDIR,RUN,ODIR,iy1,im1,id1,ih1,in1, iy2,im2,id2,ih2,in2,&
DT_FILE,slon, elon, slat, elat,  imax, jmax, kmax, KZMAX, ALONW, ALONE, ALATS, ALATN,&
entrainment_rate,w,maxlev,dt

READ(5,NML=PARA)

PRINT '(A,A)','INDIR=',TRIM(INDIR)

allocate(rlon(imax,jmax),rlat(imax,jmax),mask(imax,jmax), HGT(imax,jmax))
allocate(Z(KZMAX))
allocate(pressure(imax,jmax,KZMAX), tk(imax,jmax,KZMAX), QVAPOR(imax,jmax,KZMAX),&
theta(imax,jmax,KZMAX),EPT(imax,jmax,KZMAX),RH(imax,jmax,KZMAX) )

ALLOCATE(TT(KZMAX),RH2(KZMAX),z2(KZMAX),P2(KZMAX),EPT2(KZMAX),QV2(KZMAX))
ALLOCATE(CAPE(imax,jmax),CIN(imax,jmax),LCL(imax,jmax),DLFC(imax,jmax),&
EL(imax,jmax))

print '(A,4f7.3)','MMMMM READ Z '
OPEN(10,FILE="Z.TXT",ACTION="READ")
DO K=1,KZMAX
READ(10,*)Z(K)
END DO
CLOSE(10)

print '(A,4f7.3)','MMMMM SET JULIAN DAY '
call date2jd(iy1,im1,id1,ih1,in1,isec,jd_beg)
call date2jd(iy2,im2,id2,ih2,in2,isec,jd_end)
eps=1.0/(24.0*60.0); jd_end=jd_end+eps

print '(A,4f7.3)','MMMMM SET DOMAIN ',slon, elon, slat, elat
jd = jd_beg
call jd2date(iyr,mon,idy,ihr,imin,isec,jd)
CALL READ_WRF_LON_LAT(INDIR,RUN,iyr,mon,idy,ihr,imin, &
&            imax,jmax,kmax,rlon,rlat)

print '(A)','MMMMM TIME LOOP'
jd = jd_beg
ITREC=0

MAIN_TIME_LOOP: DO 

call jd2date(iyr,mon,idy,ihr,imin,isec,jd)

CALL READ_WRF(INDIR,RUN,iyr,mon,idy,ihr,imin, &
& imax,jmax,KZMAX,UNDEF, QVAPOR, HGT, pressure, tk, theta)

PRINT '(A)','MMMMM CAL RH'
CALL CAL_RH(imax,jmax,KZMAX,UNDEF,pressure, tk, QVAPOR, RH)

PRINT '(A)','MMMMM CAL EPT'
CALL CAL_EPT(imax,jmax,KZMAX,UNDEF, pressure, tk, rh, EPT)

PRINT '(A)','MMMMM MAIN CHECK Z,P,tk,QV,RH,EPT'
i=imax/2; j=jmax/2
DO K=1,KZMAX,5
if(pressure(i,j,k)/=UNDEF)THEN
PRINT '(f7.2,1x,2f8.1,1x,f10.4,f7.0,f7.1)',&
Z(k),pressure(i,j,k), tk(i,j,k), QVAPOR(i,j,k), RH(i,j,k),EPT(I,J,K)
END IF
END DO !K

PRINT '(A)','MMMMM CAL ECAPE'
TT=0.0;RH2=0.0;z2=0.0;P2=0.0
! calculate initial parcel parameters
DO J=1,jmax
  DO I=1,imax

    DO K=1,KZMAX
     TT(K)=tk(I,J,K) !TMP(I,J,K+1)
     RH2(K)=rh(I,J,K)
     z2(K)=Z(K)*1000.0 !km->m
     P2(K) = pressure(I,J,K)
     EPT2(K) = EPT(I,J,K)
     QV2(K) = QVAPOR(I,J,K)
   END DO !K

! PARCEL AT 500m
KDX=18
Z_P=Z(KDX)*1000.0 !km->m
TEM_P=TT(KDX)
RH_P=RH2(KDX)
P_P=P2(KDX)
QV_P=QV2(KDX)
EPT_P=EPT2(KDX)

!PRINT *,Z_P, TEM_P, QV_P, RH_P, P_P, EPT_P
!DO K=1,KZMAX,5
!PRINT *,P2(K),Z2(K),TT(K),RH2(K)
!END DO !K
!PRINT *,W,DT,ENTRAINMENT_RATE,MAXLEV

    IF(Z_P<UNDEF .AND. TEM_P<UNDEF .AND. QV_P<UNDEF)THEN

      CALL CALC_CAPE_ROMPS_AND_KUANG_2010_NO_FUSION_TOTAL_FALLOUT &
      (Z_P, TEM_P, QV_P, RH_P, KZMAX, P2, Z2, TT, RH2, UNDEF, W, DT, &
    &  ENTRAINMENT_RATE, MAXLEV, &
    &  CAPE_OUTPUT, CIN_OUTPUT, LCL_OUTPUT, DLFC_OUTPUT, EL_OUTPUT)
!     PRINT *,CAPE_OUTPUT, CIN_OUTPUT, LCL_OUTPUT, DLFC_OUTPUT, EL_OUTPUT
      CAPE(I,J)=CAPE_OUTPUT
      CIN(I,J)=CIN_OUTPUT
      LCL(I,J)=LCL_OUTPUT
      DLFC(I,J)=DLFC_OUTPUT
      EL(I,J)=EL_OUTPUT
!
    END IF !EPTMAX
   
    IF(i==imax/2 .and. j==jmax/2)THEN
    print '(A,2i4,1x,10g12.3)','I,J,CAPE,CIN,LCL,DLFC,EL=',I,J,CAPE(I,J),CIN(I,J),LCL(I,J),DLFC(I,J),EL(I,J)
    END IF

  END DO !I
END DO !J

CALL OUT_BIN(ODIR,RUN,iyr,mon,idy,ihr,imin,&
imax,jmax,KZMAX,UNDEF,Z,CAPE,CIN,LCL,DLFC,EL)

jd = jd + (dt_file/daysec)

PRINT *

IF (jd > jd_end) EXIT
END DO MAIN_TIME_LOOP

!DO K=1,KOMAX
!PRINT '(I4,1x,I4,1x,F7.3,F7.3)',K, KOUT(K),Z(KOUT(K)), ZOUT(K)
!END DO !K

PRINT '(A)','MMMMM ODIR=',TRIM(ODIR)
PRINT '(A)','MMMMM RUN =',TRIM(RUN)

END PROGRAM TEST_ECAPE

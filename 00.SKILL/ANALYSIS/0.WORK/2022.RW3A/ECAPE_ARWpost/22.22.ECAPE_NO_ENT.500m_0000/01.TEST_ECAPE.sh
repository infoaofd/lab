#!/bin/bash
# /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/22.12.CONV.STRAT/12.94.BRAUN2010_AAV_N_KYUSHU
# 2024-05-09_20-09
ulimit -s unlimited
LEV=500m
RUNNAME=$1;RUNNAME=${RUNNAME:-0000}
RUNNO=$2;RUNNO=${RUNNO:-01}

EXP=${EXP:-RW3A.00.04.05.05.${RUNNAME}.${RUNNO}}

ALONW=129.8; ALATS=32.5; ALONE=130.4; ALATN=33.0 #0702 02Z 950&800hPa

EXP=${EXP:-RW3A.00.04.05.05.${RUNNAME}.${RUNNO}}
# PERIOD
iy1=2021; im1=8; id1=12; ih1=00; in1=0
iy2=2021; im2=8; id2=14; ih2=23; in2=50
#iy2=2021; im2=8; id2=12; ih2=01; in2=0

# FOR XLONG,XLAT,U,V,WZ
EXP=RW3A.00.04.05.05.${RUNNAME}.01
INROOT=/work04/manda/RW3A.ARWpost/10MN/ARWpost_RW3A.00.04.05.05/traj/
INDIR=$INROOT/${EXP}
if [ ! -d $INDIR ];then EEEEE NO SUCH DIR, $INDIR;exit 1;fi
RUN=${EXP}.d01.traj.10MN

ODIR=/work04/manda/RW3A.ARWpost/10MN/ECAPE_NO_ENT${LEV}_${EXP}
mkdir -vp $ODIR

README=$ODIR/0.README_$(basename $0).sh
echo >> $README
echo $(pwd) >> $README
echo $0 >>$README

DT_FILE=600.0
imax=599; jmax=599; kmax=30; KZMAX=61

slon=129.0;elon=132.;slat=30.0;elat=34.5
#slon=130.15;elon=131.2;slat=32.8;elat=33.3

SRC=$(basename $0 .sh).F90
SUB="$(basename $0 .sh)_DATE.F90 $(basename $0 .sh)_SUB.F90  \
$(basename $0 .sh)_CAL_RH.F90 $(basename $0 .sh)_OUT.F90 \
module_calc_CAPE_NO_ENTRAIMENT.F90" ;#MyCAPE.f

# https://atmarkit.itmedia.co.jp/flinux/rensai/linuxtips/248chngext.html
for fname in $SUB; do
OBJ="${OBJ} ${fname%.F90}.o"
done
OBJ="${OBJ} $(basename $SRC .F90).o"

exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/openmpi-4.1.4/lib:/usr/local/grib_api-1.28.0/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -O2 -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "

# OpenMP
#OPT2=" -fopenmp "
ENTRAINMENT_RATE=${ENTRAINMENT_RATE:-0.0} #DUMMY. RH-DEPEND ROUTINE IS USED
W=10.0;DT=10.0 ;MAXLEV=15000.0

cat<<EOF>$nml
&para
INDIR="$INDIR",
RUN="$RUN",
ODIR="$ODIR",
iy1=$iy1,
im1=$im1
id1=$id1
ih1=$ih1
in1=$in1
iy2=$iy2,
im2=$im2
id2=$id2
ih2=$ih2
in2=$in2
DT_FILE=$DT_FILE
slon=$slon
elon=$elon
slat=$slat
elat=$elat
imax=$imax, 
jmax=$jmax, 
kmax=$kmax,
KZMAX=$KZMAX
ALONW=${ALONW},
ALONE=${ALONE},
ALATS=${ALATS},
ALATN=${ALATN},
entrainment_rate=${ENTRAINMENT_RATE}
w=${W}
maxlev=${MAXLEV}
dt=${DT}
&end
EOF

echo MMMMM Compiling ${SRC} ${SUB}  ...
echo
${f90} ${OPT} ${SUB} -c
if [ $? -ne 0 ];then echo;echo EEEEE COMPILE ERROR ${SUB};exit 1;fi
${f90} ${OPT} ${SRC} -c
if [ $? -ne 0 ];then echo;echo EEEEE COMPILE ERROR ${SRC};exit 1;fi
${f90} ${OPT} ${OBJ} -o ${exe}
if [ $? -ne 0 ];then echo;echo EEEEE LINK ERORR ${OBJ};exit 1;fi

rm -vf *.o *.mod
echo; echo MMMMM COMPILE DONE!; echo

echo
echo MMMMM ${exe} is RUNNING ...
echo
D1=$(date -R)
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf $exe $nml
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
D2=$(date -R)
rm -vf $exe $nml

cp -a $0 $ODIR
echo ;echo "START: $D1"; echo "END:  $D2";echo

CTL=$ODIR/ECAPE_${RUN}.CTL
cat <<EOF>$CTL
dset ^ECAPE_${RUN}_%y4-%m2-%d2_%h2_%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef 61 levels
   0.02500
   0.05000
   0.07500
   0.10000
   0.12500
   0.15000
   0.17500
   0.20000
   0.22500
   0.25000
   0.27500
   0.30000
   0.32500
   0.35000
   0.37500
   0.40000
   0.45000
   0.50000
   0.55000
   0.60000
   0.65000
   0.70000
   0.75000
   0.80000
   0.85000
   0.90000
   0.95000
   1.00000
   1.05000
   1.10000
   1.15000
   1.20000
   1.25000
   1.30000
   1.35000
   1.40000
   1.45000
   1.50000
   2.00000
   2.50000
   3.00000
   3.50000
   4.00000
   4.50000
   5.00000
   5.50000
   6.00000
   7.00000
   8.00000
   9.00000
  10.00000
  11.00000
  12.00000
  13.00000
  14.00000
  15.00000
  16.00000
  17.00000
  18.00000
  19.00000
  20.00000
tdef   1440 linear 00Z11AUG2021      10MN      
VARS  5
CAPE 1 0 ECAPE J/kg
CIN  1 0 J/kg
LCL  1 0 m
DLFC 1 0 DLFC m
EL   1 0 EL m
ENDVARS
EOF

echo; echo "MMMMM CTL: $CTL";echo

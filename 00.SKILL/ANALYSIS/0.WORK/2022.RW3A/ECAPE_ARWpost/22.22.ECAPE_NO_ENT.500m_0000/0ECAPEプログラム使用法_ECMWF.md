# ECAPEプログラム使用法_ECMWF

/work09/am/00.WORK/2022.ECS2022/26.LFM.3/22.16.ECAPE500m.RH.ECMWF_2

**ECAPE500m_NC4_RH.F90**

```fortran
DO J=1,MJX
  DO I=1,MIY

    DO K=MKZH,1,-1
     TT(MKZH-K+1)=temp_in(I,J,K,1) !TMP(I,J,K+1)
     RH2(MKZH-K+1)=rh_in(I,J,K,1)
     z2(MKZH-K+1)=z_in(I,J,K,1)
     P2(MKZH-K+1) = p_in(K)
     EPT2(MKZH-K+1) = ept_in(I,J,K,1)
     QV2(MKZH-K+1) = qv_in(I,J,K,1)
   END DO !K
```

**入力変数**: TT, RH2, Z2, P2, EPT2, QV2: 環境場のプロファイル (鉛直1次元)

```fortran
! 500m
    DO K = 1,MKZH-1
!     print *,K,Z2(K),Z2(K+1)
     IF(Z2(K) < Z_P .AND. Z_P <= Z2(K+1)) THEN
!!!       SUBROUTINE INT1D(X1,Y1, X2, Y2, X, Y)
!     print *,'MMMMMM ',K,Z_P, Z2(K),Z2(K+1)
       CALL INT1D(Z2(K),TT(K), Z2(K+1), TT(K+1), 500.0, TEM_P)
       CALL INT1D(Z2(K),RH2(K), Z2(K+1), RH2(K+1), 500.0, RH_P)
       CALL INT1D(Z2(K),P2(K), Z2(K+1), P2(K+1), 500.0, P_P)
       exit 
     ENDIF
   END DO !K
   CALL CAL_QVAPOR(TEM_P,RH_P,P_P,QV_P)
   CALL CAL_EPT(TEM_P,RH_P,P_P, EPT_P)
```

TEM_P, RH_P, P_P, QV_P, EPT_P: 持ち上げる気塊の値(上の例では高度500mから持ち上げている)

```fortran
  CALL CALC_CAPE_ROMPS_AND_KUANG_2010_NO_FUSION_TOTAL_FALLOUT &
  (Z_P, TEM_P, QV_P, RH_P, MKZH-1, P2, Z2, TT, RH2, UNDEF,W, DT, &
&  ENTRAINMENT_RATE, MAXLEV, &
&  CAPE_OUTPUT, CIN_OUTPUT, LCL_OUTPUT, LFC_OUTPUT, EL_OUTPUT)

      CAPE(I,J)=CAPE_OUTPUT
      CIN(I,J)=CIN_OUTPUT
      LCL(I,J)=LCL_OUTPUT
      LFC(I,J)=LFC_OUTPUT
      EL(I,J)=EL_OUTPUT
      CAPEO(I,J)=CAPE(I,J); CINO(I,J)=CIN(I,J)
      DLFCO(I,J)=sngl(LFC(I,J)-Z_P); DLCLO(I,J)=sngl(LCL(I,J)-Z_P); ELO(I,J)=EL(I,J)
```



入力データの例

/work09/am/00.WORK/2022.ECS2022/04.SOUNDING/22.12.SONDE/22.14.STN/42.22.BUOY.ENTRAINMENT_RH/INPUT

```
# STN K1
  63
   0.00  1000.0  -2.588   7.315   7.76    1.911  346.19   90.3    93.7 300.00  23.775   0.016987 
   0.00   990.0  -2.037   7.655   7.92    1.831  345.16   90.7   181.4 300.13  23.124   0.016553 
```

**下から上に並んでいる**

読み込み

/work09/am/00.WORK/2022.ECS2022/04.SOUNDING/22.12.SONDE/22.14.STN/42.22.BUOY.ENTRAINMENT_RH

**ECAPE.VPR_TXT_RH.F90**

```FORTRAN
OPEN(11,FILE=IN1,ACTION="READ")

READ(11,*)
READ(11,*)MKZH
I=1; J=1

DO K=1,MKZH
READ(11,*)dum,&
p_in(K), dum, dum, dum, dum, ept_in(I,J,K,1), rh_in(I,J,K,1), z_in(I,J,K,1), dum,temp_in(I,J,K,1),  &
qv_in(I,J,K,1) !, u_in(I,J,K,1), v_in(I,J,K,1)
END DO !K
```

qv_in(I,J,K,1)の単位はkg/kg
#!/bin/bash

# Mon, 10 Jun 2024 08:04:52 +0900
# /work09/am/00.WORK/2022.RW3A/26.12.ECAPE_WRF/12.22.ECAPE_500m/12.12.TEST_ECAPE

YYYYMMDDHH=$1; YYYYMMDDHH=${YYYYMMDDHH:-202108120200}

RUN=$2; RUN=${RUN:-0000}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}
MI=${YYYYMMDDHH:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}:${MI}Z${DD}${MMM}${YYYY}; echo $TIME
TEXT=${TIME}
INDIR1=/work00/DATA/HD02/RW3A.ARWpost.DAT/traj/RW3A.00.04.05.05.${RUN}.01/ECAPE/
CTL1=${INDIR1}/ECAPE_RW3A.00.04.05.05.${RUN}.01.d01.traj.10MN.CTL
if [ ! -f $CTL1 ];then echo NO SUCH FILE,$CTL1;exit 1;fi

INDIR2=/work00/DATA/HD04/RW3A.00.04/01HR/ARWpost_RW3A.00.04.05.05.${RUN}.01
CTL2=${INDIR2}/RW3A.00.04.05.05.${RUN}.01.d01.basic_p.01HR.ctl
if [ ! -f $CTL2 ];then echo NO SUCH FILE,$CTL2;exit 1;fi

GS=$(basename $0 .sh).GS; 
FIGDIR=FIG_$(basename $0 .sh)_${RUN};mkdir -vp $FIGDIR
FIG=$FIGDIR/$(basename $0 .sh)_${RUN}_${YYYY}${MM}${DD}_${HH}${MI}.PDF

LONW=128 ;LONE=132 ; LATS=30 ;LATN=34.5

LEVS="0 600 50"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
FS=2
UNIT=J/kg

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set dfile 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'
'q dims'; line=sublin(result,5)
say 'MMMMM dfile 1 'line


'set mpdset hires'
'set grid off'
'set xlint 1';'set ylint 1'
'd CAPE.1'

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

'set dfile 2'
'set z 1'
'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
'set time ${TIME}'
minute=${MI}
if ( minute = '00' )
'q dims'; line=sublin(result,5)
say 'MMMMM dfile 2 'line
t=subwrd(line,9);say 'MMMMM t='t
'set t 't+1
'q dims'; line=sublin(result,5)
say 'MMMMM dfile 2 'line

'set gxout contour'
'set cthick 10';'set ccolor 0'
'set clevs 30';'set clab off'
'd RAINRNC.2'
'set cthick 5';'set ccolor 9'
'set clevs 30';'set clab on'
'd RAINRNC.2'
endif

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2; 'draw string 'x' 'y' ${TEXT}'
x=(xl+xr)/2; y=y+0.25; 'draw string 'x' 'y' ${RUN}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=y+0.5; 'draw string ' xx ' ' yy ' ${CTL}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

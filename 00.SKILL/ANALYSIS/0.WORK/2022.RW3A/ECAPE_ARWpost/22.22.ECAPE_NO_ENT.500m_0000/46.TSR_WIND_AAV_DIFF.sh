#!/bin/bash

MM2MMM(){
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}
LEV=0.5
YYYYMMDDHH1=$1; YYYYMMDDHH1=${YYYYMMDDHH1:-2021081200}
YYYYMMDDHH2=$2; YYYYMMDDHH2=${YYYYMMDDHH2:-2021081203}

YYYY1=${YYYYMMDDHH1:0:4}; MM=${YYYYMMDDHH1:4:2}; DD1=${YYYYMMDDHH1:6:2}; HH1=${YYYYMMDDHH1:8:2}
MM2MMM 
MM1=$MM; MMM1=$MMM
YYYY2=${YYYYMMDDHH2:0:4}; MM=${YYYYMMDDHH2:4:2}; DD2=${YYYYMMDDHH2:6:2}; HH2=${YYYYMMDDHH2:8:2}
MM2MMM 
MM2=$MM; MMM2=$MMM

TIME1=${HH1}Z${DD1}${MMM1}${YYYY1}; TIME2=${HH2}Z${DD2}${MMM2}${YYYY2}
echo $TIME1 $TIME2

EXP=0702
INDIR=/work00/DATA/HD04/RW3A.00.04/10MN/ARWpost_RW3A.00.04.05.05.${EXP}.01/
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
CTL=${INDIR}/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP1=$EXP; CTL1=$CTL
#cp -vf $CTL1 .

EXP=0000
INDIR=/work00/DATA/HD04/RW3A.00.04/10MN/ARWpost_RW3A.00.04.05.05.${EXP}.01/
if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
CTL=${INDIR}/RW3A.00.04.05.05.${EXP}.01.d01.traj.10MN.ctl
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi
EXP2=$EXP; CTL2=$CTL
#cp -vf $CTL2 .

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${LEV}_RW3A.00.04.05.05.${EXP2}-${EXP}.01.PDF

RLONW=129.8 ;RLONE=130.1; RLATS=32.45;RLATN=32.75

# LEVS="-3 3 1"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
# KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
# KIND='white->lavender->cornflowerblue->dodgerblue->blue->lime->yellow->orange->red->darkmagenta'
# FS=2
UNIT=m/s

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL1}';'open ${CTL2}'

xmax = 1; ymax = 1

ytop=9

xwid = 5.0/xmax; ywid = 5.0/ymax

xmargin=0.5; ymargin=0.1

nmap = 1
ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set lev ${LEV}'
'set time ${TIME1} ${TIME2}'
say 'MMMMM MAG'
'set dfile 1'
'set lev ${LEV}'; 'set lon ${RLONW} ${RLONE}';'set lat ${RLATS} ${RLATN}'
'mag1=mag(u.1,v.1)'
say 'MMMMM AAV'
'set lon ${RLONW} ';'set lat ${RLATS}'
'AAV1=tloop(aave(mag1,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN}))'

'set dfile 2'
'set lev ${LEV}'; 'set lon ${RLONW} ${RLONE}';'set lat ${RLATS} ${RLATN}'
'set time ${TIME1} ${TIME2}'
'mag2=mag(u.2,v.2)'
say 'MMMMM AAV'
'set lon ${RLONW} ';'set lat ${RLATS}'
'AAV2=tloop(aave(mag2,lon=${RLONW},lon=${RLONE},lat=${RLATS},lat=${RLATN}))'


'set grid off';'set grads off'
'set gxout line';'set ccolor 4';'set cmark 0';'set cthick 3'
'd AAV2-AAV1'
'set xlab off';'set ylab off'
'zeroline 1 0 2 1 0'
# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

x=xl-0.7; y=(yt+yb)/2
'set strsiz 0.12 0.15'; 'set string 1 c 4 90'
'draw string 'x' 'y' Wind speed ${UNIT}'

# TEXT
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.2
'draw string 'x' 'y' ${RLONW}-${RLONE},${RLATS}-${RLATN}'
x=(xl+xr)/2; y=y+0.2
'set string 1 c 3 0'; 'draw string 'x' 'y' ${EXP2}-${EXP1}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

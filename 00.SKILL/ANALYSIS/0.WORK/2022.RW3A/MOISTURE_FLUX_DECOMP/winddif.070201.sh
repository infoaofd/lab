ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2021081406}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}
HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME0=${HH}Z${DD}${MMM}${YYYY}

FIG=$(basename $0 .sh)_${YYYYMMDDHH}.pdf
#FIG=$(basename $0 .sh).eps


LONW=126; LONE=132
LATS=30;  LATN=35
UNIT="kg/m/s"

KIND='white->wheat->orange->red->crimson->purple'
LEVS='0 400 20'



cat <<EOF>$GS

'open /work00/DATA/WRF.RW3A/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/RW3A.00.03.05.05.0000.01.d01.basic_p.01HR.ctl'

'open /work00/DATA/WRF.RW3A/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0702.01/RW3A.00.03.05.05.0702.01.d01.basic_p.01HR.ctl'

'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $TIME0'

'q dims'
say result

'cc'

'set mpdraw on'
'set mpdset hires'
'set map 1 1 6'

'set ylint 1'
'set xlint 1'

say '### wind dif'

'a=vint(lev(z=1),1.2*(U.1-U.2)*QVAPOR.2,300)'

'b=vint(lev(z=1),1.2*(V.1-V.2)*QVAPOR.2,300)'


'vap1=mag(a,b)'

'vap2=maskout(vap1,b)'


say '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd vap2'

say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.1 0.1'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'
'set ylab off'


say '### WIND'
'set gxout vector'

'set cthick 10'
'set ccolor  0'
'set arrowhead 0.15'

'vec.gs skip(maskout(a,b),20);b -SCL 0.7 300 -P 20 20 -SL kg/m/s'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
xx=xr-0.8
yy=yb-0.6

'set cthick  10'
'set ccolor  1'
'vec.gs skip(maskout(a,b),20);b -SCL 0.7 300  -P 'xx' 'yy' -SL '








say '### TITLE'
'q dims'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' '$TIME0' 1000-300 wind influence'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG



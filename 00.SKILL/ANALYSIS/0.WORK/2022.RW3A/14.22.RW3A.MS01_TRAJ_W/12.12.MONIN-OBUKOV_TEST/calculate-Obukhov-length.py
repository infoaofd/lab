"""
USAGE
 python3 calculate-Obukhov-length.py data_2020010112.grib output.grib

Running the script produces a file in the current working directory called 'output.grib' . This is a GRIB file containing the inverse of the Obukhov length. 

"""

import sys
import numpy as np
 
from eccodes import codes_count_in_file, codes_get, codes_get_values
from eccodes import codes_clone, codes_set, codes_set_values
from eccodes import codes_set_key_vals, codes_write
from eccodes import codes_grib_new_from_file, codes_release
from eccodes import CodesInternalError
 
def qswat(t, p):
    """
    Computes saturation q (with respect to water)
    In  t   : Temperature                   (K)
        p   : Pressure                      (Pa)
    Out qswt: Saturation specific humidity  (kg/kg)
    """
    rkbol = 1.380658e-23
    rnavo = 6.0221367e+23
    r = rnavo * rkbol
    rmd = 28.9644
    rmv = 18.0153
    rd = 1000 * r / rmd # Gas constant for air
    rv = 1000 * r / rmv # Gas constant for water vapour
    restt = 611.21
    r2es = restt * rd / rv
    r3les = 17.502
    r4les = 32.19
    retv = rv / rd - 1  # Tv = T(1+retv*q)
    rtt = 273.16        # Melting point (0 Celcius)
    foeew = r2es * np.exp((r3les * (t - rtt)) / (t - r4les))
    qs = foeew / p
    zcor = 1 / (1 - retv * qs)
    qs = qs * zcor
 
    return qs
 
def calculate_Obukhov_length(data_file, output_file):
    try:
        gid_list = None
        with open(data_file) as f_in:
            mcount = codes_count_in_file(f_in)
            gid_list = [codes_grib_new_from_file(f_in) for i in range(mcount)]
 
            # Input data: sp, 2d, 2t, ie, ishf, iews, inss
            data = {}
            for i in range(mcount):
                gid = gid_list[i]
                sn = codes_get(gid, 'shortName')
                vals = codes_get_values(gid) # Type: class 'numpy.ndarray'
                data[sn] = vals
 
            rd = 287.06     # Gas constant
            retv = 0.6078   # Tv = T*(1+retv*q)
            cp = 1004.7     # Air heat capacity at constant pressure
            lmelt = 2500800 # Latent heat of evaporation
            vk = 0.4        # VonKarman constant
            g = 9.81        # Gravity acceleration
 
            q2 = qswat(data['2d'], data['sp'])  # q2 is saturation value at 2d
            tv2 = data['2t'] * (1 + retv * q2)  # Virtual temperature
            rho = data['sp'] / (rd * tv2)       # Air density
            tau = np.sqrt(data['iews']**2 + data['inss']**2)
                                                # Turb. surface stress
            ust = np.maximum(np.sqrt(tau / rho), 0.001) # Friction velocity
 
            wt = -data['ishf'] / (rho * cp)     # Turbulent heat flux
            wq = -data['ie'] / rho              # Turbulent moisture flux
            wtv = wt + retv * data['2t'] * wq   # Virtual turbulent heat flux
            tvst = -wtv / ust                   # Turbulent temperature scale
            qst = -wq / ust                     # Turbulent moisture scale
            Linv = vk * g * tvst / (tv2 * ust**2)   # Inverse Obukhov length
                                                # Stull eq. (5.7c)
            Linvrange = 100.    # Range of useful Linv values to control
                                # grib-coding
            Linv = np.maximum(np.minimum(Linv, Linvrange), -Linvrange)
                                                              
            print('Linv max/min/mean: %e %e %e' %
                (Linv.max(), Linv.min(), np.abs(Linv).mean()))
         
            # Write to a file
            with open(output_file, 'wb') as f_out:
                gid = codes_clone(gid_list[0])
                codes_set(gid, 'paramId', 80) # Experimental parameter
                codes_set_values(gid, Linv)
                codes_write(gid, f_out)
                codes_release(gid)
    except CodesInternalError as ex:
        sys.stderr.write('Error: %s\n' % ex)
        sys.exit(-100)
    finally:
        if gid_list != None:
            for gid in gid_list:
                codes_release(gid)
 
def print_usage():
    print('python calculate-Obukhov-length.py data.grib output.grib')
 
def main(argv):
    if len(argv) != 2:
        print_usage()
        sys.exit(100)
 
    calculate_Obukhov_length(argv[0], argv[1])
 
if __name__ == '__main__':
    main(sys.argv[1:])


"""
https://confluence.ecmwf.int/display/CKB/ERA5%3A+How+to+calculate+Obukhov+Length#ERA5:HowtocalculateObukhovLength-Step2:ComputeinverseObukhovlength1/L

If you require the output data in netCDF, the GRIB file with the results can be converted to netCDF on your local system by using the ecCodes command grib_to_netcdf. 

Limitations 
In areas with significant orography, the inverse Obukhov length (1/L), as computed above, may be less usefull for two reasons: (a) Monin-Obkhov similarity becomes questionable in areas with orography, and (b) the ERA5 momentum fluxes, as needed for the computation of 1/L, contain the sum of turbulent flux and momentum flux due to unresolved small scale orography. The latter is parametrized by the so-called Turbulent Orographic Form Drag scheme (TOFD). To identify such areas, the retrieval job also includes parameter sdfor (standard deviation of filtered subgrid orography). It is recommended to use 1/L only in areas where sdfor < 50 m. In that case the contribution of TOFD to the total turbulent stress is small (Beljaars et al. 2004). 

References

Stull, Roland B., 1988: An Introduction to Boundary Layer Meteorology. Springer, 670 pp.

Beljaars, A., Brown, A. and Wood, N., 2004: A new parametrization of turbulent orographic form drag. QJRMS, 130, pp.1327-1347.
"""

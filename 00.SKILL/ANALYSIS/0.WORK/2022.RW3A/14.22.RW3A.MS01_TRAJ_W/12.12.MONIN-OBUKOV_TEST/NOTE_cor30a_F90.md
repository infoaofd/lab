! https://github.com/NOAA-PSL/COARE-algorithm/blob/master/Fortran/COARE3.0/f90/cor30a.F90

```fortran
von=0.4
grav=grv(lat) !9.82 
tdk=273.16 
zu=x(12) !wind speed measurement height (m)
t=x(4)   !bulk air temperature (C), height zt
u=x(1)   !wind speed (m/s)  at height zu (m)
us=x(2)  !surface current speed in the wind direction (m/s)
zt=x(13) !air T measurement height (m)
zu=x(12) !wind speed measurement height (m)
```

```fortran
ta=t+tdk 
tsr=-(dt-dter*jcool)*von*fdg/(log(zt/zot)-psit_30(zt/L)) 
Q=x(6)/1000 !bulk air spec hum (g/kg), height zq
qsr=-(dq-wetc*dter*jcool)*von*fdg/(log(zq/zot10)-psit_30(zq/L10)) 
usr=ut*von/(log(zu/zo10)-psiuo(zu/L10))
```

```fortran
du=u-us 
ug=.5 
ut=sqrt(du*du+ug*ug) 

zet=von*grav*zu/ta*(tsr*(1+0.61*Q)+.61*ta*qsr)/(usr*usr)/(1+0.61*Q) 
L=zu/zet 
```


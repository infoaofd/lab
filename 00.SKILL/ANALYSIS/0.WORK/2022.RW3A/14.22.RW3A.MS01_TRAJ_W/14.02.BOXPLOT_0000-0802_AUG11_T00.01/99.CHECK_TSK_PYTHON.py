import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import statistics

# 基本ディレクトリ
RUN="RW3A.00.04.05.05"
TRJ="T00.01"
EXP1="0000.01"
EXP2="0802.01"
BASE_DIRS = [
    RUN+"."+EXP1+".d01_"+TRJ+"/",
    RUN+"."+EXP2+".d01_"+TRJ+"/"
]

# ファイル名の範囲（24.0-25.0 から 32.0-33.0）
START_RANGES = [f"{start:.1f}-{start+1.0:.1f}" for start in np.arange(24.0, 33.0, 1.0)]

# 確認: START_RANGES と BASE_DIRS
print("START_RANGES:", START_RANGES)
print("BASE_DIRS:", BASE_DIRS)

# ファイルパスを生成
INPUT_FILES = []
COLORS = []  # 各ファイルに対応する色リスト
for range_label in START_RANGES:
    for base_dir in BASE_DIRS:
        code = base_dir.split(".")[5]
        INPUT_FILES.append(base_dir + f"RW3A.00.04.05.05.{code}.01.d01_T00.01_{range_label}.txt")

        # 確認: ファイルパス
        print(f"Generated file path: {INPUT_FILES[-1]}")

# エラーが出た場合は、ここで INPUT_FILES が正常に生成されたか確認
print("INPUT_FILES length:", len(INPUT_FILES))
print("Sample INPUT_FILES:", INPUT_FILES[:5])

# 以下にスクリプトの残りを配置
# 入力ファイルを読み込む
for file in INPUT_FILES:
    try:
        df = pd.read_csv(file, delim_whitespace=True, header=None)
        column_diff = (df[13]).dropna() - 273.15
        
        # Debug print to compare mean values
        print(file)
        print("Original column_diff:", column_diff.to_list())  # Check original data
        print("Statistics mean of column_diff:", statistics.mean(column_diff))
        
        # Append directly without np.array (for testing)
        data.append(column_diff) 
        
        print("Dataset added to data:", data[-1].to_list())  # Check stored dataset
        print("Statistics mean of dataset:", statistics.mean(data[-1]))
        print("")
    except FileNotFoundError:
        print(f"File not found: {file}")

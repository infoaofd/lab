import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import statistics

# 基本ディレクトリ
BASE_DIRS = [
    "RW3A.00.04.05.05.0000.01.d01_T00.01/",
    "RW3A.00.04.05.05.0802.01.d01_T00.01/"
]
EXP1 = BASE_DIRS[0].split(".")[5]
EXP2 = BASE_DIRS[1].split(".")[5]
# print (EXP1+" "+EXP2)

# ファイル名の範囲（24.0-25.0 から 32.0-33.0）
START_RANGES = [f"{start:.1f}-{start+1.0:.1f}" for start in np.arange(24.0, 33.0, 1.0)]

# ボックスプロットの色を指定（0000: 緑、0802: マゼンタ）
BOX_COLORS = {
    "0000": "green",
    "0802": "magenta",
}

# ファイルパスを生成
INPUT_FILES = []
COLORS = []  # 各ファイルに対応する色リスト
for range_label in START_RANGES:
    for base_dir in BASE_DIRS:
        code = base_dir.split(".")[5]
        INPUT_FILES.append(base_dir + f"RW3A.00.04.05.05.{code}.01.d01_T00.01_{range_label}.txt")
        COLORS.append(BOX_COLORS[code])

# データセットを格納するリスト
data = []

# 入力ファイルを読み込む
for file in INPUT_FILES:
    try:
        df = pd.read_csv(file, delim_whitespace=True, header=None)
        # 14列目
        column_diff = (df[13]).dropna() - 273.15
#        print('{:.2f}'.format(statistics.mean(column_diff))+" "+'{:.2f}'.format(statistics.median(column_diff)))
        data.append(np.array(column_diff, dtype=object))
    except FileNotFoundError:
        print(f"File not found: {file}")

# ボックスプロット作成
plt.figure(figsize=(2.5, 12))
# X軸の範囲を設定
plt.xlim(25, 32)

# 細い縦線を追加
plt.axvline(x=28, color='black', linestyle='--', linewidth=0.5)  # 破線で細い線
plt.axvline(x=29, color='black', linestyle='--', linewidth=0.5)  # 破線で細い線

group_spacing = 1.0
pair_spacing = 0.2
"""
positions = []
for i in range(len(START_RANGES)):
    base_pos = len(START_RANGES) - i
    positions.append(base_pos - pair_spacing / 2)  # 0802の位置
    positions.append(base_pos + pair_spacing / 2)  # 0000の位置
"""
# ここでpositionsを手動で並び替えることができます
positions = []
for i in range(len(START_RANGES)):
    base_pos = len(START_RANGES) - i
    positions.append(base_pos - pair_spacing / 2)  # greenの位置（0000）
    positions.append(base_pos + pair_spacing / 2)  # magentaの位置（0802）

# positionsリストを確認
print("Positions:", positions)

# 各ボックスプロットを個別に描画
print("Positions:", positions)

# 中央値と平均値をプロット
for pos, dataset, color, code, file in zip(positions, data, COLORS, ["0000", "0802"] * len(START_RANGES), INPUT_FILES):
    # X軸の範囲を設定
    plt.xlim(25, 32)
    # 中央値
    median_value = statistics.median(dataset)
    if code == "0000":  # 0000の場合は緑色
        plt.scatter(
            median_value,
            pos,  # ボックスプロットの位置
            facecolor='green',  # 塗りつぶしを緑色に
            edgecolor='white',  # 白枠
            s=100,  # サイズを調整
            zorder=3,  # 前面に描画
        )
    else:  # 0802の場合はマゼンタ色
        plt.scatter(
            median_value,
            pos,  # ボックスプロットの位置
            facecolor='magenta',  # 塗りつぶしをマゼンタ色に
            edgecolor='white',  # 白枠
            s=100,  # サイズを調整
            zorder=3,  # 前面に描画
        )

    # 平均値
    mean_value = statistics.mean(dataset)
    if code == "0000":  # 0000の場合は緑色
        plt.scatter(
            mean_value,
            pos,  # ボックスプロットの位置
            facecolor='green',  # 塗りつぶしを緑色に
            edgecolor='white',  # 白枠
            marker="D",  # 菱形マーカー
            s=80,  # サイズを調整
            zorder=3,  # 前面に描画
        )
    else:  # 0802の場合はマゼンタ色
        plt.scatter(
            mean_value,
            pos,  # ボックスプロットの位置
            facecolor='magenta',  # 塗りつぶしをマゼンタ色に
            edgecolor='white',  # 白枠
            marker="D",  # 菱形マーカー
            s=80,  # サイズを調整
            zorder=3,  # 前面に描画
        )
    print(file)
    print(code)
    print('{:.2f}'.format(mean_value)+" "+'{:.2f}'.format(median_value))
    print("")

# Y軸のラベルを設定（南が下、北が上）
yticks_labels = []
ytick_positions = []
for i, range_label in enumerate(START_RANGES[::-1]):  # START_RANGES[::-1] で逆順にする
    latitude_range = range_label.split('-')
    latitude_label = f"{int(float(latitude_range[0]))}-{int(float(latitude_range[1]))}N"
    yticks_labels.append(latitude_label)
    ytick_positions.append(i + 1)  # 位置を逆順にして緯度が低いほど下に来るように設定

# y軸のticks設定
plt.yticks(ytick_positions, yticks_labels, fontsize=12)
"""
    # Y軸のラベルを設定
    yticks_labels = []
    ytick_positions = []
    for i, range_label in enumerate(START_RANGES[::-1]):
        latitude_range = range_label.split('-')
        latitude_label = f"{int(float(latitude_range[0]))}-{int(float(latitude_range[1]))}N"
        yticks_labels.append(latitude_label)
        ytick_positions.append(len(START_RANGES) - i)

    plt.yticks(ytick_positions, yticks_labels, fontsize=12)
"""

# 横軸ラベルを設定
plt.xlabel(r"$\mathrm{T_s [\degree C]}$", fontsize=12)

# タイトルを設定
plt.title(r"$\mathrm{T_s}$", fontsize=14)

# スクリプト名から出力ファイル名を生成
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + "_"+ EXP1 +"_"+ EXP2+".PDF"

for pos, dataset, file in zip(positions, data, INPUT_FILES):
    mean_value = statistics.mean(dataset)
    median_value = statistics.median(dataset)
    print(f"Position: {pos:.2f}, File: {file}, Mean: {mean_value:.2f}, Median: {median_value:.2f}")
    print("")

# プロットを保存
plt.tight_layout()
plt.savefig(FIG, bbox_inches="tight")
plt.close()
print("FIG: " + FIG)

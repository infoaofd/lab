!###############################################
! Input data are
! p(hPa), t(k), rh(%)
! you will get Equivalent Potential Temperature
!###############################################
module Equiv_PT

  real, parameter :: Rv= 461.0
  real, parameter ::  L= 2.500*1e6
  real, parameter ::  p00= 1000.0

contains

  function Calc_EPT(p,t,qv)
    implicit none
    
    real p
    real t
    real rh
    real ept
    real qv
    real td
    real pi
    real pt
    real Calc_EPT
    real ti

    !##### convert from RH to Qv #####
!   qv = SS_Calc_Q2(p,rh,t)
!   write(6,*) "qv=",qv

    !##### convert from Qv to Td #####
    td = SS_Calc_Td(p,qv)
!   write(6,*) "td=",td

    !##### convert from t to pt ######
    pt = SS_Calc_PT(t,p)
!   write(6,*) "pt=",pt

    !##### LCL pressure estimate #####
    pi  = SS_ALCL(td,t,p)
!   write(6,*) "LCL height =",pi

    ti  = SS_TDA(pt,pi)

!   write(6,*) "T at LCL=", ti
    Calc_EPT = SS_OS(ti,pi)
    
    return
  end function Calc_EPT

  function SS_ALCL(tds,ts,ps)
    implicit none
    real tds
    real ts
    real ps
    integer i
    real aw,ao,pi,x
    real SS_ALCL 

    aw=SS_W(tds,ps)
    ao=SS_Calc_PT(ts,ps)
    pi=ps
    
    do i=1,10
       x=0.02*(SS_TMR(aw,pi) - SS_TDA(ao,pi))
       if(abs(x) < 0.01) goto 111
       pi = pi*(2.0**x)
    enddo

111 continue

    SS_ALCL = pi

    return

  end function SS_ALCL

  function SS_Calc_PT(t,p)
    implicit none
    real t
    real p
    real O
    real SS_Calc_PT

    SS_Calc_PT = t*(p00/p)**0.288
    return 
  end function SS_Calc_PT

  function SS_W(t,p)
    implicit none
    real x,p
    real SS_W
    real t

    x = SS_ESAT(t)
    SS_W =  622.0 * x / (p - x)

  end function SS_W

  function SS_ESAT(t)
    real ESAT
    real tmp1,tmp2,tmp3,tmp4
    real t

    tmp1 = -1.3816e-7*(10.0**(11.344-0.0303998*t))
    tmp2 =  8.1328e-3*(10.0**(3.49149-1302.8844/t))
    tmp3 = 23.832241-5.02808*log10(t)
    tmp4 = -2949.076/t
    SS_ESAT = 10.0**(tmp3+tmp1+tmp2+tmp4)
    return 
  end function SS_ESAT

  function SS_Calc_Q2(p,rh,t)
    implicit none
    integer i
    real e,es,A
    real p,rh,t,qv
    real SS_Calc_Q2
    
    A  = (t- 273.2)/(273.2*t)
    es = 6.11*exp(A*L/Rv)
    e  = rh*es /100.0
    qv = 0.622 * e/ (p - e)
   
    SS_Calc_Q2 = qv
    return
  end function SS_Calc_Q2

  function SS_Calc_Td(p,qv)
    implicit none
    integer i
    real p,qv,td
    real e,A
    real SS_Calc_Td

    e = qv*(p)/(qv+0.622)
    A =Rv/L*log(e/6.11)
    td=273.2/(1-273.2*A)
    
    SS_Calc_Td = td

    return
  end function SS_Calc_Td


  function SS_TDA(pt,p)
    implicit none
    real pt
    real p
    real SS_TDA

    SS_TDA = pt * (p/p00)**0.288

    return
  end function SS_TDA

  function SS_TMR(w,p)
    implicit none
    real t0
    real x,w,p
    real tmp1,tmp2,tmp3
    real SS_TMR

    t0 = 273.16

    x = log10(w*p/(622.0+w))
    tmp1 = 10.0**(0.0498646455*x+2.4082965)-280.23475
    tmp2 = 38.9114*((10.0**(0.0915*x))-1.2035)**2.0
    SS_TMR  = tmp1+tmp2+t0
    return 
  end function SS_TMR

  function SS_OS(t,p)
    real t,p
    real B
    real SS_OS
    
    B=2.5e6/1004.0/1000.0
    SS_OS =t*(p00/p)**0.286 * (exp(B*SS_W(t,p)/t))
    return
  end function SS_OS


end module Equiv_PT


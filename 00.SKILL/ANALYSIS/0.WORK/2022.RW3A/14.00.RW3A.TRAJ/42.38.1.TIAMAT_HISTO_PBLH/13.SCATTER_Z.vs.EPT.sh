#/bin/bash
TRAJ=T00.01
EXP1=0000;EXP2=0802
INROOT=/work04/manda/2022.RW3A/14.00.RW3A.TRAJ/42.32.TIAMAT_RW3A.TRAJ.BAK.EPT_10MN
INDIR1=$INROOT/RW3A.00.04.05.05.${EXP1}.01.d01_${TRAJ}
INDIR2=$INROOT/RW3A.00.04.05.05.${EXP2}.01.d01_${TRAJ}

INDIR=$INDIR1; if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INDIR=$INDIR2; if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INLST1=$(ls $INDIR1/RW3A.00.04.05.05.${EXP1}.01.d01_${TRAJ}_???.txt)
INLST2=$(ls $INDIR2/RW3A.00.04.05.05.${EXP2}.01.d01_${TRAJ}_???.txt)

FIG=$(basename $0 .sh)_RW3A.00.04.05.05_.01.d01_${TRAJ}.ps

. ./gmtpar.sh

lime=152/251/152;green=0/100/0;ume=221/160/221; purple=255/0/255

range=340/365/0/2
size=JX4/4
xanot=a10f5; yanot=a1f0.5; anot=$xanot:"EPT${sp}[K]":/$yanot:"Altitude${sp}[km]":WSne
psbasemap -R$range -$size -B$anot -P -K -X1 -Y3 >$FIG

for IN in $INLST1;do
awk '{print $9,$8/1000}' $IN |psxy -R -$size -Sc0.01 -G$green -O -K >>$FIG
done

for IN in $INLST2;do
awk '{print $9,$8/1000}' $IN|psxy -R -$size -Sc0.01 -G$purple -O -K >>$FIG
done

xoffset=-0.5; yoffset=5
export LANG=C
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/2 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O -P>> $FIG
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INDIR: ${INDIR}
0 0.90  9 0 1 LM FIG: ${FIG}
EOF

if [ -f $FIG ];then echo MMMMM FIG:$FIG; fi

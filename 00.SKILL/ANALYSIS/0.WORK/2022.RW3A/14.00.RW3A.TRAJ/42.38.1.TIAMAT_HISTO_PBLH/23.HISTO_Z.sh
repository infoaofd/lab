#/bin/bash
TRAJ=$1;TRAJ=${TRAJ:-T00.01}
N1=30.5;N2=32.5
EXP1=0000;EXP2=0802
INROOT=/work04/manda/2022.RW3A/14.00.RW3A.TRAJ/42.32.TIAMAT_RW3A.TRAJ.BAK.EPT_10MN
INDIR1=$INROOT/RW3A.00.04.05.05.${EXP1}.01.d01_${TRAJ}
INDIR2=$INROOT/RW3A.00.04.05.05.${EXP2}.01.d01_${TRAJ}

INDIR=$INDIR1; if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi
INDIR=$INDIR2; if [ ! -d $INDIR ];then echo NO SUCH DIR,$INDIR;exit 1;fi

INLST1=$(ls $INDIR1/RW3A.00.04.05.05.${EXP1}.01.d01_${TRAJ}_???.txt)
INLST2=$(ls $INDIR2/RW3A.00.04.05.05.${EXP2}.01.d01_${TRAJ}_???.txt)

HISTO1=$(basename $0 .sh)_RW3A.00.04.05.05.${EXP1}.01.d01_${TRAJ}.TXT
HISTO2=$(basename $0 .sh)_RW3A.00.04.05.05.${EXP2}.01.d01_${TRAJ}.TXT
INLST=$INLST1;HISTO=$HISTO1
rm -vf $HISTO
for IN in $INLST;do
awk -v N1=${N1}, -v N2=${N2} '{if(NR>1 && $7>N1 && $7<N2)print $8/1000,$7,$9}' $IN >> $HISTO
done
echo MMMMM $HISTO1

INLST=$INLST2;HISTO=$HISTO2
rm -vf $HISTO
for IN in $INLST;do
awk -v N1=${N1}, -v N2=${N2} '{if(NR>1 && $7>N1 && $7<N2)print $8/1000,$7,$9}' $IN >> $HISTO
done
echo MMMMM $HISTO2

FIG=$(basename $0 .sh)_RW3A.00.04.05.05_.01.d01_${TRAJ}.ps

. ./gmtpar.sh

lime=152/251/152;green=0/100/0;ume=221/160/221; purple=255/0/255

range=0/4/0/80;WID=.5
size=JX4/4
xanot=a1f0.5; yanot=a10f5
anot=$xanot:"Altitude${sp}[km]":/$yanot:"Frequency${sp}[%]"::.${TRAJ}${sp}${EXP1}${sp}${EXP2}${sp}${N1}N\<LAT\<${N2}N:WSne
psbasemap -R$range -$size -B$anot -P -K -X1 -Y3 >$FIG
pshistogram $HISTO2 -R -$size -W${WID} -Z1 -L6,$purple -O -K >>$FIG
pshistogram $HISTO1 -R -$size -W${WID} -Z1 -L5,$green -O -K >>$FIG

xoffset=-0.5; yoffset=5
export LANG=C
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/2 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O -P>> $FIG
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INDIR: ${INDIR}
0 0.90  9 0 1 LM FIG: ${FIG}
EOF

if [ -f $FIG ];then echo MMMMM FIG:$FIG; fi

#!/bin/bash
# Description:
# Directory: /work09/am/00.WORK/2022.RW3A/14.00.RW3A.TRAJ/32.32.RW3A.TRAJ.FWD.10MIN.01
#
. ./gmtpar.sh
gmtset ANOT_FONT_SIZE 9 LABEL_FONT_SIZE 9
gmtset HEADER_FONT_SIZE 9p HEADER_OFFSET = -0.5c

TRJ=$1; TRJ=${TRJ:-T00.01}
RUN1=RW3A.00.04.05.05.0000.01.d01
RUN2=RW3A.00.04.05.05.0802.01.d01
if [ $RUN1 = "RW3A.00.04.05.05.0000.01.d01" ];then RUNNAME1=CNTL;fi
if [ $RUN2 = "RW3A.00.04.05.05.0802.01.d01" ];then RUNNAME2="CLM AUG11-20";fi

INDIR1=${RUN1}_${TRJ}
if [ ! -d $INDIR1 ];then echo NO SUCH DIR,$INDIR1;exit 1;fi
INDIR2=${RUN2}_${TRJ}
if [ ! -d $INDIR2 ];then echo NO SUCH DIR,$INDIR2;exit 1;fi
LONW=126; LONE=131.5; LATS=24.7;LATN=33.5
#Z
ZB2=0; ZB3=0; ZB4=-3
ZT2=3; ZT3=1.5; ZT4=3

OUT=$(basename $0 .sh)_${RUN1}_${TRJ}.ps; FIG=$(basename $OUT .ps).PDF

range=${LONW}/${LONE}/${LATS}/${LATN}
size=M2
xanot=a2f1; yanot=a2f1
anot=${xanot}/${yanot}:.${RUNNAME}:WSne

XSIZE=0.6; YSIZE=3.63
XOFFSET0=2.3; XOFFSET=0.8
R2=${ZB2}/${ZT2}/${LATS}/${LATN}; S2=X${XSIZE}/${YSIZE}
xanot=a2f1; yanot=a2f1
anot2=${xanot}/${yanot}:."Z${sp}[km]":wSne

R3=${ZB3}/${ZT3}/${LATS}/${LATN};
xanot=a1f0.5; yanot=a2f1
anot3=${xanot}/${yanot}:."PBLH${sp}[km]":wSne

R4=${ZB4}/${ZT4}/${LATS}/${LATN}
xanot=a3f1; yanot=a2f1
anot4=${xanot}/${yanot}:."Z-PBLH${sp}[km]":wSne

echo MMMMM MAP
GRD1=16.GMT4_NC_TO_TEXT_DIFF_RW3A.00.04.05.05.0000-0802.01_DIFF_SST_GRD.nc
if [ ! -f $GRD1 ];then echo NO SUCH FILE,$GRD1;exit 1;fi
GRD2=06.TEST_GMT4_NC_TO_TEXT_RW3A.00.04.05.05.0802.01_SST_GRD.nc
if [ ! -f $GRD2 ];then echo NO SUCH FILE,$GRD2;exit 1;fi
CPT=$(basename $0 .sh)_CPT.txt; makecpt -Cpolar -T-2/2/0.5 -Z > $CPT
if [ ! -f $CPT ];then echo NO SUCH FILE,$CPT;exit 1;fi
grdimage $GRD1 -R$range -J$size -C$CPT -X0.5 -Y1.5 -K >$OUT
psscale -D1/-0.5/2/0.1h -C$CPT -Ba1f1 -O -K >>$OUT

gmtset D_FORMAT %3.0f
grdcontour $GRD2 -R$range -J$size -A1f8 -C1 -W1 -O -K >>$OUT
pscoast  -R$range -J$size -B$anot -Df -W1 -G150 -O -K  >>$OUT

lime=152/251/152;green=0/100/0;ume=221/160/221; purple=255/0/255

INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $6, $7}' $IN |psxy -R -J -W1/${ume} -O -K >>$OUT
done #IN

INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $6, $7}' $IN |psxy -R -J -W1/${lime} -O -K>>$OUT
done #IN

psbasemap -R -JM -B$anot -K -O >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R2} -J${S2} -B${anot2} -K -O -X${XOFFSET0} >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)

for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $8/1000, $7}' $IN |psxy -R -J -W1/${ume} -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $8/1000, $7}' $IN |psxy -R -J -W1/${lime} -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $8/1000,$7}' $IN |psxy -R -J -W5/${purple} -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $8/1000,$7}' $IN |psxy -R -J -W5/${green} -O -K >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R3} -J${S2} -B${anot3} -K -O -X${XOFFSET} >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $17/1000, $7}' $IN |psxy -R -J -W1/$ume -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $17/1000, $7}' $IN |psxy -R -J -W1/${lime} -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $17/1000,$7}' $IN |psxy -R -J -W5/$purple -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print $17/1000,$7}' $IN |psxy -R -J -W5/$green -O -K >>$OUT



echo MMMMM Y-Z SECTION
psbasemap -R${R4} -J${S2} -B${anot4} -K -O -X${XOFFSET} >>$OUT
INLIST=$(ls $INDIR2/${RUN2}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print ($8-$17)/1000, $7}' $IN |psxy -R -J -W1/$ume -O -K >>$OUT
done #IN
INLIST=$(ls $INDIR1/${RUN1}*.txt)
for IN in $INLIST;do
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print ($8-$17)/1000, $7}' $IN |psxy -R -J -W1/${lime} -O -K >>$OUT
done #IN

IN=${INDIR2}/AVE_${RUN2}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print ($8-$17)/1000,$7}' $IN |psxy -R -J -W5/$purple -O -K >>$OUT

IN=${INDIR1}/AVE_${RUN1}_${TRJ}.txt
if [ ! -f $IN ];then echo NO SUCH FILE,$IN;exit 1;fi
awk '{print ($8-$17)/1000,$7}' $IN |psxy -R -J -W5/$green -O -K >>$OUT

xoffset=-4; yoffset=3.4
curdir1=$(pwd); now=$(date -R); host=$(hostname)
pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INDIR: ${INDIR}
0 0.90  9 0 1 LM OUTPUT: ${OUT}
EOF

rm -vf $CPT
echo "INDIR: $INDIR"
if [ -f $OUT ];then rm -vf $FIG; echo MMMMM $OUT to $FIG; ps2pdfwr $OUT $FIG; fi
if [ -f $FIG ];then rm -vf $OUT; echo FIG: $FIG; fi


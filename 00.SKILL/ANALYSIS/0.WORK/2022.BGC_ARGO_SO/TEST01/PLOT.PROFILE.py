# coding: utf-8

import numpy as np
import xarray as xr  #netCDF4を使う人も多いです

INDIR="/work01/DATA/ARGO"
INFLE="BR5905194_070.nc"
#INFLE="BD1901378_181.nc"
IN=INDIR+"/"+INFLE

nc = xr.open_dataset(IN)

lat = nc['LATITUDE']
lon = nc['LONGITUDE']
date = nc['JULD']
p = nc['PRES'][0,:]
BBP700 = nc['BBP700'][0,:]
#T = nc['TEMP_DOXY'][0,:]
#S = nc['PSAL_ADJUSTED'][:]


### 余計な次元は削減 ###
p = np.squeeze(p)
BBP700 = np.squeeze(BBP700)
#S = np.squeeze(S)
print (p)
print (BBP700)

### 鉛直プロファイルを書いてみよう! ###
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(9,6))
ax = fig.add_subplot(1,2,1)
ax.plot(BBP700,p,color='k')
ax.set_xlabel('BBP700 [m-1]',fontsize=13)
#ax.set_xlabel('In-situ temperature [$\circ$C]',fontsize=13)
ax.set_ylabel('Pressure [dBar]',fontsize=13)
ax.tick_params(axis='both',labelsize=13)
ax.grid(which='major',color='gray',linestyle='dashed')
ax.invert_yaxis()

"""
ax = fig.add_subplot(1,2,2)
ax.plot(S,p,color='k')
ax.set_xlabel('Salinity [psu]',fontsize=13)
ax.set_ylabel('Pressure [dBar]',fontsize=13)
ax.tick_params(axis='both',labelsize=13)
ax.grid(which='major',color='gray',linestyle='dashed')
ax.invert_yaxis()
"""

plt.tight_layout()
#plt.show()

import matplotlib.pyplot as plt
plt.savefig("20200701.pdf")



ncFiles = dir('*.nc'); 
numfiles = length(ncFiles);
mydata = cell(1, numfiles);

for k = 1:numfiles 

  mydata{k} = imread(ncFiles(k).name); 

end

%ncid = netcdf.open('C:\DATA\ARGO\BD5904767_022.nc')
ncid = netcdf.open(C:\DATA\ARGO\*.nc)

varid1 = netcdf.inqVarID(ncid,"BBP700")
bbp = netcdf.getVar(ncid,varid1)

varid2 = netcdf.inqVarID(ncid,"PRES")
p = netcdf.getVar(ncid,varid2)

bbp1 = bbp(:,1)
  p1 = p(:,1)

%xerr = 1

%pr = 1

%max_iter = 100

%file = "C:\DATA\ARGO\BD5904767_022.nc";
%file = "C:\DATA\ARGO\*.nc";
%name = fileparts(file)


[spikes, varargout] = get_spikes_bbp700(bbp1, p1) %, xerr, pr, max_iter)

function [spikes, varargout] = get_spikes_bbp700(bbp1, p1, xerr, pr, max_iter)

DEBUG = true%false;

% Check input
if nargin < 2 || isempty(p1); p_flag = false; else; p_flag = true; end
if nargin < 3 || isempty(xerr); xerr_flag = false; xerr = NaN; else; xerr_flag = true; end
if nargin < 4 || isempty(pr); pr_flag = false; else; pr_flag = true; end % ignored if ~p_flag
if nargin < 5 || isempty(max_iter); max_iter = 100; end


if size(bbp1,2) ~= 1; error('x is not a row vector (Nx1).'); end
if p_flag && any(size(p1) ~= size(bbp1)); error('Input vectors are different size.'); end

% Check output
if nargout > 2 || (nargout == 2 && ~p_flag); neg_spike_flag = true; else; neg_spike_flag = false; end


% Live debug
if DEBUG
  figure;
  subplot(1,3,2); hold on;
  title('figure2')
  plot(bbp1,p1,'.-'); 
end

% Prepare input
spikes = false(size(bbp1));
if p_flag
  % ignore nan of x and p for processing
  sel = find(~isnan(bbp1) & ~isnan(p1) & ~isinf(bbp1) & ~isinf(p1));
  if length(sel) < 3
    warning('GET_SPIKES:EMPTY_INPUT', 'get_spikes_bbp700: Not enough valid values.');
    if nargout == 2 && ~p_flag; varargout = {spikes}; else; varargout = {[], spikes, []}; end
    return;
  end
  p1 = p1(sel);
  % ignore low sampling resolution of profile
  delta = abs([diff(p1(1:2)); (p1(3:end) - p1(1:end-2)) / 2; diff(p1(end-1:end))]); % # / dBar
  if ~pr_flag; pr = 3 * median(delta); end
  subsel = delta < pr;
  sel = sel(subsel);
  if DEBUG
    subplot(1,3,1);
    title('figure1')
    plot(delta, p1, '.-');
    set(gca, 'ydir', 'reverse');
  end
  % keep only relevant sampling
  bbp1 = bbp1(sel); p1 = p1(subsel);
else
  % only x
  sel = find(~isnan(bbp1) & ~isinf(bbp1));
  bbp1 = bbp1(sel);
end

% Apply Hampel filter (same as median filter except onlx change values of spikes)
xd = hampel(bbp1, 15); % > 5 is good
% Recursive Hampel filter
xdo = bbp1; i = 0;
while any(xd ~= xdo) && i < max_iter
  xdo = xd; 
  xd = hampel(xd, 15);
  i = i + 1;
end
if i == max_iter; warning('GET_SPIKES:MAX_ITER', 'get_spikes_bbp700: Maximum iteration (%d) reached.', max_iter); end

if ~xerr_flag
  % Compute Scaled Median Absolute Deviation (MAD)
  smad = -1/(sqrt(2)*erfcinv(3/2)) * median(abs(bbp1-median(bbp1)));
  % Noisy signal (no significant spike)
  if smad == 0
    % format output & return
    if nargout == 2 && ~p_flag; varargout = {spikes}; else; varargout = {[], spikes, []}; end
    return;
   end
  % Set spike detection threshold to 3 scaled MAD
  xerr = 3 * smad;
end

% Get spikes
x_delta = bbp1 - xd;
spikes(sel) = x_delta > xerr;

% Live debug
if DEBUG
  subplot(1,3,2);
  title('figure2')
  plot(xd, p1, '.-');
  plot(bbp1(spikes(sel)), p1(spikes(sel)), 'o');
  set(gca, 'ydir', 'reverse');
  y_lim = ylim();
  subplot(1,3,3); hold('on');
  title('figure3')
  plot(x_delta, p1, '.-');
  plot(xerr .* ones(2,1), ylim(), 'k', 'LineWidth', 2);
  ylim(y_lim);
  set(gca, 'ydir', 'reverse');
  xlim([0 0.01]);
%   set(gca, 'xscale', 'log');
  drawnow();
  waitforbuttonpress();
%   pause(1);
end

% Get negative spikes
if neg_spike_flag
  neg_spikes = false(size(spikes));
  neg_spikes(sel) = -x_delta > xerr;
end

% Set output
if p_flag
  switch nargout
    case 2
      varargout = {p1(spikes(sel))};
    case 3
      varargout = {p1(spikes(sel)), neg_spikes};
    case 4
      varargout = {p1(spikes(sel)), neg_spikes, p1(neg_spikes(sel))};
  end
else
  varargout = {neg_spikes};
end

saveas(gcf,name.pdf)


end
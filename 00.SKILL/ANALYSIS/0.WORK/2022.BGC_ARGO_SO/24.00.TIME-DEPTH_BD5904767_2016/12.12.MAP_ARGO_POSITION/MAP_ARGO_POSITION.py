# coding: utf-8
# 日本語を使いたい時はcoding: utf-8をファイルの先頭に入れる

"""
ライブラリのインポート
"""
import numpy as np # 数値計算のためのライブラリ
import xarray as xr  #netCDF4を読み込むのに使用する
import sys #python上でLinuxコマンドを用いるためのライブラリ
# https://note.nkmk.me/python-command-line-arguments/
import pandas as pd
import datetime

INDIR="/work01/DATA/ARGO/"
INFLE=["BD5904983_001", "BD5904983_002", "BD5904983_003",
"BD5904983_004", "BD5904983_005", "BD5904983_006",
"BD5904983_007", "BD5904983_008", "BD5904983_009",
"BD5904983_010", "BD5904983_011", "BD5904983_012",
"BD5904983_013", "BD5904983_014", "BD5904983_015",
"BD5904983_016", "BD5904983_017", "BD5904983_018",
"BD5904983_019", "BD5904983_020", "BD5904983_021",
"BD5904983_022", "BD5904983_023", "BD5904983_024"]
NF=len(INFLE)

OUT="BD5904983_LOC_MAP.PDF"

lat=[]
lon=[]
"""
データの読み込み
"""
for i in range(NF):
  IN=INDIR+"/"+INFLE[i]+".nc"
  print(IN)
  nc = xr.open_dataset(IN) 
# 変数INに記憶されているファイル名のファイルを開く
# 開いたファイルのポインターはncとする
  lat.append( float(nc['LATITUDE'][0]) )
# ファイルncに含まれるLATITUDEという変数を変数latに代入する
  lon.append( float(nc['LONGITUDE'][0]) )
# ファイルncに含まれるLONGITUDEという変数を変数lonに代入する
#  print(str(lon)+" "+str(lat))

  julday = pd.to_datetime(nc['JULD'])
# ファイルncに含まれるJULDという変数を変数juldayに代入する
# JULDはJulian day （ユリウス日）の意味
# 日付を抜き出す
# https://note.nkmk.me/python-pandas-datetime-timestamp/
  date=julday[0]

  xr.Dataset.close(nc)


"""
地図に観測点の位置を示す点を打つ
https://qiita.com/earth06/items/d41b74f3e5504d5c74a3
"""
import cartopy.crs as ccrs
import cartopy.feature as cfea
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(9,6))

#ax = fig.add_subplot(1,2,2,projection=ccrs.LambertConformal(central_longitude=-75.,central_latitude=-70.))
ax=fig.add_subplot(1,1,1,projection=ccrs.NearsidePerspective(
    central_longitude=137.0,
    central_latitude=-90.0,
    satellite_height=100000000,
    false_easting=0,
    false_northing=0,
    globe=None))
ax.set_extent([-80.,-60.,-70., -60.],ccrs.PlateCarree())
ax.coastlines(resolution='50m')
ax.add_feature(cfea.LAND,color='#32CD32')

# 測点番号の設定
stn=list(range(NF))

# 地図上に点を書く
cm=plt.get_cmap('jet')
mappable = plt.scatter(lon, lat, c=stn,cmap=cm,alpha=0.7,s=50, transform=ccrs.PlateCarree())

# カラーバーを付加
fig.colorbar(mappable, ax=ax)

#測点番号を地図に入れる
for i in range(0,NF,23):
#  ax.plot(lon[i],lat[i],'ro',markersize='4', transform=ccrs.PlateCarree())
# 正距円筒図法以外で作図する場合 transform=ccrs.PlateCarree()をつける
# https://yyousuke.github.io/matplotlib/cartopy.html
  ax.text(lon[i],lat[i], str(i+1),fontsize=20, 
  verticalalignment='bottom', horizontalalignment='left',
  transform=ccrs.PlateCarree())

gl=ax.gridlines(draw_labels=True,linestyle='--',xlocs=plt.MultipleLocator(5)
             ,ylocs=plt.MultipleLocator(10))
gl.xlabel_style={'size':8,'color':'black'}
gl.ylabel_style={'size':8,'color':'black'}
#ax.set_extent([-100,-40,-90,-60],ccrs.PlateCarree())

plt.title(str(lat)+" "+str(lon))

plt.tight_layout()

import matplotlib.pyplot as plt
plt.savefig(OUT) #図をファイルに保存する

print("OUT: "+OUT)





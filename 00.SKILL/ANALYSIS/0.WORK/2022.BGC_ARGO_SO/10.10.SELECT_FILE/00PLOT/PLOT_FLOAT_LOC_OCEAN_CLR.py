"""
LOCATIONS OF THE BGC ARGO FLOATS
MONTLY CLIMAT OF OCEAN COLOR 
"""

INDIR1="/work02/DATA/OCEAN-COLOR/"
INFLE1="CHL_A_CLIM_JAN_r360x181.nc"
#INFLE1="ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc"
INDIR2="../"
INFLE2="03-2.PRINT_LON_LAT_EDIT.CSV" 

import matplotlib.pyplot as plt 
# standard graphics library
import xarray
import cartopy.crs as ccrs 
# cartographic coordinate reference system
import cartopy.feature as cfeature 
# features such as land, borders, coastlines

DSET1=xarray.open_dataset(INDIR1+INFLE1,decode_times=True)

CHL = DSET1.chlor_a[0:-1,:] 

print(CHL)

plt.figure(dpi=300)

#plt.imshow(CHL),cmap='gist_ncar'); 
ax=plt.axes(projection=ccrs.Mercator())
ax.add_feature(cfeature.LAND)
 
# fill in the land areas
ax.coastlines() 
# use the default low-resolution coastline
gl=ax.gridlines(draw_labels=True) 
# default is to label all axes.
gl.xlabels_top=False 
# turn off two of them.
gl.ylabels_right=False

CHL.plot(x='lon',y='lat',cmap='gist_ncar',vmin=CHL.min(),vmax=CHL.max(),transform=ccrs.PlateCarree())

plt.savefig("CHL_CLM_01.pdf")



; https://www.atmos.rcast.u-tokyo.ac.jp/shion/NCLtips/
; https://gitlab.com/infoaofd/lab/-/blob/master/NCL/NCL_QUICK_REF.md

INDIR="/work02/DATA/OCEAN-COLOR/"
INFLE="CHL_A_CLIM_JAN_r360x181.nc"
;INFLE="ESACCI-OC-MAPPED-CLIMATOLOGY-1M_MONTHLY_4km_PML_OCx_QAA-01-fv6.0.nc"
IN1=INDIR+INFLE



a=addfile(IN,"r")

CHL=a->chlor_a(0,:,:)
FIG="CHL-A_CLIM_JAN"
TYP="pdf"
wks = gsn_open_wks(TYP,FIG)

gsn_define_colormap(wks,"MPL_BuGn")

res            = True           
res@gsnPolar   = "SH"          

res@gsnDraw  = False
res@gsnFrame = False

res@cnFillOn = True
res@cnLinesOn = False

res@mpMaxLatF =  -30     ; 緯度の最大値

res@mpGridAndLimbOn   = False

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 0.1
res@cnMaxLevelValF = 1.0
res@cnLevelSpacingF = 0.1

plot = gsn_csm_contour_map_polar(wks,CHL,res) 

draw(plot)
frame(wks)

print("FIG: "+FIG+"."+TYP)



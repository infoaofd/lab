IN=01.BBP_RESULT.TXT
OUT=$(basename $0 .sh)_RESULT.TXT

<<COMMENT
CNT1=csiro;   TMP1=$(basename $0 .sh)_${CNT1}.TXT
CNT2=incois; TMP2=$(basename $0 .sh)_${CNT2}TXT
CNT3=jma;    TMP3=$(basename $0 .sh)_${CNT3}.TXT

grep $CNT1  $IN > $TMP1
grep $CNT2  $IN > $TMP2
grep $CNT3  $IN > $TMP3
COMMENT

awk -F'[, ]' '{if ($3 <-60) print $1}' $IN > $OUT
#awk -F'[, ]' '{if ($3 <-60) print $1, $3,$4}' $IN > $OUT

echo; echo INPUT: $IN

<<COMMENT
echo OUTPUT : $TMP1
echo OUTPUT : $TMP2
echo OUTPUT : $TMP3
COMMENT

echo OUTPUT: $OUT
echo
 

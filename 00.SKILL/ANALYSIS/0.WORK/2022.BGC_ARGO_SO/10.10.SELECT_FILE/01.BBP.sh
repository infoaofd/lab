IN=argo_bio-profile_index.txt
OUT=$(basename $0 .sh)_RESULT.TXT
TMP=TMP.TXT

grep BBP $IN > $TMP
grep FLUORESCENCE_CHLA $TMP >$OUT

rm $TMP

echo INPUT: $IN
echo OUTPUT: $OUT


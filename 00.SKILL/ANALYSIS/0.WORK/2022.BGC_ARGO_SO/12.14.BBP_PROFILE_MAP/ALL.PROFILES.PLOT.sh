#!/bin/bash

EXE=PLOT.PROFILE.MAP.py
if [ ! -f $EXE ]; then echo ERROR NO SUCH FILE,$EXE; exit 1;fi
# EXE (=PLOT.PROFILE.MAP.py)が無かったらエラーメッセージを表示
# して終了

INDIR=/work01/DATA/ARGO # 入力ファイルのあるディレクトリ名

LIST=(`ls ${INDIR}/*.nc`)
# コマンドを``で囲んで、かっこでくくるとコマンドの
# 結果を配列に入れることができる。
# https://blog.suganoo.net/entry/2019/08/14/170531

NFLE=${#LIST[*]} #配列の要素数
echo NUMBER OF FILES: $NFLE
# https://maku77.github.io/linux/var/array-size.html

ODIR=OUT_$(basename $0 .sh)
# https://webkaru.net/linux/basename-command/
mkd $ODIR

I=0; ISTEP=500

while [ $I -le $NFLE ];do

if [ $I -gt 0 ]; then
IS=$(expr $I + 1)
else
IS=0
fi

SSSS=$(printf %04d $IS)
IE=$(expr $I + $ISTEP)
EEEE=$(printf %04d $IE)

ODIR="OUT_$(basename $0 .sh)/$SSSS-$EEEE"

I=$(expr $I + $ISTEP)
done #I

# Linuxでのドル記号「$(...)」の意味
#  https://uxmilk.jp/27666
# Bashのwhile文
#  https://yu-nix.com/archives/bash-while/
# printf
#  https://linuxcommand.net/printf/
# expr
#  https://www.softel.co.jp/blogs/tech/archives/3418

I=0
while [ $I -le $NFLE ];do

if [ $I -le 500 ]; then
I1=0
I2=500
else
I1=$(expr  $I / 500  \* 500 + 1)
I2=$(expr $I1 + $ISTEP - 1)
fi

SSSS=$(printf %04d $I1)
EEEE=$(printf %04d $I2)

ODIR="OUT_$(basename $0 .sh)/$SSSS-$EEEE"
if [ ! -d $ODIR ];then mkdir -vp $ODIR; fi

#echo $I ${LIST[$I]} ${ODIR}
INFLE=$(basename ${LIST[$I]})
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1;fi

OFLE=$(basename $EXE .py)_$(basename $INFLE .nc).PDF

echo $(printf %04d $I) python3 $EXE $INDIR $INFLE $ODIR $OFLE
python3 $EXE $INDIR $INFLE $ODIR $OFLE #pythonスクリプトの実行

if [ $? -eq 0 ];then
echo INDIR: $INDIR #入力ディレクトリ名の画面表示
echo INFLE: $INFLE #入力ファイル名の画面表示
echo ODIR: $ODIR #出力ディレクトリ名の画面表示
echo OFLE: $OFLE #出力ファイル名の画面表示
echo
else
echo EEEEE ABNORMAL END! #pythonスクリプトが異常終了した場合
echo EEEEE INDIR: $INDIR #入力ディレクトリ名の画面表示
echo EEEEE INFLE: $INFLE #入力ファイル名の画面表示
echo
fi

I=$(expr $I + 1)
done #I

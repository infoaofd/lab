# coding: utf-8
# 日本語を使いたい時はcoding: utf-8をファイルの先頭に入れる

"""
ライブラリのインポート
"""
import numpy as np # 数値計算のためのライブラリ
import xarray as xr  #netCDF4を読み込むのに使用する
import sys #python上でLinuxコマンドを用いるためのライブラリ
# https://note.nkmk.me/python-command-line-arguments/
import pandas as pd
import datetime

"""
コマインドライン引数の読み込み
"""
INDIR=sys.argv[1] #1番目の引数を変数INDIRに代入する
INFLE=sys.argv[2] #2番目の引数を変数INFLERに代入する
ODIR=sys.argv[3] #3番目の引数を変数INDIRに代入する
OFLE=sys.argv[4] #4番目の引数を変数INDIRに代入する

"""
入力ファイル名と出力ファイル名の設定
"""
IN=INDIR+"/"+INFLE #INDIRとINFLEをつなげたものを変数INに代入する
OUT=ODIR+"/"+OFLE #ODIRとOFLEをつなげたものを変数OUTに代入する


"""
データの読み込み
"""
nc = xr.open_dataset(IN) 
# 変数INに記憶されているファイル名のファイルを開く
# 開いたファイルのポインターはncとする

lat = float(nc['LATITUDE'][0]) 
# ファイルncに含まれるLATITUDEという変数を変数latに代入する
lon = float(nc['LONGITUDE'][0])
# ファイルncに含まれるLONGITUDEという変数を変数lonに代入する

# ファイルncに含まれるJULDという変数を変数juldayに代入する
# JULDはJulian day （ユリウス日）の意味



"""
データの抜き出し
"""
julday = pd.to_datetime(nc['JULD'])
# 日付を抜き出す
# https://note.nkmk.me/python-pandas-datetime-timestamp/
date=julday[0]

p = nc['PRES'][0,:]
# ファイルncに含まれるPRESという変数を変数pに代入する

BBP700 = nc['BBP700'][0,:]
# ファイルncに含まれるBBP700という変数を変数BBP700に代入する

# 余計な次元を削減
p = np.squeeze(p)
BBP700 = np.squeeze(BBP700)

# BBPデータが全部欠測値である場合は強制終了
if (all(np.isnan(BBP700))):
	print("EEEEE ALL BBP700 ARE NAN (NOT A NUMBER).")
	sys.exit(1) #強制終了
# NaN (Not a number)の判定 (isnan)
#   https://note.nkmk.me/python-numpy-nan-remove/


"""
鉛直プロファイルを書く
"""
import matplotlib.pyplot as plt
fig = plt.figure(figsize=(9,6))
ax = fig.add_subplot(1,2,1)
ax.plot(BBP700,p,color='k')
ax.set_xlabel('BBP700 [m-1]',fontsize=12)
ax.set_ylabel('Pressure [dBar]',fontsize=12)
ax.tick_params(axis='both',labelsize=12)
ax.grid(which='major',color='gray',linestyle='dashed')
ax.invert_yaxis()
plt.title(date) #図のタイトルをdate(UTC表示の日付・時刻)にする
# 

"""
地図を書く
https://qiita.com/earth06/items/d41b74f3e5504d5c74a3
"""
import cartopy.crs as ccrs
ax=fig.add_subplot(1,2,2,projection=ccrs.SouthPolarStereo(central_longitude=180))
ax.coastlines(resolution='50m')

# 地図上に点を書く
ax.plot(lon,lat,'ro',markersize='4', transform=ccrs.PlateCarree())
# 正距円筒図法以外で作図する場合 transform=ccrs.PlateCarree()をつける
# https://yyousuke.github.io/matplotlib/cartopy.html

gl=ax.gridlines(draw_labels=True,linestyle='--',xlocs=plt.MultipleLocator(20)
             ,ylocs=plt.MultipleLocator(15))
gl.xlabel_style={'size':8,'color':'black'}
gl.ylabel_style={'size':8,'color':'black'}
ax.set_extent([-180,180.1,-90,0],ccrs.PlateCarree())
plt.title(str(lat)+" "+str(lon))

plt.tight_layout()

import matplotlib.pyplot as plt
plt.savefig(OUT) #図をファイルに保存する

print("OUT: "+OUT)

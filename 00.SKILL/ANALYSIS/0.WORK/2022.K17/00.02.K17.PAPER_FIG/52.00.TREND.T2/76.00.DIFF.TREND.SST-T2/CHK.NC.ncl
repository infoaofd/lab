; CHK.NC.ncl
; 
; Tue, 12 Jul 2022 11:43:01 +0900
; calypso.bosai.go.jp
; /work04/manda/2022.K17/00.02.K17.PAPER_FIG/52.00.TREND.T2/10.02.PRE/12.00.NCEP1/12.00.CDO.SELMON.7
; manda
;
IN="T2.TREND_ON.SST.GRID_EAV.07.1x1.1982-2017_THEIL-SEN"
INFLE=IN+".nc"
TYP="eps"
FIG1=IN

script_name  = get_script_name()
script=systemfunc("basename "+script_name+ " .ncl")
LOG=script+".LOG"

NOW=systemfunc("date -R")
HOST=systemfunc("hostname")
CWD=systemfunc("pwd")



a = addfile(INFLE,"r")
t = a->trend                                ; read July zonal winds

wks = gsn_open_wks(TYP,FIG1)          ; send graphics to PNG file

res                   = True
res@cnFillPalette = (/\
"dodgerblue","lightskyblue","azure1","white","antiquewhite","khaki1","yellow","gold","orange","red","firebrick"/)
res@cnLevelSelectionMode = "ExplicitLevels"
res@cnLevels = (/-0.4,-0.2,0,0.2,0.4,0.6,0.8,1.0,1.2,2./)

res@gsnFrame = False
res@gsnDraw  = False

  res@cnLinesOn       = False
  res@cnFillOn        = True

;  res@mpProjection      = "Mollweide"       ; choose projection
res@mpGridAndLimbOn   = True              ; turn on lat/lon lines
res@mpPerimOn         = False             ; turn off box around plot
res@mpGridLatSpacingF = 30.               ; spacing for lat lines
res@mpGridLonSpacingF = 30.               ; spacing for lon lines

  res@cnLinesOn       = False
;res@cnFillPalette     = "gui_default"     ; set color map
res@cnLineLabelsOn    = False             ; turn off contour lines
res@txFontHeightF     = 0.015 

  res@mpFillDrawOrder       = "PostDraw"     ; draw map fill last
  res@mpLandFillColor = 1 ;164


res@vpXF            = 0.1                 ; make plot bigger
res@vpYF            = 0.9         
res@vpWidthF        = 0.8
res@vpHeightF       = 0.8

res@lbLabelFontHeightF  = 0.015           ; label bar font height

res@tiMainString       = FIG1
res@tiMainFontHeightF  = .018                               ; font height

plot = gsn_csm_contour_map(wks,t,res)  ; create the plot

draw(plot)

xh=0.1   ;Left of header lines
yh=0.9   ;Top of header lines
dyh=0.02 ;Line spacing
fh=0.01  ;Font Height

txres               = True
txres@txFontHeightF = fh

txres@txJust="CenterLeft"

today = systemfunc("date -R")
cwd =systemfunc("pwd")
lst = systemfunc("ls -lh --time-style=long-iso "+INFLE)


gsn_text_ndc(wks,today,  xh,yh,txres)
yh=yh-dyh
gsn_text_ndc(wks,"Current dir: "+cwd,      xh,yh,txres)

yh=yh-dyh
gsn_text_ndc(wks,lst,xh,yh,txres)

yh=yh-dyh
gsn_text_ndc(wks,"Output: "+FIG1+"."+TYP, xh,yh,txres)

frame(wks)
delete([/t,a,res/])
print("MMMMMM "+FIG1+"."+TYP)

print("")
print("Done " + script_name)
print("MMMMMM "+FIG1+"."+TYP)

print("")

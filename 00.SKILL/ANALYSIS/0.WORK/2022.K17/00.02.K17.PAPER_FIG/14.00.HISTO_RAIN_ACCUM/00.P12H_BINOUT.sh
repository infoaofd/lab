#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-h][-d][-b] ; exit 0
}

flagh="false"; flagd="false"; flagb="false"
while getopts hdl OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true";;
    "b" ) flagg="true";;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

GS=$(basename $0 .sh).GS

yyyy=2017; mm=07; dd=05; dp=$(expr $dd + 1)

# HOVMOLLER
hs=00; he=12; dh=1
lonw=130.5 ;lone=132; lats=33 ; latn=34

# MAP
hsm=00; hem=12
mlonw=130; mlone=132; mlats=33; mlatn=34

runname=$1; runname=${runname:-K17.R33.00.00}

echo; echo "RUNNAME = $runname"; echo 


INTERVAL=10MN; domain=d03; TYPE=basic_z
indir=/data07/thotspot/zamanda/K17.ARWpost/${INTERVAL}/${TYPE}/ARWpost_${runname}/
if [ ! -d $indir ]; then echo ERROR NO SUCH DIR, $indir; exit 1; fi

figdir=OUT_$(basename $0 .sh)
$(which mkd) $figdir

figfile=${figdir}/$(basename $0 .sh)_${runname}.${domain}_${yyyy}${mm}${dd}_${hs}-${he}.eps

BIN=${figdir}/$(basename $0 .sh)_${runname}.${domain}_${yyyy}${mm}${dd}_${hs}-${he}.BIN

CTLDIR=CTL
ctl=${indir}/${runname}.${domain}.${TYPE}.${INTERVAL}.ctl
if [ ! -f $ctl ]; then echo ERROR NO SUCH FILE, $ctl; exit 1; fi


xsm=0.8; xem=5.0; ysm=3; yem=7.0

xs=6; xe=10

host=$(hostname); cwd=$(pwd); timestamp=$(date -R)
CMD=$(basename $0)



cat <<EOF>$GS
'cc'
'set grads off'
'set xlopts 1 2 0.12'; 'set ylopts 1 2 0.12'

say; say 'OPEN ${ctl}'; say

'open ${ctl}'

yyyy=${yyyy}

mm=${mm}
if(mm='01');mmm='JAN';endif; if(mm='02');mmm='FEB';endif
if(mm='03');mmm='MAR';endif; if(mm='04');mmm='APR';endif
if(mm='05');mmm='MAY';endif; if(mm='06');mmm='JUN';endif
if(mm='07');mmm='JUL';endif; if(mm='08');mmm='AUG';endif
if(mm='09');mmm='SEP';endif; if(mm='10');mmm='OCT';endif
if(mm='11');mmm='NOV';endif; if(mm='12');mmm='DEC';endif



datetime1=${hs}'Z'${dd}''mmm''yyyy; datetime2=${he}'Z'${dd}''mmm''yyyy
dtime1map=${hsm}'Z'${dd}''mmm''yyyy; dtime2map=${hem}'Z'${dd}''mmm''yyyy

kind='white->lavender->cornflowerblue->dodgerblue->blue->yellow->orange->red->darkmagenta'

clevs='5 10 15 20 25 30 35 40'

clevsmap='5 50 100 150 200 250 300 350 400 450 500 550 600 650 700'

'set vpage 0.0 11.0 0.0 8.5'

# MAP
'set xyrev off'
'set parea ${xsm} ${xem} ${ysm} ${yem}'
'color -levs ' clevsmap ' -gxout shaded -kind ' kind

'set lon ${mlonw} ${mlone}'; 'set lat ${mlats} ${mlatn}'

say 'mmmmmm MAP RAIN'
say datetime1' 'datetime2; say dtime1map' 'dtime2map; say

'set time 'dtime1map; 'r1=RAINNC'; 'set time 'dtime2map; 'r2=RAINNC'
'ra=r2-r1'

'set grid off' ;*on 3 15 1'
'set xlint 0.5'; 'set ylint 0.2'
'set mpdraw on'; 'set mpdset worldmap' ;#'set mpdraw off'
'd 'ra

#'set mpdraw on';# 'set mpdset hires';# 'draw shp JPN_adm1'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

y1=yb-0.5; y2=y1+0.1

'color -levs ' clevsmap ' -gxout shaded -kind ' kind ' -xcbar 'xl' 'xr' 'y1' 'y2' -edge triangle -line on -ft 2 -fs 2'

xx=(xl+xr)/2; yy=yt+0.2
'set string 1 c 3 0'; 'set strsiz 0.12 0.15'
title=dtime1map'-'dtime2map
'draw string 'xx' 'yy' 'title

#'set string 1 c 3 0'
#yy=yy+0.2; 'draw string 'xx' 'yy' AVG OVER ${lonw}-${lone}E'

say; say 'WRITE AREA AVE'; say
rave'=aave(' ra ',lon=${mlonw},lon=${mlone},lat=${mlats},lat=${mlatn})'
'd 'rave
raveout=subwrd(result,4); raveoutint=math_nint(raveout)
say 'raveoutint='raveoutint'mm'

dlon=0.7
texlon=${mlone}-dlon; say 'texlon='texlon
dlat=0.2; texlat=33.9 ;#${mlatn}-dlat
say 'texlat='texlat
'q w2xy 'texlon' 'texlat
x=subwrd(result,3); y=subwrd(result,6)
'set string 1 l 3 0'
'draw string 'x' 'y' MEAN 'raveoutint' mm'

# Header
'set strsiz 0.1 0.12'; 'set string 1 l 3'
xx=0.5; 
yy=yy+0.30; 'draw string 'xx' 'yy' ${CMD} ${runname}'
yy=yy+0.25; 'draw string 'xx' 'yy' ${cwd}'
yy=yy+0.25; 'draw string 'xx' 'yy' ${timestamp} ${host}'

'gxprint ${figfile}'

say; say 'Fig file: ${figfile}'; say

say; say 'BINARY OUT: ${BIN}'; say
'set gxout fwrite'; 'set fwrite -be ${BIN}'
'd ra'; 'q dims' ; say result; say



say
say datetime1' - 'datetime2
say 'lon ${lonw} - ${lone}'; say 'lat ${lats} - ${latn}'
say

quit
EOF

grads -bcp $GS

if [ $flagd != "true" ]; then rm -vf $GS; fi
rm -vf $GS

if [ -f ${figfile} ]; then
echo mmmmm FIGFILE mmmmm
ls -lh --time-style=long-iso $figfile
echo mmmmmmmmmmmmmmmmmmm
fi

if [ -f ${BIN} ]; then 
echo mmmmm BINFILE mmmmm
ls -lh --time-style=long-iso ${BIN}
echo mmmmmmmmmmmmmmmmmmm
else
echo ERROR in $0 : NO SUCH FILE, $BIN
exit 1
echo
fi

cp -av $ctl $CTLDIR

exit 0

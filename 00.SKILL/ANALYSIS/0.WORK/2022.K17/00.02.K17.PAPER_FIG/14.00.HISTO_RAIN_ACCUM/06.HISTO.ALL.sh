#!/bin/bash

. ./gmtpar.sh
gmtset HEADER_OFFSET -0.1 HEADER_FONT_SIZE 18
echo "Bash script $0 starts."

C=K17; R=R33; IC=; ICOUT=

EXP1=${EXP1:-00.00${ICOUT}};EOUT1="CNTL";runname1=${C}.${R}.${EXP1}
EXP2=${EXP2:-06.06${ICOUT}};EOUT2="AO80";runname2=${C}.${R}.${EXP2}
EXP3=${EXP3:-00.06${ICOUT}};EOUT3="O80" ;runname3=${C}.${R}.${EXP3}

range=0/1000/0/5
size=X4.2/3
xanot=200:"P${sp}[mm/12-hr]":
yanot=1:"log@-10@-(1+N@-g@-)":
anot=${xanot}/${yanot}WSne:."${C}${sp}${R}${sp}${IC}":


INdir=OUT_02.BIN2TXT
IN1=${INdir}/00.P12H_BIN2TXT_${runname1}.d03_20170705_00-12.TXT
if [ ! -f $IN1 ]; then
  echo Error IN $0 : No such file, $IN1; exit 1
fi
IN2=${INdir}/00.P12H_BIN2TXT_${runname2}.d03_20170705_00-12.TXT
IN3=${INdir}/00.P12H_BIN2TXT_${runname3}.d03_20170705_00-12.TXT

figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
OUT=${figdir}/P12H_${C}.${R}.${EXP1}_${EXP2}_${EXP3}.ps

pshistogram $IN1 -J$size -R$range \
-W100 -L3,0/0/0,2 -V -Z4 -K -X1 -Y4 -P -K > $OUT

pshistogram $IN2 -J$size -R$range \
-W100 -L3,0/0/255,2 -V -Z4 -K -O >> $OUT

pshistogram $IN3 -J$size -R$range \
-W100 -L3,255/0/0,2 -V -Z4 -K -O >> $OUT

psbasemap -J$size -R$range -B$anot -O -K >>$OUT

xoffset=; yoffset=3

export LANG=C

curdir1=$(pwd); now=$(date -R); host=$(hostname)

 time=$(ls -l ${IN1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${OUT} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $OUT
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${IN1} (${time})
0 0.75  9 0 1 LM OUTPUT: ${OUT} (${timeo})
EOF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $IN1 $IN2 $IN3
echo "OUTPUT : "
ls -lh --time-style=long-iso $OUT
echo

echo "Done $0"

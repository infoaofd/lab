#!/bin/bash

. ./gmtpar.sh
gmtset HEADER_OFFSET -0.2 HEADER_FONT_SIZE 18
echo "Bash script $0 starts."

C=K17; R=R28
EXP=$1; EXP=${EXP:-00.00}

if [ $EXP = "00.00" ]; then EOUT="CNTL" ;fi
if [ $EXP = "06.06" ]; then EOUT="AO80" ;fi
if [ $EXP = "00.06" ]; then EOUT="O80"  ;fi

runname=${C}.${R}.${EXP}

range=0/1000/0/5
size=X4.2/3
xanot=200:"P${sp}[mm/12-hr]":
yanot=1:"log@-10@-(1+N@-g@-)":
anot=${xanot}/${yanot}WSne:."${EOUT}":


indir=OUT_16.02.02.BIN2TXT
in=${indir}/16.02.P12H_BIN2TXT_${runname}.d03_20170705_00-12.TXT
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in; exit 1
fi
figdir=. #"FIG_$(basename $0 .sh)"
#if [ ! -d ${figdir} ];then
#  mkdir -p $figdir
#fi
out=${figdir}/$(basename $in .TXT).ps


pshistogram $in -J$size -R$range -B$anot \
-W50 -G150 -V -Z4 -K -X1 -Y4 -P > $out

xoffset=
yoffset=3

export LANG=C

curdir1=$(pwd)
now=$(date)
host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')
timeo=$(ls -l ${out} | awk '{print $6, $7, $8}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${host}
0 1.05  9 0 1 LM ${curdir1}
0 0.90  9 0 1 LM INPUT: ${in} (${time})
0 0.75  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $out
echo

echo "Done $0"

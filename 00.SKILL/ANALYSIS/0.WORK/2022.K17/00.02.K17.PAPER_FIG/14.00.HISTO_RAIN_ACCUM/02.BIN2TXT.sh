#!/bin/bash

src=$(basename $0 .sh).F90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

f90=ifort
OPT=" -fpp -convert big_endian -assume byterecl"
DOPT=" -fpp -CB -traceback -fpe0 -check all"
# http://www.rcs.arch.t.u-tokyo.ac.jp/kusuhara/tips/memorandum/memo1.html

runname=$1; runname=${runname:-K17.R33.00.00}
ODIR=OUT_$(basename $0 .sh); $(which mkd) $ODIR

INFLE="00.P12H_BINOUT_${runname}.d03_20170705_00-12.BIN"
OFLE=00.P12H_BIN2TXT_${runname}.d03_20170705_00-12.TXT
cat<<EOF>$nml
&para
INDIR="OUT_00.P12H_BINOUT"
IM=300
JM=300
INFLE="$INFLE"
OFLE="${ODIR}/$OFLE"
&end
EOF

echo
echo Created ${nml}.
echo
ls -lh --time-style=long-iso ${nml}
echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "=============================================="
echo
echo "   COMPILE ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
#${exe}
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "=============================================="
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "=============================================="
echo
echo TERMINATED.
echo
D2=$(date -R)
echo "START: "
echo "END:   "
exit 1
fi
echo
echo "Done ${exe}"
echo
D2=$(date -R)
echo "START: "
echo "END:   "

rm -vf $nml; echo

if [ -f $OFLE ]; then echo OUTPUT: $OFLE ; fi

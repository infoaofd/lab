#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-d] E1 E2 E3 AREA CN RN; exit 1
}

flagd="false"
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="true" ;;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

E1=$1; E2=$2; E3=$3; AREA=$4; C=$5; R=$6 
E1OUT=CNTL; E2OUT=AO80; E3OUT=O80

MM1=07; MMM1=JUL; HH1=01; MI1=00; DD1=05
MM2=07; MMM2=JUL; HH2=12; MI2=00; DD2=05

TIME1=${HH1}:${MI1}Z${DD1}${MMM1}2017
TIME2=${HH2}:${MI2}Z${DD2}${MMM2}2017

if [ $# -lt 6 ]; then
  echo ERROR in $0: ARGUMENT ERROR; usage
fi

VAR1="U"; VAR2="QVAPOR"

VOUT1=PW ; UNIT1="[mm]"; SCL1=""
VOUT2=VIT; UNIT2="[kg/m/s]"; SCL2=""
VARS=${VOUT1}_${VOUT2}

XLI=0.2; YLI=0.5

LEV1=1000; LEV2=300

XMAX=1; YMAX=5
XLEFT=1; YTOP=9.2
PWID=6; PHIGHT=6
XMARGIN=0.6; YMARGIN=1; YUP=0.3

CLEV1="-10 10 1"   ; CI1=10 ;FS1=2
CLEV2="-100 100 10"; CI2=50 ;FS2=2

BLUE=4; PURPLE=14; GREEN=3; RED=2; BLACK=1; ORANGE=8; MAGENTA=6; AQUA=13

AREA=${AREA:-WEST}

echo mmmmmmmmmmmmmmmmmmmmmmmmmmm

if [ $AREA = "W" -o $AREA = "WEST" ]; then
echo WESTERN SIDE D03
lonw=129.3;
lats=32.5 ; latn=33.5
fi

echo mmmmmmmmmmmmmmmmmmmmmmmmmmm



runname1=${C}.${R}.${E1}; runname2=${C}.${R}.${E2}; runname3=${C}.${R}.${E3}

domain=d03;   p_or_z=z #p
type=basic_p; interval=10MN



indir_root=/work04/manda/K17.ARWpost/10MN/${type}

indir1=${indir_root}/ARWpost_${runname1}; indir2=${indir_root}/ARWpost_${runname2}
indir3=${indir_root}/ARWpost_${runname3}

ctl1=${indir1}/${runname1}.${domain}.${type}.${interval}.ctl
ctl2=${indir2}/${runname2}.${domain}.${type}.${interval}.ctl
ctl3=${indir3}/${runname3}.${domain}.${type}.${interval}.ctl

OUTTIME=${MM1}${DD1}_${HH1}${MI1}-${MM2}${DD2}_${HH2}${MI2}
fig=HOV_${VARS}_${C}.${R}_${E1}_${E2}_${E3}_${domain}_${AREA}.eps #_${OUTTIME}.eps  #${lonw}-${lone}_${lats}-${latn}.eps

KIND='darkblue->blue->dodgerblue->skyblue->white->white->gold->darkorange->red->darkred'

TIMESTAMP=$(date -R); HOST=$(hostname); CWD=$(pwd)
COMMAND="$0 $@"
GS=$(basename $0 .sh).GS

cat <<EOF>$GS
'cc'

'open ${ctl1}'
say; say 'mmmmmmm'; say 'OPEN ${ctl1}'; say 'mmmmmmm'; say

'open ${ctl2}'
say; say 'mmmmmmm'; say 'OPEN ${ctl2}'; say 'mmmmmmm'

'open ${ctl3}'
say; say 'mmmmmmm'; say 'OPEN ${ctl3}'; say 'mmmmmmm'

say; say 'mmmmmmm'; say '${C} ${R} ${E1} ${E2} ${E3} ${AREA} ${VAR1} ${VAR2} ${TIME1} ${TIME2}'; say 'mmmmmmm'



say; say 'mmmmmmm'; say 'INTEG'; say 'mmmmmmm'

say '${TIME1} ${TIME2}'; 'set time ${TIME1} ${TIME2}'
'set lev ${LEV1}'
'set lon ${lonw}'
'set lat ${lats} ${latn}'

say; say 'mmm'; say '${E1OUT} ${VOUT1}'; say 'mmm'
'${VOUT1}1=vint(lev(z=1), ${VAR2}.1, 300)'
say; say 'mmm'; say '${E1OUT} ${VOUT2}'; say 'mmm'
'${VOUT2}1=vint(lev(z=1), ${VAR2}.1*${VAR1}.1, 300)'

say; say 'mmm'; say '${E2OUT} ${VOUT1}'; say 'mmm'
'${VOUT1}2=vint(lev(z=1), ${VAR2}.2, 300)'
say; say 'mmm'; say '${E2OUT} ${VOUT2}'; say 'mmm'
'${VOUT2}2=vint(lev(z=1), ${VAR2}.2*${VAR1}.2, 300)'

say; say 'mmm'; say '${E3OUT} ${VOUT1}'; say 'mmm'
'${VOUT1}3=vint(lev(z=1), ${VAR2}.3, 300)'
say; say 'mmm'; say '${E3OUT} ${VOUT2}'; say 'mmm'
'${VOUT2}3=vint(lev(z=1), ${VAR2}.3*${VAR1}.3, 300)'



say; say 'mmmmmmm'; say 'PLOT'; say 'mmmmmmm'

'set vpage 0.0 8.5 0.0 11.'
xmax=${XMAX}; ymax=${YMAX}
xleft=${XLEFT}; ytop=${YTOP}
xwid=${PWID}/${XMAX}; ywid=${PHIGHT}/${YMAX}
xmargin=${XMARGIN}; ymargin=${YMARGIN}

say '${TIME1} ${TIME2}'; 'set time ${TIME1} ${TIME2}'
'set lev ${LEV1}'
'set lon ${lonw}'
'set lat ${lats} ${latn}'



say; say 'mmmmmmm'; say 'PLOT'; say 'mmmmmmm'

xmap=1; ymap=1; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop  - (ywid+ymargin)*(ymap-1); ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'
'set xyrev on'; 'set tlsupp month'
'set ylint 0.5'

say '${E1OUT} ${VOUT1}1 ${lonw} ${lats}-${latn}'
'color $CLEV1 -kind $KIND'
'd ${VOUT1}1-${VOUT1}2'

'set xlab off'; 'set ylab off'

'set gxout contour'
'set ccolor 0'; 'set cthick 10'; 'set cstyle 1'; 'set cmark 0'
'set cint $CI1'
'd ${VOUT1}1'
'set ccolor 1'; 'set cthick 1'; 'set cstyle 1'; 'set cmark 0'
'd ${VOUT1}1'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

#x1=xl; x2=xr-0.5; y1=yb-0.3; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS1} -ft 3 -line on -edge circle'

xx = xl+0.0; yy = yt+0.16
'set string 1 l 3 0'; 'set strsiz 0.12 0.14'
'draw string 'xx' 'yy' ${VOUT1} ${UNIT1} ${E1OUT} ${E2OUT}'



say; say 'mmmmmmm'; say '${E1OUT} ${E3OUT}'; say 'mmmmmmm'

xmap=1; ymap=2; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop  - (ywid+ymargin)*(ymap-1) + ${YUP}; ys = ye - ywid + ${YUP}

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'
'set xyrev on'; 'set tlsupp month'
'set ylint 0.5'

say '${E1OUT} ${E3OUT} ${VOUT1}1 ${lonw} ${lats}-${latn}'
'color $CLEV1 -kind $KIND'
'd ${VOUT1}1-${VOUT1}3'

'set xlab off'; 'set ylab off'

'set gxout contour'
'set ccolor 0'; 'set cthick 10'; 'set cstyle 1'; 'set cmark 0'
'set cint $CI1'
'd ${VOUT1}1'
'set ccolor 1'; 'set cthick 1'; 'set cstyle 1'; 'set cmark 0'
'd ${VOUT1}1'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS1} -ft 3 -line on -edge circle'

xx = xl+0.0; yy = yt+0.16
'set string 1 l 3 0'; 'set strsiz 0.12 0.14'
'draw string 'xx' 'yy' ${VOUT1} ${UNIT1} ${E1OUT} ${E3OUT}'



xmap=1; ymap=3; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) ; ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'

say '${E1OUT} ${VOUT2}1 ${lonw} ${lats}-${latn}'
'color $CLEV2 -kind $KIND'
'd ${VOUT2}1-${VOUT2}3'

'set xlab off'; 'set ylab off'

'set gxout contour'
'set ccolor 0'; 'set cthick 10'; 'set cstyle 1'; 'set cmark 0'
'set cint $CI2'
'd ${VOUT2}1'
'set cint $CI2'
'set ccolor 1'; 'set cthick 1'; 'set cstyle 1'; 'set cmark 0'
'd ${VOUT2}1'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

#x1=xl; x2=xr-0.5; y1=yb-0.3; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS2} -ft 3 -line on -edge circle'

xx = xl+0.0; yy = yt+0.16
'set string 1 l 3 0'; 'set strsiz 0.12 0.14'
'draw string 'xx' 'yy' ${VOUT2} ${UNIT2} ${E1OUT} ${E3OUT}'



xmap=1; ymap=4; 'set xlab on'; 'set ylab on'

xs = xleft + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1) + ${YUP} ; ys = ye - ywid + ${YUP}

'set parea 'xs ' 'xe' 'ys' 'ye
'set grads off'; 'set grid off'

say '${E1OUT} ${E3OUT} ${VOUT2}1 ${lonw} ${lats}-${latn}'
'color $CLEV2 -kind $KIND'
'd ${VOUT2}1-${VOUT2}3'

'set xlab off'; 'set ylab off'

'set gxout contour'
'set ccolor 0'; 'set cthick 10'; 'set cstyle 1'; 'set cmark 0'
'set cint $CI2'
'd ${VOUT2}1'
'set cint $CI2'
'set ccolor 1'; 'set cthick 1'; 'set cstyle 1'; 'set cmark 0'
'd ${VOUT2}1'

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6); yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs ${FS2} -ft 3 -line on -edge circle'

xx = xl+0.0; yy = yt+0.16
'set string 1 l 3 0'; 'set strsiz 0.12 0.14'
'draw string 'xx' 'yy' ${VOUT2} ${UNIT2} ${E1OUT} ${E3OUT}'



say; say '${lats}N-${latn}N, ${lonw}E'

'set string 1 c 3'
'set strsiz 0.12 0.14'
xx=4.; yy=9.7
#yy=yy+0.25
'draw string ' xx ' ' yy ' ${LEV1}-${LEV2} ${lonw}E'

yy=yy+0.25
'set strsiz 0.1 0.14'
'draw string ' xx ' ' yy ' ${C} ${R} ${E1} ${E2} ${E3}'

# Header
'set strsiz 0.1 0.12'
'set string 1 l 3'
yy=yy+0.3
'draw string 0.5 'yy' ${TIMESTAMP} ${HOST}'
yy=yy+0.2
'draw string 0.5 'yy' ${CWD}'
yy=yy+0.2
'draw string 0.5 'yy' ${COMMAND}'

'gxprint $fig'
'quit'
EOF

grads -bcp "${GS}"

cp -av $ctl1 .
if [ $flagd != "true" ]; then rm -vf $GS; fi

echo; echo mmmmmmm OUTPUT
ls -lh --time-style=long-iso $fig
echo mmmmmmm; echo

exit 0

dset ^K17.R33.1.00.00.IC2.d03.basic_p.10MN_%y4-%m2-%d2_%h2:%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V3.7.1 MODEL
pdef  300 300 lcc  32.948  130.856  150.500  150.500  34.00000  24.00000  128.00000   1000.000   1000.000
xdef  749 linear  129.18993   0.00450450
ydef  632 linear   31.51415   0.00450450
zdef   15 levels  
1000.00000
 975.00000
 950.00000
 900.00000
 850.00000
 800.00000
 700.00000
 600.00000
 500.00000
 400.00000
 300.00000
 250.00000
 200.00000
 150.00000
 100.00000
tdef  151 linear 12Z04JUL2017      10MN      
VARS   34
U             15  0  x-wind component (m s-1)
V             15  0  y-wind component (m s-1)
W             15  0  z-wind component (m s-1)
Q2             1  0  QV at 2 M (kg kg-1)
T2             1  0  TEMP at 2 M (K)
U10            1  0  U at 10 M (m s-1)
V10            1  0  V at 10 M (m s-1)
QVAPOR        15  0  Water vapor mixing ratio (kg kg-1)
QCLOUD        15  0  Cloud water mixing ratio (kg kg-1)
QRAIN         15  0  Rain water mixing ratio (kg kg-1)
QSNOW         15  0  Snow mixing ratio (kg kg-1)
SNOW           1  0  SNOW WATER EQUIVALENT (kg m-2)
SNOWH          1  0  PHYSICAL SNOW DEPTH (m)
H_DIABATIC    15  0  MICROPHYSICS LATENT HEATING (K s-1)
HGT            1  0  Terrain Height (m)
RAINC          1  0  ACCUMULATED TOTAL CUMULUS PRECIPITATION (mm)
RAINRC         1  0  RAIN RATE CONV (mm per output interval)
RAINNC         1  0  ACCUMULATED TOTAL GRID SCALE PRECIPITATION (mm)
RAINRNC        1  0  RAIN RATE NON-CONV (mm per output interval)
XLAND          1  0  LAND MASK (1 FOR LAND, 2 FOR WATER) (-)
PBLH           1  0  PBL HEIGHT (m)
HFX            1  0  UPWARD HEAT FLUX AT THE SURFACE (W m-2)
QFX            1  0  UPWARD MOISTURE FLUX AT THE SURFACE (kg m-2 s-1)
LH             1  0  LATENT HEAT FLUX AT THE SURFACE (W m-2)
SST            1  0  SEA SURFACE TEMPERATURE (K)
ept           15  0  Equivalent Potential Temperature (K)
sept          15  0  Saturated Equivalent Potential Temperature (K)
pressure      15  0  Model pressure (hPa)
geopt         15  0  Geopotential (m2/s2)
tk            15  0  Temperature (K)
theta         15  0  Potential Temperature (K)
rh            15  0  Relative Humidity (%)
slp            1  0  Sea Levelp Pressure (hPa)
dbz           15  0  Reflectivity (-)
ENDVARS
@ global String comment TITLE =  OUTPUT FROM WRF V3.7.1 MODEL
@ global String comment START_DATE = 2017-07-03_18:00:00
@ global String comment SIMULATION_START_DATE = 2017-07-03_12:00:00
@ global String comment WEST-EAST_GRID_DIMENSION =   301
@ global String comment SOUTH-NORTH_GRID_DIMENSION =   301
@ global String comment BOTTOM-TOP_GRID_DIMENSION =    31
@ global String comment DX =      1000.00
@ global String comment DY =      1000.00
@ global String comment SKEBS_ON =     0
@ global String comment SPEC_BDY_FINAL_MU =     0
@ global String comment USE_Q_DIABATIC =     0
@ global String comment GRIDTYPE = C
@ global String comment DIFF_OPT =     1
@ global String comment KM_OPT =     4
@ global String comment DAMP_OPT =     0
@ global String comment DAMPCOEF =         0.20
@ global String comment KHDIF =         0.00
@ global String comment KVDIF =         0.00
@ global String comment MP_PHYSICS =     9
@ global String comment RA_LW_PHYSICS =     1
@ global String comment RA_SW_PHYSICS =     1
@ global String comment SF_SFCLAY_PHYSICS =     1
@ global String comment SF_SURFACE_PHYSICS =     2
@ global String comment BL_PBL_PHYSICS =     1
@ global String comment CU_PHYSICS =     0
@ global String comment SF_LAKE_PHYSICS =     0
@ global String comment SURFACE_INPUT_SOURCE =     1
@ global String comment SST_UPDATE =     1
@ global String comment GRID_FDDA =     0
@ global String comment GFDDA_INTERVAL_M =     0
@ global String comment GFDDA_END_H =     0
@ global String comment GRID_SFDDA =     0
@ global String comment SGFDDA_INTERVAL_M =     0
@ global String comment SGFDDA_END_H =     0
@ global String comment HYPSOMETRIC_OPT =     2
@ global String comment USE_THETA_M =     0
@ global String comment SF_URBAN_PHYSICS =     0
@ global String comment SHCU_PHYSICS =     0
@ global String comment MFSHCONV =     0
@ global String comment FEEDBACK =     1
@ global String comment SMOOTH_OPTION =     0
@ global String comment SWRAD_SCAT =         1.00
@ global String comment W_DAMPING =     0
@ global String comment DT =         5.56
@ global String comment RADT =        30.00
@ global String comment BLDT =         0.00
@ global String comment CUDT =         5.00
@ global String comment AER_OPT =     0
@ global String comment SWINT_OPT =     0
@ global String comment AER_TYPE =     1
@ global String comment AER_AOD550_OPT =     1
@ global String comment AER_ANGEXP_OPT =     1
@ global String comment AER_SSA_OPT =     1
@ global String comment AER_ASY_OPT =     1
@ global String comment AER_AOD550_VAL =         0.12
@ global String comment AER_ANGEXP_VAL =         1.30
@ global String comment AER_SSA_VAL =         0.00
@ global String comment AER_ASY_VAL =         0.00
@ global String comment MOIST_ADV_OPT =     1
@ global String comment SCALAR_ADV_OPT =     1
@ global String comment TKE_ADV_OPT =     1
@ global String comment DIFF_6TH_OPT =     0
@ global String comment DIFF_6TH_FACTOR =         0.12
@ global String comment OBS_NUDGE_OPT =     0
@ global String comment BUCKET_MM =        -1.00
@ global String comment BUCKET_J =        -1.00
@ global String comment PREC_ACC_DT =         0.00
@ global String comment SF_OCEAN_PHYSICS =     0
@ global String comment ISFTCFLX =     0
@ global String comment ISHALLOW =     0
@ global String comment ISFFLX =     1
@ global String comment ICLOUD =     1
@ global String comment ICLOUD_CU =     0
@ global String comment TRACER_PBLMIX =     1
@ global String comment SCALAR_PBLMIX =     0
@ global String comment YSU_TOPDOWN_PBLMIX =     0
@ global String comment GRAV_SETTLING =     0
@ global String comment DFI_OPT =     0
@ global String comment SIMULATION_INITIALIZATION_TYPE = REAL-DATA CASE
@ global String comment WEST-EAST_PATCH_START_UNSTAG =     1
@ global String comment WEST-EAST_PATCH_END_UNSTAG =   300
@ global String comment WEST-EAST_PATCH_START_STAG =     1
@ global String comment WEST-EAST_PATCH_END_STAG =   301
@ global String comment SOUTH-NORTH_PATCH_START_UNSTAG =     1
@ global String comment SOUTH-NORTH_PATCH_END_UNSTAG =   300
@ global String comment SOUTH-NORTH_PATCH_START_STAG =     1
@ global String comment SOUTH-NORTH_PATCH_END_STAG =   301
@ global String comment BOTTOM-TOP_PATCH_START_UNSTAG =     1
@ global String comment BOTTOM-TOP_PATCH_END_UNSTAG =    30
@ global String comment BOTTOM-TOP_PATCH_START_STAG =     1
@ global String comment BOTTOM-TOP_PATCH_END_STAG =    31
@ global String comment GRID_ID =     3
@ global String comment PARENT_ID =     2
@ global String comment I_PARENT_START =   130
@ global String comment J_PARENT_START =   150
@ global String comment PARENT_GRID_RATIO =     3
@ global String comment CEN_LAT =        32.95
@ global String comment CEN_LON =       130.86
@ global String comment TRUELAT1 =        24.00
@ global String comment TRUELAT2 =        34.00
@ global String comment MOAD_CEN_LAT =        32.50
@ global String comment STAND_LON =       128.00
@ global String comment POLE_LAT =        90.00
@ global String comment POLE_LON =         0.00
@ global String comment GMT =        18.00
@ global String comment JULYR =  2017
@ global String comment JULDAY =   184
@ global String comment MAP_PROJ =     1
@ global String comment MAP_PROJ_CHAR = Lambert Conformal
@ global String comment MMINLU = USGS
@ global String comment NUM_LAND_CAT =    24
@ global String comment ISWATER =    16
@ global String comment ISLAKE =    -1
@ global String comment ISICE =    24
@ global String comment ISURBAN =     1
@ global String comment ISOILWATER =    14

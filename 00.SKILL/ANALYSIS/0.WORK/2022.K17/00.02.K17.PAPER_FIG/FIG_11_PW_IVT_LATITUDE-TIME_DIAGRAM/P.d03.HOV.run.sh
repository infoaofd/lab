#!/bin/bash

usage(){
  echo USAGE $(basename $0) [-h] E1 E2 E3 AREA VAR ; exit 0
}

flagh=""; flagd=""
while getopts hd OPT; do
  case $OPT in
    "h" ) usage ;;
    "d" ) flagd="-d";;
     *  ) usage
  esac
done
shift $(expr $OPTIND - 1)

C=K17; R=R33.1

E1=$1; E2=$2; E3=$3; AREA=$4; VAR=$5

#VAR=${VAR:-CNTL_PW.IVT}
VAR=${VAR:-DIFF2_PW.IVT}

E1=${E1:-00.00}
E2=${E2:-06.06}
E3=${E3:-00.06}

AREA=${AREA:-WEST}


EXE=P.d03.${VAR}.HOV.sh
$EXE ${flagd} $E1 $E2 $E3 $AREA $C $R




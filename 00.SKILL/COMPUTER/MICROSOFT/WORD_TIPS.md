# WORDのコツ

[[_TOC_]]

### 図の画質を劣化させない方法

#### (1) ワードとパワーポイントの設定

コピー元のパワーポイントと，貼り付け先のWORDで，添付ファイルに記載した内容を設定しておいておく。

WordでPDF作成時に画質を劣化させない方法

https://pdfeditor-knowledge.hatenablog.com/entry/pdf-create/word-pdf-create.html

#### (2) 図のコピーと貼り付け

1. ppt上で編集したものを，pptを使ってPDF形式で保存する

2. PDF X-change viewerでPDFファイルを開き，300%以上に拡大してから，コピーする
   (https://forest.watch.impress.co.jp/library/software/pdfxchange/)

3. WORDに張り付ける

### 一部や途中から段組みにする  

https://www.tipsfound.com/word/03019-word

### 英単語の途中で改行させる  

https://office-hack.com/word/word-line-breaks-in-the-middle-of-english-words/

### ハイフネーション  

http://office-qa.com/Word/wd656.htm

### 行が次のページへ送られる、空白が空く

「改ページ一の自動修正」

Ctrl+Aキーで全選択し、右クリック→「段落 」をクリック

「改ページと改行」タブの４つのチェックを完全にOFFにする（塗りつぶされていたりグレーの場合は、1度チェックを入れ再クリックし、完全にチェックを外す）

▼ 塗りつぶし（半分チェック）の場合は、完全にチェックを外す（4つOFF）

![img](https://www.office-qa.com/Word/wd262.2.GIF)
# SUPER COMPUTER KYOTO UNIVERSITY 

## スーパーコンピュータの使い方

https://web.kudpc.kyoto-u.ac.jp/manual/ja



2022年度に利用可能なシステム
https://web.kudpc.kyoto-u.ac.jp/manual/ja/misc/systemdeg

- システムDはシステムBの代わり

- システムEはシステムCの代わり

京大スパコンの使用方法メモ Last modified: 2014/03/31
https://sites.google.com/site/unumahp/pc/kudpc

- WRFのコンパイル for A, B, C systems



## SSH

鍵ペアの作成と公開鍵の登録
https://web.kudpc.kyoto-u.ac.jp/manual/ja/login/pubkey

SSH公開鍵
https://web.kudpc.kyoto-u.ac.jp/portal/user/pubkey

### 参考

SSH公開鍵認証で接続するまで
https://qiita.com/kazokmr/items/754169cfa996b24fcbf5

WinSCP用に秘密鍵をコンバートしてSSH接続する
https://def-4.com/winscp_private_key/



## ログイン

z56015
RxXXXxxx-cxxXXXXX6

```
ssh z56015@laurel.kudpc.kyoto-u.ac.jp
```



## ノード

| システム名         | ホスト名                     | 備考                                                         |
| :----------------- | :--------------------------- | :----------------------------------------------------------- |
| ファイル転送サーバ | hpcfs.kudpc.kyoto-u.ac.jp    | **推奨** 2台のサーバで構成。時間制限のないSFTP、RSYNC専用のサーバ。 |
| システムD/クラウド | laurel.kudpc.kyoto-u.ac.jp   | 3台のログインノードで構成。                                  |
| システムE          | cinnamon.kudpc.kyoto-u.ac.jp | 3台のログインノードで構成。                                  |
| システムG          | gardenia.kudpc.kyoto-u.ac.jp | 2台のログインノードで構成。                                  |
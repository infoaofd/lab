# mdbook

markdownファイルからhtml形式の本を作成する

\\[ \frac{\partial \zeta}{\partial t}=-\beta v \\]

[TOC]

## Home

https://rust-lang.github.io/mdBook/guide/installation.html

## Environement

Windows 10 + WSL

## Install

```
$ sudo apt install -y cargo
```

```
$ cargo install mdbook
```

### mdbookのインストール先

~/.cargo/bin/mdbook

\\wsl.localhost\Ubuntu-22.04\home\am\.cargo\bin

## Test

### Create New Book

```bash
am@DESKTOP-TJ901JQ:~/book-test$ mkdir book-test
am@DESKTOP-TJ901JQ:~/book-test$ cd .\book-test\
am@DESKTOP-TJ901JQ:~/book-test$ ~/.cargo/bin/mdbook init

# Do you want a .gitignore to be created? (y/n)
y
# What title would you like to give the book? 
TEST:wa
```

### Build

```bash
am@DESKTOP-TJ901JQ:~/book-test$ ~/.cargo/bin/mdbook build -o
2024-10-13 15:23:26 [INFO] (mdbook::book): Book building has started
2024-10-13 15:23:26 [INFO] (mdbook::book): Running the html backend
2024-10-13 15:23:26 [INFO] (mdbook): Opening web browser
2024-10-13 15:23:26 [ERROR] (mdbook): Error opening web browser: error spawning command(s) 'wslview, xdg-open'
```

### Check Contents

```bash
am@DESKTOP-TJ901JQ:~/book-test$ tree -L 2
.
├── book
│   ├── 404.html
│   ├── FontAwesome
│   ├── ayu-highlight.css
│   ├── book.js
│   ├── chapter_1.html
│   ├── clipboard.min.js
│   ├── css
│   ├── elasticlunr.min.js
│   ├── favicon.png
│   ├── favicon.svg
│   ├── fonts
│   ├── highlight.css
│   ├── highlight.js
│   ├── index.html
│   ├── mark.min.js
│   ├── print.html
│   ├── searcher.js
│   ├── searchindex.js
│   ├── searchindex.json
│   └── tomorrow-night.css
├── book.toml
└── src
    ├── SUMMARY.md
    └── chapter_1.md
```



#### ファイルの所在

<img src="image-20241013152837067.png" alt="image-20241013152837067" style="zoom:50%;" />

### ブラウザでindex.htmlを開いたところ

![image-20241013154251325](image-20241013154251325.png)



## 数式

am@DESKTOP-TJ901JQ:~/book-test$ ls
book  **book.toml**  src

**book.toml**に`mathjax-support = true`	を追加

```bash
am@DESKTOP-TJ901JQ:~/book-test$ cat book.toml
[book]
.....
title = "TEST:wa"
[output.html]
mathjax-support = true
```

\\wsl.localhost\Ubuntu-22.04\home\am\book-test

## mdBookをGitHubで公開

https://qiita.com/pyama2000/items/9d87d5dc2991d5f3c3e5

## Misc

### Ubuntuのファイルをエクスプローラーから見る

エクスプローラのアドレスバーに`\\wsl$`と入力する
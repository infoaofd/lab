# DOWNLOAD&CONVERT AMeDAS DATA

[[_TOC_]]

## Scripts

### Downloader

- DOWNLOAD-1HR.sh
- download-1hr.sh
- download-mon.sh
- download-today.sh

### Converter

- get_id.pl
- get_list.pl
- html2txt.pl
- make_all_list.pl
- test.pl

## Usage

```bash
$ DOWNLOAD-1HR.sh
```



```bash
$ make_all_list.pl 
```




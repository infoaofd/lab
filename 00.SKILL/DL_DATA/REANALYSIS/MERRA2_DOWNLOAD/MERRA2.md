# MERRA2

[[_TOC_]]

## Website

https://disc.gsfc.nasa.gov/datasets



## MERRA-2 tavgU_2d_ocn_Nx

### 2d,diurnal,Time-Averaged,Single-Level,Assimilation,Ocean Surface Diagnostics V5.12.4 (M2TUNXOCN)

**M2TUNXOCN** (or tavgU_2d_ocn_Nx) is a time-averaged 2-dimensional monthly diurnal means data collection in Modern-Era Retrospective analysis for Research and Applications version 2 (MERRA-2). This collection consists of ocean surface diagnostics, such as open water **skin temperature** (sea surface temperature), open water **latent energy flux**, open water **upward sensible heat flux**, and open water **net downward longwave** ( or **shortwave** ) flux . This data collection is the monthly mean of data fields for each hour and time-stamped with the central time of an hour starting from 00:30 UTC, e.g.: 00:30, 01:30, … , 23:30 UTC.MERRA-2 is the latest version of global atmospheric reanalysis for the satellite era produced by NASA Global Modeling and Assimilation Office (GMAO) using the Goddard Earth Observing System Model (GEOS) version 5.12.4. The dataset covers the period of 1980-present with the latency of ~3 weeks after the end of a month.

- LHF (Upward)
- SHF (Upward)

- TSK
- NLR (Downward)
- NSR (Downward)



## MERRA-2 tavgU_2d_rad_Nx

### 2d,diurnal,Time-Averaged,Single-Level,Assimilation,Radiation Diagnostics V5.12.4 (M2TUNXRAD)

**M2TUNXRAD** (or tavgU_2d_rad_Nx) is a time-averaged 2-dimensional monthly diurnal means data collection in Modern-Era Retrospective analysis for Research and Applications version 2 (MERRA-2). This collection consists of radiation diagnostics, such as surface albedo, cloud area fraction, in cloud optical thickness, **surface incoming shortwave flux** (i.e. solar radiation), **surface net downward shortwave** **flux**, and upwelling longwave flux at toa (top of atmosphere) (i.e. outgoing longwave radiation (OLR) at toa). This data collection is the monthly mean of data fields for each hour and time-stamped with the central time of an hour starting from 00:30 UTC, e.g.: 00:30, 01:30, … , 23:30 UTC.

- DSR (surface incoming shortwave flux)



## DOWNLOAD

GES DISC Data Access.pdf

### wget for Mac/Linux

1. Make sure you have set up your Earthdata account.
2. Install wget if necessary. A version of wget 1.18 compiled with gnuTLS 3.3.3 or OpenSSL 1.0.2 or LibreSSL 2.0.2 or later is
recommended.
3. Create a .netrc file in your home directory.
a.  cd ~  or  cd $HOME
b.  touch .netrc
c.  echo "machine urs.earthdata.nasa.gov (https://urs.earthdata.nasa.gov) login <uid> password <password>" >>
.netrc  (where <uid> is your user name and <password> is your Earthdata Login password without the brackets)
d.  chmod 0600 .netrc  (so only you can access it)
4. Create a cookie file. This file will be used to persist sessions across calls to  wget  or  curl .
a.  cd ~  or  cd $HOME
b.  touch .urs_cookies . 
Note: you may need to re-create .urs_cookies in case you have already executed wget without valid authentication.
5. Download your data using wget:

To download multiple data files at once, create a plain-text  <url.txt>  file with each line containing a GES DISC data file
URL. Then, enter the following command:

```bash
wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --auth-no-challenge=on --keep-session-cookies -i <url.txt>
```


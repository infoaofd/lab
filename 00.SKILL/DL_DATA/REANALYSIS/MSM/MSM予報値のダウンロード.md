# MSM予報値のダウンロード

## 使用例

### 特定の年の特定の期間をまとめてダウンロード

スクリプト (GET.MSM.GRIB.RUN.sh)の下記の箇所を書き換える

```bash
YS=2020; MMS=06; DDS=01
YE=2021; MME=06; DDE=02
```

実行する

```bash
$ GET.MSM.GRIB.RUN.sh
```

上の例の場合, 下記の期間のデータがダウンロードされる

2020年06月01日～06月02日
2020年06月01日～06月02日



### 開始日と終了日を指定してダウンロード

```bash
GET.MSM.GRIB.sh 20220601 20220901
```

上の例の場合, 2020年06月01日～09月01日の期間の全ての日付のデータがダウンロードされる



## スクリプト

### GET.MSM.GRIB.RUN.sh

```bash
#!/bin/bash

EXE=$(basename $0 .RUN.sh).sh

if [ ! -f $EXE ]; then
echo NO SUCH FILE, $EXE
exit 1
fi

YS=2020; MMS=05; DDS=31
YE=2022; MME=09; DDE=01

Y=$YS

while [ $Y -le $YE ]; do
$EXE ${Y}${MMS}${DDS} ${Y}${MME}${DDE}
Y=$(expr $Y + 1)
done
```



### GET.MSM.GRIB.sh

```bash
#!/bin/bash

usage(){
  echo
  echo "Usage $0 [-t]"
  echo "-t : test mode (do NOT download files)"
  echo
}

WGETOPT="-nv" # -nc"

export LANG=C

flagt="false"
while getopts t OPT; do
  case $OPT in
    "t" ) flagt="true" ;  ;;
     * ) usage; exit 1
  esac
done

shift $(expr $OPTIND - 1)

start=$1; end=$2

echo "MMMMMMMMMM $start $end"
start=${start:-20220601}; end=${end:-20220602}

homedir=$(pwd)

URL=http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/

starts=$(date -d "${start}" '+%s'); ends=$(date -d "${end}" '+%s')

times=$(expr $ends - $starts)

days=$(expr $times / 86400 )

sdate=${start:0:4}/${start:4:2}/${start:6:2}

yesterday=$(date -d"$sdate"-1days '+%Y/%m/%d')

n=0
while [ $n -le $days ]; do

  today=$(date -d"$yesterday"+1days '+%Y/%m/%d')
  echo; echo "MMMMMMMMMMM  $today"

  yyyy=${today:0:4}; mm=${today:5:2}; dd=${today:8:2}

  ODIR=${yyyy}/${mm}

  if [ ! -d $ODIR ];then mkd $ODIR ;fi

  cd $ODIR

  h=0
  while [ $h -le 21 ]; do
  hh=$(printf %02d $h)
  YMDH=${yyyy}${mm}${dd}${hh}

  echo "MMMMM $YMDH"
  INDIR=${yyyy}/${mm}/${dd}
  INLIST=" \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_L-pall_FH18-33_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_L-pall_FH36-39_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_Lsurf_FH34-39_grib2.bin \
  "
  for INFLE in $INLIST; do

    if [ $flagt == "false" ]; then
      wget $WGETOPT $URL/$INDIR/$INFLE
    else
      echo wget $WGETOPT $URL/$INDIR/$INFLE
    fi

  done #INFLE

  if [ $h -eq 0 -o $h -eq 12 ];then
  INLIST=" \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_Lsurf_FH40-51_grib2.bin \
   Z__C_RJTD_${YMDH}0000_MSM_GPV_Rjp_L-pall_FH42-51_grib2.bin \
  "

  for INFLE in $INLIST; do

    if [ $flagt == "false" ]; then
      wget $WGETOPT $URL/$INDIR/$INFLE
    else
      echo wget $WGETOPT $URL/$INDIR/$INFLE
    fi

  done #INFLE
  fi #h

  h=$(expr $h + 3)
  done #h

  echo "MMMMMMMMMMM DONE $today"; echo

  n=$(expr $n + 1)
  yesterday=$today

  cd ${homedir}; CWD=$(pwd); echo "MMM CWD=${CWD}"; echo

done #n

exit 0
```



## データの確認

### CHK.MSM.GRB.ncl

```
INFLE="Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin"
print("MMMMM INPUT: "+INFLE)
a=addfile(INFLE,"r")

ft=a->forecast_time0
print("MMM FORECAST TIME")
print(ft)

print(a)
delete(a)
print("")



INFLE="Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin"
print("MMMMM INPUT: "+INFLE)
a=addfile(INFLE,"r")

print("MMM FORECAST TIME")
ft=a->forecast_time0
print(ft)

print(a)
print("")
delete(a)
```



### データ形式

```fortran
ファイル名の意味は下記の通りです
Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
20200531000000：日付と時刻（最後の4桁は0）
pall: 気圧面データ
FH00-15: 予報時間
00時間から15時間後までの予報値（00は初期値）
grib2: gribのバージョン2形式のデータ

Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin
Lsurf: 地表面のデータ


概要については下記を参照してください。
http://www.jmbsc.or.jp/jp/online/file/f-online10200.html

/work01/DATA/MSM.GRIB/2020/05
2022-12-05_15-38
$ ncl CHK.MSM.GRB.ncl
MMMMM INPUT: Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
MMM FORECAST TIME


Variable: ft
Type: integer
Total Size: 20 bytes
            5 values
Number of Dimensions: 1
Dimensions and sizes:   [forecast_time0 | 5]
Coordinates:
            forecast_time0: [3..15]
Number Of Attributes: 2
  long_name :   Forecast offset from initial time
  units :       hours
3
6
9
12
15

Variable: a
Type: file
filename:       Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
path:   Z__C_RJTD_20200531000000_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
   file global attributes:
   dimensions:
      lv_ISBL0 = 16
      lat_0 = 253
      lon_0 = 241
      lv_ISBL1 = 12
      forecast_time0 = 5
   variables:
      float TMP_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Temperature
         units :        K
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Temperature
         parameter_template_discipline_category_number :       ( 0, 0, 0, 0 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float RH_P0_L100_GLL0 ( lv_ISBL1, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Relative humidity
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Moisture
         parameter_template_discipline_category_number :       ( 0, 0, 1, 1 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float UGRD_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    U-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 2 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float VGRD_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    V-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 3 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float VVEL_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Vertical velocity (pressure)
         units :        Pa s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 8 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float HGT_P0_L100_GLL0 ( lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Initialization
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Geopotential height
         units :        gpm
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Mass
         parameter_template_discipline_category_number :       ( 0, 0, 3, 5 )
         level_type :   Isobaric surface (Pa)
         forecast_time :        0
         forecast_time_units :  hours
         initial_time : 05/31/2020 (00:00)

      float TMP_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Temperature
         units :        K
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Temperature
         parameter_template_discipline_category_number :       ( 0, 0, 0, 0 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      float RH_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL1, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Relative humidity
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Moisture
         parameter_template_discipline_category_number :       ( 0, 0, 1, 1 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      float UGRD_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    U-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 2 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      float VGRD_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    V-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 3 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      float VVEL_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Vertical velocity (pressure)
         units :        Pa s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 8 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      float HGT_P0_L100_GLL0_1 ( forecast_time0, lv_ISBL0, lat_0, lon_0 )
         generating_process_type :      Forecast
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Geopotential height
         units :        gpm
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Mass
         parameter_template_discipline_category_number :       ( 0, 0, 3, 5 )
         level_type :   Isobaric surface (Pa)
         initial_time : 05/31/2020 (00:00)

      integer forecast_time0 ( forecast_time0 )
         long_name :    Forecast offset from initial time
         units :        hours

      float lv_ISBL1 ( lv_ISBL1 )
         long_name :    Isobaric surface
         units :        Pa

      float lat_0 ( lat_0 )
         long_name :    latitude
         grid_type :    Latitude/Longitude
         units :        degrees_north
         Dj :   0.1
         Di :   0.125
         Lo2 :  150
         La2 :  22.4
         Lo1 :  120
         La1 :  47.6

      float lon_0 ( lon_0 )
         long_name :    longitude
         grid_type :    Latitude/Longitude
         units :        degrees_east
         Dj :   0.1
         Di :   0.125
         Lo2 :  150
         La2 :  22.4
         Lo1 :  120
         La1 :  47.6

      float lv_ISBL0 ( lv_ISBL0 )
         long_name :    Isobaric surface
         units :        Pa


MMMMM INPUT: Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin
MMM FORECAST TIME
fatal:Dimension sizes of left hand side and right hand side of assignment do not match
fatal:["Execute.c":8637]:Execute: Error occurred at or near line 20 in file CHK.MSM.GRB.ncl



Variable: ft
Type: integer
Total Size: 20 bytes
            5 values
Number of Dimensions: 1
Dimensions and sizes:   [forecast_time0 | 5]
Coordinates:
            forecast_time0: [3..15]
Number Of Attributes: 2
  long_name :   Forecast offset from initial time
  units :       hours
3
6
9
12
15

Variable: a
Type: file
filename:       Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin
path:   Z__C_RJTD_20200531000000_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin
   file global attributes:
   dimensions:
      forecast_time0 = 18
      lat_0 = 505
      lon_0 = 481
   variables:
      float TMP_P0_L103_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Temperature
         units :        K
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Temperature
         parameter_template_discipline_category_number :       ( 0, 0, 0, 0 )
         level_type :   Specified height level above ground (m)
         level :        1.5
         initial_time : 05/31/2020 (00:00)

      float RH_P0_L103_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Relative humidity
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Moisture
         parameter_template_discipline_category_number :       ( 0, 0, 1, 1 )
         level_type :   Specified height level above ground (m)
         level :        1.5
         initial_time : 05/31/2020 (00:00)

      float UGRD_P0_L103_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    U-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 2 )
         level_type :   Specified height level above ground (m)
         level :        10
         initial_time : 05/31/2020 (00:00)

      float VGRD_P0_L103_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    V-component of wind
         units :        m s-1
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Momentum
         parameter_template_discipline_category_number :       ( 0, 0, 2, 3 )
         level_type :   Specified height level above ground (m)
         level :        10
         initial_time : 05/31/2020 (00:00)

      float PRES_P0_L1_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Pressure
         units :        Pa
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Mass
         parameter_template_discipline_category_number :       ( 0, 0, 3, 0 )
         level_type :   Ground or water surface
         initial_time : 05/31/2020 (00:00)

      float PRMSL_P0_L101_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Pressure reduced to MSL
         units :        Pa
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Mass
         parameter_template_discipline_category_number :       ( 0, 0, 3, 1 )
         level_type :   Mean sea level (Pa)
         initial_time : 05/31/2020 (00:00)

      float TCDC_P0_L1_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Total cloud cover
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Cloud
         parameter_template_discipline_category_number :       ( 0, 0, 6, 1 )
         level_type :   Ground or water surface
         initial_time : 05/31/2020 (00:00)

      float LCDC_P0_L1_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Low cloud cover
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Cloud
         parameter_template_discipline_category_number :       ( 0, 0, 6, 3 )
         level_type :   Ground or water surface
         initial_time : 05/31/2020 (00:00)

      float MCDC_P0_L1_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Medium cloud cover
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Cloud
         parameter_template_discipline_category_number :       ( 0, 0, 6, 4 )
         level_type :   Ground or water surface
         initial_time : 05/31/2020 (00:00)

      float HCDC_P0_L1_GLL0 ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    High cloud cover
         units :        %
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Cloud
         parameter_template_discipline_category_number :       ( 0, 0, 6, 5 )
         level_type :   Ground or water surface
         initial_time : 05/31/2020 (00:00)

      float APCP_P8_L1_GLL0_acc1h ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Total precipitation
         units :        kg m-2
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Moisture
         parameter_template_discipline_category_number :       ( 8, 0, 1, 8 )
         level_type :   Ground or water surface
         type_of_statistical_processing :       Accumulation
         statistical_process_duration : 1 hours (ending at forecast time)
         initial_time : 05/31/2020 (00:00)

      float DSWRF_P8_L1_GLL0_avg1h ( forecast_time0, lat_0, lon_0 )
         center :       Japanese Meteorological Agency - Tokyo (RSMC)
         production_status :    Operational products
         long_name :    Downward short-wave radiation flux
         units :        W m-2
         _FillValue :   1e+20
         grid_type :    Latitude/longitude
         parameter_discipline_and_category :    Meteorological products, Short wave radiation
         parameter_template_discipline_category_number :       ( 8, 0, 4, 7 )
         level_type :   Ground or water surface
         type_of_statistical_processing :       Average
         statistical_process_duration : 1 hours (ending at forecast time)
         initial_time : 05/31/2020 (00:00)

      float lat_0 ( lat_0 )
         long_name :    latitude
         grid_type :    Latitude/Longitude
         units :        degrees_north
         Dj :   0.05
         Di :   0.0625
         Lo2 :  150
         La2 :  22.4
         Lo1 :  120
         La1 :  47.6

      float lon_0 ( lon_0 )
         long_name :    longitude
         grid_type :    Latitude/Longitude
         units :        degrees_east
         Dj :   0.05
         Di :   0.0625
         Lo2 :  150
         La2 :  22.4
         Lo1 :  120
         La1 :  47.6

      integer forecast_time0 ( forecast_time0 )
         long_name :    Forecast offset from initial time
         units :        hours
```


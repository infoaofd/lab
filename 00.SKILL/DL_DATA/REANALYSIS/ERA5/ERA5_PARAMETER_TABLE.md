# ERA5 PARAMETER TABLE

https://codes.ecmwf.int/grib/param-db/ 

C:\Users\boofo\lab\00.SKILL\DL_DATA\REANALYSIS\ERA5

| Table ID                 | Parameter ID | CDO    | Name                                             | VAR NAME             | Unit       |
| ------------------------ | ------------ | ------ | ------------------------------------------------ | -------------------- | ---------- |
| 170, 128, 180            | 143          | var134 | Convective precipitation                         |                      | m          |
| 228                      | 218          | var218 | Convective rain rate                             |                      | kg m-2 s-1 |
| 170, 128, 180            | 142          | var142 | Large-scale precipitation                        |                      | m          |
| 228                      | 219          | var219 | Large scale rain rate                            |                      | kg m-2 s-1 |
| 235                      | 33           |        | Mean surface sensible heat flux                  | U_GRD_GDS0_SFC_ave1h |            |
| 235                      | 34           |        | Mean surface latent heat flux                    | V_GRD_GDS0_SFC_ave1h |            |
| 235                      | 37           |        | Mean surface net short-wave radiation flux       | MNTSF_GDS0_SFC_ave1h |            |
| 235                      | 38           |        | Mean surface net long-wave radiation flux        | SGCVV_GDS0_SFC_ave1h |            |
| 190,160,180,128,170      | 129          | var129 | Geopotential                                     |                      | m2 s-2     |
| 170,128,190              | 157          | var157 | Relative humidity                                |                      | %          |
| 160,180,128,170          | 133          | var133 | Specific humidity                                |                      | kg kg-1    |
| 128,170,190,160,180      | 130          | var130 | Temperature                                      |                      | K          |
|                          | 131          | var131 | U component of wind                              |                      | m s-1      |
|                          | 132          | var132 | V component of wind                              |                      | m s-1      |
|                          | 135          | var135 | Vertical velocity                                |                      | Pa s-1     |
| 128,170,190,160,180      | 146          | var146 | Surface sensible heat flux                       |                      | J m-2      |
| 128,180,170              | 180          | var180 | Eastward turbulent surface stress                |                      | N m-2 s    |
| 128                      | 34           | var34  | sea_surface_temperature                          |                      |            |
| 128,  170, 190, 160, 180 | 151          | var151 | Mean sea level pressure                          |                      |            |
| 3, 2, 1, 128, 180        | 165          |        | 10 metre U wind component                        |                      |            |
| 2,1,3,180,160,190,128    | 166          |        | 10 metre V wind component                        |                      |            |
| 128                      | 135          |        | Vertical velocity                                |                      | Pa/s       |
| 162                      | 71           |        | Vertical integral of eastward water vapour flux  |                      | kg m-1 s-1 |
| 162                      | 72           |        | Vertical integral of northward water vapour flux |                      | kg m-1 s-1 |
|                          |              |        | Total column vertically-integrated water vapour  |                      |            |

​        'convective_available_potential_energy', 'convective_inhibition', 'convective_precipitation',
​        'convective_rain_rate', 'gravity_wave_dissipation', 'large_scale_precipitation',
​        'large_scale_rain_rate',
​        '10m_u_component_of_wind', '10m_v_component_of_wind', '2m_dewpoint_temperature',
​        '2m_temperature', 'mean_sea_level_pressure', 'mean_surface_latent_heat_flux',
​        'mean_surface_sensible_heat_flux', 'sea_surface_temperature', 'surface_pressure',
# ERA5_PV_GEOPTENTIAL

## ファイルの所在

/work01/DATA/ERA5/ECS/01HR/2021/08



## ファイルの情報

```bash
$ cdo showname ERA5_ECS_PRS_01HR_20210812.grib 
 var129 var60 var133 var130 var131 var132
cdo    showname: Processed 6 variables [0.04s 10MB].
```

https://codes.ecmwf.int/grib/param-db/?id=60

| Parameter ID | 60                  |
| ------------ | ------------------- |
| Name         | Potential vorticity |

| Parameter ID | 129          |
| ------------ | ------------ |
| Name         | Geopotential |

```
2023-07-21_19-38
/work01/DATA/ERA5/ECS/01HR/2021/08
$ cdo sinfo ERA5_ECS_PRS_01HR_20210812.grib 
   File format : GRIB
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 129.128       
     2 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 60.128        
     3 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 133.128       
     4 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 130.128       
     5 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 131.128       
     6 : ECMWF    unknown  v instant      16   1     13041   1  P16  : 132.128       
   Grid coordinates :
     1 : lonlat                   : points=13041 (161x81)
                              lon : 95 to 135 by 0.25 degrees_east
                              lat : 40 to 20 by -0.25 degrees_north
   Vertical coordinates :
     1 : pressure                 : levels=16
                             plev : 20000 to 100000 Pa
   Time coordinate :  unlimited steps
     RefTime =  2021-08-12 00:00:00  Units = hours  Calendar = proleptic_gregorian
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  2021-08-12 00:00:00  2021-08-12 01:00:00  2021-08-12 02:00:00  2021-08-12 03:00:00
  2021-08-12 04:00:00  2021-08-12 05:00:00  2021-08-12 06:00:00  2021-08-12 07:00:00
  2021-08-12 08:00:00  2021-08-12 09:00:00  2021-08-12 10:00:00  2021-08-12 11:00:00
  2021-08-12 12:00:00  2021-08-12 13:00:00  2021-08-12 14:00:00  2021-08-12 15:00:00
  2021-08-12 16:00:00  2021-08-12 17:00:00  2021-08-12 18:00:00  2021-08-12 19:00:00
  2021-08-12 20:00:00  2021-08-12 21:00:00  2021-08-12 22:00:00  2021-08-12 23:00:00
cdo    sinfo: Processed 6 variables over 24 timesteps [0.28s 10MB].
```



## CDOのインストール

### Anacondaのインストール

https://www.python.jp/install/anaconda/unix/install.html

### CDOのインストール

```
conda install -c conda-forge cdo
```



## NetCDFに変換

### 

```bash
cdo -f nc4 copy INPUT.grib OUTPUT.nc
```

```bash
$ cdo -f nc4 copy ERA5_ECS_PRS_01HR_20210812.grib ERA5_ECS_PRS_01HR_20210812.nc
```

```

cdo    copy: Processed 30046464 values from 6 variables over 24 timesteps 
```

```
$ ll ERA5_ECS_PRS_01HR_20210812.* 
```

```
-rw-r--r--. 1 am  58M 2023-02-03 17:23 ERA5_ECS_PRS_01HR_20210812.grib
-rw-r--r--. 1 am 115M 2023-07-21 19:49 ERA5_ECS_PRS_01HR_20210812.nc
```

```
$ ncdump -h ERA5_ECS_PRS_01HR_20210812.nc
```



```
netcdf ERA5_ECS_PRS_01HR_20210812 {
dimensions:
        time = UNLIMITED ; // (24 currently)
        lon = 161 ;
        lat = 81 ;
        plev = 16 ;
variables:
        double time(time) ;
                time:standard_name = "time" ;
                time:units = "hours since 2021-8-12 00:00:00" ;
                time:calendar = "proleptic_gregorian" ;
                time:axis = "T" ;
        double lon(lon) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
                lon:axis = "X" ;
        double lat(lat) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
                lat:axis = "Y" ;
        double plev(plev) ;
                plev:standard_name = "air_pressure" ;
                plev:long_name = "pressure" ;
                plev:units = "Pa" ;
                plev:positive = "down" ;
                plev:axis = "Z" ;
        float var129(time, plev, lat, lon) ;
                var129:table = 128 ;
        float var60(time, plev, lat, lon) ;
                var60:table = 128 ;
        float var133(time, plev, lat, lon) ;
                var133:table = 128 ;
        float var130(time, plev, lat, lon) ;
                var130:table = 128 ;
        float var131(time, plev, lat, lon) ;
                var131:table = 128 ;
        float var132(time, plev, lat, lon) ;
                var132:table = 128 ;

// global attributes:
                :CDI = "Climate Data Interface version 1.9.10 (https://mpimet.mpg.de/cdi)" ;
                :Conventions = "CF-1.6" ;
                :institution = "European Centre for Medium-Range Weather Forecasts" ;
                :history = "Fri Jul 21 19:49:53 2023: cdo -f nc4 copy ERA5_ECS_PRS_01HR_20210812.grib ERA5_ECS_PRS_01HR_20210812.nc" ;
                :CDO = "Climate Data Operators version 1.9.10 (https://mpimet.mpg.de/cdo)" ;
}
```



## GrADSでGRIBファイルのまま読む

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/ANALYSIS/DATA_HANDLING/2022-12-14_GIRB/2022-12-14_GRIB%E3%81%AB%E6%85%A3%E3%82%8C%E3%82%8B.md

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/GRIB2GRADS.md


Y=1995; Y2=$(expr $Y + 1)

SEA=DJF
INDIR=GRIB/${Y}${SEA}
INLIST=$(ls $INDIR/ERA5*SST*grib)
#echo $INLIST
ODIR=NC.SST/${Y}${SEA}

for IN in $INLIST; do
if [ ! -f $IN ];then echo; echo NO SUCH FILE,$IN;echo;exit 1;fi

echo $IN
OUT=${ODIR}/$(basename $IN .grib).nc

cdo -f nc4 copy $IN $OUT

done



#!/bin/bash

YS=1980; YE=1980; Y=$YS

while [ $Y -le $YE ]; do

PY=$(basename $0 .sh).PY
cat <<EOF>$PY
#! /usr/bin/env python3
#
# python script to download selected files from rda.ucar.edu
# after you save the file, don't forget to make it executable
#   i.e. - "chmod 755 <name_of_script>"
#
import requests
#
files = [
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}010100_${Y}013118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}020100_${Y}022918",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}030100_${Y}033118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}040100_${Y}043018",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}050100_${Y}053118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}060100_${Y}063018",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}070100_${Y}073118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}080100_${Y}083118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}090100_${Y}093018",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}100100_${Y}103118",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}110100_${Y}113018",
    "anl_p125/${Y}/anl_p125.007_hgt.${Y}120100_${Y}123118",
]
#
# download the data file(s)
for file in files:
    idx = file.rfind("/")
    if (idx > 0):
        ofile = file[idx+1:]
    else:
        ofile = file

    response = requests.get("https://data.rda.ucar.edu/ds628.0/" + file)
    with open(ofile, "wb") as f:
        f.write(response.content)
EOF

python3 $PY

Y=$(expr $Y + 1)
done #Y

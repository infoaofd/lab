# JRA55変数

[[_TOC_]]

## 1.25 度緯度／経度格子データ

JRA-55_handbook_LL125_v2_ja.pdf
https://jra.kishou.go.jp/JRA-55/document/JRA-55_handbook_LL125_v2_ja.pdf

### 4.1.1. 定数（LL125） 

定数（LL125）には，表 4-1 の要素を出力している。 
表 4-1  定数（LL125）出力要素 

| 数字符号 | パラメータ         | 単位 |
| -------- | ------------------ | ---- |
| 81       | 陸域（1=陸, 0=海） |      |



### 4.1.5. 等圧面解析値（anl_p125） 

等圧面解析値（anl_p125）には，客観解析で作成された 表 4-5 の要素を，第5.1 節「気圧座標系」に列挙した等圧面に対して，6 時間毎（00，06，12，18UTC）に出力している。 但し，湿数・比湿・相対湿度については，1000～100hPa の 27 層のみ出力して
いる。 

| 数字符号 | パラメータ       | 単位 | ファイル名    |
| -------- | ---------------- | ---- | ------------- |
| 35       | 流線関数         |      | anl_p125_strm |
| 36       | 速度ポテンシャル |      | anl_p125_vpot |



### 4.1.10. 等圧面予報値（fcst_p125）

| 数字符号 | パラメータ | 単位 | ファイル名     |
| -------- | ---------- | ---- | -------------- |
| 221      | 雲水量     |      | fcst_p125_cwat |
| 229      | 雲氷量     |      | fcst_p125_ciwc |



### 4.1.11. 2 次元物理量平均値（fcst_phy2m125） 

2 次元物理量平均値（fcst_phy2m125）には，表 4-11 の高度の要素の 0～3 時間予報平均値（00-03，06-09，12-15，18-21UTC）と 3～6 時間予報平均値（03-06，09-12，15-18，21-24UTC）を出力している。 なお，ファイル名中の日時は平均期間の開始日時を表している。 

表 4-11  2 次元物理量平均値（fcst_phy2m125）出力要素 

| 数字符号 | パラメータ                   | 単位                      | 等位面及び層           |
| -------- | ---------------------------- | ------------------------- | ---------------------- |
| 1        | 気圧                         | $\mathrm{Pa}$             | 地表面（地面又は水面） |
| 57       | 蒸発量                       | $\mathrm{mm \, day^{-1}}$ | 地表面（地面又は水面） |
| 61       | 総降水量                     | $\mathrm{mm \, day^{-1}}$ | 地表面（地面又は水面） |
| 62       | ラージスケールの降水量       | $\mathrm{mm \, day^{-1}}$ | 地表面（地面又は水面） |
| 63       | 対流性降水量                 | $\mathrm{mm \, day^{-1}}$ | 地表面（地面又は水面） |
| 121      | 潜熱フラックス               | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 122      | 顕熱フラックス               | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 204      | 短波放射フラックス（下向き） | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 205      | 長波放射フラックス（下向き） | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 211      | 短波放射フラックス（上向き） | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 212      | 長波放射フラックス（上向き） | $\mathrm{W \, m^{-2}}$    | 地表面（地面又は水面） |
| 204      | 短波放射フラックス（下向き） | $\mathrm{W \, m^{-2}}$    | 大気の名目上の上端     |
| 211      | 短波放射フラックス（上向き） | $\mathrm{W \, m^{-2}}$    | 大気の名目上の上端     |
| 212      | 長波放射フラックス（上向き） | $\mathrm{W \, m^{-2}}$    | 大気の名目上の上端     |



### 4.1.12. 等圧面物理量平均値（fcst_phy3m125） 

等圧面物理量平均値（fcst_phy3m125）には，第 5.1 節「気圧座標系」に列挙した等圧面に対して，表 4-12 の要素の 0～6 時間予報平均値（00-06，06-12，12-18，18-24UTC）を出力している。 
なお，ファイル名中の日時は平均期間の開始日時を表している。

| 数字符号 | パラメータ                                       | 単位                                | ファイル名          |
| -------- | ------------------------------------------------ | ----------------------------------- | ------------------- |
| 146      | 雲仕事関数                                       | $\mathrm{J \, kg^{-1}}$             | fcst_phy3m125_cwork |
| 222      | 断熱過程による気温の変化率（加熱率）             | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_adhr  |
| 230      | 雲底での上向きマスフラックス                     | $\mathrm{kg \, m^{-2} \,s^{-1}}$    | fcst_phy3m125_mflxb |
| 231      | 上向きマスフラックス                             | $\mathrm{kg \, m^{-2} \,s^{-1}}$    | fcst_phy3m125_mflux |
| 236      | 断熱過程による比湿の変化率                       | $\mathrm{kg \, kg^{-1} \,day^{-1}}$ | fcst_phy3m125_admr  |
| 241      | ラージスケールの降水による気温の変化率（加熱率） | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_lrghr |
| 242      | 対流による気温の変化率（加熱率）                 | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_cnvhr |
| 243      | 対流による比湿の変化率                           | $\mathrm{kg \, kg^{-1} \,day^{-1}}$ | fcst_phy3m125_cnvmr |
| 246      | 鉛直拡散による気温の変化率（加熱率）             | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_vdfhr |
| 249      | 鉛直拡散による比湿の変化率                       | $\mathrm{kg \, kg^{-1} \,day^{-1}}$ | fcst_phy3m125_vdfmr |
| 250      | 短波放射による気温の変化率（加熱率）             | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_swhr  |
| 251      | 長波放射による気温の変化率（加熱率）             | $\mathrm{K\,day^{-1}}$              | fcst_phy3m125_lwhr  |
| 253      | ラージスケールの降水による比湿の変化率           | $\mathrm{kg \, kg^{-1} \,day^{-1}}$ | fcst_phy3m125_lrgmr |




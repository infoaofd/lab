

**/work01/DATA/JRA55C/MON/SFC/ANL**

anl_surf125.C.001_pres.197211_197212 気圧(地表面) Pa

anl_surf125.C.051_spfh.197211_197212 比湿 kg/kg

anl_surf125.C.011_tmp.197211_197212 気温 K

anl_surf125.C.033_ugrd.197211_197212 10m高度の風のu成分 m/s

anl_surf125.C.034_vgrd.197211_197212 10m高度の風のv成分 m/s



**/work01/DATA/JRA55C/MON/SFC/DIA**

fcst_surf125.C.118_brtmp.197211_197212 輝度温度 K



**/work01/DATA/JRA55C/MON/SFC/FCT**

fcst_phy2m125.C.061_tprat.197211_197212 総降水量 mm/d

fcst_phy2m125.C.121_lhtfl.197211_197212 潜熱 W/m2

fcst_phy2m125.C.122_shtfl.197211_197212 顕熱 W/m2

fcst_phy2m125.C.204_dswrf.197211_197212 下向き短波 W/m2

fcst_phy2m125.C.205_dlwrf.197211_197212 下向き長波 W/m2

fcst_phy2m125.C.211_uswrf.197211_197212 上向き短波 W/m2

fcst_phy2m125.C.212_ulwrf.197211_197212 上向き長波 W/m2
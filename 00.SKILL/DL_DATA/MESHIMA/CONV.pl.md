# 風向風速データの読み出し

/work01/DATA
2022-11-26_12-07

```
$ cd /work01/DATA/MESHIMA/WIND_JCG/
```


### CONV.RUN.sh

下記のconv.plを複数の日付に渡って使用するためのスクリプト

#### 使用法

**例**: 2020年6月20日から8月31日までのデータを抜き出す

```
$ CONV.RUN.sh 20200620 20200831 > JCG.WIND_20200620-20200831.TXT
```

上の例の場合`JCG.WIND_20200620-20200831.TXT`にデータが書き出される



### conv.pl

特定の時刻だけ抜き出すときはconv.plを単体で用いる。

#### 使用法

```
$ conv.pl 年 ⽉ ⽇ 時 分
```

データは30分単位なので，分の箇所は00か30を与える


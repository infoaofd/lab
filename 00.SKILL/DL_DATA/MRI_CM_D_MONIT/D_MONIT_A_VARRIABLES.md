

# D_MONIT_A_VARIABLES

/work02/DATA3/MRI/2018/d_monit_a/2018070100

[[_TOC_]]

## 既にあるデータ

### 全球 

#### 領域と解像度

- **東西方向: 0E-365.75E** (1.25 degree resolution)

- **南北方向: 90S - 90N** (1.25 degree resolution)

- **鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

#### 気圧面データ

**瞬時値:** U, V, T, Q, Z, CWC, OMG

#### 地表面 (2次元)データ

**時間平均:** 

RRR    Total Precipitation                             kg/m^2/s          
RRC    Precipitation (Convective)                      kg/m^2/s          
Sn   0Snowfall Amount                                  g/m^2/s          
SnC    Snowfall Amount (Convective)                    kg/m^2/s          
Evap    Evaporation Amount                              kg/m^2/s          
FLMUS   Longitudinal Momentum Flux at Grnd. Surf.       N/m^2             
FLMVS   Latitudinal Momentum Flux at Grnd. Surf.        N/m^2             
FLLHS   Latent Heat Flux at Gound Surface               W/m^2             
FLSHS   Sensible Heat Flux at Ground Surface            W/m^2             
RLUT    Radiation Flux (Long ,Up  ,Top)                 W/m^2             
RLUTc  Radiation Flux (Long ,Up  ,Top,ClearSky)        W/m^2             
RSUT    Radiation Flux (Short,Up  ,Top)                 W/m^2             
RSUTc  Radiation Flux (Short,Up  ,Top,ClearSky)        W/m^2             
RSDT    Radiation Flux (Short,Down,Top)                 W/m^2             
RLUB    Radiation Flux (Long ,Up  ,Btm)                 W/m^2             
RLDB    Radiation Flux (Long ,Down,Btm)                 W/m^2             
RLDBc  Radiation Flux (Long ,Down,Btm,ClearSky)        W/m^2             
RSUB    Radiation Flux (Short,Up  ,Btm)                 W/m^2             
RSUBc  Radiation Flux (Short,Up  ,Btm,ClearSky)        W/m^2             
RSDB    Radiation Flux (Short,Down,Btm)                 W/m^2             
RSDBc  Radiation Flux (Short,Down,Btm,ClearSky)        W/m^2             
TPW     Total Precipitable Water                        kg/m^2            
TCWC   Total Cloud Water Content                       kg/m^2            
CLA      Cloud Fraction                                  0-1               
CLL      Cloud Fraction (Low)                            0-1               
CLM     Cloud Fraction (Middle)                         0-1               
CLH     Cloud Fraction (High)                           0-1        

**瞬時値: **

PSEA   Pressure at Mean Sea Level                     
US   Longitudinal Wind Speed at 10m-height          
VS   Latitudinal  Wind Speed at 10m-height          
SST   Sea Surface Temperature                        
TSG   Surface Skin Temperature                       
SnWe   Surface Snow Amount                            
ICE   Ice Area                                       
TICES  SEA ICE Temperature (SURF)                     
TICE1  SEA ICE Temperature (L1  )                     
TICE2  SEA ICE Temperature (L2  )                     
TICE3  SEA ICE Temperature (L3  )                     
TICE4  SEA ICE Temperature (L4  )                     
QS   Specific Humidity at 2m-height                 
TS   Temperature at 2m-height               



### 領域

#### 領域と解像度

- **東西方向: 90E-180E** (0.09375 degree resolution)

- **南北方向: 0.04N - 59.95N** (0.09375 degree resolution) 

- **鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

#### 気圧面データ

**瞬時値:** U, V, T, Q, Z, CWC, OMG

#### 地表面データ

**瞬時値:** 

PSEA   Pressure at Mean Sea Level              Pa                
US   Longitudinal Wind Speed at 10m-height   m/s               
VS   Latitudinal  Wind Speed at 10m-height   m/s               
SST   Sea Surface Temperature                 K                 
TSG   Surface Skin Temperature                K                 
SnWe   Surface Snow Amount                     m                 
ICE   Ice Area                                m^2/m^2           
TICESx  SEA ICE Temperature (SURF)              K                 
TICE1x  SEA ICE Temperature (L1  )              K                 
TICE2x  SEA ICE Temperature (L2  )              K                 
TICE3x  SEA ICE Temperature (L3  )              K                 
TICE4x  SEA ICE Temperature (L4  )              K                 
QS   Specific Humidity at 2m-height          kg kg-1           
TS   Temperature at 2m-height                K   

**平均値:**

RRR   Total Precipitation                            kg/m^2/s          
RRC   Precipitation (Convective)                     kg/m^2/s          
Sn   Snowfall Amount                                kg/m^2/s          
SnC   Snowfall Amount (Convective)                   kg/m^2/s          
Evap   Evaporation Amount                             kg/m^2/s          
FLMUSx  Longitudinal Momentum Flux at Grnd. Surf.      N/m^2             
FLMVSx  Latitudinal Momentum Flux at Grnd. Surf.       N/m^2             
FLLHSx  Latent Heat Flux at Gound Surface              W/m^2             
FLSHSx  Sensible Heat Flux at Ground Surface           W/m^2             
RLUT   Radiation Flux (Long ,Up  ,Top)                W/m^2             
RLUTcx  Radiation Flux (Long ,Up  ,Top,ClearSky)       W/m^2             
RSUT   Radiation Flux (Short,Up  ,Top)                W/m^2             
RSUTcx  Radiation Flux (Short,Up  ,Top,ClearSky)       W/m^2             
RSDT   Radiation Flux (Short,Down,Top)                W/m^2             
RLUB   Radiation Flux (Long ,Up  ,Btm)                W/m^2             
RLDB   Radiation Flux (Long ,Down,Btm)                W/m^2             
RLDBcx  Radiation Flux (Long ,Down,Btm,ClearSky)       W/m^2             
RSUB   Radiation Flux (Short,Up  ,Btm)                W/m^2             
RSUBcx  Radiation Flux (Short,Up  ,Btm,ClearSky)       W/m^2             
RSDB   Radiation Flux (Short,Down,Btm)                W/m^2             
RSDBcx  Radiation Flux (Short,Down,Btm,ClearSky)       W/m^2             
TPW   Total Precipitable Water                       kg/m^2            
TCWC   Total Cloud Water Content                      kg/m^2            
CLA   Cloud Fraction                                 0-1               
CLL   Cloud Fraction (Low)                           0-1               
CLM   Cloud Fraction (Middle)                        0-1               
CLH   Cloud Fraction (High)                          0-1               





## ctlファイル

### 気圧面データ

#### 全球 (瞬時値)

**東西方向: 0E-365.75E** (1.25 degree resolution)

**南北方向: 90S - 90N** (1.25 degree resolution)

**鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

##### atm_snp_1hr_glb.ncctl

```bash
DSET ^atm_snp_1hr_glb.nc
UNDEF -9.99E33
XDEF  288  LINEAR     0.0000000    1.2500000
YDEF  145  LEVELS
      -90.00000 ...   90.00000
ZDEF   38  LEVELS
     1000.000 ... 100.000 ... 10.000 .. 1.000    0.400
TDEF   265 LINEAR  00Z01JUL2018   1HR
VARS       7
U   38  t,z,y,x  Longitudinal Wind Speed  m/s              1
V   38  t,z,y,x  Latitudinal Wind Speed   m/s              1
T   38  t,z,y,x  Temperature              K                1
Q   38  t,z,y,x  Specific Humidity        kg/kg            1
Z   38  t,z,y,x  Geopotential Height      m                1
CWC   38  t,z,y,x  cloud_water_content    kg/kg            1
OMG   38  t,z,y,x  Vertical P-Velocity    Pa/s             1
ENDVARS
```





#### 領域（瞬時値）

**東西方向: 90E-180E** (0.09375 degree resolution)

**南北方向: 0.04N - 59.95N** (0.09375 degree resolution) 

**鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

##### atm_snp_1hr.ncctl

```
XDEF  961  LINEAR    90.0000000    0.0937500
YDEF  640  LEVELS
        0.04688 ...    59.95312
ZDEF   38  LEVELS
     1000.000 ... 100.000 ...  10.000 ... 1.000    0.400
```

```bash
U   38  t,z,y,x  Longitudinal Wind Speed   m/s              1
V   38  t,z,y,x  Latitudinal Wind Speed    m/s              1
T   38  t,z,y,x  Temperature               K                1
Q   38  t,z,y,x  Specific Humidity         kg/kg            1
Z   38  t,z,y,x  Geopotential Height       m                1
CWC   38  t,z,y,x  cloud_water_content     kg/kg            1
OMG   38  t,z,y,x  Vertical P-Velocity     
```



### 地表面データ

#### 全球 (時間平均)

**東西方向: 0E-365.75E** (1.25 degree resolution)

**南北方向: 90S - 90N** (1.25 degree resolution)

**鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

##### sfc_avr_1hr_glb.ncctl

```bash
TITLE sfc_avr_1hr_glb    AVR ( Time average data )
UNDEF -9.99E33
XDEF  288  LINEAR     0.0000000    1.2500000
YDEF  145  LEVELS
      -90.00000  -88.75000  ... 90.00000
ZDEF    1  LEVELS  1000.000
TDEF   264 LINEAR  00Z01JUL2018   1HR
VARS      27
RRR   0  t,y,x  Total Precipitation                             kg/m^2/s          
RRC   0  t,y,x  Precipitation (Convective)                      kg/m^2/s          
Sn   0  t,y,x  Snowfall Amount                                  g/m^2/s          
SnC   0  t,y,x  Snowfall Amount (Convective)                    kg/m^2/s          
Evap   0  t,y,x  Evaporation Amount                              kg/m^2/s          
FLMUS   0  t,y,x  Longitudinal Momentum Flux at Grnd. Surf.       N/m^2             
FLMVS   0  t,y,x  Latitudinal Momentum Flux at Grnd. Surf.        N/m^2             
FLLHS   0  t,y,x  Latent Heat Flux at Gound Surface               W/m^2             
FLSHS   0  t,y,x  Sensible Heat Flux at Ground Surface            W/m^2             
RLUT   0  t,y,x  Radiation Flux (Long ,Up  ,Top)                 W/m^2             
RLUTc   0  t,y,x  Radiation Flux (Long ,Up  ,Top,ClearSky)        W/m^2             
RSUT   0  t,y,x  Radiation Flux (Short,Up  ,Top)                 W/m^2             
RSUTc   0  t,y,x  Radiation Flux (Short,Up  ,Top,ClearSky)        W/m^2             
RSDT   0  t,y,x  Radiation Flux (Short,Down,Top)                 W/m^2             
RLUB   0  t,y,x  Radiation Flux (Long ,Up  ,Btm)                 W/m^2             
RLDB   0  t,y,x  Radiation Flux (Long ,Down,Btm)                 W/m^2             
RLDBc   0  t,y,x  Radiation Flux (Long ,Down,Btm,ClearSky)        W/m^2             
RSUB   0  t,y,x  Radiation Flux (Short,Up  ,Btm)                 W/m^2             
RSUBc   0  t,y,x  Radiation Flux (Short,Up  ,Btm,ClearSky)        W/m^2             
RSDB   0  t,y,x  Radiation Flux (Short,Down,Btm)                 W/m^2             
RSDBc   0  t,y,x  Radiation Flux (Short,Down,Btm,ClearSky)        W/m^2             
TPW   0  t,y,x  Total Precipitable Water                        kg/m^2            
TCWC   0  t,y,x  Total Cloud Water Content                       kg/m^2            
CLA   0  t,y,x  Cloud Fraction                                  0-1               
CLL   0  t,y,x  Cloud Fraction (Low)                            0-1               
CLM   0  t,y,x  Cloud Fraction (Middle)                         0-1               
CLH   0  t,y,x  Cloud Fraction (High)                           0-1               
ENDVARS
```



#### 全球 (瞬時値)

**東西方向: 0E-365.75E** (1.25 degree resolution)

**南北方向: 90S - 90N** (1.25 degree resolution)

**鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

##### sfc_snp_1hr_glb.ncctl

```
DSET ^sfc_snp_1hr_glb.nc
DTYPE NETCDF
OPTIONS  
TITLE sfc_snp_1hr_glb    SNP ( Snapshot data )
UNDEF -9.99E33
XDEF  288  LINEAR     0.0000000    1.2500000
YDEF  145  LEVELS
      -90.00000  -88.75000 ...  90.00000
ZDEF    1  LEVELS  1000.000
TDEF   265 LINEAR  00Z01JUL2018   1HR
VARS      14
PSEA   0  t,y,x  Pressure at Mean Sea Level                Pa                
US   0  t,y,x  Longitudinal Wind Speed at 10m-height     m/s               
VS   0  t,y,x  Latitudinal  Wind Speed at 10m-height     m/s               
SST   0  t,y,x  Sea Surface Temperature                   K                 
TSG   0  t,y,x  Surface Skin Temperature                  K                 
SnWe   0  t,y,x  Surface Snow Amount                       m                 
ICE   0  t,y,x  Ice Area                                  m^2/m^2           
TICES   0  t,y,x  SEA ICE Temperature (SURF)                K                 
TICE1   0  t,y,x  SEA ICE Temperature (L1  )                K                 
TICE2   0  t,y,x  SEA ICE Temperature (L2  )                K                 
TICE3   0  t,y,x  SEA ICE Temperature (L3  )                K                 
TICE4   0  t,y,x  SEA ICE Temperature (L4  )                K                 
QS   0  t,y,x  Specific Humidity at 2m-height            kg kg-1           
TS   0  t,y,x  Temperature at 2m-height                  K                 
ENDVARS
```



#### 領域（瞬時値）

**東西方向: 90E-180E** (0.09375 degree resolution)

**南北方向: 0.04N - 59.95N** (0.09375 degree resolution) 

**鉛直方向**: 1000hPa, ..., 100hPa, ..., 10hPa, ... 1.0 hPa (成層圏を含む)

##### sfc_snp_1hr.ncctl

```bash
DSET ^sfc_snp_1hr.nc
DTYPE NETCDF
OPTIONS  
TITLE sfc_snp_1hr    SNP ( Snapshot data )
UNDEF -9.99E33
XDEF  961  LINEAR    90.0000000    0.0937500
YDEF  640  LEVELS
        0.04688    0.14062  ...  59.95312
ZDEF    1  LEVELS  1000.000
TDEF   265 LINEAR  00Z01JUL2018   1HR
VARS      14
PSEA   0  t,y,x  Pressure at Mean Sea Level              Pa                
US   0  t,y,x  Longitudinal Wind Speed at 10m-height   m/s               
VS   0  t,y,x  Latitudinal  Wind Speed at 10m-height   m/s               
SST   0  t,y,x  Sea Surface Temperature                 K                 
TSG   0  t,y,x  Surface Skin Temperature                K                 
SnWe   0  t,y,x  Surface Snow Amount                     m                 
ICE   0  t,y,x  Ice Area                                m^2/m^2           
TICES   0  t,y,x  SEA ICE Temperature (SURF)              K                 
TICE1   0  t,y,x  SEA ICE Temperature (L1  )              K                 
TICE2   0  t,y,x  SEA ICE Temperature (L2  )              K                 
TICE3   0  t,y,x  SEA ICE Temperature (L3  )              K                 
TICE4   0  t,y,x  SEA ICE Temperature (L4  )              K                 
QS   0  t,y,x  Specific Humidity at 2m-height          kg kg-1           
TS   0  t,y,x  Temperature at 2m-height                K                 
ENDVARS
```


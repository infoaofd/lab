#!/usr/bin/env python
"""
NCEP ADP Global Upper Air and Surface Weather Observations
(PREPBUFR format)
ds337.0 | DOI: 10.5065/Z83F-N512
https://rda.ucar.edu/datasets/ds337.0/dataaccess/
"""

"""
Python script to download selected files from rda.ucar.edu.
After you save the file, don't forget to make it executable
i.e. - "chmod 755 <name_of_script>"
"""
import sys, os
from urllib.request import build_opener

opener = build_opener()

filelist = [
  'https://data.rda.ucar.edu/ds337.0/tarfiles/2022/prepbufr.20220619.nr.tar.gz',
  'https://data.rda.ucar.edu/ds337.0/tarfiles/2022/prepbufr.20220620.nr.tar.gz'
]

for file in filelist:
    ofile = os.path.basename(file)
    sys.stdout.write("downloading " + ofile + " ... ")
    sys.stdout.flush()
    infile = opener.open(file)
    outfile = open(ofile, "wb")
    outfile.write(infile.read())
    outfile.close()
    sys.stdout.write("done\n")

# DL_PREPBUFR

[[_TOC_]]

## データのダウンロード

### rda-download.py

```bash
$ python3 rda-download.py 
```

```bash
$ tar xzvf prepbufr.20220619.nr.tar.gz 
prepbufr.gdas.20220619.t00z.nr
prepbufr.gdas.20220619.t06z.nr
prepbufr.gdas.20220619.t12z.nr
prepbufr.gdas.20220619.t18z.nr
```

rda-download.py

```python
#!/usr/bin/env python
"""
NCEP ADP Global Upper Air and Surface Weather Observations
(PREPBUFR format)
ds337.0 | DOI: 10.5065/Z83F-N512
https://rda.ucar.edu/datasets/ds337.0/dataaccess/
"""

"""
Python script to download selected files from rda.ucar.edu.
After you save the file, don't forget to make it executable
i.e. - "chmod 755 <name_of_script>"
"""
import sys, os
from urllib.request import build_opener

opener = build_opener()

filelist = [
  'https://data.rda.ucar.edu/ds337.0/tarfiles/2022/prepbufr.20220619.nr.tar.gz'
]

for file in filelist:
    ofile = os.path.basename(file)
    sys.stdout.write("downloading " + ofile + " ... ")
    sys.stdout.flush()
    infile = opener.open(file)
    outfile = open(ofile, "wb")
    outfile.write(infile.read())
    outfile.close()
    sys.stdout.write("done\n")
```



## ライブラリのインストール

### 要点

BUFRLIBをインストール

rda-prepbufr-decodeをインストール (BUFRLIBをリンク)



### コツ

BUFRLIBは少し前のバージョンである11.4.0にしたところ, rda-prepbufr-decodeがうまくリンクできた

rda-prepbufr-decodeのコンパイルにはgfortranを使う



### BUFRLIB

https://github.com/NOAA-EMC/NCEPLIBS-bufr

```bash
$ cd 2023-08-30_20_BUFRLIB
```

```bash
$ git clone https://github.com/NOAA-EMC/NCEPLIBS-bufr.git
```

```bash
$ cd NCEPLIBS-bufr
```

```bash
$ mkdir build && cd build
```



Installation of the library and utilities will be under `path1`. Installation of the master BUFR tables will be under `path2`, or under `path1` if `-DMASTER_TABLE_DIR=path2` is omitted from the above cmake command.

```
$ ls /usr/local/
66                           gmt4/             makefile
Changes                      grads-2.2.1/      netcdf-c-4.8.0/
bin/                         grib_api@         openmpi-4.1.4/
eccodes/                     grib_api-1.28.0/  sbin/
eccodes-2.7.3-Source.tar.gz  hdf5-1.10.9/      share/
emoslib/                     hdf5-1.8.22@      src/
emoslib-4.5.9/               include/          src2all*
etc/                         jasper-1.900.1/   szip-2.1.1/
flex_extract/                lib/              wgrib/
flexpart_v10.4_3d7eebf/      lib64/            zlib-1.2.13/
games/                       libexec/          zlib-1.2.9/
```



```bash
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local/BUFRLIB -DMASTER_TABLE_DIR=/usr/local/BUFRLIB_TABLE ..
```

```bash
CMake Error at CMakeLists.txt:4 (cmake_minimum_required):
  CMake 3.15 or higher is required.  You are running version 2.8.12.2


-- Configuring incomplete, errors occurred!
```

```bash
$ conda install -c anaconda cmake
```

```bash
$ cmake -DCMAKE_INSTALL_PREFIX=/usr/local/BUFRLIB -DMASTER_TABLE_DIR=/usr/local/BUFRLIB_TABLE ..
```

```bash
$ make -j4
```

```
/work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/test/test_ufbrw.F90:165.14:

          stop n  
              1
エラー: Parameter 'n' at (1) has not been declared or is a variable, which does not reduce to a constant expression
```

```
$ cd ../test
```

```bash
$ org.sh test_ufbrw.F90 
```

```bash
$ vi test_ufbrw.F90 
```

```bash
$ diff ORG_230830-2106_test_ufbrw.F90 test_ufbrw.F90 
165c165
<           stop n  
---
>           stop !n  
167c167
<           stop n  
---
>           stop !n  

```

```bash
$ cd ../build
```

```bash
$ make -j4
```



```bash
$ ctest
```

```
The following tests FAILED:
         20 - outtest1_4 (Failed)
         45 - outtest1_8 (Failed)
         70 - outtest1_d (Failed)
         81 - test_c_interface_2_4 (Failed)
Errors while running CTest
Output from these tests are in: /work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/build/Testing/Temporary/LastTest.log
Use "--rerun-failed --output-on-failure" to re-run the failed cases verbosely.
```

```
$ ctest --rerun-failed  --output-on-failure
```

```
Test project /work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/build
    Start 20: outtest1_4
1/4 Test #20: outtest1_4 .......................***Failed    0.02 sec
./outtest1_4
 Testing writing OUT_1 using OPENBF IO = OUT and LUNIN != LUNDX,
 and using 2-03-YYY to change reference values
cmp -s out1.bufr testfiles/OUT_1

    Start 45: outtest1_8
2/4 Test #45: outtest1_8 .......................***Failed    0.01 sec
./outtest1_8
 Testing writing OUT_1 using OPENBF IO = OUT and LUNIN != LUNDX,
 and using 2-03-YYY to change reference values
cmp -s out1.bufr testfiles/OUT_1

    Start 70: outtest1_d
3/4 Test #70: outtest1_d .......................***Failed    0.01 sec
./outtest1_d
 Testing writing OUT_1 using OPENBF IO = OUT and LUNIN != LUNDX,
 and using 2-03-YYY to change reference values
cmp -s out1.bufr testfiles/OUT_1

    Start 81: test_c_interface_2_4
4/4 Test #81: test_c_interface_2_4 .............***Failed    0.05 sec
 ++++++++++++++BUFR ARCHIVE LIBRARY+++++++++++++++++
 BUFRLIB: MAXOUT - THE RECORD LENGTH OF ALL BUFR MESSAGES CREATED FROM THIS POINT ON IS BEING CHANGED FROM   10000 TO   25000    
 ++++++++++++++BUFR ARCHIVE LIBRARY+++++++++++++++++
```



```bash
$ sudo make install
```

```
/work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/build

$ ls /usr/local/BUFRLIB
bin/  include/  lib64/

$ ls /usr/local/BUFRLIB_TABLE|tail
bufrtab.TableD_STD_0_31
bufrtab.TableD_STD_0_32
bufrtab.TableD_STD_0_33
bufrtab.TableD_STD_0_34
bufrtab.TableD_STD_0_35
bufrtab.TableD_STD_0_36
bufrtab.TableD_STD_0_37
bufrtab.TableD_STD_0_38
bufrtab.TableD_STD_0_39
bufrtab.TableD_STD_0_40
```



#### バージョンを11.4.0に落とした

```
/work09/am/INSTALL/2023-08-30_22_BUFRLIB11.4.0/NCEPLIBS-bufr-bufr_v11.4.0/build
```

```
cmake -DCMAKE_INSTALL_PREFIX=/usr/local/BUFRLIB11.4.0 ..
```

```
$ make -j4
```

```
$ ctest
*********************************
No test configuration file found!
*********************************
Usage

  ctest [options]
```

上記原因不明

```
$ sudo make install
.....
-- Installing: /usr/local/BUFRLIB11.4.0/bin/readmp.x
-- Installing: /usr/local/BUFRLIB11.4.0/bin/sinv.x
```

おそらくインストール成功

```
$ ls /usr/local/BUFRLIB11.4.0/lib
cmake/  libbufr_4_DA.a  libbufr_8_DA.a  libbufr_d_DA.a
```



### Utilities

@brief Collection of commonly-used utilities based on the library.

The NCEPLIBS-bufr library includes some command line utilities which operate on BUFR files.

| Utility                                                      | Description                                                  |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| [debufr](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#debufr) | Read BUFR file and write verbose listing of contents         |
| [readbp](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#readbp) | Read prepbufr file and print each report one at a time       |
| [readmp](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#readmp) | Read BUFR file containing embedded DX BUFR tables, and print each report one at a time |
| [binv](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#binv) | Print inventory of BUFR file by message type                 |
| [sinv](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#sinv) | Print inventory of BUFR satellite data file by platform and instrument type |
| [cmpbqm](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#cmpbqm) | Print inventory of observations from prepbufr file by variable, report type and quality mark |
| [gettab](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#gettab) | Print embedded DX BUFR tables from within a BUFR file        |
| [split_by_subset](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#split) | Split a BUFR file into separate BUFR files for each subset type |
| [xbfmg](https://github.com/NOAA-EMC/NCEPLIBS-bufr/blob/develop/docs/utils.md#xbfmg) | Split a BUFR file into separate BUFR files for each message  |



### rda-prepbufr-decode

https://github.com/NCAR/rda-prepbufr-decode

To compile the PREPBUFR libraries and extraction code, go to the install directory. Execute the install.sh script to complete the compilation.

The executable will be placed in the exe directory.
(exe/readpb_config.x: program used to extract data from PREPBUFR files) The command syntax to run the executable is as follows:

```
readpb_config.x prepbufr.in prepbufr.out config_file
```

where 'prepbufr.in' is the input PREPBUFR file, 'prepbufr.out' is the output ASCII file, and config_file is the configuration file passed into readpb_config.x. An example template for the configuration file is located in the 'config' directory.



```bash
$ cd 2023-08-30_20_RDA_PREPBUFR-DECODE
```

```bash
$ git clone https://github.com/NCAR/rda-prepbufr-decode.git
```

```bash
/work09/am/INSTALL/2023-08-30_20_RDA_PREPBUFR-DECODE
$ cd rda-prepbufr-decode/
```

```bash
$ ls
LICENSE  README.md  config/  docs/  install/  src/  test/
```

```bash
$ cd install
```

```bash
$ source /opt/intel/oneapi/setvars.sh
```

```bash
$ which ifort
/opt/intel/oneapi/compiler/2022.2.1/linux/bin/intel64/ifort
```

```bash
$ cp install.sh install.ORG.sh
```

```
$ vi install.sh
```

```
$ diff install.ORG.sh install.sh 
15c15
< LIB=/glade/apps/opt/BUFRLIB/11.0.0/intel/12.1.5/lib
---
> LIB=/usr/local/BUFRLIB
19c19
< FC=/glade/apps/opt/cmpwrappers/ifort
---
> FC=/opt/intel/oneapi/compiler/2022.2.1/linux/bin/intel64/ifort
```

```
$ install.sh
Compiling readpb_config ...
Linking ...
ifort: error #10236: File not found:  '/usr/local/BUFRLIB/libbufr.a'
```

```
$ ls /usr/local/BUFRLIB/lib64
cmake/  libbufr_4.a
```

```
$ vi install.sh
```

```
$ diff install.ORG.sh install.sh 
15c15
< LIB=/glade/apps/opt/BUFRLIB/11.0.0/intel/12.1.5/lib
---
> LIB=/usr/local/BUFRLIB/lib64
19c19
< FC=/glade/apps/opt/cmpwrappers/ifort
---
> FC=/opt/intel/oneapi/compiler/2022.2.1/linux/bin/intel64/ifort
31c31
< $FC $fflag -o $EXE/readpb_config.x readpb_config.o $LIB/libbufr.a
---
> $FC $fflag -o $EXE/readpb_config.x readpb_config.o $LIB/libbufr_4.a
```

```
$ install.sh
Compiling readpb_config ...
Linking ...
ld: 出力ファイル ../exe/readpb_config.x が開けません: そのよう
なファイルやディレクトリはありません
```

```
$ mkd ../exe
mkdir: ディレクトリ `../exe' を作成しました
```

```
$ install.sh
Compiling readpb_config ...
Linking ...
/usr/local/BUFRLIB/lib64/libbufr_4.a(datelen.f.o): 関数 `datelen_' 内:
/work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/src/datelen.f:65: `_gfortran_st_write' に対する定義されていない参照です
/work09/am/INSTALL/2023-08-30_20_BUFRLIB/NCEPLIBS-bufr/src/datelen.f:65: `_gfortran_transfer_integer_write' に対する定義されていない参照です

```

コンパイラがifortだとリンクがうまくいかないようである。

#### コンパイラをifortからgfortranに変更した

```
/work09/am/INSTALL/2023-08-30_20_RDA_PREPBUFR-DECODE/rda-prepbufr-decode/install
$ diff install.ORG.sh install.sh 
15c15
< LIB=/glade/apps/opt/BUFRLIB/11.0.0/intel/12.1.5/lib
---
> LIB=/usr/local/BUFRLIB11.4.0/lib
19c19,20
< FC=/glade/apps/opt/cmpwrappers/ifort
---
> #FC=/opt/intel/oneapi/compiler/2022.2.1/linux/bin/intel64/ifort
> FC=gfortran
31c32
< $FC $fflag -o $EXE/readpb_config.x readpb_config.o $LIB/libbufr.a
---
> $FC $fflag -o $EXE/readpb_config.x readpb_config.o $LIB/libbufr_d_DA.a
```



### 実行例

#### config.ECS202206

```
/work09/am/INSTALL/2023-08-30_20_RDA_PREPBUFR-DECODE/rda-prepbufr-decode/exe
$ cat config.ECS202206 
# Brief description of prepbufr_config:

LATS 34 21                      # Enter lats from N to S (i.e. 90 -90) 
LONS 120 130                    # Enter lons from 0 to 360

# For a description of PREPBUFR Mnemonics, see:
http://www.emc.ncep.noaa.gov/mmb/data_processing/prepbufr.doc/table_1.htm
```

緯度は<font color="red">北から南</font>

#### readpb_config.x

```bash
/work09/am/INSTALL/2023-08-30_20_RDA_PREPBUFR-DECODE/rda-prepbufr-decode/exe
$ readpb_config.x /work01/DATA/PREPBUFR/prepbufr.gdas.20220619.t06z.nr 20220619_06 config.ECS202206 
 infile = /work01/DATA/PREPBUFR/prepbufr.gdas.20220619.t06z.nr                                                                                                                                                                                                                                                        
 outfile = 20220619_06                                                                                                                                                                                                                                                                                                 
 config file = config.ECS202206                                                                                                                                                                                                                                                                                            
 Latitude Values:    21.000000000000000        34.000000000000000     
 Longitude Values:    120.00000000000000        132.00000000000000   
```



### Table 1.a  Current Table A Entries in PREPBUFR mnemonic table.

 (last revised 2/7/2018)

THE FOLLOWING ARE TABLE A ENTRIES FOR PREPBUFR MESSAGE TYPES

| MNEMONIC | NUMBER | DESCRIPTION                                                  |
| -------- | ------ | ------------------------------------------------------------ |
| ADPUPA   | A48102 | UPPER-AIR (RAOB, PIBAL, RECCO, DROPS) REPORTS                |
| AIRCAR   | A48103 | MDCRS ACARS AIRCRAFT REPORTS                                 |
| AIRCFT   | A48104 | AIREP, PIREP, AMDAR, TAMDAR AIRCRAFT REPORTS                 |
| SATWND   | A48105 | SATELLITE-DERIVED WIND REPORTS                               |
| PROFLR   | A48106 | WIND PROFILER AND ACOUSTIC SOUNDER (SODAR) REPORTS           |
| VADWND   | A48107 | VAD (NEXRAD) WIND REPORTS                                    |
| SATEMP   | A48108 | POES SOUNDING, RETRIEVAL, RADIANCE DATA (TOVS)               |
| ADPSFC   | A48109 | SURFACE LAND (SYNOPTIC, METAR) REPORTS                       |
| SFCSHP   | A48110 | SURFACE MARINE (SHIP, BUOY, C-MAN/TIGE GAUGE PLATFORM) REPORTS |
| SFCBOG   | A48111 | MEAN SEA-LEVEL PRESSURE BOGUS REPORTS                        |
| SPSSMI   | A48112 | DMSP SSM/I RETRIEVAL PRODUCTS (REPROCESSED WIND SPEED, TPW)  |
| SYNDAT   | A48113 | SYNTHETIC TROPICAL CYCLONE BOGUS REPORTS                     |
| ERS1DA   | A48114 | ERS SCATTEROMETER WIND DATA (REPROCESSED WIND SPEED)         |
| GOESND   | A48115 | GOES SOUNDING, RETRIEVAL, RADIANCE DATA                      |
| QKSWND   | A48116 | QUIKSCAT SCATTEROMETER WIND DATA (REPROCESSED)               |
| MSONET   | A48117 | MESONET SURFACE REPORTS                                      |
| GPSIPW   | A48118 | GLOBAL POSITIONING SATELLITE-INTEGRATED PRECIPITABLE WATER AND TOTAL ZENITH DELAY REPORTS |
| RASSDA   | A48119 | RADIO ACOUSTIC SOUNDING SYSTEM (RASS) VIRTUAL TEMPERATURE PROFILE REPORTS |
| WDSATR   | A48120 | WINDSAT SCATTEROMETER WIND DATA (REPROCESSED)                |
| ASCATW   | A48121 | ASCAT SCATTEROMETER DATA (REPROCESSED)                       |

### Output file header definitions: [unit]

```
SID = Station ID
XOB = Lon [DegE]
YOB = Lat [DegN]
DHR = Obs time - Cycle time  [Hours]
ELV = Station Elevation [m]
TYP = Report Type [code table]
T29 = Input Report Type [code table]
ITP = Insturment Type [code table]
lev = Observation level
var = Observation variable
OB  = Observation value
QM  = quality marker [code table]
PC  = program code [code table]
RC  = reason code [code table]
FC  = forecast value
AN  = analyzed value
OE  = observation error
CAT = PREPBUFR level category [code table]

Variables for OB, QM, PC, RC, FC, AN, and OE:

P = Pressure [mb]
Q = Specific Humidity [MG/KG]
T = Temp [DEG C]
Z = Height [m]
U = U-wind component [m/s]
V = V-wind component [m/s]
```

### Code Definitions:

-Quality marker (QM) ranks the quality of the observation value. See docs/Quality_mark.txt, or http://www.emc.ncep.noaa.gov/mmb/data_processing/prepbufr.doc/table_7.htm

-Program code (PC) indicates program used for QC processing. See docs/Program_code.txt

-Reason code (RC) is generated by the program used for QC processing. See the files located under docs/Reason_codes for Reason codes associated with each program code. There are currently no reason codes associated with program codes 003, 011, 013, 014.

-Level catagory (cat) identifies the report level. See docs/LevelCat_code.txt

-Report Type (TYP) identifies reporting platforms and specfic variables derived from those reporting platforms. See docs/Report_type.txt

-Input Report Type (T29) identifies reporting platforms. See docs/Input_Report_type.txt

-Instrument Type (ITP) identifies the instrument and the instrument's origin of manufacture. See docs/Instrument_type.txt

### Quality Control Events Stack:

-The first value of a variable in the column is the one used for data assimilation processing (QC'd obs value), if the variable is not rejected.

-The following values of the same variable (e.g. the second Q value to show up in the column) have one less layer of QC processing.

-The final variable value in the column is the original observation.

-There may be several steps of QC, and several corresponding values.

-A quality marker(qm) value > 3 indicates that the observation has been rejected for use in data assimilation processing.

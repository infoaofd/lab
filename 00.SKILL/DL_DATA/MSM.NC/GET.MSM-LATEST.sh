# /work02/DATA/MSM.NC/MSM-LATEST

yyyymmdd1=$1; yyyymmdd2=$2

yyyymmdd1=${yyyymmdd1:-20240710}
yyyymmdd2=${yyyymmdd2:-20240710}

URL=https://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/latest

yyyy1=${yyyymmdd1:0:4}; mm1=${yyyymmdd1:4:2}; dd1=${yyyymmdd1:6:2}
yyyy2=${yyyymmdd2:0:4}; mm2=${yyyymmdd2:4:2}; dd2=${yyyymmdd2:6:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart)

i=0
while [ $i -le $nday ]; do
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${i}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  h=0;he=21;dh=3

  while [ $h -le $he ];do 
    hh=$(printf %02d $h)

    YMD=${yyyy}${mm}${dd}
    wget $URL/$YMD/MSM${YMD}${hh}P.nc
    wget $URL/$YMD/MSM${YMD}${hh}S.nc

    h=$(expr $h + $dh)
  done #h

  i=$(expr $i + 1) #i
done


#!/bin/bash

EXE=$(basename $0 .RUN.sh).sh
#OPT="-t"
OPT=

if [ ! -f $EXE ]; then
echo NO SUCH FILE, $EXE
exit 1
fi

YS=2024; YE=2024
MMS=07; DDS=10
MME=07; DDE=10

Y=$YS

while [ $Y -le $YE ]; do
$EXE ${OPT} ${Y}${MMS}${DDS} ${Y}${MME}${DDE}
Y=$(expr $Y + 1)
done



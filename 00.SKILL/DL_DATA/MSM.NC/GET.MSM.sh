#!/bin/bash

usage(){
  echo
  echo "Usage $0 [-t]"
  echo "-t : test mode (do NOT download files)"
  echo
}

OPT="-nv" # -nc"

export LANG=C

flagt="false"
while getopts t OPT; do
  case $OPT in
    "t" ) flagt="true" ;  ;;
     * ) usage; exit 1
  esac
done

shift $(expr $OPTIND - 1)



if [ $# -lt 2 ];then
  echo "Error : Wrong number of arguments."
  echo "Usage: $0 start end directory"
  echo "start : start day (YYYYMMDD)"
  echo "end   : end day   (YYYYMMDD)"
  echo "directry"
  exit 1
fi



start=$1
end=$2



if [ $# -le 2 ]; then
  homedir=$(pwd)
else
  homedir=$3
fi

if [ ! -d ${homedir} ]; then
  mkdir -p $homedir
#  echo Error in $0 : No such directry, ${homedir}
#  exit 1
fi


URL=http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf


starts=$(date -d "${start}" '+%s')
ends=$(date -d "${end}" '+%s')

times=$(expr $ends - $starts)

days=$(expr $times / 86400 )

sdate=${start:0:4}/${start:4:2}/${start:6:2}
#echo $sdate

yesterday=$(date -d"$sdate"-1days '+%Y/%m/%d')
#echo $yesterday

n=0
while [ $n -le $days ]; do

  today=$(date -d"$yesterday"+1days '+%Y/%m/%d')

  yyyy=${today:0:4}
    mm=${today:5:2}
    dd=${today:8:2}

  echo
  pdir=MSM-P
  sdir=MSM-S
  rdir=MSM-S/r1h
  lpdir=${pdir}/${yyyy}
  lsdir=${sdir}/${yyyy}
  lrdir=${sdir}/r1h/${yyyy}
  mkdir -vp ${lpdir}
  if [ $? -ne 0 ]; then
    echo ERROR in $0 : Cannot create directory, ${lpdir}.
    exit 1
  fi
  mkdir -vp ${lsdir}
  if [ $? -ne 0 ]; then
    echo ERROR in $0 : Cannot create directory, ${lsdir}.
    exit 1
  fi
  mkdir -vp ${lrdir}
  if [ $? -ne 0 ]; then
    echo ERROR in $0 : Cannot create directory, ${lrdir}.
    exit 1
  fi



  echo
  echo Going down to directory, ${lsdir}.
  cd ${lsdir}
  if [ $? -ne 0 ]; then
    echo Error in $0 : Cannot go to directory, ${lsdir}
    exit 1
  fi

  echo
  cwd=$(pwd)
  echo "Current directory, ${cwd}."
  sfile=${URL}/${sdir}/${yyyy}/${mm}${dd}.nc
  echo
  if [ $flagt = "true" ]; then
    echo "Testing: target file = ${sfile}"
  else
    rm -vf ./${mm}${dd}.nc*
    wget $OPT ${sfile}
  fi


  echo
  echo Going up to parent directory.
  cd ${homedir}
  pwd



  echo
  echo Going down to directory, ${lrdir}.
  cd ${lrdir}
  if [ $? -ne 0 ]; then
    echo Error in $0 : Cannot go to directory, ${lrdir}
    exit 1
  fi

  cwd=$(pwd)
  echo "Current directory, ${cwd}."
  rfile=${URL}/${lrdir}/${mm}${dd}.nc
  echo
  if [ $flagt = "true" ]; then
    echo "Testing: target file = ${rfile}"
  else
    rm -vf ./${mm}${dd}.nc* 
    wget $OPT ${rfile}
  fi

  echo
  echo Going up to parent directory.
  cd $homedir
  pwd



  echo
  echo Going down to directory, ${lpdir}.
  cd ${lpdir}
  if [ $? -ne 0 ]; then
    echo Error in $0 : Cannot go to directory, ${lpdir}
    exit 1
  fi

  cwd=$(pwd)
  echo "Current directory, ${cwd}."
  pfile=${URL}/${pdir}/${yyyy}/${mm}${dd}.nc
  echo
  if [ $flagt = "true" ]; then
    echo "Testing: target file = ${pfile}"
  else
    rm -vf ./${mm}${dd}.nc* 
    wget $OPT  ${pfile}
  fi


  n=$(expr $n + 1)
  yesterday=$today

  echo
  echo Going up to parent directory, ${homedir}.
  cd ${homedir}
  pwd
  echo

done

exit 0

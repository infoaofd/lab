# ECCODES ERROR : unable to get descriptor 063000 from table

/work01/DATA/PREPBUFR/EXAMPLE
Wed, 30 Aug 2023 21:42:17 +0900

https://confluence.ecmwf.int/display/UDOC/Local+configuration+-+ecCodes+BUFR+FAQ

Worked Example
Download an example here:

https://confluence.ecmwf.int/download/attachments/76405363/localBufrTable.tar?version=1&modificationDate=1493298450265&api=v2

localBufrTable.tar

This tarball contains a BUFR file which uses local descriptors as well as the definition files needed to decode the tables.

```
$ cd localBufrTable/

/work01/DATA/PREPBUFR/EXAMPLE/localBufrTable
$ ls
ikco_217.local.bufr  mydefs/
```

Try decoding the BUFR file "ikco_217.local.bufr" using `bufr_dump`. This file uses a local table version (localTablesVersionNumber=66).

Decoding should *fail* as ecCodes does not know about these descriptors.

```
$ /usr/local/eccodes/bin/bufr_dump ikco_217.local.bufr
{ "messages" : [ 
ECCODES ERROR   :  unable to get descriptor 001211 from table
.....
```

Run the `codes_info` tool to find the location of the default definitions.

```
$ /usr/local/eccodes/bin/codes_info

ecCodes Version 2.7.3

Default definition files path is used: /usr/local/eccodes/share/eccodes/definitions
Definition files path can be changed setting ECCODES_DEFINITION_PATH environment variable

Default SAMPLES path is used: /usr/local/eccodes/share/eccodes/samples
SAMPLES path can be changed setting ECCODES_SAMPLES_PATH environment variable
```

Now set ECCODES_DEFINITION_PATH to include the provided "mydefs" directory.

E.g.

```
export ECCODES_DEFINITION_PATH=`pwd`/mydefs:`codes_info -d`
```



```
/work01/DATA/PREPBUFR/EXAMPLE/localBufrTable

$ export ECCODES_DEFINITION_PATH=`pwd`/mydefs:`codes_info -d`
$ echo $ECCODES_DEFINITION_PATH
/work01/DATA/PREPBUFR/EXAMPLE/localBufrTable/mydefs:/work09/am/anaconda3/share/eccodes/definitions
```

Now see if ecCodes can decode the BUFR file.

```
$ /usr/local/eccodes/bin/bufr_dump ikco_217.local.bufr &> ikco_217.local.bufr.TXT
```

```
/work01/DATA/PREPBUFR/EXAMPLE/localBufrTable
$ head *.TXT
{ "messages" : [ 
  [

    {
      "key" : "edition",
      "value" : 4
    },
    {
      "key" : "masterTableNumber",
      "value" : 0

$ tail *.TXT
                }
              ]
            ]
          ]
        ]
      ]
    ]
]

]}
```

Search the output of bufr_dump for the overridden unit "MyOwnUnits".
This was added as an example of a centre defining its own parameter attributes

```
$ grep -n MyOwnUnits *.TXT
243:          "units" : "MyOwnUnits"
```


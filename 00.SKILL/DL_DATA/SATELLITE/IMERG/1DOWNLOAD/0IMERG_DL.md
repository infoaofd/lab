https://disc.gsfc.nasa.gov/datasets?keywords=IMERG&page=1

GPM IMERG Late Precipitation L3 Half Hourly 0.1 degree x 0.1 degree V06 (GPM_3IMERGHHL 06)
Online Archive



**~/.netrc**

```
machine urs.earthdata.nasa.gov login YOUR_LOGIN_ID password YOUR_PASSWD
```

```
chmod 0600 .netrc
```



**~/.urs_cookies**



 **~/.dodsrc**

```
HTTP.NETRC=YOUR_HOME >/.netrc
HTTP.COOKIEJAR=YOUR_HOME />.urs_cookies
```



**0JULIAN.sh**

```
Y=$1; M=$2; D=$3

Y=${Y:-2022};M=${M:-06};D=${D:-18}

echo $Y $M $D
date -d$Y/$M/$D +%j
```



**1DL.TEST.sh**

```bash
JULIAN=170

wget --load-cookies ~/.urs_cookies --save-cookies ~/.urs_cookies --keep-session-cookies \
 https://gpm1.gesdisc.eosdis.nasa.gov/data/GPM_L3/GPM_3IMERGHHL.06/\
 2022/${JULIAN}/3B-HHR-L.MS.MRG.3IMERG.20220619-S190000-E192959.1140.V06C.HDF5
```


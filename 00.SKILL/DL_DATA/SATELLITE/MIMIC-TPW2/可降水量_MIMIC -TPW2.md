# 可降水量_MIMIC -TPW2.md 

可降水量 (precipitable water; PW )

2023-04-21_09-11

[[_TOC_]]

## DATA

[MIMIC-TPW ver.2](http://tropic.ssec.wisc.edu/real-time/mtpw2/)

http://tropic.ssec.wisc.edu/real-time/mtpw2/product.php?color_type=tpw_nrl_colors&prod=global2&timespan=24hrs&anim=html5

### Accuracy

[Wimmers and Velden (2010)](http://journals.ametsoc.org/doi/abs/10.1175/2010JAMC2589.1) demonstrated that the morphological compositing process added a mean **average error** of only **1-2 mm TPW** in a multi-satellite composite over the **ocean**, which is usually negligible. We assume that the **error over land is somewhat larger**, but this will have to be investigated sometime later.

 

### Notes on interpreting the product

**Use caution when interpreting the last 3 hours of the product near the coasts**. We have noticed that the **sudden change in TPW from land to water tends to persist over time**, but **when the MIMIC-TPW2 product uses only forward-advected data**, it will tend to show the higher, over-water TPW moving over land and **exaggerating the over-land TPW value**.



Common **artifacts from the MIRS retrieval** include **single-pixel aberrations** ([異常](https://ejje.weblio.jp/content/異常)) (“shot noise”), “**striping**” **along the direction of the satellite track** and general **intercalibration error**. We are working on reducing these.



# DOWNLOAD

### FTP

```bash
$ ftp ftp.ssec.wisc.edu
Connected to ftp.ssec.wisc.edu (144.92.1.6).
```

```bash
ftp> cd pub/mtpw2
250 CWD command successful
ftp> ls
227 Entering Passive Mode (144,92,1,6,254,224).
150 Opening ASCII mode data connection for file list
drwxr-xr-x  42 ftp      ftp          4096 Dec  1  2020 areas
drwxr-xr-x   2 ftp      ftp         53248 Dec 27  2020 areas_fine
drwxr-xr-x  81 ftp      ftp          4096 Apr  1 00:21 data
drwxr-xr-x   2 ftp      ftp       8388608 Apr 20 22:44 data_fine
drwxr-xr-x   3 ftp      ftp            36 Oct 13  2016 images
drwxr-xr-x 2131 ftp      ftp       4567040 Apr 20 23:03 traj_nrt
drwxr-xr-x   7 ftp      ftp           105 Jun 23  2020 traj_nrt_mult
226 Transfer complete
```

```bash
ftp> cd data
ftp> cd 202206
ftp> get comp20220619.000000.nc
local: comp20220619.000000.nc remote: comp20220619.000000.nc
227 Entering Passive Mode (144,92,1,6,214,151).
150 Opening BINARY mode data connection for comp20220619.000000.nc (3734695 bytes)
226 Transfer complete
3734695 bytes received in 2.05 secs (1824.23 Kbytes/sec)
```

```bash
ftp> bye
```



## CHECK

```bash
$ ll comp20220619.000000.nc
-rw-r--r--. 1 am 3.6M 2023-04-21 08:40 comp20220619.000000.nc
```

```bash
netcdf comp20220619.000000 {
dimensions:
        lat = 721 ;
        lon = 1440 ;
variables:
        float lonArr(lon) ;
                lonArr:units = "degrees_east" ;
        float latArr(lat) ;
                latArr:units = "degrees_north" ;
        float tpwGrid(lat, lon) ;
                tpwGrid:least_significant_digit = 1LL ;
                tpwGrid:units = "mm" ;
        float tpwGridPrior(lat, lon) ;
                tpwGridPrior:least_significant_digit = 1LL ;
                tpwGridPrior:units = "mm" ;
```



## PLOT

```ncl
; 
; TEST.MTPW2.ncl
; 
; Fri, 21 Apr 2023 08:48:15 +0900
; /work03/am/2022.MESHIMA.WV/52.12.PW/12.12.MIMIC_PTW2/12.12.TEST.MIMIC_TPW2
; am
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+ " .ncl")

;LOG=script+".LOG"

;NOW=systemfunc("date '+%y%m%d_%H%M' ")
;print("NOW="+NOW)
; LOG=script+NOW+".LOG"
HOST=systemfunc("hostname")
CWD=systemfunc("pwd")

;arg    = getenv("NCL_ARG_2")

IN="comp20220619.000000.nc"

a=addfile(IN,"r")
;print(a)

FIG=systemfunc("basename "+IN+" .nc")
TYP="pdf"

tpw=a->tpwGrid
lon=a->lonArr
lat=a->latArr
lon@units="degrees_east"
lat@units="degrees_north"

tpw!0="lat"
tpw!1="lon"
tpw&lon=lon
tpw&lat=lat
tpw&lat@units="degrees_north"
tpw&lon@units="degrees_east"


printVarSummary(tpw)

res=True
wks = gsn_open_wks(TYP,FIG)

  res@vpWidthF = 0.6
  res@vpHeightF = 0.6
  res@gsnLeftString   = ""               ; add the gsn titles
  res@gsnCenterString = ""
  res@gsnRightString  = ""

  res@gsnDraw         = False        ; plotを描かない
  res@gsnFrame        = False        ; WorkStationを更新しない
  res@cnLinesOn       = False
  res@cnFillOn        = True
  res@cnInfoLabelOn   = False

  res@cnLevelSelectionMode = "ManualLevels"
  res@cnMinLevelValF  = 0 
  res@cnMaxLevelValF  = 70
  res@cnLevelSpacingF = 5 
  res@cnFillPalette   = "CBR_wet"

  res@mpMinLonF       = 100
  res@mpMaxLonF       = 160
  res@mpMinLatF       = -10
  res@mpMaxLatF       =  50
  res@mpCenterLonF = 140  ; 経度の中心

;; res@gsnMaximize        = True 

  res@gsnRightString  = ""
  res@pmLabelBarOrthogonalPosF = 0.1        ; カラーバーの位置を微調整
  ;;; タイトルや軸の文字の大きさを設定
  res@tmYLLabelFontHeightF = 0.016
  res@tmXBLabelFontHeightF = 0.016
  res@tiMainFontHeightF    = 0.024
  res@lbLabelFontHeightF   = 0.016
  ;;; カラーバーにタイトルをつける
  res@lbTitleOn            = True
  res@lbTitleString        = tpw@units
  res@lbTitlePosition      = "Right"
  res@lbTitleDirection     = "Across" 
  res@lbTitleFontHeightF   = 0.016
  res@pmLabelBarHeightF = 0.05
plot = gsn_csm_contour_map_ce(wks,tpw(:,:),res)

draw(plot)
frame(wks)

;FINFO=systemfunc("ls -lh --time-style=long-iso "+script_name)
;LOG_HEADER = (/"# "+ NOW, "# "+ HOST, "# "+ CWD, "# "+ FINFO /)
;hlist=[/LOG_HEADER/]
;write_table(LOG, "w", hlist, "%s")



print("")
print("Done " + script_name)
print("IN: "+IN)
print("FIG:"+FIG+"."+TYP)
print("")
;print("LOG: "+LOG)
;print("")
```

```bash
$ ncl TEST.MTPW2.ncl
script=TEST.MTPW2.ncl

Variable: tpw
Type: float
Total Size: 4152960 bytes
            1038240 values
Number of Dimensions: 2
Dimensions and sizes:   [lat | 721] x [lon | 1440]
Coordinates: 
            lat: [-90..90]
            lon: [-180..179.75]
Number Of Attributes: 2
  least_significant_digit :     1
  units :       mm

Done TEST.MTPW2.ncl
IN: comp20220619.000000.nc
FIG:comp20220619.000000.pdf
```



## Appendix

### Source code for satpy.readers.mimic_TPW2_nc

https://satpy.readthedocs.io/en/stable/_modules/satpy/readers/mimic_TPW2_nc.html
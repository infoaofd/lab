import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.basemap import Basemap
import os

import h5py

filename='GPMTRM_KUR_1502_M_L3S_LHM_07A.h5'

spres,xnum,ynum=0.5, 720, 268
"""
  	phony_dim_1 = 80 ;
  	phony_dim_2 = 720 ;
  	phony_dim_3 = 268 ;
  	float LHCndMean(phony_dim_1, phony_dim_2, phony_dim_3) ;
  		LHCndMean:DimensionNames = "nlayer,nlon,nlat" ;
  		LHCndMean:Units = "K/hr" ;
"""

"""
# https://qiita.com/skotaro/items/873507dc8f8f967bbc03
def PrintOnlyDataset(name, obj):
    if isinstance(obj, h5py.Dataset):
        print(name)
        # print('\t',obj)

with h5py.File(filename,'r') as f:
    f.visititems(PrintOnlyDataset)
"""

with h5py.File(filename,"r") as infile: 
    var=np.array(infile['Grid/LHCndMean']).T #Transpose
# see file specification guide about the directories and parameters
# note the sort of array element is inverted to as in file specification
# in this program
# that is the reason of transpose matrix method ".T"

    var2=var.copy()
    var2[var <= -9999.0] = np.nan
# 
# Set Lat&Lon grid
    Lo=np.array([-180.0+spres/2+i*spres for i in range(0,xnum)])
    La=np.array([-70.0+spres/2+i*spres for i in range(0,ynum)])
    Lon,Lat=np.meshgrid(Lo,La)

#plot with Matplotlib
    im=plt.pcolormesh(Lon,Lat,var2[:,:,40]) #, vmin=0.1)
 # [lon, lat, ch=4:DPRMS, surfacetype=2:all, raintype=2:all]
#set map
    m=Basemap(projection='cyl',
    resolution='c',
    llcrnrlat=-30, urcrnrlat=30, llcrnrlon=-180, urcrnrlon=180)
    m.drawcoastlines(color='black')
    x,y=m(Lon, Lat) #compute map projection
# set colorbar
    cb=m.colorbar(im, "right", size="2.5%")

    FIG=os.path.splitext(os.path.basename(__file__))[0]+".PDF"
    plt.savefig(FIG)
    plt.close()
    print("")
    print("FIG: "+FIG)


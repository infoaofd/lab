import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.basemap import Basemap

# in case of HDF
import h5py

filename='GPMTRM_KUR_1502_M_L3S_LHM_07A.h5'

"""
# https://qiita.com/skotaro/items/873507dc8f8f967bbc03
def PrintOnlyDataset(name, obj):
    if isinstance(obj, h5py.Dataset):
        print(name)
        # print('\t',obj)

with h5py.File(filename,'r') as f:
    f.visititems(PrintOnlyDataset)
"""

with h5py.File(filename,"r") as infile: 
    var=np.array(infile['Grid/LHCndMean'])
    print(var)

# FTP

[[_TOC_]]

## スクリプトを使う

### スクリプトの準備

FTP_CMD.TXTというスクリプトをあらかじめ用意しておく

FTP_CMD.TXT (例)

```bash
open FTPサイトのアドレス
user ユーザー名 パスワード

cd pub/data

prompt

cd 202306
mget *
cd ..

by
```

匿名ログインが可能な場合のuser欄の記入例

```
anonymous メールアドレス
```



### ftpコマンドの実行例

```
ftp -n < FTP_CMD.TXT
```


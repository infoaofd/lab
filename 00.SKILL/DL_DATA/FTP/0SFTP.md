# SFTP

[[_TOC_]]

## スクリプトを使う(バッチ処理)

### スクリプトの準備

BATCH.TXTというスクリプトをあらかじめ用意しておく

BATCH.TXT (例)

```bash
cd pub/data

prompt

cd 202306
mget *
cd ..

bye
```



### sftpコマンドの実行例

```bash
sftp -b BATCH.TXT user@example.com
```


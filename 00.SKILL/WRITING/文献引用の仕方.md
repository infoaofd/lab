# 文献引用の仕方

[[_TOC_]]

## はじめに

- **文献引用には一定のルールがある**

- **守らないとそれだけで掲載拒否される**ことがある

- 引用のルールは学会誌ごとに違うので，論文を投稿する場合には学会誌の投稿規定に従う (instruction for authorsなどの名称の文書が用意されている)

- 学会発表の場合には，学会でルールが指定されていることがあるので，そのルールに従う

  例：

  - https://www.metsoc.jp/default/wp-content/uploads/2017/12/MSJ_Abstract-Template_v4.doc)

- 学内での発表の場合, ルールが決まっていない場合があるが, それでも書くべき項目は決まっているので, **必要項目を書く**。また, 一定の書式を決めて, 各引用文献に関して**書式を揃える**

   

## コンマ・ピリオドの後のスペース

英語では，**コンマ (,), ピリオド (.), カッコの後に空白（スペース）を入れる決まりがある**。原則守らなければならない規則なので注意する

誤りの例  

```
Breves,E. 
```

Eの前にスペースがない  

 **注**:  文献リストの表記法で例外を設けている学会誌もあるがそのようなケースは稀



## ラテン語の省略形の使用

科学論文では下記のラテン語の省略形がよく使われる。

- et al. (et aliaの略で「その他」という意味)  

etの後にスペースを入れる。al.の.の後にもスペースを入れる。

  

## 学内ルール

学内での発表には下記のルールを適用すること。American Geophysical Unionと日本気象学会のルールに準拠している。

カッコ, コンマ, ピリオドは全て**半角文字とする**。  

  

## 引用文献の欄に入れる情報

下の例で考える。

Estapa, M. L., Feen, M. L., and Breves, E. (2019). Direct observations of biological carbon export from profiling floats in the subtropical North Atlantic. *Glob*. *Biogeochem*. *Cycles*, **23**, 282–300. doi:10.1029/2018GB006098.

### 必須の項目

- 著者名

- 論文のタイトル

- 学会誌 (journal)の名前

- 学会誌の巻 (Volume)

- 掲載ページ

  

#### 著者名

上の例では, Estapa, M. L., Feen, M. L., and Breves, E.



講演要旨などでスペースが足りない場合、

Estapa, M. L., Feen, M. L., and Breves, E.  

を

Estapa et al.

のように第一著者のみの名前を書くことがある。et al.はラテン語で、and othersとおおよそ同じ意味。その場合、ラストネームだけで良い場合が多い。



#### 発行年

上の例では, 2019年

#### 論文のタイトル

上の例では, Direct observations of biological carbon export from profiling floats in the subtropical North Atlantic

#### 学会誌 (journal)の名前

上の例では, 

*Glob*. *Biogeochem*. *Cycles*

学会誌の名前は, 斜字体にする場合がある。略称を使う場合が多い。

#### 学会誌の巻 (Volume)

上の例では, 23

巻に関しては太字にする場合がある。

#### 掲載ページ

上の例では, 282–300

**ここまでは必須**



### 指示があれば記載する内容

**これ以降は指示があれば記載する。**

#### DOI (Digital Object Identifier)

上の例では, doi:10.1029/2018GB006098

#### 学会誌の号 (Issue)

1年で1巻分と数える学会誌が多いが, 毎月発行する学会誌は, 巻の後に号 (ISSUE)と呼ばれる番号が付く。号まで記載するような指示がある場合は,記載する。

多くの場合、巻が１００巻で号が5号の場合,

**100** (5)

のようにかっこを付けて記載する。



### 引用文献リスト

#### 例1: 原著論文 (英語)

Estapa, M. L., Feen, M. L., and Breves, E. (2019). Direct observations of biological carbon export from profiling floats in the subtropical North Atlantic. *Glob*. *Biogeochem*. *Cycles*, **23**, 282–300. doi:10.1029/2018GB006098.

#### 例1の解説

**著者名**: Estapa, M. L., Feen, M. L., and Breves, E.

- 名前の書き方は, Family name, イニシャル.とする 

- 名前と名前の間にはコンマを入れる

- 最後から2番目と最後の著者名の間には andを入れる。  

- ピリオド, コンマの後には空白を入れる。    

  

**発行年**: (2019).

発行年を括弧で囲み, 最後にピリオドを付け，空白とスペースを追加する。  

　　

**論文名**: Direct observations of biological carbon export from profiling floats in the subtropical North Atlantic.  

**論文名の最初の単語の頭文字のみ大文字にする** (Direct)。ただし**固有名詞については頭文字を大文字にする**   (North Atlantic)  

学会誌によっては，すべての単語の頭文字を大文字にするルールを採用していることもある。引用文献リストを作成する際に，**書式が不統一となる誤りが発生し易いので注意する**。  



**学会誌名**: *Glob*. *Biogeochem*. *Cycles*  

フォントは斜字体 (italic) を用いる。**各単語の頭文字は大文字**にする。略語 (abbreviation) がある場合はそれを用いる。この例では, Glob. (Global), Biogeochem. (Biogeochemical) という略称が用いられている。略語が無い場合や分からない場合は略さず，正式名称を書く。学会誌名の最後にはコンマとスペースを追加する。

その他よく使われている略語は以下の通り:  

**American Geophysical Union (AGU)が発行している学会誌** 

*J. Geophys. Res.* (Journal of Geophysical Research)

*Geophys. Res. Lett.* (Geophysical Research Letters)

*Glob*. *Biogeochem*. *Cycles* (Global Biogeochemical Cycles)

https://agupubs.onlinelibrary.wiley.com/



**American Meteorological Society (AMS)が発行している学会誌** 

*J. Clim.* (Journal of Climate)

*J.* Atmos. Sci. (Journal of Atmospheric Sciences)

*Mon. Wea. Rev.* (Monthly Weather Review)

*J. Hydrometeor.* (Journal of Hydrometeorology)

*Wea. Forecasting* (Weather and Forecasting)

*J. Phys. Oceanogr.* (Journal of Physical Oceanography)



**日本気象学会が発行している学会誌**

 *J. Meteor. Soc. Japan* (Journal of Meteorological Society of Japan)

*SOLA* (Scientific Online Letters on the Atmosphere)



**日本海洋学会が発行している学会誌**

J. Oceanogr. (Journal of Oceanography)



**European Geophysical Union (EGU)が発行している学会誌**

https://www.egu.eu/publications/open-access-journals/



**Royal Meteorological Society (RMets)が発行している学会誌**

https://rmets.onlinelibrary.wiley.com/



**注意：学会の名称と学会誌の名称を混同しない**

以下は学会の名称であって, **学会誌の名称**ではない。

American Geophysical Union

American Meteorological Society

European Geophysical Union

Royal Meteorological Society



**学会誌の名前**は論文の**ページの上の方**に書いてある

<img src="FIG/image-20230815212226594.png" alt="image-20230815212226594" style="zoom:70%;" />

![image-20230815212300627](FIG/image-20230815212300627.png)

もしくは, **文献リストに書いてある**。

<img src="FIG/image-20230815212436943.png" alt="image-20230815212436943" style="zoom:70%;" />

<img src="FIG/image-20230815212614658.png" alt="image-20230815212614658" style="zoom:70%;" />



**巻**:  **23**  

ほとんどの学会誌は1年ごとに巻 (Volume) を分けている。その場合，巻を表す数字を**太字**で記載する。数字の後にコンマとスペースを追加する。  

  

**掲載ページ**: 282–300  

論文が掲載されているページを記載する。最近**オンラインの学会誌**が増え，**ページ番号がない論文もある**が，その場合以下のようにする。

- 論文固有のdoi (Digital Object Identifier)が付与されている場合，ページ数の代わりに，doiを記載する。

- 学会誌固有の論文番号が付与されている場合，その番号を記載する

最後にピリオドとスペースを追加する。

  

**doi**: doi:10.1029/2018GB006098  

論文固有のdoi (Digital Object Identifier)が付与されている場合，doiを追加する。掲載ページとdoiの間で**改行は不要**。

  

#### 例2: 単行本の引用  

浅井冨雄，武田喬男，木村龍治 (1981). 雲や降水を伴う大気．大気科学講座 2，東京大学出版会，249pp．  

Kraus, E. B. and J. A. Businger (1994). Atmosphere‒Ocean Interaction（2nd ed.). Oxford Univ. Press,
362pp.  

pp.はpagesの略語である。**数字とppの間にはスペースを入れない**。

  

#### 例3:  共同執筆書の一部引用  

木田秀次 (1998). 地球を巡る大気の流れ. 新教養の気象学，日本気象学会編, 朝倉書店, 61‒72.  

Defant, F. (1951). Local winds. Compendium of Meteorology, T. F. Malone, ed., Amer. Meteor. Soc.,
655‒672.  

ed.は編集者の意味 (edited by)



#### 例5: ホームページ  

気象庁 (2019). 気象観測統計の解説. https://www.data.jma.go.jp/obd/stats/data/kaisetu/（2019.10.25
閲覧).  

ホームページの引用は**やむを得ない場合に限る**こと (情報の信頼性が低い上，ページが長期間保存されない場合が多いため)  

ホームページ上の図を自分の講演資料で借用する場合には，引用文献リストには入れずに，図の下に直接 URL を記載する。  
Wikipediaなどの資料でURLが極端に長くなる場合は, https://ja.wikipedia.org/のように大元のアドレスのみ記載する。

ホームページ上の図を自分の学位論文，講演要旨で借用する場合には，引用文献リストには入れずに，図のキャプションに URL を括弧で囲んで記載する。  

  

### 引用の仕方  

#### 引用文献リストに記載された文献  

著者が1人の場合の例  
Estapa (2019)  

著者のFamily name (苗字)と文献の発行年（かっこでくくる）

著者が2人の場合の例  
Estapa and Feen (2019)  

著者が3人以上の場合の例  
Estapa et al. (2019)  

  

### 引用文献リストに記載されていない文献

講演要旨などではスペースの都合上，すべての文献をリストに記載できないことがある。その場合次のようにする。  

例1  

Estapa (Glob. Biogeochem. Cycle, 2019)  

著者名 (学会誌名の略語, 発行年)  

例2  

Estapa (GBC, 2019)  

著者名 (学会誌名の略語, 発行年)  

略語の例

JGR = Journal of Geophysical Research

GRL = Geophysical Research Letters

JC = Journal of Climate

JAS = Journal of Atmospheric Sciences

MWR＝Monthly Weather Review 

 JHM = Journal of Hydrometeorology

JMSJ = Journal of Meteorological Society of Japan

例3

Estapa (2019)  

著者名 (発行年)  

どのくらいスペースに余裕があるかで，上記の3つの例のどれかを用いること。もちろん情報量の多い例1を用いるのが望ましい。  


















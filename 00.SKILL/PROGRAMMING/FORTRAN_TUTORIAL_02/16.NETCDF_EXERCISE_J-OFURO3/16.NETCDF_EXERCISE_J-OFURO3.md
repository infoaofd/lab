# 16.NETCDF_EXERCISE_J-OFURO3

実際に研究に使用されたプログラムを動作確認してみることで，

- これまで学んだことを理解を深める
- 実際の研究活動で行う作業に慣れる

[[_TOC_]]

## 課題

今回の課題で使用する下記の名称のスクリプトとプログラムは**本資料末尾に記載**されている。

- 0TEST_READ_NC.sh
- 0TEST_READ_NC.F90
- MOD_SUB_NETCDF.F90

### 課題1 

末尾に記載したスクリプト (0TEST_READ_NC.sh) を利用して上記Fortranプログラムをコンパイル，実行せよ

### 課題2 

課題1で使用したFortranプログラム (0TEST_READ_NC.F90) に以下の行を追加して，NetCDFファイルからデータを読み込み，別なファイルに書き出せるようにせよ。

```FORTRAN
print '(A)','MMMMM READ INPUT DATA'
RUN="CLM"
INDIR=TRIM(INROOT)//TRIM(RUN)//"_RUN"
INFLE="J-OFURO3_COR3_LHF_"//TRIM(RUN)//"_RUN_DAILY_"//YYYY//'.nc'
IN=TRIM(INDIR)//'/'//TRIM(INFLE)
CALL READ_NC2(TRIM(VNAME), IN, IM,JM,NM, FLX_CLM)
print *

print '(A)','MMMMM WRITE DATA FOR CHECKING'
PREFIX="1TEST_READ_NC_"//TRIM(VNAME)
ODIR="."

OUT=TRIM(PREFIX)//"_CLM_RUN_"//YYYY//'.nc'
CALL WRITE_NC(TRIM(VNAME), OUT, IM, JM, NM, FLX_CLM, timeI, latI, lonI, INDIR, INFLE, ODIR, OFLE)
print '(A,A)','OUTPUT: ',TRIM(OUT)
```

`ncdump -c` コマンドを使って，入力ファイルと出力ファイルの形式を比較せよ。

PRINT文を使って，読み込んだデータの一部を書き出してみよ。

### 課題3

課題2で使用したFortranプログラムに以下の行を追加して，NetCDFファイルからデータを読み込み，別なファイルに書き出せるようにせよ。

```fortran

RUN="RAW"
INDIR=TRIM(INROOT)//TRIM(RUN)//"_RUN"
INFLE="J-OFURO3_COR3_LHF_"//TRIM(RUN)//"_RUN_DAILY_"//YYYY//'.nc'
IN=TRIM(INDIR)//'/'//TRIM(INFLE)
CALL READ_NC2(TRIM(VNAME), IN, IM,JM,NM, FLX_RAW)

print '(A)','MMMMM READ INPUT DATA'
RUN="CLM"
INDIR=TRIM(INROOT)//TRIM(RUN)//"_RUN"
INFLE="J-OFURO3_COR3_LHF_"//TRIM(RUN)//"_RUN_DAILY_"//YYYY//'.nc'
IN=TRIM(INDIR)//'/'//TRIM(INFLE)
CALL READ_NC2(TRIM(VNAME), IN, IM,JM,NM, FLX_CLM)
print *

print '(A)','MMMMM WRITE DATA FOR CHECKING'
PREFIX="2TEST_READ_NC_"//TRIM(VNAME)
ODIR="."

OUT=TRIM(PREFIX)//"_RAW_RUN_"//YYYY//'.nc'
CALL WRITE_NC(TRIM(VNAME), OUT, IM, JM, NM, FLX_RAW, timeI, latI, lonI, INDIR, INFLE, ODIR, OFLE)
print '(A,A)','OUTPUT: ',TRIM(OUT)

OUT=TRIM(PREFIX)//"_CLM_RUN_"//YYYY//'.nc'
CALL WRITE_NC(TRIM(VNAME), OUT, IM, JM, NM, FLX_CLM, timeI, latI, lonI, INDIR, INFLE, ODIR, OFLE)
print '(A,A)','OUTPUT: ',TRIM(OUT)
```

さらに，末尾に記載したスクリプト (2TEST_READ_NC_PLT.sh) を使って，出力データを作図せよ。



### 課題に使用するスクリプトとプログラム

#### 0TEST_READ_NC.F90

```fortran
PROGRAM TEST_READ_NC

USE MOD_SUB_NETCDF

INTEGER Y
CHARACTER RUN*5,YYYY*4
CHARACTER INROOT*300,INDIR*800,INFLE*500,IN*1300
CHARACTER ODIR*500,OFLE*500,OUT*1000

REAL,ALLOCATABLE,DIMENSION(:)::timeI
REAL(8),ALLOCATABLE,DIMENSION(:)::lon,lat

NM=366
ALLOCATE(timeI(NM))

Y=1988;RUN="CLM"

WRITE(YYYY,'(I4.4)')Y
INROOT="/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/LHF/"
INDIR=TRIM(INROOT)//TRIM(RUN)//"_RUN"
INFLE="J-OFURO3_COR3_LHF_"//TRIM(RUN)//"_RUN_DAILY_"//YYYY//'.nc'
IN=TRIM(INDIR)//'/'//TRIM(INFLE)
print *;print '(A,A)','MMMMM INPUT: ',trim(IN)

CALL READ_NC1FLT("time", IN, NM, timeI)
print *;print '(A)','MMMMM time: '
print *,timeI(1),timeI(NM)

END PROGRAM
```

#### MOD_SUB_NETCDF.F90

```FORTRAN
MODULE MOD_SUB_NETCDF

USE netcdf !NetCDFライブラリを使う

CONTAINS

SUBROUTINE CHECK( STATUS )
! エラーメッセージを書き出す
  INTEGER, INTENT (IN) :: STATUS
  IF(STATUS /= NF90_NOERR) THEN 
    PRINT '(A,A)','EEEEE ERROR ',TRIM(NF90_STRERROR(STATUS))
    STOP "ABNORMAL END"
  END IF
END SUBROUTINE CHECK



SUBROUTINE READ_NC1FLT(VNAME, IN, ND, var1d)
! 1次元データの読み込み(単精度実数)
CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::IN
INTEGER,INTENT(IN)::ND
real,INTENT(INOUT)::var1d(ND)
INTEGER ncid, varid, status

!PRINT '(A)',"MMMMM INPUT FILE : "; !PRINT '(A)',TRIM(IN)

CALL CHECK( nf90_open(IN, nf90_nowrite, ncid) )

CALL CHECK( nf90_inq_varid(ncid, TRIM(VNAME), varid) )
CALL CHECK( nf90_get_var(ncid, varid, var1d) )
status = nf90_close(ncid)
print *
END SUBROUTINE READ_NC1FLT



SUBROUTINE READ_NC1DBL(VNAME, IN, ND, var1d)
! 1次元データの読み込み(倍精度実数)
CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::IN
INTEGER,INTENT(IN)::ND
real(8),INTENT(INOUT)::var1d(ND)
INTEGER ncid, varid, status

CALL CHECK( nf90_open(IN, nf90_nowrite, ncid) )

CALL CHECK( nf90_inq_varid(ncid, TRIM(VNAME), varid) )
CALL CHECK( nf90_get_var(ncid, varid, var1d) )

CALL CHECK( nf90_close(ncid) )
print *
END SUBROUTINE READ_NC1DBL



SUBROUTINE READ_NC2(VNAME, IN, IM,JM,NM,var2d)
! 空間2次元+時間1次元データの読み込み(単精度実数)

CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::IN
INTEGER,INTENT(IN)::IM,JM,NM
real,INTENT(INOUT)::var2d(IM,JM,NM)
INTEGER ncid, varid, status

PRINT '(A,A,A)',"MMMMM ",TRIM(VNAME)," INPUT FILE : "
PRINT '(A)',TRIM(IN)

CALL CHECK( nf90_open(IN, nf90_nowrite, ncid) )

CALL CHECK( nf90_inq_varid(ncid, TRIM(VNAME), varid) )
CALL CHECK( nf90_get_var(ncid, varid, var2d) )
CALL CHECK( nf90_close(ncid) )
END SUBROUTINE READ_NC2



SUBROUTINE READ_NC2_CLM(VNAME, IN, IM,JM,NM,var2d)
! 空間2次元+時間1次元データの読み込み(平年値)

CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::IN
INTEGER,INTENT(IN)::IM,JM,NM
real,INTENT(INOUT)::var2d(IM,JM,NM)
INTEGER ncid, varid, status

PRINT '(A,A,A)',"MMMMM ",TRIM(VNAME)," INPUT FILE : "
PRINT '(A)',TRIM(IN)

CALL CHECK( nf90_open(IN, nf90_nowrite, ncid) )

CALL CHECK( nf90_inq_varid(ncid, "year_day", varid) )
CALL CHECK( nf90_get_var(ncid, varid, year_day) )

CALL CHECK( nf90_inq_varid(ncid, "longitude", varid) )
CALL CHECK(  nf90_get_var(ncid, varid, longitude) )

CALL CHECK( nf90_inq_varid(ncid, "latitude", varid) )
CALL CHECK( nf90_get_var(ncid, varid, latitude) )

CALL CHECK( nf90_inq_varid(ncid, TRIM(VNAME), varid) )
CALL CHECK( nf90_get_var(ncid, varid, var2d) )

CALL CHECK( nf90_close(ncid) )
print *
END SUBROUTINE READ_NC2_CLM



SUBROUTINE WRITE_NC(VNAME, OUT, IM, JM, NM, var2d, time, lat, lon, &
INDIR, INFLE, ODIR, OFLE)
! NETCDFファイルへの書き出し

CHARACTER(LEN=*),INTENT(IN)::VNAME
CHARACTER(LEN=*),INTENT(IN)::OUT, INDIR, INFLE, ODIR, OFLE
INTEGER,INTENT(IN)::IM,JM,NM
real,INTENT(IN)::var2d(IM,JM,NM)
real,INTENT(IN)::time(NM)
real(8),INTENT(IN)::lat(JM),lon(IM)
INTEGER ncido, varido, status

CALL CHECK( nf90_create( trim(OUT), NF90_HDF5, ncido) )
CALL CHECK( nf90_def_dim(ncido, 'time', NM, id_time_dim) )
CALL CHECK( nf90_def_dim(ncido, 'lat',  JM, id_lat_dim) )
CALL CHECK( nf90_def_dim(ncido, 'lon',  IM, id_lon_dim) )

CALL CHECK( nf90_def_var(ncido, 'time', NF90_FLOAT, id_time_dim, id_time) )
CALL CHECK( nf90_def_var(ncido, 'lat', NF90_DOUBLE, id_lat_dim, id_lat) )
CALL CHECK( nf90_def_var(ncido, 'lon', NF90_DOUBLE, id_lon_dim, id_lon) )

CALL CHECK( nf90_put_att(ncido, id_time, 'units','hours since 1800-01-01 00:00') )
CALL CHECK( nf90_put_att(ncido, id_lat, 'units','degrees_north') )
CALL CHECK( nf90_put_att(ncido, id_lon, 'units','degrees_east') )

CALL CHECK( NF90_DEF_VAR(NCIDO, TRIM(VNAME), NF90_REAL, &
     (/ID_LON_DIM, ID_LAT_DIM, ID_TIME_DIM/), ID_VARO))

CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'units','W/m2') )
CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'_FillValue', -9999.) )
CALL CHECK( NF90_PUT_ATT(NCIDO, ID_VARO,'missing_value', -9999.) )

CALL CHECK( NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_dir", LEN(TRIM(INDIR)),TRIM(INDIR)) )
CALL CHECK( NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"input_file", LEN(TRIM(INFLE)),TRIM(INFLE)) )
CALL CHECK( NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"output_dir", LEN(TRIM(ODIR)),TRIM(ODIR)) )
CALL CHECK( NF_PUT_ATT_TEXT(NCIDO,NF_GLOBAL,"output_file", LEN(TRIM(OFLE)),TRIM(OFLE)) )

CALL CHECK( nf90_enddef(ncido) )

CALL CHECK( NF90_PUT_VAR(NCIDO, ID_TIME, time ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_LAT, lat ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_LON, lon ) )
CALL CHECK( NF90_PUT_VAR(NCIDO, ID_VARO, var2d ) )

CALL CHECK(NF90_CLOSE(NCIDO))

END SUBROUTINE WRITE_NC

END MODULE MOD_SUB_NETCDF
```

#### 0TEST_READ_NC.sh

```bash
#!/bin/bash

src=$(basename $0 .sh).F90
SUB="MOD_SUB_NETCDF.F90"
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml

#f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
#OPT=" -fpp -convert big_endian -assume byterecl"

f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
OPT2=" -I/usr/local/netcdf-c-4.8.0/include -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -lnetcdf"

# OpenMP
#OPT2=" -fopenmp "

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${SUB} ${src} -o ${exe}
if [ $? -ne 0 ]; then

echo
echo "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
echo
echo "   COMPILE ERROR!!!"
echo
echo "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo

echo
echo ${exe} is running ...
echo
D1=$(date -R)
${exe}
# ${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
echo
echo "   ERROR in $exe: RUNTIME ERROR!!!"
echo
echo "EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE"
echo
echo TERMINATED.
echo
D2=$(date -R)

exit 1
fi
echo
echo "Done ${exe}"
echo
rm -vf $exe
D2=$(date -R)
```

#### 2TEST_READ_NC_PLT.sh

```bash
#!/bin/bash

YYYYMMDDHH=1988081000
YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}
TEXT="J-OFURO3 V1.2_PRE LHF $DD $MMM $YYYY"

INDIR=.
INFLE1=2TEST_READ_NC_LHF_RAW_RUN_${YYYY}.nc
IN1=$(pwd)/${INDIR}/${INFLE1}
if [ ! -f $IN1 ];then echo NO SUCH FILE,$IN1;exit 1;fi

INFLE2=2TEST_READ_NC_LHF_CLM_RUN_${YYYY}.nc
IN2=$(pwd)/${INDIR}/${INFLE2}
if [ ! -f $IN2 ];then echo NO SUCH FILE,$IN2;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${YYYYMMDDHH}.PDF

# LONW= ;LONE= ; LATS= ;LATN=
# LEV=

LEVS="-80 80 10"
# LEVS=" -levs -3 -2 -1 0 1 2 3"
KIND='midnightblue->blue->deepskyblue->lightcyan->yellow->orange->red->crimson'
FS=2
UNIT=W/m\`a2\`n

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'sdfopen ${IN1}';'sdfopen ${IN2}'

xmax = 1; ymax = 2;nmax=2


'set vpage 0.0 8.5 0.0 11'

ytop=9
xwid = 5.0/xmax; ywid = 6.0/ymax
xmargin=0.5; ymargin=0.5

'cc'

nmap = 1;ymap = 1
while (ymap <= ymax)
xmap = 1
while (xmap <= xmax)

xs = 1 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
'color ${LEVS} -kind ${KIND} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'
'set time ${TIME}'
'set xlopts 1 3 0.07';'set ylopts 1 3 0.07';
'd LHF.'nmap

# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

if(nmap=1);title='RAW';endif
if(nmap=2);title='CLM';endif

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' 'title

# LEGEND COLOR BAR
x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=(x1+x2)/2; y=y2+0.2
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${UNIT}'

xmap=xmap+1; nmap=nmap+1
if(nmap >= nmax);break;endif
endwhile ;#x
ymap=ymap+1
endwhile ;#y

# LEGEND COLOR BAR
x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
x=x2+0.1; y=y1-0.12
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT}'

# TEXT
x=4; y=10
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=10; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
# rm -vf $GS

echo
if [ -f $FIG ]; then echo "FIG: $FIG"; fi
```

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

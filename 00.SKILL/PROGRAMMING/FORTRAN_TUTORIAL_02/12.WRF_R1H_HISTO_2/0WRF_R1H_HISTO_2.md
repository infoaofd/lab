# WRF 雨量のヒストグラム 2

WRFの計算結果のバイナリファイルから，雨量のデータを読み込んで，**雨量の度数分布**を作図するためのファイルを作成する。

ここでは，長いプログラムを作成する手順を学ぶ。簡単なところから，**少しづつプログラムを書き足していく**。一つ一つの手順を理解しながら進めていくこと。

このような手法でプログラムを作成することは頻繁にあるので，自分でプログラムを作る際の参考にすること。

[[_TOC_]]

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。

## viエディタの使用法

今回の課題において，コピー，ペースト機能を使うと作業が速くなるので，操作法を再確認しておく。

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/02.VIM/01.VIM_TUTORIAL.md

### 移動　(ノーマルモード)

> $	行の末尾へ
>
> 0	行の先頭へ
>
> ctrl + u	半画面分 上へ
>
> ctrl + d	半画面分 下へ
>
> gg	そのファイルの先頭へ
>
>  G	そのファイルの末尾へ

### コピーとペースト　(ノーマルモード)

> yy	カーソル行をコピー
>
> y0　行頭からカーソルの直前まで のコピー
>
> y$	カーソルの位置から行末まで のコピー
>
> p	ヤンクした文字列をカーソル位置後にペースト

### 複数行選択　(ノーマルモード)

> V (shift + v)　→　領域選択
>
> ブロック選択
> CTL + V　→　領域選択
>
> yで選択個所をヤンク (コピー)
>
> dで選択個所を削除

### 検索　(ノーマルモード)

> /	後ろにむかって検索
>
>  ?	前にむかって検索
>
> noh ハイライト表示解除

#### 置換　(ノーマルモード)

> 一括置換(確認)
>  :%s/検索文字列/置換文字列/gc
>
>  一括置換(確認しない)
>  :%s/検索文字列/置換文字列/g

## CTLファイル

#### RW3A.00.03.05.05.0702.01.d01.basic_p.01HR.ctl

```
/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0702.01/RW3A.00.03.05.05.0702.01.d01.basic_p.01HR.ctl
dset ^RW3A.00.03.05.05.0702.01.d01.basic_p.01HR_%y4-%m2-%d2_%h2:%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   30 levels  
1000.00000
....
 100.00000
tdef   73 linear 00Z12AUG2021      60MN      
VARS   30
U             30  0  x-wind component (m s-1)
....
RAINRNC        1  0  RAIN RATE NON-CONV (mm per output interval)
....
dbz           30  0  Reflectivity (-)
ENDVARS
```



## Fortranプログラムの作成

以下，少しづつプログラムに機能を加えていく。

書かれている順序に従って，**動作を確かめながら順番にプログラムを加筆していくこと**。

ステップ1から8に関しては下記を参照のこと

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_TUTORIAL_02/11.WRF_R1H_HISTO_1/0WRF_R1H_HISTO_1.md

### ステップ9: 配列の要素同士の積のテスト

指定された領域内だけのデータを使うのに，配列の要素同士の積を用いる。まず，配列の要素同士の積について練習しておく。

#### TEST_ARRAY_MUL.F90

```fortran
real,dimension(3)::a,b,c

data a/1,2,3/
data b/0.1,0.2,0.3/

print *,'a ',(a(i),i=1,3)
print *,'b ',(b(i),i=1,3)

c=a*b ! 配列の要素同士の掛け算になる

print *,'c=a*b'
print *,'c ',(c(i),i=1,3)

end
```

> $ ifort TEST_ARRAY_MUL.F90 -o TEST_ARRAY_MUL.EXE
>
> $ TEST_ARRAY_MUL.EXE 
>  a    1.000000       2.000000       3.000000    
>  b   0.1000000      0.2000000      0.3000000    
>  c=a*b
>  c   0.1000000      0.4000000      0.9000000 



### ステップ10: 不要な配列要素のマスク

配列の要素同士の積を用いて，不要な箇所をマスクする。具体的には，指定した緯度，経度の範囲内のデータのみを後の計算に使用するように設定する。

```
IF(XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
.and. XLAT(I,J)<LATN)THEN
MASK(I,J)=1.0
GRIDNUM=GRIDNUM+1.0
END IF !LON LAT
```

指定した範囲内 (`XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
.and. XLAT(I,J)<LATN`) のデータであれば，`MASK(I,J)`　とする。

有効な格子の数 (`GRIDNUM`) を加算する。

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
CHARACTER(LEN=400):: INFLE, INFLE2
CHARACTER(LEN=600):: IN, OUT, IN2
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,rlon,rlat,dummy2
REAL,DIMENSION(:,:),ALLOCATABLE::XLAT,XLON,MASK

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

REAL::LONW,LONE,LATS,LATN

LONW=129;LONE=132;LATS=31;LATN=34

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),rlon(IM,JM),rlat(IM,JM),dummy2(IM,JM))
ALLOCATE(XLAT(IM,JM),XLON(IM,JM),MASK(IM,JM))

INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
IN=TRIM(INDIR2) // '/' // TRIM(INFLE2)

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)
irec=0
write(*,'(A)') 'READ XLAT '
irec=irec+1
read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)

write(*,'(A)') 'READ XLON '
irec=irec+1
read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)

MASK=0.0; GRIDNUM=0.0

DO J=1,JM
DO I=1,IM

IF(XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
.and. XLAT(I,J)<LATN)THEN
MASK(I,J)=1.0
GRIDNUM=GRIDNUM+1.0
END IF !LON LAT

END DO !I
END DO !J
PRINT *,'GRIDNUM=',GRIDNUM

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)

write(*,'(I5,5I3)',advance='yes'),&
year,month,day,hour,min

irec=0

write(*,'(A)',advance='no') 'READ '

write(*,'(A)',advance='no') 'U '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'V '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'W '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'Q2 '
irec=irec+1

write(*,'(A)',advance='no') 'T2 '
irec=irec+1

write(*,'(A)',advance='no') 'U10 '
irec=irec+1

write(*,'(A)',advance='no') 'V10 '
irec=irec+1

write(*,'(A)',advance='no') 'QVAPOR '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'QCLOUD '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'QRAIN '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'HGT '
irec=irec+1

write(*,'(A)',advance='no') 'RAINC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINRC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINNC '
irec=irec+1

write(*,'(A)',advance='no') 'R1=RAINRNC '
irec=irec+1
read (11,rec=irec) ((R1(i,j),i=1,IM),j=1,JM)

write(*,'(A)',advance="yes")''
CLOSE(11)

R1AVE=sum(R1*MASK)/GRIDNUM
PRINT *,'R1AVE',R1AVE; PRINT *

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh
>
> .....
>
>  2021  8 15  0  0
> READ U V W Q2 T2 U10 V10 QVAPOR QCLOUD QRAIN HGT RAINC RAINRC RAINNC R1=RAINRNC 
>  R1AVE  5.1457845E-02



### ステップ11: 度数分布用の階級の作成

#### R1H_HISTO_1.F90

```bash
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
CHARACTER(LEN=400):: INFLE, INFLE2
CHARACTER(LEN=600):: IN, OUT, IN2
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,rlon,rlat,dummy2
REAL,DIMENSION(:,:),ALLOCATABLE::XLAT,XLON,MASK

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

REAL::LONW,LONE,LATS,LATN

REAL,DIMENSION(:),ALLOCATABLE::RBIN_L, RBIN_R, RFRQ, RPCT
REAL,PARAMETER::R1MIN=1.0,R1LEFT=20.0;R1MAX=300.0; DR1=20.0


namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

LONW=129;LONE=132;LATS=31;LATN=34

! 度数分布の階級の作成
MB=INT((R1MAX-R1LEFT)/DR1)
ALLOCATE(RBIN_L(MB), RBIN_R(MB), RFRQ(MB), RPCT(MB))
RBIN_L(1)=R1MIN; RBIN_R(1)=R1LEFT
DO M=2,MB
RBIN_L(M)=R1LEFT+DR1*FLOAT(M-1)
RBIN_R(M)=R1LEFT+DR1*FLOAT(M)
END DO !M
DO M=1,MB
PRINT *,'RBIN_L(M),RBIN_R(M)',RBIN_L(M),RBIN_R(M)
END DO !M

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...
>
>  RBIN_L(M),RBIN_R(M)   1.000000       20.00000
>
>  RBIN_L(M),RBIN_R(M)   40.00000       60.00000    
>
> .....
>
>  RBIN_L(M),RBIN_R(M)   280.0000       300.0000 



### ステップ12: 度数分布の作成

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
CHARACTER(LEN=400):: INFLE, INFLE2
CHARACTER(LEN=600):: IN, OUT, IN2
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,rlon,rlat,dummy2
REAL,DIMENSION(:,:),ALLOCATABLE::XLAT,XLON,MASK

REAL*8 julian_day
REAL RTOTAL
INTEGER NTOTAL

INTEGER year,month,day,hour,min,sec !FOR TEST

REAL::LONW,LONE,LATS,LATN

REAL,DIMENSION(:),ALLOCATABLE::RBIN_L, RBIN_R, NFRQ, RFRQ, RPCT
REAL,PARAMETER::R1MIN=1.0,R1LEFT=5.0;R1MAX=120.0; DR1=5.0


namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

LONW=129;LONE=132;LATS=31;LATN=34

MB=INT((R1MAX-R1LEFT)/DR1)
ALLOCATE(RBIN_L(MB), RBIN_R(MB), NFRQ(MB), RFRQ(MB), RPCT(MB))

RTOTAL=0.0; NTOTAL=0
NFRQ=0.0; RFRQ=0.0; RPCT(MB)=0.0

RBIN_L(1)=R1MIN; RBIN_R(1)=R1LEFT
DO M=2,MB
RBIN_L(M)=R1LEFT+DR1*FLOAT(M-1)
RBIN_R(M)=R1LEFT+DR1*FLOAT(M)
END DO !M



READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),rlon(IM,JM),rlat(IM,JM),dummy2(IM,JM))
ALLOCATE(XLAT(IM,JM),XLON(IM,JM),MASK(IM,JM))

INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
IN=TRIM(INDIR2) // '/' // TRIM(INFLE2)

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)
irec=0
write(*,'(A)') 'READ XLAT '
irec=irec+1
read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)

write(*,'(A)') 'READ XLON '
irec=irec+1
read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)

MASK=0.0; GRIDNUM=0.0

DO J=1,JM
DO I=1,IM

IF(XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
.and. XLAT(I,J)<LATN)THEN
MASK(I,J)=1.0
GRIDNUM=GRIDNUM+1.0
END IF !LON LAT

END DO !I
END DO !J
PRINT *,'GRIDNUM=',GRIDNUM

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)

write(*,'(I5,5I3)',advance='yes'),&
year,month,day,hour,min

irec=0

write(*,'(A)',advance='no') 'SKIP '

write(*,'(A)',advance='no') 'U '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'V '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'W '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'Q2 '
irec=irec+1

write(*,'(A)',advance='no') 'T2 '
irec=irec+1

write(*,'(A)',advance='no') 'U10 '
irec=irec+1

write(*,'(A)',advance='no') 'V10 '
irec=irec+1

write(*,'(A)',advance='no') 'QVAPOR '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'QCLOUD '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'QRAIN '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'HGT '
irec=irec+1

write(*,'(A)',advance='no') 'RAINC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINRC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINNC '
irec=irec+1

write(*,'(A)',advance='no') 'R1=RAINRNC '
irec=irec+1
read (11,rec=irec) ((R1(i,j),i=1,IM),j=1,JM)

write(*,'(A)',advance="yes")''
CLOSE(11)

DO J=1,JM
DO I=1,IM

IF( MASK(I,J)==1.0)THEN
RTOTAL=RTOTAL+R1(I,J)
NTOTAL=NTOTAL+1
DO M=1,MB
IF(R1(I,J)>=RBIN_L(M) .and. R1(I,J)<RBIN_R(M))THEN
NFRQ(M)=NFRQ(M)+1.0
END IF !R1
END DO !M
END IF !MASK

END DO !I
END DO !J

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

DO M=1,MB
RFRQ(M)=NFRQ(M)/FLOAT(NTOTAL)
END DO !M

PRINT '(A)','RBIN_L, RBIN_R, RFRQ, NFRQ'
DO M=1,MB
PRINT '(2F9.2,E12.4,F12.0)',RBIN_L(M),RBIN_R(M),RFRQ(M),NFRQ(M)
END DO !M

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO.sh 
>
> RBIN_L, RBIN_R, RFRQ, NFRQ
>      1.00     5.00  0.2460E+00     187080.
>     10.00    15.00  0.4737E-01      36029.
> .....
>    115.00   120.00  0.0000E+00          0.



### ステップ13: 完成品

結果をテキストファイルに書き出す

### R1H_HISTO.sh

**OUT: 出力ファイル名 を追加**

```bash
#!/bin/bash

src=$(basename $0 .sh).F90; exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml
SUB="DATE_CAL.F90"

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OpenMP
#OPT2=" -fopenmp "

RUNNAME=RW3A.00.03.05.05.0000.01
#RUNNAME=RW3A.00.03.05.05.0702.01
DOMAIN=d01

cat<<EOF>$nml
&para
INDIR="/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUNNAME}/",
PREFIX="${RUNNAME}.${DOMAIN}.basic_p.01HR_"
OUT="${RUNNAME}.${DOMAIN}.01HR_HISTO.TXT"
IM=599,
JM=599,
KM=30,
NM=73,
YR0=2021, !00Z12AUG2021
MO0=8,
DY0=12,
HR0=0,
MI0=0,
DH=1,
&end
EOF

echo; echo Created ${nml}.; echo
ls -lh --time-style=long-iso ${nml}; echo


echo
echo ${src}.
echo
ls -lh --time-style=long-iso ${src}
echo

echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} ${SUB} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} ${SUB} -o ${exe}
if [ $? -ne 0 ]; then

echo; echo "=============================================="; echo
echo "   COMPILE ERROR!!!"
echo; echo "=============================================="; echo
echo TERMINATED.; echo
exit 1
fi
echo "Done Compile."
echo; ls -lh ${exe}; echo

echo; echo ${exe} is running ...; echo
D1=$(date -R)
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo; echo "=============================================="; echo
echo "   RUNTIME ERROR!!!"
echo; echo "=============================================="; echo
echo TERMINATED.; echo
D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"
exit 1
fi
echo; echo "Done ${exe}"; echo

D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"
```

#### R1H_HISTO.F90

```fortran
PROGRAM R1H_HISTO

! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
CHARACTER(LEN=400):: INFLE, INFLE2
CHARACTER(LEN=600):: IN, OUT, IN2
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,rlon,rlat,dummy2
REAL,DIMENSION(:,:),ALLOCATABLE::XLAT,XLON,MASK

REAL*8 julian_day
REAL RTOTAL
INTEGER NTOTAL

INTEGER year,month,day,hour,min,sec !FOR TEST

REAL::LONW,LONE,LATS,LATN

REAL,DIMENSION(:),ALLOCATABLE::RBIN_L, RBIN_R, NFRQ, RFRQ, RPCT
REAL,PARAMETER::R1MIN=1.0,R1LEFT=5.0;R1MAX=120.0; DR1=5.0


namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

LONW=129;LONE=132;LATS=31;LATN=34

MB=INT((R1MAX-R1LEFT)/DR1)
ALLOCATE(RBIN_L(MB), RBIN_R(MB), NFRQ(MB), RFRQ(MB), RPCT(MB))

RTOTAL=0.0; NTOTAL=0
NFRQ=0.0; RFRQ=0.0; RPCT(MB)=0.0

RBIN_L(1)=R1MIN; RBIN_R(1)=R1LEFT
DO M=2,MB
RBIN_L(M)=R1LEFT+DR1*FLOAT(M-1)
RBIN_R(M)=R1LEFT+DR1*FLOAT(M)
END DO !M



READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),rlon(IM,JM),rlat(IM,JM),dummy2(IM,JM))
ALLOCATE(XLAT(IM,JM),XLON(IM,JM),MASK(IM,JM))

INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
IN=TRIM(INDIR2) // '/' // TRIM(INFLE2)

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)
irec=0
write(*,'(A)') 'READ XLAT '
irec=irec+1
read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)

write(*,'(A)') 'READ XLON '
irec=irec+1
read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)

MASK=0.0; GRIDNUM=0.0

DO J=1,JM
DO I=1,IM

IF(XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
.and. XLAT(I,J)<LATN)THEN
MASK(I,J)=1.0
GRIDNUM=GRIDNUM+1.0
END IF !LON LAT

END DO !I
END DO !J
PRINT *,'GRIDNUM=',GRIDNUM

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)

write(*,'(I5,5I3)',advance='yes'),&
year,month,day,hour,min

irec=0

write(*,'(A)',advance='no') 'SKIP '

write(*,'(A)',advance='no') 'U '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'V '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'W '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'Q2 '
irec=irec+1

write(*,'(A)',advance='no') 'T2 '
irec=irec+1

write(*,'(A)',advance='no') 'U10 '
irec=irec+1

write(*,'(A)',advance='no') 'V10 '
irec=irec+1

write(*,'(A)',advance='no') 'QVAPOR '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'QCLOUD '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'QRAIN '
DO K = 1, KM; irec=irec+1; END DO !K

write(*,'(A)',advance='no') 'HGT '
irec=irec+1

write(*,'(A)',advance='no') 'RAINC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINRC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINNC '
irec=irec+1

write(*,'(A)',advance='no') 'R1=RAINRNC '
irec=irec+1
read (11,rec=irec) ((R1(i,j),i=1,IM),j=1,JM)

write(*,'(A)',advance="yes")''
CLOSE(11)

DO J=1,JM
DO I=1,IM

IF( MASK(I,J)==1.0)THEN
RTOTAL=RTOTAL+R1(I,J)
NTOTAL=NTOTAL+1
DO M=1,MB
IF(R1(I,J)>=RBIN_L(M) .and. R1(I,J)<RBIN_R(M))THEN
NFRQ(M)=NFRQ(M)+1.0
END IF !R1
END DO !M
END IF !MASK

END DO !I
END DO !J

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

DO M=1,MB
RFRQ(M)=NFRQ(M)/FLOAT(NTOTAL)
END DO !M

PRINT *; PRINT '(A,A)','OUTPUT: ',TRIM(OUT)
OPEN(21,FILE=TRIM(OUT))

WRITE(21, '(A)') ,'# RBIN, RFRQ, NFRQ'
DO M=1,MB
RBIN=(RBIN_L(M)+RBIN_R(M))/2.0
WRITE(21,'(F9.2,E12.4,F12.0)'),RBIN,RFRQ(M),NFRQ(M)
END DO !M

END PROGRAM R1H_HISTO
```

>  $ R1H_HISTO.sh 
>
> OUTPUT: RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT

### ステップ14: 結果の確認

#### 計算結果のテキストファイル表示

```bash
$ cat RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT
```

```bash
# RBIN, RFRQ, NFRQ
     3.00  0.2460E+00     187080.
    12.50  0.4737E-01      36029.
    17.50  0.2427E-01      18462.
    22.50  0.1289E-01       9807.
    27.50  0.7075E-02       5381.
    32.50  0.3896E-02       2963.
    37.50  0.2133E-02       1622.
    42.50  0.1179E-02        897.
    47.50  0.6718E-03        511.
    52.50  0.3747E-03        285.
    57.50  0.2275E-03        173.
    62.50  0.1315E-03        100.
    67.50  0.7626E-04         58.
    72.50  0.3681E-04         28.
    77.50  0.1972E-04         15.
    82.50  0.1315E-04         10.
    87.50  0.2630E-05          2.
    92.50  0.2630E-05          2.
    97.50  0.1315E-05          1.
   102.50  0.1315E-05          1.
   107.50  0.0000E+00          0.
   112.50  0.0000E+00          0.
   117.50  0.0000E+00          0.
```

#### gnuplotによるクイックルック

##### gnuplotの起動

> $ gnuplot 

##### 出力ファイルの形式をPNGにする

>  gnuplot> set term png

##### 出力ファイル名の指定

> gnuplot> set output "R1H_HISTO_CNTL.png"  

##### グラフを書く

>  gnuplot> plot 'RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT' using 1:2 with lines

##### 縦軸を対数にする

>  gnuplot> clear

>  gnuplot> set output "R1H_HISTO_CNTL.png"

>  gnuplot> set logscale y

>  p 'RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT' u 1:2 w l ti "CNTL" lt rgbcolor "orange" lw 5

<img src="R1H_HISTO_CNTL.png" alt="R1H_HISTO_CNTL" style="zoom: 33%;" />

##### スクリプトの保存

> gnuplot> save "HISTO.gnu"

##### 終了

>  gnuplot> quit

## 課題

1. プログラムの動作を確認しながら，上記ステップ1から14までを順番に行う。プログラムは自分で打ち込むこと。

2. 疑問点がある場合，下記の手順で質問事項をまとめておく

   事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

   - 資料のどの箇所に関する質問か
   - エラーメッセージのコピー
   - エラーメッセージをgoogle検索した結果で有益と思われるもの
   - AIの回答結果のうち有益と思われるもの

   打ち合わせ時に上記に対して追加説明を行う。

3. ステップ14のプログラムを下記以外の箇所を参照せずに自分で作成する。

   ```FORTRAN
   PROGRAM R1H_HISTO
   
   ! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO
   
   CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
   CHARACTER(LEN=400):: INFLE, INFLE2
   CHARACTER(LEN=600):: IN, OUT, IN2
   INTEGER IM,JM,KM,NM
   INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH
   
   REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
   REAL,DIMENSION(:,:),ALLOCATABLE::R1,rlon,rlat,dummy2
   REAL,DIMENSION(:,:),ALLOCATABLE::XLAT,XLON,MASK
   
   REAL*8 julian_day
   REAL RTOTAL
   INTEGER NTOTAL
   
   INTEGER year,month,day,hour,min,sec !FOR TEST
   
   REAL::LONW,LONE,LATS,LATN
   
   REAL,DIMENSION(:),ALLOCATABLE::RBIN_L, RBIN_R, NFRQ, RFRQ, RPCT
   REAL,PARAMETER::R1MIN=1.0,R1LEFT=5.0;R1MAX=120.0; DR1=5.0
   
   
   namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
   SS0=0
   
   LONW=129;LONE=132;LATS=31;LATN=34
   
   MB=!.....
   ALLOCATE(RBIN_L(MB), RBIN_R(MB), NFRQ(MB), RFRQ(MB), RPCT(MB))
   
   RTOTAL=0.0; NTOTAL=0
   NFRQ=0.0; RFRQ=0.0; RPCT(MB)=0.0
   
   RBIN_L(1)=R1MIN; RBIN_R(1)=R1LEFT
   DO M=2,MB
   !.....
   RBIN_R(M)=R1LEFT+DR1*FLOAT(M)
   END DO !M
   
   
   
   READ(*,nml=!.....)
   
   ALLOCATE(dummy3(IM,JM,KM))
   ALLOCATE(R1(IM,JM),rlon(IM,JM),rlat(IM,JM),dummy2(IM,JM))
   ALLOCATE(XLAT(IM,JM),XLON(IM,JM),!.....)
   
   INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
   INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
   IN=TRIM(INDIR2) !..... TRIM(INFLE2)
   
   OPEN(11,FILE=TRIM(IN),ACTION="READ",&
   form='unformatted',access='direct',recl=!.....)
   irec=0
   write(*,'(A)') 'READ XLAT '
   irec=irec+1
   read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)
   
   write(*,'(A)') 'READ XLON '
   irec=irec+1
   read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)
   
   MASK=0.0; GRIDNUM=0.0
   
   DO J=1,JM
   DO I=1,IM
   
   IF(XLON(I,J)>=LONW .AND. XLON(I,J)<LONE .and. XLAT(I,J)>=LATS &
   .and. XLAT(I,J)<LATN)THEN
   !.....
   !.....
   END IF !LON LAT
   
   END DO !I
   END DO !J
   PRINT *,'GRIDNUM=',GRIDNUM
   
   call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)
   
   DO N=1,NM
   
   call jd2date(year,month,day,hour,min,sec,julian_day)
   
   IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
   IE=!.....
   
   IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
   !.....
   !.....
   !.....
   !.....
   IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'
   
   OPEN(11,FILE=TRIM(IN),ACTION="READ",&
   form='unformatted',access='direct',recl=!.....)
   
   write(*,'(I5,5I3)',advance='yes'),&
   year,month,day,hour,min
   
   irec=0
   
   write(*,'(A)',advance='no') 'SKIP '
   
   write(*,'(A)',advance='no') 'U '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'V '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'W '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'Q2 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'T2 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'U10 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'V10 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'QVAPOR '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'QCLOUD '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'QRAIN '
   DO K = 1, KM; irec=irec+1; END DO !K
   
   write(*,'(A)',advance='no') 'HGT '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINC '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINRC '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINNC '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'R1=RAINRNC '
   irec=irec+1
   read (11,rec=irec) ((R1(i,j),i=1,IM),j=1,JM)
   
   write(*,'(A)',advance="yes")''
   CLOSE(11)
   
   DO J=1,JM
   DO I=1,IM
   
   IF( MASK(I,J)==1.0)THEN
   !.....
   !.....
   DO M=1,MB
   IF(!.....)THEN
   NFRQ(M)=NFRQ(M)+1.0
   END IF !R1
   END DO !M
   END IF !MASK
   
   END DO !I
   END DO !J
   
   julian_day= !DH [hr]
   
   END DO !N
   
   DO M=1,MB
   RFRQ(M)=NFRQ(M)/FLOAT(NTOTAL)
   END DO !M
   
   PRINT *; PRINT '(A,A)','OUTPUT: ',TRIM(OUT)
   OPEN(21,FILE=TRIM(OUT))
   
   WRITE(21, '(A)') ,'# RBIN, RFRQ, NFRQ'
   DO M=1,MB
   !.....
   WRITE(21,'(F9.2,E12.4,F12.0)'),RBIN,RFRQ(M),NFRQ(M)
   END DO !M
   
   END PROGRAM R1H_HISTO
   ```

   

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

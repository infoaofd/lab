# WRF 雨量のヒストグラム 1

WRFの計算結果のバイナリファイルから，雨量のデータを読み込んで，**雨量の度数分布**を作図するためのファイルを作成する。

ここでは，長いプログラムを作成する手順を学ぶ。簡単なところから，**少しづつプログラムを書き足していく**。一つ一つの手順を理解しながら進めていくこと。

このような手法でプログラムを作成することは頻繁にあるので，自分でプログラムを作る際の参考にすること。

[[_TOC_]]

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。

## viエディタの使用法

今回の課題を行う上で，コピー，ペースト機能を使うと作業が速くなるので，操作法を再確認しておく。

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/02.VIM/01.VIM_TUTORIAL.md

### 移動　(ノーマルモード)

> $	行の末尾へ
>
> 0	行の先頭へ
>
> ctrl + u	半画面分 上へ
>
> ctrl + d	半画面分 下へ
>
> gg	そのファイルの先頭へ
>
>  G	そのファイルの末尾へ

### コピーとペースト　(ノーマルモード)

> yy	カーソル行をコピー
>
> y0　行頭からカーソルの直前まで のコピー
>
> y$	カーソルの位置から行末まで のコピー
>
> p	ヤンクした文字列をカーソル位置後にペースト

### 複数行選択　(ノーマルモード)

> V (shift + v)　→　領域選択
>
> ブロック選択
> CTL + V　→　領域選択
>
> yで選択個所をヤンク (コピー)
>
> dで選択個所を削除

### 検索　(ノーマルモード)

> /	後ろにむかって検索
>
>  ?	前にむかって検索
>
> noh ハイライト表示解除

#### 置換　(ノーマルモード)

> 一括置換(確認)
>  :%s/検索文字列/置換文字列/gc
>
>  一括置換(確認しない)
>  :%s/検索文字列/置換文字列/g

## CTLファイル

#### RW3A.00.03.05.05.0702.01.d01.basic_p.01HR.ctl

```
/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0702.01/RW3A.00.03.05.05.0702.01.d01.basic_p.01HR.ctl
dset ^RW3A.00.03.05.05.0702.01.d01.basic_p.01HR_%y4-%m2-%d2_%h2:%n2.dat
options  byteswapped template
undef 1.e30
title  OUTPUT FROM WRF V4.1.5 MODEL
pdef  599 599 lcc  27.000  130.500  300.000  300.000  32.00000  27.00000  130.50000   3000.000   3000.000
xdef 1469 linear  120.56867   0.01351351
ydef 1231 linear   18.56023   0.01351351
zdef   30 levels  
1000.00000
....
 100.00000
tdef   73 linear 00Z12AUG2021      60MN      
VARS   30
U             30  0  x-wind component (m s-1)
....
RAINRNC        1  0  RAIN RATE NON-CONV (mm per output interval)
....
dbz           30  0  Reflectivity (-)
ENDVARS
```



## Fortranプログラムの作成

以下，少しづつプログラムに機能を加えていく。

書かれている順序に従って，**動作を確かめながら順番にプログラムを加筆していくこと**。



### ステップ1: ネームリストを読み込む

**ネームリストファイル**: R1H_HISTO.nml

```
&para
INDIR="/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/",
PREFIX="RW3A.00.03.05.05.0000.01.d01.basic_p.01HR_"
OUT="RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT"
IM=599,
JM=599,
KM=30,
NM=73,
YR0=2021, !00Z12AUG2021
MO0=8,
DY0=12,
HR0=0,
MI0=0,
DH=1,
&end
```

#### Fortranプログラム: R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

INTEGER YR0,MO0,DY0,HR0,MI0,DH

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH

READ(*,nml=para)

PRINT *,TRIM(INDIR),TRIM(PREFIX),TRIM(OUT),IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

END PROGRAM R1H_HISTO_1
```



> $ ifort -assume byterecl -CB -traceback R1H_HISTO_1.F90 -o R1H_HISTO_1.exe
>

> $ R1H_HISTO_1.exe < R1H_HISTO.nml 
>  /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01/
>  RW3A.00.03.05.05.0000.01.d01.basic_p.01HR_
>  RW3A.00.03.05.05.0000.01.d01.01HR_HISTO.TXT         599         599          30
>           73        2021           8          12           0           0
>            1



### ステップ2: 日付からユリウス日を計算する

CTLファイルを見ると

```
options  byteswapped template
```

```
dset ^RW3A.00.03.05.05.0000.01.d01.basic_p.01HR_%y4-%m2-%d2_%h2:%n2.dat
```

```
tdef   73 linear 00Z12AUG2021      60MN  
```

とあるので，１時間おきに73個のデータが**別々の**ファイルに保存されていることがわかる。このため，時刻を指定して開くファイル名を決めていく必要がある。

まず，下記のDATE_CAL.F90に記載された２つのサブルーチン

- date2jd: 与えられた**日付からユリウス日**を計算する
- jd2date: 与えられた**ユリウス日から日付**を計算する

の使用法を覚える。ユリウス日については，下記を参照のこと。

https://eco.mtk.nao.ac.jp/koyomi/wiki/A5E6A5EAA5A6A5B9C6FC.html

また，**シェルスクリプトを使って**，ネームリストファイルの作成，プログラムのコンパイル・実行を**一括して行う**ようにする

#### DATE_CAL.F90

```fortran
      subroutine date2jd(year,month,day,hh,mm,ss,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Julian day from Gregolian caldendar
!-----------------------------------------------------------------------

! ... intent(in)
      integer :: year, month, day, hh, mm, ss
! ... intent(out)
      real(8) :: julian_day
! ... parameter
      real(8), parameter :: jd0  = 1720996.5d0 ! BC4713/ 1/ 1 12:00:00
      real(8), parameter :: mjd0 = 2400000.5d0 !   1858/11/17 00:00:00
! ... local
      integer :: y, m
      if (month < 3) then
        y = year - 1
        m = month + 12
      else
        y = year
        m = month
      end if

      julian_day = 365*(y) + int(y)/4 - int(y)/100 + int(y)/400  &
     &           + int((m+1)*30.6001d0) &
     &           + day           &
     &           + hh/24.0d0     &
     &           + mm/1440.0d0   &
     &           + ss/86400.0d0  &
     &           + jd0

! ... convert julian day to modified julian day
      julian_day = julian_day - mjd0

      end subroutine date2jd

      subroutine jd2date(year,month,day,hour,min,sec,julian_day)
      implicit none
!-----------------------------------------------------------------------
!     get Gregolian caldendar from Julian day
!-----------------------------------------------------------------------
      real(8),intent(in) :: julian_day
      integer,intent(out) :: year, month, day, hour, min, sec
      real(8), parameter :: mjd0 = 2400000.5d0 !  1858/11/17 00:00:00
      
      integer :: jalpha, ia, ib, ic, id, itime
      real(8) :: jday, d, xtime
      
! ... convert modified julian day to julian day
      jday = julian_day + mjd0

      jday = jday + 0.5d0
      if (jday >= 2299161) then
        jalpha = int( (jday - 1867216.25d0)/36524.25d0 )
        d = jday + 1 + jalpha - int(0.25d0*jalpha)
      else
        d = jday
      end if

      ia = int(d) + 1524
      ib = int(6680.0d0 + ((ia-2439870) - 122.1d0)/365.25d0)
      ic = 365*ib + int(0.25d0*ib)
      id = int((ia-ic)/30.6001d0)
      xtime = (d-int(d))*86400.d0
      itime = xtime
      if ( xtime-itime > 0.5d0 ) itime = itime + 1

      day   = ia - ic - int(30.6001*id)

      month = id - 1
      if (month > 12) month = month - 12

      year  = ib - 4715
      if (month > 2) year = year - 1
      if (year <= 0) year = year - 1

      hour  = itime/3600.; min   = (itime - hour*3600)/60
      sec   = itime - hour*3600 - min*60

      end subroutine jd2date
```

#### R1H_HISTO_1.sh

ネームリストファイルの作成，プログラムのコンパイル・実行を一括して行うスクリプト

```bash
#!/bin/bash

src=$(basename $0 .sh).F90; exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml
SUB="DATE_CAL.F90"

f90=ifort
DOPT=" -fpp -CB -traceback -fpe0 -check all"
OPT=" -fpp -convert big_endian -assume byterecl"

#f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OpenMP
#OPT2=" -fopenmp "

RUNNAME=RW3A.00.03.05.05.0000.01
DOMAIN=d01

# ネームリストファイルの作成
cat<<EOF>$nml
&para
INDIR="/work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_${RUNNAME}/",
PREFIX="${RUNNAME}.${DOMAIN}.basic_p.01HR_"
IM=599,
JM=599,
KM=30,
NM=73,
YR0=2021, !00Z12AUG2021
MO0=8,
DY0=12,
HR0=0,
MI0=0,
DH=1,
&end
EOF
echo; echo Created ${nml}.; echo

# プログラムのコンパイル
echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} ${SUB} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} ${SUB} -o ${exe}
if [ $? -ne 0 ]; then

echo; echo "=============================================="; echo
echo "   COMPILE ERROR!!!"
echo; echo "=============================================="; echo
echo TERMINATED.; echo
exit 1
fi
echo "Done Compile."

# プログラムの実行
echo; echo ${exe} is running ...; echo
${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo; echo "=============================================="; echo
echo "   RUNTIME ERROR!!!"
echo; echo "=============================================="; echo
echo TERMINATED.; echo
exit 1
fi
```

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)
PRINT '(A)','MMMMM INPUT: '
PRINT '(A)',' Y    MO  D  H MI DH'
PRINT '(I5,5I3)',YR0,MO0,DY0,HR0,MI0,DH

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

PRINT *
PRINT '(A)','MMMMM OUTPUT: '
call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)
PRINT '(A,F12.3)','MODIFIED JULIAN DAY (FROM 1858/11/17 00:00:00)=',julian_day
PRINT *

PRINT '(A,F12.3)','YEAR, MONTH, DAY FROM julian_day:'
call jd2date(year,month,day,hour,min,sec,julian_day)
PRINT '(I5,5I3)',year,month,day,hour,min

END PROGRAM R1H_HISTO_1
```



> $ R1H_HISTO_1.sh
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...
>
> MMMMM INPUT: 
>  Y    MO  D  H MI DH
>  2021  8 12  0  0  1
>
> MMMMM OUTPUT: 
> MODIFIED JULIAN DAY (FROM 1858/11/17 00:00:00)=   59438.000
>
> YEAR, MONTH, DAY FROM julian_day:
>  2021  8 12  0  0



### ステップ3: 時間に関するループを加える

**NMを総タイムステップ数**とする。

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1

! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)
PRINT '(A)',' Y    MO  D  H MI DH'
PRINT '(I5,5I3)',YR0,MO0,DY0,HR0,MI0,DH

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)
PRINT '(I5,5I3)',year,month,day,hour,min

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh 
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...
>
>  Y    MO  D  H MI DH
>  2021  8 12  0  0  1
>  2021  8 12  0  0
> .....
>  2021  8 15  0  0



### ステップ4: 入力ファイル指定の準備

INという文字型変数 (入力ファイルの名前が記憶されている) の一番最後の文字の位置を調べる。次のステップにおいて，その文字の後に日付と時刻を追記する。

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1

! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
LL=lnblnk(IN)
PRINT '(A,I5)','LL=',LL
PRINT '(A)',IN(1:LL)

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh 
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...
>
> LL=  116
> /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01//RW3A.00.03.05.05.0000.01..basic_p.01HR_



### ステップ5: 入力ファイルの指定

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month, '-'
IS=IE;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day, '_'
IS=IE;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'
PRINT '(A)',TRIM(IN)

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh 
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...
>
> /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01//RW3A.00.03.05.05.0000.01..basic_p.01HR_2021-08-12_00:00.dat
>
> .....
>
> /work00/DATA/HD01/RW3A.ARWpost.DAT/basic_p/ARWpost_RW3A.00.03.05.05.0000.01//RW3A.00.03.05.05.0000.01..basic_p.01HR_2021-08-15_00:00.dat



### ステップ6: 入力ファイルを開くことが出来るか確認

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),dummy2(IM,JM))

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)
CLOSE(11)

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N
END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh
>
> Created R1H_HISTO_1.nml.
>
> Compiling R1H_HISTO_1.F90 ...
>
> ifort -fpp -CB -traceback -fpe0 -check all -fpp -convert big_endian -assume byterecl R1H_HISTO_1.F90 DATE_CAL.F90 -o R1H_HISTO_1.exe
>
> Done Compile.
>
> R1H_HISTO_1.exe is running ...



### ステップ7: 不要データの読み飛ばしのテスト

読み込む**必要のない変数**に関しては，バイナリファイルの**レコード番号を進める**だけにして，**実際には読まない** (**処理速度が大幅に上がる**)

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX
CHARACTER(LEN=400):: INFLE
CHARACTER(LEN=600):: IN, OUT 
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,XLON,XLAT,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),XLON(IM,JM),XLAT(IM,JM),dummy2(IM,JM))

call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)

write(*,'(I5,5I3)',advance='yes'),&
year,month,day,hour,min

irec=0
write(*,'(A)') 'READ XLAT '
irec=irec+1
read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)
!PRINT *,'XLAT',XLAT(1,1),XLAT(1,JM/2),XLAT(1,JM)

write(*,'(A)') 'READ XLON '
irec=irec+1
read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)
!PRINT *,'XLON',XLON(1,1),XLON(IM/2,1),XLON(IM,1)

write(*,*)

write(*,'(A)',advance='no') 'SKIP U. '
DO K = 1, KM
irec=irec+1 !レコード番号を進めるだけにして，実際に読まない(処理速度が大幅に上がる)
END DO !K

write(*,'(A)',advance='no') 'SKIP V. '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance="yes")'';write(*,*)

CLOSE(11)

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh
>
> .....
>
>
>  2021  8 15  0  0
> READ XLAT 
> READ XLON 
>
> SKIP U. SKIP V. 



### ステップ8: データ読み込みのテスト

#### R1H_HISTO_1.F90

```fortran
PROGRAM R1H_HISTO_1
! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO

CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
CHARACTER(LEN=400):: INFLE, INFLE2
CHARACTER(LEN=600):: IN, OUT, IN2
INTEGER IM,JM,KM,NM
INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH

REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
REAL,DIMENSION(:,:),ALLOCATABLE::R1,XLON,XLAT,dummy2

REAL*8 julian_day
INTEGER year,month,day,hour,min,sec !FOR TEST

namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
SS0=0

READ(*,nml=para)

ALLOCATE(dummy3(IM,JM,KM))
ALLOCATE(R1(IM,JM),XLAT(IM,JM),XLON(IM,JM),dummy2(IM,JM))

INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
IN=TRIM(INDIR2) // '/' // TRIM(INFLE2)

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)
irec=0
write(*,'(A)') 'READ XLAT '
irec=irec+1
read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)
PRINT *,'XLAT',XLAT(1,1),XLAT(1,JM/2),XLAT(1,JM)

write(*,'(A)') 'READ XLON '
irec=irec+1
read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)
PRINT *,'XLON',XLON(1,1),XLON(IM/2,1),XLON(IM,1)


call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)

DO N=1,NM

call jd2date(year,month,day,hour,min,sec,julian_day)

IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
IE=lnblnk(IN)

IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')month,'-'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')day,  '_'
IS=IE  ;IE=IS+3; WRITE(IN(IS:IE),'(I2.2,A)')hour, ':'
IS=IE  ;IE=IS+2; WRITE(IN(IS:IE),'(I2.2,A)')min
IS=IE  ;IE=IS+4; WRITE(IN(IS:IE),'(A)')'.dat'

OPEN(11,FILE=TRIM(IN),ACTION="READ",&
form='unformatted',access='direct',recl=IM*JM*4)

write(*,'(I5,5I3)',advance='yes'),&
year,month,day,hour,min

irec=0

write(*,'(A)',advance='no') 'SKIP '

write(*,'(A)',advance='no') 'U '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'V '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'W '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'Q2 '
irec=irec+1

write(*,'(A)',advance='no') 'T2 '
irec=irec+1

write(*,'(A)',advance='no') 'U10 '
irec=irec+1

write(*,'(A)',advance='no') 'V10 '
irec=irec+1

write(*,'(A)',advance='no') 'QVAPOR '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'QCLOUD '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'QRAIN '
DO K = 1, KM
irec=irec+1
END DO !K

write(*,'(A)',advance='no') 'HGT '
irec=irec+1

write(*,'(A)',advance='no') 'RAINC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINRC '
irec=irec+1

write(*,'(A)',advance='no') 'RAINNC '
irec=irec+1

write(*,'(A)',advance="yes")
write(*,'(A)')'READ RAINRNC (=R1) '
irec=irec+1
read (11,rec=irec) ((R1(i,j),i=1,IM),j=1,JM)

write(*,*)
CLOSE(11)

R1AVE=sum(R1)/size(R1); R1MAX=maxval(R1); R1MIN=minval(R1)
PRINT *,'R1AVE',R1AVE,'R1MAX=',R1MAX,'R1MIN=',R1MIN

julian_day=julian_day+dble(DH)/24.0d0 !DH [hr]

END DO !N

END PROGRAM R1H_HISTO_1
```

> $ R1H_HISTO_1.sh
>
> .....
>
> 2021  8 15  0  0
> SKIP U V W Q2 T2 U10 V10 QVAPOR QCLOUD QRAIN HGT RAINC RAINRC RAINNC 
> READ RAINRNC (=R1) 
>
>  R1AVE  0.4319720     R1MAX=   84.05234     R1MIN=  0.0000000E+00



## 課題

1. プログラムの動作を確認しながら，上記ステップ1から8までを順番に行う。プログラムは**自分で打ち込む**こと。

2. 疑問点がある場合，下記の手順で質問事項をまとめておく

   事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

   - 資料のどの箇所に関する質問か
   - エラーメッセージのコピー
   - エラーメッセージをgoogle検索した結果で有益と思われるもの
   - AIの回答結果のうち有益と思われるもの

   打ち合わせ時に上記に対して追加説明を行う。

3. ステップ8のプログラムを下記以外の箇所を参照せずに自分で作成する。.....で記した箇所は自分で埋める必要がある。

   ```FORTRAN
   PROGRAM R1H_HISTO_1
   ! /work09/am/00.WORK/2022.RW3A/0JOS.FALL2023/02.12.RAIN/22.12.R1H_HISTO
   
   CHARACTER(LEN=200):: INDIR, PREFIX, INDIR2
   CHARACTER(LEN=400):: INFLE, INFLE2
   CHARACTER(LEN=600):: IN, OUT, IN2
   INTEGER IM,JM,KM,NM
   INTEGER YR0,MO0,DY0,HR0,MI0,SS0,DH
   
   REAL,DIMENSION(:,:,:),ALLOCATABLE::dummy3
   REAL,DIMENSION(:,:),ALLOCATABLE::R1,XLON,XLAT,dummy2
   
   REAL*8 julian_day
   INTEGER year,month,day,hour,min,sec !FOR TEST
   
   namelist /para/INDIR,PREFIX,OUT,IM,JM,KM,NM,YR0,MO0,DY0,HR0,MI0,DH
   SS0=0
   
   READ(*,nml=.....)
   
   ALLOCATE(dummy3(IM,JM,KM))
   ALLOCATE(R1(IM,JM),XLAT(IM,JM),XLON(IM,JM),dummy2(IM,JM))
   
   INDIR2='/work00/DATA/HD01/RW3A.ARWpost.DAT/hdiv_p/RW3A.00.03.05.05.0000.01'
   INFLE2='RW3A.00.03.05.05.0000.01.d01.hdiv_p.01HR_2021-08-11_00:00.dat'
   IN=TRIM(INDIR2) // '/' // TRIM(INFLE2)
   
   OPEN(11,FILE=TRIM(IN),ACTION="READ",&
   form='unformatted',access='direct',recl=IM*JM*4)
   irec=0
   write(*,'(A)') 'READ XLAT '
   irec=irec+1
   read (11,rec=irec) ((XLAT(i,j),i=1,IM),j=1,JM)
   
   write(*,'(A)') 'READ XLON '
   irec=irec+1
   read (11,rec=irec) ((XLON(i,j),i=1,IM),j=1,JM)
   
   call date2jd(YR0,MO0,DY0,HR0,MI0,SS0,julian_day)
   
   DO N=1,NM
   
   call jd2date(.....,julian_day)
   
   IN=TRIM(INDIR) // '/' // TRIM(PREFIX)
   IE=.....
   
   IS=IE+1;IE=IS+5; WRITE(IN(IS:IE),'(I4.4,A)')year, '-'
   IS=IE  ;IE=IS+.....; WRITE(IN(.....),'(.....)')month,'-'
   IS=IE  ;IE=IS+.....; WRITE(IN(.....),'(.....)')day,  '_'
   IS=IE  ;IE=IS+.....; WRITE(IN(.....),'(.....)')hour, ':'
   IS=IE  ;IE=IS+.....; WRITE(IN(.....),'(.....)')min
   IS=IE  ;IE=IS+.....; WRITE(IN(.....),'(.....)')'.dat'
   
   OPEN(11,FILE=TRIM(IN),ACTION="READ",&
   form='unformatted',access='direct',recl=.....)
   
   write(*,'(I5,5I3)',advance='yes'),&
   year,month,day,hour,min
   
   irec=0
   
   write(*,'(A)',advance='no') 'SKIP '
   
   write(*,'(A)',advance='no') 'U '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'V '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'W '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'Q2 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'T2 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'U10 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'V10 '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'QVAPOR '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'QCLOUD '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'QRAIN '
   DO K = 1, KM
   irec=irec+1
   END DO !K
   
   write(*,'(A)',advance='no') 'HGT '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINC '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINRC '
   irec=irec+1
   
   write(*,'(A)',advance='no') 'RAINNC '
   irec=irec+1
   
   write(*,'(A)',advance="yes")
   write(*,'(A)')'READ RAINRNC (=R1) '
   irec=irec+1
   read (11,rec=irec) ((R1(i,j),i=1,.....),j=1,.....)
   
   write(*,*)
   CLOSE(11)
   
   R1AVE=.....; R1MAX=.....; R1MIN=.....
   PRINT *,'R1AVE',R1AVE,'R1MAX=',R1MAX,'R1MIN=',R1MIN
   
   julian_day=..... !DH [hr]
   
   END DO !N
   
   END PROGRAM R1H_HISTO_1
   ```
   
   ```FORTRAN
         subroutine date2jd(year,month,day,hh,mm,ss,julian_day)
   ! ... intent(in)
         integer :: year, month, day, hh, mm, ss
   ! ... intent(out)
         real(8) :: julian_day
   ! ... parameter
         real(8), parameter :: jd0  = 1720996.5d0 ! BC4713/ 1/ 1 12:00:00
         real(8), parameter :: mjd0 = 2400000.5d0 !   1858/11/17 00:00:00
   .....
         end subroutine date2jd
   
         subroutine jd2date(year,month,day,hour,min,sec,julian_day)
         implicit none
   ! ... intent(in)
         real(8) :: julian_day
   ! ... intent(out)
         integer :: year, month, day, hour, min, sec
   ! ... parameter
         real(8), parameter :: mjd0 = 2400000.5d0 !  1858/11/17 00:00:00
   ! ... local
         integer :: jalpha, ia, ib, ic, id
         integer :: itime
         real(8) :: jday, d, xtime
   .....
         end subroutine jd2date
   ```
   
   

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している


# C:\Users\boofo\Dropbox\01.TOOL\22.ANALYSIS\12.STATISTICS\12.TEST.STATISTICS\22.22.CORRELATION

R=$1; R=${R:-1.0}
N=$2; N=${N:-100}

OFLE=RANDOM_NUMBER_R${R}.TXT

SRC=$(basename $0 .sh).F90
if [ ! -f $SRC ];then echo NO SUCH FILE, $SRC;exit 1;fi

SUB="RANDOM_NORMAL.F90"
if [ ! -f $SUB ];then echo NO SUCH FILE, $SUB;exit 1;fi

EXE=$(basename $SRC .F90).EXE

rm -vf $EXE
ifort -traceback -CB $SUB $SRC -o $EXE
if [ ! -f $EXE ];then echo NO SUCH FILE $EXE; exit 1;fi


NML=$(basename $SRC .F90)_NML.TXT
cat <<EOF >$NML
&para
R=${R},
N=${N}
OFLE="$OFLE",
&end
EOF

$EXE < $NML
rm $EXE

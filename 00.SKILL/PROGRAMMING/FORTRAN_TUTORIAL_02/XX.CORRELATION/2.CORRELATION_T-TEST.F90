! C:\Users\boofo\Dropbox\01.TOOL\22.ANALYSIS\12.STATISTICS\12.TEST.STATISTICS\22.22.CORRELATION
PROGRAM CORRELATION_T_TEST

INTEGER N
CHARACTER(LEN=500)::INFLE
REAL,ALLOCATABLE,DIMENSION(:)::x,y
REAL r,prob,z

NAMELIST /PARA/INFLE
READ(5,NML=PARA)
PRINT '(A,A )','MMMMM INFLE: ',TRIM(INFLE)

OPEN(11,FILE=INFLE,ACTION="READ")
READ(11,*)N
ALLOCATE(X(N),Y(N))
DO I=1,N
READ(11,*)X(I),Y(I)
END DO
CLOSE(11)

CALL PEARSN(x,y,n,r,prob,z)

SIGPCNT=95.0
IF(SIGPCNT==99.0)THEN
Z_ALPHA2=2.58
ELSE
SIGPCNT=95.0
Z_ALPHA2=1.96
END IF

! https://istat.co.jp/sk_commentary/estimation/EPcorrelation
if(n>3 .and. r/=1.0)then
RC=0.5*log((1.0+r)/(1.0-r))
ZL=RC-Z_ALPHA2*sqrt(1.0/(float(n)-3))
ZU=RC+Z_ALPHA2*sqrt(1.0/(float(n)-3))
RL=(exp(2.0*ZL)-1.0)/(exp(2.0*ZL)+1)
RU=(exp(2.0*ZU)-1.0)/(exp(2.0*ZU)+1)
else
RL=0.0; RU=0.0
end if

PRINT '(A,I7)','MMMMM      N  = ',N
PRINT '(A,F7.3)','MMMMM      R  = ',R
PRINT '(A,F7.5,2x,E11.4)','MMMMM P-VALUE = ',prob,prob
IF(RL/=0.0 .and. RU/=0.0)THEN
PRINT '(A,F5.1,A)','SIGNIFICANCE LEVEL: ',100.0-SIGPCNT,'%'
PRINT '(A,F7.3,A,F7.3,A)','CONFIDENCE INTERVAL=[',RL,',',RU,']'
END IF

STOP

CONTAINS

SUBROUTINE PEARSN(x,y,n,r,prob,z)
! This routine will regularize the unusual case of complete correlation.
! USES betai
! Given two arrays x(1:n) and y(1:n), this routine computes their correlation coefficient
! r (returned as r), the signifficance level at which the null hypothesis of zero correlation
! is disproved (prob whose small value indicates a significant correlation), and Fisher's z
! (returned as z), whose value can be used in further statistical tests as described above.

INTEGER,INTENT(IN):: n
REAL,INTENT(IN):: x(n),y(n)
REAL,INTENT(INOUT):: prob,r,z
REAL,PARAMETER::TINY=1.e-20
INTEGER j
REAL ax,ay,df,sxx,sxy,syy,t,xt,yt,betai

ax=0.; ay=0.
DO j=1,n !Find the means.
ax=ax+x(j)
ay=ay+y(j)
END DO

ax=ax/n; ay=ay/n
sxx=0.; syy=0.; sxy=0.

DO j=1,n !Compute the correlation coefficient.
xt=x(j)-ax
yt=y(j)-ay
sxx=sxx+xt**2
syy=syy+yt**2
sxy=sxy+xt*yt
END DO
r=sxy/(sqrt(sxx*syy)+TINY)

!Fisher's z transformation.
z=0.5*log(((1.+r)+TINY)/((1.-r)+TINY)) 

df=n-2 !DEGREE OF FREEDOM

t=r*sqrt(df/(((1.-r)+TINY)*((1.+r)+TINY))) !Equation (14.5.5).
prob=betai(0.5*df,0.5,df/(df+t**2))        !Student's t probability.
! prob=erfcc(abs(z*sqrt(n-1.))/1.4142136) 

!For large n, this easier computation of prob, using the short routine erfcc,
!would give approximately the same value.
END SUBROUTINE PEARSN


END PROGRAM CORRELATION_T_TEST


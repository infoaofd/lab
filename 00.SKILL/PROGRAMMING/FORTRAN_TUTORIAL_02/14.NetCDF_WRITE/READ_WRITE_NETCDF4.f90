program read_write_netcdf4

! FORTRAN90用のnetCDFを使用する
! 前のプログラム(read_write_netcdf3.f90)ではFORTRAN77用のnetCDF
! を使用していたので注意
! その為netCDFライブラリの関数名が若干変更になっている

! netCDFライブラリで使用する変数や定数の設定ファイルを読み込む
use netCDF ! netCDF4
! include 'netcdf.inc' !netCDF3

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname

integer,parameter::nx=241, ny=253, np=16, nt=8

integer stat, ncid, varid, varid1

! short型の2バイト整数
integer(2)::varin(nx,ny,np,nt)

real::var(nx,ny,np,nt), lon(nx), lat(ny), p(np), time(nt)
real::scale, offset

! 変数の名前
varname='rh'

INDIR='/work01/DATA/MSM/MSM-P/2021/'
INFLE='0812.nc'
IN=trim(INDIR)//trim(INFLE)
print '(a,a)','INPUT: ',trim(IN)

! ファイルを開く
ncid=0
stat = nf90_open(IN, NF90_NOWRITE, ncid)
print *,'nf90 open stat=',stat
print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
print '(a,i5)',trim(varname),varid
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
print *,'nf90_get_att stat=',stat
stat = nf90_get_att(ncid,varid,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
print '(a,f10.5)','scale_factor = ',scale
print '(a,f10.5)','offset       = ',offset

! 相対湿度の本当の値を配列varに入れる
var(:,:,:,:)=float(varin(:,:,:,:))*scale+offset

!座標(緯度,経度,時刻)を読み込む
print '(A,3I5)','MMMMM READ COORDINATE VARIABLES SUCH AS TIME, LAT, LON'
stat=nf90_inq_varid(ncid, 'time', varid1) 
print '(a,i5)','time: varid=',varid1
stat=nf90_get_var(ncid, varid1, time)

stat=nf90_inq_varid(ncid, 'p', varid1) 
print '(a,i5)','p: varid=',varid1
stat=nf90_get_var(ncid, varid1, p)
print *,p

stat=nf90_inq_varid(ncid, 'lat', varid1) 
print '(a,i5)','lat: varid=',varid1
stat=nf90_get_var(ncid, varid1, lat)

stat=nf90_inq_varid(ncid, 'lon', varid1) 
print '(a,i5)','lon: varid=',varid1
stat=nf90_get_var(ncid, varid1, lon)

stat=NF90_CLOSE(ncid)!入力ファイル閉じる




!相対湿度の本当の値をNETCDFとして書き出す
ODIR="./"
OFLE="read_netcdf_20210812_rh.nc"
OUT=trim(ODIR)//trim(OFLE)
!call system("rm -vf "//trim(OFLE))

! NetCDFファイルを開く
stat=nf90_create( trim(OUT), NF90_HDF5, ncido)

!変数の次元を決める
print '(A,3I5)','MMMMM DEFINE DIMENSIONS nx,ny,np,nt=',nx,ny,np,nt
stat=nf90_def_dim(ncido, 'time', nt, id_time_dim)
stat=nf90_def_dim(ncido, 'p',    np, id_p_dim)
stat=nf90_def_dim(ncido, 'lat',  ny, id_lat_dim)
stat=nf90_def_dim(ncido, 'lon',  nx, id_lon_dim)

!書き出し用の変数を定義する
print '(A,3I5)','MMMMM DEFINE VARIABLES'
stat=nf90_def_var(ncido, 'time', NF90_REAL, id_time_dim, id_time)
stat=nf90_def_var(ncido, 'p'   , NF90_REAL, id_p_dim,    id_p)
stat=nf90_def_var(ncido, 'lat', NF90_REAL, id_lat_dim, id_lat)
stat=nf90_def_var(ncido, 'lon', NF90_REAL, id_lon_dim, id_lon)

!変数の属性 (単位などの付加情報)の追加
print '(A,3I5)','MMMMM ADD ATTRIBUTES TO VARIABLES'
stat=nf90_put_att(ncido, id_time, 'units','hours since 2021-08-12 00:00')
! hours sinceのところは入力データと合わせておく必要がある。
! あらかじめ入力ファイルをncdump -hコマンドで確認しておく
stat=nf90_put_att(ncido, id_p, 'units','hPa')
stat=nf90_put_att(ncido, id_lat, 'units','degrees_north')
stat=nf90_put_att(ncido, id_lon, 'units','degrees_east')

!変数の属性などをファイルに書き出し
PRINT '(a,3I5)','MMMMM WRITE VRIABLES'
PRINT '(A,A)','MMMMM VARIABLE NAME: ',TRIM(varname)
stat=NF90_DEF_VAR(ncido, TRIM(varname), NF90_REAL, & 
     (/id_lon_dim, id_lat_dim, id_p_dim, id_time_dim/), id_varo)
stat=NF90_PUT_ATT(ncido, id_varo,'units','%') 
stat=NF90_PUT_ATT(ncido, id_varo,'_FillValue', -9999.) 
stat=NF90_PUT_ATT(ncido, id_varo,'missing_value', -9999.) 

!ファイル自体の属性 (付加情報)の追加
print '(A,A,3I5)','MMMMM GLOBAL ATTRIBUTES'
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_dir",TRIM(INDIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_file",TRIM(INFLE))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_dir",TRIM(ODIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_file",TRIM(OFLE))

stat=nf90_enddef(ncido) 

!変数の値をファイルに書き出す
print '(A,3I5)','MMMMM WRITE VARIABLES'
stat=NF90_PUT_VAR(ncido, id_time, time ) 
stat=NF90_PUT_VAR(ncido, id_p,    p  ) 
stat=NF90_PUT_VAR(ncido, id_lat,  lat ) 
stat=NF90_PUT_VAR(ncido, id_lon,  lon )
stat=NF90_PUT_VAR(ncido, id_varo, var ) 

stat=NF90_CLOSE(ncido) !出力ファイル閉じる

print *;print '(a,a)',"MMMMM OUTPUT: ",trim(OUT)

end program read_write_netcdf4


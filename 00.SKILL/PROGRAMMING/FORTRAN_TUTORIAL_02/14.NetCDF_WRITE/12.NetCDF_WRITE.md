# NetCDFの書き出し

[[_TOC_]]

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。

## NetCDFの読み込みの手順

### 準備

> netCDFモジュールを読む。
>
> netCDFの関数の引数を受け取るsubroutineを用意しておく。

### 読み込む

> ncidを得る 。
>
> varid を得る（要ncid)
>
> 変数を読む。
>
> ファイルを閉じる。




## 前回の課題

NetCDFを読んでプレーンバイナリで書き出し

**READ_WRITE_NETCDF3.f90**

```fortran
program
use netCDF !FORTRAN90用のnetCDFを使用する

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname

integer,parameter::nx=241, ny=253, np=16, nt=8

integer stat, ncid, varid, varid1

! short型の2バイト整数
integer(2)::varin(nx,ny,np,nt)

real::var(nx,ny,np,nt), lon(nx), lat(ny), p(np), time(nt)
real::scale, offset

! 変数の名前
varname='rh'

INDIR='/work01/DATA/MSM/MSM-P/2021/'
INFLE='0812.nc'
IN=trim(INDIR)//trim(INFLE)
print '(a,a)','INPUT: ',trim(IN)

! ファイルを開く
ncid=0
stat = nf90_open(IN, NF90_NOWRITE, ncid)

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
stat = nf90_get_att(ncid,varid,'add_offset',offset)

! 相対湿度の本当の値を配列varに入れる
var(:,:,:,:)=float(varin(:,:,:,:))*scale+offset

!座標(緯度,経度,時刻)を読み込む
print '(A,3I5)','MMMMM READ COORDINATE VARIABLES SUCH AS TIME, LAT, LON'
stat=nf90_inq_varid(ncid, 'time', varid1) 
stat=nf90_get_var(ncid, varid1, time)

stat=nf90_inq_varid(ncid, 'p', varid1) 
stat=nf90_get_var(ncid, varid1, p)

stat=nf90_inq_varid(ncid, 'lat', varid1) 
stat=nf90_get_var(ncid, varid1, lat)

stat=nf90_inq_varid(ncid, 'lon', varid1) 
stat=nf90_get_var(ncid, varid1, lon)
stat=NF90_CLOSE(ncid)!入力ファイル閉じる



end program
```

### GrADSのコントロールファイル

**rh.CTL**

```fortran
dset /work09/2021/nakamuro/2023_PROGRAM/2024-02-09_13_EXERCISE/read_netcdf_%y4%m2%d2_rh.bin
options template yrev
title 
undef 9.96921e+36
xdef 241 linear 120 0.125
ydef 253 levels 22.4 22.5 22.6 22.7 22.8 22.9 23 23.1
 23.2 23.3 23.4 23.5 23.6 23.7 23.8 23.9 24 24.1
 24.2 24.3 24.4 24.5 24.6 24.7 24.8 24.9 25 25.1
 25.2 25.3 25.4 25.5 25.6 25.7 25.8 25.9 26 26.1
 26.2 26.3 26.4 26.5 26.6 26.7 26.8 26.9 27 27.1
 27.2 27.3 27.4 27.5 27.6 27.7 27.8 27.9 28 28.1
 28.2 28.3 28.4 28.5 28.6 28.7 28.8 28.9 29 29.1
 29.2 29.3 29.4 29.5 29.6 29.7 29.8 29.9 30 30.1
 30.2 30.3 30.4 30.5 30.6 30.7 30.8 30.9 31 31.1
 31.2 31.3 31.4 31.5 31.6 31.7 31.8 31.9 32 32.1
 32.2 32.3 32.4 32.5 32.6 32.7 32.8 32.9 33 33.1
 33.2 33.3 33.4 33.5 33.6 33.7 33.8 33.9 34 34.1
 34.2 34.3 34.4 34.5 34.6 34.7 34.8 34.9 35 35.1
 35.2 35.3 35.4 35.5 35.6 35.7 35.8 35.9 36 36.1
 36.2 36.3 36.4 36.5 36.6 36.7 36.8 36.9 37 37.1
 37.2 37.3 37.4 37.5 37.6 37.7 37.8 37.9 38 38.1
 38.2 38.3 38.4 38.5 38.6 38.7 38.8 38.9 39 39.1
 39.2 39.3 39.4 39.5 39.6 39.7 39.8 39.9 40 40.1
 40.2 40.3 40.4 40.5 40.6 40.7 40.8 40.9 41 41.1
 41.2 41.3 41.4 41.5 41.6 41.7 41.8 41.9 42 42.1
 42.2 42.3 42.4 42.5 42.6 42.7 42.8 42.9 43 43.1
 43.2 43.3 43.4 43.5 43.6 43.7 43.8 43.9 44 44.1
 44.2 44.3 44.4 44.5 44.6 44.7 44.8 44.9 45 45.1
 45.2 45.3 45.4 45.5 45.6 45.7 45.8 45.9 46 46.1
 46.2 46.3 46.4 46.5 46.6 46.7 46.8 46.9 47 47.1
 47.2 47.3 47.4 47.5 47.6
zdef 16 levels 1000 975 950 925 900 850 800 700
 600 500 400 300 250 200 150 100
tdef 8  linear 00Z12AUG2021 180mn
vars 1
rh 16 0 relative humidity (%)
endvars
```

**GrADSのコントロールファイルの解説**

**GrADSは上記のようなCTL (コントロール)ファイルと呼ばれるテキストファイルを使って，入力ファイルの情報を得る。**

コントロールファイル自体にデータは記録されていない。実際にデータが記録されているのは上記CTLファイルの冒頭のdsetで指定されたファイルである。

### GrADSのスクリプト

**rh.gs**

```fortran
'open rh.CTL'

'set t 1'

'q dims'

say result
date=sublin(result,5)
say
say date

'cc'
'set gxout shaded'
'd rh'
'cbarn'

FIG='rh_CHECK.pdf'
'gxprint 'FIG
say 'OUTPUT: 'FIG


'quit'
```



## 今回の課題

NetCDFファイルを読み込んで，NetCDFに書き出し

## NetCDFの読み書きの手順

#### 準備

**FortranでNetCDFファイルを読み書きするためには，NetCDFライブラリを使用する必要がある。**

ライブラリの使用法の概要を下記に示す。詳細と具体例は後述する。

### 読み込む

> ファイルを開いてncidを得る （**nf90_open**）
>
> varid を得る (**nf90_inq_varid**)
>
> 変数を読む (**nf90_get_var**)
>
> ファイルを閉じる (**nf90_close**)
>

### 書き込む

> 書き込むファイルを開く(**nf90_create**)。
>
> 次元の定義 (**nf90_def_dim**)
>
> 変数の定義 (**nf90_def_var**)
>
> 変数にattribution (属性≒付加情報) を付ける (**nf90_put_att**)
>
> 変数定義モードを終える (**nf90_enddef**)
>
> 変数を書き込む (**nf90_put_var**)
>
> 書き込むファイルを閉じる (**nf90_close**)

### FORTRANプログラム

#### ソースファイル

READ_WRITE_NETCDF4.f90

```FORTRAN
program read_write_netcdf4

use netCDF ! netCDF4

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname

integer,parameter::nx=241, ny=253, np=16, nt=8

integer stat, ncid, varid, varid1

! short型の2バイト整数
integer(2)::varin(nx,ny,np,nt)

real::var(nx,ny,np,nt), lon(nx), lat(ny), p(np), time(nt)
real::scale, offset

! 変数の名前
varname='rh'

INDIR='/work01/DATA/MSM/MSM-P/2021/'
INFLE='0812.nc'
IN=trim(INDIR)//trim(INFLE)
print '(a,a)','INPUT: ',trim(IN)

print '(A)','MMMMM FILE OPEN' !ファイルを開く
stat = nf90_open(IN, nf90_nowrite, ncid)

print '(A)','MMMMM INPUT' !データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
stat = nf90_get_var(ncid, varid, varin)
stat = nf90_get_att(ncid,varid,'scale_factor',scale)
stat = nf90_get_att(ncid,varid,'add_offset',offset)

! 相対湿度の本当の値を配列varに入れる
var(:,:,:,:)=float(varin(:,:,:,:))*scale+offset

!座標(緯度,経度,時刻)を読み込む
print '(A,3I5)','MMMMM READ COORDINATE VARIABLES SUCH AS TIME, LAT, LON'
stat=nf90_inq_varid(ncid, 'time', varid1) 
print '(a,i5)','time: varid=',varid1
stat=nf90_get_var(ncid, varid1, time)

stat=nf90_inq_varid(ncid, 'p', varid1) 
print '(a,i5)','p: varid=',varid1
stat=nf90_get_var(ncid, varid1, p)
print *,p

stat=nf90_inq_varid(ncid, 'lat', varid1) 
print '(a,i5)','lat: varid=',varid1
stat=nf90_get_var(ncid, varid1, lat)

stat=nf90_inq_varid(ncid, 'lon', varid1) 
print '(a,i5)','lon: varid=',varid1
stat=nf90_get_var(ncid, varid1, lon)
stat=NF90_CLOSE(ncid)!入力ファイル閉じる

print '(A)','MMMMM OUTPUT' !相対湿度の値をNETCDFとして書き出す
ODIR="./"
OFLE="read_netcdf_20210812_rh.nc"
OUT=trim(ODIR)//trim(OFLE)
!call system("rm -vf "//trim(OFLE))

! NetCDFファイルを開く
stat=nf90_create( trim(OUT), NF90_HDF5, ncido)

!変数の次元を決める
print '(A,3I5)','MMMMM DEFINE DIMENSIONS nx,ny,np,nt=',nx,ny,np,nt
stat=nf90_def_dim(ncido, 'time', nt, id_time_dim)
stat=nf90_def_dim(ncido, 'p',    np, id_p_dim)
stat=nf90_def_dim(ncido, 'lat',  ny, id_lat_dim)
stat=nf90_def_dim(ncido, 'lon',  nx, id_lon_dim)

!書き出し用の変数を定義する
print '(A,3I5)','MMMMM DEFINE VARIABLES'
stat=nf90_def_var(ncido, 'time', NF90_REAL, id_time_dim, id_time)
stat=nf90_def_var(ncido, 'p'   , NF90_REAL, id_p_dim,    id_p)
stat=nf90_def_var(ncido, 'lat', NF90_REAL, id_lat_dim, id_lat)
stat=nf90_def_var(ncido, 'lon', NF90_REAL, id_lon_dim, id_lon)

!変数の属性 (単位などの付加情報)の追加
print '(A,3I5)','MMMMM ADD ATTRIBUTES TO VARIABLES'
stat=nf90_put_att(ncido, id_time, 'units','hours since 2021-08-12 00:00')
! hours sinceのところは入力データと合わせておく必要がある。
! あらかじめ入力ファイルをncdump -hコマンドで確認しておく
stat=nf90_put_att(ncido, id_p, 'units','hPa')
stat=nf90_put_att(ncido, id_lat, 'units','degrees_north')
stat=nf90_put_att(ncido, id_lon, 'units','degrees_east')

!変数の属性などをファイルに書き出し
PRINT '(a,3I5)','MMMMM WRITE VRIABLES'
PRINT '(A,A)','MMMMM VARIABLE NAME: ',TRIM(varname)
stat=NF90_DEF_VAR(ncido, TRIM(varname), NF90_REAL, & 
     (/id_lon_dim, id_lat_dim, id_p_dim, id_time_dim/), id_varo)
stat=NF90_PUT_ATT(ncido, id_varo,'units','%') 
stat=NF90_PUT_ATT(ncido, id_varo,'_FillValue', -9999.) 
stat=NF90_PUT_ATT(ncido, id_varo,'missing_value', -9999.) 

!ファイル自体の属性 (付加情報)の追加
print '(A,A,3I5)','MMMMM GLOBAL ATTRIBUTES'
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_dir",TRIM(INDIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_file",TRIM(INFLE))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_dir",TRIM(ODIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_file",TRIM(OFLE))

stat=nf90_enddef(ncido) 

!変数の値をファイルに書き出す
print '(A,3I5)','MMMMM WRITE VARIABLES'
stat=NF90_PUT_VAR(ncido, id_time, time ) 
stat=NF90_PUT_VAR(ncido, id_p,    p  ) 
stat=NF90_PUT_VAR(ncido, id_lat,  lat ) 
stat=NF90_PUT_VAR(ncido, id_lon,  lon )
stat=NF90_PUT_VAR(ncido, id_varo, var ) 

stat=NF90_CLOSE(ncido) !出力ファイル閉じる

print *;print '(a,a)',"MMMMM OUTPUT: ",trim(OUT)

end program read_write_netcdf4
```

#### コンパイル

ifortではなく**gfortranを使用する**

> $ gfortran READ_WRITE_NETCDF4.F90 -fbacktrace -o READ_WRITE_NETCDF4.EXE -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include

#### 実行

> $ READ_WRITE_NETCDF4.EXE 
> INPUT: /work01/DATA/MSM/MSM-P/2021/0812.nc
> MMMMM FILE OPEN
> MMMMM INPUT
> MMMMM READ COORDINATE VARIABLES SUCH AS TIME, LAT, LON
> time: varid=    4
> p: varid=    3
>    1000.00000       975.000000       950.000000       925.000000       900.000000       850.000000       800.000000       700.000000       600.000000       500.000000       400.000000       300.000000       250.000000       200.000000       150.000000       100.000000    
> lat: varid=    2
> lon: varid=    1
> MMMMM OUTPUT
> MMMMM DEFINE DIMENSIONS nx,ny,np,nt=  241  253   16
>
> MMMMM DEFINE VARIABLES
> MMMMM ADD ATTRIBUTES TO VARIABLES
> MMMMM WRITE VRIABLES
> MMMMM VARIABLE NAME: rh
> MMMMM GLOBAL ATTRIBUTES
> MMMMM WRITE VARIABLES
>
> MMMMM OUTPUT: ./read_netcdf_20210812_rh.nc

#### 出力ファイルの概要

> $ ncdump -c read_netcdf_20210812_rh.nc 

### 作図

#### GrADSスクリプト

**CTLファイルは不要**

**PLT_RH_NC.GS**

```FORTRAN
INFLE='read_netcdf_20210812_rh.nc'         ;#入力ファイル名
OFLE='read_netcdf_20210812_01_1000_rh.PDF' ;#図のファイル名

'sdfopen 'INFLE                            ;#入力ファイルを開く

'set t 1'                                  ;#時間ステップ1
'set lev 1000'                             ;#1000hPa

'cc'                                       ;#画面消去
'set gxout shaded'                         ;#カラーシェード

'd rh'                                     ;#描画
'cbarn'                                    ;#カラースケール

'gxprint 'OFLE                             ;#ファイルに書き出し
say
say 'OUTPUT: 'OFLE
'quit'
```

#### 描画

```bash
$ grads -bcp PLT_RH_NC.GS 
```



<img src="image-20240222185740050.png" alt="image-20240222185740050" style="zoom:50%;" />

## 課題

1 上記READ_WRITE_NETCDF4.f90を自分で打ちこみ，実行する

2 結果を作図して上図と同様の図が作成されるか確認する

3 下記以外を見ることなしに，READ_WRITE_NETCDF4.f90を自分で作成する。2と同じ結果がでるまで行うこと。

```FORTRAN
program read_write_netcdf4

use netCDF ! netCDF4

character(len=500)::INDIR, INFLE, ODIR, OFLE
character(len=1000)::IN, OUT
character(len=50)::varname

integer,parameter::nx=241, ny=253, np=16, nt=8

integer stat, ncid, varid, varid1

! short型の2バイト整数
integer(2)::varin(nx,ny,np,nt)

real::var(nx,ny,np,nt), lon(nx), lat(ny), p(np), time(nt)
real::scale, offset

! 変数の名前
varname='rh'

INDIR='/work01/DATA/MSM/MSM-P/2021/'
INFLE='0812.nc'
IN=trim(INDIR)//trim(INFLE)
print '(a,a)','INPUT: ',trim(IN)

! ファイルを開く
ncid=0
stat = nf90_open(IN, nf90_nowrite, ncid)
print *,'nf90 open stat=',stat; print *,'nf90 open ncid=',ncid

! データを読む
stat = nf90_inq_varid(ncid, trim(varname), varid)
print '(a,i5)',trim(varname),varid
stat = nf90_get_.....(...., ....., .....)
stat = nf90_get_.....(.....,.....,'scale_factor',scale)
stat = nf90_get_.....(.....,.....,'add_offset',offset)
print '(a,a)','varname = ',trim(varname)
print '(a,f10.5)','scale_factor = ',scale
print '(a,f10.5)','offset       = ',offset

! 相対湿度の本当の値を配列varに入れる
var(:,:,:,:)=float(varin(:,:,:,:))*scale+.....

!座標(緯度,経度,時刻)を読み込む
print '(A,3I5)','MMMMM READ COORDINATE VARIABLES SUCH AS TIME, LAT, LON'
stat=nf90_inq_varid(ncid, 'time', varid1) 
print '(a,i5)','time: varid=',varid1
stat=nf90_get_var(ncid, varid1, time)

stat=nf90_inq_varid(ncid, 'p', varid1) 
print '(a,i5)','p: varid=',varid1
stat=nf90_get_var(ncid, varid1, p)
print *,p

stat=nf90_inq_varid(ncid, 'lat', varid1) 
print '(a,i5)','lat: varid=',varid1
stat=nf90_get_var(ncid, varid1, lat)

stat=nf90_inq_varid(ncid, 'lon', varid1) 
print '(a,i5)','lon: varid=',varid1
stat=nf90_get_var(ncid, varid1, lon)

stat=NF90_CLOSE(ncid)!入力ファイル閉じる




!相対湿度の本当の値をNETCDFとして書き出す
ODIR="./"
OFLE="read_netcdf_20210812_rh.nc"
OUT=trim(ODIR)//trim(OFLE)
!call system("rm -vf "//trim(OFLE))

! NetCDFファイルを開く
stat=nf90_create( trim(OUT), NF90_HDF5, ncido)

!変数の次元を決める
print '(A,3I5)','MMMMM DEFINE DIMENSIONS nx,ny,np,nt=',nx,ny,np,nt
stat=nf90_def_.....(ncido, 'time', nt, id_time_dim)
stat=nf90_def_.....(ncido, 'p',    np, id_p_dim)
stat=nf90_def_.....(ncido, 'lat',  ny, id_lat_dim)
stat=nf90_def_.....(ncido, 'lon', ....., id_lon_dim)

!書き出し用の変数を定義する
print '(A,3I5)','MMMMM DEFINE VARIABLES'
stat=nf90_def_.....(ncido, 'time', NF90_REAL, id_time_dim, id_time)
stat=nf90_def_.....(ncido, 'p'   , NF90_REAL, id_p_dim,    id_p)
stat=nf90_def_.....(ncido, 'lat', NF90_REAL, id_lat_dim, id_lat)
stat=nf90_def_.....(ncido, 'lon', NF90_REAL, id_lon_dim, id_lon)

!変数の属性 (単位などの付加情報)の追加
print '(A,3I5)','MMMMM ADD ATTRIBUTES TO VARIABLES'
stat=nf90_put_.....(ncido, id_time, 'units','hours since 2021-08-12 00:00')
! hours sinceのところは入力データと合わせておく必要がある。
! あらかじめ入力ファイルをncdump -hコマンドで確認しておく
stat=nf90_put_.....(ncido, id_p, 'units','hPa')
stat=nf90_put_.....(ncido, id_lat, 'units','degrees_north')
stat=nf90_put_.....(ncido, id_lon, 'units','degrees_east')

!変数の属性などをファイルに書き出し
PRINT '(a,3I5)','MMMMM WRITE VRIABLES'
PRINT '(A,A)','MMMMM VARIABLE NAME: ',TRIM(varname)
stat=NF90_DEF_VAR(ncido, TRIM(varname), NF90_REAL, & 
     (/....., ....., ....., id_time_dim/), id_varo)
stat=NF90_PUT_.....(ncido, id_varo,'units','%') 
stat=NF90_PUT_.....(ncido, id_varo,'_FillValue', -9999.) 
stat=NF90_PUT_.....(ncido, id_varo,'missing_value', -9999.) 

!ファイル自体の属性 (付加情報)の追加
print '(A,A,3I5)','MMMMM GLOBAL ATTRIBUTES'
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_dir",TRIM(INDIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"input_file",TRIM(INFLE))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_dir",TRIM(ODIR))
stat=NF90_PUT_ATT(ncido,NF90_GLOBAL,"output_file",TRIM(OFLE))

stat=nf90_enddef(ncido) 

!変数の値をファイルに書き出す
print '(A,3I5)','MMMMM WRITE VARIABLES'
stat=NF90_PUT_.....(ncido, id_time, time ) 
stat=NF90_PUT_.....(ncido, id_p,    p  ) 
stat=NF90_PUT_.....(ncido, id_lat,  lat ) 
stat=NF90_PUT_.....(ncido, id_lon,  lon )
stat=NF90_PUT_.....(ncido, id_varo, var ) 

stat=NF90_CLOSE(ncido) !出力ファイル閉じる

print *;print '(a,a)',"MMMMM OUTPUT: ",trim(OUT)

end program read_write_netcdf4
```



## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

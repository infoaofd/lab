# 相関のある2つの擬似乱数の生成

統計的な数値実験をするために、相関のある乱数を生成する。

検索するとコレスキー分解を使った方法が出てくる。これは変数の数がいくつでも対応できるやり方だが，ここでは，2変数の場合に適用可能な簡便な方法を説明する[^1]。

[^1]: 結局 $2$​ 変数の場合のコレスキー分解をやってるのと同じ結果となる。

# 概要
平均がゼロで分散が等しい独立な乱数 $X$ と $Y$ がある。このとき、$X$ と相関係数 $\rho$ で相関し、同じ分散を持つ乱数 $Z$ は

$$
Z = \rho X + \sqrt{1 - \rho^2} Y
$$

のように作れる。

ここでは、

* 理論：なぜ相関がある乱数が作れるのか
* 検証：Python で実際にやってみる

について述べる。

# 理論
独立な乱数　$X$ と $Y$ について、平均はそれぞれゼロ、分散は同じ値で $\sigma^2$ とする。$X$ と $Y$ は独立なので、共分散や相関係数はゼロである。

また、$X$ と相関を持たせたい乱数 $Z$ をとりあえず $X$ と $Y$ の重ね合わせで
$$
Z = a X + b Y
$$
と定義する。$a$ と $b$ は定数である。

このように定義するのは、以下の極端な例に対して次のことが成り立つためである。

| $a$ | $b$ | $\rho$ |
| :---: | :---: | :---: |
| $> 0$ | $0$ | $1$ |
| $=0$ | $\neq 0$ | $0$ |
| $< 0$ | $0$ | $-1$ |

この表を見ると、$a$ と $b$ のバランスをうまく変えてやることで自由な相関係数が作れることが予想される[^2]。そのため、先程の $Z$ の表現は $X$ と任意の相関を持つ乱数として有望だと考えられる。

では実際に $X$ と $Z$ の相関係数 $\rho$ を計算する。相関係数は，
$$
\rho = \frac{\mathrm{Cov}[X, Z]}{\sqrt{\mathrm{Var}[X]} \sqrt{\mathrm{Var}[Z]}}
$$
で計算できる。ここで、$\mathrm{Cov}$、$\mathrm{Var}$ はそれぞれ共分散、分散である。共分散を計算すると，
$$
\begin{align}
\mathrm{Cov}[X, Z] &= \mathrm{Cov}[X, a X + b Y] & (\because Z \ の定義)
\\
&= a \ \mathrm{Cov}[X, X] + b \ \mathrm{Cov}[X, Y] & (\because \mathrm{Cov} \ の性質)
\\
&= a \ \sigma^2 & (\because \mathrm{Cov}[X, X] = \mathrm{Var}[X] = \sigma^2, \mathrm{Cov}[X, Y] = 0)
\end{align}
$$
となる。わからなくなったら定義に立ち返って落ち着いて計算すればたどり着ける。

また、$Z$ の分散については、
$$
\begin{align}
\mathrm{Var}[Z] &= \mathrm{Var}[a X + b Y] & (\because Z \ の定義)
\\
&= a^2 \mathrm{Var}[X] + b^2 \mathrm{Var}[Y] & (\because \mathrm{Var} \ の性質)
\\
&= a^2 \sigma^2 + b^2 \sigma^2 &
\end{align}
$$
となるので、相関係数は、
$$
\begin{align}
\rho &= \frac{\mathrm{Cov}[X, Z]}{\sqrt{\mathrm{Var}[X]} \sqrt{\mathrm{Var}[Z]}}
\\
&= \frac{a \ \sigma^2}{\sqrt{\sigma^2} \sqrt{a^2 \sigma^2 + b^2 \sigma^2}}
\\
&= \frac{a}{\sqrt{a^2 + b^2}}
\end{align}
$$
で与えられる。この式を $b$ について解くと、
$$
b = \frac{\sqrt{1 - \rho^2}}{\rho} a
$$
が得られる。 $a = c \rho$ （$c$ は正の定数）と置くと、
$$
Z = c \left( \rho X + \sqrt{1 - \rho^2} Y \right)
$$
となる。この $c$ は $Z$ の分散をコントロールするためのパラメータであり、次のように
$$
\begin{align}
\mathrm{Var}[Z] &= \mathrm{Var}\left[{c \left( \rho X + \sqrt{1 - \rho^2} Y \right)} \right]
\\
&= c^2 \left[ \rho^2 \sigma^2 + (1 - \rho) \sigma^2 \right]
\\
&= c^2 \sigma^2
\end{align}
$$
分散を $c^2$ 倍する（つまり標準偏差を $c$ 倍する）効果を持っている。これを $1$ とすることで冒頭の式
$$
Z = \rho X + \sqrt{1 - \rho^2} Y
$$
が得られる。

[^2]: 自由と言っても $-1$ から $1$ の範囲内である。

# 検証
Python3を使って得られた乱数が望みどおりのものか検証する。ひとまず $X$ と $Y$ には一様乱数を使ってみる。

```python
import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt

size = int(1e3) # Size of the vector
rho_in = 0.6 # Correlation coefficient between two random variables

# Generate uniform random numbers with mean = 0
x = rand.rand(size) - 0.5
y = rand.rand(size) - 0.5

# Generate random numbers correlated with x
z = rho_in * x + (1 - rho_in ** 2) ** 0.5 * y

# Calculate stats
variance = np.var(z)
rho_out = np.corrcoef(x, z)[1][0]
print("variance: {}, correlation: {}".format(variance, rho_out))

# Plot results
fig, ax = plt.subplots()
ax.scatter(x, z, s=1)
ax.set_xlabel('X')
ax.set_ylabel('Z')
plt.show()
```
結果は以下の通りである。

```
variance: 0.08332907524577293, correlation: 0.5952102586280674
```
相関係数のほうは狙い（$0.6$）に近い値が出ているようだが，分散の値が中途半端となっている。生成される $Z$ の分散は $X$ の分散と同じとなる。今 $X$ は区間$[-0.5, 0.5]$ の一様乱数で、その分散は [1 / 12 になる](http://www.geocities.jp/jun930/etc/varianceofrandom.html "一様乱数の分散")ので、$1 / 12 = 0.0833...$ に近い値が出てきているのです。

 $Z$ - $X$ プロットは下記の通り。一様乱数に一様乱数を足しているので、データ点の分布は平行四辺形の内部に一様に近いものとなる。
![uniform_uniform_rho0.6.png](https://qiita-image-store.s3.amazonaws.com/0/96638/3eada5f5-d3ca-ac9b-7118-7d39b2b0c9ca.png)

データ点の分布を変更したい場合、`y` のところを

```python
y = rand.randn(size) * (1 / 12) ** 0.5
```
のように正規乱数に変えてやればよい。$X$ と $Y$ で分散をそろえるため、一様乱数の標準偏差をかける。

結果は次の通りとなる。
`variance: 0.0823649369923887, correlation: 0.5983078612793685`
![uniform_normal_rho0.6.png](https://qiita-image-store.s3.amazonaws.com/0/96638/1f9b0bad-2b8a-41b3-322e-246133a80a18.png)

**注意**: これらふたつの方法で作った乱数 $Z$ は一様乱数とはならない。$X$ も $Y$ も一様乱数を使った場合、ヒストグラムは以下のような台形に似た形となる。
![histo_uniform_uniform_rho0.6.png](https://qiita-image-store.s3.amazonaws.com/0/96638/9118ede4-985b-f887-d34f-b7c54edaadf8.png)

これは $Y$ に正規乱数を使ったときも同じで、山のカドがちょっと丸くなる感じになります。

これは、異なる一様分布にそれぞれ従う2つの確率変数を足しても一様分布になるとは限らないからである。これを統計学の言葉で「[再生性](https://ja.wikipedia.org/wiki/%E5%86%8D%E7%94%9F%E6%80%A7 "再生性 - Wikipedia")がない」と言う。これを無理やりまた[一様乱数に戻そうという試みもある](http://ir.lib.fukushima-u.ac.jp/dspace/bitstream/10270/3043/1/3-297.pdf "相互相関のある一様乱数の一発生法")。

一方，正規分布は再生性がある。 $X$ も $Y$ も正規乱数にした場合は下記の通りとなる。変更点は，上の方の `x` と `y` の箇所のみである。

```python
import numpy as np
import numpy.random as rand
import matplotlib.pyplot as plt

size = int(1e4) # Size of the vector
rho_in = 0.6 # Correlation coefficient between two random variables

# Generate normal random numbers
x = rand.randn(size)
y = rand.randn(size)

# Generate random numbers correlated with x
z = rho_in * x + (1 - rho_in ** 2) ** 0.5 * y

# Calculate stats
variance = np.var(z)
rho_out = np.corrcoef(x, z)[1][0]
print("variance: {}, correlation: {}".format(variance, rho_out))

# Plot results
fig, ax = plt.subplots()
ax.scatter(x, z, s=1)
ax.set_xlabel('X')
ax.set_ylabel('Z')
plt.show()
```
出力は次のようになる。
`variance: 0.9884508169450733, correlation: 0.5936089719053868`

目的の，分散 $1$ の正規乱数を使って相関 $0.6$ という値に近い値が計算されている。相関係数は $[-1, 1]$ の範囲で指定すること。

作図結果は次のようになる。
![normal_normal_rho0.6.png](https://qiita-image-store.s3.amazonaws.com/0/96638/fd9df53b-94bb-3aed-9398-256327bb93e8.png)

ヒストグラムは以下の通りである。赤い点線は理論的に予測される、平均ゼロで分散 $1$ の正規分布である。
![histo_normal_normal_rho0.6.png](https://qiita-image-store.s3.amazonaws.com/0/96638/3cfae3b6-da3e-43d2-bbc3-8939f6d4e75e.png)

# まとめ
相関がある乱数の作成法と原理について解説した。一様乱数を単純に組み合わせただけでは，分布が歪な形状になってしまうことが分かった。分布の形状まで気にする場合は正規乱数を使うのがよい。

# 参考
**[相関のある n 個の擬似乱数の生成（Python サンプルつき） - Qiita](http://qiita.com/horiem/items/e3dfe0a0f0ac3a66f509 "相関のある n 個の擬似乱数の生成（Python サンプルつき） - Qiita")**
$n$ 個の乱数がどのようにして作れるか？ という解説をしています。


/work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.22.CORRELATION

https://mizumot.com/handbook/?page_id=460
https://multivariate-statistics.com/2022/04/20/r-programming-correlation-coefficient-test/

```bash
2024-03-26_20-39
/work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.22.CORRELATION/R
$ R

R version 4.1.1 (2021-08-10) -- "Kick Things"
Copyright (C) 2021 The R Foundation for Statistical Computing
Platform: x86_64-conda-linux-gnu (64-bit)

'demo()' と入力すればデモをみることができます。 
'help()' とすればオンラインヘルプが出ます。 
'help.start()' で HTML ブラウザによるヘルプがみられます。 
'q()' と入力すれば R を終了します。 
```

```R
(ins) > dat <- read.csv("ch09correl.csv", header=TRUE, fileEncoding="CP932")
(ins) > attach(dat) 
(ins) > cor(国語, 英語)
(ins) > cor.test(国語, 英語)
```

```R
    Pearson's product-moment correlation
data:  国語 and 英語
t = 4.3555, df = 18, p-value = 0.0003812
alternative hypothesis: true correlation is not equal to 0
95 percent confidence interval:
 0.4008811 0.8799217
sample estimates:
      cor 
0.7163268 
```

```R
(ins) > quit()
(ins) Save workspace image? [y/n/c]: y
```


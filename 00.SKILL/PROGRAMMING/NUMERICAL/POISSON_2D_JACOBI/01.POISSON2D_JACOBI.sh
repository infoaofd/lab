#!/bin/bash
#
# Sat, 28 Dec 2024 19:25:14 +0900
# localhost.localdomain
# /work09/ma/00.SKILL/PROGRAM/NUMERICAL/POISSON_SERIAL
#
src=$(basename $0 .sh).f90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml
OUT='POISSON_RESULT.BIN'
NX=20; NY=20
NXP1=$(expr $NX + 1); NYP1=$(expr $NY + 1)
if [ $NX -ne $NY ];then echo EEEEE nx must be equal to ny. NX=$NX NY=$NY;exit 1;fi
DX=$(echo "1.0/$NX" | bc -l | xargs printf "%.10f\n")
DY=$(echo "1.0/$NY" | bc -l | xargs printf "%.10f\n")

# export LD_LIBRARY_PATH=/usr/local/netcdf-c-4.8.0/lib:/usr/local/lib:/usr/local/netcdf-c-4.8.0/lib::.:.

#f90=ifort
#DOPT=" -fpp -CB -traceback -fpe0 -check all"
#OPT=" -fpp -convert big_endian -assume byterecl"

f90=gfortran
#DOPT=" -ffpe-trap=invalid,zero,overflow,underflow -fcheck=array-temps,bounds,do,mem,pointer,recursion"
#OPT=" -L. -lncio_test -O2 "
# OPT=" -L/usr/local/netcdf-c-4.8.0/lib -lnetcdff -I/usr/local/netcdf-c-4.8.0/include "


# OpenMP
#OPT2=" -fopenmp "
cat<<EOF>$nml
&para
OUT="$OUT"
NX=$NX
NY=$NY
&end
EOF


echo Compiling ${src} ...
echo
echo ${f90} ${DOPT} ${OPT} ${src} -o ${exe}
echo
${f90} ${DOPT} ${OPT} ${OPT2} ${src} -o ${exe}
if [ $? -ne 0 ]; then
echo
echo EEEEE COMPILE ERROR!!!
echo EEEEE TERMINATED.
echo
exit 1
fi
echo "MMMMM Done Compile."
echo

echo
echo MMMMM ${exe} is running ...
echo
D1=$(date -R)

${exe} < ${nml}
if [ $? -ne 0 ]; then
echo
echo EEEEE ERROR in $exe: RUNTIME ERROR!!!
echo EEEEE TERMINATED.
echo
rm -vf $exe $nml
D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"
exit 1
fi
echo
echo MMMMM Done ${exe}
echo
rm -vf $exe $nml
D2=$(date -R)
echo "START: $D1"; echo "END:   $D2"

if [ ! -f $OUT ];then echo EEEEE NO SUCH FILE,$OUT;echo;exit 1;fi
CTL=$(basename $OUT .BIN).CTL
cat <<EOF>$CTL
dset ^${OUT}
UNDEF 1.E30
options big_endian
* options big_endian yrev
XDEF $NXP1 LINEAR 0 $DX
YDEF $NYP1 LINEAR 0 $DY
ZDEF 1 LEVELS 1
TDEF 1 LINEAR 12Z03JUL2020 1HR
VARS 1
V 0 99 Solution of Poisson EQ.
ENDVARS
EOF

echo;echo MMMMM CTL: $CTL

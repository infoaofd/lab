PROGRAM POISSON_JACOBI
IMPLICIT NONE
INTEGER NX,NY
REAL,ALLOCATABLE,DIMENSION(:,:)::V, F
REAL X,Y,H
INTEGER I,J,IND,RECLEN
REAL, PARAMETER :: PI = 3.141592653589793, EPS = 1E-5
CHARACTER OUT*500
REAL u_exact, uxxyy_exact
NAMELIST /PARA/OUT,NX,NY

PRINT '(A)','MMMMM SOLVE TWO DIMENSIONAL POISSON EQUATION BY JACOBI METHOD'

READ(5,NML=PARA)
ALLOCATE(V(0:NX,0:NY),F(0:NX,0:NY))

H = 1.0/NX
DO J = 0, NY
   Y = H*J
   DO I = 0, NX
      X = H*I
      V(I,J) = 0
      F(I,J) = sin ( pi * x * y ) !COS(PI*X)*COS(3*PI*Y) + SIN(2*PI*X)*SIN(4*PI*Y)
   ENDDO !I
ENDDO !J

!  The "boundary" entries of F store the boundary values of the solution.
!  The "interior" entries of F store the right hand sides of 
!  the Poisson equation.
!
!H = 1.0/NX
!  do j = 0, ny
!   Y = H*J
!    y = real ( j - 1, kind = 4 ) / real ( ny - 1, kind = 4 )
!    do i = 0, nx
!      X = H*I
!      x = real ( i - 1, kind = 4 ) / real ( nx - 1, kind = 4 )
!      if ( i == 1 .or. i == nx .or. j == 1 .or. j == ny ) then
!        f(i,j) = u_exact ( x, y )
!      else
!        f(i,j) = - uxxyy_exact ( x, y )
!      end if
!    end do
!  end do

CALL JACOBI(V,F,H,NX,NY,EPS,IND)

PRINT '(A,I10)','MMMMM IND = ',IND

PRINT '(A)','MMMMMM OUTPUT'
INQUIRE(IOLENGTH=RECLEN)V
PRINT '(A,I10)','MMMMM RECORD LENGTH OF V = ',RECLEN
PRINT '(A,I10)','MMMMM (NX+1)*(NY+1)*4= ',(NX+1)*(NY+1)*4;PRINT *

OPEN(21,FILE=OUT,FORM='UNFORMATTED',ACCESS='DIRECT',RECL=RECLEN,&
CONVERT='BIG_ENDIAN')
WRITE(UNIT=21,REC=1)V
CLOSE(21)
PRINT '(A,A)','MMMMM OUT: ',TRIM(OUT)

END PROGRAM POISSON_JACOBI

SUBROUTINE JACOBI(V,F,H,NX,NY,EPS,IND)
IMPLICIT NONE
REAL V(0:NX,0:NY),F(0:NX,0:NY),H,EPS,DEL,H2
REAL,ALLOCATABLE :: V2(:,:)
INTEGER NX,NY,IND,I,J,IT
INTEGER, PARAMETER :: ITMAX = 100000
ALLOCATE ( V2(NX-1,NY-1) )

H2 = H*H
DO IT = 1, ITMAX
   DO J = 1, NY-1
      DO I = 1, NX-1
         V2(I,J) = (V(I,J-1) + V(I-1,J) + V(I+1,J) &
                                 + V(I,J+1) - H2*F(I,J))/4
      ENDDO
   ENDDO
   V(1:NX-1,1:NY-1) = V2(:,:)
   DEL = 0
   DO J = 1, NY-1
      DO I = 1, NX-1
         DEL = MAX(DEL, ABS(V(I-1,J)+V(I+1,J)+V(I,J-1)+V(I,J+1) &
                              -4*V(I,J) - H2*F(I,J)))
      ENDDO
   ENDDO
   DEL = DEL/H2
   IF (DEL < EPS) EXIT
ENDDO
IF (IT > ITMAX) THEN
   IND = -ITMAX
 ELSE
   IND = IT
ENDIF
DEALLOCATE (V2)
END SUBROUTINE JACOBI
function u_exact ( x, y )

!*****************************************************************************80
!
!! U_EXACT evaluates the exact solution.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    25 October 2011
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, Y, the coordinates of a point.
!
!    Output, real ( kind = 8 ) U_EXACT, the value of the exact solution 
!    at (X,Y).
!
  implicit none

  real ( kind = 4 ), parameter :: pi = 3.141592653589793E+00
  real ( kind = 4 ) u_exact
  real ( kind = 4 ) x
  real ( kind = 4 ) y

  u_exact = sin ( pi * x * y )

  return
end
function uxxyy_exact ( x, y )

!*****************************************************************************80
!
!! UXXYY_EXACT evaluates ( d/dx d/dx + d/dy d/dy ) of the exact solution.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    25 October 2011
!
!  Author:
!
!    John Burkardt
!
!  Parameters:
!
!    Input, real ( kind = 8 ) X, Y, the coordinates of a point.
!
!    Output, real ( kind = 8 ) UXXYY_EXACT, the value of 
!    ( d/dx d/dx + d/dy d/dy ) of the exact solution at (X,Y).
!
  implicit none

  real ( kind = 4 ), parameter :: pi = 3.141592653589793E+00
  real ( kind = 4 ) uxxyy_exact
  real ( kind = 4 ) x
  real ( kind = 4 ) y

  uxxyy_exact = - pi * pi * ( x * x + y * y ) * sin ( pi * x * y )

  return
end

# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import pylab

x = np.arange(0, 1, 0.02)
y = np.arange(0, 1, 0.02)

X, Y =pylab.meshgrid(x, y) 
U = np.sin ( 3.141592653589793 * X * Y )

fig = plt.figure(figsize=(6,6))
ax = fig.add_subplot(111)

im=plt.imshow(U,extent=(0,1,0,1))
plt.colorbar(im)
ax.set_xlabel(r"x",fontsize=14)
ax.set_ylabel(r"y",fontsize=14)
ax.tick_params(axis='x', labelsize=14)
ax.tick_params(axis='y', labelsize=14)

# スクリプト名から出力ファイル名を生成
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + ".PDF"

# プロットを保存
plt.savefig(FIG, bbox_inches="tight")
plt.close()
print("FIG: " + FIG)

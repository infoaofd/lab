# 副プログラム

[[_TOC_]]

## FAQ（よくある質問）

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。

## 内容

プログラムが長くなってくると，作業の一部を分割した方が良い場合が出てくる。例えば，同じ処理を何度も繰り返す必要がある場合，その箇所を分離させた方が分かりやすく，短いプログラムになることが多い。今回は，そのようなときにつかう，

- 関数
- サブルーチン

について学ぶ。

## 関数

### 組み込み関数

Fortranでは，$\sin x$ を計算したかったら，そのための関数がすでに組み込まれている。下記のプログラムは，$x$ の値を与えて，$\sin x$ の値を計算を行う。

#### TEST_SIN.F90

```FORTRAN
pi=3.1415926535897932384626
x=pi/2.0
y=sin(x)
print '(A,2f7.3)','x,y=',x,y
end
```

TEST_SIN.F90の実行例

```fortran
$ ifort TEST_SIN.F90 -o TEST_SIN.exe; TEST_SIN.exe 
x,y=  1.571  1.000
```

次に示すプログラムは，ベクトルの内積を計算する関数 (`DOT_PRODUCT`) の使用例である。

#### DOT.F90

```fortran
REAL,DIMENSION(3)::A,B
REAL C
A=[1,2,3]
B=[2,3,4]
F=DOT_PRODUCT(A,B)
PRINT '(A,3F3.0)','A=',(A(i),i=1,3)
PRINT '(A,3F3.0)','B=',(B(i),i=1,3)
PRINT '(A,F5.0)','F=',F
END
```

`A=[1,2,3]`の意味については，`fortran 配列構成子`で検索するか，AIに質問すると回答が得られる。

DOT.F90の実行例

```bash
$ ifort DOT.F90 -o DOT.exe

$ DOT.exe 
A= 1. 2. 3.
B= 2. 3. 4.
F=  20.
```

### 組み込み関数の一覧

https://www.nag-j.co.jp/fortran/FI_10.html

上記サイトを見るか，`fortran 組み込み関数`で検索する。

### 関数の自作

関数は自作することもできる。下記は扇形の面積を求める関数を自作した例である。

#### TEST_A_SECTOR.F90

```FORTRAN
REAL RADIUS
REAL ANGLE    ![DEGREE]
REAL A_SECTOR !AREA OF A SECTOR

RADIUS=2.0; ANGLE=30.0

A_SECTOR=AS(RADIUS,ANGLE)

PRINT '(A,2F5.0)','RADIUS,ANGLE=',RADIUS,ANGLE
PRINT '(A,F10.3)','A_SECTOR=',A_SECTOR
END


REAL FUNCTION AS(R,PHI)
REAL R   !RADIUS
REAL PHI !ANGLE [DEGREE]
PARAMETER PI=3.1415926535897932384626

AS=PI*R**2 * PHI/360.0
END
```

実行例

```
$ ifort TEST_A_SECTOR.F90 -o TEST_A_SECTOR.exe

$ TEST_A_SECTOR.exe 
RADIUS,ANGLE=   2.  30.
A_SECTOR=     1.047
```

下記いずれかの方法で，実行結果の確認が可能である

`扇形の面積`で検索してヒットした計算サイトで計算する

google の検索窓に `pi*2^2*30/360`と打ち込む

#### TEST_A_SECTOR.F90の説明

自作する場合には，

- 主プログラム
- 関数副プログラム

にプログラムを分割する。上記例では，下記のように分割されている。

##### 主プログラム

```
REAL RADIUS
... 中略
PRINT '(A,F10.3)','A_SECTOR=',A_SECTOR
END
```

主プログラムの末尾に `END` 文があり，主プログラムの終わりが明示されていることに注意する。

```FORTRAN
RADIUS=2.0; ANGLE=30.0
```

この箇所のセミコロン `;`　の使い方については，`Fotran セミコロン`で検索するかAIに質問する。

```FORTRAN
A_SECTOR=AS(RADIUS,ANGLE)
```

`A_SECTOR=AS(RADIUS,ANGLE)` : ASという名称の自作関数を使用している箇所。計算には，RADIUSとANGLEという変数に代入されている値が使用され，計算結果は A_SECTORという変数に代入される。

この例における変数RADIUSとANGLEのことを，**引数**と呼ぶ。



##### 関数副プログラム

```fortran
REAL FUNCTION AS(R,PHI)
... 中略
AS=PI*R**2 * PHI/360.0
END
```

副プログラムの末尾に `END` 文があり，副プログラムの終わりが明示されていることに注意する。

```FORTRAN
REAL FUNCTION AS(R,PHI)
REAL R   !RADIUS
REAL PHI !ANGLE [DEGREE]
```

`REAL FUNCTION AS` で，関数ASの計算結果は単精度実数型とすることを明示している。

変数`R,PHI`のことを**仮引数**という。主プログラムの引数と関数の仮引数は，

- 変数の**数**
- 変数の**並び順**
- 変数の**型**

が一致している必要がある。ケアレスミスによって，主プログラムと関数で上記の不一致がしばしば発生する。この場合，`segmentation fault` や `segmentation violation` といったエラー (メモリの不正アクセス) の原因となる。

しかし，関数内の仮引数と，主プログラム内の引数の**名称**は一致させる必要はない。上記のプログラムでは，

- 主プログラムの変数RADIUSに対し，関数の変数R
- 主プログラムの変数ANGLEに対し，関数の変数PHI

が対応している。これは，作成した関数がさまざまな主プログラムが使用されることを想定しているからである。

名称まで対応させるようにした場合，主プログラムを変更した場合に，それに合わせて関数内の引数の名称を毎回変更する必要が生じ，大変面倒になる。



## サブルーチン

関数よりも複雑な処理を行いたい場合に備えて，サブルーチン副プログラムという機能が用意されている。

### 計算結果が1変数で収まる簡単な例

理解を容易にするために，関数を使用した上述のプログラム `TEST_A_SECTOR.F90` と同じ機能をもつプログラムを，サブルーチンを使って作成する。

### TEST_SUB_AS.F90

```fortran
REAL RADIUS
REAL ANGLE    ![DEGREE]
REAL A_SECTOR !AREA OF A SECTOR

RADIUS=2.0; ANGLE=30.0

CALL SUB_AS(RADIUS,ANGLE,A_SECTOR)

PRINT '(A,2F5.0)','RADIUS,ANGLE=',RADIUS,ANGLE
PRINT '(A,F10.3)','A_SECTOR=',A_SECTOR
END


SUBROUTINE SUB_AS(R,PHI,AS)
REAL R   !RADIUS
REAL PHI !ANGLE [DEGREE]
REAL AS
PARAMETER PI=3.1415926535897932384626

AS=PI*R**2 * PHI/360.0
END
```

**実行例**

```bash
$ ifort TEST_SUB_AS.F90 -o TEST_SUB_AS.exe
$ TEST_SUB_AS.exe
RADIUS,ANGLE=   2.  30.
A_SECTOR=     1.047
```

当然のことだが，関数を用いた場合と同じ結果となる。

### サブルーチンの書式

#### 主プログラム

```FORTRAN
... 前略

RADIUS=2.0; ANGLE=30.0

CALL SUB_AS(RADIUS,ANGLE,A_SECTOR)

... 後略
END
```



```FORTRAN
CALL SUB_AS(RADIUS,ANGLE,A_SECTOR)
```

後で定義する，サブルーチン `SUB_AS` を使用するために，`CALL` 文を使用している。

サブルーチンの計算結果を代入するための変数である，`A_SECTOR`　が加えられている。



#### 副プログラム

```fortran
SUBROUTINE SUB_AS(R,PHI,AS)
... 中略
REAL AS
```

主プログラムにおける変数 `A_SECTOR` に対応する仮引数 `AS` が追加されている。

実数型の変数として，仮引数 `AS` を宣言している。



### 計算結果が複数個の例

この場合は，サブルーチンを使うことが多い (最近のバージョンのFortranであれば関数でも実装できる)。例として，ベクトルの外積を計算するサブルーチンを作成してみる。

ベクトル $\boldsymbol{A}=(A_1,A_2,A_3)$ とベクトル $\boldsymbol{B}=(B_1,B_2,B_3)$ の外積 $\boldsymbol{A}\times\boldsymbol{B}$ は，
$$
&&\boldsymbol{A}\times\boldsymbol{B}\\
\\
&=&(A_2B_3-A_3B_2,\;A_3B_1-A_1B_3,\;A_1B_2-A_2B_1)
$$
で計算できる。ベクトル $\boldsymbol{C}=(C_1,C_2,C_3)=\boldsymbol{A}\times\boldsymbol{B}$ とすると，サブルーチンを使って，ベクトル $\boldsymbol{C}$の成分$(C_1,C_2,C_3)$ を計算するプログラムは以下のように書ける。

#### TEST_OUTER_PRODUCT.F90

```FORTRAN
REAL::A1,A2,A3
REAL::B1,B2,B3
REAL::C1,C2,C3

A1=1;A2=2;A3=3
B1=2;B2=3;B3=4

CALL OUTER_PRODUCT(A1,A2,A3,B1,B2,B3,C1,C2,C3) 
PRINT '(A,3F3.0)','A=',A1,A2,A3
PRINT '(A,3F3.0)','B=',B1,B2,B3
PRINT '(A,3F3.0)','C=',C1,C2,C3
END

SUBROUTINE OUTER_PRODUCT(A1,A2,A3,B1,B2,B3,C1,C2,C3)
REAL::A1,A2,A3
REAL::B1,B2,B3
REAL::C1,C2,C3

C1=A2*B3-A3*B2
C2=A3*B1-A1*B3
C3=A1*B2-A2*B1
END
```

下記いずれかの方法で，実行結果の確認が可能である

- `ベクトル　外積　計算`で検索してヒットした計算サイトで計算する

- 上記の定義に従って，手で計算する

#### intent属性の利用

仮変数の並びをケアレスミスで間違えて，入力用の変数が出力用の変数と対応してしまっている状態や，その逆の状態を作ってしまうことがよくある。このような誤りを防ぐために，`intent`属性という機能が装備されているので，使うようにした方が良い。上記のプログラムの場合，副プログラムにおいて，

```fortran
REAL,INTENT(IN)::A1,A2,A3
REAL,INTENT(IN)::B1,B2,B3
REAL,INTENT(OUT)::C1,C2,C3
```

とすると，A1,A2,A3,B1,B2,B3は入力用で，C1,C2,C3は出力用であることが明示できるので，便利である。この場合，副プログラムにおいて，A1,A2,A3,B1,B2,B3の値を書き変えようとすると，エラーになる。

入力された変数の値を書き変えて出力させたい場合，

```fortran
REAL,INTENT(INOUT)::C1,C2,C3
```

とすればよい。

### 配列の使用

サブルーチンの引数として，配列を使用することがよくあるので，例を示しておく。先程の`TEST_OUTER_PRODUCT.F90` を配列を使用して書き変えると次のようになる。

#### TEST_OUTER_PRODUCT_ARRAY.F90

```fortran
REAL,DIMENSION(3)::A,B,C
REAL::C1,C2,C3

A=[1,2,3]
B=[2,3,4]

CALL OUTER_PRODUCT(A,B,C)
PRINT '(A,3F3.0)','A=',(A(I),I=1,3)
PRINT '(A,3F3.0)','B=',(B(I),I=1,3) 
PRINT '(A,3F3.0)','C=',(C(I),I=1,3)
END

SUBROUTINE OUTER_PRODUCT(A,B,C)
REAL,DIMENSION(3),INTENT(IN)::A,B
REAL,DIMENSION(3),INTENT(INOUT)::C

C(1)=A(2)*B(3)-A(3)*B(2)
C(2)=A(3)*B(1)-A(1)*B(3)
C(3)=A(1)*B(2)-A(2)*B(1)
END
```

**実行例**

```bash
$ ifort TEST_OUTER_PRODUCT_ARRAY.F90 -o TEST_OUTER_PRODUCT_ARRAY.exe

$ TEST_OUTER_PRODUCT_ARRAY.exe
A= 1. 2. 3.
B= 2. 3. 4.
C=-1. 2.-1.
```

## 参考

プログランミングに慣れてくると，`segmentation fault` や `segmentation violation` というエラーに遭遇することが多くなる。このエラーはほとんどの場合，下記の3つのいずれかの不整合によって生じているので，その点をまず調査してみるとよい。

- 変数の**数**
- 変数の**並び順**
- 変数の**型**

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している


# makeコマンド

[TOC]

## 概要

ここでは，本格的なプログラミングへの準備として，

- 複数のソースファイルから，一つの実行ファイルを作る方法 (makeコマンドの使用法)

について学ぶ

## FAQ（よくある質問）

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。


## 準備

```
$ cd $HOME
```

```
$ cd 2022_PROGRAM
```

```
$ mkdir -vp 07
```

```
$ cd 07
```

```
$ pwd
/work03/gu1/LAB/2022_PROGRAM/07
```

```
$ ift
```

もしくは

```
$ source  /opt/intel/oneapi/setvars.sh --force
```



## ソースファイルの分割

`$ vi main.f90`

```fortran
program main

a=10.0
b=2.0

print *,'a=',a
print *,'b=',b

call sub(c,a,b)

print *,'c=',c

end program main
```


`vi sub.f90`

```fortran
subroutine sub(c,a,b)

real,intent(in)::a,b
real,intent(out)::c

c=a+b

end subroutine sub
```
`$ ifort main.f90 sub.f90 -o test_sub.exe`     

`$ ll test_sub.exe`  
-rwxrwxr-x 1 am am 8.6K  8月 22 13:55 test_sub.exe       

`$ test_sub.exe`   
 a=   10.0000000    
 b=   2.00000000    
 c=   12.0000000      



## 分割コンパイルとリンク

### コンパイルの手順

コンパイル手順を詳しく書くと，  

ソースファイル →オブジェクトファイル→実行ファイル

となる。普段はこの一連の作業を人間に見せないようにしているが，ここでは，このような手順を踏んでいることを理解しておく必要がある。



### 分割コンパイル

```
$ ifort -c main.f90

$ ifort -c sub.f90

$ ll main.o sub.o
```

main.oとsub.oのようなファイルをオブジェクトファイルと呼ぶ。

`-c`はオブジェクトファイルを作るところで作業を止め，実行ファイルは作らないことを意味する。



### リンク

複数のオブジェクトファイルを結合させて実行ファイルを作成することをリンクと呼ぶ。

```
$ ifort main.o sub.o -o test_sub2.exe
```

```
$ ll test_sub.exe test_sub2.exe
```



### 実行

```
$ test_sub2.exe
```



## Makefileとmakeコマンド 

### makeコマンドとは

プログラムの規模が大きくなりソースファイルの数が増えてくると，コンパイルに時間がかかるようになる。

そのため，更新されたソースファイル**だけ**をコンパイルするようにすると時間が短縮できる。  

この用途で使うコマンドにmakeコマンドがある。    

makeコマンドはmakefileという名称のファイルから情報を読み取り，コンパイルに必要な作業を行う。

名称は頭文字のみ大文字のMakefileの場合もある。

### 解説記事

**下記リンクを読んでおくこと**

- http://omilab.naist.jp/~mukaigawa/misc/Makefile.html
- https://ie.u-ryukyu.ac.jp/~e085739/c.makefile.tuts.html

リンク切れの場合は，`make コマンド` で検索した結果の中から分かりやすいものを2つ選んで読んでおくこと。

### 簡単な例

`$ cat makefile`

```makefile
test_sub.exe: main.o sub.o
        ifort -o test_sub.exe main.o sub.o
main.o: main.f90
        ifort -c main.f90
sub.o: sub.f90
        ifort -c sub.f90
clean:
        rm -vf main.o sub.o test_sub.exe
```



### 実行例

```
$ make clean
```

rm -vf main.o sub.o test_sub.exe  
'main.o' を削除しました  
'sub.o' を削除しました  
'test_sub.exe' を削除しました  

`rm -vf main.o sub.o test_sub.exe`が実行され，各ファイルが削除される。   

makeの後に書かれた文字列（ここではclean）のことを**ターゲット**と呼ぶ。 



```
$ date -R
```

Thu, 22 Aug 2019 13:56:37 +0900

```
$ make
```

ifort -c main.f90  
ifort -c sub.f90  
ifort -o test_sub.exe main.o sub.o  

オブジェクトファイルmain.o, sub.oと実行ファイル test_sub.exeが削除されているので，ソースファイルのコンパイルとオブジェクトファイルのリンクが行われる。



```
$ ll test_sub.exe
```

-rwxrwxr-x 1 am am 8.6K  8月 22 13:56 test_sub.exe     



```
$ date -R
```

Thu, 22 Aug 2019 13:57:33 +0900  



```
$ touch sub.f90
$ ll main.f90 sub.f90
```

-rw-rw-r-- 1 am am 122  8月 21 19:44 main.f90  
-rw-rw-r-- 1 am am  99  <u>8月 22 13:57</u> sub.f90  

  

```
$ make
```

ifort -c sub.f90   
ifort -o test_sub.exe main.o sub.o      

更新されたのはsub.f90だけなので，sub.f90だけをコンパイルする。  

main.f90のコンパイル済みなので，コンパイル結果のmain.oをそのままリンクする。  



### makefileの書式

#### makefileの例

```makefile
test_sub.exe: main.o sub.o
        ifort -o test_sub.exe main.o sub.o
main.o: main.f90
        ifort -c main.f90
sub.o: sub.f90
        ifort -c sub.f90
clean:
        rm -vf main.o sub.o test_sub.exe
```



#### 書式

日常の言葉でmakefileの書式を説明すると，次のようになる。  

```
作りたいもの: 材料
	作り方
```

- 「作りたいもの」のことをターゲットと呼ぶ。

- 「材料」のことを依存関係行と呼ぶ

- 「作り方」には通常コマンドとそのオプション，コマンドの引数が書かれる



以下ひとまとまりを**ルール**と呼ぶことがある。

```
ターゲット: 依存関係行
	コマンド（ターゲットを作成するために必要な作業）
```

**コマンドは タブ(TAB)の後に書く必要がある**。スペース（空白文字）は**不可**。  

人間にはどちらも同じ空白文字に見えるが，コンピュータの内部ではタブとスペースは区別されている。



#### ターゲット

##### makeコマンドでターゲットを指定する場合

makeコマンド実行の際に,  

```
$ make main.o
```

のように**ターゲットを指定した場合**，makeコマンドはmakefileの**該当のターゲットが記述された部分をまず参照する**。今の例では，makefileの   

```makefile
main.o: main.f90
        ifort -c main.f90
```

の部分を参照する。上の部分に従って，コマンド

```
ifort -c main.f90
```

を実行し，ターゲットとなっているmain.oを作成する。



##### makeコマンドでターゲットが指定されていない場合

makeコマンド実行時に**ターゲットが指定されていない場合**，まずmakefileの**一番上のルール**  

```makefile
test_sub.exe: main.o sub.o
        ifort -o test_sub.exe main.o sub.o
```

に従って，ターゲットtest_sub.exeの作成のために必要な作業を行う。

もし，依存関係行に記載されたmain.oやsub.oに更新が必要な場合，下記のルールに従って，main.oやsub.oを作成する。

```makefile
main.o: main.f90
        ifort -c main.f90
```

```makefile
sub.o: sub.f90
        ifort -c sub.f90
```



##### 依存関係行がないターゲット

cleanというターゲットには依存関係行がない。ターゲットとしてcleanを指定した場合，無条件にコマンドが実行される。このようなターゲットをphonyターゲットと呼ぶ。

## makefile以外の名称のファイルを使うとき

例えば，makefileではなく，mymakefileという名前のファイルを使うときは，次のようにすればよい。

```bash
$ make -f mymakefile
```

`-f` オプションを使うことで，makefile以外のファイルを用いることが出来る。

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している


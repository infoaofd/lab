#!/bin/bash

#CTL=hgt.season.1958-2005_20_-165.CTL
CTL=hgt.season.1958-2005_50_-165.CTL
GS=$(basename $0 .sh).GS

FIG=$(basename $CTL .CTL).eps

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

xmax = 1;ymax = 1

ytop=9

xwid = 5.0/xmax;ywid = 5.0/ymax

xmargin=0.5;ymargin=0.1

nmap = 1; ymap = 1; xmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1);xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1);ys = ye - ywid

# SET PAGE
'set vpage 0.0 8.5 0.0 11'
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'

'set t 1 48'

'set gxout line'; 'set cmark 0'
'd z500'


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

# HEADER
'set strsiz 0.12 0.14'
'set string 1 l 3 0'
xx = 0.2
yy=yt+0.5
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $0."
echo

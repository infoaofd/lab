#!/bin/bash

# Thu, 09 Jun 2022 22:16:53 +0900
# p5820.bio.mie-u.ac.jp
# /work03/gu1/LAB/2022_PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON

GS=$(basename $0 .sh).GS

INFLE=hgt.season.1958-2005.nc
#LON=-165;LAT=20
LON=-165;LAT=50

BIN=$(basename $INFLE .nc)_${LAT}_${LON}.BIN

rm -f $BIN

HOST=$(hostname); CWD=$(pwd); NOW=$(date -R); CMD="$0 $@"

cat << EOF > ${GS}

# Thu, 09 Jun 2022 22:16:53 +0900
# p5820.bio.mie-u.ac.jp
# /work03/gu1/LAB/2022_PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON

'sdfopen ${INFLE}'
'q ctlinfo'; say result

'set lon ${LON}'
'set lat ${LAT}'
'set t 1 48'
'set fwrite $BIN'
'set gxout fwrite'
'd z500'
'disable fwrite'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $BIN


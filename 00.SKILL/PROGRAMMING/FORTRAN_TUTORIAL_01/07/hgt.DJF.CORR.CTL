dset ^hgt.DJF.CORR.BIN
title CORRELATION 
undef -9.96921e+36
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 1 linear 0 1
tdef 1 linear 00Z01JAN1958 12mo
vars 1
R 0 99 CORR Z 500 hPa [m]
endvars

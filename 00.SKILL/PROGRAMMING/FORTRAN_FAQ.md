# FAQ（よくある質問）

[[_TOC_]]

## 質問: 詳しい解説書を探している

**回答**

- ザ・フォートラン95 (研究室の書架)
- https://www.youtube.com/@fortran6169

- https://www.ss.cc.tohoku.ac.jp/sscc/wp-content/uploads/pdf/Fortran入門解説書2021.pdf
- http://www.research.kobe-u.ac.jp/csi-viz/members/kageyama/lectures/H22_FY2010_former/ComputationalScience/2_1_f95a.html
- https://amanotk.github.io/fortran-resume-public/
- https://www.nag-j.co.jp/fortran/index.html
- https://sites.google.com/site/fcfortran/
- http://www.hysk.sakura.ne.jp/Linux_tips/how2use_F90
- http://www.jspf.or.jp/Journal/PDF_JSPF/jspf2005_05/jspf2005_05-398.pdf
- https://qiita.com/tags/fortran
- http://site.hpfpc.org/home/seminar/20161125fortran

## 質問: プログラムがどのように動作しているかわからない

**回答**

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/RECIPE/DEBUG_FACTORIAL/0.DEBUG_FACTORIAL.md

下記サービスも有益です。コツは，AIの**回答結果をよく読み**，**疑問点を絞り込み**，**質問内容を具体的にしていく**ことです。AIに丸投げにせず**質問内容を工夫する**ことが重要。

https://chatgpt.com/c/6737d1cf-7438-8000-a706-177bbb15a6c6

## 質問: プログラムの修正方法が分からない

**回答**

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/32.12.FORTRAN_DEBUG_00.md

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/RECIPE/DEBUG_FACTORIAL/0.DEBUG_FACTORIAL.md

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/RECIPE/00.BUG_SAMPLE_FORTRAN.md

## 質問: print 文の i は整数という意味か

```fortran
print '(a,i3)','i=',i
```

`i`は整数という意味なのか。

**回答**

その通りです。`i`は整数という意味です。例えば`i3`で, 整数型の変数を3桁で書き出す, という意味になります。

## 質問: print 文の (f5.1) はどういう意味か

```fortran
'(f5.1)'
```

**回答**

fは実数の意味。5.1は全体で5桁の実数で，小数点以下は1桁で書き出す。

書式指定についての詳細は，https://www.nag-j.co.jp/fortran/FI_14.html

## 質問: print 文の (a) はどういう意味か

```fortran
print '(a,f5.1)','A=',A
```

の"a"はどういう意味なのか。

**回答**

**文字型の変数を書き出すとき**に"**a**"という**書式指定**を使います。

```fortran
print '(a,f5.1)','A=',A
```

`a`: 文字型の定数'A='を書き出すときの書式指定  

`f5.1`: 実数型の変数Aに保存されている数値を書き出すための書式指定  

書式指定の詳細は下記参照  
 https://www.nag-j.co.jp/fortran/FI_14.html  

## 質問：大文字と小文字の区別

```fortran
print *,'I=',I
print '(a,i3)','i=',i
```

のiはなぜIではないのか。

**回答**

Fortranは大文字と小文字を**区別しない**ので、`I`と`i`は同じ意味になります。

例外：引用符 (`'`)または2重引用符(`"`)で囲まれた部分のみ, 大文字と小文字を区別します。



## 質問: 数値を横並びに書きたい

```fortran
print '(2f5.1)', (x(i,j),j=1,2)
```

数値を横並びに書きたいとき，print文の書式2f5.1のようにすると，2つの数字を横並びに書いてくれる。

```fortran
print '(2f5.1)', (x(i,j),j=1,2)(x(i,j),j=1,2)
```

iの値は固定させて，jを1から2まで変化させて，数値を横並びに書く



## 質問：parameter 文の用途

```fortran
integer,parameter::im=2,jm=2,km=5
```

parameterを使うと，変数を**定数**として扱うことができる。物理定数など，いったん決めた数値を不意に書き換えたくないときにつかう

## 質問: character 文の len の意味

```fortran
character(len=80)
```

**回答**

`len=80`だったら, 80文字分の文字列を宣言するという意味になります。



## 質問: open 文の使い方

**回答**

```fortran
open(20,file='OUTPUT')
```

OUTPUTという名前のファイルを20番で開く。

```fortran
filename='OUTPUT'
```

filenameという変数にOUTPUTという文字列を代入する。

```fortran
open(20,file=filename)filename
```

という変数に代入されている値（今の場合OUTPUT）を使ってファイルを開く。番号は20番とする。

ファイルを読み書きするときのファイルの区別はファイル名ではなく，番号で行う。

今20番でファイルを開いているから，このファイルに変数の値を書き出したかったら，

```fortran
write(20,'(A)')'Hello world'
```

のようにすると，OUTPUTという名前のファイルに，Hello worldという文字列が書き出される。
プログラミングの例

```fortran
open(20,file='TEST.TXT')
write(20,'(A)')'TODAY IS FRIDAY. WEEKEND. '
end
```

自分で打ち込んでコンパイルして，実行させてみる。vi test.f90として，test.f90というファイルを作成する。

プログラムが正しくされた場合，このプログラムを実行すると，TEST.TXTというファイルが作成される。ファイルに書き出した場合，プログラムを実行した際に，画面には何も表示されない。このファイルを開くと，

TODAY IS FRIDAY. WEEKEND. 

と書いてある。

## 質問: ファイルが見つかりません (No such file or directoryというエラーがでる)

**回答1**　下記の例で説明します。

```bash
$ read_z500_djf.exe 
forrtl: No such file or directory
forrtl: severe (29): file not found, unit 11, file /work01/DATA/NCEP1/12.00.MON.2.SEASON/hgt.season.1958-2005.bin
```

`/work01/DATA/NCEP1/12.00.MON.2.SEASON/hgt.season.1958-2005.bin`　というファイルが見つからないというエラーであるので，`/work01/DATA/NCEP1/12.00.MON.2.SEASON/` に `hgt.season.1958-2005.bin` というファイルがあるか確認する。

```bash
$ ls /work01/DATA/NCEP1/12.00.MON.2.SEASON/
0.README.TXT                         hgt.mon.mean.nc*
12.00.CREATE.1958-2005.DJF.Z500.ncl  hgt.season.1958-2005.BIN
```

`hgt.season.1958-2005.bin`　ではなく，`hgt.season.1958-2005.BIN` という名前のファイルがあることが，分かった。

そのため，プログラムの中で，`hgt.season.1958-2005.bin`　としている箇所を，`hgt.season.1958-2005.BIN` に変更してみる。

**回答2**　上記でファイルが見つからないとき

```bash
$ locate hgt.season.1958-2005.bin
```

を試してください。少々時間を要する場合があります。

表示内容が多すぎる場合，

```
$ locate hgt.season.1958-2005.bin > TMP.TXT
```

として，コマンドの実行が終わったら，TMP.TXTをlessコマンドなどで閲覧してください。

locateコマンドで見つからない場合，次のfindコマンドを実行してください。

```bash
$ find / -name hgt.season.1958-2005.bin -print  2>/dev/null 
```



## 質問: 移動平均の意味

![img](https://lh7-rt.googleusercontent.com/docsz/AD_4nXegK9G3_CBHLDfiyzydbJxzwalbvg9zeVmh1UMWzxaiqip6x_2ofBtFMy5myseAeZ-bhE16WTlxpEJlLf_RrawBDFtOgObjo1t1gWG0F8fcDVLO7Ubv4g26j3K70qzfRbvRAZfEFg?key=ELcYR_xerqDdkkuu8aI43Q)

## 質問: float 関数の用途

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/F_04_SP_ARRAY.md

**時間平均のところに書かれていた、組み込み関数のfloatについてですが、今回実数型から整数型を割る形となっていて、型が一致していないので型を変える必要があるということでよいのですか。**

**回答**

その通りです

## 質問: バイナリファイルと od コマンド

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/F_05_BINARY_FILE.md

上記で，**odコマンドで表示した内容は理解が必要なのですか（16進数で表されているということを理解するだけでよいのか、表示された内容の意味まで理解する必要があるのか）**。もし表示内容の理解が必要な場合は、見方を教えていただきたいです。

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/F_05_BINARY_FILE.md

**回答**

現段階では不要です。バイナリファイルがテキストファイルとは全く異なるものであることが納得できればそれでよいです

参考までに概要を記載します。

```
$ od -t f4 pgb.200608.bin # <--4バイト浮動小数点を10進数表示

0000000  2.600000e+01  2.600000e+01  2.600000e+01  2.600000e+01

0001100  2.100000e+01  2.200000e+01  2.300000e+01  2.400000e+01

0001120  2.400000e+01  2.500000e+01  2.600000e+01  2.700000e+01
```

一番左の列：メモリのアドレス

メモリとアドレスを大雑把に説明すると次のようになる。メモリとはデータを記憶しておく部品のことで，小さい小箱に分けて使用される。アドレスとは，メモリの中のどの箱かを示す番号のこと。別な例え方をすると，

> メモリ → マンション
>
> データ → 住民
>
> アドレス → マンションの部屋番号
>

という理解の仕方で当たらずしも遠からずである。

左から2番目以降の値：データ

出力結果の見方を記しておく。

- 一番左はアドレスを**8進数**で記したもの。上記の例では4バイトの数字が4つで1行なので16バイトで1行分。8進数でバイトを表せば1行20（8×2）バイトになる。

- 2列目からが数字を表す。0001100番地に21、0001104番地に22、0001110番地に23、0001114番地に24ということ。

## 質問: odコマンドの詳細について

**odコマンドの説明のリンク先のページが途中からログインが必要で読むことができないのですが、odコマンドのオプションの説明まで読んでおけばよいですか**。

https://atmarkit.itmedia.co.jp/ait/articles/1703/10/news027.html

**回答**

はい，それでよいです。

Linuxコマンドに関しては**同様の解説ページが無数にある**ので，リンク切れや見れないページに当たった場合，**似たようなページがないか検索をかける**とよいです。**類似のページがすぐにヒットします**。

https://hydrocul.github.io/wiki/commands/od.html



## 質問: ダイレクトアクセスファイルのrec

**バイナリファイルへの書きこみ（ダイレクト・アクセス）部分でrecやirecが何を表しているのか分からなかったので、教えていただきたいです**。

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/F_05_BINARY_FILE.md

**回答1**
https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/DIRECT_ACCESS.pdf

**回答2**
Fortranでは、ダイレクト・アクセス形式を使用してバイナリファイルにデータを書き込むことができます。

この形式では、ファイル内のデータをレコード単位で読み書きし、任意のレコードに直接アクセスすることが可能です。これにより、大規模データセットの操作が効率的になります。以下に基本的な例を示します。

```fortran
program bindirout

real,allocatable::a(:)
character(len=100):: ofle

nx=3
allocate(a(nx))

nt=4

ofle="random_number_bindir.bin"

isize=4*nx

open(21,file=ofle,form="unformatted",&
access="direct",recl=isize)

irec=0
do j=1,nt
call random_number(a)

irec=irec+1
write(21,rec=irec)a

print '(3f10.5)', (a(i),i=1,nx)

end do
close(21)

print *
print '(A,A)','OUTPUT FILE = ',trim(ofle)
print *

end program bindirout
```

このプログラムでは、ダイレクトアクセス（`access="direct"`）形式を用いて、乱数をバイナリファイルに書き込んでいます。

ここで登場する `irec` と `rec` は、それぞれレコード番号に関連しています。詳しく解説します。

**プログラム全体の概要**  

乱数生成 (`call random_number(a)`)  

各ステップで乱数を生成し、それを配列 `a` に格納します。  

バイナリファイルへの書き込み  

各ステップのデータ（配列 `a`）をバイナリ形式でファイルに保存します。  

ファイルへの書き込みはレコード単位で行われ、どのレコードに書き込むかを `rec` で指定します。 

**ダイレクトアクセスの特徴**  

ダイレクトアクセス形式では、ファイルをレコードの集合として扱います。これにより、任意のレコードを効率よく読み書きすることができます。  

**irec と rec の役割 ** 

`irec `   

役割: 書き込むレコード番号をカウントする変数。  

詳細:  

初期値は `irec=0`。  

ループ内で `irec=irec+1` として、レコード番号をインクリメントしています。  

各書き込み操作ごとに、どのレコードに書き込むかを管理します。  

`rec`  

役割: 実際に `write` 文で指定されるレコード番号。  

詳細:  

`write(21, rec=irec) a` のように、`irec` を `rec` として指定。  

この指定により、生成した乱数の配列 `a` が対応するレコード（`irec`番目）に書き込まれます。  

**動作の流れ**  

ファイルのオープン  

```fortran
open(21, file=ofle, form="unformatted", access="direct", recl=isize)
```

ファイル番号21で、バイナリ形式（`form="unformatted"`）、ダイレクトアクセス（`access="direct"`）としてファイルを開きます。  

レコードサイズは `isize=4*nx` バイトで、`nx=3` なら `isize=12` バイト（3つの実数値分）になります。  

ループで乱数を生成し、レコードに書き込み  

```fortran
do j=1, nt
    call random_number(a)
    irec = irec + 1
    write(21, rec=irec) a
end do
```

各ループで `random_number(a)` により、`a` にランダムな値を生成。  

`irec` を1ずつ増やし（最初のレコードは1）、書き込むレコード番号を指定。  

`write(21, rec=irec) a` によって、`a` を指定されたレコード（`irec`）にバイナリ書き込み。 




**出力ファイルの構造**  
ファイル random_number_bindir.bin には、以下のように4つのレコードが格納されます（nt=4 のため）。  
レコード1: a(1), a(2), a(3) （ループ1回目で生成された乱数）  

レコード2: a(1), a(2), a(3) （ループ2回目で生成された乱数）  

レコード3: a(1), a(2), a(3) （ループ3回目で生成された乱数）  

レコード4: a(1), a(2), a(3) （ループ4回目で生成された乱数）  

**まとめ**  

`irec`:  

現在の書き込み対象レコード番号を保持し、ループ内でインクリメントされる。  

`rec`:  `write` 文でどのレコードに書き込むかを指定する際に使われる。  



## 質問: コンパイル時の-oオプションの指定

下記の2つは同じ意味ですか？

```bash
ifort -o PROG.exe PROG.f90
```

```fortran
ifort PROG.f90 -o PROG.exe
```

**回答**

**同じ意味**です。`-o`オプションの**直後**の単語は，プログラムの実行ファイル名になります。  

## 質問: ifort の -assume byterecl オプション

下記のプログラムを IntelOneAPI でコンパイルする際に，`-assume byterecl` を付ける場合とつけない場合で結果が変わる理由を教えてください。 

```fortran
REAL(KIND=4),DIMENSION(10) :: a = 3.141
INTEGER :: reclen 

INQUIRE(iolength=reclen)a

PRINT *;PRINT *,'RECORD LENGTH OF a = ',reclen;PRINT * 

OPEN(UNIT=10,FILE='INQUIRE.TEST.BIN',FORM='UNFORMATTED',& ACCESS='DIRECT',RECL=reclen)

WRITE(UNIT=10,REC=1)a 

CLOSE(UNIT=10) 
END
```

**回答**

Intel Fortranの-assume bytereclオプションの有無によって結果が変わる理由は、**RECLの単位が異なるため**です。このオプションは、RECL（レコード長）の単位を**「バイト」**にするか「**コンパイラ既定の単位**」（通常は「**4バイト単位**」）にするかを切り替えるものです。  

**-assume byterecl** **の動作 ** 

1. **デフォルトの動作（-assume bytereclを指定しない場合）**  
   - RECL の単位は**ワード単位**（多くの場合、1ワード = 4バイト）。  
   - INQUIRE(IOLENGTH=reclen) の結果で得られる reclen は「ワード数」として計算されます。  
2. **-assume byterecl** **を指定した場合**  
   - RECL の単位が**バイト単位**になる。  
   - INQUIRE(IOLENGTH=reclen) の結果で得られる reclen は「バイト数」として計算されます。  

**プログラムにおける影響**  

プログラムで使用している重要な部分を考慮すると：  

**INQUIREによるreclenの取得**  

```fortran
INQUIRE(iolength=reclen)a
```

1. 
   - `a`（配列 `REAL(4)` 型の要素10個）のバイナリサイズが計算されます。  
   - 1要素が4バイトで、合計 10 × 4 = 40バイト。  
   - reclen の値は以下のように異なります:  
     - **-assume byterecl** **あり**: `reclen = 40` （バイト単位）。  
     - **-assume byterecl** **なし**: `reclen = 10` （4バイト単位、すなわち10ワード）。  

**ファイルのオープン**  

```FORTRAN
OPEN(UNIT=10, FILE='INQUIRE.TEST.BIN', FORM='UNFORMATTED', &
ACCESS='DIRECT', RECL=reclen)
```

1. 
   - `RECL=reclen` によって、レコードサイズが設定されます。  
   - **-assume byterecl** **を指定しない場合**:  
     - レコードサイズが10ワード（40バイト）として解釈されます。  
   - **-assume byterecl** **を指定した場合**:  
     - レコードサイズが40バイトとして解釈されます。  
   
2. **レコードサイズのずれが書き込みに影響 ** 
   - WRITE(UNIT=10, REC=1) a でレコードにデータを書き込みます。  
   
   - ファイルに対して実際に書き込むデータ量（aのサイズは40バイト）が、指定した RECL（レコード長）と一致しないとエラーが発生します。  
   
     

## 質問: program文でのプログラム名の指定

下記でエラーが出る原因はなんですか。

```fortran
program 02.1D
end program
```

**回答**

- プログラム名が**数字から始まっている**
- **ピリオドが含まれている**

ことが原因です。

下記のように直すとよいです。

```fortran
program P2_1DIM
end program
```

**ピリオド** `.` や**コンマ** `,` **は使えません**が，アンダースコア `_` は使えます。


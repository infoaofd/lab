FORTRAN TIPS (BASIC)
=====================================
[TOC]
## EDITOR

.vimrc

```bash
" ESCの代わりにjkを使う
"inoremap jk <ESC>
 inoremap <silent> jk <ESC>

" 行番号を表示
set number

" 現在の行を強調表示
set cursorline

" 括弧の反対側のハイライトを調節する
hi MatchParen ctermbg=2

" シンタックスハイライト
syntax on

" fortranの拡張子のファイルでのインデント設定
" expandtab (スペースでインデント) shiftwidth, tabstop (インデント幅指定)
" setlocal iskeyword+=; デフォルトだとiabbrevに;が使えないので追加
augroup fortran_indent_and_abbrevation
    autocmd!
    autocmd FileType fortran setlocal expandtab shiftwidth=4 tabstop=4
    autocmd FileType fortran setlocal iskeyword+=;
    autocmd FileType fortran iabbrev ;bd BLOCK DATA
    autocmd FileType fortran iabbrev ;ch CHARACTER
    autocmd FileType fortran iabbrev ;cl CLOSE
    autocmd FileType fortran iabbrev ;c CONTINUE
    autocmd FileType fortran iabbrev ;cm COMMON
    autocmd FileType fortran iabbrev ;cx COMPLEX
    autocmd FileType fortran iabbrev ;df DEFINE
    autocmd FileType fortran iabbrev ;di DIMENSION
    autocmd FileType fortran iabbrev ;dc DOUBLE COMPLEX
    autocmd FileType fortran iabbrev ;dp DOUBLE PRECISION
    autocmd FileType fortran iabbrev ;e ELSE
    autocmd FileType fortran iabbrev ;ed ENDDO
    autocmd FileType fortran iabbrev ;el ELSEIF
    autocmd FileType fortran iabbrev ;en ENDIF
    autocmd FileType fortran iabbrev ;eq EQUIVALENCE
    autocmd FileType fortran iabbrev ;ex EXTERNAL
    autocmd FileType fortran iabbrev ;ey ENTRY
    autocmd FileType fortran iabbrev ;f FORMAT
    autocmd FileType fortran iabbrev ;fa .FALSE.
    autocmd FileType fortran iabbrev ;fu FUNCTION
    autocmd FileType fortran iabbrev ;g GOTO
    autocmd FileType fortran iabbrev ;im IMPLICIT
    autocmd FileType fortran iabbrev ;inc INCLUDE
    autocmd FileType fortran iabbrev ;in INTEGER
    autocmd FileType fortran iabbrev ;int INTRINSIC
    autocmd FileType fortran iabbrev ;l LOGICAL
    autocmd FileType fortran iabbrev ;o OPEN
    autocmd FileType fortran iabbrev ;pa PARAMETER
    autocmd FileType fortran iabbrev ;pr PROGRAM
    autocmd FileType fortran iabbrev ;ps PAUSE
    autocmd FileType fortran iabbrev ;p PRINT
    autocmd FileType fortran iabbrev ;re REAL
    autocmd FileType fortran iabbrev ;r READ
    autocmd FileType fortran iabbrev ;rt RETURN
    autocmd FileType fortran iabbrev ;rw REWIND
    autocmd FileType fortran iabbrev ;s STOP
    autocmd FileType fortran iabbrev ;sa SAVE
    autocmd FileType fortran iabbrev ;st STRUCTURE
    autocmd FileType fortran iabbrev ;sc STATIC
    autocmd FileType fortran iabbrev ;su SUBROUTINE
    autocmd FileType fortran iabbrev ;tr .TRUE.
    autocmd FileType fortran iabbrev ;ty TYPE
    autocmd FileType fortran iabbrev ;w WRITE
augroup end
```



SYSTEM
------------------------------------

### カレントディレクトリの情報を得る
https://gcc.gnu.org/onlinedocs/gfortran/GETCWD.html
```fortran
PROGRAM TEST_GETCWD
  CHARACTER(LEN=255) :: CWE
  CALL GETCWD(CWD)
  WRITE(*,*) TRIM(CWD)
END PROGRAM
```



### 現在時刻を得る
https://docs.oracle.com/cd/E19957-01/805-4942/6j4m3r8t2/index.html
```fortran
INTEGER NOW(8)
CHARACTER*10 NOWC(3)

CALL DATE_AND_TIME(NOWC(1), NOWC(2), NOWC(3), NOW)

WRITE(21,'(a,i4.4,1x,i2.2,1x,i2.2,1x,i2.2,1x,i2.2)')&
'# ',NOW(1),NOW(2),NOW(3),NOW(5),NOW(6)
```



INPUT
------------------------------------



OUTPUT
------------------------------------



ALTHEMATIC
------------------------------------



MISC
------------------------------------
'open JRA3Q.CTL'
'cc'; 'set grid off';'set mpdset hires';'set map 1 1 1'
'set lon 118 160';'set lat 15 55'
TIME='06Z03APR2024';'set time 'TIME
'set gxout contour'
'set cthick 3'; 'set ccolor 1'
'set cint 4'; 'set cmin 990'; 'set cmax 1030'
'd PRMSLmsl/100'
FIG='03.CONTOUR_SLP_'TIME'.PDF'; 'gxprint 'FIG; say 'FIG: 'FIG 
'quit'

program osc0d

character(len=5000)::outfile
real::m, k, t0, te, x0, v0, dt

namelist /para/m, k,t0, te, x0, v0, dt, outfile

read(*,para)

pi = atan(1.)*4.



n=(te-t0)/dt

open(20,file=outfile)

write(20,'(A,f10.5)')'# m =',m
write(20,'(A,f10.5)')'# k =',k
write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# x0=',x0
write(20,'(A,f10.5)')'# v0=',v0
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, x, v'



omg2=k/m



! Initial condition
t=t0
x=x0
v=v0

write(20,'(f10.5,2e14.6)')t,x,v



do i=1,N
  t=t0+dt*float(i)

  !ADD YOUR OWN CODE HERE
  xnew = x + v*dt
  vnew = v - omg2*x*dt

  x=xnew
  v=vnew

  write(20,'(f10.5,2e14.6)')t, x, v

enddo !i



print *
print '(A)','# Parameters:'
print '(A,f10.5)',' # m =',m
print '(A,f10.5)',' # k =',k
print '(A,f10.5)',' # t0=',t0
print '(A,f10.5)',' # te=',te
print '(A,f10.5)',' # x0=',x0
print '(A,f10.5)',' # v0=',v0
print '(A,f10.5)',' # dt=',dt
print '(A,i10  )',' # n =',n
print *
print '(A)', 'Output file : ',trim(adjustl(outfile))
print *

end program osc0d

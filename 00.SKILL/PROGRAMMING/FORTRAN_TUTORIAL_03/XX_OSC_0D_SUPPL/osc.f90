!----------------------------------------------------------------------
! Harmonic Oscillator
!
!    dx/dt = v; dv/dt = -k*x
!
! Euler method
!
!
!----------------------------------------------------------------------
program main
  real dt
  real k
  character ofname*100

!   Set parameters
    pi = atan(1.)*4.

    k = 1.
    dt=0.01

    n=int(2.0*pi/sqrt(k)/dt + 1.0)
    write(*,*)'Number of the time steps,n = ',n

    t0 = 0.0
    x0 = 1.0
    v0 = 0.0

!   Initial Condition
    t = t0
    x = x0
    v = v0
    xtrue = x0

    ofname='result.txt'
    write(*,*)'Output file: ',ofname
    open(20,file=ofname)
    write(20,'(A)')'# t,x,v,xtrue'
    write(20,'(10f10.6)')t,x,v,xtrue

    do i=1, n
      t=t0+dt*float(i)

!     Numerical Solution
      xnew = x + v*dt
      vnew = v - k*x*dt

!     True solution
      xtrue=x0*cos(sqrt(k)*t)

!     Output
      write(20,'(10f10.6)')t,xnew,vnew,xtrue

!     Update
      x=xnew
      v=vnew

   enddo !i

   close(20)

end program main
      

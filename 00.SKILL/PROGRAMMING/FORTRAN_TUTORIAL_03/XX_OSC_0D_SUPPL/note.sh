# Print time, current working directory and output filename

export LANG=C

currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
#comment=" "
time1=$(ls -l $in | awk '{print $6, $7, $8}')
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-0} << EOF -O >> $out
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM Input: ${in} (${time1})
0 0.1   9 0 1 LM ${comment:-""}
EOF
#  Output: ${out}

#!/bin/bash
#
# Description:
#
src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"


# SET PARAMETERS
m=0.1
k=0.1
t0=0.0
te=30.0
x0=1.0
v0=0.0
dt=0.05
outfile=$(basename $0 .run.sh)_result.txt



# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then
echo
echo ERROR in $0 : No such file, $src
echo
exit 1
fi

echo
ls -lh $src
echo



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
m=${m},
k=${k},
t0=${t0},
te=${te},
x0=${x0},
v0=${v0},
dt=${dt},
outfile="${outfile}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then
echo
echo Compile error!
echo
echo Terminated.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh $exe
echo



# RUN THE PROGRAM
echo
echo $exe is running ...
echo
$exe < $nml
if [ $? -ne 0 ]; then
echo
echo ERROR in $exe: Runtime error!
echo
echo Terminated.
echo
exit 1
fi
echo
echo "Done ${exe}."
echo

#!/bin/bash
# Description:
#
# Author: am
#
# Host: localhost
# Directory: /work2/am/TEACHING/Numerical.Modelling/01.damp0d
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 09:58 on 09-28-2017.

echo "Bash script $0 starts."

gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 18
gmtset TICK_PEN 4

range=0/10/0/1.5
size=X4/4
xanot=a5f1
yanot=a0.5f0.1
anot=${xanot}:"t":/${yanot}:"x":WSne

in=$1
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi


out=${figdir}$(basename $in .txt).ps



awk '{if ($1!="#") print $1,$2}' $in |
psxy -R$range -J$size -B$anot -W3 -X1.5 -Y3 -P -K >$out




# PRINT HEADER
export LANG=C
xoffset=
yoffset=5
comment=

currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
#comment=" "
time1=$(ls -l $in | awk '{print $6, $7, $8}')
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-0} << EOF -O >> $out
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM Input: ${in} (${time1})
0 0.1   9 0 1 LM ${comment:-""}
EOF
#  Output: ${out}



echo
echo Input : $in
echo Output : $out
echo



echo "Done $0"

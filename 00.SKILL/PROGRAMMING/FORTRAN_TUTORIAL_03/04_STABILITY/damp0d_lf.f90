!
! Numerically solve ODE,
!
!  dx/dt=-ct
!

program damp0d_lf


character (len=5000)::outfile
real::t, e,re,absre
real::v0,t0
real::vold, v, vnew



namelist /para/c, t0, te, v0, dt, outfile

read(*,nml=para)



t=t0
v=v0
n=int(te-t0)/dt

dt2=dt*2.0



open(20,file=outfile)

write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# c =',c
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, v'
write(20,'(f10.5,e14.6)')t,v



i=1
t=dt*float(i)

vnew = v - v*c*dt
vold = v
v=vnew

write(20,'(f10.5,e14.6)')t,v



do i=2, n-1

t=dt*float(i)

vnew = vold - v*2.0*c*dt2

vold=v
v=vnew

write(20,'(f10.5,e14.6)')t,v

end do



print *
print '(A)','# Parameters:'
print '(A,f10.5)', '# t0=',t0
print '(A,f10.5)', '# te=',te
print '(A,f10.5)', '# c =', c
print '(A,f10.5)', '# dt=',dt
print '(A,i10  )', '# n =',n
print *
print '(A)', 'Output file : ',trim(adjustl(outfile))
print *



stop

end program damp0d_lf


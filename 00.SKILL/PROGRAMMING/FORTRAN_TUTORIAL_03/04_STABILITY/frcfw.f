************************************************************************
* frcfw.f
* TASK
cj FORWARD SCHEMEを使って、
cj  dx/dt=-ct
cj を解く。
************************************************************************
*------------------------- PARAMETERS ----------------------------------
      parameter(n=30)
      dimension t(n),v(n),e(n),re(n),abre(n)
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
*-----------------------------------------------------------------------
      write(*,*) 'dt'
      read (*,*) dt
      
      c = 1.0

cj 初期条件
      v(1) = 1.0
      t(1) = 0.0
      e(1)=0.0 ! ERROR
      re(1)=0.0
      abre(1)=0.0

cj 最初のステップの計算
      v(2)=v(1)-v(1)*c*dt ! FORWARD SCHEME
      t(2)=float(1)*dt
      e(2)=v(2)-v(1)*exp(-c*t(2))
      re(2)=e(2)/(v(1)*exp(-c*t(2)))
      abre(2)=abs(re(2))


cj 数値積分
      do i=2, n-1
        v(i+1) = v(i-1) - v(i-1)*2.0*c*dt !FORWARD SCHEME
        t(i+1) = dt*float(i)
        e(i+1)=v(i+1)-v(1)*exp(-c*t(i+1)) !誤差
        re(i+1) = e(i+1)/(v(1)*exp(-c*t(i+1))) !相対誤差
        abre(i+1) = abs(re(i+1)) !絶対値をとる
      end do

      stop
      end


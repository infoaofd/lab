import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "osc0d_LF_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
        continue
    values = line.split()
    if len(values) == 5:
        t, x, u, xa, ua = map(float, values)
        data.append((t, x, u, xa, ua))

# NumPy 配列に変換
data = np.array(data)

# 用紙を縦向きに設定
fig, axs = plt.subplots(1, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# プロット
axs.plot(data[:, 1], data[:, 2], linestyle="-", label="Analytical", color="red")
axs.plot(data[:, 3], data[:, 4], linestyle="-", label="Numerical", color="blue")
axs.set_xlabel("x")
axs.set_ylabel("u")
axs.set_title("Phase diagram")
axs.set_aspect('equal')
axs.legend()

# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + "_PHASE.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

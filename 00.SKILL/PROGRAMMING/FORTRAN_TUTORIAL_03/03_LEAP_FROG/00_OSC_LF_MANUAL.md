# 第3回 リープフロッグ・スキーム

[[_TOC_]]

## 前回の復習

前回は，
$$
\begin{eqnarray}
dx/dt &=& u  \tag{1}　\\
\\
m\frac{du}{dt}&=&-kx \tag{2}\\
\end{eqnarray}
$$
という方程式の解析解と数値解を求めた。

式(1), (2)の数値解を求めるために，前回は式(1)，(2)を以下に示すような前進差分で近似した。
$$
\begin{eqnarray}
\frac{x(t+\Delta t)-x(t)}{\Delta t}&=&u(t) \tag{3} \\
\\
\frac{u(t+\Delta t)-u(t)}{\Delta t}&=&-\frac{k}{m}\,x(t) \tag{4} \\
\end{eqnarray}
$$
$m=k=1.0$，$\Delta t=0.05$ として数値解を求めてみると，時間の経過とともに ,  $x$ と $u$ の変動の振幅が大きくなってしまう（解析解では振幅一定となる）。 $\Delta t$ を小さくすれば計算誤差は減少するが，そうすると計算量は増大するため，長時間の計算が必要となる。

計算量はそのままで( $\Delta t$ の値を小さくしないで)，より精度の良い計算をするためにはどうしたらよいだろうか。

## 高精度の差分式

以下に示す差分式は，(3)，(4) 式の前進差分よりも精度が良い。
$$
\begin{eqnarray}
\frac{x(t+\Delta t)-x(t-\Delta t)}{2\Delta t}&=&u(t) \tag{5} \\
\\
\frac{u(t+\Delta t)-u(t-\Delta t)}{2\Delta t}&=&-\frac{k}{m}\,x(t) \tag{6} \\
\end{eqnarray}
$$


式(5), (6)のような差分の仕方をleapfrog scheme（中点蛙飛び法）という。Leapfrog schemeは今後頻繁に使用される。

式(5), (6)は,前進差分の式 (3)，(4)の時間に関する刻み幅 ($\Delta t$) を2倍にしただけのように見えるかも知れないが，実際には前進差分よりもずっと精度の良い差分式となっている。そのことを次節で見る。

## 差分式の精度

前進差分の誤差がどのくらいなのか調べてみよう。$x(t+\Delta t)$ を $x(t)$ の近傍でテイラー展開すると以下のようになる。
$$
x(t+\Delta t)=x(t)+x'(t)\Delta t+\frac{1}{2}x''(t)(\Delta t)^2+
+\frac{1}{6}x'''(t)(\Delta t)^3 \cdots \tag{7}
$$
式 (7) を式 (3) の左辺に代入すると，
$$
\begin{eqnarray}
&&\frac{x(t+\Delta t)-x(t)}{\Delta t}\\
&=& \frac{1}{\Delta t}\bigg[\bigg\{ 
x(t)+x'(t)\Delta t+\frac{1}{2}x''(t)(\Delta t)^2+\cdots\bigg\}
-x(t)\bigg]\\ \\
&=&x'(t)+\frac{1}{2}x''(t)(\Delta t)
+\frac{1}{6}x'''(t)(\Delta t)^2+\cdots \tag{8}
\end{eqnarray}
$$
式(8)は式(3)の左辺と比べて,
$$
\frac{1}{2}x''(t)(\Delta t)
+\frac{1}{6}x'''(t)(\Delta t)^2+\cdots \tag{9}
$$
の項が付加されている。$dx/dt$ を正しく表現するためには，この付加項を加える必要がある。(3) 式の左辺で $dx/dt$ を近似した場合，(9) で表される誤差が生じることになる。

$\Delta t$ は微小量であるため，通常 $\Delta t$ のべき乗は $\Delta t$ よりも小さくなる。したがって，式(9)で一番大きい項は第1項になる。このことを「式(3)の誤差は $\Delta t$ のオーダーである」とか，「式(3)の精度は( $\Delta t$ に関して) 1次のオーダーである」というふうに表現する。

## 練習

式(7)～(9) と同様の方法で，リープフロッグ・スキームの誤差の大きさを評価せよ。

## 演習

### 演習1

$x(t=0)=1$，$u(t=0)=0$ のとき，leapfrog schemeを使って，(1)，(2)の $t=0$ から$ t=3\Delta t$ までの数値解 の求め方を説明せよ。

ただし，leapfrog schemeは $t-\Delta t$，$t$，$t+\Delta t$ という3つの時刻の値が必要となるため，最初の計算ステップには適用できない。そこで，最初の計算ステップでは(3), (4)の前進差分を用いて計算し, 2番目以降の計算ステップでは, 式(5)，(6)を用いて計算を行え。

### 演習2

$m=k=1$ とする。$x(t=0)=1$，$u(t=0)=0$ のとき，leapfrog schemeを使って，(1)，(2)を解け。ただし，計算の一番最初のステップでは (3)，(4)の前進差分を用いて計算し，2番目以降の計算ステップでは, 式(5)，(6)を用いて計算を行え。時間刻みは $\Delta t=0.05$ とせよ。計算終了時刻 $t_E$ は $30$ とせよ。

下記を使用してよい

- Fortranプログラム: osc0d_LF.f90
- コンパイル・実行用スクリプト: osc0d_LF.run.sh

### 演習3

以下に示す図を作図せよ。

(i) 横軸に$t$,　縦軸に$x$, $u$ をとったグラフ（時系列と呼ぶ）。解析解と数値解を比較せよ。

末尾に記載したpythonスクリプト (osc0d_LF_PLT.py) を使用してよい。

(ii)横軸に$x$，縦軸に$u$ をとったグラフ（相図と呼ばれることがある）。

末尾に記載したpythonスクリプト (osc0d_Phase_PLT.py) を使用してよい。

[4] 使用したプログラム，スクリプトを全て読む。不明の点があれば，下記の手法で調べる。

- print文デバッグを行う (https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md)
- AIに分析させる

[5] プログラムの各文の後にコメント文を入れることで，プログラムの下記の部分の内容（何を行っているのか）を解説せよ。

```fortran
! EULER FORWARD SCHEME
unew=0.0
t=t0+dt*float(1)
unew = u - omg2*x*dt  !TIME INTEGRATION OF u
xnew = x + u*dt       !TIME INTEGRATION OF x
xold=x; uold=u        !UPDATE
x=xnew; u=unew        !UPDATE
```

```FORTRAN
! LEAP FROG SCHEME
dt2=2.0*dt
xnew=0.0; unew=0.0
do i=1,N
  t=t0+dt*float(i)

  unew = uold - omg2*x*dt2  !TIME INTEGRATION OF u
  xnew = xold + u*dt2       !TIME INTEGRATION OF x
  xold=x; uold=u        !UPDATE
  x=xnew; u=unew        !UPDATE
  write(20,'(f10.5,4e14.6)')t, x, u, xa, ua

enddo !i
```

**AIの分析結果のコピーはしない**。AIの分析結果を踏まえ，print文デバッグを使って**自分で内容を確認する**。

print文デバッグについては，

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md

を参照のこと。

自分の頭で理解したことを，自分の言葉で説明する。

### 実行例

コンパイル・実行

> $ osc0d_LF_.run.sh

作図

> $ python3 osc0d_PLT.py

下記のエラーが出たときは，

Traceback (most recent call last):
 import matplotlib.pyplot as plt
ModuleNotFoundError: **No module named** '**matplotlib**'

次のコマンドを実行した後，もう一度描画用スクリプトを実行する。

> $ conda deactivate



### プログラム例

#### osc0d_LF.f90

```Fortran
program osc0d

character(len=5000)::OUT
real::m, k, t0, te, x0, u0, dt

namelist /para/m, k, te, x0, u0, dt, OUT

read(*,nml=para)
t0=0.0

n=(te-t0)/dt

open(20,file=OUT)
write(20,'(A,f10.5)')'# m =',m
write(20,'(A,f10.5)')'# k =',k
write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# x0=',x0
write(20,'(A,f10.5)')'# u0=',u0
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, x, u, xa, ua'

! ANGULAR VELOCITY SQUARED
omg2=k/m
omg=sqrt(omg2)

! INITIAL CONDITION
t=t0; x=x0; u=u0; xa=x0; ua=u0
write(20,'(f10.5,4e14.6)')t,x,u, xa,ua

! EULER FORWARD SCHEME
unew=0.0
t=t0+dt*float(1)
unew = u - omg2*x*dt  !TIME INTEGRATION OF u
xnew = x + u*dt       !TIME INTEGRATION OF x
xold=x; uold=u        !UPDATE
x=xnew; u=unew        !UPDATE

! ANALYTICAL SOLUTION
xa=x0*cos(omg*t)+u0/omg*sin(omg*t)
ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

! LEAP FROG SCHEME
dt2=2.0*dt
xnew=0.0; unew=0.0
do i=1,N
  t=t0+dt*float(i)

  unew = uold - omg2*x*dt2  !TIME INTEGRATION OF u
  xnew = xold + u*dt2       !TIME INTEGRATION OF x
  xold=x; uold=u        !UPDATE
  x=xnew; u=unew        !UPDATE

! ANALYTICAL SOLUTION
  xa=x0*cos(omg*t)+u0/omg*sin(omg*t)
  ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

  write(20,'(f10.5,4e14.6)')t, x, u, xa, ua

enddo !i

print *; print '(A)', 'Output file : ',trim(OUT); print *

end program osc0d

```

 

### 実行用スクリプト

#### osc0d_LF.run.sh

```bash
#!/bin/bash

src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05
OUT=$(basename $0 .run.sh)_result.txt



# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then echo ERROR in $0 : No such file, $src; exit 1; fi



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then echo Compile error!; echo Terminated.; exit 1; fi
echo Done.

# RUN THE PROGRAM
echo $exe is running ...
$exe < $nml
if [ $? -ne 0 ]; then echo ERROR in $exe: Runtime error!; echo; echo Terminated.; exit 1; fi
echo Done.
```



### 描画用スクリプト

#### osc0d_LF_PLT.py

解の**時系列**を書く

```python
import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "osc0d_LF_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
        continue
    values = line.split()
    if len(values) == 5:
        t, x, u, xa, ua = map(float, values)
        data.append((t, x, u, xa, ua))

# NumPy 配列に変換
data = np.array(data)

# 用紙を縦向きに設定
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# プロット
axs[0].plot(data[:, 0], data[:, 3], linestyle="-", label="Analytical", color="red")
axs[0].plot(data[:, 0], data[:, 1], linestyle="-", label="Numerical", color="blue")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x")
axs[0].set_title("Time Series of x")
axs[0].legend()

axs[1].plot(data[:, 0], data[:, 4], linestyle="-", label="Analytical", color="red")
axs[1].plot(data[:, 0], data[:, 2], linestyle="-", label="Numerical", color="blue")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u")
axs[1].set_title("Time Series of u")
axs[1].legend()
# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + ".pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

```

#### osc0d_Phase_PLT.py

解の**相図**を書く

```python
import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "osc0d_LF_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
        continue
    values = line.split()
    if len(values) == 5:
        t, x, u, xa, ua = map(float, values)
        data.append((t, x, u, xa, ua))

# NumPy 配列に変換
data = np.array(data)

# 用紙を縦向きに設定
fig, axs = plt.subplots(1, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# プロット
axs.plot(data[:, 1], data[:, 2], linestyle="-", label="Analytical", color="red")
axs.plot(data[:, 3], data[:, 4], linestyle="-", label="Numerical", color="blue")
axs.set_xlabel("x")
axs.set_ylabel("u")
axs.set_title("Phase diagram")
axs.set_aspect('equal')
axs.legend()

# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + "_PHASE.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")
```



## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

#!/bin/bash

src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05
OUT=$(basename $0 .run.sh)_result.txt



# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then echo ERROR in $0 : No such file, $src; exit 1; fi



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then echo Compile error!; echo Terminated.; exit 1; fi
echo Done.

# RUN THE PROGRAM
echo $exe is running ...
$exe < $nml
if [ $? -ne 0 ]; then echo ERROR in $exe: Runtime error!; echo; echo Terminated.; exit 1; fi
echo Done.

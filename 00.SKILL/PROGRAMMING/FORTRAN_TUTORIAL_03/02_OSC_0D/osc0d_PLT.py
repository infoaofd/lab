import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "osc0d_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
        continue
    values = line.split()
    if len(values) == 5:
        t, x, u, xa, ua = map(float, values)
        data.append((t, x, u, xa, ua))

# NumPy 配列に変換
data = np.array(data)

# 用紙を縦向きに設定
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# プロット
axs[0].plot(data[:, 0], data[:, 3], linestyle="-", label="Analytical", color="red")
axs[0].plot(data[:, 0], data[:, 1], linestyle="-", label="Numerical", color="blue")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x")
axs[0].set_title("Time Series of x")
axs[0].legend()

axs[1].plot(data[:, 0], data[:, 4], linestyle="-", label="Analytical", color="red")
axs[1].plot(data[:, 0], data[:, 2], linestyle="-", label="Numerical", color="blue")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u")
axs[1].set_title("Time Series of u")
axs[1].legend()
# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + ".pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

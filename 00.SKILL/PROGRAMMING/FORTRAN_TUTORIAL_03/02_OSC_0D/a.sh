#!/bin/bash
# Description:
#
# Author: am
#
# Host: localhost
# Directory: /work2/am/TEACHING/Numerical.Modelling/Elementary/02.Osc0d
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 10:18 on 09-29-2017.

. ./gmtpar.sh
echo "Bash script $0 starts."

range=
size=
xanot=
yanot=
anot=${xanot}/${yanot}WSne

in=""
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir="../fig/"
if [ ! -d ${figdir} ];then
  mkdir -p $figdir
fi
out=${figdir}$(basename $in .asc).ps

echo Input : $in
echo Output : $out



xoffset=
yoffset=
comment=
. ./note.sh

echo "Done $0"

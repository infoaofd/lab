program osc0d

character(len=5000)::OUT
real::m, k, t0, te, x0, u0, dt

namelist /para/m, k, te, x0, u0, dt, OUT

read(*,nml=para)
t0=0.0

n=(te-t0)/dt

open(20,file=OUT)
write(20,'(A,f10.5)')'# m =',m
write(20,'(A,f10.5)')'# k =',k
write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# x0=',x0
write(20,'(A,f10.5)')'# u0=',u0
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, x, u, xa, ua'

! ANGULAR VELOCITY SQUARED
omg2=k/m
omg=sqrt(omg2)

! INITIAL CONDITION
t=t0; x=x0; u=u0; xa=x0; ua=u0
write(20,'(f10.5,4e14.6)')t,x,u, xa,ua

! TIME INTEGRATION
unew=0.0
do i=1,N
  t=t0+dt*float(i)

  unew = u - omg2*x*dt  !TIME INTEGRATION OF u
  xnew = x + u*dt       !TIME INTEGRATION OF x
  x=xnew; u=unew        !UPDATE

  xa=x0*cos(omg*t)+u0/omg*sin(omg*t)
  ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

  write(20,'(f10.5,4e14.6)')t, x, u, xa, ua

enddo !i

print *; print '(A)', 'Output file : ',trim(OUT); print *

end program osc0d

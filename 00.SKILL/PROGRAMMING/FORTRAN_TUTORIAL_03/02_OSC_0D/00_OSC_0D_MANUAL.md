# 第2回 振動方程式

[[_TOC_]]

## 方程式の導出

今回は**振動現象**を表現するもっとも簡単な形の方程式を考える。

振動現象に関する知識は，波動現象を理解する上での基礎となるので，今回の内容は，気象における波，光（=電磁波）などの性質を理解するうえでも非常に役に立つ。

いま，ある物体がバネにつながっている状況を考える。我々は経験的に，バネを引っ張った際に，バネの伸びが大きければ大きいほどバネの復元力が大きくなるのを知っている。

これを数学的に表現したのがフックの法則である。フックの法則は，バネの伸びを $x$ としたとき，
$$
\text{バネの復元力} = -kx \tag{1}
$$
と表される。右辺の負号は，バネの伸びる方向とは逆向きにバネを引き戻そうとする力が働くことを意味している。ここで，$k$ はバネ定数と呼ばれる定数である。

今，物体が元居た位置から $x$ だけ動いたとする。このことを「物体の変位は $x$ である」と言う。今，バネは物体とつながれているので，物体の変位が $x$ であるときバネは $x$ だけ伸びることになる。

物体の速度 $u$ は，物体の変位 $x$ の時間微分（=微小時間における $x$ の時間変化率)として表される。すなわち，
$$
u= dx/dt \tag{2}
$$
である。また，物体の加速度 $a$ (=微小時間内での $u$ の時間変化率) と定義されるので，
$$
a=du/dt \tag{3}
$$
である。式 (2) と (3) より，
$$
a=\frac{d}{dt}\bigg(\frac{dx}{dt}\bigg)=\frac{d^2x}{dt^2}				\tag{4}
$$
となる。

今，物体の質量を $m$ とする。物体に働く力はバネの復元力だけだと仮定すると，ニュートンの運動の第2法則より，
$$
m\frac{du}{dt}=-kx \tag{5}
$$
という方程式が得られる。式(5)を，ここでは便宜的に**振動方程式**と称する。

## 式の意味

(5) 式の左辺の $dv/dt$ は加速度 (速度の時間変化率) であるので，変位 $x$ の大きさが大きければ大きいほど，速度変化が大きくなることが分かる。

(5) 式の右辺に負号がついていることから，物体は常に変位と逆向きの方向に加速される。そのため，右辺の $-kx$ のことを**復元力**と呼ぶことがある。

(2) と (5) より，
$$
m\frac{d^2x}{dt^2}=-kx \tag{6}
$$
である。(6) を注意深く見ると，$x$ を $t$ で2回微分すると，元の関数と同じ形で負号がつく関数を探せばよいことが分かる。

いま，$\omega$ を定数として，
$$
\frac{d^2}{dt^2}\cos (\omega t)=-\omega^2\cos (\omega t) \\
\frac{d^2}{dt^2}\sin (\omega t)=-\omega^2\sin (\omega t)
$$
であることを思い出すと，三角関数が (6) を満たす解となりそうである。

### 練習

[1] まず，
$$
x=\cos (\omega t) \tag{7}
$$
と仮定して，この式を (6) に代入することにより，$\omega$ の値を決定せよ。

[2] つぎに，$c_1$，$c_2$ を定数として，
$$
x=c_1\cos(\omega t)+c_2\sin(\omega t) \tag{8}
$$
も (5) を満たすことを示せ。(7) は (8) で$c_2=0$ と場合であるので，(8) の特殊なケースと見なせる。

[3] 初期条件を，$t=0$ のとき, $x=x_0$，$u=u_0$ とする。 このときの，(6) の解が
$$
\begin{eqnarray}
x&=&x_0\cos(\omega t)+ \frac{u_0}{\omega} \sin(\omega t) \tag{9}\\
\\
u&=&u_0\cos(\omega t)-x_0\omega\sin(\omega t) \tag{10}
\end{eqnarray}
$$
[4] $\omega=1$，$x_0=1$, $u_0=0$ とする。上記解析解 (9) と (10) に関する下記のグラフの概形を示せ（手書きでもよい）。

- 横軸を $t$, 縦軸を $x$ としたグラフ
- 横軸を $t$, 縦軸を $u$ としたグラフ 

## 数値解法

(6) の数値解を求める場合，次のようにする。(6) は (2) と (5) を組み合わせた式と見なせるので，(2) と (5) を差分近似した次の式を連立させた式を解く。
$$
\begin{eqnarray}
\frac{x(t+\Delta t)-x(t)}{\Delta t}&=&u(t) \tag{11} \\
\\
\frac{u(t+\Delta t)-u(t)}{\Delta t}&=&-\frac{k}{m}\,x(t) \tag{12} \\
\end{eqnarray}
$$
(11) と (12) の左辺の分母を払うと，
$$
\begin{eqnarray}
x(t+\Delta t)&=&x(t)+u(t)\,\Delta t \tag{13} \\
\\
u(t+\Delta t)&=&u(t)-\frac{k}{m}x(t)\,\Delta t \tag{14}
\end{eqnarray}
$$
(13)，(14) を初期条件，
$$
\begin{eqnarray}
x(t=0)&=&x_0　\tag{15}　\\
\\
u(t=0)&=&u_0　\tag{16}
\end{eqnarray}
$$
のもとで解く。

### 解き方

1. パラメータ $m$ と $k$，計算終了時刻 $t_E$, 時間刻みの値を $\Delta t$ の値を設定する

2. 初期条件(15)，(16) の値を設定する

3. 計算終了時刻と $\Delta t$ の値から, 計算回数 $n$ を求める。

4. $t=0$に おける初期条件の値 $x_0$ と $u_0$ の値をファイルに出力する

5. $t+\Delta t$ の値を求める

6. (11)を使って, $x(t+\Delta t)$ の値を求める。

7. (12)を使って, $u(t+\Delta t)$ の値を求める。

8. $t+\Delta t$ と  $x(t+\Delta t)$ ，$u(t+\Delta t)$ の値を出力する

9. $x(t+\Delta t)$ の値を $x(t)$ の値に，$u(t+\Delta t)$ の値を $u(t)$ の値に置き換える（アップデートと呼ばれることがある)

10. $n$ 回 6から9までを繰り返す

## 演習

[1] 末尾に添付したプログラム (osc0d.f90) を自分で入力し, 実行用シェルスクリプト (osc0d.run.sh) を使って，コンパイル・実行する。

### 実行例

> $ osc0d.run.sh

[2] 横軸を $t$ , 縦軸を $x$ としたグラフと、横軸を $t$ , 縦軸を $u$ としたグラフと結果を作図する。下記のpythonスクリプト (python3 damp0d_PLT.py) を使ってもよい。

> $ python3 osc0d_PLT.py

下記のエラーが出たときは，

Traceback (most recent call last):
 import matplotlib.pyplot as plt
ModuleNotFoundError: **No module named** '**matplotlib**'

次のコマンドを実行した後，もう一度描画用スクリプトを実行する。

> $ conda deactivate

[3] dt の値を dt =0.5に変更して, [1] から [3] を繰り返せ。結果について説明せよ。

[4] 使用したプログラム，スクリプトを全て読む。不明の点があれば，下記の手法で調べる。

- print文デバッグを行う (https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md)
- AIに分析させる

[5] プログラムの各文の後にコメント文を入れることで，プログラムの下記の部分の内容（何を行っているのか）を解説せよ。

```fortran
t=t0; u=u0

write(20,'(f10.5,2e14.6)') t,u0,u0

do i=1,N
  t=dt*float(i)
  unew=u-c*u*dt                    !Time integration
  u=unew                           !Update
  ua=u0*exp(-c*t)                  !Analytical solution
  write(20,'(f10.5,2e14.6)')t,u,ua !Output solutions
enddo !i
```

**AIの分析結果のコピーはしない**。AIの分析結果を踏まえ，print文デバッグを使って**自分で内容を確認する**。

print文デバッグについては，

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md

を参照のこと。

自分の頭で理解したことを，自分の言葉で説明する。

### プログラム例

#### osc0d.f90

```Fortran
program osc0d

character(len=5000)::OUT
real::m, k, t0, te, x0, u0, dt

namelist /para/m, k, te, x0, u0, dt, OUT

read(*,nml=para)
t0=0.0

n=(te-t0)/dt

open(20,file=OUT)
write(20,'(A,f10.5)')'# m =',m
write(20,'(A,f10.5)')'# k =',k
write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# x0=',x0
write(20,'(A,f10.5)')'# u0=',u0
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, x, u, xa, ua'

! ANGULAR VELOCITY SQUARED
omg2=k/m
omg=sqrt(omg2)

! INITIAL CONDITION
t=t0; x=x0; u=u0; xa=x0; ua=u0
write(20,'(f10.5,4e14.6)')t,x,u, xa,ua

! TIME INTEGRATION
unew=0.0
do i=1,N
  t=t0+dt*float(i)

  unew = u - omg2*x*dt  !TIME INTEGRATION OF u
  xnew = x + u*dt       !TIME INTEGRATION OF x
  x=xnew; u=unew        !UPDATE

  xa=x0*cos(omg*t)+u0/omg*sin(omg*t)
  ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

  write(20,'(f10.5,4e14.6)')t, x, u, xa, ua

enddo !i

print *; print '(A)', 'Output file : ',trim(OUT); print *

end program osc0d
```

 

### 実行用スクリプト

#### osc0d.run.sh

```bash
#!/bin/bash

src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05
OUT=$(basename $0 .run.sh)_result.txt



# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then echo ERROR in $0 : No such file, $src; exit 1; fi



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then echo Compile error!; echo Terminated.; exit 1; fi
echo Done.

# RUN THE PROGRAM
echo $exe is running ...
$exe < $nml
if [ $? -ne 0 ]; then echo ERROR in $exe: Runtime error!; echo; echo Terminated.; exit 1; fi
echo Done.
```



### 描画用スクリプト

#### osc0d_PLT.py

解の時系列を書く

```python
import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "osc0d_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
        continue
    values = line.split()
    if len(values) == 5:
        t, x, u, xa, ua = map(float, values)
        data.append((t, x, u, xa, ua))

# NumPy 配列に変換
data = np.array(data)

# 用紙を縦向きに設定
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# プロット
axs[0].plot(data[:, 0], data[:, 3], linestyle="-", label="Analytical", color="red")
axs[0].plot(data[:, 0], data[:, 1], linestyle="-", label="Numerical", color="blue")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x")
axs[0].set_title("Time Series of x")
axs[0].legend()

axs[1].plot(data[:, 0], data[:, 4], linestyle="-", label="Analytical", color="red")
axs[1].plot(data[:, 0], data[:, 2], linestyle="-", label="Numerical", color="blue")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u")
axs[1].set_title("Time Series of x")
axs[1].legend()
# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + ".pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")
```

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

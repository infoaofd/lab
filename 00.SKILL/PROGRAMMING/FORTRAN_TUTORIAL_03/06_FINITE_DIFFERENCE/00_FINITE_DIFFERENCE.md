# 色々な差分式

次回以降で学ぶ偏微分方程式の数値解を求める際に使用される，空間座標の微分係数を差分近似する方法について学ぶ。

[[_TOC_]]

## 空間1次元

ここでは，$u(x,t)$ の場合を考える。
$$
u(x-\Delta x,t)=u_{i-1},\quad u(x,t)=u_i,\quad u(x+\Delta x,t)=u_{i+1},\quad
$$
と略記し，Fortranの表記法で，それぞれ

```fortran
u(i-1), u(i), u(i+1)
```

と書くことにする。

### 一階微分, $\frac{\partial u}{\partial x}$

#### 上流差分 (風上差分)

$$
\frac{\partial u}{\partial x}\simeq \frac{1}{\Delta x}
\bigg(u(x+\Delta x,t)-u(x,t)\bigg)
$$

```Fortran
dudx=1.0/dx*(u(i+1)-u(i))
```

#### 中央差分

$$
\frac{\partial u}{\partial x}\simeq \frac{1}{2\Delta x}
\bigg(u(x+\Delta x,t)-u(x-\Delta x,t)\bigg)
$$

```Fortran
dx2=1.0/(2.0*dx)
dudx=dx2*(u(i+1)-u(i-1))
```

### 二階微分, $\frac{\partial^2 u}{\partial x^2}$

$$
\frac{\partial^2 u}{\partial x^2}&\simeq& \frac{1}{\Delta x}\bigg[
\bigg(\frac{\partial u}{\partial x}\bigg)_{i+1}-
\bigg(\frac{\partial u}{\partial x}\bigg)_{i}
\bigg]\\
&\simeq&
\frac{1}{\Delta x}
\bigg[\bigg(\frac{u_{i+1}-u_{i}}{\Delta x}\bigg)
-\bigg(\frac{u_i-u_{i-1}}{\Delta x}\bigg)
\bigg]\\
&=&\frac{1}{\Delta x^2}(u_{i-1}-2u_i+u_{i+1})
$$

```fortran
dxs=1.0/(dx*dx)
du2dx=dxs*(u(i+1)-2*u(i)+u(i-1))
```



## 空間2次元

基本的には上の場合と同様であるが，$u(x,y,t)$ の場合の差分近似式を下記に記載する。
$$
u(x-\Delta x,y+\Delta y,t)&=&u_{i-1,\,j+1},\\ \\
u(x,y+\Delta y,t)&=&u_{i,\,j+1},\\ \\
u(x+\Delta x,y+\Delta y,t)&=&u_{i+1,\,j+1} \\ \\
u(x-\Delta x,y,t)&=&u_{i-1,\,j}, \\ \\
u(x,y,t)&=&u_{i,\,j}, \\ \\
u(x+\Delta x,y,t)&=&u_{i+1,\,j} \\ \\
u(x-\Delta x,y-\Delta y,t)&=&u_{i-1,\,j-1}, \\ \\
u(x,y-\Delta y,t)z&=&u_{i,\,j-1},\\ \\
u(x+\Delta x ,y-\Delta y,t)&=&u_{i+1,\,j-1} \\
$$
と略記し，Fortranの表記法で，それぞれ

```fortran
u(i-1,j+1),
u(i,j+1),
u(i+1,j+1),
u(i-1,j),
u(i,j),
u(i+1,j),
u(i-1,j-1),
u(i,j-1),
u(i+1,j-1),
```

と書くことにする。

### 一階微分, $\frac{\partial u}{\partial x}$

#### 上流差分 (風上差分)

$$
\frac{\partial u}{\partial x}\simeq \frac{1}{\Delta x}
\bigg(u(x+\Delta x,y,t)-u(x,y,t)\bigg)
$$

```Fortran
dudx=1.0/dx*(u(i+1,j)-u(i,j))
```

#### 中央差分

$$
\frac{\partial u}{\partial x}\simeq \frac{1}{2\Delta x}
\bigg(u(x+\Delta x,y,t)-u(x-\Delta x,y,t)\bigg)
$$

```Fortran
dx2=1.0/(2.0*dx)
dudx=dx2*(u(i+1,j)-u(i-1,j))
```

### 二階微分, $\frac{\partial^2 u}{\partial x^2}$

$$
\frac{\partial^2 u}{\partial x^2}&\simeq& \frac{1}{\Delta x}\bigg[
\bigg(\frac{\partial u}{\partial x}\bigg)_{i+1,\,j}-
\bigg(\frac{\partial u}{\partial x}\bigg)_{i,\,j}
\bigg]\\
&\simeq&
\frac{1}{\Delta x}
\bigg[\bigg(\frac{u_{i+1,\,j}-u_{i,\,j}}{\Delta x}\bigg)
-\bigg(\frac{u_{i,\,j}-u_{i-1,\,j}}{\Delta x}\bigg)
\bigg]\\
&=&\frac{1}{\Delta x^2}(u_{i-1,\,j}-2u_{i,\,j}+u_{i+1,\,j})
$$

```fortran
dxs=1.0/(dx*dx)
du2dx=dxs*(u(i+1,j)-2*u(i,j)+u(i-1,j))
```


#!/bin/bash
# Description:
#
# Author: am
#
# Host: localhost
# Directory: /work2/am/TEACHING/Numerical.Modelling/Elementary/06.ADV
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 11:38 on 10-18-2017.

. ./gmtpar.sh
echo "Bash script $0 starts."

# https://www.soest.hawaii.edu/gmt/gmt/pdf/GMT_Docs.pdf
xrange=0/50
yrange=0/50
zrange=-0.2/1.2
range=$xrange/$yrange
rangev=$range/$zrange
size="-JX4.5i -JZ2i"
xanot=10:"x":
yanot=10:"t":
zanot=0.5:"f":
anot="-B${xanot}/${yanot}/${zanot}SEwnZ -N-1/white"

in=$1
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
tmp=$(basename $in .txt)
sfx=$(basename $0  .sh)
out=${tmp}_${sfx}.ps

grd=$(basename $0 .sh).grd
igd=$(basename $0 .sh)_intensity.grd

cpt=gray.cpt
echo �f-5 128 5 128�f > $cpt

#                      t  x  f
awk '{if($1!="#")print $2,$1,$3}' $in | \
surface -T1 -I1/1 -R$range -G$grd

grdgradient $grd -A225 -G$igd -Nt0.75

grdview $grd $size $anot -Qm -I$igd -C$cpt -R$range -E150/35 \
-P -K -X1 >$out



xoffset=
yoffset=5
comment=
. ./note.sh

echo
rm -fv $grd $igd $cpt
echo

echo Input : $in
echo Output : $out

echo "Done $0"

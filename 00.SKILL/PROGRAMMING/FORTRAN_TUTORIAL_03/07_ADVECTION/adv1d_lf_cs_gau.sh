#!/bin/bash
#
# Description:
#
src=$(basename $0 .sh).f90
exe=$(basename $0 .sh).exe
nml=$(basename $0 .sh).nml
f90=ifort
opt="-CB -traceback -fpe0"
prog_name=$(echo adv1d_ftcs| sed -e 's/\./_/g')



u=1.0
im=51
dx=1.0
t0=0.0
te=50.0
dt=0.5
dtout=1.0
outfile=$(basename $0 .sh)_result_u${u}_dt${dt}.txt

cat <<EOF>$nml
&para
u=${u},
im=${im},
dx=${dx},
t0=${t0},
te=${te},
dt=${dt},
dtout=${dtout},
outfile="${outfile}",
&end
EOF



cat <<EOF>${src}
program ${prog_name}

integer im
real::u,dx,t0,te,dt
real,allocatable::x(:),fold(:),f(:),fnew(:)
character(len=5000)::outfile

namelist /para/u,im,dx,t0,te,dt,dtout,outfile

read(*,nml=para)



allocate(x(1:im),fold(0:im+1),f(0:im+1),fnew(0:im+1))



do i=1,im
x(i)=float(i-1)*dx
end do !i



nm=(te-t0)/dt



nout=dtout/dt



!INITIAL CONDITION
f(:)=0.0
ic=int(float(im)/2.0+0.5)
xc=dx*float(ic)
dd=(2.0*dx)**2
do i=1,im
f(i)=1.0*exp(-(x(i)-xc)**2/dd)
enddo !i



!COURANT NUMBER
c=u*dt/dx
c2=2.0*c



open(21,file=outfile)
write(21,'(A,f10.3)')'# u  = ',u
write(21,'(A,i10  )')'# im = ',im
write(21,'(A,f10.3)')'# dx = ',dx
write(21,'(A,f10.3)')'# t0 = ',t0
write(21,'(A,f10.3)')'# te = ',te
write(21,'(A,f10.5)')'# dt = ',dt
write(21,'(A,f10.5)')'# dtout = ',dtout
write(21,'(A,i10  )')'# nm = ',nm
write(21,'(A,f10.5)')'# c  = ',c
write(21,'(A,A    )')'# outfile = ',trim(outfile)
write(21,'(A,A    )')'# t, x, f'



! PRINT INITIAL CONDITION
t=t0
do i=1,im
write(21,'(f10.3,f10.1,e14.6)')t,x(i),f(i)
end do !i



!STEP 1
n=1
t=t0+float(n)*dt
!BOUNDARY CONDITION

!CYCLIC BC
f(0)=f(im)
f(im+1)=f(1)

!FORWARD STEP IN TIME
do i=1,im
fnew(i)=f(i)-c*(f(i)-f(i-1)) !EF-UPW
enddo !i

!UPDATE
do i=1,im
fold(i)=f(i)
f(i)=fnew(i)
enddo !i


!PRINT RESULT
if(mod(n,nout)==0)then
do i=1,im
write(21,'(f10.3,f10.1,e14.6)')t,x(i),f(i)
end do !i
endif



!TIME LOOP
do n=2,nm

t=t0+float(n)*dt

!BOUNDARY CONDITION

!CYCLIC BC
f(0)=f(im)
f(im+1)=f(1)

!FORWARD STEP IN TIME
do i=1,im
fnew(i)=fold(i)-c*(f(i+1)-f(i-1)) !LF-CS
enddo !i

!UPDATE
do i=1,im
fold(i)=f(i)
f(i)=fnew(i)
enddo !i

!PRINT RESULT
if(mod(n,nout)==0)then
do i=1,im
write(21,'(f10.3,f10.1,e14.6)')t,x(i),f(i)
end do !i
endif

!GO TO NEXT STEP
end do !n



print '(A,f10.3)','# u  =',u
print '(A,i10  )','# im =',im
print '(A,f10.3)','# dx =',dx
print '(A,f10.3)','# t0 =',t0
print '(A,f10.3)','# te =',te
print '(A,f10.5)','# dt =',dt
print '(A,f10.5)','# dtout =',dtout
print '(A,f10.5)','# c  =',c
print '(A,i10  )','# nm =',nm
print '(A,A    )','# outfile=',trim(outfile)

stop

end program ${prog_name}
EOF

echo
echo Created ${src}.
echo
ls -lh ${src}
echo




echo Compiling ${src} ...
echo
echo ${f90} ${opt} ${src} -o ${exe}
echo
${f90} ${opt} ${src} -o ${exe}
if [ $? -ne 0 ]; then
echo
echo COMPILE ERROR!!!
echo
echo TERMINATED.
echo
exit 1
fi
echo "Done Compile."
echo
ls -lh ${exe}
echo



echo
echo ${exe} is running ...
echo
${exe}<${nml}
if [ $? -ne 0 ]; then
echo
echo ERROR in $exe: RUNTIME ERROR!!!
echo
echo Terminated.
echo
exit 1
fi
echo
echo "Done ${exe}"
echo


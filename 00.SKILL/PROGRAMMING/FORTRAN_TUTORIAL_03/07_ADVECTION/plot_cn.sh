#!/bin/bash
# Description:
#
# Author: am
#
# Host: localhost
# Directory: /work2/am/TEACHING/Numerical.Modelling/Elementary/06.ADV
#
# Revision history:
#  This file is created by /usr/local/mybin/ngmt.sh at 11:38 on 10-18-2017.

. ./gmtpar.sh
echo "Bash script $0 starts."

# https://www.soest.hawaii.edu/gmt/gmt/pdf/GMT_Docs.pdf
range=0/50/0/50
size="5/5/0.5SEwnZ -N-1/white"
xanot=a10f10
yanot=$xanot
anot=${xanot}:"x":/${yanot}:"t":WSne

in=$1
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
tmp=$(basename $in .txt)
sfx=$(basename $0  .sh)
out=${tmp}_${sfx}.ps

grd=$(basename $0 .sh).grd

#                      t  x  f
awk '{if($1!="#")print $2,$1,$3}' $in | \
surface -T1 -I1/1 -R$range -G$grd

grdcontour $grd -JX5/5 -B$anot -K -C0.1 -A0.1 -X1 -Y3 -P -K >$out





xoffset=
yoffset=5
comment=
. ./note.sh

echo
rm -vf $grd
echo

echo Input : $in
echo Output : $out

echo "Done $0"

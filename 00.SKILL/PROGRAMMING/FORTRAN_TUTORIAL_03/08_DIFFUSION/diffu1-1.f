************************************************************************
* DIFFU1.F
*  ONE-DIMENSIONAL DIFFUSION EQUATION.
************************************************************************
      PARAMETER(MZ=50,ONEDAY=24.0*3600.0)
      PARAMETER(IUT=20)
      REAL KH ! DIFFUSIVITY
      COMMON /PARA/DZ,DT,KH

      DIMENSION FB(MZ),F(MZ)

      DZ=1.0
      DT=30.0
      KH=1.0E-4

      TTIME=ONEDAY*5.0
      NSTEP=INT(TTIME/DT)
      PTIME=3600.0*4.0
      IPRT=INT(PTIME/DT)
      STIME=ONEDAY
      IPRS=INT(STIME/DT)

      WRITE(*,*)'TTIME=',TTIME
      WRITE(*,*)'NSTEP=',NSTEP
      WRITE(*,*)'PTIME=',PTIME
      WRITE(*,*)'IPRT=',IPRT
                                           
      nfl=0

      OPEN(IUT,FILE='TSERIES.DAT',STATUS='UNKNOWN')
      TIME=0.0
      CALL INIT(FB,MZ)
      CALL PRITS(IUT,TIME,FB,MZ)
      CALL PRISH(TIME,NFL,FB,MZ)

      DO N=1,NSTEP
        CALL STEP(FB,F,MZ)
        DO K=1,MZ !UPDATE
          FB(K)=F(K)
        END DO
        IF(MOD(N,IPRT).EQ.0)THEN !TIME SERIES
          TIME=FLOAT(N)*DT/ONEDAY
          CALL PRITS(IUT,TIME,FB,MZ)
        END IF
        IF(MOD(N,IPRS).EQ.0)THEN !SNAPSHOT
          NFL=NFL+1
          TIME=FLOAT(N)*DT/ONEDAY
          CALL PRISH(TIME,NFL,FB,MZ)
        END IF
      END DO

      CLOSE(IUT)
      STOP
      END

************************************************************************
* STEP
*  STEP FORWARD IN TIME
*  FORWARD SCHEME
*   dF/dt = d(KH*(dF/dz))/dz
************************************************************************
      SUBROUTINE STEP(FB,F,MZ)
      DIMENSION FB(MZ),F(MZ)
      REAL KH ! DIFFUSIVITY
      COMMON /PARA/DZ,DT,KH
      
C BOUNDARY CONDITIONS
      F(1)=0.0
      F(MZ)=0.0

      DO K=2,MZ-1
      F(K)=FB(K)+DT*( KH*(FB(K+1)-FB(K))/DZ-KH*(FB(K)-FB(K-1))/DZ )/DZ
      END DO
      RETURN
      END

************************************************************************
* INIT
*  SET INITIAL CONDITIONS
************************************************************************
      SUBROUTINE INIT(FB,MZ)
      DIMENSION FB(MZ)

      DO K=1,MZ
        FB(K)=0.0
      END DO
      FB(MZ/2-1)=1.0
      FB(MZ/2)=1.0
      FB(MZ/2+1)=1.0
      RETURN
      END

************************************************************************
* PRITS
*  TIME SERIES
************************************************************************
      SUBROUTINE PRITS(IUT,TIME,F,MZ)

      DIMENSION F(MZ)
C      WRITE(*,*)time
      WRITE(IUT,'(2F10.5)')TIME,F(MZ/2)
      RETURN
      END

************************************************************************
* PRISH
*  SNAPSHOT
************************************************************************
      SUBROUTINE PRISH(TIME,NFL,F,MZ)
      DIMENSION F(MZ)
      COMMON /PARA/DZ,DT,KH
      CHARACTER SNOUT*3

!      j1 = mod(NFL,10)
!      j2 = mod(int(NFL/10),10)
!      j3 = int(NFL/100)

!      SNOUT=CHAR(J3+ICHAR('0'))//CHAR(J2+ICHAR('0'))
!     &//CHAR(J1+ICHAR('0'))

      write(*,*)'nfl=',nfl
      write(snout(1:3),'(i3.3)')nfl
     
      OPEN(30,FILE='prof'//SNOUT//'.txt',STATUS='UNKNOWN')
      DO K=1,MZ
        Z=DZ*(FLOAT(K)-0.5)
        WRITE(30,'(2F10.5)')Z,F(K)
      END DO
      CLOSE(30)
      RETURN
      END

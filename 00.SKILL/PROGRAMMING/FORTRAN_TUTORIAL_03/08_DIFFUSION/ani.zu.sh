#!/bin/sh

#
# 折れ線グラフのサンプル
#

# 入力ファイル名
in=$1
# 出力ファイル名
out=$2

# 入力ファイルが存在するかチェック
if [ ! -f ${in} ]; then
  echo
  echo ERROR: No such file, ${in}
  echo
  exit 1
fi



#echo INPUT:
#echo ${in}
#echo OUTPUT:
#echo ${out}
#echo

range=0/50/0/1.2/

xanot=a10f1
yanot=a0.2f1

size=5/5

#awk '{if ($1 != "#") \
#   printf "%12.6f %12.6f\n", $1, $2}' ${in}

awk '{if ($1 != "#") \
   printf "%12.6f %12.6f\n", $1, $2 }' ${in} |
psxy -JX${size} -R${range} \
-W5 -B${xanot}:"X [m]":/${yanot}:"":WSne \
  -X1.5 -Y1.5 -P -K > ${out}


# Print time, current working directory and output filename
currentdir=`pwd`
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=`date`
host=`hostname`
comment="solid=sea level. dotted=velocity"
pstext -JX6/1.2 -R0/1/0/1.2 -N -Y7.5 << EOF -O >> $out
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM $in   $out
0 0.1   9 0 1 LM ${comment}
EOF

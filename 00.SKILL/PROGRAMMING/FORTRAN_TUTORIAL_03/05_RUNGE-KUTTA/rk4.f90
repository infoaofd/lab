program rk4
  real m, k, x0, u0, t0, x, u, xa, ua, te, dt
  real k1x, k2x, k3x, k4x, k1u, k2u, k3u, k4u
  character(len=500)::OUT

  namelist /para/m, k, te, x0, u0, dt, OUT

  read(*, nml=para)
  
  n = int((te - t0) / dt)
  
  x = x0; u = u0; t = t0; xa = x0; ua = u0

  open(21, file=OUT)
  write(21, '(A,f10.3)') '# x0= ', x0
  write(21, '(A,f10.3)') '# u0= ', u0
  write(21, '(A,f10.3)') '# dt= ', dt
  write(21, '(A)') '# t, x, u, xa, ua'
  write(21, '(5e14.6)') t, x, u, xa, ua

  ! ANGULAR VELOCITY SQUARED
  omg2 = k / m
  omg = sqrt(omg2)
  
  do i = 1, n
    ! 4次のルンゲクッタ法での計算
    k1x = fx(t, x, u) * dt
    k1u = fu(t, x, u, m, k) * dt

    k2x = fx(t + 0.5 * dt, x + 0.5 * k1x, u + 0.5 * k1u) * dt
    k2u = fu(t + 0.5 * dt, x + 0.5 * k1x, u + 0.5 * k1u, m, k) * dt

    k3x = fx(t + 0.5 * dt, x + 0.5 * k2x, u + 0.5 * k2u) * dt
    k3u = fu(t + 0.5 * dt, x + 0.5 * k2x, u + 0.5 * k2u, m, k) * dt

    k4x = fx(t + dt, x + k3x, u + k3u) * dt
    k4u = fu(t + dt, x + k3x, u + k3u, m, k) * dt

    ! x, uの更新
    x = x + (k1x + 2.0 * (k2x + k3x) + k4x) / 6.0
    u = u + (k1u + 2.0 * (k2u + k3u) + k4u) / 6.0

    t = t + dt

    ! 解析解の計算
    xa = x0 * cos(omg * t) + u0 / omg * sin(omg * t)  ! ANALYTICAL SOLUTION
    ua = u0 * cos(omg * t) - x0 * omg * sin(omg * t)

    write(21, '(5e14.6)') t, x, u, xa, ua
  end do ! i

  print *; print '(A, A)', 'OUT: ', trim(OUT); print *
end program rk4


function fx(t, x, u)
  real t, x, u
  fx = u
end function fx

function fu(t, x, u, m, k)
  real t, x, u, m, k
  fu = -k / m * x
end function fu

import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "osc0d_result.txt"
filename2 = "rk2_result.txt"

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# osc0d_result.txtのデータ (0列〜5列)
data1 = read_file(filename1, 5)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename2, 5)

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# グラフ1 (xの時系列)
axs[0].plot(data1[:, 0], data1[:, 3], linewidth=2, linestyle="-", label="Analytical", color="red")
axs[0].plot(data1[:, 0], data1[:, 1], linewidth=1, linestyle="--", label="Numerical (Euler)", color="blue")
axs[0].plot(data2[:, 0], data2[:, 1], linewidth=1, linestyle="-", label="Numerical (RK2nd)", color="green")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x")
axs[0].set_title("Time Series of x")
axs[0].legend()

# グラフ2 (uの時系列)
axs[1].plot(data1[:, 0], data1[:, 4], linewidth=2,  linestyle="-", label="Analytical", color="red")
axs[1].plot(data1[:, 0], data1[:, 2], linewidth=1,  linestyle="--", label="Numerical (Euler)", color="blue")
axs[1].plot(data2[:, 0], data2[:, 2], linewidth=1,  linestyle="-", label="Numerical (RK2nd)", color="green")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u")
axs[1].set_title("Time Series of u")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_combined.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "osc0d_result.txt"
filename2 = "rk2_result.txt"

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# osc0d_result.txtのデータ (0列〜5列)
data1 = read_file(filename1, 5)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename2, 5)

# xの誤差 (解析解 - 数値解)
x_diff_euler = data1[:, 1] - data1[:, 3]  # Euler法の誤差
x_diff_rk2 = data2[:, 1] - data1[:, 3]    # RK2法の誤差

# uの誤差 (解析解 - 数値解)
u_diff_euler = data1[:, 2] - data1[:, 4]  # Euler法の誤差
u_diff_rk2 = data2[:, 2] - data1[:, 4]    # RK2法の誤差

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# xの誤差プロット
axs[0].plot(data1[:, 0], x_diff_euler, linewidth=1, linestyle="--", label="Euler (x error)", color="blue")
axs[0].plot(data2[:, 0], x_diff_rk2, linewidth=1, linestyle="-", label="RK2 (x error)", color="green")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x Error")
axs[0].set_title("Error between Numerical and Analytical Solutions (x)")
axs[0].legend()

# uの誤差プロット
axs[1].plot(data1[:, 0], u_diff_euler, linewidth=1, linestyle="--", label="Euler (u error)", color="blue")
axs[1].plot(data2[:, 0], u_diff_rk2, linewidth=1, linestyle="-", label="RK2 (u error)", color="green")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u Error")
axs[1].set_title("Error between Numerical and Analytical Solutions (u)")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_error_plot.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")


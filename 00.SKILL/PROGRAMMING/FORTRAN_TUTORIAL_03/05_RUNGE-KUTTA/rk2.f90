program rk2
  real m, k, x0,u0, t0, x, u, xa, ua, te, dt
  real k1x, k2x, k1u, k2u
  character(len=500)::OUT

  namelist /para/m, k, te, x0, u0, dt, OUT

  read(*, nml=para)
  
  n=int((te-t0)/dt)
  
  x = x0; u = u0; t = t0; xa=x0; ua=u0

  open(21, file=OUT)
  write(21, '(A,f10.3)') '# x0= ', x0
  write(21, '(A,f10.3)') '# u0= ', u0
  write(21, '(A,f10.3)') '# dt= ', dt
  write(21, '(A)') '# t, x, u, xa, ua'
  write(21, '(5e14.6)') t, x, u, xa, ua

  ! ANGULAR VELOCITY SQUARED
  omg2=k/m
  omg=sqrt(omg2)
  
  do i = 1, n
    k1x = fx(t, x, u)*dt
    k1u = fu(t, x, u, m, k)*dt
    k2x = fx(t + dt, x + k1x, u + k1u)*dt
    k2u = fu(t + dt, x + k1x, u + k1u, m, k)*dt

    x = x + 0.5 * (k1x + k2x)
    u = u + 0.5 * (k1u + k2u)

    t = t + dt

    xa=x0*cos(omg*t)+u0/omg*sin(omg*t) !ANALYTICAL SOLUTION
    ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

   write(21, '(5e14.6)') t, x, u, xa, ua
  end do ! i

  print *; print '(A, A)', 'OUT: ', trim(OUT); print *
end program rk2


function fx(t, x, u)
  real t,x,u
  fx = u
end function fx

function fu(t, x, u, m, k)
  real t, x, u, m, k
  fu = -k/m*x
end function fu

# ルンゲ・クッタ法

[[_TOC_]]

## 導入

リープフロッグ法は2次の精度であったが，オイラー前進差分を改良することで，リープフロッグ法よりも高精度の解法を得ることができる。そのような解法の一つとしてしばしば用いられているルンゲ・クッタ法について学ぶ。

## 2次のルンゲ・クッタ法

4次のルンゲ・クッタ法が良く用いられるが，説明を簡単にするため，まず2次のルンゲ・クッタ法とオイラー前進差分を比較する。

例として，
$$
\begin{eqnarray}
\frac{dx}{dt}&=&f_x(u,x,t) \tag{1} \\
\frac{du}{dt}&=&f_u(u,x,t) \tag{2}
\end{eqnarray}
$$
という連立微分方程式を考える。$f_x$ と $f_u$ はそれぞれ，$x$ と $u$ の時間変化率を表す関数である。簡単な例として，以前学習した振動方程式，
$$
\begin{eqnarray}
\frac{dx}{dt} &=& u  \tag{1}　\\
\\
\frac{du}{dt}&=&-x \tag{2}\\
\end{eqnarray}
$$
を考えると，
$$
f_x&=&u \\
f_u&=&-x
$$
となる。

以降，表記を簡単にするため，
$$
t_{n-1}&=&t-\Delta t,&&x_{n-1}&=&x(t-\Delta t),&& u_{n-1}&=&u(t-\Delta t) \\
t_{n}&=&\Delta t,&&x_n&=&x(t),\qquad && u_{n}&=&u(t) \\
t_{n+1}&=&t+\Delta t,&&x_{n+1}&=&x(t+\Delta t),&& u_{n+1}&=&u(t+\Delta t) \\
$$
と略記する。

### オイラー前進差分

(1)と(2) をオイラー前進差分で近似すると，
$$
\begin{eqnarray}
\frac{x_{n+1}-x_n}{\Delta t}&=&f_x(x_n,u_n,t) \tag{3} \\
\\
\frac{u_{n+1}-u_n}{\Delta t}&=&f_u(x_n,u_n,t) \tag{4} \\
\end{eqnarray}
$$
となる。左辺の分母を払うと，
$$
\begin{eqnarray}
x_{n+1} &=& x_n + f_x(x_n,u_n,t)\,\Delta t \tag{3}\\
\\
u_{n+1} &=& u_n + f_u(x_n,u_n,t)\,\Delta t \tag{4}
\end{eqnarray}
$$
が得られる。

### 2次のルンゲクッタ法

2次のルンゲ・クッタ法はオイラー法を改良した方法であり、概要を述べると以下のようになる。

(3)，(4) の右辺第2項を次のように計算する。
$$
\begin{eqnarray}
x_{n+1} = x_n + \frac{1}{2}(k_{1x} + k_{2x}) \tag{5}\\　
u_{n+1} = u_n + \frac{1}{2}(k_{1u} + k_{2u}) \tag{6}
\end{eqnarray}
$$
ここで，

- $k_{1\,\cdot\,}$: 現在の時刻での値（オイラー前進差分と同様）

- $k_{2\,\cdot\,}$ : 次の時刻での状態を予測した値を使った近似値

であり，それぞれ次のようにして計算される。
$$
\begin{eqnarray}
k_{1x} &=& f_x(x_n, u_n, t)\times\Delta t \\ \\
k_{2x} &=& f_x(x_{n+1}, u_{n+1}, t_{n+1})\times\Delta t \\ \\
k_{1u} &=& f_u(x_n, u_n, t)\times\Delta t \\ \\
k_{2u} &=& f_u(x_{n+1}, u_{n+1}, t_{n+1})\times\Delta t \\
\end{eqnarray}
$$
$k_{1x}$ と $k_{2x}$ の平均，$k_{1u}$ と $k_{2u}$ の平均を用いることで，精度向上を図っている。

### プログラム例

#### 2次のルンゲクッタ法

##### rk2.f90

```fortran
program rk2
  real m, k, x0,u0, t0, x, u, xa, ua, te, dt
  real k1x, k2x, k1u, k2u
  character(len=500)::OUT

  namelist /para/m, k, te, x0, u0, dt, OUT

  read(*, nml=para)
  
  n=int((te-t0)/dt)
  
  x = x0; u = u0; t = t0; xa=x0; ua=u0

  open(21, file=OUT)
  write(21, '(A,f10.3)') '# x0= ', x0
  write(21, '(A,f10.3)') '# u0= ', u0
  write(21, '(A,f10.3)') '# dt= ', dt
  write(21, '(A)') '# t, x, u, xa, ua'
  write(21, '(5e14.6)') t, x, u, xa, ua

  ! ANGULAR VELOCITY SQUARED
  omg2=k/m
  omg=sqrt(omg2)
  
  do i = 1, n
    k1x = fx(t, x, u)*dt
    k1u = fu(t, x, u, m, k)*dt
    k2x = fx(t + dt, x + k1x, u + k1u)*dt
    k2u = fu(t + dt, x + k1x, u + k1u, m, k)*dt

    x = x + 0.5 * (k1x + k2x)
    u = u + 0.5 * (k1u + k2u)

    t = t + dt

    xa=x0*cos(omg*t)+u0/omg*sin(omg*t) !ANALYTICAL SOLUTION
    ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

   write(21, '(5e14.6)') t, x, u, xa, ua
  end do ! i

  print *; print '(A, A)', 'OUT: ', trim(OUT); print *
end program rk2


function fx(t, x, u)
  real t,x,u
  fx = u
end function fx

function fu(t, x, u, m, k)
  real t, x, u, m, k
  fu = -k/m*x
end function fu
```

#### オイラー前進差分

##### osc0d.f90

```fortran
program osc0d

character(len=5000)::OUT
real::m, k, t0, te, x0, u0, dt

namelist /para/m, k, te, x0, u0, dt, OUT

read(*,nml=para)
t0=0.0

n=(te-t0)/dt

open(20,file=OUT)
write(20,'(A,f10.5)')'# m =',m
write(20,'(A,f10.5)')'# k =',k
write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# x0=',x0
write(20,'(A,f10.5)')'# u0=',u0
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, x, u, xa, ua'

! ANGULAR VELOCITY SQUARED
omg2=k/m
omg=sqrt(omg2)

! INITIAL CONDITION
t=t0; x=x0; u=u0; xa=x0; ua=u0
write(20,'(f10.5,4e14.6)')t,x,u, xa,ua

! TIME INTEGRATION
unew=0.0
do i=1,N
  t=t0+dt*float(i)

  unew = u - omg2*x*dt  !TIME INTEGRATION OF u
  xnew = x + u*dt       !TIME INTEGRATION OF x
  x=xnew; u=unew        !UPDATE

  xa=x0*cos(omg*t)+u0/omg*sin(omg*t)
  ua=u0*cos(omg*t)-x0*omg*sin(omg*t)

  write(20,'(f10.5,4e14.6)')t, x, u, xa, ua

enddo !i

print *; print '(A,A)', 'OUT: ',trim(OUT); print *

end program osc0d

```

#### 実行用スクリプト

##### rk2.run.sh

```bash
#!/bin/bash

src1=rk2.f90;   exe1=$(basename $src1 .f90).exe; nml1=$(basename $src1 .f90).nml
if [ ! -f $src1 ];then echo NO SUCH FILE, $src1;exit 1;fi

src2=osc0d.f90; exe2=$(basename $src2 .f90).exe; nml2=$(basename $src1 .f90).nml
if [ ! -f $src2 ];then echo NO SUCH FILE, $src2;exit 1;fi

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05

# COMPILE SOURCE FILE
echo; echo COMPILING $src1 ...
ifort $opt $src1 -o $exe1
if [ $? -ne 0 ]; then echo Compile error!; echo $src1; exit 1; fi
echo DONE.

# CREATE NAMELIST FILE
OUT=$(basename $src1 .f90)_result.txt
cat <<EOF>$nml1
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe1 IS RUNNING ...
$exe1 < $nml1
if [ $? -ne 0 ]; then echo ERROR IN $exe1: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.


# COMPILE SOURCE FILE
echo; echo COMPILING $src2 ...
ifort $opt $src2 -o $exe2
if [ $? -ne 0 ]; then echo COMPILE ERROR!; echo $src1; exit 1; fi
echo Done.

# CREATE NAMELIST FILE
OUT=$(basename $src2 .f90)_result.txt
cat <<EOF>$nml2
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe2 IS RUNNING ...
$exe2 < $nml2
if [ $? -ne 0 ]; then echo ERROR IN $exe2: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.

```

#### 作図用スクリプト

##### rk2_PLT.py

オイラー前進差分と2次のルンゲクッタ法の比較

```python
import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "osc0d_result.txt"
filename2 = "rk2_result.txt"

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# osc0d_result.txtのデータ (0列〜5列)
data1 = read_file(filename1, 5)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename2, 5)

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# グラフ1 (xの時系列)
axs[0].plot(data1[:, 0], data1[:, 3], linewidth=2, linestyle="-", label="Analytical", color="red")
axs[0].plot(data1[:, 0], data1[:, 1], linewidth=1, linestyle="--", label="Numerical (Euler)", color="blue")
axs[0].plot(data2[:, 0], data2[:, 1], linewidth=1, linestyle="-", label="Numerical (RK2nd)", color="green")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x")
axs[0].set_title("Time Series of x")
axs[0].legend()

# グラフ2 (uの時系列)
axs[1].plot(data1[:, 0], data1[:, 4], linewidth=2,  linestyle="-", label="Analytical", color="red")
axs[1].plot(data1[:, 0], data1[:, 2], linewidth=1,  linestyle="--", label="Numerical (Euler)", color="blue")
axs[1].plot(data2[:, 0], data2[:, 2], linewidth=1,  linestyle="-", label="Numerical (RK2nd)", color="green")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u")
axs[1].set_title("Time Series of u")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_combined.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

```

##### rk2_ERR.py

オイラー前進差分と2次のルンゲクッタ法の誤差の比較

```python
import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "osc0d_result.txt"
filename2 = "rk2_result.txt"

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# osc0d_result.txtのデータ (0列〜5列)
data1 = read_file(filename1, 5)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename2, 5)

# xの誤差 (解析解 - 数値解)
x_diff_euler = data1[:, 1] - data1[:, 3]  # Euler法の誤差
x_diff_rk2 = data2[:, 1] - data1[:, 3]    # RK2法の誤差

# uの誤差 (解析解 - 数値解)
u_diff_euler = data1[:, 2] - data1[:, 4]  # Euler法の誤差
u_diff_rk2 = data2[:, 2] - data1[:, 4]    # RK2法の誤差

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4)  # 間隔を調整

# xの誤差プロット
axs[0].plot(data1[:, 0], x_diff_euler, linewidth=1, linestyle="--", label="Euler (x error)", color="blue")
axs[0].plot(data2[:, 0], x_diff_rk2, linewidth=1, linestyle="-", label="RK2 (x error)", color="green")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x Error")
axs[0].set_title("Error between Numerical and Analytical Solutions (x)")
axs[0].legend()

# uの誤差プロット
axs[1].plot(data1[:, 0], u_diff_euler, linewidth=1, linestyle="--", label="Euler (u error)", color="blue")
axs[1].plot(data2[:, 0], u_diff_rk2, linewidth=1, linestyle="-", label="RK2 (u error)", color="green")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u Error")
axs[1].set_title("Error between Numerical and Analytical Solutions (u)")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_error_plot.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")


```

### 演習1

[1] 上記のプログラム，スクリプトを使用して，ルンゲクッタ法とオイラー前進差分の数値解を比較せよ。

[2] $m$，$k$，$\Delta t$の値を変更して上記 [1] を繰り返せ。

[3] プログラムの下記の部分の内容（何を行っているのか）を解説せよ。

```FORTRAN
    k1x = fx(t, x, u)*dt
    k1u = fu(t, x, u, m, k)*dt
    k2x = fx(t + dt, x + k1x, u + k1u)*dt
    k2u = fu(t + dt, x + k1x, u + k1u, m, k)*dt
```

```fortran
function fx(t, x, u)
  real t,x,u
  fx = u
end function fx

function fu(t, x, u, m, k)
  real t, x, u, m, k
  fu = -k/m*x
end function fu
```

**AIの分析結果のコピーはしない**。AIの分析結果を踏まえ，print文デバッグを

使って**自分で内容を確認する**。

print文デバッグについては，

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md

を参照のこと。

自分の頭で理解したことを，自分の言葉で説明する。

## 4次のルンゲ・クッタ法

中間段階の計算を増やすことで，さらに精度をあげることができる。4次のルンゲクッタ法では、(5) と (6) を次のように変更する。
$$
\begin{eqnarray} 
x_{n+1} &=& x_n + \frac{1}{6} \left( k_{1x} + 2k_{2x} + 2k_{3x} + k_{4x} \right) \\ u_{n+1} &=& u_n + \frac{1}{6} \left( k_{1u} + 2k_{2u} + 2k_{3u} + k_{4u} \right) 
\end{eqnarray}
$$
ここで，

- $k_{1\cdot}$: 現在の時刻での値（オイラー前進差分と同様）
- $k_{2\cdot}$, $k_{3\cdot}$: 時刻 $t + \Delta t / 2$ での値を使った予測値
- $k_{4\cdot}$: 次の時刻 $t + \Delta t$ での値を使った予測値。

であり，次の式で計算される。
$$
\begin{eqnarray}
k_{1x} &=& f_x(x_n,\, u_n,\, t) \times \Delta t \\ \\
k_{1u} &=& f_u(x_n,\, u_n,\, t) \times \Delta t \\ \\
k_{2x} &=& f_x(x_n + 0.5 k_{1x},\, u_n + 0.5 k_{1u},\, t + 0.5 \Delta t) \times \Delta t \\ \\
k_{2u} &=& f_u(x_n + 0.5 k_{1x},\, u_n + 0.5 k_{1u},\, t + 0.5 \Delta t) \times \Delta t \\ \\
k_{3x} &=& f_x(x_n + 0.5 k_{2x},\, u_n + 0.5 k_{2u},\, t + 0.5 \Delta t) \times \Delta t \\ \\
k_{3u} &=& f_u(x_n + 0.5 k_{2x},\, u_n + 0.5 k_{2u},\, t + 0.5 \Delta t) \times \Delta t \\ \\
k_{4x} &=& f_x(x_n + k_{3x},\,u_n + k_{3u},\, t + \Delta t) \times \Delta t \\ \\
k_{4u} &=& f_u(x_n + k_{3x},\, u_n + k_{3u},\, t + \Delta t) \times \Delta t \end{eqnarray}
$$
これらを平均することで，より高精度な数値解を得ることができる。

いま仮に，$\Delta t=0.1$とすると，オイラー前進差分の誤差が $0.1$のオーダーであるのに対し，4次のルンゲ・クッタ法の誤差は$10^{-4}$ のオーダーとなる。

オイラー前進差分で同じ精度を得るためには，計算量を1万倍増やす必要があるが，ルンゲ・クッタ法の場合は数倍で済む。

### プログラム例

#### 4次のルンゲクッタ法のFortranプログラム

##### rk4.f90

```fortran
program rk4
  real m, k, x0, u0, t0, x, u, xa, ua, te, dt
  real k1x, k2x, k3x, k4x, k1u, k2u, k3u, k4u
  character(len=500)::OUT

  namelist /para/m, k, te, x0, u0, dt, OUT

  read(*, nml=para)
  
  n = int((te - t0) / dt)
  
  x = x0; u = u0; t = t0; xa = x0; ua = u0

  open(21, file=OUT)
  write(21, '(A,f10.3)') '# x0= ', x0
  write(21, '(A,f10.3)') '# u0= ', u0
  write(21, '(A,f10.3)') '# dt= ', dt
  write(21, '(A)') '# t, x, u, xa, ua'
  write(21, '(5e14.6)') t, x, u, xa, ua

  ! ANGULAR VELOCITY SQUARED
  omg2 = k / m
  omg = sqrt(omg2)
  
  do i = 1, n
    ! 4次のルンゲクッタ法での計算
    k1x = fx(t, x, u) * dt
    k1u = fu(t, x, u, m, k) * dt

    k2x = fx(t + 0.5 * dt, x + 0.5 * k1x, u + 0.5 * k1u) * dt
    k2u = fu(t + 0.5 * dt, x + 0.5 * k1x, u + 0.5 * k1u, m, k) * dt

    k3x = fx(t + 0.5 * dt, x + 0.5 * k2x, u + 0.5 * k2u) * dt
    k3u = fu(t + 0.5 * dt, x + 0.5 * k2x, u + 0.5 * k2u, m, k) * dt

    k4x = fx(t + dt, x + k3x, u + k3u) * dt
    k4u = fu(t + dt, x + k3x, u + k3u, m, k) * dt

    ! x, uの更新
    x = x + (k1x + 2.0 * (k2x + k3x) + k4x) / 6.0
    u = u + (k1u + 2.0 * (k2u + k3u) + k4u) / 6.0

    t = t + dt

    ! 解析解の計算
    xa = x0 * cos(omg * t) + u0 / omg * sin(omg * t)  ! ANALYTICAL SOLUTION
    ua = u0 * cos(omg * t) - x0 * omg * sin(omg * t)

    write(21, '(5e14.6)') t, x, u, xa, ua
  end do ! i

  print *; print '(A, A)', 'OUT: ', trim(OUT); print *
end program rk4


function fx(t, x, u)
  real t, x, u
  fx = u
end function fx

function fu(t, x, u, m, k)
  real t, x, u, m, k
  fu = -k / m * x
end function fu

```

#### 実行用スクリプト

##### rk4.run.sh

```bash
#!/bin/bash

src1=rk4.f90;   exe1=$(basename $src1 .f90).exe; nml1=$(basename $src1 .f90).nml
if [ ! -f $src1 ];then echo NO SUCH FILE, $src1;exit 1;fi

src2=osc0d.f90; exe2=$(basename $src2 .f90).exe; nml2=$(basename $src1 .f90).nml
if [ ! -f $src2 ];then echo NO SUCH FILE, $src2;exit 1;fi

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05

# COMPILE SOURCE FILE
echo; echo COMPILING $src1 ...
ifort $opt $src1 -o $exe1
if [ $? -ne 0 ]; then echo Compile error!; echo $src1; exit 1; fi
echo DONE.

# CREATE NAMELIST FILE
OUT=$(basename $src1 .f90)_result.txt
cat <<EOF>$nml1
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe1 IS RUNNING ...
$exe1 < $nml1
if [ $? -ne 0 ]; then echo ERROR IN $exe1: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.


# COMPILE SOURCE FILE
echo; echo COMPILING $src2 ...
ifort $opt $src2 -o $exe2
if [ $? -ne 0 ]; then echo COMPILE ERROR!; echo $src1; exit 1; fi
echo Done.

# CREATE NAMELIST FILE
OUT=$(basename $src2 .f90)_result.txt
cat <<EOF>$nml2
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe2 IS RUNNING ...
$exe2 < $nml2
if [ $? -ne 0 ]; then echo ERROR IN $exe2: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.

```

### 作図用スクリプト

#### 4次と2次のルンゲクッタ法の誤差の比較

##### rk4_ERR.py

```python
import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "rk2_result.txt"    # RK2法
filename2 = "rk4_result.txt"    # RK4法

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename1, 5)

# rk4_result.txtのデータ (0列〜2列)
data3 = read_file(filename2, 5)

# 解析解の取り出し (3列目と4列目が解析解)
xa_rk2 = data2[:, 3]
ua_rk2 = data2[:, 4]
xa_rk4 = data3[:, 3]
ua_rk4 = data3[:, 4]

# xの誤差 (解析解 - 数値解)
x_diff_rk2 = data2[:, 1] - xa_rk2  # RK2法の誤差
x_diff_rk4 = data3[:, 1] - xa_rk4  # RK4法の誤差

# uの誤差 (解析解 - 数値解)
u_diff_rk2 = data2[:, 2] - ua_rk2  # RK2法の誤差
u_diff_rk4 = data3[:, 2] - ua_rk4  # RK4法の誤差

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4, left=0.15, right=0.9)  # 間隔を調整して左端の余白を広げる

# xの誤差プロット
axs[0].plot(data2[:, 0], x_diff_rk2, linewidth=1, linestyle="-", label="RK2 (x error)", color="green")
axs[0].plot(data3[:, 0], x_diff_rk4, linewidth=1, linestyle="-", label="RK4 (x error)", color="red")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x Error")
axs[0].set_title("Error between Numerical and Analytical Solutions (x)")
axs[0].legend()

# uの誤差プロット
axs[1].plot(data2[:, 0], u_diff_rk2, linewidth=1, linestyle="-", label="RK2 (u error)", color="green")
axs[1].plot(data3[:, 0], u_diff_rk4, linewidth=1, linestyle="-", label="RK4 (u error)", color="red")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u Error")
axs[1].set_title("Error between Numerical and Analytical Solutions (u)")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_error.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

```

### 演習2

[1] 上記のプログラム，スクリプトを使用して，2次と4次のルンゲクッタ法の数値解を比較せよ。

[2] $\Delta t$の値を変更して上記 [1] を繰り返せ。

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

import numpy as np
import matplotlib.pyplot as plt
import sys
import os

# 入力ファイル名
filename1 = "rk2_result.txt"    # RK2法
filename2 = "rk4_result.txt"    # RK4法

# ファイルの存在を確認して読み込む関数
def read_file(filename, columns):
    try:
        with open(filename, "r") as f:
            lines = f.readlines()
    except FileNotFoundError:
        print(f"Error: File '{filename}' not found.")
        sys.exit(1)

    data = []
    for line in lines:
        if line.startswith("#") or line.strip() == "t, x, u, xa, ua":
            continue
        values = line.split()
        if len(values) == columns:
            data.append(tuple(map(float, values)))

    return np.array(data)

# rk2_result.txtのデータ (0列〜2列)
data2 = read_file(filename1, 5)

# rk4_result.txtのデータ (0列〜2列)
data3 = read_file(filename2, 5)

# 解析解の取り出し (3列目と4列目が解析解)
xa_rk2 = data2[:, 3]
ua_rk2 = data2[:, 4]
xa_rk4 = data3[:, 3]
ua_rk4 = data3[:, 4]

# xの誤差 (解析解 - 数値解)
x_diff_rk2 = data2[:, 1] - xa_rk2  # RK2法の誤差
x_diff_rk4 = data3[:, 1] - xa_rk4  # RK4法の誤差

# uの誤差 (解析解 - 数値解)
u_diff_rk2 = data2[:, 2] - ua_rk2  # RK2法の誤差
u_diff_rk4 = data3[:, 2] - ua_rk4  # RK4法の誤差

# プロット
fig, axs = plt.subplots(2, 1, figsize=(6, 8))  # 2行1列のレイアウト
fig.subplots_adjust(hspace=0.4, left=0.15, right=0.9)  # 間隔を調整して左端の余白を広げる

# xの誤差プロット
axs[0].plot(data2[:, 0], x_diff_rk2, linewidth=1, linestyle="-", label="RK2 (x error)", color="green")
axs[0].plot(data3[:, 0], x_diff_rk4, linewidth=1, linestyle="-", label="RK4 (x error)", color="red")
axs[0].set_xlabel("Time t")
axs[0].set_ylabel("x Error")
axs[0].set_title("Error between Numerical and Analytical Solutions (x)")
axs[0].legend()

# uの誤差プロット
axs[1].plot(data2[:, 0], u_diff_rk2, linewidth=1, linestyle="-", label="RK2 (u error)", color="green")
axs[1].plot(data3[:, 0], u_diff_rk4, linewidth=1, linestyle="-", label="RK4 (u error)", color="red")
axs[1].set_xlabel("Time t")
axs[1].set_ylabel("u Error")
axs[1].set_title("Error between Numerical and Analytical Solutions (u)")
axs[1].legend()

# PDFに保存
output_file = os.path.splitext(filename2)[0] + "_error.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

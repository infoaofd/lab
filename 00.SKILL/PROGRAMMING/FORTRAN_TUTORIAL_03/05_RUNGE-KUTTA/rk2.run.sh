#!/bin/bash

src1=rk2.f90;   exe1=$(basename $src1 .f90).exe; nml1=$(basename $src1 .f90).nml
if [ ! -f $src1 ];then echo NO SUCH FILE, $src1;exit 1;fi

src2=osc0d.f90; exe2=$(basename $src2 .f90).exe; nml2=$(basename $src1 .f90).nml
if [ ! -f $src2 ];then echo NO SUCH FILE, $src2;exit 1;fi

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
m=1.0
k=1.0
te=30.0
x0=1.0
u0=0.0
dt=0.05

# COMPILE SOURCE FILE
echo; echo COMPILING $src1 ...
ifort $opt $src1 -o $exe1
if [ $? -ne 0 ]; then echo Compile error!; echo $src1; exit 1; fi
echo DONE.

# CREATE NAMELIST FILE
OUT=$(basename $src1 .f90)_result.txt
cat <<EOF>$nml1
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe1 IS RUNNING ...
$exe1 < $nml1
if [ $? -ne 0 ]; then echo ERROR IN $exe1: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.


# COMPILE SOURCE FILE
echo; echo COMPILING $src2 ...
ifort $opt $src2 -o $exe2
if [ $? -ne 0 ]; then echo COMPILE ERROR!; echo $src1; exit 1; fi
echo Done.

# CREATE NAMELIST FILE
OUT=$(basename $src2 .f90)_result.txt
cat <<EOF>$nml2
&para
m=${m},
k=${k},
te=${te},
x0=${x0},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF

# RUN THE PROGRAM
echo $exe2 IS RUNNING ...
$exe2 < $nml2
if [ $? -ne 0 ]; then echo ERROR IN $exe2: RUNTIME ERROR!; echo; echo TERMINATED.; exit 1; fi
echo DONE.

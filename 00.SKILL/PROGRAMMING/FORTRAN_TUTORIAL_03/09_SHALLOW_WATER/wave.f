************************************************************************
* wave.f
*
*     Permision to use, duplication and redistribution is granted.
*  But the author have no liability with respect to incidental damages
*  arising out of or in connection with the use of performance of this
*  program.
cj 　このプログラムの複製，再配布は自由です。しかし，このプログラムの
cj　計算結果が万が一，あなたになんらかの損益を与えるようなことがあって
cj　も，プログラム作成者は，一切その責任を負いません。
*
* Coded by A. Manda
* TASK
cj 1次元波動方程式を解く
* USAGE
* REMARK
* SLAVE ROUTINE
************************************************************************
*------------------------- PARAMETERS ----------------------------------
      include 'param.h'
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
cj 計算全般
      COMMON /VALUE/H,DELT,DELX,VP
      COMMON /SIZE/XMX
cj 出力ファイル
      character OFLE*100,OFLU*100
      common /file/OFLE,OFLU

cj初期条件or境界条件
      COMMON /IC/ef,eta0, prd,nmod

cj 実行時必要となるパラメーター
      COMMON /RPARA/NSTEP
      character ictyp*3
      COMMON /RPARA2/ICTYP
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
      DIMENSION U1(IM),UP(IM),U3(IM),UB(IM),E1(IM),EP(IM),E3(IM),EB(IM)
*-----------------------------------------------------------------------
      open(7,file='config.txt')
      read(7,*)
      read(7,*) DELX       ! 格子幅 [km]
      read(7,*)
      read(7,*) DELT       ! 時間ステップ [s]
      read(7,*)
      read(7,*) NOSTP      ! 計算結果出力のインターバル[計算ステップ]
      read(7,*)
      read(7,*) H          ! 水路の水深[m]
      read(7,*)
      read(7,*) XMX        ! 水路の長さ[km]
      read(7,*)
      read(7,*)
      read(7,*)
      read(7,'(a3)')ictyp  ! 初期条件の種類
      IF(ictyp.eq.'gau')then
        read(7,*)
        read(7,*) EF         ! ガウス型の山のe-folding scale[km]
        eta0=1.0
      else if(ictyp.eq.'sin')then
        read(7,*)
        read(7,*) prd        ! 周期[hr]
        eta0=0.5
      else if(ictyp.eq.'std')then
        read(7,*)
        read(7,*) nmod       ! モードの数
        eta0=0.5
      else if(ictyp.eq.'sch')then
        read(7,*)
        read(7,*) nmod       ! モードの数
        eta0=0.5
      end if
      close(7)

cj CGS単位系に変換
      DELX   = DELX*rk2c      
      H      = H*rm2c         
      XMX    = XMX*rk2c       
      EF     = EF*rk2c        
      eta0   = eta0*rm2c      
      prd    = prd*3600.0

      open(99,file='cond.dat')
      write(99,*) 'DELX [cm] = ',DELX  
      write(99,*) 'DELT [s] = ',DELT   
      write(99,*) 'H [cm]',H           
      write(99,*) 'XMX [cm] = ',XMX    
      write(99,*) 'EF [cm] = ',EF      


cj 格子数のチェック
      IN = INT(XMX/DELX)+1
      IF(IN.GT.IM)THEN
        write(99,*)'ERROR : NUMBER OF GRIDS : ',IN
        STOP
      END IF

cj CFL条件のチェック
      VP = SQRT(G*H)          ! 長波の位相速度
      IF(VP.gt.(delx/delt))THEN
        write(99,*)'ERROR : CFL COND. IS NOT SATISFIED.'
        STOP
      END IF


cj 総計算ステップの表示
      LASTEP = INT((IN*DELX+EF*2)/(VP*DELT))+2
cj時間稼ぎのためのループの繰り返し回数
      jik=500

cj 定常波
      IF(ICTYP.EQ.'std'.or.ICTYP.EQ.'sch')then
         LASTEP=LASTEP*5
         jik=100  ! 時間稼ぎ
      END IF

      WRITE (99,'(A,I10)') 'NUMBER OF ITERATION = ',LASTEP

*
* INITIAL CONDITION
*
      NSTEP  = 0
      CALL INIT(IN,U1,UP,U3,UB,E1,EP,E3,EB)

!!      CALL IDRAW(IN,EP)  !動画表示のための設定

*
*cj 時間積分開始
*
  100 CONTINUE
      IF (NSTEP.GE.LASTEP)GOTO 999
      NSTEP = NSTEP + 1

C--------------- CALCULATE U,ETA -------------------------
      IF(MOD(NSTEP,10).EQ.1)THEN
C EULER BACKWARD DIFFERENCE
        DO 15 I=1,IN
          UP(I) = 0.25*U3(I)+0.5*UP(I)+0.25*U1(I)
          EP(I) = 0.25*E3(I)+0.5*EP(I)+0.25*E1(I) 
   15   CONTINUE
        CALL EQN(IN,UP,UP,UB,EP,EP,EB,1.)
        CALL EQN(IN,UP,UB,U3,EP,EB,E3,1.)
      ELSE          
C CENTRAL TIME DIFFERENCE
        CALL EQN(IN,U1,UP,U3,E1,EP,E3,2.)
C
      END IF
C ---------- UPDATE U,E --------------------------------------
      DO 20 I=2,IN-1
        U1(I)=UP(I)
        UP(I)=U3(I)
   20 CONTINUE
      DO 25 I=2,IN 
        E1(I)=EP(I)
        EP(I)=E3(I)
   25 CONTINUE
C---------------OUTPUT----------------------------------------
      IF(ICTYP.NE.'gau')goto 100
      IF (MOD(NSTEP,NOSTP).EQ.0)THEN
        CALL OUTPUT(IN,EP,UP,IUNIT1,IUNIT2) !水位・流速
      END IF
C-------- GO TO THE NEXT TIME STEP ---------------------------   
      GO TO 100
C 
  999 CONTINUE

      CLOSE(IUNIT1)
      CLOSE(IUNIT2)
      CLOSE(IUNIT3)

      STOP
      END

************************************************************************
* eqn.f
* TASK
cj 1次元の浅水方程式を解く。
cj 境界条件もこのサブルーチンで与える。
* USAGE
* REMARK
* SLAVE ROUTINE
* REFERENCES
************************************************************************
      SUBROUTINE EQN(IN,U1,UP,U3,E1,EP,E3,CNFB)
*------------------------- PARAMETERS ----------------------------------
      include 'param.h'
*-----------------------------------------------------------------------
*--------------------------- ARGUMENTS ---------------------------------
      DIMENSION U1(IM),UP(IM),U3(IM),E1(IM),EP(IM),E3(IM)
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
      COMMON /VALUE/H,DELT,DELX,VP
      common /typhn/xps,pcv,pc,pinf,r0,ZA(IM)
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
*-----------------------------------------------------------------------
cj 境界条件の設定
      CALL BC(IN,U1,UP,U3,E1,EP,E3)

cj 運動方程式を解いて流速を計算する。
      C1     = G*DELT/DELX
      DO I=2,IN-1      
        U3(I)=U1(I)-CNFB*C1*(EP(I+1)-EP(I))
      END DO

cj　連続式を解いて水位を計算する
      C2     = H*DELT/DELX
      DO I=2,IN 
        E3(I)=E1(I)-CNFB*C2*(UP(I)-UP(I-1))
      END DO

      RETURN
      END


************************************************************************
* init.f
* TASK
cj 初期条件の設定
* USAGE
* REMARK
* SLAVE ROUTINE
* REFERENCES
************************************************************************
      subroutine INIT(IN,U1,UP,U3,UB,E1,EP,E3,EB)
*------------------------- PARAMETERS ----------------------------------
      include 'param.h'
*-----------------------------------------------------------------------
*--------------------------- ARGUMENTS ---------------------------------
      DIMENSION U1(IM),UP(IM),U3(IM),UB(IM),E1(IM),EP(IM),E3(IM),EB(IM)
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
      COMMON /VALUE/H,DELT,DELX,VP
      character ictyp*3
      COMMON /RPARA2/ICTYP
      COMMON /IC/ef,eta0, prd,nmod
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
*-----------------------------------------------------------------------
      DO 10 I=1,IN
        U1(I)   = 0.
        UP(I)   = 0.
        U3(I)   = 0.
        UB(I)   = 0.
        E1(I)   = 0.
        EP(I)   = 0.
        E3(I)   = 0.
        EB(I)   = 0.
   10 CONTINUE

      XMX=float(IN)*delx

      IF(ICTYP.EQ.'sin')THEN
        DO I=1,IN
          EP(I)=0.0
        END DO
      ELSE IF(ICTYP.EQ.'gau')THEN
cj ガウス型の山の形の初期水位を与える
        XC=FLOAT(1)*delx
        DO I=2,IN
          D=float(I)*delx - xc  ! 山の中心からの距離
          EP(I)=eta0*exp(-D**2/ef**2)
        END DO
      ELSE IF(ICTYP.EQ.'std')THEN
        DO I=2,IN
          EP(I)=eta0*sin(PI*FLOAT(NMOD)/XMX*delx*float(i))
        END DO
      ELSE IF(ICTYP.EQ.'sch')THEN
        DO I=2,IN
          EP(I)=eta0*cos(PI*FLOAT(NMOD)/XMX*delx*float(i))
        END DO
      END IF
      return
      end

************************************************************************
* bc.f
* TASK
cj 境界条件の設定
* USAGE
* REMARK
* SLAVE ROUTINE
* REFERENCES
************************************************************************
      subroutine bc(IN,U1,UP,U3,E1,EP,E3)
*------------------------- PARAMETERS ----------------------------------
      include 'param.h'
*-----------------------------------------------------------------------
*--------------------------- ARGUMENTS ---------------------------------
      DIMENSION U1(IM),UP(IM),U3(IM),E1(IM),EP(IM),E3(IM)
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
      COMMON /VALUE/H,DELT,DELX,VP
      COMMON /SIZE/XMX
cj初期条件or境界条件
      COMMON /IC/ef,eta0, prd,nmod
cj 実行時必要となるパラメーター
      COMMON /RPARA/NSTEP
      character ictyp*3
      COMMON /RPARA2/ICTYP
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
*-----------------------------------------------------------------------
      IF(ICTYP.EQ.'sin')THEN
        PTIME=NSTEP*delt
        ep(2)=eta0*sin(2*PI/PRD*PTIME)
        UP(IN) =  VP*EP(IN)/H
      ELSE IF(ICTYP.EQ.'gau')THEN
       UP(1)=0.0
       UP(IN) =  VP*EP(IN)/H
      ELSE IF(ICTYP.EQ.'sch')THEN
        UP(1)=0.0
        UP(IN)=0.0
      ELSE IF(ICTYP.EQ.'std')THEN
        EP(2)=0.0
        EP(IN)=0.0
      END IF

      return
      end
************************************************************************
* output.f
* TASK
* USAGE
* REMARK
* SLAVE ROUTINE
* REFERENCES
************************************************************************
      subroutine OUTPUT(IN,EP,UP,IUE,IUU)
*------------------------- PARAMETERS ----------------------------------
      include 'param.h'
*-----------------------------------------------------------------------
*--------------------------- ARGUMENTS ---------------------------------
      dimension EP(IM),UP(IM)
cj IUE : etaの出力用機番
cj IUU : uの出力用機番
*-----------------------------------------------------------------------
*------------------------ COMMON VARIABLES -----------------------------
      COMMON /VALUE/H,DELT,DELX,VP
cj 実行時必要となるパラメーター
      COMMON /RPARA/NSTEP
*-----------------------------------------------------------------------
*------------------------- LOCAL VARIABLES -----------------------------
      character fname*14
*-----------------------------------------------------------------------
      time = FLOAT(NSTEP)*DELT
      write(fname,8) 'elev',int(time)
    8 format (a4,'-',i5.5,'.txt')
cj水位の出力
      OPEN(98,FILE=fname,status='unknown')
      write(98,'(a,f10.1,a)')'elevation : time = ',time,' [s]'
      write(98,'(a)')'X [km]    elevation [cm]'
      do i=1,IN,5
        X = FLOAT(I)*DELX/rk2c       ! cm -> km
        write(98,'(2f10.3)')X,ep(i) ! cm 
      end do
      CLOSE(98)

      return
      end

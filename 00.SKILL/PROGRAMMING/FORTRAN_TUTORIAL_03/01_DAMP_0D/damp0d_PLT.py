import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "damp0d_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, u":
        continue
    values = line.split()
    if len(values) == 3:
        t, u, ua= map(float, values)
        data.append((t, u, ua))

# NumPy 配列に変換
data = np.array(data)

# プロット
plt.figure(figsize=(8, 6))
plt.plot(data[:, 0], data[:, 1], linestyle="-", label="u(t)")
plt.xlabel("Time t")
plt.ylabel("u")
plt.title("Time Series Plot")
plt.legend()

# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + ".pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")

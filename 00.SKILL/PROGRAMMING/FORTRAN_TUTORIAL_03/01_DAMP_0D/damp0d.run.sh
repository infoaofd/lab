#!/bin/bash

src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
c=0.1
t0=0.0
te=10.0
u0=1.0
dt=0.05
OUT=$(basename $0 .run.sh)_result.txt


# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then echo EEEEE No such file, $src; exit 1; fi



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
c=${c},
t0=${t0},
te=${te},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then
echo; echo EEEEE Compile error!; echo
echo Terminated.
exit 1
fi

# RUN THE PROGRAM
echo Running $exe ...
$exe < $nml
if [ $? -ne 0 ]; then
echo; echo ERROR in $exe: Runtime error!; echo
echo Terminated.
exit 1
else
echo Done.
fi

if [ -f ${OUT} ];then echo; echo OUT: $OUT; fi

program damp0d

character(len=5000)::OUT
real::c,t0, te, u0, dt

namelist /para/ c,t0, te, u0, dt, OUT

read(*,para)

n=(te-t0)/dt

open(20,file=OUT)

write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# c =',c
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, u'

t=t0; u=u0

write(20,'(f10.5,2e14.6)') t,u0,u0

do i=1,N
  t=dt*float(i)
  unew=u-c*u*dt                    !Time integration
  u=unew                           !Update
  ua=u0*exp(-c*t)                  !Analytical solution
  write(20,'(f10.5,2e14.6)')t,u,ua !Output solutions
enddo !i

end program damp0d

# 第1回 減衰方程式

[[_TOC_]]

## 方程式と初期条件

$u$を速度として $u=u(t)$とする。
$$
\begin{eqnarray}
\frac{d u}{dt}&=&-cu \tag{1} \\
\\
t=t_0 &\;で,\;& u=u_0 \tag{2}
\end{eqnarray}
$$
$c$ は定数で，摩擦係数と呼ばれることがある．$c$ の値によって系の振る舞いが変わる。しばしばパラメータ (parameter)と呼ばれる。

今回は，上記 (1) を (2) の条件のもとで解く。厳密解 (解析解) と 数値解 (後述) を比較する。

## 式の意味

(1) 式の右辺は，速度 $u$ の大きさが大きいほど，$u$ 減速させようとする働きがあることを示す。$c$ の値が大きければ大きいほど，減速の効果が大きくなる。

(2) はある特定の時刻 $t=t_0$ での $u$ の値を指定している。(2) 式が無いと，$u$ のグラフが1本に決まらない。

### 練習1

 (2) の条件のもとで (1) の厳密解 (解析解ともいう) を求める。$c=1$ とする。また，初期条件 $t=0$ のとき, $u=1$ とする。 

[1] $u=\exp(a x)$ を (1) に代入して，$a$ の値を求めよ。

[2] 初期条件 (2) を用いて，$u_0$ の値を求めよ。

[2] 横軸を $t$, 縦軸を $u$ とし，解析解のグラフの概形を示せ（手書きでもよい）。

 

## 数値解法

微分係数を下記のように近似して求めた解のことを**数値解**と呼ぶ。微分係数の近似法はいろいろあるが，今回は**差分法**と呼ばれる手法を用いる。

今回は差分法の中でも最も簡単なオイラー法（オイラー前進差分）を使う。$\Delta t$ が十分小さければ，(1) は下記 (3) のように近似できる。
$$
\frac{u(t+\Delta t)-u(t)}{\Delta t}=-c\,u(t) \tag{3}
$$
 となる。(3) の左辺を**差分商**と呼ぶことがある。

(3)の分母を払うと,
$$
u(t+\Delta t)=u(t)-c\,u(t)\,\Delta t \tag{4}
$$
(4)を初期条件，
$$
u(t=t_0)=u_0　\tag{5}
$$
のもとで解く。

### 解き方

アルゴリズム（**算法**）と呼ばれることがある。

1. パラメータ $c$, 計算開始時刻 $t_0$，計算終了時刻 $t_E$，時間刻みの値を $\Delta t$ の値を設定する

2. 初期条件(5)の値を設定する

3. 計算終了時刻と $\Delta t$ の値から, 計算回数 $n$ を求める。

4. 初期条件の値, $t_0$ と $u_0$ をファイルに出力する

5. $t+\Delta t$ の値を求める

6. (4)を使って, $u\,(t+\Delta t)$ の値を求める。

7.  $t+\Delta t$ と $u\,(t+\Delta t)$ の値を出力する

8.  $u\,(t+\Delta t)$ の値を $u(t)$ の値に置き換える（アップデートと呼ばれることがある)

9. 5から6までを $n$ 回繰り返す

## 文法事項

差分法のプログラムの解説に入る前に，Fortranの文法事項を一つ解説する。

### namelist

Fortranのネームリスト（NAMELIST）は、設定ファイルや標準入力からデータを簡単に読み込むための機能です。

#### Fortranプログラム（namelist_file.f90）

 このプログラムは `input.txt` というファイルからネームリストを読み込み、変数に格納して表示します。

```fortran
    ! 変数の宣言
    integer :: n
    real :: x
    character(len=10) :: name

    ! ネームリストの定義
    namelist /mylist/ n, x, name

    ! 初期値の設定
    n = 0; x = 0.0
    name = "default"

    ! ファイルを開く
    open(10, file="input.txt", action="read", iostat=ios)

    ! ネームリストの読み込み
    read(10, mylist, iostat=ios)
    close(10)

    ! 読み込んだ値を表示
    print *, "Read values:"
    print *, "n =", n
    print *, "x =", x
    print *, "name =", trim(name)
end program namelist_example
```

#### 解説

- `read(10, mylist, iostat=ios)`
   → ファイルからネームリストを読み込む。

#### ネームリストの設定ファイル（input.txt）

以下の内容を `input.txt` に保存してください：

```
&mylist
   n = 10,
   x = 2.71,
   name = "Example"
/
```

------

#### 実行手順

1. コンパイル

   > $ gfortran -o namelist_example namelist_file.f90

2. 実行

   > $ ./namelist_example

3. 出力例

   > Read values:
   >  n = 10
   >  x = 2.71
   >  name = Example



## 演習

[1] 末尾に添付したプログラム (damp0d.f90) を自分で入力し, 実行用シェルスクリプト (damp0d.run.sh) を使って，コンパイル・実行する。

### 実行例

> $ damp0d.run.sh

[2] 横軸を $t$ , 縦軸は $u$ とし、結果を図にする。下記のpythonスクリプト (python3 damp0d_PLT.py) を使ってもよい。

> $ python3 damp0d_PLT.py

下記のエラーが出たときは，

Traceback (most recent call last):
File "/work09/ma/00.SKILL/PROGRAMMING/FORTRAN_TUTORIAL_03/01_DAMP_0D/damp0d_PLT.py", line 2, in <module>
 import matplotlib.pyplot as plt
ModuleNotFoundError: No module named 'matplotlib'

次のコマンドを実行した後，もう一度描画用スクリプトを実行する。

> $ conda deactivate

[3] 練習1で求めた解析解と[1]の数値解との相対誤差をグラフにせよ。

末尾に記載した，damp0d_PLT_ERR.py を使ってよい。

[4] dt の値を dt =0.5に変更して, [1] から [3] を繰り返せ。結果について説明せよ。

[5] 使用したプログラム，スクリプトを全て読む。不明の点があれば，下記の手法で調べる。

- print文デバッグを行う (https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md)
- AIに分析させる

[6] プログラムの各文の後にコメント文を入れることで，プログラムの下記の部分の内容（何を行っているのか）を解説せよ。

```fortran
t=t0; u=u0

write(20,'(f10.5,2e14.6)') t,u0,u0

do i=1,N
  t=dt*float(i)
  unew=u-c*u*dt                    !Time integration
  u=unew                           !Update
  ua=u0*exp(-c*t)                  !Analytical solution
  write(20,'(f10.5,2e14.6)')t,u,ua !Output solutions
enddo !i
```

**AIの分析結果のコピーはしない**。AIの分析結果を踏まえ，print文デバッグを使って**自分で内容を確認する**。

print文デバッグについては，

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md

を参照のこと。

自分の頭で理解したことを，自分の言葉で説明する。

### プログラム例

#### damp0d.f90

```Fortran
program damp0d

character(len=5000)::OUT
real::c,t0, te, u0, dt

namelist /para/ c,t0, te, u0, dt, OUT

read(*,para)

n=(te-t0)/dt

open(20,file=OUT)

write(20,'(A,f10.5)')'# t0=',t0
write(20,'(A,f10.5)')'# te=',te
write(20,'(A,f10.5)')'# c =',c
write(20,'(A,f10.5)')'# dt=',dt
write(20,'(A,i10  )')'# n =',n
write(20,'(A      )')'# t, u'

t=t0; u=u0

write(20,'(f10.5,2e14.6)') t,u0,u0

do i=1,N
  t=dt*float(i)
  unew=u-c*u*dt                    !Time integration
  u=unew                           !Update
  ua=u0*exp(-c*t)                  !Analytical solution
  write(20,'(f10.5,2e14.6)')t,u,ua !Output solutions
enddo !i

end program damp0d
```

 

### 実行用スクリプト

#### damp0d.run.sh

```bash
#!/bin/bash

src=$(basename $0 .run.sh).f90
exe=$(basename $0 .run.sh).exe
nml=$(basename $0 .run.sh).nml

f90=ifort
opt="-CB -traceback -fpe0"

# SET PARAMETERS
c=0.1
t0=0.0
te=10.0
u0=1.0
dt=0.05
OUT=$(basename $0 .run.sh)_result.txt


# CHECK IF SOURCE FILE SURELY EXISTS.
if [ ! -f $src ]; then echo EEEEE No such file, $src; exit 1; fi



# CREATE NAMELIST FILE
cat <<EOF>$nml
&para
c=${c},
t0=${t0},
te=${te},
u0=${u0},
dt=${dt},
OUT="${OUT}",
&end
EOF



# COMPILE SOURCE FILE
echo Compiling $src ...
ifort $opt $src -o $exe
if [ $? -ne 0 ]; then
echo; echo EEEEE Compile error!; echo
echo Terminated.
exit 1
fi

# RUN THE PROGRAM
echo Running $exe ...
$exe < $nml
if [ $? -ne 0 ]; then
echo; echo ERROR in $exe: Runtime error!; echo
echo Terminated.
exit 1
else
echo Done.
fi

if [ -f ${OUT} ];then echo; echo OUT: $OUT; fi
```



### 描画用スクリプト

#### damp0d_PLT.py

数値解の時系列を書く

```python
import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "damp0d_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, u":
        continue
    values = line.split()
    if len(values) == 3:
        t, u, ua= map(float, values)
        data.append((t, u, ua))

# NumPy 配列に変換
data = np.array(data)

# プロット
plt.figure(figsize=(8, 6))
plt.plot(data[:, 0], data[:, 1], linestyle="-", label="u(t)")
plt.xlabel("Time t")
plt.ylabel("u")
plt.title("Time Series Plot")
plt.legend()

# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + ".pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")
```

### damp0d_PLT_ERR.py

数値解の相対誤差の時系列を書く

```python
import numpy as np
import matplotlib.pyplot as plt
import sys

# 入力ファイル名
filename = "damp0d_result.txt"

# ファイルの存在を確認
try:
    with open(filename, "r") as f:
        lines = f.readlines()
except FileNotFoundError:
    print(f"Error: File '{filename}' not found.")
    sys.exit(1)

# ヘッダーをスキップし、データを読み込む
data = []
for line in lines:
    if line.startswith("#") or line.strip() == "t, u":
        continue
    values = line.split()
    if len(values) == 3:
        t, u, ua= map(float, values)
        data.append((t, u, ua))

# NumPy 配列に変換
data = np.array(data)

# プロット
plt.figure(figsize=(8, 6))
plt.plot(data[:, 0], (data[:, 1]-data[:,2])/data[:,2]*100, 
    linestyle="-", label="u(t)")
plt.xlabel("Time t")
plt.ylabel("Relative Error of u [%]")
plt.title("Time Series Plot")
plt.legend()

# PDFに保存
# 出力ファイル名を設定
import os
output_file = os.path.splitext(filename)[0] + "_ERROR.pdf"
plt.savefig(output_file)
print(f"FIG: {output_file}")
```

## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

### 守破離

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。**同じ誤りを何度も繰り返す人は，自己判断でやるべきことを省略したことが原因で袋小路に入っている**ことが多い。

### エラーが出た時の対処法

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍も調べる。

#### 詳しい解説書一覧

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md
質問-詳しい解説書を探している

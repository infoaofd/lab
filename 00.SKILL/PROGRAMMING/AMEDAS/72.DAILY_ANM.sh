SRC=$(basename $0 .sh).F90
if [ ! -f $SRC ];then echo NO SUCH FILE,$SRC;exit 1;fi

INLIST=$(basename $0 .sh)_LIST.TXT
INDIR=12.CUT.DATA_OUT
NF=$(ls -1 $INDIR/*csv |wc|awk '{print $1}')
echo "NUMBER OF INPUT FILES = $NF"
echo $NF > $INLIST
ls -1 $INDIR/*csv >>$INLIST
cat $INLIST

EXE=$(basename $0 .sh).EXE

gfortran $SRC -o $EXE
if [ $? -eq 0 ]; then
$EXE
fi



INLIST="data1.csv data2.csv data3.csv"
ODIR=$(basename $0 .sh)_OUT
mkd $ODIR

YS=1993; YE=2023; Y=$YS
while [ $Y -le $YE ]; do

OUT=$ODIR/"Amedas_Tsu_$Y.csv"
README=$ODIR/0.$(basename $0 .sh)_README.TXT

pwd > $README
echo $0 >>$README
date -R >>$README

echo "time, temperature, t_quality, t_homog, precip, if_precip, precip, quality, precip_homog" > $OUT

for IN in $INLIST;do

awk -F'[/,]' -v Y=$Y '{ if($1==Y) printf \
"%04d/%02d/%02d,%6.1f\n", \
$1,$2,$3,$4}' $IN >>$OUT

done #IN

echo OUT: $OUT

Y=$(expr $Y + 1)
done #Y


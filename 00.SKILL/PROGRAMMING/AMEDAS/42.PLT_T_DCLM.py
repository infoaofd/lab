from matplotlib import pyplot as plt
import pandas as pd
import os

# CSVファイルをpandas.DataFrameとして読み込む
# index(見出し列)としてtimeの列を指定
# デフォルトでheader=0が指定されており1行目はヘッダーとして無視
INDIR="32.DAILY_CLM_OUT/"
INFLE="Amedas_DCLM_Tsu.CSV"
IN=INDIR+INFLE
data = pd.read_csv(IN, index_col='time',header=0)

# 行と列の抽出
# :は全部の行、'[0,1]'はindexとして指定した列を除き2列を抽出
#df = data.iloc[:, [0,1]]
df = data.iloc[:, [0]]
# データをプロット
df.plot()

# グラフのタイトル
plt.title("Tsu")

# グラフを表示
FIGDIR=os.path.basename(__file__)+"_FIG"
if not os.path.isdir(FIGDIR):
   os.mkdir(FIGDIR)
FIG=FIGDIR+"/Amedas_DCLM_Tsu.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)


#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=CWC; UNIT="[mg/kg]"

LONW=176; LONE=178; LATS=25; LATN=35
LEVB=1000; LEVT=300; LEV2=850

#CTL1="/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/2016053000/atm_snp_1hr.nc"
CTL1=atm_snp_1hr.ncctl
if [ ! -f $CTL1 ];then echo EEEEE NO SUCH FILE, $CTL1;exit 1;fi
FIG=$(basename $0 .sh).pdf

timestamp=$(date -R); host=$(hostname)
cwd=$(pwd); COMMAND="$0 $@"

KIND='-kind blue->cyan->dodgerblue->azure->(200,200,255)->white'
CLEV='0 60 5'

cat <<EOF>$GS
'open ${CTL1}'    ;#'sdfopen ${CTL1}'

'set vpage 0.0 8.5 0 10.5'

xmax=1; ymax=2

ytop=9

xwid =  7/xmax; ywid =  6/ymax
xmargin=0.5; ymargin=1

ymap=1; xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set lat ${LATS} ${LATN}'; 'set lev ${LEVB} ${LEVT}'
'set t 3'
'q dims';line=sublin(result,5);date=subwrd(line,6); say; say 'MMMMM 'date;say

'set lon ${LONW}'
'ucsc=ave(cwc,lon=${LONW},lon=${LONE})'

'color ${CLEV} ${KIND} -gxout shaded'
'set grid off'
'set ylint 100'
'd ucsc*1000*1000' ;# kg/kg -> mg/kg

'set xlab off'; 'set ylab off'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
x1=xl; x2=xr; y1=yb-0.5; y2=y1+0.1
'color ${CLEV} ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 3 -fs 1'

x=xl-0.7; y=(yt+yb)/2
'set strsiz 0.14 0.17'; 'set string 1 c 3 90'
'draw string 'x' 'y' P [hPa]'; 'set string 1 c 1 0'

x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.14 0.18'; 'set string 1 c 4 0'
'draw string 'x' 'y' 'date' ${VAR} ${UNIT} AVG OVER ${LONW}-${LONE}'


'set strsiz 0.08 0.1'; 'set string 1 l 3'
xx = xl+0.0; yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'
yy = yy+0.17; 'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'
yy = yy+0.17; 'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.17; 'draw string ' xx ' ' yy ' ${timestamp} ${host}'

'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

if [ -f $FIG ];then echo;echo MMMMM FIG: $FIG;echo;fi


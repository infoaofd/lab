#!/bin/bash

GS=$(basename $0 .sh).GS

VAR=U
UNIT="[m/s]"
MMM=DJF

LONW=176
LONE=178
LATS=25
LATN=35
LEVB=1000
LEVT=300
LEV2=850

CTL1="/work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/d_monit_a/2016053000/atm_snp_1hr.nc"
FIG=$(basename $0 .sh).pdf

timestamp=$(date -R)
host=$(hostname)
cwd=$(pwd)
COMMAND="$0 $@"

KIND='-kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
#KIND='-kind blue->skyblue->white->orange->red'
CLEV='-2 2 0.4'



cat <<EOF>$GS
'sdfopen ${CTL1}'





'set vpage 0.0 8.5 0 10.5'

xmax=1
ymax=2

ytop=9

xwid =  7/xmax
ywid =  6/ymax
xmargin=0.5
ymargin=1

ymap=1
xmap=1


xs = 1 + (xwid+xmargin)*(xmap-1)
xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1)
ys = ye - ywid

'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set lon ${LONW} ${LONE}'
'set lat ${LATS} ${LATN}'
'set lev ${LEVB} ${LEVT}'
'set t 1'
'set lon ${LONW}'
'ucsc=ave(cwc,lon=${LONW},lon=${LONE})'

'color ${CLEV} ${KIND} -gxout shaded'
'set grid off'
'd ucsc'

'set xlab off'
'set ylab off'

'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)
x1=xl
x2=xr
y1=yb-0.5
y2=y1+0.1
'color ${CLEV} ${KIND} -xcbar 'x1' 'x2' 'y1' 'y2' -ft 2 -fs 1'


'set gxout contour'
'set ccolor 0'
'set cthick 1'
'set clevs ${CLEV}'
'set clab off'
'set xlab off'
'set ylab off'
'd ucsc'


x=xl-0.7
y=(yt+yb)/2
'set strsiz 0.14 0.17'
'set string 1 c 3 90'
'draw string 'x' 'y' P[hPa]'
'set string 1 c 1 0'

'draw title ${MMM} ${VAR} ${UNIT} AVG OVER ${LONW}-${LONE}'



say
say 'PRINT HEADER LINES'
say
'q gxinfo'
line=sublin(result,3)
xl=subwrd(line,4)
xr=subwrd(line,6)
line=sublin(result,4)
yb=subwrd(line,4)
yt=subwrd(line,6)


'set strsiz 0.08 0.1'
'set string 1 l 2'

xx = xl+0.0

yy = yt+0.6
'draw string ' xx ' ' yy ' ${FIG}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${cwd} ${COMMAND}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${GS}'

yy = yy+0.17
'draw string ' xx ' ' yy ' ${timestamp} ${host}'




'gxprint $FIG'
'quit'
EOF

grads -bcp "$GS"
echo

ls -lh --time-style=long-iso $FIG

echo "DONE $(basename $0) $@" .
echo

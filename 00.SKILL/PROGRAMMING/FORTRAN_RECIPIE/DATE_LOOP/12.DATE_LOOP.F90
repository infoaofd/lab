PROGRAM MAKE_HISTO_DATA
! Sat, 04 May 2024 12:01:28 +0900
! /work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/32.12.HISTO_P1H/12.12.TEST_READ

integer :: id_beg(5) = (/2021, 8, 12, 0, 0/)
integer :: id_end(5) = (/2021, 8, 15, 0, 0/)
real    :: slon, elon, slat, elat
parameter (slon = 129., elon = 132., slat=31., elat = 34.)
real :: DT_FILE=3600.0  ![sec]
real   , parameter :: daysec = 86400.0 ! [sec]
real(8) :: jd_beg, jd_end, jd


iyr = id_beg(1); mon = id_beg(2); idy = id_beg(3); ihr = id_beg(4)
imin =id_beg(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_beg)
call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd_beg)

iyr = id_end(1); mon = id_end(2); idy = id_end(3); ihr = id_end(4)
imin =id_end(5); isec = 0
call date2jd(iyr,mon,idy,ihr,imin,isec,jd_end)
call jd2date(iyr2,mon2,idy2,ihr2,imin2,isec2,jd_end)

print *,'MMMMM START ',iyr1,mon1,idy1,ihr1
print *,'MMMMM END   ',iyr2,mon2,idy2,ihr2

jd = jd_beg
MAIN_TIME_LOOP: DO 

jd = jd + (dt_file/daysec)
call jd2date(iyr1,mon1,idy1,ihr1,imin1,isec1,jd)
print *,'MMMMM ',iyr1,mon1,idy1,ihr1

IF (jd >= jd_end) EXIT
END DO MAIN_TIME_LOOP

END PROGRAM MAKE_HISTO_DATA

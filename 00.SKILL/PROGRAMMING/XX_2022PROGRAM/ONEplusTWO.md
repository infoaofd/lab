# ONEplusTWO

```
$ cat exit.s
```

```assembly
.intel_syntax noprefix
.global main

main:
        mov rax,0x3c
        syscall
```

```bash
$ gfortran -c exit.s
$ ld -e main -o exit.bin exit.o
$ exit.bin
```


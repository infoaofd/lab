/work03/gu1/LAB/2022_PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON
Thu, 09 Jun 2022 21:07:51 +0900

ga-> q ctlinfo
dset hgt.mon.mean.nc
title monthly mean hgt from the NCEP Reanalysis
undef -9.96921e+36
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 17 levels 1000 925 850 700 600 500 400 300
 250 200 150 100 70 50 30 20 10
tdef 892 linear 00Z01JAN1948 1mo
vars 1
hgt=>hgt  17  t,z,y,x  Monthly mean geopotential height
endvars



ga-> sdfopen hgt.season.1958-2005.nc
Scanning self-describing file:  hgt.season.1958-2005.nc
SDF file hgt.season.1958-2005.nc is open as file 1
LON set to 0 360 
LAT set to -90 90 
LEV set to 0 0 
Time values set: 1958:1:1:0 1958:1:1:0 
E set to 1 1 
ga-> q ctlinfo
dset hgt.season.1958-2005.nc
title 
undef -9.96921e+36
dtype netcdf
xdef 144 linear 0 2.5
ydef 73 linear -90 2.5
zdef 1 linear 0 1
tdef 48 linear 00Z01JAN1958 12mo
vars 1
z500=>z500  0  t,y,x  DJF: Monthly mean geopotential height
endvars


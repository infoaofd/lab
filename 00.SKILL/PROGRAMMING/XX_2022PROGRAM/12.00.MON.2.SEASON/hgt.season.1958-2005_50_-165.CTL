dset ^hgt.season.1958-2005_50_-165.BIN
title Z500 DJF NCEP1
undef -9.96921e+36
xdef 1 linear 0 2.5
ydef 1 linear -90 2.5
zdef 1 linear 0 1
tdef 48 linear 00Z01JAN1958 12mo
vars 1
z500 0 99 GPH 500 hPa [m]
endvars

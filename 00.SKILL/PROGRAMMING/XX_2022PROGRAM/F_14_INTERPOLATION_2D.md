# バイリニア補間 (2次元)

[[_TOC_]]

## 目標

**バイリニア補間の計算手順の確認**

**プログラム作成手順の復習**

**計算結果の確認方法の習得**

## FAQ（よくある質問）

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## 質問について

質問がある場合，事前に下記を電子ファイルにまとめて，googleドライブにアップロードし，口頭で説明できるように準備する

- 資料のどの箇所に関する質問か
- エラーメッセージのコピー
- エラーメッセージをgoogle検索した結果で有益と思われるもの
- AIの回答結果のうち有益と思われるもの

打ち合わせ時に上記に対して追加説明を行う。

## 計算手順の確認

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/2022PROGRAM/F_08-09_INTERPOLATION_SP.pdf

## 準備

### インテル・フォートラン

```
$ grep -n ift /etc/bashrc
108:alias ift='source /opt/intel/oneapi/setvars.sh'
```

```
$ grep -n ift $HOME/.bashrc
19:alias ift='source /opt/intel/oneapi/setvars.sh'
```

```
$ ift
 
:: initializing oneAPI environment ...
   -bash: BASH_VERSION = 4.2.46(2)-release
   args: Using "$@" for setvars.sh arguments: 

:: compiler -- latest

:: oneAPI environment initialized ::
```



### コピー

### コマンド確認

```
$ which mkd
~/mybin/mkd
```

```
$ cat $(which mkd)
```

```
$ less $(which mkd)
```

```
$ which myymdh
~/mybin/myymdh
```

```
$ cat $(which myymdh)
```

```
$ less $(which myymdh)
```

### ディレクトリ作成

```
$ ll $HOME/00.TOOLS/ANALYSIS
```

```
$ mkd $HOME/00.TOOLS/ANALYSIS
```

```
$ cd $HOME/00.TOOLS/ANALYSIS
```

### 使用ファイルコピー

```
$ cp -ar /work03/am/LAB/00.SKILL/ANALYSIS/2022-10-07_09_2022-10-07_09_BILINEAR .
```



## 1. バイリニア補間のプログラム作成

### プログラム作成

1. 設計

2. 設定

3. データ読み込み

4. 計算

5. 計算結果表示

   

### 1. 設計

#### 各データの位置関係を決める

```
! Subscript, A indicates the input.

! For given XA(i),YA(i),ZA(i),X & Y, Z is estimated.
!
! (XA(4),YA(4),ZA(4))   (XA(3),YA(3),ZA(3))
!        +------------------+
!        |                  |
!        |    X             |
!        | (X,Y,Z)          |
!        |                  |
!        |                  |
!        |                  |
!        +------------------+
! (XA(1),YA(1),ZA(1))   (XA(2),YA(2),ZA(2))
```



#### 計算式の確認

##### 内分点を決める定数, T, U

```FORTRAN
T=(X-XA(1))/(XA(2)-XA(1))
U=(Y-YA(1))/(YA(4)-YA(1))
```

##### バイリニア補間

```
 Z =  (1.-T)*(1.-U)*ZA(1) &
&  + T*(1.-U)*ZA(2) &
&  + T*U*ZA(3) &
&  + (1.-T)*U*ZA(4)
```



### 2. 設定

```
$ vi BI.F90
```

骨格

```
PROGRAM BI

END PROGRAM BI
```

変数宣言

```
PROGRAM BI
REAL,DIMENSION(4)::XA,YA,ZA
REAL X, Y
REAL Z

END PROGRAM BI
```



### 2. データ読み込み



```
PROGRAM BI
REAL,DIMENSION(4)::XA,YA,ZA
REAL X, Y
REAL Z

DO I=1,4
READ(5,*)XA(I),YA(I),ZA(I)
END DO !I
READ(5,*)X, Y

END PROGRAM BI
```

```
ESC wq
```

もしくは

```
jk wq
```

で一旦終了

### 3. 計算

計算の主要部を独立させる

サブルーチンと呼ばれる機能を用いる

サブルーチン = SUB + ROUTINE

```
$ vi BILINEAR.F90
```



```
SUBROUTINE BILINEAR(XA,YA,ZA, X,Y, Z)
REAL, INTENT(IN) :: XA(4), YA(4), ZA(4)
REAL, INTENT(IN) :: X,Y
REAL, INTENT(OUT) :: Z

REAL T, U
T=(X-XA(1))/(XA(2)-XA(1))
U=(Y-YA(1))/(YA(4)-YA(1))

 Z =  (1.-T)*(1.-U)*ZA(1) &
&  + T*(1.-U)*ZA(2) &
&  + T*U*ZA(3) &
&  + (1.-T)*U*ZA(4)

END SUBROUTINE BILINEAR
```



### 4. サブルーチン呼び出しと計算結果表示

```
PROGRAM BI
REAL,DIMENSION(4)::XA,YA,ZA
REAL X, Y
REAL Z

DO I=1,4
READ(5,*)XA(I),YA(I),ZA(I)
END DO !I
READ(5,*)X, Y

CALL BILINEAR(XA,YA,ZA, X,Y, Z)

DO I=1,4
WRITE(6,*)XA(I),YA(I),ZA(I)
END DO !I
WRITE(6,*)X, Y, Z

END PROGRAM BI
```

ESC wq

jk wq

で終了



## 2. 入力データ作成

```
$ vi INPUT.TXT 
```





## 3. コンパイル

```
$ ifort BI.F90 BILINEAR.F90 -o BI.exe

$ ll BI.exe
```

 

## 4. 実行

```
$ BI.exe < INPUT.TXT
```



## よくある質問

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/FORTRAN_FAQ.md

## プログラムが動作しないときの対処法

- https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/PROGRAMMING/DEGBUG/BASICS/12.12.DEBUG_BASICS.md
- https://gitlab.com/infoaofd/lab/-/tree/master/DEGBUG
- https://macoblog.com/programming-debug-kotsu/

リンク切れの場合は「デバッグ　方法　プログラミング」で検索すれば，同様の内容を記載したサイトが容易に見つかる。

## 上達のためのポイント

**エラーが出た時の対応の仕方でプログラミングの上達の速度が大幅に変わる**。

コンピューターのエラーについては，まずエラーメッセージをgoogle検索するとともに， AIに質問する。かならず何らかの手掛かりが得られる。

その後の対応については，下記を参考にすること。

ポイントは次の3つである

1. エラーメッセージをよく読む
2. エラーメッセージを検索し，ヒットしたサイトをよく読む
3. 変数に関する情報を書き出して確認する

エラーメッセージは，プログラムが不正終了した直接の原因とその考えられる理由が書いてあるので，よく読むことが必要不可欠である。

記述が簡潔なため，内容が十分に理解できないことも多いが，その場合**エラーメッセージをブラウザで検索**してヒットした記事をいくつか読んでみる。

エラーの原因だけでなく，**考えうる解決策が記載されている**ことも良くある。

エラーを引き起こしていると思われる箇所の**変数の情報**や**変数の値そのものを書き出して**，**期待した通りにプログラムが動作しているか確認する**ことも重要である。

エラーメッセージや検索してヒットするウェブサイトは英語で記載されていることも多いが，**重要な情報は英語で記載されていることが多い**ので，よく読むようにする。

重要そうに思われるが，一回で理解できないものは，PDFなどに書き出して後で繰り返し読んでみる。どうしても**内容が頭に入らないものは印刷してから読む**。

一方，検索結果 (AIの回答）は，一部正しくないことや，ポイントのずれていることも多い。常に書いてあることが納得できるか，よく考える。正しいと思われることと，不明な点ははっきり分けて記録しておく（例えば，不明な点は青色にする）。

上記のように検索ページ，AIは正しくない情報を提供することも多いので，これらだけに頼らない。書籍・論文も調べる。

初めのうちは教科書や資料に書いてある通りに書いてある順番でやってみる。同じ誤りを何度も繰り返す人の多くは，自己判断でやるべきことを省略したことが原因で袋小路に入っていることが多い。

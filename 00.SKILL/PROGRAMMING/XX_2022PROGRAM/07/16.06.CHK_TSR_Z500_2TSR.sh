#!/bin/bash
# Sun, 06 Oct 2024 16:21:34 +0900
# /work09/$(whoami)/PROGRAM/FORTRAN/2022PROGRAM/07

INDIR=/work09/ma/PROGRAM/FORTRAN/2022PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON
CTL=$INDIR/hgt.season.1958-2005.CTL
if [ ! -f $CTL ];then echo NO SUCH FILE,$CTL;exit 1;fi

RLON1=200;RLAT1=20 # REF POINT
RLON2=205;RLAT2=43; COL=4; CNAME=BLUE
RLON2=249;RLAT2=58; COL=2; CNAME=RED

TIME1=00Z01JAN1958; TIME2=00Z01JAN2005

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${RLON2}_${RLAT2}_${CNAME}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

cat << EOF > ${GS}

'open ${CTL}'

# SET PAGE
'set vpage 0.0 8.5 0.0 11'

ytop=9
xmax = 1; ymax = 1
xwid = 5.0/xmax; ywid = 5.0/ymax
xmargin=0.5; ymargin=0.1

ymap = 1
#while (ymap <= ymax)
xmap = 1
#while (xmap <= xmax)
nmap = 1

xs = 1.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
'set parea 'xs ' 'xe' 'ys' 'ye

'cc'
'set grid off';'set grads off'
'set z 1'

'set time ${TIME1}'
'set lon $RLON1'; 'set lat $RLAT1'
'ZAVE=ave(z500,time=${TIME1},time=${TIME2})'
'ZSD=sqrt(ave(pow(z500-ZAVE,2),time=${TIME1},time=${TIME2}))'
say 3
'set ccolor 1';'set cthick 3';'set cmark 0'
'set vrange -3 3';'set ylint 1'
'set time ${TIME1} ${TIME2}'
'd (Z500-ZAVE)/ZSD'
'set xlab off';'set ylab off'



'set time ${TIME1}'
'set lon $RLON2'; 'set lat $RLAT2'
'ZAVE=ave(z500,time=${TIME1},time=${TIME2})'
'ZSD=sqrt(ave(pow(z500-ZAVE,2),time=${TIME1},time=${TIME2}))'

'set ccolor $COL';'set cthick 4';'set cmark 0'
'set vrange -3 3'
'set time ${TIME1} ${TIME2}'
'd (Z500-ZAVE)/ZSD'




# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3);xl=subwrd(line3,4); xr=subwrd(line3,6);
line4=sublin(result,4);yb=subwrd(line4,4);yt=subwrd(line4,6)

# LEGEND COLOR BAR
# x1=xl; x2=xr-0.5; y1=yb-0.5; y2=y1+0.1
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=x2+0.1; y=y1-0.12
#'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
#'draw string 'x' 'y' ${UNIT}'

# x1=xr+0.4; x2=x1+0.1; y1=yb; y2=yt-0.5
#'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs $FS -ft 3 -line on -lc 0 -edge square'
# x=(x1+x2)/2; y=y2+0.2
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
#'draw string 'x' 'y' ${UNIT}'

# TEXT
#'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
# x=xl ;# (xl+xr)/2; y=yt+0.2
#'draw string 'x' 'y' ${TEXT}'

# HEADER
'set strsiz 0.1 0.12'; 'set string 1 l 3 0'
xx = 0.1; yy=yt+0.5; 'draw string ' xx ' ' yy ' ${INFLE}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${INDIR}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CWD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2; 'draw string ' xx ' ' yy ' ${NOW}'

'gxprint ${FIG}'

#say; say 'MMMMM NetCDF OUT'
#'define VAROUT = VAR'
#'set sdfwrite '
#'sdfwrite VAROUT'
#say 'MMMMM DONE.'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo

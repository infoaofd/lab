program READ_Z500_DJF

character INDIR*500,INFLE*500,IN*1000
integer::IM=144,JM=73,NM=48
real,allocatable::z(:,:,:)

INDIR="/work03/gu1/LAB/2022_PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON"
INFLE="hgt.season.1958-2005.BIN"
IN=trim(INDIR)//"/"//trim(INFLE)

allocate(z(IM,JM,NM))

isize=IM*JM*4

open(11,file=IN,action="read",form="unformatted",access="direct",&
recl=isize)

irec=0
do n=1,NM
irec=irec+1
print *,irec
read(11,rec=irec)Z(:,:,n)
end do !n

end program READ_Z500_DJF


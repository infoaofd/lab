program CALC_R1P

character INDIR*500,INFLE*500,IN*1000, OFLE*500
integer::IM=144,JM=73,NM=48
real,allocatable::z(:,:,:),CORR(:,:)
real,allocatable,dimension(:)::Z0, Z1,Z0_AN,Z1_AN
integer I0,J0

INDIR="/work09/ma/PROGRAM/FORTRAN/2022PROGRAM/12.12.NCEP_PREPRO/12.00.MON.2.SEASON"
INFLE="hgt.season.1958-2005.BIN"
IN=trim(INDIR)//"/"//trim(INFLE)

OFLE="hgt.DJF.CORR.BIN"

allocate(z(IM,JM,NM), Z0(NM), Z1(NM), Z0_AN(NM), Z1_AN(NM))
allocate(CORR(IM,JM))

I0=INT(200/2.5); J0=INT(110/2.5)
! 160W=200E
! 20N=110 (LAT OF SP := -90)
print *,"I0=",I0,"J0=",J0

isize=IM*JM*4

open(11,file=IN,action="read",form="unformatted",access="direct",&
recl=isize)

irec=0
do n=1,NM
irec=irec+1
read(11,rec=irec)Z(:,:,n)
end do !n

Z0(:)=Z(I0,J0,:)
! print *,Z0
Z0_AV=SUM(Z0)/FLOAT(NM)
Z0_AN(:)=Z0(:)-Z0_AV

do j=1,JM
do i=1,IM

Z1(:)=Z(I,J,:)

Z1_AV=SUM(Z1)/FLOAT(NM)
Z1_AN(:)=Z1(:)-Z1_AV

Z0_DOT_Z1 = DOT_PRODUCT(Z0_AN, Z1_AN)
Z0MAG2 = DOT_PRODUCT(Z0_AN, Z0_AN)
Z1MAG2 = DOT_PRODUCT(Z1_AN, Z1_AN)
Z0MAG=SQRT(Z0MAG2)
Z1MAG=SQRT(Z1MAG2)

CORR(i,j)=Z0_DOT_Z1/(Z0MAG*Z1MAG)

!print *,I,J,Z0_DOT_Z1,Z0MAG,Z1MAG,CORR(i,j)

end do !i
end do !j


open(21,file=OFLE,form="unformatted",access="direct",&
recl=isize)
irec=1
write(21,rec=irec)CORR
close(21)

print *;print '(A,A)','OUTPUT: ',trim(OFLE)

end program CALC_R1P


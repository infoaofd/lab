```bash
$ cat fizzbuzz.s 
```

```assembly
# 実行方法
# $ gcc fizzbuzz.s -c -o fizzbuzz.o
# $ ld -e main -o fizzbuzz.bin fizzbuzz.o
# $ ./fizzbuzz.bin 

.intel_syntax noprefix
.global main

main:
    mov rcx, 0x1
loop:
    # fizz
    # カウンタレジスタrcxを3で割る
    mov rdx, 0
    mov rax, rcx
    mov r15, 3
    div r15
    # 余りが0以外（= 3の倍数ではない）のときにskipfizzにジャンプする
    cmp rdx, 0
    jne skipfizz

    # fizzを出力する
    mov rdx, 0x4
    lea rsi, [fizz]
    mov rdi, 0x1
    mov rax, 0x1
    push rcx
    syscall
    pop rcx
skipfizz:
    # buzz
    # カウンタレジスタrcxを5で割る
    mov rdx, 0
    mov rax, rcx
    mov r15, 5
    div r15
    # 余りが0以外（= 5の倍数ではない）のときにskipbuzzにジャンプする
    cmp rdx, 0
    jne skipbuzz

    # buzzを出力する
    mov rdx, 0x4
    lea rsi, [buzz]
    mov rdi, 0x1
    mov rax, 0x1
    push rcx
    syscall
    pop rcx
skipbuzz:
    inc rcx
    # 改行を出力する
    mov rdx, 0x1
    lea rsi, [newline]
    mov rdi, 0x1
    mov rax, 0x1
    push rcx
    syscall
    pop rcx
    # カウンタレジスタrcxが0x10と等しくなったとき、exitにジャンプする
    cmp rcx, 0x10
    jz exit
    jmp loop
exit:
    # exitシステムコールを呼ぶ
    mov rax, 0x3c
    syscall
    
fizz:
    .ascii "fizz"
buzz:
    .ascii "buzz"
newline:
    .ascii "\n"
```



```bash
$ gcc fizzbuzz.s -c -o fizzbuzz.o
```

```bash
$ ld -e main -o fizzbuzz.bin fizzbuzz.o 
```

```bash
$ ./fizzbuzz.bin 


fizz

buzz
fizz


fizz
buzz

fizz


fizzbuzz
```


# -*- coding: utf-8 -*-
"""
参考にしたウェブサイト
https://qiita.com/inashiro/items/c59e31b0f0557a7a8bca
"""

# ライブラリのインポート
import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import font_manager, rcParams

IN="01.GAUSSIAN.TXT" #入力ファイル名

# 入力ファイルが存在するか確認
if not (os.path.exists(IN)):
    print("NO SUCH FILE,"+IN)
    exit()

# フォントのパス
font_path = '/usr/share/fonts/BIZ_UDGothic/BIZUDGothic-Regular.ttf'
# フォントが存在するかチェック
if not os.path.exists(font_path):
    print(f"フォントが見つかりません: {font_path}")
    sys.exit(1)
# フォントを設定
font_manager.fontManager.addfont(font_path)
font_properties = font_manager.FontProperties(fname=font_path)
rcParams['font.family'] = font_properties.get_name()

#1列目のデータをx,2列目のデータをyに入れる
x,y = np.loadtxt(IN,unpack="true")

# グラフを書く
fig = plt.figure(figsize=(6,6))
ax = fig.add_subplot(111)
ax.plot(x, y, "-", color="k", label="Gaussian")
ax.set_xlim(-3.5, 3.5)
ax.set_ylim( 0.0, 1.2)
ax.set_xlabel("x")
ax.set_ylabel("度数")
ax.set_title('正規分布')

# 図の保存
# スクリプト名を出力ファイル名につかう
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG); print("")

# -*- coding: utf-8 -*-

# ライブラリのインポート
import os
import numpy as np
import matplotlib.pyplot as plt
from scipy import stats

# -6 から 6　の区間で100列の一次元配列を作成
x = np.linspace(-6, 6, 100)

# y=0の関数を作成（後で範囲の塗り潰しに使う）
y = 0

# 正規分布の両側90％点
t = stats.norm.ppf(0.95) #1-(1-0.95)/2)

# サブプロット（箱）を用意
fig, ax = plt.subplots(1, 1)

# 自由度3のt分布をプロット
ax.plot(x, stats.norm.pdf(x))

# 範囲の塗り潰し
ax.fill_between(x, stats.norm.pdf(x), y, where=x < -t, facecolor='lime', alpha=0.5)
ax.fill_between(x, stats.norm.pdf(x), y, where=x > t, facecolor='lime', alpha=0.5)

# tの位置にラベルを追加
ax.text( t, -0.02, '$t$', color='black', fontsize=16, ha='center')
ax.text(-t, -0.02, '$-t$', color='black', fontsize=16, ha='center')

plt.xlim(-6, 6)
plt.ylim(0, 0.4)

ax.set_xticks([]) # 横軸の数値を消す

ax.set_xlabel("$x$", fontsize=14)
ax.set_ylabel("$f\,(x)$", fontsize=14)
ax.set_title('Probability density of Gaussian Distribution')

# 図の保存
# スクリプト名を出力ファイル名につかう
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG); print("")


# 平均と分散

[[_TOC_]]

## 平均と偏差

$\boldsymbol{x}=(x_1,x_2,\cdots)$, $\boldsymbol{y}=(y_1,y_2,\cdots)$ 

$\overline{x}\equiv $ $\boldsymbol{x}$ の平均 ，$\overline{y}\equiv  $ $\boldsymbol{y}$ の平均

$x'\equiv $ $\boldsymbol{x}$ の偏差 $(x_1-\overline{x},\;x_2-\overline{x},\;\cdots)$

$y'\equiv $ $\boldsymbol{y}$ の偏差 $(y_1-\overline{y},\;y_2-\overline{y},\;\cdots)$

## 共分散と相関係数

$\boldsymbol{x}$'と $\boldsymbol{y}'$ の共分散 $\text{COV}$
$$
\text{COV}(\boldsymbol{x}',\boldsymbol{y}')\equiv\frac{\boldsymbol{x}'\cdot\boldsymbol{y}'}{|\boldsymbol{x}'||\boldsymbol{y}'|}
$$
$\boldsymbol{x}$'と $\boldsymbol{y}'$ の相関係数 $R=\sqrt{\text{COV}(\boldsymbol{x}',\boldsymbol{y}')}$ 

## 分散の加法性

> $\text{COV}(\boldsymbol{x}',\boldsymbol{y}')=0$ のとき，
>
> $\boldsymbol{x}'+\boldsymbol{y}'$ の分散 = $\boldsymbol{x}'$の分散 + $\boldsymbol{y}'$ の分散
>
> $\boldsymbol{x}'-\boldsymbol{y}'$ の分散 = $\boldsymbol{x}'$の分散 + $\boldsymbol{y}'$ の分散

$\boldsymbol{x}$'と $\boldsymbol{y}'$ が無相関であれば，

- $\boldsymbol{x}$'と $\boldsymbol{y}'$ の和
- $\boldsymbol{x}$'と $\boldsymbol{y}'$ の差

の分散は， $\boldsymbol{x}'$の分散 と $\boldsymbol{y}'$ の分散を足せばよい


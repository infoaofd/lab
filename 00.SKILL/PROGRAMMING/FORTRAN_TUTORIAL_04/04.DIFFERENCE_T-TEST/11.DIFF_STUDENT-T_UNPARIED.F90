! Studentのt‐検定 (等分散を仮定)
! 対応の無い標本
!https://bellcurve.jp/statistics/course/9446.html
INTEGER :: NX,NY
REAL,ALLOCATABLE,DIMENSION(:)::X,Y
CHARACTER OUT1*1000, OUT2*1000, CNU*3
REAL::t_dist,gamma,nu, T_VALUE, PROB, DX
REAL XN,YN,XAV, YAV, XVR, YVR, XSD, YSD
INTEGER SEED
REAL:: R1, R2
REAL,PARAMETER::PI=3.141592653589793

INTEGER, ALLOCATABLE :: seed_array(:)
INTEGER :: seed_size

NAMELIST /para/NX,NY,XAV_IN,YAV_IN,XSD_IN,YSD_IN

OPEN(10,FILE='10.T-TEST_NML.TXT',ACTION="READ")
READ(10,NML=para)

OUT1 = '11.DIFF_STUDENT-T_UNPARIED_X.TXT'
OUT2 = '11.DIFF_STUDENT-T_UNPARIED_Y.TXT'

XN=FLOAT(NX);YN=FLOAT(NY)
ALLOCATE(X(NX),Y(NY))

! Xの正規分布乱数を生成
SEED = 1; CALL RANDOM_SEED(SEED)
!CALL RANDOM_SEED()

DO I = 1, NX, 2
    CALL RANDOM_NUMBER(R1)
    CALL RANDOM_NUMBER(R2)
    ! Box-Muller法
    X(I) = SQRT(-2.0 * LOG(R1)) * COS(2.0 * PI * R2)
    IF (I + 1 .LE. NX) THEN
        X(I + 1) = SQRT(-2.0 * LOG(R1)) * SIN(2.0 * PI * R2)
    END IF
END DO

! Yの正規分布乱数を生成
SEED = 2; CALL RANDOM_SEED(SEED)
!CALL RANDOM_SEED()

DO I = 1, NY, 2
    CALL RANDOM_NUMBER(R1)
    CALL RANDOM_NUMBER(R2)
!PRINT *, "R1: ", R1, " R2: ", R2 !DEBUG
    ! Box-Muller法
    Y(I) = SQRT(-2.0 * LOG(R1)) * COS(2.0 * PI * R2)
    IF (I + 1 .LE. NY) THEN
        Y(I + 1) = SQRT(-2.0 * LOG(R1)) * SIN(2.0 * PI * R2)
    END IF
END DO

! 与えたい標準偏差の値をかけて，与えたい平均の値を足す。
DO I=1,NX
X(I)=XAV_IN+X(I)*XSD_IN
END DO !I

DO I=1,NY
Y(I)=YAV_IN+Y(I)*YSD_IN
END DO !I

!PRINT '(A)','MMMMM DEBUG'
!DEALLOCATE(X,Y)
!XN=7.0;YN=XN
!NX=INT(XN);NY=INT(YN)
!ALLOCATE(X(NX),Y(NY))
!X=[49.0, 53.0, 52.0, 50.0, 50.0, 55.0, 52.0]
!Y=[56.0, 58.0, 57.0, 59.0, 55.0, 53.0, 57.0]
!PRINT '(10F5.0)',(X(I),I=1,NX)
!PRINT '(10F5.0)',(Y(I),I=1,NY)

XAV = SUM(X) / XN ! 平均
YAV = SUM(Y) / YN

XVR = 0.0
DO I = 1, NX
  XVR = XVR + (X(I) - XAV)**2
END DO
XVR=XVR / (XN-1.0)
XSD = SQRT(XVR)

YVR = 0.0
DO I = 1, NY
  YVR = YVR + (Y(I) - YAV)**2
END DO !I
YVR=YVR / (YN-1.0)
YSD = SQRT(YVR)

! 出力ファイルに書き込み
OPEN(21, FILE=OUT1)
DO I = 1, NX
  WRITE(21, '(2F10.4)') X(I)
END DO
OPEN(22, FILE=OUT2)
DO I = 1, NY
  WRITE(22, '(2F10.4)') Y(I)
END DO

PRINT '(A,A)', 'MMMMM OUT1: ', TRIM(OUT1)
PRINT '(A,A)', 'MMMMM OUT2: ', TRIM(OUT2)

!DEBUG
!XN=30; YN=32
!XAV=75;YAV=70
!XSD=5;YSD=8
!XVR=XSD**2;YVR=YSD**2

! t値の計算
S2=((XN-1.0)*XVR+(YN-1.0)*YVR)/(XN+YN-2.0)
t_value = abs(XAV - YAV) /sqrt( S2/XN + S2/YN)

df=XN+YN-2.0 ! 自由度の計算

PRINT '(A)',"MMMMM STUDENT'S T-TEST (UNPAIRED SAMPLES)"
PRINT '(2(A,F6.0))', 'XN=',XN, ' YN=',YN
PRINT '(2(A,F7.2))', 'XAV=', XAV, ' YAV=', YAV
PRINT '(2(A,F7.2))', 'XAV_IN=', XAV_IN, ' YAV_IN=', YAV_IN
PRINT *
PRINT '(2(A,F7.2))', 'XVR=', XVR, ' YVR=', YVR
PRINT '(2(A,F7.2))', 'XSD=', XSD, ' YSD=', YSD
PRINT '(2(A,F7.2))', 'XSD_IN=', XSD_IN, ' YSD_IN=', YSD_IN
PRINT *


!Student's t probability (%)
prob_D=betai(0.5*df,0.5,df/(df+t_value**2))*100.0     !DOUBLE-SIDED
prob_S=betai(0.5*df,0.5,df/(df+t_value**2))*2.0*100.0 !SINGLE-SIDED


PRINT *
PRINT '(2(A,F7.2))', 'MMMMM T_VALUE=',t_value
PRINT '(2(A,F7.2))', 'MMMMM DEGREE OF FREEDOM, df =', df
PRINT '(2(A,G11.4))','MMMMM PROBABILITY [%] (DOUBLE SIDED), prob_D =', prob_D
PRINT '(2(A,G11.4))','MMMMM PROBABILITY [%] (SIGNLE SIDED), prob_S =', prob_S
END

! t-分布の積分値を求める
! C:\Users\boofo\Dropbox\TOOL\ANALYSIS\NUMERICAL_RECIPE
! C:\Users\boofo\Dropbox\01.TOOL\22.ANALYSIS\12.STATISTICS\12.TEST.STATISTICS\22.22.CORRELATION
! Numerical Recipes in Fortran 77
! The Art of Scientific Computing Second Edition 
! Volume 1 of Fortran Numerical Recipes
! p.207 gammln; p.220 betai; p.221 betacf

FUNCTION betai(a,b,x)
! RETURNS THE INCOMPLETE BETA FUNCTION Ix(a; b).
REAL betai,a,b,x
! USES betacf,gammln
REAL bt,betacf,gammln
if(x.lt.0..or.x.gt.1.)pause "bad argument x in betai"
if(x.eq.0..or.x.eq.1.)then
bt=0.
else ! Factors in front of the continued fraction.
bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.-x))
endif
if(x.lt.(a+1.)/(a+b+2.))then 
!Use continued fraction directly.
betai=bt*betacf(a,b,x)/a
return
else
betai=1.-bt*betacf(b,a,1.-x)/b 
!Use continued fraction after making the symmetry transformation. 
return
endif
END

FUNCTION betacf(a,b,x)
INTEGER MAXIT
REAL betacf,a,b,x,EPS,FPMIN
PARAMETER (MAXIT=100,EPS=3.e-7,FPMIN=1.e-30)
! Used by betai: Evaluates continued fraction for incomplete beta function by modified
! Lorentz's method (Section 5.2).
INTEGER m,m2
REAL aa,c,d,del,h,qab,qam,qap
qab=a+b 
!These q’s will be used in factors that occur in the coefficients (6.4.6). 
qap=a+1.
qam=a-1.
c=1. ! First step of Lentz’s method.
d=1.-qab*x/qap
if(abs(d).lt.FPMIN)d=FPMIN
d=1./d
h=d
do m=1,MAXIT
m2=2*m
aa=m*(b-m)*x/((qam+m2)*(a+m2))
d=1.+aa*d 
! One step (the even one) of the recurrence.
if(abs(d).lt.FPMIN)d=FPMIN
c=1.+aa/c
if(abs(c).lt.FPMIN)c=FPMIN
d=1./d
h=h*d*c
aa=-(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
d=1.+aa*d 
! Next step of the recurrence (the odd one).
if(abs(d).lt.FPMIN)d=FPMIN
c=1.+aa/c
if(abs(c).lt.FPMIN)c=FPMIN
d=1./d
del=d*c
h=h*del
if(abs(del-1.).lt.EPS)goto 1 !Are we done?
enddo
pause "a or b too big, or MAXIT too small in betacf"
1 betacf=h
return
END


FUNCTION gammln(xx)
REAL gammln,xx
! Returns the value ln[Γ(xx)] for xx > 0.
INTEGER j
DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
! Internal arithmetic will be done in double precision, a nicety 
! that you can omit if five-figure accuracy is good enough.
SAVE cof,stp
DATA cof,stp/76.18009172947146d0,-86.50532032941677d0, &
 24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2, &
-.5395239384953d-5,2.5066282746310005d0/
x=xx
y=x
tmp=x+5.5d0
tmp=(x+0.5d0)*log(tmp)-tmp
ser=1.000000000190015d0
do j=1,6
y=y+1.d0
ser=ser+cof(j)/y
enddo
gammln=tmp+log(stp*ser/x)
return
END


! Intel Fortran Compiler 18.0 Developer Guide and Reference
! https://jp.xlsoft.com/documents/intel/compiler/18/for_18_win_lin/GUID-A687BB5C-A35D-4F4F-9400-C903D8236AB7.html
! RANDOM_NUMBER
! 内蔵サブルーチン: 一つの擬似乱数またはそのような数の配列を返します。
! 
! CALL RANDOM_NUMBER (harvest)
! 
! harvest
! 
! (出力) 型は実数型でなければなりません。スカラーまたは配列変数であり、0 <= x < 1 の範囲内で一様分布に従う擬似乱数が格納されます。
! 
! RANDOM_NUMBERで使用される擬似乱数生成器のシードは、RANDOM_SEEDで設定または照会することができます。
! RANDOM_SEEDが使用されない場合、プロセッサはRANDOM_NUMBERのシードをプロセッサ依存の値に設定します。
! 
! RANDOM_NUMBERの生成器は、2つの異なる合同生成器を組み合わせて使用し、約10**18の周期を持ち、
! (0,1)の範囲内で一様分布に従う実数の擬似乱数を生成します。2つの整数型シードを受け付け、
! 最初のシードは[1, 2147483562]の範囲に制限されます。2番目のシードは[1, 2147483398]の範囲に制限されます。
! これは、実質的に2つの31ビットシードを使用していることを意味します。
! 
! アルゴリズムの詳細については、以下を参照してください：
! 
! Communications of the ACM vol 31 num 6 June 1988,: Efficient and Portable Combined Random Number Generators by Pierre L'ecuyer.
! Springer-Verlag New York, N. Y. 2nd ed. 1987,: A Guide to Simulation by Bratley, P., Fox, B. L., and Schrage, L. E.

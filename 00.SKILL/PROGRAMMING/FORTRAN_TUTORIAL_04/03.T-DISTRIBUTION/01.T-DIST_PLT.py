# -*- coding: utf-8 -*-
"""
参考にしたウェブサイト
https://qiita.com/inashiro/items/c59e31b0f0557a7a8bca
"""

# ライブラリのインポート
import os
import numpy as np
import matplotlib.pyplot as plt

IN="01.PDF_T-DIST_nu030.TXT" #入力ファイル名

# 入力ファイルが存在するか確認
if not (os.path.exists(IN)):
    print("NO SUCH FILE,"+IN)
    exit()

# 1行目のデータをnuとして読み込み
nu = np.loadtxt(IN, max_rows=1)
cnu=str(int(nu))

# 2行目以降のデータをx, yとして読み込み
x, y = np.loadtxt(IN, skiprows=1, unpack=True)

# グラフを書く
fig = plt.figure(figsize=(6,6))
ax = fig.add_subplot(111)
ax.plot(x, y, "-", color="k",)
ax.set_xlim(-6, 6)
ax.set_ylim( 0.0, 0.5)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_title('Probability density of T-Distribution\n degree of freedom, nu='+cnu)

# 図の保存
# スクリプト名を出力ファイル名につかう
#filename = os.path.basename(__file__)
#filename_no_extension = os.path.splitext(filename)[0]
IN_no_extension = os.path.splitext(IN)[0]
#FIG=filename_no_extension+"_"+IN_no_extension+".PDF"
FIG=IN_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG); print("")

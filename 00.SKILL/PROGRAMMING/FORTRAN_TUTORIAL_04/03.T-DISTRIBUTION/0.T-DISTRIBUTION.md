# t-分布

$\nu$: ギリシャ文字のニュー (自由度を表す)

$x$: 確率変数
$$
f(x) = \frac{ \Gamma \left( \frac{\nu + 1}{2} \right) }
{ \sqrt{\nu \pi} \Gamma \left( \frac{\nu}{2} \right) } 
\left( 1 + \frac{x^2}{\nu} \right)^{-(\nu + 1) / 2}
$$

```FORTRAN
   f(x) = gamma((nu + 1.0) / 2.0) / &
        (sqrt(nu * pi) * gamma(nu / 2.0)) *&
        (1.0 + x**2 / nu)**(-0.5 * (1.0 + nu))
```

$\nu\to\infty$ のとき，$f(x)$ は平均 $0$,分散 $1$ 正規分布に近づく

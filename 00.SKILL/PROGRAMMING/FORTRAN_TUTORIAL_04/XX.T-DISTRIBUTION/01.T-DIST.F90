REAL(8),ALLOCATABLE,DIMENSION(:)::X,Y

IM=600
ALLOCATE(X(-IM:IM),Y(-IM:IM))

nu=30.d0 !DEGREE OF FREEDOM

C=1.0/SQRT(2.0*PI)/SIGMA
DX=0.01
DO I=-IM,IM
X(I)=FLOAT(I)*DX
Y(I)=t_dist(X(I),nu)
END DO !I

OUT='01.PDF_T-DIST.TXT' !'//'_MU'//TRIM(CMU)//'_SIG'//TRIM(CSIG)//'.TXT'
OPEN(20,FILE=OUT)
DO I=-IM,IM
WRITE(20,'(F10.5,F12.8)')X(I),Y(I)
END DO !I

PRINT '(A,I5)','DEGREE OF FREEDOM, nu=',int(nu)
PRINT '(A,A)','OUT: ',TRIM(OUT)
PRINT *

END

FUNCTION t_dist(x, nu)
REAL(8) x,nu,t_dist, gamma
REAL(8), PARAMETER::pi=3.1415926535897932d0

gamma=exp(gammln(x))
t_dist=gamma((nu + 1.0d0) / 2.0d0) / &
        (sqrt(nu * pi) * gamma(nu / 2.0d0)) *&
        (1.0d0 + x**2 / nu)**(-0.5d0 * (1.0d0 + nu))
END

FUNCTION gammln(xx)
REAL(8)::gammln,xx
! Returns the value ln[Gamma (xx) ] for xx > 0.

INTEGER j
REAL(8):: ser,stp,tmp,x,y,cof(6)
! Internal arithmetic will be done in double precision, a nicety that you can 
! omit if five-figure accuracy is good enough.
SAVE cof,stp

DATA cof,stp/76.18009172947146d0,-86.50532032941677d0, &
24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2, &
-.5395239384953d-5,2.5066282746310005d0/

x=xx
y=x
tmp=x+5.5d0
tmp=(x+0.5d0)*log(tmp)-tmp
ser=1.000000000190015d0
do j=1,6
y=y+1.d0
ser=ser+cof(j)/y
end do !j
gammln=tmp+log(stp*ser/x)
return
END

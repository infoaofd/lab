## 方向余弦

### 定義

$\alpha$ を $\boldsymbol{A}$ と $x$ 軸のなす角，$\beta$ を $\boldsymbol{A}$ と $y$ 軸のなす角，$\gamma$ を $\boldsymbol{A}$ と $z$ 軸のなす角とする。このとき，
$$
A_x = |\boldsymbol{A}|\cos\alpha \\
A_y = |\boldsymbol{A}|\cos\beta \\
A_z = |\boldsymbol{A}|\cos\gamma
$$
である。このとき**方向余弦** $l$，$m$，$n$ を，
$$
l=\cos\alpha = A_x/|\boldsymbol{A}| \\
m=\cos\beta = A_y/|\boldsymbol{A}| \\
n=\cos\gamma = A_z/|\boldsymbol{A}| \\
$$
で定義する。このとき，三平方の定理より，
$$
l^2+m^2+n^2=(A_x^2+A_y^2+A_z^2)/|\boldsymbol{A}|^2=
(A_x^2+A_y^2+A_z^2)/(A_x^2+A_y^2+A_z^2)=1
$$
である。
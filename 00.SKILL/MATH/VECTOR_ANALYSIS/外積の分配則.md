C:\Users\foome\Dropbox\00.MATH\ベクトル解析

# ベクトルの外積の分配測



---

$$
\begin{eqnarray}
\mathbf{A} \times (\mathbf{B}+\mathbf{C})=\mathbf{A} \times \mathbf{B}+\mathbf{A} \times \mathbf{C}
\tag{1}
\end{eqnarray}
$$

---



## 始めに

ベクトル解析の初等的な教科書では，ベクトルの外積の分配則は自明のものとして扱われているものも多いようなので，ここで証明を与えておく。



## 参考文献
H.P.スウ (著), 高野 一夫 (訳), 1980. ベクトル解析　工学基礎演習シリーズ2, 森北出版, 392pp.



## 予備知識

### 外積の定義

ベクトル$\mathbf{C}$が$\mathbf{C}=\mathbf{A}\times\mathbf{B}$と表されるとき, $\mathbf{C}$は，

1. $\mathbf{A}$と$\mathbf{B}$の両方に直交する

2. $\mathbf{A}$を$\mathbf{B}$の方向に回したとき, 右ねじが進む方向を向く

3. $\mathbf{C}$の大きさ$|\mathbf{C}|$は，$\mathbf{A}$と$\mathbf{B}$のなす角を$\theta$とするとき, $|\mathbf{C}|=|\mathbf{A}||\mathbf{B}|\sin \theta$で与えられる。



### 外積の性質

外積の定義より，次の2式が導かれる。
$$
\begin{eqnarray}
\mathbf{B} \times \mathbf{A}=-\mathbf{A} \times \mathbf{B} \tag{2}  \\
\mathbf{A} \times \mathbf{A}=0 \tag{3} \\
\end{eqnarray}
$$

(2)は$\mathbf{A}$と$\mathbf{B}$のなす角$\theta$が180度になることから，（3）$\theta$が0度になることから導くことができる。



### スカラー3重積

#### 定義

ベクトル$\mathbf{A}$, $\mathbf{B}$, $\mathbf{C}$のスカラー3重積と呼ばれる量$[\mathbf{A} \, \mathbf{B} \, \mathbf{C}]$を
$$
\begin{eqnarray}
[\mathbf{A} \, \mathbf{B} \, \mathbf{C}]:=\mathbf{A} \cdot (\mathbf{B} \times \mathbf{C})
\tag{4}
\end{eqnarray}
$$
で定義する。



#### 性質

$\mathbf{B}$と$\mathbf{C}$のなす角を$\theta$とするとき$|\mathbf{B} \times \mathbf{C}|=|\mathbf{B}||\mathbf{C}|\sin \theta$であるから，$|\mathbf{B} \times \mathbf{C}|$は$\mathbf{B}$と$\mathbf{C}$がつくる平行四辺形Sの面積になる。

次にSを底面として，Sと$\mathbf{A}$がつくる平行6面体Vの体積を考える。

 $\mathbf{A}$と$ \mathbf{B} \times \mathbf{C}$のなす角を$\phi$とすると，
$$
\begin{eqnarray}
\mathbf{A} \cdot (\mathbf{B} \times \mathbf{C})=|A||\mathbf{B} \times \mathbf{C}|\cos\phi=|\mathbf{B} \times \mathbf{C}||A|\cos\phi
\end{eqnarray}
$$
は, Sの面積$\times$Vの高さとなることから，$[\mathbf{A} \, \mathbf{B} \, \mathbf{C}]$は$\mathbf{A}$, $\mathbf{B}$, $\mathbf{C}$がつくる**平行6面体Vの体積**を与える。

$[\mathbf{A} \, \mathbf{B} \, \mathbf{C}]=\mathbf{A} \cdot (\mathbf{B} \times \mathbf{C})$が$\mathbf{A}$, $\mathbf{B}$, $\mathbf{C}$がつくる平行6面体の体積を与えることから，
$$
\begin{eqnarray}
\mathbf{A} \cdot (\mathbf{B} \times \mathbf{C})=\mathbf{B} \cdot (\mathbf{C} \times \mathbf{A})=\mathbf{C} \cdot (\mathbf{A} \times \mathbf{B})
\tag{5}
\end{eqnarray}
$$


## 外積の分配則の証明

証明
$$
\begin{eqnarray}
\mathbf{U} =\mathbf{A} \times (\mathbf{B}+\mathbf{C})-\mathbf{A} \times \mathbf{B}-\mathbf{A} \times \mathbf{C} \tag{6}  \\
\end{eqnarray}
$$
とおく。$\mathbf{V}$を任意のベクトルとして，$\mathbf{U}$と$\mathbf{V}$の内積を取り, スカラー3重積の性質(5)を使うと，
$$
\begin{eqnarray}
\mathbf{V}\cdot\mathbf{U} &=&\mathbf{V}\cdot[\mathbf{A} \times (\mathbf{B}+\mathbf{C})]-\mathbf{V}\cdot[\mathbf{A} \times \mathbf{B}]-\mathbf{V}\cdot[\mathbf{A} \times \mathbf{C}] \\
&=&(\mathbf{B}+\mathbf{C})\cdot[\mathbf{V} \times \mathbf{A}]-\mathbf{B}\cdot[\mathbf{V}\times\mathbf{A}  ]-\mathbf{C}\cdot[\mathbf{V}\times\mathbf{A}  ] \\
&=&\mathbf{B}\cdot[\mathbf{V} \times \mathbf{A}]+\mathbf{C}\cdot[\mathbf{V} \times \mathbf{A}]-\mathbf{B}\cdot[\mathbf{V}\times\mathbf{A}  ]-\mathbf{C}\cdot[\mathbf{V}\times\mathbf{A}  ] \\
&=&\mathbf{0}
\end{eqnarray}
$$
よって，$\mathbf{U}$と$\mathbf{V}$は直交する。$\mathbf{V}$は任意としたので，$\mathbf{V}$=$\mathbf{U}$とすると，
$$
\begin{eqnarray}
\mathbf{U}\cdot\mathbf{U} = |\mathbf{U}|^2=0 
\end{eqnarray}
$$
これより，$|\mathbf{U}|=0$となるので$\mathbf{U}$は大きさゼロのベクトルとなり，$\mathbf{U}=\mathbf{0}$. これは，与式(1)が成立することを意味する。

(証明終わり)



## 問

$$
\begin{eqnarray}
\mathbf{A} \cdot (\mathbf{C} \times \mathbf{B})=-\mathbf{A} \cdot (\mathbf{B} \times \mathbf{C})
\end{eqnarray}
$$

となるが，この理由を答えよ。

#### ヒント

ベクトルの外積の定義をおさらいせよ。
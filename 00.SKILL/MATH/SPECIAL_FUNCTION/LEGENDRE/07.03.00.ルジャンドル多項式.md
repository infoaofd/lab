## 7.3 ルジャンドル多項式

ロドリゲスの公式 (7.16) 式 $P_n(s)=\frac{1}{2^n n!}\frac{d^n}{ds^n}(s^2-1)^n$ に (7.12) 式
$$
\frac{d^n}{ds^n}(s^2-1)
=\frac{(2n)!}{n!}\bigg\{
s^n-\frac{n(n-1)}{2(2n-1)}s^{n-2}
+\frac{n(n-1)(n-2)(n-3)}{2!2^2(2n-1)(2n-3)}s^{n-4}\\
\\
-\cdots+(-1)^k\frac{(n!)^2(2n-2k)!}{k!(n-k)!(n-2k)!(2n)!}s^{n-2k}\pm\cdots
\bigg\} \tag{7.12}
$$
を代入すると，
$$
P_n(s)=\frac{(2n)!}{2^n(n!)^2}\bigg\{
s^n-\frac{n(n-1)}{2(2n-1)}s^{n-2}
+\frac{n(n-1)(n-2)(n-3)}{2!2^2(2n-1)(2n-3)}s^{n-4}\\
\\
-\cdots+(-1)^k\frac{(n!)^2(2n-2k)!}{k!(n-k)!(n-2k)!(2n)!}s^{n-2k}\pm\cdots \tag{7.17a}
$$
または，
$$
P_n(s)=\frac{1}{2^n}\sum_{k=0}^N\frac{(-1)^k(2n-2k)!}{k!(n-k)!(n-2k)!}s^{n-2k} \tag{7.17b}
$$
が得られる。ここで，$n$ が偶数となるときは $N=\frac{n}{2}$, $n$ が奇数のときは $N=\frac{n-1}{2}$ とする。さらに，$n=0$ のときは $P_0(s)\equiv 1$ と定義する。

また，(7.15) $\Theta\equiv P_n(s)=P_n(\cos\theta)$ を考慮して，(7.17a)式と(7.5)式 
$$
\Theta=a_n\bigg\{s^n-\frac{n(n-1)}{2(2n-1)}s^{n-2}
+\frac{n(n-1)(n-2)(n-3)}{2\cdot4(2n-1)(2n-3)}s^{n-4}\\\\
-\cdots+(-1)^k\frac{(n!)^2(2n-2k)!}{k!(n-k)!(n-2k)!(2n)!}s^{n-2k}\pm\cdots
\bigg\}
\tag{7.5}
$$
を比較すれば，
$$
a_n=\frac{(2n)!}{2^n(n!)^2} \tag{7.18}
$$
であることも分かる。

こうして得られた (7.17a, b) 式を $n$ 次のルジャンドル多項式，または第1種ルジャンドル関数，あるいは帯球調和関数という。

そして，$n=5$ までの $P_n(s)$ を具体的に表記すればつぎのようになり，さらに，$-1\leqq s \leqq 1$ の範囲で $P_n (s)$ の曲線を描けば，図7.1のようになる。
$$
\left.
\begin{align}
P_0(s)=&\;1 \qquad\\
\\
P_1(s)=&\;s \qquad\\
\\
P_2(s)=&\;\frac{1}{2}(3s^2-1) \qquad\\
\\
P_3(s)=&\;\frac{1}{2}(5s^3-3s) \qquad\\
\\
P_4(s)=&\;\frac{1}{8}(35s^4-30s^2+3) \qquad\\
\\
P_6(s)=&\;\frac{1}{8}(63s^5-70s^3+15s) \qquad
\end{align}
\right\}\tag{7.19}
$$
<img src="image-20241228103141648.png" alt="image-20241228103141648" style="zoom:50%;" />

次に，(7.3)式
$$
a_{j+2}=-\frac{(n-j)(n+j+1))}{(j+1)(j+2)}a_j \tag{7.3}
$$
で，$j=-n-1$ とおけば，$a_{-n-1}$ の値に関わらず $a_{-n+1}=0$ となる。
$$
a_{-n-1+2}&=&-\frac{(n-(-n-1))\,(n+(-n-1)+1))}{((-n-1)+1)((-n-1)+2)}a_{-n-1}\\
\\
a_{-n+1}&=&-\frac{(2n+1)\,\times0}{(-n)(-n+1)}a_{-n-1}\\
\\
\therefore \quad a_{-n+1}=0
$$
したがって，またもや
$$
a_{-n+3}=a_{-n+5}=\cdots=0
$$
となり，$0$ とならない係数は前と同様にして
$$
\left.
\begin{align}
a_{-n-3}=&\frac{(n+1)(n+2)}{2(2n+3)}a_{-n-1} \qquad\\
\\
a_{-n-5}=&\frac{(n+1)(n+2)(n+3)(n+4)}{2\cdot 4(2n+3)(2n+5)}a_{-n-1}\\
\\
&\cdots\cdots\cdots\cdots\cdots\cdots\cdots\\
\\
a_{-n-2k-1}=&\frac{(n+1)(n+2)\cdots(n+2k)}{2^k k! (2n+3)(2n+5)\cdots(2n+2k+1)}a_{-n-1}
\qquad
\end{align}
\right\}\tag{7.20}
$$
のようになる。ここで，$a_{-n-1}$ の値はまったく任意であるから，先と同じ理由により ((7.15) を考慮して (7.17a) と (7.5) を比較する)，
$$
a_{-n-1}=\frac{n!}{(2n+1)(2n-1)\cdots3\cdot 1}=\frac{2^n(n!)^2}{(2n+1)!}\tag{7.21}
$$
とおく。この係数を使って作った (7.2) $\Theta = \sum_{j=-\infty}^{\infty}a_js^j$ 式の級数を $\Theta\equiv Q_n(s)$ と置けば，
$$
Q_n(s)=\frac{2^n(n!)^2}{(2n+1)!}
\bigg\{
\frac{1}{s^{n+1}}+\frac{(n+1)(n+2)}{2(2n+3)}\frac{1}{s^{n+3}}\\
\\
+\frac{(n+1)(n+2)(n+3)(n+4)}{2\cdot 4(2n+3)(2n+5)}\frac{1}{s^{n+5}}\\
\\
+\cdots+
\frac{(n+1)(n+2)\cdots(n+2k)}{2^k k! (2n+3)(2n+5)\cdots(2n+2k+1)}\frac{1}{s^{n+2k+1}}
\cdots
\bigg\}
$$
という無限級数となる。ダランベールの判定法を用いてこの級数の収束性を調べる。すなわち，級数の任意の隣接する2つの項 (第 $k$ 項と第 $k+1$ 項) の比をとった値が収束するかどうかを見る方法で，今考えている級数についていえば，(7.20) 式の最後の式を利用して
$$
\dfrac{ \dfrac{a_{-n-2k-1}}{s^{n+2k+1}}}{\dfrac{a_{-n-2k+1}}{s^{n+2k-1}}}
=\frac{(n+2k-1)(n+2k)}{2k(2n+2k+1)}\cdot\frac{1}{s^2}
$$
となる。したがって，$k\to\infty$ となるとき上式は，
$$
\frac{\bigg(2+\cfrac{n-1}{k}\bigg)\bigg(2+\cfrac{n}{k}\bigg)}{2\bigg(2+\cfrac{2n+1}{k}\bigg)}\cdot\frac{1}{s^2}
\to
\frac{1}{s^2}
$$
となるから，$|s|>1$ のとき絶対収束することが分かる。こうして得られた $Q_n(s)$ を第2種のルジャンドル関数と呼ぶ。$Q_n (s)$ はルジャンドルの微分方程式の解の一つとなる。



### ルジャンドル多項式のグラフを書くためのpythonスクリプト

```python
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import eval_legendre
import os

# x軸の範囲と分割
x = np.linspace(-1, 1, 500)

# 最初の数項 (n = 0 - 5) のルジャンドル多項式を計算
functions = {f"P_{n}": eval_legendre(n, x) for n in range(6)}

# グラフのプロット
plt.figure(figsize=(8, 6))
for label, values in functions.items():
    plt.plot(x, values, label=label)

# グラフの装飾
plt.title(f"Legendre Polynomial $P_n(x)$", fontsize=14)
plt.xlabel("$x$", fontsize=12)
plt.ylabel("$P_n$", fontsize=12)
plt.axhline(0, color='gray', linestyle='--', linewidth=0.7)
plt.legend(fontsize=10)
plt.grid(alpha=0.3)

# PDFファイル名をスクリプト名に基づいて設定
script_name = os.path.splitext(os.path.basename(__file__))[0]
pdf_filename = f"{script_name}.pdf"

# PDFに保存
plt.savefig(pdf_filename, format='pdf')
print(f"Graph saved as {pdf_filename}")

# 画面に表示（オプション）
# plt.show()
```


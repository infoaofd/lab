![img](20230416124423.gif)

```python
# フレーム数を指定
frame_num = 60

# 水平方向の角度として利用する値を作成
h_n = np.linspace(start=0.0, stop=360.0, num=frame_num+1)[:frame_num]

# ラベル用の符号を設定
#sgn2 = '+' if beta2 >= 0.0 else ''

# グラフオブジェクトを初期化
fig, ax = plt.subplots(figsize=(8, 8), facecolor='white', 
                       subplot_kw={'projection': '3d'})
fig.suptitle('linear dependent', fontsize=20)

# 作図処理を関数として定義
def update(i):
    
    # 前フレームのグラフを初期化
    plt.cla()

    # i番目の角度を取得
    h = h_n[i]
    
    # 線形従属の3Dベクトルを作図
    ax.quiver(0, 0, 0, *a1, 
              color='red', linewidth=3, arrow_length_ratio=l/np.linalg.norm(a1), 
              label='$a_1=('+', '.join(map(str, a1))+')$') # ベクトルa1
    ax.quiver(0, 0, 0, *a2, 
              color='blue', linewidth=3, arrow_length_ratio=l/np.linalg.norm(a2), 
              label='$a_2=('+', '.join(map(str, a2))+')$') # ベクトルa2
    ax.quiver(0, 0, 0, *b, 
              color='purple', linewidth=3, arrow_length_ratio=l/np.linalg.norm(b), 
              label='$b=('+', '.join(map(str, b.round(2)))+')$') # ベクトルb
    ax.plot_wireframe(x1_grid, x2_grid, x3_grid, 
                      label='$\\beta_1 a_1 + \\beta_2 a_2$') # ベクトルa1,a2と平行な平面
    ax.quiver([0, a1[0], a2[0], b[0]], [0, a1[1], a2[1], b[1]], [0, a1[2], a2[2], b[2]], 
              [0, 0, 0, 0], [0, 0, 0, 0], [z_min, z_min-a1[2], z_min-a2[2], z_min-b[2]], 
              color=['gray', 'red', 'blue', 'purple'], arrow_length_ratio=0, linestyle=':') # 座標用の補助線
    ax.set_zlim(bottom=z_min, top=z_max)
    ax.set_xlabel('$x_1$')
    ax.set_ylabel('$x_2$')
    ax.set_zlabel('$x_3$')
    #ax.set_title('$b=' + str(beta1.round(2))+'a_1' + sgn2+str(beta2.round(2))+'a_2$', loc='left')
    ax.legend()
    ax.set_aspect('equal')
    ax.view_init(elev=30, azim=h) # 表示角度

# gif画像を作成
ani = FuncAnimation(fig=fig, func=update, frames=frame_num, interval=100)

# gif画像を保存
ani.save('dependence360_3d.gif')
```


# 空間パターンを射影して時系列を作る



いま, 時間方向に$N$個, 空間方向に$P$個からなるデータを表す$N$行, $P$列の行列$Z$が与えられているとする。
$$
Z=(z_{ij})=
\begin{bmatrix}
z_{11} & z_{12} & \cdots & z_{1P} \\
z_{21} & z_{22} & \cdots & z_{2P} \\
\vdots & \vdots & \ddots & \vdots  \\
z_{N1} & z_{N2}&   \cdots & z_{NP} \\
\end{bmatrix}
$$
$i$を時刻を表す添え字, $j$を場所を表す添え字とすると, 模式的に<font color="red">$Z=z_{ij}=z_{時間 \, 場所}$</font>と書ける。

いま, 地点$j=1$の点のデータを抜き出す$N$次元の単位ベクトル
$$
\mathbf{e}_1:=
\left.
\begin{bmatrix}
1 \\
0 \\
\vdots   \\
0 \\
\end{bmatrix}
\right\}
 N行
$$
を用いると,
$$
\begin{eqnarray}
Z\mathbf{e}_1&=&
\begin{bmatrix}
z_{11} & z_{12} & \cdots & z_{1P} \\
z_{21} & z_{22} & \cdots & z_{2p} \\
\vdots & \vdots & \ddots & \vdots  \\
z_{N1} & z_{N2}& \cdots & z_{NP} \\
\end{bmatrix}
\begin{bmatrix}
1 \\
0 \\
\vdots   \\
0 \\
\end{bmatrix}
=
\left.
\begin{bmatrix}
z_{11} \\
z_{21} \\
\vdots \\
z_{N1} \\
\end{bmatrix}
\right\}
 N行 \\
&& \\
&=&j=1地点の時系列
\end{eqnarray}
$$
同様に$j$番目の地点のデータを抜き出す$n$次元の単位ベクトル$\mathbf{e}_j$を用いると,
$$
\begin{eqnarray}
Z\mathbf{e}_j&=&
\begin{bmatrix}
z_{1j} \\
z_{2j} \\
\vdots \\
z_{Nj} \\
\end{bmatrix}
&& \\
&=&地点jの時系列
\end{eqnarray}
$$
となる。

いま, ベクトル$\mathbf{v}$を,
$$
\mathbf{v}:=
\left.
\begin{bmatrix}
v_{1} \\
v_{2} \\
\vdots \\
v_{P} \\
\end{bmatrix}
\right\}
 P行 \\
$$
定義する。この$\mathbf{v}$を,
$$
\mathbf{v}
=
v_1\begin{bmatrix}
1 \\
0 \\
\vdots \\
0 \\
\end{bmatrix}
+
v_2\begin{bmatrix}
0 \\
1 \\
\vdots \\
0 \\
\end{bmatrix}
+
\cdots
+
v_P\begin{bmatrix}
0 \\
0 \\
\vdots \\
1 \\
\end{bmatrix}
=v_1\mathbf{e}_1+v_2\mathbf{e}_2+\cdots+v_n\mathbf{e}_P
$$
と考えると, $\mathbf{v}$は特定の空間パターンを表すベクトルと見なすことができる。ここで, $v_j$は空間パターンを表現するために単位ベクトルにかける重みである。

行列$A$とベクトル$\mathbf{v}$の積を取ると, $Z\mathbf{e}_j$が地点$j$における時系列を表すことから,
$$
\begin{eqnarray}
Z\mathbf{v}&=&Zv_1\mathbf{e}_1+Zv_2\mathbf{e}_2+\cdots+Zv_P\mathbf{e}_P \\

&=&v_1(Z\mathbf{e}_1)+v_2(Z\mathbf{e}_2)+\cdots+v_P(Z\mathbf{e}_P) \\
&=& \sum_{j=1}^{P}v_j(Z\mathbf{e}_j)
\end{eqnarray}
$$
である。言葉で書くと, 
$$
\begin{eqnarray}
Z\mathbf{v}
&=&\sum_j^{P} (j地点における重み)\times(j地点における時系列)
\end{eqnarray}
$$
である。

　いま$\mathbf{v}_m$がEOFなどによって得られた第$m$モードの空間分布であれば, $Z\mathbf{v_m}$によって第$m$モードの時間変化を表すベクトルが得られる。



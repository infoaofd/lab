import numpy
import math
import scipy.stats as ss
import mymodule as mm

class InitPCA:
    """
                   Calculation of the Principal Components
    """
    def __init__(self, data, f_out):
        self.data = data
        self.f_out = f_out


    def calc_pca(self):   
        data = numpy.array(self.data)
        n_vars = len(data[0]) - 1
        #
        #       var_names: List of the variable names
        #
        var_names = data[0]  
        n_data = len(data) - 1  
        CaseID = data.T[0][1:]

        X = []
        for i in range(n_vars):
            X.append([float(v) for v in data.T[i + 1][1:]])

        self.f_out.write("\n")
        self.f_out.write("n_vars = %d\n"%n_vars)
        self.f_out.write("Var_names...\n")
        for vn in var_names:
            self.f_out.write("%10s"%vn)
        self.f_out.write("\n")
        self.f_out.write("\n")
        for i in range(0, n_data):
            self.f_out.write("%10s"%CaseID[i])
            for j in range(0, n_vars):
                self.f_out.write("%10.3f"%X[j][i])
            self.f_out.write("\n")

        R = numpy.corrcoef(X)
        Lmbd2, V = numpy.linalg.eig(R)
        Lmbd = []
        for i in range(0, len(Lmbd2)):
            if Lmbd2[i] > 0.0:
                Lmbd.append(math.sqrt(Lmbd2[i]))
            else:
                Lmbd.append(0.0)
        sort_index = numpy.argsort(numpy.array(Lmbd) * (-1))
        Lmbd = numpy.array(Lmbd)[sort_index]
        n_eign = 0
        for v in Lmbd:
            if v > 0.0:
                n_eign += 1
        ListLV = V.T[sort_index].T

        self.f_out.write("\nLambda...\n")
        sum_L2 = 0.0
        for i in range(0, n_eign):
            sum_L2 += mm.sqr(Lmbd[i])
        cum_sum = 0.0
        self.f_out.write("%30s"%"Lambd ")
        self.f_out.write("%10s"%"cum.sqr")
        self.f_out.write("%10s\n"%"%")
        print("\n")
        print(("%30s"%"Lambda") + ("%10s"%"cum.sqr") + ("%10s"%"%") + "\n")

        for i in range(0, n_eign):
            self.f_out.write("%20s"%("Lambda-%d = "%(i + 1)))
            self.f_out.write("%10.5f"%Lmbd[i])  
            cum_sum += mm.sqr(Lmbd[i])  
            self.f_out.write("%10.5f"%cum_sum)
            self.f_out.write("%10.2f\n"%(100.0 * cum_sum / sum_L2))
            print("%20s"%("Lambda-%d = "%(i + 1)), end="")
            print("%10.5f"%Lmbd[i], end = "")  
            print("%10.5f"%cum_sum, end = "")
            print("%10.2f"%(100.0 * cum_sum / sum_L2))

        s0 = input("\nNumber of components = ")
        n_comp = int(s0)
        if (n_comp > n_vars):
            n_comp = n_vars
        elif (n_comp < 1):
            n_comp = 1    
        if (n_comp > n_eign):   
            n_comp = n_eign

        Z = ss.zscore(X, axis = 1).T

        L1 = numpy.diag(Lmbd[:n_comp])

        V1 = ListLV[:, :n_comp]
        Pat = V1 @ L1
        
        self.f_out.write("\n\nPattern matrix for the principal components =\n")
        self.f_out.write("%10s"%" ")
        for i in range(0, n_comp):
            self.f_out.write("%10s"%("comp.%d"%(i + 1)))
        self.f_out.write("\n")
        for i in range(0, n_vars):
            self.f_out.write("%10s"%var_names[i + 1])
            for j in range(0, n_comp):
                self.f_out.write("%10.5f"%Pat[i, j])
            self.f_out.write("\n")

        PComp = numpy.matrix(Z @ V1 @ numpy.linalg.inv(L1))
            
        return var_names, n_data, n_vars, CaseID, n_comp, Pat, PComp


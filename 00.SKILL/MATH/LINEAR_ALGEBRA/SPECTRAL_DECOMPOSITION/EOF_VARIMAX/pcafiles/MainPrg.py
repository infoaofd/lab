import csv
import numpy
import math
import mymodule as mm
from init_pca import InitPCA
from rotate import Varimax, Promax
from PlotComp import PlotPattern
from ArrangePat import ArrangePtn

"""
           Principal Components Analysis with Rotation of Components

                                            Yasuharu Okamoto, 2016.10, 2021.01

"""

#
#       Prepare the input data file (CSV format)
#
s = input("Input data file (*.csv) = ")
with open(s, 'r') as f:
    data = [v for v in csv.reader(f)]
#
#       Prepare the output file
#
s_out = input("Output trxt file (*.txt) = ")
f_out = open(s_out, "w")
f_out.write("Input data file = " + s + "\n\n")
    
#print('data:\n', data)

#
#           Calculate the principal components
#
calc_init_pca = InitPCA(data, f_out)

var_names, n_data, n_vars, CaseID, n_comp, Pat, PComp = calc_init_pca.calc_pca()

"""
            var_names:     A list of variable names
            n_data:        The number of cases in data
            n_vars:        The number of variables
            CaseID:        The variable for case idenfication (string type)
            n_com:         The number of componets
            Pat:           The pattern matrix of the principal components
            PComp:         The matrix of the principal components
"""

f_out.write("\n\nPrincipal components...\n")
f_out.write("%10s"%var_names[0])
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_data):
    f_out.write("%10s"%CaseID[i])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PComp[i, j])
    f_out.write("\n")    

PlotPtn = PlotPattern( Pat, var_names, n_vars, n_comp, " ...principal components" )
while True:
    print("\nSet the component number (from 1 to %d) to plot the pattern."%n_comp)
    print("If you do not want to plot the pattern, set the number less than 1.")
    xdim = int(input("xdim = "))
    if xdim < 1:
        break
    ydim = int(input("ydim = "))
    PlotPtn.Plot(xdim, ydim)


rot = Varimax(Pat, n_vars, n_comp)
PatV = rot.rotateV()
            
f_out.write("\n\nVarimax rotation was applied...\n\n")
f_out.write("\nPattern for the Varimax rotated components =\n")
f_out.write("%10s"%" ")
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_vars):
    f_out.write("%10s"%var_names[i + 1])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PatV[i, j])
    f_out.write("\n")

ArgPtn = ArrangePtn(var_names, PatV, n_vars, n_comp)
PtnArgd = ArgPtn.arrange()
f_out.write("\n\nSorted Pattern for Varimax rotation =\n")
f_out.write("%10s"%" ")
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_vars):
    f_out.write("%10s"%PtnArgd[i][0])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PtnArgd[i][1][j])
    f_out.write("\n")
    
PlotPtn = PlotPattern( PatV, var_names, n_vars, n_comp, " ...Varimax rotated components" )
while True:
    print("\nSet the component number (from 1 to %d) to plot the pattern for the Varimax criterion."%n_comp)
    print("If you do not want to plot the pattern, set the number less than 1.")
    xdim = int(input("xdim = "))
    if xdim < 1:
        break
    ydim = int(input("ydim = "))
    PlotPtn.Plot(xdim, ydim)

    

PPP =  PatV * numpy.linalg.inv(numpy.transpose(PatV) * PatV)
PCompV = PComp * numpy.transpose(Pat) * PPP 
f_out.write("\n\nComponents for Varimax...\n")
f_out.write("%10s"%var_names[0])
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_data):
    f_out.write("%10s"%CaseID[i])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PCompV[i, j])
    f_out.write("\n")

obrot = Promax(PatV, n_vars, n_comp, 4.0)
Patob, Phi = obrot.rotateP();

f_out.write("\n\nPromax rotation was applied...\n\n")
f_out.write("\nPattern for the Promax rotated components =\n")
f_out.write("%10s"%" ")
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")

for i in range(0, n_vars):
    f_out.write("%10s"%var_names[i + 1])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%Patob[i, j])
    f_out.write("\n")

f_out.write("\n\nCorrelation matrix of the components...\n")
f_out.write("%10s"%" ")
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(i + 1)))
    for j in range(0, n_comp):
        f_out.write("%10.5f"%Phi[i, j])
    f_out.write("\n")

ArgPtn = ArrangePtn(var_names, Patob, n_vars, n_comp)
PtnArgd = ArgPtn.arrange()
f_out.write("\n\nSorted Pattern for Promax rotation =\n")
for i in range(0, n_vars):
    f_out.write("%10s"%PtnArgd[i][0])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PtnArgd[i][1][j])
    f_out.write("\n")

PPP =  Patob * numpy.linalg.inv(numpy.transpose(Patob) * Patob)
PCompob = PComp * numpy.transpose(Pat) * PPP 
f_out.write("\n\nComponents for Promax...\n")
f_out.write("%10s"%var_names[0])
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_data):
    f_out.write("%10s"%CaseID[i])
    for j in range(0, n_comp):
        f_out.write("%10.5f"%PCompob[i, j])
    f_out.write("\n")

f_out.close()
print(s_out + " was saved.")



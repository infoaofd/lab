import numpy
import math
import mymodule as mm

class Varimax:
    """
                 Varima rotation 
    """
    def __init__(self, X, vn, vp):
        if (len(X) != vn):
            raise "Invalid value for n in Varimax"
        else:
            self.n = vn
        if ((numpy.size(X) / len(X)) != vp):
            raise "Invalid value for p in Varimax"
        else:
            self.p = vp

        tempA = []
        for i in range(0, self.n):
            tempA.append([])
            for j in range(0, self.p):
                tempA[i].append(X[i,j])
        self.A = numpy.matrix(tempA)

        if vp > 1:
            self.h = []
            for i in range(0, self.n):
                sum = 0.0
                for j in range(0, self.p):
                    sum += mm.sqr(self.A[i, j])
                self.h.append(math.sqrt(sum))
                for j in range(0, self.p):
                    self.A[i, j] /= self.h[i]



    def rotateV(self):
        if (self.p < 2):
            return self.A
        while True:
            ckA = []
            for i in range(0, self.n):
                ckA.append([])
                for j in range(0, self.p):
                    ckA[i].append(self.A[i, j])
                    
            for i in range(0, self.p):
                for j in range(i + 1, self.p):
                    x = []
                    y = []
                    for k in range(0, self.n):
                        x.append(self.A[k, i])
                        y.append(self.A[k, j])
                    u = []
                    v = []
                    for k in range(0, self.n):
                        u.append(mm.sqr(x[k]) - mm.sqr(y[k]))
                        v.append(2.0 * x[k] * y[k])
                    cA = 0.0
                    cB = 0.0
                    cC = 0.0
                    cD = 0.0
                    for k in range(0, self.n):
                        cA += u[k]
                        cB += v[k]
                        cC += mm.sqr(u[k]) - mm.sqr(v[k])
                        cD += 2.0 * u[k] * v[k]
                    num = cD - 2 * cA * cB / self.n
                    den = cC - (mm.sqr(cA) - mm.sqr(cB)) /self.n
                    theta4 = math.atan(num / den)
                    if (num > 0.0):
                        if (theta4 < 0.0):
                            theta4 += math.pi
                    else:
                        if (theta4 > 0.0):
                            theta4 += math.pi
                    theta = theta4 / 4.0
                    for k in range(0, self.n):
                        tx = self.A[k, i] * math.cos(theta) + self.A[k, j] * math.sin(theta)
                        ty = -self.A[k, i] * math.sin(theta) + self.A[k, j] * math.cos(theta)
                        self.A[k, i] = tx
                        self.A[k, j] = ty

            dif = 0.0
            for i in range(0, self.n):
                for k in range(0, self.p):
                    dif += mm.sqr(ckA[i][k] - self.A[i, k])
            if (dif < 1.0e-9):
                for i in range(0, self.n):
                    for k in range(0, self.p):
                        self.A[i, k] *= self.h[i]
                break
           
        return self.A


def sgn( a ):
    if a > 0.0:
        return 1.0
    elif a < 0.0:
        return -1.0
    else:
        return 0.0


class Promax:
    """
              Promax rotation
    """
    def __init__(self, V, n_vars, n_comp, gmma):
        tempA = []
        for i in range(0, n_vars):
            tempA.append([])
            for j in range(0, n_comp):
                tempA[i].append(V[i, j])
        self.A = numpy.matrix(tempA)
        self.n = n_vars
        self.p = n_comp
        self.gmma = gmma

        ua = []
        for i in range(0, n_vars):
            ua.append([])
            v2 = 0.0
            for k in range(0, n_comp):
                v2 += mm.sqr(self.A[i, k])
            for k in range(0, n_comp):
                ua[i].append(self.A[i, k] / math.sqrt(v2))
                
        mxu = []
        for i in range(0, n_comp):
            tv = 0.0
            for k in range(0, n_vars):
                if tv < abs(ua[k][i]):
                    tv = abs(ua[k][i])
            mxu.append(tv)
        u = []
        for i in range(0, n_vars):
            u.append([])
            for j in range(0, n_comp):
                u[i].append(ua[i][j] / mxu[j])
        cc = []
        for i in range(0, n_vars):
            cc.append([])
            for j in range(0, n_comp):
                cc[i].append(sgn(self.A[i, j]) * math.pow(abs(u[i][j]), self.gmma))
        self.C = numpy.matrix(cc)

    def rotateP(self):
        ApA = numpy.transpose(self.A) * self.A
        ApAinv = numpy.linalg.inv(ApA)
        Q = ApAinv * numpy.transpose(self.A) * self.C
        QpQinv = numpy.linalg.inv(numpy.transpose(Q) * Q)
        dD = []
        for i in range(0, self.p):
            dD.append([])
            for j in range(0, self.p):
                if j == i:
                    dD[i].append(math.sqrt(QpQinv[j, j]))
                else:
                    dD[i].append(0.0)
        D = numpy.matrix(dD)

        Lmbd = Q * D

        ALmbd = self.A * Lmbd

        Phi = numpy.linalg.inv(numpy.transpose(Lmbd) * Lmbd)

        return ALmbd, Phi
    




        

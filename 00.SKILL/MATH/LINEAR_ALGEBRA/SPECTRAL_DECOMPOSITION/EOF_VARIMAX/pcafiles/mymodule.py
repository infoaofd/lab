import math

#
#       Seek the line with slash / at the head.
#
def check_slash(str_data, start):
    while(True):
        pos = str_data[start].find("/",0)
        if (pos >= 0):
            return start
        start = start + 1;

#
#       The square of a 
#
def sqr(a):
    return a * a

#
#       Transforn the values in vX to the standardized scores
#
def standardize(vX):
    n = len(vX)
    for i in range(0, n):
        vX[i] = float(vX[i])
    sum = 0.0;
    for i in range(0, n):
        sum = sum + vX[i]
    mean = sum / n;
    ssum = 0.0;
    for i in range(0, n):
        ssum = ssum + sqr(vX[i] - mean)
    sd = math.sqrt(ssum / n)
    for i in range(0, n):
        vX[i] = (vX[i] - mean) / sd

    return vX
    
#
#       Get the formatted string of the value v
#
def get_str(v):
    if (type(v) == str):
        return "%10s"%v
    else:
        return "%10.5f"%v

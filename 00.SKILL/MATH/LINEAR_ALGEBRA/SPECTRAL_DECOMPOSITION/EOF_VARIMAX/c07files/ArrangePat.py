
class ArrangePtn:
    """
            Reordering of the rows of the factor pattern
            so that similar varibles come together 
    """
    def __init__(self, var_names, Pattern, n_vars, n_comp):
        self.names = var_names
        self.Pttn = Pattern
        self.n_vars = n_vars
        self.n_comp = n_comp
        self.NamePttn = []
        for i in range(0, self.n_vars):
            self.NamePttn.append([])
            self.NamePttn[i].append(var_names[i + 1])
            self.NamePttn[i].append([])
            for j in range(0, self.n_comp):
                self.NamePttn[i][1].append(Pattern[i, j])

    def check(self, a, b):
        ta = 0
        for i in range(0, self.n_comp):
            if (abs(self.NamePttn[a][1][i]) > abs(self.NamePttn[a][1][ta])):
                ta = i
        tb = 0
        for i in range(0, self.n_comp):
            if (abs(self.NamePttn[b][1][i]) > abs(self.NamePttn[b][1][tb])):
                tb = i
        if (tb < ta):
            return True
        elif (tb == ta):
            if (abs(self.NamePttn[b][1][tb]) > abs(self.NamePttn[a][1][ta])):
                return True
            else:
                return False
        else:
            return False
            

    def arrange(self):

        for i in range(0, self.n_vars -1):
            k = i
            for j in range(i + 1, self.n_vars):
                if self.check(k, j):
                    k = j
            if (i < k):
                tmp = self.NamePttn[i]
                self.NamePttn[i] = self.NamePttn[k]
                self.NamePttn[k] = tmp

        return self.NamePttn




                
                    
                

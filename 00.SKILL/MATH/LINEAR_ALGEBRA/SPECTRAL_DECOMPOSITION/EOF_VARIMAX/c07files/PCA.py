#
#               主成分分析と回転
#
import pandas as pd
import numpy as np
import scipy.stats as ss
import matplotlib.pyplot as plt
import japanize_matplotlib
from rotate import Varimax, Promax
from PlotComp import PlotPattern
from ArrangePat import ArrangePtn

in_fl_nm = input('データファイル(*.xlsx) = ')
with pd.ExcelFile(in_fl_nm) as f:
    data = pd.read_excel(f)

f_out = open('Results.txt', 'w')
    
print('data...\n', data)
print('keys...',[k for k in data.keys()])

f_out.write(f'\nData = {in_fl_nm}\n')

print(data.values)
var_names = [v for v in data.keys()]
print('var_names', var_names)
f_out.write('\nvar_names\n')
f_out.write(f'{var_names}\n')
f_out.write(f'X =\n{data.values}')
print('data.values\n', data.values)
X = data.values[:,1:]
X = np.array(X, dtype='float')      #   型をfloatに明示的設定
                                    #   Excelファイルの第１列目の値が
                                    #   文字列型のときに必要
print('X =\n', X)
n, s = np.shape(X)
print('n =', n, '   s =', s)
Z = ss.zscore(X)
print('Z =\n', Z)
#
#          拙著、p.117
#
U, Lmbd, Vh = np.linalg.svd(Z/(n**0.5), full_matrices = False)
print('U =\n', U[:10])
print('Lmbd =', Lmbd)
print('Vh =\n', Vh)

n_data = n
n_vars = s
CaseID = data.values[:,0]
print(CaseID)

f_out.write(f'\nn_data = {n_data}\nn_vars = {n_vars}\n')

p = len(Lmbd)
cumsqrLmbd = np.cumsum(Lmbd**2)
cumpercent = (cumsqrLmbd / cumsqrLmbd[-1])*100
print(f'{" ":>15s}{"Lambda":>15s}{"Cum_Sqr":>15s}{"%":>15s}')
for i in range(p):
    str = f'Lambda-{i+1}'
    print(f'{str:>15s}{Lmbd[i]:>15.5f}{cumsqrLmbd[i]:>15.5f}', end = '')
    print(f'{cumpercent[i]:>15.5f}')

f_out.write(f'\n{" ":>15s}{"Lambda":>15s}{"Cum_Sqr":>15s}{"%":>15s}\n')
for i in range(p):
    str = f'Lambda-{i+1}'
    f_out.write(f'{str:>15s}{Lmbd[i]:>15.5f}{cumsqrLmbd[i]:>15.5f}')
    f_out.write(f'{cumpercent[i]:>15.5f}\n')

plt.plot(range(p), Lmbd)
plt.xticks(range(p), [v+1 for v in range(p)])
plt.ylabel('特異値', fontsize = 16)
plt.show()

n_comp = int(input('\nNumber of components = '))
if n_comp > n_vars:
    n_comp = n_vars
if n_comp < 1:
    n_comp = 1
if n_comp > p:
    n_comp = p

U1 = U[:,:n_comp]
L1 = np.diag(Lmbd[:n_comp])
V1 = Vh[:n_comp].T

Pat = V1 @ L1
print('\nPattern...\n', Pat)

f_out.write('\nパターン行列\n')
f_out.write(f'{" ":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_vars):
    f_out.write(f'{var_names[j+1]:>15s}')
    for i in range(n_comp):
        f_out.write(f'{Pat[j][i]:>15.5f}')
    f_out.write('\n')
            

PComp = U1 * (n_data**0.5)
print('\nComponents...\n', PComp)
f_out.write('\n主成分\n')
f_out.write(f'{"ID":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_data):
    f_out.write(f'{CaseID[j]:>15}')
    for i in range(n_comp):
        f_out.write(f'{PComp[j][i]:>15.5f}')
    f_out.write('\n')

PlotPtn = PlotPattern( Pat, var_names, n_vars, n_comp,
                       " ...主成分パターン" )
while True:
    print("\nSet the component number (from 1 to %d) to plot the pattern."%n_comp)
    print("If you do not want to plot the pattern, set the number less than 1.")
    xdim = int(input("xdim = "))
    if xdim < 1:
        break
    ydim = int(input("ydim = "))
    PlotPtn.Plot(xdim, ydim)


f_out.write('\n\nバリマックス回転\n')

rot = Varimax(Pat, n_vars, n_comp)
PatV = rot.rotateV()
print('PatV...\n', PatV)

f_out.write('\nパターン行列（バリマックス回転）\n')
f_out.write(f'{" ":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_vars):
    f_out.write(f'{var_names[j+1]:>15s}')
    for i in range(n_comp):
        f_out.write(f'{PatV[j][i]:>15.5f}')
    f_out.write('\n')
            
ArgPtn = ArrangePtn(var_names, PatV, n_vars, n_comp)
PtnArgd = ArgPtn.arrange()
print('\nPtnArgd...\n')
for v in PtnArgd:
    print(v)

f_out.write('\nパターン行列（整列）（バリマックス回転）\n')
f_out.write(f'{" ":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_vars):
    f_out.write(f'{var_names[j+1]:>15s}')
    for i in range(n_comp):
        f_out.write(f'{PtnArgd[j][1][i]:>15.5f}')
    f_out.write('\n')
#
#           拙著、p.118      
#
PCompV = PComp @ Pat.transpose() @ PatV @ np.linalg.inv(PatV.transpose() @ PatV)

print('\nPCompV...\n', PCompV)
f_out.write('\n成分（バリマックス回転）\n')
f_out.write(f'{"ID":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_data):
    f_out.write(f'{CaseID[j]:>15}')
    for i in range(n_comp):
        f_out.write(f'{PCompV[j][i]:>15.5f}')
    f_out.write('\n')

PlotPtn = PlotPattern( PatV, var_names, n_vars, n_comp,
                       " ...バリマックス回転後パターン" )
while True:
    print("\nSet the component number (from 1 to %d) to plot the pattern for the Varimax criterion."%n_comp)
    print("If you do not want to plot the pattern, set the number less than 1.")
    xdim = int(input("xdim = "))
    if xdim < 1:
        break
    ydim = int(input("ydim = "))
    PlotPtn.Plot(xdim, ydim)
 

#    Promax

obrot = Promax(PatV, n_vars, n_comp, 4.0)
Patob, Phi = obrot.rotateP();

print('\nPattern Promax...\n', Patob)

print('\nCorr...\n', Phi)

ArgPtn = ArrangePtn(var_names, Patob, n_vars, n_comp)
PtnArgd = ArgPtn.arrange()
print('\nPtnArgd...')
for v in PtnArgd:
    print(v)
#
#           拙著、p.118      
#
PCompob = PComp @ Pat.transpose() @ Patob @ np.linalg.inv(Patob.transpose() @ Patob)
print('\nPCompob...\n', PCompob)

f_out.write('\nパターン行列（プロマックス回転）\n')
f_out.write(f'{" ":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_vars):
    f_out.write(f'{var_names[j+1]:>15s}')
    for i in range(n_comp):
        f_out.write(f'{Patob[j][i]:>15.5f}')
    f_out.write('\n')
            
f_out.write('\nパターン行列（整列）（プロマックス回転）\n')
f_out.write(f'{" ":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_vars):
    f_out.write(f'{var_names[j+1]:>15s}')
    for i in range(n_comp):
        f_out.write(f'{PtnArgd[j][1][i]:>15.5f}')
    f_out.write('\n')

f_out.write("\n\n因子間相関行列...\n")
f_out.write("%10s"%" ")
for j in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(j + 1)))
f_out.write("\n")
for i in range(0, n_comp):
    f_out.write("%10s"%("comp.%d"%(i + 1)))
    for j in range(0, n_comp):
        f_out.write("%10.5f"%Phi[i, j])
    f_out.write("\n")

f_out.write('\n成分（プロマックス回転）\n')
f_out.write(f'{"ID":>15s}')
for i in range(n_comp):
    str_v = f'comp-{i+1}'
    f_out.write(f'{str_v:>15s}')
f_out.write('\n')
for j in range(n_data):
    f_out.write(f'{CaseID[j]:>15}')
    for i in range(n_comp):
        f_out.write(f'{PCompob[j][i]:>15.5f}')
    f_out.write('\n')

f_out.close()
print('\nResults.txt was saved.\n')


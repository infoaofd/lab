import matplotlib.pyplot as mpt
import japanize_matplotlib


class PlotPattern:
    """
            Plot the scattergram of the factor pattern
    """
    def __init__(self, Patn, var_names, n_var, n_comp, msg):
        self.Patn = Patn
        self.var_names = var_names
        self.n_var = n_var
        self.n_comp = n_comp
        self.msg = msg



    def Plot(self, xdim, ydim):
        if (xdim < 1):
            xdim = 1
        if (xdim > self.n_comp):
            xdim = self.n_comp
        if (ydim < 1):
            ydim = 1
        if (ydim > self.n_comp):
            ydim = self.n_comp

        xcoord = []
        ycoord = []
        for i in range(0, self.n_var):
            xcoord.append(self.Patn[i, xdim - 1])
            ycoord.append(self.Patn[i, ydim - 1])

        mpt.figure(figsize=(7,7))
        mpt.axis([-1.2, 1.2, -1.2, 1.2])
        mpt.plot(xcoord, ycoord, 'bo')
        mpt.title("X軸：成分ー%d   Y軸：成分ー%d)"%(xdim, ydim) + self.msg)
        mpt.xticks([-1.0, 0.0, 1.0],[r'$-1.0$', r'$0.0$', r'$1.0$'])
        mpt.yticks([-1.0, 0.0, 1.0],[r'$-1.0$', r'$0.0$', r'$1.0$'])
        #mpt.xlabel(f'comp.{xdim}')
        #mpt.ylabel(f'comp.{ydim}')

        for i in range(0, self.n_var):
            mpt.text(self.Patn[i, xdim - 1] + 0.015, self.Patn[i, ydim - 1] + 0.015, self.var_names[i + 1])

        ax = mpt.gca()
        ax.spines['right'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.xaxis.set_ticks_position('bottom')
        ax.spines['bottom'].set_position(('data', 0))
        ax.yaxis.set_ticks_position('left')
        ax.spines['left'].set_position(('data',0))
        
        mpt.show()

        


<style>  
  .page-break {  
    opacity: 0;  
    break-after: page;  
  }  
</style>  

# 主成分分析の概要

[[_TOC_]]

これは地球科学で用いる主成分分析について概要を記した資料である。厳密さにはこだわらず, 直観的な意味を説明している。

気象学を含む地球科学では主成分分析のことをEOF解析という用語で呼ぶことの方が多い。EOFとは, Empirical Orthogonal Functionの略称であり, 日本語では経験的直交関数と呼ばれる。

統計学ではデータを縦や横に並べて括弧でくくったものをベクトルと見なすことがある。 データを縦と横に一覧表の形で並べた行列というものを使ってデータを表現することもよくある。EOFとは行列の形で与えられたデータを直交するベクトルを利用して表現し直す手法であるため, このような名称がついている。

数学では関数を無限大の次元を持つベクトルと解釈することがある。これがEOFという用語の最後にfunctionという単語が付されている由来であると思われる。



## 参考文献

EOF解析は線形代数と呼ばれる数学を用いている。EOF解析に必要な線形代数の基礎知識は

- 転置行列, 逆行列, 直交行列
- 固有値・固有ベクトル

である。学習資料として下記が参考になるかもしれない。同様の内容の資料，書籍は数えきれないほどあるので，ウェブ, 図書館, 書店などで自分に合いそうなものを探すと良い。

https://www.amazon.co.jp/%E3%82%AD%E3%83%BC%E3%83%9D%E3%82%A4%E3%83%B3%E3%83%88%E7%B7%9A%E5%BD%A2%E4%BB%A3%E6%95%B0-%E7%90%86%E5%B7%A5%E7%B3%BB%E6%95%B0%E5%AD%A6%E3%81%AE%E3%82%AD%E3%83%BC%E3%83%9D%E3%82%A4%E3%83%B3%E3%83%88-2-%E8%96%A9%E6%91%A9-%E9%A0%86%E5%90%89/dp/4000078623

https://linky-juku.com/linear-algebra-matome/



## 時系列データをベクトルとして表す

ある一点において時刻$i$に測定したデータを$x_i$ と書くことにする。測定回数は$N$回とする。このようなデータを縦に並べたものを,

<div class="page-break"></div> 

$$
\mathbf{x}=
\left.
\begin{bmatrix}
x_1 \\
x_2 \\
\vdots   \\
x_N \\
\end{bmatrix}
\right\}
 = N個
 \tag{1}
$$
書く。このように定義した$\mathbf{x}$を数ベクトルと呼ぶ。繰り返しになるが, 数ベクトルの第$i$成分は時刻$i$における測定値である。なお，(1)のように成分を縦にならべたものを縦ベクトルと呼ぶ。縦ベクトル$\mathbf{x}$の各成分$x_i$を下記のように横に並べたもの
$$
[\underbrace{x_1, x_2, \cdots x_N}_{N個}]
$$
を転置と呼ぶことがあり, $\mathbf{x}^T$と書くことがある。すなわち
$$
\mathbf{x}^T=[x_1, x_2, \cdots x_N]
\tag{2}
$$
である。$\mathbf{x}^T$のことを横ベクトルと呼ぶことがある。



## 時空間分布を行列として表す

$j=1 \cdots P$の$P$個の地点において時刻$i$に測定したデータを$z_{ij}$ と書くことにする。各地点における測定回数は$N$回とする。このとき，$N \times P$個の測定データ$z_{ij}$を下記のように一覧表の形に並べたものを行列と呼ぶ。
$$
\begin{equation*}
% a disposable command for avoiding repetitions
\newcommand{\zm}{%
  \begin{bmatrix}
    z_{11} & z_{12} & \dots & z_{1P}\\
    z_{21} & z_{22} & \dots & z_{2P}\\
    \vdots & \vdots & \ddots & \vdots\\
    z_{N1} & z_{N2} & \dots & z_{NP}\\
  \end{bmatrix}%
}
Z=\underset{N\times P}{(z_{ij})}=
  \left.
  \,\smash[b]{\underbrace{\!\zm\!}_{\textstyle\text{$P$ 列}}}\,
  \right\}\text{$N$ 行}
  \vphantom{\underbrace{\zm}_{\text{$P$ 列}}}
  \end{equation*}
  \tag{3}
$$
模式的に書くと<font color="red">$z_{ij}=z_{時刻 \; 場所}$</font>となる。



### 縦ベクトルと横ベクトルの積

いま, 縦ベクトル
$$
\mathbf{u}:=
\left.
\begin{bmatrix}
u_1 \\
u_2 \\
\vdots   \\
u_N \\
\end{bmatrix}
\right\}
 = N個
$$
と横ベクトル,
$$
\mathbf{v}^T=[\underbrace{v_1, v_2, \cdots v_P}_{P個}]
$$
の積（直積と呼ぶことがある）を,

<div class="page-break"></div> 

$$
\begin{equation*}
% a disposable command for avoiding repetitions
\newcommand{\zm}{%
  \begin{bmatrix}
    u_1v_1 & u_1v_2 & \dots & u_1 v_P\\
    u_2v_1 & u_2v_2 & \dots & u_2 v_P\\
    \vdots & \vdots & \ddots & \vdots\\
    u_Nv_1 & u_Nv_2 & \dots & u_N v_P\\
  \end{bmatrix}%
}
\mathbf{u}\mathbf{v}^T
=
  \left.
  \,\smash[b]{\underbrace{\!\zm\!}_{\textstyle\text{$P$ 列}}}\,
  \right\}\text{$N$ 行}
  \vphantom{\underbrace{\zm}_{\text{$P$ 列}}}
  \end{equation*}
  \tag{4}
$$
という行列で定義する。

例えば, 
$$
\mathbf{u}:=
\begin{bmatrix}
1 \\
2 \\
3 \\
\end{bmatrix}
$$

$$
\mathbf{v}^T=[4, 5]
$$

のとき,
$$
\begin{equation*}
% a disposable command for avoiding repetitions
\newcommand{\zm}{%
  \begin{bmatrix}
    1 \times 4 & 1 \times 5\\
    2 \times 4 & 2 \times 5\\
    3 \times 4 & 3 \times 5\\
  \end{bmatrix}%
}
\mathbf{u}\mathbf{v}^T
=
  \left.
  \,\smash[b]{\underbrace{\!\zm\!}_{\textstyle\text{$2$ 列}}}\,
  \right\}\text{$3$ 行}
  \vphantom{\underbrace{\zm}_{\text{$2$ 列}}}
  \end{equation*}
$$
である。

一般に, $N$行$P$列の行列は, $N\times P$個のデータから構成されるが, $\mathbf{u}\mathbf{v}^T$は$N$個の数と$P$個の数の各々の積を用いてはいるものの,  実質的には<font color="red">$N＋P$個の数で表現可能</font>であることに注意する。以下に見るように，$N$と$P$が非常に大きくなった時, このことから大きなご利益を得ることが出来る。



## 行列の分解

いま, 便宜上$N<P$と仮定する。気象データなどはこの条件を満たすものが多い。いま, ベクトル$\mathbf{u_m}$, $\mathbf{v_m}$を


$$
\mathbf{u_m}=
\left.
\begin{bmatrix}
u_{1m} \\
u_{2m} \\
\vdots   \\
u_{Nm} \\
\end{bmatrix}
\right\}
 = N個
$$

$$
\mathbf{v}_m^T=[\underbrace{v_{m1}, v_{m2,}, \cdots v_{mP}}_{P個}]
$$

を定義する。 計算方法を含め詳細は補足資料に譲るが, このとき, (3)のような行列は,
$$
\begin{eqnarray}
Z &=& 
\sigma_1 \mathbf{u}_1 \mathbf{v}_1^T + 
\sigma_2 \mathbf{u}_2 \mathbf{v}_2^T + 
\cdots +
\sigma_m \mathbf{u}_m \mathbf{v}_m^T
\cdots +
\sigma_N \mathbf{u}_N \mathbf{v}_N^T
\end{eqnarray}\tag{5}
$$
のように分解することが出来ることが知られている。このような分解のことを行列の特異値分解という。また, 添え字$m$のことを**モード**と呼ぶことがある。$N > P$の場合についても考え方は全く同じなので, 補足資料を参照のこと。$\mathbf{u}_m$, $\mathbf{v}_m$のことを特異ベクトルと呼び, $\sigma_m$のことを特異値と呼ぶことがある。 

下記の例で(5)が可能なことに違和感を感じることがなければ，(5)の根拠については解析に慣れてからでも良いと思う (冒頭で述べた線形代数の知識が必要であるため）。

いま, 簡単な例として, 
$$
Z=\begin{bmatrix}
   4 &  5\\
   8 & 5\\
   12 &  5\\
  \end{bmatrix}
$$
という行列を考える。この場合$Z$は,
$$
&&17.0513
\begin{bmatrix}
   -0.3475 \\
   -0.5526 \\
   -0.7576
  \end{bmatrix}
  [-0.8739, -0.4861] \\
  
&+&2.8731
\begin{bmatrix}
   0.8441 \\
   0.1675 \\
   -0.5093
  \end{bmatrix}
  [-0.4861,0.8739] \\
&=&\begin{bmatrix}
   3.9993 & 4.9997\\
   8.0004 & 5.0009\\
  12.0004 & 5.0007 
  \end{bmatrix} \\
  
 &\simeq& Z
 \tag{7}
$$
と分解できる (パソコンによる計算例を付録1に記した)。

　いま, $\sigma_m$ ($m=1,\cdots,N)$で$m$が小さいほど$\sigma_m$の値が大きくなるように番号をふることにする。このとき $\sigma_1 \gg \sigma_m \;(m>1)$ であれば, (5)は,
$$
\mathbf{u_1}:=
\left.
\begin{bmatrix}
u_{11} \\
u_{21} \\
\vdots   \\
u_{N1} \\
\end{bmatrix}
\right\}
 = N個
$$

$$
\mathbf{v}_1^T=[v_{11}, v_{12,}, \cdots v_{1P}]
$$

として, 
$$
\begin{eqnarray}
Z &\simeq& 

\sigma_1 \begin{bmatrix}
u_{11} \\
u_{21} \\
\vdots   \\
u_{N1} \\
\end{bmatrix} [v_{11} \;v_{12} \; \cdots v_{1P} \;]\\

&=&
\begin{bmatrix}
u_{11}v_{11} & u_{11}v_{12} &  \dots  & u_{11}v_{1P} \\
u_{21}v_{11} & u_{21}v_{12} &  \dots  & u_{21}v_{1P} \\
\vdots       & \vdots       &  \vdots & \vdots       \\
u_{N1}v_{11} & u_{N1}v_{12} &  \dots  & u_{N1}v_{1P} \\
\end{bmatrix}

\end{eqnarray}\tag{8}
$$
と近似できる。これは$N \times P$個の要素を持つ行列$Z$を, それよりはるかに要素数の少ない$N+P$個の要素を持つ行列$\sigma_1 \mathbf{u}_1 \mathbf{v}_1^T$で近似できることを意味する。

緯度経度方向に1度間隔で地球上をくまなく覆う地点において, 各測点で1年に1回測定した30年分のデータが有る場合, $N$が30個, $P$が6万個ほどになり, データ数は$N\times P=$180万個となるが, $m$を3個ほど用いれば事足りる場合, (5)で使用されるデータ数は約$m \times P$=18万個と約1/10に情報を圧縮することができる。

さらに, 大気データを解析する場合に重要なこととして次の性質が挙げられる。

- **$m$が小さいほど$\sigma_m$の値が大きくなるように番号をふった場合**, **$m$が小さいほどデータの大局的な分布が反映される**ことが多い

- そのため, 小数の$m$で元々$Z$がもつ時空間変動の大部分が表現できることが多い

すなわち, **小規模な現象がふるいにかけられ**, **規模の大きい変動が分析結果として抽出される**という点が重要である。

(5)の右辺の第$m$番目の項を第$m$モードと呼び, $Z_m$と書くことにすると,
$$
Z_m &\simeq& 

\sigma_m \begin{bmatrix}
u_{1m} \\
u_{2m} \\
\vdots   \\
u_{Nm} \\
\end{bmatrix} [v_{m1} \;v_{m2} \; \cdots v_{mP} \;]\\
&=&\begin{bmatrix}
u_{1m}v_{m1} & u_{1m}v_{m2} &  \dots  & u_{1m}v_{mP} \\
u_{2m}v_{m1} & u_{2m}v_{m2} &  \dots  & u_{2m}v_{mP} \\
\vdots       & \vdots       &  \vdots & \vdots       \\
u_{Nm}v_{m1} & u_{Nm}v_{m2} &  \dots  & u_{Nm}v_{mP} \\
\end{bmatrix}

\tag{9}
$$
と書くことが出来る。(3)と(9)を比較すると,
$$
\begin{eqnarray}
z_{ij}=z_{時刻 \; 場所} \rightarrow \sigma_m u_{im}v_{mj}
\end{eqnarray}
$$
という対応関係になっているので, 
$$
\begin{eqnarray}
\mathbf{v}_m:=
\begin{bmatrix}
v_{m1} \\
v_{m2} \\
\vdots   \\
v_{mP} \\
\end{bmatrix}
\end{eqnarray}
\tag{10}
$$
を$Z$の第$m$モードの空間分布を示す数ベクトルと解釈できる。同様に, 
$$
\begin{eqnarray}
\mathbf{u}_m:=
\begin{bmatrix}
u_{1m} \\
u_{2m} \\
\vdots   \\
u_{Nm} \\
\end{bmatrix}
\end{eqnarray}
\tag{11}
$$
を$Z$の第$m$モードの時間変化を表す数ベクトルと解釈できる。

(5)のように分解したとき, $\cdot \,$をベクトルの内積を表す記号とすると, $\mathbf{u}_m$と$\mathbf{v}_m$には,
$$
\mathbf{u}_m \cdot \mathbf{u}_n = 0 \quad m \ne n \\
\mathbf{v}_m \cdot \mathbf{v}_n = 0 \quad m \ne n
$$
 という重要な性質がある。これを直交性と呼ぶ。(7)の例で, $\mathbf{u}_1$と $\mathbf{u}_2$の内積を計算してみると,
$$
\begin{bmatrix}
   -0.3475 \\
   -0.5526 \\
   -0.7576
  \end{bmatrix}
\cdot  
\begin{bmatrix}
   0.8441 \\
   0.1675 \\
   -0.5093
  \end{bmatrix}
=-3.957\times 10^{-5}\simeq0
$$
であり, また，$\mathbf{v}_1$と $\mathbf{v}_2$の内積を計算してみると,やはり
$$
\begin{bmatrix}
   -0.8739 \\
   -0.4861 \\
  \end{bmatrix}
  \cdot
\begin{bmatrix}
   -0.4861 \\
    0.8739 \\
  \end{bmatrix}
  =0
$$
となる。

上記の性質は,**モード番号が異なる2つのベクトルは, それぞれ相手のモードと同じ方向を向く成分を全く含まない**ことを意味する。つまり, 一方のモードのベクトルから相手のモードのもつ成分を決して抜き出すことが出来ないのである。簡単な例として，
$$
\mathbf{e}_1=\begin{bmatrix}
   1 \\
   0 \\
  \end{bmatrix},
\quad
\mathbf{e}_2=\begin{bmatrix}
   0 \\
   1 \\
  \end{bmatrix},
  \quad
\mathbf{e}=\begin{bmatrix}
   1 \\
   1 \\
  \end{bmatrix}
$$
というベクトルを考える。いま, $\mathbf{e}_1 \cdot \mathbf{e}_2=\mathbf{e}_2 \cdot \mathbf{e}_1=0$であるが, このとき, $\mathbf{e}_1$は$\mathbf{e}_2$を使って表すことが出来ず, $\mathbf{e}_2$は$\mathbf{e}_1$を使って表すことが出来ない。一方,  $\mathbf{e}_1 \cdot \mathbf{e} \ne 0$，$\mathbf{e}_2 \cdot \mathbf{e} \ne 0$である。このとき, 
$$
\mathbf{e}=\mathbf{e}_1+\mathbf{e}_2
$$
であり，$\mathbf{e}$は, $\mathbf{e}_1$と$\mathbf{e}_2$を使って表すことが出来る。つまり, 直交する2つのベクトルは一方のベクトルを使って他方のベクトルを表現することが出来ない。

上記のことを, モードの異なる特異ベクトルは独立である，ということがある。$\mathbf{u}_m$, $\mathbf{v}_m$がそれぞれ第$m$モードの時間変化と空間分布を表すという, 上記の解釈をする場合には, 一方のモードの空間分布を相手のモードから抜き出すことが決してできないという意味で, 独立であるということがある。時間変化に関しても同様である。

各モードが独立であるということは, 

- 情報の重複が無いという意味で, (5)は最も効率よく無駄が無い分解になっている
- 各モードが大気中に存在する固有の変動を表しているとすれば, それぞれは物理的に別個の現象である可能性が有る

ということを意味する。

実際の大気データに上記の分解を行った例を別資料で説明する。

なお，気象学では上記の分解の方法ではなく, 最大共分散分析という分析手法のことを特異値分解と呼ぶことがあるので注意する（上記と全く同じ演算を使用しているためだと思われる）



## 付録

### 付録1 パソコンによる式(7)の計算例

gnu octave (https://www.kkaneko.jp/tools/win/octave.html) というソフトを利用した

```bash
octave:1> A=[4,5;8,5;12,5]
A =

    4    5
    8    5
   12    5

octave:2>  [U,S,V] = svd(A,"vector")
U =

  -0.34754   0.84412
  -0.55255   0.16739
  -0.75756  -0.50935

S =

Diagonal Matrix

   17.0513         0
         0    2.8731

V =

  -0.87391  -0.48608
  -0.48608   0.87391

octave:3> 17.0513*[-0.3475,-0.5526,-0.7576]'*[-0.8739,-0.4861]+2.8731*[0.8441,0.1675,-0.5093]'*[-0.4861,0.8739]
ans =

    3.9993    4.9997
    8.0004    5.0009
   12.0004    5.0007
```


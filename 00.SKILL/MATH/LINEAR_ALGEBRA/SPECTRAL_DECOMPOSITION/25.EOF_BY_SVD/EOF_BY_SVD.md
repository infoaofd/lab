# 特異値分解によるEOF解析

EOFとは, Empirical Orthogonal Functionの略称であり, 日本語では経験的直交関数展開と呼ばれる。直交するベクトルを用いて与えられたデータを表現し直す手法であることから, このような名称で呼ばれている。数学では関数を次元が無限大のベクトルと解釈することがあるので, 関数という用語が用いられている。



## 行列を使った時空間データの表現

いま, 行列$Z$が時間方向に$N$個, 空間方向に$P$個からなるデータを表す, 次のような行列であるとする。
$$
Z=(z_{ij})=
\begin{bmatrix}
z_{11} & z_{12} & \cdots & z_{1P} \\
z_{21} & z_{22} & \cdots & z_{2P} \\
\vdots & \vdots & \ddots & \vdots  \\
z_{N1} & z_{N2}&   \cdots & z_{NP} \\
\end{bmatrix}\tag{1}
$$
$i$を時刻を表す添え字, $j$を場所を表す添え字とすると, 模式的に<font color="red">$Z=z_{ij}=z_{時刻 \; 場所}$</font>と書ける。



## 注意

ここでは便宜上$N<P$と仮定する。再解析, 衛星データなどはこの条件を満たすものが多い。



## 特異値・特異ベクトルの計算

1. 実対称行列$Z^T Z$の固有値$\lambda_i $, 固有ベクトル$\mathbf{v}_i$ ($i=1,\cdots,P$)を求める。$\mathbf{v}_i$が右特異ベクトルになる。
2. $\mathbf{v}_i$をその大きさ$\|\mathbf{v}_i\|$で割って, $\|\mathbf{v}_i\|=1$となるようにしておく(規格化)。
3. 特異値を$\sigma_i=\sqrt{\lambda_i}$で計算する。
4. 左特異ベクトル$\mathbf{u}_i$を$\mathbf{u}_i = Z \mathbf{v}_i/\sigma_i$で計算する。



## 特異値分解

上で求めた特異値$\sigma_i$, 特異ベクトル$\mathbf{u}_i$, $\mathbf{v}_i$を用いると, $Z$は, 
$$
\begin{eqnarray}
Z &=& 
\sigma_1 \mathbf{u}_1 \mathbf{v}_1^T + 
\sigma_2 \mathbf{u}_2 \mathbf{v}_2^T + 
\cdots +
\sigma_2 \mathbf{u}_N \mathbf{v}_N^T
\\
&=&
\sigma_1 \begin{bmatrix}
u_{11} \\
u_{21} \\
\vdots   \\
u_{N1} \\
\end{bmatrix} [v_{11} \;v_{12} \; \cdots v_{1P} \;] +

\sigma_2 \begin{bmatrix}
u_{12} \\
u_{22} \\
\vdots   \\
u_{N2} \\
\end{bmatrix} [v_{12} \;v_{22} \; \cdots v_{2P} \;]  \\

&+ & \cdots \\


&+&\sigma_N \begin{bmatrix}
u_{1N} \\
u_{2N} \\
\vdots   \\
u_{NN} \\
\end{bmatrix} [v_{N1} \;v_{N2} \; \cdots v_{NP} \;]

\end{eqnarray}\tag{2}
$$
と分解できる。(1)と(2)を比較すると,

$$
\begin{eqnarray}
z_{ij}=z_{時刻 \; 場所} \rightarrow \sigma_m u_{im}v_{mj}
\end{eqnarray}
$$
という対応関係になっているので, 
$$
\begin{eqnarray}
\mathbf{v}_m:=
\begin{bmatrix}
v_{m1} \\
v_{m2} \\
\vdots   \\
v_{mP} \\
\end{bmatrix}
\end{eqnarray}
\tag{3}
$$
を$Z$の第$m$モードの空間分布を示すベクトルと解釈できる。同様に, 
$$
\begin{eqnarray}
\mathbf{u}_m:=
\begin{bmatrix}
u_{1m} \\
u_{2m} \\
\vdots   \\
u_{Nm} \\
\end{bmatrix}
\end{eqnarray}
\tag{4}
$$
を$Z$の第$m$モードの時間変化を表すベクトルと解釈できる。





## 空間分布の取得

もともとのデータが空間方向に$x$, $y$の2次元の座標を有する水平分布である場合を考え, $j$地点の$x$, $y$座標をそれぞれ$x_j$, $y_j$と書くことにする。このとき,
$$
(x_1, y_1, v_{1m}) \\
(x_2, y_2, v_{2m}) \\
\vdots \\
(x_P, y_P, v_{Pm}) \\
$$
となるデータを作成すれば, これは$Z$の第$m$モードの水平分布を表すデータとなる。

空間方向に1次元や3次元の場合も同様に考えることができる。

<font color="red">添え字$j$</font>はあくまで<font color="red">空間内のある一点を指し示す地点番号</font>であることに注意する。



## 時系列の取得

$\mathbf{v}_i$は対称行列$Z^T Z$の固有ベクトルであるから直交する。したがって, $\| \mathbf{v}_i \|=1$の場合には
$$
\begin{eqnarray}
\mathbf{v}_i^T \mathbf{v}_j = \delta_{i,j}
\end{eqnarray}\tag{7}
$$
である。$\delta_{ij}$はクロネッカーのデルタと呼ばれる記号で,
$$
\begin{eqnarray}
\delta_{i,j}:=
\left\{
\begin{array}{ll}
1 & (i=j)\\
0 & (i\neq j)
\end{array}
\right.

\end{eqnarray}
$$
で定義される。

(2)に$Z$に$\mathbf{v}_m$を右からかけると,
$$
\begin{eqnarray}
Z \mathbf{v}_m &=& 
\sigma_1 \mathbf{u}_1 \mathbf{v}_1^T \mathbf{v}_m + 
\sigma_2 \mathbf{u}_2 \mathbf{v}_2^T \mathbf{v}_m + 
\cdots +
\sigma_m \mathbf{u}_m \mathbf{v}_m^T \mathbf{v}_m + 
\cdots +
\sigma_2 \mathbf{u}_N \mathbf{v}_N^T \mathbf{v}_m
\\
&=&
0 + 
0 + 
\cdots +
\sigma_m \mathbf{u}_m+ 
\cdots +
0
\\
&=&
\sigma_m \mathbf{u}_m
\end{eqnarray}
$$
であるから,
$$
\begin{eqnarray}
\mathbf{u}_m=Z \mathbf{v}_m/\sigma_m
\end{eqnarray}
$$
によって, 第$m$モードの時系列を求めることができる。



## 注意

教科書や論文では, EOF解析を説明する際，まず
$$
V=Z^TZ
$$
で定義される誤差共分散行列$V$ (もしくは$V$と同等の相関行列)の固有値・固有ベクトルを求めるところから説明を始めることが多い。これは, 計算の手順としては, 特異値分解において特異値・特異ベクトルを求める際に, まず$Z^TZ$という行列を作っていることと全く同じである。

誤差共分散行列$V$の固有値・固有ベクトルを求める自体にも重要な意味があるのだが, 知らなくても, EOFとは行列の形で与えられたデータを直交するベクトルを利用して表現し直す手法であるという, EOF解析の肝となる部分は理解できることと，説明が長くなることから省略する。これに関しては数多くの多変量解析と名の付く書籍に詳しく説明されていることが多い。ウェブ検索しても下記のような大変良質な資料が多数ヒットする。

https://wiki.cis.iwate-u.ac.jp/~suzuki/biostat/slide/14.pdf

概要を述べるとつぎのようになる（理解できなくても問題ない）。

観測点$j$の時刻$i$における観測データ$z_{ij}$に係数をかけて, $j$について足し合わせた指標$w_i$
$$
w_i=a_1z_{i1}+\cdots+a_pz_{ip}=\sum_{j=1}^{P}a_j z_{ij}
$$
を考える。このような指標$w_i$を考えるのは, $z_{ij}$がなんらかの自然現象を表す量であれば, 空間パターンはなんらかのルールに従っているであろうから, そのパターンを$a_j$という係数を使って表せないかという考え方がもとになっている。なお，統計学においてはこの$w_i$のことを主成分と呼ぶ。

このとき, $w_i$の時刻$i$に関する分散$VAR(w)$を,
$$
VAR(w)=\sum_{i=1}^{N}(w_i - \overline{w})^2/(N-1)
\tag{8}
$$
で定義する。
$$
a_1^2+a_2^2 + \cdots + a_n^2 =1
\tag{9}
$$
という条件のもとで, この$VAR(w)$を最大になるように係数$a_j$を決めたとすると, その時, 時系列データ $w_i$ ($i=1,\cdots N$)は元のデータ$z_{ij}$の時間変動に関する情報を一番もっている空間パターン$a_j$を表すことになる。(9)を満たしながら, (8)の$VAR(w)$が最大となる$a_j$を求める問題は，制約つき最大化問題などと呼ばれることがあるが, この問題は誤差共分散行列の固有値・固有ベクトルを求める問題と数学的には同値となることが古くから知られている。






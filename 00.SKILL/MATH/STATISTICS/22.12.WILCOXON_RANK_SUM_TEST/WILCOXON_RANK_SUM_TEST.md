# WILCOXON_RANK_SUM_TEST

[[_TOC_]]

## 解説

ウィルコクソンの順位和検定 (Wilcoxon rank sum test) とはノンパラメトリック検定のひとつである．

ウィルコクソンの順位和検定は得られた2つのデータ間の代表値 (中央値) に差があるかどうかを検定する．主にサンプル数が少なく得られたデータに正規性を仮定できないときに用いられる．本検定では，観測値の大小を順位に置き換えて統計的推定を行う．

マン-ホイットニーのU検定 (Mann-Whitney U test) と呼ばれる検定法と実質的には同じものである．一方で，ウィルコクソンの符号順位検定とは異なる検定法なので注意が必要．ウィルコクソンの順位和検定 (およびマン-ホイットニーのU検定) は，パラメトリック検定における対応のないt検定，すなわち，スチューデントのt検定とかウェルチのt検定に対応するものであるが，ウィルコクソンの符号順位検定はパラメトリック検定における対応のあるt検定に相当するものである．

https://data-science.gr.jp/theory/tst_wilcoxon_rank_sum_test.html



この結果と補遺の順位和の取り方を考えますと、分布が左右対称である者同士、または分布の型が同じ場合、Wilcoxon の順位和検定は中央値の検定と考えてもよく、分布の型が異なる場合は、中央値の検定と考えるには無理がありそうです。

https://www.heisei-u.ac.jp/ba/fukui/tips/tip023.pdf





![image-20240131150104149](image-20240131150104149.png)

![image-20240131150119296](image-20240131150119296.png)

## Wilcoxonの順位和検定のサンプル

![image-20240131150141481](image-20240131150141481.png)

![image-20240131150158937](image-20240131150158937.png)

#### SAMPLE DATA

https://waidai-csc.jp/updata/2019/05/ed6ffbab7b7dd8644913ee0a31e5b477.pdf

非飲酒群
34人

```
22 11 13 13 21 19 0 3 0 16 16 18 22 27 22 23 3 18 44 45 17 27 31 25 19 30 16 52 376 19 27 68 18 0
```

飲酒群
66人

```
23 15 116 87 18 95 52 17 87 22 67 12 14 33 238 33 18 16 32 24 103 20 25 21 88 55 0 29 106 34 98 48 55 0 14 22 190 32 66 35 45 19 413 16 0 33 37 13 17 102 33 25 36 12 0 57 174 40 83 21 18 8 47 31 0 155
```



### WEB

https://www.gen-info.osaka-u.ac.jp/MEPHAS/wilc1.html

検定統計量Ｕ＝ 746.5000000
期待値＝ 1122.0000000 　分散＝ 18863.31
検定統計量Ｚ＝ 2.734015

危険率： 0.0062567

### 検定結果： P < 0.01

------

#### 結果のみかた

ここでは２群間の平均値に差があるかないかがわかります。
結果に「ｐ＜０．０１」と表示されているとき、[有意水準](http://www.gen-info.osaka-u.ac.jp/testdocs/tomocom/express/express11.html)１％で[帰無仮説](http://www.gen-info.osaka-u.ac.jp/testdocs/tomocom/express/express11.html)は[棄却](http://www.gen-info.osaka-u.ac.jp/testdocs/tomocom/express/express12.html)される、つまり「有意水準１％で平均値に差がある」といえます。
結果に「ｐ＜０．０５」と表示されているとき、有意水準５％で帰無仮説は棄却される、つまり「有意水準５％で平均値に差がある」といえます。
結果に「ｐ＞０．０５」と表示されているとき、帰無仮説は保留される、つまり「平均値に差がない可能性が高い」といえます。

有意水準について
α＝0.05（有意水準５％）というのは、同じような条件下で検定を行った時、１回の決定で誤って帰無仮説を棄却してしまう確率が５％であるということです。これは危険率ともよばれます。
通常は統計数値表に従い検定結果を判断していますが、例数が十分に多い場合や同一値のデータが含まれている場合など、統計数値表を参照できない場合は、近似的な危険率を算出、表示し、検定結果を判断しています。

### R

http://labs.kbs.keio.ac.jp/naoki50lab/nonparam2.pdf

```R
A = c(22, 11, 13, 13, 21, 19, 0, 3, 0, 16, 16, 18, 22, 27, 22, 23, 3, 18, 44, 45, 17, 27, 31, 25, 19, 30, 16, 52, 376, 19, 27, 68, 18, 0)
B = c(23, 15, 116, 87, 18, 95, 52, 17, 87, 22, 67, 12, 14, 33, 238, 33, 18, 16, 32, 24, 103, 20, 25, 21, 88, 55, 0, 29, 106, 34, 98, 48, 55, 0, 14, 22, 190, 32, 66, 35, 45, 19, 413, 16, 0, 33, 37, 13, 17, 102, 33, 25, 36, 12, 0, 57, 174, 40, 83, 21, 18, 8, 47, 31, 0, 155)
wilcox.test(A,B)
```

• c( ) の中にカンマで区切って数値を並べると，ベクトルを生成することができる．ここで，=は数学でいう等号ではなく，それは A という変数（この場合はベクトル）に c( ) の中に書かれている数値を代入（格納）することを意味する．

上記のコードを実行するには，1 行ごとにカーソルを行末に持って いき，「実行」ボタンを押す必要がある．出力結果は次の通りである．

```R
(ins) > A = c(22, 11, 13, 13, 21, 19, 0, 3, 0, 16, 16, 18, 22, 27, 22, 23, 3, 18, 44, 45, 17, 27, 31, 25, 19, 30, 16, 52, 376, 19, 27, 68, 18, 0)
(ins) > B = c(23, 15, 116, 87, 18, 95, 52, 17, 87, 22, 67, 12, 14, 33, 238, 33, 18, 16, 32, 24, 103, 20, 25, 21, 88, 55, 0, 29, 106, 34, 98, 48, 55, 0, 14, 22, 190, 32, 66, 35, 45, 19, 413, 16, 0, 33, 37, 13, 17, 102, 33, 25, 36, 12, 0, 57, 174, 40, 83, 21, 18, 8, 47, 31, 0, 155)
(ins) > wilcox.test(A,B)

        Wilcoxon rank sum test with continuity correction

data:  A and B
W = 746.5, p-value = 0.006326
alternative hypothesis: true location shift is not equal to 0
```

**p-value = 0.006326**

### FORTRAN

WILCOXON_RANK_SUM_EX2.F90

```fortran
! https://bellcurve.jp/statistics/course/26101.html
! https://qiita.com/kenkenvw/items/d7d5930bef3cc923c061
! 以下は、FortranでWilcoxon Rank Sum
! Testを実行するプログラムの基本的な例です。
! このプログラムは、2つの独立したサンプルからなるデータを入力として受け取り、
! それらのサンプルの中央値が等しいかどうかを検定します。
! Wilcoxon Rank Sum
! Testは、非対称なデータや外れ値にも強い統計的手法の一つです。

program WILCOXON_RANK_SUM
implicit none

! INPUT DATA
! https://waidai-csc.jp/updata/2019/05/ed6ffbab7b7dd8644913ee0a31e5b477.pdf
integer, parameter :: n1 = 34
integer, parameter :: n2 = 66
real(8), dimension(n1) :: sample1 = [22, 11, 13, 13, 21, 19, 0, 3,&
0, 16, 16, 18, 22, 27, 22, 23, 3, 18, 44, 45, 17, 27, 31, 25, 19, &
30, 16, 52, 376, 19, 27, 68, 18, 0]

real(8), dimension(n2) :: sample2 = [23, 15, 116, 87, 18, 95, 52, &
17, 87, 22, 67, 12, 14, 33, 238, 33, 18, 16, 32, 24, 103, 20, 25, &
21, 88, 55, 0, 29, 106, 34, 98, 48, 55, 0, 14, 22, 190, 32, 66,   &
35, 45, 19, 413, 16, 0, 33, 37, 13, 17, 102, 33, 25, 36, 12, 0,  &
57, 174, 40, 83, 21, 18, 8, 47, 31, 0, 155]

! 変数
real(8), dimension(n1+n2) :: combined_sample
integer:: rank(n1+n2), IFLAG(n1+n2)
real(8):: rank_avg(n1+n2)
real(8):: dnx,dny,U,avg_U,var_U,z
real(8) :: rank_sum1, rank_sum2, rank_sum
real(8) :: p_value
integer i,n,count
! コンバインドされたサンプルを作成
n=n1+n2
combined_sample(1:n1) = sample1
combined_sample(n1+1:n1+n2) = sample2

!do n=1,n1+n2
!print *,n,sngl(combined_sample(n))
!end do

! ランクを計算
call rankdata(combined_sample, rank_avg, rank, IFLAG)

print *
print '(A)',"MMMMM INPUT DATA AND RANK"
do i=1,n1+n2
print '(i3,2x,f5.0,f7.2)',i,sngl(combined_sample(i)),rank_avg(i)
end do

! ランク和を計算
rank_sum1 = SUM(rank_avg(1:n1))
rank_sum2 = SUM(rank_avg(n1+1:n1+n2))

if(n1<=n2)then
dnx=dble(n1); dny=dble(n2)
rank_sum=rank_sum1
else
dnx=dble(n2); dny=dble(n1)
rank_sum=rank_sum2
end if

U=(dnx*dny)+(dnx*(dnx+1.0)/2.d0)-(rank_sum)
avg_U=(dnx*dny/2.d0)
var_U=(dnx*dny)*(dnx+dny+1.d0)/12.d0
z=abs(U-avg_U)/sqrt(var_U)

! p値を計算
p_value = (1.d0-normcdf(z))*2.d0

print *
print '(A)',"MMMMM RESULT"
print '(A,f10.2)',"rank_sum=",rank_sum
print '(A,f10.2)',"dnx     =",dnx
print '(A,f10.2)',"dny     =",dny
print '(A,f10.2)',"U       =",U
print '(A,f10.2)',"avg_U   =",avg_U
print '(A,f10.2)',"var_U   =",var_U
print '(A,f10.2)',"z       =",z
print '(A,f10.4)',"p_value =",p_value

contains

! ランク付けのサブルーチン
subroutine rankdata(data,rank_avg,rank,IFLAG)
real(8), intent(inout) :: data(:),rank_avg(:)
integer, intent(inout) :: rank(:),IFLAG(:)
integer :: i, j, n, COUNT
REAL(8)::SUM
    
n = size(data)

do i = 1, n
  rank(i) = 1
  do j = 1, n
    if (data(j) < data(i) .or. (data(j) == data(i) .and. j < i)) then
      rank(i) = rank(i) + 1
    endif
  end do
end do

!同着の場合は平均とする
rank_avg=dble(rank)
IFLAG=0
do i = 1, n
  COUNT=0;SUM=0.0
  do j = 1, n
    if ( data(j) == data(i) ) then
      COUNT = COUNT + 1
      SUM=SUM+dble(rank(j))
      IFLAG(J)=1
    endif
  end do
  if(IFLAG(i)/=0)then
    rank_avg(i)=SUM/dble(COUNT)
  end if
end do

end subroutine rankdata



real(8) function normcdf(x)
    ! 標準正規分布の累積分布関数
    real(8), intent(in) :: x
    real(8) :: t, ans
    real(8), parameter :: b0 = 0.2316419, b1 = 0.319381530, &
    b2 = -0.356563782
    real(8), parameter :: b3 = 1.781477937, b4 = -1.821255978, &
    b5 = 1.330274429
    real(8),parameter::pi=3.141592653589793

    t = 1.0 / (1.0 + b0 * abs(x))
    ans = 1.0 - (b1 * t + b2 * t**2 + b3 * t**3 + b4 * t**4 + b5 *&
    t**5) * exp(-x**2 / 2.0) / sqrt(2.0 * pi)

   normcdf = ans
end function normcdf

end program WILCOXON_RANK_SUM
```

```bash
MMMMM RESULT
rank_sum=   1341.50
dnx     =     34.00
dny     =     66.00
U       =   1497.50
avg_U   =   1122.00
var_U   =  18887.00
z       =      2.73
p_value =    0.0063
```

参考

```FORTRAN
  FUNCTION ZScore(Z) RESULT(p_value)
    REAL(8), INTENT(IN) :: Z
    REAL(8) :: p_value

    p_value = 0.0

    ! You may need to replace the following with your own Z-score to p-value conversion logic

    IF (Z < -3.4) THEN
      p_value = 0.0003
    ELSE IF (Z < -3.3) THEN
      p_value = 0.0005
    ELSE IF (Z < -3.2) THEN
      p_value = 0.0007
    ! ... Add more cases as needed

    END IF

  END FUNCTION ZScore
```




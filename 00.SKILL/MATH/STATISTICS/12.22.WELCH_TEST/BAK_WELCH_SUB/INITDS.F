      function initds(dos,nos,eta)
c***begin prologue  initds
c***date written   770601   (yymmdd)
c***revision date  820801   (yymmdd)
c***category no.  c3a2
c***keywords  chebyshev,double precision,initialize,
c             orthogonal polynomial,series,special function
c***author  fullerton, w., (lanl)
c***purpose  initializes the d.p. properly normalized orthogonal
c            polynomial series to determine the number of terms needed
c            for specific accuracy.
c***description
c
c initialize the double precision orthogonal series dos so that initds
c is the number of terms needed to insure the error is no larger than
c eta.  ordinarily eta will be chosen to be one-tenth machine precision
c
c             input arguments --
c dos    dble prec array of nos coefficients in an orthogonal series.
c nos    number of coefficients in dos.
c eta    requested accuracy of series.
c***references  (none)
c***routines called  xerror
c***end prologue  initds
c
      double precision dos(nos)
c***first executable statement  initds
      if (nos.lt.1) call xerror ( 'initds  number of coefficients lt 1',
     1 35, 2, 2)
c
      err = 0.
      do 10 ii=1,nos
        i = nos + 1 - ii
        err = err + abs(sngl(dos(i)))
        if (err.gt.eta) go to 20
 10   continue

 20   if (i.eq.nos) call xerror ( 'initds  eta may be too small', 28,
     1  1, 2)
      initds = i
c
      return
      end

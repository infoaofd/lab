# 平均をとる順序の影響

[[_TOC_]]

## 要点

> 変数同士の掛け算をしてから平均をとるのと，各変数の平均を求めてから掛け算をするのでは結果が異なる。

例えば，風速と気温の積で表される量 (温度フラックス) を月平均を計算した値と，風速の月平均と気温の月平均の積の値は，一致しない

## 理由

任意の量 $x_i \; (i=1,\cdots\,N)$ の 平均値を $\overline{x}$ と書くことにする。

いま，$T_1=5, T_2=3$, $u_1=2$，$u_2=4$ というデータがあったとする。

$\overline{T}=(5+3)/2=4$，$\overline{u}=(2+4)/2=3$ である。これより，$\overline{T}\;\overline{u}=4\times3=12$である。一方，$\overline{Tu}=(5\times 2+3\times 4)/2=22/2=11$ となり，
$$
\overline{T}\;\overline{u}\ne\overline{Tu}
$$
である。

変数の掛け算 (割り算) が含まれる量の平均値を求めるときは，掛け算 (割り算) を先に行っておかなければならない。

足し算 (引き算) しか含まれない量に関しては，平均を求める計算と，足し算 (引き算) の順序を入れ替えてもよい。  

 


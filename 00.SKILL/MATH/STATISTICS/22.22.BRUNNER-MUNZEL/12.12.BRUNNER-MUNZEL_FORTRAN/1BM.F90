double precision :: x(14) = [1,2,1,1,1,1,1,1,1,1,2,4,1,1]
double precision :: y(11) = [3,3,4,3,1,2,3,1,1,5,4]

nx=14
ny=11
alpha=0.05d0
alter=1

CALL bm_test(nx,ny,x,y,alpha,alter,pst,ci,stat,df,pval)

print *,pst
print *,ci
print *,stat
print *,df
print *,pval

end

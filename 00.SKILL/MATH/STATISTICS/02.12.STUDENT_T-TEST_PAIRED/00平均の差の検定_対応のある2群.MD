C:\Users\foome\lab\00.SKILL\ANALYSIS\STATISTICS\TEST\02.12.STUDENT_T-TEST_PAIRED

# 平均の差の検定 (対応のある2群)

[[_TOC_]]

## 考え方

**差 ($d$) を元データと考え，$d$の平均$\overline {d}=0$を棄却できないか調べる**。

## 例

ここで考えるのは，次のような実験の効果を統計的に検証する方法である。血圧を下げる薬の効果を見る実験を行う。被験者3人に対して薬の投与前と投与後の血圧を測定し，各被験者の投薬前後の血圧を比較することで、投薬によって血圧は下がったと言えるか。

|                |    投与前 (A群)    |    投与後 (B群)    | 差, $d_i \, (=A_i-B_i)$ |
| :------------: | :----------------: | :----------------: | :---------------------: |
|   サンプル1    |        180         |        120         |           60            |
|   サンプル2    |        200         |        150         |           50            |
|   サンプル3    |        250         |        150         |           100           |
| 群内の標本平均 | $\overline{A}=210$ | $\overline{B}=140$ |    $\overline{d}=70$    |

上の表のように，比較対象とする2つのサンプルの集まり (標本群) を，ここではA群，B群と呼ぶことにする。それぞれの群の標本数は3個とする ($n=3$)。A群のデータを ($A_1, A_2, A_3$) を表記し，B群のデータを ($B_1, B_2, B_2$)と表記することにする。

例えば$A_1$と$B_1$のように，添え字が同じサンプルは，ただ1つの条件を除き，同じ条件下で同じ個体に対して得られた測定値とする。このような場合を**対応のある2群**と呼ぶことがある。

対応のある2群の平均値に差があるかどうか検定するためには，いったん**A群とB群の各標本の差の平均$d$が0であると仮定**して，**これが棄却できないかを検討する**。棄却できる場合は，「少なくても与えられたデータを見る限り，$d$が0であるようなことはめったに起こらない」，と結論できる。「めったに」というのをどのくらいの頻度ととらえるかは解析者が決めることだが，20回に1回 (5%)，10回に1回 (10%) を基準とすることが多い。100回に1回 (1%)，1000回に1回 (0.1%) などのように，基準を非常に厳しく設定することもある。

例として上の表のデータで検討する。$u^2$を$d$の標本不偏分散とする。$u^2$の値は，
$$
\begin{eqnarray}
u^2&=&\frac{1}{3-1}\bigg((60-70)^2+(50-70)^2+(100-70)^2\bigg) \\
&=& 700
\end{eqnarray}
$$
標本不偏標準偏差 $u$は，$u=\sqrt{700}=26.5$となる。

ここで，
$$
t_{\overline{d}}=\frac{\overline{d}}{\frac{u}{\sqrt{n}}} \tag{1}
$$
で定義される$t$‐値 (ティーち)と呼ばれる量が0から大きく外れた値となる場合，$d$は0であるという仮定が棄却される。すなわち，$d$が0であるようなことはめったに無い，と言える。このことは，「統計学的にはA群とB群の平均値は異なると判断できる」ということを意味する。

$t_{\overline{d}}$の外れ度合いを評価するためには，$t$分布と呼ばれる確率分布の値を知る必要がある。めったに起こらないはずのことが起こってしまう可能性を表す数値を危険率と呼び，$\alpha$というギリシャ文字を使って表す。$\alpha=0.05 \, (5\%)$であれば，めったにないことが5%の頻度 (危険率) で起こりうる，という意味になる。

危険率$\alpha$のときの$t$分布の値を$t_\alpha$と書くことにする。たとえば，$\alpha=0.05$のときは$t_{0.05}$と書く。$t_{\overline{d}}>t_{0.05}$のとき，このような$t_{\overline{d}}$の値が出現するのは，20回に1回しか起こらないまれなことである，と判断できる。

すぐ後ほど説明するように，$n=3$のとき，$d$の値が0より大きくなる，もしくは0より小さくなるようなことが$5\%$の確率で起こるのは，$t_{\overline{d}}$が3.18を超える場合である。$t_{\overline{d}}=\frac{\overline{d}}{\frac{u}{\sqrt{n}}}=4.57$であるので，$d$が0であるという仮説は棄却される。よって，統計学的に見て，A群とB群の平均値は異なると判断できる。



## 危険率のグラフ上での意味

平均0でない$t_{\overline{d}}$の値が出現する確率は，図1に示す$t$分布の確率密度のグラフでは，グラフ上で黄緑色を付けた部分の面積として表される。面積が0.05となるような横軸の値を求めれば，$t_{0.05}$の値を知ることが出来る。以前はあらかじめ計算された数表を使うことが多かったが，最近はプログラミング言語のライブラリを用いることが多くなっている（こちらの方が正確な値を知ることが出来る）。

表1. 両側5%，片側2.5％の場合のt値

| 自由度 |  t値  |
| :----: | :---: |
|   3    | 3.182 |
|   5    | 2.571 |
|   10   | 2.228 |
|   20   | 2.086 |
|   30   | 2.042 |

<img src="image-20240406095715743.png" alt="image-20240406095715743" style="zoom:50%;" />

図1. $n=3$のときのt-分布の確率密度関数のグラフ。黄緑色の部分の面積は0.05である。確率密度関数の面積は確率を表しているので，黄緑色の部分の面積が0.05であるということは，横軸の値が0とならない確率が5％であることを意味している。



<img src="image-20240406092936800.png" alt="image-20240406092936800" style="zoom: 50%;" />

図2. $n$の値を変化させた場合のt‐分布の確率密度関数のグラフ。

図2は横軸に$t$値を，縦軸に$t$値に応する確率密度をとった$t$分布のグラフである。$t$分布は標本数$n$の値によって変化する。ただし，図2に示すように$n$が大きくなるとほとんどグラフの形は変化しない。



<img src="image-20240406093008802.png" alt="image-20240406093008802" style="zoom:50%;" />

図3. $n=30$のときのt-分布と平均0，標準偏差1の正規分布の確率密度関数のグラフ。

また，図3に示すように$n$の値が大きくなるにつれ，$t$分布のグラフは正規分布のグラフに近づいていくことが明らかにされているので，$n>30$の場合には，$t$分布の代わりに正規分布を用いることがよくある。



## 両側検定と片側検定

平均値の検定をする多くの場合，めったにないこととして，平均値が0より大きい場合と小さい場合の2つのケースが起こりえるので，平均値より大きい場合と小さい場合を両方が2.5%で起こりえる場合を，危険率5%と表現する。これを両側検定という。両側検定の場合，
$$
t_{\overline{d}} > t_{0.025} \quad \mathrm{or} \quad t_{\overline{d}} < - t_{0.025}
$$
が起こりえるかどうかを調べていることになる。

実験条件によっては，最初から平均値より大きくなるケースしか起きないことが分かっている場合がある。この場合は平均値より大きい値が5％で起こりえる場合を，危険率5%と表現する。平均値より小さくなるケースについても同様である。これを片側検定という。



## p-値

最近は，(1)式で計算された$t_{\overline{d}}$の値そのものよりも，計算された$t_{\overline{d}}$の値が出現する確率を示すことが多くなっている。この確率のことを$p$値と呼ぶ。$p$値が最初に与えた危険率$\alpha$を下回れば，データから計算された$t_{\overline{d}}$のような値が出現する確率は危険率以下であることになる。こちらの方が直観的にも分かりやすい。図1に示したように，$p$値を求めるためには，与えられた$t_{\overline{d}}$の値からグラフの面積を求める必要がある。こちらについても以前は数表を用いていたが，最近はプログラミング言語のライブラリを用いることが多くなっている。



<img src="image-20240406111408561.png" alt="image-20240406111408561" style="zoom:50%;" />

図4. t-分布の累積密度関数。図2の確率密度を$-\infty$から特定の$t$の値まで積分することでこのグラフを作成することができる。

$p$値は確率であるので，図2に示したグラフを積分すれば求めることが出来る。図4は図2に示した$t$‐分布の確率密度関数を$f(t)$と表したときの，
$$
F(t)=\int_{-\infty}^{t} f(t)dt
$$
の値を縦軸に，$t$の値を横軸にとったグラフである。このグラフの縦軸の値を読み取れば，$t$が$-\infty$から横軸の$t$の値の範囲の値を取る場合の確率が分かる。$t>2$の場合，$F(t)$の値がほぼ1となることから，ほとんどすべての場合で$t$の値は2以下となることが分かる。言い換えると$t$が2以上の値を示すことは非常にまれである。



### Python

**プログラム**: P-VALUE.py

```python
import scipy.stats as st

# 自由度3, t値が3.18のときのp値

df=3
tvalue=3.18

print("t-value="+str(tvalue))
print("df     ="+str(df))

# 両側検定
p_dsided=2.0*st.t.cdf(-tvalue,df=df)
print("p-value (double sided) = "+str(p_dsided))
# 片側検定
p_ssided=st.t.cdf(-tvalue,df=df)
print("p-value (single sided) = "+str(p_ssided))
```

**プログラム実行例**

```bash
$ python3 P-VALUE.py 
t-value=3.18
df     =3
p-value (double sided) = 0.05009402062585418
p-value (single sided) = 0.02504701031292709
```



### Fortran

**プログラム**: P-VALUE.f90

```fortran
! /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/02.12.T-TEST_PAIRED
PROGRAM P_VALUE_T_DIST

tvalue=3.18
df=3.0
print '(A,f5.2)',"t-value=",tvalue
print '(A,f5.2)',"df     =",df

! 両側検定
p_dsided=betai(0.5*df,0.5,df/(df+tvalue**2))   !Student's t probability.
print '(A,f10.3)',"p-value (double sided) = ",p_dsided

! 片側検定
p_ssided=0.5*betai(0.5*df,0.5,df/(df+tvalue**2))   !Student's t probability.
print '(A,f10.3)',"p-value (double sided) = ",p_ssided

END PROGRAM P_VALUE_T_DIST


FUNCTION betai(a,b,x)
! RETURNS THE INCOMPLETE BETA FUNCTION Ix(a; b).
REAL betai,a,b,x
! USES betacf,gammln
REAL bt,betacf,gammln
if(x.lt.0..or.x.gt.1.)then; print *,"EEEEE bad argument x in betai";stop; end if
if(x.eq.0..or.x.eq.1.)then
bt=0.
else ! Factors in front of the continued fraction.
bt=exp(gammln(a+b)-gammln(a)-gammln(b)+a*log(x)+b*log(1.-x))
endif
if(x.lt.(a+1.)/(a+b+2.))then 
!Use continued fraction directly.
betai=bt*betacf(a,b,x)/a
return
else
betai=1.-bt*betacf(b,a,1.-x)/b 
!Use continued fraction after making the symmetry transformation. 
return
endif
END


FUNCTION betacf(a,b,x)
INTEGER MAXIT
REAL betacf,a,b,x,EPS,FPMIN
PARAMETER (MAXIT=100,EPS=3.e-7,FPMIN=1.e-30)
! Used by betai: Evaluates continued fraction for incomplete beta
! function by modified
! Lorentz's method (Section 5.2).
INTEGER m,m2
REAL aa,c,d,del,h,qab,qam,qap
qab=a+b 
!These q’s will be used in factors that occur in the coefficients
!(6.4.6). 
qap=a+1.
qam=a-1.
c=1. ! First step of Lentz’s method.
d=1.-qab*x/qap
if(abs(d).lt.FPMIN)d=FPMIN
d=1./d
h=d
do m=1,MAXIT
m2=2*m
aa=m*(b-m)*x/((qam+m2)*(a+m2))
d=1.+aa*d 
! One step (the even one) of the recurrence.
if(abs(d).lt.FPMIN)d=FPMIN
c=1.+aa/c
if(abs(c).lt.FPMIN)c=FPMIN
d=1./d
h=h*d*c
aa=-(a+m)*(qab+m)*x/((a+m2)*(qap+m2))
d=1.+aa*d 
! Next step of the recurrence (the odd one).
if(abs(d).lt.FPMIN)d=FPMIN
c=1.+aa/c
if(abs(c).lt.FPMIN)c=FPMIN
d=1./d
del=d*c
h=h*del
if(abs(del-1.).lt.EPS)goto 1 !Are we done?
enddo
print *, "WWWWW a or b too big, or MAXIT too small in betacf"
1 betacf=h
return
END


FUNCTION gammln(xx)
REAL gammln,xx
! Returns the value ln[Γ(xx)] for xx > 0.
INTEGER j
DOUBLE PRECISION ser,stp,tmp,x,y,cof(6)
! Internal arithmetic will be done in double precision, a nicety 
! that you can omit if five-figure accuracy is good enough.
SAVE cof,stp
DATA cof,stp/76.18009172947146d0,-86.50532032941677d0, &
 24.01409824083091d0,-1.231739572450155d0,.1208650973866179d-2, &
-.5395239384953d-5,2.5066282746310005d0/
x=xx
y=x
tmp=x+5.5d0
tmp=(x+0.5d0)*log(tmp)-tmp
ser=1.000000000190015d0
do j=1,6
y=y+1.d0
ser=ser+cof(j)/y
enddo
gammln=tmp+log(stp*ser/x)
return
END
```

Fortranのプログラムの方がPythonのプログラムより長くなっているように見えるが，Pythonのプログラムでは，FortranのEND PROGRAM P_VALUE_T_DISTより下の部分をユーザーから見えなくしているだけである。



**プログラム実行例**

```bash
$ gfortran -o P-VALUE.EXE P-VALUE.f90
$ P-VALUE.EXE 
t-value= 3.18
df     = 3.00
p-value (double sided) =      0.050
p-value (double sided) =      0.025
```

t-分布の累積密度関数$A(t|n)$は，下記で与えられる。
$$
A(t|n)=\frac{1}{n^{\frac{1}{2}}B(\frac{1}{2}, \frac{n}{2})}
\int_{-t}^{t}\bigg(1+\frac{x^2}{n}\bigg)
^{\frac{-(n+1)}{2}}dx
$$
ここで，$B(x,y)$はベータ関数
$$
B(x,y)=\int_{0}^{1}t^{x-t}(1-t)^{y-1}dt
$$
である。

不完全ベータ関数
$$
I_x(a,b)=\frac{\Gamma(a+b)}{\Gamma(a)\Gamma(b)}
\int_{0}^{x}t^{a-1}(1-t)^{b-1}dt
$$
を用いると，$A(t|n)$は，
$$
A(t|n)=1-I_{\frac{n}{n+t^2}}\bigg(\frac{n}{2},\frac{1}{2}\bigg)
$$
と表すこともできる。このプログラムでは，
$$
1-A(t|n)=I_{\frac{n}{n+t^2}}\bigg(\frac{n}{2},\frac{1}{2}\bigg)
$$
を計算することで，自由度$n$と$t$値が与えられたときの$p$値を得ている。

# /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/02.12.T-TEST_PAIRED

import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt

x = np.linspace(-6, 6, 100)

# サブプロット（箱）を用意
fig, ax = plt.subplots(1, 1)
# y=0の関数を作成（後で範囲の塗り潰しに使う）
y = 0
# 自由度3のt分布の両側９５％点
t = st.t.ppf(1-(1-0.95)/2, 3)

# 自由度3のt分布をプロット
ax.plot(x, st.t.pdf(x, 3), label='t-dist $n=3$')
# 範囲の塗り潰し
ax.fill_between(x, st.t.pdf(x, 3), y, where=x < -t, facecolor='lime', alpha=0.5)
ax.fill_between(x, st.t.pdf(x, 3), y, where=x > t, facecolor='lime', alpha=0.5)

plt.xlim(-6, 6)
plt.ylim(0, 0.4)

plt.legend()

FIG="04.PLT_P-VALUE_T-DIST.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)

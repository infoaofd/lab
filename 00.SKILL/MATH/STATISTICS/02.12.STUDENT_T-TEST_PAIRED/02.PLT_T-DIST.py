# /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/02.12.T-TEST_PAIRED

import numpy as np
import scipy.stats as st
import matplotlib.pyplot as plt

dfs = [3,10,30,100]
clrs = ["red","blue","green","purple"]
x = np.arange(-6,6,0.1)

plt.figure(figsize=(10,5))
for i,df in enumerate(dfs):
  y = st.t.pdf(x,df)
  plt.plot(x,y,color=clrs[i],label = 'n='+str(df))
  plt.xlabel(r"$t") #_\alpha")
  plt.ylabel('PDF')
  plt.legend()

plt.legend(loc=2)
FIG="02.PLT_T-DIST.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)

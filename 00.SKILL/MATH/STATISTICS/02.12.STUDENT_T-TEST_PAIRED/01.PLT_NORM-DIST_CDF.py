# /work09/am/16.TOOL/22.ANALYSIS/12.STATISTICS/12.TEST.STATISTICS/22.12.DIFF.STATISTICS/02.12.T-TEST_PAIRED

import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

alpha = np.array([0.1, 0.05, 0.025, 0.01, 0.005]) 
x = np.arange(-3.,3.01,0.01)
F_cdf = stats.norm.cdf(x)

plt.plot(x,F_cdf,label="cdf")
for i in range(alpha.shape[0]):
    print("alpha: {}, statistic_upper: {:.2f}".format(alpha[i], stats.norm.ppf(1.-alpha[i]))) 
    x_upper = np.repeat(stats.norm.ppf(1.-alpha[i]), 100)
    y = np.linspace(0,1.-alpha[i],100)
    plt.plot(x_upper,y,label="alpha: {}".format(alpha[i]))

plt.legend(loc=2)
FIG="01.PLT_NORM-DIST_CDF.PDF"
plt.savefig(FIG)
print("FIG: "+FIG)

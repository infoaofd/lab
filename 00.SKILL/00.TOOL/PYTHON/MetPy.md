# MetPy

## Install

```bash
$ conda deactivate
```

```bash
$ conda create -n met
```

```bash
$ conda activate met
```

```bash
$ conda install -c conda-forge metpy
```

```bash
$ conda install -c conda-forge pygrib 
```


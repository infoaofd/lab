# Python Tips

[[_TOC_]]

## Tutorial

https://www.atmarkit.co.jp/ait/subtop/features/di/pybasic_index.html

https://github.com/tenomoto/nwu-geoex2/blob/master/README.md

https://gitlab.com/infoaofd/lab/-/tree/master/PYTHON



## Tips

### 日本語を使う

プログラムの最初の行に下記のどちらかを入れる。

```
# -*- coding: shift-jis -*-
```

```
# -*- coding: utf-8 -*-
```



### 座標軸の設定

https://www.yutaka-note.com/entry/matplotlib_axis

### cartopyとmatplotlibを使って緯度経度座標のデータの可視化

https://qiita.com/convection/items/6d5fa546d81fd5c9b452

### matplotlibでジャーナルに投稿可能な図を作る

https://qiita.com/HajimeKawahara/items/03f29367744d5209be42

### matplotlibでカラーバーの高さを図と合わせる

http://hydro.iis.u-tokyo.ac.jp/~ikeuchi/it-memo/python/python-colorbar.html

### カラーバーの範囲指定

`Normalize`をimportして，`norm=Normalize(vmin=, vmax=)`で範囲を指定する。

```python
from matplotlib.colors import Normalize # Normalizeをimport
CS = mp.pcolor(x,y,np.transpose(drm),cmap=cmap, norm=Normalize(vmin=-1, vmax=1))
```



### スクリプト名を出力ファイル名につかう

```python
# スクリプト名を出力ファイル名につかう
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG); print("")
```



## Recipe

[Histogram](RECIPE/HISTOGRAM.md)

[NETCDF](RECIPE/NETCDF.md)

https://qiita.com/OSAKO/items/d25b8484d35ef4fe19e0






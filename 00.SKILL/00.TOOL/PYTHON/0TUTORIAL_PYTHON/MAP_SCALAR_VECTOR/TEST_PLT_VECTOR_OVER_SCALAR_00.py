# -*- coding: shift-jis -*-
"""
u, v, phiという2次元配列が与えられたとき，phiを色で，(u,v)を矢印でプロットするスクリプトを作って。
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap, ListedColormap

# 仮のデータ（実際のデータに置き換えてください）
# 例えば、u, v, phiのサイズは10x10で、適当な値を設定
u = np.random.rand(10, 10)  # u成分
v = np.random.rand(10, 10)  # v成分
phi = np.random.rand(10, 10) - 0.5  # phi

# グリッド作成
X, Y = np.meshgrid(np.arange(u.shape[1]), np.arange(u.shape[0]), indexing='ij')
# (Nx, Ny)型の配列をプロットする場合はindexing="ij"をつける

# プロットの作成
fig, ax = plt.subplots()

# カラーマップ作成
# 反転させたRdBuカラーマップを作成
#cm = plt.cm.RdBu_r  # ここで _r を追加すると反転されます

# 青->白->オレンジ
top = mpl.colormaps['Blues_r'].resampled(128)
bottom = mpl.colormaps['Oranges'].resampled(128)
newcolors = np.vstack((top(np.linspace(0, 1, 128)),
                       bottom(np.linspace(0, 1, 128))))
cm = ListedColormap(newcolors, name='BlueOrange')

# phiを色でプロット
contour = ax.contourf(X, Y, phi, cmap=cm)

# (u, v)のベクトルを矢印でプロット（白抜きの黒い矢印）
ax.quiver(X, Y, u, v, scale=10, edgecolors='white', facecolors='black', linewidths=0.5)

# カラーバーの追加
fig.colorbar(contour)

# 軸ラベル
ax.set_xlabel('X')
ax.set_ylabel('Y')

# グラフの表示
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = filename_no_extension + ".PDF"
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

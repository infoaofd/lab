import numpy as np

# np.arangeの出力を確認
start_range = np.arange(24.0, 33.0, 1.0)
print(f"np.arange(24.0, 33.0, 1.0) output: {start_range}")

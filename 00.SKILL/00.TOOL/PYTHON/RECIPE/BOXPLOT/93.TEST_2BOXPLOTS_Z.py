import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

# 入力ディレクトリとファイルパス
INDIR1 = "RW3A.00.04.05.05.0000.01.d01_T00.01/"
INDIR2 = "RW3A.00.04.05.05.0802.01.d01_T00.01/"
IN1 = INDIR1 + "RW3A.00.04.05.05.0000.01.d01_T00.01_24.0-25.0.txt"
IN2 = INDIR2 + "RW3A.00.04.05.05.0802.01.d01_T00.01_24.0-25.0.txt"

# ファイルを読み込む
df1 = pd.read_csv(IN1, delim_whitespace=True, header=None)
df2 = pd.read_csv(IN2, delim_whitespace=True, header=None)

# 8列目のデータを取得
column1_8 = df1[7].dropna()  # NaNを削除
column2_8 = df2[7].dropna()  # NaNを削除

# numpyのdtype=objectでデータをラップ
data = [np.array(column1_8, dtype=object), np.array(column2_8, dtype=object)]

# ボックスプロット作成
plt.figure(figsize=(8, 6))

# 横軸の位置を調整して配置 (vert=Falseで横向きに変更)
positions = [1, 2]  # ボックスプロットの位置
boxplot = plt.boxplot(
    data,
    positions=positions,
    patch_artist=True,
    boxprops=dict(facecolor="skyblue", color="black"),
    vert=False,  # 横向きのボックスプロット
    showmeans=False,  # 中央値用に独自プロットをするので無効化
)

# 中央値をプロット（赤い線を引く）
for i, dataset in enumerate(data, start=1):
    median_value = np.median(dataset)
    plt.scatter(
        median_value,
        i,  # ボックスプロットの位置
        color="red",
        s=50,  # サイズを調整
        zorder=3,  # 前面に描画
        label="Median" if i == 1 else ""  # 最初のデータセットのみ凡例
    )

# グラフ設定
plt.title("Boxplot of Column 8", fontsize=14)
plt.xlabel("Values in Column 8", fontsize=12)
plt.yticks(positions, ["Dataset 1", "Dataset 2"])  # Y軸ラベルを設定

# 凡例を追加（Median用）
plt.legend(loc="upper right")

# 出力ファイル名をスクリプト名から生成
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + ".PDF"

# プロットを保存
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

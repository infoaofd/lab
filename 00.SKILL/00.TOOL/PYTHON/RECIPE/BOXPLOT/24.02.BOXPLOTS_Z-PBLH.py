import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

# 基本ディレクトリ
BASE_DIRS = [
    "RW3A.00.04.05.05.0000.01.d01_T00.01/",
    "RW3A.00.04.05.05.0802.01.d01_T00.01/"
]

# ファイル名の範囲（24.0-25.0 から 32.0-33.0）
START_RANGES = [f"{start:.1f}-{start+1.0:.1f}" for start in np.arange(24.0, 33.0, 1.0)]

# ボックスプロットの色を指定（0000: 緑、0802: マゼンタ）
BOX_COLORS = {
    "0000": "green",
    "0802": "magenta",
}

# ファイルパスを生成
INPUT_FILES = []
COLORS = []  # 各ファイルに対応する色リスト
for range_label in START_RANGES:
    for base_dir in BASE_DIRS:
        code = base_dir.split(".")[5]
        INPUT_FILES.append(base_dir + f"RW3A.00.04.05.05.{code}.01.d01_T00.01_{range_label}.txt")
        COLORS.append(BOX_COLORS[code])

# データセットを格納するリスト
data = []

# 入力ファイルを読み込む
for file in INPUT_FILES:
    try:
        df = pd.read_csv(file, delim_whitespace=True, header=None)
        # 8列目と17列目の差を計算し、キロメートルに変換
        column_diff = (df[7] - df[16]).dropna() / 1000
        data.append(np.array(column_diff, dtype=object))
    except FileNotFoundError:
        print(f"File not found: {file}")

# ボックスプロット作成
plt.figure(figsize=(2.5, 12))

group_spacing = 1.0
pair_spacing = 0.2
positions = []
for i in range(len(START_RANGES)):
    base_pos = len(START_RANGES) - i
    positions.append(base_pos - pair_spacing / 2)  # 0802の位置
    positions.append(base_pos + pair_spacing / 2)  # 0000の位置

# 各ボックスプロットを個別に描画
for pos, dataset, color in zip(positions, data, COLORS):
    boxplot = plt.boxplot(
        [dataset],
        positions=[pos],
        widths=0.1,
        patch_artist=True,
        boxprops=dict(facecolor=color, color="black"),
        vert=False,
        showmeans=False,
    )

    # 外れ値の色を設定（黒色の線ではなく、指定された色）
    for flier in boxplot['fliers']:
        if color == "green":  # 0000データセットの場合は緑色
            flier.set(marker='o', color='green', alpha=0.5, linewidth=2)  # 外れ値を緑色に
        elif color == "magenta":  # 0802データセットの場合はマゼンタ色
            flier.set(marker='o', color='magenta', alpha=0.5, linewidth=2)  # 外れ値をマゼンタ色に

# 中央値と平均値をプロット
for pos, dataset, color, code in zip(positions, data, COLORS, ["0000", "0802"] * len(START_RANGES)):
    # 中央値
    median_value = np.median(dataset)
    if code == "0000":  # 0000の場合は緑色
        plt.scatter(
            median_value,
            pos,  # ボックスプロットの位置
            facecolor='green',  # 塗りつぶしを緑色に
            edgecolor='white',  # 白枠
            s=100,  # サイズを調整
            zorder=3,  # 前面に描画
        )
    else:  # 0802の場合はマゼンタ色
        plt.scatter(
            median_value,
            pos,  # ボックスプロットの位置
            facecolor='magenta',  # 塗りつぶしをマゼンタ色に
            edgecolor='white',  # 白枠
            s=100,  # サイズを調整
            zorder=3,  # 前面に描画
        )

    # 平均値
    mean_value = np.mean(dataset)
    if code == "0000":  # 0000の場合は緑色
        plt.scatter(
            mean_value,
            pos,  # ボックスプロットの位置
            facecolor='green',  # 塗りつぶしを緑色に
            edgecolor='white',  # 白枠
            marker="D",  # 菱形マーカー
            s=80,  # サイズを調整
            zorder=3,  # 前面に描画
        )
    else:  # 0802の場合はマゼンタ色
        plt.scatter(
            mean_value,
            pos,  # ボックスプロットの位置
            facecolor='magenta',  # 塗りつぶしをマゼンタ色に
            edgecolor='white',  # 白枠
            marker="D",  # 菱形マーカー
            s=80,  # サイズを調整
            zorder=3,  # 前面に描画
        )

# Y軸のラベルを設定
yticks_labels = []
ytick_positions = []
for i, range_label in enumerate(START_RANGES[::-1]):
    latitude_range = range_label.split('-')
    latitude_label = f"{int(float(latitude_range[0]))}-{int(float(latitude_range[1]))}N"
    yticks_labels.append(latitude_label)
    ytick_positions.append(len(START_RANGES) - i)

plt.yticks(ytick_positions, yticks_labels, fontsize=12)
plt.xticks(fontsize=12)

# 横軸ラベルを設定
plt.xlabel("Z-PBLH [km]", fontsize=12)

# タイトルを設定
plt.title("Z-PBLH", fontsize=14)

# スクリプト名から出力ファイル名を生成
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + ".PDF"

# プロットを保存
plt.savefig(FIG, bbox_inches="tight")
plt.close()
print("FIG: " + FIG)

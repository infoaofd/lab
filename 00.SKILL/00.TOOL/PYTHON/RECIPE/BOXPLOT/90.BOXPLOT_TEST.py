import matplotlib.pyplot as plt
import numpy as np

"""
# step1 発生する乱数の固定化
np.random.seed(19680801)
# step2 データの作成
all_data = [np.random.normal(0, std, 100) for std in range(7, 10)]
labels = ['x1', 'x2', 'x3']
"""

# step3 グラフフレームの作成
fig, ax = plt.subplots()

# step4 箱ひげ図の描画
# 水平箱ひげ図
ax.boxplot(all_data, sym='rs', vert=False, labels=labels)
ax.set_title('basic plot')

ax.set_xlabel('X label')
ax.set_ylabel('Y label')


import matplotlib.pyplot as plt

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)
print("")


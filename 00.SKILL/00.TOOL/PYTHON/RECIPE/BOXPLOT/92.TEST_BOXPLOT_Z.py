import pandas as pd
import matplotlib.pyplot as plt

INDIR="RW3A.00.04.05.05.0000.01.d01_T00.01/"
# ファイルパスを指定
file_path = INDIR+"RW3A.00.04.05.05.0000.01.d01_T00.01_24.0-25.0.txt"

# ファイルを読み込む（空白を区切り文字として認識）
df = pd.read_csv(file_path, delim_whitespace=True, header=None)

# 各列を個別のリストに変換
columns = [df[col].tolist() for col in df.columns]

# 結果を表示（最初の5行だけ表示）
for i, col in enumerate(columns):
    print(f"列 {i + 1}: {col[:5]} ...")

# 8列目のデータを取得
column_8 = df[7]  # 0-indexedなので7を指定

# ボックスプロットを作成
plt.figure(figsize=(8, 6))
plt.boxplot(column_8, vert=True, patch_artist=True, boxprops=dict(facecolor="skyblue", color="black"))
plt.title("Boxplot of Column 8", fontsize=14)
plt.ylabel("Values in Column 8", fontsize=12)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)
print("")

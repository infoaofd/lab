! PROGRAM 
! Thu, 21 Nov 2024 17:27:38 +0900
! tiamat.bosai.go.jp
! /work04/manda/2022.RW3A/14.22.RW3A.MS01_TRAJ_W/42.34.12.TIAMAT_RW3A.MS01_TRAJ_W/14.BOXPLOT

!IMPLICIT NONE
!
CHARACTER RUN*200,INDIR*500,INFLE*500,IN*1000,ODIR*500,OFLE*500,&
OUT*1000
CHARACTER IP*3, STRM*1000
INTEGER NP

INTEGER,PARAMETER::MP=500, MT=1000
INTEGER NT(MP)

INTEGER,DIMENSION(MP,MT)::iyr,mon,idy,ihr,imin
REAL,DIMENSION(MP,MT)::lonpos, latpos, zpos, &
&  EPTpos, LHpos, QSATpos, Q2pos,&
&  HFXpos, TSKpos, T2pos, U10MAGpos, PBLHpos, L_MOLpos, &
&  MU_MOLpos, wpos
REAL LATS,DLAT
INTEGER NLAT

REAL,ALLOCATABLE,DIMENSION(:)::BLATS,BLATN
CHARACTER(LEN=4)::CLATS,CLATN

NAMELIST /PARA/RUN,NP,LATS,DLAT,NLAT,ODIR
READ(*,NML=PARA)

ALLOCATE(BLATS(NLAT),BLATN(NLAT))
DO J=1,NLAT
BLATS(J)=LATS+DLAT*FLOAT(J-1)
BLATN(J)=LATS+DLAT*FLOAT(J)
PRINT '(A,I3,1x,2f7.1)','J, BLATS(J),BLATN(J) = ',J,BLATS(J),BLATN(J)
END DO !J

INDIR="../"//TRIM(RUN)//"/"

DO I=1,NP
WRITE(IP,'(I3.3)')I
INFLE=TRIM(RUN)//"_"//IP//".txt"
IN=TRIM(INDIR)//TRIM(INFLE)
!PRINT '(A,A)','MMMMM IN: ',TRIM(IN)
OPEN(11,FILE=TRIM(IN),ACTION="READ")

n=0
count_vaild_data: do
  read(11,'(A)',iostat=ios)strm
  if(ios<0)exit
  if(strm(1:1) == "#")then
    cycle
  else
    n=n+1
  endif
enddo count_vaild_data
NT(I)=n

rewind(11)

n=0
skip_comment: do
  read(11,'(A)',iostat=ios)strm
  if(ios<0)exit
  if(strm(1:1) == "#")then
    cycle
  else
    n=n+1
    read(strm,*)&
iyr(I,N),mon(I,N),idy(I,N),ihr(I,N),imin(I,N), &
&  lonpos(I,N), latpos(I,N), zpos(I,N), &
&  EPTpos(I,N), LHpos(I,N), QSATpos(I,N), Q2pos(I,N),&
&  HFXpos(I,N), TSKpos(I,N), T2pos(I,N), U10MAGpos(I,N), PBLHpos(I,N), L_MOLpos(I,N), &
&  MU_MOLpos(I,N), wpos(I,N)
  endif
!PRINT '(I4,1x,I2,1x,I2,1x,I2,1x,I2,f8.3,1x,f7.2,1x,f8.0,1x,f7.1,1x,f5.0,1x,f5.1,1x,f5.1,1x,f5.1)',&
!iyr(I,N),mon(I,N),idy(I,N),ihr(I,N),imin(I,N), &
!&  lonpos(I,N), latpos(I,N), zpos(I,N), &
!&  EPTpos(I,N),LHpos(I,N), QSATpos(I,N), Q2pos(I,N)
enddo skip_comment

!PRINT '(A,2I5)','MMMMM I,NT(I)=',I,NT(I);PRINT *

CLOSE(11)
END DO !I

IU0=21
DO J=1,NLAT
WRITE(CLATS,'(F4.1)')BLATS(J)
WRITE(CLATN,'(F4.1)')BLATN(J)
OFLE=TRIM(RUN)//'_'//CLATS//'-'//CLATN//'.txt'
OUT=TRIM(ODIR)//'/'//TRIM(OFLE)
IU=IU0+J; OPEN(IU,FILE=OUT)
PRINT '(A,I3,A,A)','MMMMM OUT ',IU,' ',TRIM(OUT); PRINT *
END DO !J

DO I=1,NP
DO N=1,NT(I)
DO J=1,NLAT
IU=IU0+J
IF(latpos(I,N)>=BLATS(J).and.latpos(I,N)<BLATN(J))THEN
   write(IU,88) iyr(I,N),mon(I,N),idy(I,N),ihr(I,N),imin(I,N), &
&  lonpos(I,N), latpos(I,N), zpos(I,N), &
&  EPTpos(I,N), LHpos(I,N), QSATpos(I,N), Q2pos(I,N),&
&  HFXpos(I,N), TSKpos(I,N), T2pos(I,N), U10MAGpos(I,N), PBLHpos(I,N), L_MOLpos(I,N), &
&  MU_MOLpos(I,N), wpos(I,N)

88 format(i4,1x,i2,1x,i2,1x,i2,1x,i2,   f10.4,1x,f9.4,1x,f8.2,1x,&
& f8.3,1x,f7.1,1x,2f7.2, 1x,  f8.2,1x,2f8.2, 1x, f7.1, 1x, f8.1, 1X, f8.1, 1X, &
E11.4,1x,e11.4) !MOL
END IF !latpos
END DO !J
END DO !N
END DO !I

DO J=1,NLAT;IU=IU0+J; CLOSE(IU); END DO !J


END !PROGRAM

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os

# 基本ディレクトリ
BASE_DIRS = [
    "RW3A.00.04.05.05.0000.01.d01_T00.01/",
    "RW3A.00.04.05.05.0802.01.d01_T00.01/"
]

# ファイル名の範囲（24.0-25.0 から 32.0-33.0）
START_RANGES = [f"{start:.1f}-{start+1.0:.1f}" for start in np.arange(24.0, 33.0, 1.0)]

# ボックスプロットの色を指定（0000: 緑、0802: マゼンタ）
BOX_COLORS = {
    "0000": "green",  # 0000の場合は緑
    "0802": "magenta",  # 0802の場合はマゼンタ
}

# ファイルパスを生成
INPUT_FILES = []
COLORS = []  # 各ファイルに対応する色リスト
for range_label in START_RANGES:
    for base_dir in BASE_DIRS:
        # コードをディレクトリ名から取得（'0000' または '0802'）
        code = base_dir.split(".")[5]  # '0000' または '0802' を正確に取得
        # ファイル名を正確に生成
        INPUT_FILES.append(base_dir + f"RW3A.00.04.05.05.{code}.01.d01_T00.01_{range_label}.txt")
        COLORS.append(BOX_COLORS[code])  # 各ファイルの色を設定

# データセットを格納するリスト
data = []

# 入力ファイルを読み込む
for file in INPUT_FILES:
    try:
        df = pd.read_csv(file, delim_whitespace=True, header=None)
        column_8 = df[7].dropna()  # 8列目を取得し、NaNを削除
        data.append(np.array(column_8, dtype=object))  # 配列として追加
    except FileNotFoundError:
        print(f"File not found: {file}")

# ボックスプロット作成
plt.figure(figsize=(10, 12))

# 横軸の位置を調整して配置 (vert=Falseで横向きに変更)
group_spacing = 1.0  # 範囲間の間隔
pair_spacing = 0.2  # ペア内の間隔を狭く設定
positions = []
for i in range(len(START_RANGES)):
    base_pos = len(START_RANGES) - i  # ラベルの逆順で配置
    positions.append(base_pos - pair_spacing / 2)  # 0802の位置
    positions.append(base_pos + pair_spacing / 2)  # 0000の位置

# 各ボックスプロットを個別に描画
for pos, dataset, color in zip(positions, data, COLORS):
    boxplot = plt.boxplot(
        [dataset],
        positions=[pos],
        widths=0.1,  # ボックスの幅をさらに狭く設定
        patch_artist=True,
        boxprops=dict(facecolor=color, color="black"),
        vert=False,  # 横向きのボックスプロット
        showmeans=False,  # 中央値用に独自プロットをするので無効化
    )

    # 外れ値の色を設定（黒色の線ではなく、指定された色）
    for flier in boxplot['fliers']:
        flier.set(marker='o', color=color, alpha=0.5, linewidth=2)  # 外れ値を指定色で描画

# 中央値と平均値をプロット
for pos, dataset, color in zip(positions, data, COLORS):
    # 中央値
    median_value = np.median(dataset)
    plt.scatter(
        median_value,
        pos,  # ボックスプロットの位置
        facecolor='white',  # 白抜き
        edgecolor='black',  # 黒枠
        s=100,  # サイズを調整
        zorder=3,  # 前面に描画
        label="Median" if pos == positions[0] else ""  # 最初のデータセットのみ凡例
    )

    # 平均値
    mean_value = np.mean(dataset)
    plt.scatter(
        mean_value,
        pos,  # ボックスプロットの位置
        facecolor='white',  # 白抜き
        edgecolor='black',  # 黒枠
        marker="D",  # 菱形マーカー
        s=80,  # サイズを調整
        zorder=3,  # 前面に描画
        label="Mean" if pos == positions[0] else ""  # 最初のデータセットのみ凡例
    )

# Y軸のラベルを設定
yticks_labels = []
ytick_positions = []
for i, range_label in enumerate(START_RANGES[::-1]):
    # ラベルを「24.0-25.0」から「24-25N」の形式に変更
    latitude_range = range_label.split('-')
    print(f"latitude_range: {latitude_range}")  # ここで確認
    latitude_label = f"{int(float(latitude_range[0]))}-{int(float(latitude_range[1]))}N"
    print(f"latitude_label: {latitude_label}")  # ここで確認
    yticks_labels.append(latitude_label)  # 新しいラベル形式を追加
    ytick_positions.append(len(START_RANGES) - i)  # グループの中心位置

plt.yticks(ytick_positions, yticks_labels, fontsize=12)  # Y軸のフォントサイズを調整
plt.xticks(fontsize=12)  # X軸のフォントサイズを調整

# グラフ設定
plt.title("Boxplot of Column 8 with Median and Mean", fontsize=14)
plt.xlabel("Values in Column 8", fontsize=12)

# 凡例を追加（MedianとMean用）
legend_handles = [
    plt.Line2D([0], [0], marker='o', color='w', markerfacecolor='white', markeredgecolor='black', markersize=8, label='Median'),
    plt.Line2D([0], [0], marker='D', color='w', markerfacecolor='white', markeredgecolor='black', markersize=8, label='Mean')
]
plt.legend(handles=legend_handles, loc="upper right")

# 出力ファイル名をスクリプト名から生成
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
FIG = filename_no_extension + ".PDF"

# プロットを保存
plt.savefig(FIG, bbox_inches="tight")
print("FIG: " + FIG)
print("")

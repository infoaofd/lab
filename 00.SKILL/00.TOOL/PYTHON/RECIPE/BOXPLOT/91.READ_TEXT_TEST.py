import pandas as pd

INDIR="RW3A.00.04.05.05.0000.01.d01_T00.01/"
# ファイルパスを指定
file_path = INDIR+"RW3A.00.04.05.05.0000.01.d01_T00.01_24.0-25.0.txt"

# ファイルを読み込む（空白を区切り文字として認識）
df = pd.read_csv(file_path, delim_whitespace=True, header=None)

# 各列を個別のリストに変換
columns = [df[col].tolist() for col in df.columns]

# 結果を表示（最初の5行だけ表示）
for i, col in enumerate(columns):
    print(f"列 {i + 1}: {col[:5]} ...")


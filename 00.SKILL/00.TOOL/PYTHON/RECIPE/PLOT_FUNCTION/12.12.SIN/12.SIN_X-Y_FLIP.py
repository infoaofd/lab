# /work09/ma/00.SKILL/00.TOOL/PYTHON/RECIPE/PLOT_FUNCTION/12.12.SIN/12.12.FLIP_AXES
import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0, 2 * np.pi, 0.1)
y = np.sin(x)

plt.plot(y, x)

import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_=filename_no_extension

FIG=filename_no_extension+".PDF"
plt.savefig(FIG)
print("FIG: "+FIG)
print("")




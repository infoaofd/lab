# -*- coding: shift-jis -*-

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime
import os
import matplotlib.font_manager as fm

# 日本語フォントを自動検出
def get_japanese_font():
    fonts = [f.name for f in fm.fontManager.ttflist]
    for jp_font in ["MS Gothic", "Hiragino Sans", "Meiryo", "Noto Sans CJK JP", "TakaoPGothic"]:
        if jp_font in fonts:
            return jp_font
    return "sans-serif"  # 見つからない場合はデフォルトの sans-serif を使用

plt.rcParams["font.family"] = get_japanese_font()

# ファイル名
file_name = 'BAIU_TSUYU_AKE.TXT'

# データを読み込む
df = pd.read_csv(file_name, sep="\s+", header=None, skiprows=3, usecols=[0, 1, 2, 3, 4], 
                 names=['Year', 'Start_Month', 'Start_Day', 'End_Month', 'End_Day'])

# 無効な日付（例: "99"）をNaNに置き換える
df['End_Month'] = df['End_Month'].replace(99, pd.NA)
df['End_Day'] = df['End_Day'].replace(99, pd.NA)

# 梅雨入り・梅雨明けの日付を「仮の年（2000年）」に統一
df['Start_Date'] = pd.to_datetime('2000-' + df['Start_Month'].astype(str) + '-' + df['Start_Day'].astype(str), errors='coerce')
df['End_Date'] = pd.to_datetime('2000-' + df['End_Month'].astype(str) + '-' + df['End_Day'].astype(str), errors='coerce')

# 無効な日付を削除
df = df.dropna(subset=['Start_Date', 'End_Date'])

# プロットの設定
plt.figure(figsize=(12, 8))

# 各年ごとに横線を引く（細めの線に調整）
for _, row in df.iterrows():
    plt.plot([row['Start_Date'], row['End_Date']], [row['Year'], row['Year']], 
             marker='|', markersize=8, lw=2, color='skyblue')

# グラフのタイトルと軸ラベル
plt.title('梅雨入り・梅雨明け期間（九州北部）', fontsize=14)
plt.xlabel('日付', fontsize=12)
plt.ylabel('年', fontsize=12)

# X軸の範囲を5月10日?8月15日に固定
start_xlim = datetime.datetime(2000, 5, 10)
end_xlim = datetime.datetime(2000, 8, 15)
plt.xlim([start_xlim, end_xlim])

# X軸のフォーマット設定（「月-日」表示）
plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%m-%d'))
plt.xticks(rotation=45, fontsize=10)

# Y軸のラベルを間引く（5年ごとに表示）
years = df['Year'].unique()
plt.yticks(years[::5], fontsize=10)

# グリッドを追加（視認性向上）
plt.grid(axis='x', linestyle='--', alpha=0.7)

# レイアウト調整
plt.tight_layout()

# ファイル名をスクリプト名から作成し、PDFに保存
filename_no_extension = os.path.splitext(os.path.basename(__file__))[0]
fig_filename = filename_no_extension + ".PDF"
plt.savefig(fig_filename)
print("FIG: " + fig_filename)

# プロットを表示
plt.show()

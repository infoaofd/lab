import pandas as pd
import matplotlib.pyplot as plt

# Specify the file name
file_name = 'BAIU_TSUYU_AKE.TXT'

# Load the data, selecting the necessary columns (Year, Start, Month, Day)
df = pd.read_csv(file_name, sep="\s+", header=None, comment='#', usecols=[0, 3, 4], names=['Year', 'Month', 'Day'])

# Set 99 as missing value (NaN)
df['Month'] = df['Month'].replace(99, pd.NA)
df['Day'] = df['Day'].replace(99, pd.NA)

# Function to calculate the number of days from July 1st
def calculate_days_from_july_1(month, day):
    if pd.isna(month) or pd.isna(day):  # If month or day is NaN, return NaN
        return pd.NA

    # Number of days in each month
    days_in_month = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]  # Days in each month
    # Calculate the number of days from July 1st
    if month == 6:
        return day  # For June, the number of days is directly the day of the month
    elif month == 7:
        return day - 1  # For July, we subtract 1 to make July 1st as day 0
    else:
        # For other months, calculate the days after June
        return sum(days_in_month[6:month]) + day - 1

# Calculate the number of days from July 1st using 'Month' and 'Day'
df['Days_from_July_1'] = df.apply(lambda row: calculate_days_from_july_1(row['Month'], row['Day']), axis=1)

# Drop rows with missing values (NaN) in 'Days_from_July_1'
df.dropna(subset=['Days_from_July_1'], inplace=True)

# Plot the line graph
plt.figure(figsize=(10, 6))
plt.plot(df['Year'], df['Days_from_July_1'], marker='o', linestyle='-', color='b')

# Add a wavy line
plt.plot(df['Year'], [ 9] * len(df), '--', lw=1)  # red dashed line at y=9
plt.plot(df['Year'], [30] * len(df), '--', lw=1)  # red dashed line at y=30

# Set title and labels
plt.title('Number of Days from July 1st by Year')
plt.xlabel('Year')
plt.ylabel('Days from July 1st')

# Display the graph
plt.xticks(rotation=45)

# Save the graph as a PDF file
plt.savefig("TSUYU_AKE.PDF")

import numpy as np
import matplotlib.pyplot as plt
import os
from netCDF4 import Dataset  # netCDF4 ライブラリをインポート

# Helper functions for Union-Find
def idx(i, j, nx):
    return i * nx + j

def find(parent, x):
    if parent[x] != x:
        parent[x] = find(parent, parent[x])
    return parent[x]

def union(parent, rank, x, y):
    root_x = find(parent, x)
    root_y = find(parent, y)
    if root_x != root_y:
        if rank[root_x] > rank[root_y]:
            parent[root_y] = root_x
        elif rank[root_x] < rank[root_y]:
            parent[root_x] = root_y
        else:
            parent[root_y] = root_x
            rank[root_x] += 1

def find_clusters(pr, threshold, size_threshold):
    """Union-Findを用いてクラスターを見つける"""
    nx, ny = pr.shape
    parent = np.arange(nx * ny)  # Initialize each cell as its own parent
    rank = np.zeros(nx * ny, dtype=int)

    # Union adjacent cells if they meet the threshold condition
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                if i > 0 and pr[i - 1, j] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i - 1, j, nx))
                if j > 0 and pr[i, j - 1] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i, j - 1, nx))

    # Count the size of each cluster
    cluster_size = np.zeros(nx * ny, dtype=int)
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                cluster_size[root] += 1

    # Filter clusters by size (using size_threshold)
    large_clusters = {}
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                if cluster_size[root] >= size_threshold:
                    if root not in large_clusters:
                        large_clusters[root] = []
                    large_clusters[root].append((i, j))

    return large_clusters

def plot_data(pr, clusters, threshold, filename):
    """データとクラスタリング結果を可視化し、PDFに保存する"""
    fig, ax = plt.subplots(1, 2, figsize=(12, 6))

    # ヒートマップ（入力データ）
    ax[0].imshow(pr, cmap='viridis', origin='upper')
    ax[0].set_title('Input Data')
    ax[0].set_xlabel('j (Column Index)')
    ax[0].set_ylabel('i (Row Index)')
    ax[0].invert_yaxis()

    # クラスタリング結果
    cluster_map = np.zeros_like(pr, dtype=int)
    for cluster_id, cells in clusters.items():
        for (i, j) in cells:
            cluster_map[i, j] = cluster_id

    im = ax[1].imshow(cluster_map, cmap='tab20', origin='upper')
    ax[1].set_title('Clusters (Threshold > {})'.format(threshold))
    ax[1].set_xlabel('j (Column Index)')
    ax[1].invert_yaxis()
    fig.colorbar(im, ax=ax[1], orientation='vertical', label='Cluster ID')

    plt.tight_layout()

    # PDFに保存
    plt.savefig(filename, format='pdf')  
    plt.close(fig)  # 図を閉じてメモリを解放

    # 保存したファイル名を表示
    print(f"Saved the cluster results to {filename}")

# Main testing
if __name__ == "__main__":
    # netCDFファイルを読み込む（適切なパスに置き換えてください）
    netcdf_file = 'input_data.nc'  # 読み込むnetCDFファイルの名前

    # netCDFファイルの読み込み
    nc_data = Dataset(netcdf_file, 'r')

    # 例として、変数名 'data' を使っています。実際のファイルの変数名を確認してください。
    pr = np.array(nc_data.variables['data'][:])  # 2Dデータを取得（適切な変数名に置き換えてください）

    # nx, ny をデータのサイズに基づいて動的に設定
    nx, ny = pr.shape

    # クラスターサイズの閾値（500に変更可能）
    size_threshold = 500  # 変更可能なクラスターサイズの閾値
    threshold = 80

    # スクリプト名を動的に取得してファイル名に使用
    script_name = os.path.splitext(os.path.basename(__file__))[0]

    # クラスターを確認
    print("Testing with data from netCDF file:")
    clusters = find_clusters(pr, threshold=threshold, size_threshold=size_threshold)
    print(f"Number of clusters with size >= {size_threshold}: {len(clusters)}")
    plot_data(pr, clusters, threshold=threshold, filename=f"{script_name}_netCDF_clusters.pdf")

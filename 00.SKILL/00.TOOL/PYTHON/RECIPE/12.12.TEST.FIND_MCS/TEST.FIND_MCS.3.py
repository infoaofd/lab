# /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/34.12.FIND_MCS/12.12.TEST.FIND_MCS

import numpy as np
import matplotlib.pyplot as plt
import os
from netCDF4 import Dataset  # netCDF4 ライブラリをインポート

# Helper functions for Union-Find
def idx(i, j, nx):
    return i * nx + j

def find(parent, x):
    if parent[x] != x:
        parent[x] = find(parent, parent[x])
    return parent[x]

def union(parent, rank, x, y):
    root_x = find(parent, x)
    root_y = find(parent, y)
    if root_x != root_y:
        if rank[root_x] > rank[root_y]:
            parent[root_y] = root_x
        elif rank[root_x] < rank[root_y]:
            parent[root_x] = root_y
        else:
            parent[root_y] = root_x
            rank[root_x] += 1

def find_clusters(pr, threshold, size_threshold):
    """Union-Findを用いてクラスターを見つける"""
    nx, ny = pr.shape
    parent = np.arange(nx * ny)  # Initialize each cell as its own parent
    rank = np.zeros(nx * ny, dtype=int)

    # Union adjacent cells if they meet the threshold condition
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                if i > 0 and pr[i - 1, j] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i - 1, j, nx))
                if j > 0 and pr[i, j - 1] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i, j - 1, nx))

    # Count the size of each cluster
    cluster_size = np.zeros(nx * ny, dtype=int)
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                cluster_size[root] += 1

    # Filter clusters by size (using size_threshold)
    large_clusters = {}
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                if cluster_size[root] >= size_threshold:
                    if root not in large_clusters:
                        large_clusters[root] = []
                    large_clusters[root].append((i, j))

    return large_clusters

def plot_data(pr, clusters, threshold, filename):
    """データとクラスタリング結果を可視化し、PDFに保存する"""
    fig, ax = plt.subplots(1, 2, figsize=(12, 6))

    # ヒートマップ（入力データ）
    ax[0].imshow(pr, cmap='viridis', origin='upper')
    ax[0].set_title('Input Data')
    ax[0].set_xlabel('j (Column Index)')
    ax[0].set_ylabel('i (Row Index)')
    ax[0].invert_yaxis()

    # クラスタリング結果
    cluster_map = np.zeros_like(pr, dtype=int)
    for cluster_id, cells in clusters.items():
        for (i, j) in cells:
            cluster_map[i, j] = cluster_id

    im = ax[1].imshow(cluster_map, cmap='tab20', origin='upper')
    ax[1].set_title('Clusters (Threshold > {})'.format(threshold))
    ax[1].set_xlabel('j (Column Index)')
    ax[1].invert_yaxis()
    fig.colorbar(im, ax=ax[1], orientation='vertical', label='Cluster ID')

    plt.tight_layout()

    # PDFに保存
    plt.savefig(filename, format='pdf')  
    plt.close(fig)  # 図を閉じてメモリを解放

    # 保存したファイル名を表示
    print(f"Saved the cluster results to {filename}")

# Main testing
if __name__ == "__main__":
    nx, ny = 100, 100  # Grid size for the test
    threshold = 80
    size_threshold = 500  # 変更可能なクラスターサイズの閾値

    # スクリプト名を動的に取得してファイル名に使用
    script_name = os.path.splitext(os.path.basename(__file__))[0]

    # 1. サイズが80より大きいけれども、ブロックの数が500未満のデータを作成
    pr_large_block = np.zeros((nx, ny), dtype=int)
    pr_large_block[20:30, 20:30] = 85  # サイズが80以上のブロック
    pr_large_block[50:55, 50:55] = 90  # サイズが80以上のブロック

    # 2. サイズが500以上のクラスターを作成
    pr_large_cluster = np.zeros((nx, ny), dtype=int)
    pr_large_cluster[10:40, 10:40] = 90  # サイズが500以上の大きなクラスター
    pr_large_cluster[60:90, 60:90] = 70  # もう一つ大きなクラスター

    # 3. ブロック数が500を超えるが、サイズが80より小さいデータ
    pr_many_small_blocks = np.zeros((nx, ny), dtype=int)
    for i in range(0, 100, 5):
        for j in range(0, 100, 5):
            pr_many_small_blocks[i:i+3, j:j+3] = 90  # 80未満で小さいブロックが多数

    # 4. クラスターを確認
    print("Testing with large block but less than 500 clusters:")
    clusters_large_block = find_clusters(pr_large_block, threshold=80, size_threshold=size_threshold)
    print(f"Number of clusters with size >= {size_threshold}: {len(clusters_large_block)}")
    plot_data(pr_large_block, clusters_large_block, threshold=80, filename=f"{script_name}_1.pdf")

    print("\nTesting with large clusters > 500 cells:")
    clusters_large_cluster = find_clusters(pr_large_cluster, threshold=80, size_threshold=size_threshold)
    print(f"Number of clusters with size >= {size_threshold}: {len(clusters_large_cluster)}")
    plot_data(pr_large_cluster, clusters_large_cluster, threshold=80, filename=f"{script_name}_2.pdf")

    print("\nTesting with many small blocks but with clusters size < 80:")
    clusters_small_blocks = find_clusters(pr_many_small_blocks, threshold=80, size_threshold=size_threshold)
    print(f"Number of clusters with size >= {size_threshold}: {len(clusters_small_blocks)}")
    plot_data(pr_many_small_blocks, clusters_small_blocks, threshold=80, filename=f"{script_name}_3.pdf")

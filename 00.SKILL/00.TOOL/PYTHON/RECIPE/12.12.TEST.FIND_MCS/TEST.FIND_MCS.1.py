# # /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/34.12.FIND_MCS/12.12.TEST.FIND_MCS

import numpy as np
import matplotlib.pyplot as plt

# Helper functions for Union-Find
def idx(i, j, nx):
    return i * nx + j

def find(parent, x):
    if parent[x] != x:
        parent[x] = find(parent, parent[x])
    return parent[x]

def union(parent, rank, x, y):
    root_x = find(parent, x)
    root_y = find(parent, y)
    if root_x != root_y:
        if rank[root_x] > rank[root_y]:
            parent[root_y] = root_x
        elif rank[root_x] < rank[root_y]:
            parent[root_x] = root_y
        else:
            parent[root_y] = root_x
            rank[root_x] += 1

def find_clusters(pr, threshold, min_cluster_size):
    """Union-Findを用いてクラスターを見つける"""
    nx, ny = pr.shape
    parent = np.arange(nx * ny)  # Initialize each cell as its own parent
    rank = np.zeros(nx * ny, dtype=int)

    # Union adjacent cells if they meet the threshold condition
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                if i > 0 and pr[i - 1, j] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i - 1, j, nx))
                if j > 0 and pr[i, j - 1] > threshold:
                    union(parent, rank, idx(i, j, nx), idx(i, j - 1, nx))

    # Count the size of each cluster
    cluster_size = np.zeros(nx * ny, dtype=int)
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                cluster_size[root] += 1

    # Filter clusters by size
    large_clusters = {}
    for i in range(nx):
        for j in range(ny):
            if pr[i, j] > threshold:
                root = find(parent, idx(i, j, nx))
                if cluster_size[root] >= min_cluster_size:
                    if root not in large_clusters:
                        large_clusters[root] = []
                    large_clusters[root].append((i, j))

    return large_clusters

def plot_data(pr, clusters, threshold, filename):
    """データとクラスタリング結果を可視化し、PDFに保存する"""
    fig, ax = plt.subplots(1, 2, figsize=(12, 6))

    # ヒートマップ（入力データ）
    ax[0].imshow(pr, cmap='viridis', origin='upper')
    ax[0].set_title('Input Data')
    ax[0].set_xlabel('j (Column Index)')
    ax[0].set_ylabel('i (Row Index)')
    ax[0].invert_yaxis()

    # クラスタリング結果
    cluster_map = np.zeros_like(pr, dtype=int)
    for cluster_id, cells in clusters.items():
        for (i, j) in cells:
            cluster_map[i, j] = cluster_id

    im = ax[1].imshow(cluster_map, cmap='tab20', origin='upper')
    ax[1].set_title('Clusters (Threshold > {})'.format(threshold))
    ax[1].set_xlabel('j (Column Index)')
    ax[1].invert_yaxis()
    fig.colorbar(im, ax=ax[1], orientation='vertical', label='Cluster ID')

    plt.tight_layout()
    plt.savefig(filename, format='pdf')  # PDFに保存
    plt.close(fig)  # 図を閉じてメモリを解放

# Main testing
if __name__ == "__main__":
    nx, ny = 100, 100  # Grid size for the test
    threshold = 80
    min_cluster_size = 20

    # Generate test data with random values
    np.random.seed(42)
    pr = (np.random.rand(nx, ny) * 100).astype(int)

    print("Testing with random data:")
    clusters = find_clusters(pr, threshold, min_cluster_size)
    print(f"Number of clusters with size >= {min_cluster_size}: {len(clusters)}")

    # Plot the input data and clustering result, and save to PDF
    plot_data(pr, clusters, threshold, "random_data_clusters.pdf")

    # Create custom test data
    print("\nTesting with custom data:")
    pr_test = np.zeros((nx, ny), dtype=int)
    pr_test[20:30, 20:30] = 85  # High-value block 1
    pr_test[50:55, 50:55] = 90  # High-value block 2

    clusters_test = find_clusters(pr_test, threshold, min_cluster_size)
    print(f"Number of clusters with size >= {min_cluster_size}: {len(clusters_test)}")

    # Plot the custom test data and clustering result, and save to PDF
    plot_data(pr_test, clusters_test, threshold, "custom_data_clusters.pdf")


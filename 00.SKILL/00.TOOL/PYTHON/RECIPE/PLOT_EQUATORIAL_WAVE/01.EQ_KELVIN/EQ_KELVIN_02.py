# -*- coding: shift-jis -*-
"""
u, v, phiという2次元配列が与えられたとき，phiを色で，(u,v)を矢印でプロットするスクリプトを作って。
"""
import numpy as np
import matplotlib.pyplot as plt


# 2次元ガウス分布の関数
def gaussian_2d(y):
    return np.exp(-y**2)

TITLE="EQUATORIAL KELVIN WAVE"

# 配列のサイズ
size = (50, 50)

# x, yのグリッドを作成
x = np.linspace(0, 2, size[0])  # xの範囲
y = np.linspace(-3, 3, size[1])  # yの範囲
X, Y = np.meshgrid(x, y, indexing='ij')  # x, yの2次元グリッドを作成
# (Nx, Ny)型の配列をプロットする場合はindexing="ij"をつける

# 圧力場
#k=6.0 #x方向の波数
phi = np.cos(X*np.pi-np.pi/2)*gaussian_2d(Y)

# 速度場
u=np.cos(X*np.pi-np.pi/2)*gaussian_2d(Y)
v = np.zeros_like(u) #vは全部0にする

# プロットの作成
fig, ax = plt.subplots()

# カラーマップを作成

"""
# カスタムカラーマップの定義
import matplotlib.colors as mcolors
colors = [
    "#FFFFFF",  # 白
    "#ADD8E6",  # 薄い青
    "#90EE90",  # 薄い緑
    "#FFFF00",  # 黄色
    "#FFA500",  # オレンジ
    "#FF0000",  # 赤
    "#FF00FF"   # マゼンタ
]
cm = mcolors.LinearSegmentedColormap.from_list("custom_cmap", colors)
"""

#cm='Oranges' #'jet' #'YlOrRd'

# 反転させたRdBuカラーマップを作成
#cm = plt.cm.RdBu_r  # ここで _r を追加すると反転されます

# 青->白->オレンジ
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
top = mpl.colormaps['Blues_r'].resampled(128)
bottom = mpl.colormaps['Oranges'].resampled(128)
newcolors = np.vstack((top(np.linspace(0, 1, 128)),
                       bottom(np.linspace(0, 1, 128))))
cm = ListedColormap(newcolors, name='BlueOrange')

# phiを色でプロット
contour = ax.contourf(X, Y, phi, cmap=cm)

# X,Y,u,vを間引く
partial = lambda aa: aa[::5, ::5]
Xsub, Ysub, usub, vsub = list(map(partial, [X, Y, u, v]))

# (u, v)のベクトルを矢印でプロット（白抜きの黒い矢印）
q = ax.quiver(Xsub, Ysub, usub, vsub, scale=10, width=0.005, edgecolors='white', facecolors='black', linewidths=0.1)

# カラーバーの追加
fig.colorbar(contour)

# 軸ラベル
ax.set_xlabel('X'); ax.set_ylabel('Y')

# タイトルの追加
ax.set_title(TITLE)

# グラフの表示
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = filename_no_extension + ".PDF"
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

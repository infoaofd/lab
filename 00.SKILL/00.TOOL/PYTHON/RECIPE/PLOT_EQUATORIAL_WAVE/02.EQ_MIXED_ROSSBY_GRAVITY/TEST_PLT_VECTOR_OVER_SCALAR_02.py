# -*- coding: shift-jis -*-
"""
u, v, phiという2次元配列が与えられたとき，phiを色で，(u,v)を矢印でプロットするスクリプトを作って。
"""
import numpy as np
import matplotlib.pyplot as plt

# 2次元ガウス分布の関数
def gaussian_2d(x, y, sigma=1):
    return (1 / (2 * np.pi * sigma**2)) * np.exp(-(x**2 + y**2) / (2 * sigma**2))

# 中心差分、前進差分、後退差分を使って1次元の導関数を計算する関数
def derivative_1d(f, axis=0, dx=1):
    """
    中心差分、前進差分、後退差分を使って1次元の導関数を計算する関数。
    
    Parameters:
    f : ndarray
        微分したいデータ（2D配列）
    axis : int, optional (default=0)
        微分を計算する軸。0はx軸方向、1はy軸方向
    dx : float, optional (default=1)
        微分の間隔
    """
    # 結果用の配列を作成
    dfdx = np.zeros_like(f)
    
    # 中央部分の計算（中心差分）
    if axis == 0:  # X方向の微分
        dfdx[1:-1, :] = (f[2:, :] - f[:-2, :]) / (2 * dx)
        # 両端の計算（前進差分、後退差分）
        dfdx[0, :] = (f[1, :] - f[0, :]) / dx  # 前進差分（右端）
        dfdx[-1, :] = (f[-1, :] - f[-2, :]) / dx  # 後退差分（左端）
        
    elif axis == 1:  # Y方向の微分
        dfdx[:, 1:-1] = (f[:, 2:] - f[:, :-2]) / (2 * dx)
        # 両端の計算（前進差分、後退差分）
        dfdx[:, 0] = (f[:, 1] - f[:, 0]) / dx  # 前進差分（上端）
        dfdx[:, -1] = (f[:, -1] - f[:, -2]) / dx  # 後退差分（下端）  
        
    return dfdx

# 配列のサイズ
size = (50, 50)

# x, yのグリッドを作成
x = np.linspace(-3, 3, size[0])  # xの範囲
y = np.linspace(-3, 3, size[1])  # yの範囲
X, Y = np.meshgrid(x, y, indexing='ij')  # x, yの2次元グリッドを作成
# (Nx, Ny)型の配列をプロットする場合はindexing="ij"をつける

# 2次元ガウス分布の値を計算
sigma = 1  # 標準偏差
phi = gaussian_2d(X, Y, sigma)

# x方向の導関数を計算
dx = x[1] - x[0]  # x方向の間隔
dphi_dx = derivative_1d(phi, axis=0, dx=dx)

# y方向の導関数を計算
dy = y[1] - y[0]  # y方向の間隔
dphi_dy = derivative_1d(phi, axis=1, dx=dy)

# 流れのベクトルを計算（時計回りの流れ）
u = -dphi_dy
v =  dphi_dx


# プロットの作成
fig, ax = plt.subplots()

# カラーマップを作成
cm = 'Oranges'  # 色のカラーマップ

# phiを色でプロット
contour = ax.contourf(X, Y, phi, cmap=cm)

# X,Y,u,vを間引く
partial = lambda x: x[::5, ::5]
Xsub, Ysub, usub, vsub = list(map(partial, [X, Y, u, v]))

# (u, v)のベクトルを矢印でプロット（白抜きの黒い矢印）
q = ax.quiver(Xsub, Ysub, usub, vsub, scale=1.5, width=0.005, edgecolors='white', facecolors='black', linewidths=0.1)

# カラーバーの追加
fig.colorbar(contour)

# 軸ラベル
ax.set_xlabel('X')
ax.set_ylabel('Y')

# グラフの表示
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = filename_no_extension + ".PDF"
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

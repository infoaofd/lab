# -*- coding: shift-jis -*-
"""
u, v, phiという2次元配列が与えられたとき，phiを色で，(u,v)を矢印でプロットするスクリプトを作って。
"""
import numpy as np
import matplotlib.pyplot as plt
#import os

# 2次元ガウス分布の関数
def gaussian_2d(x, y, sigma=1):
    return (1 / (2 * np.pi * sigma**2)) * np.exp(-(x**2 + y**2) / (2 * sigma**2))

# 配列のサイズ
size = (50, 50)

# x, yのグリッドを作成
x = np.linspace(-3, 3, size[0])  # xの範囲
y = np.linspace(-3, 3, size[1])  # yの範囲
X, Y = np.meshgrid(x, y, indexing='ij')  # x, yの2次元グリッドを作成
# (Nx, Ny)型の配列をプロットする場合はindexing="ij"をつける

# 2次元ガウス分布の値を計算
sigma = 1  # 標準偏差
phi = gaussian_2d(X, Y, sigma)

# プロットの作成
fig, ax = plt.subplots()

"""
# カスタムカラーマップの定義
import matplotlib.colors as mcolors
colors = [
    "#FFFFFF",  # 白
    "#ADD8E6",  # 薄い青
    "#90EE90",  # 薄い緑
    "#FFFF00",  # 黄色
    "#FFA500",  # オレンジ
    "#FF0000",  # 赤
    "#FF00FF"   # マゼンタ
]
cm = mcolors.LinearSegmentedColormap.from_list("custom_cmap", colors)
"""

# カラーマップを作成
cm='Oranges' #'jet' #'YlOrRd'

# phiを色でプロット
contour = ax.contourf(X, Y, phi, cmap=cm)

# (u, v)のベクトルを矢印でプロット（白抜きの黒い矢印）
#ax.quiver(X, Y, u, v, scale=10, edgecolors='white', facecolors='black', linewidths=0.5)

# カラーバーの追加
fig.colorbar(contour)

# 軸ラベル
ax.set_xlabel('X')
ax.set_ylabel('Y')

# グラフの表示
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = filename_no_extension + ".PDF"
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

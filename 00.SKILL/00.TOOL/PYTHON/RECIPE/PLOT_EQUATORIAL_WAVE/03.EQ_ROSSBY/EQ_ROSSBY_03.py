# -*- coding: shift-jis -*-
"""
u, v, phiという2次元配列が与えられたとき，phiを色で，(u,v)を矢印でプロットするスクリプトを作って。
"""
import numpy as np
import matplotlib.pyplot as plt


# 2次元ガウス分布の関数
def gaussian_2d(y):
    return np.exp(-0.5*y**2)

# 中心差分、前進差分、後退差分を使って1次元の導関数を計算する関数
def derivative_1d(f, axis=0, dx=1):
    """
    中心差分、前進差分、後退差分を使って1次元の導関数を計算する関数。
    
    Parameters:
    f : ndarray
        微分したいデータ（2D配列）
    axis : int, optional (default=0)
        微分を計算する軸。0はx軸方向、1はy軸方向
    dx : float, optional (default=1)
        微分の間隔
    """
    # 結果用の配列を作成
    dfdx = np.zeros_like(f)
    
    # 中央部分の計算（中心差分）
    if axis == 0:  # X方向の微分
        dfdx[1:-1, :] = (f[2:, :] - f[:-2, :]) / (2 * dx)
        # 両端の計算（前進差分、後退差分）
        dfdx[0, :] = (f[1, :] - f[0, :]) / dx  # 前進差分（右端）
        dfdx[-1, :] = (f[-1, :] - f[-2, :]) / dx  # 後退差分（左端）
        
    elif axis == 1:  # Y方向の微分
        dfdx[:, 1:-1] = (f[:, 2:] - f[:, :-2]) / (2 * dx)
        # 両端の計算（前進差分、後退差分）
        dfdx[:, 0] = (f[:, 1] - f[:, 0]) / dx  # 前進差分（上端）
        dfdx[:, -1] = (f[:, -1] - f[:, -2]) / dx  # 後退差分（下端）  
        
    return dfdx

TITLE="EQUATORIAL ROSSBY WAVE"

# 配列のサイズ
size = (50, 50)

# x, yのグリッドを作成
x = np.linspace(0, 2, size[0])  # xの範囲
y = np.linspace(-3, 3, size[1])  # yの範囲
X, Y = np.meshgrid(x, y, indexing='ij')  # x, yの2次元グリッドを作成
# (Nx, Ny)型の配列をプロットする場合はindexing="ij"をつける

# モード番号
n=1
print("Mode number, n="+str(n))

# 波数の設定
k=1
print("Wave number, k=",str(k))

# 分散関係式を解いて角振動数を求める
omega1, omega2, omega3 = [], [], []
#omega1_n, omega2_n, omega3_n = [], [], []

coeffs = [1, 0, -(k**2 + 2*n + 1), -k]
#coeffs = [1, 0, (-k**2 -3), -k]
roots = np.roots(coeffs)
roots.sort()
omega1=(roots[0])  # 負の慣性重力波
omg=(roots[1])  # ロスビー波
omega3=(roots[2])  # 正の慣性重力波
#print("omega1="+str(omega1))
print("omg="+str(omg))
#print("omega3="+str(omega3))

# エルミート多項式
from scipy.special import hermite
Hn = hermite(n) # n次のエルミート多項式の計算

theta_0=np.pi #初期位相
theta=X*np.pi-theta_0 #位相
print("theta_0="+str(theta_0))

# 速度場
vhat=Hn(Y)*gaussian_2d(Y)
v=(vhat*np.exp(1j*theta)).real
u = np.zeros_like(v) #uは仮に全部0にしておく

# y方向の導関数を計算
dy = y[1] - y[0]  # y方向の間隔
dvhat_dy = derivative_1d(vhat, axis=1, dx=dy)

u=(1j/(omg**2-k**2)*(omg*Y*vhat - k*dvhat_dy)*np.exp(1j*theta)).real


# プロットの作成
fig, ax = plt.subplots()

# 圧力場
phi=(1j/(omg**2-k**2)*(k*Y*vhat - omg*dvhat_dy)*np.exp(1j*theta)).real
# カラーマップを作成

"""
# カスタムカラーマップの定義
import matplotlib.colors as mcolors
colors = [
    "#FFFFFF",  # 白
    "#ADD8E6",  # 薄い青
    "#90EE90",  # 薄い緑
    "#FFFF00",  # 黄色
    "#FFA500",  # オレンジ
    "#FF0000",  # 赤
    "#FF00FF"   # マゼンタ
]
cm = mcolors.LinearSegmentedColormap.from_list("custom_cmap", colors)
"""

#cm='Oranges' #'jet' #'YlOrRd'

# 反転させたRdBuカラーマップを作成
#cm = plt.cm.RdBu_r  # ここで _r を追加すると反転されます

# 青->白->オレンジ
import matplotlib as mpl
from matplotlib.colors import LinearSegmentedColormap, ListedColormap
top = mpl.colormaps['Blues_r'].resampled(128)
bottom = mpl.colormaps['Oranges'].resampled(128)
newcolors = np.vstack((top(np.linspace(0, 1, 128)),
                       bottom(np.linspace(0, 1, 128))))
cm = ListedColormap(newcolors, name='BlueOrange')

# phiを色でプロット
contour = ax.contourf(X, Y, phi, cmap=cm)

# X,Y,u,vを間引く
partial = lambda aa: aa[::4, ::4]
Xsub, Ysub, usub, vsub = list(map(partial, [X, Y, u, v]))

# (u, v)のベクトルを矢印でプロット（白抜きの黒い矢印）
q = ax.quiver(Xsub, Ysub, usub, vsub, scale=20, width=0.005, edgecolors='white', facecolors='black', linewidths=0.1)


fig.colorbar(contour) # カラーバーの追加


ax.set_xlabel('X'); ax.set_ylabel('Y') # 軸ラベル

ax.set_title(TITLE) # タイトルの追加

#ax.set_aspect('equal') # 縦横比を軸の範囲に合わせる

# グラフの表示
import os
filename = os.path.basename(__file__)
filename_no_extension = os.path.splitext(filename)[0]
fn_ = filename_no_extension

FIG = filename_no_extension + ".PDF"
plt.savefig(FIG)
print("FIG: " + FIG)
print("")

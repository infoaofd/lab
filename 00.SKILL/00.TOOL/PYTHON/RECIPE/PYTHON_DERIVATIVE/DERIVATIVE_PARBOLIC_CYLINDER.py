import sympy as sp

# シンボリック変数 y を定義
y = sp.symbols('y')

# n次のエルミート多項式 H_n(y) を定義
n = sp.symbols('n')
Hn = sp.hermite(n, y)  # エルミート多項式 H_n(y)

# 式 v = H_n(y) * exp(-1/2 * y^2)
v = Hn * sp.exp(-1/2 * y**2)

# v の y に関する導関数を計算
dv_dy = sp.diff(v, y)

# 特定の y の値で評価する
y_val = 1.0  # ここで y の値を指定
n_val = 2    # 例えば、n=2 の場合

# エルミート多項式 H_n(y) を計算
Hn_val = sp.hermite(n_val, y)

# v とその導関数を評価
v_val = Hn_val * sp.exp(-1/2 * y_val**2)

#diff関数を使って導関数の値を求める
dv_dy_val = sp.diff(Hn_val * sp.exp(-1/2 * y**2), y).subs(y, y_val)

# 結果の表示
print(f"v({y_val}) = {v_val}")
print(f"dv/dy({y_val}) = {dv_dy_val}")


# # -*- coding: utf-8 -*-
# /work09/$(whoami)/00.SKILL/00.TOOL/PYTHON/RECIPE/PLOT_ENSO_MEI_INDEX
import pandas as pd
import matplotlib.pyplot as plt
import os
import requests

# ダウンロードするURL
url = "https://psl.noaa.gov/enso/mei/data/meiv2.data"
file_path = "meiv2.data"  # ダウンロードしたファイルの保存先

# ファイルをダウンロードする関数
def download_file(url, file_path):
    response = requests.get(url)
    with open(file_path, 'wb') as f:
        f.write(response.content)
    print(f"File downloaded: {file_path}")

# データの読み込み
def read_data(file_path):
    # ファイルを読み込んで行を処理する
    data = []
    months = ["DJ", "JF", "FM", "MA", "AM", "MJ", "JJ", "JA", "AS", "SO", "ON", "ND"]
    
    with open(file_path, 'r') as file:
        lines = file.readlines()

    # 1行目（開始年と終了年）は無視
    # 2行目以降のデータを処理
    for line in lines[1:]:
        # コメント行（欠損値の記録部分）や空行をスキップ
        if line.startswith('Multivariate') or line.startswith('https://'):
            break
        if line.strip() == '':
            continue
        
        values = line.split()
        try:
            year = int(values[0])
        except ValueError:
            continue  # 年が無効な場合はスキップ（-999.00 など）

        months_data = [float(value) if float(value) != -999.00 else None for value in values[1:]]
        data.append([year] + months_data)

    df = pd.DataFrame(data, columns=["Year"] + months)
    return df

# グラフを描画
def plot_data(df):
    dates = []
    values = []
    
    # データを1本の線として描画
    for _, row in df.iterrows():
        for month, value in zip(row[1:].index, row[1:]):
            dates.append(f"{row['Year']} {month}")
            values.append(value)

    plt.figure(figsize=(12, 6))  # 縦のサイズを大きくして見やすくする

    # 塗りつぶし: 正の部分は赤、負の部分は青
    plt.fill_between(dates, values, where=[v > 0 for v in values], color='r', alpha=0.5, label='Positive')  # 正の部分を赤で塗りつぶし
    plt.fill_between(dates, values, where=[v < 0 for v in values], color='b', alpha=0.5, label='Negative')  # 負の部分を青で塗りつぶし

    plt.title('Multivariate ENSO Index Version 2 (MEI.v2)', fontsize=16)
    plt.xlabel('Year', fontsize=12)
    plt.ylabel('MEI', fontsize=12)

    # X軸のラベルを5年ごとに表示
    years = df["Year"]
    tick_positions = [i * 12 for i in range(len(df))]
    tick_labels = years[::5]

    plt.xticks(tick_positions[::5], tick_labels, rotation=45)

    # 正のピークに横軸の値（年と月）を追加
    for i in range(1, len(values) - 1):
        if values[i] > 1.5 and values[i] > values[i - 1] and values[i] > values[i + 1]:
            plt.text(dates[i], values[i] + 0.1, dates[i], ha='center', fontsize=10, color='black')

    # 負のピークに横軸の値（年と月）を追加
    for i in range(1, len(values) - 1):
        if values[i] < -1.5 and values[i] < values[i - 1] and values[i] < values[i + 1]:
            plt.text(dates[i], values[i] - 0.1, dates[i], ha='center', fontsize=10, color='black')

    plt.grid(False)
    plt.tight_layout()

    script_name = os.path.basename(__file__).replace(".py", "")
    output_pdf = f"{script_name}_timeseries_plot.pdf"
    
    plt.savefig(output_pdf, format='pdf')
    plt.close()
    
    print(f"Plot saved as {output_pdf}")

# メイン処理
def main():
    # meiv2.data ファイルをダウンロード
    download_file(url, file_path)
    
    # ダウンロードしたファイルを読み込んでプロット作成
    df = read_data(file_path)
    plot_data(df)

if __name__ == "__main__":
    main()

# cdo入門

## cdoとは

- Climate Data Operator という名称のソフトウェア（アプリ）。

- 気象・海洋のデータ(gribやnetCDF)の処理につかう

- Linuxのコマンドとして使う

[[_TOC_]]

## 予備知識

下記が必要な予備知識である。

### Linuxのシェルの基本操作

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/01.BASH/0.LINUX_TUTORIAL_01.md

### シェルスクリプトの文法の基礎

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_SCRIPT_TUTORIAL.md



## 参考資料

http://www.hysk.sakura.ne.jp/Linux_tips/how2use_CDO

https://ccsr.aori.u-tokyo.ac.jp/~obase/cdo.html

http://kodama.fubuki.info/wiki/wiki.cgi/CDO/tips?lang=jp

https://code.mpimet.mpg.de/projects/cdo/wiki/Tutorial

https://code.mpimet.mpg.de/projects/cdo/embedded/cdo.pdf



## 基本

CDOは**Linuxのコマンド**として提供されているので，Rloginなどのコンソールからサーバーに接続し，コマンドを打ち込むか，シェルスクリプトを書いて使用留守。基本的には下記の書式で使用する

```
$ cdo オペレーター 入力ファイル 出力ファイル 
```

オペレーター(演算子)では，入力ファイルをどのように処理するか記載する

以降, 明記しない場合は, 入力ファイルを, **INFLE**, 出力ファイルを**OFLE**と表記する。



## データ情報の表示

```
$ cdo sinfo INFLE
```

データの基本情報を表示する  



```
$ cdo showname INFLE
```

ファイルのデータの変数名を表示する  



```
$ cdo showlevel INFLE
```

データの鉛直層の高さリストをターミナルに表示  



```
$ cdo showtime INFLE
```

時間ステップをターミナルに表示



## テキストファイルで出力

#### OUTPUTTAB

```
cdo outputtab,date,lon=129,lat=32,value INFLE > OFLE
```

入力ファイルINFLEに含まれる経度129度，緯度32度におけるデータをOFLEというファイルに出力する



## データの切り出し

#### selllonlatbox

```
$ cdo sellonlatbox,120,150,20,50 infile.nc outfile.nc
```

データから指定した空間的領域を抜き出す。この例では，経度120－150度，緯度20－50度の領域。



#### selyear

```
$ cdo selyear,1970/1999 variable.nc variable.1970-1999.nc
```

variable.ncに含まれるデータのうち， 1970年から1999年までを切り出して，variable.1970-1999.ncに出力する。



#### 季節平均

##### DJF平均

seasmeanを使うと同じ年の1,2,12月の平均を求めてしまう。当年の12月と翌年の１，2月の平均を求めたい時はseasmeanを使うのではなく，下記のようにする。

`$IN`に1月からひと月ごとのデータが収納されているとする。

```bash
cdo timselmean,3,11,9 $IN $TMP
```

- 3 = 3か月の平均を求める (DJF)
- 11 = 最初の11か月はスキップする (1月から11月)
- 9 = skip 9 months between every 3 months interval (3月から11月)

##### 11月から翌年3月の平均

```
cdo timselmean,5,10,7 ifile ofile
```

- 5 = mean over 5 months (November to March)
- 10 = skip the first 10 months (January to October)
- 7 = skip 7 months between every 5 months interval (April to October)

## 内挿

### 水平内挿

#### remapdis

```
$ cdo remapdis,r360x180 INFLE OFLE
```

1°x1°の緯度経度格子へ，その周囲4点の距離の重みで線形内挿を行い，結果をOFLEに書き出す



#### remapbil

```bash
$ cdo  remapbil,r360x181 INFLE　OFLE
```

remapbilはバイリニア補間    

r360x181は経度，緯度方向にそれぞれ360個，181個の解像度に補間する（1度×1度格子） 



```
cdo -remapbil,lon=130_lat=32 INFLE OFLE 
```

経度130度，緯度32度の一点のデータを抜き出す



#### remapcon

ファイルで指定した格子情報をもとに内挿する  

水平高解像度なファイルを低解像度なファイルへ変換  
https://qiita.com/wm-ytakano/items/6d3ef4aa5d032c516162  

remapping from fine to coarse grid  
https://code.mpimet.mpg.de/boards/1/topics/562  

```bash
cat > MYGRID.TXT << EOF
gridtype = lonlat
xsize    = 481
ysize    = 505
xunits = 'degree'
yunits = 'degree'
xfirst   = 120
xinc     = 0.0625
yfirst   = 22.4
yinc     = 0.05
EOF
```

```bash
$ cdo remapcon,MYGRID.TXT $IN $OUT 
```


The bilinar interpolation uses only 4 surrounding grid points. The conservative remapping with **remapcon** takes **all source grid points** into account.



### 鉛直内挿

#### intlevel

```
$ cdo intlevel,100000,50000,10000 INFLE OFLE
```

INFLEに含まれる等圧面データを，1000hPa, 500hPa, 100hPa上に内挿して，OFLEに出力する  



## 緯度の南北を逆にする

#### invertlat

```
$ cdo -invertlat INFLE OFLE
```

北から南の順にならんでいるデータを南から北に並び替える



## データの結合

#### merge


```bash
$ cdo merge t.1000hPa.nc t.975hPa.nc t.950hPa.nc t.1000-950hPa.nc
```

複数の入力ファイルを並んだ順 (t.1000hPa.nc t.975hPa.nc t.950hPa.nc)に結合する。各ファイルは次元がそろっている（結合する次元以外）必要がある。



#### mergetime

時間方向に結合する。ファイルの順序をばらばらに入れても自動的に時間順に結合される。

```
$ cdo mergetime variable.1951-1980.nc variable.1981-2000.nc variable.nc
```

variable.1951-1980.ncとvariable.1981-2000.ncを連結して，variable.ncに出力する  



```bash
$ cdo mergetime t.00.nc t.06.nc t.12.nc t.18.nc t.day.nc
```

00, 06, 12, 18 UTCに分かれたファイル (t.00.nc, t.06.nc, t.12.nc, t.18.nc)を結合して，t.day.ncに出力する。



## 変数名変更

```
$ cdo chname,z,gph INFLE OFLE
```

INFLEに含まれるzという名前の変数を，gphという名前に変更してOFLEに出力する



## 計算

### 同一ファイル内の変数を計算する

#### expr

`expr,'式'`で演算を指定する

```bash
$ cdo expr,'VOUT=VIN1-VIN2;' INFLE OFLE
```

INFLEに含まれるVIN1とVIN2という変数の差を計算し，VOUTとしてOFLEに書き出す

```
$ cdo expr,'UMAG=sqrt(sqr(U)+sqr(V))'  INFLE OFLE
```

風速の東西成分Uと南北成分Vから風速の大きさUMAGを計算し，OLFEに書き出す。



### ばらばらのファイル内の変数を計算する

#### 変数が一つの場合

```bash
$ cdo div prw.nc sprw.nc crh.nc
```

prw.ncのデータをsprw.ncのデータで割って， crh.ncに出力する

add, sub, mul, divなどの演算子が使える

#### 例: 定数をかける

```bash
$ cdo mulc,2.5e6 evap.nc lhf.nc
```

evap.ncのデータに2.5e6をかけて，lhf.ncに出力 (蒸発量から潜熱フラックスの換算）



## 変数名を変更する

#### setpartabn

```bash
cat <<EOF> mypartab
&parameter
name=olr # 変更対象の変数名
out_name=rlut　＃出力する変数名
standard_name=outgoing_long_wave_radiation
long_name="outgoing long wave radiation"
units ="W m-2"
/
EOF
$ cdo setpartabn,mypartab,convert olr.nc rlut.nc
```

cdoで計算した後の結果は、元のnetcdfファイルの変数名で出力されてしまうので，setpartabnで変換する。  

事前に名称を変更する変数に関する情報を記述したファイルを作成しておく（上の例ではmypartab）  



## パイプライン

2つの処理を一つのコマンドで書くことができる（最初の処理結果を次の処理に渡す）

```bash
$ cdo daymean -merge t.1000hPa.nc t.975hPa.nc t.950hPa.nc t.1000-950hPa.day.nc
```

mergeを用いてt.1000hPa.nc t.975hPa.nc t.950hPa.ncを結合し，その結果を時間平均する     

**右側のオペレータから順に実行される** (上の例では, **mergeしてからdaymean**を計算する)  



## 欠損値処理

### 欠損値を内挿して埋める 

#### setmisstonn

```bash
$ cdo setmisstonn INFLE OFLE
```



## マルチモデル解析

```
$ cdo ensmean INFLE1 INFLE2 INFLE3 OFLE
```

3つの同形式のファイルINFLE1, INFLE2, INFLE3に含まれる各変数に関して，アンサンブル平均を計算し，OFLEに出力する。  

入力ファイルの全ての次元が一致している必要がある  



```
$ cdo ensstd INFLE1 INFLE2 INFLE3 OFLE
```

アンサンブル平均からの標準偏差を計算して，OFLEに出力する



## シェルスクリプトで効率化

```bash
#! /bin/bash

for model in A B C ;do   # 1

cdo selyear,1970/1999 ${model}/ta.nc ${model}/ta.1970-1999.nc  # 2

cdo selmon,1,2,12 ${model}/ta.1970-1999.nc ${model}/ta.1970-1999.djf.nc # 3

cdo timmean ${model}/ta.1970-1999.djf.nc ${model}/ta.1970-1999.djf_mean.nc # 4

cdo zonmean ${model}/ta.1970-1999.djf_mean.nc \
${model}/ta.1970-1999.djf_mean.zonmean.nc #5

rm ‒f ${model}/ta.1970-1999.nc ${model}/ta.1970-1999.djf.nc \
${model}/ta.1970-1999.djf_mean.nc #6

done

cdo ensmean */ta.1970-1999.djf_mean.zonmean.nc \
./ta.1970-1999.djf_mean.zonmean.ensmean.nc #7
```

1. モデルA,B,Cに対して   
2. taから1970-1999を切り取り  
3. DJF(12月から2月のデータ)を切り取り  
4. DJF平均  
5. 東西平均  
6. 不要なファイルを消去する  
7. モデルA,B,Cに対する上記の計算結果をアンサンブル平均する


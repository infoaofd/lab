
VER=04

DSET=JRA55
VAR=LHF
YS=1982; YE=2021
MM=07
BI=01

BIPEN=${MM}BP${BI}
if [ $BIPEN = "07BP01" ]; then
MON=7
DAY='1/10'
fi
ANAME=EASIA
AREA='80,150,0,50'

if [ $VER = "01" ];then
OPT="-selday,$DAY -selmon,$MON"
fi
if [ $VER = "02" ];then
OPT="-timmean -selday,$DAY -selmon,$MON"
fi
if [ $VER = "03" ];then
OPT="-timmean -selday,$DAY -selmon,$MON -sellonlatbox,$AREA"
fi
if [ $VER = "04" ];then
OPT1="-chname,lhtfl_gds0_sFC,LHTFL_GDS0_SFC_ave -timmean -selday,$DAY -selmon,$MON -sellonlatbox,$AREA"
OPT2=$OPT1 #"-timmean -selday,$DAY -selmon,$MON -sellonlatbox,$AREA"
fi

INFO=${DSET}_${ANAME}_${VAR}.INFO.TXT

INDIR="/work01/DATA/JRA55/6HR/SFC/LHF"

ODIR="/work01/DATA/JRA55/EASIA/10DAY/SFC/LHF/${BIPEN}"
mkdir -vp $ODIR
rm -v $ODIR/*.grib &> $INFO

date -R >$INFO
pwd    >>$INFO
echo   >>$INFO

YYYY=$YS
while [ $YYYY -le $YE ]; do 

if [ $YYYY -lt 2014 ]; then
INFLE=$(ls $INDIR/fcst_phy2m125.121_lhtfl.${YYYY}* )
OPT=$OPT1
fi
if [ $YYYY -ge 2014 ]; then
INFLE=$(ls $INDIR/fcst_phy2m125.121_lhtfl.${YYYY}${MM}* )
OPT=$OPT2
fi

OFLE=$ODIR/${DSET}_${ANAME}_${VAR}_${YYYY}${MM}_${BI}.grib 

rm -vf $OUT
echo


echo "======================="
echo $INFLE
echo "======================="
echo "=======================" &>>$INFO
echo $INFLE&>>$INFO
echo "======================="&>>$INFO

IN=${INFLE}
echo cdo $OPT $IN $OFLE &>>$INFO
cdo $OPT $IN $OFLE
if [ $? -ne 0 ]; then
echo
echo ERROR in $0
echo
exit 1
fi

if [ -f $OFLE ]; then
echo "======================="
echo $OFLE
echo "======================="
echo "=======================" &>>$INFO
echo $OFLE&>>$INFO
echo "======================="&>>$INFO
cdo sinfo $OFLE 
#<<COMMENT
echo &>>$INFO
cdo sinfo $OFLE &>> $INFO
echo &>>$INFO
#COMMENT
fi

echo
YYYY=$(expr $YYYY + 1)
done


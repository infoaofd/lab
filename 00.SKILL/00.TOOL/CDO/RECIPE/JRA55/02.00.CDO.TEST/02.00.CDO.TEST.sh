
VER=03

BIPEN=07BP01
if [ $BIPEN = "07BP01" ]; then
MON=7
DAY='1/10'
fi
AREA='80,150,0,50'

if [ $VER = "01" ];then
OPT="-selday,$DAY -selmon,$MON"
fi
if [ $VER = "02" ];then
OPT="-timmean -selday,$DAY -selmon,$MON"
fi
if [ $VER = "03" ];then
OPT="-timmean -selday,$DAY -selmon,$MON -sellonlatbox,$AREA"
fi



INDIR="/work01/DATA/JRA55/6HR/SFC/LHF/"

INLIST="\
fcst_phy2m125.121_lhtfl.2014070100_2014073121 \
"
#fcst_phy2m125.121_lhtfl.1982010100_1982123121 \
#fcst_phy2m125.121_lhtfl.2021010100_2021013121 \


for INFLE in $INLIST; do

OFLE=${INFLE}.TEST${VER}.grib

IFLE=${INFLE}.TEST${VER}.TXT

rm -vf $OUT
echo


echo "======================="
echo $INFLE
echo "======================="

IN=${INDIR}/${INFLE}
cdo $OPT $IN $OFLE

if [ -f $OFLE ]; then
echo "======================="
echo $OFLE
echo "======================="
cdo sinfo $OFLE 
<<COMMENT
echo
cdo showtime $OFLE
cdo show
echo
COMMENT
fi

echo
done


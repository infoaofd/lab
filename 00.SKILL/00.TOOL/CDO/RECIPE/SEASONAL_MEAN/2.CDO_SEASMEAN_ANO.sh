LEV=925
SLAT=20
SEA=DJF
YS=1949; YE=2022
INDIR=/work01/DATA/NCEP1/MON
INFLE=air.mon.mean_${SLAT}N_${LEV}.nc
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo NO SUCH FILE, $IN;exit 1; fi

ODIR=$INDIR
OFLE_AVE=$(basename $INFLE .nc)_${SEA}_AVG.nc
OUT_AVE=$ODIR/$OFLE_AVE
OFLE_ANO=$(basename $INFLE .nc)_${SEA}_ANO.nc
OUT_ANO=$ODIR/$OFLE_ANO

echo MMMMM SEASONAL MEAN $SEA
TMP=$(basename $0 .sh)_TMP.nc; rm -vf $TMP
cdo timselmean,3,11,9 $IN $TMP
#cdo -seasmean -selseas,DJF $IN $TMP
if [ ! -f $TMP ];then echo ERROR seasmean;exit 1; fi
echo

echo MMMMM SELYEAR $YS - $YE
TMP2=$(basename $0 .sh)_TMP2.nc; rm -vf $TMP2
cdo selyear,$YS/$YE $TMP $TMP2
if [ ! -f $TMP2 ];then echo ERROR selyear;exit 1; fi
echo

echo MMMMM LONG-TERM MEAN
rm -vf $OUT_AVE
cdo -timmean $TMP2 $OUT_AVE
if [ ! -f $OUT_AVE ];then echo ERROR timmean;exit 1; fi
echo

echo MMMMM ANOMALY
TMP3=$(basename $0 .sh)_TMP3.nc; rm -vf $TMP3
cdo -sub $TMP2 $OUT_AVE $TMP3
if [ ! -f $TMP3 ];then echo ERROR sub;exit 1; fi
echo

echo MMMMM REMOVE TREND
rm -vf $OUT_ANO
cdo detrend $TMP3 $OUT_ANO
if [ ! -f $OUT_ANO ];then echo ERROR detrend;exit 1; fi
echo

rm -vf $TMP $TMP2 $TMP3; echo
if [ -f $OUT_AVE ];then echo OUTPUT: $OUT_AVE;fi 
if [ -f $OUT_ANO ];then echo OUTPUT: $OUT_ANO;fi 



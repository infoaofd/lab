#!/bin/bash

ys=1949; ye=2022
y=$ys
mm=12; dd=01; hh=00

i=0
while [ $y -le $ye ]; do

  yyyy=$y
  4.CHECK_AVE_ANO.sh $yyyy

  y=$(expr $y + 1)
done

exit 0
<<COMMENT
https://i.kuku.lu/
Magical Converter
ブラウザだけでかんたんに画像を変換
COMMENT
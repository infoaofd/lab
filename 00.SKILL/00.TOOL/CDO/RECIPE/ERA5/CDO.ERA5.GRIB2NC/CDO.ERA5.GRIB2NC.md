# CDO.ERA5.GRIB2NC.sh

cdo selvar with multiple input files
https://code.mpimet.mpg.de/boards/1/topics/350

```bash
cdo select,name=var1,var2,...,varN ifiles ofile
```



```bash
/work02/work01/DATA/ERA5/ECS/01HR/NC
$  CDO.ERA5.GRIB2NC.sh 
cdo    select: Processed 1080288 values from 16 variables over 24 timesteps [0.05s 27MB]

var134 Convective precipitation [m]
var218 Convective rain rate [kg m-2 s-1]
var142 Large-scale precipitation [m]
var219 Large scale rain rate [kg m-2 s-1]

[kg m-2 s-1]=[1 mm/s]

IN: /work02/work01/DATA/ERA5/ECS/01HR/2022/06/ERA5_ECS_SFC.FLUX_01HR_20220619.grib
OUT: ./ERA5_ECS_SFC.FLUX_01HR_20220619_PRECIP.nc
```

```bash
$ ncdump -h ERA5_ECS_SFC.FLUX_01HR_20220619_PRECIP.nc 
netcdf ERA5_ECS_SFC.FLUX_01HR_20220619_PRECIP {
dimensions:
        time = UNLIMITED ; // (24 currently)
        lon = 93 ;
        lat = 121 ;
variables:
        double time(time) ;
                time:standard_name = "time" ;
                time:calendar = "proleptic_gregorian" ;
                time:axis = "T" ;
                time:units = "hours since 2022-6-19 00:00:00" ;
        double lon(lon) ;
                lon:standard_name = "longitude" ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
                lon:axis = "X" ;
        double lat(lat) ;
                lat:standard_name = "latitude" ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
                lat:axis = "Y" ;
        float var218(time, lat, lon) ;
                var218:table = 228 ;
        float var142(time, lat, lon) ;
                var142:table = 128 ;
        float var219(time, lat, lon) ;
                var219:table = 228 ;
        float var134(time, lat, lon) ;
                var134:table = 128 ;
```

```bash
/work02/work01/DATA/ERA5/ECS/01HR/NC
$ cat CDO.ERA5.GRIB2NC.sh 
INDIR=/work02/work01/DATA/ERA5/ECS/01HR/2022/06
INFLE=ERA5_ECS_SFC.FLUX_01HR_20220619.grib
IN=$INDIR/$INFLE
ODIR=.
OFLE=$(basename $IN .grib)_PRECIP.nc
OUT=$ODIR/$OFLE

var1=var134 ;v1='Convective precipitation [m]'
var2=var218 ;v2='Convective rain rate [kg m-2 s-1]'
var3=var142 ;v3='Large-scale precipitation [m]'
var4=var219 ;v4='Large scale rain rate [kg m-2 s-1]'


cdo -f nc4 select,name=$var1,$var2,$var3,$var4 $IN $OUT


echo
echo $var1 $v1
echo $var2 $v2
echo $var3 $v3
echo $var4 $v4
echo
echo "[kg m-2 s-1]=[1 mm/s]"
echo
echo IN: $IN
echo OUT: $OUT

```


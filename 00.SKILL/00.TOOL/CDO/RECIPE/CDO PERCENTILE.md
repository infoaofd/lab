# CDO PERCENTILE

[[_TOC_]]

# EXAMPLE

## INPUT DATA

```bash
/work01/DATA/J-OFURO3/V1.2_PRE/22.12.DECOMP_GLOBE/0.OUT_COR3.0_DECOMP_GLOBE_JOF3/LHF/RAW_RUN
$ cdo sinfo J-OFURO3_COR3_LHF_RAW_RUN_DAILY_1988.nc 
   File format : NetCDF4
    -1 : Institut Source   T Steptype Levels Num    Points Num Dtype : Parameter ID
     1 : unknown  unknown  v instant       1   1   1036800   1  F32  : -1            
   Grid coordinates :
     1 : lonlat                   : points=1036800 (1440x720)
                              lon : 0.125 to 359.875 by 0.25 degrees_east  circular
                              lat : -89.875 to 89.875 by 0.25 degrees_north
   Vertical coordinates :
     1 : surface                  : levels=1
   Time coordinate :  366 steps
     RefTime =  1800-01-01 00:00:00  Units = hours  Calendar = standard
  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss  YYYY-MM-DD hh:mm:ss
  1988-01-01 00:00:00  1988-01-02 00:00:00  1988-01-03 00:00:00  1988-01-04 00:00:00
```

## PREPROCESS





# MANUAL

## Worksheet 3: Thresholds and extremes analysis

https://www.precisrcm.com/WORKSHOP/Worksheet3_UK.pdf

### 3.2 Calculating percentiles

1 a. ) Calculate in mm/day the **baseline** (1961-1990) and **future** (2021-2050) **95th** percentile of precipitation. Do this for cahpa, cahpb and also for APHRODITE baseline.

`timpctl,c` command calculates the cth percentile of each point in a field over all time steps. 

To use this command you need to have calculated the minimum and maximum of the field over all time steps e.g.,

```bash
cdo timpctl,c [ifile] [min_file] [max_file]
```

timmax/timin commands find the maximum/minimum of a field over all time steps.



This loop will take a few minutes to complete.

```bash
for runid in "cahpa" "cahpb"; do

cd $DATA/daily/$runid/05216
infile=${runid}a.pa.6190.05216.rr8.mmday.nc

cdo timpctl,95 $infile -timmin $infile -timmax $infile $DATA/daily
/climatology/${runid}a.pc95.05216.baseline.mmday.nc
infile=${runid}a.pa.2150.05216.rr8.mmday.nc

cdo timpctl,95 $infile -timmin $infile -timmax $infile $DATA/daily
/climatology/${runid}a.pc95.05216.future.mmday.nc

done
```

```bash
infile=aphro.day.6190.nc
cdo timpctl,95 $infile -timmin $infile -timmax $infile $DATA/daily/climatology/aphro.pc95.05216.baseline.nc
```



2. ) Calculate the baseline (1961-1990) and future (2021-2050) 95th percentile of maximum temperature and the difference between them. Do this for cahpa and cahpb.

cahp**a**: baselineとfutureで**格子サイズがそろっている**

```bash
cd $DATA/daily/climatology

cdo sub cahpaa.pc95.05216.future.mmday.nc
cahpaa.pc95.05216.baseline.mmday.nc cahpaa.pc95.05216.diff.mmday.nc

cdo sub cahpba.pc95.05216.future.mmday.nc
cahpba.pc95.05216.baseline.mmday.nc cahpba.pc95.05216.diff.mmday.nc
```

cahp**b**: **格子サイズがそろっていない**

```bash
cdo griddes aphro.pc95.05216.baseline.nc > mygrid

cdo remapbil,mygrid cahpaa.pc95.05216.baseline.mmday.nc
cahpaa.pc95.05216.baseline.mmday.rg.nc

cdo sub cahpaa.pc95.05216.baseline.mmday.rg.nc
aphro.pc95.05216.baseline.nc cahpaa.95pc.05216.obs_diff.mmday.nc

cdo remapbil,mygrid cahpba.pc95.05216.baseline.mmday.nc
cahpba.pc95.05216.baseline.mmday.rg.nc

cdo sub cahpba.pc95.05216.baseline.mmday.rg.nc
aphro.pc95.05216.baseline.nc cahpba.95pc.05216.obs_diff.mmday.nc
```



## CDO User Guide

### 2.8.14. TIMSELPCTL - Time range percentile values

p. 135

**Synopsis** 

```
timselpctl,p,nsets[,noffset[,nskip]] infile1 infile2 infile3 outfile
```

**Description** 

This operator computes percentile values over a selected number of timesteps in infile1. The algorithm uses histograms with minimum and maximum bounds given in infile2 and infile3, respectively. The default number of histogram bins is 101. The default can be overridden by setting the environment variable CDO_PCTL_NBINS to a different value. The files infile2 and infile3 should be the result of corresponding timselmin and timselmax operations, respectively. The time of outfile is determined by the time in the middle of all contributing timesteps of infile1. This can be change with the CDO option --timestat_date . For every adjacent sequence t_1, ..., t_n of timesteps of the same selected time range it is:

```
o(t, x) = pth percentile{i(t', x), t1 < t' ≤ tn}
```

このオペレータは、infile1内の選択されたタイムステップにわたってパーセンタイル値を計算します。アルゴリズムは、それぞれinfile2およびinfile3で指定された最小および最大の範囲を持つヒストグラムを使用します。ヒストグラムのデフォルトのビン数は101です。デフォルトは、環境変数CDO_PCTL_NBINSを異なる値に設定することで上書きできます。ファイルinfile2およびinfile3は、それぞれ対応するtimselminおよびtimselmax操作の結果である必要があります。outfileの時間は、infile1のすべての寄与するタイムステップの中央の時間によって決まります。これはCDOオプション--timestat_dateで変更できます。同じ選択された時間範囲の連続するタイムステップの場合、t_1、...、t_nについて次のようになります：

```
o(t, x) = pth percentile{i(t', x), t1 < t' ≤ tn}
```

**Parameter** 

p FLOAT Percentile number in 0, ..., 100 

nsets INTEGER Number of input timesteps for each output timestep 

noffset INTEGER Number of input timesteps skipped before the first timestep range (optional) 

nskip INTEGER Number of input timesteps skipped between timestep ranges (optional) 

### Environment 

CDO_PCTL_NBINS Sets the number of histogram bins. The default number is 101.
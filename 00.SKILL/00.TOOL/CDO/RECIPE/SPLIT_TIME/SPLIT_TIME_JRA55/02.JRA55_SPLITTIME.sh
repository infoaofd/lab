# /work09/$(whoami)/00.WORK/2022.MESHIMA.WV/62.12.COMPOSITE_CYCLONE/12.12.CYCLONE_TRACKING/12.12.TEST_MKPOSITION_JRA55/12.12.JRA55_GRIB2BIN
# 02.JRA55_SPLITTIME.sh

INDIR=/work01/DATA/JRA55/6HR/SFC/anal_surf/prmsl
if [ ! -d $INDIR ];then echo EEEEE NO SUCH DIR,$INDIR; exit 1;fi
ODIR=/work01/DATA/JRA55/6HR/SFC/anal_surf/prmsl/SPLIT_TIME; mkd $ODIR; echo
rm -vf $ODIR/slp.*

YS=2020;YE=2023;Y=$YS
MS=1;ME=12


while [ $Y -le $YE ]; do
M=$MS

while [ $M -le $ME ]; do

DS=01; DE=31
if [ $M -eq 2 ];then DE=28;fi
a=$(expr $Y % 400); b=$(expr $Y % 100); c=$(expr $Y % 4)
if [ $a -eq 0 -o \( $c -eq 0 -a $b -ne 0 \) -a $M -eq 2 ]; then DE=29; echo MMMMMM LEAP YEAR $Y;fi
if [ $M -eq 4 -o $M -eq 6 -o $M -eq 9 -o $M -eq 11 ];then DE=30;fi

MM=$(printf %02d $M)

INFLE=anl_surf125.002_prmsl.${Y}${MM}${DS}00_${Y}${MM}${DE}18
IN=$INDIR/$INFLE
if [ ! -f $IN ];then echo EEEEE NO SUCH FILE,$IN; exit 1;fi
echo IIIII IN: ${IN}; echo

#D=$DS ;#DE=1
#while [ $D -le $DE ];do
#DD=$(printf %02d $D)
#HS=0;HE=18;H=$HS
#while [ $H -le $HE ];do
#HH=$(printf %02d $H)

TMPBASE=$ODIR/TEMP_slp_${Y}${MM}_
rm -vf ${TMPBASE}*

cdo -splitsel,1 $IN $TMPBASE

echo

#H=$(expr $H + 6)
#done #H
#D=$(expr $D + 1)
#done #D

cdo showtimestamp $IN > list.txt
times=($(cat list.txt))
OBASE=$ODIR/slp.

x=0
for TMP in $(ls ${TMPBASE}*); do
TSTAMP=${times[$x]}
YYYY=${TSTAMP:0:4}; MM=${TSTAMP:5:2}; DD=${TSTAMP:8:2}; HH=${TSTAMP:11:2}
YMDH=${YYYY}${MM}${DD}${HH} ;# echo $TSTAMP $YMDH
OUT=${OBASE}${YMDH}.grib
mv -vf $TMP $OUT
echo OOOOO OUT: $OUT;echo
x=$(expr $x+1)
done #x

M=$(expr $M + 1)
done #M
Y=$(expr $Y + 1)
done #Y

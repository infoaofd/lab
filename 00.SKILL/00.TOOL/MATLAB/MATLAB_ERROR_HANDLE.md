# MATLAB ループのエラー処理

エラーが発生した場合, 何もせず次のiに移る

```matlab
do i=1:10

try
  webread(‘www.something that could or could not generate an error.com’)
catch 
  continue
end %try

end %i
```


#!/bin/bash
# 
# gnuplotで日付・時刻の書かれた時系列データを
# 二軸でグラフ描画し保存するシェルスクリプト
# https://hack-le.com/45576248-2/
#
# gnuplotのデータプロットで日付を扱う場合
# https://www.netplan.co.jp/archives/1625
#
# gnuplot で pdf が出力されない時: 最後にunset outputを指定する
# https://gordiustears.net/trouble-of-pdf-terminal-on-gnuplot/
#
# gnuplotでプロットなどの色をcolornameの指定で変更する
# https://yutarine.blogspot.com/2018/12/gnuplot-colorname.html
# 
INFLE1=$1; INFLE1=${INFLE:-SLP.TSR.CNTL.TXT}
INFLE2=$2; INFLE2=${INFLE:-SLP.TSR.0702.TXT}

FIG=$(basename $0 .sh).PDF
rm -vf $FIG
TITLE=""
XLABEL=""
YLABEL="SLP [hPa]"; Y2LABEL="hPa"

echo MMMM PLOT USING GNUPLOT
gnuplot <<EOF
set datafile separator ','
set xdata time
set timefmt "%Y/%m/%d %H:%M"
set format x "%m/%d\n%H:%M"
set mxtics 2
set mytics 2
#set grid xtics ytics mxtics mytics
#set key outside

set title '$TITLE'
set terminal pdfcairo color enhanced font "Helvetica,12"
set xlabel '$XLABEL'
set ylabel '$YLABEL'
set output '$FIG'
p '$INFLE1' u 1:2 w l ti "CNTL" lt rgbcolor "orange" lw 5 , '$INFLE2' u 1:2 w l ti "0702" lt rgbcolor "green" lw 5
set output
EOF
echo MMMMM DONE!
echo
if [ -f $FIG ];then echo MMMMM FIG: $FIG;fi

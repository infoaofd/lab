ST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).eps


LEV=1000
LONW=122; LONE=130; LATS=28;  LATN=34
TIME0=00Z12AUG2021 ;TIME1=12Z14AUG2021
TIME2=18Z12AUG2021 ;TIME3=12Z13AUG2021
TIME4=18Z14AUG2021

OUT=SLP.0702.TXT




#KIND='cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
#LEVS='320 350 5'

cat <<EOF>$GS

'open /work00/DATA/WRF.RW3A/HD04/RW3A.00.04/01HR/ARWpost_RW3A.00.04.05.05.0702.01/RW3A.00.04.05.05.0702.01.d01.basic_p.01HR.ctl'

'q ctlinfo'
say result

'set lev $LEV'
'set time $TIME0 $TIME1'




'q dims'
say result

'cc'

'set parea 0.6 7.0 1.0 9.0'
'set xlopts 1 6 0.18'
'set ylopts 1 6 0.18'
'set grid on 3 15'


'set gxout line'
'set vrange 0 50'
'set ylab on'
'set xlint 1'
'set ylint 5'
'set grid off'

'set cthick 5'
'set ccolor 1'
 
'set lon $LONE'
'set lat $LATS $LATN'
say 'MMMMM 0702 LONAVE'; '!date -R'
'q dims'; say result
'LONAVE=tloop(ave(slp,lon=$LONW,lon=$LONE))'

say 'MMMMM 0702  LATAVE'; '!date -R'
'set lat $LATS'
'q dims'; say result
'AVE=tloop(ave(LONAVE,lat=$LATS, lat=$LATN))'
'd AVE'


'set gxout print'
'set prnopts %10.3f 1 1'
'd AVE'
rc=write($OUT, result)
rc=close($OUT) 

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

echo
echo $LONW $LONE
echo $LATS $LATN
echo 
echo $TIME0 $TIME1
echo
echo $OUT




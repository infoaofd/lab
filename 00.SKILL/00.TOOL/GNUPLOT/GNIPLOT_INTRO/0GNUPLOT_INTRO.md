# GNUPLOT初歩

簡便な操作で色々なグラフを書けるソフト。

Windows, Mac, LinuxとOSを選ばず使用できる。

対話型処理とスクリプト処理どちらでも使える。

対話型処理で作成した図を書くためのスクリプトを自動作成することもできる（対話型処理とスクリプト処理のいいとこ取りをするための機能がある）。



## 参考資料

https://wwwnucl.ph.tsukuba.ac.jp/~hinohara/compphys2-18/doc/gnuplot.pdf



## 起動と終了

### 起動

```bash
$ gnuplot

        G N U P L O T
Terminal type is now 'qt'
(ins) gnuplot> 
```

### 終了

```
(ins) gnuplot> quit
```



## LINUXコマンドの実行

ファイル一覧

```
(ins) gnuplot> !ls
```

現在いるディレクトリ名の表示

```gnuplot
(ins) gnuplot> pwd
```



## ファイル読み込み

### ファイルの例

```
$ head -4 CNTL.TXT
```

```bash
Printing Grid -- 61 Values -- Undef = -9.99e+08
     0.657 
     0.604 
     0.606 
```

1行目 ヘッダー（メモ）

2行目以降データ

### とりあえず開いてみる

```
$ gnuplot
```

```gnuplot
(ins) gnuplot> plot "./CNTL.TXT" with line
              ^
         Bad data on line 1 of file ./CNTL.TXT
```

1行目のデータがおかしいと言っている→ヘッダー行は数値ではないので，これが原因

### ヘッダー行読み飛ばし

```bash
(ins) gnuplot> plot "./CNTL.TXT" every ::1 with line
```

`every ::1`でファイルの先頭1行目を読み飛ばす

### 省略形

`with line`は省略形である`w l`と書いても良い

```
(ins) gnuplot> plot "./CNTL.TXT" every ::1 w l
```



## ファイルに書き出し

リモート接続の場合，グラフを画面に表示することができないので，ファイルに書き出す

pngに書き出す (term=terminal)

```
gnuplot> set term png
```

ファイル名を決める (out=output)

```
gnuplot> set out "CNTL.png"
```

ファイルに書き出す

```
(ins) gnuplot> plot "./CNTL.TXT" every ::1 w l
```

2回目以降はreplotだけでよい (rep=replot)

```
gnuplot> rep
```

![CNTL](CNTL.png)

PDFファイルに書き出すこともできるが，作成したPDFが開けないソフトがある (PDF exchanger)



## 複数のデータを同じグラフに書く

コンマ`,`で区切ると複数のファイルを同時に1つのグラフにプロットすることができる。

ファイル2つの例

```
gnuplot> set term png
```

```
gnuplot> set out "HDIABATIC.png"
```

```
(ins) gnuplot> plot "./CNTL.TXT" every ::1 w l,"./070201.TXT" every ::1 w l
```



ファイル4つの例

```
gnuplot> clear
```

```
gnuplot> set term png
```

```
gnuplot> set out "SLP_TEND.png"
```

```
gnuplot> plot "./CNTL.TXT" every ::1 w l,"./070201.TXT" every ::1 w l, "./ADV_CNTL.TXT" every ::1 w l, "./ADV_0702.TXT" every ::1 w l
```

![SLP_TEND](SLP_TEND.png)

## コマンドをスクリプトへ保存する

保存方法

```
(ins) gnuplot> save 'SLP_TEND.gnuplot'
```

保存されたスクリプトの一部抜粋

```bash
$ head -2 SLP_TEND.gnuplot 
#!/work09/am/anaconda3/bin/gnuplot -persist
#
```

```bash
$ tail -2 SLP_TEND.gnuplot 
plot "./CNTL.TXT" every ::1 w l,"./070201.TXT" every ::1 w l, "./ADV_CNTL.TXT" every ::1 w l, "./ADV_0702.TXT" every ::1 w l
#    EOF
```


2021-05-06_01 ゼミ
===========================


[[_TOC_]]
## 本日の主な内容
下記の図を作成する

<img src="2021-05-06_FIG/2020-07-02_MSM.png" alt="2020-07-02_MSM" style="zoom:30%;" />



## LINUX

### 困ったときは

**検索してみる**

#### キーワードの例  

##### コマンド名

例 ls, pwd

##### やりたいこと

例　linux まとめてコピー

##### できないこと

例 linux まとめてコピー　できない

##### エラーメッセージ

メッセージ全体もしくは一部をブラザーの検索窓にコピペしてEnterキーを押す



### コマンド履歴検索

後方検索  
CTL + R   
(reverse-i-serach) :  
の後に検索したいコマンド名を含む文字を入れる  

前方検索  
準備
$HOME/.bashrcに下記の行を追加  
```
stty stop undef
```
CTL + S 
(i-serach) :  
の後に検索したいコマンド名を含む文字を入れる  



### tmux

#### マウスでスクロール
```
$ cat $HOME/.tmux.conf
set-option -g mouse on #>2.1
bind -n WheelUpPane   select-pane -t= \; copy-mode -e \; send-keys -M
bind -n WheelDownPane select-pane -t= \;                 send-keys -M
```
#### コピペ
Rlogin ->   表示 ->　オプション設定 -> クリップボード  

制御コードによるクリップボード操作  

- OSC 52 によるクリップボードの読み込みを許可する  
- OSC 52 によるクリップボードの書き込みを許可する  

にチェックを入れる  

CTL+B 矢印 (CTL+B 数字) ウィンドウの移動  
CTL+B C ウィンドウ（画面）を追加  

CTL+D  　tmux終了   


#### 画面分割
CTL+B % 左右に分割  
CTL+B  "   上下に分割  
CTL+B　矢印キー　画面間の移動  
CTL+B　x 画面分割の解除   

CTL+B [ →コピーモード（画面スクロールが可能となるモード）    

q スクロール終了（元のモードに戻る）



#### 分割した画面のサイズ変更

RLOGINだとうまくいかない

~~準備~~
~~Rlogin~~
~~表示→オプション設定→キーボード~~
~~キーコードの以下を削除~~

~~DOWN CTRL \$SPLIT_HEIGHT
LEFT CTRL \$SPLIT_OVER
RIGHT CTRL \$SPLIT_WIDTH
UP CTRL \$PAN_ROTATION~~

~~CTL+B CTL+矢印キー~~

#### 参考資料
https://tabeta-log.blogspot.com/2019/08/tmux.html  

  

### LINUXコマンド

#### pushd

pushdコマンドを使うと、自分が以前いたディレクトリにすぐに戻れる  

使用例  
/work03/am　→　現在いるディレクトリ  

```
$ mkdir -vp A/A1/A2/A3
```

mkdir: created directory `A/A1/A2/A3'

/work03/am　→　現在いるディレクトリ  

```
$ mkdir -vp B/B1/B2/B3
```

mkdir: created directory `B/B1/B2/B3'  

/work03/am　→　現在いるディレクトリ  

```
$ pushd A/A1/A2/A3
```

/work03/am/A/A1/A2/A3 /am/manda  

/work03/am/A/A1/A2/A3　→　現在いるディレクトリ  

```
$ pushd /work03/am/B/B1/B2/B3/
```

/work03/am/B/B1/B2/B3 /work03/am/A/A1/A2/A3 /work03/am  

/work03/am/B/B1/B2/B3　→　現在いるディレクトリ  

```
$ dirs -v
```

0  /work03/am/B/B1/B2/B3  
1  /work03/am/A/A1/A2/A3  
2  /work03/am  

/work03/am/B/B1/B2/B3　→　現在いるディレクトリ  

```
$ pushd +1
```

/work03/am/A/A1/A2/A3   /work05/manda /work03/am/B/B1/B2/B3  

/work03/am/A/A1/A2/A3  

```
$ pushd +2
```

/work03/am/B/B1/B2/B3 /work03/am/A/A1/A2/A3 /work03/am  

/work03/am/B/B1/B2/B3　→　現在いるディレクトリ    

   

## メソ客観解析
### 概要
客観解析＝データ同化を用いて作成された格子データ。  

客観解析では，着目している時刻よりも前の時刻データが用いられる（予報の際の初期値として用いるため）。      

 一方再解析では，着目している時刻の前後の時刻のデータが用いられる。  

#### 情報

https://www.data.jma.go.jp/add/suishin/cgi-bin/catalogue/make_product_page.cgi?id=Kyakkan
http://www.jma.go.jp/jma/kishou/know/whitep/1-3-3.html
http://www.jma.go.jp/jma/kishou/books/nwptext/45/1_chapter3.pdf

  

### データ
#### データの所在
http://database.rish.kyoto-u.ac.jp/arch/jmadata/gpv-netcdf.html  
#### 情報
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/README  


#### ダウンロード
wgetコマンドを使う  

wgetの使用法については下記を参照  
https://www.atmarkit.co.jp/ait/articles/1606/20/news024.html   

https://tatsushid.github.io/blog/2014/08/specify-wget-savedir/       

https://www.4web8.com/2435.html     

  

データの保存先

```bash
/work03/gu1
$ tree /work01/DATA/MSM/
/work01/DATA/MSM/
├── MSM-P
└── MSM-S
    └── 2020
```

  

ダウンロード前の設定

```bash
/work03/gu1
$ ORG=http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/MSM-S/

$ DEST=/work01/DATA/MSM/MSM-S/2020

$ Y=202

$ Y=2020

$ M=07

$ D=02

$ echo $ORG
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/MSM-S/

$ echo $DEST
/work01/DATA/MSM/MSM-S/2020

$ echo $Y
2020

$ echo $M$D
0702
```



ダウンロード  

```
$ wget -nv -nc -P $DEST $ORG/$Y/$M$D.nc

$ ll /work01/DATA/MSM/MSM-S/2020/0702.nc
-rw-r--r--. 1 gu1 134M 2020-07-03 10:05 /work01/DATA/MSM/MSM-S/2020/0702.nc
```



データの概要を確認  

```
$ ncdump -h /work01/DATA/MSM/MSM-S/2020/0702.nc
```
> netcdf \0702 {  
> dimensions:  
>         lon = 481 ;  
>         lat = 505 ;  
>         time = 24 ;  
>         ref_time = 8 ;  
> variables:  
>         float lon(lon) ;  
>                 lon:long_name = "longitude" ;  
>                 lon:units = "degrees_east" ;  
>                 lon:standard_name = "longitude" ;  
>         float lat(lat) ;  
>                 lat:long_name = "latitude" ;  
>                 lat:units = "degrees_north" ;  
>                 lat:standard_name = "latitude" ;  
>         float time(time) ;  
>                 time:long_name = "time" ;  
>                 time:standard_name = "time" ;  
>                 time:units = "hours since 2020-07-02 00:00:00+00:00" ;  
>         float ref_time(ref_time) ;  
>                 ref_time:long_name = "forecaset reference time" ;  
>                 ref_time:standard_name = "forecaset_reference_time" ;  
>                 ref_time:units = "hours since 2020-07-02 00:00:00+00:00" ;  
>         short psea(time, lat, lon) ;  
>                 psea:scale_factor = 0.4587155879 ;  
>                 psea:add_offset = 95000. ;  
>                 psea:long_name = "sea level pressure" ;  
>                 psea:units = "Pa" ;   
>                 psea:standard_name = "air_pressure" ;  
>         short sp(time, lat, lon) ;   
>                 sp:scale_factor = 0.9174311758 ;  
>                 sp:add_offset = 80000. ;  
>                 sp:long_name = "surface air pressure" ;  
>                 sp:units = "Pa" ;  
>                 sp:standard_name = "surface_air_pressure" ;  
>         short u(time, lat, lon) ;  
>                 u:scale_factor = 0.006116208155 ;  
>                 u:add_offset = 0. ;  
>                 u:long_name = "eastward component of wind" ;  
>                 u:units = "m/s" ;  
>                 u:standard_name = "eastward_wind" ;  
>         short v(time, lat, lon) ;  
>                 v:scale_factor = 0.006116208155 ;  
>                 v:add_offset = 0. ;  
>                 v:long_name = "northward component of wind" ;  
>                 v:units = "m/s" ;  
>                 v:standard_name = "northward_wind" ;  
>         short temp(time, lat, lon) ;  
>                 temp:scale_factor = 0.002613491379 ;  
>                 temp:add_offset = 255.4004974 ;  
>                 temp:long_name = "temperature" ;  
>                 temp:units = "K" ;  
>                 temp:standard_name = "air_temperature" ;  
>         short rh(time, lat, lon) ;  
>                 rh:scale_factor = 0.002293577883 ;  
>                 rh:add_offset = 75. ;  
>                 rh:long_name = "relative humidity" ;  
>                 rh:units = "%" ;  
>                 rh:standard_name = "relative_humidity" ;  
>         short r1h(time, lat, lon) ;  
>                 r1h:scale_factor = 0.006116208155 ;  
>                 r1h:add_offset = 200. ;  
>                 r1h:long_name = "rainfall in 1 hour" ;  
>                 r1h:units = "mm/h" ;  
>                 r1h:standard_name = "rainfall_rate" ;  
>         short ncld_upper(time, lat, lon) ;  
>                 ncld_upper:scale_factor = 0.001666666591 ;  
>                 ncld_upper:add_offset = 50. ;  
>                 ncld_upper:long_name = "upper-level cloudiness" ;  
>                 ncld_upper:units = "%" ;  
>         short ncld_mid(time, lat, lon) ;  
>                 ncld_mid:scale_factor = 0.001666666591 ;  
>                 ncld_mid:add_offset = 50. ;  
>                 ncld_mid:long_name = "mid-level cloudiness" ;  
>                 ncld_mid:units = "%" ;  
>         short ncld_low(time, lat, lon) ;  
>                 ncld_low:scale_factor = 0.001666666591 ;  
>                 ncld_low:add_offset = 50. ;  
>                 ncld_low:long_name = "low-level cloudiness" ;  
>                 ncld_low:units = "%" ;  
>         short ncld(time, lat, lon) ;  
>                 ncld:scale_factor = 0.001666666591 ;  
>                 ncld:add_offset = 50. ;  
>                 ncld:long_name = "cloud amount" ;  
>                 ncld:units = "%" ;  
>                 ncld:standard_name = "cloud_area_fraction" ;  
>         short dswrf(time, lat, lon) ;  
>                 dswrf:scale_factor = 0.0205 ;  
>                 dswrf:add_offset = 665. ;  
>                 dswrf:long_name = "Downward Short-Wave Radiation Flux" ;  
>                 dswrf:units = "W/m^2" ;  
>                 dswrf:standard_name = "surface_net_downward_shortwave_flux" ;  
>
> // global attributes:    
>                 :Conventions = "CF-1.0" ;  
>                 :history = "created by create_1daync_msm_s.rb  2020-07-03" ;  
> }  




## Xming

### 概要
Windows上でLinuxのGUI (Graphical User Interface)を使えるようにできる。  
<u>ネットワークが重い場合は動作が遅くなる</u>ので，その場合は使うのを避ける  

https://ja.osdn.net/projects/sfnet_xming/




## GrADS
### 概要
気象・海洋学で多用されている作図ソフト       　　
ある程度の解析もできる  

#### 利点 
- 手軽につかえる  
- 数多くの既成コマンドが活用できる  
#### 欠点
内部の動作が確認できない  

- 操作ミスを発見しにくい 

- いつ頃処理が終わりそうか読めない

  

### GrADS初めの一歩

#### 準備  

**FortiClientを起動  　**　    

**Rlogin起動**  　　　　

サーバーに接続　  　

オプション設定→プロトコル→ポートフォワード→X11ポートフォワードを使用するにチェックを入れる    

**Xmingを起動**  

Windowsのスタート→ X → Xming → XLaunch    

Start no Client　→  Clipboadにチェック入れたまま, 次へ　→  完了    



#### GrADSの起動
Rloginで,
```
$ grads -p
```
> Grid Analysis and Display System (GrADS) Version 2.2.1    
> 
> Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile    
> 
>GX Package Initialization: Size = 8.5 11     



#### シェルとプロンプトについて

##### シェル

https://eng-entrance.com/linux-shell

ユーザーからの命令を受け取って，OSに伝える役割をするインターフェースとなるソフトウェア

シェルにはいくつか種類がある。当研究室では最も一般的なbashを使っている。

##### `プロンプト`

https://e-words.jp/w/%E3%83%97%E3%83%AD%E3%83%B3%E3%83%97%E3%83%88.html

コマンド入力待ちの状態であることを示す記号

GrADSのプロンプト

```
ga->
```

 Bashのプロンプト

```
$
```

##### 注意

bashのプロンプト`$`が表示されているときは，bashのコマンドが使用可能であるが，GrADSのコマンドは使用できない。

GrADSのプロンプト`ga->`が表示されているときは，GrADSのコマンドが使用可能であるが，bashのコマンドは<u>そのままでは</u>使用できない



#### GrADS上でbashのコマンドを使用する方法

!に続けてbashコマンド名

例：

```
ga-> !ls
```



#### GrADSの終了
```
ga-> exit
```

> GX Package Terminated 



#### GrADSの起動

Rloginで,

```
$ grads -p
```



#### データを開く

```
ga-> sdfopen /work01/DATA/MSM/MSM-S/2020/0702.nc
```
> Scanning self-describing file:  /work01/DATA/MSM/MSM-S/2020/0702.nc  
> SDF file /work01/DATA/MSM/MSM-S/2020/0702.nc is open as file 1  
> LON set to 120 150   
> LAT set to 22.4 47.6   
> LEV set to 0 0   
> Time values set: 2020:7:2:0 2020:7:2:0   
> E set to 1 1  



#### データの格子と変数に関する情報を表示する
```
ga-> q ctlinfo
```

> dset /work01/DATA/MSM/MSM-S/2020/0702.nc  
> title   
> undef 9.96921e+36  
> dtype netcdf  
> xdef 481 linear 120 0.0625  
> ydef 505 levels 22.4 22.45 22.5 22.55 22.6 22.65 22.7 22.75  
> 22.8 22.85 22.9 22.95 23 23.05 23.1 23.15 23.2 23.25  
> 
> 47.3 47.35 47.4 47.45 47.5 47.55 47.6  
> zdef 1 linear 0 1  
> tdef 24 linear 00Z02JUL2020 60mn  
> vars 12  
> psea=>psea  0  t,y,x  sea level pressure  
> sp=>sp  0  t,y,x  surface air pressure  
> u=>u  0  t,y,x  eastward component of wind  
> v=>v  0  t,y,x  northward component of wind  
> temp=>temp  0  t,y,x  temperature  
> rh=>rh  0  t,y,x  relative humidity  
> r1h=>r1h  0  t,y,x  rainfall in 1 hour  
> ncld_upper=>ncld_upper  0  t,y,x  upper-level cloudiness  
> ncld_mid=>ncld_mid  0  t,y,x  mid-level cloudiness  
> ncld_low=>ncld_low  0  t,y,x  low-level cloudiness  
> ncld=>ncld  0  t,y,x  cloud amount  
> dswrf=>dswrf  0  t,y,x  Downward Short-Wave Radiation Flux  
> endvars      



#### 現在の次元を表示する

いつ，どこの値が処理対象となっているか
```
ga-> q dims
```
> Default file number is: 1   
> X is varying   Lon = 120 to 150   X = 1 to 481  
> Y is varying   Lat = 22.4 to 47.6   Y = 1 to 505  
> Z is fixed     Lev = 0  Z = 1  
> T is fixed     Time = 00Z02JUL2020  T = 1  
> E is fixed     Ens = 1  E = 1  



#### 等値線図の作図

```
ga-> d psea
```
> Contouring: 100000 to 101800 interval 200   

知識: 等値線のことをコンター (contour)ということがある



#### 画面の消去

```
ga-> cc
```



#### 再描画

psesaの単位がパスカルなので，ヘクトパスカルに変換して作図

```
ga-> d psea/100
```

> Contouring: 1000 to 1018 interval 2 



#### 等値線間隔の変更

```
ga-> set gxout contour
ga-> set cint 4 
cint = 4 
ga-> d psea/100
```

> Contouring: 1000 to 1016 interval 4



#### 特定の値の等値線を引く

```
ga-> cc
ga-> set clevs 1000 1001
Number of clevs = 2 
ga-> d psea/100
```

> Contouring at clevs =  1000 1001



#### 等値線のラベルを消す

```
ga-> cc
ga-> set clab off
ga-> d psea/100
```

> Contouring: 1000 to 1018 interval 2 



#### 等値線の文字の大きさを変更する

```
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```

> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

set clopts 色番号 [太さ [大きさ]]

```
ga-> d psea/100
```

> Contouring: 1000 to 1018 interval 2 



#### 等値線の文字を入れる間隔を変更する

```
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```

> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

```
ga-> set clskip 2
ga-> d psea/100
```

> Contouring: 1000 to 1018 interval 2 



#### ベクトル図

```
ga-> cc
ga-> set gxout vector
ga-> d u;v
```

ベクトルの本数が多すぎて (5km間隔にデータがある)，真っ黒になる 



#### ベクトルを間引く

```
ga-> cc
ga-> d skip(u,20,20);v
```



#### カラーシェードを書く

```
ga-> set gxout shade2
ga-> d psea/100
Contouring: 1000 to 1018 interval 2 
ga-> cbarn
```



#### 色を変える

```
ga-> cc
ga-> color 1000 1018 2 -kind blue->white->red
```

> blue->white->red  
> clevs= 1000 1002 1004 1006 1008 1010 1012 1014 1016 1018  
> ccols= 16 17 18 19 20 21 22 23 24 25 26  

```
ga-> d psea/100
```

> Contouring at clevs =  1000 1002 1004 1006 1008 1010 1012 1014 1016 1018  

```
ga-> cbarn
```

colorコマンドについては下記参照 (grads colorで検索するとヒットする)  

http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script/color.gs?lang=jp   

色見本については下記も参照  

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/GRADS_TIPS.md  



#### 色分けの間隔を指定する

```
ga-> cc
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
```

> white->darkblue->green->yellow->orange->darkred  
> clevs= 1000 1001 1002 1003 1004 1005 1006 1010  
> ccols= 16 17 18 19 20 21 22 23 24    

```
ga-> d psea/100
```

> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010  

```
ga-> cbarn
```



#### カラーシェードと等値線の重ね合わせ

```
ga-> cc
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
```

> white->gold->orange  
> clevs= 1000 1001 1002 1003 1004 1005 1006 1010  
> ccols= 16 17 18 19 20 21 22 23 24  

```
ga-> d psea/100
```

> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010  

```
ga-> cbarn
ga-> set gxout contour
ga-> set cint 1
```

> cint = 1

```
ga-> d psea/100
```

> Contouring: 1000 to 1019 interval 1  

 

**図を重ねるときの注意**  

軸ラベルの文字が汚くなるので，一枚目の図を書き終わった後に,  

```
ga-> set xlab off
ga-> set ylab off
```

を入れる。

例:

```
ga-> cc
ga-> set xlab on
ga-> set ylab on
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
ga-> d psea/100
ga-> set xlab off
ga-> set ylab off
ga-> set gxout contour
ga-> set cint 1
ga-> d psea/100
```



#### 図をファイルに書き出す

##### epsファイルに書き出す

```
ga-> gxprint FIG.eps
```

#### pngファイルに書き出す

```
ga-> gxprint FIG.png
```

注: FIGの部分は自由に変えてよい

図を後から加工したり，高画質の図が必要な場合epsで書き出した方がよい（irfan viewを使うと，後からpngに変換できる）



#### 終了

```
ga-> exit
```



###  

### GrADSはじめの一歩（続き）

#### サーバに接続

FortiClientを起動    

Rloginを起動   



#### Xmingを起動
Windowsのスタート→ X → Xming → XLaunch  

Start no Client　→  Clipboadにチェック入れたまま, 次へ　→  完了  



#### GrADSの起動
Rloginで,
```
$ grads -p
```



#### ファイルを開く

```
ga-> sdfopen /work01/DATA/MSM/MSM-S/2020/0702.nc
```



#### 海面気圧の等値線

```
ga-> cc
ga-> d psea/100
```



### 描画範囲の指定

```
ga-> set lon 132 144
```

> LON set to 132 144     

```
ga-> set lat 36 45
```

>   LAT set to 36 45   

```
ga-> cc
ga-> d psea/100
```



### 定点の時系列

```
ga-> set lon 137
```

> LON set to 137 137 　　  

```
ga-> set lat 40
```

> LAT set to 40 40   

**ポイント：緯度・経度は固定しておく**

```
ga-> set time 00ZJUL2020 23ZJUL2020
```

> Time values set: 2020:7:2:0 2020:7:2:23 

```
ga-> cc
ga-> d psea/100
```



#### データ範囲の確認

```
ga-> q dims
```

> Default file number is: 1  
> X is fixed     Lon = 137  X = 273  
> Y is fixed     Lat = 40  Y = 353  
> Z is fixed     Lev = 0  Z = 1  
> T is varying   Time = 00Z02JUL2020 to 23Z02JUL2020  T = 1 to 24  
> E is fixed     Ens = 1  E = 1  



#### 領域平均の時系列

```
ga-> PAAV=aave(psea/100,lon=130,lon=140,lat=32,lat=50)
```

> Define memory allocation size = 192 bytes  

```
ga-> d PAAV
```



領域平均と定点の値が異なることを確認

```
ga-> q dims
```

Default file number is: 1  
X is fixed     Lon = 137  X = 273  
Y is fixed     Lat = 40  Y = 353  
Z is fixed     Lev = 0  Z = 1  
T is varying   Time = 00Z02JUL2020 to 23Z02JUL2020  T = 1 to 24  
E is fixed     Ens = 1  E = 1  

定点の時系列を重ねて書く

```
ga-> set xlab off
ga-> set ylab off
ga-> d psea/100
```



#### 時間平均の平面図

```
ga-> reset
```

> LON set to 120 150 　　
> LAT set to 22.4 47.6 　　
> LEV set to 0 0 　　　
> Time values set: 2020:7:2:0 2020:7:2:0 　　
> E set to 1 1 　　
> Most GrADS attributes have been reset　　

```
ga-> set t 1
```

> Time values set: 2020:7:2:0 2020:7:2:0

**ポイント：時刻は固定しておく**

```
 ga-> PTAV=ave(psea/100,time=00Z02JUL2020,time=23Z02JUL2020)
```

> Averaging.  dim = 3, start = 1, end = 24　　
> Define memory allocation size = 1943240 bytes　　

```
ga-> d PTAV
```

> Contouring: 1006 to 1018 interval 1 　　



### 時間平均からの偏差

```
ga-> set time 00Z02JUL2020
```

> Time values set: 2020:7:2:0 2020:7:2:0  

```
ga-> PTAN=psea/100-PTAV
```

Define memory allocation size = 1943240 bytes  

```
ga-> cc
ga-> color -6 6 1 -kind blue->white->red
```

> blue->white->red
> clevs= -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6
> ccols= 16 17 18 19 20 21 22 23 24 25 26 27 28 29

```
ga-> d PTAN
```

> Contouring at clevs =  -6 -5 -4 -3 -2 -1 0 1 2 3 4 5 6

```
ga-> cbarn
```

　　

#### 偏差に時間平均の等値線を重ねる

```
ga-> set xlab off
ga-> set ylab off
ga-> set gxout contour
ga-> d PTAV
```

> Contouring: 1006 to 1018 interval 1 



#### 図にタイトルを入れる

```
ga-> draw title PSEA 00Z02JUL2020
```



#### 図にヘッダーを入れる

```
ga-> q gxinfo
```

> Last Graphic = Contour
> Page Size = 8.5 by 11
> X Limits = 0.5 to 8
> Y Limits = 1.72 to 9.28
> Xaxis = Lon  Yaxis = Lat
> Mproj = 2

```
ga-> set strsiz 0.12 0.14
```

> SET STRSIZ values:  hsize = 0.12  vsize = 0.14

```
ga-> set string 1 l 2 0
```

> SET STRING values:  color = 1  just = l  thickness = 2  rotation = 0

```
ga-> draw string 0.2  10 HEADER LINE
```





#### 図をファイルに書き出す

##### epsファイルに書き出す

```
ga-> gxprint PSEA.eps
```

##### pngファイルに書き出す

```
ga-> gxprint PSEA.png
```

注: FIGの部分は自由に変えてよい

図を後から加工したり，高画質の図が必要な場合epsで書き出した方がよい（irfan viewを使うと，後からpngに変換できる）

##### 作成した図の情報を得る

```
ga-> !ls -lh PSEA.*
```



#### 終了

```
ga-> exit
```





### 情報源

- GrADS講座  
  http://akyura.sakura.ne.jp/study/GrADS/kouza/grads.html  
  
- はじめてのGrADS  
  https://sites.google.com/site/afcgrads/  
  
- 東北大学大学院理学研究科 流体地球物理学講座 公開情報/GrADS  
  http://wind.geophys.tohoku.ac.jp/index.php?%B8%F8%B3%AB%BE%F0%CA%F3/GrADS  
  
- GrADS-Note  
  http://seesaawiki.jp/ykamae_grads-note/  
  
- 研究室作成のマニュアル
  https://gitlab.com/infoaofd/lab/-/tree/master/GRADS
  
- スクリプト集  
  http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script?lang=jp  
  
- GrADS コマンドリファレンス  
  http://akyura.sakura.ne.jp/study/GrADS/Command/  

- IT memo  
  http://hydro.iis.u-tokyo.ac.jp/~kei/?IT%20memo%2FGrADS%20memo  

- GrADS リファレンスマニュアル  
  http://mausam.hyarc.nagoya-u.ac.jp/%7Ehatsuki/grads/gradsmanu.html  

- Bin Guan's GrADS Script Library  
  http://bguan.bol.ucla.edu/bGASL.html  

- GrADS Functions (chapter 10)  
  https://www.lmd.jussieu.fr/~lmdz/grads/ga10.htm  

- GrADS Documentation  
  http://cola.gmu.edu/grads/gadoc/gadoc.php  


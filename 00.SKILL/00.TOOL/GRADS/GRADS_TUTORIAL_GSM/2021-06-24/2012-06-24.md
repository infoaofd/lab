# スクリプトの書き換え方  

## 練習用データのダウンロード   

ブラウザーを開く  
生存研 GSM    
でググる  
  
気象庁データ: オリジナルデータ: 数値予報GPV  
http://database.rish.kyoto-u.ac.jp/arch/jmadata/gpv-original.html  
  
数値予報GPV  
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/  
  
GPVデータの説明  
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/README.pdf   
  
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/2021/06/23/
  
ダウンロードしたいファイルの「リンクのアドレスをコピー」
  
Rloginのウィンドウに移る
gu1@p5820  
/work03/gu1/2021-06-24_14  
2021-06-24_18-38  
```
$ cd /work01/DATA/GSM/2021/  
```
```
$ wget http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/original/2021/06/23/Z__C_RJTD_20210623180000_GSM_GPV_Rgl_FD0000_grib2.bin
```
```
$ ll Z__C_RJTD_20210623180000_GSM_GPV_Rgl_FD0000_grib2.bin
-rw-rw-r--. 1 am 32M 2021-06-24 06:32 Z__C_RJTD_20210623180000_GSM_GPV_Rgl_FD0000_grib2.bin
```

## 作業ディレクトリへ移動
```
$ cd $HOME
```
```
$ cd 2021-06-24_14/
```
  
## grib2ファイルをGrADSで読めるようにする
grib2ファイルからコントロール (CTL) ファイルを作成する  
  
CTLファイルについて  
http://akyura.sakura.ne.jp/study/GrADS/kouza/grads.html#section_3  

```
$ g2ctl /work01/DATA/GSM/2021/Z__C_RJTD_20210623180000_GSM_GPV_Rgl_FD0000_grib2.bin>gsm.ctl
```
```
$ ll *.ctl
-rw-r--r--. 1 gu1 2.0K 2021-06-24 18:51 gsm.ctl
```
```
$ gribmap -0 -i gsm.ctl
gribmap: scanning GRIB2 file: /work01/DATA/GSM/2021/Z__C_RJTD_20210623180000_GSM_GPV_Rgl_FD0000_grib2.bin 
gribmap: Writing out the GRIB2 index file (version 3)
```
  
## 作図
```
$ vi plot.gsm.sh
```
```
GS=$(basename $0 .sh).GS

LON="100 160"
LAT="20 55"
LEV="850"

cat <<EOF>$GS
'open gsm.ctl'
'q ctlinfo'
say result

'set lon $LON'
'set lat $LAT'
'set lev $LEV'
'q dims'
say result

'd TMPprs'

'gxprint TMPprs.eps'

'quit'
EOF

grads -bcp "$GS"
```
CTL+Z
```
$ bash plot.gsm.sh
```
```
$ ll TMPprs.eps 
-rw-r--r--. 1 gu1 190K 2021-06-24 19:05 TMPprs.eps
```
再びplot.gsm.shを編集する場合  
```
$ fg
```
## スクリプトの書き換え方のポイント
1. ひな型となるスクリプトを探す  
2. ひな型の一部を抜き出した短いスクリプトを作成してみる

- 入力部
- 実行に最低限必要な箇所
- 描画部
- ファイルに書き出す部分

3. ひな型に少しづつコマンドを追加して機能を調べる
4. コマンドの名前をググって,  コマンドの意味・使用法を調べる
5. 3に戻る

## ひな型が記載されているサイトの例
https://gitlab.com/infoaofd/lab/-/blob/master/MEETING/2021/2021-05-20_LAB.md  
  
https://gitlab.com/infoaofd/lab/-/blob/master/MEETING/2021/2021-05-06_02_LAB.md  
  
https://gitlab.com/infoaofd/lab/-/blob/master/MEETING/2021/2021-06-10_LAB/2021-06-10_LAB.md  
  
https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A4%C7%A4%E8%A4%AF%BB%C8%A4%A6%CA%B8%A4%DE%A4%C8%A4%E1    
https://gradsaddict.blogspot.com/p/grads-script-library.html  
  

## コマンド・スクリプトの文法の解説
http://wind.gp.tohoku.ac.jp/index.php?%B8%F8%B3%AB%BE%F0%CA%F3/GrADS/GrADS%A4%CETips  
  
http://wind.gp.tohoku.ac.jp/index.php?%B8%F8%B3%AB%BE%F0%CA%F3/GrADS/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A4%CETips  

http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script?lang=jp  

https://seesaawiki.jp/ykamae_grads-note/  
  
http://isotope.iis.u-tokyo.ac.jp/~kei/?IT%20memo%2FGrADS%20memo  
  
https://www.sci.hokudai.ac.jp/grp/poc/top/old/software/other/grads_tips/index.htm　　

※「コマンド名, 「やりたいこと GrADS」でググった方が早く必要な情報が見つかる場合が多い  
  
## 参考資料
grib2データ  
https://www.data.jma.go.jp/add/suishin/jyouhou/pdf/205.pdf    
　　 
g2ctl   
https://www.cpc.ncep.noaa.gov/products/wesley/g2ctl.html  
  
gribmap  
http://cola.gmu.edu/grads/gadoc/gradutilgribmap.html 


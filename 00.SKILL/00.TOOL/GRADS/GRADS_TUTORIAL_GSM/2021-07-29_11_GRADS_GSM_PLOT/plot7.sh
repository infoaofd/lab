GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh).eps

LONW=138
LATS=20;  LATN=50
LEV=850
TIME0=00Z02JUL2021
TIME1=18Z03JUL2021

KIND='cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
LEVS='320 350 5'

cat <<EOF>$GS
'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $TIME0 $TIME1'

'q dims'
say result

'cc'

'temp=tmpprs'
'rh=rhprs'

say '### EPT (Bolton 1980)'

'tc=(temp-273.15)'

'es= 6.112*exp((17.67*tc)/(tc+243.5))'         ;# Eq.10 of Bolton (1980)

'e=0.01*rh*es'                                 ;# Eq.4.1.5 (p. 108) of Emanuel (1994)

'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))' ;# Inverting Eq.10 of Bolton since es(Td)=e

'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56' ;#Eq.15 of Bolton (1980)

'mixr= 0.62197*(e/(lev-e))*1000'              ;# Eq.4.1.2 (p.108) of Emanuel(1994) 

'TDL=temp*pow(1000/(lev-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'

'EPT=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))' ;#Eq.39 of Bolton

say '### END EPT (Bolton 1980)'

'color ${LEVS} -kind ${KIND} -gxout shaded'
'd EPT'



say '### GET COORDINATES OF 4 CORNERS'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

say '### COLOR BAR'
x1=xl; x2=xr
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'



say '### GEOPOTENTIAL HEIGHT'
'set gxout contour'
'set cint 20'
'd HGTprs'
say '### END GEOPOTENTIAL HEIGHT'

'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"

ls -lh $FIG


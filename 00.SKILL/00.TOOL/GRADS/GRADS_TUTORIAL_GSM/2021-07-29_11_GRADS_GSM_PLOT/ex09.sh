#!/bin/bash

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).png
#FIG=$(basename $0 .sh).eps

LONW=134 ;* LONE=160
LATS=28  ;* LATN=42
LEV1=1000;LEV2=300
TIME0=12Z03JUL2021

cat <<EOF>$GS


'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW' ;# $LONE'
'set lat $LATS' ;# $LATN'
'set lev $LEV1 $LEV2'
'set time $TIME0'

'q dims'
say result

'cc'
'set grid off'

say '### DEWPOINT'
'temp=TMPprs'

'rh=RHprs'

'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'         ;# Eq.10 of Bolton (1980)


'e=0.01*rh*es'                                 ;# Eq.4.1.5 (p. 108) of Emanuel (1994)


'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))' ;# Inverting Eq.10 of Bolton since es(Td)=e

'dwpk=td+273.15'


say '### SKEW-T/LOG-P DIAGRAM'
'wspd=mag(UGRDprs,VGRDprs)'


'wdir=57.3*atan2(UGRDprs,VGRDprs)+180'

rc=plotskew(tc,td,wspd,wdir)

'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

say '### TITLE'
'q time'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 4 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'ctime1' $LATS $LONW'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint $FIG'

'quit'

#$(cat /usr/local/grads-2.2.1/scripts/plotskew.gs)
$(cat ./plotskew.gs)
EOF

grads -bcp "$GS"
#rm -vf $GS

ls -lh $FIG


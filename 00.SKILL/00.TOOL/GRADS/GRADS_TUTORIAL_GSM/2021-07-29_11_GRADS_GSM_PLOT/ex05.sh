HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).png
#FIG=$(basename $0 .sh).eps

LONW=90; LONE=160
LATS=15;  LATN=55
LEV1=925
TIME0=12Z03JUL2021
UNIT="g/kg"

KIND='midnightblue->deepskyblue->lightcyan->wheat->orange->red->crimson'
LEVS='0 22 2'

cat <<EOF>$GS
'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set time $TIME0'

'q dims'
say result

'cc'
'set grid off'

say '### EPT'
'set lev $LEV1'
'temp=tmpprs'
'rh=rhprs'

say '### QV'

'tc=(temp-273.16)'
'td=tc-( (14.55+0.114*tc)*(1-0.01*rh) + pow((2.5+0.007*tc)*(1-0.01*rh),3) + (15.9+0.117*tc)*pow((1-0.01*rh),14) )'
'vapr= 6.112*exp((17.67*td)/(td+243.5))'
'e= vapr*1.001+(lev-100)/900*0.0034'
'QV = 0.62197*(e/(lev-e))'
'QV = QV*1000'

say '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'd QV'

say '### CONTOUR'
'set gxout contour'

'set cmax 300'
'set cmin 300'
'set cmax 370'
'set cint 5'
'set clab off'
#'set clab on'
#'set clskip 2'
#'set clopts 1 2 0.12'
'set ccolor 0'
'set cthick 1'
'd QV'




say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'
'set ylab off'



say '### WIND'
'set lev $LEV1'
'set gxout barb'
'set cthick 10'
'set ccolor  0'
'vec.gs skip(UGRDprs,5,7);VGRDprs -SCL 0.7 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65
yy=yb-0.35
'set cthick  3'
'set ccolor  1'
'vec.gs skip(UGRDprs,5,7);VGRDprs -SCL 0.7 20 ' ;# -P 'xx' 'yy' -SL m/s'




say '### TITLE'
'q time'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'ctime1' QV&V$LEV1'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG


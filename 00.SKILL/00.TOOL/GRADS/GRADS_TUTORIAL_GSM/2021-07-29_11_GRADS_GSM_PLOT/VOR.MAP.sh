#!/bin/bash

Y=2020; MM=07; MMM=JUL; DD=02; HH=00
PLEV1=950
LONW=120; LONE=140
LATS=26 ; LATN=42

TIME=${HH}Z${DD}${MMM}${Y}

POW=4
FAC=1E${POW}
SCL=10\`a-${POW}\`n
# https://www.sci.hokudai.ac.jp/grp/poc/top/old/software/other/grads_tips/index.htm

UNIT="${SCL}s\`a-1\`n"

VAR=VOR
TITLE="${VAR} $TIME ${PLEV1}hPa"

INDIR=/work01/DATA/MSM/MSM-P
INFLE=${INDIR}/$Y/${MM}${DD}.nc

PREFIX=$(basename $0 .sh)
FIG=${PREFIX}_${Y}${MM}${DD}_${HH}_${PLEV1}.eps

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"

GS=$(basename $0 .sh).GS

# GRADS SCRIPT GOES HERE:
cat <<EOF>$GS
say

'sdfopen $INFLE'

'q ctlinfo 1'

'set time ${TIME}'
'set lev $PLEV1'
'set lon $LONW $LONE'
'set lat $LATS $LATN'

'cc'
'set grads off'
'set grid off'
'set mpdset hires'



'set vpage 0.0 8.5 0.0 11'

xs=1; xe=6
ys=3; ye=10
'set parea 'xs ' 'xe' 'ys' 'ye

'VOR=hcurl(u,v)*${FAC}'


'set xlab on'
'set ylab on'
'set xlint 4'
'set ylint 2'

say '### COLOR SHADE'
'color -6 6 0.5 -kind cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'

'd VOR'

'set xlab off'
'set ylab off'


say '### VECTOR'
'set gxout vector'
'set cthick 10'
'set ccolor  0'
'vec.gs skip(u,10);v -SCL 0.5 30 -P 20 20 -SL m/s'

# https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A5%E9%A5%A4%A5%D6%A5%E9%A5%EA#content_3_26

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65
yy=yb-0.35
'set cthick  2'
'set ccolor  1'
'vec.gs skip(u,10);v -SCL 0.5 30 -P 'xx' 'yy' -SL m/s'

say '### COLOR BAR'
x1=xr+0.4; x2=x1+0.1
y1=yb    ; y2=yt-0.5
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.12 -fh 0.16 -ft 4 -fs 2'

say '### SCALE & UNITS'
'set strsiz 0.12 0.16'
'set string 1 c 4 0'
xx=(x1+x2)/2
yy=y2+0.2
'draw string ' xx ' ' yy ' ${UNIT}'

say '### TITLE'
'set strsiz 0.12 0.14'
'set string 1 c 4 0'
xx = (xl+xr)/2; yy=yt+0.2
'draw string ' xx ' ' yy ' ${TITLE}'

say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yy+0.5
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint $FIG'
say
'!ls -lh $FIG'
say
'quit'
EOF

grads -bcp "$GS"

HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).png
#FIG=$(basename $0 .sh).eps

LONW=90; LONE=160
LATS=15;  LATN=55
LEV1=925; LEV2=850
TIME0=12Z03JUL2021
UNIT="K"

KIND='darkblue->deepskyblue->skyblue->white->white->gold->orange->crimson'
LEVS='-15 15 1'

cat <<EOF>$GS
'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set time $TIME0'

'q dims'
say result

'cc'
'set grid off'

say '### EPT'

'set lev $LEV1 $LEV2'
'temp=tmpprs'
'rh=rhprs'

say '### EPT (Bolton 1980)'

'tc=(temp-273.15)'
'es= 6.112*exp((17.67*tc)/(tc+243.5))'         ;# Eq.10 of Bolton (1980)
'e=0.01*rh*es'                                 ;# Eq.4.1.5 (p. 108) of Emanuel (1994)
'td=(243.5*log(e/6.112))/(17.67-log(e/6.112))' ;# Inverting Eq.10 of Bolton since es(Td)=e
'dwpk= td+273.15'
'Tlcl= 1/(1/(dwpk-56)+log(temp/dwpk)/800)+56' ;#Eq.15 of Bolton (1980)
'mixr= 0.62197*(e/(lev-e))*1000'              ;# Eq.4.1.2 (p.108) of Emanuel(1994) 
'TDL=temp*pow(1000/(lev-e),0.2854)*pow(temp/Tlcl, 0.28*0.001*mixr)'
'EPT=TDL*exp((3.036/Tlcl-0.00178)*mixr*(1.0+0.000448*mixr))' ;#Eq.39 of Bolton


say '### COLOR SHADE'
'color ${LEVS} -kind ${KIND} -gxout shaded'
'set lev $LEV1'
'DEPT=EPT(lev=$LEV1)-EPT(lev=$LEV2)'
'd DEPT'

say '### CONTOUR'
'set gxout contour'

'set cmax 300'
'set cmin 300'
'set cmax 370'
'set cint 5'
'set clab off'
#'set clab on'
#'set clskip 2'
#'set clopts 1 2 0.12'
'set ccolor 0'
'set cthick 1'
'd DEPT'




say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'
'set ylab off'



say '### TITLE'
'q time'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'ctime1' EPT(${LEV1}-${LEV2})'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG


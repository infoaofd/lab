HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).png
#FIG=$(basename $0 .sh).eps

LONW=90; LONE=160
LATS=15;  LATN=55
LEV1=700; LEV2=850
TIME0=12Z03JUL2021
UNIT="[hPa/h]" 

KIND='darkred->orange->white->skyblue->darkblue'
LEVS='-80 80 5'

cat <<EOF>$GS
'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $TIME0'

'q dims'
say result

'cc'

say '### VVELprs'
'set lev $LEV1'
'color ${LEVS} -kind ${KIND} -gxout shaded'

'set map 1 1 3'
'set xlint 10'
'set ylint 5'
'set grid off'

'd VVELprs/100*3600'

say '### COLOR LABEL BAR'
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

x1=xl; x2=xr-1
y1=yb-0.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 2 -ft 3 -line on -edge circle'

x=x2+0.5; y=(y1+y2)*0.5
'set string 1 c 3 0'
'set strsiz 0.15 0.18'
'draw string 'x' 'y' ${UNIT}'

'set xlab off'
'set ylab off'



say '### TMPprs'
'set lev $LEV2'
'TC=TMPprs-273.15'
'set gxout contour'
'set clab off' ;#LABEL OFF
'set cthick 10';#THICK CONTOUR
'set ccolor 0' ;#WHITE
'set cint 2'   ;#CONTOUR INTERVAL
'set cmin -20'
'set cmax  20'
'd TC'

'set clab on'
'set cthick 1'
'set ccolor 1' ;#BLACK
'set cint 2'   ;#CONTOUR INTERVAL
'set cmin -20'
'set cmax  20'
'set clskip 2'
'd TC'



say '### WIND'
'set lev $LEV2'
'set gxout barb'
'set cthick 10'
'set ccolor  0'
'vec.gs skip(UGRDprs,10);VGRDprs -SCL 0.7 20 -P 20 20 -SL m/s'
'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)

xx=xr-0.65
yy=yb-0.35
'set cthick  3'
'set ccolor  1'
'vec.gs skip(UGRDprs,10);VGRDprs -SCL 0.7 20 ' ;# -P 'xx' 'yy' -SL m/s'



say '### TITLE'
'q time'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'ctime1' W$LEV1 V&T$LEV2'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG


HOST=$(hostname);     CWD=$(pwd)
TIMESTAMP=$(date -R); CMD="$0 $@"
GS=$(basename $0 .sh).GS

FIG=$(basename $0 .sh).eps

LONW=90; LONE=160
LATS=15;  LATN=55
LEV=1000
TIME0=12Z03JUL2021

#KIND='cyan->blue->palegreen->azure->white->papayawhip->gold->red->magenta'
#LEVS='320 350 5'

cat <<EOF>$GS
'open sm_t.ctl'
#D 'open gsm_t.ctl'
'q ctlinfo'
say result

'set lon $LONW $LONE'
'set lat $LATS $LATN'
'set lev $LEV'
'set time $TIME0'

'q dims'
say result

'cc'

say '### PRMSLmsl'
'set gxout contour'
'set cmax 1020'
'set cmin 800'
'set cint 4'
'set clskip 2'
'set clopts 1 2 0.12'
'set ccolor 1'
'set cthick 2'

'set map 1 1 3'
'set xlint 10'
'set ylint 5'
'set grid off'
'd PRMSLmsl/100'

'set xlab off'
'set ylab off'



say '### WIND'
#'set gxout vector'
'set gxout barb'

'set cthick 10'
'set ccolor  0'

'vec.gs skip(UGRD10m,10);VGRD10m -SCL 0.7 20 -P 20 20 -SL m/s'

'q gxinfo'
line=sublin(result,3); xl=subwrd(line,4); xr=subwrd(line,6)
line=sublin(result,4); yb=subwrd(line,4); yt=subwrd(line,6)
xx=xr-0.65
yy=yb-0.35

'set cthick  3'
'set ccolor  1'
'vec.gs skip(UGRD10m,10);VGRD10m -SCL 0.7 20' ;# -P 'xx' 'yy' -SL m/s'



say '### TITLE'
'q time'
say result
ctime1=subwrd(result,3)  

hh=substr(ctime1,1,2)
dd=substr(ctime1,4,2) 
month=substr(ctime1,6,3) 
year=substr(ctime1,9,4)  

'set strsiz 0.15 0.18'
'set string 1 c 3 0'
x=(xl+xr)/2; y=yt+0.25
'draw string 'x' 'y' 'ctime1' SURFACE'



say '### HEADER'
'set strsiz 0.12 0.14'
'set string 1 l 2 0'
xx = 0.2; yy=yt+0.7
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${TIMESTAMP}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'


'gxprint $FIG'

'quit'
EOF

grads -bcp "$GS"
rm -vf $GS

ls -lh $FIG


dset /work01/DATA/GSM/%y4/Z__C_RJTD_%y4%m2%d2%h20000_GSM_GPV_Rgl_FD0000_grib2.bin
index /work01/DATA/GSM/2021/Z__C_RJTD_20210702000000_GSM_GPV_Rgl_FD0000_grib2.bin.idx
options template
undef 9.999E+20
title /work01/DATA/GSM/2021/Z__C_RJTD_20210702000000_GSM_GPV_Rgl_FD0000_grib2.bin
* produced by g2ctl v0.1.8
* command line options: /work01/DATA/GSM/2021/Z__C_RJTD_20210702000000_GSM_GPV_Rgl_FD0000_grib2.bin
* griddef=1.1:0:(720 x 361):grid_template=0:winds(N/S): lat-lon grid:(720 x 361) units 1e-06 input WE:NS output WE:SN res 48 lat 90.000000 to -90.000000 by 0.500000 lon 0.000000 to 359.500000 by 0.500000 #points=259920:winds(N/S)

dtype grib2
ydef 361 linear -90.000000 0.5
xdef 720 linear 0.000000 0.500000
tdef   8 linear 00Z02jul2021 6hr
* PROFILE hPa
zdef 17 levels 100000 92500 85000 70000 60000 50000 40000 30000 25000 20000 15000 10000 7000 5000 3000 2000 1000
options pascals
vars 16
HCDCsfc  0,1   0,6,5 ** surface High Cloud Cover [%]
HGTprs    17,100  0,3,5 ** (1000 925 850 700 600.. 70 50 30 20 10) Geopotential Height [gpm]
LCDCsfc  0,1   0,6,3 ** surface Low Cloud Cover [%]
MCDCsfc  0,1   0,6,4 ** surface Medium Cloud Cover [%]
PRESsfc  0,1   0,3,0 ** surface Pressure [Pa]
PRMSLmsl  0,101   0,3,1 ** mean sea level Pressure Reduced to MSL [Pa]
RHprs    8,100  0,1,1 ** (1000 925 850 700 600 500 400 300) Relative Humidity [%]
RH2m   0,103,2   0,1,1 ** 2 m above ground Relative Humidity [%]
TCDCsfc  0,1   0,6,1 ** surface Total Cloud Cover [%]
TMPprs    17,100  0,0,0 ** (1000 925 850 700 600.. 70 50 30 20 10) Temperature [K]
TMP2m   0,103,2   0,0,0 ** 2 m above ground Temperature [K]
UGRDprs    17,100  0,2,2 ** (1000 925 850 700 600.. 70 50 30 20 10) U-Component of Wind [m/s]
UGRD10m   0,103,10   0,2,2 ** 10 m above ground U-Component of Wind [m/s]
VGRDprs    17,100  0,2,3 ** (1000 925 850 700 600.. 70 50 30 20 10) V-Component of Wind [m/s]
VGRD10m   0,103,10   0,2,3 ** 10 m above ground V-Component of Wind [m/s]
VVELprs    17,100  0,2,8 ** (1000 925 850 700 600.. 70 50 30 20 10) Vertical Velocity (Pressure) [Pa/s]
ENDVARS

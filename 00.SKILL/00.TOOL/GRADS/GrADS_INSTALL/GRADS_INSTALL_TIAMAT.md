# GRADS TIAMAT

2024-08-08_10-48

```bash
/work05/$(whoami)/APP/grads-2.2.1
$ ls
bin/  data/  example/  include/  lib/  scripts/  src/
```

```bash
$ grep -n grads $HOME/.bashrc
14:export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/work05/$(whoami)/APP/grads-2.2.1/lib
15:export PATH=$PATH:/work05/$(whoami)/APP/grads-2.2.1/bin
16:export GADDIR=/work05/$(whoami)/APP/grads-2.2.1/data
17:export GASCRP=/work05/$(whoami)/APP/grads-2.2.1/scripts
18:export GAUDPT=/work05/$(whoami)/APP/grads-2.2.1/data/udpt
```

```bash
$ cat data/udpt
# Type     Name     Full path to shared object file
# ----     ----     -------------------------------
gxdisplay  Cairo    /work05/$(whoami)/APP/grads-2.2.1/lib/libgxdCairo.so
gxdisplay  X11      /work05/$(whoami)/APP/grads-2.2.1/lib/libgxdX11.so
gxdisplay  gxdummy  /work05/$(whoami)/APP/grads-2.2.1/lib/libgxdummy.so
*
gxprint    Cairo    /work05/$(whoami)/APP/grads-2.2.1/lib/libgxpCairo.so
gxprint    GD       /work05/$(whoami)/APP/grads-2.2.1/lib/libgxpGD.so
gxprint    gxdummy  /work05/$(whoami)/APP/grads-2.2.1/lib/libgxdummy.so
```






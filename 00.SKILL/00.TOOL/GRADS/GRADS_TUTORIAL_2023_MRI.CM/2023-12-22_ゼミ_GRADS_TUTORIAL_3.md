# ゼミ 2023-12-15

[[_TOC_]]

前回までの

## 今日の概要

気象庁全球結合モデルの結果を作図する練習

## 気象庁全球結合モデル

### 概要

全球大気モデルと全球海洋モデルを、カップラーというソフトを使って結合させたもの。

#### 情報

https://www.jma.go.jp/jma/kishou/know/whitep/1-3-7.html

https://repository.dl.itc.u-tokyo.ac.jp/records/2007330



## 演習

ステップバイステップで目的のスクリプトを作成する。

1TEST.sh 

```bash
#!/bin/bash

YYYYMMDDHH=$1 #スクリプトの引数を日付として読み込む
YYYYMMDDHH=${YYYYMMDDHH:-2016060500} #引数の指定が無い場合既定の値に設定する

echo "DATE/TIME=${YYYYMMDDHH}"

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d${YYYY}/${MM}/${DD})
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" ) #YMD1にYYYY年MM月DD日の2日前の日付を入れる

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/ 
#大気データが保存してあるディレクトリ名
CTLA=atm_snp_1hr_glb.ncctl 
#コントロールファイルの名前
CTL11=${INROOT1}/${YMD1}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
# コントロールファイルが存在しなければエラーメッセージを表示して終了する

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
#海洋データが保存してあるディレクトリ名
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi

GS=$(basename $0 .sh).GS
# 埋め込むGrADSスクリプトの名前。
# 元のシェルスクリプト名の末尾の.shを削除して末尾に.GSをつける
FIG=$(basename $0 .sh)_${YYYYMMDDHH}.PDF
#図のファイルの名前の設定。

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"
# hostnameは使用しているコンピュータの名前を表示するコマンド
# pwdは今自分がいるディレクトリの名前を表示するコマンド (print working direcotry)
# dateは日付と時刻を表示するコマンド(-Rを付けると英語表記になる)
# $0=今自分が使っているシェルスクリプトの名前
# $@=スクリプトのすべての引数

# ここからGrADSのスクリプトの埋め込み
cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL21}'
# コントロールファイルを開く

'quit'
# Gradsの終了
EOF

grads -bcp "$GS"
# 埋め込んだGrADSスクリプトの実行

rm -vf $GS
# 実行したGrADSスクリプトの削除(毎回埋め込むので保存しておく必要はない)
# デバッグの時は残しておくと良いので，その場合この行の先頭に#をつけて
#コメント行にする。
```

```bash
$ 1TEST.sh 
DATE/TIME=2016060500
```

```bash
$ 1TEST.sh 2016060200
DATE/TIME=2016060200
```



2TEST.sh

```bash
#!/bin/bash

YYYYMMDDHH=$1 #スクリプトの引数を日付として読み込む
YYYYMMDDHH=${YYYYMMDDHH:-2016060500} #引数の指定が無い場合既定の値に設定する

echo "DATE/TIME=${YYYYMMDDHH}"

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d${YYYY}/${MM}/${DD})
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" ) #YMD1にYYYY年MM月DD日の2日前の日付を入れる

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/ 
#大気データが保存してあるディレクトリ名
CTLA=atm_snp_1hr_glb.ncctl 
#コントロールファイルの名前
CTL11=${INROOT1}/${YMD1}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
# コントロールファイルが存在しなければエラーメッセージを表示して終了する

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
#海洋データが保存してあるディレクトリ名
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi

GS=$(basename $0 .sh).GS
# 埋め込むGrADSスクリプトの名前。
# 元のシェルスクリプト名の末尾の.shを削除して末尾に.GSをつける
FIG=$(basename $0 .sh)_${YYYYMMDDHH}.PDF
#図のファイルの名前の設定。

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"
# hostnameは使用しているコンピュータの名前を表示するコマンド
# pwdは今自分がいるディレクトリの名前を表示するコマンド (print working direcotry)
# dateは日付と時刻を表示するコマンド(-Rを付けると英語表記になる)
# $0=今自分が使っているシェルスクリプトの名前
# $@=スクリプトのすべての引数

KIND1='-kind white->lightgray->gray->dimgray->black'
LEVS1='0 2 0.2'; UNIT1="0.1 g/kg"
# 図の色見本(カラーバー)の設定
# LEVS1は, 最低値, 最大値, 間隔の順に数値を指定する。

KIND2='-kind midnightblue->deepskyblue->green->wheat->orange->red->magenta'
LEVS2='1 30 1'; UNIT2="\`ao\`nC"

# ここからGrADSのスクリプトの埋め込み
cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL21}'
# コントロールファイルを開く

'set rgb 50 127 73 45'
# 海岸線をこげ茶で書くために, こげ茶のRGB値(127 73 45)を
# 色番号50番として保存


xmax = 6; ymax = 2 
# 図の中に入れるパネル(1枚の図の中に入れる小さい図のこと)の数
# xmax=図の横方向のパネルの数, ymax=図の縦方向のパネルの数
ytop=7
# ytop=図の上端を紙の度の高さに持ってくるか

#図の中に入れる各パネル(1枚の図の中に入れる小さい図のこと)の大きさの指定
xwid = 10.5/xmax; ywid = 5.0/ymax

# 各パネル間の余白
xmargin=0.3; ymargin=0.5

'cc'
# 画面を白色にする

# ここから実際に画面の大きさを決めていく
'set vpage 0.0 11 0.0 8.5'
# vpageで図の外枠の大きさを決める
# 横方向が0から11インチ, 縦方向が0から8.5インチ

# ここからパネルを一枚一枚書いていく
nmap = 1; ymap = 1; xmap = 1
# nmap=パネルの通し番号
# ymap=縦方向のパネルの番号 (行番号）
# xmap＝横方向のパネルの番号(列番号)

while (xmap <= 1 & nmap <=5)

#ここで各パネルの左端 (xs), 右端(xe)，上端(ye), 下端(ys)の座標を決める
xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
#縦方向(y方向）に関してはｙ座標の大きい上から書いていくので引き算している
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# ここで1枚のパネルの左端 (xs), 右端(xe)，上端(ye), 下端(ys)の座標を設定している
'set parea 'xs ' 'xe' 'ys' 'ye

# グラフの中に縦線と横線を入れない (grid off)
# 図の下にこのソフトの名称(GrADS)などのメッセージを入れない
'set grid off';'set grads off'

xmap=xmap+1; nmap=nmap+1
# パネルの番号を更新

endwhile #xmap
# while文の終了（1枚のパネルの作図終了）

'quit'
# Gradsの終了
EOF

grads -bcl "$GS"
# 埋め込んだGrADSスクリプトの実行
# lはLANDSCAPEの略で，紙を横に使う

rm -vf $GS
# 実行したGrADSスクリプトの削除(毎回埋め込むので保存しておく必要はない)
# デバッグの時は残しておくと良いので，その場合この行の先頭に#をつけて
#コメント行にする。
```



## エラーが出た場合の対応

```
  Error occurred on line 103
  In file 3TEST.GS
```

埋め込んだGrADSスクリプトの103行目にエラー。元のシェルスクリプトと行番号が一致していないので，3TEST.GSの方を確認する。

```
'set lev 1000'
＃気圧を1000hPaに設定
```

#が全角文字になっていることが原因



3TEST.sh

```BASH
#!/bin/bash

YYYYMMDDHH=$1 #スクリプトの引数を日付として読み込む
YYYYMMDDHH=${YYYYMMDDHH:-2016060500} #引数の指定が無い場合既定の値に設定する

echo "DATE/TIME=${YYYYMMDDHH}"

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d${YYYY}/${MM}/${DD})
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" ) #YMD1にYYYY年MM月DD日の2日前の日付を入れる

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/ 
#大気データが保存してあるディレクトリ名
CTLA=atm_snp_1hr_glb.ncctl 
#コントロールファイルの名前
CTL11=${INROOT1}/${YMD1}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
# コントロールファイルが存在しなければエラーメッセージを表示して終了する

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
#海洋データが保存してあるディレクトリ名
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi

GS=$(basename $0 .sh).GS
# 埋め込むGrADSスクリプトの名前。
# 元のシェルスクリプト名の末尾の.shを削除して末尾に.GSをつける
FIG=$(basename $0 .sh)_${YYYYMMDDHH}.PDF
#図のファイルの名前の設定。

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"
# hostnameは使用しているコンピュータの名前を表示するコマンド
# pwdは今自分がいるディレクトリの名前を表示するコマンド (print working direcotry)
# dateは日付と時刻を表示するコマンド(-Rを付けると英語表記になる)
# $0=今自分が使っているシェルスクリプトの名前
# $@=スクリプトのすべての引数

KIND1='-kind white->lightgray->gray->dimgray->black'
LEVS1='0 2 0.2'; UNIT1="0.1 g/kg"
# 図の色見本(カラーバー)の設定
# LEVS1は, 最低値, 最大値, 間隔の順に数値を指定する。

KIND2='-kind midnightblue->deepskyblue->green->wheat->orange->red->magenta'
LEVS2='1 30 1'; UNIT2="\`ao\`nC"

# ここからGrADSのスクリプトの埋め込み
cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL21}'
# コントロールファイルを開く

'set rgb 50 127 73 45'
# 海岸線をこげ茶で書くために, こげ茶のRGB値(127 73 45)を
# 色番号50番として保存


xmax = 6; ymax = 2 
# 図の中に入れるパネル(1枚の図の中に入れる小さい図のこと)の数
# xmax=図の横方向のパネルの数, ymax=図の縦方向のパネルの数
ytop=7
# ytop=図の上端を紙の度の高さに持ってくるか

#図の中に入れる各パネル(1枚の図の中に入れる小さい図のこと)の大きさの指定
xwid = 10.5/xmax; ywid = 5.0/ymax

# 各パネル間の余白
xmargin=0.3; ymargin=0.5

'cc'
# 画面を白色にする

# ここから実際に画面の大きさを決めていく
'set vpage 0.0 11 0.0 8.5'
# vpageで図の外枠の大きさを決める
# 横方向が0から11インチ, 縦方向が0から8.5インチ

# ここからパネルを一枚一枚書いていく
nmap = 1; ymap = 1; xmap = 1
# nmap=パネルの通し番号
# ymap=縦方向のパネルの番号 (行番号）
# xmap＝横方向のパネルの番号(列番号)

while (xmap <= 1 & nmap <=5)

#ここで各パネルの左端 (xs), 右端(xe)，上端(ye), 下端(ys)の座標を決める
xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
#縦方向(y方向）に関してはｙ座標の大きい上から書いていくので引き算している
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid

# ここで1枚のパネルの左端 (xs), 右端(xe)，上端(ye), 下端(ys)の座標を設定している
'set parea 'xs ' 'xe' 'ys' 'ye

# グラフの中に縦線と横線を入れない (grid off)
# 図の下にこのソフトの名称(GrADS)などのメッセージを入れない
'set grid off';'set grads off'


'set dfile 'nmap
# 複数CTLファイルを開いていた場合何番目のCTLファイルを使うか指定する

'set time ${TIME}'
# set timeで時刻を指定

'q dims';say result
# q dimsとsay resultで, 現在指定している座標の情報を表示
# スクリプトでGrADSを動かしている場合q dimsだけだと画面に何も表示されない
# 画面に表示させたい場合say resultを付ける

say sublin(result,5)
# sublinはresultの5行目を抜き出す（この場合時刻のみ表示される）
#この箇所は自分が意図した時刻を間違いなく指定しているか確認している

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
# 横軸，縦軸の文字の色，太さ，大きさを指定している

'set xlint 30';'set ylint 10'
# 横軸，縦軸の目盛りの間隔

'set mpdraw on'; 'set map 50 1 1'
#　図に海岸線を入れる。
# 海岸線の色番号を50（今の場合前に指定したこげ茶）
# 海岸線の線の太さを1，線の種類を1（実線）に指定

'set gxout shaded'
# カラーシェードに設定

'color ${LEVS1} ${KIND1} -gxout shaded'
# 塗分けに関する設定。LEVS１とKIND1はこのスクリプトの最初の方で指定した。

'set lev 900'
# 気圧を900hPaに指定

# ここで雲水量の作図を行う
'd CWC.'nmap'*10000'
# dはdisplayの略で図を書け，という意味
# CWCはいま作図する物理量の名前。
# CWCはcloud waterで雲の状態で存在している水の乾燥空気に対する質量比
# nmapは開いているCTLファイルの番号
#　cwcは非常に小さい量なのでここでは10000倍していいる

# 次に等圧面高度の作図を行う
'set gxout contour';'set cint 40';'set ccolor 4';'set cthick 1'
# 等値線を書く。等値線間隔は40とする。等値線の色番号は4（青）
# 等値線の太さは1
'set clopts 4 1 0.05';'set clskip 2'
# 等値線のラベル（等値線に入れる数値）の文字の色番号を4，太さを1，
# 大きさを0.05とする。等値線2本に1本づつ数字を入れる。

'set lev 1000'
# 気圧を1000hPaに設定

'd Z.'nmap
# Zという量を作図する。CTLファイルの番号はnmap

'gxprint $FIG'
#図をファイルに書き出し

xmap=xmap+1; nmap=nmap+1
# パネルの番号を更新

endwhile #xmap
# while文の終了（1枚のパネルの作図終了）

'quit'
# Gradsの終了
EOF

grads -bcl "$GS"
# 埋め込んだGrADSスクリプトの実行
# lはLANDSCAPEの略で，紙を横に使う

rm -vf $GS
# 実行したGrADSスクリプトの削除(毎回埋め込むので保存しておく必要はない)
# デバッグの時は残しておくと良いので，その場合この行の先頭に#をつけて
#コメント行にする。

if [ -f $FIG ]; then echo; echo FIG: $FIG;echo; fi
# 図のファイルが存在していたらその名前を書き出す
```

```
$ 3TEST.sh 
DATE/TIME=2016060500

Grid Analysis and Display System (GrADS) Version 2.2.1
Copyright (C) 1988-2018 by George Mason University
GrADS comes with ABSOLUTELY NO WARRANTY
See file COPYRIGHT for more information

Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile
Issue 'q config' and 'q gxconfig' commands for more detailed configuration information
GX Package Initialization: Size = 11 8.5 
Running in Batch mode
Description file warning: Missing options keyword
Default file number is: 1 
X is varying   Lon = 0 to 360   X = 1 to 289
Y is varying   Lat = -90 to 90   Y = 1 to 145
Z is fixed     Lev = 1000  Z = 1
T is fixed     Time = 00Z05JUN2016  T = 49
E is fixed     Ens = 1  E = 1

T is fixed     Time = 00Z05JUN2016  T = 49
white->lightgray->gray->dimgray->black
clevs= 0 0.2 0.4 0.6 0.8 1 1.2 1.4 1.6 1.8 2
ccols= 16 17 18 19 20 21 22 23 24 25 26 27
GX Package Terminated 
`3TEST.GS' を削除しました

FIG: 3TEST_2016060500.PDF
```



## GrADSの資料

https://researchmap.jp/multidatabases/multidatabase_contents/detail/232785/1c5fd957c7713a7370a2532f6889c9db?frame_id=822841



## 演習

今日作ったサンプルをよく読んで，各行の意味を調べて理解しておく。

### GrADSスクリプトに関する資料

GrADSスクリプトのTips https://wind.gp.tohoku.ac.jp/archives/199

GrADS-Note https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A4%C7%A4%E8%A4%AF%BB%C8%A4%A6%CA%B8%A4%DE%A4%C8%A4%E1

### シェルスクリプトに関する資料

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_SCRIPT_TUTORIAL.md

https://gitlab.com/infoaofd/lab/-/blob/master/LINUX/03.BASH_SCRIPT/LINUX_DATE.md

不明の箇所は次回聞く



完成品のスクリプト（0MAP_ENS5_AO.sh）を実行させて，できた図を確認する。

スクリプトの各行の意味を調べて理解しておく。

不明の箇所は次回聞く

0MAP_ENS5_AO.sh

```bash
#!/bin/bash

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )
YMD2=$(date +"%Y%m%d%H" -d"${T3} 1 day ago" )
YMD3=$(date +"%Y%m%d%H" -d"${T3} 0 day ago" )
YMD4=$(date +"%Y%m%d%H" -d"${T3} 1 day " )
YMD5=$(date +"%Y%m%d%H" -d"${T3} 2 day " )

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/
CTLA=atm_snp_1hr.ncctl
CTL11=${INROOT1}/${YMD1}/$CTLA
CTL12=${INROOT1}/${YMD2}/$CTLA
CTL13=${INROOT1}/${YMD3}/$CTLA
CTL14=${INROOT1}/${YMD4}/$CTLA
CTL15=${INROOT1}/${YMD5}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi
if [ ! -f $CTL12 ];then echo NO SUCH FILE,$CTL12;exit 1;fi
if [ ! -f $CTL13 ];then echo NO SUCH FILE,$CTL13;exit 1;fi
if [ ! -f $CTL14 ];then echo NO SUCH FILE,$CTL14;exit 1;fi
if [ ! -f $CTL15 ];then echo NO SUCH FILE,$CTL15;exit 1;fi

INROOT2=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_o/monit_glb/hourly
CTLO=ocn_snp_1hr_sst.ncctl
CTL21=${INROOT2}/${YMD1}/$CTLO
CTL22=${INROOT2}/${YMD2}/$CTLO
CTL23=${INROOT2}/${YMD3}/$CTLO
CTL24=${INROOT2}/${YMD4}/$CTLO
CTL25=${INROOT2}/${YMD5}/$CTLO
if [ ! -f $CTL21 ];then echo NO SUCH FILE,$CTL21;exit 1;fi
if [ ! -f $CTL22 ];then echo NO SUCH FILE,$CTL22;exit 1;fi
if [ ! -f $CTL23 ];then echo NO SUCH FILE,$CTL23;exit 1;fi
if [ ! -f $CTL24 ];then echo NO SUCH FILE,$CTL24;exit 1;fi
if [ ! -f $CTL25 ];then echo NO SUCH FILE,$CTL25;exit 1;fi

GS=$(basename $0 .sh).GS
FIG=$(basename $0 .sh)_${VAR}_${YYYYMMDDHH}.PDF

HOST=$(hostname);CWD=$(pwd);NOW=$(date -R);CMD="$0 $@"

KIND1='-kind white->lightgray->gray->dimgray->black'
LEVS1='0 2 0.2'; UNIT1="0.1 g/kg"

KIND2='-kind midnightblue->deepskyblue->green->wheat->orange->red->magenta'
LEVS2='1 30 1'; UNIT2="\`ao\`nC"

cat << EOF > ${GS}

'open ${CTL11}'; 'open ${CTL12}'; 'open ${CTL13}'; 'open ${CTL14}'
'open ${CTL15}'

'set rgb 50 127 73 45' ;# red : 127 Green : 73 Blue : 45

xmax = 6; ymax = 2

ytop=7

xwid = 10.5/xmax; ywid = 5.0/ymax
xmargin=0.3; ymargin=0.5

'cc'
'set vpage 0.0 11 0.0 8.5'
'set grid off';'set grads off'

nmap = 1
ymap = 1
xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - (ywid+ymargin)*(ymap-1); ys = ye - ywid
# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 30';'set ylint 10'
'set mpdraw on'; 'set map 50 1 1'

'set gxout shaded'
'color ${LEVS1} ${KIND1} -gxout shaded'
'set lev 900'
'd CWC.'nmap'*10000'

'set gxout contour';'set cint 40';'set ccolor 4';'set cthick 1'
'set clopts 4 1 0.05';'set clskip 2'
'set lev 1000'
'd Z.'nmap


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)
if (nmap = 1) 
yt0=yt
endif


# TEXT
'set t 1';'q dims';TEMP=sublin(result,5);INITIME=subwrd(TEMP,6)
x=(xl+xr)/2; y=yt+0.2
'set strsiz 0.1 0.12'; 'set string 1 c 3 0'
'draw string 'x' 'y' INIT:'INITIME

xmap=xmap+1
nmap=nmap+1
endwhile #xmap

# LEGEND COLOR BAR
x1=3; x2=7.5; y1=3; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -ft 3 -line on -edge circle'
x=x2+0.5; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT1}'

'allclose'



'open ${CTL21}'; 'open ${CTL22}'; 'open ${CTL23}'; 'open ${CTL24}'
'open ${CTL25}'

nmap = 1
ymap = 2
while (ymap <= ymax)

xmap = 1
while (xmap <= xmax & nmap <=5)

xs = 0.5 + (xwid+xmargin)*(xmap-1); xe = xs + xwid
ye = ytop - 1.5; ys = ye - ywid

# SET PAGE
'set parea 'xs ' 'xe' 'ys' 'ye

# SET COLOR BAR
'color ${LEVS2} ${KIND2} -gxout shaded'

# 'set lon ${LONW} ${LONE}'; 'set lat ${LATS} ${LATN}'
# 'set lev ${LEV}'

'set dfile 'nmap
'set time ${TIME}';'q dims';say sublin(result,5)

'set xlopts 1 1 0.08';'set ylopts 1 1 0.08';
'set xlint 30';'set ylint 10'
'set clopts 1 1 0.05'
'set mpdraw on'; 'set map 50 1 1'
'd tos.'nmap


# GET COORDINATES OF 4 CORNERS
'q gxinfo'
line3=sublin(result,3); line4=sublin(result,4)
xl=subwrd(line3,4); xr=subwrd(line3,6)
yb=subwrd(line4,4); yt=subwrd(line4,6)

xmap=xmap+1
nmap=nmap+1
endwhile #xmap

ymap=ymap+1
endwhile #ymap

# LEGEND COLOR BAR
x1=3; x2=7.5; y1=2.5; y2=y1+0.1
'xcbar 'x1' 'x2' 'y1' 'y2 ' -fw 0.1 -fh 0.13 -fs 5 -ft 3 -line on -edge circle'
x=x2+0.5; y=y1
'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
'draw string 'x' 'y' ${UNIT2}'

xx = 5.5; yy=yt0+0.5
'set strsiz 0.12 0.15'; 'set string 1 c 3 0'
'draw string ' xx ' ' yy ' VALID: ${TIME}'; say 'VALID:  ${TIME}'

'set strsiz 0.12 0.15'; 'set string 1 l 3 0'
xx = 0.2; yy=ytop+1
'draw string ' xx ' ' yy ' ${GS}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CMD}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${NOW}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${HOST}'
yy = yy+0.2
'draw string ' xx ' ' yy ' ${CWD}'

'gxprint ${FIG}'
'quit'
EOF

grads -bcl "$GS"
rm -vf $GS

echo
if [ -f $FIG ]; then
echo "OUTPUT : "
ls -lh --time-style=long-iso $FIG
fi
echo

echo "DONE $(basename $0)."
echo
```


# 2024-01-29_ゼミ_GRADS_TUTORIAL_5

C:\Users\atmos\lab\MEETING\2024

[[_TOC_]]

## BASHスクリプトの文法

### ; (セミコロン) の意味

文の区切り。英語ではピリオドと同じ意味で;セミコロンを使う。



## echo文の注意点

コンマ (, ) はスクリプトの分の区切りという意味でつかわれることはない。

ダブルクォーテーション (")で囲まれた範囲は，引用の意味になる。したがって，ダブルクォーテーション (")を一つだけ使うことはできない。組で使う。シングルクォーテーション (')も同じ



## cat文のヒアドキュメント

`<<`をヒアドキュメントと呼ぶ

```
cat << EOF
Hello!
EOF

# EOFで囲まれた箇所をcatコマンドに入力せよ
```

```
$ TEST.HEAR.DOC.sh 
Hello!
```



## リダイレクト

`>`をリダイレクトと呼ぶ。

TEST.RIDIRECT.sh 

```
cat << EOF > TEST.RIDIRECT.TXT
THIS IS A TEST FOR RIDIRECT.
EOF
```

```
$ TEST.RIDIRECT.sh 
```

画面には何もでない

```
$ cat TEST.RIDIRECT.TXT 
THIS IS A TEST FOR RIDIRECT.
```

ファイル (TEST.RIDIRECT.TXT ) に書き出される。



### if文の; (セミコロン) の使い方

#### セミコロンなし

```bash
a=1
echo $a
if [ $a -gt 0 ]
then
  echo "a>0"
else
  echo "a<=0"
fi
echo
```

```bash
$ TEST.IF.sh 
a>0
```



#### セミコロンあり1

```bash
a=1
echo $a
if [ $a -gt 0 ]
then
  echo "a>0"
else
  echo "a<=0"
fi
echo

a=-1; echo $a
if [ $a -gt 0 ]; then
  echo "a>0"
else
  echo "a<=0"
fi
```

### セミコロンあり2

```bash
a=-1; echo $a
if [ $a -gt 0 ]; then echo "a>0"; else echo "a<=0"; fi
```

```BASH
$ TEST.IF.sh                                  
1
a>0
-1
a<=0
```

thenの直後はセミコロン不要

elseの直後もセミコロン不要

後はセミコロンがいる



## GrADS

### GrADSスクリプトにおけるresultの意味

直前に実行したGrADSのコマンドの出力，という特別な意味がある

```
#!/bin/bash

YYYYMMDDHH=$1
YYYYMMDDHH=${YYYYMMDDHH:-2016060500}

YYYY=${YYYYMMDDHH:0:4}; MM=${YYYYMMDDHH:4:2}; DD=${YYYYMMDDHH:6:2}; HH=${YYYYMMDDHH:8:2}

if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi

TIME=${HH}Z${DD}${MMM}${YYYY}

T3=$(date +"%Y/%m/%d %H:%M:%S" -d2016/06/01)
YMD1=$(date +"%Y%m%d%H" -d"${T3} 2 day ago" )

INROOT1=/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/
CTLA=atm_snp_1hr.ncctl
CTL11=${INROOT1}/${YMD1}/$CTLA
if [ ! -f $CTL11 ];then echo NO SUCH FILE,$CTL11;exit 1;fi

GS=$(basename $0 .sh).GS

cat << EOF > ${GS}

'open ${CTL11}'


'set t 1'
'q dims'
say 'MMMMMMM result'
say result
say

TEMP=sublin(result,5)
say 'MMMMMMM TEMP'
say TEMP
say

INITIME=subwrd(TEMP,6)
say 'MMMMMMM INITIME'
say INITIME
say

'allclose'

'gxprint ${FIG}'
'quit'
EOF

grads -bcl "$GS"
rm -vf $GS
```

```
MMMMMMM result
Default file number is: 1 
X is varying   Lon = 110.062 to 199.969   X = 1 to 960
Y is varying   Lat = 15.0469 to 63.9844   Y = 1 to 523
Z is fixed     Lev = 1000  Z = 1
T is fixed     Time = 00Z30MAY2016  T = 1
E is fixed     Ens = 1  E = 1


MMMMMMM TEMP
T is fixed     Time = 00Z30MAY2016  T = 1

MMMMMMM INITIME
00Z30MAY2016
```



複数のファイルを開いた時のファイル番号の指定方法

```
d VAR.FILE_NO
```

例：2番目に開いたファイルのCWPという変数を描画する

```
d CWP.1
```



## xcbarの使い方

https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A5%E9%A5%A4%A5%D6%A5%E9%A5%EA#content_3_28



# 演習

## 1日毎に海面水温分布の図を書く

/work09/am/00.WORK/2023.MRI.CM.2016LLC/12.12.CHK.DAT/12.36.MAP_ENS5_A.vs.AO

```
$ cp -r /work09/am/00.WORK/2023.MRI.CM.2016LLC/12.12.CHK.DAT/12.36.MAP_ENS5_A.vs.AO .
```



## 1日毎に雲水と等圧面高度の図を書く

/work09/am/00.WORK/2023.MRI.CM.2016LLC/12.12.CHK.DAT/12.36.MAP_ENS5_A.vs.AO

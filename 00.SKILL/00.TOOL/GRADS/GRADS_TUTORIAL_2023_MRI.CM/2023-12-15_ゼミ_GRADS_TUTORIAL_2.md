# ゼミ 2023-12-15

[[_TOC_]]

## 今日の概要

毎回同じコマンドを打たずに作業を行う方法について説明する。それを行うためにスクリプトと呼ばれるファイルを作成する。2通りのやり方を説明する。

1. GrADSが用意しているスクリプト（GrADSスクリプト）を直接使う
2. シェルスクリプトからGrADSスクリプトを呼び出す方法

２番目のやり方は、特に日付の処理、定数をスクリプトの中に埋め込む、等を行いたいとき非常に便利である。

## 気象庁全球結合モデル

### 概要

全球大気モデルと全球海洋モデルを、カップラーというソフトを使って結合させたもの。

#### 情報

https://www.jma.go.jp/jma/kishou/know/whitep/1-3-7.html

https://repository.dl.itc.u-tokyo.ac.jp/records/2007330



### データの所在

```shell
$ ls /work02/DATA3/MRI.CM/2016
0README_MRI_2016.TXT
1UNTAR.sh*
JRA-3Q_Hist_Daily_201606.tar
SFTP_BATCH.TXT
v1.7_HiHi/
v1.7_HiHi_AGCM/
```

v1.7_HiHiは大気海洋結合モデルの計算結果

```
$ ls /work02/DATA3/MRI.CM/2016/v1.7_HiHi/
d_monit_a/  d_monit_o/
```

d_monit_a/は大気のデータ 

d_monit_o/は海洋のデータ

v1.7_HiHiは大気モデルの計算結果

```
$ ls /work02/DATA3/MRI.CM/2016/v1.7_HiHi_AGCM/
d_monit_a/
```



## GrADSスクリプトの作成

作り方は簡単で、GrADSのコマンドを引用符`'`で囲って並べたファイルを作ればよい。例えば次のようになる。

まずviを開いて下記を打ち込み、T.GSという名前で保存する。

```
'open /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.ncctl'
'cc'
'd t'
'gxprint T2016-05-30_00_T.PDF'
'quit'
```

最後のquitはGrADSを終了させるためのGrADSコマンドである。

GrADSを起動させる。

```
$ grads -bp
```

作成したGrADSスクリプトを実行させる。

```
ga-> T.GS
Description file warning: Missing options keyword
GX Package Terminated 
```

```
$ ls
0.README.TXT  T.GS  T2016-05-30_00_T.PDF
```



## GrADSのスクリプトの文法

Fortranと同様にGrADSのスクリプトも、繰り返し、条件によって作業を分岐させる、などを行うことができる。

### 繰り返し

以下の内容のファイルをviで作成して、LOOP.GSという名前で保存する。

```bash
i=1
ie=10
while (i < ie)
say i
i=i+1
endwhile
```

上記には引用符がいらないことに注意する。

GrADS起動

```
$ grads -bp
```

GrADSスクリプトLOOP.GSを実行

```
ga-> LOOP.GS
1
2
3
4
5
6
7
8
9
```

GrADS終了

```
ga-> quit
```

内容の説明

```
i=1
ie=10
while (i < ie)
```

iがie未満である限り, endwhileまでの内容を繰り返す。

```
say i
```

sayはFortranのprintに相当するもので, 変数の内容を書き出したりするのに用いる。

```
i=i+1
```

iを一つ増やす。



### 条件分岐

以下の内容のファイルをviで作成して、CONDITION.GSという名前で保存する。

```
a=1
if(a>0)
say 'a='a
say 'a is POSITIVE.'
endif

a=-1
say 'a='a
if(a<0)
say 'a is NEGATIVE.'
endif
```

GrADSスクリプトCONDITION.GSを実行

```
ga-> CONDITION.GS 
a=1
a is POSITIVE.
a=-1
a is NEGATIVE.
```

```
ga-> quit
```

**注意**

```
say 'a='a
```

`a=`という文字列を書きたいときは引用符で囲む。aという変数の値を書き出したいときは引用符で囲まない。

### else文の使用

上のCONDITION.GSと同じ作業を行うためのスクリプトは、次のように書くこともできる。

```
$ cp CONDITION.GS ELSE.GS
```

viで次のファイルELSE.GSを作成する。

```
a=1
say 'a='a
if(a>0)
say 'a is POSITIVE.'
else
say 'a is NEGATIVE.'
endif

a=-1
say 'a='a
if(a>0)
say 'a is POSITIVE.'
else
say 'a is NEGATIVE.'
endif
```

```
$ grads -bcp
```

```
ga-> ELSE.GS
a=1
a is POSITIVE.
a=-1
a is NEGATIVE.
```

```
ga-> quit
```

else文は, ifのかっこで囲まれた部分の条件を満たさないときの処理をどうするか指定するときに使う。

GrADSスクリプトの文法に関しては下記の資料参照

GrADSスクリプトのTips https://wind.gp.tohoku.ac.jp/archives/199

GrADS-Note https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A4%C7%A4%E8%A4%AF%BB%C8%A4%A6%CA%B8%A4%DE%A4%C8%A4%E1



## シェルスクリプトからGrADSスクリプトを動かす

viで下記の内容のファイルを作成して, SHELL2GRADS01.shという名前で保存する。 

```bash
cat <<END >LOOP2.GS
i=1
ie=5
while (i <= ie)
say i
i=i+1
endwhile
'quit'
END

grads -bcp "LOOP2.GS"
```

SHELL2GRADS01.shに実行許可を与える。

```
$ chmod u+x SHELL2GRADS01.sh 
```

SHELL2GRADS01.shを実行する

```bash
$ SHELL2GRADS01.sh 

Grid Analysis and Display System (GrADS) Version 2.2.1
.....
Running in Batch mode
1
2
3
4
5
GX Package Terminated 
```

シェルスクリプトの中で作成されたGrADSスクリプトの確認

```bash
$ cat LOOP2.GS
i=1
ie=5
while (i <= ie)
say i
i=i+1
endwhile
'quit'
```

シェルスクリプトのGrADSスクリプトを埋め込む方法

```bash
cat <<END >ファイル名
埋め込みたい内容
END
```

ENDの部分はなんでもよく、対応がとれていればよい。

```bash
cat <<EOF >ファイル名
埋め込みたい内容
EOF
```



シェル変数の値をGrADSスクリプトに埋め込む

```
cp SHELL2GRADS01.sh SHELL2GRADS02.sh
```

viでSHELL2GRADS02.shを編集する。

```bash
I=1
IE=5

cat <<END >LOOP3.GS
i=${I}
ie=${IE}
while (i <= ie)
say i
i=i+1
endwhile
'quit'
END

grads -bcp "LOOP3.GS"
```

SHELL2GRADS02.shに実行許可を与える。

```
$ chmod u+x SHELL2GRADS02.sh 
```

SHELL2GRADS02.shを実行する

```
$ SHELL2GRADS02.sh 

Grid Analysis and Display System (GrADS) Version 2.2.1
....
Running in Batch mode
1
2
3
4
5
GX Package Terminated 
```

埋め込まれたGrADSスクリプトの確認

```
$ cat LOOP3.GS
i=1
ie=5
while (i <= ie)
say i
i=i+1
endwhile
'quit'
```

意図したとおり、SHELL2GRADS02.sh

```
i=${I}
ie=${IE}
```

がcat LOOP3.GSでは

```
i=1
ie=5
```

に置き換わっている。



上記の機能を使って、少し高度な例に挑戦する。→次回



### 課題

今日の内容をもう一度自分でやってみる

GrADSスクリプトのTips https://wind.gp.tohoku.ac.jp/archives/199

を読んで、文法を理解しておく。わからない点は次回質問する。



## GrADSスクリプトに関する資料

GrADSスクリプトのTips https://wind.gp.tohoku.ac.jp/archives/199

GrADS-Note https://seesaawiki.jp/ykamae_grads-note/d/GrADS%A5%B9%A5%AF%A5%EA%A5%D7%A5%C8%A4%C7%A4%E8%A4%AF%BB%C8%A4%A6%CA%B8%A4%DE%A4%C8%A4%E1

## 付録

### GrADSの起動・終了

#### 起動

Rloginで,

```plaintext
$ grads -bcp
```

> Grid Analysis and Display System (GrADS) Version 2.2.1
>
> Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile
>
> GX Package Initialization: Size = 8.5 11
>
> ga-> 

#### 終了

```
ga-> quit
GX Package Terminated 
```



### シェルとプロンプトについて

#### シェル

https://eng-entrance.com/linux-shell

ユーザーからの命令を受け取って，OSに伝える役割をするインターフェースとなるソフトウェア

シェルにはいくつか種類がある。当研究室では最も一般的なbashを使っている。

#### プロンプト

[https://e-words.jp/w/%E3%83%97%E3%83%AD%E3%83%B3%E3%83%97%E3%83%88.html](https://e-words.jp/w/プロンプト.html)

コマンド入力待ちの状態であることを示す記号

GrADSのプロンプト

```plaintext
ga->
```

Bashのプロンプト

```plaintext
$　
```



##### 注意

bashのプロンプト`$`が表示されているときは，bashのコマンドが使用可能であるが，GrADSのコマンドは使用できない。

GrADSのプロンプト`ga->`が表示されているときは，GrADSのコマンドが使用可能であるが，bashのコマンドはそのままでは使用できない



### GrADSの初歩

#### CTLファイルの所在確認

ＧrADSで作業を行う際は、データファイルに関する情報（データの種類、並べ方）などを書いたCTL（コントロール）ファイルというファイルを用いる。

まずコントロールファイルの所在を確かめる。

今回使用するコントロールファイルは、

/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/

というディレクトリに存在する、末尾がncctlで終わるファイルである。

```BASH
/work09/am
$ ls /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/*ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_diag_avr_1hr.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr_glb.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_zonal_snp_1hr.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_zonal_snp_1hr_glb.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/sfc_avr_1hr.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/sfc_avr_1hr_glb.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/sfc_snp_1hr.ncctl
/work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/sfc_snp_1hr_glb.ncctl
```

実際にコントロールファイルの内容を画面に表示させてみる。

```bash
$ less /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.ncctl
```

lessは長いテキストファイルを画面表示させるためのコマンドで、

終了させるときはq

1画面分先に進むときは, スペース

gで先頭に戻る

```bash
DSET ^atm_snp_1hr.nc
DTYPE NETCDF
OPTIONS  
TITLE atm_snp_1hr    SNP ( Snapshot data )
UNDEF -9.99E33
XDEF  960  LINEAR   110.0625000    0.0937500
YDEF  523  LEVELS
```

大文字・小文字の区別はない

`DSET`: データファイルの名前

`^`: カレント・ディレクトリ，自分が現在いるディレクトリ）を意味する記号 この場合したがって，ctlファイルとデータファイルが同じ場所に保存されていることを仮定している (^を実際にデータがあるディレクトリに書き換えることで変更可能)

`DTYPE`: ファイルの種類で、今回はNetCDFという種類のファイルを用いている。

`undef -9.99E33`: undefined（定義されていない）の略。値が存在しない場所には，ダミーの値として-9.99×10の33乗という値が入っている。

`xdef`: 東西方向のデータ並びに関する情報

```
XDEF  960  LINEAR   110.0625000    0.0937500
```

東西方向に９６０個データがある。LINEARは等間隔でデータが並んでいるという意味。西の端の点の経度が東経110.0625000度である。隣り合う点の間の間隔が0.0937500度（約10km）である。

`ydef`: 東西方向のデータ並びに関する情報

```bash
YDEF  523  LEVELS
```

南北方向に523個データがある。LEVELSは一個一個の点の座標をすべて指定する。

`tdef`: 時間方向のデータ並びに関する情報

`zdef`: 鉛直方向のデータ数

```
ZDEF   27  LEVELS
     1000.000  975.000  950.000  925.000  900.000
      875.000  850.000  825.000  800.000  775.000
      750.000  700.000  650.000  600.000  550.000
      500.000  450.000  400.000  350.000  300.000
      250.000  225.000  200.000  175.000  150.000
      125.000  100.000
```

鉛直方向に27個データがある。LEVELSは一個一個の点の座標（今の場合は気圧）をすべて指定する。高度(km)を指定することもできる。（ZDEFに書かれた数値の範囲を見てGrADSが自動判別する）。

`VARS`: 保存されている変数の数

`template`: ファイル名の指定にひな型 (template)を使う。

`byteswapped`: バイナリデータはビッグエンディアンである。



#### GrADSの起動

Rloginで,

```plaintext
$ grads -bcp
```

#### CTLファイルを開く

```bash
ga-> open /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.ncctl
```

```bash
Data file /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.nc is open as file 1
LON set to 110.062 199.969 
LAT set to 15.0469 63.9844 
LEV set to 1000 1000 
Time values set: 2016:5:30:0 2016:5:30:0 
E set to 1 1 
```



#### データの格子と変数に関する情報を表示する

```plaintext
ga-> q ctlinfo
```

> dset /work02/DATA3/MRI.CM/2016/v1.7_HiHi/d_monit_a/2016053000/atm_snp_1hr.nc  
> title atm_snp_1hr    SNP ( Snapshot data )  
> undef -9.99e+33  
> dtype netcdf  
> xdef 960 linear 110.062 0.09375  
> ydef 523 levels 15.0469 15.1406 15.2344 15.3281 15.4219 15.5156 15.6094 15.7031  
>  15.7969 15.8906 15.9844 16.0781 16.1719 16.2656 16.3594 16.4531 16.5469 16.6406  
> .....  
>  62.6719 62.7656 62.8594 62.9531 63.0469 63.1406 63.2344 63.3281 63.4219 63.5156  
>  63.6094 63.7031 63.7969 63.8906 63.9844  
> zdef 27 levels 1000 975 950 925 900 875 850 825  
>  800 775 750 700 650 600 550 500 450 400  
>  350 300 250 225 200 175 150 125 100  
> tdef 265 linear 00Z30MAY2016 60mn  
> vars 7  
> u  27  t,z,y,x  Longitudinal Wind Speed                                                          m/s              1  
> v  27  t,z,y,x  Latitudinal Wind Speed                                                           m/s              1  
> t  27  t,z,y,x  Temperature                                                                      K                1  
> q  27  t,z,y,x  Specific Humidity                                                                kg/kg            1  
> z  27  t,z,y,x  Geopotential Height                                                              m                1
> cwc  27  t,z,y,x  cloud_water_content                                                              kg/kg            1
> omg  27  t,z,y,x  Vertical P-Velocity                                                              Pa/s             1
> endvars

**undef**はundefinedの略で，欠損値（データが無い点に入れるダミーの値）のことを意味する

**dtype**はデータファイルの型を指定している。ここではnetcdf



#### 現在の次元を表示する

いま利用可能な変数の次元を表示する。

```plaintext
ga-> q dims
```

> Default file number is: 1   
> X is varying   Lon = 110.062 to 199.969   X = 1 to 960  
> Y is varying   Lat = 15.0469 to 63.9844   Y = 1 to 523  
> Z is fixed     Lev = 1000  Z = 1  
> T is fixed     Time = 00Z30MAY2016  T = 1  
> E is fixed     Ens = 1  E = 1  

経度は110.062から199.969度の範囲が利用できる。

緯度は 15.0469から63.9844度の範囲が利用できる。

気圧は1000hPaで固定されている

時刻は00Z30MAY2016で固定されている

アンサンブル(実験番号)は1で固定されている。



## 作図の練習

### 等値線を書く

知識: 等値線のことをコンター (contour)ということがある

```bash
ga-> d t
Contouring: 273 to 300 interval 3 
```

dはdisplayの略

d tでtという量（今の場合気温）の図を書け、という意味になる。

#### 図をファイルに書き出す

```
ga-> gxprint T2016-05-30_00_T.PDF
Created PDF file T2016-05-30_00_T.PDF
```

T2016-05-30_00_T.PDFはファイル名。末尾のPDFは拡張子と呼ばれ、画像ファイルの種類を示す。今の場合PDFファイルとなる。そのほかには, gif, png, jpg, ps, epsが使用できる。

![Clipboard01](2023-11-24/Clipboard01.png)

#### 画面の消去

```plaintext
ga-> cc
```

この場合、背景が白色になる。



#### 再描画

tの単位がKなので，摂氏に変換して作図

```plaintext
ga-> d t-273.15
```

```
ga-> gxprint T2016-05-30_00_T.PDF
Created PDF file T2016-05-30_00_T.PDF
```

![Clipboard02](2023-11-24/Clipboard02.png)

```
ga-> cc
```

#### 等値線間隔の変更

```plaintext
ga-> set gxout contour
ga-> set cint 10 
ga-> d t-273.15
```

```
ga-> gxprint T2016-05-30_00_T.PDF
Created PDF file T2016-05-30_00_T.PDF
```

![Clipboard03](2023-11-24/Clipboard03.png)

#### 特定の値の等値線を引く

```
ga-> cc
```

```plaintext
ga-> set clevs 5 15 25
ga-> d t-273.15
```

```
ga-> gxprint T2016-05-30_00_T.PDF
Created PDF file T2016-05-30_00_T.PDF
```

![Clipboard04](2023-11-24/Clipboard04.png)

#### 等値線のラベルを消す

```plaintext
ga-> cc
ga-> set clab off
ga-> d t-273.15
```



> Contouring: 1000 to 1018 interval 2

#### 等値線の文字の大きさを変更する

```plaintext
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```



> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

set clopts 色番号 [太さ [大きさ]]

```plaintext
ga-> d t-273.15
```



> Contouring: 1000 to 1018 interval 2

#### 等値線の文字を入れる間隔を変更する

```plaintext
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```



> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

```plaintext
ga-> set clskip 2
ga-> d t-273.15
```



> Contouring: 1000 to 1018 interval 2

#### ベクトル図

```plaintext
ga-> cc
ga-> set gxout vector
ga-> d u;v
```

ベクトルの本数が多すぎて (10km間隔にデータがある)，真っ黒になる

#### ベクトルを間引く

```plaintext
ga-> cc
ga-> d skip(u,50,50);v
```



#### カラーシェードを書く

```plaintext
ga-> set gxout shade2
ga-> d t-273.15
ga-> cbarn
```



#### 色を変える

```plaintext
ga-> cc
ga-> color 0 30 2 -kind blue->white->red
```



> blue->white->red
>  clevs= 1000 1002 1004 1006 1008 1010 1012 1014 1016 1018
>  ccols= 16 17 18 19 20 21 22 23 24 25 26

```plaintext
ga-> d t-273.15
```



> Contouring at clevs =  1000 1002 1004 1006 1008 1010 1012 1014 1016 1018

```plaintext
ga-> cbarn
```



colorコマンドについては下記参照 (grads colorで検索するとヒットする)

http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script/color.gs?lang=jp

色見本については下記も参照

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/GRADS_TIPS.md

#### 色分けの間隔を指定する

```plaintext
ga-> cc
ga-> color -levs 0 5 10 15 20 25 -kind blue->white->red
```





```plaintext
ga-> d t-273.15
```



> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010

```plaintext
ga-> cbarn
```



#### カラーシェードと等値線の重ね合わせ

```plaintext
ga-> cc
ga-> color -levs 0 5 10 15 20 25 30 -kind blue->white->red
```



> white->gold->orange
>  clevs= 1000 1001 1002 1003 1004 1005 1006 1010
>  ccols= 16 17 18 19 20 21 22 23 24

```plaintext
ga-> d t-273.15
```



> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010

```plaintext
ga-> cbarn
ga-> set gxout contour
ga-> set cint 5
```

**図を重ねるときの注意**

軸ラベルの文字が汚くなるので，一枚目の図を書き終わった後に,

```plaintext
ga-> set xlab off
ga-> set ylab off
```

を入れる。

```plaintext
ga-> d t-273.15
```



#### 図をファイルに書き出す

##### pdfファイルに書き出す

```plaintext
ga-> gxprint FIG.pdf
```



## GrADSの資料

https://researchmap.jp/multidatabases/multidatabase_contents/detail/232785/1c5fd957c7713a7370a2532f6889c9db?frame_id=822841



## 演習

残りの部分を自分でやってみる

上記資料を読んでおく。

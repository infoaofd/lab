# ゼミ 2023-11-24

[[_TOC_]]

## GrADSの練習

GrADSは作図と簡単な計算を行うソフトウェア。

GrADSもスクリプトを作成することができる。ただし文法はbashやFortranとは異なる。



## 今日の資料

https://gitlab.com/infoaofd/lab/-/blob/master/MEETING/2021/2021-05-06_01_LAB.md?ref_type=heads



## メソ客観解析

### 概要

客観解析＝データ同化を用いて作成された格子データ。

客観解析では，着目している時刻よりも前の時刻データが用いられる（予報の際の初期値として用いるため）。

一方再解析では，着目している時刻の前後の時刻のデータが用いられる。

#### 情報

https://www.data.jma.go.jp/add/suishin/cgi-bin/catalogue/make_product_page.cgi?id=Kyakkan

 http://www.jma.go.jp/jma/kishou/know/whitep/1-3-3.html

 http://www.jma.go.jp/jma/kishou/books/nwptext/45/1_chapter3.pdf

### データ

#### データの所在

http://database.rish.kyoto-u.ac.jp/arch/jmadata/gpv-netcdf.html

#### 情報

http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/README

#### ダウンロード

wgetコマンドを使う

wgetの使用法については下記を参照
 https://www.atmarkit.co.jp/ait/articles/1606/20/news024.html

https://tatsushid.github.io/blog/2014/08/specify-wget-savedir/

https://www.4web8.com/2435.html

データの保存先

```shell
/work03/gu1
$ tree /work01/DATA/MSM/
/work01/DATA/MSM/
├── MSM-P
└── MSM-S
    └── 2020
```



ダウンロード前の設定

```shell
/work03/gu1
$ ORG=http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/MSM-S/

$ DEST=/work01/DATA/MSM/MSM-S/2020

$ Y=202

$ Y=2020

$ M=07

$ D=02

$ echo $ORG
http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/gpv/netcdf/MSM-S/

$ echo $DEST
/work01/DATA/MSM/MSM-S/2020

$ echo $Y
2020

$ echo $M$D
0702
```

ダウンロード

```bash
$ wget -nv -nc -P $DEST $ORG/$Y/$M$D.nc

$ ll /work01/DATA/MSM/MSM-S/2020/0702.nc
-rw-r--r--. 1 gu1 134M 2020-07-03 10:05 /work01/DATA/MSM/MSM-S/2020/0702.nc
```



## データの概要を確認

```bash
$ ll /work01/DATA/MSM/MSM-S/2020/0702.nc
-rw-rwxrwx. 1 am 134M 2020-07-03 10:05 /work01/DATA/MSM/MSM-S/2020/0702.nc*
```

nc: netCDFの略

### netCDFの概要

- 気象・海洋のデータを保存するためのデータ形式
- 静止画でpngや，動画でmp4などファイルにはいろいろな形式があるが，それと同様に気象・海洋のデータを保存するためのデータ形式にもいくつか種類がある。代表的なものとして，netCDFとgrib (ぐりぶ)があるが，ここでは, netCDFについて学ぶ。

#### 特徴

- データ自体にデータに関する情報が記述されている（どのようなデータか，分かるようになっている）
- データのサイズを小さくしたり，高速でファイルを読み書きできるような工夫がなされている
- netCDFライブラリと呼ばれるライブラリを使用して読み書きを行う

```
$ ncdump -h /work01/DATA/MSM/MSM-S/2020/0702.nc
netcdf \0702 {
dimensions:
        lon = 481 ;
        lat = 505 ;
        time = 24 ;
        ref_time = 8 ;
variables:
        float lon(lon) ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
                lon:standard_name = "longitude" ;
        float lat(lat) ;
                lat:long_name = "latitude" ;
                lat:units = "degrees_north" ;
                lat:standard_name = "latitude" ;
        float time(time) ;
                time:long_name = "time" ;
                time:standard_name = "time" ;
                time:units = "hours since 2020-07-02 00:00:00+00:00" ;
        float ref_time(ref_time) ;
                ref_time:long_name = "forecaset reference time" ;
                ref_time:standard_name = "forecaset_reference_time" ;
                ref_time:units = "hours since 2020-07-02 00:00:00+00:00" ;
        short psea(time, lat, lon) ;
                psea:scale_factor = 0.4587155879 ;
                psea:add_offset = 95000. ;
                psea:long_name = "sea level pressure" ;
                psea:units = "Pa" ;
                psea:standard_name = "air_pressure" ;
        short sp(time, lat, lon) ;
                sp:scale_factor = 0.9174311758 ;
                sp:add_offset = 80000. ;
                sp:long_name = "surface air pressure" ;
                sp:units = "Pa" ;
                sp:standard_name = "surface_air_pressure" ;
        short u(time, lat, lon) ;
                u:scale_factor = 0.006116208155 ;
                u:add_offset = 0. ;
                u:long_name = "eastward component of wind" ;
                u:units = "m/s" ;
                u:standard_name = "eastward_wind" ;
        short v(time, lat, lon) ;
                v:scale_factor = 0.006116208155 ;
                v:add_offset = 0. ;
                v:long_name = "northward component of wind" ;
                v:units = "m/s" ;
                v:standard_name = "northward_wind" ;
        short temp(time, lat, lon) ;
                temp:scale_factor = 0.002613491379 ;
                temp:add_offset = 255.4004974 ;
                temp:long_name = "temperature" ;
                temp:units = "K" ;
                temp:standard_name = "air_temperature" ;
        short rh(time, lat, lon) ;
                rh:scale_factor = 0.002293577883 ;
                rh:add_offset = 75. ;
                rh:long_name = "relative humidity" ;
                rh:units = "%" ;
                rh:standard_name = "relative_humidity" ;
        short r1h(time, lat, lon) ;
                r1h:scale_factor = 0.006116208155 ;
                r1h:add_offset = 200. ;
                r1h:long_name = "rainfall in 1 hour" ;
                r1h:units = "mm/h" ;
                r1h:standard_name = "rainfall_rate" ;
        short ncld_upper(time, lat, lon) ;
                ncld_upper:scale_factor = 0.001666666591 ;
                ncld_upper:add_offset = 50. ;
                ncld_upper:long_name = "upper-level cloudiness" ;
                ncld_upper:units = "%" ;
        short ncld_mid(time, lat, lon) ;
                ncld_mid:scale_factor = 0.001666666591 ;
                ncld_mid:add_offset = 50. ;
                ncld_mid:long_name = "mid-level cloudiness" ;
                ncld_mid:units = "%" ;
        short ncld_low(time, lat, lon) ;
                ncld_low:scale_factor = 0.001666666591 ;
                ncld_low:add_offset = 50. ;
                ncld_low:long_name = "low-level cloudiness" ;
                ncld_low:units = "%" ;
        short ncld(time, lat, lon) ;
                ncld:scale_factor = 0.001666666591 ;
                ncld:add_offset = 50. ;
                ncld:long_name = "cloud amount" ;
                ncld:units = "%" ;
                ncld:standard_name = "cloud_area_fraction" ;
        short dswrf(time, lat, lon) ;
                dswrf:scale_factor = 0.0205 ;
                dswrf:add_offset = 665. ;
                dswrf:long_name = "Downward Short-Wave Radiation Flux" ;
                dswrf:units = "W/m^2" ;
                dswrf:standard_name = "surface_net_downward_shortwave_flux" ;

// global attributes:
                :Conventions = "CF-1.0" ;
                :history = "created by create_1daync_msm_s.rb  2020-07-03" ;
}
```

```bash
$ which ncdump
~/anaconda3/bin/ncdump
```

#### データ内容

```plaintext
dimensions:
        lon = 481 ;
        lat = 505 ;
        time = 24 ;
```



データのもつ次元に関する記述

**データは配列として保存**されている。

経度方向のデータ数は481, 緯度方向のデータ数は505, 時間方向のデータ数は24個。

```shell
variables:
```



変数の内容に関する記述

```shell
        float lon(lon) ;
                lon:long_name = "longitude" ;
                lon:units = "degrees_east" ;
                lon:standard_name = "longitude" ;
```



`float`: 4バイト実数型の配列

`lon(lon)`: 配列名はlonで配列の要素数はlon個 (481個)

`lon:long_name = "longitude" ;`以降は**アトリビューション**と呼ばれ，**変数に関する付加情報**を記述する。

```shell
        float time(time) ;
                time:long_name = "time" ;
                time:standard_name = "time" ;
                time:units = "hours since 2021-08-14 00:00:00+00:00" ;
```



timeは`float`型なので，4バイト実数型の配列であり，配列要素数は`time`個。

```shell
time:units = "hours since 2021-08-14 00:00:00+00:00" ;
```



timeの単位 (`units`)は時間 (`hours`)で，timeの基点となる時刻は`2020-08-14 00:00:00`である。世界標準時を使用している (`+00:00`)。

```shell
    short r1h(time, lat, lon) ;
            r1h:scale_factor = 0.006116208155 ;
            r1h:add_offset = 200. ;
            r1h:long_name = "rainfall in 1 hour" ;
            r1h:units = "mm/h" ;
            r1h:standard_name = "rainfall_rate" ;
```



r1hは2バイト整数 (`short`)で，(time, lat, lon) の3つの次元をもつ配列である（3次元配列）。

```shell
            r1h:scale_factor = 0.006116208155 ;
            r1h:add_offset = 200. ;
```



ファイルに保存されている2バイト整数の数値 (ここではr1hとする)から，実際の値 (ここではr1h_actualとする)に換算するためには，`scale_factor`と`add_offset`を使って，次のように計算する

```shell
r1h_actual = r1h*scale_factor+add_offset
```



#### ncdumpのオプション

`ncdump -h`でデータの概要のみ表示する (hはヘッダーの意味)

`ncdump -c`で，データの座標（緯度，経度，高度，時間など）を表示

`ncdump`で，データの情報をすべて表示。表示が多すぎるときは, `CTL`+`C`で表示を停止させる

## GrADSの起動・終了

### 起動

Rloginで,

```plaintext
$ grads -bcp
```

> Grid Analysis and Display System (GrADS) Version 2.2.1
>
> Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile
>
> GX Package Initialization: Size = 8.5 11
>
> ga-> 

### 終了

```
ga-> quit
GX Package Terminated 
```



## シェルとプロンプトについて

##### シェル

https://eng-entrance.com/linux-shell

ユーザーからの命令を受け取って，OSに伝える役割をするインターフェースとなるソフトウェア

シェルにはいくつか種類がある。当研究室では最も一般的なbashを使っている。

##### `プロンプト`

[https://e-words.jp/w/%E3%83%97%E3%83%AD%E3%83%B3%E3%83%97%E3%83%88.html](https://e-words.jp/w/プロンプト.html)

コマンド入力待ちの状態であることを示す記号

GrADSのプロンプト

```plaintext
ga->
```



Bashのプロンプト

```plaintext
$
```



##### 注意

bashのプロンプト`$`が表示されているときは，bashのコマンドが使用可能であるが，GrADSのコマンドは使用できない。

GrADSのプロンプト`ga->`が表示されているときは，GrADSのコマンドが使用可能であるが，bashのコマンドはそのままでは使用できない



## GrADSの初歩

#### GrADSの起動

Rloginで,

```plaintext
$ grads -bcp
```

#### netCDFファイルを開く

```bash
ga-> sdfopen /work01/DATA/MSM/MSM-S/2020/0702.nc
```

> Scanning self-describing file:  /work01/DATA/MSM/MSM-S/2020/0702.nc
>  SDF file /work01/DATA/MSM/MSM-S/2020/0702.nc is open as file 1
>  LON set to 120 150
>  LAT set to 22.4 47.6
>  LEV set to 0 0
>  Time values set: 2020:7:2:0 2020:7:2:0
>  E set to 1 1

#### データの格子と変数に関する情報を表示する

```plaintext
ga-> q ctlinfo
```

> dset /work01/DATA/MSM/MSM-S/2020/0702.nc
>  title
>  undef 9.96921e+36
>  dtype netcdf
>  xdef 481 linear 120 0.0625
>  ydef 505 levels 22.4 22.45 22.5 22.55 22.6 22.65 22.7 22.75
>  22.8 22.85 22.9 22.95 23 23.05 23.1 23.15 23.2 23.25
>
> 47.3 47.35 47.4 47.45 47.5 47.55 47.6
>  zdef 1 linear 0 1
>  tdef 24 linear 00Z02JUL2020 60mn
>  vars 12
>  psea=>psea  0  t,y,x  sea level pressure
>  sp=>sp  0  t,y,x  surface air pressure
>  u=>u  0  t,y,x  eastward component of wind
>  v=>v  0  t,y,x  northward component of wind
>  temp=>temp  0  t,y,x  temperature
>  rh=>rh  0  t,y,x  relative humidity
>  r1h=>r1h  0  t,y,x  rainfall in 1 hour
>  ncld_upper=>ncld_upper  0  t,y,x  upper-level cloudiness
>  ncld_mid=>ncld_mid  0  t,y,x  mid-level cloudiness
>  ncld_low=>ncld_low  0  t,y,x  low-level cloudiness
>  ncld=>ncld  0  t,y,x  cloud amount
>  dswrf=>dswrf  0  t,y,x  Downward Short-Wave Radiation Flux
>  endvars

**undef**はundefinedの略で，欠損値（データが無い点に入れるダミーの値）のことを意味する

**dtype**はデータファイルの型を指定している。ここではnetcdf





##### 注:　sdfopenでは開けないnetCDFが時々あるので注意。

#### 現在の次元を表示する

いつ，どこの値が処理対象となっているか

```plaintext
ga-> q dims
```



> Default file number is: 1
>  X is varying   Lon = 120 to 150   X = 1 to 481
>  Y is varying   Lat = 22.4 to 47.6   Y = 1 to 505
>  Z is fixed     Lev = 0  Z = 1
>  T is fixed     Time = 00Z02JUL2020  T = 1
>  E is fixed     Ens = 1  E = 1

#### 等値線図の作図

```plaintext
ga-> d psea
```



> Contouring: 100000 to 101800 interval 200

知識: 等値線のことをコンター (contour)ということがある

#### 画面の消去

```plaintext
ga-> cc
```



#### 再描画

psesaの単位がパスカルなので，ヘクトパスカルに変換して作図

```plaintext
ga-> d psea/100
```



> Contouring: 1000 to 1018 interval 2

#### 等値線間隔の変更

```plaintext
ga-> set gxout contour
ga-> set cint 4 
cint = 4 
ga-> d psea/100
```



> Contouring: 1000 to 1016 interval 4

#### 特定の値の等値線を引く

```plaintext
ga-> cc
ga-> set clevs 1000 1001
Number of clevs = 2 
ga-> d psea/100
```



> Contouring at clevs =  1000 1001

#### 等値線のラベルを消す

```plaintext
ga-> cc
ga-> set clab off
ga-> d psea/100
```



> Contouring: 1000 to 1018 interval 2

#### 等値線の文字の大きさを変更する

```plaintext
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```



> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

set clopts 色番号 [太さ [大きさ]]

```plaintext
ga-> d psea/100
```



> Contouring: 1000 to 1018 interval 2

#### 等値線の文字を入れる間隔を変更する

```plaintext
ga-> cc
ga-> set clab on
ga-> set clopts 1 3 0.2
```



> SET CLOPTS values:  Color = 1 Thickness = 3 Size = 0.2

```plaintext
ga-> set clskip 2
ga-> d psea/100
```



> Contouring: 1000 to 1018 interval 2

#### ベクトル図

```plaintext
ga-> cc
ga-> set gxout vector
ga-> d u;v
```



ベクトルの本数が多すぎて (5km間隔にデータがある)，真っ黒になる

#### ベクトルを間引く

```plaintext
ga-> cc
ga-> d skip(u,20,20);v
```



#### カラーシェードを書く

```plaintext
ga-> set gxout shade2
ga-> d psea/100
Contouring: 1000 to 1018 interval 2 
ga-> cbarn
```



#### 色を変える

```plaintext
ga-> cc
ga-> color 1000 1018 2 -kind blue->white->red
```



> blue->white->red
>  clevs= 1000 1002 1004 1006 1008 1010 1012 1014 1016 1018
>  ccols= 16 17 18 19 20 21 22 23 24 25 26

```plaintext
ga-> d psea/100
```



> Contouring at clevs =  1000 1002 1004 1006 1008 1010 1012 1014 1016 1018

```plaintext
ga-> cbarn
```



colorコマンドについては下記参照 (grads colorで検索するとヒットする)

http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script/color.gs?lang=jp

色見本については下記も参照

https://gitlab.com/infoaofd/lab/-/blob/master/GRADS/GRADS_TIPS.md

#### 色分けの間隔を指定する

```plaintext
ga-> cc
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
```



> white->darkblue->green->yellow->orange->darkred
>  clevs= 1000 1001 1002 1003 1004 1005 1006 1010
>  ccols= 16 17 18 19 20 21 22 23 24

```plaintext
ga-> d psea/100
```



> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010

```plaintext
ga-> cbarn
```



#### カラーシェードと等値線の重ね合わせ

```plaintext
ga-> cc
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
```



> white->gold->orange
>  clevs= 1000 1001 1002 1003 1004 1005 1006 1010
>  ccols= 16 17 18 19 20 21 22 23 24

```plaintext
ga-> d psea/100
```



> Contouring at clevs =  1000 1001 1002 1003 1004 1005 1006 1010

```plaintext
ga-> cbarn
ga-> set gxout contour
ga-> set cint 1
```



> cint = 1

```plaintext
ga-> d psea/100
```



> Contouring: 1000 to 1019 interval 1

**図を重ねるときの注意**

軸ラベルの文字が汚くなるので，一枚目の図を書き終わった後に,

```plaintext
ga-> set xlab off
ga-> set ylab off
```



を入れる。

例:

```plaintext
ga-> cc
ga-> set xlab on
ga-> set ylab on
ga-> color -levs 1000 1001 1002 1003 1004 1005 1006 1010 -kind white->gold->orange
ga-> d psea/100
ga-> set xlab off
ga-> set ylab off
ga-> set gxout contour
ga-> set cint 1
ga-> d psea/100
```



#### 図をファイルに書き出す

##### epsファイルに書き出す

```plaintext
ga-> gxprint FIG.eps
```



#### pngファイルに書き出す

```plaintext
ga-> gxprint FIG.png
```



注: FIGの部分は自由に変えてよい

図を後から加工したり，高画質の図が必要な場合epsで書き出した方がよい（irfan viewを使うと，後からpngに変換できる）

#### 終了

```plaintext
ga-> exit
```

## GrADSの資料

https://gitlab.com/infoaofd/lab/-/tree/master/GRADS?ref_type=heads



## 演習


# 領域内の最大値と最大値が出現した緯度経度を求めて，値を図に書き出す

```

# MAX AND ITS LOCATION
'set lon $LONW'; 'set lat $LATS'
'd amax(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3MAX=subwrd(result,4);say 'MMMMM P3MAX='P3MAX

'd amaxlocx(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3X=subwrd(result,4)
'set x 'P3X; 'q dims'; LIN=sublin(result,2);P3LON=subwrd(LIN,6)
say 'MMMMM P3LON='P3LON

'd amaxlocy(P60.3,lon=$ALONW,lon=$ALONE,lat=$ALATS,lat=$ALATN)'
P3Y=subwrd(result,4)
'set y 'P3Y; 'q dims'; LIN=sublin(result,3);P3LAT=subwrd(LIN,6)
say 'MMMMM P3LAT='P3LAT

'markplot 'P3LON' 'P3LAT' -c 1 -m 3 -s 0.04'
'q w2xy 'P3LON' 'P3LAT
xx=subwrd(result,3); yy=subwrd(result,6)
'set strsiz 0.1 0.12'; 'set string 1 l 4 0'
'draw string 'xx+0.02' 'yy+0.05' 'math_int(P3MAX)
```


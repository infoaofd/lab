# GrADS NetCDF書き出し

[[_TOC_]]

## 基本

先に全ての時間・格子情報を定義してから、出力する。

tという変数に収納されたデータをtempという名前でgrads.ncというファイルに書き出す

```
'open grads.ctl'

'set x 1 96'
'set y 1 96'
'set z 1 38'
'set t 1 10'
'define temp = t'
'set sdfwrite grads.nc'
'sdfwrite temp'
```

netCDFの定義の都合上，一度「define」しないといけない。デフォルトでは、1ファイル1変数しか出力できない。「set sdfattr」で設定できるようである。

出力ファイルは「ncdump -h grads.nc」などで、内容をチェックできる。

## 例 時間平均・データの範囲を変更する

```
'set lon $LONW $LONE'; 'set lat $LATS $LATN'; 'set z 1'
'set time $TIME1'
say; say 'MMMMM AVG OVER $TIME1 $TIME2'
'TAV=ave(PBLH,time=${TIME1},time=${TIME2})'
say 'MMMMM DONE.'

say; say 'MMMMM WRITE NC FILE'
'define PBLH = TAV'
'set sdfwrite $NC'
'sdfwrite PBLH'
say 'MMMMM DONE.'
```

```
'set lon $LONW $LONE'; 'set lat $LATS $LATN'; 'set z 1'
'set time $TIME1'
say; say 'MMMMM AVG OVER $TIME1 $TIME2'
'TAV=ave(PBLH,time=${TIME1},time=${TIME2})'
say 'MMMMM DONE.'

say; say 'MMMMM WRITE NC FILE'
'define PBLH = TAV'
'set sdfwrite $NC'
'sdfwrite PBLH'
say 'MMMMM DONE.'
```

```
function YYYYMMDDHHMI(){
YYYY=${YMDHM:0:4}; MM=${YMDHM:4:2}; DD=${YMDHM:6:2}; HH=${YMDHM:8:2}
MI=${YMDHM:10:2}
if [ $MM = "01" ]; then MMM="JAN"; fi; if [ $MM = "02" ]; then MMM="FEB"; fi
if [ $MM = "03" ]; then MMM="MAR"; fi; if [ $MM = "04" ]; then MMM="APR"; fi
if [ $MM = "05" ]; then MMM="MAY"; fi; if [ $MM = "06" ]; then MMM="JUN"; fi
if [ $MM = "07" ]; then MMM="JUL"; fi; if [ $MM = "08" ]; then MMM="AUG"; fi
if [ $MM = "09" ]; then MMM="SEP"; fi; if [ $MM = "10" ]; then MMM="OCT"; fi
if [ $MM = "11" ]; then MMM="NOV"; fi; if [ $MM = "12" ]; then MMM="DEC"; fi
}

LONW=126;LONE=131.5;LATS=24.5;LATN=34

YMDHM1=$2; YMDHM1=${YMDHM1:-202108111200}
YMDHM=$YMDHM1
YYYYMMDDHHMI $YMDHM
Y1=${YYYY};MM1=${MM};MMM1=${MMM};DD1=${DD};HH1=${HH};MI1=${MI}
YMDHM2=$3; YMDHM2=${YMDHM2:-202108120000}
YMDHM=$YMDHM2
YYYYMMDDHHMI $YMDHM
Y2=${YYYY};MM2=${MM};MMM2=${MMM};DD2=${DD};HH2=${HH};MI2=${MI}

TIME1=${HH1}:${MI1}Z${DD1}${MMM1}${Y1}
TIME2=${HH2}:${MI2}Z${DD2}${MMM2}${Y2}
```



## 例 領域平均

可降水量の領域平均値をNetCDFに書き出す

`set sdfwrite`で`-3dz`オプションをつけるのがポイント

-3dt  forces the output data file to have at least 3 coordinate dimensions (lon, lat, and time)

出力データ ファイルに少なくとも 3 つの座標次元を強制します

```bash
'set z 1'
'PW=vint(lev(z=1),QV,$LEV2)'

'set lon $ALONE'; 'set lat $ALATS $ALATN'
#'q dims'; say result
'LONAVE=ave(PW,lon=$ALONW,lon=$ALONE)'
#'q dims'; say result
'set lat $LATS'
#'q dims'; say result
'AAV=ave(LONAVE,lat=$ALATS, lat=$ALATN)'
'd AAV'
say 'MMMMM $TIME 'subwrd(result,4)

'set lon ${ALONW}'; 'set lat ${ALATS}'; 'set z 1'; 'set time ${TIME}'
'define pw = AAV'
OUT='${ODIR}/$(basename $0 .sh)_${ALONW}_${ALATS}_${YYYY}${MM}${DD}_${HH}.nc'
'set sdfwrite -3dz -nc4 'OUT
'sdfwrite pw'
say 'MMMMM OUT: 'OUT
say

```

```
$ ncdump 12.COMP_PW_1STEP_20240614_00.nc 
netcdf \12.COMP_PW_1STEP_20240614_00 {
dimensions:
        lon = 1 ;
        lat = 1 ;
        lev = 1 ;
variables:
        double lon(lon) ;
                lon:units = "degrees_east" ;
                lon:long_name = "Longitude" ;
        double lat(lat) ;
                lat:units = "degrees_north" ;
                lat:long_name = "Latitude" ;
        double lev(lev) ;
                lev:units = "millibar" ;
                lev:long_name = "Level" ;
        double pw(lev, lat, lon) ;
                pw:_FillValue = -999000000. ;
data:

 lon = 120 ;

 lat = 22.3999996185303 ;

 lev = 1000 ;

 pw =
  74.1739901972265 ;
}
```



## 例 欠損値 (UNDEF)の設定

WVXOUT < 0 もしくは WVYOUT<0のとき，WVMAGを強制的に欠損値 (UNDEF) にする

**'set undef'**で欠損値を設定できる

```bash
say 'CCCCC WVXOUT= 'WVXOUT' WVYOUT= 'WVYOUT
if (WVXOUT < 0 | WVYOUT < 0 )
'WVMAG=const(WVMAG,$UNDEF,-a)'
endif

'd WVMAG'
WVMGOUT=subwrd(result,4)
say 'CCCCC WVMGOUT= 'WVMGOUT

'set lon ${ALONW}'; 'set lat ${ALATS}'; 'set z 1'; 'set time ${TIME}'
'define WVF = WVMAG'
OUT='${ODIR}/$(basename $0 .sh)_${ALONW}_${ALATS}_${YYYY}${MM}${DD}_${HH}.nc'
'set sdfwrite -3dt -nc4 'OUT
'set undef $UNDEF'
'sdfwrite WVF'
```



## 4次元データにして書き出す

```
'set sdfwrite -4d -nc4 $OUT'
```



## オプション一覧

> **-3dt**: 出力データファイルに少なくとも3つの座標次元（経度、緯度、時間）を強制します。
>
> **-3dz**: 出力データファイルに少なくとも3つの座標次元（経度、緯度、レベル）を強制します。
>
> **-4d**: 出力データファイルに少なくとも4つの座標次元（経度、緯度、レベル、時間）を強制します。
>
> **-4e**: 出力データファイルに少なくとも4つの座標次元（経度、緯度、時間、アンサンブル）を強制します。
>
> **-5d**: 出力データファイルに5つの座標次元（経度、緯度、レベル、時間、アンサンブル）を強制します。
>
> **-rt**: T軸をレコード（無制限）次元として設定します。
>
> **-re**: E軸をレコード（無制限）次元として設定します。

> **-flt**: 出力データは浮動小数点精度で書き込まれます。
>
> **-dbl**: 出力データは倍精度で書き込まれます（デフォルト）。
>
> **-nc3**: 出力データファイルはnetCDFクラシック形式で書き込まれます（デフォルト）。
>
> **-nc4**: 出力データファイルはnetCDF-4形式で書き込まれます。
>
> **-chunk**: 出力データはチャンク化されます（-nc4の場合のみ）。チャンクサイズはset chunksizeコマンドで設定します。
>
> **-zip**: 出力データは圧縮されます（-nc4および-chunkが含まれます）。

# シア―渦度と曲率渦度

[[_TOC_]]

## 流線座標系 (自然座標系)

Holton (1994)に従って，流線座標系について説明する。

<img src="image-20241006105820751.png" alt="image-20241006105820751" style="zoom: 33%;" />

$\mathbf{V}$を流れを表すベクトルとする。$\mathbf{t}$, $\mathbf{n}$をそれぞれ，流れの接線方向，法線方向の単位ベクトルとする。$\mathbf{t}$は常に$\mathbf{V}$と平行であるので，$\mathbf{V}$の大きさを$V$とすると，
$$
\mathbf{V}=V\mathbf{t}
$$
である。また，$V$は常に正となる。$s=s(x,y,t)$を流線を表す方程式とすると，
$$
V=\frac{Ds}{Dt}
$$
である。これより，
$$
\frac{D\mathbf{V}}{Dt}=\frac{D(V\mathbf{t})}{Dt}
=\mathbf{t}\frac{DV}{Dt}+V\frac{D\mathbf{t}}{Dt}
$$
上の図より，角度の微小変化$\delta \psi$は，
$$
\delta \psi = \frac{\delta s}{|R|}
=
\underbrace{
\frac{|\delta \mathbf{t}|}{|\mathbf{t}|}
}_{|\mathbf{t}|=1} 
=|\delta \mathbf{t}|
$$
である。$R$は曲率半径と呼ばれる。


> $R>0$のとき，気塊は進行方向に対して左に曲がる (北半球の低気圧)
>
> $R<0$のとき，気塊は進行方向に対して右に曲がる(北半球の高気圧)

いま，$R>0$とする。このとき，
$$
\frac{\delta s}{R}
=|\delta \mathbf{t}|
$$

であるが，これより，
$$
\frac{1}{\delta s}
=\frac{1}{R|\delta \mathbf{t}|} \\
\\
\frac{\delta \mathbf{t}}{\delta s}
=\frac{1}{R|\delta \mathbf{t}|}\delta \mathbf{t} \\
$$
$\delta s \to 0$のとき，$\delta \mathbf{t}$は$\mathbf{n}$に平行になる。$\mathbf{n}$は単位ベクトルであることから，
$$
\delta \mathbf{t}=|\delta \mathbf{t}|\mathbf{n}
$$
である。よって，
$$
\frac{D \mathbf{t}}{D s}
=\frac{1}{R} \mathbf{n} \\
$$

$R<0$のときも，同様に考えると上式と同じ式を得ることができる。

これを使うと，
$$
\frac{D \mathbf{t}}{Dt}=\frac{D\mathbf{t}}{Ds}\frac{Ds}{Dt}=\frac{\mathbf{n}}{R}V
$$
であるので，$\frac{D\mathbf{V}}{Dt}
=\mathbf{t}\frac{DV}{Dt}+V\frac{D\mathbf{t}}{Dt}$は，
$$
\frac{D\mathbf{V}}{Dt}
=\mathbf{t}\frac{DV}{Dt}+\mathbf{n}\frac{V^2}{R}
$$
となる。右辺第2項を遠心力と呼ぶことがある。
$$

$$


## 流線座標系における渦度

![image-20241006104732209](image-20241006104732209.png)



上図の閉曲線にそった循環$\delta C$は，
$$
\delta C=V[\delta s + d(\delta s)]-\bigg(V+\frac{\partial V}{\partial n}\delta n\bigg)\delta s
$$
となる。$d(\delta s)=\delta \beta \delta n$をつかうと，
$$
\delta C=\bigg(-\frac{\partial V}{\partial n}+V\frac{\delta \beta }{\delta s}\bigg)\delta n\delta s
$$
がえられる。$\delta n, \delta s \to 0$とすると，渦度$\zeta$が，
$$
\zeta = \lim_{\delta n, \delta s \to 0}\frac{\delta C}{(\delta n \delta s)}=-\frac{\partial V}{\partial n}+\frac{V}{R_s}
$$
ここで，$R_s$は，流線の曲率半径で
$$
\frac{\partial \beta}{\partial s}=\frac{1}{R_s}
$$
で定義される。

## デカルト座標系におけるシア―渦度と曲率渦度

Bell and Keyser (1993)に基づいて説明する。

$\mathbf{V}$が常に流れの向きをむいており，流れの向きの方向の単位ベクトルが$\mathbf{t}$であること，$\mathbf{t}$に直交する単位ベクトルが$\mathbf{n}$であることに注意する。

$x$, $y$, $z$を右手系をなすデカルト座標系とする。$x$,$y$平面は$\mathbf{t}$と$\mathbf{n}$がつくる平面と平行であるとする。また，$\mathbf{k}$を$z$方向の単位ベクトルとする。このとき，
$$
\mathbf{t}=\mathbf{V}/V, \quad \mathbf{n}=\mathbf{k}\times\mathbf{s}
$$
であることに注意すると
$$
\begin{eqnarray}
\frac{\partial}{\partial s}=\frac{u}{V}\frac{\partial}{\partial x}+\frac{v}{V}\frac{\partial}{\partial y} \\
\\
\frac{\partial}{\partial n}=-\frac{v}{V}\frac{\partial}{\partial x}+\frac{u}{V}\frac{\partial}{\partial y} \\
\end{eqnarray}
$$
と表すことができる。$\mathbf{V}$の大きさ$V$と$\mathbf{V}$が$x$軸となす角度$\alpha$は，
$$
\begin{eqnarray}
V &=& (u^2+v^2)^{1/2} \\
\\
\alpha&=&\tan^{-1}(v/u)
\end{eqnarray}
$$
と表される。
$$
&&\frac{\partial (u^2+v^2)^{1/2}}{\partial x}\\
&=&\frac{1}{2}(u^2+v^2)^{-1/2} \cdot 2(uu_x+vv_x) \\
&=&\frac{uu_x+vv_x}{V}
$$

$$
&&\frac{\partial (u^2+v^2)^{1/2}}{\partial 7}\\
&=&\frac{1}{2}(u^2+v^2)^{-1/2} \cdot 2(uu_y+vv_y) \\
&=&\frac{uu_y+vv_y}{V} \\
$$

であるから，シア―渦度$-\frac{\partial V}{\partial n}$は，
$$
\begin{eqnarray}
-\frac{\partial V}{\partial n}
&＝&\frac{v}{V}\frac{\partial V}{\partial x}-\frac{u}{V}\frac{\partial V}{\partial y} \\
\\
&=& \frac{1}{V}\bigg(v\frac{\partial (u^2+v^2)^{1/2}}{\partial x}-u\frac{\partial (u^2+v^2)^{1/2}}{\partial y}\bigg)\\
\\
&=&\frac{1}{V}\bigg(v\frac{uu_x+vv_x}{V}-u\frac{uu_y+vv_y}{V}\bigg) \\
&=&-\frac{1}{V^2}(-uvu_x-v^2v_x+u^2u_y+uvv_y)

\end{eqnarray}
$$
となる。曲率渦度$\frac{V}{R_s}$は，相対渦度$\zeta=v_x-u_y$から，シア―渦度$-\frac{\partial V}{\partial n}$を引けば求められる。
$$
\begin{eqnarray}
\frac{V}{R_s}&=&(v_x-u_y)+\frac{1}{V^2}(-uvu_x-v^2v_x+u^2u_y+uvv_y) \\
\\
&=&\frac{1}{V^2}\bigg((u^2+v^2)v_x-(u^2+v^2)u_y-uvu_x-v^2v_x+u^2u_y+uvv_y\bigg)\\
\\
&=&\frac{1}{V^2}\bigg(-uvu_x+u^2v_x-v^2u_y+uvv_y\bigg)

\end{eqnarray}
$$
曲率渦度は，$V\frac{\partial \alpha}{\partial s}$で求めることもできる。これを使って，次のように計算してもよい。

いま，$r$を$x$, $y$, $p$, $t$のような独立変数を表すとすると，$V$と$\alpha$の$r$に関する偏微分係数は，それぞれ，
$$
\begin{eqnarray}
\frac{\partial V}{\partial r}&=&\frac{u}{V}\frac{\partial u}{\partial r}+\frac{v}{V}\frac{\partial v}{\partial r} \\
\\
\frac{\partial \alpha}{\partial r}&=&-\frac{v}{V^2}\frac{\partial v}{\partial r}+\frac{u}{V^2}\frac{\partial v}{\partial r}
\end{eqnarray}
$$
これと，$\frac{d}{dx}(\tan^{-1}x)=1/(1+x^{2})$を使うと，
$$
\begin{eqnarray}
V\frac{\partial \alpha}{\partial s}&=&
V\bigg(\frac{u}{V}\frac{\partial \alpha}{\partial x}+\frac{v}{V}\frac{\partial \alpha}{\partial y}\bigg) \\
\\
&=&u\frac{\partial \tan^{-1}(v/u)}{\partial x}+v\frac{\partial \tan^{-1}(v/u)}{\partial y}\\
\\
&=&u\frac{1}{1+(\frac{v}{u})^2}\frac{v_xu-vu_x}{u^2}
+v\frac{1}{1+(\frac{v}{u})^2}\frac{v_y u - v u_y}{u^2}\\
\\
&=& \frac{1}{V^2}\bigg(u(v_xu - v u_x)+v(v_y u -v u_y)\bigg) \\
\\
&=&\frac{1}{V^2}\bigg(- uvu_x + u^2v_x -v^2u_y + uvv_y\bigg) \\
\end{eqnarray}
$$

## GrADS用コード

GrADSは，球面座標 (経度, 緯度, 気圧) を用いていることに注意する。

### 微分を行うコードの書き方

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/00.TOOL/GRADS/0.GRADS_TUTORIAL_04_MET_VARS.md

#### パラメータの設定

```
pi=3.14159265359; dtr=pi/180; r=6378137.0; FAC=1.0E5 ;# km->m
```

#### 東西方向の微分 ($\frac{1}{r\cos\phi}\frac{\partial (\mathrm{var})}{\partial \lambda}$)

```
dx '=' r '*cos(' dtr '*' lat ')*' dtr '*cdiff(' lon ',' x')'
dtdx '=cdiff(' var ',' x ')/' dx
```

#### 南北方向の微分  ($\frac{1}{r}\frac{\partial (\mathrm{var})}{\partial \phi}$)

```
dy '=' r '*' dtr '*cdiff(' lat ',' y ')'
dtdy '=cdiff(' var ',' y ')/' dy
```

### 渦度の計算

#### 微分の計算

```
dx '=' r '*cos(' dtr '*' lat ')*' dtr '*cdiff(' lon ',' x')'
dy '=' r '*' dtr '*cdiff(' lat ',' y ')'
'dudx =cdiff(u,x)/dx'; 'dudy =cdiff(u,y)/dy'
'dvdx =cdiff(v,x)/dx'; 'dvdy =cdiff(v,y)/dy'
'rv2=1.0/(u*u+v*v)';'u2=u*u';'v2=v*v'
```

### 曲率渦度

$$
\frac{V}{R_s}
=\frac{1}{V^2}\bigg(-uvu_x+u^2v_x-v^2u_y+uvv_y\bigg)
$$

```
'CVOR=rv2*(-u*v*dudx+u2*dvdx-v2*dudy+u*v*dvdy)'
```

### シアー渦度

$$
-\frac{\partial V}{\partial n}
=-\frac{1}{V^2}(-uvu_x-v^2v_x+u^2u_y+uvv_y)
$$

```
'SVOR=-rv2*(-u*v*dudx-v2*dvdx+u2*dudy+u*v*dvdy)'
```



## 参考文献

Holton, J., 1994: Introduction to Dynamic Meteorology, Elsevier.

Bell, G. D., and D. Keyser, 1993: Shear and Curvature vorticity and Potential-Vorticity Interchanges: Interpretation and Application to a Cutoff Cyclone Event. *Mon. Wea. Rev.*, **121**, 76–102, https://doi.org/10.1175/1520-0493(1993)121%3C0076:SACVAP%3E2.0.CO;2.

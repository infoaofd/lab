#!/bin/bash

INFLE=DATE.TXT
cat <<EOF> $INFLE
20200704
20210815
20210812
20210612
EOF

INROOT=/work01/DATA/MSM/MSM-P

GS=$(basename $0 .sh).GS

cat <<EOF>$GS

is=1
while ( is > 0 )
ret = read(${INFLE})
stat=sublin(ret,1)
if(stat > 0)
break
endif

date=sublin(ret,2)
yr=substr(date,1,4)
md=substr(date,5,4)
#say yr' 'md
INFLE='${INROOT}/'yr'/'md'.nc'
say INFLE

'sdfopen 'INFLE
'allclose'

endwhile
# retには2行の情報が返される。1行目がファイル状態、2行目にファイルの内容である
# ファイル状態は「0」で正常、「1」でエラー、「2」でファイルの最後まで来ています、などとなる。
# 一度のreadで1行分の情報しか読み取らないために、複数の行を読み取りたい場合は、
# 複数回実行する必要がある

'quit'
return
say
EOF

grads -bcp "$GS" 

#rm -vf $GS

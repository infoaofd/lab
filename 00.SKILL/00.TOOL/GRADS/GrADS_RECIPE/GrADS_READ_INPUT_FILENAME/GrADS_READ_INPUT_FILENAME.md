# GrADS: テキストファイルで記載された入力ファイルのリストを読み込む

## スクリプトの例

```fortran
#!/bin/bash

#入力ファイルのリスト
INFLE=DATE.TXT
cat <<EOF> $INFLE
20200704
20210815
20210812
20210612
EOF

# 入力ファイルが存在するディレクトリ
INROOT=/work01/DATA/MSM/MSM-P

GS=$(basename $0 .sh).GS


cat <<EOF>$GS ;#ここから次のEOFまでがGrADSスクリプトとして埋め込まれる。

is=1
while ( is > 0 )
ret = read(${INFLE}) ;#テキストファイルを1行だけ読み込む
stat=sublin(ret,1)   ;#このファイルの末尾の説明参照
if(stat > 0)　　　　　 ;#ファイルの終端を検出したらwhileループの強制終了
break
endif

date=sublin(ret,2)
yr=substr(date,1,4)
md=substr(date,5,4)
#say yr' 'md
INFLE='${INROOT}/'yr'/'md'.nc'
say INFLE

'sdfopen 'INFLE

'allclose'

endwhile
# retには2行の情報が返される。1行目がREADコマンド終了時の状態、2行目にファイルの内容である
# 終了時の状態は「0」で正常、「1」でエラー、「2」でファイルの最後まで来ています、などとなる。
# 一度のreadで1行分の情報しか読み取らないために、複数の行を読み取りたい場合は、
# 複数回実行する必要がある

'quit'
return
say
EOF

grads -bcp "$GS" 

#rm -vf $GS
```

## 実行例

```bash
/work09/am/00.WORK/2022.MESHIMA.WV/12.12.MSM_SNAPSHOT
$ MSM_SNAPSHOT_SAMPLE_2.sh 

Grid Analysis and Display System (GrADS) Version 2.2.1
Copyright (C) 1988-2018 by George Mason University
GrADS comes with ABSOLUTELY NO WARRANTY
See file COPYRIGHT for more information

Config: v2.2.1 little-endian readline grib2 netcdf hdf4-sds hdf5 opendap-grids geotiff shapefile
Issue 'q config' and 'q gxconfig' commands for more detailed configuration information
GX Package Initialization: Size = 8.5 11 
Running in Batch mode
/work01/DATA/MSM/MSM-P/2020/0704.nc
closing 1 files
/work01/DATA/MSM/MSM-P/2021/0815.nc
closing 1 files
/work01/DATA/MSM/MSM-P/2021/0812.nc
closing 1 files
/work01/DATA/MSM/MSM-P/2021/0612.nc
closing 1 files
GX Package Terminated 
```


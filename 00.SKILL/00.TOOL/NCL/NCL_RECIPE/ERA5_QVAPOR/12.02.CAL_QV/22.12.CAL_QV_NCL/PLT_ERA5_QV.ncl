; 
; PLT_QV.ncl
; /work09/am/00.WORK/2022.ECS2022/12.16.ERA5_ANALYSIS/32.14.ANOMALY_FILED_QV/12.02.CAL_QV/22.12.CAL_QV_NCL
;
script_name  = get_script_name()
print("script="+script_name)
script=systemfunc("basename "+script_name+" .ncl")

month_abbr = (/"","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep", \
                    "Oct","Nov","Dec"/)

HOST=systemfunc("hostname")
CWD=systemfunc("pwd")

;arg    = getenv("NCL_ARG_2")

DSET="JRA55"
Y=2022
YYYY=tostring(Y)
INDIR="/work01/DATA/ERA5/ECS_PS/06HR_FLUX_ANOMALY/QVAPOR/"
INFLE="ERA5_ECS_PS_QVAPOR_"+YYYY+".nc"
IN=INDIR+INFLE
a=addfile(IN,"r")

qv=a->qvapor
lon=a->g0_lon_2
lat=a->g0_lat_1
;printVarSummary(qv)

IS=(31+28+31+30+31+18)*4
IE=IS+4
time=a->initial_time0_hours(IS)
utc_date = cd_calendar(time, 0)
   year   = tointeger(utc_date(:,0))    ; Convert to integer for
   month  = tointeger(utc_date(:,1))    ; use sprinti 
   day    = tointeger(utc_date(:,2))
   hour   = tointeger(utc_date(:,3))
   minute = tointeger(utc_date(:,4))

date_str = sprinti("%0.2i ", day) + \
           month_abbr(month) + " "  + sprinti("%0.4i", year)
datenum=sprinti("%0.4i", year)+sprinti("%0.2i", month)+sprinti("%0.2i", day)
FIG=script+"_ERA5_ECS_PS_QVAPOR_"+datenum
TYP="pdf"

qvtav=dim_avg_n_Wrap(qv(IS:IE,:,:)*1000.0,0)
qvtav!0="lat"
qvtav!1="lon"
lat@units="degrees_north"
lon@units="degrees_east"
qvtav&lat=lat
qvtav&lon=lon
qvtav&lat@units="degrees_north"
qvtav&lon@units="degrees_east"
printVarSummary(qvtav)

print("MMMMM PLOTTING")
wks = gsn_open_wks(TYP, FIG)
;作図するファイルを開く
gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")


res=True
; resmpの情報をresに引き継ぐ
res@gsnDraw  = False ;don't draw
res@gsnFrame = False ;don't advance frame

res@cnFillOn = True
; Trueの場合, 色で塗分けする
res@cnLinesOn = False
; Falseの場合, 等値線を書かない
res@gsnAddCyclic = False
;地球一周分のデータでない場合Falseにする

res@cnLevelSelectionMode = "ManualLevels"
res@cnMinLevelValF = 6.
res@cnMaxLevelValF = 22.
res@cnLevelSpacingF = 2.

res@pmLabelBarHeightF = 0.03
res@pmLabelBarOrthogonalPosF = .15
res@cnFillDrawOrder = "PreDraw"
res@pmLabelBarWidthF=0.6
res@lbTitleOn        = True
res@lbTitleString = "g/kg" ;"W/m~S~2~N~"
res@lbTitlePosition = "Right" ; title position
res@lbTitleFontHeightF= .02 ;Font size
res@lbTitleDirection = "Across" ; title direction
res@gsnLeftString   = DSET+" QV 2m "+date_str               ; add the gsn titles
res@gsnCenterString = ""
res@gsnRightString  = ""



resmp=res
; 地図の描画の設定に関する情報を記載する変数(resmp)を用意する
resmp@mpDataBaseVersion="HighRes"
resmp@mpFillOn               = True    ;地図を塗りつぶす
resmp@mpLandFillColor = "black"
resmp@mpMinLonF = 120     ; 経度の最小値
resmp@mpMaxLonF = 150     ; 経度の最大値
resmp@mpMinLatF =   5     ; 緯度の最小値
resmp@mpMaxLatF =  35     ; 緯度の最大値

plot=gsn_csm_contour_map(wks,qvtav,resmp)
; カラーシェード図と地図の作図

draw(plot)

frame(wks)
;print("MMMMM IN : "+IN)
print("MMMMM FIG: "+FIG+"."+TYP)

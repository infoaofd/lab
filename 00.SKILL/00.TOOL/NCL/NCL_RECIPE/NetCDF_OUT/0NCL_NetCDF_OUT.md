# NCLでのNetCDFの出力

[[_TOC_]]

## 出力ファイルを開く

```fortran
ODIR="/work01/DATA/J-OFURO3/V1.2_PRE/DAILY_CLIM/"
system("mkdir -vp "+ODIR)
OUT="DAILY_CLIM_SMO_"+ys+"-"+ye+"_"+VAR+".nc"
print("MMMMM filo="+filo)
OUT=ODIR+OFLE
; 同名のファイルが存在していたらまず消去
system("/bin/rm -f "+OUT)

; 出力ファイルを開く
b    = addfile (OUT, "c")
```

## データ出力

### 略式

```bash
b->書き込み先のファイルの中<での変数の名称 = スクリプトの中で使用している変数の名称
```

**xClmDay_sm**が**スクリプトの中**で使用している変数の名称，<u>VAR_NC</u>が<u>書き込み先のファイルの中</u>での変数の名称とすると，下記のようになる。

```bash
b->VAR_NC = xClmDay_sm
```



### 正式

```fortran
print("MMMMM CREATE NETCDF")
; 出力データの次元の設定
dimx   = dimsizes(xClmDay_sm)
ntim   = dimx(0)
nlat   = dimx(1)
mlon   = dimx(2)
year_day=xClmDay_sm&year_day

; ファイル属性(ファイルに関する情報)の設定
filAtt = 0
filAtt@title = vName+": Daily Climatology: "+yrStrt+"-"+yrLast  
filAtt@input_directory_ = INDIR
filAtt@creation_date = systemfunc("date -R")
fileattdef( b, filAtt )           ; copy file attributes  

setfileoption(b,"DefineMode",True)
; 変数名の設定
varNC_sm   = "VAR_NC"

;変数の設定
; 次元
dimNames = (/"year_day", "latitude", "longitude"/)  
dimSizes = (/ ntim , nlat,  mlon/) 
dimUnlim = (/ True , False, False/)   
filedimdef(b,dimNames,dimSizes,dimUnlim)

filevardef(b, "year_day"  ,typeof(year_day),  getvardims(year_day)) 
filevardef(b, "latitude"  ,typeof(latitude),  getvardims(latitude)) 
filevardef(b, "longitude" ,typeof(longitude), getvardims(longitude))
filevardef(b, varNC_sm,typeof(xClmDay_sm),getvardims(xClmDay_sm))    

; 属性
filevarattdef(b,"year_day"  ,year_day)   ; copy time attributes
filevarattdef(b,"latitude"  ,latitude)   ; copy lat attributes
filevarattdef(b,"longitude" ,longitude)  ; copy lon attributes
filevarattdef(b,varNC_sm, xClmDay_sm)                

; 変数の値の書き出し
b->year_day   = (/year_day/)     
b->latitude   = (/latitude/)
b->longitude  = (/longitude/)
b->$varNC_sm$ = (/xClmDay_sm/)
; 書き込み先のファイルの中での変数の名称をvarNC_smの変数の中身(今の場合VAR_NC)とする。
; 書き出す変数の値はxClmDay_smの値
print("MMMMM OUTPUT:")
print(b)
```



## サンプル

### 4次元データの場合

```FORTRAN
print("MMMMM CREATE NETCDF")
OUT=ODIR+"/"+OFLE
; 同名のファイルが存在していたらまず消去
system("/bin/rm -vf "+OUT)

; 出力ファイルを開く
b    = addfile (OUT, "c")

; 出力データの次元の設定
dimx   = dimsizes(VAR4D)
ntim   = dimx(0)
nlev   = dimx(1)
nlat   = dimx(2)
mlon   = dimx(3)

; ファイル属性(ファイルに関する情報)の設定
filAtt = 0
filAtt@output_directory = ODIR
filAtt@input_file = INFLE
filAtt@input_directory = INDIR
filAtt@script=script_name
filAtt@working_directory=systemfunc("pwd")
filAtt@creation_date = systemfunc("date -R")
fileattdef( b, filAtt )           ; copy file attributes  

setfileoption(b,"DefineMode",True)
; 変数名の設定
varNC   = VAR

;変数の設定
; 次元
dimNames = (/"time", "lev", "latitude", "longitude"/)  
dimSizes = (/ ntim , nlev, nlat,  mlon/) 
dimUnlim = (/ True , False, False, False/)   
filedimdef(b,dimNames,dimSizes,dimUnlim)

filevardef(b, "time"  ,typeof(time),  getvardims(time)) 
filevardef(b, "lev"  ,typeof(lev),  getvardims(lev)) 
filevardef(b, "latitude"  ,typeof(latitude),  getvardims(latitude)) 
filevardef(b, "longitude" ,typeof(longitude), getvardims(longitude))
filevardef(b, varNC,typeof(VAR4D),getvardims(VAR4D))    

; 属性
filevarattdef(b,"time"  ,time)   ; copy time attributes
filevarattdef(b,"lev"  ,lev)   ; copy time attributes
filevarattdef(b,"latitude"  ,latitude)   ; copy lat attributes
filevarattdef(b,"longitude" ,longitude)  ; copy lon attributes
filevarattdef(b,varNC, VAR4D)                

; 変数の値の書き出し
b->time   = (/time/)
b->lev = (/LEV/)     
b->latitude   = (/latitude/)
b->longitude  = (/longitude/)
b->$varNC$ = (/VAR4D/)
; 書き込み先のファイルの中での変数の名称をvarNCの変数の中身とする。
; 書き出す変数の値はVAR4Dの値
print("MMMMM OUTPUT:"+OUT)
```

### データファイル別サンプル

#### JRA3Q湿球温度

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/00.TOOL/NCL/NCL_RECIPE/NetCDF_OUT/12.SAMPLE_TWB_SFC.JRA3Q.ncl

#### J-OFURO3潜熱フラックス

https://gitlab.com/infoaofd/lab/-/blob/master/00.SKILL/00.TOOL/NCL/NCL_RECIPE/NetCDF_OUT/10.SAMPLE_1.DAILY_CLIM_SMTH_QA.ncl

# NCL Thermodynamic Functions

[[_TOC_]]

## wetbulb

Compute wetbulb temperature.

```
	function wetbulb (
		p    : numeric,  
		tc   : numeric,  
		tdc  : numeric   
	)

	return_val [dimsizes(p)] :  numeric
```

**Arguments**

*p*: A scalar or multi-dimensional array of pressures [**hPa**/mb].

*tc*: A scalar or multi-dimensional array of temperatures [**C**]. Must conform to *p*.

*tdc*: A scalar or multi-dimensional array of dew point temperatures [**C**]. Must conform to *p*.

## stickiness

```
stickiness(T: numeric, q: numeric)
```

**Arguments**

*T*: (dry bulb) temperature (C)

*q*: specific humidity (kg/kg)

```fortran
function stickiness(T: numeric, q: numeric)
begin
    v = (/ 1.199557787089589, -7.75269363e+02, 3.02416275e-01, 3.08627625e+00, \
          7.74095672e+03, 1.78241619e-03, -2.38012318e+02, -4.98714576e-01, \
          2.40170949e+01, 7.18600120e+03, -2.75682633e-05, 4.29814844e+02, \
          7.01883383e-03, -2.83672170e+02, -3.67109506e-01, 5.09430371e+00 /)

stickiness_value = -1.0 * (v(0) + v(1)*q + v(2)*T + v(3)*q*T + \
                   v(4)*q^2 + v(5)*T^2 + v(6)*(q^2)*T + \
                   v(7)*(T^2)*q + v(8)*(q^2)*(T^2) + \
                   v(9)*q^3 + v(10)*T^3 + v(11)*(q^3)*T + \
                   v(12)*(T^3)*q + v(13)*(q^3)*(T^2) + \
                   v(14)*(T^3)*(q^2) + v(15)*(q^3)*(T^3))
                   
return(stickiness_value)
end
```

## mixhum_ptd

Calculates the mixing ratio or specific humidity given pressure and dew point temperature.

```
	function mixhum_ptd (
		p         : numeric,  
		tdk       : numeric,  
		iswit [1] : integer   
	)

	return_val [dimsizes(p)] :  numeric
```

**Arguments**

*p*: An array of any dimensionality equal to pressure in Pa.

*tdk*: An array of any dimensionality equal to temperature dew point in K. Must be the same size as *p*.

*iswit*: An integer scalar that determines which variable is returned. 1 = mixing ratio (kg/kg), **2** = specific humidity (**kg/kg**). A negative value changes the units to g/kg.



## mixhum_ptrh

Calculates the mixing ratio or specific humidity given pressure, temperature and relative humidity.

```
	function mixhum_ptrh (
		p         : numeric,  
		tk        : numeric,  
		rh        : numeric,  
		iswit [1] : integer   
	)

	return_val [dimsizes(p)] :  numeric
```

**Arguments**

*p*: An array of any dimensionality equal to pressure in hPa (mb).

*tk*: An array of any dimensionality equal to temperature in K. Must be the same size as *p*.

*rh*: An array of any dimensionality equal to the percent relative humidity. Must be the same size as *p*.

*iswit*: An integer scalar that determines which variable is returned. 1 = mixing ratio (kg/kg), 2 = specific humidity (kg/kg). A negative value changes the units to g/kg.



## relhum_ttd

Calculates relative humidity given temperature and dew point temperature.

	function relhum_ttd (
		t    : numeric,  
		td   : numeric,  
		opt  : integer   
	)
	
	return_val [dimsizes(t)] :  numeric
**Arguments**
*t*: A multi-dimensional array containing the temperature (K). Array size and shape must match td.

*td*: A multi-dimensional array containing the dew point temperature (K). Array size and shape must match t.

opt: Set opt=0 for units of percent and 1 for units of fraction [0-1].


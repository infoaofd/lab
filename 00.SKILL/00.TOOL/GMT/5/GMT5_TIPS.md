https://staff.aist.go.jp/h.horikawa/GMT/GMT5.1guide/GMT5guide_v6-2.pdf

gmtdefaults -D

gmtset ANNOT_FONT_SIZE_PRIMARY = 14p
gmtset ANNOT_OFFSET_PRIMARY    = 0.2c

gmtset LABEL_FONT_SIZE         = 24p
gmtset LABEL_OFFSET            = 0.3c



```
xoffset=
yoffset=4
export LANG=C
curdir1=$(pwd)
now=$(date -R)
host=$(hostname)

time=$(ls -l ${in} | awk '{print $6, $7, $8}')
#time1=$(ls -l ${in1} | awk '{print $6, $7, $8}')

pstext -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} <<EOF \
-O >> $out
0 1.50  LM 0.0 9p $0 $@
0 1.35  LM 0.0 9p ${now}
0 1.20  LM 0.0 9p ${host}
0 1.05  LM 0.0 9p ${curdir1}
0 0.90  LM 0.0 9p Input: ${in} (${time})
EOF
```
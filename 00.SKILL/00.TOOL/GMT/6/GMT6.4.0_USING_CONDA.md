# GMT6.4.0_USING_CONDA

## INSTALL

### conda用の環境作成

```bash
$ conda config --prepend channels conda-forge
```

gmt6用の環境を作成する

```bash
$ conda create --name gmt6
```

```bash
$ conda activate gmt6                                        
(gmt6)
```

**作成した環境** (ここでは, gmt6) **のアクティベート**を忘れずに行う

### インストール

```bash
$ conda install -c conda-forge gmt
```

もしくは下記の3つのいずれか

```bash
$ conda install -c "conda-forge/label/cf201901" gmt
```

```bash
$ conda install -c "conda-forge/label/cf202003" gmt
```

```bash
$ conda install -c "conda-forge/label/dev" gmt
```



```bash
$ conda update -c conda-forge gmt
```


GMT4 Tips
=========================
C:\Users\atmos\Dropbox\MARKDOWN\GMT
GMT4_TIPS.md

[[_TOC_]]

gmtset
-----------------------------
```bash
gmtdefaults -D
```
```bash
gmtset ANNOT_FONT_SIZE_PRIMARY = 14p
gmtset ANNOT_OFFSET_PRIMARY    = 0.2c

gmtset LABEL_FONT_SIZE         = 24p
gmtset LABEL_OFFSET            = 0.3c

HEADER_FONT_SIZE        = 24p
HEADER_OFFSET           = -0.3c
```

欠損値
-----------------------------
```bash
awk '{if ($1!="#" && $2>-999) {printf "%7.3f %6.2f\n", $1, $2} \
else if($1!="#" && $2<-999) {printf "%7.3f >\n", $1 } }' $in
```

ヘッダー行の追加
-----------------------------
```bash
XOFFSET=-1；YOFFSET=5
CWD=$(pwd); NOW=$(date -R)；HOST=$(hostname)
TIME=$(ls -l ${IN} | awk '{print $6, $7, $8}')

pstext -JX20/4 -R0/1/0/1.5 -N  -X${XOFFSET:-0} -Y${YOFFSET:-0} <<EOF \
-O >> $OUT
0 1.5 9 0 1 LM $0 $@
0 1.4 9 0 1 LM ${NOW}
0 1.3 9 0 1 LM ${HOST}
0 1.2 9 0 1 LM ${CWD}
0 1.1 0 9 0 1 LM Input: ${IN} (${TIME})
EOF
# x   y   size   angle   fontno   justify   text
```



カレンダー
-----------------------------

http://gmt.soest.hawaii.edu/gmt4/gmt/html/GMT_Docs.html#x1-540004.4.3

http://kdo.la.coocan.jp/gmt33_bpbs.html

-Bsa1OSn
sa1O = 目盛り軸の下に1ヶ月ごとに月の名前を入れる。書式は、

gmtset PLOT_DATE_FORMAT "o y"
=> Month YYYY



### 一年の時系列

```
gmtset PLOT_DATE_FORMAT o TIME_FORMAT_PRIMARY Character ANNOT_FONT_SIZE_PRIMARY +14p
psbasemap -JX7/3 -R$rw/$re/$rs/$rn -Bpa1O/a10f5WSne -Bsa1Y/ -P -K -X1 -Y5 > $ps
```



pa1dfad = 1日毎に


```
-Bpa1df1d/a0.5f0.5g100:"[UNIT]":W \
```


pa7Rf1d = 目盛りは1日毎に, 数字は1週間毎に

```
-Bpa7Rf1d/a0.5f0.5:"[UNIT]":W \
```



目盛り軸の下に1ヶ月ごとに月の名前を入れる

```
-Bsa1OSn
```
sa1O = 目盛り軸の下に1ヶ月ごとに月の名前を入れる。書式は、

```
gmtset PLOT_DATE_FORMAT "o y"
```
のように指定する。この場合は、月の名前、年、で表示される

dd o y => 22 January 2011



月をJ, A, J, Oの一文字で、その下に年を書く

```
xanot=pa3Of1O
yanot=a1f.5g10000
yrange=-2.5/2.5

gmtset PLOT_DATE_FORMAT o TIME_FORMAT_PRIMARY Character ANNOT_FONT_SIZE_PRIMARY +12p 

anot=${xanot}/${yanot}:T@-anom@-${sp}[K]::.${TITLE}:WSne
psxy -R$range -J$size -W3  \
-B${anot} -Bsa1Y \
-Y6 -P -K > $out
```

月の名前を一文字にする (J,F,M, ...)

```bash
gmtset TIME_FORMAT_PRIMARY Character
```

月の名前を3文字にする (Jan, Feb, Mar, ...)

```bash
gmtset TIME_FORMAT_PRIMARY  abbreviated
```

サンプル
/work2/am/WRF/WRF.Post/NCL/PL1101/SAMPLE.ALONG.TRAJ
pl.tsrs.traj.sh

```
rangeT="2011-01-22T00:00:00/2011-01-23T00:00:00"
size=X5/1
#xrange=0/24
xanot=""
yanot=a100f50
anot=${xanot}/${yanot}EW
yrange=900/1050
range=$rangeT/$yrange

anot=${xanot}/${yanot}:"P${sp}[hPa]":eW

awk '{gsub("_","T",$1);print $1,$4}' $in1|\
psxy -R$range -J$size -W3 -B${anot} \
-Y7 -P -K > $out
```
gsubは日付・時刻データの_をTに置き換えている

横軸のみを書く
```
psbasemap -J$size -R$range -Bpa6hf3h -Bsa1DnS -O -K >>$out
```



### 欠損値を含む場合

-999以下の数を欠損値としたい場合
```
awk '{if($2>-999.){print $1,$2} else{print ">"} }' $IN|\
psxy -R$range -J$size -W3 -B${anot} -M \
-Y7 -P -K > $out
```



マニュアルのカレンダーに関する説明
http://gmt.soest.hawaii.edu/gmt4/gmt/html/GMT_Docs.html
Section 4.4.3   Cartesian time axes





A First Course in GMT　小技集
----------------------
https://sites.google.com/site/afcgmt/home/tips



### ベクトルのプロット

#### 軸を反転

[psxy] 線の種類を変える（破線、点線、一点破線、など）
[psxy][Error bar] エラーバーを書く
[psxy][calendar]月の表示を数字から、英語標記に変換する
[pstext]オフセット
[awk] データの最初と最後の点をプロットする

[psbasemap] 著作権を表示させる
[ゾンデ]ゾンデデータチェック用の図を作成
2011/01/22 0:43 に A M が投稿

明星電気のゾンデ(RS-06G)のCSVファイルから鉛直分布図を作成する

注：試作品です

作図用シェルスクリプト
plvphu.sh ダウンロード
gmtpar.sh ダウンロード
note.sh ダウンロード

サンプルデータ
F2010120311S7006249.CSV ダウンロード

実行例
$ plvphu.sh
Input /work1/aym/11.Work10/03.ECS/01.Obs_Amami/10.preliminary_analysis/43.vertical_profiles/02.sonde_data_launch/CSV/F2010120311S7006249.CSV
Output ps/F2010120311S7006249.ps

作図例　F2010120311S7006249.ps　ダウンロード

(投稿を編集) | 添付ファイル: F2010120311S7006249.CSV F2010120311S7006249.ps gmtpar.sh note.sh plvphu.sh
[grdgradient] 勾配を求める
2011/01/10 18:04 に A M が投稿   [ 2011/01/10 18:10 に更新しました ]

```
grdgradient $GRD1 -G$GRD2 -S$GRD3 -Dc -M #-V
```

GRD1 : 入力データのGRDファイル
GRD2 : 勾配ベクトルの向き（出力）
GRD3 : 勾配ベクトルの大きさ（出力）

-Dc : 勾配ベクトルの向きをデカルト座標系における値に換算
-M : 勾配ベクトルにおける長さの単位をメートルに換算

海底地形のデータから勾配を求めるシェルスクリプト
入力データ
J-EGG500_2_med.asc　ダウンロード

シェルスクリプト
gb.sh　ダウンロード
note.sh ダウンロード

結果の図
J-EGG500_2.ps ダウンロード

実行例
[Tue Jan 11 10:58:47 JST 2011]
[aym@aofd30 processor=x86_64]
[~/temp/gmt_renshu/grdgradient]
$ gb.sh

INPUT:  J-EGG500_2_med.asc
OUTPUT:  J-EGG500_2.ps


(投稿を編集) | 添付ファイル: J-EGG500_2.ps J-EGG500_2_med.asc gb.sh note.sh
[grdimage][grdgradient] 陰影をつける
2011/01/06 20:55 に A M が投稿   [ 2011/01/09 18:59 に更新しました ]

### ファイル名の設定
infle=jcope2_depth.asc
#### temporally files (gridded bathymetry data in netCDF format)
```
grd1=$(basename $infle .asc)_1.nc
grd2=$(basename $infle .asc)_2.nc
```



# 格子データの作成
```
blockmean $in1 $range0 -I$reso | surface -T0.8 $range0 -I$reso -G$grd1 #-V
```


# 勾配の計算
```
grdgradient $grd1 -Ne0.6 -A40 -G$grd2 #-V
```

色で水深を図示するとき、影をつけると見やすくなる

サンプルスクリプト

    gmtpar.sh  ダウンロード
    plotb.sh  ダウンロード
    note.sh ダウンロード

図

    jcope2_depth_sample.ps - 2011/01/06 20:59、m a (バージョン 1) 削除
    295KB ダウンロード

(投稿を編集) | 添付ファイル: gmtpar.sh jcope2_depth_sample.ps note.sh plotb.sh
[minmax] 範囲の自動指定
2010/11/17 1:29 に A M が投稿

 [-I[p]<dx>[/<dy>[/<dz>..]
 -I returns textstring -Rw/e/s/n to nearest multiple of dx/dy (assumes 2+ col data)
→ -Rオプションを自動的に設定できる。

実行例
$ minmax -I1 output/ektest03-2/ektest03-2_vp_i008j125.asc
-R23/24/-25/0

(投稿を編集)
[gmt][grdtrack] 任意の線に沿う断面図を作成する
2010/11/15 21:23 に A M が投稿   [ 2010/11/16 0:34 に更新しました ]

section.sh : 断面データの作成・プロット
dists.f90 : 緯度・経度を指定して2点間の距離を計算するFotranプログラム (section.shの中で使用）

使用例
[Tue Nov 16 16:56:29 JST 2010]
[aym@aofd30 processor=x86_64]
[~/11.Work10/36.vertical2d/21.VertSectBathymetry]

解凍
$ tar xzvf section.tar.gz

プログラムのコンパイル
$ ifort -o dists dists.f90

スクリプトの実行
$ section.sh test23-01dyke_bathy.txt test23-01dyke_bathy_2010-11-16_1.txt
test23-01dyke_bathy.txt : 地形データ
test23-01dyke_bathy_2010-11-16_1.txt : 断面の経度・緯度を指定するファイル

Input : test23-01dyke_bathy.txt
Output : ps/test23-01dyke_bathy_section_2010-11-16.ps
130.202 33.072
0

結果の例　
test23-01dyke_bathy_2010-11-16_1_section_2010-11-16.ps ダウンロード

参考：圧縮時のログ
$ tar czvf section.tar.gz cntr.sh dists.f90 gmtpar.sh section.sh test23*.txt
cntr.sh
dists.f90
gmtpar.sh
section.sh
test23-01dyke_bathy.txt
test23-01dyke_bathy_2010-11-16_1.txt
test23-01dyke_bathy_2010-11-16_2.txt
test23-01dyke_bathy_2010-11-16_3.txt

(投稿を編集) | 添付ファイル: section.tar.gz test23-01dyke_bathy_2010-11-16_1_section_2010-11-16.ps
[gmt][psxy] 球面上の2点を結ぶ線を円弧として書かない
2010/11/14 22:12 に A M が投稿   [ 2010/11/14 22:39 に更新しました ]

-A  線を大円の円弧として描かない(直線で描く)

例
大円の円弧として描いた場合：ダウンロード
書かない場合： ダウンロード

(投稿を編集) | 添付ファイル: grid_test.ps mesh_test.ps
[gmt][psxy][vector]ベクトルの描画
2010/10/27 0:10 に A M が投稿   [ 2011/01/16 22:03 に更新しました ]

-Sv
ベクトル(矢印)を描きます。データファイルの3、4番目の桁に矢印の角度と長さを指定する必要があります。 パラメータの与え方は-Sv[arrowwidth/headlength/headwidth][nnorm](矢軸の幅、矢尻の長さ、矢尻の幅)となります。 normが指定されると、 矢印がその値より短い場合矢尻の大きさが調整されます。

-SV
-Svと同じですが、角度の代わりに方位(北から東回り)を与えます。

風向きの場合、風が吹いてくる方向をあらわしているので注意してください！
「180度+方位」にすると、正しい風向きをあらわすようになります。
awkをつかうとすると、下記のような感じになるでしょう。
# Vector Size
```
VSIZE=10 #0.1  # ms-1
VSCALE=0.05 # inch
VFACTOR=`echo "scale=6; ${VSCALE}/${VSIZE}" | bc`

awk -v SC="$VFACTOR" '{if ( $1 !="#" && NR%20 == 0) \
{printf "%12.6f %12.6f %10.3f %10.4f\n", $6, $8, 180+$9, $10*SC} }' $in |\
 psxy -R$range  -JX$size \
 -SV0.01/0.1/0.03n0.2 -G0 \
  -O -K \
  >> $out


size=4
cntr_lon=135

xanot=a10f1
yanot=a5f1
```



# Vector Size
```
VSIZE=0.5 #0.1  # ms-1
VSCALE=0.05 # inch
VFACTOR=`echo "scale=6; ${VSCALE}/${VSIZE}" | bc`

awk -v SC="$VFACTOR" '{if (NR > 1 && $1 !="#") \
{printf "%12.6f %12.6f %10.3f %10.4f\n", $1, $2, \
$3, $4*SC} }' $in3 | psxy -R -JQ${cntr_lon}/${size} \
 -Sv0.01/0.1/0.03n0.2 -G180 \
 -O -K >> $out
```

ベクトルの大きさが負の値の場合の処理

```
VSIZE=1  # ms-1
VSCALE=0.2 # inch
VFACTOR=$(echo "scale=6; ${VSCALE}/${VSIZE}" | bc)
```

.....

# Vector
```
awk -v SC="$VFACTOR" '{if (NR > 1 && $1 !="#") \
  {if ($4 > 0) \
    {printf "%12.6f %12.6f %10.3f %10.4f\n", $2, $3*(-1), \
    0, $4*SC} \
  if ($4 < 0) \
    {printf "%12.6f %12.6f %10.3f %10.4f\n", $2, $3*(-1), \
    180, $4*(-1)*SC} \
  } \
}' $IN | \
psxy -R -JX \
 -Sv0.01/0.1/0.03n0.2 -G0 \
 -O -K >> $OUT
```

u, v →角度、ベクトルの大きさ（極座標）に変換
# Vector Size
```
VSIZE=100 #0.1  # cms-1
VSCALE=0.1 # inch
VFACTOR=`echo "scale=6; ${VSCALE}/${VSIZE}" | bc`

awk -v SC="${VFACTOR}" '{if ( ($1 !="#") && ((NR % 2)==0) ) \
  printf "%13.7f %12.7f %10.3f %10.4f\n", \
  $1, $2, (180.0/3.1415926535)*atan2($4, $3), SC*sqrt($3*$3+$4*$4)}' ${in2} |\
  psxy -R${range} -J${maptype}${size} -Sv0.01/0.1/0.03n0.2 -G0 \
  -O -K >> $out
```

(投稿を編集)
[gmt][surface]　バイナリデータの入力
2010/10/26 3:36 に A M が投稿

[Tue Oct 26 19:35:18 JST 2010]
[aym@aofd30 processor=x86_64]
[~/11.Work10/15.HeatBudjet/51.jcope2/01.test_read/05.gmt_bin_io]
$ ls
a2b*  a2b.f90  test09.bin  test09.grd  test09.ps  test09.sh*  test09.txt

$ ifort a2b.f90 -o a2b

$ ./a2b
Input file: test09.txt
Output file: test09.bin

$ ./test09.sh
INPUT:
test09.bin
OUTPUT:
test09.ps　ダウンロード

```
$ cat -n test09.sh > tmp.txt
```

    95    surface ${in} -R${range} -T${tension} -I${reso} -fg -G${grdfile} -bis

−bi
	
	

Selects binary input. Append s for single precision [Default is d (double)].

Uppercase S or D will force byte-swapping.

Optionally, append ncol, the number of columns in your binary input file if it exceeds the columns needed by the program.

Or append c if the input file is netCDF.

Optionally, append var1/var2/... to specify the variables to be read. [Default is 3 input columns].

(投稿を編集) | 添付ファイル: a2b.f90 test09.ps test09.sh test09.txt
[gmt] pstext
2010/10/21 0:48 に A M が投稿

```
pstext << end -R -JQ -O -K >> $out
120 45 14 0 1 LM moji
end
```

120 = x座標
45 = y座標
14 = フォントサイズ
0 = 文字を傾ける場合の角度
1 = ?
LM = 文字の位置 (L=Left, M=Middle)
moji = 書きたい文字列
end = endより上の行がpstextの入力データとなる

[gmt][psmask][grdmask] データの無いところをマスクする
2010/10/19 1:28 に A M が投稿

スクリプト：cont.sh ダウンロード
図の例：　sstm.20100101.ps ダウンロード

# 陸地のマスク
```
landmask=landmask.grd
maskedgrd=masked.grd
grdlandmask -R$range -Dl -I$reso -N1/NaN -G$landmask  #-V
grdmath $grd $landmask OR = $maskedgrd
```



# データが無いところをマスク
  >> ```
  >> psmask $in -R$range -I$reso -JQ${cntr_lon}/${size}  \
  >> -X1.5 -Y2.5 \
  >> -P -K > $out
  >> grdimage $grd -R$range -JQ${cntr_lon}/${size} \
  >>  -C$cpt \
  >>  -O -K \
  >
  >>   >> $out
  >>   >> psmask -C -O -K >> $out
  >>
  >>   ```
# 最後もpsmask必要

(投稿を編集) | 添付ファイル: cont.sh sstm.20100101.ps
[gmt][surface] データの下限（上限）を設定
2010/07/13 0:50 に A M が投稿   [ 2010/07/21 4:16 に更新しました ]

surface コマンドのオプション

入力データに物理的下限（上限）がある場合につかう。補間すると，下限（上限）をオーバーしてしまうことがあるので，その場合にこれを使う。

-Ll0.0 : 出力データの下限を0.0に設定
入力データは絶対に負にならないことがあらかじめ分かっているとき。

-Lu10.0: 出力データの上限を10.0に設定


(投稿を編集)
[psxy] 色つきのベクトル
2010/06/25 2:44 に A M が投稿   [ 2010/06/25 2:51 に更新しました ]

入力データの並びを下記のようにする

x座標，y座標, 実際のベクトルの大きさ（データ）, 角度, 描画するベクトルの長さ

cptファイルを参照し，実際のベクトルの大きさの値に応じて色分けする。

描画するベクトルの長さは別途指定する。

VECSIZE=0.07

例(これだけでは動かない)

```
SIZE=3
mapopt=M${SIZE} #Q130.0/${SIZE}
VECSIZE=0.07

#....

col1=3
col2=4
awk -v vsize="$VECSIZE" \
'{if (NR > 1 && $1 !="#" && $'"$col1"' !="-999.000" && $'"$col2"' != "-0.999E+03" && \
  $'"$col1"' != "0.000" && $'"$col2"' !="0.000E+00" ) \
  printf "%12.6f %12.6f %10.3f %10.4f %10.4f\n", \
  $1, $2, $'"$col2"', $'"$col1"', vsize}' $IN |
  psxy -R$RANGE -J${mapopt} \
    -Sv0.01/0.1/0.05n0.2 -W0.05 \
    -C$cpt \
    -K -P -X1.5 -Y1.5 > $OUT
```

(投稿を編集)
[gmt][地図投影]JQオプション
2010/06/21 23:10 に A M が投稿   [ 2010/06/21 23:13 に更新しました ]

Cylindrical Equidistant
−JQ[lon0/[lat0/]]width

lon0は経度
lat0は緯度（なくても描ける）
widthは図の横幅

日本ぐらいの緯度だと，メルカトル図法（−JMwidth）よりも縦を短くできる（縦横比が１：１に近づく）。

(投稿を編集)
[awk][ベクトル][変換] (u, v)を(angle, magnitude)に変換する
2010/06/20 18:52 に A M が投稿   [ 2010/06/20 18:54 に更新しました ]


$ cat va.sh
read u v
echo u= $u , v=  $v
echo $u $v |  awk '{printf "%12.6f %12.6f\n", (180.0/3.1415926535)*atan2($2,$1),sqrt($1*$1+$2*$2)}'

$ va.sh
1 0
u= 1 , v= 0
    0.000000     1.000000

$ va.sh
0 1
u= 0 , v= 1
   90.000000     1.000000

$ va.sh
-1 1
u= -1 , v= 1
  135.000000     1.414214


[Mon Jun 21 10:50:35 JST 2010]
[aym@oceani18 processor=x86_64 kernel=Linux 2.6.18-164.15.1.el5 ]
[~/11.Work10/35.EkmanNumber/01.TestEstimateEkman/02.Test02/post/83.tdmn]


(投稿を編集)
[minmax][makecpt][bc] cptファイルの値の範囲の自動設定
2010/06/17 2:54 に A M が投稿   [ 2010/06/17 2:55 に更新しました ]

```
tmp=$(minmax -I -C $in)

echo $tmp
i=0
for v in $tmp; do
  i=$(expr $i + 1)
  r[$i]=$v
  echo ${r[$i]}
done

w=${r[1]}
e=${r[2]}
s=${r[3]}
n=${r[4]}
```



# set max and min values
```
minabs=$(echo "scale=6; sqrt(${r[5]}^2)" |bc )
maxabs=$(echo "scale=6; sqrt(${r[6]}^2)" |bc )
x=$(echo "scale=6;if( $maxabs > $minabs ) 1 else 0" | bc)
if [ $x = 1 ]; then
  max=$maxabs
  min=$(echo "scale=6; (-1)*$maxabs" |bc )
else
  max=$minabs
  min=$(echo "scale=6; (-1)*$minabs" |bc )
fi

d=$(echo "scale=6; ($max - $min)/50.0" | bc )

makecpt -Cno_green -T$min/$max/$d > $cpt
```

(投稿を編集)
[gmt][指数] 指数を上付き文字で表す
2010/05/26 19:28 に A M が投稿   [ 2010/05/28 0:24 に更新しました ]

例: 1.E5を105に変換する。

```
 echo  $a
1.E5

$ echo $a | sed "s/${a#1.E}/\@+${a#1.E}\@+/" | sed "s/1.E/10/"
10@+5@+

$ b=$(echo $a | sed "s/${a#1.E}/\@+${a#1.E}\@+/" | sed "s/1.E/10/")

$ echo $b
10@+5@+
```

gmtでは上付き文字を@+を使ってあらわす。@+で囲まれた文字列は上付き文字となる。

(投稿を編集)
[gmt][線グラフ][psxy] 線グラフを描く
2010/05/21 2:17 に A M が投稿   [ 2010/05/21 2:22 に更新しました ]

シェルスクリプトのサンプル
lplot.sh (本体のファイル): ダウンロード
gmtset.sh (gmtのパラメータを設定): ダウンロード
argcheck.sh (引数のチェック，入力ファイル名の設定): ダウンロード
setaxis.sh (座標軸の最小・最大値の自動設定): ダウンロード
fileinfo.sh　(図に入力ファイル名，作業ディレクトリ名，作成日時を書く): ダウンロード

実行例
$ lplot.sh randomwind.txt
Input: randomwind.txt
Output: randomwind.ps
xmin= 0
xmax= 38250
ymin= -2.89694
ymax= 3.37354
pstext: Record 6 is incomplete (skipped)

作図例：ダウンロード

[Fri May 21 18:18:13 JST 2010]
[aym@aofd32 processor=x86_64 kernel=Linux 2.6.18-164.6.1.el5xen ]
[~/11.Work10/24.TestSW1d/05.RandomWind/05.Fortran]


(投稿を編集) | 添付ファイル: argcheck.sh fileinfo.sh gmtset.sh lplot.sh randomwind.ps setaxis.sh
[gmt][minmax] 欠測値を含むデータの最大・最小値を見つける
2010/05/17 23:05 に A M が投稿   [ 2010/05/17 23:09 に更新しました ]

欠測値がrmissで設定してある場合

例
colで指定された列の最大値がmax1に，最小値がmin1に代入される．

```
min1=$(awk -v miss=$rmiss '{if($1 !="#" && $'"$col"' !=miss) print $'"$col"' }' $in| \
  minmax -C | awk '{print $1}' )
max1=$(awk -v miss=$rmiss '{if($1 !="#" && $'"$col"' !=miss) print $'"$col"' }' $in| \
   minmax -C | awk '{print $2}' )
```

注：#で始まる行はコメント行と解釈している．

[Tue May 18 14:40:13 JST 2010]
[aym@oceani18 processor=x86_64 kernel=Linux 2.6.18-92.el5 ]
[~/11.Work10/33.Budjet/03.sigma_to_z/03.Test_Interpolation/post/21.Sigma2z/post]

(投稿を編集)
[linux][bash]複数ファイルの描画
2010/05/13 2:49 に A M が投稿   [ 2010/05/13 2:54 に更新しました ]

[Thu May 13 18:48:48 JST 2010]
[aym@aofd32 processor=x86_64 kernel=Linux 2.6.18-164.6.1.el5xen ]
[~/11.Work10/24.TestSW1d/03.TestFwdModel/post]

入力ファイル（複数）
$ ls ../output/cyc01
cyc01_i00000.txt 
略
cyc01_i00500.txt

xsall.shは，内部で1dswxs.sh（描画を行うスクリプト）を呼び出す
$ xsall.sh cyc01
Input: ../output/cyc01/cyc01_i00000.txt
Output: ps/cyc01/cyc01_i00000.ps
略
Input: ../output/cyc01/cyc01_i00500.txt
Output: ps/cyc01/cyc01_i00500.ps

図のファイル（結果）
$ ls -1 ps/cyc01
anim_cyc01.gif
cyc01_i00000.ps
略
cyc01_i00500.ps

シェルスクリプトのサンプル（GMT使用）
xsall.sh: ダウンロード
1dswxs.sh: ダウンロード

[animation][動画][convert][Image Magick] アニメーションの作成
2010/04/07 1:41 に A M が投稿   [ 2010/06/23 2:46 に更新しました ]

convertを使う.

元となる画像ファイルの例
$ ls
test01m0000.ps  test01m0011.ps  test01m0022.ps  test01m0033.ps 
...中略
test01m0050.ps

コマンド例
$ convert -loop 0 -delay 20 test01m*.ps anim_test01.gif

コマンド例の説明
test01m*.ps: 複数の入力ファイル.*の部分は番号が入っていることが仮定されている．
anim_test01.gifという動画ファイルができる．
-loop 0 : 繰り返し再生させる．
-delay 20 : 1枚の画像を100分の20秒間表示する．

画像を拡大したいときは，-resizeオプションを使う
-resize 150%
で拡大率150%の図が作成される．

-resize 600 -density 600
とすると,600dpi

(投稿を編集)
[gmt][気象] 矢羽を描くためのパッチ
下記に引っ越しました。
https://sites.google.com/site/afcgmt/home/install/wind_barb


[gmt][psxy]色見本
2010/02/14 18:18 に aofd info が投稿   [ 2010/06/28 22:21 に A M さんが更新しました ]

色見本を見るためには，ghostscriptとghostviewというソフトが必要．

色見本1:ダウンロード 青・緑
色見本2:ダウンロード 茶色・赤・紫
色見本3:ダウンロード 肌色・緑・青（パステルカラー）
色見本4:ダウンロード 青・緑系統
色見本5:ダウンロード 茶色から赤系統
色見本6:ダウンロード ピンクから紫
色見本7:ダウンロード 灰色系統(濃いもの)
色見本8:ダウンロード 灰色系統(薄いもの)


http://www-seis.planet.sci.kobe-u.ac.jp/~kakehi/GMT/kunugi/cp_cps.html

| 添付ファイル: cp_cps_1.ps cp_cps_2.ps cp_cps_3.ps cp_cps_4.ps cp_cps_5.ps cp_cps_6.ps cp_cps_7.ps cp_cps_8.ps
[gmt][psxy]シンボルの種類
2010/02/14 18:03 に aofd info が投稿   [ 2010/02/14 18:06 に A M さんが更新しました ]

-Sオプションで指定

A(星)、 C(円)、 D(菱形)、 H(六角形)、 I(逆三角形)、 S(正方形)、 T(三角形)が指定された場合はそれぞれの記号を描く．

これらの指定が大文字の場合はsizeを直径とする円と同じ面積を持つ大きさとなる．

小文字の場合はsizeを直径とする円が外接する大きさとなります。 symbolを指定しない場合は、データファイルの最後の桁に記号を指定する．

sizeを指定しない場合は、データファイルの3番目の桁を大きさとする．

http://www5.plala.or.jp/kashima/gmt/gmt_psxy.html

[gmt] データの最初と最後の点をプロットする
2010/02/02 23:14 に A M が投稿   [ 2010/02/03 5:20 に更新しました ]

# 始点
```
awk '{if ($1 != "#" && $1 == "1") print $2, $3 }' $in | \
psxy -JM$SIZE -R$RANGE \
-M -G255/0/0 -Sc0.05 \
-O -K >> $out
```



最初のデータの最初の行が1で始まっていることを前提としている．

データの例

データ番号  経度　緯度　

   1                130   32
   2                131   33

最後の点

最後の行が何行目か調べて，last_lineに代入

```
last_line=$(awk 'END { print NR }' $in)
awk -v last=${last_line} '{if ( NR == last) print $2, $3 }' $in | \
psxy -JM$SIZE -R$RANGE \
-M -G0/255/0 -Sc0.05 \
-O -K >> $out
```

(投稿を編集)
[gmt] 水深データの等値線と色分け
2010/01/26 21:13 に aofd info が投稿   [ 2010/01/26 21:26 に A M さんが更新しました ]

シェルスクリプト：bath.sh ダウンロード
結果の図：ecs_topo2.ps ダウンロード

注：データは容量が大きいのでおいていない．

| 添付ファイル: bath.sh ecs_topo2.ps
[gmt] 陸地をマスクするためのファイル作成
2010/01/19 21:56 に A M が投稿   [ 2010/01/23 17:29 に更新しました ]

grdlandmask
http://marine-geophysics.hp.infoseek.co.jp/gmt/GMT4.1.4/grdlandmask.html
http://triton.ori.u-tokyo.ac.jp/~okino/gmtscripts/maskedgrd.html

例

```
#/bin/sh

IN=$1

RANGE=129:45/130:43/32:20/33:18
xanot=a40mf10m
yanot=a20mf10m
SIZE=5
type=M
reso=0.5m/0.5m
tension=0.2
cpttable=no_green

cpt=cptsal.txt
grd=sal.grd
maskedgrd=maskedsal.grd
makecpt -C${cpttable} -T$min/$max/$int > $cpt

awk '{ if($1 != "#") print $1, $2, $7}' $IN |
  surface -R$RANGE -I$reso -T$tension -G$grd
```



# 陸地の全ての格子点をNaN に、水域の格子点を 1 に設定する。
```
landmask=$landmask.grd
grdlandmask -R$RANGE -Df -I$reso -N1/NaN -G$landmask #-V
```



# 陸地をマスクする．
```
grdmath $grd $landmask OR = $maskedgrd

grdimage $maskedgrd -J${type}$SIZE -R$RANGE -C$cpt \
-P -K -X1.5 -Y2.5 > $OUT
```

(投稿を編集)
[gmt][shell script] gmt用シェルスクリプトの雛形
2010/01/14 17:50 に aofd info が投稿   [ 2011/01/16 20:54 に A M さんが更新しました ]

```
#!/bin/sh
#

# GMTのパラメータの設定 (version 4以上対象）

#

# 長さの単位はインチとする

gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 18
gmtset TICK_PEN 4

# 地図の縦横軸に縞々を使わない

gmtset BASEMAP_TYPE PLAIN

# 紙のサイズはA4

gmtset PAPER_MEDIA A4

# 地図の°の記号

gmtset DEGREE_SYMBOL = degree # Available for ver. 4 or later

# 空白文字の設定

sp="\040" # White space

# =の記号

eq="\075" # equal

# 温度の°の記号

deg="\260" #deg="\312"  # degree symbol

# 色のRGB値を設定 (線や点に色をつけるときRGB値を直接指定するより分かりやすい)

red="255/0/0"
green="0/255/0"
blue="0/0/255"

# 数字の出力フォーマット(project, grd2xzy等で使う)

gmtset D_FORMAT %lg #デフォルト(規定値)

# 桁数を指定（例：123.45678)

#gmtset D_FORMAT %12.6f

# 状況に応じて

#gmtset D_FORMAT %12.5g

#
```


# 引数のチェック
```
#
if [ $# == 0 ]; then
echo Error in $0 : No argument
echo Usage: $0 FILENAME
echo
exit 1
fi
```



# 引数が複数の場合は以下を使うと良い
```
#num_arg=2 #num_arg: 引数の数
#if [ $# != ${num_arg} ]; then

# echo Error in $0 : Wrong number of arguments

# echo Usage: $0 In1 In2 ...

# echo In1:

# echo In2:

#fi
```



# 最初の引数を入力ファイル名とする．
```
in=$1

if [ ! -f $in ]; then
echo Error in $0 : No input file, $in
exit 1
fi
```



入力ファイルの拡張子を指定

拡張子が3文字であることを仮定しているので注意

```
ext="txt"
```



入力ファイル名から拡張子を取得

拡張子が3文字であることを仮定しているので注意

```
ext_infile=${in:${#in}-3:${#in}}
```


# 拡張子のチェック
```
if [ ${ext_infile} != ${ext} ]; then
echo "Error in $0 : Extension of the input file name must be ${ext}."
exit 1
fi

out=$(echo $in | sed -e "s/\.${ext}/.ps/")
```


# 入力ファイルを上書きしないよう，念のためチェック
```
if [ $in == $out ]; then
echo Error in $0 : Input file name is identical to output file name.
exit 1
fi

echo Input: $in
echo Output: $out
```





図を作成した時刻，現在のディレクトリ名，入出力ファイル名，コメントなどを図に入れる．

```
comment=$(ls -l $in | awk '{print $6, $7, $8}') # コメント
currentdir=$(pwd)
if [ ${#currentdir} -gt 90 ]; then
curdir1=${currentdir:1:90}
curdir2=${currentdir:91}
else
curdir1=${currentdir}
curdir2="\ "
fi
now=$(date)
host=$(hostname)
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-0} << EOF -O >> $out
0 1.1 9 0 1 LM $0 $@
0 0.95 9 0 1 LM ${now}
0 0.80 9 0 1 LM ${host}
0 0.65 9 0 1 LM ${curdir1}
0 0.50 9 0 1 LM ${curdir2}
0 0.35 9 0 1 LM Input: ${in} (${comment})
0 0.1 9 0 1 LM Output: ${out}
EOF
```


[gmt] A4の紙の大きさ（インチ）
2010/01/14 4:24 に A M が投稿   [ 2010/01/14 4:25 に更新しました ]

横8.27インチ
縦11.7インチ

(投稿を編集)
[gmt] pscoast
2009/11/19 2:01 に aofd info が投稿   [ 2010/01/14 0:57 に A M さんが更新しました ]

```
-L[f][x]lon0/lat0/slat/length[m|n|k]
```

lon0/lat0を中心とする簡単なスケールを描画する。 装飾スケールにするには-Lf (fancyの意味)を、座標をx/yで与えるには-Lxとすればよい。 スケールは緯度slatで計算される。lengthはデフォルトではk、つまりkm単位のスケールの長さで、mを付けるとマイル、nを付けると海里になる。

[gmt] インストール時のエラー
2009/11/16 0:15 に aofd info が投稿   [ 2010/01/14 0:57 に A M さんが更新しました ]

[Mon Nov 16 17:19:11 JST 2009]

GMT4.5.1 w/netCDF3.6.2

エラーその１
エラーメッセージ
usr/bin/ld: skipping incompatible /usr/local/lib/libnetcdf.a when searching for
 -lnetcdf

netCDFが64ビットでコンパイルされていて，gmtが32ビットでコンパイルされている可能性がある。

対応策
GMTparam.txtの
GMT_64=n
を
GMT_64=y
に変更

参考
http://www.unidata.ucar.edu/support/help/MailArchives/netcdf/msg04069.html

エラーその２
エラーメッセージ
while loading shared libraries: libnetcdf.so.4: cannot open shared object file: No such file or directory

原因
不明

対応策
netCDFのコンパイルにはinstall_gmtを使わないで，netCDFとGMTを別々にコンパイルする。

参考
http://old.nabble.com/problems-linking-gmt-commands-with-netcdf-libraries-td10375686.html
対応策

```
$ export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/netcdf3.6/lib
```

http://nhh.mo-blog.jp/ttt/2007/12/unixos_ld_libra_63dd.html
LD_LIBRARY_PATHは、リンクされる共有ライブラリの検索パスを強制的に変更するために使うものです。

[gmt][awk] 10のべき乗でスケーリング
2009/09/10 5:13 に A M が投稿   [ 2010/01/14 0:58 に更新しました ]

```
pow=-2
fac=$(echo "scale=15; e((-1)*${pow}*l(10))" | bc -l)

awk -v f=$fac '{if ($1 != "#") print $3, $4*f }' $IN | psxy -JX${SIZE} -R${RANGE} \
  -W6/255/0/0 -B${XLABEL}:"x [km]":/${YLABEL}:"@~h@~ [10@+$pow@+m]":WsNe \
```

(投稿を編集) | 添付ファイル: jul_neap_01_egf.ps jul_neap_01_egf.txt lg.sh
[gmt] 折れ線グラフ　色分け
2009/08/18 1:27 に A M が投稿   [ 2010/01/14 0:58 に更新しました ]

多数の折れ線を色分けする


(投稿を編集) | 添付ファイル: lg2.sh testtb05_tb_k020.ps testtb05_tb_k020.txt
[gmt] 等値線 (コンター, contour)
2009/08/04 20:27 に aofd info が投稿   [ 2010/01/14 0:59 に A M さんが更新しました ]

サンプル
cont.sh　シェルスクリプト
testic1.txt　サンプルデータ
testic1.ps  出力結果のサンプル

| 添付ファイル: cont.sh testic1.ps testic1.txt
[gmt] 空白を入れる
2009/07/31 2:23 に A M が投稿   [ 2010/01/14 1:00 に更新しました ]

```
sp="\040"  # White space
title="tidamp=${tidamp}${sp}${sp}${sp}j=${idx}${sp}${sp}${sp}${timeout}"

.....

pstext -JX6/1.2 -R0/1/0/1.2 -N -Y4 << EOF -O >> $OUT
0 0.35  9 0 1 LM $title
EOF
```

(投稿を編集)
[gmt] パラメータの設定
2009/02/22 18:13 に A M が投稿   [ 2010/11/15 0:52 に更新しました ]

#
# GMTのパラメータの設定 (version 4以上対象）
```
#

# 長さの単位はインチとする

gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 18
gmtset TICK_PEN 4

# 地図の縦横軸に縞々を使わない

gmtset BASEMAP_TYPE PLAIN

# 紙のサイズはA4

gmtset PAPER_MEDIA A4

# 地図の°の記号

gmtset DEGREE_SYMBOL = degree # Available for ver. 4 or later

# 空白文字の設定

sp="\040"  # White space

# =の記号

eq="\075"  # equal

# 温度の°の記号

deg="\260"  # degree symbol (ISOLatin1+). See Chart of octal codes for characters GMT Tech. Ref.

# 数字を出力する場合(project, grd2xzy等)

#

# 正確に   123.45678

#gmtset D_FORMAT %12.6f
#
#Default
gmtset D_FORMAT %lg
#
#状況に応じて
#gmtset D_FORMAT %12.5g
```

上記をファイルにまとめたもの(gmtpar.sh):  ダウンロード

使い方
シェルスクリプトの中に
. gmtpar.sh
という1行を入れると、上の内容がインクルードされる。

(投稿を編集) | 添付ファイル: gmtpar.sh
[gmt] grdcontourによる等値線作図
2009/02/14 19:47 に A M が投稿   [ 2010/01/14 1:01 に更新しました ]

manページの日本語訳
http://www5.plala.or.jp/kashima/gmt/gmt_grdcontour.html

実践！ GMT その1（アメダスデータを使ってみよう）
(grdcontourの基本的な使用方法)
http://www.geocities.jp/ne_o_t/GMT-USE/use-medium/gmt-medium1.htm

アスキー水深データからグリッドをつくる
http://ofgs.ori.u-tokyo.ac.jp/~okino/gmtscripts/asciigrid.html

生データのないところをマスクしたグリッドをつくる
http://ofgs.ori.u-tokyo.ac.jp/~okino/gmtscripts/maskedgrd.html



(投稿を編集)
[gmt][awk] awkで書式付出力
2009/02/14 18:20 に A M が投稿   [ 2010/01/14 1:00 に更新しました ]

例：
入力ファイル名はinと仮定

    #から始まる行はコメント行と見なす
    1列目と2列目を1000で割って,小数点以下第6位まで出力
    3列目は少数点以下第3位まで出力

上記の結果をpsxyに渡す

 

```
awk '{if ($1 != "#") \
   {printf "%12.6f %12.6f %10.3f\n", $1/1000., $2/1000., $3} }' in |
   psxy ...
```

(投稿を編集)
[gmt] 役に立つリンク
2009/02/14 17:18 に A M が投稿   [ 2010/01/14 1:41 に更新しました ]

関東学院大学工学部　前田 直樹先生のページ
http://home.kanto-gakuin.ac.jp/~nmaeda/gmtpk/
http://home.kanto-gakuin.ac.jp/~nmaeda/labo/gmt_ind.shtml
主な内容
散布図(サンプル) 両対数プロット，　片対数プロット
地図の作図
震央分布図， 　震央分布図（その２）， 　
いろいろなカラーパレット
グリッドデータ テストデータの作成， 　カラーパレットの作成， 　段彩図， 　
三次元の透視図 標高データ（世界） TerrainBase（NOAA)， 　
段彩図， 　陰影段彩図 発震機構 深さ分布 project， 　深さ分布

GMT備忘録　(kashimaさんのページ)
http://www5.plala.or.jp/kashima/gmt/gmt.html
主なコマンドの日本語マニュアル

man GMT
使用法のメモ
http://wwwod.lowtem.hokudai.ac.jp/comp/gmt.html

(投稿を編集)
[gmt] 図に時刻、ホスト名、ディレクトリ名などを加える
2009/02/13 19:58 に A M が投稿   [ 2011/01/11 21:05 に更新しました ]

# Print time, current working directory and output filename
```
currentdir=$(pwd)
if [ ${#currentdir} -gt 90 ]; then
  curdir1=${currentdir:1:90}
  curdir2=${currentdir:91}
else
  curdir1=$currentdir
  curdir2="\ "
fi
now=$(date)
host=$(hostname)
comment=" "
pstext -JX6/1.2 -R0/1/0/1.2 -N -X${xoffset:-0} -Y${yoffset:-5} << EOF -O >> $out
0 1.1   9 0 1 LM $0 $@
0 0.95  9 0 1 LM ${now}
0 0.80  9 0 1 LM ${host}
0 0.65  9 0 1 LM ${curdir1}
0 0.50  9 0 1 LM ${curdir2}
0 0.35  9 0 1 LM Input: ${in}  Output: ${out}
0 0.1   9 0 1 LM ${comment}
EOF
```

参考

```
${変数 :-value }
```

変数が空でないとき→変数 の値が使われる
変数が空のとき→valueが変数の値になる

http://www.atmarkit.co.jp/flinux/rensai/shell05/parameter.html

GMT for windows
2009/01/27 5:57 に aofd info が投稿   [ 2009/05/25 2:38 に A M さんが更新しました ]

http://gmt.soest.hawaii.edu/gmt/gmt_windows.html

    GMT_basic_install.exe Basic GMT distribution 

をダウンロードしてダブルクリックすると、自動的にインストールされる。デフォルトだと、
c:\programs\gmtにインストールされる。

パスが通っていない場合があるので、
コントロールパネル→システム→詳細設定→環境変数
を選択し、

ユーザー環境変数のpathに
c:\programs\gmt\bin
を加える。それでもだめな場合、
システム環境変数のpathに
c:\programs\gmt\bin
を加える(管理者のみ変更可)。

高解像度の海岸線データが必要な場合、さらに

    GSHHS_highfull_install.exe High and Full resolution GSHHS coastlines

をダウンロードしてインストールする。


コントロールパネル→詳細設定→環境変数→ユーザー変数
変数名　GMT_SHAREDIR
変数値　C:\programs\GMT\share

[gmt] 引数のチェックと出力ファイル名の設定
2009/01/24 20:44 に aofd info が投稿   [ 2010/01/14 1:08 に A M さんが更新しました ]

シェルスクリプトでGMTを動かす場合のサンプル

```
#!/bin/sh

if [ $# == 0 ]; then
  echo Error in $0 : No argument
  echo Usage: $0 FILENAME
  echo
  exit 1
fi

#j 引数が複数の場合は以下を使うと良い

# if [ $# == 0 ]; then

#   echo Error in $0 : Wrong number of arguments

#   echo Usage: $0 In1 In2 ...

#   echo In1:

#   echo In2:

# fi
```



# 最初の引数を入力ファイル名とする．
```
in=$1

if [ ! -f $in ]; then
 echo Error in $0 : No input file, $in
 exit 1
fi
```





projectコマンド

緯度・経度のデータをデカルト座標系に変換する
[aym@oceani18]
[~/10.Work09/13.FVCOM_Ariake2D/03.Preprocess/03.Coastline]

```
$ cat > aaa.txt
```

130.5 32.5
131   33
131.5 33.5

[Fri Jan 23 17:59:01 JST 2009]
[aym@oceani18]
[~/10.Work09/13.FVCOM_Ariake2D/03.Preprocess/03.Coastline]

```
$ project aaa.txt -C130.5/32.5 -A0 -Q -N -fg -Fpq
```

0       0
55.5975 55.5975
111.195 111.195
-Qオプションをつけると出力データの単位はkmになる。

[Fri Jan 23 17:59:06 JST 2009]
[aym@oceani18]
[~/10.Work09/13.FVCOM_Ariake2D/03.Preprocess/03.Coastline]

```
$ project aaa.txt -C130.5/32.5 -A0 -N -fg -Fpq
```

0       0
0.5     0.5
1       1

カラーパレットファイル(cpt file)
2009/01/23 21:44 に aofd info が投稿   [ 2010/01/18 3:17 に A M さんが更新しました ]

 カラー図を作成するときに使う

http://users.nature.waseda.ac.jp/kiri/gmt/makecpt/makecpt.html
(GMT標準のcptファイル)
cpt-city http://soliton.vm.bytemark.co.uk/pub/cpt-city/
(サンプル多数)






#!/bin/bash
#
#/work09/am/00.WORK/2022.ECS2022/0.ECS2022.06_SUMMARY_2023-02-21/04.SOUNDING/22.12.SONDE/22.14.STN/00.12.OBS_POINT
#
. ./gmtpar.sh
gmtset ANOT_FONT_SIZE 9
gmtset HEADER_FONT_SIZE 16;gmtset HEADER_OFFSET 0.05

range2=127.9/129.5/30/31.3
size=JM2.5
xanot=a30mf10m; yanot=a30mf10m
anot=${xanot}/${yanot}WSne

in=0ECS_2022-06_ENV_PAR_STN.TXT
if [ ! -f $in ]; then echo Error in $0 : No such file, $in; exit 1; fi

fig=$(basename $0 .sh)_$(basename $in .TXT).ps

OBS_AREA="OBS_AREA.TXT"
cat <<EOF>$OBS_AREA
128 30.15
129.34 30.15
129.34 31.15
128 31.15
128 30.15
EOF

curdir1=$(pwd); now=$(date); host=$(hostname)
time=$(ls -l ${in} | awk '{print $6, $7, $8}')

CPT=seis; CPTFILE=CPTFILE.TXT
CPT2=no_green

echo MMMMM 1
makecpt -C$CPT -T334/354/1 -I -Z >$CPTFILE

awk '{if($1!="#")print $3, $2, $4}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K \
-K -P -Y8.5 -X1 >$fig
awk '{if($1!="#"&&$4=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba4f1::/:K: -O -K  >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (a)
EOF

echo MMMMM HEADER
xoffset=; yoffset=4
pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O -K >> $fig
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${in} (${time})
0 0.90  9 0 1 LM OUTPUT: ${fig} (${timeo})
EOF



echo MMMMM 2
makecpt -C$CPT2 -T-600/600/50 >$CPTFILE

awk '{if($1!="#"&&$5!="-")print $3, $2, $5}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K -O \
-K -X4 -Y-6.5 >>$fig
awk '{if($1!="#"&&$5=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba200f50::/:m@+2@+/s@+2@+: -O -K  >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (b)
EOF



echo MMMMM 3
makecpt -C$CPT -T0/120/20 -I -Z >$CPTFILE

awk '{if($1!="#"&&$6!="-")print $3, $2, $6}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K -O \
-K -X-3 -Y-6.2 >>$fig
awk '{if($1!="#"&&$6=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba20f20::/:"1/s": -O -K  >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (c)
EOF

echo MMMMM 4
makecpt -C$CPT -T4.7/5.2/0.1 -I -Z >$CPTFILE

awk '{if($1!="#"&&$7!="-")print $3, $2, $7}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K -O \
-K -X4 -Y-2.5 >>$fig
awk '{if($1!="#"&&$7=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba0.1f0.1::/:K/km: -O -K  >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (d)
EOF



echo MMMMM 5
makecpt -C$CPT -T0/600/50 -I -Z >$CPTFILE

awk '{if($1!="#"&&$8!="-")print $3, $2, $8}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K -O \
-K -X-3 -Y-6.2 >>$fig
awk '{if($1!="#"&&$8=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba100f50::/:m: -O -K  >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (e)
EOF



echo MMMMM 6
makecpt -C$CPT -T0/500/50 -I -Z >$CPTFILE

awk '{if($1!="#"&&$9!="-")print $3, $2, $9}' $in |\
psxy -R$range2 -$size -Sc0.09 -C$CPTFILE -W1 -K -O \
-K -X4 -Y-2.5 >>$fig
awk '{if($1!="#"&&$9=="-")print $3, $2}' $in |\
psxy -R$range2 -$size -Sc0.09 -W1 -K -O \
-K >>$fig

psscale -D1.25/-0.35/2/0.08h -C$CPTFILE \
-Ba100f50::/:g/m@+2@+/s: -O -K  >>$fig

awk '{if($1!="#")print $3,$2," 6 0 1 LM ",$1}' $in |\
pstext -R$range -$size -D0.03/0.05 -O -K >>$fig
psxy $OBS_AREA -R -$size -B$anot -W3 -A -O -K >>$fig

pstext <<EOF -JX2.5/0.1 -R0/1/0/1 -N -X-0.5 -Y2.5 -O -K >> $fig
0 0.2 12 0 1 LB (f)
EOF


PDF=$(basename $fig .ps).pdf; ps2pdfwr $fig $PDF
if [ $? -eq 0 ];then rm -vf $fig; fi
echo
echo "INPUT : "
ls -lh --time-style=long-iso $in
echo "OUTPUT : "
ls -lh --time-style=long-iso $PDF
echo

echo "Done $0"

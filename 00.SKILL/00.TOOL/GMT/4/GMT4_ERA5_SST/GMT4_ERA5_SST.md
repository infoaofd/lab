# ERA5のSSTをGMT4で描画

[[_TOC_]]

<img src="image-20230827212706776.png" alt="image-20230827212706776" style="zoom:33%;" />

## CDOで変数切り出しとテキストに変換

CDO.ERA5.DAILY.sh

```bash
INDIR=/work01/DATA/ERA5/ECS/01HR/2022/06
INFLE=ERA5_ECS_SFC.FLUX_01HR_20220619.grib
IN=$INDIR/$INFLE

ODIR=/work01/DATA/ERA5/ECS/1DAY/2022/06
OFLE=ERA5_SST_20220619.nc
OUT=$ODIR/$OFLE

ASC=$(basename $OUT .nc).TXT

mkd $ODIR

rm -vf $OUT

TMP1=$(basename $IN .grib)_TMP
cdo -f nc4 splitparam  $IN $TMP1
ls *nc |  grep -v -E 'nc34.128' | xargs rm -vf

TMP=$(ls *nc34.128*)
echo $TMP

if [ ! -f $TMP ];then echo EEEEE ERROR NO SUCH FILE,$TMP;exit 1;fi

echo
cdo -f nc4 select,name=var34 -daymean  $TMP $OUT
if [ $? -eq 0 ];then
  echo ;echo OUT: $OUT
#  cdo sinfo $OUT

  echo
  cdo gmtxyz $OUT|awk '{if($3>-500)print $1,$2,$3}' >$ASC
  echo ASCII OUT: $ASC;echo
  exit 0
else
  echo EEEEE CDO ERROR.; exit 1
fi
```



## GMT4で描画

GMT4_TEST.ERA5.SST.sh

```bash
#!/bin/bash

. ./gmtpar.sh
echo "Bash script $0 starts."

range=121/132/20/34
size=M5
xanot=a5f1
yanot=a5f1
anot=${xanot}/${yanot}WSne
infle=ERA5_SST_20220619.TXT
in=$infle
if [ ! -f $in ]; then echo Error in $0 : No such file, $in;exit 1;fi

out=$(basename $in .TXT).ps; pdf=$(basename $in .TXT).pdf
GRD=$(basename $in .TXT).NC

awk '{print $1,$2,$3-273}' $in |\
surface -R$range -T0.8 -I0.25/0.25 -G$GRD

pscoast -R -J$size -B$anot -Di -G0 -P -X1.5 -Y2 -K >$out
grdcontour $GRD -R$range -J$size -B$anot -C1 -A2f12 -O -K >>$out

xoffset=0; yoffset=6.5

curdir1=$(pwd); now=$(date -R); host=$(hostname)
time=$(ls -l  --time-style=long-iso  ${in} | awk '{print $6, $7}')
timeo=$(ls -l  --time-style=long-iso ${out} | awk '{print $6, $7}')

pstext <<EOF -JX6/1.5 -R0/1/0/1.5 -N -X${xoffset:-0} -Y${yoffset:-0} -O >> $out
0 1.50  9 0 1 LM $0 $@
0 1.35  9 0 1 LM ${now}
0 1.20  9 0 1 LM ${curdir1}
0 1.05  9 0 1 LM INPUT: ${in} (${time})
0 0.90  9 0 1 LM OUTPUT: ${out} (${timeo})
EOF

echo
echo "INPUT : "
ls -lh --time-style=long-iso $in

if [ -f $out ];then
ps2pdf $out $pdf
if [ $? -eq 0 ];then
rm -vf $out $GRD
echo "OUTPUT : "
ls -lh --time-style=long-iso $pdf
echo
fi
fi
```


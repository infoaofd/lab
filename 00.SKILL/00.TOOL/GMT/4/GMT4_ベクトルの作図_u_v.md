# ベクトルの作図 (u, v)

x,y, u, vのデータを与えて, ベクトル図を作図する。x,yは緯度経度, u, vは風ベクトルの東西, 南北成分

pl.u10.sh

```bash
#!/bin/bash

. ./gmtpar.sh

range=119/131/19/31
size=5
xanot=a5f1; yanot=a5f1

anot=${xanot}/${yanot}WSne
indir=output/Meari2011_R02.04/Meari2011_R02.04_u10.ascii.out.ncl

in=$indir/Meari2011_R02.04.d02.u10.ascii.out.020.txt
if [ ! -f $in ]; then
  echo Error in $0 : No such file, $in
  exit 1
fi
figdir="./fig/"
if [ ! -d ${figdir} ];then
  mkdir -p $figdir
fi
out=${figdir}$(basename $in .asc).ps

echo Input : $in
echo Output : $out

PI=3.1415926535

pscoast -R$range -JM$size -W1 -G200 -Di -K -X1.5 -Y3 -P > $out

# 10 m/s => 0.1 inch 

scl=0.1; legmag=10
fac=$(echo "scale=5; $scl/$legmag" | bc)

# column 1=x, column 2=y, column 3=u, column 4=v

awk -v f=$fac -v pi=${PI} '{if($1!="#") \
printf "%12.6f %12.6f %12.6f %12.6f\n", \
$1,$2,(180.0/pi)*atan2($4,$3), sqrt($3*$3+$4*$4)*f}' $in| \
psxy -R$range -JM$size -Sv0.01/0.1/0.05n0.2 -G0 \
-B$anot \
 -K -O >> $out
```



gmtpar.sh

```bash
gmtset MEASURE_UNIT INCH
gmtset LABEL_FONT_SIZE 18
gmtset HEADER_FONT_SIZE 20
gmtset ANOT_FONT_SIZE 18
gmtset TICK_PEN 4
gmtset BASEMAP_TYPE PLAIN
gmtset PAPER_MEDIA A4
gmtset DEGREE_SYMBOL = degree # Available for ver. 4 or later

sp="\040"  # White space
eq="\075"  # equal
deg="\260" #deg="\312"  # degree symbol

red="255/0/0"
orange="255/165/0"
pink="255/192/203"
green="0/255/0"
blue="0/0/255"
midnightblue="25/25/112"
yellow="255/255/0"
gold="255/215/0"
purple="160/32/240"
magenta="255/0/255"
white="255/255/255"

dash="t15_5:15"
dot="t5_5:5"
dotdash="t12_5_5_5:5"

# -W5t20_5:5
# -W5t15_5:5
# -W5t20_5_5_5_5_5:5
# -W5t30_5_5_5:5
# -W5t20_5_10_5:5

gmtset D_FORMAT %lg 
#gmtset D_FORMAT %12.6f
#gmtset D_FORMAT %12.5g
```


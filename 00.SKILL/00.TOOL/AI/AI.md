# AI

Adobe Firefly（画像生成、ロゴやイラスト作成、デザイン提案）
https://www.adobe.com/sensei/generative-ai/firefly.html
Canva AI（デザイン提案、ロゴやイラスト作成、画像生成）
https://www.canva.com/
ChatGPT（文章生成、アイデアブレインストーミング、学習アシスタント、カスタマーサポートチャットボット作成）
https://chatgpt.com/
Claude（文章生成、アイデアブレインストーミング、学習アシスタント）
https://claude.ai/
DALL·E（画像生成）
https://openai.com/dall-e-2/
DeepL（翻訳）
https://www.deepl.com/
ElevenLabs（音声合成）
https://elevenlabs.io/
Elicit（文献調査、学習アシスタント）
https://elicit.com/
Figma AI（デザイン提案）
https://www.figma.com/
GitHub Copilot（プログラムコード生成）
https://github.com/features/copilot
Google Gemini（文章生成、アイデアブレインストーミング、学習アシスタント）
https://gemini.google.com/
Grammarly（文法チェック）
https://www.grammarly.com/
IBM Watson NLU（感情分析、文献調査）
https://www.ibm.com/cloud/watson-natural-language-understanding
Intercom（カスタマーサポートチャットボット作成）
https://www.intercom.com/
Looka（ロゴやイラスト作成）
https://looka.com/
Medallia（感情分析、文献調査）
https://www.medallia.com/platform/text-analytics/
Microsoft Copilot（文章生成、アイデアブレインストーミング、学習アシスタント）
https://copilot.microsoft.com/
MidJourney（画像生成）
https://www.midjourney.com/
NotebookLM（文献調査、学習アシスタント）
https://notebooklm.google/
音読さん（音声合成）
https://ondoku3.com/ja/
Otter.ai（文字起こし）
https://otter.ai/
Perplexity AI（文献調査、学習アシスタント）
https://www.perplexity.ai/
Power BI AI（データ可視化）
https://powerbi.microsoft.com/
Quillbot（文法チェック、文章生成）
https://quillbot.com/
Quizlet（学習アシスタント）
https://quizlet.com/
Runway ML（動画生成、画像生成）
https://runwayml.com/
Stable Diffusion（画像生成）
https://stability.ai/stable-image
Synthesia（動画生成、音声合成）
https://www.synthesia.io/
Tableau AI（データ可視化）
https://www.tableau.com/
Voicevox（音声合成）
https://voicevox.net/
Whisper（文字起こし）
https://openai.com/whisper/
Zendesk AI（カスタマーサポートチャットボット作成）
https://www.zendesk.com/
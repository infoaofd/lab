# gifアニメーションを作る

Linuxのconvert コマンドを使う

http://www.gfd-dennou.org/library/cc-env/make_animation/convert.htm

## 例1

カレントディレクトリにあるPDFファイルすべてをつなげてアニメーションにする。

```bash
convert *.PDF ANIM.gif
```



## 例2

速すぎる動画を遅くする　(delayの設定)

```bash
convert -delay 100 -loop 0   *.PDF ANIM.gif
```

-delay:  画像の切り替わる時間（単位は1/100秒）
-loop: ループする回数（0なら無限）

function [CAPE,CIN,LFC,EL]=compute_CAPE_AND_CIN(T0,p0,q0,start_loc,fracent,prate,z0,T1,T2)

    %this function computes CAPE and CIN
    
    %input arguments
    %T0: sounding profile of temperature (in K)
    %p0: sounding profile of pressure (in Pa)
    %q0: sounding profile of water vapor mass fraction (in kg/kg)
    %start_loc: index of the parcel starting location (set to 1 for the
    %lowest: level in the sounding)
    %fracent: fractional entrainment rate (in m^-1)
    
    %CONSTANTS
    Rd=287.04; %dry gas constant
    Rv=461.5; %water vapor gas constant
    epsilon=Rd./Rv;   
    g=9.81; %gravitational acceleration
    
    %compute lifted parcel buoyancy
    [T_lif,Qv_lif,Qt_lif,B_lif]=lift_parcel_adiabatic(T0,p0,q0,start_loc,fracent,prate,z0,T1,T2);
    
    %CAPE will be the total integrated positive buoyancy
    B_pos = B_lif;
    B_pos(find(B_pos<0)) = 0;
    dz = z0(2:length(z0)) - z0(1:length(z0)-1);
    CAPE = sum( 0.5.*B_pos(1:length(z0)-1).*dz + 0.5.*B_pos(2:length(z0)).*dz );
    
    %CIN will be the total negative buoyancy below the height of maximum
    %buoyancy
    B_neg = B_lif;
    [mx,imx]=max(B_lif);
    B_neg(1:imx)=min( B_neg(1:imx), 0 );
    B_neg(imx+1:length(z0))= 0;
    CIN = sum( 0.5.*B_neg(1:length(z0)-1).*dz + 0.5.*B_neg(2:length(z0)).*dz );
    
    %LFC will be the last instance of negative buoyancy before the
    %continuous interval that contains the maximum in buoyancy
    fneg = find(B_lif<0);
    fneg = fneg(find(fneg<imx));
    LFC = 0.5*z0(max(fneg)) + 0.5*z0(max(fneg)+1);
    
    %EL will be last instance of positive buoyancy
    fpos = find(B_lif>0);
    EL = 0.5*z0(max(fpos)) + 0.5*z0(max(fpos)+1);

end
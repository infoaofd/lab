function [data_new] = comp_deriv(data,dim,dx)
    
    [xlen,ylen,zlen]=size(data);
    data_new=zeros(size(data));
    
    switch dim
        
        case 1
            data_new(2:xlen-1,:,:)=(data(3:xlen,:,:)-data(1:xlen-2,:,:))/(2*dx);
            data_new(1,:,:)=2*data_new(2,:,:)-data_new(3,:,:);
            data_new(xlen,:,:)=2*data_new(xlen-1,:,:)-data_new(xlen-2,:,:);
        case 2
            data_new(:,2:ylen-1,:)=(data(:,3:ylen,:)-data(:,1:ylen-2,:))/(2*dx);
            data_new(:,1,:)=2*data_new(:,2,:)-data_new(:,3,:);
            data_new(:,ylen,:)=2*data_new(:,ylen-1,:)-data_new(:,ylen-2,:);
        case 3
            data_new(:,:,2:zlen-1)=(data(:,:,3:zlen)-data(:,:,1:zlen-2))/(2*dx);
            data_new(:,:,1)=2*data_new(:,:,2)-data_new(:,:,3);
            data_new(:,:,zlen)=2*data_new(:,:,zlen-1)-data_new(:,:,zlen-2);
    end
    
end


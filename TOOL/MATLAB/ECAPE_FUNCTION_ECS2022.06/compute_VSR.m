function V_SR = compute_VSR(z0,u0,v0)
    %compute 0-1 km storm-relative flow (V_SR) using the storm motion
    %estimate of Bunkers et al. (2000)
    %https://doi.org/10.1175/1520-0434(2000)015<0061:PSMUAN>2.0.CO;2
    
    f6000=find(z0<=6000);
    meanx=mean(u0(f6000));
    meany=mean(v0(f6000));
    
    f0500=find(z0<=500);
    lowx=mean(u0(f0500));
    lowy=mean(v0(f0500));
    
    f560=find(z0<=6000&z0>=5500);
    highx=mean(u0(f560));
    highy=mean(v0(f560));
    BK_SHRx=highx-lowx;
    BK_SHRy=highy-lowy;
    BK_mag=sqrt(BK_SHRx.^2 + BK_SHRy.^2);
    BK_dirx=BK_SHRx./BK_mag;
    BK_diry=BK_SHRy./BK_mag;
    BK_orthx=BK_diry*7.5;
    BK_orthy=-BK_dirx*7.5;


    C_x=meanx+BK_orthx;
    C_y=meany+BK_orthy;
    
    u_sr = u0 - C_x;
    v_sr = v0 - C_y;
    
    f1000=find(z0<=1000);
    V_SR = nanmean(sqrt(  u_sr(f1000).^2 + v_sr(f1000).^2  ));
    
end
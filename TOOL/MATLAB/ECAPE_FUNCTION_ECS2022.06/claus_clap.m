function [es] = claus_clap(T)
T=T-273.15;
es=100*6.1094*exp((17.625*T)/(T + 243.04));
end


# 複数のファイルの一部を一括して変更する

例

### 対象ファイルの確認

```bash
$ ls *sh
```

```
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh*
12.03.P1D_13-15_0000_EACH.sh*
.....
```



### 変更前の確認

```bash
$ find *sh |xargs grep 00Z13
```

```
/work09/am/00.WORK/2022.RW3A/24.12.WRF.RAIN/12.04.1DAY_ACCUMRATED_13-15
$ find *sh |xargs grep 00Z13
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh:'R3D1=RAINNC.1(time=00Z13AUG2021)-RAINNC.1(time=00Z12AUG2021)'
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh:'R3D2=RAINNC.2(time=00Z13AUG2021)-RAINNC.2(time=00Z12AUG2021)'
...
```

### 変更

```bash
$ find *sh |xargs sed -i "s/00Z13/00Z15/g"
```

### 変更後の確認

```
find *sh |xargs grep 00Z15
```

```
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh:'R3D1=RAINNC.1(time=00Z15AUG2021)-RAINNC.1(time=00Z12AUG2021)'
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh:'R3D2=RAINNC.2(time=00Z15AUG2021)-RAINNC.2(time=00Z12AUG2021)'
12.02.P1D_13-15_ENSEMBLE_MEAN_0000.sh:'R3D3=RAINNC.3(time=00Z15AUG2021)-RAINNC.3(time=00Z12AUG2021)'
```

### 確かに00Z13がなくなっているか確認

```
$ find *sh |xargs grep 00Z13
```


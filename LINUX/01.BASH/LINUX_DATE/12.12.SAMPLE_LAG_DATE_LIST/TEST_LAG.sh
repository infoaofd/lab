#!/bin/bash

#ラグコンポジット作成用に一定時間前の時刻リストを作成する

INDIR=22.00.AVE_R1D.ge.1mm_QFLUX.ge.ave
INFLE=N-KYUSHU_R3HMAX.ge.ave.txt
IN=$INDIR/$INFLE

LAGHR=6
LAGPM=MINUS #PLUS
LAG=${LAGPM}${LAGHR}

OUT=$(basename $INFLE .txt)_${LAG}.txt

N=0
while read BUF ; do
  ary=(`echo $BUF`)   # 配列に格納
  Y=${ary[0]}; M=${ary[1]}; D=${ary[2]}; H=${ary[3]}

  YMDH="${Y}/$(printf %02d $M)/$(printf %02d $D) $(printf %02d $H):00"

  if [ $LAGPM = "PLUS" ];then
  date_out=$(date -d"${YMDH} ${LAGHR} hour" +%Y%m%d%H)
  fi
  if [ $LAGPM = "MINUS" ];then
  date_out=$(date -d"${YMDH} ${LAGHR} hour ago" +%Y%m%d%H)
  fi

  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}; hh=${date_out:8:2}

  echo $YMDH $yyyy $mm $dd $hh
  echo $yyyy $mm $dd $hh >>$OUT

  N=$(expr $N + 1 )
done < $IN

echo;echo OUT: $OUT; echo


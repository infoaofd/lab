ymdh1=$1; ymdh2=$2; dh=$3

ymdh1=${ymdh1:-2021081201}; ymdh2=${ymdh2:-2021081203}
dh=${dh:-1}

yyyy1=${ymdh1:0:4};mm1=${ymdh1:4:2};dd1=${ymdh1:6:2};hh1=${ymdh1:8:2}
yyyy2=${ymdh2:0:4};mm2=${ymdh2:4:2};dd2=${ymdh2:6:2};hh2=${ymdh2:8:2}

start=${yyyy1}/${mm1}/${dd1}; end=${yyyy2}/${mm2}/${dd2}

jsstart=$(date -d${start} +%s);   jsend=$(date -d${end} +%s)
jdstart=$(expr $jsstart / 86400); jdend=$(expr   $jsend / 86400)

nday=$( expr $jdend - $jdstart + 1)

i=1
while [ $i -le $nday ]; do
  im1=$(expr $i - 1)
  date_out=$(date -d"${yyyy1}/${mm1}/${dd1} ${im1}day" +%Y%m%d)
  yyyy=${date_out:0:4}; mm=${date_out:4:2}; dd=${date_out:6:2}

  if [ $i -eq $nday ]; then
     hs=0; he=$hh2
  elif [ $i -eq 1 ]; then
     hs=$hh1; he=23 
  elif [ $nday -eq 1 ]; then
     hs=$hh1; he=$hh2
  else
     hs=0; he=23
  fi

  h=$hs
  while [ $h -le $he ]; do
    hh=$(printf %02d $h)
    h=$(expr $h + $dh )
    echo $yyyy $mm $dd $hh 
  done

  i=$(expr $i + 1)
done

### bcコマンドによる実数の計算

#### 基本

```bash
L=$(echo 2*$1*3.14" | bc -l)
echo $L
```

#### bcコマンドにおける有効数字の取り扱いの注意事項

https://qiita.com/h-pod/items/d8b98b00538d292e0c81

#### bcコマンドの書式指定

printfと組み合わせる(下記)

```bash
$ FAC=$(echo "1.0/7.0" | bc -l | xargs printf "%.4f\n"); echo $FAC
0.1429
```

上記場合小数点以下4桁で出力される。
# LINUXグループの変更

[[_TOC_]]

## グループ情報の確認

```bash
id ma
uid=1011(ma) gid=1011(ma) groups=1011(ma)
```

## グループの作成

```bash
sudo groupadd test-group
```

## 作成されたグループの確認

```bash
cat /etc/group |grep test
test-group:x:1012:
```

## 副グループの追加

```bash
sudo usermod -G test-group ma
```

## 追加された副グループの確認

```bash
id ma
uid=1011(ma) gid=1011(ma) groups=1011(ma),1012(test-group)
```

## 主グループの変更

```bash
sudo usermod -g test-group ma
```

時間がかかる場合がある

## 変更された主グループの確認

```
uid=1011(ma) gid=1012(test-group) groups=1012(test-group)
```

## 主グループをデフォルト設定 (今の場合ma=ユーザー名)に戻す

```
sudo usermod -g ma ma
```

## 主グループがデフォルト設定に戻ったか確認

```
uid=1011(ma) gid=1011(ma) groups=1011(ma),1012(test-group)
```

## グループの削除

```
sudo groupdel test-group
```

## グループが削除されたか確認

```
cat /etc/group |grep test
```



